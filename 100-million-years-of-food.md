---
id: 58b42aad8566cb0004c1f455
slug: 100-million-years-of-food-en
published_date: 2017-03-03T00:00:00.000+00:00
author: Stephen Le
title: 100 Million Years Of Food
subtitle: What Our Ancestors Ate and Why It Matters Today
main_color: D9AF79
text_color: 735D40
---

# 100 Million Years Of Food

_What Our Ancestors Ate and Why It Matters Today_

**Stephen Le**

_100 Million Years of Food_ (2016) is about the foods our ancestors ate and how that diet relates to our eating habits today. These blinks will take you way back in time to explore the evolution of eating. They'll explain that, while there's no one-size-fits-all diet, there are a few general rules to abide by.

---
### 1. What’s in it for me? Learn about our complicated history with food. 

Something is clearly wrong with our eating habits. Today, diet-related diseases plague the Western world. More people suffer from type 2 diabetes, obesity and food allergies, not to mention many forms of cancer, than ever before.

But what exactly is wrong with how we're eating — and what should we be doing instead? The diets of our ancestors provide a few answers. They were generally much healthier than modern-day humans, so it's no wonder that the last decade has witnessed a major uptick in _caveman_ or _paleo_ diets.

But to get a full picture we have to go back even further in time — starting with our earliest, tree-dwelling ancestors 100 million years ago and exploring how these forebears adapted to changing environments and diets. These blinks discuss whether the dietary choices of these distant progenitors would work for us today.

On this journey through human evolution, you'll learn

  * why we should eat some bugs every now and then;

  * about a strange illness that affected mainly the well-off; and

  * why milk isn't nearly as healthy as you may think.

### 2. The insect- and fruit-based diets of our early ancestors wouldn’t work for us today. 

If one of our first ancestors walked into one of today's supermarkets, he'd be stunned by the options. After all, the difference between the overflowing shelves of contemporary grocery stores and the dinnertime options available to our ancient ancestors could hardly be greater.

Our earliest ancestors, who emerged around 100 million years ago, lived in the trees of tropical forests and primarily ate insects. That might sound gross to us now, but insects are actually a calorie-rich source of vitamins and iron.

In fact, insects would still make a great addition to the modern human diet. But for us to attempt to live _only_ on bugs wouldn't be so smart. Our ancestors had enzymes that allowed them to break down the exoskeletons of insects, which are made of chitin, a substance that we can no longer digest. Another problem with eating bugs is that they can trigger allergies and produce harmful toxins.

But in moderation, the consumption of insects would be a great boon to modern food production. For instance, crickets produce about 50 percent less carbon dioxide than cows per pound and convert feed into calories 12 times more efficiently.

Nonetheless, our ancestors transitioned away from creepy crawlies around 60 million years ago. Around this time, the climate began cooling and, as the air grew more humid, the first fruit-bearing trees emerged.

During the same period, our ancestors lost the ability to synthesize vitamin C, which is essential to preventing cell damage. They only survived this change because they could get plenty of vitamin C from fruit.

So around 30 million years ago our ancestors became full-time fruit eaters. However, eating too much fruit can also be bad since fruit contains fructose, something our body can only metabolize so much of; overconsumption can lead to insulin resistance and pancreatic cancer.

The actor Ashton Kutcher learned this the hard way. When preparing to portray Steve Jobs, Kutcher followed the tech CEO's fruitarian diet for a month. After just 30 days, Kutcher was hospitalized with pancreatic issues.

### 3. Meat, with both its benefits and costs, has played a major part in the history of the human species. 

About two million years ago, our ancestors began migrating out of the trees and adapting to a terrestrial lifestyle. They started to look more human and their diets changed, too.

Around the same time, these early humans began hunting and foraging, eating more meat than they ever had before. As a result, their brains started growing rapidly.

In fact, the brain size of our ancestors doubled over the course of just one million years, a change that may well have been the result of their newly carnivorous diets. Meat, packed with important fatty acids, is the perfect fuel for a growing brain.

And big brains also gave our ancestors an evolutionary advantage. Smarter, more coordinated groups of hunters could bring home more prey, which meant their families were in turn more likely to survive and reproduce.

But while meat has plenty of benefits, eating too much of it is bad for our health. Meat is loaded with protein, which we can only tolerate in limited quantities.

When the human body digests proteins, it produces potentially toxic substances called _nitrogen compounds_. If a person gets more than 40 percent of their daily calories from protein, the levels of these compounds get too high.

Another reason excess meat consumption can be detrimental to our health is that it contains a lot of cholesterol, which can combine with other substances and clog up our arteries. But cholesterol isn't _all_ bad. It's a precursor to essential sex hormones like testosterone and estrogen, and it also increases the level of _high-density lipoprotein_, or HDL, which is good for our moods.

Our livers and intestines produce most of the body's cholesterol on their own, but animal products like meat and dairy provide additional inputs that affect our hormone levels. As a result, girls on a cholesterol-rich diet reach sexual maturity earlier. That means they can procreate earlier, potentially producing more offspring, but they also have reduced life expectancies.

> _"As it turns out, there is some scientific justification behind the connection between meat, mood, and sex."_

### 4. Some cultures embraced meat substitutes, but they weren’t all healthy. 

Nowadays, with all the choices available to us, it's easy to eat a balanced vegetarian diet. But alternatives to red meat are nothing new. Our meat-loving ancestors even branched out. For instance, in many cultures, fish predominated, although not all people developed a taste for it.

Here's how it happened:

In many areas where meat was hard to come by, locals embraced fish as an accessible and nutritious food. This was a smart choice since, as we now know, fatty fish contain loads of healthy omega-3 fatty acids and vitamin D, which is key to bone health.

However, not every culture with access to fish chose to eat it. This bias wasn't merely due to the difficulties of eating fish (all those pesky bones!); there were also cultural reasons. Some cultures regarded fish as a sacred animal that lived in a sacred element. Others, like the Apache Indians, considered fish to be unclean.

Another important meat substitute emerged around 8,000 years ago: animal milk. Though it's often still regarded as an elixir of sorts, animal milk isn't as healthy for human consumption as some think.

The Northern European nations were among the first people to consume animal milk, which has innumerable benefits. It's nutritious, rich in calcium and whereas an animal can only be eaten once, it can be milked many times over.

Statistics link milk consumption to increased growth in children, but this boost in height may come at the price of bone health. This correlation can be seen in the nations with the highest dairy intake; the citizens of these nations, because of their above-average height, also experience the highest rates of hip fractures in the world.

A further issue with milk is that people from regions with little history of dairy consumption absorb calcium more efficiently. This means that if a man from, say, Africa drinks a lot of milk, he might experience dangerously elevated calcium levels. This is especially risky since research has linked high blood-calcium levels to prostate cancer.

Next up we'll explore the history of vegetables and their place in the nutritional hierarchy.

### 5. Humans only began eating plants out of necessity. 

All parents have insisted, at one time or another, that their kid eat her veggies. After all, veggies are healthy and packed with nutrients, right? Well, the truth is, most plants are actually unhealthy and many are even dangerous.

Why?

Well, plants are surrounded by other organisms that want to eat them and they have no way to run. Since they can't escape, they have to defend themselves. They do this by engaging in chemical warfare: producing chemicals that discourage, harm and sometimes kill the animals that try to eat them.

For instance, vegetables like squash and cucumbers can contain a bitter substance known as _cucurbitacin_ to discourage consumption, and it's only the domesticated versions where the excessive bitterness can be bred out. Or take beans, lentils and soybeans, which contain a group of chemicals called _lectins_. If you eat too many of them, you'll get sick and you might suffer liver damage.

In fact, one of the most lethal poisons known to man is a type of lectin. It's called _ricin_ and it's found in the seeds of the castor oil plant. Even a tiny amount of ricin is enough to cause a painful death.

So why did humans begin eating plants in the first place?

Well, the truth is, we turned to farming and eating plants because the other food options became scarce. This turn toward agriculture occurred around 12,000 years ago in many parts of the world simultaneously.

There are lots of theories as to why this happened, but it was primarily caused by the extinction of large, delicious animals such as the mammoth. These creatures likely died due to overhunting by humans and the spread of trees into the grasslands they called home.

With the loss of this major food source, humans began looking for other options. They settled on plants because they were readily available and could be easily produced. From that point on, plant-based foods dominated in densely populated areas and places where animals were hard to keep.

### 6. Rapid changes in diet and lifestyle have brought on new diseases. 

The human body is surprisingly adaptable and we can get used to big dietary changes. But doing so can take many generations. The slow nature of this process posed a problem when food processing began to become the norm.

Humans, with little time to adapt to this change, suddenly faced a variety of new illnesses. For instance, in the second half of the nineteenth century, a frightening disease called _beriberi_ emerged among the richer people of East and Southeast Asia. The affected patients presented with heart issues, mobility problems and mental confusion.

In the end, it was discovered that the illness was caused by a severe B1 deficiency. Wealthier people were affected because they could afford the "better" highly polished rice, which had been stripped of most of its B1.

Then, around 1900, _pellagra_ became endemic among poorer populations of the American South. These groups had been subsisting on products made from industrially milled corn, which, compared to fresh corn, is extremely low in vitamin B3. The disease caused horrible symptoms like red lesions, weakness and even dementia.

Obviously, processed food is an issue, but it's not the only problem. Other lifestyle changes can also be detrimental to our health. Just take asthma and food allergies, both of which are on the rise. There's some debate about why, but it's likely that lifestyle decisions are to blame.

After all, most people spend lots of time indoors. As a result, we're not exposed to much sunlight, and suffer lower levels of vitamin D, also known as the "sunshine vitamin." This in turn causes human allergies to rise as pregnant women with reduced vitamin D levels are more likely to give birth to allergic children.

If you don't buy that theory, consider the _hygiene hypothesis_. It postulates that modern day kids develop allergies and asthma because they grow up too clean. The idea is that a child's immune system needs to be exposed to _some_ germs to learn how to discern harmless proteins from deadly bacteria and fight infections without unnecessarily harming the body.

### 7. A few extra pounds might not be such a bad thing after all and caloric intake doesn’t explain your weight. 

Did you know that Japanese people eat an average of 300 fewer calories per capita than Americans? That sounds healthy, but is it worth following their example?

Actually, there are upsides and downsides to limiting your caloric intake. For instance, the Japanese live longer than Americans, but that's not proof that their way is better.

Eating too few calories can deprive your brain of fuel and might cause you to lose focus. Not just that, but consuming too little protein for a period will cause muscle weakness.

So it's a question of trade-offs. For instance, lots of animals, when going through a period of food scarcity, will reduce inessential bodily functions, like reproduction. And the same goes for humans: women who eat fewer calories will live longer, but they'll be less fertile and more irritable.

In fact, even if you're overweight, counting every calorie isn't a good idea. For starters, it's actually healthier to be slightly overweight.

For example, people who are a few pounds heavier than average, those with a body mass index between 25 and 30 live longer than people of normal weights. This could be because heavier people have more fat to protect them from toxins and more energy to compensate for weight lost during periods of severe illness.

But even if it weren't healthy to have a few extra pounds, the link between weight and caloric intake is actually weaker than you might expect. The truth is, research has found that slim, modern-day hunter-gatherers eat about as many calories as your average contemporary American and engage in comparable levels of physical activity. The only difference is they have a greater variance in caloric intake over the seasons.

Simply put, not all differences in body weight can be chalked up to calories and exercise.

And finally, paying attention to your total calories, without considering what type of food they come from, is useless. If all your calories come from junk food and soda, it doesn't matter how few of them you consume.

### 8. Dietary needs vary from person to person but eating can and should be a communal activity. 

Imagine your friend invites you out to lunch at a buffet but you're trying to eat healthily. Are you better off going for a bowl of oatmeal, some meatballs or just sticking with Prosecco? The answer is, it depends.

After all, most foods and beverages aren't simply good or bad. Everyone is different and, to determine which foods are good for _you_, you need to consider your age, heritage and how much of any given thing you'll eat.

Just take alcohol. If you drink too much, you'll damage your brain and gut. However, studies have found that moderate consumption of alcohol in people over the age of forty helps to fight coronary heart disease.

That being said, lots of people of Asian descent need to be cautious when drinking. That's because they're genetically predisposed to produce lower levels of _alcohol dehydrogenase_, the enzyme your body needs to break down alcohol in your stomach. As a result, more alcohol enters the bloodstream of Asian people per drink than enters the bloodstream of their Caucasian peers, causing them to become more intoxicated.

Similarly, as you learned in a previous blink, girls who eat lots of meat reach sexual maturity more quickly and are therefore at increased risk of certain cancers. However, for elderly women, early onset puberty is obviously a non-issue and eating more meat will likely increase their strength.

So dietary needs vary dramatically from person to person, but that doesn't mean food should be a private affair. Eating today can be a lonely activity. But remember that our ancestors used to hunt together and share their food communally. This served to strengthen community bonds and ensured that everyone got a fair share.

To reap these benefits, we should make meals more communal. Doing so can be as easy as sharing more meals with friends or endorsing pay-what-you-can restaurants. By taking such actions, we can once again make eating an area of life in which people care for one another.

### 9. Final summary 

The key message in this book:

**The human diet has evolved dramatically over the past few million years. By following this evolutionary trail, we can understand the factors that shape our modern diets. While there's no general dietary prescription that applies to all people, there are some guidelines that might improve both health and happiness.**

Actionable advice:

**Sell your car to save your health.**

Everyone knows that an active lifestyle is a healthy lifestyle. But as long as we have the choice, we tend to stick with what's comfortable. So consider selling your car and forcing yourself to use modes of transportation that require some exercise.

If you're not sure it's a good move for your health, just consider the residents of certain mountainous islands, on which it was impractical to build roads. Since they never had roads, traveling by car was never an option. These islanders get around on foot and by bike and, in the end, they live longer, healthier lives than their mainland peers.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Consider the Fork_** **by Bee Wilson**

Eating and cooking have always been crucial to our survival, but over time they have also become a subject of cultural and scientific interest. In _Consider the Fork_ (2012), author Bee Wilson blends history, anthropology and technology to tell the fascinating story of the evolution of cooking, while also taking a closer look at the creation of cooking tools and how they have shaped our culture and eating behavior.
---

### Stephen Le

Stephen Le is a visiting professor of Biology at the University of Ottawa. He holds a PhD in biological anthropology from UCLA. This is his first best seller.

