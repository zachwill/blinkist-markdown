---
id: 54d89702383963000a4f0000
slug: 100-plus-en
published_date: 2015-02-11T00:00:00.000+00:00
author: Sonia Arrison
title: 100+
subtitle: How the Coming Age of Longevity Will Change Everything, from Careers and Relationships to Family and Faith
main_color: A7DDE7
text_color: 4A6266
---

# 100+

_How the Coming Age of Longevity Will Change Everything, from Careers and Relationships to Family and Faith_

**Sonia Arrison**

_100+_ explains the science and technology that will help us lead longer and healthier lives, and considers how society will handle a rapidly aging population. The implications of the forthcoming demographic shift are massive, and big changes lie ahead. _100+_ became a best seller and one of the _Financial Times_ ' best books of 2012.

---
### 1. What’s in it for me? Learn how society will cope when we all start living longer. 

The average human life span has climbed steadily over the last two centuries: from 29 in the early nineteenth century to nearly 70 today. As technological and scientific developments continue to unroll, this trend can only increase in the future.

Yet if we all keep living longer and longer, there will — of course — be many societal effects. The global population will increase, there will be more elderly people who need looking after than ever before, and a larger workforce will face economic problems.

How will we cope? In these blinks we discover that our worst fears about this new world are largely false. Even though we will all live to a ripe old age, society will cope and even blossom.

In these blinks you'll discover

  * why the first person to live to 1,000 may already live amongst us;

  * why we might have to cope with a younger brother 50 years our junior; and

  * how you can "hack" genes.

### 2. Throughout history, humans have tried to understand mortality and aging, and to overcome them. 

What separates humans from animals? Most would say that it's the awareness that we're going to die someday. This knowledge of our fate — and the motivation to change it — has occupied human thought for millennia.

Death used to be understood by the ancients as punishment from the gods for human transgression or immorality. In Ancient Greece it was believed that our mortality, illness and suffering was Zeus' retribution for accepting the gift of fire from the titan Prometheus.

Our ancestors devoted a lot of time to trying to cheat this fate.

People in ancient and medieval civilizations were preoccupied with slowing down or even stopping the aging process. For instance, although the practice of alchemy was famously concerned with turning base metals into gold, alchemists were also fixated on immortality and elixirs that would extend our lives.

Yet despite their attempts to transcend death, our ancestors were also aware that becoming immortal might not always be desirable.

A great example of this is found in Jonathan Swift's _Gulliver's Travels_. Gulliver visits the land of Luggnagg, where a small minority known as "struldbrugs" are born immortal. At first Gulliver delights in this fact, but then slowly realizes that it's a curse. The struldbrugs enjoy a wonderful youth, but when they age, their health deteriorates as they lose their teeth and hair, and they are unable to speak. They end up embittered and detached from society.

So our ancestors desired immortality, yet dreaded compromising their health as a result. In the next blink we'll see how modern science is tackling this issue by both preventing aging and keeping us healthier for longer.

> _"I don't want to achieve immortality through my work; I want to achieve it by not dying." — Woody Allen._

### 3. New technology will dramatically advance, not only in our life spans, but our health spans. 

Most of us want to live long lives, but many of us don't want to do so in pain or infirmity. Fortunately, modern science is finding ways to alleviate this dilemma by helping us lead longer and healthier lives.

New medical technology is even enabling us to treat diseases that we haven't been able to heal before. Tissue engineering is one of these breakthroughs.

In 2008, Claudia Castillo survived tuberculosis, only for the left branch of her windpipe to collapse, cutting off air to her left lung. Previously the only solution to mitigate the damage would have involved removing the entire left lung — an operation with a high risk of death. However, Claudia received a new treatment, in which stem cells were taken from her bone marrow and "seeded" into a donated windpipe, which was then transplanted into her body. The procedure was successful, and in a matter of weeks, her full lung capacity was restored. Two months later, she was dancing in Ibiza.

Regeneration is another recent medical marvel. We observe this in nature when a salamander loses a leg and miraculously grows another one. Scientists are now beginning to emulate this process for humans.

When patient Deepa Kulkarni accidentally cut off the tip of her finger, it was dipped in a new kind of material called _extracellular matrix (ECM_ ), which prevents scarring and stimulates tissue growth. Seven weeks after treatment, her finger had grown back to its normal size.

Progress in genomics will also help us by making screening and treatment of diseases more effective. The sequencing of human genomes, for example, is becoming faster and more affordable.

With access to someone's entire genome we can personalize cancer and HIV treatments, making them more effective.

We're entering a time when scientists can prevent diseases and help our bodies regenerate. In the years to come, many of us will enjoy a far more pleasant and longer life.

Yet if we achieve lengthened, healthy life spans, is this always a good thing? We'll see in the next blink.

### 4. Many of the common moral objections to increasing human life spans are false. 

Advances in science and medicine have been so significant that it's been suggested that the first of us to live to 1,000 has already been born.

But, as impressive as this progress is, what about the moral implications? Is this something we should encourage? Is living longer necessarily better? Despite what the critics may say, it is!

Scientist Leon R. Kass posits that greatly extended life spans are incompatible with human nature. He argues that if we lived longer, we would lose interest in life and struggle to find meaning. Surely it's our looming death that provides our lives with purpose?

But having an extended, healthy life span _won't_ make us less human. For example, in 1850 life expectancy was around 42. It would therefore be absurd to say that we used to be "more human" simply because we had a more potent fear of dying sooner.

In fact, humans are remarkably adept at coping with change, and increased life spans are just a form of change.

Another objection to extending our lives is that it's a poor use of resources. Yet this is untrue.

Dr. Audrey Chapman of the Connecticut Health Center says that it's simply immoral to invest in expensive longevity enhancement technologies and aging research while those in developing countries are dying young from easily treatable diseases. The trouble is, this view wrongly assumes an either/or situation and doesn't recognize the potential health advantages this research could offer poorer communities.

There are multiple factors that add to injustice and inequality. Aging research isn't a significant part of any wealthy nations' budget, and singling it out as immoral seems unreasonable.

We needn't become pessimistic about our future aging population, but what will it actually look like? In the following blinks we'll find out.

### 5. Increased life spans will lead to population growth, but the impact may not be as big as we think. 

It seems inevitable that if the same number of people continue to be born while our life spans increase, the world will soon become crammed with people. According to many, this growth will be massive, leading to a lack of resources and dangerous environmental repercussions. But is this true?

Actually, the effect of long lives on population growth isn't as big as you might expect.

Researchers at the University of Chicago created a model to estimate the growth of populations around the world, assuming fertility rates remained at today's levels. Using data from Sweden, they found that even if Swedes stopped aging completely, the Swedish population would only increase by 22 percent over the course of one hundred years.

And a larger, older population won't necessarily mean a scarcity of resources, nor have a long-term damaging impact on the environment.

In his 1968 book, _The Population Bomb_, Stanford professor Paul Ehrlich predicted that as we approached the 1970s, hundreds of millions of people would starve from lack of food. The opposite happened. The daily calorie intake per capita has been increasing all over the world ever since, and is predicted to continue creeping up, despite the population increase.

The idea that negative impact on the environment can be blamed on a larger, older population may also be far-fetched.

According to Bill Gates, we don't often take into account the power and influence of the human mind. A larger population means _more ideas_ and innovation that will help us curtail any harmful impact on the environment.

Also, as we live longer, we're more inclined to think about our future needs and therefore become more environmentally conscious.

> _"Greater numbers of humans do not necessarily translate into fewer resources on which to live."_

### 6. Increased life spans will change the family unit of the future. 

One consequence of living longer is that we would have more time to form relationships and have children. It's probable we would do this several times over, which would have interesting implications.

New fertility technology will enable women to have children later — an example of which occurred in 2008 when an Indian woman called Devi gave birth to a healthy child at the age of 70. Fertility technology is progressing swiftly and will make late pregnancies less risky and more common.

One consequence of this is that there will potentially be a wider age gap between siblings. Currently, there are seldom age differences of over 20 years between siblings. However, if we were to live to 150, which may not be that far off according to researchers, age differences of 40, or even 50 years could become fairly normal.

Another byproduct of having children later will be a rising number of aging, decrepit parents, who would have difficulty handling and playing with their kids. However, if science can improve our health and vitality even as we age, this may not be such a concern.

Living longer could also lead to a greater variety in our family structure.

Until fairly recently, it was commonplace for a child to grow up without one or both of its biological parents. In the past, war, disease, and death in childbirth made remarriage with existing children or adoption fairly usual. As we start to live longer and have more relationships throughout our lifetime, it's likely that we'll go back to these family relationships built on social bonds (between stepchildren, foster parents, and so on), rather than the biological ones that we experience today.

### 7. Increased life spans will boost the economy and create a more educated workforce. 

Some people fret about the impact a larger and older population will have on the economy. Who's going to pay for all these old people and their pensions? But living longer might turn out to be an economic advantage.

According to economists David Bloom and David Canning, if one country has a life expectancy five years higher than another, then, all other factors remaining equal, the real income per capita will rise 0.3 to 0.5 percent faster in the "healthier" country.

Why is this?

One reason is that longer living results in a workforce that is better educated, paving the way for a more robust economy.

Also, if we live longer, we experience greater benefits from higher wages. If you work for 40 years after studying, that means 40 years of higher wages, but if you work 70 years after studying, you can enjoy those wages for 70 years.

What's more, when educated people stay in work, the pool of educated workers will broaden as more graduates keep flowing into the job market.

Economists Rodolfo Manuelli and Ananth Seshadri have shown that the difference in economic output between countries is linked to fluctuations in human capital (a society's combined knowledge and skills), and education has a large effect on this.

Global studies have shown that the poorest countries have the most to gain economically from increased longevity, as the potential for increased wealth and income from longevity is greater in these places.

For example, the National Bureau of Economic Research published a study that showed that between 1965 and 1995, the welfare gains due to increased longevity were 27 percent of GDP for Mexico versus just five percent for the far wealthier United States.

### 8. Living longer and healthier lives will not make us less religious, but will change our relationship with religion and spirituality. 

How would a world where we live exceedingly long lives and seldom experience disease and death change religion? What impact would longevity have on spirituality?

As we live longer, the concept of an afterlife will start to lose its appeal.

All the major world religions point to some kind of afterlife. In fact, anthropologist Ernest Becker famously said that without death, religion would not exist. Belief in an afterlife assuages many people's anxiety and is a great attraction of many faiths.

Yet as science enables us to live longer, major religions will need to draw focus to other things in order to stay relevant.

Living longer will give us more time to ponder the big questions in life and make room for new sorts of spirituality.

Livia Kohn, religious studies professor at Boston University and an expert on Daoism, predicts that the question of how to fill our time and organize our lives will become increasingly important as we move closer to what was once our ultimate goal: immortality. Some kind of spiritual guidance may be able to help us navigate our lives.

The futurist Ray Kurzweil calls for a new kind of religion. Kurzweil is a member of a movement sometimes referred to as "transhumanism," which holds that we're coming to a point, or "singularity," where science and technology will enable us to eclipse the limitations of our biology.

In his book _The Singularity is Near_, Kurzweil considers the need for a new religion based on reverence for human consciousness and knowledge. As we witness more dramatic changes to human life with new technology, this movement is likely to gain momentum.

> _"As science advances, there seems to be less and less for God to do." — Carl Sagan_

### 9. Future leaders, politicians and other influential people must learn how to effectively promote longevity. 

As with any movement, the movement for longevity requires ambassadors and spokespeople to continue to raise and attract funding. There are many ways to help this cause.

Proliferating the idea or "meme" that healthy life extension is achievable and worth investing in will garner support and add to public understanding of the issue.

One example of this is Oprah Winfrey's shows on longevity, and her support for doctors like Mehmet Oz, who have become influential by giving advice on how we can live longer, healthier lives.

Biology has become the new growth area for engineering as scientists experiment with genes, finding ways to make cells glow or even smell like banana. This surge is not unlike the beginnings of the personal computer revolution, except instead of hacking electronics, now we can hack genes. Movers and shakers in the technology industry such as Google's Larry Page and Microsoft co-founder Paul Allen are already keenly interested in biohacking.

We can encourage the longevity movement through incentive prizes and investments in biohacking, increasing the chances of discovering ways to live longer and more healthily.

The $10-million Archon Genomics XPRIZE is geared toward speeding up and reducing costs for genome sequencing. Physicist Stephen Hawkings is one of its supporters, as he believes such prizes will lead to the treatment of serious diseases.

So we can see there is huge potential here. But in order to progress further, politicians and policymakers need to understand how to promote new technologies and utilize the insights and knowledge we can gain from biohacking.

> _"Humanity is finally in a position to shed its acceptance of disease and death and instead launch a true offensive against it."_

### 10. Final summary 

The key message in this book:

**New technologies and progress in science are already providing us with ways to live much longer and healthier lives. As our life spans extend, this will have a huge impact on society and the way we approach our existence. However, humanity is creative enough and strong enough to deal with this, and we shouldn't fear an aging, growing population.**

**Suggested** **further** **reading:** ** _Unretirement_** **by Chris Farrell**

_Unretirement_ exposes the strain an early retirement puts not just on the economy, but on the individual. A more positive alternative is offered: "Unretirement," where older workers reorient themselves to more pleasant careers, using this new phase in their lives to make a difference to the world at large.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Sonia Arrison

Sonia Arrison is an expert on the impact of new technologies on society. She is a trustee of the Singularity University _,_ an institute which uses technology and education to solve humanity's pressing issues of the future.

