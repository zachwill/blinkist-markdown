---
id: 59062d1bb238e10008b856fb
slug: 12-en
published_date: 2017-05-04T00:00:00.000+00:00
author: Rodd Wagner & James K. Harter, PhD
title: 12
subtitle: The Elements of Great Managing
main_color: E62E2E
text_color: BF2626
---

# 12

_The Elements of Great Managing_

**Rodd Wagner & James K. Harter, PhD**

_12_ (2006) crunches the data from millions of surveys conducted by Gallup, Inc. to distil the essence of what makes employees happy, and why happy workers are good for business. If you're searching for practical solutions for improving employee relations and the quality of managers, look no further.

---
### 1. What’s in it for me? Discover the science of motivation. 

We've all been there: it's seven o'clock in the morning and time to get up, shower and head to work. But how motivated are you? Whether you write novels, take care of patients as a doctor or therapist or work as a manager in an office, there always comes a time when the motivation to work seems to have run out.

Sometimes it doesn't even happen to you — but to your employees. And you have to watch them wallow in misery, their performance gradually getting worse and worse until they finally make enough mistakes that you can justify firing them. But that doesn't boost your motivation either, right? So how can you manage to increase not only your own motivation but also that of your employees?

Gallup, Inc. wondered the same thing and, after taking data from thousands of surveys, found an answer: there are 12 elements to great management. In these blinks, we'll explore the most important ones.

We'll also discuss

  * why motivated employees create more profit for companies;

  * what really motivates employees; and

  * why team spirit is more important than hierarchies.

### 2. There are many reasons for managers and businesses to cultivate happy employees. 

Talk to any manager and they'll tell you that one of the hardest parts of the job is keeping all of their employees happy and productive.

To help identify the best management techniques, and the reasons they work so well, Gallup, Inc. conducted millions of surveys with managers and employees. The following information is the result of years of work investigating what really motivates employees.

One of the most significant and apparent findings is that happy employees are good for business. They show up to work more often and they're less prone to quitting, all of which saves the employer time and money.

More precisely, employees who were positive about their work had a 27-percent higher attendance rate than others. These are the people you want on your payroll. Otherwise, you'll be paying the same for less time spent at work, which is, of course, bad for business.

The data also showed that unhappy or disengaged employees are 31 percent more likely to quit their job, and that finding a suitable replacement costs significant time and money — especially when the position requires highly specialized skills. So if you lose a chemical engineer or an elevator mechanic — people with unique talents — it'll be difficult and costly to find a replacement.

Perhaps more surprising is that happy employees are also less likely to have accidents on the job.

Many jobs come with the risk of injury. If you work for a shipping company, for instance, you might get hurt loading a truck. What makes happy workers less prone to injury is their engagement and focus. Since they like what they're doing, they pay attention to it. And that means they're less likely to get careless.

Gallup found that those in the bottom quarter of the happiness index suffered 62 percent more injuries than those in the top quarter.

Clearly, it's mutually beneficial for both employees and companies to have a work environment that promotes happiness. In the blinks that follow, we'll take a closer look at how to foster such an environment.

### 3. Happy employees know how they contribute to a company’s goals and what is expected of them. 

If you're hiring someone to build your house, you'd want to be sure of two things: that they have all the tools they'll need and that they understand exactly what is expected of them. If they lack equipment or understanding, you might end up with a log cabin instead of your desired stucco villa.

The same is true in organizations. One significant piece of data showed that only 50 percent of employees strongly agreed with the statement, "I know what's expected of me." But when a company's employees scored higher on this question, they also turned out to be 5- to 10-percent more productive.

Employees also benefit from knowing exactly how their jobs fit in with those of other people in the company and what they can expect from others.

This is illustrated by the work of management experts Robert Huckman and Gary Pisano, who in 2005 discovered that surgeons who work at multiple locations do their best work at the hospital where they spend the most time overall. This was attributed to the fact that at these hospitals, the surgeons were familiar with the staff and knew exactly what they could expect from each person, for instance knowing which residents would provide the most accurate patient data.

Another key to happy employees is making sure they know precisely how their work contributes to the company's goals.

This was a big problem at the Winegardner & Hammons Hotel in Dallas. When Nancy Sorrells was hired to help get the business back on track, she discovered that no one understood how his or her job fit into the overall success of the company.

To remedy this situation, she went over the company's mission statement with the employees to help them understand how their individual roles could help provide a great experience for every guest.

With this in mind, receptionists could see that there was more they could do to accommodate guests than just handing them a room key. As a result, everyone became more motivated — and a few months later, the hotel scored 95 out of 100 points in an inspection!

### 4. Managers should recognize and reward hard work by giving positive feedback on an individual basis. 

If your parents wanted you to clean your room when you were a child, they might have tried using a reward to motivate you. Maybe they said they'd take you out for ice-cream if you did a good job.

Believe it or not, incentives like this are effective in adulthood, too — and especially in the workplace. But, rather than offering sweet treats, a good manager will offer sweet words.

In fact, when employees receive regular praise, productivity and revenue generally increase by 10 to 20 percent.

So don't underestimate the power of giving employees recognition; it's a simple way to make them much happier.

Most people find praise pleasant. An employee' brain will release dopamine — which triggers feelings of satisfaction and happiness — when he hears kind words. This release also creates the urge to recapture this feeling, and this goal will keep the employee working at his best.

This chemical process also explains why some people lose motivation. When there's a lack of dopamine associated with work, there's less reason to perform a task or to put in anything more than the minimum amount of effort required to get it done.

And employees who feel their work is not rewarded or recognized are twice as likely to quit during the next year as those who do feel appreciated.

However, praise should be doled out prudently. It can lead to jealousy and resentment, so it's important for managers to deliver praise in one-on-one situations.

Companies often make the mistake of praising employees in team meetings, which can backfire in ways that manager Elżbieta Górska is all too familiar with.

In 2002, she oversaw the warehouse team at the International Paper Company in Poland, where many employees were only familiar with the procedures of the times when the state owned the business and everyone just received orders and no compliments were given.

During a team meeting, Górska made the mistake of praising four employees and creating an atmosphere of jealousy that broke the team spirit. Górska realized her error and fixed it by setting up one-on-one meetings every week where she could listen to employee concerns and give individuals recognition for work well done.

> _"Because of its power, ridiculously low cost and rarity, [positive feedback] is one of the greatest lost opportunities in the business world today."_

### 5. Employee performance improves if they feel they’re part of a tribe, and this requires personal managerial leadership. 

As humanity has developed as a species, we've transitioned into a more city-based economy. But this doesn't mean we aren't still drawn to the benefits of a tight-knit community.

This is why another step toward employee satisfaction is to make them feel as though they're part of a tribe.

This means forming a cohesive team of coworkers, giving them shared goals to work toward and prioritizing employee interests.

Employees are happy to be a member of a tribe because it means they're part of a network of individuals who care for each other, a state of affairs that historically has provided an evolutionary advantage. It's been shown that those who were sociable and remained part of a tribe were more likely to survive and flourish, since jobs like hunting and farming became much easier when more people were involved.

And all the attributes of a tribal community can be found in a modern company: there's a shared language and wardrobe, there are territorial boundaries and there's a manager who acts as a sort of tribal leader.

However, to take advantage of an employee's desire to be part of a community, a manager needs to reinforce her role as the tribal leader by showing genuine interest in her employees.

This was how Larry Walters was able to turn around the disastrous conditions at Qwest's call center in Idaho Falls. When he was hired as a manager in 2003, employees were unmotivated and delivering poor results, which was partly due to the other managers' impersonal approach to leadership.

To get things on the right path, Walters went in the opposite direction — instead of just focusing on numbers, he learned the names of every employee, got to know their interests and asked them how their kids were doing in school.

Like a football coach, Walters fostered a positive team spirit and practiced daily affirmations by placing signs in the middle of the call center proclaiming things like "You're all rock stars!"

The employees soon felt more committed and motivated. They wanted to impress their new leader because, in his eyes, each of them was much more than a mere cog in the machine.

> _"The question of why people have this need for someone taking a personal interest in them goes to the core of much larger issues about humanity itself."_

### 6. Rather than using the scientific approach, managers should let employees be part of the problem-solving process. 

Being part of a team isn't all camaraderie and praise; it can also lead to tiresome conflict — and this is why some managers think it's easier for everyone to just put their heads down and follow orders rather than working together.

Let's call this the "scientific approach" to management. It's an outdated, often frustrating and demotivating approach which many large organizations still adhere to. The man to blame for this is Frederick Winslow Taylor, who helped businesses thrive during the industrial revolution.

To make labor more efficient, Taylor devised a way to improve employee performance by delegating all planning and thinking work to managers who supervised and controlled the workflow. In this model, employees had no reason to question anything — all they needed to do was show up and do what they're told.

This approach may have increased productivity at the time, but it also created a workforce that followed orders like a horde of mindless robots and was controlled by managers whose sole interest was meeting production quotas.

But, of course, employees are not mindless robots — they are human beings. And they're naturally motivated and productive when they have the chance to work on projects that they have had a hand in developing.

Knowing this can make problem-solving in the workplace much smoother. Just consider the following example:

In 2001, a children's hospital in Toronto was facing a reduction in their MRI budget. This meant they had to find a way to work more quickly while still getting quality images.

A standoff followed: Technicians proposed sedating children so that they'd remain still and reduce the number of times the MRI would need to be retaken due to movement. But nurses didn't like this proposal; to them, unnecessary sedation seemed like a dangerous solution.

Manager Susan Jewell solved the problem by setting up a day-long meeting where representatives of both teams could think about a solution that everyone could agree on. This way, the nurses felt like they were involved in the process, and, when they understood that they were being entrusted with the responsibility to decide which children _wouldn't_ need sedation, the plan to sedate went through.

### 7. Good managers make sure their employees discover their job’s meaning. 

Which would you rather do: Work on a product that generates profit for a corporation, or one that helps children learn? The second proposition probably sounds better and more meaningful, yet these two company goals aren't necessarily mutually exclusive.

Often all that's needed is a change in perspective, and this is another important part of making sure employees are satisfied with their work.

Once we humans have our basic needs for food and shelter taken care of, our minds begin searching for ways to fulfill our higher needs, such as finding a purpose in life.

Why humans have this kind of self-awareness is still a mystery, but it is clearly there. In a 1990 Gallup poll, 1,657 Americans were asked to rate how important it is for life to have meaning, and 83 percent gave the highest rating of "very important."

When a company taps into the happiness provided by a worthwhile purpose, they can reap major rewards. Companies were found to be 5- to 15-percent more profitable when they had employees who gave the most positive responses to the statement, "The mission or purpose of my company makes me feel my job is important."

But it takes the right managers to give employees the proper perspective on their work.

A new tablet device could be seen as just another cash grab by a greedy corporation, but when it's framed in the right way, it can be a humanitarian effort to provide children with better access to education.

This is why a company needs to lay out meaningful goals in their mission statement, and then hire managers who can convince their employees of that mission's merit.

The mission statement of Cabela's, a successful outdoor-equipment company, reads: "We passionately serve people who enjoy the outdoor lifestyle by delivering innovation, quality and value in our products and services."

Part of Cabela's success is thanks to the fact that 60 percent of their employees strongly agree with this meaningful goal. And this is due to Cabela's managers, all of whom are passionate about an outdoor lifestyle and make sure the company hires employees with similar passions.

### 8. Happy employees continue to learn and grow on the job. 

The very fact that you're engaged with these blinks right now probably means that you have a desire to learn exciting new facts about the world around you. Maybe you even like mastering new skills, like cooking, coding or speaking Hindi.

This desire to develop and grow is another natural urge for the human mind.

In our first twenty years of life, it's what drives us to learn how to walk, write, read, ride a bike and so on, without needing an external reward to motivate us.

In our very first years, our brains are growing rapidly by the day. At school, our knowledge progresses more and more each year.

But this desire to learn doesn't just stop completely when we finish our schooling. Experts believe that looking forward to further development in life is hardwired into our nature.

The influential American psychologist Abraham Maslow believed that continued development was of the utmost importance to human beings, which is why he put "self-actualization" — or the fulfillment of one's potential — at the top of his hierarchy of needs.

Naturally, the desire to continue developing extends to the workplace. It even factors into the meaning of the word "career," which is defined thusly: "professional progress: somebody's progress in a chosen profession or during that person's working life."

In short, a developing employee is a happy employee who will be motivated to do better work.

This is evidenced by the fact that businesses tended to outperform other companies by 10 percent when their employees strongly agreed with the statement, "This last year, I have had opportunities at work to learn and grow."

And a good manager should make sure her employees know the ways in which they can develop their skills.

Research shows that having a strong interest in something is directly related to higher brain activity in the areas responsible for both concentration and happiness, both of which are crucial to delivering good work.

So don't forget: There are fundamentally human reasons why happy employees are the best employees a company could hope for.

### 9. Final summary 

The key message in this book:

**Many companies still fail to recognize the fact that satisfied and motivated employees can have a significant positive impact on corporate performance. These companies need to learn how to motivate their employees, or they'll find themselves fighting a losing battle in the marketplace.**

Actionable advice

**Make sure that your employees have the equipment they need.**

If people are allowed to explain what they need to do their jobs correctly, they will feel more valued and motivated. So you should always listen to your employees when they describe what materials they need, and ensure they get them. Otherwise, you run the risk of fueling frustration about not being able to perform their duties, which can lead to frustration about the job and workplace in general.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Widgets_** **by Rodd Wagner**

_Widgets_ (2015) is a guide to improving employees' performance, regardless of the field they work in. These blinks outline the fundamental principles, based on scientific research, that will help any company improve its efficiency, profitability and worker satisfaction.
---

### Rodd Wagner & James K. Harter, PhD

Rodd Wagner is an advisor to business executives around the world, helping them improve performance and efficiency. A former principal at Gallup, Inc., he is the author of the book _Widget: The 12 New Rules for Managing Your Employees As If They're Real People_.

James K. Harter, PhD, is the Chief Scientist of Workplace Management at Gallup, Inc. and a researcher whose work has appeared in over a thousand published studies. He is also a contributor to the _New York Times_ and _Harvard Business Review_.

