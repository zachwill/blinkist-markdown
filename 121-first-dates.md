---
id: 58ecb487511add00045ae2e2
slug: 121-first-dates-en
published_date: 2017-04-14T00:00:00.000+00:00
author: Wendy Newman
title: 121 First Dates
subtitle: How to Succeed at Online Dating, Fall in Love, and Live Happily Ever After (Really!)
main_color: EB3B4D
text_color: 9E2935
---

# 121 First Dates

_How to Succeed at Online Dating, Fall in Love, and Live Happily Ever After (Really!)_

**Wendy Newman**

_121 First Dates_ (2016) is a woman's guide to online dating. These blinks will help you create a winning profile, prepare for your first date and deal with the difficulties that crop up when looking for Mr. Right.

---
### 1. What’s in it for me? Dare to date and find your man. 

We live in a world of lonely hearts. Whether fearing disappointment or just plain fed up, many women have decided that life is better without dates. But it doesn't have to be that way. Thanks to online dating, there are more opportunities than ever to try your luck and meet your man.

With the right mindset, and the help of a few small tips, dating can be a pleasurable and exciting experience — and a successful one, too.

These blinks reopen the world of dating. You'll learn how to mentally prepare for a date, how to get organized before it and what to expect from it. You'll also gain techniques for escaping from a bad date and dealing with rejection.

You'll also learn

  * what it takes to live happily ever after;

  * why glamming yourself up can work miracles; and

  * why your brother has no place in your dating-page profile picture.

### 2. Mentally prepare yourself before going on a first date. 

On the stormy sea that is modern dating, any voyage is sure to be exciting. But before you set sail, it's essential to prepare yourself — to batten down the hatches, so to speak.

First, it's important to be able to bring yourself to a mental safe haven in case of emergency. This ability is the result of self-awareness; you need to know what you need and what makes you happy.

Maybe you feel cranky without a solid eight hours of sleep. Or perhaps you get tense if you don't take a walk in the morning.

Once you've determined what you need to feel good, think about what makes you happy. These are things that you don't necessarily _need_, but which add sparkle to your life. Maybe it's going to the movies or getting a massage or reading a good novel.

Determining these desires in advance will make the contingency of dating less daunting. Whenever a date takes an unwanted turn, you'll be able to fall back on yourself for support. Knowing yourself and what you want will empower you to find happiness internally, without relying on someone else.

The next step is to be sure you have an open mind before going out on a first date. This is very important — so important that you probably shouldn't go on a first date straight after work when you'll probably still be preoccupied with the worries of work.

Most people prefer hanging out with a woman who is relaxed and self-assured. So even if you only have an hour to take a bath, change your outfit and spruce yourself up, do it: such a routine can do wonders for your energy.

It's also a good idea to set aside unrealistic expectations. Be prepared for the possibility that the two of you simply won't click. If you aren't expecting to meet prince charming, you'll be less likely to go down the rabbit hole of wondering what your date thinks of you.

Being able to engage in good conversations and share funny or uplifting stories is usually the best litmus test of a date's success.

### 3. Online dating offers many opportunities but requires commitment and good self-presentation. 

Now that you're prepared for your first date, it's time to dive into the world of online dating. There are hundreds of dating sites to choose from, but to make them work for you, you'll need to be steadfast.

Dating sites essentially make the process of meeting the people you want to meet much easier. They do this in part by establishing communities of people with similar needs. Match.com and OKCupid, for example, are sites for people in search of a romantic relationship. People who are in the market for casual sex might turn to Tinder or simply do a quick Google search.

Another bonus of dating sites is that you can contact people you probably _wouldn't_ encounter in your everyday life — from surgeons to public speakers, rock stars to rock climbers.

But there's a downside to all this freedom, too. Your online acquaintance might vanish, never to appear again. Don't let this discourage you; it's just part of the deal.

After all, a catch might disappear because he thinks you're not right for each other. Or maybe he just got scared because he's new to online dating. Whatever the case may be, you shouldn't take it personally and it shouldn't put a damper on your commitment.

But commitment alone won't reel in Mr. Right. You'll also need to present yourself well.

When it comes to profile pictures, the natural look always wins. Post at least three photos and use a natural backdrop. Select pictures that are representative of who you feel yourself to be — pictures that make you feel happy and sexy.

It's best to avoid posting pictures of yourself with female friends. This may confuse people looking at your profile. And forget posting pictures with your brother. Guys will assume he's your ex-boyfriend and that you're still in love with him.

As for the written section of your profile, it's best to be honest, humorous and concise. Don't make the mistake of starting a relationship based on little lies. It's harder to backpedal later than it is to be up front straight off.

### 4. Surprise, surprise: the point of online dating is to go on actual dates! 

Online dating can help you find singles near you whom you might be compatible with. But, contrary to the seeming implication of the name, online dating isn't about chatting on the internet. It's about going on actual dates!

Services like this aren't set up for you to get to know your partner through long email threads and social media exchanges. After all, you might exchange a few emails, check him out on social media and start fantasizing about your perfect life together — only to be disappointed in the end.

Internet romances are bound to fail because they're built on illusions. You'll never know if you really click until you meet face-to-face. That's why, once you've found a potential match, it's key to set up a date as soon as you can.

That being said, arranging a date involves negotiating a few roadblocks. The first is to find a good meeting place.

When your date asks you where you'd like to meet, don't respond with, "wherever you want." Answers like this are unhelpful, especially if they're untrue.

Many women are under the impression that men find low-maintenance women more attractive. This is a misconception. It's better to be assertive and opinionated. So, rather than leaving it up to him, offer two potential locations and let him know that you'd be open to his suggestions, too.

The places you suggest should largely depend on how you think yourself and your date will get on. If you think you will be able to share a conversation for an hour then suggest a dinner date. If you are unsure how you'll get on, then suggest a more public place, like a museum or a park. This way you'll feel more comfortable cutting and running if things don't go so well.

> _If he talks a lot about sex, he surely wants to have sex with you on your first date. And this might be the only thing he wants from you._

### 5. Know what you’re looking for in a man – but don’t be too rigid. 

In life, it's important to be able to strike a healthy balance — between work and play, friends and family, and so on. Well, balance is equally important in dating. So, if you're in search of a committed relationship, it's important to find a partner that you're physically attracted to, but also one who makes you happy.

A guy who you _only_ find physically attractive might not be able to make you happy down the road. Then again, if you make your romantic decisions based solely on what's on the inside, you might end up with someone who inspires you and makes you feel safe, but offers you zero physical excitement.

The solution to this quandary is to make a _unicorn list_ on which you write every characteristic that your ideal fantasy partner would have. For instance, you might say you want a man who is tall, empathetic, super wealthy and fantastically smart.

Next, ask yourself, _Would I rather be single than be with a guy who isn't..._ finishing the question with one of the desirable qualities you listed. If you'd rather be alone than with a partner who lacks a certain quality, then keep it on your list.

From there you can use the list as a guide to check your prospective matches against your desires. But remember, it's important not to be too rigid with your list. Embracing the differences in each person, even if they're not what you're used to, is essential to successful dating.

For example, maybe you're a bit shy and therefore attracted to confident men but you end up on a date with a guy who is so nervous he's stuttering at the beginning of each sentence. It would be a total loss not to give him the chance to show you his sexy-confident side, which could be waiting just beneath the surface.

You can help a man like this relax by talking about his accomplishments. Discuss his work or the sports he's into or other hobbies. Talking about his strengths will help him seize his own confidence.

### 6. Rejection is difficult – but it’s essential to deal with it correctly. 

Imagine you go out with someone for the first time. It goes OK but you realize pretty soon that you and the guy just don't match up. What should you do?

Well, what you should definitely _not_ do is disappear without an explanation and start ignoring his calls. If you don't want to meet again, just be fair and tell him the truth.

Turning people down is never easy and lots of women hesitate, not wanting to hurt a man's feelings. The problem is that, if you're really not interested in dating a guy, there's no point in prolonging the inevitable or pretending you feel differently. Everyone prefers a straightforward, clear answer that let's them move on.

So refrain from telling guys how special they are. Instead, say something like, "We just aren't right for each other" or "I had a great time but I don't see a future between us."

And if things go the other way around, and a guy ends up rejecting you, learn to accept it and don't blame yourself. Remember, you're doing your best; if a guy doesn't call you, he's probably not the right one.

There are many reasons that someone might not call you back. He might have liked you, but could tell that you wanted something he couldn't offer. Or maybe he just broke up with a partner and, after his date with you, realized he wasn't ready to move on.

If a guy says he's very busy and doesn't have time to date, it's best to take him at his word. Don't question whether or not he's telling the truth.

And finally, don't tell yourself that a guy didn't call because he's intimidated by your success. Men love women who are strong and capable. This won't be a problem unless you're utterly self-sufficient, in which case he might have a hard time seeing himself as a part of your life.

### 7. In difficult moments, you can take care of yourself and even heal your broken heart. 

Say you just experienced a brutal breakup and are completely discouraged. Getting back on the dating scene seems like the worst thing ever. Well, instead of suppressing your pain — as many people are prone to do — it's important to address and overcome your heartbreak.

The author developed an exercise that can help with this. It's called _heart healing_ and it's designed to clear out the harm from your body and spirit.

But keep in mind, this exercise can only be done with someone that you trust completely and feel comfortable confiding in. Before you begin, both you and your healing partner should take a moment to breathe and relax. From there, you can begin confessing why you're sad.

After your confession, your healer should say that she's sorry you're suffering and repeat everything you just confessed. For instance, you might say, "Marc hurt my feelings when he promised to call me back then never got in touch." And your healer might respond, "I'm sorry Marc hurt your feelings when he promised to call but then never got in touch."

At first, this might feel a bit forced and false. But it works. Hearing someone acknowledge your pain and say that they're there for you will liberate you from that pain.

You can also help heal your broken heart on your own.

One way to do so is to reserve _you moments_ — moments in which you let yourself be unhappy and angry, to cry and express your true feelings. Allowing yourself these moments will help you process your sadness or anger and clear space for new, happier thoughts and feelings.

Another strategy is to distract yourself with healthy physical activities. Go for a hike, do some yoga or go out dancing; engaging your body will help put your ex out of your mind.

And remember, don't pay any attention to your inner critic. You might feel like you should drop dating altogether, because you're too fat or too old or for some other ridiculous and untrue reason. Replace these self-doubts with optimistic thinking. You can develop your own positive mantras, but start by telling yourself that, by moving on, you're freeing yourself to find your soulmate.

> _You can make a couch-appointment through the author's web page wendyspeaks.com if you don't have someone for the heart-healing exercise._

### 8. Sex can be a delicate subject, but you shouldn’t sacrifice your needs. There are lots of ways to make it work. 

When it comes to first dates, one question always seems to come up: Is it OK to have sex on a first date, or is it totally out of the question?

Well, books and dating coaches have produced so many rules and strategies about when it's fine to have sex that the question has become, if anything, harder to answer. But the truth is, there's no hard-and-fast answer. The best thing to do is to listen to yourself and pay attention to what you feel.

Women tend to think that if they go to bed with someone on the first date, they won't be taken seriously. However, if you trust the man and it feels right, there's nothing wrong with taking it to the next level.

The point here is that men just aren't as strategic as women when it comes to sex. Men believe that sex is nothing special — that it's just part of the experience of life.

That being said, if you have sex on a first date and feel terrible about it after the fact, be open and let the guy know. Take what you need and slow down the pace until you feel comfortable.

Remember, just because you're searching for a serious relationship doesn't mean you should ignore your sex life. You can always have a friend with benefits or even decide to make a relationship non-exclusive.

If the former option doesn't work because you fall in love with the friend you're sleeping with, you can always opt for an _open relationship_. This designation encompasses friends with benefits and polyamorous relationships — partners who have multiple romantic relationships simultaneously — and "play partners," who meet up at random without commitments.

### 9. Living happily ever after is possible – if you live a life that matches your needs. 

Many women dream of living "happily ever after" once they fall in love. Such a dream often turns out to be no more than an empty fantasy — but that doesn't mean it's unobtainable.

To truly be happy for the rest of your years with your partner, you need to figure out whether your relationship lets you truly be yourself. During the first year or so of a relationship, love is in the air and it doesn't matter so much to what extent you can _truly_ be yourself. But as the years go by, you'll quickly find that love simply isn't enough.

So, to really be happy, you also need to stay true to the things that matter most to you. You can get perspective and clarity on this by asking yourself questions like, "Do I change my personality when I'm with him?" or "Has my relationship with my friends suffered?"

Beyond that, be sure you have freedom from the pressures of society, family and friends, which can interfere with the way you and your partner choose to live. Keep in mind that only you and your partner can know what's right for you.

Be clear about what you value in life and stick with it, even when tradition and other norms make it scary. For example, say you're in love with your partner, but you don't want to move in together because you like living alone. You shouldn't feel obligated to move in with your partner just because that's what most people would do or because your best friend thinks you should.

In other words, you don't need to adhere to cultural norms. Instead, focus on what truly makes you happy.

Maybe you want to get married and have a huge wedding party. Maybe you'd rather have an intimate gathering celebrating your partnership. Both are fine choices. The same goes for decisions like whether to have children or whether your partner should become a stay-at-home dad.

> "_The key to your happily-ever-after is living the life you want with the partner (or partners) that match you and your needs well."_

### 10. Final summary 

The key message in this book:

**For single women today, the dating scene is full of possibilities. Online dating has opened up an entirely new realm and anyone can put themselves out there. There's a lot you can do to prepare for a first date, but if it doesn't go as expected, just keep your head up and don't blame yourself.**

Actionable advice

**Make your Facebook research analog to avoid hurt feelings.**

It's tempting to check out a date's Facebook profile. Giving in to this temptation, however, isn't always such a great idea. Often, you'll stumble upon photos of his exes or information you weren't so keen to know. So, the next time you want to do a little Facebook stalking, just download and print out his profile. That way you can look at all the pictures you want, without the risk of unpleasant surprises.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Modern Romance_** **by Aziz Ansari**

The internet and modern technology have revolutionized the way we communicate, learn, work and live. They've even revolutionized our concept of love. _Modern Romance_ (2015) explains how our idea of "love" has changed in recent generations, and how you can make the most of today's technology in your quest to find it.
---

### Wendy Newman

Wendy Newman leads workshops on dating, relationships and sex. She was inspired to write this book by the 121 first dates she went on before meeting her life partner.

