---
id: 59e8c9a6b238e10007a75064
slug: 15-secrets-successful-people-know-about-time-management-en-kevin-kruse
published_date: 2017-11-03T00:00:00.000+00:00
author: Kevin Kruse
title: 15 Secrets Successful People Know About Time Management
subtitle: The Productivity Habits of 7 Billionaires, 13 Olympic Athletes, 29 Straight-A Students, and 239 Entrepreneurs.
main_color: FE7333
text_color: 99451F
---

# 15 Secrets Successful People Know About Time Management

_The Productivity Habits of 7 Billionaires, 13 Olympic Athletes, 29 Straight-A Students, and 239 Entrepreneurs._

**Kevin Kruse**

_15 Secrets Successful People Know About Time Management_ (2015) reveals the secrets of time management used by billionaires, Olympic athletes, honor students and entrepreneurs. These blinks are a guide to extreme productivity that won't wear you out.

---
### 1. What’s in it for me? Up your time management skills. 

Modern life is full of time thieves. Whether it's your phone buzzing in your pocket with the latest tweet, a colleague interrupting you in the middle of an important task or the daily barrage of emails, your time is constantly being stolen from you, minute by minute, hour by hour. And once you've lost that time, you can never get it back again.

To remain focused and make sure these time thieves don't get the upper hand, you need to keep a few tricks up your sleeve. These blinks will get you started.

In these blinks, you'll learn

  * why the number 1,440 is so important;

  * about the 321-Zero system; and

  * how many books George W. Bush read throughout his presidency.

### 2. Time is your greatest asset and should be spent wisely. 

Do you ever wish you could have an extra hour in the day to read, exercise or hang out with the people you love? Well, you can and there's no magic required! But first, you need to understand how precious time is.

That's why the first secret of time management is learning that time is your most valuable asset and that, once it's lost, you can never get it back. A good way to monitor this precious resource is by breaking it down. For instance, there are 1,440 minutes in a day, each of which should be invested wisely.

To remind himself of this fact, the author taped a "1,440" poster to his office door. It reminds him of the limited time in each day.

But why use minutes? Why not think about the 86,400 seconds that every day offers?

Well, there are a bunch of daily tasks that you can _actually do_ in a minute, like knocking out 30 sit-ups, reading a poem or watering a plant. This fact makes minutes key to monitoring your time.

Okay, now that you appreciate how valuable time is, it's time to start prioritizing it. This is where the second secret comes in: determine and prioritize your _most important task_ , or your _MIT_. This is the single task that will have the biggest impact on your life or work.

Just take Therese Macan, a professor at the University of Missouri-St Louis. She found that one of the most important productivity determinants is the ability to identify priorities. So pinpointing an MIT is central to time management.

For instance, if a senior executive sets a goal of developing a new app, her MIT might be to hire a new programmer. Or the MIT for a start-up CEO could be to prepare a great presentation to land a major investment. Research has shown that having a daily MIT, whatever it is, results in greater levels of happiness and improved focus.

> _"To do two things at once is to do neither"_ – Publius Syrus

### 3. Ditch your to-do list for a calendar to accomplish more while stressing less. 

Lots of people are weighed down by never-ending to-do lists. Take a look at yours. How many of the tasks have been lingering there for weeks, unnecessarily stressing you out?

You probably have at least a few such tasks, and the best way to deal with them is by pulling out a good old fashioned calendar. Here's where the third secret comes into play: ditch your to-do list and pick up your calendar to de-stress your day.

Research has found that an average of 41 percent of items on to-do lists never actually get completed. One of the reasons for this shocking statistic is that the tasks on such lists usually aren't accompanied by a note stating how long it'll take to complete them. As a result, tasks that are more difficult or less important generally get left undone.

That might not be such a problem, except for the fact that the unfinished items on your to-do list will inevitably produce a lot of stress that could just as easily be avoided. In fact, researchers from Florida State University discovered that you can avoid this stress by simply coming up with a plan to complete a task.

The Olympic gymnast Shannon Miller offers a good example. She succeeded at spending time with her family, completing her school obligations, training for the Olympics and even doing media interviews, all by scheduling time for important tasks.

This strategy is known as _time blocking_ or _time boxing_ and, incredibly, all it requires is maintaining a detailed calendar. By doing so, Miller forced herself to prioritize tasks that would bring her closer to achieving her goal and, to this day, she keeps an almost minute-by-minute schedule.

However, you'll inevitably encounter tasks on your calendar that you can't accomplish. When this happens, instead of letting them drift into the past, simply reschedule. For example, if you usually make it to the gym at noon, but have a flight to catch at the same time, simply move your workout to earlier in the day.

### 4. Overcome procrastination by anticipating how you’ll act in the future and accepting that there’ll always be more to do. 

Everyone's been there: an important deadline is creeping up and, instead of working on the project at hand, you're planted in front of a screen – scrolling through Facebook, texting a friend or watching your favorite TV show. Procrastination is a tough one, but, luckily, there are strategies to break free from it and start getting things done.

This is where the fourth secret comes in: procrastination can be overcome by imagining your future self.

After all, you don't procrastinate because you're lazy, but because you don't have sufficient motivation. Imagining yourself in the future can fix this problem and it's as simple as asking yourself two questions: "What pleasure will I get by doing this thing?" and "What pain will I feel if I don't do it?"

For instance, if your goal is to work out every single day, but you can't get yourself to exercise, just imagine having a huge beer belly and feeling totally sluggish. Such a mental routine will get you off the couch and onto the treadmill.

At the same time, being honest about the actions your future self will take can also help you achieve your goals. For example, if you know you'll be inclined to eat unhealthy snacks during a future break, you can protect yourself by throwing out all the junk food in your house. You could even go a step further by filling the house with healthy options like baby carrots and hummus.

From there, you can move on to the fifth secret: there will always be more to do; you can't do everything. And, actually, that's fine!

In fact, prioritizing and scheduling the tasks you want to do is much more valuable than crossing off as many items as possible. Just take President George W. Bush as an example. He knew there would always be more to do. So instead of trying to do everything, he made it his priority to read tons of books, since he found it therapeutic and educational. As a result, he read some 95 titles during his presidency!

> Procrastination expert _Joseph Ferrari, PhD, found that 20 percent of Americans are chronic procrastinators when it comes to their personal life, career, school and even relationships._

### 5. Writing down your ideas and limiting trips to your inbox will clear your mind and boost your productivity. 

Have you ever had an incredible idea while shopping for groceries or walking the dog? Wouldn't it be great if, instead of straining to remember it later, you could just jot it down right then and there?

That's why the sixth secret is to always have a notebook handy. After all, writing down your thoughts helps you hold onto them. Virgin Group founder Sir Richard Branson says he never would have built his business empire without his trusty notebook.

He was so committed to writing down his ideas that, one time when he had a business idea and no notebook, he wrote down the thought in his passport! For him, if an idea doesn't get written down, it could be lost forever.

Taking notes by hand also helps your memory. For instance, the psychologists Pam Mueller and Daniel Oppenheimer found that students who hand wrote their notes during a TED talk were better able to recall the material than students who took notes on their laptops.

Writing down your thoughts is crucial – as is maintaining control over your schedule, which is where the seventh secret comes into play. It says that you should avoid checking your email too often, lest other people dictate how you spend your time.

In fact, contrary to popular belief, constantly checking emails is unproductive. That's because the anticipation felt when checking your inbox is comparable to pulling the handle of a slot machine. Often, you check your messages, and there's nothing new. But sometimes there is a new message. This unpredictability is addictive, and one begins to check more and more often, hoping for the hit of dopamine that a message affords. Obviously, this costs you time and interrupts your focus.

A good way to untether yourself from your email is by unsubscribing from newsletters by using a program like unroll.me. But you can also adopt the _321-Zero system_. To do so, just limit yourself to _three_ email checks per day, while trying to get your inbox to zero in just _21_ minutes.

### 6. Regain control over your time by avoiding meetings and saying no. 

If you've ever had an office job, you know how incredibly boring meetings can be. But that's not the only problem with meetings. The eighth secret is that most meetings are inefficient and you should only schedule them as a last resort.

In fact, a 2015 survey found that 35 percent of respondents considered weekly status meetings to be a waste of time, for these two primary reasons:

First, in accordance with _Parkinson's law of triviality_ , meeting participants tend to waste lots of time on insignificant issues. Second, extroverts usually dominate meetings, making others less likely to participate. As a result, valuable information might not be shared during such gatherings.

That being said, if you absolutely _have_ to have a meeting, opt for a stand-up affair rather than a sit-down one. This might seem odd, but researchers at Washington University found that meetings during which participants stand result in better collaboration, less attachment to ideas, higher levels of engagement and more effective problem-solving.

The Yahoo CEO, Marissa Mayer, offers another good tip: by scheduling meetings based on increments of five or ten minutes, she's able to have up to 70 meetings a week. If she stuck with the standard 30-minute block she would never be able to accomplish this.

In other words, controlling the timing of meetings will prevent people from sucking up your time. This is key since other people will constantly ask you for things, a fact that dovetails nicely with the ninth secret: achieve your immediate goals faster by saying no to most things.

After all, every time you say yes to something, you're saying no to something else. The Olympic rower Sara Hendershot is a good example. She's a pro at saying no to social and other engagements. This hard-learned skill enabled her to keep her eyes on the prize in the lead up to the 2012 Olympics in London, where she qualified for the finals.

As a cherry on top, research has even found that people who tend to say no in response to requests for their time are happier and have more energy. ****

> _"The difference between successful people and very successful people is that very successful people say 'no' to almost everything."_ – Warren Buffett

### 7. The 80/20 rule and a few self-reflective questions will increase your efficiency and satisfaction. 

By now you know that it's essential to spend your limited time on tasks that will have the greatest impact. The tenth secret can help you do that. It says that by applying the _Pareto Principle_ you can uncover shortcuts to success. Here's what that means:

In the 1890s, the Italian philosopher and economist Vilfredo Federico Damaso Pareto found that 20 percent of the pea plants in his garden produced 80 percent of his healthy peapods. He extrapolated this 80/20 rule into the general principle that now bears his name. It can be applied to a number of areas.

For instance, by applying the 80/20 rule to your employees, you might decide that the majority of your salespeople should be let go since they're your lowest performers. From there, you could focus your energy on the remaining 20 percent, who already generate 80 percent of your sales, by giving them rewards and greater levels of support. The end result will likely be an overall improvement in sales.

Or you could use the 80/20 rule in your personal life by analyzing the tasks you do on a weekly basis, then identifying which of them has the greatest impact.

That being said, you can also accomplish more by critically assessing the tasks in front of you. The eleventh secret is designed to help you do that. It says that leveraging your skills and delegating work will increase your productivity. Just take a 2013 experiment published in the _Harvard Business Review_ by the professors Julian Birkinshaw and Jordan Cohen. It found that 43 percent of workers were unsatisfied with the tasks they do at work.

By simply training employees to slow down and ask themselves a few questions, the study's authors were able to identify important tasks, freeing up an extra eight hours per week. The first question they had people ask themselves was, "How important is this task to the company?" Then, "Is there anyone else who can complete it?" And finally, "How could this task be accomplished if I had half as much time?"

### 8. Theming each day and immediately completing short tasks will boost your efficiency. 

On a normal day in the office, do you ever have difficulty focusing? Most people do and a little bit of advice here can go a long way. This is the twelfth secret, which says that grouping your work into recurring themes each week will make you more effective.

A great example comes from Jack Dorsey, the co-founder of Twitter and founder of Square. He says that the secret to running both his companies was to have a theme for each day. For instance, on Mondays, he would focus on management; Wednesdays would be dedicated to marketing; and Sunday would be reserved for reflection, feedback and strategy for the next week.

Or take the entrepreneurial coach Dan Sullivan. He recommends theming each week based on three types of days to stay focused and remain invigorated. The first type is called a _focus day_ , which is for vital activities like revenue-growing tasks. The second type is called a _buffer day_ , which is for catching up on emails, returning calls, having meetings, delegating tasks and doing paperwork. And finally, a _free day_ is one on which no work should be done. This last type is reserved for vacation, family time and charity work.

Another simple way to boost your efficiency has to do with tackling small tasks. This is the thirteenth secret, which says that you should immediately take action on tasks that'll take fewer than five minutes to accomplish and avoid returning to the same task over and over.

Just consider the straight-A student Nihar Suthar. He completes five-minute assignments right away, avoiding a long list of tiny tasks.

Or take the author. His sister Debbie recently emailed him, but instead of writing her back, he called her to make sure they could talk. By scheduling a call in his calendar and thereby saving the mental energy he would otherwise spend trying to remember to get back to her, he decided to handle this task immediately. If he had instead put it on a to-do list or left it in his inbox, he probably would have never remembered to address it.

### 9. Increase your energy level and attention with a morning ritual and a simple technique. 

Imagine waking up at six in the morning, working out for 45 minutes and then whipping together a delicious, high-protein breakfast. It might sound difficult but the fourteenth secret shows why it's essential.

It says that dedicating the first hour of each day to a morning routine will enhance your health – mind, body and soul. In fact, starting the day with a workout is a great way to get your creative juices flowing.

Just consider the _New York Times_ best-selling author Dan Miller, who starts off each day by meditating for half an hour, then working out for 45 minutes while listening to audio programs. He avoids checking the news or looking at his phone during this time, devoting his first hour to positive and inspirational experiences. He even claims that his most creative ideas come to him during this daily "me time."

From there, you can further increase your energy and well-being by eating a healthy breakfast and drinking lots of water. This is huge for the best-selling author and podcast host Shawn Stevenson. He considers energy to be everything, and so he starts off each day with what he calls an _inner bath_. He simply drinks 30 ounces of purified water to jumpstart his metabolism by flushing out waste.

In fact, according to the fifteenth secret, energy is paramount. The secret is that productivity isn't about time, but about maintaining focus and energy.

That's why Francesco Cirillo came up with the _Pomodoro Technique_ – a method designed to reduce distractions and boost productivity. His approach involves setting a timer for 25 minutes, devoting your full attention to a single task for the full 25 minutes and then taking a five-minute break before repeating the cycle.

Author Monica Leonelle found ample success with the Pomodoro Technique after realizing that she didn't have a single spare hour in the day. By using the Pomodoro Technique, she recharged during her breaks, maintaining steady energy throughout the day and, with the help of other techniques, went from writing 600 words per hour to 3,500!

### 10. Final summary 

The key message in this book:

**Highly successful people consider time to be their most valuable asset. By applying their most effective life hacks – which do everything from prioritizing tasks to boosting your energy and keeping you focused – you too can make the most of your time.**

Actionable advice:

**Design a morning routine!**

When forming your morning routine, use the _LIFE S.A.V.E.R.S_ system developed by the success coach Hal Elrod. This acronym will help you include the most important things.

**_S_** stands for _Silence_ to foster gratitude and meditation

**_A_** is for _Affirmations_ of goals and priorities

**_V_** is for _Visualizations_ of your ideal life

**_E_** is for _Exercise_

**_R_** is for _Reading_

And the final **_S_** is for _Scribing_ in your journal.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: _The Subtle Art of Not Giving a F*ck_** ** __****by Mark Manson**

_The Subtle Art of Not Giving a F*ck_ (2016) is your guide to living a calm, happy life. These blinks explain how to use suffering to boost your success, define your values and purpose and live a fulfilling life in every moment.
---

### Kevin Kruse

Kevin Kruse is a _New York Times_ best-selling author. His first failed business venture occurred at 22, but instead of giving up, he went on to build several multimillion-dollar companies. He's now a _Forbes_ leadership columnist and keynote speaker on time management.

