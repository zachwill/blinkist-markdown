---
id: 581745419907fc0003a25fed
slug: 21-days-to-a-big-idea-en
published_date: 2016-11-03T00:00:00.000+00:00
author: Bryan Mattimore
title: 21 Days to a Big Idea
subtitle: Creating Breakthrough Business Concepts
main_color: 48BFC5
text_color: 2C7478
---

# 21 Days to a Big Idea

_Creating Breakthrough Business Concepts_

**Bryan Mattimore**

_21 Days to a Big Idea_ (2015) is your guide to rekindling playful creativity and discovering the countless bright ideas your brain is capable of producing. These blinks share the secrets behind simple yet effective techniques to get your creative juices flowing, and provide helpful tips for turning your wild ideas into a viable business.

---
### 1. What’s in it for me? Get your creative juices flowing to come up with new business ideas. 

We usually associate the word "creativity" with poets, musicians and painters, or even great scientists like Albert Einstein and clever inventors like Nikola Tesla. Creativity and business, however, doesn't make for a very familiar combination.

But if you want to come up with a great idea for a new business, you need to be creative; if you want to turn your idea into a great product, you need to be creative; and if you want to successfully sell a product, you ought to be creative, too.

These blinks will provide you a with a number of simple but ingenious tricks to foster your creative spirit. They bring together the most important aspects of a 21-day program the author developed to think up big ideas. You will learn how to easily come up with a variety of ideas for new businesses, as well as how to reinvent old ones.

You'll also learn

  * how many new ideas Thomas Edison had per day;

  * why a cooked chicken in the fridge can make for a great business idea; and

  * how to come up with completely new ideas in just 30 seconds.

### 2. Brainstorm to get your creative juices flowing and pinpoint your passions. 

Blank pages are daunting. They can make accomplished novelists, brilliant illustrators and successful business leaders nervous as they embark on new projects. Luckily, there's one method that can help us conquer that blank page: _brainstorming_.

In fact, some of the world's greatest scientific geniuses relied on brainstorming to kick-start their biggest ideas; this includes Thomas Edison, the inventor of the electric light bulb.

An entry in one of his notebooks, dated January 3rd, 1888, contains a list of over 100 ideas that Edison scribbled down in hopes of finding the one invention he wanted to create. He came up with ideas for a snow compressor, artificial silk, synthetic ivory, an electric piano and many more.

Brainstorming has been the go-to trick for creatives throughout history because it helps shut out internal criticism. When we brainstorm, we reconnect with our playful, imaginative selves. This, in turn, allows us to see new possibilities that we'd normally dismiss.

On top of this, brainstorming helps us uncover our passions. If you brainstorm long enough, you'll see patterns emerge that connect a handful of your ideas to deeper themes. This allows us to map out the areas that we're passionate about, which we can then focus on and develop.

We may even realize that we're passionate about something outside of our profession or skill set. This can be confusing for some, who worry that they can't delve into a particular field without sufficient technical knowledge.

However, research into top-performing start-ups has shown that you don't need to be a specialist in a given sector to come up with original solutions to problems. On the contrary, you may be in an even better position to kick-start innovation. When people know a lot about one specific industry, it may mean that they are too set in their ways to see new opportunities.

If you're ready to try brainstorming, where do you start? Just grab a piece of paper and write down 30 to 50 ideas.

### 3. Your everyday annoyances are the springboards to products that solve universal problems. 

The teenage Mark Zuckerberg had a problem: no girls wanted to date him! Facebook turned out to be his attempt at a solution, and while it may or may not have made him dateable, it certainly made him wealthy. Facebook, therefore, like most inventions, began as a solution to a problem. To create your own solutions, you need to identify problems that are worth solving.

Of course, the world seems to be full of problems! So how do you find the right one? Start by sniffing out everyday problems that bother you. Getting a dry mouth at night, feeling guilty after eating a sugary dessert, dragging luggage through a huge airport — these problems are just a few of the author's examples to get you started.

You can also learn to identify problems by continually questioning them. Make the question "what is the problem?" your mantra, and return to it throughout your day. This will help you pay close attention to things that are less than satisfactory in your life. With these in mind, you can start looking for solutions.

Although the problems we encounter in everyday life are rather mundane, solving them isn't. Developing new solutions can present a huge technical challenge. When done right, they can also bring significant financial rewards and benefit many people around the globe.

The problem of having a dry mouth at night was actually solved by Biotene and Therabreath; they created oral rinses that increase salivation to prevent the mouth from drying out.

A comparable problem was also solved by a Bruce Johnson from Minnesota — he had trouble breathing at night, and set out to solve this. By creating the Breathe Right nasal strips, he helped a lot of other people solve this problem too, creating a multi-million-dollar business in the process.

### 4. Ask how existing technology could be used differently to find game-changing ideas. 

Which of the following do you consider a better idea: one that's entirely original? Or one that just borrows bits and pieces of other people's ideas? Beware — the latter type actually has the potential to be groundbreaking as well!

Think about it: thousands of entrepreneurs have used the invention of the _smartphone_ as the home for their inventions — apps! You needn't feel shy about making the most of other people's great ideas and hard work.

For example, a company called Aerodesigns has recently created a way for people to inhale vitamins, dietary supplements and caffeine through their nose and into their bloodstream. Why not use this instant pick-me-up innovation to develop a new deep breathing diet? By combining the benefits of antioxidant yogic breathing with the new technology of inhalable supplements, you could help a lot of people be healthier and happier!

The truth is that existing technology can help you dream up new ideas for services. Take a technology that impresses you, consider its strengths and then think about how you could apply them in new ways. For example, remote-controlled drones, as innovations largely used for warfare, have also been made useful in entirely different circumstances.

The strength of drones lies in their ability to get from point A to point B without traffic or other obstacles stopping them; this could be just the thing for emergency situations. Drones could deliver medical kits to those who need them urgently, or they could drop life vests to swimmers struggling to stay afloat. There are countless possibilities!

### 5. Bring ideas to life by helping your playful and rational sides work together. 

Before the invention of the telephone, Alexander Graham Bell's idea to send voices through a wire would have seemed like childish nonsense. However, children have also been proven to have more creative minds than adults. If we want to bring great ideas into the world, we need to help our inner child team up with our rational, adult selves.

Back in 1968, social scientist George Land gave a NASA creativity test to 1,600 children and got them to repeat it every five years. The results were, to put it bluntly, a bit depressing. As five-years-olds, nearly 100 percent of the children showed creativity at the highest levels, yet just five years later only 30 percent of the children displayed the same level of creativity.

The same study showed that only 12 percent of 15-year-olds displayed a high level of creativity — a similar percentage of creativity most adults display. So how do we reconnect with the creative brains we had at age five? By making wishes.

What do you wish you could do? As children, we'd have answered things like "I wish I could fly!" or "I wish I could teleport to another world!" From the airplane to virtual reality, many childhood wishes have been transformed into some of the most thrilling inventions.

Of course, lots of technical and rational thinking on a very high level went into both the airplane and virtual reality; great ideas come to life when our creative and rational sides are able to work together. Once you've discovered your childlike wishes, bring back your adult mind to work out ways to make those wishes real. Even if your ideas seem absurd, keep thinking! The solution might be right under your nose.

Think of the NeverWet spray paint developed by RustOleum. If you wished you could make things invisible as a kid, from the mess in your room to the secret writing in your diary, this invention will strike a chord with you! It allows us to spray hidden messages on sidewalks and houses that reveal themselves when it rains.

> _"As absurd or impractical as this wish is, is there some way I can make it a viable idea?"_

### 6. Use the “and” technique to come up with new ideas fast. 

Can you think up a new invention in under 30 seconds? You might consider it impossible, but a simple technique might prove otherwise! Enter, the _"and" technique_.

In 2015, the author joined the librarians of the Chicago Public Library to help them work on their creativity. He challenged them to come up with a brand new idea in just 30 seconds; he also taught them the "and" technique to help them along the way. The result? Every librarian came up with something original.

The "and" technique simply entails combining two words that seemingly have nothing to do with each other. The librarians were given two sets of cards with either nouns or adjectives on them. They were then told to choose two cards and develop a new invention based on the combination of words.

This led to moments of genius like the "illuminated sock," an idea for socks that glow in the dark so you can find them easily in the early morning, or simply socks that look striking on a Saturday night out!

How might you apply the "and" technique if you already have an idea of the industry in which you want to start a business? Say you're interested in creating a floristry business; combining the words "fish" and "film" probably won't get you far.

What you can do is customize the technique to make one of the words a fixed noun. In this case, it could be "flowers," which you'd then combine with 20 or more nouns or adjectives. You might end up with the combination of "flowers" and "celebrities," for instance. Where might that lead you?

Well, why not create a celebrity endorsement for a signature bouquet? That's rather like what Kay Jewelers did when they asked actress Jane Seymour to design a pendant for them. You could invite a celebrity to design a beautiful bouquet in collaboration with you, which you could both proudly show off to boost your brand.

> **"** _If you let yourself go, and allow yourself to play, and be a bit silly, you will probably be able to do it."_

### 7. The internet is your guide to the most important industry trends – use it well! 

How productive do you feel after sitting and surfing the web for a few hours? Chances are you don't feel productive at all! Well, it's time to rethink that assumption; the internet is a fascinating, dynamic source of inspiration that might just get those creative cogs turning.

Today, blogs and websites are updated constantly. This makes the internet a reliable source of information about what's going on _right now_, from fashion trends to current affairs. While some companies request you pay for their trend reports in certain industries, a lot of great information is yours for free online.

For instance, Cassandra Daily is a company specializing in trend analysis, with a free newsletter that provides regular updates in leading innovation fields. JWT, a worldwide advertising firm, offers users a free annual report on the 100 things to look out for in the coming year. Meanwhile, TrendHunter.com creates similar updates on a monthly basis, listing up to 50 groundbreaking products that are new on the market.

Once you've got topical trends at your fingertips, how can you put them to good use? Start by asking the following six simple questions: _who_, _what_, _when_, _where_, _why_ and _how_? Let's illustrate this by looking at the trend toward mindfulness training in classrooms.

_Who else_ might need mindfulness training? Doctors working long hours in hospitals perhaps? _What kind_ of mindfulness has potential? Exercises designed to lower blood pressure seem promising. _When, where_ and _why_ do you want to teach mindfulness? Perhaps to a bride and groom at their wedding ceremony, to help them get their marriage off to a compassionate start.

Finally, ask _how_ mindfulness could become a viable business, taking into account funding, scientific evidence and popular appeal. These questions should sketch out an outline of how you could grow a new business.

### 8. Try the billboarding technique to find your product’s edge. 

Do you know Bounty Paper Towels' famous slogan? "_Bounty — the quicker picker upper."_ Even if you're not familiar with it, you can't deny that it's catchy! Slogans like these are also a good litmus test for uncovering what's special about your product, which is why the author recommends trying a simple method called _billboarding_.

Billboarding helps you identify what makes your product stand out using three steps. First, be clear about what your idea is and what problem it solves for customers in their everyday lives. And don't forget to create a catchy name for your product!

Second, list all your product's benefits or _plus points_. What are all the things it can do for your customers? And third, pick out the strongest plus point your product has to offer; this will be the subject of a catchy phrase that you create to sell this strength to customers.

At the end of this process, you'll have a catchy name as well as a memorable slogan that captures exactly what it is about your product that makes it so special.

Let's see how billboarding might apply to a new product idea. Take the cardboard stroller for children, for instance. This product has a whole range of plus points: it's lighter and easier to move and transport than a regular stroller but is still secure enough for you to trust that your baby is safe inside. It's also less likely to cause injury if it falls over, it's cheaper and so on!

On top of these pragmatic benefits, the cardboard baby stroller also has a few creative strengths. Parents and their kids can draw on the cardboard, keeping kids entertained and engaged during trips to the grocery store. Your billboard headline could be: _The Playhouse Stroller: Go Shopping and Your Kids Will Have Fun Too!_

### 9. Final summary 

The key message in this book:

**Blank pages don't have to be daunting. With a few helpful strategies up your sleeve, from brainstorming to the "and" technique to billboarding, you'll soon learn to get in touch with your playful side and fill up that page with dozens of ideas for a brand new business.**

**Actionable Advice:** Get talking to find inspiration.

Your journey toward great business ideas doesn't have to be a solitary one! Do your best to surround yourself with people who know how to think playfully. Open your ears and mind to what they have to say and discuss your budding ideas with them — they might have a fresh perspective that becomes a game-changing idea for you!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Creator's Code_** **by Amy Wilkinson**

_The Creator's Code_ (2015) unlocks the essential skills needed to take a big idea and turn it into a successful company. Drawn from 200 interviews with dozens of leading entrepreneurs, including the founders of LinkedIn, eBay, Tesla Motors, Airbnb, PayPal, JetBlue and others, this book will help you better see what areas you need to address to turn your passion into tomorrow's market leader.
---

### Bryan Mattimore

Bryan Mattimore is an innovation specialist and founder of his own company, Growth Engine, based in Connecticut. Mattimore has fostered creativity as a session leader for organizations and an advisor for major companies, including Ford, Unilever and Pepsi. He's also the creator of Bright Ideas, a game designed to help us train our creativity.

