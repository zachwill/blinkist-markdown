---
id: 53aa85eb32633900070b0000
slug: 23-things-they-dont-tell-you-about-capitalism-en
published_date: 2014-06-24T00:00:00.000+00:00
author: Ha-Joon Chang
title: 23 Things They Don't Tell You About Capitalism
subtitle: None
main_color: D72B36
text_color: 991F27
---

# 23 Things They Don't Tell You About Capitalism

_None_

**Ha-Joon Chang**

In _23_ _Things_ _They_ _Don't_ _Tell_ _You_ _About_ _Capitalism_ Ha-Joon Chang destroys the biggest myths of our current economic approach. He explains how, despite what most economists believe, there are many things wrong with free market capitalism. As well as explaining the problems, Chang also offers possible solutions which could help us build a better, fairer world.

---
### 1. What’s in it for me? Learn why free market capitalism isn’t all it's cracked up to be! 

Most economic experts on TV or in the newspapers tend to follow the same theory: free market economics. With all these people sharing the same view, you might be fooled into thinking that there is only one economic path to take.

But you'd be wrong — there is plenty wrong with free market economics. Far from being a perfect, scientific approach, it contains many false ideas about how the economy and society works. And there are other approaches to take, even though they are largely ignored by the media.

In these blinks you will find out just what is wrong with free market capitalism and what we can all do to find better alternatives. After reading the following arguments, your understanding of economics will be changed forever.

In these blinks you'll learn: 

  * why government planning won't always lead to Soviet Russia,

  * why economists think you should avoid paying your cab fare and

  * why winning the Nobel Prize for Economics doesn't make you a financial expert.

### 2. Despite what free-market economists say, their subject is not an objective natural science. 

You will probably remember the huge financial crisis which swept the globe in 2008. You may also recall that in the months that followed, economists were — bankers aside — the most mistrusted professionals in the world. Yet this backlash was far from unfair to economists — it was a perfectly reasonable response to their arrogant attitude in previous years. Quite simply, economists had grown far too big for their boots.

One sign of economists' overconfidence was their belief that only they could fully understand the complexities of economic theory. This led them to dismiss any criticism of their beliefs as too simplistic.

Yet this was, and is, simply not the case. Instead of being too complex for outsiders, 95 percent of economics is simple common sense.

You can think of it in this way. When you go to a restaurant you know what standards of hygiene you desire even though you are not a qualified epidemiologist. And it's the same for economics; the basic principles of the subject can be appreciated by anyone. After all, you don't need to be president of a central bank to know that a country shouldn't gamble all its money on risky investments.

This arrogance also led mainstream economic teaching to discount any alternative theories.

For the last few decades, one particular economic theory has dominated: _neo-classical_ _free_ _market_ _theory._ This belief assumes that every individual in society acts as a rational, selfish agent, who only makes an economic decision by calculating how much it will benefit them. The economic profession came to regard this theory almost as a natural science. This led them to focus too much on a normative theory rather than on its application in the real world.

But rather than being an objective science like physics, economics is a social science. This means that there are many potential alternative theories — each just as provable as the free market approach.

In the following blinks we will discover the central faults in free market economic theory.

### 3. Despite what free market economists believe, individuals cannot make completely rational economic choices. 

Way back in 1997, two economists called Robert Merton and Myron Scholes were awarded the Nobel Prize in their field. Their theories built mostly on the idea that when making economic decisions — such as where to invest their money or which product to buy — humans made completely rational choices.

After winning such a prestigious prize, the two economists applied their theories to the real world. However, instead of going on to make huge piles of cash, they found their companies bankrupt, not just once, but twice in little over a decade!

The failure of Merton and Scholes teaches us one important point. You cannot expect human beings to always act rationally.

Why?

In order to make rational decisions, individuals need to take every possible detail into consideration. For example, when deciding where to invest our life savings, we need to know about every possible scenario, every alternative possibility, and so on. Only when we have all this information can we make the best choice.

Yet in the modern world one cannot possibly hope to process every speck of information before making a decision, and so our choices cannot be rational.

However, this does not mean that we act completely irrationally. Instead we are subject to _bounded_ _rationality._ We try really hard to be rational, we just lack the intellectual capacity to make the perfect decision. But how can we change our economic thinking to accommodate this?

To help us make the best possible decisions the government needs to step into the marketplace to limit our economic choices. If we are only presented options whose effects we can understand, we can start to make better decisions. The government already acts in this way in other areas. For example, the authorities stop us from getting hold of drugs with unknown side effects, or cars with poor safety standards. Why shouldn't they apply the same rules in the financial world?

### 4. Human beings are not completely selfish; we often act out of altruistic concerns. 

Have you ever been tempted to run off without paying for your taxi ride? The chances are that, as long as the driver isn't Usain Bolt, you could easily get away with it. Yet although the thought may sometimes cross your mind, you _always_ ignore it and pay your fare.

But, as reasonable as paying for your cab may seem, free market economists would argue that you are acting irrationally. They claim that we are all programmed to only act selfishly and therefore we should always run away without paying.

To explain why we don't seem to follow their theory, free market economists point to our actions' _hidden_ _rewards_ _and_ _sanctions._ These are the costs and benefits that, while not being immediately clear, will have an impact in the long term.

So the reason we pay for taxis is that we don't want to build up a reputation as a fare dodger. A person with such a reputation would be completely shunned by taxi drivers, and would never be able to get a cab again.

However, the theory of hidden costs and sanctions doesn't work in a selfish society.

Let's return to the taxi example. If we ran away, the role of sanctioning us would fall to the taxi driver. He would have to chase us down, get our fare from us, and probably take our picture to show other taxi drivers in the area. And while he was doing this, his cab would be left completely unattended, at risk of theft or damage.

If he were to only think of himself there would be no benefit for him — the fare he'd reclaim would be small and why should he help other taxi drivers out by tracking us down?

The fact is that we pay for our taxi ride because we have other concerns, like honesty, honor and respect, and not just pure selfishness.

### 5. The economy does not pay people what they deserve to earn. 

Would you agree with the statement "we should all earn what we deserve"? Sounds reasonable, doesn't it? However, if you are from a rich country then you might want to think twice about desiring this. If you were to be paid what the market thinks you deserve then you'd probably find your income levels dropping at an alarming rate. How can this be?

This is because the wages of workers in the developed world are protected from the pressures of the market, which means they remain high, no matter how valuable the work may be.

For example, whatever your job, there will be people in other countries who will do it for less money. You aren't subjected to this competition because the government protects your job. They use strict immigration controls to prevent people from poorer countries entering the workforce. Safe from these workers, your wages remain artificially high.

This example also shows that it is not your skills that determine how much you earn, but purely the society you live in. If you happen to live in a prosperous, productive society, your wage will be pulled up by everyone else. Even if you happen to be the laziest, most unproductive worker, you will still earn more than a hard worker in a poor country.

And this unfairness in wages can also be seen in individual societies.

Those whose incomes place them toward the top of society will find themselves earning far more than they deserve compared to those at the bottom. For example, in the early 1990s top executives found that their wages increased by 100 times the salary of the average worker. 20 years later this gap had rocketed up to 400 times.

Is this because the executive is worth more than the worker every year? The evidence would suggest not; the average executive is simply not 400 times more productive than the average worker. Thus in market terms their increase is not deserved.

### 6. A strong manufacturing sector is more essential to economic growth than a strong service or tech economy. 

What do you think when you drive past an abandoned, crumbling factory? If you live in the developed world you may connect it with media stories about the decline in domestic industry. Together these might make you think that manufacturing is nearly dead in the West — yet you'd be wrong.

People often think that industry is in decline because they misread statistics.

For example, people see that fewer people are employed in manufacturing than in the past. Yet it doesn't mean there is less industry, just that it is more efficient.

Nevertheless, many policymakers have suggested that developing countries should think seriously about moving away from manufacturing to the service and knowledge economies. Yet this move would be bad for the overall economy.

Take services, for example. The service economy, such as retail work or the IT sector, has grown in size over the last few decades. Yet it would be risky to grow too dependent on this sector.

One problem with the service sector is its slow rates of productivity growth. It most cases an increase in a given service's productivity will lead to a lower quality end product. For example, a performance of _Macbeth_ that lasts ten minutes would be a more productive one, yet its quality would be seriously compromised. Therefore an economy reliant on services will find itself growing relatively slowly.

Then there is the _knowledge_ _economy,_ which relies on the creation and spread of information. Since the creation of the internet people have suggested that the knowledge economy has enormous potential.

Yet this is massively overrated. Far from being a revolutionary invention, the internet has had far less impact that previous developments in communication. The telegraph, for example, reduced the speed it took to pass a message from two weeks to 7.5 minutes, bringing the time down by a factor of 2,500.

Yet the internet only improved efficiency from ten seconds (from a fax machine) to two seconds, a drop of only five times.

### 7. The financial crisis was caused by the deliberate build up of risk in the system. 

The financial crisis of 2008 hit the entire global economy hard. It ended over a decade of economic growth and brought some of the biggest financial firms to their knees.

However, many of the companies most affected by the crash — for example, the insurance giant AIG or the investment bank Lehman Brothers — actually played a large role in their own downfall.

How?

In the years before the crisis the financial system had grown more complicated. In order to find new products to trade, bonds called _financial_ _derivatives_ were created. Although these were highly profitable at first, their sheer complexity hid high levels of risk.

In order to create these derivatives, pools of securities such as mortgage loans were used to create more and more types of financial derivatives. And the more of these that were created from the same security, the higher the risk climbed.

Imagine building a house on a tiny piece of land. As you can't build outward you decide to build upward and so add storey after storey onto the house. What do you think would happen? With every new storey you add, the stability of the house decreases, and it becomes more and more wobbly.

And added to this was another problem. Each new financial product that was created was of a poorer quality than the last. Let's return to our narrow, yet tall house. Imagine building each new level out of increasingly shoddy material, such as paper or plasticine. It is clear that such a structure could not last long before collapsing.

And although countries across the world suffered from the crash, those hardest hit were the ones who had most liberalized their markets.

For example, Ireland and Latvia, which had both opened up their markets in the years before the crash, suffered greatly; the Irish economy shrunk by 7.5 percent and the Latvian by 16 percent.

### 8. Despite economists’ fear of government economic planning it is already happening and it is performing well. 

Should the government ever interfere in the running of the economy?

Free market economists would be quick to answer no. They would argue that when a state meddles in the economy the result is always chaos. These economists point to the complete failure of controlled economies such as those in the Soviet Bloc, declaring this is what will _always_ happen when the state interferes.

Yet despite what free market theory suggests, governments can and do play a crucial role in economic growth.

For example, the state often has a better knowledge of the whole economy than individual companies. And this knowledge can be used to support the most profitable industries.

This is what happened in South Korea. The huge electronics firm LG originally wanted to focus on the textile market, but the government disagreed. They knew that the firm would have more success focusing on electronics and so pushed it into that market.

And this isn't just government policy in the developing world. The US government also carefully supported the early development of the now massive internet, biotechnology and aircraft industries.

But why did government planning work in these cases but not in Soviet Russia? The key is not trying to control too much.

If the state tries to control every aspect of the economy, as it did in the communist world, it will come unstuck. However, if it merely helps guide the system by setting loose goals, such as inflation targets and controlling interest rates, then it can achieve success.

In doing this the state acts a bit like a company CEO. The goal of a CEO is to set strategic goals to ensure that their business travels steadily in the right direction. The goal of the state is to do the same thing for the whole economy.

> _"There is no such thing as the free market."_

### 9. Social welfare is vital for strong economic growth. 

Across the developed world free market economists are calling for governments to cut their social welfare benefits. They argue that paying people benefits like unemployment allowance or holiday pay rewards them for doing no work.

Yet despite what their theories suggest, evidence from the real world shows that far from being a drain, social welfare is vital for economic growth.

We can see this if we look at labor markets. Countries which provide lots of help for the unemployed have more dynamic economies than those who limit support.

The reason for this is clear. In countries with little assistance for the unemployed, people are terrified of losing their jobs. Therefore they look for work in areas of the economy where they feel safest. They often gravitate toward stable professions such as healthcare and law. While these may be socially important, they do not deliver high levels of economic growth.

In order to achieve growth, you need to encourage people to enter riskier, more entrepreneurial areas of the economy. Not surprisingly, the nations who offer support for those that try, but fail, will always do better than those where the cost of failure is poverty.

And while evidence shows that social welfare helps growth, the opposite can be said of the free market alternative, called the _trickle-down_ _effect_.

Free market advocates suggest that if the government spent less money on welfare it wouldn't need to collect this in taxes. Rich people would then be free to invest their money directly in the economy. This money would then trickle down the economy as investments create more growth and jobs.

Yet where it has been tried, the theory has not produced the desired results. In countries which adopted free market policies in the 1980s, such as the United States and United Kingdom, growth actually slowed as a result. And as growth stuttered, the money stopped trickling down and stayed at the top.

For example, between 1979 and 2006 the top one percent of earners in the United States more than doubled their share of national income from ten percent to 22.9 percent.

### 10. We need to stop trying to fix developing countries with the wrong tools. 

Many politicians, economists and popstars in the West are sure they know what policies will help end poverty in the developing world.

Yet despite their confidence, it seems that much of the developed world's thinking towards poor nations is based on false ideas.

One fallacy popular among Western policymakers is that the causes of poverty in the developing world are structural. Structural causes include an inhospitable climate, being landlocked, or having difficult terrain. However, if this were the case then wouldn't the landlocked, mountainous countries of Austria and Switzerland struggle economically?

Another misplaced Western theory is that developing countries lack the dynamic entrepreneurial spirit of the developed world. Yet this is simply not the case: self-employed people in developing countries make up 30–50 percent of the workforce. Whereas in the rich West only ten percent of the workforce are self-employed. You cannot possibly argue that non-Western countries lack entrepreneurial spirit.

Westerners who question why developing nations remain poor should look for their answer closer to home. The fact is that the Western free market policies imposed on the developing world are a huge reason for their poverty.

For example, in the 1960s and 1970s, the countries of Sub-Saharan Africa were experiencing decent growth. This was because their governments protected their economies: domestic industries were subsidized and protected from outside competition. However, as soon as Western governments in the 1980s forced them to open their markets, their domestic economies stuttered and failed.

If we want to reverse this trend and get developing countries moving forward, we need to remember how we in the West became rich in the first place. It happened because in the nineteenth century Western countries protected their economies from foreign competition. In the United States, for example, foreigners were forbidden from becoming financial directors and custom tariffs on goods coming into the country stood at 50 percent.

Wouldn't it be better to let developing nations follow this path?

> US vice-presidential nominee Sarah Palin once thought Africa was a single country!

### 11. It is not capitalism that is the problem, but the way we design it. 

After reading these blinks you might be starting to get really angry with capitalism. Yet before you sign up as a member of the Communist Party, you need to realize one thing: it is not capitalism itself at fault, just one type — free market capitalism.

In fact, capitalism can be be a very effective way of managing the economy.

For example, _the_ _profit_ _motive_, or the desire to make money, is a powerful driver. A great many inventions and innovations have resulted from people's desire to create a successful enterprise.

And capitalism is also an efficient method of coordinating the economy.

The market is a great way to ensure that labor and capital quickly gets to the areas where it is needed the most. Without the market directing people where they are needed, we might end up with hundreds of rock stars and not enough plumbers!

However, despite the advantages that capitalism can bring, it can be incredibly dangerous if not regulated properly.

We should think of the capitalist economy like a car. If you produce a car without any safety features, such as brakes or seatbelts, then the chances are that eventually the vehicle will crash and people will be injured.

Unfortunately, the current dominant approach to capitalism suggests that we should create a largely unregulated system. But there are alternatives. It is possible to move away from the free market and build a better, fairer, and safer capitalist system.

One way we could do this is to apply the idea of bounded rationality — the idea that we make better choices when we have a limited range of options.

To follow this approach we would need to give the government a little more power in the economic system. It would then use this power, for instance, to remove the ability of bankers to make risky investment choices. This would allow society to make more informed and safer choices.

> **"** _Capitalism_ _is_ _the_ _worst_ _economic_ _system,_ _except_ _for_ _all_ _the_ _others."_

### 12. Final summary 

The key message in this book:

**Don't** **believe** **professional** **economists** **who** **tell** **you** **that** **the** **free** **market** **is** **the** **only** **way** **to** **manage** **the** **economy;** **there** **are** **other,** **fairer** **options** **for** **us** **to** **chose** **from.** **We** **all** **need** **to** **focus** **on** **these** **alternatives** **to** **build** **a** **better,** **more** **stable** **and** **equal** **world.**

Actionable advice:

**Be** **careful** **whom** **you** **vote** **for.**

Often politicians try to encourage votes by telling the electorate they will give them tax cuts. Although this might sound positive at first, try and remember what will have to be cut to provide this rebate. If you value public services, then it may be better for you to vote for someone else.

**Suggested further reading: _Economics_ by Ha-Joon Chang**

_Economics: The User's Guide_ lays out the foundational concepts of economics in an easily relatable and compelling way. Examining the history of economics as well as some critical changes to global economic institutions, this book will teach you everything you need to know about how economics works today.
---

### Ha-Joon Chang

Ha-Joon Chang is one of the leading critics of free-market economics. He's a professor at Cambridge University specializing in institutional and development economics and has published many widely discussed books about economic development such as _Bad_ _Samaritans_ and _Kicking_ _Away_ _the_ _Ladder_.

