---
id: 575410843ead360003c8e752
slug: 30-ways-to-reboot-your-body-en
published_date: 2016-06-06T00:00:00.000+00:00
author: Ben Greenfield
title: 30 Ways to Reboot Your Body
subtitle: A Complete User Manual for Getting the Most Out of the Human Body
main_color: B52442
text_color: B52442
---

# 30 Ways to Reboot Your Body

_A Complete User Manual for Getting the Most Out of the Human Body_

**Ben Greenfield**

_30 Ways to Reboot Your Body_ (2015) gives you the keys to repairing and regenerating your body so you can take your health and fitness goals to the next level. These blinks explain the importance of digestive health, light exercise and solid routines. Get ready to have your ideas about diet and exercise challenged.

---
### 1. What’s in it for me? The healthy body you’ve always desired. 

How healthy do you think you are? Very? Well, perhaps you don't run a marathon every few months. But you're probably healthy _enough_, which is the category that most people find themselves in. You might not be a dedicated athlete, but you move around enough to stay in shape.

And that might be just fine for you. But did you know that with just a few changes, you could easily become a _very_ fit person, able to take on challenges you never before thought you could achieve? These blinks showcase the highlights of the author's 30 easy ways to become a fitter, stronger, healthier person.

In these blinks, you'll also discover

  * how a microwave can help get you moving;

  * why no exercise regimen is as important as a good night's sleep; and

  * why you need to be wary of electrical pollution.

### 2. To start pushing your health and fitness goals forward, identify which habits hold you back. 

Let's face it: you're not as fit as you'd like to be. But that doesn't mean you should just give up.

The first step is learning what can be improved. Do you eat too many processed foods, drink too much alcohol or caffeinated beverages, or take paracetamol or other medications unnecessarily? These are the kinds of bad habits that lead to chronic inflammation and damage your body.

Consuming these "bad" foods essentially wreaks havoc on your gut. You might not know it, but your gut is one of the most important systems your body has. It makes up _three-quarters_ of your immune system! And yet so many people don't properly care for their gut. Their poor diets strain their digestive systems, which leads to hormone imbalances, excessive hunger, fatigue and worse.

It's not just what you eat that can damage your gut, but your stress levels have a powerful impact, too.

We accept stress as a normal part of our professional lives. But stress is dangerous. Constant stress increases levels of the hormone cortisol in the body. When it's produced at high levels for extended periods of time, cortisol can harm bones and muscles, and disrupt normal hormone regulation.

Our jobs and relationships aren't the only sources of stress, either. Excessive exercise can stress your muscles and joints. Many think that staying active means exercising hard and pushing yourself to the limit. But guess what: people burn more fat by performing lighter exercises more frequently.

By all means, keep up your heavy program if you like it! But do so only occasionally. Regular, light exercises will help you stay fit, and they're much easier on your body, too.

With these basics in mind, let's move on to a more detailed fitness strategy.

> _"If your body is not exposed to loads, your precious muscles atrophy quickly! And the older you get, the more difficult it can be to get that muscle back."_

### 3. Begin your journey toward a healthier lifestyle by taking the time to reboot your body. 

Wouldn't it be great if you could wake up tomorrow and simply be healthier? But Rome wasn't built in a day. A realistic plan for rebooting your body takes four to 12 weeks.

So, let's start with a solid first step: an assessment of your body.

If you've been avoiding a check-up at the doctor's, now's the time to book an appointment. From blood tests to examining your family's history of disease, it pays to be thorough. This check-up will help you identify problem areas or extra precautions you may need to take. Check if diabetes is prevalent in your family, for example, or if your thyroid isn't functioning at 100 percent.

The next step is to create a plan. What are your health goals? Do you want to lose a specific amount of weight, get fit enough to run a marathon or stop eating junk food once and for all?

Define your goal and break it down into tasks you can achieve every day, from eating a certain amount of green vegetables to exercising for one hour each evening.

But how do you stay on track? Easy! By _measuring_. Look at your goals and decide which metrics would be most helpful in assessing your progress. One metric might be your _heart rate variability_, which indicates how fast your heart reacts when you start exercising (the higher the better).

Your gut has an important role to play, too. These weeks are your chance to clean all that bad stuff out of your body. Inflammatory foods that contain gluten, caffeine or artificial sugars have to go.

Build a new foundation for better nutrition by remembering how food gives you energy. Carbohydrates and fats are the main sources of energy in the human diet. We tend to rely on carbohydrates for energy, and as a result, consume more carbohydrates than we should. This causes fat to accumulate in our bodies, and can even lead to allergies and other complications.

So try to switch to getting your energy from healthy fats, and reduce your carbohydrate consumption.

> Eat nutrient-dense foods such as organ meats, as they give you all the nutrients you need and won't leave you hungry.

### 4. After rebooting your body, introduce new elements into your health and fitness routines. 

So you've built the foundations for a healthier body. But why stop now? There's so much more to gain.

Learn how to consume delicious things in a more sustainable way and take your body to the next level. Food, after all, should be enjoyable. And eating a restricted diet, while useful during a reboot, isn't a long-term solution for your health.

What you've got to work on now is _reintroducing_ foods into your diet. Here it's important to focus on only reintroducing foods with the least negative effects, and enjoying them only in moderation. For example, a small piece of dark chocolate every now and then can still be justifiable.

Get your fix for wheat and grains by eating varieties that are soaked, sprouted or fermented. Quinoa is a great option. Dairy can also be a part of your diet, ideally raw or organically raised. Fermented varieties of soy, such as tempeh, are also good. Black coffee is a great way to start the day, and you can even indulge in a glass of organic red wine, on occasion.

Measurement is just as important in this phase as in the previous one, so keep an eye on how your body responds to new foods. Everyone's gut is different and sensitive to different things. What works for others may not work for you.

At this stage, you're also ready to start expanding your exercise routine. You'll want to increase both your strength and lung capacity, combining slow training and high-intensity interval training.

It's important at this stage to keep your activity levels high by making movement an integral part of your day. For example, set up a pull-up bar at home and do five pull-ups while you're waiting for the microwave to ding! And avoid sitting down for too long. Try experimenting with a standing desk — you'll be surprised at how much better you feel after a few hours of not slouching in a chair!

You can even bring movement into your social life by exercising with coworkers or signing up for running groups. It's a great way to meet people and prevents your exercise routine from becoming stale.

But we're not finished! The next stage is about making living healthy instinctive.

> _"To truly elevate your body, movement must become a part of your daily routine."_

### 5. Live to the fullest by vitalizing your body with smart exercises and solid routines. 

If you've come this far, your body is performing better than ever. If you're ready to kick things up yet another notch, then it's time to get _vitalizing_.

Start by incorporating new tricks into your training routine. When you perform daily exercises and sprint a few times a week, you'll get tired and fatigued. But highly specialized training can fix that. For example, you can try _hypoxia_, the exercise of depriving yourself of oxygen for brief periods. You can do this by swimming 25 meters without breathing — it can help keep you fresh and fight off your body's aging mechanisms.

You can also test new strategies to develop stamina. In early stages, pushing your body to the limit is not recommended; but now that you've achieved peak fitness, you're ready for more. Exercise at your utmost capacity at least once a month, and at most once a week. Just make sure you allow your body time to recover. You won't get stronger unless your body can restore itself regularly.

That's why you have to change your lifestyle, as well, like by making sleep a very high priority. A healthy sleep rhythm is essential for maintaining health and fitness, and it's no surprise: sleep is your brain and body's prime time for recovery and repair.

Eight hours of sleep is a minimum which should never be compromised — people who sleep any less tend to develop health issues.

One trick for better sleep is to beware of electrical pollution. Since your cells' ability to absorb nutrients depends on their voltage, it's critical you stay away from areas with lots of electrical currents. You can do this by switching off your digital devices.

There's one last thing to remember in this final stage, and it might seem surprising. If you ever feel like "cheating" when handed a slice of cake, for example, sometimes it's ok to give in.

Cheating occasionally or eating something that's not healthy is nothing to feel guilty about. In fact, eating a calorie-rich meal can even boost your body's weight-loss ability, as the body's hormone signals go into overdrive in response to a surprise treat.

### 6. Final summary 

The key message in this book:

**Unhealthy eating, chronic inflammation, imbalanced hormones and poor training plans have wrecked our bodies and brains. You can change this situation not only through more training, but through a radical reboot of your body. Only then can you enhance your health and performance and push on to the highest levels of achievement.**

Actionable advice:

**Get some air!**

We've been conditioned to spend our days indoors, sitting, without complaint. It's time to break the habit and learn to love the outdoors again! Getting out is a great motivation to move. Locate your nearest parks, woodland reserves or even playgrounds. Make time in your weekly routine to lose yourself outdoors and reconnect with your body and local environment.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Eat, Move, Sleep_** **by Tom Rath**

_Eat, Move, Sleep_ (2013) offers simple tips for improving your health and well-being in some very important ways. You don't have to revolutionize your lifestyle to get in shape and increase your energy levels — little changes can make a big difference, and these blinks will show you how.
---

### Ben Greenfield

Ben Greenfield is a bestselling author, _New York Times_ writer, former bodybuilder, Ironman triathlete and coach. He is a regular speaker on health and fitness topics.

