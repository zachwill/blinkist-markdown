---
id: 55102687666564000a760000
slug: 5-levels-of-leadership-en
published_date: 2015-03-26T00:00:00.000+00:00
author: John C. Maxwell
title: 5 Levels of Leadership
subtitle: Proven Steps to Maximize your Potential
main_color: 2EADE5
text_color: 1F7499
---

# 5 Levels of Leadership

_Proven Steps to Maximize your Potential_

**John C. Maxwell**

_5 Levels of Leadership_ is a step-by-step guide to becoming a true leader with a lasting influence. Using engaging real-life anecdotes and inspiring quotes from successful leaders, it describes key pitfalls that may be holding you back and explains how to overcome them.

---
### 1. What’s in it for me? Climb the five-step ladder to leadership success. 

Have you ever worked for a boss who thinks that having the word "manager" in his job title automatically makes him an excellent leader? If you have, you'll know that such people are usually the most incompetent leaders imaginable.

The best leaders recognize that a position of authority is just one step on the path to becoming a great leader, and these blinks will show you all five steps anyone can take to become the type of leader people willingly follow.

In these blinks you'll learn

  * what the US Marine Corps can teach you about leadership;

  * why you should become a flock of geese rather than a herd of buffalo; and

  * how you can leave a legacy.

### 2. Follow the five levels of leadership to become a true leader with lasting impact. 

If you've ever had the pleasure of working under a great boss, you already know that having an inspiring leader can make a world of difference. Great leaders have the vision — that is, the ability to navigate a dynamic, fast-changing business world.

A modern company is like a sailboat on a stormy sea. Like the captain, the leader knows how to navigate the stormy waters and chart the course to get to where you're going. After all, the steering wheel and sails can be operated by anyone on-board, as long as there's a leader to set the right course.

As you can see, having a visionary, forward-thinking leader is crucial. However, it may surprise you to learn that these kinds of leaders are made, not born.

Pretty much anyone can be appointed to a leadership position. But that doesn't automatically give them influence or sway over other people. We've all experienced bosses who seem to think that their rank allows them to do anything they please, but leaders like that never manage to motivate their team or achieve the desired results.

On the other hand, influential leaders know how to reach the right people, develop a good strategy and get things moving. And as we'll see in the following blinks, there are five distinct levels you have to pass in order to become that kind of effective leader.

It's important to note that learning these leadership skills is like climbing a ladder: you have to master each step before you can climb higher. So keep reading to find out how to climb the leadership ladder and become a truly inspiring boss.

> _"Leaders are always taking people somewhere. They aren't static. If there is no journey, there is no leadership."_

### 3. Don’t just rely on your management position: Inspire others by bringing your personality and core values to your leadership role. 

Have you ever had a boss with lots of formal authority but no actual talent for leadership? If so, you've experienced a _positional leader_ : someone at level one of the leadership ladder, who thinks position alone gives them the power to command.

Unfortunately, positional leaders waste energy and fail to get the best out of their subordinates. After all, when formal position is the only thing legitimizing your leadership, you have to spend all your time defending your authority.

In fact, we often see positional leaders behaving like medieval rulers, obsessed with accumulating huge armies and castles to buttress their power. In real-world terms, imagine a manager obsessed with attaining the largest possible staff and the biggest budget all in order to make themselves look important.

Exacerbating the poor leadership, positional leaders have trouble giving their employees direction. They may order people around, but the subordinates will do only what is specifically expected, and very little more.

The lesson here is that instead of seeing a leadership position as your only goal, you should see it as a starting point. It's a chance to develop your own way of leadership and create lasting influence.

Here's another way to think about it: Although you may have a defined position within the company, you still have to fill it with life by choosing core values and deciding to implement specific working practices. Ultimately, you can develop any kind of leadership style, as long as it's effective and true to who you are.

The former CEO of Southwest Airlines learned this lesson while observing the behavior of two court litigators. One was very diplomatic and calm; the other was aggressive and emotional. But despite the difference in style, both were outstandingly successful. He realized that you have to bring your own personality to the position.

### 4. You need people’s permission to lead them: Build trust and positive relationships to make the most of your leadership. 

As we've learned, positional leaders waste energy protecting their own interests and power. The best way to break free from this type of leadership is by shifting focus from ourselves to others.

The advantages of being more outward-facing are manifold: positive relationships and a pleasant environment raise energy levels and foster an enjoyable workplace.

Consider what happens when you spend time with people you don't like. Most likely, it drains your energy and feeds a negative outlook. On the contrary, spending time with friends is like recharging your batteries — it makes us feel energetic and full of life!

So, how do you build up these positive relationships? You need to get people's _permission_ to lead them. This is the next step on the managerial ladder.

You have to value the people around you and treat them accordingly. You'll find that if you practice a positive mode of communication, you'll connect with others easily.

To that end, imagine commanding a co-worker in an aggressive way; he'll probably respond defensively. On the other hand, if you stay calm and gently ask for a favor, he'll probably be open to your message, making it more impactful.

This kind of communication is crucial, because when people around you trust you and feel appreciated, they tacitly give you permission to lead them.

In other words, people cling to leaders they can trust. Especially when your workplace is dealing with difficult situations, feeling that someone's looking out for everyone on the team strengthens ties.

For instance, at the U.S. Marines, one of the core values is to leave no one behind, regardless of status. That's why when they go into combat, officers waive their insignias of rank. Not wearing this symbol sends a clear message: we are in this together; we live and die together, regardless of rank. And this ethos plays a key role in the organization's imposing reputation.

> _"People go along with leaders they get along with."_

### 5. Leadership is all about producing the right results: A good team needs a good leader to chart the course to success. 

Though getting your subordinates' permission to lead them is important, in most companies leaders are only measured by the outcome of their leadership.

This is why level three of the leadership ladder is the ability to _produce_ results. And for any leader, the best way to produce results is to build an effective team.

But how is this done?

First, it's important to recognize that an effective team isn't simply a collection of individuals. Rather it's a finely-calibrated balance of various skills and attributes. In other words, a good team is greater than the sum of its parts, because all the strengths are fused together to balance out the weaknesses.

For instance, think of a basketball team, which consists of specialized players each perfectly fulfilling the demands of their specific position, whether dunking or shooting three-pointers. Members play in their own unique sweet spot, thereby maximizing strengths and minimizing weaknesses. 

This kind of collaborative action is also well-suited to solving complex problems. Or as basketball coach John Wooden said, "Everyone who scores a basket has ten hands." Meaning, each time an individual player scores, it's thanks to his teammates' contributions.

But of course, every good team needs a good leader, someone who ensures that everyone's working together and heading in the right direction.

And that leader is also responsible for identifying strengths and weaknesses to compose the team. Bringing back the basketball analogy, you have to put your best defensive players in defense and the best attackers in offense.

Furthermore, it's crucial for the team that the leader never loses sight of the big picture and charts a course towards the final destination.

Even in sports, the final goal isn't always obvious. Just as in business, succeeding at basketball is way more complex than just "winning the game." The coach has to also ask herself how she can prevent the team from overexertion, to save some energy for upcoming tournaments? In the business world, leaders have to ask the same questions.

> _"The job of a leader is to build a complementary team, where every strength is made effective and each weakness is made irrelevant."_

### 6. Great leaders produce results by setting an example to their team and creating a wave of momentum. 

There's more to producing results than building the right team. As a leader, you must also lead by example. If you perform aggressively or reluctantly, this attitude will translate into poor team performance. But if you show enthusiasm and drive, people will follow suit.

For example, legend has it that once during the Revolutionary War, George Washington rode up to a group of soldiers who were having trouble raising a beam. Instead of helping directly, the commander first stood aside and shouted words of encouragement. But when he realized that the task exceeded the soldiers' strength, Washington dismounted his horse and joined the struggle. Soon enough, the group succeeded in lifting the beam.

But there's one thing that's even more important than inspiring people through your own actions: capitalizing on your victories to build _momentum_.

Producing positive results is uplifting, and it will motivate your team to produce even better results. As essayist Thomas Carlyle said: "Nothing builds self-esteem and self-confidence like accomplishment."

This virtuous cycle allows you to build up momentum in your team, and momentum is every leader's best friend, for it amplifies good results and nullifies bad ones.

Just think if Apple, for example. For years it was a niche company struggling to make inroads against the market leaders IBM and Microsoft. But thanks to products like the iPhone, it managed to turn the tide. Since then, it seems the company can do no wrong: nearly every move it has made has been lauded. This is thanks to the tremendous wave of momentum it has been riding: success begets success.

> _"Level 3 leaders take their people where they want them to go — they do not send them."_

### 7. The key to level 4 leadership is to foster personal development throughout your team. 

One key challenge all leaders face is today's ever-changing business-environment. What works now may no longer work next year, so attaining sustainable, long-term success is very challenging. What can you as a leader do about it?

Simple: remember that any company's greatest resource is its people. After all, markets change, facilities deteriorate, supplies get used up, but people have the potential to grow and surpass themselves.

And that's exactly why successful companies focus on improving their people — that is, giving them room to learn and grow.

In 1999, _Fortune_ named Synovus Financial Corporation the best place to work in America. And looking back, the former CEO says that the award was only possible because the company always emphasized team-building and people development.

A true leader is like a mentor, and this is reflected in the next step of the leadership ladder; focusing on transforming the people around him, the leader will help them grow. To this end, people development is one of the most important aspects of leadership.

For example, the author observed that transformative leaders only put 20 percent of their effort in personal productivity; expending the other 80 percent on staff development. Meaning, great leaders put their own advancement far behind the broader goal of building a phenomenal team.

It's important to note that personal development isn't just good for the company; it's also a major human need. Fulfilling it will give your employees a sense of loyalty to the company, reducing possible interest in seeking opportunities elsewhere.

As we've learned, people development is extremely important for the long-term success of an organization. And as a mentor, you have to find the balance between teaching your employees and empowering them to grow on their own.

As you'll see in the next blink, that means you have to put some power in their hands.

> _"Leadership is accepting people where they are, then taking them somewhere."_

### 8. Developing future leaders is the pinnacle of leadership and creates a lasting legacy. 

Becoming a successful leader is a long process, but the rewards are great. Still, the journey never really ends. Because a true leader keeps giving back by teaching others to become leaders, too.

There are a few reasons why this is important.

Firstly, having just one leader creates an organizational bottleneck. In other words, when just one person makes all the decisions, the rest of the company becomes passive and dependent. The authors even compare traditional leadership models to a herd of buffaloes.

Instead of blindly following a head buffalo, truly effective organizations are organized like a flock of geese, flying in a V formation and continually rotating who is at the head to oppose the wind.

Another reason having just one leader doesn't make sense: when that person quits, how can anyone else be ready to take on the load?

Before that time comes, it's crucial to create a culture of leadership within the organization and empower team members to become leaders themselves.

How? Simply include them in the managerial process, allowing them to partake in key decisions. This allows them to develop their own leadership skills.

Developing future leaders is not only an organizational necessity; it's also the final step in the ladder; the pinnacle of all you do: creating fellow leaders who can act autonomously and also rear their own new leaders will effectively reproduce your work to infinity.

Consider Socrates' model. The Ancient Greek philosopher's student, Plato, founded an academy that passed on knowledge to generations of students. Among these students was Aristotle, who later became an important mentor of Alexander the Great. Leaders crafted future leaders.

And, there's one more important benefit of teaching others: seeing other people grow and follow in your footsteps is greatly rewarding. In fact, it's the greatest source of personal fulfillment there is!

> _"The final test of a leader is that he leaves behind him in other men the convictions and the will to carry on." — Walter Lippmann (journalist)_

### 9. Final summary 

The key message:

**A true leader isn't characterized by her formal job description, but rather by her influence on other people. To have the greatest impact as a leader, support your staff, build effective teams and allow people to grow and become leaders themselves.**

Actionable advice:

**Listen carefully to how your employees are doing.**

In our daily lives when most of us ask, "How are you?" we don't really pay attention to the response. As a leader, you need to change this. In order to establish a connection to the people you work with, try to really _listen_ to their answers: Watch the other person's body language to try to gauge how they're feeling; be genuine to establish a new level of understanding and nurture trust.

**Suggested** **further** **reading:** ** _The Effective Executive_** **by** **Peter Drucker**

In _The Effective Executive,_ author Peter Drucker offers a step-by-step guide to becoming a more productive and effective executive. By mastering a few procedures and principles, you can develop your own capacities as a leader and also support your employees' strengths, with the goal of improving results across your organization.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### John C. Maxwell

John C. Maxwell is a prolific best-selling author who focuses on leadership. He's also the founder and leader of EQUIP, a non-profit organization specializing in leadership development. In his past career, he was a reverend.

