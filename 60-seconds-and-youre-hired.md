---
id: 58f146a586c51e0004dd646b
slug: 60-seconds-and-youre-hired-en
published_date: 2017-04-19T00:00:00.000+00:00
author: Robin Ryan
title: 60 Seconds & You're Hired!
subtitle: None
main_color: EFBD30
text_color: 806519
---

# 60 Seconds & You're Hired!

_None_

**Robin Ryan**

_60 Seconds & You're Hired!_ (1994) is your guide to nailing your next job interview. These blinks are packed with actionable advice on how to grab a potential employer's attention, sell yourself and land the job of your dreams.

---
### 1. What’s in it for me? Nail that dream job. 

The job market is increasingly competitive, and keeping the attention of a recruiter for longer than a few seconds is often a challenge even for those with the fanciest résumé-formatting skills.

Well-intentioned tips on how to nail your dream job are endless, covering everything from the perfect time to send in your application to the pitch of your voice when speaking to the interview panel. But with so much to think about, it's easy to lose sight of the real essentials.

That's where these blinks come in. They break down the interview process and remind us of the handful of critical elements that will ensure a successful application.

You will find out

  * why looks matter more than books;

  * why the ideal interview would last 60 seconds; and

  * what you really need to tell your future boss to grab his or her attention.

### 2. Employers have to make quick hiring decisions; grab their attention. 

Have you ever been nervous before a job interview? Well, you're not the only one. Employers get worried before interviews too.

After all, they're under tremendous pressure to make good hiring decisions because bad decisions cost their company money. Just remember that for every employee who's sacked, the company has to pay three times their monthly salary plus all the costs of hiring and training a replacement as well as fixing their errors.

Tony Hsieh is the CEO of the top online shoe and clothing store, _Zappos_. Hsieh says that bad hiring decisions have cost his company upwards of $100 million!

These costs accrue because the people recruiting new employees are required to make rapid decisions. They receive hundreds of applications and, after reviewing several candidates, find these potential staff members all start to blur together.

That's why the 60-second strategy is necessary to secure the job you want. It's all about grabbing the attention of your interviewer with concise answers of 60 seconds or less.

Such brevity is important in a world that's moving faster than ever. People are used to communicating through text messages, Twitter updates of 140 characters or less and news clips of just one to three minutes.

Since everyone is affected by these rapid forms of communication, holding a person's attention can be a gargantuan task. That's especially true for job interviews, as employers want to make a fast hire and will get distracted by long-winded or rambling answers.

By sticking to answers of 60 seconds or less, you can communicate your point quickly and in the right language, easily explaining why you're the right person for the job.

> _"Believing in yourself is the starting point. Effectively communicating your abilities to others is the necessity."_

### 3. Convince the interviewer that you can do the job by repeatedly mentioning your five most marketable skills. 

You just learned that when a recruiter has to interview lots of people for a job, the lines between the candidates often blur together. Luckily, there's a strategy that can help you stick out from the crowd. It's called the _5 Point Agenda_.

To succeed, it should be based on your five most marketable qualities and skills. By paring things down, you can focus your interviewer on your biggest strengths, especially those that match the company's needs.

Once you've outlined these five points, mention them throughout your interview. By stressing the qualities you want the interviewer to focus on, you'll help her remember your skills and make her believe that you're the right fit.

Imagine you're applying for a job as an online graphic designer. Your 5 Point Agenda might be: first, you have ten years' experience with a popular online retailer. Second, you have a massive portfolio of original design work. Third, you're experienced in building websites. Fourth, you've got stellar editing skills. And fifth, you're fluent in three languages.

Don't forget, your 5 Point Agenda also needs to stick to the 60-second rule.

But, the traits you put on your 5 Point Agenda shouldn't be the same every time you prepare for an interview.

Actually, before every interview, you should make a new, customized 5 Point Agenda that corresponds closely to the job for which you're applying. To be successful at this, your primary abilities need to fit the objectives and goals of the company.

Once you've made sure that's true, you can customize your agenda in three simple steps. Begin by writing out your main experiences and the responsibilities you've so far held. Focus in particular on the work strengths and skills at which you excel.

Research the company by looking them up on websites like LinkedIn and Glassdoor or asking friends or colleagues with connections at the firm. From there, you can determine which of your skills are most relevant to the employer and tailor your 5 Point Agenda accordingly.

### 4. By preparing, you can ace even the toughest questions. 

It's no secret that job interview anxiety can lead to nervous rambling. It also shouldn't come as a surprise that long-winded speeches aren't the best way to land a job.

So, it's critical to handle tough interview questions with composure, and there are three factors that are essential for doing so.

First, you should do some solid basic preparation, so you have answers for the most common interview questions. You should also research the products and services in which the company specializes.

Second, be prepared with specific examples of how you've performed in the past, which show you're qualified for the job. Employers want to be certain that you can do the job you're applying for, and specifics will help you convince them that you can.

You might describe a situation in which you solved a challenging problem or helped save money and time. That being said, your examples don't have to be restricted to work. Feel free to throw in positive achievements in volunteer positions, student organizations, and the like. And of course, don't forget to mention that you're excited to take on new tasks and develop into a true asset for the company.

The third and final essential is to be prepared to sell yourself as the _ideal worker persona_. This is important since employers like to categorize people neatly.

Millennials are dismissed as technology-obsessed and entitled, while it's assumed that older employees are less driven or innovative. You need to fight these stereotypes.

To guide the interviewer's assessment of you, make sure your answers set you apart as an ideal worker. That means being a quick and dedicated learner with stellar communication and computer skills. You should also be flexible and have a _success attitude_, which means you're focused on productivity and ways to improve your performance as well as that of the company.

### 5. The questions you ask during an interview should be insightful and strictly job-related. 

As your interview wraps up, the employer will likely ask if you've got any questions. At this moment, you're being handed a golden opportunity to make a good impression, but it's also a chance to figure out if the job is the right one for you.

Interviewers pay a great deal of attention to the questions that job-seekers ask. What you're curious about can say a lot about you, which makes it essential to avoid queries related to salary or payscale.

While some applicants will blurt out whatever comes to mind, this will only make them seem silly and unprepared. Instead, take the time before the interview to write a list on a piece of paper that you can take out when asked for questions.

Doing so will demonstrate to the employer that you really took the time to research the company and consider how you can help it succeed.

For this purpose, it's appropriate to prepare around ten to 15 questions. From there, you can also ask the interviewer about something that he brought up during the interview that made you curious.

Or, if your questions were answered over the course of the interview, be sure to say so explicitly. For example, you might say, "looking over my list, the questions I had regarding your training policy have been clearly answered."

And, finally, although it's been said already, it's important to stress that you should never ask a salary or benefits-related question in an interview. Just take the opinion of a top manager at AT&T who claims to judge prospective hires primarily on the questions they ask, which he says reveal a lot about their motivation.

For him, if a candidate only asks salary-related questions, they come off as solely concerned with money and disinterested in the actual job. That's not to say you should _never_ ask these questions. It's just a better idea to ask them once you've been offered the job.

### 6. Dress the part and practice your nonverbal communication skills. 

The modern office may have a relatively casual dress code, but appearances are still a huge factor during interviews. It's simply not OK to dress casually for a job interview. If you don't present yourself appropriately, you'll have lost the job without even opening your mouth.

This is especially relevant during the initial round of interviews. In the first few moments, employers will analyze your appearance and decide if you look right for the job. They'll focus primarily on your clothes and your personal hygiene.

Dirty, wrinkled clothing or missing buttons are an instant recipe for failure. Beyond that, your clothes should be clean and well fitting, your nails clean and trimmed and, if you're a woman, you should avoid going overboard with makeup. Instead, stick to tasteful applications and simple, appropriate jewelry.

By presenting yourself neatly, you'll show the employer that you're capable of representing the company. Enter with your most polished foot forward!

But looks aren't everything. You must also practice your nonverbal communication skills as well as your eye contact and handshake. Nonverbal communication is essential as it can be greatly revealing. That's why your movements, gestures and facial expressions make a good impression during an interview.

To nail this, start your interview in the best way possible. Smile at the recruiter and offer her a firm handshake. Don't wreck that first impression with a subpar handshake.

From there, focus on eye contact, which is crucial to a successful interview. Being able to maintain eye contact will show the employer that you're confident and trustworthy. And finally, don't be afraid to smile, as long as it's natural. Nothing sends a warmer message than a brimming, sincere smile.

> _"Smile your best smile, wear your best clothes, and you will feel like you can conquer anything."_

### 7. Final summary 

The key message in this book:

**Interviews are an art, and anyone can land the job they want by following some helpful pointers. To impress your interviewer, answer her questions in 60 seconds or less while tying your responses to your main strengths, experience, and the skills relevant to the position for which you're applying.**

Actionable advice:

**Send a handwritten thank-you note to follow up after your interview.**

Sending a handwritten note after an interview will automatically set you apart from the crowd as few people will take the time to make such a gesture. Be sure that you write this thank-you letter on professional stationery, including a short message thanking the employer for their time and reiterating your interest. Just be sure to mail it within 24 hours of your interview

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Make People Like You in 90 Seconds or Less_** **by Nicholas Boothman**

_How to Make People Like You in 90 Seconds or Less_ (2000) is a guide to connecting, communicating and expanding your social world. These blinks will teach you how to strike up a conversation with strangers, make them like you and understand even their most subtle gestures.
---

### Robin Ryan

Robin Ryan is a job search expert, career counselor and the best-selling author of books on job hunting, interviews, résumé writing and salary negotiations. She has been a guest on _Oprah,_ where she discussed her book and gave advice on hiring techniques.

