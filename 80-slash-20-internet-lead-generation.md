---
id: 588d199c1a56850004e76da5
slug: 80-slash-20-internet-lead-generation-en
published_date: 2017-01-31T00:00:00.000+00:00
author: Scott A. Dennison
title: 80/20 Internet Lead Generation
subtitle: How A Few Simple, Profitable Strategies Can Lead to Marketplace Domination
main_color: 2EABE5
text_color: 1A5F80
---

# 80/20 Internet Lead Generation

_How A Few Simple, Profitable Strategies Can Lead to Marketplace Domination_

**Scott A. Dennison**

_80/20 Internet Lead Generation_ (2015) reveals how your business can take advantage of the internet's lead generation potential. From SEO to pay-per-click advertising to content creation, simple and actionable strategies are what you'll need to get your business ahead in the information age.

---
### 1. What’s in it for me? Learn new ways to use the internet to identify new customers. 

Next time you're driving or walking, count all the billboards or other print ads that you see. The number may shock you. These "old school" advertising strategies still have an important role to play in the marketplace. But slowly, they're being overtaken by another, more sophisticated kind of marketing.

Forget about one message for all, and forget too about expensive printing and distribution costs. When you use online marketing tools, you can target exactly the customers you want for a fraction of the cost of traditional marketing.

If you want to stay ahead of the competition, it's time to learn how the internet can help you. Let's get started!

In these blinks, you'll learn

  * how to use the internet to sell toys for pet tarantulas;

  * what samurai have to do with online marketing; and

  * how to lower your bounce rate.

### 2. If you’re still just sticking to newspaper or TV ads, it’s time to update your marketing mentality. 

Every business owner wants company marketing to grab more clients, but few companies get how modern advertising works. This explains why so many brands still try to milk outdated advertising approaches. Bad luck for them — the old strategies just don't work anymore.

At its simplest, marketing is the act of putting your company brand out in the world so customers can find you. Just 100 years ago, the only avenues to do so were radio, print newspapers and billboards.

But what if you wanted to sell tiny leashes for pet tarantulas? Chances are, if you placed an ad in the local newspaper, you wouldn't sell many leashes. But why? Your tarantula leash is such a niche product that only a small percentage of people who see your ad would be interested.

Today, technology has thrown advertisers a whole new set of challenges.

Some 90 percent of potential customers find products and services _online_ while the rest encounter ads while they read a newspaper or watch TV. So as a tarantula leash seller, not only is the percentage of people interested in your product small but the percentage of people who will see your ad is even smaller!

Modern marketing isn't just putting your brand out there so anyone can find you — you need to attract the attention of the _right people_. This can save a lot of time, money and effort!

Unlike a billboard visible to every passerby, _direct response marketing_ targets a specific group of people. It gets this select group engaged by using a _call to action,_ which we'll learn more about in the following blinks.

Direct response marketing is harnessed using a range of online tools, allowing you to reach the right customers at the right time.

Sound exciting? Before you start, you'll need to work out who your ideal customers are!

### 3. Use the 80/20 rule to find exactly the customers your product or service will suit best. 

Not all customers are created equal! Some of them aren't worth your time; others are ideally suited to become huge fans of your product and remain loyal to your brand.

How can you tell the difference? Say hello to the _80/20 rule_.

This rule is simple: 80 percent of your company profit stems from just 20 percent of your customers.

Those 20 percent are your _ideal_ customers. Want more of them? First, you have to know who they are!

Identify your ideal clients by examining sales records over the past two years. Create an overview that shows sales and profit per product as well as results per marketing campaign.

Now, determine the top 20 percent of your products that bring in 80 percent of your profits. You'll find your customers among those who purchased those products.

Next, create a list of your ideal potential customers and start eliminating those who are fussy or hassle you for discounts. Customers who buy your product simply because they couldn't be bothered to take the time to find what they need should also be avoided. They're not worth your time.

Finally, identify a past marketing campaign that was particularly successful with this core group of clients. See what happens when you base future campaigns on these same ideas. Chances are, your products will be a hit with exactly the customers you want.

But what if you want to attract a new kind of customer? Let's learn how in the next blink.

### 4. Define your product’s unique selling point and stay on message to create a loyal customer base. 

Wouldn't it be nice if absolutely everyone knew how your product could change their lives just by looking at it? This, of course, will never be the case.

Spreading the word about your product takes effort. So what's the best strategy? The first step is to develop a selling point that's _unique_. Then stick to that point in all your marketing outreach.

To come up with a unique selling point, write down as many as ten problems that your product solves for the customer. Then compare this list with competing products. Does your product fulfill needs that the competition doesn't?

Pick out the solutions that only your brand offers and build your marketing message around these solutions. And once you've created a message, be consistent! If you keep changing your selling point, your ideal customers will become confused and move on to a company with a clearer message.

Developing a pricing strategy is the next step. With a unique selling point, you're already ahead of the game. Why? Well, price is only relevant if customers can't distinguish your product from competing alternatives. A unique, useful product doesn't need to compete on price.

Give your product an added edge by creating "rattle and hum" around your persona in the media, if you're comfortable doing so. Even if you have no interest in becoming the next Donald Trump, you can learn a lot from the way he fearlessly generates publicity around his name!

Another way to ignore pricing games is to generate a sense of exclusivity around your brand. People tend to pay more for things that are rare. Scarcity can help increase your margins _and_ help you build an elite customer base. Focus on the quality rather than quantity of clients, and you'll secure your brand's future.

So far we've covered the fundamentals of good business. But what about good business in the twenty-first century? The information age has brought new opportunities as well as challenges.

> "_Price is only a factor...when your prospect can't tell you apart from your competition."_

### 5. A sleek and searchable website is crucial for any business; know your SEO, too. 

Some websites are a headache. Careless design and confusing navigation can turn a customer off just like a sweaty, scattered salesperson would. Your business deserves a good online presence.

So how can you create a website that will solidify your brand message? Let's start with the basics, and that's your website's structure.

Say you've got a home renovation business. Your website starts with a _homepage_, which links to other pages on your site. You might think one page with contacts and another page that lists all your services might be the easiest way to organize things. Think again! Your customer will have no idea if your business meets their needs until they've clicked and scrolled for some time.

Show customers that you've got what they want with _content category pages_. For a home renovation business, the homepage might feature illustrated links to pages that address roofing, flooring and kitchen remodeling.

All important pages should be linked directly to your homepage. Search engines will not index any page that is more than two clicks away from your homepage!

This matters. Search engines are often the vehicle your customer will use to stumble upon your site in the first place. You can use this to your advantage by making your website search-engine friendly. Doing so is called _Search Engine Optimization,_ or SEO.

Often people think that SEO is about _finding_ the right keywords to match your content with the words people type into Google. But in fact, the _placement_ of keywords is just as important.

Take the meta description tag, for example. This is the line of text that appears under the heading of your website on a page of search engine results. Including a _primary keyword_, or word that explains your business more than any other, will boost your SEO ranking significantly.

Where else should primary keywords go? Good options include in header tags, image captions, linked text and the main copy on your site. But remember that you can't just shove keywords into copy, as search engines are smarter than this. You might even get penalized by Google!

Additionally, another SEO strategy is to use professional backlinks, allowing you to share content with other websites and increase your search engine ranking.

In addition to good SEO, let's look at another way of getting ahead in the search engine race.

### 6. Pay-per-click advertising will catch you new customers when done correctly. 

SEO isn't the only strategy that'll get your brand ahead online. Pay-per-click advertising is a great tool to have in your arsenal. If you use it correctly, that is!

All it takes is a little common sense. You need to include keywords that people are most likely to use when searching for a product like yours.

Linking the wrong keywords to your Google Adwords account won't bring many visitors to your site. This problem is known as Google's "stupidity tax." If you want to avoid it, get smart and create specific landing pages on your website for each campaign.

Anyone who clicks your ad wants to end up at a predictable location — your website! Each and every AdWords campaign needs a landing page on your website. This landing page should be clear, inviting your guest to get involved in your offer without unnecessary distractions.

With the help of online services such as Wordpress, Medium and Tumblr, you can set up a sleek website in no time at all. These platforms are useful because they're _dynamic_ — you don't have to add pages, pictures and posts to them every time you make a new site. In this way, these tools are a much better option for building sites than a static HTML platform.

Once you've got an AdWords campaign up and running, how do you know if it's working?

By examining traffic and campaign data, you can learn how to improve your online presence and identify what to look for in future campaigns. Google Analytics is a free tool that makes this process easy, showing you how much traffic your page receives and where it comes from.

Again, keep an eye out for which pay-per-click campaigns attract your ideal 20 percent of clients!

So you've figured out how to draw customers to your site, but how can you keep them coming back for more? The solution is great content.

### 7. Great content connects you with great customers. 

So you've got a sleek landing page and a user-friendly website. Now the big question: what sort of content should you place on your website?

You've probably heard how great content is a must for any successful business. But could you explain why this is the case?

It works like this. Google wants to rank the top content for any keyword, which means the search engine has to find where the content is located. Google achieves this by sending out _spiders_, or automated software programs, to crawl the web.

By hosting on your site the content these spiders are looking for to match certain keywords, your site will rank higher in Google searches. With poor-quality content, however, you'll end up at the bottom of search results, where customers will never find you.

To see how great content works, let's imagine you're a lawyer who specializes in rental disputes. A piece of great content — not just thinly veiled advertising, but information genuinely useful to customers — might include a free sample letter for a landlord who hasn't refunded a rental deposit.

Similarly as you should do with pay-per-click advertising, you need to track and evaluate the performance of your online content. A great metric is _bounce rate_, or the percentage of visitors who visit your website without looking at any other pages. In other words, people who look at your homepage and then leave.

Low bounce rates mean that your content keeps people reading, which in turn means a higher search ranking for your site on search engines!

If your content is top-notch, the chances are that readers will want to share it with friends online. This is great news, as sharing content means your brand will pop up in the newsfeeds of their followers, and their followers' followers, and so on.

Be sure to make your content shareable by including sharing buttons and comment sections. You should also make sure to include your social media profile where you share content and curate other interesting pieces of information for potential customers.

Most themes on platforms like Wordpress offer options like these, as well as the capability to make content easier to read and share for visitors who are looking at your site on a smartphone. 

With such tools, you're ready to boost your business with an online presence that makes the most of the internet's lead generation potential!

### 8. Final summary 

The key message in this book:

**Doing great business is a matter of discovering your unique selling point, and finding the 20 percent of customers that give you 80 percent of your profits. New online tools, from SEO to pay-per-click advertising to content curation and creation, make this easier than ever, by connecting your brand with your ideal customers.**

Actionable advice:

**Choose your keywords wisely.**

Keywords can make or break an online campaign. Your goal is to identify five keywords that describe your product best but aren't also being used by competitors. Some tools to help you discover which keywords are golden include Google Keyword Planner and Market Samurai.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The 80/20 Principle_** **by Richard Koch**

_The 80/20 Principle_ (1997) was named one of GQ's Top 25 Business Books of the Twentieth Century. It's about the 80/20 principle, which says that 80 percent of results are generated by just 20 percent of effort. This phenomenon has huge implications for every area of life, as it helps single out the most important factors in any situation.
---

### Scott A. Dennison

Scott A. Dennison is a public speaker, author and marketing expert. As a business advisor, Dennison has worked with businesses across many industries, helping them boost lead generation using online tools.

