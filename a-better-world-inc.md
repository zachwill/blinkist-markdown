---
id: 54e36b5b303764000a420000
slug: a-better-world-inc-en
published_date: 2015-02-18T00:00:00.000+00:00
author: Alice Korngold
title: A Better World, Inc.
subtitle: How Companies Profit by Solving Global Problems...Where Governments Cannot
main_color: BED547
text_color: 5B6622
---

# A Better World, Inc.

_How Companies Profit by Solving Global Problems...Where Governments Cannot_

**Alice Korngold**

Many of us are quick to assume that big corporations are the enemies of the environment. _A Better World, Inc._ explains how the opposite is true: companies are in a better position to solve some of the world's biggest problems than many governments and campaign groups. This book outlines why, and lists the steps companies can take to improve our planet, while raising their profits at the same time.

---
### 1. What’s in it for me? Learn how companies can profit by addressing the world’s problems. 

Population explosion, global warming, immense poverty, failed education systems, labor abuses and more. The world today is facing many crises. Who's supposed to fix them all? 

Although we've long thought of nonprofits, NGOs and governments as the answer, if you take a fresh look at the issues you'll find a surprising savior: multi-national corporations.

Look past their bad reputation and you'll find that they're actually best suited to solving some of the world's most pressing problems. They can reach millions of people, they have the cash to spend, and most importantly, it turns out that helping solve the world's problems can actually improve their bottom line.

That's what this pack is about: How companies' interests often overlap with the world's, and how addressing issues like green energy by promoting cleaner fuels can save huge amounts of money for everyone — corporations included.

In these blinks, you'll learn

  * why governments and NGOs can't solve the world's biggest problems;

  * why engaging with communities will lead companies to bigger profits; and

  * how Nike and Intel benefit from helping the planet.

### 2. Governments and NGOs both fail to solve the world’s most challenging problems. 

When you imagine organizations that may be able to solve the world's thorniest, most complex problems, you probably think of NGOs. But NGOs have serious drawbacks.

They tend to lack sufficient resources, for a start. They're usually short of money, and don't have staff with the necessary skills in finance, social media or fundraising. They also often rely on volunteers who may be motivated, but not necessarily qualified.

The international NGO mothers2mothers, for example, was quite effective at first, but struggled as it continued to grow. They succeeded in the end, but only through the help of the global corporation Pfizer, which provided funds, human resources and technology.

Governments also often fail to solve serious problems because they have conflicts of interest. Just think of all the numerous international conferences that have failed to produce serious, game-changing agreements. The 2012 Rio +20 United Nations Conference on Sustainable Development, for instance, only resulted in a non-binding agreement.

There's often tension when nations come together like this, as governments have to weigh their own country's interests against those of the rest of the group. Why should China agree to enforce higher environmental standards when its economy relies on industry and manufacturing?

Governments also usually don't set long-term goals, because they have short-term mandates. Politicians are mostly just interested in getting re-elected, after all. So they work to satisfy the immediate needs of the voting public instead of concentrating on lasting projects.

When unemployment is high, the public probably won't be happy if the government invests in long-term scientific research or healthcare. They'll want the government to invest in something that will give them a job right now!

### 3. Global companies are perfect for finding solutions for social, environmental and economic problems. 

You might think that international corporations only destroy the environment, and don't care about social problems. This is changing. Now, global companies are actually best positioned to find real solutions to environmental problems.

Corporations have a lot of influence in the world. They affect many people's lives, and they have tremendous financial power that can be reinvested in creating sustainable products and policies.

Think of it this way: if you want to leave your mark on the world, you have to reach a lot of people. Global companies don't just reach people through their products, but also through lobbying that influences political decisions.

The company Ecolab, for example, has 40,000 employees in 171 countries, and their patented dish washing machine uses only half the water of conventional models.

It's also simply in the best interests of big companies to solve economic and environmental problems.

Environmental and business issues now overlap. Customers often prefer products from companies that care, so offering environmentally friendly products can give companies an edge over their competitors. This can increase their profits, and help them save money as well.

Kimberly-Clark, for instance, was dubbed an "evil empire" by Greenpeace because of their use of fiber from the Canadian boreal forest. Greenpeace withdrew this statement in 2009, after Kimberly-Clark changed their mission, and decided to use Forest Stewardship Certified (FSC) fibers instead. This made them much more popular among their customers, and even made their recruitment easier. Tom Falk, the CEO, said the changes saved the company tens of millions of dollars!

### 4. Big corporations want economies and health care systems in developing countries to grow – and they can help. 

Corporations don't just have an incentive to help the environment — they also have an interest in making sure people are healthy. Smart companies know they'll benefit in the long run if local economies around the world are strong.

A stable economy requires a healthy population and an effective health care system. In fact, a study by the World Health Organization found that in low-income countries, only 17 percent of the population lives for more than 70 years, while in high-income countries the value is 71 percent. Countries with a lower life expectancy have lower average incomes and more unstable economies.

This is important for corporations, because people can't work if they're unhealthy. Businesses need healthy employees and consumers, so they have an incentive to help strengthen local economies and health care systems.

Solving the world's problems also creates new market opportunities. As a society becomes healthier and its economy grows, its people will live longer and seek out a higher quality of life. They'll be able to afford more and more goods, which benefits both their economy as well as corporations supplying those goods. This, in turn, will lead to better health care, so the cycle keeps spiraling upward!

All in all, corporations stand to gain a lot if they support the economies of developing countries.

The information and communication technology company Ericsson, for example, is establishing high-performing networks in Myanmar. Less than five percent of people in Myanmar have mobile phones, but Ericsson's construction of mobile communication networks will create about 70,000 jobs, and some studies predict that it could increase Myanmar's GDP by up to 7.4 percent!

### 5. Companies can increase their profits by helping the environment. 

Ultimately, companies are driven by their own interests. This is why they focus on maximizing profits. But what if there's a way to increase profits _and_ protect the environment?

Companies save money when they increase their productivity, which they can do by reducing their consumption of energy and resources. They benefit financially when they become more environmentally friendly.

In fact, the consulting firm McKinsey says that companies can save 20–30 percent of the energy they use on production if they become more green. This way, their goals are aligned with the planet's, because if they reduce their energy consumption, they help the environment _and_ their bottom line.

Investing in renewable energies also saves costs in the long run. When a company transitions into using renewable energy sources, they reduce their dependence on fossil fuels, which are very expensive.

Intel did this in 2013, when they purchased 3.1 billion kWh of green power, which was enough to take care of their energy needs in the United States. This saved the equivalent of 320,000 American homes' CO2 emissions.

Solving global problems can also give companies a competitive advantage. Customers, employees and investors alike prefer innovative new organizations that show goodwill, and make their sustainability efforts transparent.

A survey by BAV consulting shows that brands owned by companies who have a higher record of social responsibility have about 33 percent greater usage, are 39 percent more preferred and have 27 percent greater customer loyalty.

Customers are also demanding more energy efficient products, so they can help the environment as well.

Intel is one of the companies striving to fulfill this need. They aim to lower the energy costs of the products they supply to businesses. When the University of Oklahoma High Performance Computing Center was built, Intel Xeon processors were used — not just because they functioned well, but also because they helped reduce energy costs by around 30 percent.

### 6. Climate change and poverty threaten corporations, international security and the global economy. 

People in wealthy, Western nations often assume they aren't affected by global crises. Obviously, they're very mistaken. Global warming will hurt everyone, including rich people in New York or Beverly Hills.

Climate change also threatens the profits — and even the very existence — of major companies. It damages them and their customers alike.

Natural disasters like Hurricane Sandy are especially dangerous — and not only to people living in the affected areas. Hurricane Sandy resulted in $50 billion in damages in the northeastern United States, and caused 8.5 million people to lose electricity.

Environmental experts predict that big storms like Sandy will only become more frequent as the planet continues to warm up. The global community needs to stop climate change if we want to save lives and be able to spend our money elsewhere.

Climate change and poverty also impact international security and economics. Natural disasters endanger huge numbers of people, by destroying living spaces, infrastructure and food supplies, even in developed countries.

When people's lives are threatened, they need expensive shelter and care. They're also much more likely to resort to violence if their situation gets desperate. This can lead to refugee crises, or terrorist attacks in developed countries.

In fact, some experts believe that the Arab Spring was partially a result of food scarcity and increased food prices, which in turn, were results of climate change.

Of course, most businesses suffer when security is low, regardless of the region. So businesses have extra incentives to contribute to certain causes, like the fight against poverty, natural disasters and global warming.

> _"Climate change threatens national security."_

### 7. A company needs a sustainability committee to implement, monitor and report on its strategy. 

So what are the first steps your company should take toward implementing sustainable practices?

First of all, you need a _sustainability committee._ The sustainability committee should be associated with the company's board of directors, or another governing board within the organization.

Every company has a board that's responsible for its legal and financial well-being. In this day and age, it's important that it also takes on the responsibility of ensuring that the company plays a positive role in the world, too.

This is especially true now, because the stakes are much higher. As we've seen, helping the world isn't just about doing the right thing anymore — it can also lead to higher profits and better brand performance, and help quell natural disasters that might wipe out sections of your market!

The committee needs to help the board develop, carry out and monitor their sustainability strategy. It should regularly report to the board on the strategy's performance, and the board needs to understand the committee's significance.

Some successful companies, like Unilever and Nike, have already established sustainability committees, which meet up to four times a year.

Nike's sustainability committee has considerable responsibility, because it doesn't just deal with the company's energy issues. It's also tasked with reviewing all of Nike's policies and actions, and it offers advice on other matters, like labor, charitable contributions and diversity in the company. A sustainability committee can become a very useful asset, so don't underestimate the value it will bring to your organization.

### 8. Companies should engage with their stakeholders when developing and implementing sustainability practices. 

Another way you can help your company reach its sustainability goals is to engage with your stakeholders. Working with more people will only benefit the company, and increase your chances of achieving what you want.

Companies benefit from incorporating their stakeholders' input — whether they're customers, employees, investors or even the community around the company.

Interested stakeholders will always make their voices heard, whether you ask them or not. So to avoid conflicts and look out for their best interests, ask for their input directly. A good way to do this is to set up a _Stakeholder Advisory Council_ (SAC).

The SAC can ensure that the company maintains its communication channels with various stakeholders, and engages with them when working toward its sustainability goals. The SAC is also important for anticipating any tensions or problems between the groups.

Stakeholder-related risks are the biggest of all _nontechnical risks_, which are risks that don't relate directly to the business. One large oil company, for example, lost an estimated $6.5 billion to nontechnical risks in a two-year period. An SAC can help prevent these kinds of financial disasters.

Companies also increase their sales and enhance productivity when they engage with their stakeholders. Employees are more motivated when they're asked for feedback about social and environmental issues, so they'll perform better.

Remember, you'll improve your company's bottom line when you make your employees and local community happy. Productivity, brand loyalty and sales will all rise.

A survey in _Harvard Business Review_ reported that people have more trust in a company, and are more willing to buy from it, if they trust its leadership and the firm engages in social media.

### 9. Companies should collaborate with NGOs and other businesses when working toward their environmental goals. 

As we've seen, NGOs aren't always the best at improving the world. This doesn't mean they shouldn't be involved in the process at all, however! Companies stand to gain a lot by working _with_ NGOs to reach their common goals. Businesses and NGOs can often accomplish much more together than they could on their own.

NGOs have a lot to offer businesses. They can provide expertise and networking opportunities, and increase your company's credibility in the eyes of your customers, employees or investors. They also have their own proven approaches for solving important social, environmental and economic problems. These skills are vital for any company that's trying to get involved in these issues.

The Dow Chemical Company, for example, collaborates with the Nature Conservancy. Together, they've developed methodologies that dramatically reduce costs and risks for both organizations. In fact, their methodologies have been so successful, they'll soon be adopted by four additional companies.

A business can only go so far on its own — it'll get much further with collaboration. Businesses are also more effective when they work with _other_ businesses, because they can share their expertise, information and experience with each other.

The Clinton Global Initiative _,_ founded by Bill Clinton, is an example of an effective collaboration. It's comprised of both businesses and NGOs, and invites influential people and organizations around the world to come together to discuss and solve common problems.

The Clinton Global Initiative has improved the lives of over 400 million people in more than 180 countries. When businesses and NGOs work together, they can accomplish a great deal of good work.

### 10. Final summary 

The key message in this book:

**Big corporations aren't enemies of the planet — they're actually some of its best potential allies. They're in a much better position to help than governments or NGOs. And companies shouldn't just fight global warming because it's the right thing to do — it can also dramatically increase their profits and lower their costs. When corporations fight climate change, everyone wins.**

Actionable advice:

**Collaborate.**

Work with NGOs and other businesses, and also your stakeholders: your customers, employees, investors and community. The more you engage with people who can provide a wide range of ideas and skills, the easier it'll be to achieve your environmental goals.

**Suggested further reading:** ** _With Charity for All_** **by Ken Stern**

_With Charity for All_ offers an in-depth view of the inner workings of a sector which dominates ten percent of the US economy and employs 13 million people: the nonprofit industry. Subject to few controls, some huge nonprofit organizations are all too often afflicted with incompetence or even fraud.
---

### Alice Korngold

Alice Korngold is the president and CEO of Korngold Consulting, LLC. She advises global corporations on working with nonprofit organizations and raising profits by helping the world. She's written many other successful books, including _Leveraging Good Will: Strengthening Nonprofits by Engaging Businesses_.

