---
id: 5799e5ec355cc30003d1ff01
slug: a-bigger-prize-en
published_date: 2016-08-03T00:00:00.000+00:00
author: Margaret Heffernan
title: A Bigger Prize
subtitle: How We Can Do Better Than the Competition
main_color: C82832
text_color: C82832
---

# A Bigger Prize

_How We Can Do Better Than the Competition_

**Margaret Heffernan**

_A Bigger Prize_ (2014) explains how competition is holding us back. These blinks demonstrate how our competitive schools, economy and society — believed to produce higher grades, lower prices and better results — are actually stifling collaboration and preventing us from realizing our full potential.

---
### 1. What’s in it for me? Discover the perils of competition. 

In 1984, researcher Robert Goldman asked 198 top athletes whether they'd take an undetectable performance-enhancing drug if it would guarantee them a gold medal but kill them within five years. The result? Over half said they would take it.

But it's not just athletes who fetishize competition. Most of us are obsessed with winning. Somehow we've come to think of ourselves in terms of being winners or losers. It matters a lot to us that we have the smartest child, the cutest cat, the best-performing stock portfolio, the most Facebook friends and the most awe-inspiring CV. It's all about being the best rather than just a part of a team.

As you'll learn from these blinks, this cultural obsession with winning harms all of us, our kids and even our economy.

You'll also find out

  * How the Finnish education system avoids competitiveness;

  * why rewards can harm your child's creativity; and

  * why strenuous workouts in a rubber suit are not a good idea.

### 2. If we turn life into a competition, we’re bound to lose. 

What does it take to become a star athlete?

Obviously, a lot of hard work and talent. But in the late nineteenth century, the psychologist Norbert Triplett discovered that competition also helps. For instance, cyclists will ride faster in a competition than in practice.

However, life isn't a bicycle race. If we think about everything in terms of winning or losing, most of us are bound to end up disappointed. After all, whenever people compete for top positions, only a handful can emerge victorious.

At the London Olympic Games, a mere 8.8 percent of all competitors left with a medal. Similarly, only a small fraction of society is a member of the economic elite. In other words, most competitors end up losing, regardless of how hard they try.

So, if we define the purpose of life as competition, most of us will fail and end up miserable.

But that's not the only downside to competition. It's also detrimental to our health and can be a major source of stress. If we always feel the need to be the absolute best and can't rely on others because we see them as competition, our only option is to do everything by ourselves. It's easy to see how this can lead to overwork and losing sleep.

Simply believing that life is a competition and that everyone is against us will cause our stress levels to spike. And all of this sets us up to burn out.

Furthermore, highly competitive people often take unnecessary risks. For example, to make their weight class, some wrestlers do high-intensity workouts while wearing rubber suits. This induces profuse sweating and can lead to death from heat stroke.

And finally, competition can discourage people from doing things that are good for them. If we believe that an activity, like sports, is only about winning and we feel that we can't win, we'll simply avoid it altogether, even though it might have other benefits.

> _"Competition corrupts the natural pleasure and joy of what we are doing. It takes the joy out of work, out of life, if you succumb to it."_

### 3. Excessive competition at school hampers creativity and promotes cheating. 

Did you ever participate in a spelling bee or similar competition when you were young? If you did, you probably enjoyed it. Chances are you were so eager to master all your vocabulary lists that you even studied a bit harder.

But in many schools, competition goes far beyond these harmless games. Students are ranked based on performance with the assumption that the competitive format will make them work harder.

However, when competition is used to constantly compare peers, it ceases to be a game. In cases like this, education becomes a matter of winning or losing. And when schools put so much emphasis on competition, students learn to depend on it as their sole source of motivation.

They study as hard as they can to raise their class rank and perform better than the person sitting next to them. But external motivations like competition or praise weaken a student's _intrinsic motivation_ — the motivation he derives from internal rewards. With intrinsic motivation, a student studies for his science test simply because he's fascinated by bugs, or devotes himself to painting because he loves it.

Intrinsic motivation is the only type of motivation that will keep students continually learning once the competition and praise disappear. It is also a driving force for creativity, as only intrinsically motivated students will be keen to try new and innovative things.

Furthermore, if a student is a low performer anyhow, it's unlikely that competition will affect his drive to study. Competition can't motivate all students, and some will simply resort to cheating.

After all, why would you study your butt off to raise your rank from 28 to 25? On the other hand, such students can excel if they're intrinsically motivated, say by the joy of learning a new skill. This will make them less likely to rely on cheating, which is key because, according to Rushworth Kidder, the founder of the Institute for Global Ethics, 75 percent of students have cheated by the time they reach university.

> _"Education is no longer about a learning experience; it's a game of Survivor where kids are strategizing to work against each other and beat the system."_

### 4. Competition can ruin your relationships. 

Do you have brothers or sisters? If so, you're probably familiar with sibling rivalry. After all, it's normal for siblings to compete. But competition between siblings can be disastrous for family relationships.

This is a major issue since parents tend to play one child against the other. They do this because it can help kids score huge achievements or make them behave better. But when siblings feel they're in competition for their parents' approval and affection, they become less generous and more mistrusting of one another.

For instance, what if you love your cute baby sister but feel like you're in competition with her for your father's attention. Your envy of her will prevent you from being as loving and caring as you would normally be.

And it's not just siblings; the same thing goes for all other relationships. If you act too competitively, you'll begin to see relationships solely in terms of comparison and performance.

Say you just started dating someone. This should be an exciting and romantic experience where you'd enjoy getting to know the other person.

But if your competitive impulse is so deeply ingrained that you think of dating as a competition for the best partners, you'll see your date as a mere trophy rather than as a person. This will turn the time you spend with them into a performance to win their favor, instead of an opportunity to enjoy your intimacy.

Worse, some established relationships develop an unhealthy rivalry between partners. This sets you up for disaster: marriage therapist Emily Brown says that cheating is nearly always caused by a power struggle, with the defeated partner taking revenge through infidelity.

So, competition is deadly for relationships. But as you'll see in the next blink, it gets even worse.

### 5. Competition harms us by hindering sharing and cooperation. 

If you could boil sports down to their essentials, what are they really about? Playing fair? Building community?

The answer today is none of the above. That's because nowadays the main focus of sports is winning and competition. This ethos has spread far and wide.

So, what's so bad about this?

Well, one problem with competition is that it prevents cooperation, sharing and mutual support. After all, if you think of the people around you as your competition, you'll always feel that in order for you to win, everyone else needs to lose. As a result, you'll hesitate to help others or share ideas.

For example, since scientists have to compete with one another for grants, jobs and priority, they see science as a competition and not a collaborative effort. That means they keep their discoveries and ideas to themselves for fear that a peer will take credit for their work.

In the same way, if you feel your blog is in a popularity contest, you might hesitate to link to another blogger's superb article because it could help them attract more followers than you.

But the truth is, most situations aren't simple competitions, and often cooperation and mutual support are far better strategies. If you do link your blog to the other one, everyone will benefit: your readers will thank you for the interesting article, the other blog will get more traffic and the blog's author will discover your blog, possibly recommending it to his readers and expanding your audience.

Nowhere is collaboration more important than in the field of science, where progress depends on the sharing and discussion of ideas. Even Albert Einstein relied on the help of a mathematician friend named Marcel Grossmann to choose the correct mathematical models when developing his general theory of relativity.

Next, we'll explore how competition is also detrimental to the economy.

> _"If I have seen further, it is because I have stood on the shoulders of giants." — Isaac Newton_

### 6. Even in business, competition can produce devastating results. 

We all know that free market economics require companies to compete for customers. But it turns out that this may not be a good thing.

To cope with a competitive environment, companies work to become as big as possible by expanding through mergers and acquisitions. After all, once they've swallowed up some of the competition, they're in a better place to influence prices. Not just that, but a bigger company is more respected and can exercise greater political clout.

However, this often results in serious issues. For instance, when BP expanded by buying the oil companies Amoco, Richfield and Castrol, it had to go into massive debt.

Then, to recoup this money, the management slashed costs by investing less in the maintenance of certain old refineries. In 2005, these cuts caused an explosion at their Texas City refinery, which killed 15 people and injured another 180.

And this intense corporate growth causes other issues for the public too. The massive corporations and banks become _too big to fail_, meaning that so much of a nation's economy depends on them that the government is willing to pay huge sums of money to prevent their collapse.

From a banker's perspective, this is wildly advantageous. After all, if they know the government will save them, they can make all the risky investments they want. But for the public, it means billions of tax dollars are spent to save private corporations.

But surely one benefit of competition is that it lowers prices as companies engage in price wars?

Not necessarily. In fact, there too the outcome tends to be negative, because companies pass many costs onto society.

For instance, some companies pay their employees such low wages that they need to rely on governmental support for survival.

And when meat producers compete to slash prices, the living conditions of their livestock deteriorate. This leads to pigs and cows living in narrow sheds or overcrowded feedlots, causing great suffering.

In short, outlandish growth is dangerous; competition drives this need for perpetual growth, causing market disruptions, corruption and a worse economy in general. But don't worry, there's a solution.

### 7. We can overcome competition. 

We've seen that competition can cause serious damage, but obviously, it's not all bad. When used appropriately, it can even be good source of added motivation. Unfortunately, society favors aggressive competition over productive collaboration, whether in the economy, our schools or our families.

Luckily, we have the power to change it.

First, employee ownership and horizontal hierarchies can boost cooperation and deter competition among companies. If all your employees share the same goals, this will promote cooperation. So, give every employee stock in the company, and they'll all share the goal of generating profit.

Flat hierarchies can also help. Without the distinction between superior and inferior positions, it's no longer possible to compete to reach the top. But that's not all that horizontal hierarchies can change.

The company W. L. Gore & Associates, famous for making Gore-Tex material, uses a horizontal hierarchy model. Work isn't controlled by managers and people don't have to follow strict orders or work on a specific team. If they want someone to work with them on a project, they only need to convince them. In other words, they need to make their colleagues want to work with them, which fosters collaboration.

In the field of education, Finland is a great model for lessening competition. Finnish schools don't use grades or standardized tests. Instead, students get assessed based on their individual progress and are never compared to their classmates. As a result, they compete much less. Nonetheless, Finland is always in the top scoring group for the international scholastic achievement survey, PISA.

It seems that students can be powerfully motivated without resorting to competition.

So, a bit of competition can be fun, but in general, we'll all do better if we shift our cultural focus away from competition and onto collaboration.

> _"We're just waiting for permission to live and work in ways that feel so much better than winning. That would be the biggest prize of all."_

### 8. Final summary 

The key message in this book:

**From the living room to the board room, competition drives our culture. But instead of producing more wealth, creativity and progress, competition has meant exactly the opposite. It's holding us back as a society and it's time we adopted a more collaborative system — for all our sakes.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Co-opetition_** **by Barry J. Nalebuff and Adam M. Brandenburger**

_Co-opetition_ (1996) combines game theory with business strategy, presenting a roadmap for how to create a successful venture. The authors explain how running a business is just like playing a game: it involves mastering the rules, knowing the players and the value they bring, understanding tactical approaches and being able to see the big picture. With these elements in place, you can utilize them to improve your own position in the game of business.
---

### Margaret Heffernan

Margaret Heffernan is a multimedia entrepreneur. She began blogging at an early age before going on to teach and write books. _A Bigger Prize_ is her fourth bestseller.

