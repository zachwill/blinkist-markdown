---
id: 56143a0836323300072b0000
slug: a-bone-to-pick-en
published_date: 2015-10-08T00:00:00.000+00:00
author: Mark Bittman
title: A Bone to Pick
subtitle: The Good and Bad News About Food
main_color: FC5D32
text_color: B24224
---

# A Bone to Pick

_The Good and Bad News About Food_

**Mark Bittman**

_A Bone to Pick_ (2015), a compilation of articles originally published in the _New York Times_, outlines the systemic problems in the American food industry. It lays out the governmental and agricultural problems that are holding the industry back — and harming us and our planet in the process.

---
### 1. What’s in it for me? Take the few steps to better understand food and improve your diet. 

Eating is one of those things we all have to do everyday, but that we rarely think about in depth. We may think about _what_ we eat; rarely do we consider the myriad processes that produced our meal. So why do we eat what we eat? Where do we eat it? How much do we spend for it? Is it making us better? Maybe most importantly, where does this food come from?

We tend to ignore these questions, and that's where _New York Times_ columnist and foodie legend Mark Bittman comes in. Bittman's been writing about food and the American food system for years. Besides explaining why we ought to know more about our food system, Bittman also provides concrete and actionable ways we can change it for the better.

These blinks represent the best of Bittman's column, covering topics ranging from agriculture to cooking at home, from dieting to governments.

In these blinks, you'll discover

  * how to become a flexitarian;

  * why you should avoid asparagus from Peru; and

  * what the government could do to help us be healthier.

### 2. Changes in the food industry could have a major impact on many pressing global issues. 

Did you know there's enough food in the world to feed everyone? And yet, roughly one billion people don't have enough to eat. This problem isn't caused by a _lack_ of food — it's caused by our flawed agricultural system. In fact, a third of all the calories we produce go into feeding animals; another third is wasted in the production process; and five percent goes into fuel processing.

There might be a solution, however. It's called _agroecology_, and it's a combination of ecology and agriculture.

_Crop rotation_, the planting schedule for a set of crops, is a prime example of a process agroecology could improve. Imagine you have two plots of land. In Plot One, during one year, you plant soybeans; the following year, you plant corn. In Plot Two, you also plant soybeans in the first and corn in the second year, but then you switch to oats in the third year, before going back to soybeans in the fourth.

Plot Two would have a higher yield, because biodiversity enriches the soil. If you added alfalfa into the rotation, the yield would become even greater. 

So we need to make it standard practice for farms to include more crops in their crop rotations, so that we can produce more efficiently.

But what if you don't own a farm? You can still make a difference in the global food industry. How? Stop buying imported fruits and vegetables.

There's no reason to buy asparagus from Peru in December. Because of it must be transported a great distance, that asparagus leaves a much greater carbon footprint than local vegetables.

Buy your products from local farmers' markets instead. Eat foods that are in season. If you can, change your lawn into a garden and start growing your own vegetables and kitchen herbs. By decreasing its dependence on imported fruits and vegetables, the US would greatly further the fight against global warming.

### 3. The overconsumption of meat is harmful to animals and humans. 

Did you know that the average amount of meat consumed in America is almost _twice_ as high as the average amount consumed globally? In the American meat market alone, upwards of ten billion animals are killed yearly.

There are four things we can do to ease the suffering of our livestock.

First, we should provide them with better living conditions. Chickens need to live in open spaces where they can move around, not in tiny cages.

Second, we might minimize or even eliminate the ability of livestock to feel pain. One way to do this would be to remove their cerebral cortexes.

Third, we can eat organic meat instead of meat produced industrially. Animals on organic farms are treated much better than those kept in industrial meat production centers, and organic meat also tastes better!

Finally, we can consume less meat in general. We'd reduce the demand in the industrial meat sector if we stopped eating animals (especially those that are treated poorly). You don't have to go full vegetarian or vegan; if you still want a steak now and then, become a _flexitarian –_ a person who eats meat occasionally.

Overconsumption of meat isn't just bad for animals; it's harmful to us, too.

That's why the Food and Drug Administration should start limiting the use of antibiotics in animals. Most animals, such as pigs, are given _prophylactic antibiotics_ that prevent them from getting infections.

But prophylactic antibiotics aren't good for us. When we eat the meat from pigs that have consumed them, we become more resistant to antibiotics in general.

The US Department of Agriculture also needs to declare _salmonella_ and other deadly shiga toxin-producing E. coli (or STECs) that derive from the E. coli bacterium as _adulterants_. It's illegal to add adulterants to food products, so foods containing salmonella would have to be taken off the market.

### 4. Take more control of your diet by cooking for yourself. 

Do you ever have a hard time deciding where to eat dinner? What's better — that new burger joint or the drive through? The answer is neither: the best option is cooking for yourself!

There are many advantages to cooking. The biggest one? Well, cooking is all about being in control.

You know where your food comes from when you obtain it yourself. You know where you bought it and what you've added to your raw ingredients.

Cooking is also cheaper. A family of four might spend $28 for a meal at McDonald's — that's about the price of two Big Macs, a cheeseburger, some chicken nuggets, fries and a few sodas. If you cook, however, you can feast on a roasted chicken, vegetables, salad and milk, all for $14.

Now compare the ingredients in the McDonald's dinner to those in your scrumptious home-cooked meal. You have no idea what mysterious, fatty and chemical materials go into the making of a Big Mac. You know exactly what went into that roast chicken, however.

Of course, no one has time to cook _every_ meal, but the more you cook, the healthier you'll be.

And the first part of cooking is knowing what _food_ really is. Food is anything that has nutritional value and that you can eat or drink. That means that a lot of sugary beverages — like Coca Cola, Fanta or Dr. Pepper — don't really count as food.

We have a lot of access to soda. Any five-year-old with a dollar can get some from a vending machine. And keep in mind: a can of soda is as sugary as a cup of coffee with _nine_ teaspoons of sugar in it.

It's also a good idea to avoid _solid fats and added sugars_, or _SOFAs_. That means avoiding highly processed foods like potato chips and other foods that contain added sugar, such as cereal bars.

### 5. Small changes to your diet can have a major impact on your health. 

What should you eat to have the perfect diet? The answer is simple: _real food_.

Real food is any food that contains up to six ingredients, such as bread, which has four: flour, water, salt and possibly olive oil. A vegetable is the "realest" food because it's made up of only one ingredient.

Maintaining a diet of real food means avoiding hyper-processed, industrially produced foods. Such foods score high on the _glycemic index_ and are full of solid fats and added sugars.

The glycemic index is a tool for describing the effects of carbs on your blood-sugar level. Healthier carbs, like whole-grain bread, have a low glycemic index, meaning they don't have a big impact on your blood sugar. Unhealthier carbs, like highly processed granola bars, have a high glycemic index, meaning they cause your blood sugar and insulin levels to rise.

There are also a few rules to follow if you don't want to exclusively consume plants. You need a diet that has _variety_, _balance_ and _moderation_. Eat a wide range of foods; don't eat too much of any one thing; and don't take in too many calories.

Eat more fruits and vegetables, too. And eat like a Greek! The Mediterranean diet is a good guide — it prescribes only a small amount of red meat, sugar and hyper-processed carbs. It's also rich in healthy fat (like olive oil), vegetables, fruits, legumes and some animal products, such as fish, eggs and low-fat dairy.

Imagine a grilled fish with a Greek salad and a glass of cold white wine. Delicious and healthy! A plate with eggplants, hummus and marinated cherry tomatoes would also fit in with the Mediterranean diet — and that's very good for you, too.

### 6. Humanity stands to gain a lot from reforming the food industry. 

When people list the reasons we need to improve the food industry, they often talk about animals, the environment or the dangers of certain chemicals. They sometimes forget the most important reason: us!

Nearly 70,000 people die every year from diabetes II, a preventable disorder that's usually caused by a poor diet. And in the US, 17 percent of children are obese. Obesity leads to a number of other health complications, which can ruin lives and even take them.

What's causing our obesity problem? The answer is complicated, but a part of it is that American children watch 5,500 food ads on TV every year! We need to eliminate ads for junk food, or at least find a better way to protect children from them.

Obesity is far from inevitable. In fact, if Americans just ate one extra serving of fruit per day, roughly 30,000 _less_ people would die each year from cardiovascular disease. If everyone ate the recommended amount of fruits and vegetables, another 100,000 would be saved.

The food industry also needs to be reformed for economic reasons. People who work in the food industry deserve fair pay and working conditions. After all, they're providing us with something we can't live without!

The Food Chain Workers Alliance recently released a report titled "The Hands that Feed Us," which contains over 700 stories collected from food industry workers with jobs that paid less than $19,000 per year; these jobs also didn't provide health insurance, paid sick days or paid vacation time.

The success story of the Coalition of Immokalee Workers (or CIW) also gives us some hope. Immokalee is a center for tomato production in Florida. In 2011, the CIW fought for and won a number of improvements in their working conditions, like more tents for providing shade and reducing exposure to harmful pesticides.

### 7. The food industry should be regulated more strictly. 

Is food-industry reform a matter of business or of health? If it's about business, economists should take care of it and there should be fewer market regulations. If it's about health, it should be left to the government.

Governments can influence any economic market by implementing tax policies, which affect consumer demand. Higher taxes mean higher prices and higher prices mean less demand. And if there's less demand, the industry will be forced to reduce production.

That's why the Mexican government introduced a 10 percent tax on sugar-sweetened beverages and an 8 percent tax on junk food in 2014, after officially becoming the world's most obese country.

Governments could also subsidize weaker economic sectors in agriculture to help them compete in the market. They might reward farms that treat animals better by lowering their taxes or granting them cash awards, for instance.

It would be particularly effective if the government subsidized organic farming or fruit and vegetable farming, for example. Currently, too much land is used to farm corn and soybeans, which are predominantly fed to animals.

Governments also need to be stricter on food labels. There has been _some_ progress here. In 2014, for instance, the American government began requiring brands to label foods with added sugars more clearly.

The ideal food label should have three different values (measured in a number between one and five), for _nutrition, foodness_ and _welfare_.

So a can of Coke would have a zero in _nutrition_, because it doesn't have any real nutritional value. The _foodness_ score would indicate how close the product is to being real food. So frozen broccoli would be a four. (That Coke gets a zero for foodness, too.)

The _welfare_ value would refer to how harmful the product is for the earth, livestock and the people working to produce it. Industrial chicken, for instance, would score very low on welfare.

### 8. Final summary 

They key message in this book:

**The American food industry is in dire need of change. It's harmful to the environment and our livestock animals and it's a serious danger to our health. The government needs to start regulating the food industry more strictly, and we must start monitoring our diets more consciously. If we reform the way we eat, we'll be healthier, our planet will thrive and our agricultural sector will be more efficient. We stand to gain a lot.**

Actionable advice:

**Do what you can — even if it's small.**

Small changes can make a big difference. Figure out where your food comes from; support producers that both pay their workers fairly and strive to be green. Cook for yourself whenever possible. Go for veggie pasta instead of carbonara. Those little things do add up. 

**Suggested further reading:** ** _Eating Animals_** **by Jonathan Safran Foer**

_Eating Animals_ offers a comprehensive view of the modern meat industry and demonstrates how the entire production process has been so completely perverted that it is unrecognizable as farming anymore.

The book explains the moral and environmental costs incurred to achieve today's incredibly low meat prices.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an e-mail to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Mark Bittman

Mark Bittman is a writer for the _New York Times_ and _New York Times Magazine_. He's written a number of bestselling books, including _How to Cook Everything_ and _VB6_.

