---
id: 5a782800b238e100065411ef
slug: a-brief-history-of-everyone-who-ever-lived-en
published_date: 2018-02-08T00:00:00.000+00:00
author: Adam Rutherford
title: A Brief History of Everyone Who Ever Lived
subtitle: The Human Story Retold Through Our Genes
main_color: 7AE5E5
text_color: 366666
---

# A Brief History of Everyone Who Ever Lived

_The Human Story Retold Through Our Genes_

**Adam Rutherford**

_A Brief History of Everyone Who Ever Lived_ (2016) tells the story of humanity through genetics. These blinks explain how humans evolved, the role that genes played — and continue to play — in our development, and the ways in which our genetic past can shine a light on the present.

---
### 1. What’s in it for me? Take a trip through time on the genetic railroad. 

Just about everyone has fantasized about traveling back through time and seeing what the world was like during a different era. Well, modern science may have just made this voyage possible. No, time travel hasn't been invented, but genetics can take you on a tour of history that's just as riveting.

New scientific approaches can explain how culture developed and was transmitted, the patterns of human migration, and even such socially charged concepts as race and ancestry. Interestingly enough, as we peer deeper into this world, many of the preconceptions we hold about our species and society begin to disintegrate. The truth is, far from setting us apart from one another, genetics prove just how similar we all really are.

In these blinks you'll learn:

  * how every person is descended from royalty;

  * why genetic tests to determine Native American ancestry are unscientific hogwash; and

  * why race has no scientific basis.

### 2. Genetic analysis opens a window into the past. 

When you open a history book, everything seems to be set in stone. But in reality, much of human history is foggy. And the longer you travel back in time, the hazier our knowledge becomes. While historians are pretty confident about what happened in ancient Greece, as you delve back further, it becomes difficult to put your finger on the truth.

The good news is that genetic analysis has enabled science to look deeper into the past, truly uncovering the ancient history of humanity. This technology rests on the discoveries of nineteenth- and twentieth-century scientists like Gregor Mendel, Francis Crick and James Watson, who slowly unraveled the mysteries of DNA and the human genome.

Thanks to their work, scientists at the Human Genome Project finally deciphered a full set of human DNA in 2000, following a Herculean effort. Now medical science can analyze the genes of living humans. But it can also extract DNA from archeological samples to study the genes of our ancient ancestors. This new field is called _paleogenetics_ and it uses DNA to tell an exciting story.

Modern humans are known as _Homo sapiens_ or "wise man." But before we evolved, other _Homo_ species existed, like _Homo neanderthalensis, Homo habilis, Homo ergaster, Homo heidelbergensis_ and _Homo erectus_.

_Homo erectus_ was one of the early upright apes. The species evolved into existence some 1.9 million years ago on the African continent before spreading across the globe. _Homo sapiens_, our own species, likely evolved in Africa as well, specifically in the eastern part of the continent, around 200,000 years ago. As the first _Homo sapiens_ left Africa and entered Eurasia, they encountered another species of human, _Homo neanderthalensis_, more commonly known as Neanderthals.

As it turned out, the encounter was a happy one. Our two species had sex — actually, a lot of sex.

In fact, through genetic analysis, we now know that the average European shares around 2.7 percent of her genetics with Neanderthals. So, while they may be a separate species that eventually died out, they never truly went extinct. A more accurate description is that they merged with our species.

### 3. Changes in cultural practices and the environment leave genetic marks. 

You now know that genetic analysis can help us get a better read on human evolution. But it can also shine a light on the development of culture. That's because changes in cultural practices impact our genes, leaving clues to their existence.

Oddly enough, the best example here is milk. For instance, while some people drink it every day and with pleasure, most adults in the world are lactose intolerant and can't digest the stuff. So, why is a small group capable of metabolizing dairy?

Well, to digest milk humans need an enzyme called _lactase_, which is coded into the LCT gene common to all humans. However, for most people, this gene goes inactive after infancy — that is, except for those of European descent.

The ability to digest milk emerged in Europe between the years 5,000 and 10,000 BCE as a result of a single letter change in the LCT gene. This change was brought about by the emergence of dairy farming among Europeans.

More specifically, genetic analysis can pinpoint its emergence somewhere around what's now Slovakia, Poland or Hungary. Meanwhile, groups in Africa and Asia developed the ability to digest lactose thanks to different mutations. Simply put, lactase persistence likely proved advantageous and was therefore selected for during human evolution.

And it's not just culture. Since genes change due to environmental factors, genetic analysis also allows us to study human migration.

People of European descent offer another good example because of one more stand-out characteristic: their skin color. After all, the humans who migrated from Africa to Europe 50,000 years ago were dark-skinned, which makes sense since dark skin is an adaptation to sunny weather. Yet the people who live there now are not.

How come?

Well, bone analysis tells us that 7,700 years ago, the humans living in what's now Sweden had genes that acted together to produce light skin, as well as blonde hair and blue eyes.

### 4. Native American people share similar genes, but DNA analysis can’t confirm tribal heritage. 

When Columbus arrived in the Americas, he was certainly not the first outsider to make the trip. In fact, five hundred years prior, Viking warriors had landed on American shores and decided not to bother the people living there.

Native Americans had been calling the continent home for at least 20,000 years when Columbus landed. Interestingly, genetic analysis can also help us understand why they moved to America and how their different nations are related to one another.

Here's the story:

Between roughly 29,000 BCE and 14,000 BCE, the earth was so cold that the entire northern hemisphere was covered with huge glaciers. During this period, people from Siberian Asia used a frozen bridge to cross the Bering Strait, the space between modern-day Russia and Alaska.

From there, they headed south, spreading out across the continent. All of this is verifiable through genetic analysis. That's because all Native Americans, both in the north and south of the continent, share versions of genes that trace back to the Inuit people of Greenland, implying that they all share a common ancestry.

The tell-tale sign here is the Inuit diet. Since it was dominated by seafood, the Inuit adapted versions of _fatty-acid-desaturases_ or _FADS_ genes, which transform the fatty acids in fish into unsaturated fats. These Inuit FADS genes are found in all the indigenous people of the Americas.

Despite this, DNA analysis can't tell us to which specific tribe a person belongs. Nonetheless, companies claim to do just that. For $99, DNA Consultants will sell you a Cherokee test. If your DNA is a confirmed "match," you can even buy a verification certificate for an extra $25.

These tests have no scientific basis, and they couldn't have one if they tried. That's because no tribe is marked by specific genetic characteristics. The American tribes all mixed together, both before and after colonization, and later even genetically combined with European settler populations. As a result, there's no such thing as tribal purity.

### 5. All people descend from royalty. 

Have you heard of Charlemagne? Well, this ninth-century king and Holy Roman Emperor may just be your ancestor.

In fact, everyone comes from noble ancestry; it's just a matter of mathematics.

Joseph Chang, a Yale University statistician, discovered this by analyzing ancestry through numbers, rather than genes. To do so, he built a mathematical model to uncover how far back you had to go to find a common ancestor for all Europeans. The answer is just 600 years.

So if you documented the full family tree of every European alive today, dating back 600 years, all the lines would cross around the time of Richard II of England.

But how can that be? Each person has two parents, four grandparents, eight great-grandparents, and so on. So every person of European descent today should have had billions of different ancestors living a thousand years ago. How's that possible? A thousand years ago there _weren't_ billions of Europeans.

The answer is that every European alive today is descended from literally every single person alive in the ninth century, many of whom fill multiple positions in any given family tree. Charlemagne fathered 18 children, making him the ancestor of every person alive today with European ancestry.

If you're Asian, the same is true of Genghis Khan and, if you're from Africa, Nefertiti fills the same role. Across the globe, there's an ancient ruler in every person's family tree.

But, it's a good job that not all our ancestors were royal. A great deal of inbreeding occurred in royal families, which posed a serious health risk to those involved. To preserve their bloodline, royalty tends to marry royalty — often cousins. That means most royal people have fewer ancestors than average and are therefore at increased risk of genetic disease.

Take Charles II of Spain, born in 1661. He suffered from a serious genetic disorder that meant he was born with shriveled genitals and couldn't have children. As you might have expected, he had far fewer ancestors than is common. A person whose family avoided incest altogether should have 254 different ancestors over eight generations. Charles II had just 82.

### 6. Racism may be real, but race is not a scientific category. 

It's hard to talk about genetics without getting into the thorny subject of race. What does science really have to say on the topic?

Well, in the past there have been attempts to justify racism through genetics. In 2013, Nicholas Wende, a former science editor at the _New York Times_, published a book called _A Troublesome Inheritance_. The book not only claimed that race could be defined genetically, but it also said that the DNA of a population accounted for its culture. He went so far as to claim that Jews have genes that are "adapted for success in capitalism."

Meanwhile, true genetic analysis has proved that race cannot be described as a scientific category. That said, it took a while to clarify.

Scientists of the eighteenth and nineteenth centuries like the German anthropologist Johann Blumenbach tried to define all humans by five racial categories: Caucasian, Mongolian, Ethiopian, Malayan and Native American.

Such reductive theories were upended in 2002 when Stanford scientist Noah Rosenberg published a major study using genetic samples from 1,056 people from 52 regions. He used a computer to cluster these samples into categories based on their degree of similarity. When he asked the computer to divide the samples into five categories, they corresponded roughly to earlier racial groupings. However, if he asked the computer to divide the samples into two, three or four categories, completely different "racial" groupings appeared.

When Rosenberg asked the computer for six categories, the next "race" to appear was the northern Pakistani tribe, the Kalasha, which only has some 4,000 members.

The inclusion of such a small, indistinct group as a "race" shows just how arbitrary the notion of race is. There's so much overlap in the human genome that dividing people up into groups by using such tiny differences is akin to splitting hairs.

For instance, in 1975 geneticist Richard Lewontin researched the differences between blood types. He found that the greatest genetic differences occurred not between but _within_ racial groups.

So, while there are clearly physical differences between races, our eyes are deceiving us. You're actually more likely to find genetic differences between two black people than between a black person and a white person.

> _"As far as genetics is concerned, race does not exist."_

### 7. Deciphering the human genetic code has unraveled several myths. 

Do you remember June 26, 2000? Well, it was a historic day, but probably not one to which many people paid attention. US President Bill Clinton invited the head scientists at the Human Genome Project to the White House to publicly announce that the entire human genome had been deciphered for the first time.

Naturally, this feat was groundbreaking. The Human Genome Project was one of the largest scientific enterprises ever attempted; in just eight years it had successfully read the three billion letters of DNA that compose the human genetic code.

What did all this work reveal?

Well, three discoveries stood out. First, humans have far fewer genes than was previously hypothesized. Prior to this decoding, most scientists had assumed that the human genome would contain something like 100,000 genes or even more. The real number is closer to 20,000. Shockingly, roundworms and bananas have more individual genes than people do.

The second surprise was that most of this DNA has little or no function. In other words, readable genes only compose about two percent of the human genome. The rest is entirely incomprehensible. This letter salad is known as "junk DNA," but it's not yet clear whether it's entirely useless.

And finally, the project uncovered one more major insight: the complex nature by which genes interact. For instance, humans have long believed that specific genes code for specific outcomes, say certain types of cancer. But that's simply not the case.

To prove it, _Genome-Wide Association Studies_, or _GWAS_, analyzed the genomes of thousands of individuals with identical medical conditions. Rather than identifying single genes as the cause of their ailments, these studies found tens to hundreds of genes that play a role in any given medical condition. This intricate interplay produces cumulative effects that can eventually result in illness.

### 8. Genes perform many interactions, and traits acquired during life can be passed onto your offspring. 

If you search Google for the phrase, "scientists discover the gene for…" you'll get an endless list of shocking results. For instance, you might stumble upon a 2008 article from the _Guardian_, entitled, "Scientists discover the gene for cocaine addiction."

Despite the reputability of the source, such claims are unscientific nonsense. So, why the confusion?

Well, single genes are often misunderstood as determining specific behavior. Just take the case of Davis Bradley Waldroup. In 2006, in Polk County, Tennessee, he murdered his wife's friend and attempted to take his wife's life as well.

He faced execution for homicide and had shown murderous intent. Nonetheless, he escaped the death sentence.

The core of his defense was the MAO-A gene, which is essential to normal life. A dysfunction or variation in this gene has been linked to increased aggression, impulsiveness and criminal behavior, and Waldroup's DNA showed that variation.

But that doesn't relieve Waldroup of guilt. After all, genetics is a vast field, and no single gene could possibly account for such a specific behavior. So, while his legal team may have persuaded the jury, they can't trick the world of science.

That being said, genetics _are_ quite malleable in other ways. Traits acquired during life can, in rare cases, be passed on to the next generation. In the winter of 1944, the Nazis cut off the food supply to the western Netherlands, starving the population. Countless lives were lost during this _Hongerwinter_ and those who made it through faced serious medical issues later in life. Oddly enough, so did their children, even when they were born years later. The offspring of the survivors went on to develop medical problems like obesity and diabetes.

The study of the inheritance of acquired traits is known as _epigenetics_, and it's a scientific curiosity. Since Darwin, science has rejected the idea that such a thing is possible. However, this theory really isn't in conflict with evolution by natural selection. Rather, epigenetic outcomes are rare, and while acquired traits can be passed on for one or two generations, eventually they fade away.

### 9. Although natural selection has slowed down, humans are still evolving. 

In science-fiction movies like _X-Men_, mutants utilize superhuman powers to accomplish all kinds of extraordinary feats. So when are humans going to evolve to the point where we _actually_ develop such powers?

Well, the shocking answer is, we already have. We have invented airplanes, night-vision goggles and all manner of other things that increase our abilities. The best part is, there's no need to grow huge wings or odd-looking feline eyes.

But — jokes aside — humans _are_ still evolving and will continue to do so as long as we have sex and reproduce. In fact, with every new baby, the human genome changes and the species as a whole evolves. Because of this, from a genetic perspective, every one of us is transitional insofar as our DNA is a transition between that of our parents and that of our children.

However, not all genetic changes are positive. In 2013, Josh Akey of the University of Washington in Seattle found that many of the recent changes to human DNA had made the production of proteins less efficient or entirely dysfunctional. In other words, it's clear that humans are still evolving, but not necessarily through _natural selection_, which should pick advantageous traits that increase our species' chances of survival.

We've altered our living conditions so greatly that it's hard to say what's "natural" anyway. Humans have devised ways to overcome the harsh challenges we face from nature, which means we're combating disease and aging with ever-increasing sophistication. The advent of modern medicine, for example, has dramatically slowed the pace of natural selection simply because we no longer lose individuals to now curable conditions.

However, natural selection hasn't stopped altogether. Humans still die, reproduce and therefore continue to evolve. Until something else changes, Darwin's theory of evolution is still the best we've got.

### 10. Final summary 

The key message in this book:

**Modern genetic analysis has opened new windows into the past; science can now paint sophisticated narratives of human history, based solely on genes and their development. Such approaches offer new insights into the complex evolution of humanity and major social issues like race and discrimination.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Gene_** **by Siddhartha Mukherjee**

_The Gene_ (2016) offers an in-depth look at the history of genetics. These blinks take you on a journey from the field's humble beginnings to its modern day applications in diagnosing illnesses, debunking racist claims and creating genetically modified life.
---

### Adam Rutherford

Adam Rutherford is a science writer and broadcaster who earned his doctorate in genetics at University College London. He's the creator of such award-winning BBC programs as _Inside Science_, _The Cell_ and _Playing God_.

