---
id: 5a2dd1afb238e1000a9f2cd1
slug: a-crack-in-creation-en
published_date: 2017-12-12T00:00:00.000+00:00
author: Jennifer A. Doudna, Samuel H. Sternberg
title: A Crack in Creation
subtitle: Gene Editing and the Unthinkable Power to Control Evolution
main_color: 7288A0
text_color: 495766
---

# A Crack in Creation

_Gene Editing and the Unthinkable Power to Control Evolution_

**Jennifer A. Doudna, Samuel H. Sternberg**

_A Crack in Creation_ (2017) describes everything you need to know about CRISPR, a new technique to alter the genes of living organisms. These blinks explain the scientific details of gene editing, while also discussing its medical and ethical implications.

---
### 1. What’s in it for me? Dive into the wonderful – and terrifying – world of gene editing. 

Imagine a world where genetic diseases, or even HIV or cancer, could be cured by simply editing them out of your DNA. This seemingly fanciful idea is an example of what the wonder of gene editing could one day provide. And this isn't a far-off idea confined to sci-fi movies alone — we may actually be lucky enough to be alive when it becomes a standard practice in medicine.

However, just as with the nuclear bomb, such an incredible leap forward in technology also has its dark sides. Why stop at curing diseases? With gene editing, we could create "designer babies" from a catalog of different improved genes, such as ones offering better muscle development or increased intelligence.

Of course, this poses a myriad of ethical issues that we, as a species, will have to work out before proceeding with caution.

In these blinks, you'll learn

  * what CRISPR is, and why it's poised to change the future of medicine;

  * how gene editing could save our food supply; and

  * why one of the authors declined a position at a "designer baby" start-up.

### 2. Genetic modifications can occur naturally. 

For billions of years, life on Earth has evolved through random genetic variations, blossoming into an ornate display of biological diversity. This process has long been understood through the Darwinian principles of evolution, but today, scientists are turning conventional theories of evolution on their heads.

In fact, a new age of biological mastery is dawning on us, and the author is a key player in this process; she made a decisive contribution to research that enabled the rational and intentional modification of the genetic code — no evolution necessary.

The first thing to understand is that modifying the genetic code isn't as unnatural as it might sound, and cases of natural "gene editing" do occur. For instance, in 2013, scientists at the National Institutes of Health, or NIH, were perplexed by one of their patients: Kim suffered from an obscure hereditary disease called WHIM syndrome. WHIM is a painful and potentially lethal immunodeficiency disorder brought on by a single "spelling mistake" in human DNA.

Kim was diagnosed with WHIM in the 1960s, but when scientists observed her again in 2013, she was miraculously symptom-free. When the researchers took a closer look at Kim's blood cells, they discovered that in one of her chromosomes, an incredible 35 million letters were missing in her DNA code and that the rest of her DNA was also in complete disorder.

Their only conclusion was that a cell in Kim's body had undergone a cataclysmic event known as _chromothripsis_, in which a chromosome suddenly explodes, rearranging the genes it contains. The chain of events brought on by this disruption erased the misspelling in Kim's genetic code that was causing her illness and resulted in the total abatement of her symptoms.

In other words, nature spontaneously and unintentionally "edited" Kim's genome to the great benefit of her health. But what if such edits didn't depend on improbable flukes or the whims of nature? What if science could reverse the often devastating effects of genetic misspellings and correct them to cure genetic disorders?

Well, such questions have been the topic of a long-standing line of scientific inquiry that you'll learn all about in the next blink.

### 3. Deliberate modifications of DNA, were impractical until a new genetic discovery was made. 

Before we dive headfirst into the biological details of gene editing, it's helpful to have a short recap on the scientific language involved. First, there's the _genome_, which refers to the complete set of genetic information contained within our cells. The genome determines our physical characteristics like height, skin color and even susceptibility to disease.

The genome itself is made up of deoxyribonucleic acid, more commonly known as _DNA_, a molecule that consists of four chemical groups: _adenine,_ or _A_ ; _guanine_, or _G_ ; _cytosine_, or _C_ and _thymine_, or _T_. In this sense, A, G, C and T are the four letters of the genetic language.

From there, the human genome is divided into packages of DNA called _chromosomes_. These chromosomes contain smaller groups called _genes_, which are regions of DNA responsible for specific bodily functions.

Now that we've got that biology lesson out of the way, let's get back to gene editing and the long search for a way to modify the genetic code. This process all began when scientists observed that viruses could insert their DNA into cells. Even more incredibly, viruses can weave their genome into a bacterial chromosome.

From this basic observation, gene editing made its first breakthrough in the 1980s; the researchers Mario Capecchi and Oliver Smithies succeeded in overwriting defective genes with healthy ones through a process known as _homologous recombination_. Unfortunately, the technique had no therapeutic viability, since it was only successful in one out of 100 attempts.

During the 1990s and 2000s, other techniques were pioneered, but all of them were plagued by extreme complexity and impracticality in clinical application.

That is, until researchers noticed a region of bacterial DNA that they dubbed _CRISPR_, or _clustered regularly interspaced short palindromic repeats_. In layman's terms, this means the DNA was repeating itself exactly over certain intervals. This discovery led to the development of a technique that's both simple and effective enough to be used in the real world.

### 4. Research on CRISPR paved the way to the discovery of a DNA cutting machine. 

So, now you know that CRISPRs are the key to simple, effective gene editing — but what exactly are they?

In a nutshell, they are regions of bacterial DNA characterized by exact repetitions of genetic sequences. In between these repetitions are similarly-sized lines of DNA known as _spacer sequences_.

CRISPRs are quite common in the DNA of bacteria and, interestingly enough, when genetic peculiarities like this pop up so frequently, it's usually because nature has an important function for them. In the mid-2000s, scientists came closer to figuring out what exactly this function is when they noticed that the spacer sequences within CRISPRs perfectly match the DNA of bacterial viruses.

Pretty soon, they realized that CRISPR DNA is central to the bacterial immune system responsible for fighting off viruses.

In bacteria, CRISPRs function like molecular "vaccination cards" by storing memories of past viruses as spacer sequences in CRISPR DNA. In so doing, they enable bacteria to recognize and destroy viruses during future infections. 

To do so, CRISPRs rely on three essential components to cut through virus DNA. First are the _CRISPR-associated_, or _CAS_, genes. These genes reside in the regions adjacent to CRISPR DNA. Most important among them is the _Cas9 gene_, which codes for a special protein that cuts through and disables invading DNA.

Second is the _CRISPR RNA_. Generally speaking, _RNA_, or _ribonucleic acid_, is DNA's cousin. It's produced from DNA simply by swapping out the letter T of thymine for the U of _uracil_. CRISPR RNA is essentially a messenger within cells that guide the Cas9 protein to the location where a cut is needed.

And finally, _tracrRNA_ plays the role of an assistant, helping to activate the cutting process.

Having made all of these findings, researchers asked the next logical question: if CRISPRs help bacteria cut viral DNA, could they be used in a lab to target and splice other DNA?

> _"Evolution had generated a virtual Swiss army knife to fight viruses."_

### 5. Using CRISPR, the author discovered a cheap and easy method for gene editing, inspiring further research. 

You now know about the CRISPR cutting machine, but there are quite a lot of technical concepts and terms to take in. So, let's give it a quick recap:

Given a piece of CRISPR DNA, the CRISPR RNA associated with this genetic sequence can guide the cutting protein produced by the Cas9 gene to a location in foreign DNA. This location would be one that precisely matches one of the spacer sequences in the original CRISPR DNA. This Cas9 protein then cuts out the matching piece and, with its work done, is led away by its guide RNA.

From there, through the body's natural repair functions, that piece of cut DNA begins to try to mend itself. But before that happens, scientists have a brief window during which they can insert a different piece of DNA into the gap. In this way, CRISPRs can be utilized as a gene editing tool, and the author was the first to demonstrate how.

In a groundbreaking 2012 paper that she coauthored with Emmanuelle Charpentier and published in the journal _Science_, the author demonstrated how the CRISPR gene editing method could be applied to this end.

In the paper, the pair sliced jellyfish DNA apart at specific and deliberate locations. This act alone was a major breakthrough in the field, but what made the CRISPR method even more incredible was that it was remarkably cheap and easy to use.

As a result, it quickly aroused excitement in the scientific community and soon came to inspire further research. For instance, in 2013, the Harvard professor Kiran Musunuru applied the method to the DNA of patients suffering from sickle cell anemia, a disease that makes it difficult for red blood cells to transport oxygen through the body.

The disease is caused by a single-letter mutation in the beta-globin gene, and by applying the CRISPR method, Musunuru was able to correct the misspelling in his laboratory.

Naturally, such an ability to play God held the potential to transform medicine and, in an instant, CRISPR became the most valuable tool available in genetic research.

> _"Instead of remaining an unwieldy, uninterpretable document, the genome would become as malleable as a piece of literary prose at the mercy of an editor's red pen."_

### 6. Gene editing has a number of practical applications in agriculture alone. 

The CRISPR gene editing method opened up a wide new world of opportunities; after all, the prospect of controlling the human genome broke down any and all limits to human imagination in gene engineering.

Could scientists now simply make woolly mammoths or unicorns?

Well, as surprising as it might be, such ideas do cross the minds of prominent gene thinkers — but there are clearly more practical applications for the process. One is in agriculture, where such gene editing tools could be used to create higher yields, more resilient crops and healthier food.

For instance, CRISPR could rescue the citrus industry from a bacterial plant disease known as _huanglongbing_, or _yellow dragon disease_ when translated from Chinese. This infection has decimated plantations in Asia and now threatens the orchards of Florida and California.

CRISPR could even make food better for us. Humans produce and consume millions of tons of soybean oil every year, but this oil contains unhealthy levels of trans fats, which have been linked to elevated cholesterol and heart disease. By using CRISPR, soybean genetics could be altered to reduce the unhealthy fatty acids present in this ubiquitous food.

And it doesn't have to stop with plants; there are plenty of ways gene editing could be applied to livestock as well. Canadian researchers have already produced what they call the "Enviropig," a genetically modified sow that contains a gene from the _E. coli_ bacteria to improve its digestion, resulting in a 75-percent reduction in the phosphorous content of its manure.

This is a significant development, since phosphorous-laden manure often makes its way into streams and rivers, spawning algal blooms that kill aquatic life. Cows could be another potential application for such gene manipulation. Cows' horns are often removed, causing great pain and stress to the animal, but this trauma could all be avoided by simply designing a cow that never grows horns in the first place.

### 7. CRISPR gene editing could also usher in a new world of medical possibilities. 

Did you know that there are over 7,000 human genetic diseases that are caused by the mutation of just a single gene each?

It's a shocking statistic, but thankfully, CRISPRs can be used to identify cures to many of these ailments. In fact, now that science has a cheap and easy-to-use genetic editing tool at its fingertips, we could soon see the rise of precise genetic therapies that usher in a new era of medicine.

Just take _HIV_, the _human immunodeficiency virus_. Most people are aware that this sexually transmitted disease afflicts millions of people globally. However, you might not know that there are a few lucky people who have a natural resistance to HIV. This resistance is the result of a mutation in the _CCR5_ gene, which codes for a specific protein.

Current research strongly indicates that, by using CRISPR to edit the CCR5 gene into nonresistant subjects, it could be possible to prevent HIV infection in the first place.

Or consider the fatal muscle-degenerative disease known as _Duchenne muscular dystrophy_, or _DMD_. DMD is a genetic disease inherited by one in 3,600 male babies. Sufferers of the disorder experience severe muscular degeneration and often end up in a wheelchair by the age of ten. This disease is also caused by the mutation of a single gene; in this case, the so-called DMD gene. Recent studies conducted on mice again indicate that CRISPR could help find a cure.

And finally, cancer research could experience a leap forward as a result of the opportunities opened up by CRISPR gene editing. After all, cancer is caused by DNA mutations, some of which are inherited and others of which are acquired through habits, like smoking. By eliminating such mutations, CRISPR could offer new treatments or even prevent cancer altogether.

So, it's clear that gene editing has much to offer. But being in total control of our genes, and therefore evolution, is no simple matter. This breakthrough also comes with ample risks, which we'll explore in the next blink.

### 8. Gene editing raises ethical questions and requires careful discussion. 

In 2014, just two years after the author and her colleague made their breakthrough discovery, public excitement about CRISPR was on the rise. Around the same time, an up-and-coming entrepreneur approached the coauthor, Samuel Sternberg — who was then a PhD student of the author's — with an offer: would she like to join a start-up that would offer couples the first "CRISPR baby."

Sternberg declined the proposition, but it raised serious questions nonetheless. Had gene editing become _too_ easy and accessible? Which ethical questions were at stake with the development of this technology?

Gene editing using CRISPR can undoubtedly do a world of good by preventing genetic diseases. But what about offering "designer babies?" What are the ethical dimensions of using gene editing to choose a baby's gender or implant genes associated with desirable characteristics like bigger muscles?

Naturally, the author was concerned about CRISPR being abused. In fact, her fear became so strong that she had a dream in which she met Adolf Hitler, who asked her about the details of CRISPR and sparked a thought about what would have happened if this genocidal maniac had used her discovery to meticulously create a genetically filtered Aryan race.

The long and the short of it is that there are many difficult questions like these to address, and the only solution is an open discussion about gene editing. That was the aim of the author and several other experts in the field when they published a white paper in 2015, which discussed the ethical implications of gene editing for the scientific community and society at large.

More specifically, the paper focused on gene editing in the human _germline_, which refers to the cells that pass on genetic information during reproduction. The paper urged the scientific community to hold back this line of research until a thorough discussion could take place about the social, ethical and philosophical dilemmas involved.

In summary, the decision about how to use CRISPR must be made by society at large, and to make such a decision, people first need to be educated on the matter.

> The first monkey babies with a genome rewritten with the help of CRISPR have already been born.

### 9. The future of gene editing hinges on a number of considerations. 

So, there's a lot of thinking that needs to be done before gene editing moves forward and, at the moment, there are a few contending opinions. For example, while the National Institutes of Health, in accordance with directives from the Barack Obama administration, called for a suspension of all research involving gene editing of human embryos, others have argued that such research should be aggressively pursued.

It's complicated terrain, but for the author, there are three general themes to consider before taking any decisions on whether human gene editing should be pursued: _safety_, _ethics_ and _regulation_.

Regarding the first, safety, the author believes that, sooner or later, germline editing will be safe enough for clinical use. After all, the human body experiences around 1 million natural genetic mutations every second, and even if CRISPR produced unintended mutations while eliminating disease-causing genes, the overall benefits would likely outweigh the dangers.

Then there's the ethical dimension. This is important because if we have the tools to safely correct disease-causing mutations, there will be a strong argument to use them. However, there's a thin and potentially dangerous line between improving people's health and creating genetic enhancements to things like intelligence, athleticism, or beauty.

If we steer toward the latter approach, rich people would be able to benefit more from germline editing, which only they will be able to afford.

That being said, such legitimate objections shouldn't lead to a general prohibition on germline editing.

Finally, there are regulatory considerations to be made. Since governments are tasked with representing their societies, they must play a role in overseeing the methods used to modify the human germline. Ideally, there should even be a global consensus about how such policy is written and applied.

Luckily, steps are already being taken in this direction. One example is the 2015 International Summit on Human Gene Editing, an event attended by politicians and scientists alike. Such events will no doubt become more common in the future, creating the space necessary for these important conversations to unfold.

### 10. Final summary 

The key message in this book:

**By taking cues from nature, scientists have learned that the human genome can be edited through a process called CRISPR. But whether it** ** _should_** **be edited is a whole different story. We have reached a point in medical science where major genetic interventions are possible, and we must now carefully consider the implications of this ability.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _A Life Decoded_** **by J. Craig Venter**

_A Life Decoded_ (2007) is the autobiography of the prominent American biochemist and geneticist Craig Venter, who played a key role in one of the greatest scientific achievements of our time — the deciphering of the human genetic code. These blinks describe the personal experiences that drove his scientific research, even at times when his methods were attacked by the scientific community.
---

### Jennifer A. Doudna, Samuel H. Sternberg

Jennifer A. Doudna, PhD, is a professor of chemistry and molecular and cell biology at the University of California, Berkeley. She is a leading international expert on CRISPR biology and genome engineering.

Samuel H. Sternberg, PhD, is a biochemist and the author of a number of high-profile scientific publications on CRISPR technology. He is the recipient of the Scaring Award and the Harold M. Weintraub Graduate Student Award.

