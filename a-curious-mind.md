---
id: 558072946232320007800000
slug: a-curious-mind-en
published_date: 2015-06-19T00:00:00.000+00:00
author: Brian Grazer and Charles Fishman
title: A Curious Mind
subtitle: The Secret to a Bigger Life
main_color: D6BE2B
text_color: 706416
---

# A Curious Mind

_The Secret to a Bigger Life_

**Brian Grazer and Charles Fishman**

_A Curious Mind_ (2015) investigates a vital attribute that many of us simply don't value highly enough: curiosity. These blinks explain the vital importance of curiosity, and outline the ways it can improve your relationships with your employees, customers or loved ones — and even help you conquer your fears.

---
### 1. What’s in it for me? Discover why adopting a curious mindset can lead you to success. 

If you've ever spent any time in a room with a five-year-old, you'll know that children ask a lot of questions. Everything from the color of the sky to the number of fingers on their hands can spark their curiosity; they want to know the reasons behind everything.

But then, as we grow older, we tend to lose this thirst for knowledge and learning. We tend to accept things as they are, and stop asking the simple question of "why?" These blinks show you why it is vital that we rediscover our love of finding things out, and why our culture, careers and even democracies depend on our curiosity.

In these blinks, you'll discover

  * why the founder of Wal-Mart actively encouraged taking rivals' ideas;

  * how curiosity can help save a moribund relationship; and

  * why our society depends on our curiosity toward our leaders.

### 2. Curiosity is an essential tool for gathering knowledge. 

Everyone has heard the saying "knowledge is power." We all know that knowledge is essential, but what's the best way to acquire it?

The secret to gaining knowledge is _curiosity_ — the more curious you are, the more information you'll collect. By collecting more information, you'll be better equipped to react to changing situations, since you'll be able to consider more options. Better still, you might even be able to anticipate changes before they happen.

Sam Walton, the founder of Walmart, understood the central importance of curiosity. He held regular meetings with his top 500 managers every Saturday morning. Each time, he'd ask what they'd seen when visiting Walmart's competitors. He was curious to know every detail about his rivals, in case they had any interesting new ideas.

The knowledge he gained from these meetings allowed him to optimize his stores; Walmart has fairly comfortably held onto its position as the market leader.

Collecting information like this is always important. You never know when you'll need any given piece of information, but you'll be glad to have it when the time comes around.

A good strategy one of the authors uses for obtaining more knowledge is to have _curiosity conversations,_ and he's had many of them over the past few decades. Curiosity conversations don't have a specific purpose, but they've often served him in unexpected ways.

For example, in 1992, he had a conversation with Daryl Gates, the chief of the LAPD at the time, who was notorious for his autocratic style of leadership. He didn't think much about the conversation at the time, but when he was working on a film about J. Edgar Hoover, the first director of the FBI, a few years later, he thought about Daryl Gates; their conversation had given him an insight into the thoughts and behavior of people in powerful positions, and he used Gates as an inspiration.

> _"Life isn't about finding the answers, it's about asking the questions."_

### 3. Curiosity can help you conquer your fears. 

When you possess a deep curiosity about something, your desire to learn more about it will let you push past just about any obstacle that presents itself. Curiosity helps you overcome your fear of rejection, because it motivates you to keep pushing each time you are told "no" until you finally get a "yes."

One of the authors, Brian Grazer, has plenty of personal experience with overcoming rejection, but his keen sense of curiosity was especially important in earning his first taste of commercial success with the film _Splash_. The film told the story of the love between a mermaid and a human, and at first, no one was interested in making it.

He could have easily given up and started on an entirely new project, but he kept going because he was so curious to see how the film would turn out. He continuously tweaked his strategy for getting the movie made, and eventually he got a "yes." His curiosity pushed him all the way and the film turned out to be a surprise hit.

You should also be curious about your fears themselves, because that's precisely how you can come to understand and confront them.

Here's an example: Because he's a renowned movie producer, Grazer is frequently asked to give speeches, and he admits that he's always dreaded such occasions. But once again, curiosity can come to the rescue.

First he became curious about the origin of his fear, and he realized he was actually afraid of being unprepared. So he started thoroughly preparing for every speech by asking himself tough questions about his goals, his audience and the nature of his speech.

This curiosity for even the smallest details helps him prepare carefully and deal with his nerves. These days, he's not afraid of giving speeches at all.

> _"If you harness curiosity to your dreams, it can help power them along to reality."_

### 4. Curiosity helps you tell great stories and great stories fuel your curiosity. 

Life is often about telling stories, not only for writers or film directors, but also for anyone sharing an anecdote among friends or even trying to sell a product.

When it comes to telling a good story, curiosity plays a key role in more ways than one might think.

Curiosity allows you to collect the information you need to tell a good story. When you want to engage your audience, you need to know something they don't. Always staying curious helps you bring together interesting experiences that you can tell others about later.

For example, Brian Grazer has long been curious about the work of intelligence agencies. Over the years, he's managed to have curiosity conversations with two CIA directors and a number of intelligence agents from other countries like the United Kingdom and Israel.

These conversations are what gave him the idea for _24_, a fast-paced TV series about an intelligence agent, with an hour of storyline elapsing over the course of each hour-long episode. The things he learned in his curiosity conversations helped him make the show as realistic and entertaining as possible.

Curiosity doesn't just help you write a good story though — it helps you grab and hold the audience's attention.

Whenever you watch a film or TV series, or read a book or newspaper article, it's your curiosity that entices you to read on. A good title makes you curious to read an article; the cliffhanger at the end of an episode makes you curious about the next one.

Make someone curious about your story and they'll want to know the rest of it. Make someone curious about your product and they'll try to find out more.

### 5. In business, curiosity helps you connect with your colleagues and customers. 

When listing the factors most essential to running a successful business, curiosity might not be the first thing that comes to mind, but it's actually a powerful business tool. Why?

When you're curious about other people's thoughts, you collaborate with them more effectively. Curiosity fosters cooperation.

Grazer uses this strategy as the head of his production firm. He strives to lead by asking questions, and avoids giving direct orders whenever possible. He also encourages his employees to ask questions of him.

This gives people more opportunities to explain and reflect on their work. They'll become more confident about their work's positive potential and be more aware of any flaws.

Mutual curiosity often leads to meaningful dialogue, which can inspire innovation and help unearth solutions to any problem.

Additionally, curiosity doesn't just help your relationships with your colleagues — it helps you connect with your customers too.

If you're looking to buy a new TV, for instance, you won't be pleased with a salesperson who aggressively pushes the TV they assume is best for you. You'll be much more appreciative of a person who tries hard to find out what you want, then tailors their suggestions accordingly.

So a curious salesperson will make you feel more comfortable. They'll be better able to find what you want and you'll be much more likely to want to go back to their store.

Anytime you're offering something to a customer, be sure to find out exactly what they need. If you're curious about what they truly want, you'll be better equipped to serve them, which also means you'll be more likely to make a sale.

> _"Even when you're in charge, you are often much more effective asking questions than giving orders."_

### 6. Curiosity improves your relationships with your loved ones. 

As we've seen, curiosity motivates us to explore the world around us and gain more knowledge, leading us to have new and exciting experiences. But we should also direct it toward the things closest to us, like our friends and family. You might think you already know everything there is to know about your loved ones, but chances are you actually don't.

Stay curious about the lives of people you care about — it's a sign of respect and it strengthens your relationships.

But it can also be easy to lose your curiosity about loved ones, especially in long-term romantic relationships. If you get trapped in a routine, you might find yourself asking your partner "How was your day?" every evening, and just getting the same answer.

So instead, try asking more specific questions. A question like "How did your meeting with that new client go?" shows that you're genuinely interested in your partner's day. It shows that you remembered they had a meeting in the first place, and that you care about how it went.

A lack of curiosity is definitely a sign of a deteriorating relationship. The good news is that it's easy to revive a relationship by being genuinely curious.

Curiosity is what helps you connect with people in the first place. When you meet someone — whether you're in a bar, a business meeting or an airplane — you don't get to know them just by saying "Hi!" and launching into your life story. You ask them about their own story first.

Nearly everyone would rather be asked questions than be told a story they didn't ask to hear. Think about a good first date: if you're interested in the person, you'll enjoy asking them questions and telling them about yourself. Curiosity brings people closer together.

> _"Human connection requires sincerity. it requires compassion. It requires trust. Can you really have sincerity, or compassion, or trust, without curiosity?"_

### 7. Public curiosity is an essential part of a functioning democracy. 

When was the last time you voted? Did you feel well informed beforehand? Were you confident about your decision? Curiosity isn't just a tool for enriching our personal and business lives — it's an important component of democracy too.

If we aren't curious about the institutions in our society, we can't hold them accountable for whatever it is they're doing. Every election invites us — or even obliges us — to be curious. You need to know about the candidates, their political views and the plans they have for the future; you can't make a good decision otherwise.

Of course, this curiosity can take up a lot of time and effort, but consider the act of reading a newspaper or watching the news on TV: when you take in news, you're essentially outsourcing your curiosity to journalists, whose very job it is to be curious and hunt for the most important stories.

Throughout history, curious people have always been agents of social progress. They've led our society to where it is today.

In Europe, for example, the Church held immense power before the Enlightenment. As an institution, it decided what people were allowed to think, and the questions they were allowed to ask. It suppressed curiosity in ideas that went against its teachings.

But gradually, people managed to break free thanks to extremely curious scientists like Leonardo da Vinci and Isaac Newton. They kept asking tough questions about their world and the answers they found propelled their societies into modernity.

Curiosity is still considered dangerous in some parts of the world today. The Chinese government, for instance, censors a great deal of the internet to keep its citizens away from information that might help them threaten the established system.

> _"Curiosity is itself a form of power, and also a form of courage."_

### 8. Sometimes it’s important to put limits on your curiosity. 

After looking at all the great benefits that curiosity brings, it might seem as if you can never have enough of it. However, it is indeed possible to have too much curiosity. Too much information can be distracting or demotivating, so it's important to know what your limits are.

Sometimes, when you're confident about what you're doing and you don't want to get distracted, it's good to be _anti-curious_ about other people's opinions.

For example, Brian Grazer has produced some movies that weren't commercially successful, despite the fact that he himself thought they were very good. His unconventional films like _Rush_ and _Frost/Nixon_ were always unlikely to attract the same audiences or success as _A Beautiful Mind_ or _Apollo 13_. But he was determined to get them made, so he had to stop listening to people who thought he was making the wrong choice.

It's not always easy to know when you should tone down your curiosity. You have to be confident about your opinions before you can know when to stop listening to the opinions of others.

Grazer has worked in the film industry for decades, so he's confident about his tastes. He knows which films are well written, so he can recognize a good idea like _Frost/Nixon_ even when others don't.

_Frost/Nixon_ wasn't a big hit at the box office, which wasn't a surprise, but it received five Academy Award nominations and was applauded by critics. Sometimes it pays to keep a lid on your curiosity and go with what you already know and believe.

> _"You can't get anything done trying to absorb and neutralize everyone else's criticisms."_

### 9. Final summary 

The key message in this book:

**Curiosity is what drives us to learn and encourages us to seek out new experiences. It's a critical part of achieving success in business, getting closer to your loved ones and growing as a person. It can help you overcome fear, amass useful knowledge and gain surprising insights. Curiosity is an important lifelong asset, so don't let yours fade!**

Actionable advice:

**Don't be afraid to ask questions.**

Even if you're talking to a complete stranger, questions allow you to learn. They also show the person you're talking to that you're interested in them, which is vital to forging strong relationships.

**Suggested further reading:** ** _The Eureka Factor_** **by John Kounios and Mark Beeman**

_The Eureka Factor_ (2015) looks at the remarkable phenomena of insights and creativity, and how the two are intertwined. By laying out the latest scientific research, it sheds light on how insights work, including what supports and hinders them. In addition, it provides powerful advice on how everyone can train themselves to have more eureka moments.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Brian Grazer and Charles Fishman

Brian Grazer has been one of Hollywood's most successful producers since the 1990s. He has produced films such as _Apollo 13_ and _A Beautiful Mind_, which won the Academy Award for Best Picture in 2002.

Charles Fishman is a renowned business journalist, and is the author of _The Wal-Mart Effect_ and _The Big Thirst_.

