---
id: 5cfe628a6cee070007117686
slug: a-first-rate-madness-en
published_date: 2019-06-12T00:00:00.000+00:00
author: Nassir Ghaemi
title: A First-Rate Madness
subtitle: Uncovering the Links Between Leadership and Mental Illness
main_color: 80998E
text_color: 55665F
---

# A First-Rate Madness

_Uncovering the Links Between Leadership and Mental Illness_

**Nassir Ghaemi**

_A First-Rate Madness_ (2011) argues that some of the world's most effective leaders were able to achieve such heights because of their experiences with mental illness. Conversely, the book makes the argument that while mentally healthy leaders may succeed when the world is running smoothly, their mental health actually inhibits their leadership abilities in times of upheaval.

---
### 1. What’s in it for me? Discover how experience with mental illness can work in your favor. 

Mental health issues don't get cast in the best light. Despite progress in recent years, a certain stigma still surrounds them. It means people who have experiences with psychiatric illnesses are often unwilling to speak out. That's unfortunate and, as far as Nassir Ghaemi is concerned, things don't have to be that way.

His approach is plain. If we look to the past, we can see that many of the world's greatest leaders had mental health issues. If we accept and elevate that tendency, we can find a way to celebrate what can be the positive consequences of psychiatric illness.

In the past, this taboo has meant that we preferred to support the representatives and leaders who seemed the most "normal." But having depression or bipolar disorder, for example, may be an advantage when serving in office.

As you'll see in these blinks, some of the most impactful leaders in the world's history personally experienced mental health issues. By looking at these leaders' behavior, we can shed some light on the relationship between mental health and political leadership, challenging some of our deep-seated prejudices about this topic.

In these blinks, you'll learn:

  * how hyperthymic personality disorder may have prevented nuclear war;

  * why Tony Blair wasn't as monomaniacal as he's often portrayed; and

  * into what surprising profession the prejudice against mental health reaches.

### 2. Some mental illnesses may improve leadership abilities. 

Mental illness is serious. In its most extreme forms, it can be devastating for those who have it and their loved ones. Furthermore, a social taboo still surrounds mental illness. That may explain why some people experiencing mental illness are ashamed and try to hide it.

However, the author believes that some forms of mental illness involve abilities that are otherwise inaccessible to the rest of the population.

Specifically, the author has two mental illnesses in mind: _major depressive disorder_ — commonly called depression — and _bipolar disorder_. They each foster important character traits.

For instance, let's consider someone who's faced depression. Depression is a disorder that affects the mood and leads to feelings of sadness. It can also reduce motivation and interest in the world. However, it often means that someone who experiences it is more empathic than average since he'll have experience and understanding of sadness.

People who've not suffered from depression might be able to conceive of the experience of severe depression, but it's not the same as having been through it. And if you've managed to come through it, then you may have a better grasp of the struggles of the human experience.

Bipolar disorder is somewhat different. People who have it may rapidly oscillate in their moods between mania and depression. It can make them more spontaneous. However, those high levels of energy and heightened moods are balanced with deep lows of depression.

Also — and we'll return to these in later blinks — this means bipolar-disorder personalities often look at things from non-standard perspectives. They are able to imagine creative solutions to problems that someone who hasn't experienced bipolar disorder would find hard to conceive.

Both of these disorders also occur in "milder" forms. _Dysthymic personality disorder_ is associated with depressive types, while _hyperthymic personality disorder_ corresponds with bipolar disorder.

People with dysthymic personalities lead their lives in a constant state of mild depression, which may culminate in several depressive episodes over the course of a lifetime. Dysthymic personalities are often empathic and deep thinkers.

Hyperthymic personalities are often bursting with energy and creativity. They are charming and without fear. They are, essentially, in a constant state of mild mania.

Now that we've gotten an overview of these disorders, let's look at some of the most influential leaders of recent decades with dysthymic and hyperthymic personalities.

### 3. Martin Luther King Jr. and Gandhi struggled with depression, which conceivably led them to advocate non-violent resistance. 

In all likelihood, few people would suspect that two leaders who inspired — and continue to inspire — hope in millions of people were depressive characters. The author, Ghaemi, believes Mahatma Gandhi was a dysthymic personality, while Martin Luther King Jr. experienced at least three episodes of severe depression. Both of these leaders initially suffered periods of depression in childhood, which set a trend into adulthood. As it happens, both King and Gandhi attempted suicide at young ages.

When Gandhi was a teenager, he deliberately ate some poisonous seeds with a friend. His rationale was that he wasn't able to care properly for his elderly father. He felt guilty.

As for King, he twice jumped out of a window when he was 12. King's friend, the psychiatrist Alvin Poussaint, explained this behavior as merely an impulsive reaction to the death of King's grandmother. However, the author is firmly of the belief that these were real suicide attempts.

The author sees similar behavior patterns in these two leaders' later years. Apparently, depression was either triggered or exacerbated by the pressure in their lives. After all, both led rights movements that were faced with huge counter-reactions. When you think about it, then, these leaders probably felt great responsibility for their followers and were frustrated that obstacles seemed unsurpassable.

Empathy can be observed as a neurological phenomenon — for example, when we see another person being abused, our brains react as though we were the ones on the receiving end of the abuse. It's known as the _mirror neuron system_ and has been seen in experiments on macaques.

As we saw in the last blink, depression strengthens empathy. A study of college students showed that depression lets people feel what others feel _more_ intensely, even when they're not in a depressive episode.

The author sees a link with King and Gandhi's politics here, interpreting their politics as a form of "radical empathy." In other words, both leaders advocated for love and understanding; opponents were not to be hated.

The connection is clear for the author. These two leaders set about resolving conflict through non-violent means, but it was an approach that drew upon non-normative world views shaped by mental illness.

### 4. Churchill and JFK’s struggles with mental health benefited the world, but mania in a leader can also lead to disaster. 

In 1938, British Prime Minister Neville Chamberlain stood before the House of Commons to make an announcement. He would travel to Germany to meet Hitler in an attempt to convince him of the unnecessariness of war.

MPs jumped up and cheered at the plan. But Winston Churchill remained seated, despite others rebuking him for not joining in.

Churchill had his reasons. In 1930, long before anyone else in British government came to the same conclusion, Churchill had recognized the Nazis' threat. They were a danger to world peace.

In the author's professional opinion, Churchill likely had type II bipolar disorder. This resulted in several manic and depressive phases.

Therefore, in the author's eyes, Churchill's depressive experience meant he was able to discern situations realistically. In comparison, his mentally healthy colleagues' optimism was misplaced.

The thirty-fifth president of the United States, John F. Kennedy, is also an interesting figure. For the author, JFK exemplifies hyperthymic personality disorder. Simply put then, he was a hyperactive and creative risk-taker — with a sex drive to match.

This was particularly in evidence during the Cuban Missile Crisis in October 1962. The Soviet leader Nikita Khrushchev sent Russian nuclear missiles to Cuba to goad Kennedy into a pre-emptive strike — with atomic weapons.

But Kennedy held firm. He ignored the advice of everyone around him. By doing so, he averted nuclear disaster. Other leaders who might not have had Kennedy's experience with mental health struggles would probably have acted differently. No doubt they would have shared the natural impulse common among Kennedy's advisers and ordered a preemptive attack.

Adolf Hitler is another world leader worth considering when thinking about mental health. The author is certain that Hitler had bipolar disorder. But what really caused the evils he leveled on the world was his vile ideology mixed with an overreliance on methamphetamines.

Certainly, Hitler suffered from bouts of mania and depression during his childhood. It's thanks to his friend August Kubizek that we know this.

Hitler had always had an aggressively megalomaniacal streak, but it was as a leader capable of manipulating the masses that he found his feet. The author even argues that his demagoguery might have been facilitated by his personality disorder.

It's also worth remembering that Hitler's aggressive and maniacal behavior went into overdrive from 1937 onward. That's when his doctor started prescribing him methamphetamines.

It's quite possible that Hitler's prolonged use of methamphetamines heightened his mental instability. Consequently, over time he became less and less likely to listen to the advice given to him by subordinates. His outbursts of violent rage also became more frequent.

### 5. Tony Blair and George W. Bush’s sound mental health did not help them lead. 

It's fairly common for journalists and political commentators to start blaming politicians' mental health when they don't like their decisions and actions.

That was very much the case with forty-third US President George W. Bush and Tony Blair, the UK prime minister from 1997 to 2007. Many people thought that their political decisions were indicative of mental instability. But as far as the author is concerned, their thought processes were fairly typical for mentally healthy minds.

After 9/11, both leaders felt their countries were threatened. They wanted to act. It's now clear that this impulse led them to invade Iraq on evidence that later proved extremely dubious.

Let's run a thought experiment here. What if these leaders had either hyperthymia or bipolar personalities? Both of these disorders are closely associated with higher creativity. Therefore, we may speculate that such leaders would have been able to see the unfolding situation from multiple perspectives.

Such a leader might well have realized that the evidence was too insubstantial to support an invasion. He would have found another way forward, probably one inconceivable to less creative, mentally healthy types. Just think of Kennedy's response during the Cuban Missile Crisis for a comparison.

Bush and Blair's next move was also typical for the mentally healthy. Once their decision had been made, they found themselves unable to change course.

They were simply unwilling to see that their assessment of the situation in Iraq had been wrong and that the evidence on which they based their arguments for invasion was insubstantial. Even once it was clear that they hadn't been successful, they refused to budge.

The author is therefore of the opinion that Bush and Blair chose to keep troops in Iraq instead of acknowledging their mistakes. Such stubbornness is typical of those who are mentally healthy.

So Bush and Blair were hardly exceptional — characteristically, most people just don't like admitting they have ever been wrong.

This thought experiment just goes to show that had the United Kingdom and the United States had leaders with more experience of mental illness, then the whole world might ultimately have benefited.

In a hypothetical world, had Kennedy been president after 9/11 there would have been no Iraq invasion. Or, had there been an invasion, troops wouldn't have been stationed there long once it was clear an error had been made.

### 6. Recognizing the relative benefits of mental illness will aid in de-stigmatizing it. 

Despite the author's sympathetic portrayal of mental illness, the fact remains that mental illness remains a taboo subject.

There's clear evidence that we, as a society, are scared of mental illness. A 2004 study published in the _Psychiatric Bulletin_ showed that even medical professionals were prejudiced against people diagnosed with mental illnesses.

This situation makes it incredibly difficult for someone faced with mental illness. It also makes it very tricky for non-sufferers to get a clear perspective on it. Specifically, the continuing stigma surrounding mental illness means we're unable to see its upsides.

The first thing we could do is simply acknowledge and accept that past world leaders weren't always mentally healthy. That doesn't mean we have to rewrite history. It's just another layer of knowledge contributing to our picture of the past.

Mentally ill people have contributed to make the world a better place. The examples we've looked at in these blinks barely scratch the surface.

The author has also analyzed other renowned figures. For instance, Ted Turner, a pioneer of 24-hour cable news and the founder of CNN, experienced mental illness, as did President Abraham Lincoln. Turner displayed a hyperthymic personality type, while Lincoln was dysthymic.

We should accept, then, that mental health isn't just a question of "abnormal" types contrasted with seemingly normative behavior. That approach is typical of people who consider themselves mentally fit and distance themselves from anything that is marked as different.

Once we've accepted that beloved historical figures were mentally unwell and started de-stigmatizing mental health, we can move onto the next step. In this changed atmosphere, more people will doubtless seek diagnosis and help. No longer will diagnosis be something to be ashamed of, but rather something with appreciable qualities.

That analysis seemed true to the author as he wrote this book during the presidency of the forty-fourth US president, "No drama Obama", as he is often known. At the time, an ideal president seemed to be someone who was balanced and "middle-of-the-road" — psychologically as well as politically.

For the author, here's where the rub lies. That sort of president might be effective in peaceful times, but what kind of leader is needed when times are rockier? To his mind, what's needed is someone who can think differently, someone who's an asset because of the mental health issues he's experienced. Think of Churchill in World War Two or JFK in the teeth of the Cold War — the leaders you need in times of crisis.

### 7. Final summary 

The key message in these blinks:

**Mental illness is not something of which to be ashamed. Although it's true that living with a mental illness can be difficult, exhausting and upsetting for those affected and their loved ones, there is an upside. History shows us that some of the greatest world leaders were effective and unique because of their histories of mental health issues. It was precisely what was "wrong" with them that made them successful.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Shrinks_** **, by Jeffrey A. Lieberman, Ogi Ogas.**

As you've seen in these blinks, psychiatry and its history are fascinating. Psychiatry has developed over time, contributing to our understanding in both the medical and social sciences. In our blinks to _Shrinks_, you'll learn the story of psychiatry's astonishing development. It's taken 300 years, but we've come a long way. From the discipline's crude and brutal past methods based on strange and shocking therapies through to its much improved modern iteration, these blinks have an astonishing story to tell.
---

### Nassir Ghaemi

Nassir Ghaemi is a psychiatrist and professor of psychiatry at Tufts Medical Center. He also lectures on psychiatry at Harvard Medical School. His research specialisms include depression and bipolar disorder. He has published over 200 scientific articles and is a Distinguished Fellow of the American Psychiatry Association.

