---
id: 563770c262346500074a0000
slug: a-force-for-good-en
published_date: 2015-11-04T00:00:00.000+00:00
author: Daniel Goleman
title: A Force for Good
subtitle: The Dalai Lama's Vision for Humanity
main_color: 54B8EC
text_color: 27556E
---

# A Force for Good

_The Dalai Lama's Vision for Humanity_

**Daniel Goleman**

_A Force For Good_ (2015) reveals how you can swap your negative thoughts for positive actions. By taking a closer look at the Dalai Lama's wisdom and vision, these blinks explain how individuals acting positively can together form a global force of good, driving change in our world through mutual compassion.

---
### 1. What’s in it for me? Discover the Dalai Lama’s vision for a better and more compassionate world. 

When you turn on the evening news, it's easy to get depressed. All over the world there are wars, conflicts and overwhelming human misery. Is this really the best we can do? 

According to the Dalai Lama, many of the world's problems stem from a lack of compassion and moral responsibility; we care more about money than about each other. So how do we get out of this mess? 

As these blinks will show you, when we learn to replace our own negative emotions with compassion and love, we become more dedicated to the well-being of everyone around us. If we take care to listen to both science and religion, and what they can teach us about how to live our lives, we can truly become _a_ _force for good._

In these blinks, you'll learn

  * the difference between good and bad selfishness;

  * why we need a new, more compassionate economic system; and

  * what the Dalai Lama does at 5:30 in the morning every day.

### 2. A force for good will lead us toward compassionate moral responsibility. 

Every day at 5.30 a.m., the Dalai Lama wakes up bright and early to listen to the BBC news while he eats his breakfast. While this might not be the morning routine you imagined for the Dalai Lama, he maintains that through this daily ritual, he's come upon a great revelation. 

Listening to the news reveals how full of violence, cruelty and tragedy our world really is. But why? The Dalai Lama believes it actually comes down to one single deficiency: a lack of compassionate moral responsibility. Today, we act out of self-interest and disregard our moral obligations to others. 

Seems quite grim, doesn't it? But look at this way: if humans have the power to wreak so much damage and destruction, then we might also have the power to exert an equivalent positive impact. This is what the Dalai Lama calls _a force for good_. 

A force for good begins with individuals, and from within them. By creating an inner shift that diminishes our negative emotions and strengthens our capacity to act morally, we become better able to overcome impulsive reactions such as rage, frustration and hopelessness. This shift will also see us become more compassionate to those around us, and to our shared planet. 

Unlike the Dalai Lama, not all of us are able to commit five hours a day to inner practices such as meditation — but we can still take some small steps. The Dalai Lama has fashioned a plan that each individual can follow. It begins with looking inward and managing our own minds and hearts. This will help us look out at our world and see the places where we can do good.

> _"But our united efforts, if based on this inner shift, can make an enormous difference."_

### 3. Reflect on your emotional responses to make better decisions. 

Even the Dalai Lama had a short temper once upon a time. Of course, he learned to master his emotions, and he did so with a few techniques that are simpler than you'd expect. One important technique entails taking a step back when tempted to act on your feelings and considering the consequences of your choices.

In March 2008, the Chinese army shot at demonstrators and arrested many Tibetan protesters, particularly monks, during a series of protests in Lhasa and other cities. How did the Dalai Lama react? Of course, hearing such news would have filled him with rage. Nevertheless, the Dalai Lama chose to stay calm. 

He visualized the Chinese officials and replaced their negative feelings with his love, compassion and forgiveness. Having reasoned that the consequence of acting out of anger would only be further damage, he chose instead to control his feelings. 

But remember: controlling your feelings is not the same as suppressing them altogether. Bottling up negative emotions can lead to outbursts that are impossible to control. When dealing with powerful emotions, it's best to stay mindful. 

We're better off _recognizing_ negative emotions when we experience them, and asking whether the emotions we feel are in proportion to the situation or whether they're familiar. By understanding our negative emotions, we're better equipped to channel them into positive actions.

### 4. We need to become more compassionate to live kinder, happier lives. 

Compassion and awareness go hand in hand. Now that we've taken a closer look at emotional awareness, it's time to delve into compassion, starting with _where_ the notion comes from in the first place. 

In the Dalai Lama's view of the concept, compassion is deep in our nature and does _not_ come from religion. Think about it: even dogs and cats can be compassionate and altruistic to some extent. So why should compassion be bound up in religious institutions and their traditions? 

Compassion is superior to and separate from religion. In fact, it is rooted in our biological makeup. Parents' instinctive care for their young, who would otherwise die, is one sign of a biological predisposition for caring and compassion.

Moreover, our bodies have built-in needs for positive emotions such as love, joy and playfulness. These experiences help to boost our immune strength and lower the risk of heart disease. But above all, we're psychologically predisposed to seek comfort in affection, compassion and a sense of belonging within a group. 

Compassion puts our attention on something bigger than our petty concerns. This larger goal energizes us in turn. Having explored where compassion comes from and why we need it, let's investigate how it manifests itself in our world. Find out more in the next blink!

> _"The very constitution of our body guides us toward positive emotions."_

### 5. Compassion in action involves fairness, transparency and accountability. 

The Dalai Lama's form of compassion isn't the wishy-washy kind that is confined to holidays or Sunday school. Rather, he calls for moral responsibility in all spheres of public life, which includes having a profound distaste for injustice, as well as taking initiative to expose and reform corrupt systems.

Three principles exemplify such compassion in action: _fairness, transparency_ and _accountability._ By treating everyone equally, remaining open and honest, and taking responsibility for our mistakes, we can create a powerful form of compassion to drive our actions. 

Compassion in action doesn't just mean relieving suffering, but also getting engaged in rectifying wrongs by actively opposing injustice or protecting people's rights. 

Moreover, the Dalai Lama encourages us to learn how we can reduce our destructive emotions. Of course, feelings such as anger and frustration can also be constructive, functioning as drivers of positive action. 

For example, the Dalai Lama once met a social worker whose group had been given too many cases, making it impossible for them to help any of the individuals. The social worker became morally outraged, and it was with this anger that he motivated his team to protest and successfully get their workload reduced. 

However, it doesn't take much for anger to go from constructive to destructive. One way to ensure we use frustration to drive positive actions is by maintaining basic compassion towards a person that we take issue with. 

It's clear that compassion is a recurring theme here. In the next blink, we'll explore another situation where compassion plays a central role: the divide between science and religion.

> _"Oppose the act, but love the person — and make every effort to help him change his ways."_

### 6. Science and religion make a great team. 

Does it surprise you that the Dalai Lama routinely meets Nobel Prize-winning scientists, discussing intricate theories with the likes of Bob Livingston, David Bohm, Wolf Singer and Paul Ekman? Well, it shouldn't. The Dalai Lama acknowledges the strength of both science and religion, and we should too!

Despite what we might think, spirituality and science are not mutually exclusive. They're merely alternative strategies in the quest for reality. So why not bring them together? 

Science can connect with individuals on a larger scale than any religious faith, as it is not marked by the divisions and conflicts of differing religious denominations. 

However, science still hasn't revealed everything to us about how the world functions. To better understand our minds, for instance, we need to meld ancient Buddhist sources with contemporary scientific findings. 

Science can even give religious thinking greater credibility, even among skeptics. Though most people tend to dismiss the Dalai Lama's buddhist methods as "just religion," these methods have been scientifically proven to be effective in a number of contexts.

For instance, Thupten Jinpa, the Dalai Lama's interpreter, developed Compassion Cultivation Training, or CCT, a variation of classical Tibetan methods suitable for anyone. An evaluation of CCT by researchers at the Center for Compassion and Altruism Research and Education at Stanford University found that it lessened people's anxiety and increased happiness, even in those suffering from acute social phobia. In patients suffering from chronic pain, the sensitivity to pain decreased after nine weeks.

This is just one example of how religion and science can come together to complement each other's strengths and support each other's weaknesses. 

But science and religion aren't the only facets of contemporary society that need a more compassionate and cooperative character. The economy needs them too, and urgently. Find out why in the next blink.

### 7. We need a compassionate economy that blends entrepreneurial spirit with social responsibility. 

Today, it's clear that capitalism is far from perfect. On the other hand, neither is socialism. Is it even possible to create an economy that doesn't result in lasting social damage? The Dalai Lama believes it is.

First, it's important to recognize that most problems don't arise from the principles of an economic system. Rather, it's the lack of moral compassion on the part of the people that implement the system. Both capitalism and communism can be corrupted by selfishness and exploitation. 

Our current capitalist predicament has led to a rapidly growing divide between the rich and poor. In his book _Capital_, economist Thomas Piketty analyzes data trends over centuries to reveal how those with money to invest will always earn more than those who labor for their wages. An ever-increasing disparity and inequality between the rich and poor seems inherent to a free-market economy.

The Dalai Lama consequently positions himself as a Marxist in this respect, as Marxism at least features a moral dimension that takes people's well-being into account. Of course, many attempts at socialist economies have proven disastrous. So what's the Dalai Lama's solution?

He envisions a compassionate economy where entrepreneurial spirit is accompanied by a sound social support system and taxes on wealth. In other words, we need for-profit companies with the hearts of nonprofits. 

Such companies actually exist already. One is Prosperity Candle, which provides Iraqi or Thai-Burmese refugees, Haiti earthquake victims and about 600 underprivileged women with the opportunity to make a living by making candles. 

In a similar vein, Muhammad Yunus's Grameen Bank in Bangladesh pioneered microloans for people living in poverty. These loans help them start their own businesses, allowing them to become self-sufficient and eventually pay the money back, which can then be lent to others. 

Companies like this reshape capitalism into something meaningful, not just profitable. This emerging movement may prove to be very successful at turning business into a force for good.

### 8. Both the privileged and the underprivileged play vital roles in creating social change. 

As humans, we all share the same potential. Unfortunately, we often don't share the same opportunities. Even so, both advantaged and disadvantaged groups in society are responsible for working together toward change. 

Rather than looking down on marginalized groups in society, the privileged should do their part by learning about what resources would benefit the less fortunate, be it education, job training or community support. Wealthier sectors of society can make a huge difference in the lives of the poor, simply by donating a little of their time and energy. 

And what about those who are in need? Although they face considerable challenges, they too have a responsibility to _help themselves_, even if it seems useless. Many Tibetans have learned to approach their experience of poverty and oppression with this attitude. 

In the past, Chinese Communist officials spread propaganda about the inferiority of the Tibetan brain, lies which some Tibetans even began to believe themselves. 

But when given the same opportunities in education and the workforce, Tibetans naturally performed just as well as the Chinese. Realizing that they were perfectly capable of helping themselves, Tibetans freed themselves from this racial stereotype and started working harder at school, resulting in greater success and a brighter future.

Humans' ability to improve their own lives is quite incredible, and psychologists have described this phenomenon in many different terms. Carol Dweck, a Stanford psychologist, refers to it as _mindset_ : the belief that you can succeed. By maintaining such a mindset, you're more likely to keep trying. The more you try, the more likely you are to succeed.

Another psychologist, Angela Duckworth of the University of Pennsylvania, calls it _grit_ : persevering toward long-term goals despite setbacks and obstacles. 

Finally, Gandhi used the Hindi term "swaraj," meaning self-mastery or self-rule. No matter what you call it, one thing is certain — circumstances only change for the better as a result of this powerful attitude.

### 9. An obsession with profit and our tendency to block out guilt has placed our planet under threat. 

Would you burn your furniture to stay warm during winter? Of course not! Likewise, the Dalai Lama thinks we shouldn't be laying waste to our planet, as it's our only home. Unfortunately, our home has been placed at incredible risk over the last 60 years. Why?

An obsession with profit and money has seen humans' impact on the planet become increasingly damaging. Growing numbers of cars on the road, wasteful use of water, paper and other resources, and the irresponsible use of chemical fertilizers are just a few of the human activities that are wreaking havoc on the environment. 

There's no way we can continue pretending to be ignorant of the destructive impact of human activities; we all know full well the damage we cause. So why do we continue to exploit our planet? Because our desire for money outweighs our fear of future risks. 

Though the Chinese central government has tried to restrict logging practices that have repeatedly caused major floods in northern India, Bangladesh and China, some people, in the interests of continued profits, have found ways to continue cutting trees that protect river systems from silt and flooding. 

Cognitive scientist Elke Weber explains that our apparently shameless exploitation of the planet comes from our ability to block out the guilt we feel about our negative environmental footprint. As individuals, it's our responsibility to stop ourselves from tuning out. 

One simple way to do this is using a "handprint" as a way of tracking your personal impacts and the sum total of your better ecological practices. A person's handprint is a measure of positive ecological practices like turning off light switches or biking instead of driving. Each action can enlarge the handprint, motivating us to stay aware of human impacts on the planet and act accordingly.

### 10. Positive statements and individual friendships are powerful solutions for conflict. 

Even the Dalai Lama concedes that humans will always create conflict — clashes of ideas are only natural. In order to cope with such clashes, good communication and mutual understanding are vital. In fact, it's easier to create a healthy dialogue than you think. 

There are a handful of basic moves that you can turn to during a confrontation with another. The first is as simple as saying something positive about the other person and something positive about yourself. 

That's exactly what philosopher A. J. Ayer did in 1987 at a high-society party in New York. Notified that somebody was being assaulted, Ayer rushed to the scene to find Mike Tyson forcing himself on then-unknown Naomi Campbell. 

Ayer insisted that Tyson stop, to which Tyson asked him, "Don't you know who the (expletive) I am? I'm Mike Tyson, heavyweight champion of the world."

Ayer replied, "And I am the former Wykeham Professor of Logic. We are both preeminent men in our field; I suggest we talk about this like rational men." While they talked, Campbell slipped safely out of the room. 

In this situation, Ayer demonstrated commendable emotional intelligence. By saying something positive about Tyson and about himself, he established the foundation for an open dialogue on a level playing field. 

But what if you're facing a conflict that's been around for months, years, even centuries? The solution is simple: friendship between individuals. 

To prove that this approach really works, social psychologist Thomas Pettigrew tracked down more than 500 studies from more than 38 countries, with responses from a quarter of a million people. He found that time and time again, an emotional involvement with someone from an opposing group, be it a friendship or a romance, was enough to overcome prejudice.

### 11. Children need an education of the heart. 

What parent doesn't want their kid to get good grades? Although it seems healthy to encourage children to pursue academic success, it can lead to immense pressure and emotional damage. In a world where academic achievement is everything, the Dalai Lama believes that modern schooling needs a reform that prioritizes the heart. 

One way to educate the heart is through mind training. Mind training is not the same as learning facts, figures and historical dates. Rather, training the mind centers on improving a student's ability to concentrate, regulate and reflect on their thoughts. 

Simran Deol, an eleventh-grader, sat with her eyes fixed on a dot in front of her while wearing a helmet that measured her concentration levels. Her concentration soon began to waver, so the Dalai Lama reminded Simran that, when training our mind, it's useful to make a distinction between the mental and sensory levels of thought. 

As Simran observed the dot, her mind was focused on it on the sensory level. But this focus was hindered by other sounds and sensations. In order to sharpen her focus, Simran began to concentrate on the dot within the mental plane as well; this meant holding the image in her mind's eye. 

Her concentration made a striking improvement, demonstrating the power of a rather simple, but very useful technique. Just think of all the times when you know you could have made a better choice if you had just been more concentrated on the task at hand!

Today's children are tomorrow's leaders, so we should equip them with what they really need: powerful, reliable ethics and the capacity for living by compassionate values. 

Using mind training exercises like the one performed by Simran, the Dalai Lama's proposed education of the heart covers the basics of how the mind works: the dynamics of our emotions; skills for healthy regulation of emotional impulse; the cultivation of attention, empathy and caring; learning to handle conflicts nonviolently; and, above all, a sense of oneness with humanity.

### 12. When things seem dire, consider the situation from a long-term perspective. 

Though the current global situation can often seem quite dire, we've got a lot to be thankful for — it's all a matter of perspective. Think about it: in the past, when nations declared war, citizens would proudly join in the violence. These days, people are fed up with the glorification of war, and strong movements for peace have shaken the political foundations of countries around the world. 

Looking at things in the long-term can help us stay optimistic, even when the present seems overwhelmingly grim. The late Carl Friedrich von Weizsäcker, a German philosopher and the Dalai Lama's tutor in quantum physics, recalls how the German and French were once bitter enemies. Yet within von Weizsäcker's lifetime, Charles de Gaulle, who had led the Free French Army against the Nazis, became close friends with the German chancellor Konrad Adenauer. 

The two leaders joined forces to support the formation of the European Union. De Gaulle and Adenauer's actions resulted in a positive change in Europe that would have been unimaginable during the Second World War. 

Today, peaceful relations between certain countries at war seems equally unimaginable, especially when we watch the news. Of course, the function of mass media is to inform us about current problems and threats. This can give us the impression that compassion among humans has long since disappeared, and that the cruelty will only escalate as each new day brings another round of frightening headlines. 

But we must remember that, on any given day, the amount of kindness in the world vastly exceeds the incidents of cruelty — we just rarely hear about the positive side of things. What if more positive news was spread? Perhaps then we would see that kindness, not cruelty, is at the heart of human interaction, and would act accordingly.

### 13. The power of change lies with individuals, regardless of their situation. 

Being able to maintain a positive attitude is vital, as is the ability to act on it and _persist_. Rather than simply talking about creating change, we've got to _just do it._

The Reverend Bill Crews runs a range of humanitarian projects in Sydney, Australia, from soup kitchens to homeless shelters and free health clinics, or even providing reading tutors for underprivileged schoolchildren. 

When the Dalai Lama visited, even he put on an apron over his monk's robes and joined Bill Crews while serving food. And that's what each and every one of us needs to do: get involved. It doesn't matter who you are, where you are or what means you have; we all have the potential to take action. 

The Dalai Lama firmly believes that the power to create change lies far more in the hands of individuals, than in the hands of organizations, governments or dictators. No matter how comprehensive a set of top-down changes may be, you simply can't force people to be compassionate. So don't wait for society to change — change yourself, and provide an example for others. 

So how can you start? It depends on you. As the Dalai Lama says: "Everyone can find a context where they make a difference. The human community is nothing but individuals combined."

### 14. Final summary 

The key message in this book: 

**In a world of rampant cruelty and suffering, it's time to make a change — and it all starts with you. The power for change lies with individuals and their ability to shift away from self-interest and negativity, and toward compassion and positive action.**

Actionable advice:

**Breath deeply to eliminate fear and anxiety.**

Next time you need to calm down, try this. Take a deep breath, filling your lungs; hold it in for two or three seconds and then let the air out slowly. Take five to ten deep breaths this way. If you need help focusing fully on your breathing, you can think of mental cues: "in" while inhaling, "out" while exhaling. Or, imagine tension draining from your body as you exhale.

**Suggested** **further** **reading:** ** _The Art of Happiness_** **by Dalai Lama**

_The Art of Happiness_ is based on interviews of His Holiness the Dalai Lama conducted by the psychiatrist Howard C. Cutler. The combination of Tibetan Buddhist spiritual tradition with Dr. Cutler's knowledge of Western therapeutic methods and scientific studies makes this a very accessible guide to everyday happiness. The book spent 97 weeks on the _New York Times_ bestseller list.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Daniel Goleman

Daniel Goleman is an author, psychologist and science journalist. For 12 years, he worked as a journalist for _The New York Times_, specializing in articles on psychology and brain sciences. Goleman is the author of several books on psychology, education and science.

