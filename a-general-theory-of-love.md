---
id: 5421426a37303300081a0000
slug: a-general-theory-of-love-en
published_date: 2014-09-25T00:15:00.000+00:00
author: Thomas Lewis, Fari Amini and Richard Lannon
title: A General Theory of Love
subtitle: None
main_color: C4AC64
text_color: 78693D
---

# A General Theory of Love

_None_

**Thomas Lewis, Fari Amini and Richard Lannon**

In _A General Theory of Love_, three psychiatrists take a scientific look at the phenomenon of love. Arguing that our emotional experience in adulthood is profoundly influenced by our childhood relationships, the authors suggest ways to undo this emotional "programming" and establish healthier relationships with friends and romantic partners.

---
### 1. What’s in it for me? Find out what science can teach us about why and how we love. 

Usually, when we talk about love, we don't talk about it scientifically. Human love is often seen as a mysterious and irreducible phenomenon that exists outside the cold realm of science, and as one that simply won't yield to its scientific understanding.

Because the subject of love has long been considered the dominion of poets and artists exclusively, we tend to think that scientists have little or nothing to contribute to our understanding of it.

Fortunately, three psychiatrists — Thomas Lewis, Fari Amini and Richard Lannon — don't believe in this unilateral distinction. In order to address the mystery of love as incisively and comprehensively as possible, they combine their scientific and practical expertise with the rich cultural heritage that artists, poets and philosophers have left for us over the centuries.

In the blinks, you'll discover:

  * how our brain evolved over time to enable us to feel attachment to others;

  * how our adult relationships are shaped by our childhood experiences;

  * how psychotherapy can help us rewire our brains, enabling us to develop healthy relationships;

  * why self-harm reduces severe emotional pain in certain people; and

  * why it's essential that we understand the difference between _loving_ and being _in love_.

### 2. The evolutionary history of the human brain can be seen in its three subsections. 

The demystification of human emotions by science is not a recent thing. As early as 450 BC, the Western world's first physician, Hippocrates, proposed that emotions — such as love — are a product of the brain.

Even though Hippocrates' hypothesis turned out to be correct, it took more than 2000 years before scientists began to closely examine the brain and the effect it has on human behavior.

Today, thanks to the scientific discoveries of the past few decades, our knowledge of the brain has far exceeded that which even the prescient Hippocrates could have predicted.

One such discovery is that of how the human brain evolved over the millennia.

In order to survive, our ancestors had to adapt to their changing environments. This included changes in their brains, which helped them to survive in new climates and conditions.

For example, our distant ancestors were forced by climate change to move from the forest habitat to the dry savannah planes. In order to survive in this harsh environment, their brains had to adapt to out-smart predators and find food. Gradually, step by step, adaptation by adaptation, our established brain structures were transformed.

What evidence do we have to support this theory?

The human brain's evolutionary history can be found in its three subsections **.**

The oldest of these, the _Reptilian Brain,_ sits at the top of the spinal cord and controls our most basic bodily functions and impulses.

Next is the _Limbic Brain_, situated around the reptilian brain. Here you can find such famous components as the _amygdala,_ which plays a major role in the production of fear.

The development of the limbic brain has been crucial for the evolution of mammals. In contrast to reptiles, it enables them to feel attachment towards their young. As a result, mammals — unlike reptiles — form close social groups, will protect children or mates and play with each other.

The newest and largest section of the human brain is the _Newest Brain_ — or Neocortex. The Neocortex is behind such qualities as reasoning, planning and speaking, and allows us, for example, to make decisions based on careful thinking rather than instinct.

As you'll see in the following blinks, this three-part brain schema helps us to understand why our behavior in relationships is often surprising.

### 3. Feelings of attachment are the product of neurotransmitters such as serotonin and oxytocin in the brain. 

Emotions such as love or attachment feel so powerful and wonderful that we often think they must be the product of something as equally profound and mysterious.

Unfortunately, this isn't true. Love and attachment are, like all feelings, the product of chemicals in the brain, called _neurotransmitters_.

There are three important neurotransmitters that influence our sense of attachment.

The first is _serotonin_ and its role includes relieving feelings of anxiety and depression.

In some people, serotonin can even reduce the traumatic effects of grief and heartbreak when they lose a person with whom they felt a close attachment.

And for people in unhappy relationships, who can't let go because they fear the feelings of loss, increasing their levels of serotonin — for example, by means of anti-depressant medications such as Prozac — can help them to finally make the break.

The second neurotransmitter responsible for attachment is _oxytocin_.

This chemical is present in high quantity during childbirth and is therefore responsible for the bond between mother and child. However, it also plays a role in the emotion of attachment throughout life too.

In a study of two species of prairie dog — the _vole_ and the _montane vole_ — Thomas Insel observed that, in adulthood, voles are monogamous: they mate for life and spend much of their day sitting side by side. In contrast, the _montane vole_, is a much less social creature: their display of attachment behavior is minimal, as they practice promiscuity and often abandon their young.

What could be behind such wildly different behavior?

Insel's major insight was that the answer could be found in the brains of the two species — and the only difference he found between them was the level of oxytocin.

In the next blink, we'll take a look at the third important chemical: _opiates_.

### 4. The neurotransmitters known as opiates help relieve physical and emotional pain. 

When a child touches a hot stove, he will most likely cry out in pain. Because the child won't want to experience such pain again, he'll probably think twice about going near a hot stove again.

The development of a neural system that senses injury was a crucial step in human evolution, because physical damage puts the body at risk of death.

Thus, the feeling of pain helps humans to stay away from anything that could potentially hurt them.

However, although the brain's ability to _produce_ pain is crucial to human survival, an equally important ability is that it can _reduce_ pain when it arises.

This is where the third main neurotransmitter involved in attachment comes into play: _opiates_.

Opiates can alleviate both physical and emotional pain: they help us to feel better when going through emotional trauma and the hardships that follow painful events such as the breakdown of a romantic relationship.

Why do these chemicals perform this dual function?

When the limbic brain was developing, mammals needed the ability to become attached to each other _and_ to deal with the pain that strikes when an attachment is lost.

Since the brain had already developed a system for experiencing and alleviating physical pain (using opiates), this simply was adapted to deal with _emotional_ pain.

Sounds nifty? Well, there's a dark side: this dual role of opiates might be responsible for why some people harm themselves physically when experiencing emotional hardship.

Most teens who cut themselves suffer from overwhelming emotional pain caused by traumatic social experiences.

When they cut their skin, pain signals are sent to their brain. The brain then releases opiates to numb the physical suffering and — as a side-effect — relieve the emotional pain at the same time. In this way, self-harm might help them to feel better.

### 5. Our memories and feelings depend on the connections in the brain that are called Attractors. 

Have you noticed that when you read a text, even several times, you often fail to spot small typos, such as "taht" instead of "that"?

Why does this happen?

This kind of common oversight is due to the presence of _Attractors_ in the human brain.

In neural terms, they are the interconnected elements of our memory that govern or influence our perception, directing what we learn and experience.

For example, many of us have poor handwriting: our letters are misshapen and joined together so that each word becomes a scrawl.

Yet, with very little effort, a reader can interpret even the worst handwriting correctly. For example, even if your handwritten "H" looks more like an "A," your readers will still read the word "aouse" as "house."

Why?

Engraved in our memories is an ideal or _prototype_ letter "H." Each time we see a letter that appears similar to this ideal, the prototype overpowers our perception, taking command of it, thus enabling us to quickly "autocorrect" any misshapen letters or typos. For example, if we come across the typo "taht," the Attractor responsible for the prototype "that" kicks in, enabling us to understand the writer's intended meaning.

What do Attractors have to do with attachment?

Attractors are established through our life experiences, and those experiences shape our memories.

From our earliest experiences, our brains construct Attractors which link our memories together. Whether one is linked to another depends on the connections we have built in our brain between them. For instance, if we have an ideal "H," that's because we've been taught what an "H" should look like.

The same is true of our limbic brain — the part of the brain responsible for our feelings. Our experiences enable us to develop the ideal feeling of attachment. Throughout our lives, this ideal will determine how we experience attachment and towards whom we feel it most intensely.

> _"No individual can think his way around his own Attractors, since they are embedded in the structure of thought."_

### 6. Attachment is necessary for our emotional development. 

In the previous blink, we saw that our emotional memory works as an ensemble of many different, interconnected elements called _Attractors_.

Every human being develops such networks during its lifetime and, as with almost everything in life, it's important to get a good start.

At the beginning of our emotional development, as newborn infants, the part of the brain responsible for emotions — the limbic brain — is completely unregulated.

Newborns don't come into the world knowing how to behave; they need their mothers to teach them. Through their mother's influence, babies build their emotional prototype — their ideal — which will then help to shape any further emotional experiences they will have.

Imagine, for example, a toddler staggering across the grass of a park. Suddenly, he loses his balance and falls. At this point, the toddler checks his mother's face: if she expresses alarm or concern, the toddler will start to cry. If, on the other hand, her face expresses amusement, the toddler may smile or even laugh with her.

The stability of this connection between parent and child is crucial for the youngster's development. Indeed, it's the basis of the emotional intelligence the child needs to empathize with others — in other words, gaining an intuitive understanding of the other's emotion and responding to it.

But this "limbic regulation" is not only for the young. Because we remain social creatures throughout our lives, adults also require emotional stabilization from outside.

While, as adults, we may think that our being so dependent on external feedback is a weakness, it's actually a source of our power. Our continued ability to connect with other people enables us to modify the emotional Attractors in our brains, allowing us to grow and change emotionally. Without this ability to continually influence our limbic brain into old age, we'd all behave like big kids!

All we require are stable, trusting and healthy connections with others, the kind that can be found in our close relationships, such as with our romantic partners or close friends.

> _"Stability means finding people who regulate you well and staying near them."_

### 7. Long-term therapy can change our brains for the better by optimizing how we connect emotionally. 

As we have seen, our feelings of attachment depend to a large degree on our childhood. The relationships we experienced during childhood shape our emotional brain, which strongly influences our adult relationships.

If the people (usually parents) who influenced the development of our limbic prototypes were, during our childhood, themselves not emotionally developed, nor _aware_ of their own emotional shortcomings, we'll inherit their emotional problems.

This is how our emotional programming gets passed down the generations. So, how can we break the pattern?

One way to deal with such unfavorable programming is psychotherapy.

To see why psychotherapy can be effective, let's take another look at Attractors.

Our Attractors determine our experience. Imagine you have to wear spectacles that allow you to only see green. Everything in your life will be, in one way or another, tinged by and limited to green.

In much the same way, the Attractors in your limbic system are shaping and limiting your emotions — an influence that determines your choice of friends and romantic partners.

Yet if our relationships with those close to us during our childhood aren't stable, we may end up with shoddy programming.

Fortunately, through psychotherapy, it is possible to reprogram our emotional brain. This is achieved by altering the network of Attractors in the brain.

Although psychotherapists often disagree vehemently on the most effective approach, disputing each other's theories and methods, the particular approach taken is of relatively little consequence.

What _does_ matter is that the therapist is able to modify the patient's network of Attractors — a process called _limbic revision_. When psychotherapy is successful, it's because it has assisted the patient in revising his or her limbic patterns — it has widened the color spectrum beyond green, so to speak — to the degree that the patient is able to begin choosing friends and partners who are better suited to him or her.

> _"Relatedness engenders a brand loyalty that beer companies would kill for: your own relationship style entices."_

### 8. The cultural myth that being in love and loving are the same thing leads inevitably to disappointment. 

The English poet Elizabeth Barrett Browning once wrote: "Whoso loves believes the impossible."

Though Browning captured the essence of what it means to be in love, let's try to be more specific:

When we're in love, we experience three related feelings.

First, we're so enraptured by the way our partner fits so perfectly with us that we believe we'll never fall for another person.

Of course, we _are_ able to fall in love more than just once in our lives. But what's important is the subjective feeling that this person is "the one."

Second, there's the strong desire to be physically close to that person. When this desire diminishes, we begin to doubt our love.

Finally, there's the irresistible urge to ignore anything that has nothing to do with our experience of being in love. In this way, the experience of being in love is basically a "rewriting" of reality.

As this suggests, there's a huge difference between being in love and loving.

Falling in love is crucial for bringing two people together romantically. But it's no more than a prelude to loving (which, as we'll see in the next blink, is all about long-term attachment). Therefore, it's inevitable that the period of being in love — the "honeymoon period" — will come to an end.

Nevertheless, because the feeling of being in love is so profound, it leads us to believe that the relationship and the feeling will last forever. So, when it does ultimately end we are extremely disappointed, saddened and even depressed.

Unfortunately, through an incessant stream of TV shows, romantic comedies, romance novels and so on, our culture perpetuates the misleading ideal that love is eternal.

Typically, such stories follow this schema: two characters, who know very little about each other, fall in love over a very short period, and — after overcoming some obstacle to their relationship — they _end up together_.

Because this story schema is so prevalent in our culture, we have come to believe that it's the _ideal_ version of love — which is why we are so disappointed and surprised when we realize (perhaps again and again) that love is fleeting.

> _"True relatedness has a chance to blossom only with the waning of its intoxicating predecessor."_

### 9. The brain structures of two loving people are changed so that they begin to sense the world in the same way. 

As we saw in the previous blink, when we take the feeling of being in love as the essence of loving itself, we're inevitably disappointed.

But what is at the heart of the difference between being in love and loving?

The fundamental difference is in the emotional connection.

While we can be _in love_ with a person who is not in love with us, _loving_ is always mutual. Each person attunes him- or herself to the other, and modulates their personality and behavior to fit the other.

Moreover, adult love means _knowing_ each other deeply. By contrast, all that's required to fall in love is that you're acquaintances who've known each other for a short time.

Therefore, loving needs time and arises from long-term intimacy, as the loving persons have to become accustomed to the details of each other's soul.

Through the creation of such profound love relationships, over time both people become "limbically attuned" to each other.

Why?

The reason is simple: as we have seen, good psychotherapists enable the patient to revise established structures in their limbic brain. And for that, patient and therapist must establish a _limbic connection_.

Yet, between people who love each other, such a connection is already in place.

Indeed, both partners are in a constant limbic exchange. The networks between their respective Attractors are transformed and a shared way of sensing the world emerges, manifest in the brain structures of the loving persons.

From this literal transformation of the Attractors comes the expression that people often say when they lose a partner: "A part of me is gone."

> _"Who we are and who we become depends, in part, on whom we love."_

### 10. Final summary 

The key message in this book:

**How we experience the world emotionally is a kind of neural programming that we learn during childhood. If we want to change this programming, we need to nurture profound empathic relations with others — not only with friends and romantic partners, but also psychotherapists.**

Actionable advice:

**Learn to separate** ** _loving_** **from** ** _being in love._**

If you find that you're consistently surprised when a romantic relationship ends, it's time for some serious reflection. For the longest time, through movies, novels, TV shows, etc., Western culture has taught us to confuse the feeling of being _in love_ with loving. Since this leads to disappointment when the "honeymoon period" comes to an end, it's important to reflect on the kind of "story" you're telling yourself about a particular relationship. If you notice that your story is about eternal love, you have to remind yourself that romantic love is fleeting.

**Suggested** **further** **reading:** ** _Why We Love_** **by Helen Fisher**

Helen Fisher's _Why We Love_ is not only a report on her latest astonishing research but a sensitive description of the infinite facets of romantic love. This book is a scientifically grounded examination of love that reveals how, why and who we love.
---

### Thomas Lewis, Fari Amini and Richard Lannon

The book is written by three psychiatrists: Thomas Lewis, Fari Amini and Richard Lannon, who are professors at the University of California, San Francisco, School of Medicine (UCSF). Lewis is an assistant clinical professor of psychiatry; Amini, a professor of psychiatry; and Lannon, an associate clinical professor of psychiatry.

