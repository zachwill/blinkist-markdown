---
id: 5612504bfd22820009000002
slug: a-life-decoded-en
published_date: 2015-10-05T00:00:00.000+00:00
author: J. Craig Venter
title: A Life Decoded
subtitle: My Genome: My Life
main_color: FFDD33
text_color: 806F19
---

# A Life Decoded

_My Genome: My Life_

**J. Craig Venter**

_A Life Decoded_ (2007) is the autobiography of the prominent American biochemist and geneticist Craig Venter, who played a key role in one of the greatest scientific achievements of our time — the deciphering of the human genetic code. These blinks describe the personal experiences that drove his scientific research, even at times when his methods were attacked by the scientific community.

---
### 1. What’s in it for me? Learn about the life and achievements of one of this century’s greatest scientists. 

One of the biggest scientific achievements of the modern era must be that of free-spirited American J. Craig Venter: the deciphering of the human genome. But what makes a person take on such an immense task? How did he grow up to become so relentlessly ambitious, and what fueled his ongoing motivation right up to the amazing breakthrough and beyond?

These blinks tell the tale of how Venter's life unfolded — from a childhood full of playful creativity, to his rebellious attitude and meteoric rise within the scientific community.

In these blinks, you'll learn

  * how a shark attack saved Venter's life;

  * why identifying human genes quickly made him a center of controversy; and

  * which amazing aquatic task Venter took on after deciphering the human genome.

### 2. During his childhood, Venter discovered that he loved taking risks, facing challenges and constructing. 

Among J. Craig Venter's most vivid memories from his early childhood is his freedom. Born in 1946, Venter grew up in Millbrae, a small town 15 miles south of San Francisco, California. During his childhood, his parents would often just tell him to "go play." With this liberty, Venter first discovered three lasting personal traits.

As a young boy, Venter loved taking risks and facing challenges. One of his favorite destinations was the local airport, San Francisco International Airport, where he would watch the planes taking off and landing. In the '50s, the airport was very different from how it is today: there was no security, no cameras and no wired fences.

Together with his friends, Venter came up with the idea to race the airplanes on the runway. They'd wait until a plane started to taxi in preparation for its ascent, and then they would jump onto their bikes to race it for as long and as far as they could.

Pilots occasionally notified the control tower, which then dispatched the airport police. But because the runway was such a long way from the terminal, the boys could always escape easily. 

Another key trait that became apparent early on was Venter's insatiable urge to build things.

Venter's efforts at construction were modest at the beginning, mostly small but elaborate tunnels and forts. School curriculums were never among Venter's interests or strengths — he wanted to do something practical. So, in the seventh grade he built an electronic scoreboard for the junior high school baseball field.

In his woodworking class, instead of making furniture, he built a complex hydroplane, a motorboat based on a new design that had set a speed record of 60 mph. The young Venter would do anything to get his hands on the material he needed to build things from scratch.

> _"I think that one reason I was able to become a successful scientist was that my natural curiosity was not driven out of me by the education system."_

### 3. Being a hospital corpsman in the Vietnam War gave Venter medical training – and changed his understanding of life itself. 

In 1967, at the age of 20, Venter could not avoid being sent to Vietnam. But because an IQ test revealed a respectable score of 142, he was given the option to go to hospital corps school. This allowed Venter to avoid combat missions and serve as a corpsman, an enlisted member of the military medical unit, instead.

Practicing medicine at the Navy hospital in Da Nang, Vietnam, gave Venter a number of medical skills. Venter was a senior corpsman in the intensive care ward where he looked after victims of mines, bullets and napalm. 

Later, he joined the dermatology and infectious disease clinic, where he dealt with a range of disorders, from malaria to tumors and venereal diseases — the latter almost an epidemic among soldiers in Vietnam due to widespread prostitution.

Venter also treated children at a local orphanage in Da Nang. He dealt with everything from pregnancies to insect infestations, major wounds to broken bones. His experiences in the Vietnam War taught him a lot about just how vulnerable humans are.

Venter had seen several hundred soldiers die, often while he was massaging their hearts or attempting to breathe life into them. Badly wounded patients who survived showed him the impact of spirit and willpower on one's survival. This difficult job eventually took its toll on Venter; after five months in Vietnam, he was determined to take his own life.

He tried to escape the horror by swimming away in the ocean, planning to become so exhausted that he'd drown. But instead, he was attacked by a shark, and ended up swimming back to shore.

Vietnam was the starting point for Venter's scientific drive to understand life. Having experienced the fragility of life, Venter wanted to understand the very essence of it.

> _"I was not studying at the university of life but of death, and death is a powerful teacher."_

### 4. Venter embarked on a meteoric scientific career at the University of California. 

After having seen the darkest depths of mind, body and soul in Vietnam, Venter put all his efforts into his own education. Given his lackluster academic record, Venter returned to school in early 1969 and enrolled in classes at the college of San Mateo. 

His hard work at school paid off: he received straight As in all three semesters and was accepted to the University of California, San Diego, to study biochemistry. Venter soon made his start in research, publishing his findings in scientific journals.

Due to his excellent academic performance and his unusual medical background, Venter caught the attention of the distinguished biochemist Nathan O. Kaplan, a leading enzymologist. Kaplan encouraged Venter to come up with an idea for a research project.

Venter wanted to study the "fight or flight" response caused by adrenaline. One theory was that adrenaline worked inside human cells; Venter, however, was able to prove that it actually worked on the cell surface. While still an undergraduate student, Venter submitted a paper on his findings in the prestigious journal _Proceedings of the National Academy of Science_. 

In his two years of graduate study, Venter completed and published eleven more papers in quality journals — much more than most doctoral students manage in five years. In 1975, Venter earned a PhD in biochemistry. It was during this time that he was also introduced to the scientific community. 

During his university career, Venter was astonished by the high caliber of the scientists with whom he was working. Whenever Kaplan's famous friends visited the university, he would host a party and invite Venter to meet great names in science such as Carl and Gerty Cori (Nobel Prize winners in 1947), Ephraim Katzir (a biochemist and former president of Israel) or William McElroy, the university chancellor.

### 5. At the National Institutes of Health (NIH), Venter first encountered the idea of decoding the entire human genome. 

To launch the next phase of his career, Venter chose the State University of New York in Buffalo, where he was offered a postdoctoral junior faculty position. But when he was refused a permanent position, Venter decided to leave Buffalo.

In 1983, he was offered the chance to continue his research at the National Institutes of Health (NIH), the powerhouse of medical research in America. This offer would come to change Venter's life and the course of his scientific research.

At the NIH, Venter moved into molecular biology and entered the emerging field of _genomics_ — the branch of molecular biology that looks at genes. It was there that Venter found himself in scientific heaven. 

He had been given hundreds of thousands of dollars to set up and equip his laboratory and had an annual budget of over $1 million. The NIH campus was also home to hundreds of the nation's finest researchers, with whom he could collaborate.

At the NIH, Venter wrote the first molecular biology paper of his career: he and his team had found a gene responsible for the recognition of adrenaline. With his entry into genomics, Venter also came across the idea of decoding the entire human genome.

Genes are regions of the DNA, which itself stores all biological information. Decoding or sequencing a gene means determining the precise order of the chemical building blocks from which the gene is built.

Venter had spent about a decade decoding the one single adrenaline receptor gene of what was at the time estimated to be 100,000 human genes. It's therefore easy to imagine that the idea of decoding the entire human genome was considered impossible by most scientists at the time. 

But Venter was immediately drawn to the thought of having a database showing the sequence of every human gene.

### 6. Venter became a controversial scientist for attempting to patent genes and for continuing his research at a private institute. 

Venter had become fascinated by the idea of unraveling the secrets of the human genome — the entire genetic material of the human organism. But when he made a breakthrough that he thought could change genomic science, he suddenly found himself at the center of a heated controversy.

At the NIH, Venter developed a technique to rapidly identify human genes. This technique detects so called Expressed Sequence Tags, or ESTs, which are short fragments of DNA that can be used to draw inferences about genes. Venter became a controversial scientist when he attempted to patent newly identified genes. 

Venter was convinced that his approach was of incredible value for gene discovery and the understanding of the human genome. Together with the NIH, Venter decided to attempt to patent the genes he identified on basis of the ESTs.

This attempt unleashed a great wave of criticism, instantly making Venter a divisive figure in the scientific community. His critics complained that Venter was attempting a wholesale patenting of human genes, which, they believed, shouldn't be patented at all. 

The _New York Times_ quoted one researcher as saying that Venter's approach was a "quick and dirty land grab." When Venter felt too little support for his genomic research efforts, he left the NIH and continued his research at a private institute. 

Venter left the NIH in 1992. He wanted to scale up genomics to realize the potential of the EST method without being held back by government bureaucracy at the NIH. He then received an offer from Human Genome Sciences (HGS), a company that was willing to fund his research and market his discoveries. This allowed him to set up his own research institute, which he called The Institute for Genomic Research (TIGR).

However, other researchers began to fear the possibility that the human genome could be locked up and owned by private investors.

> _"Like most human endeavors, science is driven in no small part by envy."_

### 7. Venter was the first to decode the genome of a living species and one of the first to decode the human genome. 

On his way to deciphering of the human genome, Venter had a noteworthy success: he was the first to decode the genome of a living species.

Venter had developed a new method to decode a genome. He called this method "shotgun sequencing," since it worked by fragmenting a genome into thousands of fragments of DNA that could then be analyzed more easily.

But would this method work when attempting to decode something as complex as the whole human genome? To find out, Venter needed a testing ground. He chose to decode the genome of the bacterium _H.influenzae_ to see whether the shotgun sequencing method was appropriate. 

It was: in 1995, Venter successfully decoded the entire _H.influenzae_ genome. It was a historic moment. Venter had become the first to decipher the entire genetic code of a living species — but he didn't stop there.

The deciphering of the _H.influenzae_ genome was a great success, but Human Genome Sciences (HGS), which was expected to fund Venter's research, became increasingly unwilling to give financial support.

Conflicts about Venter's right to publish his findings arose since HGS wanted to capitalize them. Even before the deciphering of the human genome could get started, Venter terminated the relationship with HGS.

Venter, however, got another chance to decode the human genome. He was approached with an offer to build Celera, a new subsidiary of the American biotech company PerkinElmer. Celera would aim to provide a high-quality database of the complete human genome to aid scientific discovery.

But Celera would eventually come to compete with the publicly-financed Human Genome Project (HGP) in its effort to decode the human genome. In 2000, Celera and the HGP jointly announced to have successfully decoded the entire human genome, with Venter at the very center of this amazing achievement.

### 8. Venter publicly announced the deciphering of the human genome at the White House – and recognition for his discoveries came fast. 

June 26, 2000 was probably the most important day in Venter's life. At the White House, he met president Bill Clinton and told the world that long years of scientific research efforts had come to a successful end.

On that fateful day, Venter and Francis Collins of the Human Genome Project (HGP) jointly announced the deciphering of the entire human genome at the White House. Parallel to Venter's research efforts at Celera, the HGP had also aimed to decode the human genome.

Many of the HGP's researchers, however, didn't believe that Venter's technique of shotgun sequencing was really appropriate for the human genome.

But the race was declared to be over when Venter and Collins were together invited to the White House to announce their historic scientific achievement, alongside US President Bill Clinton and UK Prime Minister Tony Blair.

Although Venter and his team had always considered themselves to be ahead of the HGP, they felt relieved that the race was over. The audience at the White House in June 2000 was made up of prominent politicians as well as the leading contributors within the genome community.

Recognition for the scientific success came thick and fast. Venter went to Saudi Arabia to receive the King Faisal International Prize for Science, and to Vienna to receive the World Health Award from former Soviet president Mikhail Gorbachev.

Venter received a multitude of honorary degrees from top universities around the world and was given the Paul Ehrlich and Ludwig Darmstaedter Prize. From Japan came the Takeda award, and from Canada the Gairdner Award, both notable prizes in science.

So what comes after such a great success? In the final blink, find out where Venter found a new scientific purpose.

> _"There is still an ocean of great science left for me to explore."_

### 9. Venter now devotes his scientific research to reading the ocean’s genome and to the creation of synthetic biological organisms. 

After deciphering the human genome, Venter found himself in search of a new purpose. He turned his attention to a project that combined two great loves of his life: science and sailing. Venter now devotes his scientific efforts to reading the ocean's genome. 

Knowing that the billions of tons of carbon dioxide that we are pumping into the atmosphere each year are altering global climate patterns, it was clear to Venter that modern life will eventually become unsustainable. But he wanted to do more than just use less oil and gas, or install a solar panel. He felt that genomics had something unique to offer.

Venter figured that he needed to work out exactly what is in the oceans to accurately assess the effects of climate change, such as ocean acidification. So he embarked on a project to take seawater and analyze the genetic material of all the microorganisms swimming in it.

The idea was to obtain a snapshot of the microbial diversity in a single drop of seawater. Venter has since discovered tens of thousands of new species, many of them strange and exotic. In all, he has discovered more than 1.3 million new genes in only 200 liters of surface seawater.

Furthermore, Venter works on the development of synthetic biological organisms. Could we design new organisms to live in the emission-control system of a coal-fired plant to soak up its carbon dioxide? Or could we harness microbes and their biochemistry to alter the atmosphere?

Venter currently works on the creation of synthetic organisms that could help clean up our polluted environment. In 2006, Venter founded the J. Craig Venter Institute, one of the largest private research institutes in the world with over five hundred scientists and staff and an annual budget of over $70 million.

> _"[...] I want to discover whether a life decoded is truly a life understood."_

### 10. Final summary 

The key message in this book:

**Already as a child, J. Craig Venter loved facing challenges. After being confronted with the fragility of human life in the Vietnam War, Venter devoted his scientific efforts to one of the greatest challenges that science had to offer: deciphering the human genome, and thus understanding of the very essence of life. In 2000, Venter was among the first group of scientists to decode the entire human genome.**

Actionable advice:

**Let curiosity lead you.**

J. Craig Venter's life is a perfect example of how great achievements can stem from nothing more than natural curiosity. As a child, Venter was curious as to whether he could race airplanes or whether he could build a functioning hydroplane. His scientific career was always driven by his curiosity to understand life in its every detail. So, keep your natural curiosity alive and try to follow it. Don't be discouraged by systems or people that try to keep your thinking inside the box!

**Suggested** **further** **reading:** ** _Genome_** **by Matt Ridley**

_Genome_ (2006, second edition) takes you on an exciting journey into your own body, exploring the genetic building blocks that make up not only who you are but also all life on earth. You'll examine the basics of genetics and discover what genes influence, from aging to illness to even your own personality. Importantly, you'll better understand why the future of healthcare and wellness may be found in the human genome.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Cover image: © Wikipedia: Calliopejen**
---

### J. Craig Venter

J. Craig Venter is a biochemist and geneticist, and is considered one of the leading scientists of the twenty-first century. In 2001, Venter published the complete sequence of the human genome. He is the founder of Celera Genomics, The Institute for Genomic Research (TIGR) and the J. Craig Venter Institute (JCVI). Venter is also the author of the book _Life at the Speed of Light_.

