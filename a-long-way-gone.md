---
id: 565c83b99906750007000076
slug: a-long-way-gone-en
published_date: 2015-12-04T00:00:00.000+00:00
author: Ishmael Beah
title: A Long Way Gone
subtitle: Memoirs of a Boy Soldier
main_color: 899C31
text_color: 5C6921
---

# A Long Way Gone

_Memoirs of a Boy Soldier_

**Ishmael Beah**

_A Long Way Gone_ (2007) is a story of how, as a young boy in Sierra Leone, the author found himself caught in a civil war and recruited as a child soldier. You'll travel alongside during his harrowing journey, eventual rescue and recovery guided through the kindness and grace of loving people.

---
### 1. What’s in it for me? Get the personal story of a boy deeply embroiled in Sierra Leone’s civil war. 

In the early 1990s, a civil war in Liberia inspired rebels in neighboring Sierra Leone to take up arms and fight against their government. The war raged for more than ten years, and thousands of people across the country were killed. 

As horrible as all civil wars are, the conflict in Sierra Leone was characterized by a particularly horrendous fact: young boys, some as young as seven and barely able to hold a gun, were recruited to fight. 

The author was one of those boy soldiers. Early in the conflict he was separated from his family and had to fend for himself, and in the process, was recruited by the national army, drugged and forced to perpetrate atrocities and live through horrors no person should ever have to experience — let alone a child.

These blinks tell the author's personal story of loss and redemption amid the horrors of civil war. 

In these blinks, you'll discover

  * how the national army used propaganda to keep boys bloodthirsty;

  * why peacetime was hardly peaceful for rescued boy soldiers; and 

  * how love and kindness can heal even the deepest scars of war.

### 2. The civil war in Sierra Leone seemed to come out of nowhere, turning life upside-down. 

From the perspective of the Western world, Africa may seem synonymous with civil strife and war. But this is just a generalization; African nations desire peace just as much as any other country. Even Sierra Leone enjoyed years of peace before civil war erupted in the 1990s. 

As a child, the author recalls happy memories, such as watching his mother prepare meals, or joining friends to dance to hip-hop music at talent shows. All in all, his childhood was not unlike that of many other children across the world. 

In fact, any talk of war usually came from movies and books, or the occasional BBC report on the ongoing war in Liberia, a neighboring country. So when war did break out, it came as a surprise. 

One day in January 1993, the author, his older brother Junior and a friend named Talloi traveled to the town of Mattru Jong, a 16-mile journey from their hometown of Mogbwemo. The boys had come to Mattru Jong for a talent show, and planned to stay at a friend's house. 

When their friend met them, however, he brought bad news: the author's home of Mogbwemo had just been attacked by a rebel group. 

The author and his brother decided to return home to search for their parents and siblings, but on the way their minds were quickly changed. They saw hordes of people fleeing Mogbwemo, many wounded, every person carrying what they could with them. 

And when they saw the first dead body, it became clear to the boys that their family must have also fled Mogbwemo. Frightened, they turned back toward safety and the village of Mattru Jong. 

The change from living a peaceful life with family one day to having no idea  where your parents are the next was a horror in and of itself, but the war brought horrors much worse.

Next, you'll learn what war means for the most vulnerable population of any country — its children.

### 3. Children suffered the most during the civil war, having to fend for themselves alone or in groups. 

The author's experience was unfortunately not unique. While people all over Sierra Leone suffered, children were hit the hardest by the chaos and violence of the civil war. 

Since the war began so suddenly, it was common for children to be separated from their families during attacks in towns and villages and left to fend for themselves. There were also few ways to reach separated family members, as cellphone networks weren't yet available in the early 1990s. 

But more importantly, the citizens of Sierra Leone just weren't prepared for war and didn't have safe places to go when rebels attacked. The author was involved in the simple, everyday task of cooking a meal when the war arrived at his front door. He was staying at his friend's house in Mattru Jong when rebels entered the town, shooting at civilians in the streets. 

So what did children do when they suddenly found themselves on their own?

Many children had to roam from village to village in search of food, despite the danger of rebel bands. While war is certainly devastating for adults, imagine trying to get by as a child who, in an instant, is handed all of life's adult responsibilities. The only chance the author had for survival was to join together with other lone boys like himself. 

During a surprise attack, the author was separated from his brother and was forced to flee to save his own life. After some time, he met six other boys with whom he began traveling, in search of food and safety. 

But the author would soon learn that banding together with a group of boys would not keep him safe from the ravages of war.

### 4. Young boys in Sierra Leone had few safe places, hunted by rebel groups and attacked by civilians. 

While the horrors of war were especially difficult for children, young boys had a particularly hard time of it. Not only did rebel groups target them, but also civilians saw them as a threat. 

But how could village populations see harmless local boys as dangerous?

Early in the war, rumors circulated about the rebels' use of boy soldiers, and this terrified the local population of Sierra Leone. For instance, people would talk about how rebels forced boys to fight — even to kill their own families and friends. 

One brutal tactic the rebels used to coerce boys into fighting for them was to carve the group's initials, RUF — for Revolutionary United Front — into the bodies of children with a hot bayonet. 

Once the boys were branded in this fashion, they were at the mercy of the rebel group. Sierra Leone soldiers and civilians alike would kill anyone bearing the RUF brand without question. 

Civilians, after hearing stories about boy soldiers, viewed any young man suspiciously. The author and his six companions soon realized that they weren't welcome anywhere when a group of civilians captured and tortured them. 

The boys found themselves in a village they thought abandoned. All of a sudden, a group of villagers surrounded the boys and attacked them, taking their shoes and chasing them over hot sand. 

The boys' feet were seriously blistered and burnt, and their wounds took days to heal. The author was just 12 years old at the time; others were even younger. 

While villagers were suspicious of and sometimes violent toward boys, this paled in comparison to the cruelty of the Sierra Leone national army. Read on to take a few steps in the shoes of a child soldier.

### 5. Even the Sierra Leone national army forced boys to fight in the country’s long civil war. 

You can just imagine the terrible things that rebels forced child soldiers to do. Yet even the national army fighting the rebels engaged in cruel tactics. In fact, if the army demanded it of them, young boys had no choice but to fight. 

After some time on the road, the author and his friends were captured by the army, which took them to a military base in Yele. The early days there were peaceful, as the boys enjoyed some of the comforts of a normal life, like regular food and rest. 

But that all changed as rebel groups drew closer to Yele and national soldiers started being killed in great numbers. At this point commanding officer Lieutenant Jebati called an assembly and announced that the army needed new soldiers and that the boys should enlist.

While the lieutenant initially said that anyone who didn't want to fight could leave, the boys felt their hands were tied. They longed for calm and safety, remembering the fear and hunger of life on the run, and yet felt that they had to fight.

The following day, however, the lieutenant's offer was proven to be less than sincere. He showed the corpses of two people, a man and a boy, to the assembled group, and said that while he had given permission for the two to leave, rebels had ambushed them outside Yele. 

And what happened to the children who did join the national army?

Every boy was treated like a grown soldier. There were 30 boys in Yele, ranging in age from seven to 17. Each boy was provided with weapons and strenuous military training. Although the youngest could barely hold a weapon, he was given a stool to prop it up during exercises. 

Once the drills stopped, the real thing began. In the author's first battle, he was too disoriented to even fire his weapon. This quickly changed, however, when he saw his friends being butchered all around him.

### 6. Jacked up on drugs and army propaganda, boy soldiers were made into efficient killing machines. 

It's difficult to imagine a 13-year-old boy doing the work of a soldier. That is, until you hear how the army brainwashed young recruits into becoming efficient killing machines. 

The first tactic was to distort a boy's reality by pumping him full of drugs.

Before the author's first battle, army leaders gave the boys white capsules, which the author recalls gave him a burst of energy. From then on taking drugs was just a part of the boys' everyday reality — a method to turn the boys into youth death squads, incapable of feeling pain or compassion. 

The army also gave the boys marijuana, but the most common drug was called _brown brown_, a mixture of cocaine and gunpowder. 

But drugs weren't the only method used to transform children into soldiers. The army also inundated them with propaganda. 

On their first day as official soldiers, Lieutenant Jebati gave a speech, telling the boys that it was the rebels who were responsible for all their troubles — that rebels had burned the boys' villages and had murdered their families. 

In drills, the boys were made to picture the enemy who had inflicted all of this pain upon them, in effect fostering a blind hatred and desire for cruelty under the guise of avenging their loved ones. 

This type of propagandizing was standard procedure for the army until it was eventually unnecessary, as the boys felt and believed every bit of it deep inside themselves. 

In fact, once the war was over and groups came together under UN guidance, some boy soldiers shared stories with their rebel counterparts, who said they had been told the same propaganda lies.

So what happens when a boy soldier is liberated from his role in a brutal war? Next, we'll explore the challenges of recovery for young fighters in Sierra Leone.

### 7. Boy soldiers who survived the war were deeply traumatized by what they did and lived through. 

Any war veteran can tell you that returning home from war doesn't mean the war is over. Whether combatant or civilian, the survivors of way carry the experience with them. 

Even after hostilities ended, many child soldiers acted as though they were still at war. In 1996, when UNICEF aid workers arrived at the village in which the author was stationed, he had already spent years living the life of a hardened soldier.

On that day, the commanding officer gathered 15 boys — among them the author — and told them that they didn't have to fight anymore. They were going to leave with the UNICEF workers, who would help them start a new life. 

The author and hundreds of other boy soldiers were taken to a rehabilitation facility called Benin Home near the capital Freetown. It was here that the boys' long transition began. It took months for the drugs that the boys had been given daily to leave their systems; and even once they were clean, they were all still traumatized by war. 

The boys would fight with each other without provocation, attack the staff and set fire to school supplies. They refused to take orders from the civilian help. 

In short, they were not yet prepared to live in peace. 

The author's memories of the war tortured him. Even though a clean bed and regular meals were nice, such amenities didn't do much to treat trauma. Thus the author was still living with the haunting memories of the atrocities he and others had committed.

For instance, one day the author overheard a nurse speaking to a junior lieutenant. The mention of this military rank caused the author to remember the duties he had been tasked with as a junior lieutenant — murdering entire villages and burning rebels alive. 

While doing so seemed normal when he was drugged and surrounded by soldiers doing the same, the author was now sobered and struggling with the reality of his brutal past.

### 8. The kindness of caregivers and family helped the author turn his trauma into something productive. 

The future certainly seemed dark for the former boy soldiers who, while free from war, still acted like brutal monsters. But there was hope for them nonetheless. 

In fact, the gentleness and kindness with which the author was treated helped him recover from the horrors he had perpetrated and lived through. 

Initially, boy soldiers abused Benin Home staff both verbally and physically. But the staff never lashed out or gave up. When a boy would fight a staff member, that person would always return the abuse with kindness, saying the boy's bad behavior wasn't his fault. 

The author's friendship with a nurse named Esther played a major role in his healing process. Esther convinced him to get a medical examination, bought him a Walkman and some music tapes and also listened as he recounted his memories. In short, she was kind to him, and it made all the difference. 

But Esther wasn't the only person who helped the author heal. His uncle Tommy also aided him by showering him with love. Most importantly, Tommy eventually took him into his own home, letting him return to the life of a normal boy. 

As a result, the author was eventually able to leave the war behind and transform his experience into something positive. 

After the author performed in a talent show organized by Benin Home for the United Nations, the European Union and non-governmental organizations, he was asked to be the spokesperson for Benin Home. 

Later on, he traveled to New York to attend the United Nations First International Children's Parliament, along with other children from across the globe. There he spoke about the trying experiences of children in Sierra Leone. 

And through his book and story, the author was able to educate thousands of people about the struggles of Sierra Leone and its people.

### 9. Final summary 

The key message in this book:

**The civil war in Sierra Leone came on suddenly, brutally tearing families apart. While thousands of people were killed, the war especially traumatized the lives of young boys. Boys no older than seven were forced into both the national army and rebel groups as active soldiers, experiencing traumas that only the deepest kindness and care could eventually heal.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ishmael Beah

Former child soldier Ishmael Beah was born in Sierra Leone and is now an author and human rights activist living in New York.

