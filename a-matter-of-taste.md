---
id: 53b4161e3262310007230000
slug: a-matter-of-taste-en
published_date: 2014-07-01T00:00:00.000+00:00
author: Stanley Lieberson
title: A Matter of Taste
subtitle: How Names, Fashions, and Culture Change
main_color: A69642
text_color: 73682E
---

# A Matter of Taste

_How Names, Fashions, and Culture Change_

**Stanley Lieberson**

_A_ _Matter_ _of_ _Taste_ examines how and why fashions and tastes in things like baby names change over time.

---
### 1. What’s in it for me? Find out why and how fashions change over time. 

We live in an individualistic time: most people want to express their personality and strive towards inner fulfillment to suit their individual needs.

And while we're all still certainly unique, what we personally like or dislike can never be seen out of context of society as a whole. But if it's not our individuality, what is it that brings about changes in fashion? Why do different generations keep choosing different names for their children?

These blinks address all those questions, and you'll also find out:

  * why you aren't the master of your sense of taste,

  * why Donald Duck is relevant for a name fashion,

  * why Mexican-Americans wouldn't name their son Joshua and

  * why freak shows aren't in vogue anymore.

### 2. Different from customs, fashion is regularly undergoing change for novelty’s sake. 

When we hear the word _fashion_, the first thing that springs to mind are catwalks and designer labels. But the term describes far more than that. Fashion is a phenomenon that can be found in all aspects of society.

Changes in fashion usually happen for novelty's sake, not to enhance or alter an object or concept. In other words, a fashion is something new for the sake of its novelty as well as aesthetic reasons: last season's sweater still fits and keeps you warm, but you'll buy a new one anyway simply because it's the latest "thing."

However, we don't only buy new things because of fashion: we'll also buy something because it's objectively enhanced, e.g., a smartphone with a longer battery life. This doesn't qualify as a fashion since the change hasn't occurred purely for novelty's sake.

It's also important to distinguish fashions from _customs_ — though customs can develop into fashions.

Different cultures may have distinctive habits of dressing, furnishing, etc. that aren't "purely utilitarian," but as long as these habits don't change regularly, they're _customs_ and not a _fashion_. For example, the kimono might be a distinctive clothing style that doesn't serve a direct purpose, but for a long period of time it was the traditional dress in Japan, hence it is a custom rather than a fashion.

But if at some point the kimono makes a comeback among Japanese designers, it would be considered a fashion. That's because dressing habits first become a matter of fashion when there's a regular change in styles for the sake of novelty.

Finally, fashion also has an influence on non-material things. Fashions change over time and an actress's hairdo or a piece of music from the early 20th century no longer have the same impact on an audience today. Even Beethoven's most famous symphony that enthused a whole generation isn't played in every concert house today.

### 3. The development of fashions is determined by external and internal social influences. 

Most of us think our taste is an expression of our own individuality. If that were true, why is there such a thing as fashion, where big groups of people change what they like collectively, whether it's furniture, clothing or entertainment?

It's because, in the end, our individual choices always happen in the greater framework of social influences. 

There are many external influences that impact the development of fashions, from broad political influences, like the democratization of a society, to technological influences, like the invention of the sound recording or color TV. These influences are external in that they happen regardless of the fashion they might bring about.

For example, until the first half of the last century, freak shows were a normal form of entertainment. In many Western countries, "attractions" such as quintuplets or "savages" brought in from Africa were displayed in zoo-like conditions for the visitors' amusement. Thanks to improved human rights, a fashion like this is unfathomable today.

This transitions us nicely into the fact that fashions can also be affected by internal social influences.

_Class_ _imitation_ is an example of an internal influence where fashions change even when external influences stay the same. Often, the upper class of a society wants to set itself apart from others through a certain taste. After a while, other classes imitate that taste, which again brings the upper class to develop new tastes — just for the sake of standing out again.

For example, middle- and upper-class citizens used the "Jr." suffix in the nineteenth and early twentieth century to mark their high status. Then, later in the twentieth century, the working class started adding Jr. to its children's names, leading the upper class to use it less frequently.

### 4. First names are great for studying fashion developments because everyone can do it. 

Have you ever looked at old pictures of your parents and been amused by their huge glasses or petticoats? How could those things ever have been in fashion? Fashions and tastes are hard to understand, but we can get a better grasp of them by studying first names.

There are a couple of good reasons why.

Firstly, names aren't subject to intentional promotion. In general, there aren't any organizational influences actively promoting names like there are with clothes or cars.

When a company wants to sell a certain kind of car, for instance, it actively advertises that style as chic and fashionable. When studying changing tastes in cars, this can skew our conclusions — because we don't know how fashion patterns would look without promoters there to influence them.

Secondly, there aren't any social restrictions involved in choosing a name. Think about it: studying fashion means you're not only studying what people like, but what they can afford. And everybody, no matter how rich or poor, has the ability to choose a name.

For example, when only a few people buy golden cutlery, that doesn't automatically tell us that society thinks it's tasteful. The truth is, most people simply can't afford it, which makes it a less-than-optimal object to study if you want to obtain a clear picture of the general changes of fashion.

Finally, all in all, choices of names are relatively free. And the more name-giving traditions (e.g., in families and religions) decline, the more name-giving becomes a matter of pure taste. And even parents adhering to religious rules when naming their children usually still have a great range of names to choose from.

> _"The development of fashion in names, such that they are increasingly given to children because of aesthetic measures, is a relatively recent development."_

### 5. Names became a matter of fashion rather than custom when societies entered the modern era. 

If you've ever had children, you're sure to remember the debate surrounding what their names should be. While this process is typical part of the journey of becoming a parent today, it used to be a far simpler matter in former times.

In general, family and religion used to play a bigger role in people's lives, and the customs involved in both of those things influenced name-giving. In many cultures, it was common to name children after certain family members or ancestors. In traditional Greek villages, the first-born male usually received the name of the paternal grandfather.

But now, in modern times, as education and urbanization have become more widespread, the spectrum of names to choose from has widened accordingly.

Education brings people in contact with names used outside their society and fosters a break with traditions and religion. And thanks to the number of people who have moved out of smaller communities and away from their families into large cities, name-giving customs have also been on the decline. On top of that, the potpourri of people and cultures in cities has also exposed people to different tastes and a far greater variety of names.

That's why the change rate among the most popular names started to increase rapidly at the beginning of the twentieth century: names became a matter of fashion.

In the 1980s in France, for example, an average of 2.5 of the 10 most popular girls names dropped out of the top ten list every year, showing the high frequency of changes in popular names!

So no wonder there's a certain amount of anticipation in picking out the right name for your child — wouldn't it be a shame if fashions changed!

### 6. To a certain extent, social changes affect naming fashions. 

Humans are always affected by what goes on around them — and the choice of names is no exception.

Once something has become a matter of fashion, social influences can affect changes in it. This is called secondary influence. Once the broad influence of modernization turned names into a matter of taste, other social changes started determining which names become fashionable and when.

For example, the name Franklin became popular after Franklin Roosevelt became president of the United States and started tackling the Great Depression. This is what the author refers to as a random external influence since, had his name been, say, Peter or Simon, those names would have risen in popularity.

There are many different social events that have influenced names. For example, the civil rights movement in the United States made an observable impact on the way African-American parents named their babies.

From the 1960s on, the percentage of uniquely invented names given to black babies in the United States, e.g, "La Verne," was much higher than those given to white babies. In Illinois in 1989, for example, a mere 5 percent of white girls' names were invented versus 29 percent of black girls' names. This can be traced back to the social events of the civil rights movement and the desire to emphasize the distinctiveness of black culture.

But there are limits to the effects that external events have on fashions. For example, the broad developments of name fashions, like the concentration of popular names, stayed more or less unaffected by such important events like the World Wars, the Depression or the protests against the Vietnam War. 

This shows that external events only shape the fashion of names to a certain extent. So just because there might be a possible connection to different social events doesn't mean there always has to be an external cause.

### 7. Internal mechanisms make fashions develop gradually and in a certain direction. 

Have you ever wondered why we can figure out approximately when a picture was taken just from what the people in it are wearing? That's because there are some internal mechanisms that prevent fashion from just randomly changing.

First of all, new tastes are always based on already existing tastes. As a matter of fact, "modest variants on existing tastes" tend to be what's most appealing to us. Extreme changes, on the other hand, often don't seem as appealing because we still judge them based on our current tastes.

Just imagine today's clothing trends being introduced in 1850. The difference between today's flexible individuality and the 1850s' covering gowns is too extreme to be appealing.

Second, the internal mechanisms of fashion are continuously developing in one direction. If fashion changes oscillated back and forth instead, a new fashion could easily be confused with a slightly older one.

For example, if the fashion in dress length would have changed in the last five years to five centimeters shorter than before, the five-centimeter-longer version would slowly start to look out of fashion. But if the longer version became fashionable again, it could easily be confused with the older fashion and look dated.

If fashion changes in one persistent direction — in this case, dresses getting gradually shorter — the new fashion will always clearly be distinguishable from the old fashion

Finally, taste in names changes based on the same sort of internal mechanism occurring independently of external influences.

For example, the popularity of girls' names like Laura and Sarah that end in an "a" sound gradually grew from 1700 to 1850 and then declined again until 1900. The popularity of the exact name probably changed, but the popular names still had similar endings, and the general name theme only slowly changed, showing that the appeal of small variations also holds true for names.

However, there are other internal mechanisms at play aside from the two mentioned here.

> _"Existing tastes affect later tastes."_

### 8. Tastes in names change internally through incremental replacement. 

Have you ever had the urge to change your hair, your home, your life as you know it? While some of us look for radical change on occasion, fashion usually doesn't: the internal mechanisms of taste are much more conservative than that.

Tastes and fashions usually change gradually. For example, when dresses started getting shorter, they did so centimeter by centimeter. Imagine they had gone from ankle- to knee-length out of the blue — such a sudden shift wouldn't have been appealing to most people. And although the modest shift of a few centimeters might not be noticeable from our perspective anymore, it was a change in fashion back then.

Accordingly, new tastes in names usually reflect incremental changes of previously fashionable names.

For example, "Jennifer" was the most popular girls name in California in 1975. But it didn't come out of nowhere: names with a "dj" sound in the beginning, e.g., Jennie and Joan, had been rising in number in the top 100 list since the beginning of the twentieth century. Jennifer was the first "djeh" variety to gain popularity. Once Jennifer established its popularity, other names with the "djeh" sound became popular as well, with the name "Jessica" replacing Jennifer as most common name in 1985.

Innovations of new names also adhere to the rules of incremental change. Even invented names often use stems, prefixes and suffixes from names that are already popular.

For example, the invented name "Latonya" reached the top 50 among black girls in California in 1967. At the time, the name "Tonya" ranked place 14. That's because made-up names only become fashionable if they appeal to other parents: a new name that's similar to already popular names is — according to the logic of incremental change — more appealing.

> _"Internal mechanisms — once set in motion — will generate new preferences indefinitely without the addition of any external influences."_

### 9. Symbolism can influence the attractiveness of a name. 

When we hear somebody's name, we often think: "that name fits her" or "that name doesn't suit him at all." This happens because a name is usually more than just an accumulation of sounds: there's a whole lot of symbolism attached to them.

Random external events give names symbolic meanings. If a person with an uncommon name becomes famous, people connect that name with that person. For example, the name "Barack" wasn't well-known before Barack Obama became president of the United States, but now most people probably connect the name "Barack" with him.

And these symbolic meanings can either enhance or taint the names they're connected with. The movie _Breakfast_ _at_ _Tiffany's_ made the name Tiffany more appealing as it was associated with the luxurious New York jeweler.

But if the name has negative connotations or a negative symbolic significance, the name gets contaminated by it.

For example, the name "Donald" steadily grew in popularity among boys in California between 1905 and 1933, and suddenly declined from 1934 on. Apparently, it was contaminated by its association with the cartoon character Donald Duck, who first appeared that same year. Even though the character was generally positive, parents most likely didn't want their children to be associated with a cartoon duck who didn't wear pants. 

Almost every name has some kind of symbolism attached to it, but the imagery can mean something very different to different parts of society.

For example, parents with lower levels of education more frequently choose boys' names associated with strength, such as Alexander or Arthur. That's because some features or images are more important to some subgroups than to others. For example, in families with lower educational levels, it might be common for boys to be expected to become the "strong" provider of the family.

### 10. Fashions in names change through collective action. 

Most of us care about what others think of us. Even if we want to be unique, we still define ourselves in relation to what others do. This is why fashion can never be reduced to individual choices alone. We always have to consider collective action as well.

Collective action means that people's behavior depends on how others behave or are expected to behave. For example, if you want to fit in a crowd of party people, you might suggest going to a nightclub for an evening out because you expect your crowd to like that suggestion. Others might act in the same way, which can decisively influence the outcome of the decision.

Applied to fashion, that means a certain taste's appeal also depends on other people's behavior: if we see a certain style of clothing, e.g., in magazines or stores, we either expect that others will also like that style and dress accordingly or we'll try to actively avoid that certain style, thinking that others will find us more unique and interesting that way.

Parents select names according to how they believe the name will be evaluated by others, but they don't always get what they want. Some parents want uncommon, rare names, while others want typical, popular names for their children. But since parents usually don't know what other parents will name their own children, they might pick a name they think is uncommon and end up having chosen the generation's most popular name.

For example, the author wanted to choose a unique name for his daughter and decided to call her "Rebecca," only to realize later that this name had become trendy just around that time. The author now has a different view on the name "Rebecca" than when he chose it, showing that our attitude towards names depends on others.

### 11. The changing name tastes of immigrants show how both external and internal mechanisms influence our choices. 

Names are often connected to one's origin or heritage. So what happens to the taste in names in groups that migrate?

Many groups of immigrants are influenced by the external event of their migration when choosing a name. When immigrant groups from a certain country start coming to the United States, they often choose fashions according to the logic of assimilation. In this case, they chose names popular among US citizens and not those typical of their native cultures.

That's why, in the mid-1980s, the most common names given to white children in California were also the most popular names given by immigrants from China, Japan and South Korea.

There are also internal mechanisms at play, such as the fact that new tastes are influenced by old tastes, that influence their choice.

The names given by Mexican-Americans are still influenced by the old traditions of Spanish names, e.g., the "a" ending is a strong gender marker for females. Even second-generation Mexican-Americans who've already assimilated to Anglo fashions follow their old Spanish taste by choosing mainly female names ending with "a" and rarely giving boy's names this ending — that's why the name "Joshua" isn't very popular among Mexican parents.

Name fashions among immigrants can also shift through the external change of the subgroups' role in society. Names connected with the heritage of a certain ethnic group become more popular once that group is accepted in society. Immigrant groups that are already well established in society might turn back to names connected to their origin because they don't need to do the extra effort to fit in.

For example, the Irish were looked down upon when they first migrated to the United States in waves. But now that they're a respected subgroup of society, names like Kelly, which are perceived as typically Irish, are also more prominent.

### 12. The influence of popular culture on names is higher when the names already fit the trend. 

It's easy to believe that the names of celebrities or fictional characters could influence fashions in names, since they reach many people and often have positive connotations. But we shouldn't be too quick to believe this simple explanation.

The general belief that movies and novels have a great impact on name choices is not all true.

Even though some stars have obviously led to the popularity of certain names, no change could be found in name fashions in many cases of widely celebrated media representatives.

For example, while the name Gary gained popularity after Gary Cooper became famous, stars such as Humphrey Bogart, who was equally as successful, did not leave a mark in the statistics for the popularity of "Humphrey."

In other words, when a name is already on the rise, it's more likely to be chosen for novels, personas or movie characters. The name "Marilyn" was already popular when Norma Jeane Baker chose to call herself "Marilyn Monroe." The fact that it still ranked high the years after she became famous is thus not surprising.

Rather, her picking the name is a sign of the name's popularity, as actors and other artists usually choose names they think will appeal to their audiences.

The influence of the entertainment field on name trends is difficult to measure in general.

One reason for this is the question of timing: a movie popular among teens might only have an impact 10 to 20 years later because that's when its audience is naming their children. A movie appealing to people who've already had kids might influence the taste of its fans regarding names, but won't have an impact in naming statistics.

Only in some rare cases can entertainment's influence be clearly seen in the long term, when an unusual name suddenly becomes popular. Like when people starting naming their daughters "Marlene" after German singer Marlene Dietrich became popular in the United States.

### 13. Final summary 

The key message in this book:

**Fashions** **are** **influenced** **by** **external** **events** **and** **the** **influence** **of** **media** **–** **but** **not** **as** **exclusively** **as** **we** **think.** **Internal** **influences** **that** **operate** **independently** **of** **external** **events** **let** **fashions** **and** **taste** **change** **over** **time.** **If** **we** **want** **to** **understand** **why** **a** **certain** **taste** **came** **about,** **we** **must** **thus** **always** **search** **for** **both** **types** **of** **influences.**

Actionable advice:

**Do** **your** **research.**

If you want your child to have an uncommon name, do some thorough research on upcoming name trends — otherwise you might be surprised that the "uncommon" name you chose suddenly isn't so uncommon anymore.
---

### Stanley Lieberson

Stanley Lieberson is a Canadian sociologist and research professor at Harvard University. Besides first names and fashions, he has studied language conflicts, race and ethnic relations.

