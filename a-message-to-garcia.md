---
id: 5787797dc6cd380003a5c5ba
slug: a-message-to-garcia-en
published_date: 2016-07-21T00:00:00.000+00:00
author: Elbert Hubbard
title: A Message to Garcia
subtitle: And Other Essential Writings on Success
main_color: 99623D
text_color: 99623D
---

# A Message to Garcia

_And Other Essential Writings on Success_

**Elbert Hubbard**

_A Message to Garcia_ (1899) outlines the lessons to be learned from a Spanish-American War-era lieutenant who displayed a level of dedication to his work that we should all strive to match. At its core, the book describes how working hard is the key to living well, staying virtuous, finding happiness and improving the world at large.

---
### 1. What’s in it for me? Dig into the ideas that helped shape American ideals. 

It's 1899, the world is about to enter the twentieth century and one man, Elbert Hubbard, is writing an essay that will have an immense impact in the next decades. Hubbard, previously a traveling soap salesman and future father of the Arts and Craft movement, is writing down his views on how to be a good person and live a good life.

His essay, with its curious title, would serve as an inspiration not only for two well-known movies in the early days of cinema, but also for many of the people who would help shape the modern-day United States. Among his admirers were industrialist John D. Rockefeller, car manufacturer Henry Ford and President Teddy Roosevelt. Within Hubbard's lifetime, _A Message to Garcia_ was reprinted more than any other book besides the Bible.

These blinks bring Hubbard's messages into a new light for us in the twenty-first century.

In these blinks, you'll find out

  * who Garcia is and what bringing him a message means;

  * one simple experiment that shows how lazy some people are; and

  * why you shouldn't avoid manual labor.

### 2. The phrase “a message to Garcia” refers to the story of Lieutenant Andrew Rowan, the ideal worker. 

Right now, you might be asking yourself, "Who is Garcia and what does he have to do with me?" To answer that, we have to look back to a notable incident that occurred during the Spanish-American War.

When the war broke out in 1898, American President William McKinley wanted to recruit Cuban rebels to fight for the American cause. Spain still ruled Cuba at the time, so Cuban insurgents would have been a valuable military asset to the United States.

President McKinley asked military leaders to deliver a message to one rebel leader in particular: Calixto Garcia. Getting a message to him would be no easy feat, however. It would be dangerous to send an American soldier through Cuba in the midst of the war, and military leaders didn't even know exactly where Garcia was. They had to find someone up to the task.

It was Colonel Arthur L. Wagner who found the right person: Lieutenant Andrew Rowan. Rowan successfully penetrated enemy territory and delivered the oil-skin pouch that contained the message for Garcia, all without once stopping to question the nature of the mission.

Lieutenant Rowan truly exemplifies the idea worker — it's a shame statues of him don't adorn university campuses! After all, swift responses and dedication to one's assignments are often more important than learning from books, but we still tend to neglect the lessons of Rowan's story.

As an employee, you should strive to maintain the same levels of initiative and devotion that Rowan displayed in delivering the message to Garcia. If you don't, you'll suffer the consequences.

### 3. Employers are always looking to weed out lazy, incompetent workers. 

A great deal has been written about factory workers, sweatshop seamstresses and homeless people desperate for any kind of employment. Not all of them are victims of "the system." They may bring their desperation upon themselves.

Incompetence and feelings of entitlement are prevalent among workers. In fact, you can do an experiment on this phenomenon on your own.

Try making six people who work below you do a bit of encyclopedia research on the life of Antonio da Correggio, an Italian Renaissance painter. They'll probably fire off some unnecessary questions:

  * "Which encyclopedia?"

  * "Is it my job to do that?"

  * "Is this an emergency?"

  * "Would you like an encyclopedia so you can do it yourself?"

  * "Why?"

Even if you patiently answer these questions, they'll probably still try to delegate this simple task to someone else, or come back saying da Coreggio doesn't exist! You'd be better off just reading the encyclopedia article yourself, instead of dealing with the laziness and incompetence of your subordinates, who will never last in their jobs anyway.

Employers can tell when their workers slack off, and there are consequences for those who don't give it their all. Only workers who can "get a message to Garcia" have the grit to survive in the business world.

You're certain to be fired if you're constantly complaining or being negative, for instance. Employers are always on the lookout for weaker links in the chain, even when business is going well. And incompetent employees aren't only useless, they drain precious resources as well.

> _"Get your happiness out of your work, or you'll never know what happiness is!"_

### 4. Being negative is a waste of time, so get out or get in line! 

You know those people who are always complaining about work or other problems in their life? You probably think more negatively of them than they do of whatever they're complaining about!

Complaining about or criticizing any of your superiors only reflects badly on you. For example, the author met a Yale University student who had a very bitter attitude toward the school. However, this bitterness just showed his rather unattractive tendency to focus on trivial problems, blow them out of proportion and let them get in the way of his incredible educational opportunity.

And if you ever say that your company or boss is "rotten," you're just revealing the rottenness in yourself. Just picture a worker who's paranoid that their boss is oppressing them in some way. If they were asked to get a message to Garcia, they'd probably respond, "Why don't you take it yourself?"

Who would that reflect more poorly on, the boss or the worker?

If your employment situation is absolutely intolerable, just quit. No company or boss is flawless; but you should invest 100 percent of your energy into being the best worker you can, or else leave the company entirely. In other words, _get out_ or _get in line_.

So how does one get in line? By controlling their thoughts and attitude.

Negative thoughts create a negative reality, while positive thoughts create success and happiness. So, train yourself to be courageous and cheerful; such traits pave the way toward success and goodwill.

Your character is ultimately the result of your attitude and the way you spend your time. Your thoughts shape your actions and, in turn, end up determining who you are.

> _"All things come through desire and every sincere prayer is answered. We become like that on which our hearts are fixed."_

### 5. Your thoughts and actions determine your happiness and the way you live your life. 

Work isn't just about work. The way you act at work has an impact on how you live your life, how you treat others and how you treat yourself.

You'll only achieve happiness by working hard. When you put effort into your job, you spend your energy in a productive way, gain lifelong training and skills and learn to divide your time between work, play and study in appropriate amounts.

A lot of people mistakenly assume that if they become wealthy and successful, they'll be happier because they won't have to work. But that's where they go wrong, because discipline and focus toward work creates an inner satisfaction, and without it we often lose this source of joy.

So wealth shouldn't render you useless, not only because it's good for you, but also because it's good for others. For instance, if everyone worked a bit, no one would have to be overworked. If no one wasted any resources, everyone would have enough. If no one over-ate, no one would have to go hungry.

So don't aspire to be rich, famous or powerful — strive to lead a good life instead. Aim to radiate health, cheerfulness, courage and goodwill, and live a life free of hate, jealousy or fear. Be honest, live simply and don't be ashamed to admit it if you don't know the answer to something.

We should seek to treat all people equally, and help others by teaching them the virtues of self-reliance. People must be led by example, not force.

Living a self-aware life in which you consciously focus on your own happiness is the key to being righteous and prosperous.

> _"The person who lives on the labor of others . . . is really a consumer of human life and therefore must be considered no better than a cannibal."_

### 6. Final summary 

The key message in this book:

**The ideal worker, leader and person works and lives with dedication, focus, honesty and integrity. These types of people are never lazy, and instead lead by example. So give your work your all, or don't waste your and your boss's time — be the kind of worker who can get a message to Garcia.**

Actionable advice:

**Consider your employer's needs.**

The next time you have a negative feeling about your boss, stop and think about what they may be going through. Are you making their job as easy as you can, or are you acting in a way that may generate suspicion about your character? Try to see things from their side, as it may well show you an entirely different perspective.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Do the Work_** **by Steven Pressfield**

_Do the Work_ (2011) outlines ways to help you conquer your fears, stop procrastinating and accomplish the things you've long desired. Learn about the many ways you can fight _resistance_, the negative internal force that tries to stop all of us from reaching our goals.
---

### Elbert Hubbard

Elbert Hubbard (1856-1915) was an American writer, artist and philosopher. His essay _A Message to Garcia_ sold over 40 million copies and was translated into 37 languages.

