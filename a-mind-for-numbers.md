---
id: 54be8273623238000a000000
slug: a-mind-for-numbers-en
published_date: 2015-01-23T00:00:00.000+00:00
author: Barbara Oakley
title: A Mind for Numbers
subtitle: How to Excel at Math and Science (Even if You Flunked Algebra)
main_color: 7269CC
text_color: 554F99
---

# A Mind for Numbers

_How to Excel at Math and Science (Even if You Flunked Algebra)_

**Barbara Oakley**

_A Mind for Numbers_ offers insight into the way our brains take in and process information. It outlines strategies that can help you learn more effectively, especially when it comes to math and science. Even if mathematical or scientific concepts don't come naturally to you, you can master them with the right kind of dedication and perseverance — and this book will teach you how.

---
### 1. What’s in it for me? Awaken your inner mathematician. 

Do numbers make your head spin, and equations freak you out? Barbara Oakley hated and sometimes even failed her math and science courses throughout school, but after graduating, she found that she needed these skills to pursue her dream career. In _A Mind for Numbers_, Oakley, now a professor in engineering, shares easily applicable methods and fun tricks for excelling at math and science.

You'll also learn

  * why napping and letting your mind wander facilitates learning;

  * that planning play time is as important as planning work time; and

  * why you wouldn't necessarily want to be a genius.

### 2. The human brain has two ways of thinking: the focused and the diffuse modes. 

Imagine a flashlight with two settings: it can intensely focus its beam on one small area, or it can spread its light less powerfully, but more broadly.

Our brains work in a similar way. They can switch between two kinds of networks or functions.

First, there's the _focused mode._ This is when we concentrate on information that's already tightly embedded in our minds. We usually use the focused mode for concepts we already find familiar or comfortable.

Focused mode thinking is essential for studying math and science. We use it to direct our attention to solving problems using rational and analytical thought.

For example, you use focused mode thinking when you multiply numbers (assuming you already know how to multiply). If you're studying a language, you might use it to incorporate the verb conjugation you learned last week.

Our second way of thinking is _diffuse mode thinking._ Diffuse mode is what happens when you relax your attention and let your mind wander a bit. We use it to get a "big picture" understanding of something.

Diffuse mode is also important for learning math and science. It allows you to gain new insights into problems you've been struggling with.

When trying to understand something new, it's better to not focus on one thing. Use your "big picture" diffuse mode of thinking instead.

Consider this sentence: _Thiss sentence contains threee errors._ The first two errors — in spelling — are easily noticeable when using a focused approach.

But the third error? The third error — that the sentence is untrue because _there is no_ _third error_ — only becomes clear when we use the diffuse approach to consider the sentence more abstractly. Focused and diffuse thinking both have their purposes.

### 3. Focused thinking, diffuse thinking and sleep are all fundamental for successful learning. 

As we go through our daily activities, we switch back and forth between the focused and diffuse modes. We're in one or the other, but we're never consciously in both at the same time.

Switching between them is important for our learning process — it allows for understanding and problem solving.

To tackle intellectual problems and learn new concepts, we almost always need periods of time when we aren't consciously thinking about the issue. This is when we use our diffuse mode.

Then, when we turn our attention back to the problem, we can consolidate the bigger-picture insights we've gained.

When we learn, one mode of thinking processes the information it receives, then sends the results back to the other mode. This volleying back and forth of information is essential; it lets us work toward a solution.

Sleep is also essential in learning. Think of it this way: you can't make your muscles bigger by constantly lifting weights. They need time to rest before they're used again. When you take time off in between weight-lifting sessions, you build strong muscles in the long run.

The same goes for learning. We need periods of rest to refresh and reinvigorate our thinking.

Toxic products are created in our brains when we're awake for too long, and sleep clears them away. Sleep keeps our brain healthy, so it's a vital part of learning and memory. Also, research has shown that if we review learning material right before sleep, there's a higher chance we'll dream about it, and dreaming about it can enhance our understanding.

So if you get frustrated with something, try switching your mode of thinking, or taking a nap.

### 4. Chunking and recalling learning material is necessary for gaining expertise. 

The human mind has extraordinary capacities. Did you know you have an innate ability to see forests, so your mind doesn't have to process each individual tree?

This mental ability is called _chunking_. It's an important part of learning, and we use it in our first steps toward gaining expertise in math and science.

_Chunks_ are pieces of information that are connected by their common meaning. For example, we can take the letters _p_, _o_, and _p_ and put them together into one conceptual chunk: the word _pop_.

Chunking information helps the brain run more efficiently. Once we chunk an idea or concept, we don't need to remember each underlying detail or tree — we've got the main meaning.

As we learn, we build a chunked, mental library of concepts we can go through. When we're trying to solve puzzles, we train our brains to not just recognize _specific_ problems, but different _types_ and _classes_ of problems as well. This helps us solve them much more quickly.

While learning, you can keep the chunking process going by _recalling_ the material you're trying to learn. _Recall_ is when you mentally retrieve the main ideas of your learning material. It helps build new chunks of information.

The best way to build chunks is to focus your attention on understanding the basic ideas, then practice going over them. Remember that you also need to understand them in the context of other information you already know.

You can't understand a math problem just by looking at the solution. If you really want to understand it, you have to knit the concepts of it into the web of concepts and chunks you already have in your brain.

So when you're studying, don't just passively reread information — practice recalling it. This will force you to work your way through problems and solve them.

### 5. Focusing on the process instead of the product will help you defeat procrastination. 

Imagine how much pain your calf muscles would be in if you didn't practice for a big marathon until the night before. Similarly, if you procrastinate, you can't compete in math and science, either.

Procrastination is tempting, and it's important to prevent it. It's something we can all easily succumb to, because it offers temporary relief from an unappealing present. We procrastinate on things that are boring, difficult, or make us feel uncomfortable.

If you want to go far in math and science, you've got to take control of your procrastination.

Think of learning math and science this way: you need brief study sessions where you lay "bricks" of information, and time in between for them to dry.

In other words, your learning process needs to be spread out over a long period of time. You can't learn it quickly at the last minute.

You can prevent procrastination by focusing on the _process_, rather than the _product,_ of your learning.

The process is the ongoing time you spend doing something — it happens when you say "I'm going to study for the next 20 minutes."

The product is the outcome. It could be a homework assignment that you finish, for example.

When you focus on the _process_ of learning, you'll relax and go with the flow of your work. You won't judge yourself as much, because you'll be less focused on quickly getting the end result.

This also helps prevent procrastination. Sometimes, a product is so challenging to reach that we avoid _trying_ to reach it. We don't try because we're uncomfortable with the difficulty, or we fear failure. That's a kind of procrastination.

As you get better at focusing on the _process_, you won't just find it easier to avoid procrastination — you'll also enjoy learning more.

### 6. Mental tricks are powerful tools that help you learn. 

The management specialist David Allen once said, "To a great degree, the highest-performing people I know are those who have installed the best tricks in their lives."

There are many simple but effective mental tricks you can use when learning.

First, you can try putting yourself in a new environment. Figure out where you're most productive. Some people need a busy coffee shop, while others need a quiet library. However, research has shown that having a special place devoted to working is particularly helpful for fending off procrastination.

_Mindfulness training_ is also a good idea. Mindfulness training is like meditation — it means ignoring distracting thoughts by letting them drift past without paying attention to them. You can meditate by doing things like counting down numbers. Focusing on counting relaxes you, and trains your mindfulness.

Finally, try _reframing your focus_. Reframing your focus means shifting your attention from something negative to something positive. It helps you adjust your attitude.

For example, if you have trouble getting up in the morning, don't think about how tired you are — think about how good your breakfast will be.

One of the most effective learning tricks is keeping reasonable, achievable to-do lists. This helps you gain control of your habits.

So write a brief to-do list once a week, or even every day. You'll be able to step back, look at the bigger picture, and set your priorities.

Keeping a planner journal can also help keep you on track. And when scheduling your time, remember: "play" is just as important as work. Planning for "play" also helps you avoid procrastination, because it will take away your reasons to do it.

### 7. Memorization techniques can make learning material meaningful, memorable and fun. 

We learn and recall information much more easily when the information is meaningful to us. This means your memorization process can become an exercise in creativity.

Suppose you want to remember Newton's second law: _f = ma_. This law relates _force_ to _mass_ and _acceleration_. The letter _f_ could stand for flying, _m_ for mule and _a_ for whatever comes to your creative mind (apples?)! So if you want your _mule_ to start _flying_ faster, you have to feed it more _apples_.

One of the best memorization techniques is creating _metaphors_. A metaphor lets you see how one thing is similar to another.

For example, if you're trying to understand electrical currents, it helps to visualize them as water. You can imagine the voltage as pressure on that water.

Metaphors help cement ideas in your mind, because they connect them to structures that are already there. The more connections you build, the easier it'll be to recall concepts and what they mean.

It's often helpful to pretend _you_ are the concept you're trying to understand. You can imagine yourself as an electron, or that you're inside the _x_ of an algebraic equation.

_Acronyms_ are another useful tool in memorization. They help you simplify your learning material.

Imagine you want to remember four plants — garlic, rose, hawthorn and mustard. The first letters of each word abbreviate to GRHM, so you can visualize a GRAHAM cracker.

In fact, academics in many disciplines use _memorable sentences,_ which are a kind of acronym. In memorable sentences, the first letter of each word is also the first letter of each word in a list that needs to be memorized.

These sentences can form a story, and the _meaning_ of that story makes it easier to retain the information.

> _"There are hidden meanings in equations, just as there are in poetry."_

### 8. Practice and persistence are often more important than intelligence. 

It can be intimidating to work alongside other people, especially if they understand new concepts more quickly than you. However, "average" students sometimes have advantages when it comes to work ethic and creativity.

Gifted people often have their own set of challenges to face. For one, smarter people often have a longer _working memory_. This is the part of our memory that handles whatever we're immediately and consciously processing in our mind.

However, a strong working memory can hold its thoughts so tightly that new thoughts can't easily get through. This sometimes leads smart people to overthink simple problems and overlook simple solutions. Research has shown that smart people have a higher tendency to lose themselves in complex thoughts, so they may have trouble focusing.

_Practice_ and _persistence_ are often more important than intelligence.

Practice cements memories into our _long-term memory_. This is where thoughts are stored, so they can be accessed later.

Unlike high-powered working memories, average working memories don't lock everything in so tightly, so they can get more input from other areas of the brain, like our long-term memory.

These other areas are often the source of creative ideas. So the memory connections we build by _practice_ help us become more creative.

_Persistence_ in learning allows us to go deeply into a problem. It also improves our ability to think independently, and leads us to reach out and ask the right questions for getting the right answers.

Even people with average intelligence can achieve the same mental feats as those who are "naturally" gifted, if they work hard and practice diligently.

> _"We all have a natural feel and flair for math and science. Basically, we just need to master the lingo and culture."_

### 9. Testing is a powerful learning experience in itself. 

Testing isn't just about measuring how much we know. It's an extraordinarily powerful learning experience in itself. Studying and preparing for exams are highly important.

Testing improves our ability to retain what we've learned. It doesn't merely assess learning, it facilitates it. This is called the _testing effect_.

In 2009, two psychologists named Julie Campbell and Richard Meyer conducted research into the testing effect. In their study, two groups of participants watched a PowerPoint presentation of information they had to learn. One group was given a multiple choice question after every few slides, so they had to recall what they'd just seen.

When the participants were given an exam at the end, the ones who'd been tested continually as the presentation went on performed better, because of the testing effect. In fact, it's been shown that the testing effect still occurs even when the test performance is bad, or no feedback is given.

Testing also helps us deal with stress or anxiety. The body puts out chemicals like _cortisol_ when it's under stress. This can cause sweaty palms or a racing heart.

This sounds like a bad thing, but research has found that the way we interpret these symptoms makes a big difference. If you shift your thinking from "This test makes me afraid" to "This test makes me excited to do my best, and that's why I feel this adrenalin," it can significantly improve your performance.

If you're feeling panicky before a test, it's also good to momentarily turn your attention to breathing. Deep breathing sends more oxygen to critical areas in the brain.

If you practice these techniques, you'll get better at dealing with stress or anxiety in exams, and you'll be able to channel it into productivity. All in the all, testing is very useful in learning.

### 10. Final summary 

The key message in this book:

**With diligence and hard work, anyone can improve their understanding of math and science. So** ** _recall_** **your learning material, and focus on the** ** _process_** **of your learning. Even if it's challenging at first, you'll be able to achieve great things.**

Actionable advice:

**Take breaks.**

Don't forget: you have to build your math and science skills slowly, like muscles. So rest in between your study sessions, and be sure to get enough sleep. That's more effective in the long run.

**Suggested further reading:** ** _How Not to Be Wrong_** **by Jordan Ellenberg**

_How Not to Be Wrong_ gives us an intimate glimpse into how mathematicians think and how we can benefit from their way of thinking. It also explains how easily we can be mistaken when we apply mathematical tools incorrectly, and gives advice on how we can instead find correct solutions.
---

### Barbara Oakley

Barbara Oakley is an engineering professor at Oakland University and a fellow of the American Institute for Medical and Biological Engineering. She's written extensively about learning strategies, and her work has been featured in the _Wall Street Journal_.

