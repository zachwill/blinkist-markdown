---
id: 5485c7a76331620009410000
slug: a-more-beautiful-question-en
published_date: 2014-12-09T00:00:00.000+00:00
author: Warren Berger
title: A More Beautiful Question
subtitle: The Power of Inquiry to Spark Breakthrough Ideas
main_color: 52B3D4
text_color: 2A5D6E
---

# A More Beautiful Question

_The Power of Inquiry to Spark Breakthrough Ideas_

**Warren Berger**

_A More Beautiful Question_ highlights the importance of finding the right questions in our search for success. With the help of cutting-edge research and examples from the business world, this book defines the "beautiful" question and shows you how to start asking such questions yourself.

---
### 1. What’s in it for me? Discover how to ask beautiful questions that can transform your life. 

When you were a child, you never stopped asking questions. "Why can't I fly?" "How many people are there in the world?" "If the world is spinning, why don't we fall off?"

As children, our hunger for knowledge is so strong that we are compelled to ask, ask and ask again.

Yet once at school, our native love of questioning is dampened by the need to cram facts and figures to pass a never-ending series of tests. Although night after night of memorizing might have helped you get good grades, losing our passion for questioning is actually a dangerous thing.

These blinks tell you why we all need to ask more questions. They show you how by asking the right questions, you can discover new opportunities in life and in business. You'll learn the true power behind the "beautiful question," a power that when harnessed can transform your world.

In the following blinks, you'll also discover

  * why question-storming is superior to brainstorming;

  * what the "Right Question Institute" actually does; and

  * just how many questions a four-year-old child asks on a daily basis.

### 2. Questions drive creativity and stimulate new ideas; they also tell us what we don’t know. 

We humans like to think we're special, a class apart from the rest of the animal kingdom.

But what exactly makes us special? Opposable thumbs? Or our ability to create and use tools?

While these things are important, what really makes us unique is our innate desire to ask questions. As you'll find out, the ability to ask questions has been vital to our success as a species.

For starters, asking questions ignites our creativity and stimulates new ideas.

Whenever you ask a question, you automatically highlight the things you don't know. And once you know what you don't know, you can then think about how to fill the gaps in your knowledge.

It's precisely this process that has driven some of the greatest innovations in human history.

For instance, we only have to look back a few decades to see how a single question revolutionized the photography industry.

In the first half of the twentieth century, a young girl asked her photographer father why she had to wait to see the pictures he had just taken of her. Unsurprisingly, she wasn't satisfied by his technical answer — but neither was he.

His daughter's question continued to nag at him, so he set out to find a better answer. Eventually, after several years of experimentation, he finally came up with a solution: the Polaroid Instant Camera.

Moreover, our questions often turn out to be more important than answers.

In our digital world, facts are more easily accessible than ever before. Whether we need the date of a historical event, a chart of a stock's performance or even a medical opinion, search engines can comb the internet and deliver the information within seconds.

These bits of information, however, are totally useless by themselves. The facts are only as good as the questions that you ask.

### 3. Rote learning in school dulls our innate desire to ask questions as we discover our world. 

Kids can't seem to ask enough questions, wondering why the sky is blue or why chocolate cookies are not an acceptable dinner. Their insatiable curiosity is both inspiring and frustrating for tired parents!

For children, the world is new — and to learn about it, they ask questions. In fact, research by Harvard University psychologist Paul Harris shows that children aged between two and five ask a total of some 40,000 questions!

Though many questions may seem silly, they are seldom frivolous; and a child's desire to learn is demonstrated by her attitude toward the answers she receives.

For example, University of Michigan researcher Brandy Frazier found that preschoolers aren't just looking for attention through questioning, and genuinely care about the answers they receive.

What's more, a child is more likely to continue to ask "why?" when a parent's answer doesn't satisfy his thirst for knowledge.

Unfortunately, our love of questioning fades as we enter structured schooling. According to research conducted by the Right Question Institute, a nonprofit organization concerned with teaching people to ask the right questions, there's a dramatic decline in questioning once children enter elementary school.

Why is this? It would seem that the American education system discourages children from asking questions, placing emphasis instead on discipline and memorization.

A student's success at school centers around how well she or he is able to take tests. In this kind of environment, providing the correct answers on an exam takes precedence over formulating good questions.

Placed under the constant pressure to achieve good grades, there's little time for students to ask "why?"

This can have major consequences for us as adults. By the time we've entered the working world or college, we've lost the ability to ask effective questions — right when our questions could make the biggest difference!

The next blinks will show you how to rediscover your innate ability to ask good questions.

### 4. Though direct, the question “why?” is child’s play. The question “why not?” creates solutions. 

In regaining our ability to ask questions, our goal should be to formulate _beautiful questions_ — questions with the potential to close the gaps in our knowledge through creativity and innovation.

First, we have to return to our childlike mind-set by asking "why?" Such questions are known as _naive questions_. Just as a child will ask why the sky is blue, you too should question the simple or seemingly obvious things that you otherwise take for granted.

The power of "why" questions lies in their ability to get to the bottom of complex issues. Asking a politician "Why has the country run out of money?" during a financial crisis forces him to give a straightforward answer: "Because we spent too much" or "We didn't earn enough revenue."

"Why" questions, however, can only tell us so much. If we want to turn answers into solutions, we need to ask "why not?"

Asking "why not" questions challenges our basic assumptions about how things are or should be done, and is also a great source of innovation.

For example, if you've ever attended a large conference, you've probably struggled to find an inexpensive room at a local hotel, all of which seem always to be completely booked weeks ahead.

Considering the situation, in 2007, flatmates Joe Gebbia and Brian Chesky asked themselves: Why do these people have to stay in an overpriced hotel? Why couldn't we just offer a room in our place for them to crash, for far less cash?

Starting with inflatable mattresses in an apartment in San Francisco, the duo have since expanded their idea to become the industry-changing online homestay company, Airbnb.

> _"The mind of the beginner is empty, free of the habits of the expert. Such a mind is open to all possibilities and can see things as they are." — Japanese Zen Master Shunryu Suzuki_

### 5. Asking “what if?” is the spark that combines inspirations. Answering “how?” puts ideas in motion. 

Asking "why" and "why not" questions are great ways to identify problems.

However, you don't want to stop there. You want solutions! To start, consider asking "what if?"

Here's a simple scenario. You live in a village along a river. When people need to cross the river, they walk along the bank until they reach a shallow part, where one can safely wade across.

"But why?" you may ask. "Why not cross the river right at the village?"

The river is too deep there, the villagers answer. _But what if we built a bridge_?

"What if" questions allow us to think outside our normal paradigms, synthesizing old ideas or creating entirely new solutions. This _connective inquiry_ — the process of combining, mixing and connecting ideas that don't usually go together — is at the heart of "what if" questions.

Such innovative combinations of ideas can be found everywhere. For example, the online movie rental service Netflix was born from a mix of a video-rental service and a monthly membership club.

It's not enough, however, to simply come up with ideas — they have to be feasible enough to put into practice. To go from idea to action, you'll have to ask "how?"

"How" questions are the most difficult part, requiring endurance, patience and commitment.

Say you have a great idea for an alarm clock. To keep sleepers from just hitting the snooze button, the clock will physically move away from the bed once it goes off. You start by adding wheels — but is that enough? How do you keep the clock from breaking if it falls off the side table?

Although a solution might seem simple, it may take a lot of time and energy to figure out how to make it feasible. The rewards are worth it, if you persevere!

So you've learned how finding the right answer is all about asking the right questions. In our final blinks, you'll learn how to put these strategies into practice.

> _"The creative act is no longer about building something out of nothing, but rather building something new out of cultural products that already exist" –Wired Magazine_

### 6. Asking beautiful questions can help a company innovate to stay ahead of the competition. 

Once you understand the power of beautiful questions, you'll notice that such questions can be applied everywhere, including in business.

As business today is so fast-paced, asking the right questions is crucial. Today's rapidly changing market pushes companies to constantly reinvent. This is where beautiful questions come into play.

Businesses should always be asking the four beautiful questions: Why, Why not, What if and How?

Sporting goods company Nike observed that runners increasingly had to manage a multitude of gadgets while running — things like a stopwatch, a heart monitor and a music player. The company asked itself: _What if_ we created a tool that combined the functionality of all these devices but was also connected to a Nike running shoe itself?

In cooperation with Apple, Nike designed an all-in-one product to do exactly that: Nike+.

Businesses don't have to wait for problems to arise before they start asking beautiful questions. Instead, companies should be proactive and hold _question storming sessions._

Typically, _brainstorming_ is the method many companies use to stimulate creative thinking. However, the pressure to come up with "good" answers can mean that employees might be too scared to put forward their ideas.

Question storming, on the other hand, in which _questions_ are generated instead of solutions, is more effective.

For example, a bar soap called Irish Spring was an instant hit in the early 1970s, which inspired a competing soap company to come up with a new, refreshing and innovative product.

Company executives tried brainstorming on other permutations of a refreshing soap with green stripes, but such a strategy didn't yield many answers. They then tried generating questions instead.

Eventually, they stumbled upon the question that would help turn things around: "How might we create our own, even more refreshing soap?" This led to the creation of the bestselling blue-and-white, seaside-themed bar soap called Coast.

### 7. Asking beautiful questions can help you lead a fulfilling and purposeful life. 

Beautiful questions aren't only useful in business; by asking questions in our own life, we can become happier, more fulfilled and be more successful.

In fact, our very purpose in life is determined by beautiful questions!

Imagine that your career is like climbing a mountain. As you clamber up, hoping to reach the top before someone else, it's easy to forget _why_ you started climbing in the first place!

Considering this question and answering it honestly gives purpose to your actions and meaning to your life. Finding an answer that is truly your own keeps you from just following others, without knowing whether such a course is actually right for you.

There's nothing worse than sweating your way up a mountain, only to realize that you're not at all interested in reaching the top!

Once you realize that you need to make a change, you can then ask, "What if I took a different path?" and then follow up with, "How do I get started?"

Moreover, asking beautiful questions can also make you happier. Indeed, discovering the key to your happiness starts with a simple question: "What do I have that I am grateful for?"

According to Harvard happiness researcher Tal Ben-Shahar, practicing the "habit of gratitude" makes people more optimistic, happier and more successful in achieving their goals.

Consider, for instance, that some poor people seem happier than those who are wealthy.

How could this be?

These people appreciate the basics in life — friends and family, or simple pleasures like hobbies or belonging to a community. They count their blessings and are grateful for what they do have.

Wealthy people, on the other hand, never seem to ask this vital question, perhaps because they don't have to. Instead, they strive for more yet are never happy with what they have.

### 8. Final summary 

The key message in this book:

**The best way to get the answers you need in life is to ask beautiful questions. The right questions help you to identify problems, find solutions and figure out how to turn solutions into reality.**

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Humble Inquiry_** **by Edgar H. Schein**

_Humble Inquiry_ sets out the basic principles of the art of asking the right questions in the right way. It examines how your approach to inquiry affects your relationships at the office, your ability to get quality work completed and, ultimately, your success as a leader.
---

### Warren Berger

Warren Berger is an American author, journalist and blogger on the site, _A More Beautiful Question_. He has written and co-authored a number of books, including _Glimmer_ and _No Opportunity Wasted_.

