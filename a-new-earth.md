---
id: 56660d46cb2fee0007000012
slug: a-new-earth-en
published_date: 2015-12-09T00:00:00.000+00:00
author: Eckhart Tolle
title: A New Earth
subtitle: Awakening to Your Life's Purpose
main_color: F2A130
text_color: 8C5D1C
---

# A New Earth

_Awakening to Your Life's Purpose_

**Eckhart Tolle**

_A New Earth_ (2005) expands on the author's spiritual teachings, focusing on how people should live in the present moment. These blinks show how transcending the human ego is pivotal to individual happiness and can bring an end to global suffering. You'll learn how to let go of your ego and achieve true fulfillment in life!

---
### 1. What’s in it for me? Overcome your ego and make the world a better place. 

When you watch the news, it's hard not to be depressed by all the conflicts and man-made catastrophes plaguing groups of people all over the world. 

Even if there's plenty in society that is improving, there just seems to be something wrong with humanity in general — our perpetual violence, our greedy destruction.

So what is the underlying cause of all these conflicts and constant misery? The answer, you'll discover, is found in our minds. 

By being too caught up in either the past or the future and by worrying too much, our egos dominate and steer us away from the true happiness and fulfillment of the present moment. In short, if we overcome our egos, we can create a better world.

In these blinks, you'll learn

  * what is really meant by "sin" in Christianity;

  * why "evolve or die" is the twenty-first century mantra; and

  * why you should aspire to be like a duck.

### 2. Society’s madness is evident in the violence people inflict upon each other and the planet. 

Most people say we live in crazy, overwhelming times. One of the most renowned Indian sages, Ramana Maharshi, once said that the "mind is maya." In Hinduism, the word _maya_ describes a form of collective mental illness.

In fact, most ancient religions agree that dysfunction — even madness — makes up a large part of our natural way of living. 

Buddhism articulates this idea differently, describing the mind's natural state as _dukkha_, one of suffering and misery. Buddha saw dukkha as an essential component of the human condition. 

In Christianity, the concept of _sin_, when translated from the ancient Greek of the New Testament, means "to miss the mark." Therefore to sin means to miss the point of human existence. 

And yet despite humanity's profound achievements in art, medicine and technology, we still seem to be tainted by an insane, destructive force — regardless of whether we call it suffering, madness or sin.

In fact, people of the twentieth century have both created and witnessed some of the most horrific, systemized methods of destruction, from bombs and machine guns to poisonous gas. Such developments led to the mass killings in Soviet Russia and to the brutal Khmer Rouge regime in Cambodia, responsible for massacring a quarter of the nation's population. 

Even today such violence, greed and hate continues, not only among ourselves but also toward other species and even the Earth itself. We destroy forests, pollute the air and water and mistreat and slaughter animals in factory farms. 

Though many religions have tried to provide ways to counter or mitigate these apparently very human tendencies, none have discovered the way to stem violence. 

So what is the solution? Read on to find out.

### 3. Religion isn’t the medicine to cure our inner madness; yet we must find a new solution. 

People have always attempted to improve society, through ideas such as communism — an example of a philosophy inspired by good, if rather quixotic, intentions. 

Yet communism as an organizing principle failed, as the people who tried to lead this new society lacked the proper state of consciousness and ability to change themselves. 

To guide us, we still have the enduring wisdom of ancient religious teachers, from Buddha to Lao Tzu, the author of the _Tao Te Ching_. However, many of these teachings have been misunderstood or distorted by both the teachers' contemporaries and following generations.

Ideas were often added to such teachings that bore no relation to the original message, and some teachers were ridiculed and killed — or sometimes, in contrast, even worshipped as gods. 

In this fashion, an original message of kindness, humility and unity might be twisted into a religion of hate and division — becoming part of the very insanity it was trying to cure in the first place. Considering that Jesus spoke about empathy and kindness, it is shocking that brutal periods such as the Crusades and the Spanish Inquisition could take place under the banner of Christianity.

The desperate search for a way to rid ourselves of destructive habits can be seen in how we live today. Ironically, it's this pattern that is threatening the very survival of humanity.

Progress in science and technology has only accelerated our capacity to destroy ourselves and our planet, and magnify the problems created by the egoistic human mind. 

Although slavery and torture have always been part of human history, the twentieth century and its cycle of brutality has raised the stakes to an unsustainable level. The need for a fundamental shift in our actions and how we live in the world can be stated simply as, "evolve or die."

### 4. To combat our inner destructive forces, we must understand that it is the ego that fuels these forces. 

Identifying with the ego keeps us floundering in our thoughts, feelings and desires to connect with anything outside of ourselves. It perpetuates our misunderstanding of the world. 

It's time to stop identifying with the ego, and release it.

The problem with the ego is that it tricks us into thinking that knowing ourselves is the same as knowing _about_ ourselves. 

The world today feeds on ego. We maintain the myth that our identity is defined through our accomplishments, backgrounds and material possessions. 

Letting go of the ego, however, is far more than simply giving up our attachment to material goods. It also requires the recognition that what we normally refer to as the self — the "I," or the stream of consciousness that feels, thinks and forms opinions — is not who we are. 

This self — the ego — is a mental construct, a story we tell ourselves about who we are. The true "I" is the I who can observe this stream of consciousness from the outside. 

Releasing the ego is not an easy thing to do, but it is necessary as it is the source of all our discontent, insecurities and feelings of anxiety.

While he was a college student, the author noticed a woman on the subway who was oblivious to her surroundings and talking loudly and angrily to herself. 

The author observed the woman, thinking "I hope I don't end up like her." Yet he then realized that he had said that out loud. At that moment he had an insight: he was just like the woman, ego-driven and self-absorbed, lacking awareness in the present moment!

He then thought that if she was mad, everyone else was mad, too. This event sparked a shift in his awareness and as he detached himself from his thoughts and began to analyze them, he was able to gradually disentangle himself from the trappings of his ego.

### 5. The ego fuels the human tendency to hold onto pain and suffering from the past. 

You may know the feeling of experiencing something that's annoying or hurtful and then, instead of letting the issue go, turning it over in your head until there's no room in your mind for anything else.

The consequences of ego-driven over-thinking are alienation and suffering. When we think too much, we constantly ruminate over past hurts or dwell on anxiety about the future.

Spiritual texts provide many examples of negative over-thinking. One in particular is a story of two Zen monks, Tanzan and Ekido.

Tanzan and Ekido were walking down a muddy road when they saw a young woman trying to keep her silk kimono clean as she tried to cross the road. Wanting to help, Tanzan picked her up and carried her safely across. 

The two monks walked on in silence, but after five hours, Ekido could no longer contain his outrage. He said to Tanzan, "We monks are not supposed to do things like that!" Tanzan said simply, "I put the girl down hours ago. Are you still carrying her?"

The majority of us are like Ekido, constantly collecting situations, resentments, hurts and other negative emotions that prevent us from enjoying life. 

Instead, we should take inspiration from nature, such as examining the behavior of ducks. After a fight, ducks quickly separate and swim away in opposite directions as if nothing happened. Picture a person in a similar situation! He would continue thinking about the event, fueling resentment and anger within himself, creating stories and speculating about the other person. 

It would be far better to let go of the incident and return to the present moment, which is where we can always find peace.

### 6. You must tune into your life’s two different goals: the outer purpose and the inner purpose. 

Whether you're struggling to make ends meet financially or have plenty of money in the bank, having a true life purpose is the only thing that will bring you contentment. 

But how do you find it?

We all share the same inner purpose: to awaken by experiencing a change in consciousness that separates thought from awareness. This state of enlightenment may also be described as presence, or a state in which we are conscious but without thought. 

Instead of being caught up in the ego and our thoughts, we can recognize that our real "I" is the awareness that exists outside of our thoughts.

Being aware of this inner purpose is vital. Outer purposes, like making money or building a career, are always subject to external change. Inevitably, then, the things that feed our outer purposes will at some point let us down.

Say you believe your purpose is to raise children. This means that you depend on your children depending on you! What happens when they grow up, leave home and no longer need you? 

In addition, if you define your purpose as being the best at something, it also means you depend on others being "worse" than you. In this way, meaning in your life depends on another person's failure.

We should remember that it's not the goals or actions themselves but rather the state of consciousness from which they come that determine whether something is motivated by ego. 

A person working as an activist for homeless people, for example, may have an outer purpose that seems selfless and noble, but she might be doing it for the selfish inner reason of ego: to gain accolades or to feel superior to others working in "lesser" fields.

### 7. The two main elements of living an enlightened life are acceptance and enjoyment. 

Do you ever wish you could dial down the pressures of your daily life to experience more peace, perhaps even a sense of enlightenment? 

Then you need to learn how to enjoy and accept life as it is right now. But how do you do this?

Acceptance is the willingness to do whatever you need to do in the moment, in a peaceful, open fashion — even when a task is not inherently enjoyable, perhaps even stressful like filing your taxes, taking a driving test or doing the laundry. The aim is to reach a state of mind where you can accept the task and be at peace with it. 

If you cannot bring yourself to enjoy — or at least accept — what you're doing, you should stop the activity. If you carry on without a joyful or accepting mind-set, you're surrendering responsibility for the one thing you have true control of in your life — your state of mind. Only you can control the way you deal with the situations life presents.

When you attain a state of enlightenment, your motivation to act will stem from enjoyment rather than feelings of desire or wanting.

The good news is that enjoyment happens naturally when you are able to focus on the present moment. Doing this enables the joy of being to move through your body — this is the joy of consciousness!

Remember, though, that even if you're able to affect the lives of others through the enjoyment and enthusiasm you show, you mustn't forget that you're still human. Maintaining humility will keep the ego in check when it feels the need to crow about any achievements or successes you've had.

> _"If you are not taking responsibility for your state of consciousness, you are not taking responsibility for life."_

### 8. Final summary 

The key message in this book:

**The human ego fuels a dangerous, self-sustaining cycle of violence and destruction within each person and in the world at large. Learning the destructive nature of the ego is essential to letting go of it and experiencing the satisfaction of non-judgment, non-resistance and non-attachment. This is the way to achieve both internal joy and greater world peace.**

Actionable advice:

**Just breathe.**

Most of us are too distracted by the constant litany of thoughts and anxieties within ourselves to feel our inner aliveness. Focusing on your breath will help you reconnect with this. Simply take two or three deep breaths and feel your limbs, fingers, toes, stomach and chest swell with air as you fill yourself with life. It's an incredibly simple and powerful exercise, yet we so often neglect to do it with the intent to calm and focus ourselves. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Eckhart Tolle

Eckhart Tolle is one of the world's most renowned and influential spiritual teachers. His previous bestseller, _The Power of Now_ (also available in blinks), was translated into over 33 languages.

