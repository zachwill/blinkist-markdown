---
id: 56e6e15e9854a500070000fb
slug: a-planet-of-viruses-en
published_date: 2016-03-16T00:00:00.000+00:00
author: Carl Zimmer
title: A Planet of Viruses
subtitle: None
main_color: 6F5EC3
text_color: 51458F
---

# A Planet of Viruses

_None_

**Carl Zimmer**

_A Planet of Viruses_ (2011) takes you on a whirlwind tour into the hidden world of viruses. You'll discover how our understanding of these tiny, abundant organisms has evolved over time and how our lives are influenced by them, from their power to kill to their protective properties.

---
### 1. What’s in it for me? Take a peek into the astonishing world of viruses. 

Every year an influenza epidemic sweeps across the world, leaving hundreds of millions sniffling and coughing in its wake. Although it kills a number of people every year, there are far worse and far deadlier viruses around. The last Ebola outbreak killed thousands, and it could have been a whole lot worse. In our globalized world, viruses can spread farther and faster than ever before. 

But are viruses all bad? If they can do so much harm to us, maybe they can also keep other dangers in check?

In these blinks, you'll learn

  * why the Romans rubbed their faces with mice;

  * how viruses save lives; and

  * why Ebola is so hard to eradicate.

### 2. The common cold has been a nuisance for thousands of years. 

The common cold is a relatively harmless, but rather annoying sickness we're all forced to deal with now and again. Our parents had it, our great-grandparents had it, and even our most ancient ancestors had to deal with fevers, coughs and runny noses.

The common cold is typically caused by the _rhinovirus_, a virus that was a nuisance to the Ancient Egyptians too. In the 3,500-year-old medical text _Ebers Papyrus_, an Egyptian scholar documents the symptoms of "resh," including a persistent cough and excess mucus in the nose. Sounds familiar, doesn't it? 

While the symptoms of the common cold in ancient times match what we see today, methods of treatment have changed considerably. Egyptians were given a rather sensible prescription of herbs, incense and honey to apply around the nose, whereas the Romans were convinced that rubbing a mouse around their nose was the best way to defeat the sniffles! 

History also provides us with a range of wildly different explanations for the common cold. Ancient Greeks chalked it up to an imbalance of the four bodily fluids, i.e., blood, yellow bile, black bile and phlegm. Seems rather unbelievable now, but the common cold's cause eluded physicians even up to 1900, when physiologist Leonard Hill posited that colds were caused by moving from hot air to cold air, the way you might do when taking a morning walk. 

Fortunately, scientific research from the early to mid-twentieth century helped uncover the true cause of the common cold. We now know that the rhinovirus is responsible. So do we know how to defeat it?

Despite centuries of experimenting with different remedies for the common cold, we still lack a fool-proof cure. The answer may lie in attacking the genetic code of the virus. But it's worth asking whether we should bother finding a cure in the first place. After all, the rhinovirus and other harmless viruses teach our immune systems to react appropriately to benign infections, making it better able to deal with viruses that are serious threats to our health. 

What are these deadly viruses? That's what we'll find out in the next blink.

### 3. Influenza can be deadly and will continue to surprise us. 

Influenza, or the flu, is another viral infection we're all familiar with and that has plagued humans for centuries. Today, we know a lot more about influenza than we did in the past. And yet, we still struggle to find a reliable cure for this sickness, which can be deadly. 

The influenza virus works by destroying the protective membrane lining a person's airways, making them susceptible to pathogens present in the air they breathe. This in turn can lead to fatal lung infections. Up to 50 million people died as a result of influenza during the 1918 global flu epidemic. Even this year the flu is likely to kill between 250,000 to 500,000 people. 

What is it that makes influenza so dangerous? For starters, it's not just one virus. There are many different types of flu viruses, and they are constantly shifting, evolving and swapping genes with each other. It's rather hard to create a cure for a virus that never stays the same! 

The way that influenza spreads also adds to its deadliness. Influenza viruses are typically carried by birds and usually don't spread to other species. But when it does happen, the results can be disastrous. During the 2009 H1N1 "Swine Flu" pandemic, three forms of bird flu virus infected pig populations, eventually combining with each other to form one deadly super virus, which in turn joined with a pig flu virus that made it possible for humans to contract the sickness. 

Frighteningly enough, we simply can't know when a new strain will travel from birds to humans. But we aren't completely helpless in the face of the flu. Even the simplest habits, such as washing our hands, significantly reduces the risk of influenza.

### 4. Some viruses are crucial to our survival. 

Would you believe that some viruses are actually good for you? If not, it's time to take a closer look at the biology of a particular kind of virus: _bacteriophages_. 

Bacteriophages, or "phages" for short, are viruses that have the power to cure diseases. How? They eat bacteria. Bacteriophages were discovered and named by Canadian-born doctor Felix d'Herelle while studying the feces of French soldiers suffering from dysentery in WWI. 

By filtering the stool samples, d'Herelle was able to isolate the bacteria _Shigella_ that caused the illness. And, he noticed that in some places, the _Shigella_ was being destroyed by none other than an additional virus. After testing the virus on himself, he used it to treat his patients.

Despite this, the idea of injecting live viruses into patients made many doctors uncomfortable. By 1940, phages were superseded by antibiotics in the treatment of diseases. Today, phages have another important role in our world: as protectors of the water. 

Every liter of seawater is home to about 100,000,000,000 viruses. Add up all the liters of water in the ocean and that's a whole lot of viruses — roughly 10^30! Luckily, marine phages make up part of this virus population. Phages destroy between 15 and 20 percent of nasty ocean bacteria each day, which prevents cholera and other harmful illnesses from spreading. Without phages, water-borne illnesses would wreak a lot more havoc.

Aside from phages, there's another form of virus that has been a big help. _Endogenous retroviruses_ are viruses generated within the bodies of animals, including humans. These viruses insert their genetic information into the DNA of the host. When the host's cells divide, the DNA of the virus is also replicated, thus shaping our genes. 

One important endogenous retrovirus is HERV-W, discovered in 1999 by Jen-Luc Blond among others. One of the genes of this virus produces syncytin, a protein essential for the bonding of cells in the outer layer of the placenta. In other words, without this retrovirus, we wouldn't be able to carry children. In this way, we can thank viruses for the survival of our species!

> _"In our most intimate moment, as new human life emerges from old, viruses are essential to our survival. There is no us and they — just a gradually blending and shifting mix of DNA."_

### 5. A knowledge of the history of viruses prepares us for our future with them. 

Scientific knowledge of viruses is constantly developing. Can this tell us anything about the role of viruses in our future? 

Take HIV for instance. Since it was discovered in the 1980s, 60 million people have contracted the virus, and nearly 30 million died as a result. HIV works by fusing the host's immune cells together, inserting its DNA and multiplying at an incredible rate. In its efforts to fight off the virus, the immune system is weakened, making the host susceptible to dangerous diseases such as pneumonia. 

Research into HIV-1, the most infectious strain, has lead scientists to one compelling hypothesis. It's likely that HIV-1 was carried by monkeys in Cameroon. The strain was contracted by monkey-eating hunters in isolated villages. It wasn't until the arrival of colonial settlers in the early twentieth century that the virus really began to take hold. Investigating the history of HIV has allowed scientists to retrace the evolution of HIV, which in turn allows them to uncover weaknesses in the virus's structure. 

It also helps to look to the past when investigating viruses because, like it or not, history tends to repeat itself. Mosquito-borne viruses are a prime example. 

People contracted the West Nile virus via mosquito bites, causing encephalitis (inflammation of the brain) to develop, leading to fever, paralysis and even fatality. This virus traveled from the Eastern Hemisphere to the United States with the birds that carried it, and was then spread by mosquitoes that carried the virus from birds to humans in North America. 

The West Nile case is likely to repeat itself with different mosquito-borne viruses. North American birds have proven to be good carriers of the West Nile virus, and mosquitoes will continue to thrive as the US climate becomes warmer and wetter. Researchers must keep this in mind and learn from mistakes made during the West Nile case to combat viruses more effectively in the future.

### 6. Virus epidemics are dangerously unpredictable. 

Advancement in modern medicine allows us to be more effective at fighting viruses than ever before. But how safe are we really?

The problem is that we simply can't predict when a virus such as Ebola will take root. The first outbreak of the disease took place in 1976 in Guinea, where nightmarish symptoms such as violent vomiting, explosive, bloody diarrhea and even bleeding from the eyes caused the deaths of 318 sufferers. 

Since then, Ebola has reemerged several times, most recently in December 2013. And, these outbreaks cover more and more ground. In 1976, the virus traveled only between remote villages, and infected relatively fewer people. 

Since then, the susceptible regions of Guinea and Liberia have become increasingly connected to the rest of the globe. This allows the virus to travel rapidly, making it trickier to quarantine and more likely to infect and kill individuals. The December 2013 outbreak left more than 10,000 people dead. 

Between outbreaks, dangerous viruses including Ebola disappear from human populations. But they continue to circulate and develop among wild animals. It is not possible to know ahead of time when these viruses will make the jump and infect humans. 

Our inability to predict the behavior of viruses isn't the only factor that makes them a threat to our future. Our ability to synthesize these viruses means that they could be used in biological warfare in the future. Scientists are now able to sequence DNA from scratch, allowing them to rebuild relatively simple diseases such as polio after investigating their structure. 

Viruses such as smallpox were sampled for research purposes. Although it was ordered that all vials of smallpox were to be destroyed, it is possible that some are still intact. Smallpox, a complicated virus, is more difficult to sequence than polio, but the feat is certainly achievable. The calculated use of viruses as biological weapons would have devastating consequences for future generations on our planet.

### 7. Final summary 

The key message in this book:

**Viruses have been a part of human life since ancient times. Although they can cause suffering, they also keep our environment safe. Through historical and scientific research, we know much more about viruses today than ever before. Still, we can't predict when outbreaks will occur, and we can't prevent their power being used for evil in biological warfare. Continued investigation into the frightening and fascinating world of viruses will ensure we're prepared for the future.**

Actionable advice: 

**Antibiotics will not cure the common cold.**

Why do people believe you can take antibiotics to treat a cold? The common cold is caused by a _virus_, not by bacteria, and antibiotics are only effective in combating bacteria. But sometimes antibiotics can cause bacteria to evolve into more resistant mutations. That means they won't only get rid of your cold: they'll put your body in danger by creating untreatable strains of bacteria. So next time your nose starts running, stick to bed rest, lots of fluids and vitamin C! 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Spillover_** **by David Quammen**

_Spillover_ takes a look at where the world's most deadly diseases come from, explaining how humanity is at risk from viruses and bacteria hiding in animal populations. It also shows that the closer we get to the natural habitats of wild animals, the greater our risk of coming face to face with deadly foes: pathogens.
---

### Carl Zimmer

Carl Zimmer is a columnist for the _New York Times_ and a lecturer at Yale University, where he teaches how to write about science and the environment. He writes for _National Geographic_ and is the author of thirteen books, including _Parasite Rex_ and _Microcosm_.

