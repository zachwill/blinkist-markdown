---
id: 538f2c443761300007020100
slug: a-quiet-word-en
published_date: 2014-06-03T14:35:13.000+00:00
author: Tamasin Cave and Andy Rowell
title: A Quiet Word
subtitle: Lobbying, Crony Capitalism and Broken Politics in Britain
main_color: CB292A
text_color: B22425
---

# A Quiet Word

_Lobbying, Crony Capitalism and Broken Politics in Britain_

**Tamasin Cave and Andy Rowell**

_A_ _Quiet_ _Word_ explains what lobbyism is, how it works and why it can be dangerous for democracy. The authors reveal the extent of lobbying today, detail different strategies used by lobbyists to influence governments, and offer a solution to help defend democracy.

---
### 1. What’s in it for me? Discover the dark influence lobbying has on politics. 

Many people nowadays feel that politicians care more about the interests of big corporations than of citizens — and that politics is simply one giant corrupt business itself. 

Although this point of view might be pessimistic, the influence of companies and businesses on politics should not be underestimated: commercial players spend _billions_ to influence governments through lobbyism and public relations strategies. 

This book focuses on the situation in the United Kingdom to explain in more detail what the so-called _influence_ _business_ is, how it works and why it is threatening democracy. 

In these blinks you'll find out why — at least in theory — lobbyism isn't all bad. 

And you'll find out how an army of British shopkeepers fighting to defend their interests were actually agents for the tobacco giant Philip Morris in disguise. 

Finally you'll find out how oil giant Shell tricked critics into allowing them to extract gas in Peru.

### 2. Contrary to what you might think, lobbyism isn’t necessarily a bad thing. 

Generally speaking, the word "lobbyism" refers to any attempts of citizens and groups of citizens to influence the political process — for example, meeting with a senator to convince her to fund your social project. But for most people, lobbyism has a negative connotation: first and foremost it is associated with big business, corruption and secretive scandals.

However, the issue is more complex.

Some say that because most of the time lobbyists represent commercial interests, lobbyists shouldn't be heard by the government at all.

This suspicion follows from the fact that most lobbyists work for big companies like Nestlé or Google, and industries like the tobacco or the sugar industry. Seen in this way, lobbying seems to necessarily undermine good government, because most companies don't care about the common good — the issues important to the society as a whole — they only care about profit. For example, tobacco industry lobbyists fight regulations that might prevent people from smoking, because this would reduce their profits.

However, at least in theory, lobbyism can actually help governments do a good job.

Despite its bad reputation, lobbyism can still bring about good governance by keeping politics and outside interests in touch. To make intelligent and informed decisions, politicians require insights from external actors such as businesses — and lobbyists can provide just those insights. For example, lobbyists can provide information about the current problems and threats companies face, and thus help the government adapt accordingly to support the economy. Furthermore, lobbyists are often experienced in legislation and politics themselves, which allows them to reduce the workload for politicians by helping them with tasks like drafting the paperwork for a new law.

### 3. Lobbyism in Britain has become a professional business with a variety of actors. 

Lobbyism, for many people, basically means bribery: corporate agents pushing money-stuffed envelopes into the hands of politicians under the tables of fancy restaurants. While this may have been true in the past, today lobbying is a much more complex industry.

In Britain, a very influential and professional lobbying industry has emerged during the last century to drive forward business interests. And just like in the United States, the UK lobbying industry originally emerged to tackle the dangers democracy poses to the interests of industrialists.

Simply speaking, during the twentieth century, more and more people got the vote, and started to demand social change — including the redistribution of wealth. So the rich industrialists created lobbying to ensure that the politicians would write laws defending the right to their riches.

This has made lobbying an essential part of British politics: today, London has the third largest lobbying industry after Washington and Brussels.

So what is the lobbying business actually made up of?

Britain's gigantic lobbying business today consists of a variety of actors who help push forward the interests of businesses and companies.

One main group of actors are the _lobbyists_ paid to influence politics through personal relationships to politicians.

Lobbyists meet with politicians in order to convince them to enforce or stop policies that could help or harm their interests of their clients. For example, lobbyists for the meat industry might try and convince politicians that looser slaughterhouse rules are good for the economy.

But it's less well known that on top of the lobbyists, there are other powerful players in the lobbying business. There is the _public_ _relations_ _industry_, which works to improve the public image of big businesses. And then there are the _think_ _tanks_ that companies pay to do research that will support their interests and indirectly influence public debates.

For example, the International Policy Network is an institution funded by the oil giant Exxon in order to misrepresent scientific knowledge about climate change, support climate skeptics, and discredit environmental regulations.

### 4. Lobbying activities can undermine the democratic process. 

One might argue that in a democracy big companies have the same right as anyone else to campaign for their interests. However, a closer look at the problem reveals why professional lobbying has become a threat to democracy itself.

Firstly, by using lobbyists, big companies and industries have privileged access to the government and its decision-making powers.

This is because professional lobbyists spend much time and energy bonding with politicians. For example, they will first try to get in contact by sending a congratulatory email after an election, or approach politicians at public events like party conferences in an attempt to form a relationship. The lobbyists can then use these personal connections to influence the government to pass laws and policies in the interest of the businesses and companies that pay them. This means that those who can't afford lobbyists — like the public or poorer NGOs — are more or less evicted from the decision-making process.

Secondly, think tanks and public relations companies undermine _rational_ _democratic_ _discourse_ to the benefit of commercial interests.

For a society to be truly democratic, the decision-making process must operate through a public discourse where every voice is equal. However, big businesses and corporations use think tanks and public relations strategies to manipulate public discourses for the benefit of their financiers and clients. Through funded research institutions and questionable media campaigns, corporations ensure their interests are represented and protected in public discourses — again, something the public and less wealthy NGOs cannot afford.

A striking example: the sugar industry has spent much money in recent decades to raise public doubts about the risks linked to excessive sugar consumption. The industry paid scientists, funded research and even academic journals in order to fight other studies and critical arguments.

In the following blinks you'll learn how exactly the lobbying business works in more detail.

### 5. Because lobbying relies so strongly on personal relationships with politicians, many top lobbyists have been or currently are politicians. 

Even though it sounds like something out of political thriller, the clichéd scenario where lobbyists cunningly influence politicians at dinner parties is an actual phenomenon.

This is because personal relationships are the best way to ensure that the interests of a lobbyist's client are represented. This makes networking and socializing with government advisers or important politicians a lobbyist's main task, because this allows them to gain a direct influence. For example, lobbyists often invite politicians to expensive dinners or other high profile social events, like a football cup final.

And once lobbyists have a connection to politicians, they can try to convince them of their position. For example, they could attempt to get the politician to oppose tax increases which would hurt their employer's profits.

But many lobbyists don't have to go looking for politicians to meet — often they've already formed personal relationships with government officials by working or having worked within the government themselves.

For example, it is not uncommon for active politicians, their advisers and other ministry officials to work as lobbyists on the side. Furthermore, many former Members of Parliament in the United Kingdom today work for lobbying agencies, and can prove to be effective lobbyists thanks to their personal connections to the government.

### 6. Besides personal lobbyists, big industries and corporations also use third parties to push their interests. 

As seen in the previous blink, most lobbyists rely on personal relationships. However, big businesses also have to use less direct ways to promote and defend their interests.

When personal lobbying can go no further, because a politician is starting to worry about their perception in the eyes of the voters, lobbyists turn to more subtle methods. Instead of using direct influence, they attempt to get other parties to argue their side: they finance scientists and think tanks, for example, to do biased research.

For example, Frederick Stare, the founder of the Harvard School of Public Health — which was funded by companies such as Coca Cola — was paid by lobbyists to advocate the interests of the sugar industry in the media, and tell politicians that sugar is healthy.

On top of professional researchers, businesses also use certain groups of society to surreptitiously campaign for their interests.

In 2009, for example, the Labour government in Great Britain wanted to impose stricter rules on selling cigarettes. In response, the tobacco industry — and particularly Philip Morris International — mobilized shopkeepers to fight the new regulations.

These third parties are useful because they are more credible than lobbyists.

This is because corporations and businesses can't credibly claim to be fighting for anything other than their profits. On the other hand, third parties — like the thinks tanks and scientists doing seemingly independent research, while actually being paid by big businesses — appear much more credible to the public.

The same is true for the shopkeepers who appeared to be merely representing their own interests against the government. If Philip Morris had challenged the legislation themselves, citizens and politicians would have perceived their protests as a manifestation of corporate greed. By contrast, small shop owners seemed far more sympathetic, and their objections were therefore considered more carefully.

So before you believe that latest piece of research, google its origin and find out who funded it!

### 7. One essential part of lobbyism is dealing with critics. 

The lobbyist industry is not just about promoting certain goals. Another central task is dealing with all those who pose a threat to the interests of big businesses.

And lobbyists don't always fight fair.

One reason for this is that critics can be very dangerous for large corporations, for whom profits come first, and ethical considerations second. For example, Nestlé has been widely criticized for its promotion of baby milk formula to impoverished mothers in developing countries — despite the fact that, most of the time, breast milk is better for children.

The main critics of corporations like Nestlé are social movements and NGOs — non governmental organizations — created to criticize immoral business practices. By raising public awareness, they can damage a company's reputation and lead boycotts — which means reduced profits.

So to maintain high profits for their clients, lobbyists need to find the best way to effectively deal with opponents and criticism.

For this purpose, companies such as Nestlé and Shell have developed subtle public relations strategies to manage criticism. When Shell, for example, planned a project in a sensitive area of the Peruvian rain forest, it pretended to be ready for dialogue, and invited NGOs to an open table.

The catch, however, was that Shell was only willing to talk about how best to proceed — from the very beginning there was no possibility that the project would be cancelled. The more moderate critics accepted Shell's invitation, whereas the more radical critics did not. The result? A divided and less powerful group of critics — which was exactly what Shell had intended.

However, if these methods do not work, the lobbying industry doesn't hesitate to aggressively threaten critics.

For example, whistleblowing filmmakers and scientists get intimidated. After making a critical movie about the banana company Dole, the filmmaker Fredrik Gertten was not only threatened by legal action, but also by aggressive bullying and scare tactics from Dole lobbyists and PR campaigns. Gertten documented this in his later movie, _Big_ _Boys_ _Gone_ _Bananas!_

In the following blinks we'll find out what can be done about lobbyism.

### 8. A more transparent lobbying industry would allow lobbyists' agendas to be discussed publicly. 

As we've seen in the previous blinks, lobbying activities are far from democratic and fair — and this is particularly true in the United Kingdom. Even though the lobbying industry's impact in the United Kingdom is not as big as in the United States — the motherland of the lobbying industry — it is still growing every year.

However, even compared to the United States, Britain's lobbying activities lack _transparency_.

For example, in Britain, almost nothing is publicly known about who is a lobbyist, who she or he is working for, and how much money has changed hands. This is because for over 40 years now, the lobbying industry has managed to hold back governmental regulation and stress that "self-regulation" suffices. And yet transparency is a necessary condition for an open and honest debate about the interests and issues that lobbyists represent.

How could this be achieved?

A register for lobbying activities would force businesses and lobbyists to declare who is lobbying the government and how much money is being spent. Then the public would be informed of the scale and impact of lobbying activities.

In the United States, for example, the public knows how much money the financial industry spent on lobbying from 1998 to 2008 — $3.4 billion — and who worked as a lobbyist fighting against increased financial regulation (i.e., many ex-US government officials).

But for the lobbying activities of the City in London, no such information is available.

This lack of information is dangerous because as long as we don't know the impact and size of lobbying activities, we cannot hold abusive lobbyists accountable and, as a society, discuss the consequences of lobbyism. More transparency would therefore make more democratic a business that has so far operated in the dark.

> _"It is the effort put in to denying transparency that exposes dangerous truth."_

### 9. Despite the obvious problem, there's little hope that the British government will regulate lobbyism. 

Despite the lobbying system's hidden methods, the British public is both aware and concerned about their consequences. Polls show that a majority in Britain believes the government care less about the British public's concerns than the concerns of big corporations.

In recent years, this has led politicians and the government to recognize the need for greater transparency. And many politicians and ministers in Great Britain seem to indeed realize that the impact of lobbyism and the lobbying business as it works today threatens faith in the democratic system.

For example, at the end of his election campaign, British Prime Minister David Cameron gave a speech criticizing the lobbying industries' methods: the invitations to expensive lunches, the ex-ministers and ex-advisers' use of political contacts to find work as lobbyists. Cameron continued, saying that lobbying in the United Kingdom had gotten out of control, with the rich and powerful able to buy too much influence.

Unfortunately, the government has so far failed to effectively regulate the lobbying industry.

As mentioned in the previous blink, an effective register would force all lobbyists to reveal their activities. In September 2013, the British government finally came up with a proposal for such a register. It would force lobbyists to divulge who they worked for, as well as how much cash they spent.

However, by the time the final register passed into law, it proved very ineffective: only a couple of lobbyists were actually affected, and they only have to name their clients, and not how much money changes hands.

It seems the fight for more transparency must go on.

### 10. Final Summary 

The key message in this book:

**Lobbying has become a huge commercial industry that uses various tactics to influence politics and the public for the benefit of for-profit corporations. This poses serious risks to our democracy, as big businesses have more influence over the government than individual citizens. Therefore we must become aware of the influence of lobbyists, and develop effective rules to make the industry more transparent.**
---

### Tamasin Cave and Andy Rowell

Tamasin Cave and Andy Rowell are directors of Spinwatch, a non-profit organization that raises public awareness about government and business propaganda, and the lack of transparency in the lobbying business.

