---
id: 5b46c813b238e1000718d556
slug: a-river-in-darkness-en
published_date: 2018-07-12T00:00:00.000+00:00
author: Masaji Ishikawa
title: A River in Darkness
subtitle: One Man's Escape from North Korea
main_color: 5C3E82
text_color: 5C3E82
---

# A River in Darkness

_One Man's Escape from North Korea_

**Masaji Ishikawa**

_A River in Darkness_ (2000) is the harrowing true story of one man's life in and eventual escape from the brutal dictatorship of North Korea. Born in Japan, Masaji Ishikawa was one of hundreds of thousands of Koreans who moved to the country between the 1950s and 1980s. His memoir chronicles the life of drudgery, terror and endless hardship that awaited them.

---
### 1. What’s in it for me? A captivating chronicle of life in totalitarian North Korea. 

North Korea is in the headlines a lot these days. In its dealings with other nations, it puts on a show of force. Militaristic and menacing, it rattles its expensive nuclear saber at anyone and everyone that crosses its path.

But what's it really like inside the notorious hermit kingdom?

Outsiders rarely scratch the surface of everyday life in the self-proclaimed workers' state. Visitors are closely monitored and only shown what the regime wants them to see. And "tour guides" are always on hand to make sure embarrassing pictures never make it out of the country. 

That leaves the courageous North Koreans who've managed to escape the clutches of the totalitarian state.

Masaji Ishikawa is one of the few men and women who have attempted the perilous border crossing into China and lived to tell the tale. _A River_ _in Darkness_ is his harrowing memoir of growing up in North Korea. Peeling back the propaganda, he gives us a glimpse of the horrors and hardships that define life in one of the world's most brutal dictatorships.

In the following blinks, you'll learn

  * why thousands of Koreans left Japan to build a new life in North Korea;

  * what it's like to go to school in a totalitarian state; and

  * about Ishikawa's perilous journey to freedom.

### 2. Ishikawa’s family were promised a better life in North Korea, but they were cruelly persecuted. 

Between the late 1950s and mid-1980s, over 100,000 Koreans and 2,000 Japanese citizens left Japan aboard ships bound for North Korea. It's a remarkable chapter in history. It was the first — and remains the only — time such a large number of people left a capitalist country for a socialist one.

But the emigrants were soon confronted with plenty of evidence that this so-called "paradise on earth" wasn't all that it'd been cracked up to be.

The first sign came when they docked. The arrivals — Ishikawa's family among them — were shocked at how poorly attired the North Koreans helping unload the ship were. Their clothes suggested that the locals were poorer than they'd ever been in Japan.

Their first meal was another red flag. They were given terrible-smelling dog meat. Hardly anyone in their group managed more than a mouthful.

Ishikawa's family spent the next week confined in a small, cold room before being assigned to their future home in the village of Dong Chong-ri.

It was a pretty out-of-the-way spot, but the family didn't have connections in the Korean Workers' Party or the League of Koreans. Knowing the right people was the only way of securing a residence in the capital of Pyongyang, where the best opportunities could be found.

Things didn't improve when they arrived in their new home, either. Their neighbors regarded them as Japanese. Discrimination was commonplace.

Take Ishikawa's first school day. One of his classmates called him a "Japanese bastard" as soon as he entered the classroom. Other students made snide remarks about his fancy watch and bag.

Such items were uncommon in North Korea, and most students usually wrapped their belongings in a cloth that they carried with them. Ishikawa quickly learned to do the same.

But he wasn't the only family member struggling to fit in. His mother had studied mathematics and worked as a nurse — but this didn't impress the village's party officials. They refused to give her a job until she learned Korean.

Not having anything else to do, she wandered the mountains looking for things she could pick and cook later. That helped supplement the meager diet the family could afford on the small salary Ishikawa's father earned as a farmer.

### 3. Ishikawa’s school years were defined by strict obedience, endless propaganda and rigidly enforced social distinctions. 

Ishikawa was determined to help improve his family's lot. If he studied hard, he thought, he'd be able to attend university. That would put them all on the path to a better life.

But he soon found out that that wasn't going to happen. His career had already been mapped out by the authorities. Indeed, his place in society was predetermined by his ranking in the caste system.

North Korean society recognized three paths from childhood into adulthood. If you came from a good family and were smart, you went to university. If you were strong, you joined the military. Everyone else became a laborer.

Despite slaving over his schoolwork, Ishikawa was classified as "hostile" by his teacher. That was the very lowest rung of the social ladder. Whatever he did, he'd never be given an opportunity to pursue his dreams.

When he was instructed to write down what he wanted to do in life, Ishikawa simply wrote factory work. That was better than being a farmer, since industrial laborers were at least theoretically eligible for promotion. But even that position was denied him; he'd have to become a farmer like his father.

He'd first have to finish school, however, which was an unrelentingly harsh place.

On the one hand, there was endless propaganda designed to brainwash students. Alongside math and science, children had to study the revolutionary changes initiated by the "Great Leader," Kim Il-sung.

This had more of an effect on some children than on others. As an outsider, Ishikawa was naturally skeptical, but he still had to play along to avoid trouble.

Then there were grueling social rituals. Every year, for example, school pupils were tasked with collecting two rabbit pelts that could be used to make cold weather gear for soldiers.

Catching the rabbits was hard enough; keeping them alive long enough was next to impossible. Food supplies were so short that families often had to eat the rabbits their children had caught. Fur could also be sold on the black market.

But if a pupil failed to bring two rabbits to school on the appointed day, he would face harsh punishment. The only escape was if his parents had spare cigarettes or alcohol to bribe the teachers.

> _"They'd only ever known bondage. North Koreans didn't have anything to compare their country with because they'd never experienced anything else."_

### 4. Repression in North Korea was so severe that it ended up dehumanizing its citizens. 

In the spring of 1968, things suddenly got a lot worse. A convoy of military trucks came screeching into Ishikawa's village. Soldiers leaped out of the vehicles and declared that the settlement was now a garrison. What exactly the soldiers were protecting the locals against remained unclear.

Once they'd settled in, the army presided over a ferocious regime of repression. Kim Il-sung's old comrade in arms Kim Chan-bon headed up the new order.

One morning, two terrifying soldiers turned up on Ishikawa's doorstep. They ordered the family to evacuate the house immediately.

When Ishikawa asked them why they were supposed to leave their home, the soldiers laughed in his face and made fun of his low social standing. The family started packing their bags while the military men stomped around and swore at them. They realized that they'd have to move to the nearby settlement of Pyungyang-ri.

But they weren't alone. The soldiers tormented the whole village. They stole equipment from the farm factory and confiscated the animals that laborers kept for food.

This reign of terror lasted a year. And then, one day, they packed up and left as quickly as they'd arrived.

As it turned out, Kim Chan-bon had been an effective military modernizer. Too effective, in fact. His rise through the ranks had begun to trouble the paranoid Kim Il-sung, who, desperate to prevent any challenge to his own power, had had Chan-bon purged.

That was par for the course in a totalitarian dictatorship. The government controlled everything and kept close tabs on everyone.

But the government wasn't only omnipotent; it was also massively corrupt.

Take food. The state controlled the distribution of foodstuffs through a strict rationing system. But some received special favor. Those who had connections to powerful figures were given more than even those who needed it most. The elderly and sick often went hungry.

Nepotism gradually corroded every aspect of life. North Koreans relied on bribes and theft to get essentials, a system that gradually dehumanized them entirely.

Ishikawa vividly remembers carrying a sick neighbor on his back to a supposedly free clinic. When he arrived, he asked the doctor to help the sick man but was met with scorn. The doctor wouldn't help anyone unless they paid him money or bribed him with alcohol and cigarettes!

> _"The penalty for thinking was death. I can never forgive Kim II-sung for taking away our right to think."_

### 5. The North Korean famine killed millions and the only way to survive was crime. 

On July 8, 1994, news broke that the Great Leader Kim Il-sung had died. The entire nation was in a state of shock. Ishikawa remembers people hysterically crying in the street. His emotions also got the better of him. A mixture of shock, fear and relief overwhelmed him. What did the future hold in store?

But the head of state's death didn't improve life. In fact, things were about to get a whole lot worse.

Between 1991 and 2000, the country was wracked by famine. Around three million North Koreans died of starvation and hunger-related illnesses.

The trigger was a spell of extremely cold weather and a series of floods in 1994 and 1995, which destroyed crops and farming infrastructure. The already fragile food supply was stretched to its limits.

Initially, rations were distributed — everyone got one and a half pounds of grain. But as the situation worsened, the deliveries became more and more irregular before drying up completely. Soon enough, people were trying to make do with three days' worth of food per month.

Hunger was everywhere. It was all people could think of. But, for many, relief never arrived. They succumbed and died where they stood. Starving children roamed the streets in search of scraps.

Desperation drove thousands of North Koreans to desperate measures. Crime was often the only means of survival.

Ishikawa heard tell of a man who'd killed his wife so he could eat her, for example. It was an extreme act. Cannibalism was punished by public execution.

Then there were everyday shakedowns. One day, a gangster visited Ishikawa's father and offered him a deal. If he managed to sell a whale's penis, he could keep a share of the profits.

Whale penises are highly valued in traditional Chinese medicine and fetch high prices on the market. But when Ishikawa's father tried to sell it, another man robbed him. It was clear that he'd been conned.

Sure enough, the gangster returned and demanded his money or the return of the penis — both impossible demands.

The hoodlum started turning up regularly to inflict merciless beatings on Ishikawa's father and sister. In the end, his father couldn't take it anymore. He weakened and grew frail before eventually dying a broken man.

### 6. Thousands of North Koreans brave the deadly Yalu river crossing in search of a better life in China. 

Amid the desperation caused by the famine and the pervasive repression of the state, Ishikawa had a revelation. If he was going to die anyway, he might as well do it while trying to return to Japan. If he was successful, he'd be able to send money back to his family.

So he said his farewells and made his way to the capital, Pyongyang.

The best option for North Koreans trying to escape the country is the Yalu river. The two banks of the waterway mark the North Korean and Chinese sides of the joint border.

But it's a perilous journey. Those who get caught face a horrific death.

The worst story Ishikawa heard was known as the "nose-ring case." A family of four had made it across the river to China. But the North Korean secret police had agents there. The family was caught and dragged back after having a wire inserted through their noses to link them together. Once they'd reached the other side of the border, they were all shot.

The tentacles of the North Korean state extend deep into China. The two countries share a "friendship signed in blood" that stretches back to their alliance in the Korean War in the early 1950s. That means they cooperate closely. China regularly returns North Koreans to the "home" they fled from.

But Ishikawa was determined.

He took the train to Hyesan, a city on the border. From there, he walked to the Yalu river. Hiding in the bushes, he saw that rifle-wielding soldiers had been posted at 50-yard intervals along the river bank. He started to doubt he could make it.

Then, on his third night by the river, he had a stroke of luck. The heavens opened and torrential rain obscured the soldiers' view of the river. Ishikawa took his chance and started swimming.

But, just as he was about to reach the other side, the current dragged him toward a rock. He hit his head and lost consciousness.

After two days of unconsciousness, he awoke. And to his surprise, when he looked around, he saw a happy dog wagging its tail. After a moment, it dawned on him that the dog wasn't for eating — it was a pet!

That's when he knew he'd made it. He was in China.

### 7. Even after reaching China, Ishikawa faced a perilous journey back to Japan. 

Once he'd recovered from his dramatic escape, Ishikawa contacted the Japanese Red Cross. He told them his life story. The organization was astonished. Ishikawa was the first former Japanese citizen who'd managed to escape North Korea.

They put him in touch with the embassy in Beijing. A week later, his story had been verified. He was cleared to return to Japan! But getting there wasn't a simple matter.

Ishikawa lived in constant fear. Every knock on the door might be the North Korean secret police. He was given a place to stay, but sleep evaded him. He was tormented by the stories he'd heard of escapees who'd been caught.

Caution was essential. He didn't leave his room once while waiting for his visa to be approved. Even the maids and cooks in his hotel might be in the pay of the police. He waited five minutes before opening the door after someone had knocked.

Finally, he received a ticket to Japan from Dalian city. But even good news was terrifying. What if the North Koreans had tapped the phone and heard the news, too? Ishikawa insisted on being moved immediately.

The Japanese consul's wife lent him one of her husbands suits to smuggle him past the police stationed outside the hotel. Leading him into the garage, she told him to crawl through a specially dug tunnel. When he emerged at the other end, he found a car waiting to take him to the airport.

On October 15, 1995, Ishikawa finally arrived back in Japan. It was the first time he'd been on Japanese soil in 36 years.

But assimilating into his new home was difficult. He struggled to adapt to Japanese culture and couldn't find a job. He lacked employable skills and had to rely on government welfare.

And his past haunted him even when he did make headway. A job with a cleaning company, for example, came to an end once his colleagues started speculating that he was from North Korea. He quit before the authorities could catch up with him.

Isolated and unemployed, he couldn't send money back to his family. The last letter he received from them informed him that his wife and daughter had died of starvation.

### 8. Final summary 

The key message in these blinks:

**Ishikawa's family left Japan and moved to North Korea after being promised a better life. But once they arrived, they found themselves trapped in a totalitarian nightmare. They faced endless hardships and the merciless repression of the all-powerful state. In the midst of a famine, Ishikawa hatched a daring escape plan. Back in Japan, he wrote his memoirs — a harrowing chronicle of human suffering and courage.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Nothing to Envy_** **by Barbara Demick**

_Nothing to Envy_ (2010) presents fascinating first-hand anecdotes from North Korean defectors, giving intimate insights into the lives of North Koreans under the rule of Kim Il-sung, Kim Jong-il and Kim Jong-un. The thousands of refugees who arrive in South Korea each year bring with them stories of famine, repression and an isolated nation that has fallen out of touch with the developed world.
---

### Masaji Ishikawa

Masaji Ishikawa was born in 1947 to a Korean father and a Japanese mother. in 1960, he moved with his parents and three sisters to North Korea. Promised a better life in the new workers' state, the family found themselves trapped in a totalitarian nightmare. Ishikawa finally managed to escape and return to Japan in 1996. His memoir, _A River in Darkness_, is an Amazon Charts Most Read and Most Sold Book.

