---
id: 5405ee756632650008450000
slug: a-short-history-of-nearly-everything-en
published_date: 2014-09-02T00:00:00.000+00:00
author: Bill Bryson
title: A Short History of Nearly Everything
subtitle: None
main_color: 494691
text_color: 494691
---

# A Short History of Nearly Everything

_None_

**Bill Bryson**

_A_ _Short_ _History_ _of_ _Nearly_ _Everything_ offers an enlightening summary of contemporary scientific thinking relating to all aspects of life, from the creation of the universe to our relationship with the tiniest of bacteria.

---
### 1. What’s in it for me? Expand your knowledge of the universe and the mysteries of our world. 

How did we get here? Where did the universe come from? What _is_ the universe?

Great thinkers and scientists have been tackling these questions for millennia, but only now are we beginning to come close to creating a complete picture of our fascinatingly complex universe.

These blinks will give you a crash course in all of the major existential questions. You'll learn how the universe was formed, how life came to be and how the world's great minds came up with their groundbreaking ideas.

But as much as science has brought us in terms of our understanding of the world, many questions are yet unanswered. The many life forms living in the depths of our oceans, much of what makes up the universe, and even elements of the world beneath our feet still remain shrouded in mystery.

In these blinks, you'll also discover

  * why even though extraterrestrial life probably exists, you won't see a UFO anytime soon;

  * why we owe our very existence to the good graces of bacteria; and

  * how many things you share in common with a banana or a fruit fly.

### 2. The Big Bang theory suggests the universe was formed by a singularity in a brief moment. 

In 1965, two radio astronomers puzzled over a strange noise they noticed while experimenting with a communications antenna. It turned out that this noise wasn't just an annoyance.

The sound originated 90 billion trillion miles away, at the very moment of the universe's creation: an event now known as the _Big_ _Bang._

Although the discovery was accidental, it earned the astronomers the Nobel Prize in physics and helped popularize the Big Bang theory. This states that our universe began from a single point of nothingness called a _singularity_, a point so compact that it has no dimensions.

It is in this single, dense point that the building blocks of the universe were once confined. Suddenly, for reasons yet unknown, this singularity exploded in "a single blinding pulse," flinging the future contents of our universe across the void.

Although the reasons why the "bang" happened are still unknown, scientists have more clarity about what happened afterwards.

In the Big Bang, matter, or the contents of that singularity, expanded so rapidly that the entire universe formed within the time it might take you to assemble a sandwich. Practically immediately after the explosion, the universe inflated dramatically, doubling in size every 10^-34 seconds — that is, very very quickly. 

We know that, alongside the fundamental forces that govern our universe, 98 percent of all matter in the universe was created within a mere three minutes. The universe now spans a diameter of at least one hundred billion light years, and continues to expand, even now.

### 3. The enormity of the universe makes it likely that there are other thinking beings out there. 

The universe is so gigantic that it's almost beyond imagining. Astronomers estimate that there are around 140 billion galaxies in the universe that we can actually see.

So imagine if all those galaxies were frozen peas: you'd have enough peas to fill a large auditorium!

For quite some time, scientists weren't sure just how large the universe actually was. That is, until Edwin Hubble came along.

In 1924, Hubble demonstrated that a constellation once thought to be a gas cloud was actually an entire galaxy, located at least 900,000 light years away.

This fact opened our minds to the idea that our universe doesn't just consist of the Milky Way galaxy — where Earth is found — but many other galaxies too. In other words, the universe had to be far more vast than anyone had ever supposed.

With this revelation also came the disillusion that humans could be the only thinking beings in the universe.

According to professor Frank Drake's famous equation from 1961, he suggested that it is possible we are merely one of millions of other advanced civilizations.

Drake came to this conclusion by dividing the number of stars in a selected part of the universe by the number that were likely to support planetary systems. He then divided that number by the number of systems that could theoretically support life, finally dividing that number by the number on which life might then evolve to become intelligent.

Although the number shrinks enormously with each division — even with the most conservative inputs — Drake's estimations on the number of advanced civilizations within the Milky Way alone always comes to somewhere in the millions.

However, because the universe is so enormous, the average distance between any two hypothetical civilizations is estimated to be _at_ _least_ 200 light years — with just one light year being the rough equivalent of 5.8 trillion miles. So even if alien civilizations do exist, their potential distance away from us keeps the idea of a casual weekend visit in the realm of science fiction.

Contemplating the size of the universe may leave you feeling a little dizzy! Now, the following blinks will discuss how we learned to measure the Earth itself.

> _"So even if we are not really alone, in all practical terms we are."_

### 4. Newton made sense of how the Earth moves, how it is shaped and how much it weighs. 

Isaac Newton was an eccentric scientist. In addition to experimenting with poking a needle between his eyeball and eye socket, and staring into the sun for as long as he could stand, he was also a brilliant and influential mathematician.

His groundbreaking work, _Principia_ _Mathematica,_ completely changed the way we think about motion.

In addition to laying out Newton's three laws of motion, _Principia_ _Mathematica_ also explains his universal law of gravitation, which states that all bodies in the universe — large and small — exert a pull on every other body.

These laws made it possible to take measurements that were previously impossible. For example, Newton's laws gave us a way to estimate the weight of the Earth.

His laws also helped us understand that our Earth isn't completely round. According to Newton's theories, the force of the Earth's spin should cause the globe to flatten slightly at its poles and bulge at the equator.

This discovery was a major blow to scientists who had based their measurements on the assumption that the Earth was spherical.

The French astronomer Jean Picard, for example, had determined the Earth's circumference through a complicated method of triangulation — a scientific achievement which was a great source of pride for the French. Unfortunately, Newton's laws made Picard's measurements totally obsolete.

Newton's laws inspired a whole new understanding of how to measure heavenly objects. Not only did scientists improve their knowledge of the Earth's motion, shape and weight, but also the motions of other planets, tidal motion, and importantly — why our spinning planet doesn't fling us into space!

### 5. Rocks and fossils showed that the Earth was old, but it was only radioactivity that showed how old. 

Although it may seem surprising, our knowledge of the Earth's age is more recent than the invention of instant coffee or the advent of television, even the discovery of atomic fission.

In fact, for the longest time, all geologists could say was that the Earth was _old_.

Although they were able to order various rocks by age — categorizing them by the periods in which the sediment had been laid — geologists had no idea how long any of these periods lasted.

By the turn of the twentieth century, paleontologists had joined the quest to determine the age of the Earth by further dividing these ages into epochs, using fossil records.

But no one could tell how old any of the bones were, with estimates ranging between 3 million and 2.4 billion years.

It wasn't until we gained an understanding of radioactive materials that the Earth's actual age could be determined. 

In 1896, Marie and Pierre Curie discovered that certain rocks released energy without exhibiting any change in size or shape. They named this phenomenon _radioactivity_.

This caught the interest of physicist Ernest Rutherford, who later discovered that radioactive elements decayed into other elements in a very predictable way. More specifically, he noticed that it always took the same amount of time for half the sample to decay — a process known as _half-life_ — and that this information could be used to determine a material's age.

Rutherford then used his theory to date a piece of uranium, finding it to be 700 million years old.

It wasn't until 1956 when Clair Cameron Patterson worked out a more precise dating method that we started getting a real picture of the Earth's age. By dating ancient meteorites, he determined that the Earth was around 4.55 _billion_ years old (plus or minus 70 million years) — which is very close to today's scientific consensus of 4.54 billion years!

As we have seen, understanding the universe is a complicated process. The next blinks will look at scientific attempts to create order from complexity: the theory of relativity and quantum theory.

### 6. Einstein’s theory of relativity had huge implications for understanding the universe at large. 

As a student, Albert Einstein wasn't brilliant. After he failed his first college entrance exams, he ended up working in a patent office. While he was there, however, he got serious about studying physics, and in 1905 published a paper that would change the world completely.

His groundbreaking _Special_ _Theory_ _of_ _Relativity_ explains that the notion of time is relative, and does not progress constantly, as does an arrow.

This concept is unintuitive and difficult to grasp for most, as we don't experience the effects of relative time in our daily lives. For light, gravity, and the universe itself, however, Einstein's theory has huge implications.

In essence, the theory states that the speed of light is constant, meaning that it doesn't change for observers regardless of how fast they may travel. However, the inverse is true for time: if one person travels faster than another, their experience of time will seem slower.

Even more challenging than his special theory, Einstein's _General_ _Theory_ _of_ _Relativity_ totally changed how we look at gravity.

After seeing a workman fall from a roof, Einstein began thinking more about gravity, which was the one element missing from the special theory.

Published in 1917, his general theory proposed that time is interwoven with the three dimensions of space as _spacetime_.

You can think of spacetime like a sheet of stretched rubber. If you place a big round object in the middle, the sheet will stretch and sag slightly. Massive objects, such as the sun, do the same to spacetime.

If you then roll a smaller object across the sheet, it will try its best to travel in a straight line. However, as the smaller object nears the larger object and the slope of the fabric, it will then start rolling downward. In essence, gravity works as a product of the bending of spacetime.

In one elegant theory, Einstein explained to the world how time and gravity function!

### 7. Quantum theory helped explain the subatomic world, but then physics had two bodies of laws. 

The more scientists studied atoms, the more they realized that atoms couldn't be explained by the conventional laws of physics.

Conventional theory stated that atoms shouldn't be able to exist: the positively charged protons in the nucleus should repel one another, causing the atoms to rip apart, while the electrons that orbit them should be crashing into each other constantly.

Scientists overcame this problem by developing a new theory, which revealed the mechanics of the subatomic world.

In 1900, the German physicist Max Planck introduced a _quantum_ _theory_, which said that energy isn't some everlasting thing but instead is created in individual packets called _quanta_, particles even smaller than atoms.

His idea remained mostly theoretical until 1926, when another German physicist, Werner Heisenberg, developed the concept of "quantum mechanics," that sought to make sense of atoms' strange behavior.

At the heart of this discipline was his _uncertainty_ _principle_, which demonstrated that electrons have the characteristics of both particles and waves. As a result, you can never predict with absolute precision where an electron will be at any given moment — you can only determine the _probability_ that it is in a certain point in space.

The introduction of quantum theory provided as much confusion as it did clarity, ultimately dividing physics into two bodies of laws: one for the subatomic world and the one for the larger universe.

The theory of relativity has no influence on this subatomic world, and the quantum theory is entirely incapable of explaining phenomena like gravity or time.

This untidiness frustrated Einstein to the extent that he spent the entire second half of his life trying to come up with what he called a _Grand_ _Unified_ _Theory_. Yet he ultimately failed.

For some, the most interesting things about atoms are the visible things they create, like mountains and oceans. Next, we'll return to Earth and learn how life on our planet is possible at all.

### 8. Though life on Earth is challenging, it’s a wonder of the universe that it even exists at all. 

Despite the extraordinary diversity of life on Earth, our planet is far from a friendly place to live.

In fact, according to one estimate, 99.5 percent of the Earth's habitable space is completely off limits to humans, as we need land and oxygen to live. And even on land we don't have free reign: only 12 percent of the globe's total land mass is habitable.

Some scientists have gone to great lengths to demonstrate just how frail humans really are. Father and son team John and Jack Haldane conducted experiments on their own bodies to show just how tough the conditions are when a human leaves the surface world.

Jack built a decompression chamber to simulate life at the deepest part of the oceans, and in doing so, essentially would poison himself as he experienced the elevated oxygen levels found in the deep sea. During one experiment, oxygen saturation caused him to experience a fit so violent that he crushed several vertebrae.

Considering just how tough it is to live on most of the Earth, it's a surprise that we're here at all!

Looking at the known planets, it's clear that finding a place suitable for life is a rare thing. In fact, a planet must meet four specific criteria to be habitable.

First, it has to be just the right distance from a star — too close and everything burns, too far and everything freezes.

Second, the planet must be able to build an atmosphere to shield us from cosmic radiation.

Third, we would need a moon to steady the many gravitational influences on this planet, essential for spinning at just the right speed and angle.

And finally, timing is everything. The complex sequence of events that led to our existence had to play out in a particular manner at particular times to produce life and avoid catastrophe.

> _"To attain any kind of life in this universe of ours appears to be quite an achievement."_

### 9. We know surprisingly little about the dynamics that rule life in the oceans. 

It's a wonder that we call our planet "Earth" and not "water." Water is literally everywhere!

Just think: our bodies are composed of 65 percent water; and beyond, there are no less than 1.3 billion cubic kilometers of water covering the planet.

Considering how essential water is for life, it's surprising how long it was before we took a scientific interest in the seas.

Even though 97 percent of all water on Earth is found in the ocean, the first real investigation of the oceans wasn't organized until recently. In 1872, a former English warship was sent out for three and a half years to sail the world, sample the waters and collect new species of marine organisms, thus giving rise to a new scientific discipline: _oceanography_.

This exploration continued into the deep seas with two American adventurers, Otis Barton and William Beebe.

In 1930, they set a world record by descending 183 meters into the ocean depths in a tiny iron chamber called a _bathysphere_. By 1934, they dove over 900 meters.

Unfortunately, however, they weren't actually trained oceanographers and didn't have sufficient lighting and tools. All they could report was that the ocean depths were filled with strange things. As a result, academics and scientists largely ignored their findings.

Today, scientists have explored beyond 10,918 meters into the ocean's depths, yet even still, we don't know that much more. In fact, we have better maps of the planet Mars than we do of the seabeds. According to one estimate, we may have only investigated a millionth or even a billionth of the ocean abyss.

There could be as many as 30 million species of sea-dwelling creatures down there — most of which remain undiscovered. Even details of the lives of the most visible ocean creatures, such as the blue whale, remain mostly a mystery.

### 10. Bacteria are Earth’s most abundant life forms, and we’re here because they allow us to be. 

Germaphobes have it tough. No matter how clean you are, you are always covered with or surrounded by an overwhelming amount of bacteria.

Consider this: if you're healthy, approximately one trillion bacteria will be living on your skin!

It's true that bacteria are the most abundant and adaptable of Earth's diverse life forms. In fact, there are so many bacteria that if we could add up the mass of all living things on the planet, these tiny bacteria would account for 80 percent of that total.

One reason for this has to do with how quickly bacteria reproduce. Bacteria are prolific; they can produce a new generation in less than 10 minutes. This means that, without outside influences, a single bacterium could theoretically produce more offspring in two days than there are protons in the universe!

Furthermore, bacteria can live and thrive on almost anything. As long as they have a little moisture, they can survive in even the harshest environments, such as in the waste tanks of nuclear reactors.

Some are so resilient that they appear indestructible. Even when a bacterium's DNA is blasted with radiation, it will simply reform as if nothing has happened.

But thank goodness bacteria are everywhere — they are extremely important to our survival.

Bacteria recycle our wastes, purify our water, keep our soil productive, convert our food into useful vitamins and sugars, and pass along the nitrogen in the air to us — among other crucial things.

In fact, _most_ bacteria are either neutral or beneficial for humans. However, about one in 1,000 bacteria is pathogenic; and even this tiny demographic represents the third-most lethal killer of humans worldwide. Some of the most virulent illnesses, from plague to tuberculosis, are caused by bacteria.

> _"Bacteria may not build cities or have interesting social lives, but they will be here when the sun explodes."_

### 11. Life started spontaneously as a bundle of genetic material that found a way to copy itself. 

Imagine that just the right ingredients from your kitchen cupboard magically started mixing and baking themselves into a delicious cake, and that this cake then began dividing to produce more delicious cakes.

Does that sound strange to you? Even stranger is the fact that groups of molecules, such as amino acids, do just this all the time.

Upon closer inspection, however, this spontaneous process isn't so mysterious. Self-assembling processes happen constantly: from the symmetry of snowflakes to the rings of Saturn, patterned complexity can be found everywhere in the universe.

Thus it seems natural that amino acids would arrange themselves into the proteins that build living organisms. After all, a living organism is merely a collection of molecules. The only real difference between inorganic and organic matter, whether a carrot or goldfish, is the essential ingredients — carbon, hydrogen, oxygen and nitrogen.

So, spontaneous life is possible. But how did it happen? Life as we know it is the result of a single genetic trick that's been handed down through generations, for around 4 billion years.

This moment of creation, sometimes called the _Big_ _Birth_ by biologists, occurred when a tiny bundle of chemicals managed to cleave itself, thus sending a copy of its genetic code into the primordial ooze.

This process eventually created bacteria, which remained the sole life forms on the planet for 2 billion years. These bacteria gradually learned to tap into water molecules, thus creating the process of photosynthesis and filling the world with oxygen.

Then, some 3.5 billion years ago, the world's first ecosystems began to appear in shallow waters. When oxygen levels reached modern levels, complex life forms arrived, divided into those that expel oxygen (like plants) and those that consume it (like us).

So no matter how different living organisms seem, every single living object uses the same genetic dictionary and "reads" the same code. Between bananas and chimpanzees there is much more that is similar than is different.

> _"Whatever prompted life to begin, it happened just once."_

### 12. Though the Earth supports an uncountable number of species, all life can be seen as one. 

To say that there are many different species on the planet is an understatement. Estimates range from 3 million to 200 million. According to a report in _The Economist_, up to 97 percent of the world's plant and animal species are likely still undiscovered. What's more, there isn't even a central registry of the species we already know about, leaving us even further befuddled by the diversity of life on Earth. And yet, despite the differences between and among species, all living things are connected.

In 1859, with the publication of _The Origin of Species_, Charles Darwin explained that all living things are connected, and that species differentiate and become "fitter" through a process of natural selection, thus suggesting a shared common ancestor in the distant past.

Modern investigations into our genes and DNA further suggest that we have more in common than we once thought. For example, if you compare your DNA with any other person's DNA, you would find that 99.9 percent of the code would be exactly the same.

But these similarities don't just exist within species. Believe it or not, approximately half of your DNA would match up perfectly with the DNA of a banana. Moreover, 60 percent of human genes are exactly the same as those found in the fruit fly, and at least 90 percent of human genes correlate on some level with those found in mice.

Stranger still, scientists have discovered that parts of our DNA are interchangeable between species. For example, we can insert human DNA into certain cells of flies that will "accept" this DNA as if it were their own, further suggesting that life originates from a single blueprint.

In this way we can see human beings as archives of a long history of modification, stretching all the way back to when life originally began. Looking at the rich diversity of life seems nothing short of a miracle. Our final blink will look at whether it's possible that this miracle could abruptly end.

### 13. The Earth is always at risk of asteroid collisions, volcanic eruptions or earthquake damage. 

Although the Earth has enjoyed a long period of relative calm, that doesn't mean there aren't existential dangers looming within the solar system or even on our own planet.

In fact, our solar system is quite a dangerous place to live.

The Earth often comes dangerously close to colliding with _asteroids_, rock-like objects that follow various orbits within our solar system. There are at least a billion asteroids tumbling through near space, and many of these asteroids make regular passes near Earth.

In fact, more than one hundred million asteroids larger than ten meters across regularly cross the Earth's orbit. Scientists even estimate that as many as 2,000 of these are large enough to put civilization as we know it in danger.

Even more unsettling is the fact that near misses with deadly asteroids could be happening around two or three times a week, entirely unnoticed. 

Moreover, the Earth has its own "in-house" dangers. Earthquakes, for example, can happen anytime.

An earthquake occurs when two tectonic plates meet and build pressure until eventually one gives way. This is a particular problem for places such as Tokyo, which sits on the meeting point of three tectonic plates.

In addition, a different sort of earthquake, an intraplate quake, can happen far away from plate edges. Since they originate from a much greater depth in the Earth's crust, they are completely unpredictable. 

Volcanoes are also a threat. In 1980, Mount St. Helens erupted in the U.S. state of Washington, killing 57 people. Even though most of the government's volcanologists were actively monitoring and forecasting the volcano's behavior, they didn't expect an actual eruption. And yet, the volcano blew.

This is cause for concern, as an enormous volcanic hot spot is located directly under the western United States. It is predicted to erupt every 6,000 years, leaving a three-meter coat of ash on everything within 1,600 kilometers.

Unfortunately for us, the last time it was active was 6,000 years ago!

### 14. Final summary 

The key message in this book:

**Over** **the** **past** **few** **hundred** **years,** **humanity** **has** **slowly** **accumulated** **pieces** **to** **the** **puzzle** **of** **our** **existence.** **We** **now** **know** **more** **about** **our** **universe,** **our** **planet** **and** **ourselves** **than** **anyone** **could** **have** **once** **possibly** **imagined.** **Yet,** **there** **is** **still** **much** **more** **to** **learn,** **as** **the** **process** **of** **scientific** **discovery** **never** **stops!**

**Suggested** **further** **reading:** **_Where_** **_Good_** **_Ideas_** **_Come_** **_From_** **by** **Steven** **Johnson**

_Where_ _Good_ _Ideas_ _Come_ _From_ examines the evolution of life on Earth and the history of science. The blinks highlight many parallels between the two, ranging from carbon atoms forming the very first building blocks of life to cities and the World Wide Web fostering great innovations and discoveries. In addition to presenting this extensive analysis, replete with anecdotes and scientific evidence, Johnson also considers how individual and organizational creativity can be cultivated.
---

### Bill Bryson

Bill Bryson is an American bestselling author who writes on topics as diverse as the English language, science and travel. He is also well-known for his humorous portrait of Great Britain in his book, _Notes_ _From_ _A_ _Small_ _Island,_ voted by BBC4 Radio listeners as the book most representative of their country.

