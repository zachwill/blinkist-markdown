---
id: 5ad3ee4ab238e10006b904e0
slug: a-spy-among-friends-en
published_date: 2018-04-18T00:00:00.000+00:00
author: Ben Macintyre
title: A Spy Among Friends
subtitle: Philby and the Great Betrayal
main_color: 54A1C4
text_color: 346378
---

# A Spy Among Friends

_Philby and the Great Betrayal_

**Ben Macintyre**

_A Spy Among Friends_ (2014) details the life of Kim Philby, a highly respected operative who rose through the ranks of the British secret services during World War II and the Cold War. Though a seeming paragon of British values, he actually spent his career working as a double agent for the Russians.

---
### 1. What’s in it for me? Learn what kind of deeper truths may lie beneath British manners and charm. 

It's hard not to romanticize the British espionage operations of the twentieth century. One moment, you'd be quietly studying Greek lyric verse by the fire of a Cambridge college, then, a finger would tap you on the shoulder and, next thing you knew, you'd be involved in derring-do and escapades across Europe and the Middle East.

In many ways, the story of Kim Philby is a reminder to avoid being taken in by such romance. Philby was a charming traitor who never wavered in his allegiance to Moscow, even as he rose through the ranks of British intelligence. The deaths caused by his treacherous actions may number in the thousands.

Philby remains the quintessential double agent. Indeed, he and his associated Cambridge spy ring inspired John le Carré's _Tinker Tailor Soldier Spy_ as well as numerous other works of fiction and film.

These blinks tell the thrilling tale of a man who worked as a Soviet mole in MI6 for longer than you would even think possible.

In these blinks, you'll learn

  * which indiscreet drunkard was possibly the worst spy of all time;

  * what kind of charm can persuade a nation of your innocence; and

  * what handlers teach recently recruited spies.

### 2. Kim Philby’s nascent Socialist inclinations were formed at Cambridge and in revolutionary Vienna. 

To the Cambridge of 1930, Kim Philby was no outlier. When he went up to the university to read history, he was, like so many other first-year 18-year-olds, distinctly upper class and burning with academic ambition.

However, as his political outlook shifted, his scholastic tendencies were soon overtaken by more controversial occupations. He began moving in a left-wing direction by canvassing for the moderate Labour Party. But a trip to Berlin in 1933 radicalized him. There, he witnessed Nazi thugs demonstrating against Jewish citizens.

By this point, he was personally committed to the Socialist cause, but he was hardly outspoken about it. It's known that he purchased some of Karl Marx's works, but there's no evidence of him ever reading or studying them in any detail, let alone of him preaching Communist ideas.

Though Philby did eventually devote himself to the Socialist cause, he did so far from home, where he was unlikely to be recognized.

Vienna, Austria, 1934. Revolution was in the air. At the time, the country was under the thumb of Engelbert Dollfuss, a right-wing dictator. A Socialist movement had coalesced, and tensions between right and left had reached boiling point.

Within just a few weeks of Philby's arrival in Vienna, Dollfuss began a crackdown. Socialist leaders were arrested, and trade unions banned. A short but violent civil war erupted.

In the ensuing chaos, Philby fell in love with Alice Kohlman, a young Jewish Socialist activist. Kohlman found herself on the proscription list of wanted Socialists. Arrest, if not worse, was imminent. Consequently, Philby married her so that she could flee to Britain and safety. Even though the couple divorced in 1946, it's thought that Kohlman remained Philby's only true love.

After this adventure, and once he was back in Britain, Philby's determination to fight for the Socialist cause was fixed.

### 3. Philby started out as a journalist before being recruited by the secret services. 

A covert operator requires many tools. And Philby was certainly blessed with one of the most important: innate charisma. It was a characteristic that stood him in good stead for his entire career.

After Cambridge, Philby's natural charms were of immense help in his chosen profession. He soon became a popular correspondent at the _Times_. However, his success didn't derive solely from his charm. Along the way, he'd also had a little help from lady luck.

In 1937, Philby was posted to Spain to report on the Civil War from within Franco's forces. One day, a bomb went off near the car in which he was sitting with three other journalists. The others died, but Philby was lucky; he escaped with minor injuries. For this, he was heralded as a war hero and even decorated by Franco himself.

Soon after, once World War II had begun, Philby was sent to France to report on the Nazi advance. The copy he filed crackled with wryness. It was only as the Nazi Panzers entered Amiens, a city near the English Channel, that Philby made his escape.

It had been an adventure indeed, but Philby had other ambitions. He wanted to become a British spy.

In those days, it was common for the secret services to find new recruits informally through networking or chance encounters. Philby's first direct contact occurred in 1940 when he happened to share a first-class train compartment with Hester Harriet Marsden-Smedley, a famous war correspondent. After a brief exchange, Marsden-Smedley was charmed enough to recommend that he be recruited. Just two days later, after a routine background check and another interview, Philby officially became a spy for the British Empire.

All had seemingly gone swimmingly — but the British Secret Intelligence Service, otherwise known as MI6, had failed to notice a few critical details about their new recruit.

> _"Philby resigned from the_ Times _, and duly reported to a building near MI6 headquarters."_

### 4. Philby was recruited by the Soviet secret services, and he soon began leading a double life. 

What MI6 didn't know about Philby was that he'd been a spy for quite some time.

Back in the summer of 1934, mere days after returning from Vienna, Philby was sitting in Regent's Park, waiting for a mysterious man his wife, Alice Kohlman, had arranged for him to meet. The contact went by the name of Otto, and thanks to his cultured Eastern European airs, he soon had Philby wrapped around his finger.

Many years later, Philby learned the man's true identity. He was Arnold Deutsch, one of the top recruiters of Soviet spies in Britain. In fact, he was largely responsible for enlisting the infamous spy ring known as _The Cambridge Five_. These were young men who were recruited at Cambridge University in the early 1930s to work for Soviet intelligence. They betrayed their country for decades after.

Philby didn't need much persuasion from Deutsch. After being briefed, he was ready to begin his life as a Russian spy.

First, Deutsch instructed Philby and Alice to break off all contact they had with Communist circles. Then Philby had to create a new persona — he would be a right-wing conservative, with Fascist sympathies. This meant he had to pursue the kind of conventional career in bourgeois society that he had already rejected as a student.

While this was going on, Philby's training in espionage proceeded. Deutsch taught him how to arrange secret meetings, where to leave messages and how to ascertain whether a telephone was under surveillance. He was trained on how to identify whether anyone was following him, and how to disappear if it was ever deemed necessary.

Deutsch also gave him a tiny camera for copying documents. It was physical proof that his double life had truly begun.

### 5. Philby’s reputation as an Allied agent was impeccable, and the Soviets distrusted him for it. 

A good spy needs good cover. A low-brimmed hat just won't cut it. Philby's own success boiled down to his perceived reputation. In the eyes of the British, his activities during World War II made him a hero.

Philby's standing grew and grew, and, as it did, he was assigned ever more responsibilities.

His initial domain was the Iberian Peninsula, a neutral region that both the Allies and the Axis powers were working hard to bring onside.

Philby was then tasked with heading the counterintelligence team for North Africa. It was a critical assignment since the Allies were planning to invade Morocco and Algeria.

Then his brief was extended to cover Italy as well. Finally, Philby's boss, Felix Cowgill, asked him to represent him in all areas while Cowgill was traveling in the United States for MI6.

But, in many ways, Philby was a victim of his own monumental success. The Soviets were suspicious that Philby's rapid rise was, in fact, a set-up job. Elena Modrzhinskaya, an NKVD analyst based in Moscow, was particularly skeptical. She headed the British sector of Russian intelligence and was as brilliant as she was paranoid.

Maybe because of her precarious position as a woman in power in Soviet Russia, or because of the general fear that most felt under Stalin, she was sure that something was amiss. She believed that the Cambridge spies, Philby included, had had too easy a time infiltrating British intelligence. It was very likely, she thought, that they were actually double agents for the British.

In fact, for a time, Russian agents tracked Philby through London in the hope of pinning some solid evidence of betrayal on him. But they uncovered nothing.

### 6. By the end of World War II, Philby was head of anti-Soviet intelligence. 

By 1944, it was becoming increasingly clear that World War II would end in Allied victory. This raised a tricky dilemma for Philby and other double agents. During the war, the Soviet Union and the Western Allies had been fighting a common enemy. But there was little doubt that the Soviets were themselves becoming the new enemy.

It was understandable. The British establishment had always been afraid of Communism, which had been the main focus of British intelligence prior to the war. In fact, the fight against fascism was only a temporary respite.

But this dormant fear of Communism was refreshed by new intelligence. The British realized that Soviet spies must have infiltrated the secret services. Suspiciously, the Russians seemed to hold information they could not have obtained by any other means.

At this point, Philby devised a ruse. He suggested to C, the head of MI6, that a new section be established. Section IX — as it became known — would have one purpose: to fight Communism. C was enthusiastic and thought Cowgill should become its new head. But Philby and Moscow had other plans.

Step-by-step, Philby meticulously undermined Cowgill's putative appointment and maneuvered himself into position as head of Britain's new anti-Soviet section.

He began by exacerbating the pre-existing feelings of dislike that existed between Cowgill and his superiors, Valentine Vivian and Claude Dansey. For example, he insinuated that Cowgill's relationship with MI5, Britain's domestic intelligence agency, was particularly sour. Simultaneously, he was sure to keep himself in the good books of all those who were senior decision-makers in the service.

Consequently, it came as no surprise to Philby when in September 1944, C summoned him and offered him the post as head of Section IX. Philby was now in the best possible position to keep Moscow informed about every move made by British intelligence.

### 7. In 1950, a British plan to disrupt Communist Albania was undermined by Philby. 

A few years after the end of World War II, Philby traveled to the United States, still in his capacity as head of anti-Soviet intelligence.

It was around this time that the Soviets' initial doubts about Philby's loyalty were put to rest. The information he was providing them clearly compromised British intelligence plans.

The British and US plan to sabotage Communist Albania is the best example of this. The idea behind Operation Valuable, as it was known, was to recruit anti-Communist Albanian partisans, extract them to a base outside Albania, and train them for insurgency. They would then be parachuted back into the country, so they could coordinate with other rebels, and lead them in revolution.

However, when the rebels landed in Albania in late 1950, the communist government's troops were there waiting for them.

Many of the partisans were shot the moment they landed, while others were imprisoned. Very few escaped. Even rebels who'd tried to return on foot or by hiding in ships fared no better. On top of that, the rebels' family members and anyone close to them were themselves massacred.

If the finger of blame were pointed anywhere, then it would be at Philby. It was he who turned over vital information to the Russian and Albanian governments, right down to intelligence about the rebels' landing sites. Russian informants have since confirmed that they knew exactly how many insurgents to expect, when and where they would land, and the kind of weapons they'd be armed with.

All in all, including the families and acquaintances of the murdered rebels, Philby was probably responsible for the deaths of several thousand Albanians.

### 8. Philby was joined in the United States by a fellow Soviet spy, but the CIA had them in their sights. 

As well as a busy professional life, Philby had a somewhat dramatic personal one. In total, he married four times, and, in 1951, his relationship with his second wife, Aileen Furse, was under definite strain.

To make matters worse, one of Philby's fellow Soviet spies suddenly came to pay the family a visit. It was Guy Burgess, another member of the Cambridge Five, ostensibly working for the British Foreign Office. Now, Burgess was no ordinary spy. In fact, he was not only one of the least discreet spies of all time; he was also a drunk and a promiscuous homosexual, who made a habit of insulting influential people. These factors hardly helped him to maintain a low profile in the dour 1950s.

Aileen was furious. She knew full well that once Burgess had set up camp in their home, he would be impossible to remove. A fight between the couple ensued, but, in the end, Philby got his way, and Burgess moved in. It was to be a short-lived victory, though: the CIA was closing in fast on Soviet spies.

In May 1951, Meredith Gardner, a decoder who worked for the CIA, succeeded in deciphering a message that had been intercepted years earlier in the summer of 1944. This interception led to the identification of one member of the Cambridge spy ring, Donald Maclean, a diplomat in the British Foreign Office.

News of the discovery was dispatched to London, only then to be sent straight back to Kim Philby in Washington. Incredibly, Maclean was not arrested right away. Instead, he was put under surveillance. This provided Philby with time to devise an escape plan for Maclean: he sent Burgess to England to warn Maclean in person. Then, under cover of night and armed with falsified identities, Maclean and Burgess fled together to France and then on to Moscow.

### 9. In the aftermath of Maclean’s flight, Philby’s cover was nearly blown. 

The investigations into Philby had achieved nothing definitive, other than leaving him suspended and the case in a state of limbo. Not everyone was satisfied. One Labour MP in particular, the notoriously fusty Colonel Marcus Lipton, sought to force the situation.

In October 1955, he made use of a parliamentary privilege, which allows MPs to make potentially libelous statements in debate without fear of prosecution. Lipton used this to ask the prime minister how long he intended to cover up the Philby affair. As a statement, it was intended to fan the flames of press interest in Philby's activities in Washington, and it was wildly successful.

Incredibly, once again Philby managed to maneuver himself out of a corner, through the use of a rather unorthodox stratagem. In early November 1955, he invited the press over to his mother's sitting room. He knew what he was doing and sweet-talked the gathering to great effect, at one point even suggesting that a male journalist give his seat over to a female journalist caught in the doorway.

The filmed interview is a remarkable document of Philby's stone-faced chutzpah. He stared down the journalists and the TV audience and let forth a deluge of lies. Not once did he stammer, stutter or give the faintest sign of nervousness.

He calmly explained that he'd not been able to illuminate the matter earlier, because, as an MI6 official, he'd been bound to confidentiality.

He then pivoted and threw Burgess under the bus for all that had happened in Washington. He outright denied any trace of involvement with the Soviets.

It was a performance for the ages: not only did it convince the congregated journalists and the thousands of TV viewers, it even satisfied MI6, which promptly welcomed him back into the fold.

### 10. In 1955, Philby came under public scrutiny for the first time, but he managed to wheedle his way out. 

The investigations into Philby had achieved nothing definitive, other than leaving him suspended and the case in a state of limbo. Not everyone was satisfied. One Labour MP in particular, the notoriously fusty Colonel Marcus Lipton, sought to force the situation.

In October 1955, he made use of _parliamentary privilege –_ which allows MPs to make potentially libelous statements in debate without fear of prosecution — to ask the prime minister how long he intended to cover up the Philby affair. As a statement, it was intended to fan the flames of press interest in Philby's activities in Washington, and it was wildly successful.

Incredibly, once again Philby managed to maneuver himself out of a corner, through use of a rather unorthodox stratagem. In early November 1955, he invited the press over to his mother's sitting room. He knew what he was doing and sweet-talked the gathering to great effect, at one point even suggesting that a male journalist give his seat over to a female journalist caught in the doorway.

The filmed interview is a remarkable document of Philby's stone-faced chutzpah. He stared down the journalists and the TV audience and let forth a deluge of lies. Not once did he stammer, stutter or give the faintest sign of nervousness.

He calmly explained that he'd not been able to illuminate the matter earlier, because, as an MI6 official, he'd been bound to confidentiality.

He then pivoted and threw Burgess under the bus for all that had happened in Washington. He outright denied any trace of involvement with the Soviets.

It was a performance for the ages: not only did it convince the congregated journalists and the thousands of TV viewers; it even satisfied MI6, which promptly welcomed him back into the fold.

### 11. Kim Philby’s web of deceit finally unraveled. 

In 1956, Philby was sent to Beirut as a journalist on assignment for the Observer. In reality, he was set on continuing his work as a spy.

But his past was about to catch up with him. Back in 1935, Philby had lunched with Flora Solomon, a department store executive, who later introduced him to his second wife. Philby had mentioned the Communist cause and had even tried to recruit the leftist Solomon as a Soviet spy.

In 1962, disgruntled by Philby's behavior, she mentioned this tête-à-tête to her friend, Lord Victor Rothschild, who had himself been an MI5 agent. Days later, Solomon was called in to MI5 to formally repeat her statement. It was this evidence that persuaded the British secret services of Philby's treason. But they were still missing a smoking gun: they wanted a confession from Philby.

In the end, in late 1962, Nicholas Elliott — one of Philby's oldest friends in MI6, and a man who'd fervently defended him in the past — challenged Philby face-to-face in Beirut. Wracked by anxiety and alcohol addiction brought on by the pressures of leading a double life, Philby soon folded.

Nicholas Elliott told him that MI6 were prepared to offer Philby immunity if he told them everything he knew about Russian intelligence networks. Elliott began by extracting some key information from Philby but then traveled on to Africa, leaving Philby behind without surveillance. In retrospect, it seems a very odd decision. Perhaps it was negligence on the part of MI6, or maybe, as the author suggests, it was intentional.

Either way, Philby took advantage of the situation and, with the help of the KGB, made his escape to Moscow in January 1963. For the remaining 25 years of his life, he never returned to the West, nor did he ever face justice.

### 12. Final summary 

The key message in these blinks:

**Kim Philby was a master of deception. Recruited by Soviet intelligence in Cambridge during the early 1930s, he was a cornerstone member of the famous Cambridge Five spy ring. During World War II, he managed to infiltrate MI6, later also gaining access to CIA circles in the United States. For more than 20 years, he provided the Soviet Union with crucial intelligence about the British and American secret services. In 1963, his cover was finally blown, and he fled to Moscow.**

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next: No Place to Hide, by Glenn Greenwald**

**** In _No Place to Hide_ (2014), author Glenn Greenwald details the surveillance activities of secret agencies based on information leaked by American whistleblower Edward Snowden. Rather than serving as a means to avoid terrorist attacks, as the US National Security Agency (NSA) claims, Greenwald explains that these dubious activities instead seem to be a guise for both economic espionage and spying on the general public. Head over to the blinks to _No Place to Hide_ to learn about the media's lack of freedom in detailing certain government and intelligence agency activities, and the consequences whistleblowers face for revealing secret information.
---

### Ben Macintyre

Ben Macintyre is a journalist and historian. He is an associate editor, columnist and writer for the _Times_, and has written several best-selling books on various war-related intelligence operations and events, including the D-day landings and Operation Mincemeat.

