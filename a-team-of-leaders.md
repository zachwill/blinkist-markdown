---
id: 5534f88c6638630007420000
slug: a-team-of-leaders-en
published_date: 2015-04-21T00:00:00.000+00:00
author: Paul Gustavson and Stewart Liff
title: A Team of Leaders
subtitle: Empowering Every Member to Take Ownership, Demonstrate Initiative, and Deliver Results
main_color: 257EBA
text_color: 1B5C87
---

# A Team of Leaders

_Empowering Every Member to Take Ownership, Demonstrate Initiative, and Deliver Results_

**Paul Gustavson and Stewart Liff**

_A Team of Leaders_ (2014) provides companies facing internal problems, such as lost productivity, high turnover and low employee commitment, with a powerful solution. These blinks outline practical methods and tools, including the five-stage team development model, to guide you toward an engaged and high-performance work environment.

---
### 1. What’s in it for me? Make your team a team of leaders. 

Anyone whose workweek is a haze of boredom won't be surprised by this statistic: As much as $550 billion is lost per year in the US thanks to the low productivity of employees.

The sad truth is that many employees feel completely disengaged from their workplace. They don't feel motivated to do any more than the bare minimum required of them by their bosses.

But if building a successful company is your goal, the bare minimum won't cut it. You need employees who love and take responsibility for their work, and who seek their own approaches to making the company better — without your having to reward them with bonuses.

In other words, you need a team of leaders. These blinks explain some quick and easy ways to encourage your employees to be more self-sufficient, as well as to stir up new inspiration and ideas at the office.

In these blinks, you'll discover

  * why your team should constantly be learning new things;

  * how to improve the design of your office; and

  * one way to ensure your whole office is united by good purposes.

### 2. Accomplish shared leadership in five stages. 

Imagine you're at the office and it's midday. After 4 hours of typing and staring at a computer screen, how do you feel? In all likelihood, you're bored stiff.

You probably have moments when you'd like to forsake your computer and incite your coworkers to join you in revolutionizing the office. You'd like to be part of a team, not a hierarchy. You want to be a member of a _team of leaders_.

But, as in most offices, your boss has all the decision-making power. What do you do?

Well, your workplace may simply be in the first of five stages in the _team development model._ Stage one is that hierarchical model we're used to; there's one leader and a team of subordinates following orders.

So how do you move past that stage?

First, it's important to know what the next stage looks like.

Imagine an organization that's agreed to strive toward developing a team of leaders. There'd no longer by one executive adjudicating on all work-related issues. Rather, every member of the team would have an equal say.

This is the stage during which change really begins. The following stages are just the ideal consequences of that initial shift.

Stage three, for instance, is when new leaders begin thinking about creating and taking responsibility for their own team, perhaps by recruiting new members.

In stage four, the transition continues as more and more team members step up to the plate and take on leadership roles. For instance, instead of waiting for an assignment from your management, you'd approach HR yourself, learn the ropes and start selecting candidates to fill open positions.

By stage five, the goal of completely shared leadership is realized, and each employee is filled with a greater sense of engagement. Because everyone now knows how to recruit people according to the company's processes, each team is able to self-manage.

### 3. Design your team to give every leader a purpose. 

Hold on — design? That verb might seem a little out of place here. We're talking about businesses, not Eames chairs, right? Well, design actually has a major influence on many aspects of an organization, too.

To get a better sense of this, consider the following questions:

How many members does your team have? How should you implement interview procedures? How do you define a company's mission?

Fundamentally, these are all questions of _design_. So, then, what's the ultimate goal of design?

Simple: uniting people with a shared _sense of purpose_. We'd all want our job to be meaningful, to have a positive impact on the world: knowing that our work has meaning gives us more energy and more fulfillment.

For example, when asked what they do, you want your employees answering purposefully, like "I improve the usability of a website!" instead of "I just fix bugs." When they don't feel connected to a higher purpose or believe their work has meaning, they'll start channeling energy away from work.

In addition to each individual's sense of higher purpose, the team of leaders will thrive as a whole when united under a clearly communicated mission, such as "We support American veterans."

But a truly ingenious way of giving your team drive is to implement a _team value creation model._ This is an approach where your team operates as a mini-business, by providing team members with the incentive to access more information and a higher level of inside knowledge.

Being your own mini-business boss, you might have insights into financial data — balance sheets, say — that allows you to track your team's performance and engage them with it. This increased employee engagement will not only boost their satisfaction; it'll boost customer satisfaction, too.

How?

See, customers usually benefit when an organization functions at a high level. When employees are more productive and better able to respond and update quickly, customers will want in on the action.

### 4. Align the incentives of each leader to boost the team as a whole. 

So we now know that every leader in your team should have her own purpose. However, it's also vital that these purposes allow team members to work with, not against, each other.

In other words, you must achieve _alignment_ among your team members. Alignment is how your team and its elements work together.

To get a better idea of what alignment looks like, let's consider an unaligned team.

An organization says that they're shifting focus to improving the quality of their products. But little attention is given to improving quality, and the focus remains on maximizing productivity. At management meetings, the focus is on the quantity of work, not the quality. And at the end of the year, bonuses are awarded to the teams that performed best, including one who scored, out of all the company's teams, second-lowest in quality but highest in productivity.

This unaligned situation only confirmed the employees' suspicions that management didn't really care about quality. The company's senior executives eventually realized their mistake, but they had lost the employees' trust and it took many years to make up for it.

So, how to avoid this? By creating alignment _._

Take the above example. The management should've aligned the teams toward making higher quality products: That means considering which principles would guide the teams' focus toward improving quality, which strategies would improve quality, which specific projects or technologies were needed to optimize quality and which rewards would entice teams to produce at a higher quality.

If everyone had striven for quality, all design and behavioral incentives would've been in accordance in the team.

And that would've meant that everyone was working toward the same goal: shared success.

> _"Key principle: All your processes must support the goal of making everyone a leader."_

### 5. Give your team access to all the knowledge they need. 

You've probably heard that old saying "knowledge is power." It's more than just a cliché. In fact, it's incredibly relevant when it comes to how your team functions. Every organization has _organizational knowledge_, that is, the sum of the knowledge belonging to each member within it.

Organizational knowledge can also be broken down into _key knowledge_, which tells us how to create value for the customer; _codifiable knowledge_, referring to facts and routines in the company; and _tacit knowledge_, or beliefs and expertise.

Let's take a closer look at why organizational knowledge is vital, and how it works.

Imagine you work in a call center with two other people, whom we'll call Anton and Berta. Anton and Berta have _key knowledge,_ but what happens when Anton and Berta are sick, and you receive a call from a customer named Tom, whose credit card has been stolen?

Somewhere in your company handbook there'll be a section on credit cards: codifiable knowledge that might help Tom. But nobody told you where to find it. And, if you're unable to contact Anton and get his advice, your tacit knowledge — that you're 99 percent sure Tom's money is safe — is insufficient, and probably won't make Tom feel any better, and he definitely won't think too highly of your company.

Clearly, there's something wrong with this model.

So instead of each team member hoarding a specific kind of knowledge, the goal should be for tacit, codifiable and key knowledge to be equally accessible to each team member in your company.

How can you ensure this? The best way is to facilitate learning.

S _tructured learning methods_, like manuals, customer research and videos, help everyone access codifiable knowledge. On the other hand, _unstructured learning methods_, like storytelling, role-playing or personnel rotation, increase the exchange of tacit knowledge because experiences get shared.

> _"Key principle: To build leaders, you must determine the knowledge they need, decide how to acquire it, and then manage its distribution."_

### 6. Your leaders need a working space that supports them. 

Have you ever had the unfortunate experience of working in a labyrinth of office cubicles under the harsh glare of fluorescent lighting? Many of us have, and it's not the way it should be. A working space should double as a leader's place, an environment that honors both your company's mission and your customers' feelings.

To create a space that fosters a team of leaders, we'll need something a little more powerful than interior decorating. It's called _visual management._

So where do we begin?

The first thing to consider is how to optimize the workplace so that ideas and inspirations can be exchanged. Rather than having separate rooms for each team member, why not create an open, yet close-knit layout. It could also help to have charts on the walls displaying current problems and possible solutions.

Additionally, whiteboards illustrating the progress of individual projects will make team members feel appreciated and meaningful. In this way, visual management can boost the alignment of a team, too.

A clever use of space will also allow you to send powerful messages to your customers. Displays outlining your mission can send positive signals: "This is how we perform and we like it!"

And, even just a hand written sign displaying customer feedback makes every customer feel that they're both welcomed and respected.

### 7. Final summary 

The key message in this book:

**Your organization is just five steps away from becoming a team of leaders. Savvy team design, sharing of knowledge and strong visual management will smooth your transition to an aligned and enthusiastic team, united under a shared purpose.**

Actionable advice:

**Remember why you do, what you do!**

If you've ever felt demotivated, just take a step back and reflect on how your individual contribution matters, not just to your team, but to the greater good that your company serves. Even if it doesn't feel like it, your activities are meaningful, appreciated and important. Channeling these thoughts will keep you energized, happy and on track.

**Suggested** **further** **reading:** ** _Leaders Eat Last_** **by Simon Sinek**

_Leaders_ _Eat_ _Last_ explores the influence that neurochemicals have on the way people feel and consequently act, and examines the discrepancies between how our bodies were designed to function and how they function today. Ultimately, we need true leaders to direct us back on the right path.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Paul Gustavson and Stewart Liff

Paul Gustavson is an organization design consultant and the founder of Organization Planning & Design, a company that creates and sustains high-performance teams around the world. He also coauthored _The Power of Living by Design_ and _Running into the Wind._

Stewart Liff is a human resources and visual management expert, as well as the president and CEO of his own consulting group. He is the author of several other successful books, including _Managing Government Employees._

