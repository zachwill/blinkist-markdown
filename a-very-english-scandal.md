---
id: 57fbe5ed70ec6300032cf689
slug: a-very-english-scandal-en
published_date: 2016-10-13T00:00:00.000+00:00
author: John Preston
title: A Very English Scandal
subtitle: Sex, Lies and a Murder Plot at the Heart of the Establishment
main_color: 5BA8CE
text_color: 396A82
---

# A Very English Scandal

_Sex, Lies and a Murder Plot at the Heart of the Establishment_

**John Preston**

_A Very English Scandal_ (2016) tells the story of former British politician Jeremy Thorpe's affair with Norman Scott. From a botched assassination to a biased murder trial, the story of this scandal shines a harsh light on the petty yet powerful relationships within the British Establishment and how those ties work to silence justice and protect reputations.

---
### 1. What’s in it for me? Learn how the British Establishment got away with (attempted) murder. 

According to many British politicians, the country's judicial system is the envy of the world. Its strength, they claim, is its fairness. Whether a lord or commoner, a person will receive the same treatment before the court. After all, English courts even tried and convicted a ruling monarch!

But is this claim of fairness true? These blinks show you that the British judicial system is not as impartial as many claim it to be.

The story of Jeremy Thorpe, a Liberal Party politician educated at both Eton and Oxford, is a telling tale. In these blinks, you'll find out how through Thorpe's connections with the "Establishment," or British elite, he was able to dodge accusations of attempted murder.

In these blinks, you'll discover

  * how money donated for "electoral reform" was used for murder;

  * why police changed a statement from a key witness; and

  * which other crimes the Establishment have tried to cover up.

### 2. Jeremy Thorpe was a powerful yet arrogant politician with a secret: he was a closeted homosexual. 

British politician Jeremy Thorpe was a man whose charm and cunning helped him climb the ranks of the elite. Leader of the Liberal Party from 1967 to 1976, he had a spotless public image.

But he also had a secret.

Thorpe was homosexual, a fact he kept hidden for fear of damaging his political career.

Yet Thorpe was also reckless when it came to his affairs. Many felt Thorpe was an arrogant man; Thorpe certainly knew that with his charm and the help of influential friends, he could escape any sticky situation.

One particular relationship, however, changed this.

In the early 1960s, Thorpe met a young and emotionally unstable man named Norman Scott. The two made their acquaintance through one of Thorpe's friends who was Scott's employer at the time.

Thorpe took a shine to the young dressage rider and suggested that Scott get in touch with him should he ever need a "favor."

Scott was unaware of the politician's real intentions. Shortly after their meeting, Thorpe initiated a sexual relationship that Scott claims at first wasn't consensual, saying that Thorpe raped him during their first sexual encounter.

Scott later moved to Ireland but kept in touch with Thorpe. He was determined to have Thorpe help him in securing a National Insurance card, which he needed to get a job.

Gradually Scott's obsession turned into behavior that threatened to expose Thorpe. As Scott traveled, he would share stories of his affair with the prominent politician. Homosexuality was still illegal at the time, and, desperate and depressed, Scott went so far as to show the police letters Thorpe had sent him. At one point, Scott even contacted Thorpe's mother, asking her for money.

Feeling threatened, Thorpe realized he needed outside help to keep Scott's mouth shut about their relationship.

### 3. Two of Thorpe’s loyal friends agreed to help the politician sort out his sordid affair with Scott. 

All too aware of the risks Scott's loose mouth posed to his political career, Thorpe called on two loyal friends — David Holmes and Peter Bessell — to help him figure out what to do.

Holmes was openly homosexual and a close friend of Thorpe's since their student years at Oxford. Bessell was a member of Parliament with the Liberal Party as well as a failing businessman known for his love of gossip. While it's unknown whether Holmes and Thorpe had ever had a relationship, they were said to have shared sexual partners. Either way, Holmes was unwaveringly loyal to Thorpe.

Bessell, on the other hand, didn't run with Thorpe's circle of friends but had admired Thorpe for some time. Thorpe had used his powerful connections to support Bessell financially. One such friend, wealthy Liberal Party donor Jack Hayward, was defrauded of hundreds of thousands of pounds to help buoy Bessell's many floundering ventures. In short, Bessell was very much indebted to Thorpe.

Holmes and Bessell quickly agreed to help Thorpe sort out his sticky situation.

Bessell communicated with Scott and supplied him with a weekly retainer in an attempt to mollify him. Yet Bessell decided that the National Insurance card demand was a no go, as giving Scott the card would create a solid link between the politician and his estranged lover.

Despite the friends' efforts, however, Scott continued to spread stories of his relationship with Thorpe.

At this point, it was clear that something had to give. Thorpe suggested to his friends that only one course of action remained, something he called "the ultimate solution."

This solution was murder.

Holmes and Bessell were against the idea of having Scott murdered — yet they didn't walk away. Hoping Thorpe would forget about his drastic plan, the two men continued to send Scott money and keep him away from London.

But Scott still wouldn't be silenced.

### 4. Funds intended for a political campaign were used to hire Scott’s assassin. 

By this time, Thorpe had been elected as the leader of the Liberal Party. He'd made many sacrifices to get to the top and refused to let his affair with Scott ruin his career.

In 1975, Thorpe asked for a donation of £17,000 from Jack Hayward to help fund his "lobby for electoral reform." Thorpe requested that £10,000 of the total £17,000 be deposited into an account separate from the official Liberal Party account.

Thorpe was vague about his intentions for this hefty sum but assured Hayward that the donation would be used for the greater good of the party. In the end, the £10,000 was actually deposited in the account of Nadir Dinshaw, the godfather of Thorpe's son.

This "donation" was earmarked to pay for Scott's killer.

Yet it was Holmes's job to hire the man to kill Scott. After one potential assassin had backed out, Holmes's friends led him to an airline pilot named Andrew Newton. Holmes told Newton he could kill Scott any way he chose. In return, he would be paid between £5,000 and £10,000.

Newton began following Scott, and after making contact with him, convinced him one day to go for a drive. Yet Scott felt something was amiss — Newton appearing out of the blue, just to befriend him — and asked a friend to jot down the license plate number of the car Newton was driving, just in case.

Scott took his dog along for the drive with Newton to Exmoor in South West England. At one point in the trip, Newton pulled over in an attempt to carry out his assassination. He succeeded in shooting the dog but his gun jammed when he tried to shoot Scott.

Newton drove off in a panic. Scott, left on the side of the road with his dead dog, was eventually picked up by a passing car.

The car's license plate was a crucial detail that led investigators through Newton back to Holmes.

Once this connection was made, Scott was able to convince the authorities to take his claims seriously and open a criminal investigation into politician Jeremy Thorpe.

### 5. After the botched assassination, Thorpe was called to court to stand trial for attempted murder. 

In 1979, Thorpe became the first and only member of Parliament to stand trial for attempted murder in British history.

The undeniable links between Holmes and Newton were certainly incriminating. When Bessel also agreed to give testimony against Thorpe, the politician could no longer avoid criminal charges.

Bessell received immunity in return for testifying against Thorpe. Holmes, on the other hand, was apparently too loyal to Thorpe to say a word against him in court.

This loyalty was sorely misplaced, as Holmes would find out later; Thorpe's lawyer, George Carman, accused him and one of his accomplices of planning the murder independently.

While Carman argued vehemently for his client's innocence, the British media was having a field day.

Journalist Auberon Waugh, son of author Evelyn Waugh, was one journalist who followed the case from the start, reporting via political satire magazine _Private Eye_. The media spectacle turned public opinion firmly against Thorpe. It was clear that his political career was tarnished, perhaps forever.

This fact was clear to everyone — except Thorpe. The arrogant former MP never lost his confidence. He believed that despite the odds, he'd one day earn a peerage. Thorpe made fruitless attempts until 1999 to do so.

In the end, Carman succeeded in clearing the criminal charges against Thorpe. He managed to persuade the jury during the trial that Bessell, Scott and Newton were simply petty, untrustworthy liars.

Carman's skills in the courtroom did much to help get Thorpe off the hook. Yet the real helping hand, as Bessell had predicted, came from the British Establishment.

### 6. The British Establishment may have had a hand in securing Thorpe’s freedom. 

While Carman fought ferociously in the courtroom, other forces behind the scenes were at work to ensure Thorpe didn't end up behind bars.

A friend of Thorpe's named Lord Elwyn-Jones chose the judge for the criminal trial. The judge in question was Joseph Cantley, a man rumored to have abstained from sex up until the age of 56. Chaste and puritanical, Judge Cantley could hardly imagine that a well-to-do political figure such as Thorpe would ever be involved in such a sordid affair.

Openly biased in Thorpe's favor, Cantley closed the trial with an astonishing speech. He declared to the jury that he believed Thorpe innocent and made it clear that if the jury voted for a verdict of guilty, they had better be 100 percent certain.

Yet even before the trial commenced, Bessell agreed to a deal that would further undermine his credibility. The _Sunday Telegraph_ offered him £50,000 in exchange for six stories on the Thorpe affair. Bessell's lawyer made sure to check with the assistant director of public prosecutions as to whether such a deal would affect the perception of Bessell's testimony in court. The director assured him it wouldn't.

This was either very bad or very cunning advice.

It was clear that Bessell selling his story to the press would make him seem like a witness who would be eager to lie or exaggerate, accusing Thorpe in court for financial gain. It has been speculated that Kenneth Dowling, assistant director of public prosecutions, encouraged this deal between Bessell and the press to protect Thorpe and discredit Bessell.

The Thorpe affair is one example of the British Establishment ensuring that its members stay out of trouble, even at the expense of justice. Yet it's not the only example.

### 7. The Thorpe case is just one example of the British Establishment putting reputation over justice. 

Authorities have worked furiously for decades to mask and minimize the blunders and potential crimes of politicians.

One example is the case of Cyril Smith. Smith was a former Liberal member of Parliament and, as was revealed after his death, a serial child molester. In 2014, it was discovered that there were over 114 complaints against Smith from boys as young as eight.

Yet Smith was never prosecuted during his lifetime.

Why? It was revealed that police officers were simply threatened with losing their positions if they continued to stir up trouble by making any allegations public.

Smith was close friends with Jimmy Savile, another pedophile whose crimes also weren't exposed until after his death. Incidentally, Savile endorsed Thorpe during one of his election campaigns.

Separately, Jimmy Savile's brother stood as a Liberal candidate in Battersea North. For many years after his death, accusations surfaced that he had been sexually assaulting mentally ill patients whom he was supposedly helping while working as a recreation officer in Tooting.

Cases such as these seem to demonstrate that preserving political reputations is more important to the Establishment than serving the greater good.

While sex scandals often seem to be little more than a wild spectacle stoked by media coverage, when those in power are revealed to be capable of cruelty behind closed doors, the public must not turn a blind eye.

### 8. Final summary 

The key message in this book:

**While we often associate corruption in politics with financial greed, the Thorpe affair shows that it can go much deeper. There is often more to a powerful public figure than meets the eye. The coverage and outcome of criminal investigations into the deeds of political figures may not reflect justice but rather the invisible hand of influence working to clean up the mess.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Run of His Life_** **by Jeffrey Toobin**

_The Run of His Life_ (1997) examines the essentials of the O. J. Simpson murder case: the personalities involved and the social forces that led to the shocking acquittal of this football superstar. These blinks don't just chronicle the story of one man but also explore how American society turned Simpson's story of triumph into one of tragedy.
---

### John Preston

John Preston is the former arts editor for newspapers the _Evening Standard_ and the _Sunday Telegraph._ For ten years he was the _Sunday Telegraph_ 's television critic. He published a novel, _The Dig,_ in 2007.

