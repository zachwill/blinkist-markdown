---
id: 5e57d6b56cee07000673b9bb
slug: a-very-stable-genius-en
published_date: 2020-02-28T00:00:00.000+00:00
author: Philip Rucker, Carol Leonnig
title: A Very Stable Genius
subtitle: Donald J. Trump's Testing of America
main_color: None
text_color: None
---

# A Very Stable Genius

_Donald J. Trump's Testing of America_

**Philip Rucker, Carol Leonnig**

_A Very Stable Genius_ (2020) is the definitive account of Donald Trump's time in the White House. After three years of silence, dozens of public officials and other first-hand witnesses familiar with the workings of the Trump administration went on record with reporters Carol Leonnig and Philip Rucker. Their testimony forms the backbone of these blinks, which reveal the forty-fifth president of the United States up close.

---
### 1. What’s in it for me? Take a tour of Trump’s White House 

In July 2016, Donald J. Trump gave a speech accepting the Republican party nomination to run for the presidency of the United States. It was a foretaste of what was to come. 

Trump's predecessors had used this moment to emphasise unity and common purpose. They had been humble, extolled shared values, and spoken of "we" and "us." That wasn't Trump's style. His speech was brash and bombastic. Most of all, though, it was self-centered.

"I am your voice," he said, and "I will be your champion." Most memorably, he insisted that "Nobody knows the system better than me, which is why I alone can fix it."

What has followed is a presidency seemingly powered by solipsism — an extraordinary, and at times extraordinarily chaotic, one-man show. 

It has been a dizzying journey. As reporters who surf the news cycle every day, attempting to keep up with Trump, Carol Leonig and Philip Rucker know that better than most. 

When history moves so fast, it's hard to put things in perspective. That's why they decided to hit the "pause" button and take stock of Trump's first three years in office. It was time, they thought, to drill deeper and figure out what was really going on behind the scenes. 

And that's just what we'll be doing in these blinks. We'll join Leonig and Rucker in exploring the controversies and defining moments of Trump's presidency so far. 

Along the way, you'll learn

  * how Trump's disdain for expertise alienated the US military hierarchy; 

  * what foreign leaders think about the American president; and 

  * how Trump's abusive behavior has affected his subordinates.

### 2. An outburst early on in Trump’s presidency alienated the US military’s top brass. 

On a scorching day in July 2017, Donald Trump attended a meeting in the Pentagon. It was held in room 2E924, aka "the Tank," a secure, windowless conference room with a large table at its center. It's here that America's senior military leaders — the Joint Chiefs of Staff — meet to discuss confidential issues and grapple with the nation's security. That day, however, they had a different agenda — educating the new president on America's strategic priorities. They were in for a shock. 

**The** **key message here is** : **an outburst early on in Trump's presidency alienated the US military's top brass.**

Trump had already been in office for five months when he took his seat in the Tank. In that time, officials had realized that the president's attention span was limited. As they entered the room that day, Rex Tillerson, the secretary of state, and Jim Mattis, the secretary of defense, knew that explaining foreign policy to Trump was going to be an uphill battle. 

Desperate to keep the president focused, they had created a short presentation with plenty of graphics, maps, and charts. Over the next 45 minutes, Tillerson and Mattis tried to convince Trump that America's safety depended on a complex web of overseas commitments, alliances like NATO, and free trade with economic partners.

Trump wasn't happy. Not only did he resent being lectured like this, he also objected to Tillerson and Mattis's internationalist language. 

NATO, he began, was "worthless." So-called allies were simply living on America's dime. "We are owed money," Trump said, his blood pressure rising, "and you aren't collecting." Then there were the troops stationed in the Gulf. "We spent 7 billion," the president boomed. "Where is the fucking oil?"

Mattis objected, arguing that the purpose of these alliances and military bases wasn't profit, but America's security. Trump wasn't interested. Why, he now demanded, hadn't the United States won the war in Afghanistan? The president answered the question himself — because it was a "loser war." 

By this point, he was in a rage. What he said next stunned the room. "I wouldn't go to war with you people," he screamed, adding that the Joint Chiefs were a "bunch of dopes and babies." It was a grave insult, and the Tank fell into shocked silence. 

Later, when the meeting had broken up and Trump had left, Tillerson said what others were thinking. "He's a fucking moron," the secretary of state growled, referring to America's commander-in-chief.

> _"So many people in that room had gone to war and risked their lives for their country, and now they were being dressed down by a president who had not."_

### 3. Time and again, Trump demonstrates ignorance of subjects presidents have traditionally been expected to know. 

Trump's secretary of state, Rex Tillerson, was worried about containing China. But how should the United States go about that task? Tillerson's answer was to strengthen America's friendship with China's next-door neighbor, India. To get the ball rolling, he arranged for Trump to meet the Indian prime minister, Narendra Modi.

As Modi talked about Chinese aggression, Trump interrupted him, telling him not to sweat it. "It's not like you've got China on your border," he stated. Modi left convinced that Trump couldn't be trusted to protect India's interests. Tillerson's plan was dead on arrival. Trump's ignorance had killed it. 

**The key message in this blink?** **Time and again,** **Trump demonstrates ignorance of subjects presidents have traditionally been expected to know.**

Trump's ignorance isn't restricted to geography. Take his understanding of history. 

On November 3, 2017, Trump was in Honolulu, Hawaii. Approximately 76 years and one month earlier, on December 7, 1941, Japanese planes had launched a surprise attack on Pearl Harbor — a US naval base in the city — in which some 2,300 sailors died. The United States declared war on Japan the following day.

With the anniversary of the attack coming up and Trump already in Honolulu, his advisors decided it was a good time to visit the Pearl Harbor memorial — a sombre rite of passage for US presidents. As they reached the harbor, Trump pulled his chief of staff, John Kelly, to one side. "Hey John," he said, "what's all this about? What's this tour of?" Kelly was astounded. How could a president be unaware of this momentous event — the cause of America's entry into the Second World War? 

Advisors tried to help Trump avoid such gaffes, but he failed to read even simple, one-page summaries of issues. Behind his back, he was known as the "two-minute man," a reference to how long his patience lasted when people tried to brief him on a subject or issue.

Trump went into events unprepared even when his image was at stake. When an HBO crew came to the White House to record him reading a section of the Constitution, for example, it quickly became clear he hadn't practiced beforehand. He complained that the text's archaic prose sounded like a "foreign language" and struggled to deliver his lines. 

Other politicians like vice president Mike Pence and former vice president Dick Cheney also read sections of the Constitution on camera. They didn't have these issues. As one witness recalls, when they were reading, "I knew they knew the Constitution." Trump, by comparison, appeared to be hearing these words for the very first time.

### 4. Trump shows little respect for experts, is often abusive, and makes impossible demands. 

Two kinds of people went to work for Trump in 2017: diehards convinced he was saving the world and skeptics who thought the world needed saving from him. These skeptics didn't disagree with Trump's political aims — they just didn't trust his unorthodox methods. As they saw it, the impetuous president needed the cool-headed guidance of bonafide experts. As "the adults in the room," this is what they could give him. 

It was a flattering self-image, but it rarely survived contact with the reality of working in the Trump White House.

**The key message here is:** **Trump shows little respect for experts, is often abusive, and makes impossible demands.**

Take HR McMaster, Trump's first national security adviser. A military man with a PhD in history, McMaster believed it was his responsibility as an adviser and an expert to give his commander-in-chief the unvarnished truth when things went wrong. 

Trump didn't see it this way. As his aides quickly figured out, the president was a proponent of shooting the messenger. Unsurprisingly, subordinates learned to soft-pedal bad news or simply "forget" to include various problems in their reports. 

McMaster, who refused to do this, became Trump's punching bag. Every time he entered the room, Trump puffed up his chest and fake-shouted in the style of a drill sergeant: "I'm your national security advisor, McMaster, sir — I'm here to give you your briefing, sir!" After just over a year of this kind of constant mockery, Mcmaster retired and took up an academic position at Stanford University. 

Then there was Kirstjen Nielsen, the administration's short-lived secretary of homeland security. When Trump wasn't castigating Nielsen as a soft touch who didn't "look the part," he was bombarding her with demands to carry out policies that were impossible, illegal, or both.

In November 2018, for example, Trump demanded that Nielsen close the US-Mexico border to prevent an "invasion" of migrants fleeing Central America. Nielsen, an immigration hawk herself, pointed out that people have a legal right to cross the border and claim asylum. Closing it, she added, wouldn't just be against the law — it would also strangle international trade flows. The chewing out she received for refusing to implement this illegal and impractical policy was so bad that she considered resigning on the spot. 

After just five months of service, Nielsen quit in April 2019 and took on an advisory role in a different department.

> _"The president doesn't fire people. He just tortures them until they're willing to quit."_ — __ One of HR McMaster's assistants.

### 5. Trump often ignores his advisers and makes impulsive foreign policy decisions. 

On February 26, 2017, Tillerson and Kelly were in Mexico to smooth over the hurt caused by Trump's campaign pledge to make Mexico — a long-standing American ally — pay for a border wall. Just as it was looking like they'd succeeded, Trump made an off-the-cuff policy announcement in Washington: he was sending troops to the border to stop "bad dudes" from entering the United States. 

It was exactly what Tillerson and Kelly had been reassuring their hosts wouldn't happen. They'd been blindsided by their boss's impulsive decision-making, and it wouldn't be the last time that happened.

**The key message in this blink?** **Trump often ignores his advisers and makes impulsive foreign policy decisions.**

In March 2018, Russian president Vladmir Putin was reelected in a contest most observers regarded as rigged. One candidate had been barred from the ballot and a second relentlessly attacked by state TV. As the BBC put it, Putin "could not lose." 

The fact that the election had been neither free nor fair didn't bother Trump, though — in fact, he wanted to congratulate Putin on his "amazing" victory. 

His national security adviser, HR McMaster, tried to steer him away from this idea, arguing that the Kremlin would paint this as an endorsement. When Trump insisted on calling anyway, McMaster prepared cue cards with talking points. One, written entirely in capital letters, simply stated: "DO NOT CONGRATULATE." Trump ignored it and did just that as soon as he was patched through.

In December 2019, Trump made another impulsive decision and announced the United States was pulling out of Syria. 

US special forces stationed in the war-torn country had been assisting Kurdish forces fighting against the Islamic State for around two years. The operation had been a brilliant success, but military leaders in Washington weren't counting their chickens. As one of General Mattis's aides put it, the United States had learned that its enemies often reappeared when American forces packed up and left. 

As far as Trump's advisers were concerned, the United States was in it for the long haul. Trump himself seemed to agree — until he suddenly changed his mind. On December 14, he spoke with the Turkish president, Recep Tayyip Erdogan. Why, Erdogan asked, did the United States need 2,000 soldiers in Syria when victory was all but guaranteed and Turkish forces were on hand to keep a lid on the situation? 

Trump didn't take long to answer the question. "You know what," he said, "it's yours — I'm leaving." A policy commitment established over years had been abandoned in seconds. All it had taken was a single phone call.

### 6. Despite the conclusions drawn by US intelligence services, Trump doesn’t view Putin’s Russia as a threat. 

It's a long-established rule of diplomacy that the personal rapport between leaders shapes the relationship between states. Anglo-American relations, for example, have never been cozier than when their heads of state got along — think of Winston Churchill and Franklin D. Roosevelt or Margaret Thatcher and Ronald Reagan. 

When Trump was elected, foreign leaders lined up to audition for the part of top ally. Japan's prime minister, Shinzo Abe, presented Trump with a golden, $4,000 golf club while his British counterpart, Theresa May, was photographed holding hands with the president. Trump, however, only had eyes for one leader: Vladimir Putin. 

**The key message in this blink is: despite the conclusions drawn by US intelligence services, Trump doesn't view Putin's Russia as a threat.**

No one in Trump's circle knew the Russian president better than his secretary of state, Rex Tillerson. A former Exxon man, he had negotiated oil prices with Putin back in the 1990s, and they'd stayed in touch ever since. Tillerson liked Putin, but he was also aware of his broader agenda.

Putin, he told Trump, was obsessed with restoring Russia's power. Making Russia great again, however, meant weakening the United States and its Western allies. Each morning, Tillerson added, Putin woke up with a single thought on his mind: "Where is America having problems? Let's go there and make it worse." 

Trump said that he didn't think that was what Putin was up to. After meeting the Russian president in person at a summit in Germany in July 2017, Trump stopped listening to Tillerson altogether. "I know more about this than you," he told him. 

A year later, Trump and Putin met again at the Russia-US summit in Helsinki, Finland. By this stage, US intelligence services had conclusive proof that Russia had interfered in the 2016 election. This was something Putin had explicitly denied in Germany. Trump had believed him then. What would he do now in light of the new evidence? 

When journalists posed this question, Trump mocked them, suggesting that they'd become obsessed by non-existent meddling. Putin, he went on, was telling him that it wasn't Russia and had issued an "extremely strong and powerful" denial. More to the point, he couldn't see any reason why Russia would have put its thumb on the scale to help him become president. 

It was a remarkable statement. Here was an American president ignoring the collective assessment of his own nation's intelligence agencies and instead taking the word of the leader of a hostile foreign power!

### 7. Trump regularly attacks America’s closest allies. 

On November 9, 2018, Trump received a phone call from the British prime minister, Theresa May, who congratulated him on his performance in the recent midterm elections. This was standard diplomatic protocol — a small gesture from a close ally designed to preserve its "special relationship" with the United States. 

But Trump wasn't in the mood for diplomacy. Over the next half hour, he laid into May, shouting that she was wrong about everything from Brexit negotiations to the EU's "unfair" trade deals with the United States. British officials were shocked — they couldn't remember a president treating a prime minister like this. For Trump, however, this was simply par for the course. 

**The key message in this blink is: Trump regularly attacks America's closest allies.**

Trump's belligerent behavior is most often on display at international meetings. Take the G7 summit in Canada on June 8, 2018. 

The G7 is an annual gathering of leaders from seven of the world's industrial powers — Canada, France, Germany, Italy, Japan, the UK, and the US. Typically, it provides a forum for America and its allies to express their shared principles. In 2018, they agreed to issue a statement in support of the "rules-based international order" — a thinly veiled attack on Russia's recent acts of aggression in Ukraine and its illegal annexation of Crimea. 

Trump wasn't impressed and, over the next two days, effectively blew up the summit. He withdrew US endorsement of the joint declaration after his own representatives had signed off on it and then took to Twitter to denounce the Canadian prime minister, Justin Trudeau, as "very dishonest and weak." Before departing, Trump threatened the other six members of the G7 with a trade war, claiming that the United States was like "the piggy bank that everybody is robbing, and that ends."

A month later, on July 11, Trump was in Brussels for a NATO summit. It was another opportunity to pick a fight with America's allies. Trump began by accusing them of being "delinquent" and refusing to pay their way inside the military alliance. One by one, other NATO members came in for criticism for their defense spending totals. 

A natural-born showman with an impeccable sense of timing, Trump wrapped up his performance by dropping a bombshell. If other NATO nations didn't raise their military budgets to 2 percent of their GDP by January next year, he said, the United States would consider leaving the alliance. It was another unprecedented departure from the rule book followed by US presidents.

### 8. The Mueller Report contained a catalog of misconduct, but Trump was able to claim it exonerated him. 

On March 22, 2019, former FBI director Robert Mueller released the results of an investigation opened two years earlier. His 448-page "Report on the Investigation into Russian Interference in the 2016 Presidential Election," or the "Mueller Report," was finally complete. America was about to find out if Trump had colluded with the Russians and broken the law. It was a question Mueller refused to answer.

**The key message in this blink is: the Mueller Report contained a catalog of misconduct, but Trump was able to claim it exonerated him.**

Mueller's report was divided into two parts. The first concluded that while illegal Russian interference in the 2016 election had been sweeping and systematic, there wasn't sufficient evidence to prove Trump had colluded with the Russian government. 

The second part investigated claims that Trump and his aides had deliberately hampered Mueller's investigation and thereby obstructed justice. As the report noted, Trump had launched a series of public attacks on the investigation and individuals whose evidence might undermine his claim to innocence. 

In private, the report went on, Trump had tried to control proceedings. The FBI director James Comey, for example, was fired after he refused to publicly state that the president wasn't under formal investigation — a decision Trump boasted about to the Russian foreign minister, Sergey Lavrov. 

These details demonstrated a pattern of misconduct, but were they enough to open a path toward impeachment? Mueller's report was brimming with damning facts, but he refused to reach a legal verdict. In the famous words of the report, he had neither concluded that the president had committed a crime, nor exonerated him. 

This refusal to issue a judgment of his own reflected Mueller's understanding of his role. As he saw it, he had gathered the relevant data and presented it to the American people. It was now up to them, and their elected representatives, to decide what to do next. 

Mueller's old FBI colleague Frank Figliuzzi pointed out the shortcomings of this approach. We're the Twitter society, he argued, the "scan-the-headlines-to-get-some-news society." Mueller's dense report just wasn't going to cut through in this environment. 

Figliuzzi was right. It wasn't the report itself that mattered so much as how it was interpreted. Here, the loudest voice won out — Trump's. In the absence of official charges of wrongdoing, Trump was able to claim that Mueller's report had put him in the clear. As he told TV cameras a couple of days later, "It was a complete and total exoneration."

### 9. Final summary 

The key message in these blinks:

**Donald Trump is a president unlike others. He made that much clear early on in his tenure when he went to the Pentagon to call America's most senior military leaders "losers."** **Unbeholden to convention and precedent, Trump refuses to read or listen to the experts who attempt to brief him, preferring instead to make his own impulsive decisions. Internationally, he has realigned the United States with a traditional enemy — Russia — and alienated long-standing allies like the UK, Germany, and Mexico. And, despite engaging in misconduct domestically, the long-awaited Mueller report failed to open a path toward impeaching Trump.**

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Catch and Kill_** **, by Ronan Farrow**

Philip Rucker and Carol Leonnig's decision to document the first three years of the Trump administration wasn't a partisan project. Like the journalists' employer, the _Washington Post_, Rucker and Leonnig believe that democracy dies in darkness. Setting the record straight, in other words, is a public service. 

That's something Pulitzer-winning journalist Ronan Farrow also believes. An investigative reporter with a knack for tracking down leads, Farrow has made it his mission to uncover abuses in high places. That instinct didn't lead him toward the White House, though — it led him to Hollywood and Harvey Weinstein. 

What happened next? Check out our blinks to _Catch and Kill_, by Ronan Farrow, to find out!
---

### Philip Rucker, Carol Leonnig

Carol Leonnig is an investigative reporter at the _Washington Post_. She has won three Pulitzers for her reporting on misconduct in the Secret Service, Russian interference in the 2016 presidential election, and government surveillance. She is a regular contributor to NBC News and MSNBC.

Philip Rucker is the White House Bureau Chief at the _Washington Post_. He and a team of _Post_ reporters won the 2016 Pulitzer for their reportage of Russian meddling in the 2016 US election. Rucker is an on-air political analyst for NBC News and MSNBC.

