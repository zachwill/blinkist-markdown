---
id: 590ed4b3b238e1000792cb33
slug: a-vindication-of-the-rights-of-woman-en
published_date: 2017-05-10T00:00:00.000+00:00
author: Mary Wollstonecraft
title: A Vindication of the Rights of Woman
subtitle: None
main_color: D9A236
text_color: 73561D
---

# A Vindication of the Rights of Woman

_None_

**Mary Wollstonecraft**

_A Vindication of the Rights of Woman_ (1792) is a historical text that details the gender disparities of the eighteenth century. These blinks explain the ways in which women were subservient to men at this time, why the author wanted a fairer world to take shape and how she suggested it could be done.

---
### 1. What’s in it for me? Get insight into the book that’s inspired feminists for the last three centuries. 

Today we hear about feminism and women's rights on a daily basis, but in 1792 this was not the case.

_A Vindication of the Rights of Woman_ is often considered one of the first feminist texts and has been an inspiration for feminist thinkers through the ages. Although it is over 200 years old, many of the issues Wollstonecraft raises are still debated today. These include the difference in education between boys and girls, why some traits are seen as male and some female, and how women should be part of the democratic process.

These blinks take a look at some of Mary Wollstonecraft's main ideas from this seminal work. They are crucial for anyone who wants to understand the development of feminist ideas and intellectual history.

In these blinks, you'll find out

  * how a focus on women being beautiful deprives them of the respect men receive;

  * why the way girls are encouraged to play emphasizes an existing physical disadvantage; and

  * how inequality between men and women led to bad marriages.

### 2. Women should not be looked down on as second-class citizens. 

Ever since the Bible said that Eve was made from a piece of Adam's rib for his entertainment, women have been considered mere servants to men. But if we want a fair society, this notion has got to change.

A first step would be for women to be better represented in society; for instance, by having a greater voice in literature. This may seem like a small step, but it's quite profound. Men have always been able to express their views on women, perpetuating the idea that they're lesser than men. Meanwhile, women have rarely been able to voice their disagreement. Such behavior is deemed unladylike. Plus, to prevent any kind of disagreement, women were often given mind-numbing stories to read and denied any educational materials that might help them think critically.

Ensuring that women had more meaningful representation in government would be another step in the right direction. Governments composed solely of men often fail to even consider the rights of women.

From there, we should enable women to become as virtuous as men. Women are deprived of this opportunity because real virtue can only be attained by understanding and doing — neither of which women are encouraged to do.

Instead, women are merely taught to _appear_ virtuous. That is, they're taught to focus on their beauty and grace rather than on their ability to reason and the intellectual power that would command true respect.

People do not come by such respect easily, they must earn it. Women are at a tremendous disadvantage here as they aren't allowed access to the education needed to earn the type of respect that one man has for another. Women won't be able to break down this barrier until they have access to the knowledge needed to develop virtuousness and make rational decisions that are beneficial to society.

> _"How can women be just and generous, when they are the slaves of injustice?"_

### 3. Girls should receive the same education and opportunities as boys. 

Is there any material reason why girls shouldn't be able to do all the same things as boys? Absolutely not. Yet from an early age, boys and girls are taught that they're not equal, a lesson that's perpetuated throughout their lives.

To combat this miseducation, girls should go to school alongside boys and study all the same subjects as them. Not just that, but these schools should also be public so that they're full of kids from different backgrounds.

There's a parallel to be drawn between women and the wealthy. In the fancy and expensive schools that the wealthy attend, students are often taught that they're above reason and knowledge and that such skills are irrelevant to their lives. While women are denied an education, they're indoctrinated with much the same idea.

However, such a thought is counterproductive to building a cohesive society and private schools limit children, boys included, by teaching them that they don't need to develop the same skills as children of lesser means.

A similar process occurs on the playground as girls are discouraged from developing their physical strength. While boys are told to go out and play in the fields, developing a healthy physique from a young age, girls are made to stay indoors and play with dolls.

Such a division is an absolute travesty as it simply serves to redouble the physical advantage that boys already hold over girls. So, while the physical inferiority of girls to boys is a fact, the difference wouldn't be as pronounced if girls were encouraged to engage in physical play.

By exaggerating the disparity in strength between women and men, society is simply making girls more dependent on boys. Beyond that, dressing up their dolls to be pretty only confirms for young girls that looks are the only important life pursuit.

### 4. Women are kept mentally and physically weak to enable their oppression. 

So, education and physical activity are promoted primarily to boys, but what problems does this cause?

For one, it allows men to suppress the status of women in order to sustain their own power. For the most part, these men either want to keep women as sexual objects or are simply bullies with the sole goal of absolute control.

That's why these men think women should only focus on being attractive to men. They're also careful to distinguish between attractiveness and physical strength, as the latter would be deemed manly.

After all, women are taught to measure their value based on their ability to secure a desirable husband, and getting a good husband means being pretty and docile. This focus on marriage and aesthetic upkeep that is forced upon women in place of knowledge causes them to become vicious and cruel under their controlled and calm exteriors. It also puts women in a perpetual state of competition with one another and prevents them from forming caring bonds or learning through discussion.

For example, imagine a man's single sister moves in with her brother and his wife only to be kicked out by the wife, who feels threatened by another female presence — even if she poses absolutely no threat to her marriage.

Because of such fears, women are often sneaky and deceitful to their male companions as they believe such tactics are key to maintaining these relationships. In turn, men use this behavior to justify their harsh treatment of their wives.

If women were simply taught to view themselves as equal with men, both in body and in mind, it would not only benefit their own well-being but also that of their husbands — and their marital relationships as well.

> _"I wish to see my sex become more like moral agents."_

### 5. Equal rights would encourage more stable and loving relationships between men and women. 

A man's relationship with his wife is molded by the inequality of the sexes, and men often treat their wives solely as a necessary means to procreation or a fashionable accessory.

Instead, men and women should forge friendships and not just love affairs. Such a practice would establish stronger and more sustainable bonds.

After all, friendship is far more important than simple romantic love. But accomplishing such a feat is easier said than done.

If women only _appear_ morally virtuous and don't have a proper understanding of morals and virtue, men will be inclined to treat them poorly and dismissively, especially if they happen to reject the virtues women have had projected onto them.

On the other hand, if women could truly acquire virtue and morals in the way that men do, they would earn the respect of the other sex. Friendship between men and women will only be possible when the sexes can interact as intellectual equals.

Not just that, but if men and women were equals, men would solicit prostitutes less often, and have fewer affairs. Such indiscretions are caused by the fact that men and women share little in common other than a will to sustain the human race. As a result, men tend to grow bored of their wives following the honeymoon phase.

So, instead of building a life with their wives, men seek thrills and treat their wives like strangers. At the same time, women mistrust their husbands, manipulating them whenever possible in an attempt to ensure faithfulness. However, if men and women were treated as equals, the double standard that currently allows men to sleep around — while women are forced to maintain sexual purity — would shrink.

Beyond that, if women were raised to be virtuous rather than taught to be superficially focused on their looks, they'd enjoy more ways of engaging their husbands. Such a change would liberate women from prostitution as a means of subsistence as they'd be able to support themselves with their intellect.

### 6. Greater rights for women would benefit society as a whole. 

So, women should have greater rights than they do, but not so that they can overpower men. Rather, increasing the rights of women is necessary to put them on a fair footing with men and enable them to equally contribute to the world around them.

If women were men's equals, they would be better able to bring up children and raise future generations. To this end, equal access to education would have a particularly strong impact. Well-educated women would be able to help their children with their studies. No longer would their sons have to rely on tutors to help them with their school work, and their daughters would be able to learn in their spare time, rather than being sent off to play with dolls.

Not just that, but women would also be kinder to their servants in front of their children. They would no longer feel the need to cruelly and desperately exert their power over someone else. As it is, children, especially girls, see their mothers' poor behavior and make it their own, perpetuating a culture of malice rather than respect.

And last, but certainly not least, when treated equally, women would be able to take up matters of great importance to the world, rather than simply worrying about what to wear. They could become doctors who cure previously lethal diseases. They would also be better prepared to raise healthy children and prevent the deaths of infants.

Imagine if more women knew that breastfeeding is healthy for both the child and the mother. Since women can't get pregnant while breastfeeding, the widespread adoption of this practice would end the issue of having too many babies in rapid succession, which would reduce the need for servants and ensure each child individual attention.

### 7. Final summary 

The key message in this book:

**Men and women should be treated as equals, not just for the benefit of women but for the prosperity of society as a whole. At the core of claims that women are inferior to men is an educational imbalance, wherein girls are denied access to the resources afforded to boys.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Second Sex_** **by Simone de Beauvoir**

_The Second Sex_ (1949), an 800-page feminist classic, explains how woman has been shaped into the "Other," second sex — the negative counterpart to man. By examining history, myths, biology and life experience, de Beauvoir paints a clear picture of why woman is subjugated to man, and how womankind should respond.
---

### Mary Wollstonecraft

English writer Mary Wollstonecraft was one of the first to argue for equal rights for women. Among her other works, she wrote the pamphlet, "Thoughts on the Education of Daughters."

