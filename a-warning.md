---
id: 5dfcc60a6cee07000788a295
slug: a-warning-en
published_date: 2020-01-03T00:00:00.000+00:00
author: Anonymous
title: A Warning
subtitle: None
main_color: None
text_color: None
---

# A Warning

_None_

**Anonymous**

_A Warning_ (2019) presents a harrowing view of the inner workings of the Donald Trump presidency. Penned by an anonymous White House insider, it details the tensions and turmoil behind the scenes of the most chaotic administration in modern American history.

---
### 1. What’s in it for me? A political tell-all of an administration in disarray. 

Ever since he crashed the political scene during the Republican primary back in 2015, Donald Trump has failed to follow the rules of Washington. After scraping a narrow win in the 2016 election, he has overseen an unusually tumultuous presidency.

These blinks, based on the account of an anonymous Trump administration insider, paint a dire portrait of just how out-of-hand things have gotten in the executive branch. At the center of this chaos is President Trump himself, a brash and unpredictable leader who often pushes the limits of decency and decorum. 

Filled with scandalous stories both personal and political, this is an insider's view into an administration so unstable it may rock the foundations of the entire country. 

In these blinks, you'll learn 

  * what Cicero can tell us about Trump's character;

  * why the president loves dictators; and

  * how to end this roller-coaster ride of a presidency.

### 2. Despite their best efforts, experienced officials are unable to rein in the president’s worst impulses. 

It's a crisp December morning, and Donald Trump is tweeting. With a few taps on his phone, the president informs the world he's pulling U.S. forces out of Syria. It's news to his millions of Twitter followers. Unfortunately, it's also news to the officials at the Pentagon.

The unexpected announcement runs counter to the careful plans put together by the intelligence community. Most officials believe the move is disastrous for U.S. interests in the area. As one top aide remarks, "People are going to fucking die because of this."

The decision is particularly disturbing to Secretary of Defense, Jim Mattis. The former Marine Corps general is a respected veteran and a sober, disciplined leader. He feels he cannot stand by such a short-sighted policy. Within a day, he announces his resignation.

By 2018, this story was typical for the Trump administration.

Despite being an unconventional candidate, Trump entered the White House with a fairly conventional team of advisors and cabinet officials. These political insiders, a so-called "steady state" of experienced policy-makers, were determined to keep the government operating somewhat normally. But the constant chaos caused by Trump's unstable leadership was driving them away. 

**The key message here is: Despite their best efforts, experienced officials are unable to rein in the president's worst impulses.**

The loose collection of officials who make up this "steady state" is not trying to undermine the president. They are merely trying to maintain the type of order and restraint necessary to run the country smoothly.

Trump's impulsiveness and lack of attention make this nearly impossible. Even giving the president a standard policy briefing is a challenge. Officials normally provide the chief executive with detailed reports about complex policy issues. With Trump, aides must distill their briefings down to a single point or flashy picture. Otherwise, they're ignored.

When the president does engage with issues, his solutions are often impractical or outright illegal. On school shootings, he has suggested arming teachers. On immigration, he's proposed detaining migrants at Guantanamo Bay. Rather than govern, advisors must constantly steer him away from such unwise ideas.

As pragmatic administration officials leave in frustration, those who remain are die-hard supporters, like anti-immigration hardliner Stephen Miller. The danger is that Miller and those like him will only encourage the worst aspects of Trump's character. But what exactly is Trump's character?

### 3. Donald Trump is uniquely unfit for the role of president. 

In 44 B.C., the Roman statesman Cicero wrote _De Officiis_, a philosophical work detailing the four virtues necessary to become a great leader: wisdom, justice, courage, and temperance. Some presidents have lacked one or two of these virtues. President Trump embodies none of them.

**The key message here is: Donald Trump is uniquely unfit for the role of president.**

Let's examine each of these virtues and see how Trump stacks up.

First, wisdom. Having never served in government before becoming president, Trump knows very little about how it actually works. Worse still, he shows no desire to learn. Despite this, he often claims expertise in subjects he knows nothing about. In 2015 he claimed to know "more about ISIS than the generals do," which is, of course, nonsense. 

Aides and advisors can't ignore Trump's ignorance. Behind closed doors, they refer to him as an "idiot" with less knowledge than a "fifth- or sixth-grader."

What about justice? The president fails here too. Rather than use his office to ensure the fair execution of the law, Trump uses the bully pulpit to be an actual bully. He loves engaging in petty personal bickering. As president, he has picked fights with everyone from the prime minister of Denmark to soccer star Megan Rapinoe.

The president does not display courage either. It is well known that as a young man, Trump avoided the draft using the dubious claim of having "bone spurs," a condition causing pain and stiffness to the joints. Now, as president, he hides from tough political battles as well. When he failed to accomplish his goal of repealing Obamacare, he blamed everyone but himself. He did not have the guts to take responsibility.

Finally, the virtue of temperament, which is probably the president's most glaring weakness. Trump is far from the model of restraint and moderation called for by Cicero. Just consider his treatment of women. He has called rival women everything from bimbos to dogs. Even as president, he refers to female staffers as "sweetie" or "honey."

These four faults were on full display in the aftermath of the neo-Nazi "Unite the Right" rally in Charlottesville, Virginia. Trump did not condemn the violent right-wing extremists. Instead, he called them "very fine people." 

Of course, this is just one example of the president's flaws leading to poor governing. A close examination of his presidency reveals many more.

### 4. President Trump does not uphold the principles of the party he is supposed to represent. 

Trump's critics have a saying about his political positions, "There's a tweet for everything." And in this case, the critics are right. If Trump has tweeted against something, there's a good chance he's also tweeted in favor of it. According to Trump, the Electoral College is both a "disaster for democracy" and "actually genius." Attacking Syria? That's both a great move and "very foolish!"

Trump's flip-flopping and incoherent politics are the result of governing without principles. The truth is, the president has no true ideals and defers to _convenience_ over _convictions_. Remember, this is a man who has changed political parties _five times_.

Republican leaders must constantly remind Trump which bills to support and which to oppose. Still, even with careful guidance, he often strays from even the most basic conservative fundamentals.

**The key message here is: President Trump does not uphold the principles of the party he is supposed to represent.**

To understand how little Trump adheres to conservative values, just look at three major topics: the size of the government, national defense, and economic policy. The president has failed the GOP on all three.

Let's start with the size of government. It's long been a Republican talking point that government should be small. This means less federal spending and fewer regulations. From 2009 to 2016, Republicans even worked with Obama to reduce the budget deficit from $1.4 trillion to around $500 billion. 

But what does Trump do in office? He immediately goes on a wild spending spree. In 2019, he proposed a recording-breaking budget and pushed the national debt to an all-time high of $22 trillion.

On national defense, the commander-in-chief doesn't do much better. Rather than provide a steady and strong foreign policy, Trump makes erratic decisions. Early in his term, he openly defied GOP orthodoxy by proposing to meet with Iranian leaders. Then, when Iran shot down an American drone in June 2019, the president nearly ordered an airstrike. Such an aggressive response could have sparked a full-blown war.

However, the president did start one war. A trade war with China. Which brings us to the last of the three major topics: _economic policy_. Here too, Trump is straying from conservative values. Instead of sticking to the classic GOP position advocating free trade, he has pushed for a series of tariffs. According to experts, these unnecessary fees on consumer goods risk setting off another recession.

While these unwise moves may look bad for the GOP, a president with no principles could do much more severe damage; he could undermine our entire democracy.

### 5. President Trump cares more about securing his power than protecting democracy. 

In November 2016, Trump defeated Hillary Clinton in the presidential election. For most politicians, taking control of the White House would be enough of a prize. Not so for President Trump.

Years into his administration, Trump is still fixated on his political rival. His obsession runs so deep that he frequently interrupts briefings with long, unhinged rants about Clinton. During one particular tantrum, he called out former Attorney General Jeff Sessions for not harassing the Democratic candidate enough. In typical Trump style, he yelled: "Man, he is one of the stupidest creatures on this earth God ever created!" 

As absurd as these outbursts seem, they reveal a troubling truth: the president has no qualms about using his office to attack anyone he sees as a threat. In fact, if he could, Trump would probably prefer ruling as a tyrannical king.

**The key message here is: President Trump cares more about securing his power than protecting democracy.**

The founders of the United States were wary of authoritarian rule and took special care to limit the president's power by a system of checks and balances from other branches of government.

President Trump sees things differently. Any person or institution standing in his way is part of the corrupt "Deep State," a term sometimes used by political scientists. It refers to institutions that maintain their integrity even as administrations change. Trump smears it on anyone beyond his immediate control. Bureaucrats enforcing ethics rules? The Deep State. Agencies reporting unfavorable information? The Deep State.

Trump is especially suspicious of the intelligence community. This includes the Central Intelligence Agency (CIA) and the National Security Agency (NSA). The president either ignores crucial briefings from these institutions or directly contradicts them. In January 2019, he even called these agencies "passive and naive."

He reserves even more ire for the judiciary. If a court rules against his administration — which they are entitled to do — he blasts their findings as biased or "a disgrace." In one instance, he even suggested breaking up the Ninth Circuit Court to ensure more Trump-friendly decisions, saying, "Let's get rid of the fucking judges."

Trump has no problems bending the law to protect himself. His most glaring breach of decorum came in late 2019. According to reports, the president pressured Ukraine to investigate Hunter Biden, the son of former Vice President Joe Biden. Needless to say, such a power play is more than underhanded; it's also illegal.

Moves like this threaten to destabilize the entire democratic system. And that's just how Trump likes it.

### 6. President Trump’s flippant approach to diplomacy threatens America’s stance on the world stage. 

In June 2018, representatives from France, Germany, and other major powers met in Canada for the G7 economic summit. Usually, leaders use these meetings to shore up partnerships and strengthen international relations. For Trump, it was just another venue to settle scores.

After arriving late, the president picked fights with his distinguished counterparts. He threw candy at German chancellor Angela Merkel. He mocked Japanese prime minister Shinzo Abe with crude jokes about sending his country Mexican immigrants. He even insulted the host, Canadian Prime Minister Justin Trudeau, calling him "dishonest and weak."

Coming from any other leader, the performance would have been shocking. For Trump, it's part of an emerging pattern. When it comes to foreign policy, he prefers to alienate allies and cozy up to enemies.

**The key point here is: President Trump's flippant approach to diplomacy threatens America's stance on the world stage.**

When Trump was elected, he promised to put "America first." Yet, with his actions, the president has not lived up to this vague declaration. Instead, he has stuck to a different principle: be unpredictable. 

So far, Trump's forays into international relations have been a disaster. He threatens to pull out of long-standing trade agreements. He insults world leaders on social media. Aides who overhear his diplomatic phone calls report that he brags, goes off topic, and in general, embarrasses himself.

The only leaders Trump seems to get along with are dictators, despots, and other authoritarian strong men. He's praised the violent Philippine President Rodrigo Duterte for doing an "unbelievable job." After meeting with North Korea's dictator Kim Jong Un, he called him a "pretty smart cookie." 

The most unsettling friendship Trump has is probably with Russian president Vladimir Putin. While Trump's own intelligence community warns of Putin's unscrupulous efforts to meddle in U.S. elections, the president seems unbothered by the threat. In 2018, the president even defied his advisors and arranged a private two-hour meeting with the Russian president.

So why does Trump love these authoritarian leaders? According to a top national security aide, Trump wants what they have. That is, "total power, no term limits, enforced popularity, and the ability to silence critics for good."

Unfortunately, as long as he pursues these goals, Trump will continue tanking his country's reputation abroad.

### 7. Trump’s communication style is dangerous for democracy. 

The words of a president carry a special weight. Just think of Lincoln's eloquent Gettysburg Address or FDR's cordial and comforting fireside chats. The language we get from Trump is different. The phrases this president gives us are "fake news," "nasty woman," and "alternative facts."

How a president speaks matters. It shapes the national discourse and sets the tone for how politicians and everyday Americans relate to each other. President Trump's love for coarse language and inflammatory rhetoric does real damage to the nation.

**The key message here is: Trump's communication style is dangerous for democracy.**

In politics, it's important to talk to your rivals, even when you disagree. This is how you find common ground and work out compromises. But Trump degrades the civility needed to govern. He doesn't talk with his opponents; he insults them. He's labeled his colleagues everything from "low testosterone" to "low IQ." Such talk turns even easy dialogues into arguments.

Here's an example from a meeting Trump had with Senators Nancy Pelosi and Chuck Schumer to negotiate a bipartisan infrastructure bill. Rather than chat calmly, Trump launched into a heated tirade about unrelated matters. Within minutes, the meeting ended with no roads or bridges fixed. 

Worse still, Trump is dishonest. He has a very loose relationship with the truth, and it shows in his near-constant lies, fibs, and fabrications. When Trump said he forced NATO allies to spend more on defense? That was a lie. When Trump claimed violent crime was at an all-time high before he took office? Also a lie. In fact, the _Washington Post_ found the president has made a staggering eleven thousand misleading statements since entering the White House.

Trump's verbal assault on decorum and truth is making America more divided. Just watch any Trump rally, and you'll see why. The president relishes in the mob mentality of crowds. Nothing energizes him more than leading anti-Clinton chants like "Lock her up!" and "Send her back!" 

Whether it's through rallies or tweets, Trump loves to stoke partisan fervor — and the effects are startling. A depressing Pew Research Center survey found that 85 percent of Americans feel political debate has become more negative and difficult. And when words fail, the basic functioning of democracy is at risk.

### 8. Republicans can either be loyal to their principles or be loyal to Trump, but they can’t be both. 

In 2016, there were 17 candidates running in the Republican primary. Back then, if you had asked any Republican leader, Trump would have been their seventeenth choice. He was a joke. But now, three years later, no one is laughing.

So what happened? After Trump's unexpected election night victory, the majority of Republicans changed their tune. Many who had criticized Trump now quickly became his allies. Senator Ted Cruz, who once called Trump "utterly amoral," now calls him "bold" and "courageous." During the campaign, South Carolina Representative Mick Mulvaney said Trump was one of "the most flawed human beings ever to run for president." Mulvaney is now Trump's third chief of staff.

These U-turns are embarrassing but they are also troubling. More and more Republicans are becoming Trump apologists, and few are willing to stand up to him.

**The key message here is: Republicans can either be loyal to their principles or be loyal to Trump, but they can't be both.**

So who are these Trump apologists? They generally fall into two different camps. There are the _Sycophants_ and the _Silent Abettors_. 

The Sycophants are Trump's ardent fans. They truly believe the president can do no wrong. These are the aides who go on television to defend even the worst decisions their leader makes. These Sycophants are simply delusional. Luckily, they're relatively rare.

In contrast, the second group, the Silent Abettors, are more common. These are people who know Trump is a disaster but keep working for him anyway. They continue serving the president because they want power — and they're willing to abandon their values to get it. 

This often means sitting quietly by as the president makes terrible decisions. For example, in 2018, Trump shut down the federal government in a vain attempt to get money for his Mexican border wall. Most of his aides and staffers knew this was a terrible idea, but no one spoke up. They would rather see the nation at a standstill than risk upsetting the commander-in-chief. As one aide put it, "This place is so fucked up, there is literally no one in charge here."

Those who do challenge the president often suffer consequences. Just look what happened to Steve Bannon. Once one of Trump's closest advisors, Bannon was ousted from the White House for giving dissenting views on the administration's policies.

If those in government won't speak out, who can? The truth is, if the American people want to stop Trump, they must take action themselves.

### 9. The best way to end the Trump presidency is to vote him out of office. 

For many people, the sooner Trump's presidency ends, the better. Even those working within the White House have considered taking steps to oust Trump.

How would this happen? It's tricky. Trump's closest advisors would have to invoke a little-known provision attached to the Twenty-fifth Amendment. This constitutional power lets cabinet officials remove a president they consider unfit for its duties from office. If this were to occur, Vice President Pence would be left in charge.

While there has been no formal talk of invoking the Twenty-fifth Amendment, there have been whispers. Senior White House officials have even kept an informal "whip count" of who may be willing to sign on to the plan. It's been speculated that Pence would give his approval if asked.

Ultimately though, staffers consider this scenario too risky. Such a move would be incredibly disruptive, and there is no guarantee that Trump would leave office without a fight. His supporters could resort to violence. These same concerns make impeachment a dicey proposition as well.

**The key message here is: The best way to end the Trump presidency is to vote him out of office.**

American democracy already has an effective system in place to do away with unwanted leaders: elections. If citizens are truly dissatisfied with how the government is currently run, it is their duty to make their voice heard at the ballot box.

In 2020, the public has another shot at electing their leader — and it must choose wisely. Citizens must ask themselves tough questions. Does Trump reflect their values? Is a man of his temperament really fit for the highest office? Can the nation withstand another four years of erratic rule? Surely, the answer is no.

Republicans have a special responsibility here. The choice will be between Trump and a Democratic candidate. Last time, many conservatives opted for the former. By now, it should be clear that was the wrong choice. Just remember, even if you vote against Trump, you can always vote for more respectable Republican candidates in other races.

If Trump somehow manages to get re-elected, there's no telling what disasters and disorder it will lead to. However, one thing is certain: whatever occurs will be the result of our decisions and our responsibility. Choose wisely.

### 10. Final summary 

The key message in these blinks:

**Donald Trump is a man uniquely unfit to be the president of the United States. His brash demeanor and lack of political principles make his administration chaotic and unstable. Those senior officials who keep his worst impulses at bay are slowly losing their influence. If we are not careful, his presidency could deepen the divisions in American society and plunge the nation into authoritarian rule.**

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Liars, Leakers, and Liberals_** **, by Jeanine Pirro.**

You just got one insider's take on the current situation within the White House. Still curious about what's happening at the highest level of the American government? Get a completely different point of view with _Liars, Leakers, and Liberals_ by Judge Jeanine Pirro.

This book-in-blinks offers an alternative view of reality from one of President Trump's favorite media figures, former prosecutor and host of _Justice with Judge Jeanine_ on the Fox News Channel, Judge Pirro. She makes her case that a shadowy cabal of liberals is working to undermine the Trump administration.
---

### Anonymous

The author, who remains anonymous, is a senior Trump administration official who made headlines with a _New York Times_ op-ed titled "I Am Part of the Resistance Inside the Trump Administration," published in the fall of 2018. While many have speculated as to the author's true identity, the individual has chosen to remain nameless to avoid compromising their position.

