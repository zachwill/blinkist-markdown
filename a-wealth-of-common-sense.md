---
id: 576177ea0beabd0003161b19
slug: a-wealth-of-common-sense-en
published_date: 2016-06-21T00:00:00.000+00:00
author: Ben Carlson
title: A Wealth of Common Sense
subtitle: Why Simplicity Trumps Complexity in Any Investment Plan
main_color: D4353A
text_color: A1282C
---

# A Wealth of Common Sense

_Why Simplicity Trumps Complexity in Any Investment Plan_

**Ben Carlson**

_A Wealth of Common Sense_ (2015) reveals how sound decisions can lead you to long-term success as an investor. These blinks provide the tips that every investor should know from the outset and explain how you can create a diverse, consistent strategy that will stand the test of time.

---
### 1. What’s in it for me? Become an unbreakable investor. 

Maybe in another world it's possible to become rich in an instant — just think of fairy tales, where the hero stumbles on an immense treasure, becomes the richest man in the kingdom and lives happily ever after. If you browse the internet for investment advice, some "experts" seem to live in that world, too (or maybe they just like fairy tales).

Sadly, their investment schemes don't work in _this_ world. But you can become a very successful investor. And to get there, you don't even need to study finance. This pack will tell you how to use your common sense to make a solid investment plan and compose your personal portfolio — one that matches your goals, your temperament and your situation in life.

Additionally, you will learn about a few common and costly mistakes — and about the best way to keep your money safe in turbulent times.

You'll also discover

  * why Yale's investment strategy won't do you any good;

  * the benefits of neglecting your investment account; and

  * why you're not Marty McFly — and what that means for your portfolio.

### 2. Investors aren’t all equal. 

Ever toy with the idea of adopting the same investment strategy as a filthy-rich company? Even if it was risky and complicated, it worked for them. Why shouldn't it work for you, too? Well, for a number of reasons! Institutional investors face very different conditions to individual investors like you.

First of all, trading is less expensive for institutional investors. Why? Their size gives them the leverage they need to negotiate lower fees when dealing with investment platforms. Secondly, institutions employ several professionals, even full-time staff members, to manage their portfolios on a day-to-day basis. This is something that most individual investors simply can't afford.

Not all institutional investors are created equal, either. The amount of funds available differs widely, and so too do the deals they get into as a result. Take Yale University. Yale's endowment fund enjoys hundreds of millions of dollars in grants and donations annually. All of this is managed by chief investment officer David Swensen. So far, Swensen has done a great job. With 14 percent gains every year since the mid-1990s, Swensen's portfolio management style is widely admired, and even earned its own name: the "Yale Model."

Most institutions can't invest on the same massive scale as Yale. And only large-scale investors, like Yale, can afford the high minimum investments required to get into the funds that are so attractive because of their low management fees.

Yale is not only a large-scale investor, but also a nonprofit. Nonprofits benefit from additional advantages that other investors can't access. As a university, Yale has a perpetual time horizon. This means that there is no time limit on when an investment pays out. This is particularly handy because it means Yale is not obliged to restrict its strategy to short-term investments. Non-profits may also be exempt from paying taxes whereas a portfolio conducted on the same level by a private investor would face significant tax burdens.

In short, the investment strategies of institutional giants won't help you out much. To succeed as an individual investor, you'll need to find your own path. Before you set out on your journey, let's investigate a few common mistakes that you'd be wise to avoid.

> _"Large pools of capital get a foot in the door [of certain deals] simply for having so much money at their disposal."_

### 3. To start your investing journey, you need to know what not to do. 

While many books explain what you need to _do_ as a successful investor, few reveal what _not_ to do. And yet being able to avoid bad habits and silly mistakes can have a huge impact. According to financial advisor Nick Murray, investors who correct common mistakes can boost their yearly returns by 3 to 4 percent.

If there's one thing you should avoid from the outset, it's _expecting to get rich fast._ Today, we're nothing short of obsessed with discovering the key to immediate wealth. Truth is, it doesn't exist! Anyone claiming to have the secret to instant success is either fooling themselves or attempting to fool you. Don't pay attention to them!

Another common mistake is _overconfidence_. Markets are incredibly difficult to predict, due to the countless variables we humans can't control. Overconfident investors tend to forget this, and call the shots as if they know exactly how the future will pan out. For example, they might invest a lot of money into a stock that did well for a few months, and end up suffering considerable losses not long after that.

Finally, resist the temptation to _follow the herd_. We tend to do what everyone else does because it feels safer — after all, so many people can't all be wrong, right? Unfortunately, they can.

We saw this in the real estate bubble in the mid-2000s. People bought property they couldn't afford because everyone else was also buying more than they could afford. But the bubble burst and lives were ruined. So, do your investing career a favor and always think for yourself!

Of course, knowing what to avoid isn't everything. Success as an investor also relies on a few key characteristics. Find out if you have what it takes in the next blink!

> _"If you can get good at destroying your own wrong ideas, that is a great gift." - Charlie Munger_

### 4. Successful investors are emotionally aware, keep their cool and stay wary. 

You might consider yourself fairly intelligent. But is this enough to achieve success? Depends. Despite what you might think, a high IQ isn't the only marker of intelligence. It's emotional intelligence that will keep you afloat.

Emotional intelligence is the ability to recognize and manage our own feelings and those that exist between people, says psychologist Daniel Goleman. In other words, we need to know how our emotions influence our actions and the people around us. But what does this have to do with investing?

A great deal. An investor who feels upbeat and adventurous on a given day is likely to make reckless, potentially devastating decisions if he isn't careful. Recognizing when your emotions are clouding your rational judgment could save you a lot of trouble!

Another trait that successful investors have is the ability to stay calm and composed. Yes, even in situations where your finances are in deep trouble! But how? Well, let's look back in time for an example.

It's the 1989 Super Bowl, and things aren't looking great for the San Francisco 49ers: Time is running out, and the opposing team is ahead by three points. Despite this, quarterback Joe Montana keeps his cool, reassures his teammates and scores a perfect touchdown to win the game. His ability to stay composed enabled him to win four Super Bowls!

Investors can take a leaf out of Montana's book when facing market crashes or an economic crisis. Rather than panicking and selling off their assets for pennies, they've got to keep it together to assess the situation with a clear head and create a sustainable strategy.

Last but not least, good investors are always _wary_. In other words, they know when they _don't_ know something. If they don't know their way around a market or scheme, they stay out of it. For instance, if you wanted to invest in the Chinese stock market, you'd have to ask yourself first whether you _truly_ understand it. If not, don't get involved. You simply won't be able to identify risks, like a developing bubble, until it's too late.

So, if you've got all these traits down pat and are determined to become an investor, what's next? Start your journey by learning about the risks you'll face along the way.

> _"Real knowledge is to know the extent of one's ignorance." - Confucius_

### 5. High rewards come with high risk. 

Risk is a word that gets thrown around a lot by investors. But it can mean many different things to different people. For some, risk is tied to losing money, whereas others see it as volatility. But in every case, risk is always attached to _rewards_, a relationship which also shapes your investment choices.

To put it simply, greater risks yield greater rewards. In other words, don't expect big payouts when you play it safe. And by the same token, be prepared for a bumpy ride if you're hunting big rewards. Nothing is free! This is the case with different asset classes in investment.

Consider the average yearly returns for stocks, bonds and cash from 1928 through to 2013 (all corrected for inflation): Stocks return 6.5 percent, bonds 1.9 percent and cash 0.5 percent.

Stocks yield the highest returns, and they're also susceptible to the greatest losses at particular times. Why are stocks so inconsistent? Well, their value is based on the dividends and other earnings they'll yield in the future. But these are dependent on the humans running the business and the nature of the financial market. Stocks, in other words, involve a lot of _risks_, as reflected in their high-risk premium.

Bonds earned three times less than stocks and are considered a little less risky. Why? Investors are more likely to get their returns within a fair amount of time. This, in turn, is reflected in the lower risk premium of stocks.

Cash is the safest of them all, with stable annual returns and no losses. But this safety is accompanied by lower returns: With annual returns of a mere 0.5 percent (after inflation), you'd have to wait 150 years to double your investment!

Now that we've investigated the benefits and risks of these three asset classes, you're ready to start planning your journey. Learn how to create your personal investment roadmap in the next blink.

> _"If there is an ironclad rule in the world of investing, it's that risk and reward are always and forever attached at the hip."_

### 6. Create an investment plan tailored to your personality. 

Ever taken a personality quiz? They're a lot of fun, and they can come in handy when you start investing! Why? Because investors need to define who they are and find the label that fits them best.

Are you a trend follower, a short-term trader or a diversified asset allocator? The list goes on, and it's your job to find one that fits your personality like a glove. Ask yourself what your core investment values are, and whether a certain style fits your behavioral habits and personal situation. Are you a risk-taker? Or do you want to hold your assets for years?

With this figured out, you'll need to create your own _investment plan_. And don't underestimate the importance of this step! Every time you're ready to make an impulsive move, a glance at your plan will keep you from acting recklessly.

An investment plan helps you chose what to do each day in order to reach your long-term goals. From which kinds of stocks and bonds to combine in your portfolio, to which circumstances you plan on buying and selling in, your plan will nip unnecessary errors in the bud.

This is rather like how Alabama Crimson Tide coach Nick Saban led his team to win four national championships. Rather than following every new trend in offensive and defensive ballplay, Saban makes one plan only. And his team adheres to this plan religiously. With your own tailor-made investment strategy, you'll be able to resist the dodgy advice of self-appointed investment "gurus." Stick to your guns and reap the benefits.

### 7. For your future’s sake: create a diverse portfolio and stick to it! 

Have you seen _Back to the Future Part II_? In this film, Marty McFly travels to the future and buys a record of sports statistics to take back to the past. Armed with the knowledge of every sports event in the "future," he hopes to make a killing betting on the results. But, unlike Marty McFly, we have no idea what the future will hold. But we can make our bets safer by investing in a diverse range of assets.

By diversifying across different asset classes and risk factors, we can prepare ourselves for the worst. If anything goes wrong in one class, our losses will level out through the gains made in the other classes. Having your portfolio spread across multiple assets might mean you miss out on extra big gains when one of your assets does well, but this is an acceptable sacrifice to make to safeguard yourself against going broke.

Once you've allocated your assets, don't reallocate them unless you have a very good reason to do so. Changing your thoughtfully structured portfolio spontaneously is bad news. A study by _Fidelity Investments_ revealed that the top-performing portfolios were those the owners had forgotten about. In other words, the portfolios where nothing had been changed for years.

Continuously second-guessing your portfolio decisions can create extra trading costs, tax implications and a psychological burden. If you're certain you want to change your portfolio, you'll need reasons more substantial than "because I was afraid that . . ." or "because I got so excited about . . ." The market fluctuates, and these fluctuations should never be a reason to change your portfolio.

### 8. Final summary 

The key message in this book:

**Stay aware of your financial situation, your personality and emotional states when making investment decisions. With a simple, consistent strategy and thoughtful approach, common sense is your guide on your journey to becoming a successful investor.**

Actionable advice:

**Put your investment plan in writing.**

Don't just rely on your memory! Get your investment plan written down. It doesn't matter how long it is — the process of writing it is crucial. Why? Because to get something on paper, you need to be specific about how you're going to manage your portfolio. This is a central aspect of successful investing.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Little Book of Common Sense Investing_** **by John C. Bogle**

_The Little Book of Common Sense Investing_ provides a detailed overview of two different investment options: actively managed funds and index funds. These blinks explain why it's better to your money in a low-cost index fund instead of making risky, high-cost investments in wheeling-and-dealing mutual funds.
---

### Ben Carlson

Ben Carlson is the director of institutional asset management at Ritholtz Wealth Management, specializing in financial planning and asset management. He's also the blogger behind acclaimed site [www.awealthofcommonsense.com](<http://www.awealthofcommonsense.com>), which provides vital information about wealth management, financial markets and investor psychology.

