---
id: 51483c90e4b0f71597bd1354
slug: a-whole-new-mind-en
published_date: 2014-01-15T11:30:00.000+00:00
author: Daniel H. Pink
title: A Whole New Mind
subtitle: Why Right-Brainers Will Rule the Future
main_color: F26230
text_color: A64321
---

# A Whole New Mind

_Why Right-Brainers Will Rule the Future_

**Daniel H. Pink**

In an age where computers and well-trained workers from low-paid countries are taking over even white-collar jobs, what can you do to stand out? As we move out of the Information Age and into a new Conceptual Age, the answer is to start embracing the aptitudes associated with the right side of your brain, which were previously thought of as less valuable than analytical left-brain skills.

---
### 1. Our brain has two parts: the left hemisphere for details and the right hemisphere for more holistic, big-picture thinking. 

Since ancient times, people have assumed a division of the brain into a left and a right hemisphere, a neurological divide that has been supported by modern science.

While today we know that every activity we engage in requires cooperation between both the right and left hemisphere, we are also aware that each hemisphere takes the dominant role in certain activities. Generally, we can say that the left hemisphere focuses on breaking things into details, while the right hemisphere is in charge of providing the broader picture.

These differing roles can be seen, for instance, in the context of language use. Much of our language originates in the left hemisphere, where we process symbols in sequence (for example, when reading). However, the right hemisphere also plays an important role by allowing us to take a step back from the language itself and interpret the context of the message. Without our right cerebral hemisphere, we would not be able to understand irony or metaphors.

Reasoning is another area where the hemispheres have different, complementary roles:

Responses that originate from the left are derived from what we have learned in the past. If someone points a gun at you, it's the left hemisphere that tells you to be alarmed because you have learned that guns are dangerous.

The right hemisphere, on the other hand, doesn't recognize the gun, but it can draw on more intuitive knowledge and recognize other signs of danger, like an angry facial expression. The fact that all cultures tend to interpret facial expressions similarly illustrates how natural and intuitive these functions of the right hemisphere are.

We have always sought to understand which part of our brain is responsible for different activities. Today we know that although the two halves are constantly cooperating, they specialize in different ways of thinking.

**Our brain has two parts: the left hemisphere for details and the right hemisphere for more holistic, big-picture thinking.**

### 2. Historically, the importance of the right side of the brain and the way of thinking it represents has been undervalued. 

Ever since we discovered that the left side of the brain is responsible for more analytical tasks, it has been viewed as being of greater importance than the right side.

But where does this idea of the hemispheres being "separate but unequal" come from?

First of all, it was once thought that because the left hemisphere allows people to solve analytical  

tasks, it must be the side that separates us from animals.

Second, we know that the left side of the brain actually controls the right side of the body, which is responsible for many important movements in a world where most people are right-handed and where Western language is written from left to right.

This difference in appreciation also manifests itself commonly when the two halves and their different modes of thinking are used as metaphors for different approaches to life:

Left-Directed Thinking is thought to draw on traits from the left hemisphere, meaning it is sequential, literal, functional, textual and analytic. These traits would then also dominate a Left-Directed thinker's approach to life.

Right-Directed Thinking, on the other hand, would be characterized by being simultaneous, metaphorical, aesthetic, contextual, and synthetic — all traits of Right-Directed thinkers.

Much as the left side of the brain has been appreciated more than the right, Left-Directed Thinking has been viewed as the more successful approach to life. This can be seen on a societal level — for example, in the exams students must take in the United States. These exams reward linear, sequential thinking to arrive at a single correct answer in the time allotted, and thus teach students to reason like computers.

Today, however, the undervaluing of Right-Directed Thinking is slowly coming to an end, as the ability to see the "big picture" is growing in importance.

**Historically, the importance of the right side of the brain and the way of thinking it represents has been undervalued.**

### 3. As we move from the Information Age to the Conceptual Age, Right-Directed Thinking is becoming more and more important. 

Today, we live in an era of abundance enabled by left-brain thinking.

How?

To be successful in the Information Age, you had to accumulate a lot of knowledge, most importantly by getting a college education. As this option became increasingly available to everyone, more and more people were able to follow this inherently left-brain method to success and become well-paid white-collar workers. Thus, the Information Age has contributed to the economic growth and rising living standards we now enjoy.

However, this situation is changing. Today, Right-Directed Thinking is becoming more and more important for a number of reasons:

First of all, certain right-brain directed aptitudes are highly prized: these are a sense for design, story, symphony, empathy, play and meaning. Someone who develops their aptitude in these areas gains a crucial competitive advantage in today's crowded marketplace, especially as left-brain intensive white-collar work is increasingly being outsourced to low-cost countries.

But Right-Directed Thinking is also gaining importance outside the office. Living in this era of abundance has made us yearn for the immaterial: we search for purpose and meaning in life, and Right-Directed Thinking is how people excel at such ambiguous tasks.

These developments are part of a change of eras, as we move away from the Information Age into the so-called _Conceptual Age_. In this age, it is no longer an aptitude for "high tech" that is important; rather, the skills in demand fall under the categories of _high concept_, meaning the ability to combine unrelated ideas from different spheres into completely new inventions, and _high touch_, meaning the ability to empathize with others. The rising demand for these attributes can be seen in the fact that students are now taught classes on developing their creativity and increasing their empathy.

As can be seen, the move towards the Conceptual Age is happening everywhere: at school, at the office, and in our hearts and minds.

**As we move from the Information Age to the Conceptual Age, Right-Directed Thinking is becoming more and more important.**

### 4. Design: In an age of material abundance, design has become crucial for most modern businesses. 

If you look around, you'll find that every object in sight has been carefully designed, from the furniture you're sitting on to the clothes you're wearing.

But what constitutes good design?

Basically, good design combines _utility_ with _significance_. For example, a graphic designer making a brochure will try to use good design to make the brochure easy to read (utility) and to transmit ideas that the readable words themselves cannot convey (significance).

The ability to create good design requires both kinds of thinking: Left-Directed Thinking helps find the utility component, whereas Right-Directed Thinking helps find creative ways to add significance.

Today, good design is more important than ever. This is because people are more aware of what constitutes good and bad design. For example, these days most people can easily distinguish between different fonts like Times New Roman and Arial, but just 20 years ago, only typesetters would have been aware of such knowledge.

Also, people are more aware of the impact that design can have:

For example, a poorly designed ballot during the US election in the year 2000 actually confused so many voters that it is likely the outcome of the election would have been different if the ballot had been better designed.

Also, research indicates that improving the design of a school's physical environment can increase the performance of students.

This trend means that product design has become a crucial aspect of businesses. It is no longer enough to produce functional products at a reasonable price; customers demand good design.

This trend can be seen even in such trivial objects as kitchen utensils: bottle openers now come shaped like animals, and spoons may have faces imprinted on them.

Research at the London Business School shows that for every percent of sales invested in product design, a company's sales and profits rise by an average of 3 to 4 percent.

**Design: In an age of material abundance, design has become crucial for most modern businesses.**

### 5. Story: To be successful today, presenting facts is not enough; you must know how to tell stories. 

Throughout history, telling stories has been an integral part of the human experience. We tend to remember stories far better than individual facts, and we also tend to see our lives as stories: they begin somewhere and unfold from there into a certain direction. Looking at life as a story gives us a sense of purpose and meaning.

In the Information Age, factual knowledge was very important: knowing more than others allowed you to stand out from the crowd. Yet nowadays, as we move into the Conceptual Age, facts can be accessed easily online, and so knowledge has become less relevant.

Instead, what is important is an aptitude for _story_ : the ability to place facts in context and deliver them as a story.

Indeed, in various fields of business such as advertising and consulting, being able to tell a story is crucial to success.

Similarly, the ability to understand and tell stories is becoming increasingly important for doctors. Nowadays, many medical schools are offering their students courses in the humanities so they can become more sensitive to the stories their patients tell.

Telling stories has also become a key way for companies and individuals to make their goods and services stand out in a crowded marketplace. This is because people think of their lives as stories, so they also find it easier to relate to companies and products if they hear about them as stories, not as a series of facts.

There is even a movement called "organizational storytelling," where organizations collect the stories their employees tell so that the company has relatable human stories to share. For example, rather than relying solely on manuals to train their technicians, Xerox collects all the stories their technicians tell about repairing machines and passes them on for others to learn from.

These phenomena demonstrate how, in the Conceptual Age, storytelling is becoming more important than ever.

**Story: To be successful today, presenting facts is not enough; you must know how to tell stories.**

### 6. Symphony: Putting the pieces together is more important than taking them apart. 

In the past, what was important in most jobs was the ability to acquire knowledge. Typically, this was done by looking at a complex topic, taking it apart and memorizing the individual pieces of knowledge it consists of. This approach is also emphasized by the modern education system: disassemble and memorize.

But today, as knowledge is widely available (for example, online), it is no longer necessary to store individual facts in your memory. Instead, what has become important is an aptitude for _symphony_ : being able to combine individual elements and put them together to form something greater, much like a composer or conductor does with music.

This is because, first of all, we live in a very diverse age, and people who can combine expertise from multiple cultures and languages are better suited for such an age.

Second, the life cycle of a new product is becoming increasingly short: yesterday's innovation is tomorrow's near-obsolete commodity. This means companies must constantly innovate, and the best innovators are those able to combine unrelated ideas and thoughts into something new. For example, when George de Mestral saw how burdock seeds stuck to his dog's fur thanks to the tiny hooks that covered them, he realized he could use something similar to bind materials together. This led him to invent Velcro.

The aptitude for symphony also helps people see the big picture instead of the individual parts, which can bring both success and happiness.

Consider a study that showed self-made millionaires are more likely than average to be dyslexic. This is significant because dyslexics struggle with linear reasoning but excel at broad, big-picture thinking — a trait that seems to make them more successful than average.

Also, big-picture thinking can be useful in your personal life: it is easier to deal with stress and worries if you see that they are just small parts of the big picture.

**Symphony: Putting the pieces together is more important than taking them apart.**

### 7. Empathy: As computers take over more and more tasks, abilities unique to people, such as empathizing with others, become more important. 

Many of the jobs that existed in the Industrial Age and Information Age are increasingly being taken over by computers and machines — for example, assembly-line work and data analysis.

In the face of this development, it is comforting to know there are some tasks that computers will never be able to do. While any work that can be reduced to a set of rules that must be followed can be given to machines or outsourced to other countries, tasks that demand a deeper understanding of the subtleties of human interaction are more immune to such developments.

At the core of such tasks is _empathy_, which is important in a variety of fields.

For example, think of lawyers: whereas legal research could be done by smart algorithms or lawyers elsewhere in the world, the crucial part of a lawyer's job — to empathize with clients and understand their needs — is irreplaceable.

Similarly, physicians cannot be totally substituted by computers because computers do not feel empathy, and patients want their doctors to empathize with them. In fact, empathy is increasingly being recognized as a very important part of healing.

Not only is empathy important in the professional sphere but it is necessary for understanding others in general. Empathy is a universal language, and research has shown it is communicated mostly through universally understood facial expressions. Also, an aptitude for empathy supports and enables other aptitudes, like design and story, as it helps you to see things from another person's perspective.

The good news is that everyone can learn to become more empathetic. For example, at Stanford Business School, students are already taking classes in "interpersonal dynamics." Likewise, people from government agencies like the FBI and CIA receive training in reading facial expressions, the language of empathy.

**Empathy: As computers take over more and more tasks, abilities unique to people, such as empathizing with others, become more important.**

### 8. Play: Lightheartedness will likely play an ever more important and beneficial role both at work and outside of work. 

You may have heard that one should not mix business with pleasure, or work with play. But today, introducing elements of _play_, meaning humor and lightheartedness, into all areas of life is not only more common but necessary.

To understand how much more prevalent the element of play is, consider the huge industry that has grown around video games. They are now an essential part of the lives of young Americans, with half of the population over six years of age playing video games.

This increased tendency to play has brought with it some positive influences. Research has shown that video games can promote skills vital in the Conceptual Age. For example, the most popular video games are role-playing games (i.e., simulation games), and these can improve players' aptitude for empathy.

Another element of play, humor, is also becoming increasingly valuable in the world of work because humor reduces hostility, deflects criticism, relieves tension, improves morale and helps communicate difficult messages.

And which side of the brain is humor generated in? The right side, because humor requires skills specific to it: placing situations in context, seeing the big picture and seeing things from new angles.

At the same time, this new-found joyfulness is likely to make us more productive and fulfilled — both in private as well as professionally. For example, research has shown that laughter decreases stress hormones and boosts the immune system, and that playing video games at work can increase productivity and job satisfaction.

**Play: Lightheartedness will likely play an ever more important and beneficial role both at work and outside of work.**

### 9. Meaning: Now that our material needs are met, we search for meaning, purpose and fulfillment in life. 

Most people feel the desire to seek some kind of meaning in their lives; it is a fundamental drive in humans and has become one of the essential aptitudes of the Conceptual Age.

Nowadays, with aging populations and the constant threat of terrorism, and with increasing prosperity thanks to technological advancement, spiritual and immaterial concerns are becoming of profound importance to people.

This means that _spirituality_, the search for the higher meaning and purpose of life (for example, through religion) has recently become increasingly important — both in business and in private life.

This is exemplified by the fact that employees today increasingly want to experience greater spirituality at their workplaces. The rise of new businesses like yoga studios and "green" products also demonstrate how spirituality and the new emphasis on meaning are affecting the business landscape.

Similarly, increased spirituality is impacting our personal health. Studies have shown it can improve maladies and it reduces the risk of suicide. It has even been found that people who regularly go to a place of worship like a church, mosque or synagogue live longer than others.

These effects are why many medical schools now have courses on spirituality, and why many physicians take some form of spiritual history of their patients.

Of course, the pursuit of meaning is strongly connected to the pursuit of happiness. While happiness is derived from a mix of biological and environmental factors, people who find meaning in life are also the most likely to be happy.

**Meaning: Now that our material needs are met, we search for meaning, purpose and fulfillment in life.**

### 10. Final summary 

The key message in this book:

**It used to be thought that right-brain aptitudes are useless. But as we move from the Information Age into the Conceptual Age, they are becoming increasingly important. Being innovative, creative and empathetic are traits that help us succeed professionally, while also increasing our personal well-being. While left-brain thinking is still important, it is insufficient on its own.**

The questions answered in this book:

**How do the two sides of the brain differ, and why is appreciation for the right side growing?**

  * Our brain has two parts: the left hemisphere for details and the right hemisphere for more holistic,big-picture thinking.

  * Historically, the importance of the right side of the brain and the way of thinking it represents have been undervalued.

  * As we move from the Information Age to the Conceptual Age, Right-Directed Thinking is becoming more and more important. 

**In the Conceptual Age, which six right-brain aptitudes are becoming essential?**

  * Design: In an age of material abundance, design has become crucial for most modern businesses.

  * Story: To be successful today, presenting facts is not enough; you must know how to tell stories.

  * Symphony: Putting the pieces together is more important than taking them apart.

  * Empathy: As computers take over more and more tasks, abilities unique to people, such as empathizing with others, become more important.

  * Play: Lightheartedness will likely play an ever more important and beneficial role both at work and outside of work.

  * Meaning: Now that our material needs are met, we search for meaning, purpose and fulfillment in life.

**Suggested further reading: _You Are Not Your Brain_ by Jeffrey M. Schwartz and Rebecca Gladding**

_You Are Not Your Brain_ explores our deceptive brain messages which program us to have harmful thoughts such as "I'm not good enough." And it tells us how we can change this detrimental wiring by challenging these brain messages and focusing our attention elsewhere. In doing so, we can rewire our brain to make it work _for_ us, not against us. To find these blinks, press "I'm done" at the bottom of the screen.
---

### Daniel H. Pink

Daniel H. Pink is an American author of bestselling books on business, management and work. _A Whole New Mind_ (2005) was a long-running _New York Times_ and _BusinessWeek_ bestseller and has been translated into 20 languages.

