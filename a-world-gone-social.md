---
id: 5460aed431363000084b0000
slug: a-world-gone-social-en
published_date: 2014-11-10T00:00:00.000+00:00
author: Ted Coiné and Mark Babbit
title: A World Gone Social
subtitle: How Companies Must Adapt to Survive
main_color: D82B3D
text_color: A6212F
---

# A World Gone Social

_How Companies Must Adapt to Survive_

**Ted Coiné and Mark Babbit**

Social media isn't a temporary fad — it's changing business culture in a big way. _A World Gone Social_ explains why it's important for companies to evolve their own social media tactics, and includes helpful tips for business owners who want to embrace new technologies and build them into their gameplay.

---
### 1. What’s in it for me? Learn how to use social media to tap into a gigantic pool of potential customers. 

Social media isn't a temporary fad that's only relevant for too-cool-for-school teens and twentysomethings. Today, social media platforms are changing our lives by shaping our decisions and choices. There are over two billion people active on online networks, so social media offers business owners a huge competitive advantage: By reaching customers wherever they are, you're tapping into a gigantic potential market.

But to see how you can really leverage social media to engage with customers, you need to understand the immense power the consumer holds in this new social age. Today, a single customer has the potential to influence thousands of other people in just a few short moments.

You'll also learn

  * why you should encourage your employees to complain about their workplace;

  * how to sell shoes on Twitter; and

  * why you should never, ever send out automessages.

### 2. In the age of social media, the human side of business has become extremely important. 

Many still believe that social media is a passing fad, and that popular platforms like Facebook and Twitter will one day simply go away. But these people are dead wrong.

Social media is not only changing the way we live, it's also profoundly altering the way companies operate by forcing business to prioritize the _human side_ — not just the bottom line.

Here's one way that's happening: Social media has produced a strong incentive for companies to create appealing working environments in order to attract top talent.

This wasn't so important prior to social media, when jobseekers relied on recruiters and HR teams to form an impression of a company. But now, candidates can use websites to consult current and former employees, to find out what working at the company is really like.

Ten years ago, if you were unsatisfied with your job, there was little recourse. Today, however, you can share your experiences online, thus influencing new applicants and potentially limiting your company's ability to attract top talent.

But social media hasn't only changed the way companies operate internally, it's also created a huge shift in the way products are marketed to the public. Today's marketers are increasingly transferring emphasis to the human side of their products, as well as focusing on how to reach targeted groups of consumers.

_Rolling Stone_ was once the dominant authority in pop music — but that's all changed. Today, bloggers and digital influencers are the primary generators of buzz.

And the record industry is paying attention: These digital influencers sometimes receive review copies ahead of major authorities like the _New York Times_.

So, instead of the huge, top-down systems that once supported a strict hierarchy, today small agents, like start-ups and freelancers, have become increasingly powerful.

### 3. A single viral video can irreversibly damage a company’s reputation. 

Many companies believe that losing one customer won't really impact their bottom line. While that might have been true in the past, it's seriously misguided today.

That's because social media platforms can amplify a single voice, making it heard by millions of people.

For example, have you ever heard of Dave Carroll? While seated on a parked United Airlines plane, he saw baggage handlers throwing his guitar around on the tarmac. When he was reunited with his guitar at his final destination, Carroll saw that it had been destroyed beyond repair. He tried to resolve the situation with United's customer service agents, but the airline refused to replace his guitar.

So instead, he recorded a song called "United Breaks Guitars," which amassed over 14 million views on YouTube. United's questionable policies got a lot of attention.

Carroll's story demonstrated the huge power shift that's occurred between customer and firm. Today, the individual customer does indeed matter.

And within this new climate, it can be hard — and sometimes impossible — to repair a bad reputation. After all, a single viral video or tweet can effectively destroy millions of dollars in marketing efforts.

In Dave Carroll's case, a new guitar would have cost United $3,500. That's a paltry sum compared to the losses United suffered to its brand image, when 14 million users were informed about the airline's bad customer service.

But even non-viral messages can reach enough people to damage a company, because every social media user exists within a network of other users — which means he or she can potentially influence other people's decisions about whether or not to purchase a product.

Of course, it's not all bad: social media can also have positive effect on a company's reputation. And as we'll see once more, the way the firm treats employees can have a huge impact on its public image.

> _"You only need a Robin Hood-style just cause, a heartbreaking moment, or a moral outrage — and you are on your way."_

### 4. Engagement with employees and customers via social media is the first step to building a great business. 

Every so often, a scandal sweeps through the media, exposing a company's shameful mistreatment of employees. Although the incident likely won't destroy the company, it will tar its public image.

Today's most socially savvy companies do their best to avoid this scenario by engaging with their workers and figuring out how to meet their needs. This is the very first step to building a great business.

That's because happy employees don't just boost a company's reputation, worker well-being is also linked to higher productivity, sales growth, better profits and customer satisfaction. Who wouldn't want that?

So how do you go about establishing employee engagement at your company? Many CEOs mistakenly believe this is an HR issue. But in fact, it has to come from the top.

Being social with employees through in-person communication or via Twitter or Facebook will make them feel more comfortable and appreciated, which will improve their attitude toward work.

And communicating with customers via social media will have a similar effect.

The author learned this when he tweeted a question about buying shoes to mega-brand _Zappos_. Zappos didn't respond, but Alex Stoyle, an intern at _Topo Athletics_, a new footwear brand, contacted Ted directly, suggesting he check out the company's products. They secured the deal, and the author then shared this experience on Twitter.

When a friend saw this Tweet, she also engaged with Topo Athletics. Then later, writing for a newspaper about effective social media use, she mentioned that she was training for a marathon. Again, Topo got in touch with her, tweeting that they'd be proud to see her wear their running shoes at the marathon. Then they quickly sent her a sample. Her response? "I can't imagine buying any other running shoes, ever."

As you can see, by cleverly leveraging social media, Topo Athletics earned amazing word-of-mouth buzz and built valuable customer loyalty.

> _"A single person, especially in a top-level position, can change an entire culture."_

### 5. Smaller companies have the agility that’s necessary to adapt to a fast-moving marketplace. 

We've taken a look at how companies operate differently in the social media age. But there's one important aspect we haven't talked about: It's important for modern organizations to think small-scale in order to respond more quickly to specific demands.

That's partly because in general, smaller companies are more nimble and can innovate more quickly.

Take the case of _Growth Hacking TV_, which is a three-man tech start-up that focuses on low-cost alternatives to traditional marketing. The small company released a webseries within seven weeks of launching. Even more impressively, their monthly break-even point was only _$1,000_.

Compare that to the traditional model used by television networks and media sites: These big behemoths start with a pilot, build a team around each project and end up spending lots and lots of money.

However, this traditional approach does include one effective aspect that modern companies should adopt: _Nano corps_ — teams that come together for one project and then disband — can focus purely on the task at hand in order to achieve the best final product.

If you watch the closing credits of any movie, you'll realize that Hollywood relies on this exact approach. Movie studios set up small teams — lighting, makeup, etc. — each with a specific responsibility, who all come together for one project. Then, after the movie wraps, individuals and teams move on to the next gig.

Since there are no huge hierarchies that need to be managed, this system saves bureaucratic effort. It's also better for the workers, since it allows each team member the opportunity to approach every project with renewed motivation and fresh passion.

### 6. Companies that adopt flat hierarchies – ultimately eliminating managers – are handsomely rewarded. 

Imagine a company where employees are allowed to make big decisions on their own, without managers? This dream world is increasingly becoming a reality, as companies move to adopt _flat hierarchies_.

Why are flat hierarchies so appealing? Well, when employees are able to make their own decisions without asking authority figures for permission, they take more responsibility for their work, feel more engaged and are more likely to identify with the company.

Consider the multi-billion-dollar gaming firm, _Valve_. Each one of the company's 400 employees regularly makes decisions in real time, without consulting a manager first. In fact, there are no managers to be found. Thus employees are wholly accountable for their own actions — so if they make a mistake, for example, it's up to them to spot the problem and fix it.

Although Valve's employees understand that their work is valued, they also understand that it needs to benefit the company. This system intertwines the company's interest with the employee's self-interest: because Valve's employees identify with the company, they also care about maintaining its profitability and ensuring its success.

Still, if you're used to a traditional corporate structure, Valve's flat hierarchy might seem like an unattainable utopia. How does a company without management avoid descending into chaos?

Well, although there aren't formal bosses, of course there are still people that coordinate work on any given project — but these people aren't leaders, in the conventional sense.

Rather, they are co-workers who are aware of all aspects of a project. Think of these individuals as a communicative home base, rather than an authoritarian boss demanding obedience.

Still not convinced that flat hierarchies work? Well, consider the fact that at Valve, each employee generates around $1 million in annual revenue, which makes the company more profitable per employee than Microsoft, Apple or Google.

### 7. Together, ordinary people can come together to form an extraordinary network of knowledge. 

One of the most powerful benefits of social media networks is that they allow us to tap other people's knowledge and share our own.

Think about it this way: Although most us are probably relatively good at something specific, we lack expertise in many other areas. But social media platforms allow us to combine our own knowledge with the expertise of all the individuals who make up our network. This means a group of mostly ordinary people can come together to form an extraordinary network of knowledge.

Of course, there will always be famous experts in any given field. These individuals might have enviable pedigrees, but in the social age, what matters most is whether their knowledge is available to others.

So how can we share our expertise? People can pool their knowledge freely on specific platforms like Wikipedia. And they can also connect with friends, colleagues and even strangers via social media networks like Facebook and Twitter.

Using these extraordinary open networks to crowdsource may be the best and cheapest way to find solutions for the problems your business faces.

_InnoCentive_ is powered by this idea, creating tailor-made contests that invite entrants to come up with innovative ideas for their clients.

One InnoCentive competition challenged participants to imagine the future of laundry. InnoCentive asked people to come up with a process to wash clothing without creating wrinkles, destroying the material or using liquid. As an incentive, it offered a $40,000 prize for the winner.

In the end, over 600 submissions came in. The client could save a huge amount on research and development costs, and just pay out a fee to InnoCentive plus the prize money.

### 8. Social media isn’t just for tech start-ups – it’s crucial for the success of any company. 

Not yet convinced by the power of social media? Do you still think that these platforms aren't relevant to traditional business? Well, you're wrong. Even banks can benefit from social media.

Think about it this way: There are over two billion people — all potential customers — engaging on social networks. And it's easier to meet potential customers where they are already hanging out than to get them to come to you. So ultimately, being a social leader is a competitive advantage.

Peter Aceto, CEO of the bank _Tangerine_, was one of the first executives to fully embrace this idea. When a disgruntled customer tweeted at him, complaining that the bank had mishandled his paperwork and hurt his business, Aceto immediately sought the staffer responsible and ended up fixing the problem in 90 minutes.

At other banks, this simply wouldn't have happened, since the customer's only avenue for recourse would have been a complaint form or customer service hotline. But Tangerine's willingness to engage with its customers on social media allowed it to build customer loyalty and position itself to compete with larger banks.

But being a social leader doesn't just offer you a competitive advantage — using social networks within your company is also hugely beneficial.

No one expects a CEO to know every little detail about what goes on, but by engaging with employees regularly, a leader can gain valuable insight and improve the way his or her company operates.

For example, Tangerine has an internal collaboration tool that allows employees to communicate and voice issues. The platform includes a safe space for employees to share gripes, a blog called _The Right to B*@#h_. Often these complaints point toward things that can be improved.

This was what happened when one employee complained that the company still used paper for internal communication. Aceto noted this problem and quickly implemented a new paperless policy.

> _"Today, customers don't choose a product; they choose a responsive brand that not only listens, but provides a personalized exceptional customer experience."_

### 9. To preserve your reputation as a social leader, periodically conduct social media audits. 

Since mistakes are as common on social media as they are in non-digital communication, you should periodically conduct check-ups using these best-practice guidelines to ensure you stay on top.

First of all, make sure your company is presented properly and that all your profiles across social networks use the same logo, fonts, schemes and branding messages. By being consistent in your presentation, you're making it easy for members of your community to recognize you instantly, on any platform.

It's also important to stick with your niche. On the internet, it can be tempting to try to be everything to everyone — but don't fall into this trap. In order to build a brand that attracts jobseekers and customers, you need to have a clearly defined expertise.

And when you run into challenges online, try to look at them as opportunities. When you receive customer complaints, don't ignore them or get angry. Instead, use the incident as an opportunity to engage with the problem and publically resolve it.

This last point is a major component of an effective social media presence: Make sure your company is actively engaging with customers online, and always look for ways to improve.

It's important to be quick to respond on social media, especially since it's a real-time platform and a slow response is seen as a relic of the pre-internet era. So in order to demonstrate real engagement, try to comment or retweet as quickly as possible.

You should also try to monitor key words, hashtags and branded terms. Here's a pro-tip: Get a monitoring tool so that you can prevent the hijacking of your company's hashtag, or step early in if any trouble starts to brew. Monitoring will also allow you to track what your competitors are up to.

And a final piece of advice: Make sure you never, ever automessage — that is, send an automatic, computer-generated response. Automessages are easy to recognize and they make it look as though you aren't serious about your clients.

### 10. Final summary 

The key message:

**Social media has shifted the power balance between customers and companies, because a single viral video could potentially irreversibly damage a company's reputation. But social media also offers tremendous opportunities for business. And in fact, embracing these technologies is the first step to building a great company.**

Actionable advice:

**Instead of using traditional HR channels to find new employees, tap into your social network.**

Post a socially optimized job description across your various media accounts, and then encourage your followers to spread the message by sharing it with their network. This approach will help your company save a lot of time (and boring paperwork), and will help you find a person who is social, engaged and part of a larger network.

**Suggested** **further** **reading:** ** _Social Media is Bullshit_** **by B.J. Mendelson**

_Social Media Is Bullshit_ puts a damper on the hype around social media by unveiling the economy behind it and addressing the commonly held misconceptions about the value of social networks for business. While social media can be a valuable asset for certain companies, it is _not_ the cure-all that marketers and social media gurus would have you believe!
---

### Ted Coiné and Mark Babbit

Ted Coiné is a blogger and author who writes about leadership and change. He specializes in social media, and has just been named a _Forbes_ Top Ten Social Media Power Influencer.

Mark Babbitt is founder of _YouTern_, a career-building platform for recent graduates and young people. _Mashable_ named it one of the top five web communities for people starting out in the workplace.

