---
id: 59a95ad8b238e10005b9a22a
slug: a-world-in-disarray-en
published_date: 2017-09-04T00:00:00.000+00:00
author: Richard Haass
title: A World in Disarray
subtitle: American Foreign Policy and the Crisis of the Old Order
main_color: E62E37
text_color: 991F25
---

# A World in Disarray

_American Foreign Policy and the Crisis of the Old Order_

**Richard Haass**

_A World in Disarray_ (2017) is an overview of the major transformations in global politics since World War Two. These blinks describe an evolution from a non-interventionist order of nation states to one of globalization and international involvement.

---
### 1. What’s in it for me? A look at what created our current world order. 

Since the end of the Cold War, international relations have changed drastically. From a grounding in non-interventionist foreign policies, the world is moving toward a more interventionist world order with a growing number of powerful players.

These blinks examine a geopolitical system which is increasingly defined by global disorder, from atrocities that are all too often ignored, to terrorist threats that are used to justify illegitimate wars. You will learn about the foreign policies that have shaped our new world order. You will discover just what the United States, as the most dominant country, needs to do to create a more stable planet.

You will also discover

  * that the Cold War era was, in fact, a rather peaceful time;

  * how it took a genocide to make international intervention a protective principle; and

  * why the Syrian War is an example of passivity leading to disaster.

### 2. Our relative post-war stability resulted from balanced power and the threat of nuclear war. 

If you look back at history, it appears that World War Two was followed by a long, relatively peaceful period. But such apparent calm wasn't the result of a sudden pacifist turn among world leaders. More accurately, during the Cold War era, which lasted from 1947 to 1991, a global balance of power prevented active conflict.

For instance, the North Atlantic Treaty allied North American countries with a set of European nations, all of which decided to cooperate militarily through NATO. This agreement meant that an attack on any one of these nations was an attack on them all, a fact that deterred military action from outside the coalition.

Not just that, but following the war, the United States implemented the Marshall Plan. This package of economic support for the countries of Western Europe, particularly France, Germany and the United Kingdom, was intended to insulate these countries from the growing influence of the communist Soviet Union.

The power balance achieved through these military and financial agreements acted as a major bulwark against all-out war. When the Soviet Union blocked road and rail access into West Berlin in 1948 due to disagreements about how the divided city should be run, no combat ensued despite the dramatic stand off. Instead, Western countries launched a series of supply drops into the city, with hundreds of planes flying over East Germany and into West Berlin daily.

However, the greatest safeguard against armed conflict was the existence of nuclear weapons, which significantly reduced the military ambitions of the two major power blocs, the United States and the USSR. After all, both countries recognized that a nuclear confrontation would be unconscionably destructive, and were therefore highly motivated to avoid any direct altercation.

But this balance of power and the threat of nuclear destruction weren't the only factors that contributed to stability. In the next blink, we'll take a look at some other key elements that maintained global peace during this period.

> _"If nuclear weapons had never been developed, one could make a plausible case that the Cold War would not have stayed cold."_

### 3. Economic and diplomatic agreements were major contributors to a stable post-war world order. 

Do you ever feel like financial interests rule the world? Well, you wouldn't be so far off. In fact, there was a shift toward prioritizing finance after World War Two. This shift is in part a result of the way the economy was propped up after the war, which also contributed to global stability.

Here's what happened.

The _Bretton Woods_ system was inaugurated in 1944 as the first uniform financial system — one that would reunite enormous portions of the Western world. Under Bretton Woods, the dollar was set as the world currency, and all other currencies were valued relative to it. The plan also backed all paper money with gold.

Around the same time, the _International Monetary Fund_ was formed. This new body enabled financially troubled countries to take out temporary loans to cover their insolvency.

And finally, the _General Agreement on Tariffs and Trade_, or _GATT_, cut costs on exporting and importing goods across national borders, thereby encouraging a global economy.

Such systems of financial support more generally reinforced a global world order, which in turn reduced the risk of armed conflict.

From there, diplomacy played a major role in stabilizing this newly formed Western world order. Most famously, it was during this time that the diplomatic institution known as the _United Nations_, or _UN,_ emerged. It created a space for world leaders to negotiate their interests and resolve disputes without resorting to war.

The Security Council of the UN was tasked with monitoring the world order and was even authorized to intervene militarily if global or regional peace was at risk. Since the major emerging world powers — Russia, China and the United States — could veto the Security Council, there was also an assurance that the UN would not be inappropriately used to attack a single country.

This diplomatic system, the world order it protected and the economic aid that supported such a shift were yet other means of avoiding armed conflict. Together, they succeeded in maintaining peace throughout the Cold War. But what came next?

> _"Overall, the post-World War II order was predicated on familiar, traditional approaches, as state sovereignty was for the most part at its core."_

### 4. The United States has maintained stability with China by not interfering with its domestic affairs. 

During the spring of 1989, Chinese students descended on Tiananmen square in Beijing. They were assembling to pay respects to Hu Yaobang, a communist who was killed after attempting to reform the government.

The state responded by ordering the military to clear the square, resulting in thousands of people being injured or killed. At the time, the United States was tasked with making a decision about how it would react to such a violation of human rights.

In the end, the country chose a non-interventionist approach. In other words, President George H.W. Bush did not take punitive measures in response.

How come?

Well, for starters, America had major political and economic incentives to maintain good relations with the powerful nation of China. To put it more bluntly, the United States was unwilling to give up what they stood to gain from China because of the country's violent domestic policies. But beyond that, sanctioning China and further isolating the country from the world order would likely have produced detrimental consequences that would only lead to more repression.

The situation was similar in Taiwan, a country toward which the United States also adopted a non-interventionist policy. The island nation, also known as the Republic of China, had been a contentious topic between the United States and China ever since World War Two. During this period, a nationalist government ruled China and, during the war, fought alongside America against Japan.

But that changed in 1949, when the communists led by Mao Tse-Tung took power, forcing the nationalists to flee to the island of Formosa, which would later become Taiwan. To the present day, communist China will not acknowledge the independence of Taiwan, and Taiwan will not recognize China as having dominion over it.

Over the years, the United States has worked tirelessly to balance this situation and, thus far, armed conflict hasn't been necessary. It's another good example of how outright war has largely been avoided since World War Two, but, as you'll soon learn, interference in domestic policies is a whole different story.

### 5. Rwandan atrocities led to laws governing international intervention, but they have proven difficult to implement. 

In 1994, a long-drawn-out conflict in Rwanda between the Hutu majority and Tutsi minority reached a breaking point. Within months, nearly one million Tutsi were murdered as the international community stood by, failing to take action. Although the lives of hundreds of thousands of people could have been saved with minimal military risk, nothing was done.

This cataclysmic event marked a major shift in international relations. More specifically, it precipitated a shift toward intervention in the domestic affairs of other countries. The Rwandan genocide would also eventually result in a large scale change in the international military order.

For instance, in 2005, the UN adopted a principle known as the _responsibility to protect_. According to this doctrine, sovereign nations are tasked with ensuring the safety of their citizens from war crimes and genocide. When a state fails to fulfill this responsibility or commits such atrocities itself, the international community must defend the victims — through military intervention if necessary.

The passage of this resolution was revolutionary. For the first time in history, a law was signed which allowed countries to be invaded, even if they hadn't attacked another sovereign nation.

However, applying the new approach has been anything but straightforward as the Syrian war amply demonstrates. This conflict began during the Arab Spring of 2011, at a time when Syria was ruled by an authoritarian family that belonged to the ethnic minority of the Muslim Alawites. The majority of the country's population are Sunni Muslims, many of whom rebelled against the minority government.

In response, the government became violent, and pretty soon a full blown civil war broke out. Hundreds of thousands of Syrians perished in the chaos that ensued, while millions were forced to emigrate.

It was a clear-cut case of a state failing to protect its citizens, but despite the obvious nature of the situation, the international community didn't act on its responsibility to protect. Diplomatic disagreements about who was responsible simply couldn't be resolved.

So, the responsibility to protect can be tricky to apply, but there's one case in particular in which the United States _clearly_ violated the rules. You'll learn all about it in the next blink.

### 6. The illegal invasion of Iraq dramatically shook world opinion of the United States. 

America plummeted in popularity in 2003 when it decided to invade Iraq. At the time, people across the globe responded with outrage, speaking out against what they saw as a violation of humanitarian law.

But despite the vocal public disapproval, it wasn't until things calmed down that the inappropriate nature of the invasion was truly apparent. Once the dust _did_ settle, it was clear that the US invasion was illegitimate, despite the doctrine of responsibility to protect.

Here's the story.

During the Gulf War in 1991, President George H.W. Bush's government sent troops into Iraq. This initial invasion was in response to Iraq's attack on Kuwait, a sovereign nation. Because of this, the deployment could technically be justified.

However, when US troops again landed on Iraqi soil in 2003, based on the notion that Iraq was amassing an arsenal of nuclear weapons, there was not only no definitive evidence, but this supposed buildup of armaments was also no stand-in for a real armed attack on another nation.

So, while an attack could have been justified under the responsibility to protect if Saddam Hussein had failed to protect Iraqi citizens from genocide, nothing of the sort occurred. Iraq's dictatorship was definitely acting in violation of domestic human rights, but they weren't coming close to committing something like ethnic cleansing or genocide. As a result, the US invasion was illegitimate.

Or, to put it differently, the invasion of Iraq was preventive, rather than preemptive.

What's the difference?

Well, a preemptive invasion can sometimes be justified. However, for this to be the case in Iraq, the country would have had to be on the verge of attacking the United States, which it clearly was not.

The preventive attack that did occur was spurred by America's knowledge of a potential threat, a hypothetical menace that the United States foolishly decided to extinguish. After all, in a world order with numerous emerging powers, preventative warfare can easily spark chaos.

### 7. Action must follow threats, and military activity should never be abandoned casually. 

Anyone who reads the news knows that the war in Syria has been devastating. The conflict has been so horrific that it has even raised fundamental questions about how the international community should respond to such events.

One major topic of discussion in this broader debate is the difference between words and actions. After all, the UN is tasked with upholding the responsibility to protect, which means not only speaking out against atrocities but acting to stop them.

The Syrian War is a prime example of how failing to do so can swiftly lead to disaster. Just take Obama's reaction when the rebellion began to grow. He simply called on President Bashar al-Assad to step down. The conflict escalated and, in 2012, rumors began to spread that the Syrian government was using chemical weapons against rebels.

Obama issued a public statement that using such weapons would force America to reconsider its military involvement in Syria. However, when Assad used gas in 2013 to kill 1,500 people outside Damascus, the US government didn't intervene.

While the United States convinced Syria to destroy their chemical weapons to avoid invasion, Obama should have instead stood by his promise of retaliation if such weapons were used.

So, it's important to follow through on threats that are made, but it's also key that, once military action is taken, it's not dropped lightly. When President George W. Bush — son of George H.W. Bush — invaded Iraq in 2003, the situation in the country fell apart over the course of the years that followed. So, in 2007, Bush increased the presence of American troops, offering military support to certain Sunni tribes.

This course of action was proving successful, but when President Obama pulled troops out faster than originally agreed, the area soon destabilized. Not long after, militant Islamic extremism returned, this time as Islamic State, or ISIS. It just goes to show how dangerous it can be to prematurely abandon a military responsibility once it's undertaken.

### 8. The new world order depends on cooperation between the three major powers. 

These days, it's easy to get worried about looming conflicts, such as Russia expanding into Ukraine or China claiming territory in the South Seas. However, while these countries are certainly broadening their influence, there's nothing to suggest that they intend to expand their territory very far beyond their borders.

Since the West can count on this fact, it should make every effort to work with these nations for our mutual benefit. In fact, forging stronger diplomatic bonds between China, Russia and America would do wonders for stabilizing the world order.

Take the Cold War as an example. During this period, cooperation usually came with conditions. The United States would agree to work with another nation economically, only if it agreed to cooperate on a different level, say, militarily. In so doing, each country endeavored to derive the maximum benefit for itself.

But today, a new cooperation with China and Russia could come with far fewer strings attached. The countries could agree to cooperate whenever possible, avoiding disagreements that can't be solved. In this new climate, it's essential that the United States restrains itself when presented with opportunities to intervene in domestic issues that affect the other two nations.

More specifically, economic cooperation between the three nations should be ensured. China and Russia must be allowed to expand their network of bilateral agreements, a critical tool for maintaining strong economies.

The United States should not attempt to undermine the economies of the other two nations. That's because the best chance for global stability and peace is a scenario in which all the major powers are prosperous and content.

By sticking to this strategy, the West will be able to respond to the new world order, one dominated by interventionist politics and the three new major powers.

### 9. Final summary 

The key message in this book:

**World War Two marked a major shift in the world order and set off the Cold War era. This period was largely peaceful due to a variety of factors, but the global scene is once again changing. New tactics are required to maintain peace in this volatile terrain, and the West must adapt.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Why Nations Fail_** **by Daron Acemoglu & James A. Robinson**

_Why Nations Fail_ revolves around the question as to why even today some nations are trapped in a cycle of poverty while others prosper, or at least while others appear to be on their way to prosperity. The book focuses largely on political and economic institutions, as the authors believe these are key to long-term prosperity.
---

### Richard Haass

Richard Haas is a diplomat, and president of the Council on Foreign Relations. He has received the Tipperary International Peace Award for his work in the field. Among others, he has advised Secretary of State Colin Powell and President George H. W. Bush.

