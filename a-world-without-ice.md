---
id: 56cb410b9bb2160007000007
slug: a-world-without-ice-en
published_date: 2016-02-21T00:00:00.000+00:00
author: Henry Pollack
title: A World Without Ice
subtitle: None
main_color: 2FB9EA
text_color: 15556B
---

# A World Without Ice

_None_

**Henry Pollack**

_A World Without Ice_ (2009) is about our planet, its climate, its human residents — and ice. Ice has always been a major player in Earth's climate. These blinks explain why we may soon see a world without ice, why that would a have dramatic consequences for Earth and humans alike, and how we can cope with climate change.

---
### 1. What’s in it for me? Understand what it would mean to live in a world without ice. 

Does ice mean a lot to you? Unless you live near one of the poles, your encounters with ice might be limited to the cubes you put in drinks and the local ice-skating rink. 

However, there are some good reasons to take a closer look at ice and its place in the world. Why? Because polar and glacial ice have a decisive effect on Earth's climate. And, scarily enough, due to the emission of huge amounts of greenhouse gases, Earth is warming and ice is melting at a rapid pace.

These blinks explain the role ice plays in Earth's climate as well as how human activities became the main driver of climate change. You'll find out about the severe and unavoidable consequences the disappearance of ice will wreak on our environment and the global economy. And, finally, you'll learn about ways in which climate change can be mitigated.

You'll also discover

  * where there's ice that's more than 10,000 feet deep;

  * what a great landscape artist ice is; and

  * why the next major wave of refugees will likely be created by climate conditions.

### 2. Not quite twins: the Arctic and the Antarctic are rather different. 

In 1768, young English naval officer James Cook was assigned the position of captain on the search for Terra Australis Incognita. This was rather daunting, given that some didn't believe that this southern continent existed at all. 

Ancient Greek philosophical writings show the first arguments for the existence of a landmass in the southern hemisphere matching those in the north, for reasons of symmetry. But it wasn't a mass of land that Cook first discovered. It was mass of _ice_. 

Given that Earth has masses of ice at the North and South Poles, the Greeks were right about the planet's symmetry. But even though the poles might seem rather similar, they actually have very little in common. 

The South Pole is located in the continent of Antarctica, around 850 miles inland from the nearest coastline. The North Pole, on the other hand, is located in the waters of the Arctic Ocean, around 450 miles away from the nearest landmass. Both poles are set in ice, but in varying ways. The South Pole lies below more than 10,000 feet of ice, while the North Pole floats on top of a relatively thin 10- to 20-foot sheet of frozen ocean water. 

The ice in both polar regions is constantly shifting, but at very different rates. The South Pole's ice masses move at a pace of 30 to 40 feet each year. At the North Pole, by contrast, the ice averages a speed of three to four miles each _day_. 

These differences aside, humans have found the North and South Poles equally fascinating throughout history. Explorers, adventurers, whalers, sealers, scientists and soldiers have all made their way to the poles. Today huge numbers of tourists also journey to the Arctic and Antarctic. Arctic tours offer excursions to glaciers and wildlife tours where people can observe reindeer, walrus and polar bears in their natural habitat. Penguins are one of the main attractions in the Antarctic, with some 45,000 tourists heading south each year to experience these marine birds firsthand. 

So should we be concerned for the safety of Arctic and Antarctic ecosystems given the rise of tourism at the poles? Actually, it only poses a marginal risk. Because what really damages the poles is what we do at home.

> _"Icebergs are to the polar imagination what cloud forms are to people elsewhere."_

### 3. There are two key players in our planet’s climate: ice and the greenhouse effect. 

Ever thought about what life without ice would be like? For starters, it'd mean no more ice hockey or drinks served "on the rocks." But ice also has significance on a global scale, which means its absence would have a bigger impact than you might expect. 

Ice has unique properties that make it a key player in our planet's climate. As we know, ice is just plain old frozen water, or solid H2O in chemical terms. When most fluids solidify, they usually contract. But not water. When water forms ice, it _expands_ in volume. Ice has a lower density than water as a result, which is what allows it to float without sinking! There you have the mechanics of the iceberg. 

Ice has another important property: reflectivity. If you've ever gotten sunburned after a day of skiing, you'll understand this. And ice is not only capable of reflecting sunlight onto our faces, but also back into space. 

This has a substantial influence on Earth's climate, as polar ice caps reflect a lot of warming sunlight away from polar regions. So, when Arctic sea ice melts, Earth becomes warmer because there's less ice to reflect solar energy away. 

But there's yet another factor crucial to keeping our climate in check: _the greenhouse effect._ Earth's atmosphere comprises of 99 percent nitrogen and oxygen. The remaining 1 percent of other gases such as carbon dioxide and methane play an extraordinarily vital role: preventing heat from escaping Earth's atmosphere. 

This creates a _natural greenhouse effect_, and Earth would be very different without it. Picture an Earth-sized snowball that's 60°F colder than Earth, without any sign of human life: that's pretty much how much the trouble we'd be in without that 1 percent. 

Of course, you can have too much of a good thing. Today, we're facing a new kind of greenhouse effect: the _anthropogenic greenhouse effect_, which is caused by the extensive burning of fossil fuels and the massive amounts of carbon dioxide emissions it causes. The anthropogenic greenhouse effect is what's throwing Earth's climate out of balance.

### 4. Our planet’s many ice ages over history have shaped our seas and landscapes. 

If you took a trip back in time to Europe or North America 20,000 years ago, there wouldn't be much to see. Why? Because back then everything was covered with a layer of ice _two miles thick_! That's right, around 120,000 years ago, Earth saw the beginnings of an ice age that would last 100,000 years. And that was our most recent ice age. 

In fact, ice ages are a rather common event in Earth's history. As many as twenty ice ages have taken place over the past 3 million years. But hold on: how do we know this? 

The characteristic signatures of past ice ages can be found in the chemistry of sea shells that form layers on the ocean floor. During an ice age, the volume of ocean water decreases, as it's all been turned into snow on continental ice sheets and doesn't run back out to sea. As a result, the chemicals in the remaining ocean water become more concentrated. These chemicals are used by smaller marine creatures to grow their shells. So, the chemical composition of such shells give us some idea of water levels and ice age conditions. 

The landscape also gives us clues about ice ages throughout history. Ice has shaped a significant amount of our planet's landscape. As ice expands over land, it's powerful enough to excavate, shift, break and even crush rocks. This is what produced landscapes strewn with rocks and boulders of all sizes, which perplexed early settlers in northern Europe and North America. 

As well as breaking down rocks, ice can also sculpt mountains and carve valleys. The Norwegian Fjords are an impressive testament to this. The sheer number of lakes in Scandinavia also signals earlier presence of ice, as melting ice filled lowlands with water.

### 5. Several scientific methods allow us to reconstruct past climate changes and confirm the current global warming situation. 

Some 20,000 years ago, the most recent ice age reached its end. The ice began to melt, Earth began to warm again and, over the next 10,000 to 12,000 years, it reached an average surface temperature a little hotter than today's. But how can we be sure that's what really happened?

Well, there are several methods we can use to reconstruct historic climate patterns. The first entails investigating _tree rings._ Every year, a tree grows a little and adds a new ring of materials around its trunk. In years when conditions are great for growth (not too hot or cold, not too wet or dry), these rings are thicker. Conversely, these rings are thinner in times of drought.

We can also study the layers of ice in polar ice sheets to find out about our climate history. Yearly accumulations of snow are compressed into distinct layers of ice. A thick annual layer indicates greater snowfall.

Using these two techniques, scientists have deduced that, after reaching a temperature high, Earth had cooled about 2°F by the time it reached the twentieth century.

It's great that we can analyze the climate of the past, but what about the climate of our future? It's thanks to the thermometer that we know where things are going. The first thermometers appeared in the early 1600s, though it wasn't until 1850 that people all around the world began tracking daily temperatures.

The temperature data collected since then points in one direction: our Earth is warming, and fast, the average temperature being about 1.8°F higher than 150 years ago. In light of this rapid warming, the UN created the International Panel on Climate Change (IPCC) in 1988. The IPCC is an international scientific working group that issues reports summarizing and evaluating research into climate change. In the 2007 report, the IPCC declared global warming "unequivocal."

### 6. Human activities have driven climate change for centuries. 

Our planet's climate is subject to natural fluctuations. In the past these occurred so gradually that one century tended to look a lot like the next. The same can no longer be said today, as it is widely acknowledged that rapid global warming is the result of human activity. 

This is no surprise when we consider how drastically humans have altered the land they live in. Forests have been wiped out to make room for agricultural land and housing is built with the resulting timber. The logging and burning of forests releases the CO2 contained in living trees into the atmosphere. 

And, as we know, this contributes to the anthropogenic greenhouse effect. Yet deforestation continues in many parts of the world, such as Brazil and Indonesia. And, even worse: the massive growth of the human population to about 7 billion people intensifies the need for cleared forest lands.

But the biggest driver of climate change today is human industrial activity. Since the eighteenth century, the ever-increasing extraction and burning of coal, petroleum and natural gases has had a massive impact on global climate. 

The burning of fossil fuels produced such great quantities of carbon dioxide that it caused the concentration of CO2 in the atmosphere to increase by 22 percent between 1958 and 2009. Like deforestation, fossil fuel use is a known contributor to global warming. And yet, we continue to rely on these activities to sustain human productivity. Why?

> _"Nature, long the conductor of the climate orchestra, has been replaced by the human population."_

### 7. Climate change has concrete impacts for human life today. 

Many of us tend to think that climate change is an abstract phenomenon, something that will happen far off in the future. Yet the opposite is true: climate change affects today's ecology and economy in a big way. 

Take glacial ice, for a start. Municipal systems and agriculture in the foothills and plains surrounding high mountains rely on melting glacial ice as their water source. This is the water that millions of inhabitants use to drink, flush away sewage and water crops. 

For agricultural activities, meltwater arrives right on time for the spring planting and summer growing seasons. In the winter, rainfall freezes to create a source of water in the warmer seasons. But in a warmer world, mountaintops will have less and less snow, which means less meltwater when it's needed most. This could lead to water shortages, which are already a real threat for many nations, as well as a potential cause for international conflict and war. 

However, rising sea levels will be the most dramatic consequence of a world losing its ice. As temperatures rise, so do ocean temperatures. And since water expands at higher temperatures, this, in turn, causes the sea level to rise. Finally, as glacial meltwater returns to the ocean, it further exacerbates the rising sea level. But what does this mean for us?

Well, about 100,000,000 people on Earth live on terrain no more than three feet above sea level. This means that even a modest rise in sea level will create 100,000,000 climate refugees in the near future.

### 8. Climate change is unavoidable, but we can learn to manage it. 

So the big question is: Can we do anything to prevent global warming, melting polar ice and flooded coastlines? Though many attempt to stay positive, the short answer is _no_. 

Climate change is unavoidable. For one thing, the sheer amount of greenhouse gases that have already been pumped into the atmosphere will have lasting effects far into the future. Carbon dioxide, the main greenhouse gas, lingers in the atmosphere for more than 100 years. Even if no CO2 were to be emitted into the atmosphere starting today, Earth would still warm by about 1°F — enough to turn the American agricultural heartland around Nebraska into desert, for example.

On top of that, a great part of the global industrial economy is based on the consumption of fossil fuels. A sudden stop in the supply of oil, coal and natural gas would cause the global economy to collapse. So, if we're unable to prevent climate change, what can we do?

Well, we can _mitigate_ it. The focus of climate mitigation is slowing and then reversing the overloading of our atmosphere with greenhouse gases. First, this requires energy-efficiency measures in transportation, manufacturing, household appliances and buildings. 

For example, doubling fuel efficiency of cars in America is already possible using existing hybrid technology. A tripling could be achieved by reducing the weight of vehicles through the use of lightweight materials. In addition, there are a variety of energy sources that do not produce greenhouse gases. Solar energy could be collected on an industrial scale to produce electrical energy. 

In places with strong winds there is great potential for generating abundant electricity through wind turbines. Finally, just ten feet underground, the temperature is higher in winter and cooler in summer than at the surface, making geothermal home heating and cooling systems possible.

These are our opportunities for climate mitigation. Though they can't provide us with a surefire solution, they will provide us with a realistic option for continuing to sustain ourselves and our planet.

> _"The world needs to chart a bold course into a new sea of sustainability."_

### 9. Final summary 

The key message in this book:

**Ice plays an instrumental role in our planet's climate, which is why global warming poses such a huge threat to our future. A range of scientific methods allows us to pinpoint the evidence not just of historic climate fluctuations, but also of the rapid rate at which our man-made climate change is advancing. While the consequences will be severe, there are opportunities for us to learn to manage climate change.**

**Suggested** **further** **reading: _This Changes Everything_ by Naomi Klein**

**_This Changes Everythin_ g addresses one of the most pressing issues today: climate change. The book outlines exactly how we're harming the planet and why we've thus far failed to stem our destructive behavior. Author and activist Naomi Klein also points out how some early movements are meaningfully fighting climate change and what more needs to be done to prevent global disaster.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Henry Pollack

Henry Pollack is a professor of geophysics at the University of Michigan. Together with his colleagues on the International Panel on Climate Change and former vice president Al Gore, he won the 2007 Nobel Peace Prize. Pollack travels regularly to Antarctica and has conducted scientific research on all seven continents. He also the author of _Uncertain Science … Uncertain World_.

