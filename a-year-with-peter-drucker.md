---
id: 54883ed93835630009050000
slug: a-year-with-peter-drucker-en
published_date: 2014-12-11T00:00:00.000+00:00
author: Joseph A. Maciariello
title: A Year with Peter Drucker
subtitle: 52 Weeks of Coaching for Leadership Effectiveness
main_color: AE2326
text_color: AE2326
---

# A Year with Peter Drucker

_52 Weeks of Coaching for Leadership Effectiveness_

**Joseph A. Maciariello**

_A Year with Peter Drucker_ (2014) offers a treasure trove of the management guru's most essential insights, known as "Druckerisms." Over decades, Peter Drucker (who died in 2005) developed a groundbreaking philosophy of leadership and success, teachings blissfully free of the jargon that dominates management studies today.

---
### 1. What’s in it for me? Learn the secrets of management according to guru Peter Drucker. 

Peter Drucker's ideas are everywhere. For business managers, saying Drucker is important is as obvious as saying humans need oxygen to breathe.

Yet too often, Drucker's clear, insightful theories on modern business management have been muddied by the jargon-filled gobbledygook of later studies and management books.

A close associate of Drucker, author Joseph Maciariello, has changed all this. _A Year With Drucker_ shines a clarifying light on Drucker's core ideas on what makes a good business manager.

Drucker's philosophy explains that managers aren't just faceless robots, locked up in dreary corporate office parks. Instead, they are like superheroes, capable of incredible feats of intelligence — even able to save the world!

After reading these blinks, you'll know:

  * why "Druckerisms" are such a big deal in the business world;

  * how business managers by donating their time could cure illiteracy; and

  * which two important concentration skills a manager needs to have.

### 2. To become a strong leader, use your early career as a launchpad for future professional success. 

So you've just earned your MBA, and the first few job offers are rolling in!

Although you're ready to take the world by storm, don't just rush in. Your early work decisions will play a huge part in shaping your future career.

Drucker suggests you take your time in finding the right job, ideally a position that will give you room to grow and develop as a leader.

It helps to seek out businesses that don't just create products, but also are known to promote the intellectual and moral growth of employees and future managers.

These tips are especially pertinent for _knowledge workers_, a concept which first appeared in Drucker's 1959 book, _Landmarks of Tomorrow_.

As with scientists, lawyers and business managers, a _knowledge worker_ 's primary skill is her intellectual expertise.

Since most knowledge workers won't stay with the same company for their entire career, it's especially important to start thinking early on about what the future will bring. More precisely, knowledge workers should prepare themselves for future leadership positions.

So, if you want to position yourself for a successful managerial career, you need to start by managing yourself. This means figuring out your strengths, weaknesses and unique skills.

To do this, Drucker recommends _feedback analysis_. Whenever you make a key decision, record what you expect will happen as a result. After a period of time (six months, 12 months and so on), compare the actual results with your preliminary expectations.

If you keep this up for a few years, a pattern should emerge. You'll get a better sense of your strengths and weaknesses, and also figure out how and where to direct your professional efforts.

> _"To start with the question, 'What should I contribute?' gives freedom, because it gives responsibility."_

### 3. Creating a base of knowledge workers in an economy can benefit society as a whole. 

Let's look at _knowledge workers_. Where do they come from? And how can they transform society?

Education is an important factor. After acquiring knowledge through education, knowledge workers become empowered and rise to positions of leadership in society.

For Drucker, the virtue of the knowledge worker was perfectly exemplified by E-Veritas, a Philippines-based electronic trading company. The firm trained uneducated people living in poverty in some of Manila's poorest districts to work as electronic traders.

Through this process, E-Veritas created human capital (in other words, knowledge workers) where it is often needed most — at the bottom of the social and economic hierarchy.

And thanks to _microfinance_ initiatives organized by E-Veritas, these workers were able later to launch their own small-scale business ventures. (Microfinancing refers to small, low- or no-interest loans.)

The takeaway? As a company, E-Veritas used education to empower impoverished communities.

It's important to highlight the fact that capital investment is most likely to be effective in these kinds of circumstances — that is, when capital is targeted at knowledge workers.

Drucker didn't realize this early in his career. While he was working as an adviser to the World Bank in the 1940s, Drucker endorsed the "standard model of development," which is all about giving money to needy populations in an unstructured way.

But Drucker later recanted, after he realized that capital investment fails in many cases.

For example, considering the amount of money that's been poured into Egypt over recent decades, the country's economic development should essentially be on par with Japan's. Yet the fact that Egypt's economy is still sorely underdeveloped all but invalidates the "standard model."

And yet, capital investment served a country such as South Korea surprisingly well. But in this case, investment was entirely geared to educate and train knowledge workers, through a policy which brought 200,000 Korean students to United States.

Many of these students returned home as knowledge workers, started businesses and helped make South Korea the innovative, economic powerhouse it is today.

### 4. Efficient managers keep their focus on long-term goals while addressing short-term needs. 

There are plenty of bad bosses. Yet we've all had at least one manager who has earned our respect.

What is it that "good" managers have in common?

First, let's take a step back and discuss what a manager does. Typically, managers are responsible for one broad, overarching goal — or as Drucker calls it, the _true whole_.

The true whole in sum is what a company or department is trying to create. It encompasses the productivity (time, skills and resources) that goes into creating a final product or service, that ideally generates revenue for the business.

The true whole is made up of many different parts: from employee training to financing to setting project deadlines. As the ultimate multitasker, a manager oversees all of these issues.

Ultimately, the manager works to make sure that the company's final product is more valuable than all its associated costs.

So an effective manager is one who is skilled at balancing short-term goals (like profitability) with longer-term strategic goals (dominating a market).

And that's precisely what makes managing so challenging. Managers constantly have to make tough choices that require trade-offs.

For example, management might have to cut back on employee training to accelerate a product launch, so the company can meet its quarterly sales goal.

And yet, this move could stunt the company's innovative potential, thus harming its long-term goal of being the leader in cellphone technology, for example.

From Drucker's perspective, the long-term survival of the company should always be the first priority. Thus managers should try to deal with urgent demands in a way that also keeps future goals in focus.

One way to do this is to create a mission statement that focuses on the company's future. Then managers can structure short-term objectives in a way that supports the broader vision.

> _"A manager must, so to speak, keep his nose to the grindstone while lifting his eyes to the hills — quite an acrobatic feat."_

### 5. Concentration and information literacy are the cornerstones of effective management. 

As we've seen, efficient managers skillfully balance short-term interests with long-term goals. But what specific skills are required to achieve this?

Drucker says there are two crucial managerial skills: concentration and information literacy.

Let's start with concentration. For managers, it has nothing to do with reading a book straight through in six hours; rather, concentration is about knowing where to focus your efforts for maximum results.

To that end, effective managerial concentration boils down to one key principle. You should always concentrate on tasks that require the least effort and generate the greatest amount of productivity.

There are two ways you can achieve this.

First, focus on your core competencies and strengths. Or as Drucker succinctly put it, "Don't major in the minors." If your employees have a strong innovative streak, urge them to focus on developing cutting-edge technologies, not on churning out low-end consumer goods.

And second, abandon projects or products before they become unproductive. You'll simply waste too many resources keeping a declining product afloat; you really _should_ be focusing your efforts on the next big thing.

The second crucial managerial skill is _information literacy_. This is the ability to understand what raw data is actually telling you about your business.

This skill is particularly important in our "big data" era. Today's companies work with gargantuan data sheets; businesses dedicate tons of resources to statistics-gathering. Yet data is utterly useless if you can't make heads or tails of it.

In the late 1990s, for example, many American financial institutions were blindsided by the economic collapse of many countries in mainland Asia. Yet other businesses had spotted patterns in economic data from the region in the years leading up to the "sudden" crash.

These companies saw a disaster looming and adjusted their investments, ultimately riding out the crisis in much better shape than others.

### 6. Good corporate managers can help government; they can put their skills to nonprofit use, too. 

Although many might question the combination of corporations and the social good, Drucker believed that managers were especially well-positioned to work for the benefit of society.

Why is that?

First, corporations have gargantuan budgets with which they can support social programs. And second, as a company brings together so many knowledge workers under one roof, it can also leverage its vast human capital to solve some of society's problems.

To that end, Drucker believed that managers should share their skills across society via _executive sabbaticals_ — that is, having managers take time off to work elsewhere.

In fact, it was an executive sabbatical that led a group of managers to help solve California's public debt crisis in 1967. At the time, California's budget deficit was so large that it led to a series of forced tax hikes.

Some 200 managers decided to go on sabbatical to help California deal with the crisis. This team identified key areas of government inefficiency, like an unnecessary $4 million development in the state capitol. Eventually, California was able to lower taxes, and even offer tax refunds!

Governments aren't the only non-corporate sector that's increasingly eager to attract such leaders. Managerial skills are also now highly sought-after in the nonprofit world.

Nonprofits face a particular business challenge, as they don't sell a product (as a business does) and aren't trying to create more effective regulations (as government does). Rather, nonprofits pursue something more abstract: societal change.

Since managers are trained to define clear goals and find ways to measure success, they can use these skills to appraise and legitimize a nonprofit's more abstract aims.

For example, a manager can clarify a nonprofit's objective, "helping children read," and turn it into, "a specified amount of students reaching their age-appropriate reading level."

### 7. Good leaders know how to keep their organization even-keeled through disruptive change. 

Effective managers perform the ultimate balancing act: finding just the right tempo between ensuring continuity and inspiring change.

Drucker stresses that both are crucial to a company's long-term success.

Change happens when companies grow. And when companies grow, restructuring often brings with it more bureaucracy. In addition to structural changes, growth inspires greater innovation — in essence, more change! New products and new markets necessitate new branding, and so on.

Although this is a challenging, volatile period for a company, it's also necessary for the continued health of any business. Without change, a company can stagnate, which can lead to eventual failure.

Consider the story of Henry Ford, a manager whose unwillingness to change nearly cost him his business. When Ford started his company in 1905, he had nothing. But in just 15 years, he was able to create one of the world's largest and most profitable businesses.

Yet by 1927, Ford's company was in financial disarray. So what happened?

Ford was deeply committed to an owner-entrepreneur style of business, a system with one big boss, many workers — and no management. He was so controlling that he would fire employees who tried to make managerial decisions or assert leadership.

But his approach stood in stark contrast to what his contemporaries were doing. At the time, other companies were using management strategy as a way of effectively handling growth.

Because Ford refused to cede any control, his company struggled for years. Ford only experienced a reversal in the company's fortunes in 1944, when his inexperienced grandson introduced a management team.

### 8. Identify promising future leaders early on, and start grooming them as your successors. 

If you're a leader at your company, it can be scary to think about moving on. How can you ensure that the venture continues to flourish without you?

You can start by finding the right successor. According to Drucker, your best choice is probably going to come from inside your company.

That's because the ultimate goal of any "succession decision" should be to keep the spirit of the institution alive. The new leader has to be someone who can continue to carry out the core practices that define the company.

For example, when he took over at Apple, Tim Cook's task was to maintain the company's legacy of innovation, as established by his predecessor, Steve Jobs. Cook rose to the challenge, and has even introduced the Apple Watch, a whole new product category.

It's worth noting that although choosing a successor is always a gamble, you're far less likely to disrupt the spirit of an organization when you select someone from within — someone who already lives and breathes the company culture and values.

Harvard University learned this lesson the hard way when it chose economist Lawrence Summers to head the institution. While his predecessors had all been selected internally, Summers, despite having taught at Harvard, was essentially an outsider. And after only six years, he was pushed out by a vote of no-confidence from Harvard's faculty.

As you can see, succession is tricky business, and that's why planning for it is essential.

One way to plan is to implement a systematic development program that identifies and grooms potential leaders. That way, when a position opens up, your company has a ready pool of qualified candidates.

And if you're serious about succession planning, it's also critical that you elevate your organization's human resources (HR) department to a position of power. Your HR team should include skilled, qualified executives capable of identifying and supporting promising leadership potential.

> _"A fish dies from the head down." — Old proverb_

### 9. Final summary 

The key message in this book:

**Since management is a human endeavor, it demands that its practitioners honestly reflect on their own strengths and weaknesses. And although management is commonly associated with the business world, it's also well-positioned to help influence society on a broader scale.**

Actionable advice:

**Create a lasting contribution by getting involved in projects that aren't solely profit-driven.**

Even if you've achieved success, it's not easy to create a lasting contribution. But if you do want to feel a sense of fulfillment at the end of your career, it's important that you start finding a way to get involved in projects that advance the common good. To this end, you could volunteer at an inspiring nonprofit or at your local church.

**Suggested further reading:** ** _The Effective Executive_** **by Peter F. Drucker**

In _The Effective Executive,_ author Peter Drucker offers a step-by-step guide to becoming a more productive and effective executive. By mastering a few procedures and principles, you can develop your own capacities as a leader and also support your employees' strengths, with the goal of improving results across your organizatio

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Joseph A. Maciariello

Joseph Maciariello holds a PhD in economics from New York University. He is the current Marie Rankin Clarke professor of Social Science and Management at the Peter F. Drucker and Masatoshi Ito Graduate School of Management. Both a colleague and close acquaintance of Peter Drucker, Maciarello has authored a number of books addressing Drucker's management insights.

