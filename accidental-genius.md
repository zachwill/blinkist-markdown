---
id: 59751a44b238e100057032af
slug: accidental-genius-en
published_date: 2017-07-24T00:00:00.000+00:00
author: Mark Levy
title: Accidental Genius
subtitle: Using Writing to Generate Your Best Ideas, Insight, and Content
main_color: EC5A41
text_color: BA4733
---

# Accidental Genius

_Using Writing to Generate Your Best Ideas, Insight, and Content_

**Mark Levy**

_Accidental Genius_ (2010) outlines techniques, ideas and exercises that utilize _freewriting._ It's a method that many people use to organize their thoughts, solve problems and access the great ideas buried in their minds. The techniques and tips detailed here can be used to achieve better concentration, bring order to disorder and free up creative capacity.

---
### 1. What’s in it for me? Free your thoughts and develop great ideas through freewriting. 

Imagine you're confronted with the task of developing an innovative solution, coming up with a new idea, or writing a creative short story — immediately! How would you go about this task? You might struggle to make a start.

In our working and family lives, we are constantly asked to tackle difficult problems, find ideas and develop good solutions. This is where the technique of _freewriting_ comes into play.

These blinks will introduce you to freewriting. Freewriting allows you to tackle problems, to think through ideas, to free and organize your mind, and to get creative. You will learn the golden rules of freewriting and how to get your best ideas on paper.

In these blinks, you'll learn

  * why it's better to give only 90 percent of your effort, not 110 percent;

  * why lying is (sometimes) a good idea; and

  * why you might consider publishing a book — after you've mastered your freewriting skills.

### 2. Freewriting is an excellent method for organizing your mind and capturing your best ideas. 

You no doubt have tons of great ideas swirling around your head; you're not alone if you struggle to articulate them.

The human mind is capable of producing fantastic thoughts and hypotheses, but sometimes directing or clarifying them can be tricky.

We all have eureka moments that come to us like lightning bolts — some are nonsensical, others can change the world. Remember the legend of the apple falling on Isaac Newton's head? At that moment, he suddenly understood how gravity worked.

There's no doubt that the human mind can conjure up incredible leaps of imagination.

However, sometimes we struggle to organize and refine our thoughts into understandable forms.

That's because we're predisposed to laziness or leaving our ruminations indistinct and wooly. We start daydreaming, lose focus, and just like that the best ideas are lost.

There's a method for combating this: _freewriting_. Freewriting is a way to arrange our minds, come up with ideas, or make decisions by putting our thoughts down rapidly on paper.

It's more than just directionless and unfocused scribbling. You have to apply rules and techniques in order to reap huge rewards from your initial thinking.

Freewriting is a method that honors everything within your mind. It keeps track of it and permanently chronicles your opinions as they develop.

It's especially useful if you have to make difficult business decisions. But it's also excellent in situations where you have to think about your personal life or get to grips with big challenges like writing a book or a thesis.

But don't be fooled by the name. Freewriting is relevant not just for authors, business consultants or wordy professionals — it's for everyone.

Now that we know the theory, how does freewriting actually work? Let's get started by looking at some basic rules.

### 3. To start freewriting, relax, write quickly and give yourself a time limit. 

Freewriting, despite the name, isn't just "free writing." There are rules to follow, so don't rush headlong into the exercise. It's important to grasp the three basics.

First, temper your expectations. This will get you out of the starting blocks with ease. It's called a _try easy_ attitude.

Let's look at a real-world example. The attitude was typified by business consultant and mental coach Robert Kriegel. He managed to get athletes to set world-beating records while competing for a spot in the Olympics. How? He told them to relax and give only 90 percent instead of 110 percent.

The same mind-set applies in freewriting too. Don't aim to produce flawless prose. Just smash out some decent words and thoughts. Begin by telling yourself to "try easy" and just scribble something without pressuring yourself.

Secondly, _write quickly and continuously_. This will stop you judging yourself too soon. Aim for volume and don't worry about the actual content for the time being.

Write or type as fast as you can. If you hit a wall, try to keep on moving. Repeat the last words or phrase you wrote to keep the momentum up, even if it makes no grammatical sense! It's all about ensuring you don't get stuck in a rut. Quality isn't of the highest importance for now. You can always go back to tweak and amend your work once it's on the page.

Thirdly, _work to a time limit_.

Setting a time limit for your session means you'll maintain focus on your freewriting. That way you know when it'll end.

Chuck Palahniuk, the author of _Fight Club_, works like this. He likes to write while his washing machine is on a laundry cycle. This way he can compartmentalize his writing and concentration according to the machine.

Three rules down! Let's look at three more in the next blink.

### 4. To freewrite successfully, write how you think, go with the thought and redirect your attention. 

Now you've got the basics, let's look at three more golden rules for freewriting.

First of all, _write the way you think_.

This means writing for yourself and not an imaginary audience. You needn't over-explain, and you certainly shouldn't worry about grammar. You'll really feel the benefits if you're not constrained by any style or grammar rules. Your mind can roam free.

Consequently, explore anything that comes to you. You can skip around as you see fit.

What does this mean in practice? Well, say you're describing a product you've recently heard about. If you produce a few paragraphs that are easy for others to understand, then you haven't engaged your creative faculties. Try again and free yourself up!

Second, _go with the thought_. Don't feel bound by apparently false turns. There's no need to go back and correct yourself. Just keep going.

Maybe the problem you're facing is complex, and there are many ways to solve it. Don't get distracted. Follow one path and stick to it, so you won't get stuck if your mind starts flicking back to think about different paths.

Consider this analogy: Imagine you and a colleague find out that a computer at work is faulty. You want to search for the person who damaged the machine, but your colleague wants to fix the technical problem immediately.

They're both legitimate routes, but in freewriting, you can't do both. So choose one path and stick to it.

Finally, _redirect your attention_. You know that feeling when your mind is stuck, and you're unsure what to do? You can use _focus changers_ to redirect your attention.

Focus changers are questions you should have on hand. They help you decide if you should continue along a particular freewriting path or change direction.

Keep them simple. Questions like "why?", "what am I doing wrong?" and "how do I improve this?" will do.

### 5. Writing is better than just thinking, and writing prompts will warm you up. 

So, just what is the purpose of freewriting? What is it trying to achieve? Well, for starters, there are some obvious advantages over just mulling things over.

When we think, our minds tend to wander. We get lost in thought. This means there's a decent chance that great ideas will get lost in the melee and forgotten. However, if you write things down you'll always have a record of what you thought and can trace your logic back.

Imagine going to a supermarket. If you shop with only a mental list, there's a high probability that you'll forget something. However, if you've written your list down, the chances of forgetting, say, milk or eggs are considerably reduced.

Freewriting preserves a record of your thoughts. This stops ideas from being lost in the ether.

Before you start freewriting, you should prepare the ground with a few _prompts._ These will warm up your mind and help you later explore unforeseen directions.

A prompt is the base line for your thinking and writing. Use a prompt as the starting gun for a session, or even as a signpost to send you off on new paths.

Robyn Steely, executive director of Write Around Portland, a non-profit organization that promotes writing workshops, has some good tips. He suggests the best prompts are short and open ended. Something like "After the storm . . ." or "I can still remember . . .".

Other examples of open phrases might include "If I didn't have to work I'd . . . " or "The best part of my workday is . . .". It's your job to complete the sentence as you see fit!

A prompt is a fantastic warm-up tool. What's more, a good prompt can easily push your thoughts into new and unexpected directions.

### 6. Refine your freewriting by reducing complexity, disconnecting yourself and churning out the goods. 

When it comes to freewriting, unnecessary complexity is best avoided. Think in terms of melodies rather than symphonies. Keep things simple.

In fact, in freewriting, too much complexity can get you into trouble.

When a solution seems beyond your grasp, it's often because you're overthinking or prioritizing complexity over succinctness.

You can escape this trap by working with facts. Facts are simple and concrete. What's more, they can often set off a train of thought as you follow one fact to the next.

For instance, a drop in company sales is verifiable. This leads to the discovery that weak productivity is affecting numbers, and this to the revelation that employee salary is too low to incentivize staff to work hard. One simple fact has set off a clear chain of thought that gets right to the heart of the problem.

There's another way to hone your freewriting skills: _disconnect_ and abandon old ideas when new ones emerge.

Disconnection is easy in theory, but you should try to do it actively. When you reject a hypothesis or trash a thought, try to find a lesson before moving on, but, whatever you do, keep writing. Write about why and how you abandoned the idea.

You'll find that it's when you ditch ideas that don't develop that freewriting starts to pay off.

This brings us to the point of producing all this material. An abundance of ideas is better than a single one. Focusing on finding a perfect idea can actually be counterproductive because you demand too much of yourself too early.

You can put this into practice. Try freewriting sessions where you come up with one hundred ideas for a problem. Take a few days to do it, and do it in more than one sitting. The ideas can be mundane, fanciful or even just plain silly — it's a numbers game that will stop you feeling frustrated at not having the perfect idea immediately.

### 7. A little bit of lying can actually boost your freewriting. 

All your life you've been told that lying is bad, but that isn't true with freewriting. Lying is a good way of exploring ideas. Think of it as playing make-believe on a piece of paper.

In freewriting, lying means breaking with reality. You can explore situations which might seem silly or even absurd on initial reflection. But by doing so, you can actively channel your freewriting sessions in new and fecund directions, ending up in unexpected places.

When you're trapped in reality, your responses are limited. Lying is a fantastic way to bolster your freewriting efforts.

Let's think of an easy way to start lying.

Say you're writing about something "small." Instead, think of it as "tiny" or "gigantic." If it's "important," make it "crucial" or "ordinary." If it's "boring," rebrand it as "pathetic" or "super exciting." Repeat the technique as much as you need!

Another way to boost freewriting is to imagine fictitious conversations.

Create a roster of real or fictitious characters to ask you simple questions. They're there to motivate you to react. Particularly useful are characters who hold completely different points of view. That'll really test your thought processes.

Or maybe you could address a "future you," and imagine how that person might be different. That's also a good way to stimulate ideas.

But you shouldn't create characters who are too abstract, like God or Buddha. Characters who are too wise and successful, like Steve Jobs or Abraham Lincoln, aren't great either. You might feel a bit insecure expressing yourself to them!

Your character should be clear and vivid rather than historical or too abstract. A best friend or a teacher you admired in school should work well.

### 8. Share your writing, collect stories and try longer freewriting sessions. 

Freewriting sessions are a personal experience. Use them to explore your thoughts and probe your processes. They're great for discovering fresh ideas and solutions.

However, it's also good to share the results.

If you circulate your writing, you can benefit from feedback. This will, in turn, help you organize your ideas all the better.

A close friend or colleague is probably the best source of constructive feedback.

She should be willing and have the time to read your writing. You should be clear about the kind of feedback you expect from her, too.

You could prompt her with questions like "what works?", "what doesn't?", "is it too much?" or "is something missing?"

Another way to inspire and motivate yourself in freewriting is to gather stories from your everyday life.

Daily experience can quickly become rich prose, especially when you're short of material. Keep your eyes peeled and freewrite what you see.

Begin by doing it just for a day. A short episode can be worked up in just a few minutes. After that, set aside whole sessions for these stories. Almost every event has a lesson in it.

Look at it this way. If a story arouses your curiosity, it'll probably engage other audiences too.

Finally, if you're stuck on complex problems, longer freewriting sessions can be helpful.

Short five- to 20-minute sessions can work great. But if deep and involved thinking is required, or if you're gathering material for a book, you could opt for freewriting marathons.

Over several hours you can amass tons of fantastic material, but a little bit of strategy is important.

Long freewriting sessions can comprise several smaller spells. Freewrite for 20 minutes then, instead of taking a break, reread your notes and flag what seems interesting. Delve right back in and try for another 20 minutes. Repeat. Before you know it, you'll have completed a freewriting marathon.

### 9. Archive your freewriting. You can transform it into finished prose later. 

Have you ever thought about writing a book?

The rules and techniques used in freewriting can be a great help. You'll be able to thrash out great brainstorming sessions and solve whatever problems you're facing, but you'll also accumulate loads of publishable material brimming with your greatest thoughts.

For this, you'll need to archive your freewriting. File away those golden nuggets with an inventory!

Freewriting isn't just a way to clarify your thoughts; you can also use the results for further research. It's essential therefore to keep records of all your freewriting sessions. You never know when they can be revised and provide you with solutions in the future.

A good filing technique is critical for this exercise. Your inventory should have themes like "business," "writing techniques," "sales," or "love stories," depending on what you like to write.

You can use these tidbits as the foundations for further work or as prompts for new freewriting sessions. You can even recycle them by adding them to later writing.

At its best, freewriting can also produce a nutrient-rich culture for finished prose, whether for a book, a master's thesis or a dissertation.

If it's polished prose you're after, start with a few warm-up freewriting sessions. The topic itself isn't important.

Once you're all warmed up, freewrite with a little more focus. You might have to aim for marathons rather than sessions.

Simultaneously, try to build up your archive. Cut, rephrase and tweak the material. Keep things relaxed and limber, then start to link ideas together with transitions and new passages. Edit again and again, and before too long, you'll have completed the manuscript!

It's clear freewriting can solve simple day-to-day problems. But with a bit of effort, the method can be adapted for more complex material. You might even get a book out of it. The most important thing? Keep on writing.

### 10. Final summary 

The key message in this book:

**Never stop writing, even if you don't know what to write. Just let it flow out. Follow the rules set out in the blinks, and before too long great ideas will start to emerge.**

Actionable advice:

**Try adjusting your writing processes.**

Once you have completed a few freewriting sessions, reflect on your writing. You can refocus by posing yourself new questions. These could lead you down a whole different path. Start a fresh session and go from there.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Words That Work_** **by Dr. Frank Luntz**

_Words That Work_ (2007) is a guide to getting your point across more efficiently and effectively. These blinks explain the power of language and how it can help you in any number of situations, from business to political discussions to getting out of a traffic ticket.
---

### Mark Levy

Mark Levy is the founder of marketing strategy firm Levy Innovation. He has written for the _New York Times_, written or co-authored five books, and has taught research writing at Rutgers University. He also has an interest in magic.

