---
id: 5e872ddc6cee070007bf0488
slug: adventures-in-the-anthropocene-en
published_date: 2020-04-06T00:00:00.000+00:00
author: Gaia Vince
title: Adventures in the Anthropocene
subtitle: A Journey to the Heart of the Planet We Made
main_color: 2790C5
text_color: 10577B
---

# Adventures in the Anthropocene

_A Journey to the Heart of the Planet We Made_

**Gaia Vince**

_Adventures in the Anthropocene_ (2014) explores how humanity has altered the planet so radically in recent decades that a new geological epoch is said to be coming into being — we're crossing over from the Holocene to the Anthropocene, or the Age of Man. Author Gaia Vince examines what the changes we've made really mean for the world. From disappearing islands to urban slums, from Mekong fishermen to ancient nomadic tribes in Kenya, these blinks tell the story of our new relationship with nature and our hopes for the future.

---
### 1. What’s in it for me? Learn how we can help humanity and our planet in the Age of Man. 

If you've spent any time on this planet in the last few decades, you've probably noticed that things are changing fast. From exciting new technologies like smartphones, to hotter summers caused by climate change, to mountains of garbage piling up, there are both good and bad developments here on planet Earth.

And the changes aren't just on the surface. We've altered the planet so radically, that a new epoch is dawning. Scientists call the new era the _Anthropocene_, or the Age of Man.

Humans are no longer just one more species on Earth. We're a natural force equal to the volcanoes and asteroids that determined the planet's fate in times gone by. But what do those changes really mean for all of us who live here?

In these blinks, you'll learn about the effects of humanity's impact on the world. You'll see the destruction we've wrought in the depths of the oceans, on the peaks of mountains, and even upon the barren desert plains. You'll discover the ways we're reshaping our planet, how we must adapt ourselves to the new nature we're creating, and what the future may hold.

You'll also find out

  * what the consequences of an ice-free Arctic could be;

  * how giant mirrors could save the planet; and

  * why chapatis in Nepal may never taste so good again.

### 2. Humans are changing the atmosphere beyond recognition. 

Ever laid under a tree on a warm summer's day and watched the clouds drifting by? Or marveled at the stars at night? The skies above us and the air we breathe are as familiar to us as they were to our ancestors thousands of years ago.

But what we're doing to the atmosphere these days is completely new.

**The key message here is:** **Humans are changing the atmosphere beyond recognition.**

As you may have guessed, we're talking about pollution. It's not that our releasing noxious substances into the air is anything unusual. Just think of the infamous London smog. However, the extent of the damage has changed dramatically.

Humans are no longer just a tiny puff of smoke on the vast surface of the earth. Today, we're altering the atmosphere on a global scale.

This is partly because there are so many more of us now; the world's population is already more than seven billion. But that's not the only reason. The days of the "dark Satanic mills" of the Industrial Revolution may be long gone. Stricter pollution controls have cleared up the visible soot and sulfurous gasses that used to dirty the skies. What they haven't got rid of is the source of the problem itself — coal power. Pollution from coal power stations in Europe alone still kills more than 22,000 people a year.

And the developing world is also contributing more pollution than ever. In China, there's so much dirty industry that only a tiny 1 percent of the population is breathing air that European Union standards count as clean.

What's more, people are adding their own homemade pollution to the industrial emissions.

In Nepal, for example, the largest polluter is the wood and dung cooking fires used all over the country. They arguably make for the tastiest chapatis, the popular local flatbread. But what they also do is infuse the air with a distinctive acrid brown haze.

The effects of the haze are many, from rising temperatures to frequent droughts, which result in failed harvests. And the health effects are as alarming as the environmental ones. In India alone, estimates say that, annually, almost two million people die from conditions related to the haze — more people than are killed by malaria worldwide.

The good news is that the effects need not be permanent. If all the emissions were to stop at once, it wouldn't take many years for the atmosphere to recover.

But we all know that's not about to happen. So the challenge for humanity is going to be to learn to live in this new atmosphere and the new climate it brings.

> _"Living in a changed climate is like living in a different world."_

### 3. Dramatic changes to mountains threaten our survival. 

Humans have always been enthralled by the gleaming majesty of snow-capped mountain summits. But scale any mountain today, and you're more likely to find a trail of litter than an untouched snowy wonderland.

And, tragically, the changes are more than just aesthetic.

**The key message here is:** **Dramatic changes to mountains threaten our survival.**

Mountains are more than just a source of wonder for humans. They're an essential source of life. For starters, we depend on mountains for fresh water. Over half of all the world's fresh water is stored in mountain glaciers.

But global warming is destroying these natural stores. Rising temperatures are melting the glaciers that hold all that water.

Take the magnificent Himalayas. Outside of polar regions, it's the largest area in the world covered by glaciers — 35,000 square kilometers of them, storing almost 4,000 cubic kilometers of ice. But they're melting so fast that more than two-thirds are expected to be gone by the end of the century.

And with our supply of fresh water melting away, the race is on to replenish our reserves.

One way governments around the world are starting to respond is by building reservoirs to catch and keep the meltwater from disappearing glaciers. But that's expensive, and often simply logistically impossible.

Luckily, there is another option — and that's to try to hit the problem at its source. In other words, we could try recreating the cooler conditions that glaciers require.

In fact, artificial cooling might be the future. The best way to cool the planet could be to reflect heat away from it. How do you do that? One idea is sending giant mirrors into outer space to orbit the earth and reflect sunlight before it's even reached us. Another is to release tiny reflective particles into the atmosphere, mimicking the effects of pollution, but for the good of humanity this time.

But artificial cooling brings its own dangers. The biggest one is known as the _termination problem_. If this intensive cooling were to stop abruptly, it would lead to a sudden rise in temperature by several degrees.

And that would be much more catastrophic for the planet than the gradual warming we're experiencing now.

### 4. Our rivers supply much-needed energy, but this comes at great human and environmental cost. 

Drinking, washing, fishing, sailing — from food to transport, we've been relying on rivers for our basic needs since the beginning of time. But in the last century we've also used them as a source of power. Today, two-thirds of the world's rivers already have a dam somewhere along them — and ever more are planned.

**The key message here is:** **Our rivers supply much-needed energy, but this comes at great human and environmental cost.**

Dams provide power — a particularly efficient type of power called _hydropower_. Hydropower produces energy by storing water in reservoirs behind dams, and then releasing it in bursts past turbines to generate electricity.

In effect, this means that the dam comes with its very own battery — the reservoir itself. And this natural battery is 80–90 percent efficient. What's more, unlike, say, solar power, it doesn't depend on the weather and it produces power constantly. Also, since the infrastructure needed to build a dam is relatively inexpensive, the economic benefits are also huge.

Sounds perfect, right? Well, not completely. **** The arrival of a dam also brings a lot of negative consequences for the environment and its inhabitants.

When a reservoir is built, the immediate result is that fertile land is flooded. As though that's not bad enough, the fertility of the entire area beyond the flooded parts is also affected. That's because dams stop sediment carried by rivers from reaching land, and soil washed away during rainfall can't be replaced. And the environmental impact doesn't stop there. The sheer weight of so much water can cause earthquakes. At the same time, downstream, fish are prevented from reaching their spawning areas and can no longer breed.

It's a major dilemma. On the one hand, hydropower brings much-needed energy to poor countries and regions. The remote sleepy villages along the Mekong River, in Laos, will soon have reliable electricity and internet thanks to the construction of new dams.

But, on the other hand, as a local fisherman points out, you can't eat electricity. No longer able to fish or live off the land, local people are at risk of being displaced from their homes. And there are also losses that are harder to quantify — like the disappearance of ancient burial grounds. Without their traditional means of survival, entire communities and ways of life are under threat.

And that's the key challenge of our times: how to meet our growing needs without doing further damage to the very resources that provide for them.

### 5. To feed our growing population in a warming world, humanity needs to make farming more efficient. 

In this age of global warming, where water shortages are becoming the norm, land is becoming increasingly less hospitable for farming.

Africa is a prime example. Across the continent, there is already little irrigation and little rainfall. And with the climate changing, it's getting ever harder for people to survive there.

**The key message here is: To feed our growing population in a warming world, humanity needs to make farming more efficient.**

Take Uganda. Here, rainy seasons used to run like clockwork, and farmers could plant and harvest at specific times. Sowing seeds could begin at exactly the right moment, just before the water came. But now rains have become sporadic and unpredictable. Sometimes it rains for weeks; more often, for just a day or two. No one knows if they should start planting with the first few drops, or wait and see if the rain will last.

Poor rains mean poor harvests, and poor harvests mean food shortages.

Food shortages are then exacerbated by rising prices, caused by speculation by foreign corporations. So we can't count on them for help. But, luckily, there's another way — increasing the productivity of small farmers.

Meet Winifred, a local farmer in the east of Uganda. She lives in an area where almost 80 percent of the people depend on food aid to survive. But she's able to feed her husband and nine children from her own fields, where she grows sunflower, cassava, and sesame. What's more, she's turning a profit — and looking to expand.

It took some well-placed advice from the National Semi-Arid Resources Research Institute to get her there. They gave her training on things like the best crops to grow and the best way to prepare her land. With a loan, she also got access to the institute's higher-quality seeds.

The loan paid off — and then some. The very next season Winifred had a surplus she could sell on the market. Now she's doing so well that she can send her children to private boarding schools.

Like Winifred, humanity as a whole needs to get more out of our farmland. Population growth means that the demand for food may soon exceed the supply. Already, 300 children die every hour because of malnutrition.

To feed the ever-growing population in this changing climate, we need solutions like making crops more nutritious and developing heat- and drought-resistant crops. That way we won't have to rely on environmentally-unfriendly industrial agriculture. Instead, farming can be adapted to the local climate in each place. And no one will need to go hungry.

### 6. We’ve transformed the oceans and now we must deal with the consequences. 

The earth's oceans are vast. We've explored only around 3 percent of the world beneath their surface. And yet we've already transformed them drastically.

We've built bridges over them and tunnels under them; we've even joined oceans with the canals in Panama and Suez. We throw our waste into them. We get food from their surface and gas and oil from their depths. And, most worrying of all, as the global temperature rises, the frozen waters of the Arctic are melting.

**The key message here is** : **We've transformed the oceans and now we must deal with the consequences.**

Melting ice is the most pressing issue.

With temperatures going up, the North Pole is losing its ice at an alarming speed. The Arctic is warming at least twice as fast as anywhere else on the planet. Almost all its glaciers are getting smaller. In fact, by 2030, the Arctic could be completely free of ice.

But the consequences reach far beyond the North Pole.

For Europe, melting ice in the Arctic will cause increasingly extreme climate conditions. No more balmy summers and snowy but relatively mild winters. The region could become subject to monsoon-like flooding, alternating with months of drought. And winters could become much colder.

And that's not the end of it. Thousands of kilometers to the south, entire islands and nations are already on course to vanish beneath the ocean waves.

The Maldives is one such nation. Comprising more than a thousand tiny islands, it's the lowest country in the world. The risk of disappearing into the ocean is very real here.

In fact, even if the average global temperature rise is kept down to 2°C above pre-industrial levels, as international climate negotiations aim, it's going to be too late for the Maldives. Unless drastic measures are taken, the country will be submerged and become uninhabitable.

So, what solutions are there?

Well, one answer is to adapt existing land or even to build artificial landmasses to which displaced populations could be moved. Examples have already sprung up nearby. There are storm-safe "designer islands" like Dhuvaafaru, purpose-built from scratch on previously uninhabited land, and specially designed to withstand tsunamis.

And there are also entirely artificial islands like Thilafushi, which is made of garbage.

But the Maldives isn't the only place under threat. On a more global scale, these kinds of local stop-gap measures — "designer islands," artificial landmasses — will not save the planet. What we need is to engineer radical new ways to offset the damage we're doing.

### 7. Climate change is destroying traditional ways of life in the desert while creating new power sources. 

For centuries, nomadic desert tribes like the Turkana have been wandering the barren landscapes in the north of Kenya with their cattle, goats, and camels.

But their way of life is disappearing fast. **** In 2006, Christian Aid reported that one-third of northern Kenya's nomadic peoples had been forced to leave their traditional way of life behind. Within two years, that number doubled.

**The key message here is:** **Climate change is destroying traditional ways of life in the desert while creating new power sources.**

What's forcing them out? Well, in recent times, it's been mainly drought.

People here have always been used to harsh, dry conditions. **** But periods of drought are getting more frequent and lasting longer. And the rains that come between droughts are simply too short.

This means that there's not enough time to recover from each dry spell. Vegetation dies, and, without sufficient food, livestock grow weak or succumb to starvation. There's no time to breed the animals and herds dwindle and, eventually, die out.

Yet greater changes loom on the horizon. The droughts have destroyed one way of using the desert. But, by expanding the area of arid land, they've also enabled another.

The continent's biggest wind farm is planned on the land where the Turkana tribe has always lived. Thanks to the reliably strong winds, wind turbines in deserts can produce double the power that they do in Europe. Up to 20 percent of Kenya's energy needs will be generated by the new wind farm.

As the droughts render Kenya's existing hydropower increasingly unreliable, the need for a stable energy source is becoming ever more pressing. But it's not just windy deserts that can be used to generate energy.

Even where there are no winds, the sun still shines all year round. In a desert, it's a reliable source of power that never runs out.

A new development is that it can now work even without individuals investing in solar panels. Across Africa, pay-as-you-go solar-powered phones are already used to provide connectivity to the poorest people, who have no access to electricity. They no longer need to spend a fortune on dirty kerosene or diesel. Instead, they get a battery to power their device for a small fee. They then simply swap it for a recharged one from time to time at a community solar-charging center.

And as we ramp up our use of this neglected resource, the earth's massive and ever-expanding deserts are set to become key to future energy production.

### 8. We’re no longer a mere part of nature. We control it. And its future depends on us. 

Ever since we learned to walk upright among the lush green grasses of the savannah, we have been gradually altering our natural environment. Whether cutting down trees or introducing new plants, hunting wild animals or creating domesticated species, we've put our stamp on virtually every corner of the earth.

But it's one thing to _influence_ ecosystems. It's quite another to fundamentally reshape them.

**The key message here is:** **We're no longer a mere part of nature. We control it. And its future depends on us.**

We've already set off a new wave of extinctions. There have already been five mass extinctions on the planet. Each one was triggered by a cataclysmic event; the most recent one occurred 65 million years ago, when a meteorite crashed into the earth.

Today, we're doing it all by ourselves — no asteroid needed. According to Anthony Barnosky, a biologist at the University of California, Berkeley, we're heading toward a sixth mass extinction, caused by human activity. He estimates that the current rate of extinction is somewhere between one and ten thousand times faster than it would be if we left nature to itself.

There's no question that humanity has made its mark on the natural world. Now we have to decide how to deal with the changes we're creating.

We do have the power to determine what our natural world will look like in the future. But we'll need to think collectively about which parts of existing wildlife we want to preserve — as well as how we want to preserve them.

For example, one result of our influence on nature is that it can no longer keep itself in balance. A case in point: we're driving jaguars to extinction with our demand for farmland. But at the same time we're losing their service as a natural predator that keeps rodents, and the disease-spreading ticks they carry, in check.

To counteract this problem, we could attempt to restore balance to ecosystems by artificial means. For example, one proposal is to bring elephants and rhinos to Australia — which is not their natural habitat — to eat grasses and hold back wildfires. 

We have the power to change nature. The question is what we'll do with that power.

### 9. Forests have never been more threatened. 

Roughly half of the forests that once covered the earth have already disappeared because of human activities. If we keep going at this rate, we'll wipe out all of the world's rainforests before the century is over.

But what, precisely, is driving all this destruction?

**The key message here is:** **Forests have never been more threatened.**

It comes down to one main cause across the world: the deforestation that accompanies road-building. That's because, after a road is built, the area around it gets destroyed. According to scientists, almost all the world's deforestation — 95 percent of it — happens within 25 kilometers of a road. Farmers deforest to clear land for crops, and drug growers also use the territory. In the Amazon alone, 50 thousand kilometers of roads were built in just three years, and, on average, a 50-meter radius of deforestation surrounds each one. 

Still, there are good reasons why roads are built. Take any remote, impassable region anywhere on the planet, and it's likely to be crying out for a road. We need them to create access for things like mines and dams, and to connect distant towns and villages. In a way, it makes good economic sense. Build a road and suddenly a community is connected to the benefits of civilization, like education and trade.

But there's also a more sinister side to this.

It's not just ordinary people who benefit from better infrastructure. It's also animal poachers, as well as traffickers and growers of drugs like cocaine. That's why resisting deforestation is so hard in the Amazon specifically. In fact, in 2011, one activist per week was killed on average because of this.

With such powerful nefarious interests at stake, the future may look bleak for forests.

Fortunately, there are things that can be done to reduce the damage. When it comes to connecting massive infrastructure projects to the rest of the world, one answer is to build railways instead of roads, or to use existing river networks.

This is what the Camisea gas project did. Located deep in the Amazon, in Las Malvinas, Peru, the project is unconnected to roads. The area works like an island. Goods and people travel by boat, by air or through underground pipeline, leaving the rainforest mostly untouched.

We need every patch of forest we can save. And the sad truth is that, as global warming accelerates, forests are disappearing faster than ever.

### 10. Humans need to rethink their use of the earth’s mineral resources. 

Humans now move more sediment and rocky materials around the planet than any natural cause — more than all the rivers, glaciers, wind, and rain of the world combined. Take coal alone; we mine around 8 billion metric tons of it each year. You could build 16 Great Walls of China with that amount of material.

But the earth's supplies are finite.

**The key message here is:** **Humans need to rethink their use of the earth's mineral resources.**

Coal may still be plentiful, but silver is among the resources that are running out. Silver has many uses beyond mere adornment, from electronics to heart valves. What's more, the scarcer such minerals become, the more we sacrifice to mine for them.

The harder we have to search for silver, the more land, energy, and water are needed to extract it. In the mining process, rainforests and rivers are destroyed. And abuses of human rights increase.

Take Potosí, for example, a city nestled in the Bolivian mountains, some 4,000 meters above sea level. Once prosperous and bustling thanks to its silver mine, the city has been in decline since the silver ran out. And the human cost of mining for what remains of the supplies is huge — workers die within ten years of entering the mine, usually before they turn 35.

The problem is that human demand for just about everything is growing. Our use of fossil fuels, ores, minerals, and biomass is predicted to reach an astonishing 140 billion metric tons a year within the next three decades. Just think how many Great Walls of China you could build with _that_.

Of these, it's fossil fuels like coal that cause the most pollution. The coal power stations built in the first decade of our century will by themselves emit more carbon dioxide in the next 25 years than has been emitted since the Industrial Revolution.

But reducing our use isn't going to be easy. Right now, 86 percent of the world's energy comes from fossil fuels, and energy consumption is growing.

To feed the demand, electrification is the way to go. Heating, lighting, transport — all of it can be powered by batteries instead of fuel tanks.

And the light, efficient magic ingredient for batteries is an element that is also found near the earth's core. It's lithium. The smartphone you probably have in your hand right now uses it. And there's much potential to expand its use.

It's just a question of creating the right infrastructure.

### 11. Our age is the urban age and cities are the future. 

Whether you're listening to these blinks on your commute to work or reading at home, chances are you're in an urban environment. In the not so distant past, as an urban dweller, you'd be part of a tiny minority. But these days more than half of all people on the planet live in cities — and the number is growing fast.

**The key message here is:** **Our age is the urban age and cities are the future.**

Full of culture and buzzing nightlife, cities are exciting places to be. And they're also the most productive and efficient way for us to live.

In fact, the denser a city, the more efficient it is. Just look at the numbers. When a city's population doubles, average wages go up by 15 percent. At the same time, the use of resources and carbon emissions falls by the same amount. And what's more, in economic terms, it will outperform the combined output of two cities half its size by as much as a fifth.

Knowing all that, you might already be convinced that cities will be the answer to humanity's environmental and social problems. Surely, they're the way to deal with population growth while increasing sustainability.

Unfortunately, things aren't so simple. Urbanization comes with many problems of its own, and they could well turn out to be our undoing.

For one, the picture changes if we look not just at prosperous Western cities, but at the developing world. In many poorer countries, sustainability is actually worse in cities. That's because as people in cities increase their wealth, they use more energy, produce more waste, and consume more food than the poor rural populations. In contrast to this, in richer countries, the wealthy tend to live in rural areas, so this effect is reversed.

What's more, the process of urbanization is itself a squalid business. **** Cities often grow as a result of fast, unplanned migration from the countryside. And that brings informal settlements in slums without infrastructure, leading to mountains of trash, polluted water, and contaminated land.

As the world urbanizes, whether we overcome these problems or let them pile up depends on us. If we all work together to come up with innovative solutions, cities may, after all, fulfill their promise. They could become the perfect environment for us to survive and thrive on our changing planet.

> _"Cities are the most artificial environment on Earth, and the one in which humans feel most at home."_

### 12. Final summary 

The key message in these blinks:

**As humans, we're part of the natural world and we rely on our planet not just for the air we breathe, the food we eat, and the water we drink, but also for the resources that power our ways of life. We're currently changing the earth on an unprecedented scale and adjusting its every corner to our demands, and this takes away from its ability to meet our needs. If we're to continue to thrive here, much work and ingenuity are needed to avoid more destruction and undo the damage we've already done.**

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Novacene_** **, by James Lovelock**

Now that you understand the deep and irrevocable impact we humans are having on our planet, you might be feeling a little pessimistic. You might be wondering if we'll be able to undo the damage before it's too late. But there's no need to despair.

The good news is that help may be on the way. In _Novacene_, James Lovelock argues that yet another change is on the way — a new age based on artificial intelligence. He believes that new, hyperintelligent beings are about to emerge, and that they'll work with us to save the planet and lead us to a new prosperity.

So if you're feeling anxious about the future of our planet, have a look at our blinks to _Novacene._ They'll give you some good scientific reasons to be hopeful.
---

### Gaia Vince

Gaia Vince is a science writer, journalist, and broadcaster specializing in the environment. She is a former editor of _Nature_, one of the most renowned scientific journals in the world. Her work has appeared in newspapers and magazines including the _Guardian_, the _Times_, _Scientific American_, and _New Scientist_. She also writes about science for radio and television, including the BBC.

