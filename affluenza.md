---
id: 54298f6d6266610008000000
slug: affluenza-en
published_date: 2014-10-02T00:00:00.000+00:00
author: John de Graaf, David Wann and Thomas H. Naylor
title: Affluenza
subtitle: How Overconsumption is Killing Us – and How to Fight Back
main_color: F78656
text_color: 78412A
---

# Affluenza

_How Overconsumption is Killing Us – and How to Fight Back_

**John de Graaf, David Wann and Thomas H. Naylor**

This book is about our serious addiction to consumption: affluenza. Since the Industrial Revolution, we've become addicted to shopping, believing we can buy happiness. Affluenza affects us and our society like a disease, and this book offers advice on how we can immunize ourselves against it.

---
### 1. What’s in it for me? Learn about our addiction to overconsumption. 

How many electronic gadgets do you own? Well, you're reading this on at least one of them, but it wouldn't be unreasonable to assume you have more! And when the tech companies come out with another edition, what do you do? Are you still satisfied with what you have? Or would you prefer to have the newest model?

Society as a whole has become addicted to consumption. Our sheer desire for more _stuff_ is inhibiting our lives. It makes us work so much that we don't have time for things that matter in life, and we're destroying our planet in the process.

In these blinks, you'll learn about our current affliction with affluenza, and what we can do to solve it.

You'll also learn

  * why Americans are so eager to buy and consume more and more products;

  * about the damage overconsumption does to our physical and mental health;

  * why our overconsumption is destroying the planet;

  * how you can cure your affluenza and lead a simpler life; and

  * how schools and governments can help reduce and end overconsumption.

### 2. The Post-Industrial increase in productivity has created an addiction to consumption. 

Imagine if the history of Earth was compressed into just seven days. How much time do you think human civilization would take up? Well, the development of agriculture would take two seconds. The Industrial Age — the last 200 or so years — would take just _one hundredth_ of a second.

In this brief blip since the Industrial Revolution, we've consumed more resources than all people in Pre-Industrial human history combined. Americans now spend _71 percent_ of their $15 trillion economy on consumer goods.

This vast increase in consumption can mostly be attributed to technological advances. The Industrial Revolution dramatically increased our productivity: we can produce a great deal more, with a smaller workforce and lower costs.

People used to think that this increase in productivity would allow us to be more relaxed and have more free time. In fact, in 1965, the United States Senate estimated that by the year 2000, the working week would only be 14–22 hours long.

Instead, we've continued to work long hours, and some people work even _more_ now thanks to laptops and mobile phones. Rather than enjoying society's increase in production, we've become afflicted with _affluenza_.

Affluenza is our addiction to consumption, and it's taking over our lives. Nowadays, we spend most of our brief free time buying products or consuming them. Affluenza prevents us from giving proper attention to things that make us truly happy, like relationships or exercise, for example.

The United States essentially reached a "happiness plateau" in 1957. Since then, the number of Americans who consider themselves "very happy" has steadily declined. People think consumption brings them joy, but affluenza is actually lowering our quality of life.

### 3. We try to compensate for our unhappiness by buying things, but it only makes us feel worse. 

Do you have kids? If so, what do you do when you're not looking after them or working? Do you go out, or are you too exhausted? Do you find you prefer to spend your free time just flopping down on the sofa?

You aren't alone if you answered yes to that last question — most people would prefer to flop. Because we spend most of our time working to raise money to buy things, we scarcely have time left for our family, friends and communities.

Even our time with our children is often not spent well. Most often, we're simply driving our kids from one class or activity to the next. In fact, the number of hours families spend together on holidays or at meal times has declined by one third since the 1930s.

Most of our precious time away from work and children is spent wearily watching TV. Couples no longer socialize together, but stay at home. This process of social withdrawal is called _cocooning_.

We try to compensate for this unhappiness by buying things. In our society, cocooning is prevalent and people often lack the time to form meaningful relationships. We end up trying to buy our social lives.

For example, one compulsive American buyer who spoke to the authors bought several expensive TVs and stereos in the hope of feeling less lonely. He knew his neighbors would regard him as an expert in electronics and come to him when they needed advice.

Our addiction to work and shopping leads to a vicious cycle. We buy more stuff to fix the problems caused by our desires to buy stuff in the first place. This cycle prevents us from seeking out things we truly need — connections to other people and nature.

> _"Parents who spoil their children out of 'love' should realize that they are performing acts of child abuse."_

### 4. The environmental consequences of overconsumption are devastating. 

The social cost of affluenza is massive, but something else is being destroyed at an even greater rate: the environment.

After decades of rapidly increasing consumption, we've mined most of the world's easily accessible resources. This means we have to turn to more dangerous and complicated mining procedures.

Copper, which we use for a wide range of products, has been extensively overmined — yet we continue to extract it. In fact, it's estimated that more than half of all copper ever used in history has been dug out of the Earth in the last 24 years.

We have to keep digging deeper to fulfill our needs, and we take big risks by doing so. One copper mine near Salt Lake City was three-fifths of a mile deep and 2.5 miles wide before it collapsed after a landslide.

Our desire for oil has also led us to mine in dangerous areas. The Deepwater Horizon oil mine was an unfortunate example of this. It ran a mile deep into the ocean, and when it accidentally exploded, oil gushed into the sea for 87 days. It released about 4.9 million gallons and caused great damage to the wildlife around it.

Our overconsumption has a massive impact on the Earth's ecosystem. Each year, the average middle class family is indirectly responsible for the movement, processing and disposal of _four million_ pounds of material.

This has dire consequences for nature. In fact, we might currently be on the verge of the most high-impact species extinction since the dinosaurs: Coral reefs are dying off at alarmingly rapid rates. Huge chunks of reef in the Americas have died off in this decade, due to environmental changes like rising ocean temperatures and increased pollution. Our affluenza isn't just harming us — it's harming the planet we call home as well.

> _"Of the 84,000 chemicals in common commercial use, only about 1,500 to 2,000 have been tested for carcinogenicity."_

### 5. Although the poor are affected most by affluenza, inequality hurts us all. 

It's probably not a surprise to anyone that the worst problems caused by overconsumption are felt most by the poor.

In our quest to have cheaper and cheaper products, we've shifted a great deal of our production to developing countries like Bangladesh, where wages and working conditions are abysmal. At least 1,800 workers have died in factory fires and building collapses in Bangladesh since 2005.

Cost-cutting strategies that hurt the environment also disproportionately affect the poor. One manufacturing area of Louisiana, for example, has been nicknamed "Cancer Alley" because there are so many carcinogens in the air and water. Not surprisingly, the area is mostly inhabited by the poor.

The standards for consumption set by the media only exacerbate our affluenza. Countless popular TV shows, like _Desperate Housewives_ or _CSI_, are set in wealthy neighborhoods. Viewers see these affluent areas, and internalize them as the standard they want to reach.

As American shows become more popular in developing countries, viewers in poorer nations also internalize American standards of consumption, however unrealistic they might be.

This is illustrated very sadly in cities like Manila. One of Manila's most luxurious shopping malls sits next to an enormous garbage dump called "Smokey Mountain" where thousands of people live.

Rising income inequality hurts everyone, not only the poor. Of the 22 leading industrial nations in the world, the United States is ranked last in income equality. Countries with high income inequality have lower standards in health and greater crime rates across _all_ classes.

Although rich Americans are happier (on average) than poor Americans, they have the same life expectancy as poorer people in more egalitarian European states.

> **Fact:  

** In 2012, CEOs earned _354 times_ as much as their average workers, while more than ten million Americans couldn't afford enough food.

### 6. Overconsumption is not “human nature,” and there’s a long history of philosophical argument against it. 

When people criticize affluenza, they're often told that overconsumption is inevitable. The desire for material things, people say, is "human nature." But is there any truth to this claim?

There isn't. Affluenza is _not_ a part of human nature.

In the Stone Age, humans spent about three to eight hours a day "working." Even today in remote tribes, people follow similar patterns. Allen Johnson, a UCLA anthropologist, lived with the Machiguenga tribe in the Amazon rain forest for two years and observed this. He described the Machiguenga as "people who always have enough time. They're never in a hurry."

This sort of lifestyle is arguably more "affluent," in the sense of being truly fulfilling. In the West we perpetually need more material things to feel satisfied, but the Machiguenga know that as long as they can survive, they can spend time enjoying their life. Time spent socializing or making crafts brings much greater happiness than overconsumption.

Even before the current onslaught of affluenza, intelligent thinkers throughout history have criticized the desire for material things. Aristotle, for instance, denounced people "who have managed to acquire more external goods than they can possibly use, and are lacking in the goods of the soul."

The Stoic philosophers also spoke against materialism. Seneca once said, "A thatched roof once covered free men; under marble and gold dwells slavery."

Many religions also encourage their followers to stay away from overconsumption. In the Garden of Eden, for example, Adam and Eve had everything but still wanted more. Their greed led to the fall of mankind.

In fact, Jesus himself was one of the strongest opponents of affluence. He encouraged his followers to let go of all their possessions. The Bible preaches that you should be content with what you have.

> _"Unlimited wealth is great poverty."_ — Aristotle

### 7. Overconsumption has been systematically facilitated. 

One reason that affluenza is so difficult to escape is that it's actually built into our economy.

Many companies have a _planned obsolescence_ for their products. That means the products are designed to only last a short time, or else be continually upgraded. Gillette's disposable razors are an example of this. Planned obsolescence means that customers are forced to continuously buy a new version of the same product.

Products are often upgraded only in style rather than quality, so consumers buy them even when they don't function differently to the old version. This idea was first promoted by General Motors. After the Great Depression, they began introducing a new model every year to boost the demand for cars.

After World War Two, this became commonplace for many more products. Just think about how often a new iPhone comes out to make you feel yours is old.

To feed our desire for consumption, we have low-interest loans and credit cards so we can put off paying until later.

Few people can afford a new car every year, so the masses needed another way to increase their purchasing power: cheap personal loans. Personal loans allow people to buy whatever they want and worry about paying for it later.

A TV advert by Bank of America crassly illustrated how easily Americans could be seduced into getting loans. "Do you have money jitters?" it asked, "Ask the obliging Bank of America for a jar of soothing instant money. M-O-N-E-Y. In the form of a convenient personal loan."

When credit cards became popular, the problem only got worse. Now people can carry their personal loan in their pocket, without even having to ask Bank of America.

### 8. Omnipresent advertisements and PR strategies continually seduce us into wanting more stuff. 

How long do you think you can walk down the street without seeing an advertisement? Probably only a few seconds, if that.

Adverts are everywhere. We see them constantly: two-thirds of newspaper space and half the mail we receive is advertisements.

Nowadays, there are even adverts in some student textbooks. You might see a math problem for children that reads, "If Joe has 30 Oreo™ cookies and eats 15, how many does he have left?" Naturally, it will be next to an enticing photo of Oreos.

Adverts are highly effective, and their ubiquity deeply impacts our lives. In fact, the average American can identify _fewer than ten_ types of plants, but _hundreds_ of corporate logos.

Alongside the adverts, PR strategists also try to manipulate us into affluenza without us noticing.

One PR tactic for getting people to consume more is funding _front groups_. Front groups are organizations with scientific-sounding names that endorse the companies that create them, to make them seem more credible.

For example, "The American Council on Science and Health" may sound like a respectable organization, but it was actually created by fast food companies and petrochemical firms to defend their products against criticism.

The "Heartland Institute" is another front group. It supports energy companies in their campaign against climate change awareness.

The Heartland Institute even launched a billboard campaign that featured a photo of the Unabomber Theodore Kaczynski, an anarcho-environmentalist who killed three people. The caption below his picture read, "I still believe in global warming. Do you?"

Unfortunately, campaigns like this are quite effective: about two-thirds of Americans still believe that global warming is controversial within the scientific community, when in fact it's widely acknowledged.

### 9. You can lead a happier life by reducing your levels of consumption. 

So now that we've learned about affluenza, how can we move on? How can we cure ourselves of it?

Well, the first step is to identify its main symptoms. Recognize that consumption can't buy satisfaction, while _reducing_ your consumption _can_ lead to greater happiness.

One former stockbroker, Joe Dominguez, for example, remarked that his rich colleagues on Wall Street were just as unhappy as people in the ghetto where he grew up. Clearly, happiness must be determined by something other than money.

In 1995, a survey conducted by the Center for a New American Dream concluded that 86 percent of Americans who voluntarily decreased their level of consumption were happier afterward.

So, to increase your quality of life, find ways to consume less. Try to get more out of what you _already have_, rather than trying to have more.

Some young professionals in Seattle have found an interesting way to break away from cocooning. They move into tiny apartments called "apodments" which are only four by four by ten square metres. Having a tiny apartment means you have to spend more time outside, in nature or with friends.

We also need support from others to cure our affluenza, just as an alcoholic needs support to stay sober.

Cecile Andrews's book _The Circle of Simplicity_ offers good advice on this. Andrews describes how anyone can start voluntary study circles where members help each other live well on a lower income. She's started hundreds herself, and thousands of people have benefited from this.

Today, finding like-minded people who also want to cure their affluenza is even easier — you just have to look online. Arranging to meet in person, or even just meeting online, can be very helpful in your battle against affluenza.

### 10. Media education and subversion is necessary to immunize us against the affluenza virus. 

Affluenza is all around us. Every street, every TV and every newspaper we see compels us to buy more and more things we don't truly need.

As with any other disease, we need a kind of vaccination to protect us. And that means we need to use the virus _itself_ to generate the resistance.

Anti-ads have thus far been a good implementation of this idea. They seem authentic at first, until viewers realize that they're seeing the exact opposite of an advert. That moment of realization causes the viewers to really think about what they've seen, which is the opposite of what regular adverts want.

One anti-ad, for instance, featured two cowboys similar to the Marlboro Man, riding horses into the sunset. Below them, the caption read, "I miss my lung, Bob."

Anti-affluenza efforts like this are especially important for children. While children spend 40 minutes a week outside, they now spend about 50 hours a week with electronic media.

Many schools nowadays teach children to analyze the media and question the advertisements they see. This is called _"media literacy"_ and in our digital age of growing affluenza, it might be just as important as literacy itself.

A good documentary for increasing your media literacy is _The Story of Stuff,_ directed by a former Greenpeace activist. It details the director's 12 years of experience studying waste disposal and its effect on the environment.

The authors consider _The Story of Stuff_ one of the most important tools for immunizing against affluenza. So if you're interested in learning more after these blinks, why not look it up? We need to educate ourselves about our affliction if we want to rid ourselves of it.

> _"The average American will spend nearly two years of his or her lifetime watching TV commercials."_

### 11. Final summary 

The key message in this book:

**In the Post-Industrial age, our society has become afflicted with a serious disease: affluenza. We're obsessed with consuming, and it's ruining our lives and planet. We urgently need to cure ourselves if we want true happiness, and want to save our home.**

Actionable advice:

**Make** ** _more_** **out of your money, not more money.**

We're stuck in a rat race where we chase money all our lives — try to resist it. Don't expect that more money will mean more happiness. It won't, and in fact, you'll be happier if you buy _less_ stuff.

**Suggested** **further** **reading:** ** _Manufacturing Consent by_** **Edward Herman and Noam Chomsky**

_Manufacturing Consent_ takes a critical view of the mass media to ask why only a narrow range of opinions are favored whilst others are suppressed or ignored. It formulates a propaganda model which shows how alternative and independent information is filtered out by various financial and political factors allowing the news agenda to be dominated by those working on behalf of the wealthy and powerful. Far from being a free press, the media in fact maintain our unequal and unfair society.
---

### John de Graaf, David Wann and Thomas H. Naylor

John de Graaf has won over a hundred awards for documentary film making and is cofounder of The Happiness Initiative. Environmentalist David Wann is president of the Sustainable Futures Society, and the author of ten books, including _The New Normal_. Thomas H. Naylor was professor emeritus of economics at Duke University, and a radical thinker who was consulted by governments and major corporations in over 30 countries.

