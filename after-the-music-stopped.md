---
id: 5360d40c6233360007c10000
slug: after-the-music-stopped-en
published_date: 2014-04-29T10:53:36.000+00:00
author: Alan S. Blinder
title: After the Music Stopped
subtitle: The Financial Crisis, the Response, and the Work Ahead
main_color: 6B8C3F
text_color: 587334
---

# After the Music Stopped

_The Financial Crisis, the Response, and the Work Ahead_

**Alan S. Blinder**

_After The Music Stopped_ explains and analyzes the causes of the last decade's great financial crisis. It details the mechanics of the underlying problems as well as the sequence of events as panic began to set in. Finally, it also explains how the US government managed to halt the chaos and rescue the economy.

---
### 1. What’s in it for me: find out how the US government saved you from unemployment and abject poverty. 

Do you remember what watching the news was like back in, say, 2004?

There may have been some gloomy items about terrorism and climate change, but the economy was looking better than ever: jobs were ripe for the taking and that house you'd just bought was getting more valuable by the day!

So what exactly happened in 2007–2008 to transform the economic news into near uniform gloom?

In short: one of the biggest financial crises of all time. In these blinks you'll find out exactly what caused it and what the US government did to rescue the economy from going into a complete tailspin. If they hadn't pulled it off, you wouldn't have needed to worry about bad economic headlines as you wouldn't have been able to afford a computer, television or a newspaper to learn the news.

In these blinks, you'll find out:

  * Why some people thought their house values would quadruple in just seven years.

  * What caused the collapse of a 150-year-old prestigious Wall Street icon.

  * What links banks to Wile E. Coyote, the cartoon coyote who falls off vertical cliff faces.

Let's start off by looking at why the financial crisis happened in the first place.

### 2. After the year 2000, enormous price bubbles developed in the housing and mortgage-bond markets. 

Before the financial crisis first became evident in 2007, buying a house or investing in bonds seemed like a very profitable investment. (In case you're wondering, bonds are financial instruments for which the issuing company pays you an annual fixed rate of interest).

But underneath this positive outlook both markets were in fact in serious trouble.

First of all, a _price_ _bubble_ developed in the housing market, meaning that the prices had risen to wholly unsustainable levels. Indeed, between 1997 and 2006, house prices had risen by an average of 85 percent.

Despite this rise, or perhaps because of it, people kept investing in houses, naively believing that the prices would just keep rising year after year.

This was shown by a survey of Los Angeles' home buyers, many of whom believed that their house values would rise by 22 percent each year, so that if they were to buy a house for $500,000, it would be worth over $3.6 million in ten years. Clearly these expectations were delusional.

Meanwhile, the bond market had also developed a price bubble.

To bounce back from a slump in the economy in 2001, the US Federal Reserve cut the interest rates at which banks could borrow money from each other to just one percent. The idea was that banks themselves would begin paying lower interest on people's bank accounts, thus encouraging people to spend rather than save money. At the same time, the cut was reflected in the interest rate that investors got from buying _government_ _bonds_, so they looked for more profitable investments.

What they found were bonds known as _Mortgage_ _Backed_ _Securitie_ s ( _MBS_ ) — essentially pools of mortgages from which the investor buys a slice as a bond and then earns money from the interest payments homeowners make on their mortgages.

At the time the housing market was booming, so MBSs were highly profitable, and hence desirable. This fuelled a price bubble in the mortgage-bond market too.

### 3. Poor regulation by banks and the government was partially responsible for the housing and bond market bubbles. 

The price bubbles discussed in the previous blink did not just magically appear in the housing and bond markets. Rather, they developed as a result of poor regulation by both the banks and the government.

The housing bubble, for example, was partly caused by banks approving people for mortgages for which they did not qualify. These _subprime_ _mortgages_ were issued to borrowers even when their income levels and credit history should have given lenders cause for concern. But for the banks, the main thing was to sell lots of mortgages to fuel the housing boom.

For example, one married couple in California who picked strawberries for a living and made a combined annual income of $27,000 were given a $720,000 mortgage! It was clear that in the long term most borrowers like them would eventually default on their loans.

As for the bond market bubble, it was partially due to the government's poor understanding and regulation of a new type of financial instrument: the C _redit D_ _efault S_ _wap_ _(CDS)_.

CDSs are basically an insurance policy that an investor can buy against a bond defaulting. For example, if they bought a mortgage bond, and the homeowners defaulted on their mortgages, the bond would default as well.

In this case the investor who bought the bond is normally left with nothing. However, if they have taken out a CDS for the bond, they pay an annual fee to the CDS issuer, but then receive compensation should the bond default. This is analogous to car insurance: you pay a premium, but the insurance company pays you if your car is totaled.

So why was this instrument a problem?

Without proper regulation, CDSs became so popular and widespread that if people had started defaulting on their mortgages en masse, so many bonds would have defaulted that the CDS issuers wouldn't have been able to pay out. And this is what happened to AIG Insurance, one of the biggest CDS-issuing insurance companies in the world: when many bonds defaulted, AIG was essentially bankrupted.

### 4. As the bubbles burst, major financial institutions found themselves in or near bankruptcy. 

In 2007 it was time to pay the Pied Piper: the bubbles began to burst.

In the following months and years, shockwaves traveled throughout the financial system, bringing some of the largest financial institutions in the world to their knees.

Perhaps the biggest shock was the downfall of Lehman Brothers, the fourth largest American investment institution, which had been around since the 1850s.

Lehman Brothers failed primarily because it had invested heavily in subprime mortgages, and was stuck when all these borrowers defaulted on their loans.

In 2008 the company filed for bankruptcy, an event which panicked Wall Street. If a Wall Street icon like Lehman's could fall, who'd be next?

This panic spread throughout the market, and banks simply stopped lending to each other, worrying only about holding on to enough capital themselves. This development put several financial institutions in a precarious position of potentially facing huge losses but being unable to raise funds to cover them.

One such company was the giant AIG Insurance. The company had half a trillion dollars worth of CDSs, and would have to pay out on them should the mortgages underlying them default.

Exactly one day after the Lehman Brothers' bankruptcy, AIG was in trouble: major international _credit_ _rating_ _agencies_ — companies that judge the financial health of other companies — downgraded AIG's _credit_ _grade_. This indicated that it was considered less _solvent_, meaning less capable of paying back its debts.

To prevent this huge company from failing, the Federal Reserve was forced to loan AIG $85 billion. This was done because the government feared that should AIG also collapse, the economy would slide from a mere recession into a fully fledged depression.

Thus it was the taxpayers who ended up rescuing the financial system after the two toxic market bubbles burst.

In the following blinks you'll discover exactly how the US government managed to rescue the financial system and the economy.

### 5. To avoid a depression, the US government invested in the economy’s lynchpins. 

As the financial system collapsed, governments around the world decided to take action.

The US government started the ball rolling by enacting the _Troubled_ _Assets_ _Relief_ _Program_ _(TARP)_ in 2008. The program's main goal was to help revitalize the big banks, because their failure would have affected the entire economy.

TARP helped banks by providing them with capital. This capital injection made them more solvent, so they could better pay their creditors and bills.

The first banks to receive money were the major Wall Street banks like Citigroup, JP Morgan Chase and Bank of America, who received $125 billion in total.

After this, smaller banks got their share too, and overall, nearly $205 billion was allocated to 700 different American banks.

But in addition to this, TARP funds were also used for other purposes.

They were used to guarantee the _assets_ of Citigroup and Bank of America. This meant that the banks' loans and bonds were guaranteed by the US government should they fail, thus assuaging the fears of creditors.

TARP funds were also used to bail out the failing American auto industry, which was on the verge of collapse thanks to high gas prices, superior foreign competition and the recession that followed from the financial crisis.

All in all, TARP is considered by some economists to have been one of the most successful economic policies in the history of the United States. Although it's hard to say what would have happened if TARP had not been enacted, one thing is for sure: a depression was avoided in the years that followed the crisis.

### 6. The US government also increased spending and cut taxes to boost employment. 

After the financial crisis, one of the most immediate concerns on the mind of the American public was the lack of job security. This is because a financial crisis usually causes the economy to contract, resulting in fewer jobs.

The US government resolved to do something about this and enacted the _American_ _Reinvestment_ _and_ _Recovery_ _Act_ (ARRA) in 2009. This fiscal stimulus package bill was meant to inject funds into the economy to boost and stabilize it, and it was one of President Barack Obama's first big moves in office.

In essence, the ARRA used the principles advocated by twentieth-century British economist John Maynard Keynes: One of Keynes' main theories was that the government could increase demand for goods and services in the economy by increasing government spending or decreasing taxation.

For instance, if the government creates a job for you through its spending, you would have more money thanks to your wage. Alternatively, if the government were to decrease taxation, the amount of money you would take home would be higher. In both scenarios, you're likely to spend the excess cash on goods and services.

This in turn stimulates employment, as businesses would need to hire more workers to churn out more goods and services.

To achieve this effect, ARRA comprised both elements: tax cuts and aid to state and local governments to help them keep their taxes low, and increased spending on things like unemployment benefits and work projects to create new jobs.

The ARRA spending continues to this day, but it hit its peak during the second quarter of 2009, at $100 billion per quarter. One report estimated that this translated into a cost of $100,000–$170,000 per new job created.

Whether or not this can be considered a success story is still debated by Republicans and Democrats today, but if anything ARRA showed the government's willingness to support the private sector to save the economy.

### 7. The US government passed new economic legislation to prevent future financial disasters. 

After the financial crisis, the government realized it couldn't just focus on damage control.

Much as doctors aim to prevent diseases rather than just cure them, the US government wanted to safeguard the sickly financial sector, and in 2010 signed the _Dodd-Frank_ _Act_ into law to reform America's economic regulatory policy.

The law aimed to change the way the financial system is regulated by placing limitations to prevent the get-rich-quick behavior that was endemic in leading financial institutions, as well as by ensuring that the government had a more advanced legislative "lookout tower" to detect potential future economic crises.

Some of the other laws in Dodd-Frank aimed to:

  * Prevent taxpayer-funded bailouts in the future by stating that troubled financial institutions would no longer be saved by the government.

  * Regulate the way Wall Street's biggest executives were compensated so that they would not be rewarded for taking huge risks.

  * Create a new regulatory body — The Consumer Financial Protection Bureau — to protect the interests of consumers in the financial system.

Unfortunately, it's too early to gauge the effectiveness of the Dodd-Frank Act, because although it was signed into law in 2010, it hasn't been fully implemented and is still working its way through the machinery of Senate and Congress. This slow roll-out comes because some policy changes can't be pushed too quickly, as drastic changes could once again cause panic and confusion.

Of course, the true effectiveness of the act in preventing or alleviating financial crises won't become apparent until another crisis presents itself.

In the last blink, you'll discover the main lesson learned by governments and private institutions in the aftermath of the mess.

### 8. Stricter government regulation and better banking practices are needed to avoid future financial meltdowns. 

Hopefully, new legislation will help purify the financial system.

First of all, the government must regulate the financial sector more strictly, and indeed this is one of the goals of the Dodd-Frank Act.

One of the causes of the financial crisis was the attempt by some banks to evade the law by, for example, lying about their debts to regulatory authorities. Increased regulation will help prevent this, as well as enabling price bubbles to be spotted sooner.

But secondly, financial institutions must also regulate themselves by instituting better banking practices. Too often they chase growth and profits in booming markets without giving any thought to the risks to which they're exposing themselves.

This foolishness calls to mind the antics of the cartoon character Wile E. Coyote. He's usually in pursuit of the Roadrunner, an ultra-fast bird he wants to eat. In his fervor to catch his prey, Wile E. Coyote often runs so fast he shoots off the road and over a cliff. He doesn't immediately realize this and keeps running in midair without falling. It is only when he glances down and realizes that his feet are no longer on solid ground that he plummets several hundred feet.

This behavior is actually analogous to the way the banks operated during the crisis: they kept chasing after profits that came from sources that were unsustainable in the long term. When they finally realized the problem, it was too late: they had already run off a cliff.

Rather than continue this behavior, financial institutions should tread carefully, working to identify bubbles and avoiding actions that inflate them further.

All in all, while the government has a role to play in regulating the financial system, private financial institutions must also try to ensure that similar financial meltdowns never occur again.

### 9. Final Summary 

The key message in this book:

**The financial crisis stemmed from two toxic price bubbles in the housing market and the mortgage-bond market. When they burst, several major financial institutions were in such dire straits that the US government had to support them. Thankfully, the measures were successful and prevented the economy from sliding into a full-blown depression.**

Actionable advice:

**Think long and hard before taking on a mortgage.**

Many people dream of homeownership, and when large mortgages are easy to get, it often seems one would be foolish not to seize the chance. But before you take on a mortgage, think carefully about the long-term obligation it carries, often lasting decades. Many things can change in that time span. What if you lose your job? What if interest rates quintuple? What if house prices tank and your neighborhood becomes a deserted dust bowl? Aim to get a mortgage that is sustainable even in a worst-case scenario, not one you can barely handle when everything's stable.

**Don't get carried away by the crowd.**

In the early 2000s many people were extolling the virtues of bonds and houses. "Enter either of these markets and it's a sure-fire winner," they said, "the prices will go up and up forever." People believed their optimism and rushed to join in. They put all they had into buying bonds and real estate, thinking that the good times would last forever. It is important that you don't fall into this trap — when lots of people say something is a good idea, don't blindly follow them. Do some research and find out how good it is for yourself.
---

### Alan S. Blinder

Alan S. Blinder is an American economist and professor at Princeton University who has published dozens of works on economics and economic policy. He has served at the US Federal Reserve and was one of President Bill Clinton's economic advisers.

