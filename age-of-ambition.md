---
id: 54cf877c323335000a2f0000
slug: age-of-ambition-en
published_date: 2015-02-03T00:00:00.000+00:00
author: Evan Osnos
title: Age of Ambition
subtitle: Chasing Fortune, Truth and Faith in the New China
main_color: E92F42
text_color: 821A25
---

# Age of Ambition

_Chasing Fortune, Truth and Faith in the New China_

**Evan Osnos**

_Age of Ambition_ offers an insightful look into the many changes that have taken place in China over the past 30 years, transforming the country from a destitute, developing nation into the economic powerhouse it is today. Evan Osnos examines the day-to-day lives of Chinese citizens as they navigate a new social and economic reality.

---
### 1. What’s in it for me? Learn what’s behind China’s recent rise and what it’s doing to Chinese society. 

In recent decades one country has outstripped all others. China — once seen as a rural, poverty-stricken and paranoid nation — has become the world's largest exporter and one of its biggest industrial nations. Its population is becoming richer by the year. What's behind this rapid turn around?

These blinks show you just how China has managed to become one of the world's leading economies. They also show you how this growth is having huge knock-on effects for Chinese society as a whole. Perhaps most importantly, the nation is moving from its traditionally centralized and autocratic roots to a more individualist and open society.

In these blinks you'll discover

  * why the Chinese department which controls media doesn't officially exist;

  * why the Chinese Communist Party employ professional internet trolls; and

  * how Chinese officials use gift shops to funnel their bribes.

### 2. Since the 1980s China’s economy has been growing spectacularly – and continues to expand today. 

The Chinese economy has become an incredible success story: over the course of only a few decades, the country has leapt from poverty to economic domination, and become one of the biggest players in the world — its rapid growth continues to this day.

In fact, over the last 30 years China's economy has grown at a rate of approximately eight percent per year in terms of GDP. In other words, it has doubled in size every seven to eight years!

That makes China the fastest-growing economy worldwide. Between 1989 and 2014, China's annual GDP growth rate averaged an astonishing nine percent, while the United States' grew only three percent during the same period.

Coupled with China's amazing GDP growth is an equally impressive rise in income — one that continues to soar today.

Consider, for example, that in 1978 the average Chinese per capita income was a meager $200 per year. In contrast, the average Chinese citizen today brings home 30 times that amount — around $6,000. What's more, wages, just like China's overall economic conditions, continue to improve.

In addition, China has become increasingly important as an exporter over the years.

Back in 1999, China's exports amounted to less than a third of those of the world's largest exporter, the United States, placing it at number nine on the list of top global exporters. However, a mere decade later China had launched itself into first place.

Clearly, China's economy is booming. That only leaves us asking: Why?

> _"In my years in China . . . airline passengers doubled; cell phone sales tripled; the length of the Beijing subway quadrupled."_

### 3. China’s economic jump-start came from its peasants – not from some clever political plan. 

In the 1950s China was still a very poor country. Even as late as 1979, the People's Republic of China's per capita income was still a mere third of those in sub-Saharan Africa. So what changed? What triggered this enormous growth?

Let's start with what didn't work: when Mao Zedong tried to lift China out of poverty in the late 1950s, he was far from successful.

In 1958 Mao initiated a program called the _Great Leap Forward_, which, among other things, prohibited private farming and attempted to jump-start Chinese industrialization to modernize the economy.

Initially, his plan seemed to work, and China saw production rise. Soon, however, the Great Leap Forward wasn't going forward, but backward: China was hit with both a recession and a terrible famine, killing 30 to 45 million people — more than the combined military and civilian casualties of World War One!

Faced with destitution, desperate farmers turned to other means to survive. In the village of Xiaogang, 18 impoverished farmers made a secret pact to divide the land of the collective between them and cultivate it separately, and promised to protect each other's families in case of arrest.

While farmers were required to hand over a large portion of their harvests to the state, these farmers decided to sell their surplus yield secretly at the market for profit. By the following year they were earning _20 times_ what they had been earning before.

The farmers' scheme served as a model for others and, eventually, the markets began to thrive.

The government discovered the scheme, but allowed it to continue because it was successful. They started gradually expanding it to 800 million Chinese farmers from 1979 onward, and China enjoyed a major boost in GDP.

In the years that followed, China's leader Deng Xiaoping extended the freedom to start private or semi-private businesses to other sectors. As a result, innumerable small enterprises began popping up, and the economy boomed.

> _"Let some people get rich first and gradually all the people should get rich together." - Deng Xiaoping._

### 4. The Chinese people are hungry for success. 

In 1966 Mao Zedong initiated a radical sociopolitical movement, the _Cultural Revolution_, which aimed to create a state based on total equality. The military, for example, eliminated rank, and the Party banned competitive sports in order to imbue their culture with greater solidarity.

However, once the Chinese were allowed to start their own businesses, their ambitions became more apparent. This drive to achieve success is still a characteristic of many Chinese people.

For example, the Chinese have their own equivalent to the American dream, the "bare-handed" fortune. Chinese newspapers are full of stories of people getting rich through nothing more than their own hard work.

One such success story is that of Gong Haiyan. Growing up in a small, remote village with illiterate parents, Haiyan nonetheless received a great education. In 2003, she founded a free online dating site that would later develop into China's largest. By 2011, she had made roughly $80 million.

Unlike Gong, many of these self-made millionaires have started out without even a good education, such as one street food vendor who later became a Chinese fast-food tycoon.

What's more, many Chinese strive for a decent education, if not a prestigious one. Many of the country's nouveau riche, whom urban intellectuals consider to be rubes, came from peasant families who could only provide them with a mediocre education.

They compensate for this by enrolling their children at prestigious American high schools and colleges — a trend which continues to grow. While only 65 Chinese students attended American private high schools in 2005, the number skyrocketed to 7,000 over the next five years!

In addition, the demand for higher education is so great that the government has doubled the number of colleges and high schools in just ten years. Despite this, only one in four aspiring students can hope to matriculate each semester.

### 5. China’s wealth is distributed very unevenly, leading to increasing public discontent. 

Many Chinese biographies read like incarnations of the American dream: with increasing privatization and a booming market, it seems as if everyone could "make it," provided they are willing to put in the hard work.

As it turned out, however, that pervasive sense of endless possibility was overly optimistic.

Despite unprecedented prosperity, the People's Republic is experiencing enormous disparities in income and life expectancy. The gap between China's richest cities and poorest provinces is striking; in fact, it's comparable to the gap between the United States and Ghana.

In terms of raw numbers, the top ten percent of urban Chinese earned 9.2 times the amount earned by the poorest ten percent in 2007. To grasp the severity of this disparity, consider this: if an average Chinese citizen wanted to buy a typical apartment in a Chinese city, they would have to pool their entire income with eight to ten others for a year.

What's more, income disparity has a major effect on the life trajectories of Chinese citizens. We can see this in a study conducted by the scholars Yinqiang Zhang and Tor Eriksson, who discovered that Chinese children's career opportunities depended primarily on their _parents'_ social connections.

In other words, if you're the child of an average worker and your social circle is composed of similar workers, then it is highly likely that, no matter how great your education, you'll struggle to attain social and economic mobility.

In surveys of China's emerging middle class, sociologists have found that citizens are increasingly aware of and unhappy about this lack of equal opportunity. The result is public discontent in the form of protests and even riots.

Between 2004 and 2009 the number of strikes, riots and other forms of rebellion doubled, reaching almost 500 incidents per day.

Clearly, the economic prosperity brought by opening up enterprise has not been spread equally among Chinese citizens. The following blinks will look at how the Chinese state manages the disruption that results from this disparity, and what citizens are doing to fight back.

> _"The longer I lived in China, the more it seemed that people had come to see the economic boom as a train with a limited number of seats."_

### 6. Dissidents and protesters face harsh sanctions. 

China's constitution, like most in the modern world, guarantees its citizens freedom of speech and of the press. Yet the constitution has no legal authority over the Party, which has led to massive civil rights abuses. Here are only a few examples.

Writer and civil rights activist Liu Xiaobo, for instance, was arrested simply for launching a civil rights initiative that aimed to collect signatures for _Charter 08,_ a declaration on civil rights signed by thousands of Chinese intellectuals.

Half a year after his informal arrest in December 2009, he was formally charged with "subversion of state power" and sentenced to 11 years in prison.

Chen Guangcheng is a blind, self-taught lawyer, a strong supporter of women's rights and an advocate for land rights. He, too, was arrested after organizing a class-action lawsuit in 2005 against local authorities who he claimed had excessively enforced the one-child policy. In preparation for the suit, Chen collected the accounts of women who had been forced to abort their child or undergo sterilization against their will.

Following the suit, local authorities placed Chen under house arrest. Six months later, Chen was implausibly charged with "destruction of property" as well as "assembling a crowd to disrupt traffic," and sentenced to four years in prison.

What's more, even participating in a peaceful demonstration can get you into trouble with Chinese authorities.

In 2011, for instance, pro-democracy activists organized the protests called the _Chinese Jasmine Revolution_ via social networks. Small crowds answered the call, gathering in 13 cities across the country and shouting slogans.

The authorities responded by sending in the police to beat and arrest protesters.

The following week, similar protests — this time silent — took place, during which police made arrests, confiscated cameras and beat several foreign journalists, including one American reporter who was dragged by the leg, kicked and punched in the face.

Following these protests, 200 people were questioned or placed under house arrest, and 35 "dissidents" were either sent to prison or simply disappeared.

> _"Can we tolerate this kind of freedom of speech which flagrantly contravenes the principles of our constitution?" - Deng Xiaoping, 1979_

### 7. The Central Publicity Department strives for maximum control of public life in China. 

Many people know that the Chinese media isn't exactly free. But few know about a specific department that heads China's media control system: the _Central Publicity Department_.

This is partly because the Department operates in the background, thus evading public scrutiny.

The Department doesn't appear in public charts outlining the Party structure, and its headquarters has no address. Officially, its central offices don't even exist, despite being located right next to the central government building.

Though it "doesn't exist," the Department is still very much in control of the media and many public events. In 2008 its control extended to over 2,000 newspapers and 8,000 magazines, films and TV programs.

Its power is extensive. For example, chief media representatives throughout China are required to attend a weekly priming session at the Department's headquarters, where they are instructed on how to approach the week's news — what to leave out, what to emphasize, etc.

In 2003, for example, when the SARS epidemic began to develop in Guangdong, local newspaper editors were ordered to publish only reassuring stories about it — no panic, and certainly no criticism of the response.

In the past the Department controlled the largest endowment for the social sciences, enabling it to ban unflattering words like "totalitarianism" from all research concerning the Chinese political system.

It has the power to ban books and films, as well as fire editors. Even beauty pageants and amusement parks are subject to the scrutiny of the Department!

In recent decades the Department has both expanded and grown more sophisticated. By studying the works of American PR specialists like political scientist Harold Lasswell, and those of international firms and foreign governments, they hope to further refine their own strategies.

What's more, the Department has grown so large that experts estimate there to be around one propaganda officer per 100 Chinese citizens today.

> "Keep the masses ignorant and they will follow." Qin Shi Huang, Qin Dynasty ruler."

### 8. The government tries to control the internet, but users nonetheless find ways to exchange ideas. 

The Central Publicity Department gives the Chinese government considerable influence over traditional media, but the internet does not lend itself to such clumsy controls.

Nevertheless, the Party tries to restrict the information and ideas that are exchanged over the internet. Censors block influential international news sources such as the _New York Times_ and Facebook, while the Chinese version of Google, "Baidu," automatically blocks search results that include politically sensitive words.

So if you wanted to access sites that discuss Tibetan independence, freedom of speech or police brutality, for example, you'll find yourself out of luck.

The Department is even directly involved in online political discourse. As a means of quelling the spread of unwanted ideas on the internet, the Party actually pays people to influence the direction that social media discussions take.

These so-called "ushers of public opinion" infiltrate social networks and discussion boards as seemingly ordinary users in order to derail unwanted conversations.

For example, if users are lamenting rising oil prices, an "usher of public opinion" will post a provocative reply, like: "You're too poor to drive a car! Peasants like you shouldn't be on the road anyway!" The idea is to enrage — and thus distract — the other participants.

Yet despite the government's efforts, the internet continues to enable the exchange of ideas and information on an unprecedented scale.

Blocked sites like Facebook, for example, can be unblocked with a little tinkering by users. People can also get around digital filters, e.g., by replacing a Chinese character with another, similar sounding one in their search queries.

The internet also allows people to easily publish texts and photos online without waiting for governmental approval, allowing users to download and share content before censors have the opportunity to block it.

That's exactly what followers of the famous dissident artist Ai Weiwei did with his many blog posts before his blogs were shut down and/or deleted in 2009.

### 9. Today, many Chinese people disclose and discuss government censorship and manipulation. 

With the rise and spread of the internet, ordinary people are turning into media experts who have the tools to identify and expose the Party's propaganda ploys.

For example, whenever photos have been doctored by Party propagandists, bloggers are likely to notice — and to point it out. They're sure to comment on images that have been altered in such a way as to make officials look more physically imposing, for example, or make a crowd appear larger.

Take, for instance, a photo op set up for President Hu Jintao, who was supposedly visiting a low-income family. Bloggers were able to prove that the entire scenario was staged, and that the alleged mother of the family was actually a well-to-do civil servant.

In another case, a blogger revealed that a news report supposedly showing China's newest fighter jet actually showed a snippet from _Top Gun_ with Tom Cruise's character destroying a Soviet MiG!

Once exposed, these attempts at manipulation are met with great outrage and ridicule, which of course doesn't help the Party's credibility.

Furthermore, the internet offers great opportunities to witness and expose censorship.

On the internet, users witness acts of censorship in real time: with a simple click of the "Refresh" button, they see subversive posts manipulated or removed entirely.

The disappearance of posts from best-selling author Han Han's blog, for instance, was witnessed by his followers in real time. Even censorship orders get leaked from time to time, and are then posted on overseas websites where censors can no longer delete them.

Of course, despite all these opportunities to expose government manipulation, criticism of the government remains unsafe. As we saw above, repression and persecution are still a favored tactic for the Chinese state.

> _"All websites are requested to delete...the essay entitled 'Many Massively Corrupt High Officials Receive Stays Of Execution.'" - Leaked censorship order._

### 10. Corruption is flourishing in China. 

The juxtaposition between a modern, booming economy and a repressive government is striking. The reality is, however, that the boom and the authoritarian nature of governance are interrelated: the more the economy grows, the more public servants strive (successfully) to get rich through bribery.

Indeed, corruption began to mushroom as soon as the government began allowing private ownership of land and factories, and opened up the distribution of capital to investors in 1992.

This cracking open of the market meant that officials had plenty of opportunities to accept bribes in exchange for licenses for private enterprise. And there was plenty of money to be made: in the first year of privatization alone, the average bribe tripled from $2,000 to $6,000!

Bribery is hard to prove, however. Still, there are strong indications that public servants receive plenty of backhanders.

Conspicuously, the headquarters of China's mighty planning agency, the National Development and Reform Commission, is surrounded by gift shops. Day in and day out, citizens enter the agency with bags in their hands, only to leave empty-handed later. And when the public servants leave the building, they have bags tucked under their arms.

Similarly curious is the fact that, whenever the National People's Congress is in session, Beijing's high-end boutiques run out of stock — even though, officially, public servants earn an annual income of only $30,000.

Individual politicians have also amassed suspicious amounts of wealth, as was the case with China's former Prime Minister Wen Jiabao.

Wen did not come from a wealthy family. His father was a pig farmer and his mother worked as a teacher. But in the years that he was a high-ranking politician, his family amassed assets worth $2.7 billion — enough to put them on the _Forbes_ list of the richest families worldwide.

Chinese political institutions do their best to control certain aspects of life in China. Despite this, as you'll see in our remaining blinks, the Chinese are nevertheless becoming more and more intent on making their own choices.

> _"In the modern age, corruption and growth have flourished together."_

### 11. In modern China, people are increasingly individualistic. 

Until very recently, the Chinese defined themselves above all else in terms of the groups they belonged to — families, work collectives, etc. We even find this reflected in the lyrics of older popular songs: very rarely would you find the word "I" in place of "we."

But times have changed. China's younger generation is very individualistic. The Chinese call the generations born since the 1980s the _"Me" generation_.

These young people, in contrast to their grandparents, focus on their own experiences and are intent on making their own choices.

We can see this reflected in pop culture — for example, the success of glamorous young writer Han Han's autobiographical novel _Triple Door._ The book focused primarily on his personal experiences as a high school student who suffers from the school's insistence on uniformity, and it hit a nerve: the first 30,000 copies sold out in only three days, and the book went on to sell more than two million copies.

So where does this sudden individualism come from? Likely, it coincides with Chinese citizens' increasing freedom of choice. More people today have access to higher education than ever before, and the internet offers a multitude of options regarding both culture and ideas.

What's more, the Me Generation has benefitted from changes that allow them to start their own enterprises instead of working in a collective.

Social life is also very different for the Me Generation. For example, since the 1980s people have had more choice regarding their life partners.

Traditionally, Chinese marriages were arranged by local matchmakers. Even after Mao banned the practice, marriages were nonetheless arranged by family elders, factory bosses and Communist cadres.

Contrast this with today, when Jiayuan, the giant online dating site founded by Gong Haiyan, had 56 million registered users as of 2011.

Still, this newfound freedom of choice doesn't seem to extend to anything beyond heterosexual marriage: even today 98 percent of _all_ Chinese women eventually marry.

> _"[Individualism is] the heart of the Bourgeois worldview, behavior that benefits oneself at the expense of others." - China's definitive dictionary, The Sea of Words._

### 12. Communism’s destruction of belief systems left a spiritual void that the Chinese are still trying to fill. 

For some Chinese today, a weasel in the house is nothing to fret about. In fact, it's an omen of imminent wealth! For decades, however, admitting to such a belief would have been ill-advised.

This is because, in the past, Chinese communists actively fought religious belief systems.

Before China adopted Communism, several widespread belief systems coexisted: most people were Buddhist (many among them Lamaist), Daoist, or adhered to one of many folk religions.

But in 1966 Mao's Cultural Revolution set out to destroy or forbid old customs and ideas, and especially religious belief systems.

Most forms of religious life were thoroughly stamped out, and religious officials were likewise persecuted. At Mao's command, the Red Guard demolished temples and smashed all sacred objects.

Then, paradoxically, Mao Zedong stepped into the vacuum left behind, and was treated like a god.

The media trumpeted, "Let Mao Zedong's thoughts control everything," and his _Little Red Book_ of quotations was thought to have magical properties. It was this book, for instance, that was said to have enabled surgeons to remove a gigantic tumor in one famous case.

People confessed their sins at the foot of his statues, as if they were shrines. Things took a turn for the strange in 1968, when a Pakistani delegation gave Mao a basket of mangoes, which he then re-gifted to some workers. These workers wept and placed the fruit on altars around which crowds would later gather to worship.

When Mao died in 1976, he left behind another great spiritual void — one which people wanted desperately to fill. Eventually, old temples were once again erected and new ones were built. Now, in today's China, there are a multitude of religions and cults.

This has led some to become rather eclectic in their faith, worshipping, for example, in the Confucian Temple, a Catholic church, _and_ a Lama temple before important exams — just to be sure.

### 13. Final summary 

The key message in this book:

**In today's China, people have lots of opportunity and choice** — **but not when it comes to politics. While the people are adopting individualism as their defining philosophy, the government's authoritarian practices create great tensions.**

**Suggested further reading:** ** _From the Ruins of Empire_** **by Pankaj Mishra**

In _From the Ruins of Empire_, author Pankaj Mishra examines the past 200 years from the perspective of Eastern cultures and how they responded to Western dominance. The book charts in detail the colonial histories of Persia, India, China and Japan in the nineteenth century to the rise of nation-states in the twentieth century. Select stories of cultural figures help to humanize the often violent clashes of cultures, showing the powerful influence of individuals in the course of history.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Evan Osnos

Evan Osnos is an American journalist who has worked both as bureau chief of the _Chicago Tribune_ and as the _New Yorker's_ China correspondent. In addition, he contributed to a series on government safety standards that won a Pulitzer Prize for investigative reporting, and has been awarded two Overseas Press Club prizes. His first book, _Age of Ambition_, won the National Book Award for Nonfiction in 2014.

