---
id: 5a1c1438b238e10006ebb4de
slug: age-of-anger-en
published_date: 2017-12-01T00:00:00.000+00:00
author: Pankaj Mishra
title: Age of Anger
subtitle: A History of the Present
main_color: 917553
text_color: 786145
---

# Age of Anger

_A History of the Present_

**Pankaj Mishra**

_Age of Anger_ (2017) examines the world and the upheaval it's undergoing. These blinks look back to earlier societies and dissect the origins of our current travails. They also pay close attention to the philosophical teachings of the Enlightenment, which still influence Western thought today.

---
### 1. What’s in it for me? Learn why there’s so much anger and despair in the world today. 

Many today are baffled by the state of the world. We ask ourselves how we got here. How did this chaos seemingly rise out of nowhere? What exactly does globalization have to do with it all? If we look closer, however, we'll soon see that there have been various signposts over the last few hundred years indicating the direction we would one day take.

There are many roots to our current predicament. They are as diverse as the Enlightenment's failed promises of a more just society, or the blame inaccurately placed on religion by many in the global political establishment. The following blinks will help you understand just how we got where we are today, and they'll indicate where we can go from here.

In these blinks, you'll learn

  * how Enlightenment concepts of _ressentiment_ and _amour propre_ shaped the world;

  * why liberal capitalism is failing global society; and

  * how French philosopher Rousseau foretold our current predicament.

### 2. Societal upheaval and anger have been present for many centuries. 

Whichever way you look at it, Western society is built on the principles of the Enlightenment. If we're going to understand the problems inherent in the modern world, we're going to need a short history lesson.

The Enlightenment is a shorthand term that refers to the ideas advocated by a group of European philosophers in the eighteenth century. They proclaimed the value of science, reason and art. They wanted humanity to break free from the shackles of religion above all.

They claimed that any individual who pursued these core values, could be both equal to and as influential as every other member of society.

These teachings form the basic principles of modern European society.

When these ideas first circulated, there was much to be excited about. But disappointment soon set in. It was clear that adopting individualist and secular values wasn't enough. Merely embracing these ideas didn't enforce equality across society.

In fact, the increasingly competitive society of the period only succeeded in cementing inequality. If the circulation of these rational Enlightenment principles did anything, it revealed wealth discrepancies and societal unfairness more clearly to more people.

This still holds true today: middle- and working-class people are aware of their plight and remain disillusioned. It's this suffering that makes them volatile.

People are alienated. Their quests for autonomy, power and a voice have failed.

In this atmosphere of disillusion, many have put their faith in strong leaders. From Napoleon to Trump, the phenomenon of the populist messiah is not a new one.

In short, while the concepts behind the Enlightenment are stirring and powerful, the impracticality of implementing them has caused rage and friction to accumulate against the system and the Enlightenment values that underpin it.

### 3. Ressentiment and amour-propre result in an aggressive and individualistic outlook on the world. 

It doesn't take much to spot it. People are seething with resentment at the world around them. They appear to care only for their own advancement, even when others must pay the price.

There's a philosophical term for this sensation of anger and social disenchantment: _ressentiment_.

It was coined by the Danish philosopher Søren Kierkegaard in the nineteenth century. It describes the backlash against society's apparent beneficiaries, particularly when they appear to profit at the expense of the many, while simultaneously evangelizing about how the rest of society should behave.

It's a pertinent idea these days. Antipathy toward journalists, artists and the liberal elite is rife. Now, just as in the eighteenth and nineteenth centuries, people are tired of being told the "right" way to think and act, sick of being scolded for not conforming. This feeling prompts them to lash out at those whom they think are setting society's moral compass.

There's another key concept to consider here too: _amour-propre_. It describes an individual's preoccupation with his own worth and appearance to others.

The idea was formulated by the philosopher Jean-Jacques Rousseau. He envisaged it as one half of an illustrative and contrasting pair. Both _amour-propre_ and _amour de soi_ involve love for oneself. But _amour-propre_ is a vacillating love dependent on the opinions of others.

It's a perfect fit for social media. People are anxious about how they appear to strangers on the internet. Their focus is on individual image presentation and working out what they can get from others.

As a result, people are infused with a combination of selfishness and combativeness. Their self-interest can harm the rest of society, and they might not even be aware of the damage they're doing.

> _A socially insecure as well as economically marginal lower middle class aimed its_ ressentiment _at the liberal elite."_

### 4. Rousseau saw early on the danger posed by Enlightenment philosophy. 

Rousseau never really fitted into the usual mold of an Enlightenment philosopher. He was a somewhat unusual thinker, almost an outsider of sorts.

His approach to free enterprise is a good example of his foresight. While most Enlightenment philosophers celebrated this concept, they did so because they confused it with freedom. Rousseau, on the other hand, recognized the threat of commerce to humankind's psyche.

He understood that competition based on acquiring and losing money damages our egos and can encourage us to act in unaccustomed and even cruel manners.

Not only do those who have nothing in the first place lose out, those who are already rich can also be corrupted by the idea of amassing wealth.

Rousseau's insight here, in fact, ties in with his concept of _amour propre._ He was troubled that people acquired possessions just for the supposed status boost the objects brought.

What also marked Rousseau out was his approach to religion. Unlike the other Enlightenment philosophers, he recognized its value.

The others didn't hold organized religion in high regard, not least because it had spawned many irrational wars like the Crusades. Rousseau's contemporary, Voltaire, who was in many ways the Enlightenment incarnate, became something of a bogey figure for the Catholic Church.

Although Rousseau was not himself religious, he still valued religion as a moral guide for the masses. In contrast, out-of-touch Voltaire just didn't get the simple messages promulgated by the church. After all, he was far too busy hobnobbing with the aristocracy to realize he was patronizing the poor.

Rousseau can, therefore, be seen as a harbinger: he highlighted problems with the Enlightenment's teachings that still linger today.

### 5. Globalization has intensified anger and discontent across the world. 

It was tricky enough to control the anger associated with _ressentiment_ when it was local. Nowadays _ressentiment_ is operating on a global scale.

Globalism has meant that local communities are no longer strongly held together. It seems as though the maxim "every man for himself" has never seemed more relevant. Where once a social apparatus like the church was central, now individuals are lost in a sea of consumerism. The internet hasn't helped either.

As the role of globalization has increased, so the perceived value of national identity has decreased. Consequently, many groups feel forgotten and try to resist. Often old national identities are resurrected with a call to arms. Consider the so-called Islamic State (IS). They have been attempting to establish a nation-state in the Middle East, a region which has certainly seen the worst side effects of globalization.

Unfortunately, when you're filled with _ressentiment_ at globalization, you're not in the best state to make decisions. It's easy to be led astray.

For terrorist groups, it's an opportunity to prey on the young. Whether it's IS or a white supremacist network, these "freedom fighters" give individuals a sense of purpose and identity. Ultimately, we've all been sold the idea that we're each special.

The same set of circumstances nurtures the rise of demagogues. These agitators promise stability in uncertain times. When people are lost or feel let down by the establishment, it's understandable that they put their faith in strongmen, even violent ones. After all, they too have anger to vent.

We're in an unnerving situation. Anger is commonplace, and many regions have become hotspots for resentment and rage. If there's one thing predictable about these aggrieved individuals, it's their unpredictable nature. A global civil war is upon us.

### 6. The future can be brighter, but the West needs to face the facts and change its ways. 

The West is pretty convinced of its analysis of history — and that's a misplaced conviction. Some important factors can't just be ignored. The West can break this cycle of social unrest if it recognizes them.

Firstly, the West can't shrug it off any longer: liberal capitalism has failed.

Capitalism was supposed to make everyone wealthier and more successful if they focused on work and consumerism. But people have just become more individualistic, self-centered and avaricious. When they realize they'll never be satisfied they become disappointed, paranoid or miserable. In these circumstances, ordinary people can commit worrisome acts, sometimes even violent ones.

Nonetheless, much of the West blames religion for the interminable violence in the East. But a closer look reveals that the perpetrators have suffered the same malady as the West: failure by liberal capitalism.

For instance, there's Abu Musab al Zarqawi. He was an Islamist militant who founded the forerunner to IS. But he began as a drug dealer and pimp who'd been failed by the economic system. He only started preaching when he realized that there was a better way to attack those who represented that failure.

Secondly, the West has got to shed the pernicious and divisive Clash of Civilizations theory.

This was the brainchild of American political scientist Samuel P. Huntington. He argued that Islam is an inherently bloodthirsty religion, for which democracy was an anathema.

Consequently, he claimed, as democracy is one of the cornerstones of Western society, Islam is a threat. These naturally violent Muslims were supposedly chomping at the bit to destroy the West.

Needless to say, this kind of attitude only succeeds in fanning the flames of discontent.

So where next? Western politicians, thinkers and intellectuals need to wise up and take responsibility. There's nothing to be gained by defending liberal capitalism and blaming and ostracizing others for its failures and malevolent effects.

### 7. Final summary 

The key message in this book:

**The current state of global upheaval has been long in the making. But there's no need to see it as a mystery. If we look at history and learn about how the Enlightenment has shaped our societal expectations, we can understand just why globalism and the failure of liberal capitalism have wreaked havoc. Forewarned is forearmed. Know your history, and you'll know what to do next.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Orientalism_** **by Edward W. Said**

_Orientalism_ (1978) shines a light on the often unquestioned assumptions about Eastern civilizations that are persistently prevalent in the West. By unearthing and analyzing the West's biases, Edward Said aims to undermine Orientalism's influence on how the West perceives and interacts with the East.
---

### Pankaj Mishra

Pankaj Mishra is an Indian author and essayist whose other works include _From the Ruins of Empire: The Revolt Against the West_ _and the Remaking of Asia_, which became the first book by a non-Western writer to win the Leipzig Book Award for European Understanding. He also contributes to the _New York Times_, the _Guardian_ and the _New Yorker_ as well as numerous other publications.

