---
id: 58f1363086c51e0004dd6411
slug: age-of-discovery-en
published_date: 2017-04-17T00:00:00.000+00:00
author: Ian Goldin, Chris Kutarna
title: Age of Discovery
subtitle: Navigating the Risks and Rewards of Our New Renaissance
main_color: E5523E
text_color: 822F23
---

# Age of Discovery

_Navigating the Risks and Rewards of Our New Renaissance_

**Ian Goldin, Chris Kutarna**

_Age of Discovery_ (2016) looks back at the European Renaissance of 500 years ago to draw parallels with contemporary times. These blinks delve into the good and the bad while providing a deep historical context that has much to teach us about the "New Renaissance" we're currently experiencing.

---
### 1. What’s in it for me? Celebrate the New Renaissance. 

Are you feeling overwhelmed, stressed and disorientated? Maybe that's because we are currently living in one of the most dynamic periods that human civilization has experienced in the past 500 years.

The last of these eras of great economic and cultural expansion was the original Renaissance, which lasted from 1450 to 1550. It was a time of great discoveries and accomplishments, ranging from the development of the printing press to new forms of art that culminated in the masterpieces of Michelangelo and Leonardo Da Vinci. Similar phenomena are happening today, from the development of international trade and exchange at an unprecedented level to the effect of the internet and new media on the exchange of information.

In these blinks, you'll learn

  * how computer chips are changing the world we live in;

  * that you know even more people on Facebook than you think; and

  * why periods of progress are also periods of instability causing radicalism and violence.

### 2. We are living in a modern Renaissance, but it’s not all pretty. 

Just about everyone has heard of Leonardo Da Vinci and Michelangelo. These famous artists created some of history's most iconic masterpieces, and millions stand in line every year to see their work, which are landmarks of the incredible achievements of the Renaissance.

But what exactly was the Renaissance?

It was a period from about 1450 to 1550 during which supreme achievements of scientific and artistic genius were made. But it also had a dark side.

The term Renaissance itself is controversial as it suggests something that is universally good. But in reality, the idea of "Renaissance Europe" was crafted by nineteenth-century European historians as a way to fortify the standing of European nations. This concept of European cultural superiority was then used to justify nineteenth-century European imperialism and colonialism across the globe.

Despite the innovations made during this period by the likes of astronomer Nicolaus Copernicus, theologian Martin Luther and explorer Christopher Columbus, the Renaissance was also a time of great destruction and suffering. Diseases like smallpox were spread across oceans, practically exterminating the Aztecs, Incas and other Native Americans.

Just like the Renaissance of the fifteenth century, our current _New Renaissance_ has also been a time of great growth. The New Renaissance could be said to have started in 1990 with the fall of the Berlin Wall and the end of the Cold War. Commercial internet service began, and China stepped back into the world economy.

The world suddenly felt very different, and historical data backs up that sensation. For example, the _World Trade Organization_ or _WTO_, formed in 1995, is a powerful symbol of international economic advancement, cooperation and a radically different historical period. Today it numbers 161 members, representing every major economy in the world.

However, just like the Renaissance of centuries past, this progress comes with a hefty bill. Just consider the catastrophic toll 30 years of unprecedented growth have had on the environment.

### 3. New technology radically changed the Renaissance world and the same is happening today. 

Our world is practically unrecognizable from the one that existed a mere 25 years ago. Politics, the economy and society have all dramatically transformed, opening up stunning parallels between this modern transformation and the one that took place in the fifteenth century.

For instance, centuries ago, the printing press revolutionized communication over the period of just a single lifetime. It began around 1450 when the German entrepreneur Johann Gutenberg (1395–1468) invented movable type. He used it to make the world's first major printed book, _Gutenberg's Bible_.

Because of this breakthrough, a person born in the 1450s could look back on her fiftieth birthday to see that 15 to 20 million books had been printed during her lifetime. This number easily surpassed the number of books written since ancient Roman times by all European scribes _combined_.

This hypothetical person would even have a difficult time remembering a world without books, although the generation before had relied solely on face-to-face conversations and handwritten manuscripts for communication.

Similarly, the internet has transformed at an incredible speed the way people today interact with one another and the world. Since 1988, when the first intercontinental fiber-optic cables were installed, the number of users connected through this infrastructure has grown more than seven-fold. In 2000 it was 400 million; in 2005, it was one billion, and by 2015 it had reached three billion.

The internet age marked easily the fastest mass adoption of a technology in the whole of human history, and one that radically connected all of humanity.

Just think about it this way: if Facebook were a country, it would have a population of 1.5 billion, making it the largest nation on Earth. What's more remarkable is the fact that two average users on this social network are less than four degrees of separation apart. Most people on Facebook know someone who knows someone who knows one of their friends.

### 4. The New Renaissance has meant better outcomes in health and wealth than ever before in history. 

You can't read the news today without getting a seriously dreary picture of the world. But is it really all doom and gloom?

Actually, today, the quality of global health and wealth are at their highest level ever, even for the most disadvantaged populations on earth. Life expectancy has risen by almost two decades since 1960, climbing from 52 to 71 years.

To put that in perspective, the last 20-year improvement in this metric took 1,000 years to achieve, not 50. As a result, a baby born today in practically any country can expect to live longer than at any previous point in that country's history.

Crushing poverty has also seen a dramatic improvement over the last 25 years. In 1990, something like two billion people were living below the World Bank's international poverty line; by 2015, although the human population had grown by two billion, the total number of people living in extreme poverty on less than $1.25 per day had dropped by over half to just 900 million.

This progress is the result of interconnected economies and improved medical care. The expansion of trade and the jobs it has created have helped raise the incomes of poor people.

Competition has increased, which has, in turn, lowered the prices of goods and services while boosting their quality. As a result, small businesses and households on a budget have been empowered.

During this time, technological advancements and better practices in public sanitation, clean water, hygiene, pest control, vaccines and drugs — along with growing public budgets to support them — have helped combat disease. In 1990, 13 million children under the age of five died from infectious diseases like pneumonia, tuberculosis and measles. In 2015, only 5.9 million children died from the same infections.

### 5. Progress may be rapid, but it’s not equally distributed. 

The incredible accomplishments realized during the first Renaissance makes it seem like a magnificent cornucopia of progress. But we now know that wasn't the case.

In fact, there were clear signs that the progress attained during this period had disparate outcomes. While average welfare rose during much of the period, the wealth gap between rich and poor grew dramatically.

Exact data from the first Renaissance is a bit patchy, but all the information that's available points to a rise in income inequality alongside the expansion of manufacturing and trade. By 1550, in practically every Western European town of any decent size, the top 5 to 10 percent of residents owned between 40 and 50 percent of the town's total wealth, while the bottom half owned little more than their own labor power.

This phenomenon was primarily precipitated by falling wages on the lower half of the income scale. Such cuts were felt particularly acutely by women. Between 1480 and 1562, a nanny's wages did not increase at all, yet the cost of her daily necessities rose by 150 percent.

Outside Europe, matters were far worse. Some 150,000 Africans were enslaved between 1450 and 1550, and the European Age of Discovery meant the total destruction of entire civilizations in the Americas.

Unsurprisingly, a polarity between rich and poor is also central to our current Renaissance. While average global welfare is rising, the extremes have grown even more distant.

In 2010, the 388 richest people in the world controlled more wealth than the poorest 50 percent of the population. By 2015, this number dropped dramatically as just 62 people controlled more wealth than the bottom half.

Meanwhile, the bottom 50 percent of society — some 3.6 billion people globally — subsist on an average of just a few dollars per day.

### 6. The progress of the Renaissance caused serious problems – which we’re also facing today. 

In 1495, Italians began to experience horrific symptoms caused by a mystery disease. The victims of this ailment spat up blood and suffered from boils the size of breakfast rolls, which oozed dark green pus. The disease left people sick for months and sometimes years.

What was it?

Well, disease spread rampantly during the Renaissance, and we now know this particular affliction as syphilis. Within four years of its emergence, this sexually transmitted disease had spread all over Europe and was a global menace a mere five years later.

The deadly advance of syphilis was facilitated by populations concentrated in urban centers. This urbanization created mutual interdependence, which did wonders for creativity and cultural exchange but also produced ideal conditions for the spread of disease.

Given the similarities between then and now, you might have guessed that the modern world's concentrated yet fluid populations produce comparable risks, not just for health, but also in other aspects of life. The massive rise in air travel neatly mimics the population exchange and economic linkages that enabled syphilis to spread throughout Europe and across the sea.

A good example is the Ebola epidemic that broke out in December 2013 in West Africa. By March 2014, it had claimed 60 lives in Liberia and Sierra Leone; by mid-2015, over 28,000 cases had been reported, along with 11,300 deaths.

And this was no accident. Such massive pitfalls appear regularly as side-effects of positive phenomena like rising global financial investments and the construction of the incredible infrastructure behind the internet. In fact, the interconnectivity of the modern world means that when something happens far away, it's likely to rapidly become a problem at home.

Just consider the 2008 financial crisis, which began in the United States but swiftly became a global issue. Or take cyber attacks: these data crimes can be committed anywhere in the world and prey on anybody, anywhere.

### 7. Fear and doubt sparked by rapid changes fueled radicalism in the Renaissance, as they do today. 

As the world of the first Renaissance evolved at an astonishing rate, people often experienced fear, doubt and insecurities. This climate proved fertile ground for radical messages proclaiming doom and a need for extreme action.

Just take Girolamo Savonarola, a Dominican friar who, in 1497, incited his fanatical followers to gather every piece of evidence they could find of their strange new age. They hunted out immoral books, nude paintings and sculptures, indecent perfumes, heretical texts and exotic musical instruments.

They piled everything up in Florence's central piazza and set it ablaze in what has become known as the Bonfire of the Vanities.

Within a year of this spectacle, the friar became the most powerful figure in Florentine politics by launching a public campaign of moral purification that called for draconian laws prohibiting vice. His rapid ascent to power was mostly a product of good timing. After all, he was preaching his apocalyptic message as the year 1500 drew near — a year many Christians believed would bring the Last Judgment.

Today radical messages of hate and extremism also pose a threat. Just take the rise of Islamic State, fringe Christian denominations that preach Islamophobia and homophobia, Orthodox Israeli Jews who assault gay people at pride parades and conceive plots to bomb Arab girls' schools and, of course, the resurgence of neo-Nazi groups.

All of these hate groups are examples of what can happen in a rapidly changing world marked by deepening inequality and general disorientation.

So, the Renaissance wasn't all great back then and it's not all great now. But does that mean the future is bleak? As you'll find out in the next blink, we have good reason to think the opposite.

### 8. Although they have downsides, innovation and genius can pave the way to a better future. 

This comparison of ugly histories and current situations can be a bit frightening, but it's essential to understand that even at times when the world as we know it might vanish down the drain, there's still potential for tremendous good.

Despite all the worrying developments, there's also great progress being made today. While extremism and fear make it easy to be skeptical about the role of scientific and artistic advances in transforming the world, there's good reason to believe in them.

However, it's difficult to measure this type of progress as the gains produced by acts of genius are often unquantifiable. The strength of innovations in the creation of wealth, health, art and justice is hard to measure in numbers. However, new technologies are still dramatically transforming the way we understand the world.

Just remember, a mere 20 years ago, we thought Earth-like planets were rare. Today, because of better telescopes, computers and the thousands of astronomers who are linked together through the internet, we know that the Milky Way alone holds at least 10 billion other planets that are of the appropriate size, temperature and orbit to support life. This discovery has upgraded the possibility of alien life from remote to virtually certain.

Or consider other intangible goods like those in the digital realm. If one million copies of the Encyclopaedia Britannica were sold for $1,000 each, it would add $1 billion to GDP. Contrast that with the fact that when one million users log on to Wikipedia, no money is produced — although immeasurable good is still provided.

Thanks to phenomena like Wikipedia, humanity is much better off in terms of time and money saved, even though GDP isn't directly affected. And just about anyone can attest to the tremendous wealth of education and entertainment that can be gleaned from the internet without spending a penny.

### 9. Final summary 

The key message in this book:

**The first Renaissance was a period of tremendous creativity and innovation, but also one of profound destruction and suffering. Today's world is in many ways similar to the Renaissance of 500 years ago, and a historical perspective can shine a light on its intricacies.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Where Good Ideas Come From_** **by Steven Johnson**

_Where Good Ideas Come From_ examines the evolution of life on Earth and the history of science. This _New York Times_ bestseller highlights many parallels between the two, ranging from carbon atoms forming the very first building blocks of life to cities and the World Wide Web fostering great innovations and discoveries.

In addition to presenting this extensive analysis, replete with anecdotes and scientific evidence, Johnson also considers how individual and organizational creativity can be cultivated.
---

### Ian Goldin, Chris Kutarna

Ian Goldin is Professor of Globalization and Development at the University of Oxford. He was formerly the vice president of the World Bank, chief executive of The Development Bank of Southern Africa and an adviser to President Nelson Mandela.

Chris Kutarna is a fellow at the Oxford Martin School, specializing in international politics and economics. He previously worked as a strategy consultant at the Boston Consulting Group and currently advises senior executives in Asia, North America and Europe.

