---
id: 590343c5b238e10008b85639
slug: age-of-propaganda-en
published_date: 2017-05-01T00:00:00.000+00:00
author: Anthony Pratkanis and Elliot Aronson
title: Age of Propaganda
subtitle: The Everyday Use and Abuse of Persuasion
main_color: D8A868
text_color: 735937
---

# Age of Propaganda

_The Everyday Use and Abuse of Persuasion_

**Anthony Pratkanis and Elliot Aronson**

_Age of Propaganda_ (2001) is an in-depth look into the world of deception that is propaganda. These blinks will walk you through the different techniques propagandists rely on to successfully change people's opinions and show how these tactics have become part of your everyday life.

---
### 1. What’s in it for me? Learn to spot and fight propaganda. 

With the exception of dictators and authoritarian leaders whose every whim becomes law, those in power usually have to use alternative methods — namely persuasion and propaganda — to achieve their aims and bring the general public on board. By carefully manipulating our view of the world, people in positions of power strive to make us act the way they want us to, often without us being aware of it at all.

But persuasion and propaganda are not limited to the realm of politics; in fact, we're subject to propaganda from companies and other organizations every day, most of which is designed to, say, make us buy things against our better judgment or lure us into shady cults.

The good news is we can learn to see this propaganda for what it is: hot air. By exposing the techniques and strategies of the propagandists, we can create a better society and make better choices.

In these blinks, you'll learn

  * the difference between persuasion and propaganda;

  * why Rush Limbaugh calls his supporters "dittoheads;" and

  * how to protect your kids from propaganda.

### 2. Persuasion is built on rational thought and the weighing of different perspectives. 

People are constantly trying to sway others' decisions in one direction or another. But this isn't always as nefarious as it sounds; by using persuasive techniques to influence decision making, these people are offering us the opportunity to make informed choices grounded in facts.

Such persuaders intend to offer people enough information on the issue at hand to allow them to make a rational choice. A typical persuasion technique is to state both an argument and a counterargument, only to immediately disprove the counterargument based on facts to support one's case.

In this way, persuasion is often a result of long discussions in which one person's opinion actively transforms as a result of another person making a stronger case for his perspective. It's clear to the persuaded person that his opinion has been changed and he's comfortable with that.

This manner of argumentation is based on the _central route_ of information processing, which transfers information through detailed argumentation and is crucial to successful persuasion.

People who are persuaded centrally are ready to receive information-dense messages. They're not interested in any old information, but would rather take their time weighing the strengths and weaknesses of different positions.

Because of this focus, such people are fully concentrated on the message they're discussing, and they thus devote all their mental faculties to understanding the message, as well as their own opinions on the issue. When arguments are made in this way, people are capable of making educated decisions by assessing information from different sources.

However, not all arguments are made in such a straightforward way. In the following blinks, you'll learn about another widely used technique of persuasion that's not nearly as fair; it's called propaganda and you can find it just about everywhere you look.

### 3. Propaganda confuses its message in order to disseminate information without people realizing it. 

Unlike persuasion, propaganda doesn't intend for its targets to have a fair chance of holding onto their own opinions. Instead, it works to catch them off guard, influencing them without them even realizing it.

To do so, propagandists deliver their messages in attractive packages, preventing consumers from focusing on what's actually being said. This practice is accomplished through positive language and the framing of information in an appealing way in order to distract consumers from contemplating the factuality of the statements.

For instance, consumers are much more likely to buy beef that's labeled 75 percent lean ground beef, than the same product labeled 25 percent fat. Similarly, gas stations advertise a discount on cash purchases, when customers are actually just avoiding the credit card surcharge.

While persuasion relies on the central route of information transfer, propaganda uses the _peripheral route_, which relies on a distracted consumer not being able to concentrate on the real message he's being fed. Just consider advertisers selling a product; the reasons they provide as to why a customer should buy something can rarely stand up to much scrutiny.

To get around this, they flood the customer's senses with as many things as possible, from music to rapidly changing scenes and loads of color. With so much going on at once, a person watching the commercial can't actually focus on the quality of the information she's taking in.

This strategy makes these ads effective, even if people only half-watch commercials during a break from their favorite shows. While they might expect that failing to concentrate on an ad will make them impervious to its message, they're still just as likely to remember a catchy jingle that reminds them to buy a product the next time they're at the store. Because of this, a person might pick up one brand over another, without having any rational reason for doing so.

After all, failing to concentrate on listening also means that people aren't concentrating on _not_ listening. This failure means that all kinds of things get through to their subconscious that they wouldn't expect.

### 4. The credibility of a source and the message it conveys are both crucial to successful propaganda. 

So, propaganda is all about misleading people, and while it comes in many forms, it's always based on the _four stratagems of influence_. Let's take a look at the first two: _source credibility_ and _message_.

Source credibility means that the person receiving the message will be inclined to trust the person delivering it. Propagandists carefully select people to deliver their messages as a way of dazzling their audiences and forcing them to focus on the physical appearance of the source, rather than the words being delivered. A classic trick is to use a much-adored, respected or trusted public figure who appeals to the audience.

For instance, famous and well-liked athletes have long been used to sell breakfast cereal. They're naturally concerned with eating right to boost their athletic performance, and the implication is that if they eat a particular breakfast cereal, you should too.

In this way, the endorsement of an athlete convinces people to buy cereal without checking how healthy it actually is.

In the second strategy used by propagandists, the _message_ often misleads people on purpose. After all, propagandists don't care if the product they're pushing is actually the best one available, they just want people to think it is.

To accomplish this goal, they say things that initially sound like strong positive statements but are actually just an intelligent way of dressing up a mediocre truth. Consider aspirin commercials; a company that sells aspirin might say that no other brand of aspirin works faster than theirs, but fail to mention that no other brand works any slower, either.

By making positive affirmations like this, people are misled into believing that such a product is better than all the others. This, in turn, means they're willing to pay more for one brand when it's actually identical to the rest.

### 5. Propagandists set us up to side with them and use our emotions to guide our decisions. 

You just learned how propagandists use source credibility and misleading messages — now it's time to take a look at the other two stratagems of influence: _prepersuasion_ and _emotions_.

The first, prepersuasion, is a way of creating a vulnerable mindset in the target. For instance, the incredible levels of violence depicted on TV don't reflect the world at large; crimes are ten times less likely to happen in real life than they are on TV.

Nonetheless, politicians work to push the news toward crime stories as a way of building public support for projects like the war on drugs and to distract from economic issues that are a more credible threat to working-class people. Meanwhile, this strategy makes these politicians popular as they implement programs to crack down on drug crime and make neighborhoods "safer."

Another good example is gun companies. They're much more likely to sell guns to people when firearms are promoted as a tool for defending yourself against the dangerous world people see depicted in the mass media.

Prepersuasion is powerful tool, but so is the fourth stratagem, emotions. When people are emotional, they often make decisions that will ease their pain without properly considering their actual consequences.

For example, an experiment conducted by Merrill Carlsmith and Alan Gross had certain subjects deliver electric shocks to others sitting in another room when they answered questions incorrectly. Other participants were told to simply press a buzzer when another person gave a wrong answer.

After this exercise, the volunteers who got buzzed or received shocks — who, unbeknownst to those pressing the buttons, weren't actually getting shocked — asked the people pushing the buttons to make calls to garner support to "Save the Redwood Forest." The participants who thought that they had been shocking the others were three times more likely to step up and make these calls.

### 6. The entertainment value of the mass media increases our vulnerability to propaganda. 

It's no secret that the modern world is a message-rich environment. Every day we're told all kinds of different things, but few of these messages come with many details or explanations.

Such an overload of generalized information only further increases our predisposition toward laziness and exacerbates our failure to properly consider what we're seeing and hearing. In this climate, many people become addicted to entertainment, an addiction that the mass media is perfectly positioned to cater to.

In fact, we're so used to being entertained that even the news has become a form of entertainment. The more "boring" news stories, ones that go into detail about political policy or economic processes, are swept aside for more exciting reports on terrorism, murder and the extramarital affairs of public figures.

Because of the overload of senseless entertainment we experience, most people are out of practice when it comes to hearing and dissecting detailed arguments. Not only that, most of us, when faced with serious news coverage, would rather switch channels than deal with an often troubling reality.

Since most people won't watch or read things that demand any mental effort, the messages we receive are grossly oversimplified. For instance, political messages get cut down to sound bites — bits of language that sound powerful, but lack any real purpose or meaning.

The general failure of the public to sit and listen to explanations means that politicians are rarely forced to explain, in any sufficient detail, just what these chopped up messages mean and how they intend to put their ideas into action. Just consider former US president Richard Nixon: over the course of his 1968 campaign for president, he said that he would win "honorable peace" through the Vietnam War.

But what exactly does that mean?

An "honorable peace" could be all manner of things depending on who you ask, and Nixon never had to elaborate on his vision. As a result, two people with completely different ideologies and interpretations of Nixon's message might have voted for him, as they both thought he was speaking on the behalf of each.

### 7. Our need to rationalize our behavior and be socially accepted are turned against us by propagandists. 

Humans are naturally social and rational creatures, and propagandists know just how to take full advantage of this. The reality of human tendencies can often trap people in a vicious cycle, as we're likely to prolong bad decisions as a means of rationalizing our behavior and saving face.

Take smokers, who are _huge_ rationalizers. Lots of people who attempt to give up smoking end up failing because they come up with infinite reasons why they should _continue_ their unhealthy habit. These people tell themselves they have to smoke because all their friends do, or that they'd rather live a short and happy life than a long, miserable one.

Because of all the work smokers do to convince themselves to keep puffing away, the work of the tobacco companies becomes much easier. All big tobacco has to do is make sure people get hooked, and the customers take it from there.

But propagandists also take full advantage of the social character of humans by using something called the _granfalloon technique_, a way of grouping people together while excluding others to produce a sense of camaraderie for some — and isolation for others. A prominent example of this strategy comes from the US radio personality Rush Limbaugh.

This icon of conservative talk radio is well known for his extremist Republican views and is a master of the granfalloon. He calls his supporters _dittoheads_, referring to the fact that they agree with everything that comes out of his mouth, and contrasts them with other outside groups that he insults and berates.

For instance, he calls liberals stupid and claims that essentially all racial minorities are criminals. To avoid being assigned an undesirable label, the dittoheads become proud members of their group, viciously defending it against all attacks.

This group identity, just like rationalization, plays on the human fear of loneliness and the terror of being wrong to make people do and think whatever Limbaugh wants.

### 8. War makes great use of propaganda. 

One specific use for the tools of propaganda is war. In fact, some of the biggest beneficiaries of propaganda are politicians who use misleading information to build public support for brutal military campaigns. They rely on public backing to maintain societal stability and convince soldiers to go into battle.

War is thus a great example of how propaganda, rationalization and fear work in tandem to influence the public. For instance, the US invasion of Iraq was sold to the American people as a necessary, rational action to depose Saddam Hussein, a Hitler-like dictator.

Naturally, this message engendered fear that, if the United States didn't invade, Hussein would eventually pose a direct threat to the country. Beyond that, the public was manipulated into seeing itself as supporting the Iraqi people by freeing them of this menace.

Such tactics were used to brush aside thousands of Iraqi civilian casualties and quash criticism of US military action. Americans were so terrified that they rationalized any decision that diminished their fear and actually thought the United States was helping Iraqis by killing them.

When it comes to granfallooning, which defines a clear enemy and strengthens the public resolve to fight it, there's perhaps no better example than the Nazis. They used this technique against Jews during World War II by describing them as rich money-grabbers.

By comparing the blonde hair and blue eyes of Aryans to the stereotypes of dark hair and big noses used to characterize Jews, the Nazis made it easy to pick out, and pick on, the elements of society that they deemed undesirable.

Finally, the Vietnam War is another perfect example of extreme rationalization. This catastrophe cost thousands of American lives, mostly because pulling out of the war would have implied the US military admitting that it had been wrong. As a result, the strongest military power on earth acted completely irrationally.

The objective of the war became increasingly vague as time went on, so much so that the final objective was to "win at any cost," just to prove that it hadn't all been for nothing.

### 9. Cults use propagandistic techniques to gain members. 

People tend to think that cults brainwash people through some sort of witchcraft, but the fact of the matter is that cults use exactly the same techniques as any other propagandistic group — just with different names.

For cults, the name of the game is _reciprocity_, _distraction_ and _self-sell_. The first of these, reciprocity, is a powerful tool since humans are inclined to reciprocate acts of kindness they're shown, even something as small as the gift of a flower, which Hare Krishnas hand out to entice new devotees. Such gifts increase your likelihood of engaging with such people, as you feel like you owe them something.

Then, once you agree to give a cult the time of day, they use point number two: distraction to obscure their true intentions. To do so, they might encourage you to join in some light-hearted singing that takes the focus off the message, or constantly stay by your side, never giving you time alone to consider what's happening.

Once a new recruit is securely hooked in this way, they're encouraged to go find other potential followers using the same values and benefits of joining, over and over again. This is point number three, the self-sell, which serves a second function of cementing the cult mindset in the new devotee.

Another strategy employed by cults is to isolate their members from outside influences while promoting complete dependence on the cult's leader. Followers are discouraged or forbidden from seeing family members and anybody else that might turn them away from the cult. In a great example of the granfalloon technique, they're even taught to see outsiders as evil, and think of themselves as enlightened.

To accomplish this, cult leaders tend to be charismatic communicators who can list off multiple reasons for their superiority and come up with plenty of explanations as to why people should be devoted to them.

In the end, members find themselves trapped in a rationalization spiral that produces more and more extreme actions to justify their decisions. Such entrapment can even result in death, as in the case of the mass suicide pacts engaged in by the members of some cults.

### 10. By understanding propaganda, you can fight it. 

It's easy to get frustrated when you consider the sheer volume of propaganda people are exposed to on a daily basis from politicians, advertisers and the mass media. The disillusionment that this climate fosters also breeds complacency, as people tend to think that propaganda won't affect them. But there are concrete ways that you can make positive changes to this situation.

For one, you can educate yourself and your children about how propaganda functions. This is a vital step, as children are commonly targeted by propaganda in the form of commercials for toys and fast food. These ads play on repeat during Saturday morning cartoons and during the commercial breaks for educational programming in schools.

Such propaganda discourages kids from becoming individuals who will abide by the central persuasion path of argument and analysis. It deters them from considering the quality of a message itself, and they end up becoming peripherally persuaded from a very young age.

As such, kids should be challenged about why they think, a new toy will make them happier. By posing such questions, you can teach your kids to think on a deeper level that counters the aims of advertisers.

Another powerful strategy for combating propaganda is to personally challenge politicians and companies on the claims they make. Lots of people want to simply opt out of politics and refuse to vote because they don't agree with the system.

But there is another way; you can write to politicians and force them to support their claims with facts. You can do the same thing with media outlets and demand that they offer in-depth coverage of an important issue. If these people and companies know that they'll only persuade people through facts and honesty, they'll have to change their strategies.

You can also write letters to companies, questioning their claims about their products. The way they respond can be very telling — did they actually answer your question or just send you more advertising materials?

If you don't get a satisfactory answer, it may be time to take your business elsewhere.

### 11. Final summary 

The key message in this book:

**Propaganda is all around us, convincing us to spend money on things we don't want, vote for politicians who don't care about us and convince our kids that they need toys that aren't worth the plastic they're made of. Fortunately, we can combat this deception by identifying propaganda's central strategies and understanding how it works.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Mistakes Were Made (But Not by Me)_** **by Carol Tavris and Elliot Aronson**

Through studies and anecdotes, these blinks explain why, when we make mistakes, we often come up with self-justifications instead of admitting the mistakes to ourselves. It also shows how detrimental these self-justifications can be to personal relationships, medicinal care, the justice system and even international relations.
---

### Anthony Pratkanis and Elliot Aronson

Anthony Pratkanis is a professor at the University of California, Santa Cruz as well as an advertising and political consultant. He is a coauthor of _Weapons of Fraud: A Source Book for Fraud Fighters_, among other titles.

Elliot Aronson is one of the 100 preeminent psychologists of the twentieth century and the recipient of many awards, including the William James Award for Lifetime Achievement, awarded by the Association for Psychological Science.

