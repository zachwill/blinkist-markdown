---
id: 54734ba736356100098e0000
slug: agile-selling-en
published_date: 2014-11-26T00:00:00.000+00:00
author: Jill Konrath
title: Agile Selling
subtitle: Getting Up to Speed Quickly in Today's Ever-Changing Sales World
main_color: 2A95D4
text_color: 2071A1
---

# Agile Selling

_Getting Up to Speed Quickly in Today's Ever-Changing Sales World_

**Jill Konrath**

In _Agile Selling,_ you'll discover how to boost your productivity and learn to thrive in any kind of sales environment. The book explores the strategies of great salespeople and how they adapt to new circumstances, and offers practical advice on how you too can become more adaptive in your business.

---
### 1. What’s in it for me? To win more sales, you have to learn how to learn. 

The one thing about sales and selling is that it doesn't ever get easier.

Even with plenty of experience, you might feel that you've finally got the knack, only to have the market and environment change in front of your eyes.

This is why both young _and_ experienced salespeople often struggle — the art of selling is always changing. Entry-level salespeople labor to learn their products and gather clients, while experienced sellers can trip over updated company strategy or the introduction of a new product.

In these blinks, you'll learn how to roll with the punches in an ever-changing sales environment, through knowing the secret of all great salespeople: _agile learning_. By becoming an agile learner, you too can transform yourself into that great salesperson you've always wanted to be.

In the following blinks, you'll also discover:

  * how sitting down with the company CEO might actually lose you a sale;

  * why optimists sell more than pessimists; and

  * why role-playing games aren't just for fans of Harry Potter.

### 2. To succeed as a salesperson, you need to be adaptable to change. 

No matter how innovative a new product might be, it still needs to be sold. And that's where salespeople come in.

But unfortunately, selling isn't so simple, as the world of sales is constantly changing.

Sales teams constantly need to be on the lookout for more effective ways to sell their products. That might mean setting new strategies or embracing new technology (like software).

What's more, salespeople have to be nimble and adaptable to succeed. Think of it this way: Techniques that effectively sell coffee machines to housewives probably won't sell smartphones to large retailers.

And there's another important element of sales that constantly changes — the people you sell to. You need to know that buyers tend to become savvier and smarter over time.

In fact, buyers have changed more than ever in recent years. Today no one needs a salesperson to rattle off a list of product features, for example; a buyer can simply find the details online.

But this doesn't mean that salespeople are irrelevant. Rather, a 2011 study found that 53 percent of consumers care more about the sales experience than the price — or even the product itself!

So how can you as a salesperson deliver an experience that a buyer wants? By putting yourself in the shoes of a customer to explain why your product is a perfect fit.

Yet this kind of personalized sales experience is particularly difficult to deliver when everything around you is changing constantly. That's why to succeed, you need to become an _agile learner_.

_Agile learning_ is the ability to easily adapt to new circumstances by quickly absorbing the knowledge and skills that are relevant to each individual situation.

For example, when your biggest client gets a new CEO, perhaps your sales goals will change as a result. Roll with the change and take advantage of this opportunity to learn something new, gain experience and become a better seller!

> Fact: 53 percent of people care more about a sales experience than about a product.

### 3. To better manage the demands of selling, let go of negativity and embrace a new perspective. 

So how does an agile learner and salesperson actually go about learning?

Well, it's not just a matter of absorbing information. It's rather a mind-set that involves letting go of negative emotions to stay motivated.

When it comes to learning, many of us face internal obstacles, such as uncertainty and doubt. These kinds of negative emotions can wreak havoc on your motivation. In some cases, having a negative attitude could cause you to quit a difficult task altogether, for fear of failure.

The author herself experienced a career crisis. She was convinced that her sales tactics were effective, but she kept failing to close deals with big companies. But she didn't allow these failures to hold her back; instead, she re-evaluated her sales methods. Pretty soon she started closing deals again — and eventually wrote a book about the whole experience!

It makes sense to follow the author's example and use your own failures as a way to evaluate your goals and methods. While failure might be inevitable, it needn't hold you back.

Failure can even be productive. The author realized this when she fumbled a relationship with a prospective client, going above his head to the company's CEO. Her prospect was upset by this slight so he cancelled the entire deal. While the author was disappointed, she used the experience as a learning opportunity. Today, she knows how to better value her established contacts.

And even if you aren't facing a specific obstacle, you can still set small goals to motivate yourself to improve.

Most people set performance goals for themselves. For example, you might say, "I want to close 25 deals this month." But instead of specific goals with a particular outcome, set "getting better" goals. Tell yourself, "I closed 15 deals last month. This month, I want to close 25, because that will indicate that I'm improving."

By now, you're probably convinced that you need to find a way to learn new things constantly to succeed in sales. Read on to find out exactly what you ought to be learning.

### 4. Learning efficiently is more important than learning everything all at once. Make mental folders! 

Every time you start a new job, you have to learn lots of new things.

A young salesman needs to know all his company's services, pricing structures, market competition and so on. But he couldn't possibly master all this information in one night!

Whether a change is minor (like when a client expands a product line) or major (like when you enter a new industry), you don't always have enough time to acclimatize yourself to the new situation and learn everything you need to immediately.

And that's why you have to know how to make the best of the time you have. You can simplify the learning process using a few key strategies.

One such strategy is to organize everything you need to learn into _mental folders_. These are blocks of information, each folder on one subject, that you can access quickly.

For example, it isn't helpful to think about target markets when you're improving your selling skills. That's why you should file "target markets" into the "products" section of your brain — away from the "sales" folder that houses your selling skills.

Mental folders also ensure that you're learning in the right order. After all, it doesn't make any sense to work on your selling skills if you have no idea what products you'll actually be selling.

Another strategy to simplify learning is to make information more memorable by connecting it with things you already know.

And a final strategy is to _imagine yourself as the buyer._ Think about your client's thought process. Consider these questions: How will buying from you make a difference? What events might trigger someone to buy your products?

This kind of creative visualization will help you learn how to be an effective seller.

### 5. Since you can’t learn everything at once, find ways to prioritize to get up to speed quickly. 

What's the best way to adapt to a new situation quickly?

It's easy: take it step by step.

To this end, it helps to achieve a small success soon after you begin a new job. Otherwise, you might become frustrated and feel like quitting.

Although we learn from failure, failing at the very start of a new endeavor can lead to feelings of self-doubt. You might even start thinking of yourself as a loser and hating your job.

Yet the problem isn't with you. Lee Salz, an expert in sales management, says that even experienced sellers need at least eight months when starting a new job to perform on the same level as people who are already accustomed to the sales environment.

So to gain confidence and start off on the right foot, begin by concentrating on details you absolutely need to know to gain _situational credibility_.

Situational credibility is the ability to have a conversation with other people working in your industry without sounding completely oblivious.

Because as we mentioned earlier, you can't learn everything at once. So prioritize! Concentrate on these three absolute need-to-knows, which apply to every field.

  1. _Insider language_ : Take notes whenever someone in your field uses a word you aren't familiar with. Learning the jargon will help you sound like a member of the group quickly.

  2. _Your buyers_ : Figure out who's responsible for buying. Is it usually the CEO, or her secretary? What's most important to these people?

  3. _The status quo_ : Find out how buyers are doing without you. Which products and services are they using, instead of what you're offering? What value do you offer?

As you can see, learning plays a huge role in a salesperson's success. And yet it isn't everything. To really succeed at your job, you need a solid foundation of selling skills.

### 6. Successful sellers thoroughly prepare for everything that could possibly go wrong in a meeting. 

Once you catch up to your colleagues and learn tons of new information, how can you use it to seal a deal?

Excellence in sales demands thorough preparation. Not everyone realizes this: Some people think they can simply charm their way into closing a deal. But no matter how charming you are, potential customers still won't buy from you unless you've done your research.

For example, although the author has many years of sales experience, she almost blew a meeting with a big firm. After role-playing with a colleague, she realized that she wasn't ready for that conversation. She couldn't deliver a structured presentation about the product and stumbled to answer even basic questions.

Once she realized that she didn't have a good plan for the meeting, the author started preparing more seriously. She realized that no amount of skills or experience could eliminate the need for preparation.

To that end, there are a few different techniques you can use to prepare before going into a meeting.

For example, try role-playing the conversation with a colleague. You might even want to capture the conversation on video, so you can better evaluate your performance.

It's also helpful to learn from others. Just observing seasoned, expert sellers in conversation with a prospect might produce useful insights you can apply in your own work.

Another way to prepare for a meeting is to create a plan of the flow of the conversation, with "recovery strategies" that will allow you to get back to the script if anything goes wrong.

The point is, be prepared for anything — that's what helps prevent bad surprises. Basically, you should expect that everything that could potentially go wrong _will_ go wrong.

Ask yourself: what's the most difficult or inconvenient question you could be asked? What will you do if your computer doesn't start up? Prepare for _everything_!

> Fact: The best sellers spend lots more time preparing for meetings than do less-successful colleagues.

### 7. Selling is all about personalizing your pitch, and that’s why communication skills are so crucial. 

Once you've got the knowledge and you're well-prepared, it's time to refine your communication skills.

Because as a seller, it's your job to lead a conversation — not just to push products. Some people mistakenly view salespeople as a billboard, but you're not there to simply fill a prospect's ear with sales pitches, you're there to provide a personalized sales experience.

That's why it helps to research prospects in advance. You want to have a good idea of what they're looking for, what they need and what you should expect from your interactions. And on the basis of all that information, you need to prepare powerful, informed questions that _show you care_.

For example, ask about the impact a product change might have on your prospect's business. Or ask your prospect to describe how they perceive the risks of switching from the current product they use to the one that you're selling.

Ultimately, this kind of conversation is all about asking how you can meet your prospect's needs. It's not about citing different product outlines but personalizing your pitch — and that's exactly why communication is so crucial.

To this end, you need to be careful about body language, and pay attention to the signals your client is sending you. Being sensitive to the way other people react to you is essential when you're trying to charm them into buying from you.

For example, a salesperson who had great success in New York moved to Minneapolis. Once there, he struggled to close a sale as people were put off by his pushy ways. He sat too close to people, and didn't give them enough personal space; what's more, he didn't realize that people actually shied away from him during meetings.

Simple awareness of how people were responding to him would have certainly helped him sell!

> _"Selling is a soft skill."_

### 8. Practice makes perfect: Work hard to improve your sales skills on a daily basis. 

In addition to all of the strategies you've acquired over the course of your career, you should also work to keep your skills honed on a daily basis.

To stay on top, you have to stay inspired and motivated. In sales, attitude counts for a lot: Optimistic sellers outperform their pessimistic peers. So make sure you keep up a good attitude.

For instance, you should try to find the value you're bringing to your buyers — this will help you keep your chin up even on the bad days!

Don't just concentrate on making more sales and earning more money, but concentrate on the great products you're working to deliver to make people's lives better. Even if it just comes down to price — maybe your product even saves people money!

It also helps to treat obstacles like a game, which will make challenges easier to deal with. It can be as easy as seeing if you could "finish this project in under 90 minutes!"

Although everyone gets down sometimes, you shouldn't underestimate the power of bad habits to _keep_ you down. An agile seller can't afford to wallow in old habits, so evaluate: Which habits are holding you back?

To change your habits, you have to figure out where they come from. Generally speaking, habits are formed by a _trigger event_ (which provokes the action) followed by the routine and finally, the reward. To change a habit, keep the trigger event and the reward, but change the linking routine.

For example, the author noticed that she was losing lots of time in the morning checking email and reading the news, and she was rewarding herself for this with coffee.

So instead, she started working out as soon as she woke up, rewarding herself with a nice coffee once she was finished. This new habit boosted her productivity, as she came home feeling energized from her workout; and once the coffee arrived, her brain knew it was time to switch into work mode.

> Fact: Optimistic sellers outsell pessimistic ones by more than 30 percent.

### 9. Final summary 

The key message in this book:

**A great salesperson needs to be adaptive to change. This means you have to be able to learn new information quickly, and constantly work on refining your selling skills.**

Actionable advice:

**If you feel insecure before a meeting, strike a pose to boost your confidence!**

If you need a confidence boost before a big pitch, consider singing a song that pumps you up or striking a "power pose" — position your body in a way that takes up a lot of space and displays confidence and control. That way, your body will release hormones that suppress stress and stimulate your confidence. You'll feel like a selling superhero in no time!

**Suggested further reading:** ** _To Sell Is Human_** **by** **Daniel H. Pink**

_To Sell Is Human_ explains how selling has become an important part of almost every job, and equips the reader with tools and techniques to be more effective at persuading others.
---

### Jill Konrath

Jill Konrath is an award-winning sales strategist and author of books _Selling to Big Companies_ and _SNAP Selling_, among other business and sales strategy titles.

