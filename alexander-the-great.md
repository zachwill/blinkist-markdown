---
id: 57cdd1042b2b0d0003fa29ec
slug: alexander-the-great-en
published_date: 2016-09-09T00:00:00.000+00:00
author: Philip Freeman
title: Alexander the Great
subtitle: None
main_color: C09458
text_color: 735835
---

# Alexander the Great

_None_

**Philip Freeman**

The eponymous hero of _Alexander the Great_ (2011) is remembered as one of the greatest military commanders who ever lived. Setting out from Greece at the age of 21, Alexander waged a ten-year campaign, during which he defeated the Persian Achaemenids and, in so doing, created the largest empire the world had ever seen. By spreading Greek culture and language throughout Eurasia, his legacy remained influential for centuries after.

---
### 1. What’s in it for me? Learn what’s so great with Alexander the Great! 

Alexander the Great is one of the few ancient Greeks that most people can name. Maybe they've seen a movie about him or heard him referenced in a discussion. But do you really know anything about Alexander? Or why he was so great?

At the time of Alexander's death, his empire was the biggest the world had ever seen. Even by today's standards, the amount of land he managed to conquer is massive, stretching from Macedonia in Europe to Afghanistan. This accomplishment has made him the archetype of the conquering king. Let's explore the circumstances that created this king and his empire, and follow him on a journey so full of adventure it would make anyone great.

In these blinks, you'll discover

  * what importance The Battle of Isus played in Alexander's campaign against Persia;

  * why Alexander's time in Egypt changed him forever; and

  * how the conquests of Alexander had a major impact on the spread of Christianity.

### 2. Born into the Macedonian royal family, Alexander the Great’s talents were spotted early on. 

Alexander was born in 356 BC, in the northern region of Greece known as Macedonia.

His father, Philip II of Macedon, was a legendary conqueror who managed the impressive feat of bringing nearly all of the Greek states under his control.

Though an imposing father figure, Philip was soon impressed by Alexander.

One day, a horse dealer offered Philip an extremely majestic horse for an extremely high price. This horse was reputed to be untameable, however, so Philip turned down the offer. But the young Alexander, who was around 13 at the time, intervened, imploring his father not to pass up such an opportunity.

Alexander's public outburst infuriated Philip, but he proposed a deal: If Alexander could mount the animal, he would buy it.

Alexander was quite clever, and he realized the horse was only jumpy when it caught sight of its own shadow. So Alexander simply led the horse into the sun and, once it was calm, skillfully mounted it.

This horse was named Bucephalus — and became one of the most famous animals in history.

Everyone, including his father, was astounded. Philip proudly proclaimed, "My son, you must seek out a kingdom equal to yourself — Macedonia is not big enough for you!"

Philip's pride didn't last long, however; indeed, the young Alexander's talents made his father feel increasingly threatened.

When Alexander outperformed his father on the field of battle, Philip had had enough and began efforts to rein in his son's growing popularity.

To punish Alexander, Philip divorced Alexander's mother, Olympias, and quickly remarried. In an effort to keep things relatively calm, however, Philip invited Alexander to the wedding banquet, where everyone proceeded to drink, according to custom, vast quantities of wine.

When a guest offered to toast the happy couple and the prospect of a new heir, Alexander, in a drunken rage, threw his cup across the table. Philip drew his sword, but, with a belly full of wine, promptly fell to the floor.

To escape the situation, Alexander and his mother fled to her homeland in the mountains of Epirus. Happily, however, mediation efforts were successful, and they soon returned.

### 3. After consolidating his rule in Greece, Alexander set out to invade Persia. 

Less than a year after Alexander's return, Philip was assassinated and, before long, Alexander captured the Macedonian throne by disposing of his rivals and winning over the army with powerful rhetoric.

Remarkably, Alexander began his ambitious rule at just 20 years of age.

Eager to make a name for himself, Alexander dreamt of picking up where his father left off. This meant invading Persia and putting an end to their meddling in Greek affairs.

But first he had business to take care of at home: there were some rebellious southern Greek states that needed a swift silencing.

The southern city of Thebes had a particularly rebellious leader who declared Alexander a tyrant. So, to set a violent example and keep others from thinking they could get away with such behavior, Alexander had the city destroyed, killing 6,000 Thebans in the process.

This brutality had the desired effect; all other Greek cities quickly abandoned thoughts of rebelling.

With this taken care of, Alexander was now free to launch his military campaign against Persia.

So, with a huge army behind him, Alexander left Macedonia in the spring of 334 BC.

That May, his first major battle against the Persians took place near Troy, on the banks of the Granicus River.

During the battle, Alexander's talent for military strategy was on full display. His most experienced general, Parmenion, had advised him against using this battlefield, since the river could break up his tightly formed army. But Alexander managed to use it to his advantage.

Even though the Persians gained an early foothold, Alexander got the upper hand by striking the enemy from multiple sides with two wings of cavalry. After this brilliant maneuver, Alexander struck down the Persian King's son-in-law and the Persians had no other choice but to retreat.

### 4. Alexander’s keen military mind helped him move swiftly through Asia Minor. 

Alexander had no interest in wasting time with victory celebrations. He quickly moved on to take the cities of Sardis and Ephesus before reaching the ancient city of Miletus, in what is now southwestern Turkey.

Since Miletus was a base for the Persian navy, it was a vital part of Alexander's plan. And, since the city initially offered a surrender, it seemed like it would be taken with ease. But word soon came that a Persian fleet was fast approaching, and another battle began.

Once again, Alexander prevailed by defying the advice of Parmenion.

While forming a plan of attack, they noticed an eagle perched upon one of their ships. Parmenion saw this as a sign that the gods favored a naval attack and advised to first attack the Persian navy, and then storm the city of Miletus.

But Alexander interpreted the sign differently. Since the eagle was facing landward, he decided to take the city first and then deal with the Persian navy.

This resulted in a decisive victory. The city fell so quickly that the Persian navy was never even able to dock its ships.

After taking Miletus, Alexander made a curious decision that historians have been debating ever since: he disbanded the Greek navy.

One of Alexander's contemporary historians, Arrius, suggested that Alexander knew his fleet was no match for the Persian navy, so avoided confronting it altogether and focused instead on conquering the whole eastern Mediterranean coastline — a strategy that would leave nowhere for the Persians to dock their ships.

Alexander continued to defy conventional military wisdom by refusing to rest and pushing his campaign forward through the harsh winter of 334 BC, continuing his streak of victories.

Alexander also used unusual methods to take the port city of Telmessus.

With a little help from inside the city, he managed to sneak a group of female dancers past the gate to perform for the Persian soldiers.

After much carousing, the soldiers, sleepy with drink, dropped their guard, and the dancers assassinated the whole garrison, allowing Alexander to take the city.

### 5. Sudden illness and death drastically changed the course of history. 

Alexander's campaign continued, and by the spring of 333 BC, he and his army had reached central Anatolia.

It was at this point that Alexander received some troubling news: The great Persian general Memnon's fleet was approaching southern Greece and appeared to be on the verge of launching an invasion.

Alexander knew that, despite Persia's brutal invasion of Greece in the previous century, the ongoing hatred of Macedonian rule meant that Memnon's advances would be largely welcomed.

Alexander faced a dilemma: Continue conquering or go home? After all, what good would his current campaign be if the Persians took over his homeland?

But perhaps the gods truly were on Alexander's side, for, at this moment, Memnon unexpectedly died after his health rapidly deteriorated on the Greek island of Lesbos.

Now it was Darius, the Great King of Persia, who had to make a decision. And with his most trusted general dead, he decided to call off his invasion of Greece and bring his troops home so that he could fight Alexander head-on.

This is when Alexander's luck took a turn for the worse.

The intense summer heat beat down on Alexander's army as they reached southern Turkey, and Alexander, sweltering, threw off his clothes and jumped in the Cydnus River.

But the water was so cold that Alexander ended up becoming feverish and ill. It got so bad that many were convinced he wouldn't survive.

Luckily, among the troops was a doctor named Philip whom Alexander had known since childhood. Philip recommended a dangerous medicinal treatment, which Alexander accepted, knowing that, otherwise, he might die.

Just before the treatment began, however, Alexander received a warning: Philip may have been bribed by the Persians to poison him!

Alexander faced yet another dilemma: Place his trust in Philip, or risk death by not taking the medicine?

Alexander chose wisely. He took the medicine and was back on his feet in a matter of days, ready to continue his campaign.

### 6. Alexander first encountered Darius at the Battle of Issus in November, 333 BC. 

At this time, all that separated the 23-year-old Alexander and King Darius's Persian armies was a small Turkish mountain range.

Darius hoped to face Alexander on a large, open battlefield, where his superior number of cavalry could overwhelm him. But, instead, The Battle of Issus took place on a narrow stretch of land near the Pinarus River.

What followed would go down in history as one of the greatest battles of all time.

Alexander's forces were initially pushed back, but, during a vicious counterattack, his right wing cut through the Persian army, allowing Alexander to begin attacking the rear of Darius's forces.

This turned the tides of the battle, and since Alexander was now fighting across two fronts, the Persian army began to disintegrate, and Darius realized that the battle was lost.

At this point, Darius and Alexander locked eyes, and Alexander charged him.

This climactic moment has been forever memorialized in a mosaic that is on display in the city of Pompeii. With bodies strewn across the battlefield, the two kings stare each other down, and, rather than anger, Darius's face betrays surprise.

Despite this epic confrontation, however, Darius managed to escape the battlefield in one piece.

Now victorious, Alexander was in possession of many Persian captives, including Darius's mother and son, who were both certain they would soon be slaughtered.

However, Alexander respectfully greeted Darius's mother, and promised to raise the Great King's son as his own.

Soon thereafter, Darius sent forth a peace treaty that proposed to give Alexander all of Asia Minor and a sizable ransom for his captive family.

It was a generous deal, and he knew his generals would advise him to accept it, but Alexander was not about to stop now. Instead, he forged another version of Darius's treaty that removed the concessions and added insulting remarks.

His advisors took the bait, and Alexander met no resistance in continuing his campaign to conquer all of Persia.

> The small battlefield at Issus all but eliminated the advantage of Darius's larger army, which numbered, by contemporaneous Greek estimates, around 600,000 men.

### 7. Alexander’s time in Egypt proved to be a momentous turning point in his life. 

After the Battle of Issus, Alexander continued down the eastern Mediterranean coastline, and after a year's travel, arrived in Egypt.

He encountered no resistance along the way, for the local population had no love for the Persians, who had been ruling Egypt on and off for centuries.

During his time here, Alexander made sure that the Egyptians understood his intentions to rule their land as a benevolent king who would respect their way of life.

After visiting the ancient pyramids of Giza, Alexander decided to found an Egyptian city that would end up serving as a permanent link to Greece.

While Egypt's Mediterranean coast already had a modest port, Alexander knew he would need to build a significantly larger city that could act as a major hub for trade as well as provide a safe haven for military vessels.

Further inspiration came to him in a dream, in which an elderly man spoke to him of the island of Pharos. When Alexander awoke the next morning, he knew where to build the city of Alexandria: on the Egyptian coast opposite the island of Pharos.

To mark the city's borders, Alexander's soldiers began laying barley on the ground, but were soon descended upon by thousands of hungry birds. Alexander was worried that this might be a dreadful sign from the gods, but his soothsayer, Aristander, reassured him that the birds were a glorious sign that foretold how the city would prosper and help feed the entire world.

From here, Alexander spent weeks trekking across the barren Sahara to visit an oracle at the sanctuary of Ammon. This event had a significant impact on his life.

Historians have differed on why he made this dangerous expedition, but it's clear that, at this stage in his life, he was looking for some answers and wanting to understand the importance of his journey.

Alexander asked the oracle whether he was destined to conquer the world. The oracle then responded with a nod and told him that he would indeed end up changing the course of history.

> _"Egypt was such an ancient kingdom that the great pyramids of Giza were almost as old to Alexander as he is to us."_

### 8. After again defeating Darius, Alexander took the ancient Mesopotamian city of Babylon. 

From Egypt, Alexander set off toward the ancient city of Babylon. But after crossing both the Euphrates and Tigris rivers, he was once again confronted by King Darius's army, which had set up camp on the plain of Gaugamela.

The stage was set for another of history's greatest battles.

Darius's forces were still far greater than Alexander's army, and, this time, they included impressive Indian war elephants, a sight that Alexander had never before seen.

The battlefield was wide open. Darius's had the advantage, and Alexander knew he had to come up with a clever plan.

The night before the Battle of Gaugamela, it came to him: he would ride his cavalry parallel to Darius's front line, which, hopefully, would draw his men away from the center of battle and create a gap that he could then charge straight through!

It was a huge gamble — but Alexander was indeed able to ride straight through the middle of the Persian army and directly toward Darius.

But before Alexander reached Darius, he was told that the Persians had also broken through his line of defense and were cutting down his men.

Forced to let Darius escape, Alexander turned to assist his troops and defeat the remainder of the Persian army.

Finally, Alexander was free to continue his journey to Babylon.

As he approached, his eyes fell upon a city the likes of which he had never imagined: the impressive walls — which contemporary sources say were at least 300 feet tall — towered above him. And then there was the city itself, planned out in a perfect grid system, with hundreds of bronze gates serving as entryways.

This time, there was no fighting or bloodshed. The citizens of Babylon greeted Alexander with music, flowers and gifts. As with the Egyptians, it's likely they were overjoyed to be freed from oppressive Persian rule.

With the inclusion of Babylon, Alexander's empire now straddled three continents and consisted of dozens of ethnicities.

### 9. After an embarrassing setback, Alexander finally conquered Persepolis, the capital of Persia. 

From Babylon, Alexander journeyed through the harsh and snowy mountains of Persia before finally arriving at the Persian Gates, a narrow mountain pass that led directly to Persepolis, the Persian capital.

It was here that the remainder of the Persian army ambushed his men and inflicted heavy losses.

Upon regrouping, Alexander discovered a lesser-known alternative route through the mountains that allowed him to sneak up behind the Persians under the cover of night and massacre the remaining forces.

Alexander's path to Persepolis was now open.

After years of travel and bloodshed, many of his restless soldiers saw the taking of Persepolis as the grand finale to their efforts. So, when the city surrendered, Alexander did nothing to stop the ugly pillaging that ensued.

Though this behavior went against Alexander's character, he knew that if he tried to stop his men, he'd have a full-blown rebellion on his hands and all would be lost.

But at Persepolis, Alexander also made a damaging blunder.

According to one version of events, Alexander, drunk at the Palace of Persepolis, was convinced by an Athenian woman that it was a good idea to burn down the whole palace. After all, it would be justifiable revenge for the Persians' burning of Athens a century earlier.

Alexander drunkenly agreed and proceeded to light the first flame. As the fire spread, he quickly came to his senses and tried to put the fire out, but by then it was too late.

This event is an uncomfortable one for many historians, and the fact that it is blamed on women and wine is reminiscent of how earlier Greeks blamed Helen for the mistakes at Troy.

After the ugly actions at Persepolis, Alexander set out to finally get his hands on Darius, who'd been taken prisoner after a successful coup by one of Darius's relatives, Bessus, who was now the new Persian King.

However, when Alexander caught up with him, Bessus quickly killed Darius before escaping.

This cowardly murder greatly saddened Alexander, who at this point had grown to respect Darius as a worthy adversary.

### 10. In pursuit of Bessus, Alexander began a treacherous march that would eventually take him to India. 

Despite Alexander's feelings, his army was overjoyed by Darius's death. Naturally, many of them took this news as the sign that their battles were over and they could now go home.

But Alexander had a new purpose: to make sure Bessus was punished for his cowardly betrayal of Darius.

On top of that, Alexander also had an unstoppable passion for campaigning that compelled him to keep pushing his empire further east.

So, he delivered a rousing and inspiring speech to his men that miraculously convinced them to continue their march.

Little did they know, however, that with Bessus as their new target of pursuit, they were about to face the treacherous Hindu Kush mountains, located in present-day Afghanistan.

By now, Alexander had already crossed more than his fair share of mountains, but none of these could have prepared him for the Hindu Kush.

Not only is the average height of these Himalayan mountains around 15,000 feet, the passage is also so narrow that the army had to walk single file — and they did all this in the middle of winter!

The only good news was that Bessus didn't believe that Alexander was crazy enough to attempt this climb, so he didn't bother to put any troops at the end of the pass.

It took five arduous days for Alexander's army to reach the other side of the range and descend into the land of Bactria.

Finally, in the summer of 329 BC, Alexander caught up with Bessus, and the village he was hiding out in quickly turned him over to the Macedonians.

When Alexander approached Bessus, he was eager to find out why he had murdered his own king and relative in such cowardly fashion. Surprisingly, Bessus explained that he thought Alexander would have appreciated his actions.

This was not what Alexander wanted to hear, so he proceeded to flog and torture Bessus before sending him back to Darius's family, who finished the job by executing him.

> _"We, the army of Macedonia . . . have done the impossible. We have conquered the world, my friends."_ \- Alexander the Great

### 11. Alexander made it all the way to the banks of the Ganges in India before realizing his soldiers could not go on. 

By 327 BC, Alexander and his army had spent seven years away from Macedonia. He had even married a local nobleman's daughter, Roxane.

But he wasn't about to settle down: Alexander now set his sights on India. He believed that, by conquering it, he would be able to go down in history as King of the World.

Conquering this strange new world started off peacefully, though not without confusion.

As he came to the settlement of Taxila, in present day Pakistan, he mistook the huge group of men and enormous elephants who were riding out to meet him as a threatening army.

Luckily, Omphis, the king of Taxila, noticed the confusion and saw that Alexander was preparing an offensive. He quickly rode to assure Alexander the display was simply their way of greeting foreign leaders.

But some Indian kingdoms weren't so eager to surrender.

When Alexander reached the kingdom of Pauravas, King Porus was prepared to put up a fight. And it was during this battle that Alexander's trusty steed Bucephalus was killed.

Alexander could only grieve after the battle had been won. In honor of his lost friend, he established a city nearby and named it Bucephalus.

But Alexander was also losing something else at this point in the campaign: the faith of his army. When they reached the Ganges, Alexander was unable to inspire them with one of his usual speeches.

This point was driven home when one of his generals gave a speech of his own. He told Alexander that the men were proud to have come so far and accomplished so much, but they longed to see their families and homeland again. This was met with a thunderous cheer.

The general went on to convince Alexander that the best idea would be to go home and raise a new army with fresh Macedonian soldiers.

After days of mulling it over, he agreed. After seven long and bloody years, Alexander was finally going home.

> _"We have marched ten thousand miles and accomplished the impossible. There is no limit to what men of noble spirit can accomplish."_ \- Alexander the Great

### 12. Alexander died at the age of 32, before he could accomplish any of his future campaign plans. 

The march home was a relatively uneventful journey, despite the fact that Alexander almost drowned in river rapids, and his army nearly perished in the Gedrosian desert.

By the time he returned, it had been ten years since he'd left Macedonia, and his empire had become the largest the world had ever seen.

But Alexander wasn't the sort of person to be satisfied by something like this.

On his way home, he kept himself busy by making plans for further expanding his empire.

He dreamt of controlling the entire Arabian and North African coastline and being able to travel the entire length of the African coastline, from Egypt to the western Mediterranean.

He'd also started thinking about how to respond to recent reports about a troublesome tribe called the Romans.

Alas, Alexander did not live long enough to realize all his plans. Merely three years after he left India, troubling signs began to appear.

One day, near the city of Babylon, Alexander was stopped by Chaldean priests who warned him not to enter the city.

Alexander laughed off their warning, but the priests persisted. At the very least, they told him, don't enter the city while walking west toward the setting sun. At the time, the setting sun was widely regarded as a symbol of death.

But Alexander was suspicious of the priests and didn't heed their advice. But, upon reaching Babylon, bad omens from the gods began haunting him.

While he was out sailing, Alexander's crown was swept away by the wind. And, worst of all, when he returned to his palace a few days later, an ex-convict was sitting on his throne and wearing his crown!

But it wasn't until after a night of heavy drinking that Alexander began to feel gravely ill.

As his condition continued to worsen, it began to dawn on Alexander that he would not survive.

When his companions asked him who would succeed him, Alexander uttered his dying words, "To the strongest."

> _"With that, the king of the world closed his eyes and breathed his last."_

### 13. Alexander’s legacy would go on to have wide-reaching effects on the world. 

Alexander's remarkable ten-year campaign did a lot more than just cement Greek culture's influence on a large portion of Eurasia. These effects certainly outlasted his empire, which began to collapse soon after his death.

Persia and India are just two places that were forever changed by Alexander.

A series of kingdoms that mixed Greek and Indian cultures appeared in Alexander's wake, and Hellenistic culture forever transformed Indian art and architecture.

For example, the emergence of statues of Buddha in human form are clearly inspired by the statues of the god Apollo.

Although Alexander was reviled by many in Persia, he was also remembered for his philosophical nature. The Qur'an mentions Alexander's philosophic bent, calling him a philosopher-king "whom God made mighty in the land and gave the means to achieve all things."

In this way, he began a Greek philosophical tradition in the region that continued to influence the Islamic age and its religious philosophy.

But the land where Alexander's legacy thrived the most was actually a place he never visited: Rome.

As the Roman Empire was getting its start, they embraced Greek as one of their intellectual languages, and Greek art and architecture heavily influenced their own work.

The Jews and early Christians also used the Greek language to replicate the Gospels. And since Greek was the primary language of the Mediterranean after Alexander's campaign, this meant Christianity had a ready audience. So, one could argue that, without Alexander, Christianity would never have extended beyond Roman Palestine.

And even though other conquerors like Julius Caesar, Augustus and Napoleon looked to Alexander as a hero to be emulated, no one was ever able to extend an empire further than the empire of Alexander the Great.

> _"It seems that there was no nation, no city in those days, no person in any land that the name of Alexander had not reached"_ \- Arrian

### 14. Final summary 

The key message in this book:

**Alexander the Great was one of the greatest military commanders of ancient times. He extended a small Macedonian empire from Greece all the way to India. By combining military genius with a keen political mind, Alexander became king of the largest empire the ancient world had ever seen.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Art of War_** **by Sun Tzu**

Thousands of years old, _The Art of War_ is a Chinese military treatise that is considered the definitive work of military tactics and strategy. It has greatly influenced military tactics as well as business and legal strategy in both the East and West. Leaders such as general Douglas MacArthur and Mao Zedong have drawn inspiration from it.
---

### Philip Freeman

Philip Freeman is a professor of classics at Luther College in Decorah, Iowa. He received his Ph.D. from Harvard University and is a respected and renowned author. His numerous books include _Julius Caesar_ and _St. Patrick of Ireland_.

