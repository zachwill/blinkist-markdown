---
id: 55db31ed2647b10009000057
slug: alibabas-world-en
published_date: 2015-08-26T00:00:00.000+00:00
author: Porter Erisman
title: Alibaba's World
subtitle: How a Remarkable Chinese Company Is Changing the Face of Global Business
main_color: EB672F
text_color: 9E4520
---

# Alibaba's World

_How a Remarkable Chinese Company Is Changing the Face of Global Business_

**Porter Erisman**

_Alibaba's World_ (2015) tells the story of Chinese e-commerce company Alibaba, which was founded in 1999. _Alibaba's World_ offers valuable insights into leadership and business acumen that can help you put your company on the path to success.

---
### 1. What’s in it for me? Learn about Alibaba – and how to discover the hidden treasure of successful business. 

Simply shouting "Open sesame!" won't gain you access to the land of successful business. But the gates _can_ be opened — and can be kept that way. Why not take a few tips from someone with some less familiar magic words?

In these blinks, the author introduces valuable insights furnished by the true story of the Chinese e-commerce company, Alibaba. He tells how its founder, Jack Ma, launched his ideas, gaining traction for Alibaba in a market that hardly existed, and how the company eventually became the largest of its kind in China.

In these blinks, you'll discover

  * which dirty tricks Yahoo! deployed in the name of competition;

  * how offering a service for free can kick-start an economically sustainable business; and

  * why being an enthusiastic, entrepreneurial person is more important than being the brightest boy in class.

### 2. When starting a business, reach for the sky. 

Entrepreneurship is easier today than ever before. You don't need a million dollars to get started; all you need is a great idea and some free time. And yet, the world is full of people too afraid to take those first steps.

Overcoming difficulties in your life is only possible if you put in serious effort instead of sitting around complaining. Starting a business is no different.

So then why isn't everyone an entrepreneur? Many people think it's too hard. After all, most industries already have a big player, and even if you trust yourself enough to compete with giants such as eBay or Google, there are still many legal and social hurdles to overcome. 

Every entrepreneur faces these problems. The only way to succeed is to try.

When Jack Ma started Alibaba in 1999, only one percent of the Chinese population used the internet. And those who did use it could hardly imagine buying anything online. People had enough trust issues when their business partner was standing right in front of them! Online buying was out of the question.

And yet, in the course of 15 short years, Alibaba overcame international competitors and social mistrust, and it now dominates 80 percent of China's e-commerce market.

Hearing a success story like Alibaba's might cause you to start doubting yourself. "What can _I_ do that's so special?" you might ask. Well, you don't have to be as smart as Einstein to start a business that will one day become successful. In fact, success has more to do with brio than brains.

Before starting Alibaba, Jack Ma was an English teacher earning $20 per month. He sometimes jokes that Alibaba is so successful precisely because he doesn't really know anything about computers. It wasn't his computer genius that made him so successful, but his entrepreneurial spirit.

> Fact: Alibaba went public in September, 2014, with the largest IPO in history.

### 3. If you want a successful business, build a sustainable company. 

Starting a company is easy these days; having it go under is easier. But it needn't be that way. Unfortunately, many entrepreneurs set short-term goals and either go out of business or look for a quick exit that will make them millionaires.

If you want a successful company, however, it needs to be designed to last, not just to get through the season.

One of the goals Ma set at the very beginning of Alibaba was for the company to last at least 80 years. In other words, a human lifetime. Although he later stretched that goal to 102 years, he stayed true to the idea of building a sustainable company that lasts.

Such a decision impacts the day-to-day business of your company. For instance, while Ma wanted to take the company public as early as 1999, he postponed the decision until the moment he was sure that going public would actually take the company further.

Also, a sustainable business is one that stays true to its culture. As a business grows, it will encounter many unexpected events that can change its very culture. To achieve longevity, avoid these changes at all costs!

Alibaba encountered one such event in 2005, during the course of negotiating Alibaba's buy-out of Yahoo! China. At the very last moment, Ma backed out when Yahoo! came up with one last demand: to name their venture "Alibaba-Yahoo!".

Ma believed that this would cause Alibaba to lose face in the eyes of domestic customers, making it appear as if Alibaba were Yahoo!'s puppet. This buy-out, the deal that turned Alibaba into a global phenomenon, was only finalized once Yahoo! finally gave up on its demand.

### 4. Build your business around the wishes of your customers. 

A successful business is one that has found a way to win customers' hearts. But how do you do this?

Start by finding an important _customer insight_ to build upon.

In the beginning, Alibaba tested various profit-making opportunities, like partnering and investing, on a global website to satisfy his investors. They even tried to open an office in Silicon Valley. Unfortunately, these early efforts failed, because the company didn't have that customer insight.

This all changed when Alibaba finally uncovered the barrier that made customers wary about e-commerce: placing trust in someone they had never met. After Alibaba developed authentication services that verified its status as a legitimate company, customers showed a greater willingness to use Alibaba's services.

This crucial customer insight was also used on Alibaba's C2C-platform, TaoBao. Rather than simply copying the sleek model used by similar American websites, TaoBao offered a fun platform with a live-chat application on which users could bond and build trust with one another.

But a customer insight isn't all it takes. That insight must also be compatible with the market.

Before Alibaba launched its premium services, use of its website was completely free. Though pressure from investors made the decision to offer free services quite difficult, the Alibaba team knew that the Chinese market would prefer trying something out before committing to it. This was simply the only way to gain a loyal customer base. From a strategic point of view, providing free services was really the only option available on the market — at least at the start.

This was even truer of the consumer market. For this reason, TaoBao has been a free service since its launch, in 2003. Although eBay mocked TaoBao for their free-service model, they later had to shut down their China website after TaoBao snatched up the majority of the market with their free services.

But even if you build a business based on keen customer insights, you'll still face challenges along the way. Our following blinks will help you overcome those challenges.

> _"If you do what is right for your customers and employees, investors will be rewarded in the end."_

### 5. Successful entrepreneurs turn hardships into advantages. 

Every business will face difficulties along the way, but such inevitabilities shouldn't discourage you from trying. In fact, you can actually turn the barriers that stand in your way into advantages!

A limited market, for example, could also be considered a great opportunity for development.

When Alibaba started, China was still discovering the internet — and very cautiously at that. The average person would probably think that starting an online business there would undoubtedly lead to disaster. There simply weren't enough potential customers! 

However, where most people saw limitations, Ma saw opportunity — the buying power of a billion people.

What's more, you can change seemingly limited social conditions over time in order to make a positive impact.

Another difficulty for internet businesses in China is dealing with local laws and officials. This has caused foreign companies such as Google to give up on the Chinese market and all it's potential.

Despite his international ambitions and time spent abroad, Ma was never too quick to judge local politics that involved the free usage of the internet. He simply recognized them as part of the deal, and decided to concentrate on whatever good he could achieve through his business. For example, an e-commerce business that brings jobs and prestige would surely make local officials reconsider their intolerant position regarding the internet.

Finally, unexpected crisis can also be an opportunity to further develop your business.

In early 2003, China was hit by the deadly SARS epidemic. After an Alibaba employee contracted the virus, all of the company's employees had to stay quarantined in their homes. But instead of letting their fear incapacitate them, the Alibaba team kept the site running from home.

They were rewarded for their efforts with a huge influx of traffic. People all over the country were either quarantined or too scared to go out to buy things in person, and as a result, they were practically forced to give e-commerce a try.

### 6. Competition is your best friend, not your worst enemy. 

If your business is the first on the market, will it always stay that way? Probably not. Once they catch wind of the opportunity you've created, others are likely to follow.

Staying ahead of the competition starts by differentiating yourself from them. Young businesses often fall into the trap of copying someone they admire, forgetting that true success only comes when you become more than someone else's shadow.

For this reason, Ma worked hard to ensure that TaoBao would be more than a mere copy of eBay. 

TaoBao understood that consumer culture in China was different from that in Western countries. Chinese consumers didn't have garages full of old stuff that they wanted to sell online. Rather, they wanted to sell products like they would in a small back-alley shop.

Knowing this, TaoBao encouraged this culture by offering tools such as the WangWang live-chat. They also encouraged buyers and sellers to call each other and build a relationship, just like they would in real life.

Moreover, you should appreciate competition as a way to develop yourself, not simply as an opponent who needs to be crushed.

People easily fall into the trap of creating a hostile relationship with the competition, and, as a result, focus all their energy on hurting them. Contrastingly, you should use competition to inspire positive action.

Take, for example, the war between eBay and TaoBao. Although partnering up with Alibaba would have been advantageous for them, eBay instead engaged in a war full of dirty tricks, such as inviting the TaoBao team to an event they were hosting and then un-inviting them at the last minute. And this although eBay's own community was calling them to partner up with Alibaba! By the time eBay finally offered an adequate deal, Alibaba was already negotiating with Yahoo! instead.

Building your organization is one thing. Building relationships with your team is another. Our final blinks will examine the interpersonal aspects of a strong business.

### 7. Be a bold, confident and inspiring leader. 

As your business grows larger, the difficulties it faces will only increase in number. As the leader of the company, it's therefore important for you to be strong enough to inspire others to endure the hard times and thrive during the good.

In part, this will require an ability to make _bold_ decisions. As an employee, taking the path of least resistance may be the smartest thing you can do; for leaders and entrepreneurs, however, things are different .

Look at Alibaba: their entire story is a series of bold steps. From the company's humble beginnings, operating from a small apartment in Hangzhou, to becoming a publicly traded company in 2014, Jack Ma had to constantly make hard decisions along the way.

Offering a free service when everyone else is charging; going global when 99 percent of your compatriots have never even used the internet; and postponing public trading until the company could rock internet history — all these decisions have strategic meaning, and demonstrate a willingness to act boldly.

But not only will you have to make bold decisions; you'll have to make _hard_ decisions as well.

As a leader, you will sometimes have to make decisions that strain your ethical compass. But that's just part of the deal if you want to build a company that lasts.

Alibaba, for example, was forced to make some difficult decisions during a bout of hard times in late 2000. Although the company was gaining both customers and popularity, they hadn't turned a profit. This forced Ma, who at this point had only ever hired people, to lay off half the staff in his US office only months after he'd opened it. And yet that decision had to be made for the company to survive and thrive.

### 8. A strong company needs a strong team. 

A business is only as good as its staff. Though company leaders play a critical role in the organization's development, they can't do everything alone. They have to build a strong team. But how?

When hiring people for a new business, try to avoid becoming seduced by prestigious college diplomas and fancy resumes. It's more important that the team works well together. After all, a business is a group effort; it won't work if everyone is doing their own thing — no matter how skilled they are.

For example, when the author first joined Alibaba, in 2000, he was put in a team of Western managers — all with perfect resumes — in a Hong Kong office. These Western managers had separated themselves from the actual Alibaba team in the Hangzhou office, and were simply doing their thing, totally disconnected from the rest of the team. A poor arrangement at best!

Working well together isn't all your employees need to do, though. They also have to be personally invested in the company to make it truly succeed. This way, you can be sure that they'll always give their very best.

In the early days of Alibaba, nearly all employees had stock options. As a result, people were working hard to cut costs, plodding away in cramped office spaces and sacrificing their personal time to ensure the company grew strong. 

Developing a personally invested team was also one of the key measures Alibaba took in getting through their financial troubles in 2001. Part of these measures involved hiring an experienced manager, who tested the team on the company's core values. The way he figured it, the bare minimum requirement for the team is that they share the company's core values. If they don't, they probably aren't as personally invested as they should be.

Solid team-building, along with the boldness and deep understanding of their customer base, made Alibaba what it is today. If you follow in their footsteps, the sky's the limit for your business.

### 9. Final summary 

The key message in this book:

**Anyone can start a business and succeed, provided they have enough determination and the right attitude. As the story of Chinese e-commerce company Alibaba reveals, even a small start-up from a developing country can compete with Western giants if they set their minds to it.**

Actionable advice:

**Choose a goal that seems too big to achieve.**

When setting your goals for your personal and professional life, don't be afraid to dream too big. Your goals shouldn't create artificial limitations; rather, they should toy with the idea of what's possible. Even if you don't know how you'll achieve your goals, the important thing is that you can at least imagine achieving them in the future. 

**Suggested** **further** **reading:** ** _The Everything Store_** **by Brad Stone**

Despite being a billion-dollar company today, Amazon was built on humble beginnings in Jeff Bezos' garage. From the get-go, Bezos was driven by the grand vision of creating an _Everything Store_ — which has, in the meantime, virtually come true. Focusing equally on the company and its founder, this book shows how he turned his dream into a reality.

_The Washington Post_ and _Forbes_ both dubbed _The Everything Store_ the best book of 2013. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Porter Erisman

Porter Erisman is the former Vice President of Alibaba. Having worked there from its early days until 2008, he has witnessed the company's ups and downs, and is one of the few Westerners involved in the business.

