---
id: 5e448b236cee0700068c4f30
slug: all-about-love-en
published_date: 2020-02-14T00:00:00.000+00:00
author: bell hooks
title: All About Love
subtitle: New Visions
main_color: None
text_color: None
---

# All About Love

_New Visions_

**bell hooks**

_All About Love_ (2000) breaks down why love remains elusive for many of us. From our flawed understanding of what love is to our misguided expectations of romantic love, author bell hooks examines common barriers to love and explains the steps individuals need to take for society to become more loving and nurturing.

---
### 1. What’s in it for me? Learn how to enhance your relationships by looking at love differently. 

It begins when we're children. Fairy tales, the media, the romantic stories of older relatives — all teach us the same thing: that love has the power to change our lives for the better. But love seems to be lacking in our current world — a lot of people feel cynical about it, and as a culture, our focus is more on power and materialism. This, however, hasn't stopped many of us from continuing to look for love, even if we do so hopelessly. 

But the problem isn't that love is hard to find; it's that we don't really know what it is in the first place. 

In these blinks, you'll discover the flaws not just in our ideas of what love is, but also in what we expect to gain from it and how we look for it. You'll gain the knowledge to rebuild your understanding of love and transform your approach to it going forward — from family bonds to the relationships you have with yourself and others. 

In these blinks, you'll discover

  * why loving feelings enable hurtful actions;

  * how a simple life can make room for more love; and 

  * the important lessons you can learn from your friends.

### 2. We need to change our definition of love. 

What's a topic we're all familiar with, one that can be found in the movies we watch, the books we read, and the songs we sing along to? If you're guessing love, then you're spot on. But despite being surrounded by images of, and ideas about love, a lot of us have a flawed understanding of what it is exactly. This makes finding love in our own lives a lot harder.

We tend to think of love as strong affection for family, friends, or romantic partners. However, the problem with focusing on that feeling is that we ignore the choices and actions that are necessary for love to truly flourish. In some cases, this emphasis on the feeling even causes us to overlook or excuse actions that are clearly devoid of love. Just consider the fact that many people who are mistreated in relationships insist that they love their partners and their partners love them. 

This idea that we're loved even when we're being hurt isn't only associated with romantic relationships; we actually learn it as children. Many parents discipline their children with physical punishment or shouting while still maintaining that they love them. Because of this, children become adults who believe that it's okay for those who claim to love them to mistreat them.

If we want to truly know love, we need to understand it not just as a feeling, but also as a verb. The author suggests a definition shared by many psychologists and theorists before her: that love is the active nurturing of spiritual growth in ourselves and others. 

When we think of love in this way, we start looking for acts of love that we can perform — rather than relying only on feelings — and we create a society that's less tolerant of abuse and neglect in any kind of relationship.

### 3. Our culture encourages dishonesty and celebrates materialism, making it difficult for us to experience love. 

Imagine you're creating an online dating profile. You've got a great picture next to all your interesting hobbies, and now you have to list your qualities. What are the chances that you'll describe yourself as someone who lies? Slim, right? 

We've been raised with the notion that lying is wrong — but strangely enough, when it comes to relationships, our society actually encourages men and women to lie to each other. 

In our patriarchal society, men are assigned more power than women and are expected to be dominant. This dominance grants them the ability to go against rules and norms without any regard for the consequences, which in turn encourages them to lie freely. Men have often lied to the author, in attempts to hide their feelings or avoid confrontation and responsibility. 

Patriarchy also leads women to lie. Instead of using dishonesty to gain power, women lie in order to be loved or to get what they want from men. Think of how often women are encouraged to change their appearance so that they can be considered "attractive." Or how some women dumb themselves down or pretend to be weaker than they are because they believe that being smart or independent will intimidate potential partners. 

For both sexes, lies make it difficult to trust. And when there's no trust, it's hard to form the real connections that allow love to flourish.

Another way that our culture has made it harder for people to experience love is by encouraging greed and emphasizing the importance of material possessions. Ideas of success and happiness propagated by the media tell us to consume and seek instant gratification constantly. We want more money, bigger houses, and the latest gadgets — and we want them _now_. 

Unfortunately, this greed spills over into our relationships. We end up treating people like objects, discarding them if they don't immediately meet our needs, and then moving on to the next person. Love, which requires time and commitment, can't develop in this context.

The good news is that it's possible to change our approach to life and, as a result, to relationships.

Instead of always trying to acquire more and meet our own needs at the expense of others, we can learn to live simply by focusing on sharing resources and practicing compassion toward those around us.

### 4. Self-love and spirituality are the foundations for loving relationships with others and the wider world. 

How many times have you heard that people need to love themselves first before they can love others? This advice is repeated over and over again by well-meaning friends and family, psychologists, and self-help books. And the truth is, they all have a point. 

When we love ourselves and nurture our own growth, we can then do the same for others. This sets the stage for positive relationships. However, self-love isn't something that we're born knowing how to do; we have to work to learn it.

The first step toward self-love is acknowledging any low self-esteem and negative feelings you might have about yourself, and then recognizing how they came about. Maybe you had a parent who was always judgmental, or you grew up believing there was something wrong with your body. Whatever the issue, once you know the root of the negativity, you can start countering it with affection and self-care.

Self-love isn't the only ingredient you need in order to experience loving relationships. You also need to learn how to extend care and affection to those around you. One way to do this is by embracing spirituality. 

Although many people link spirituality to religion, this doesn't have to be the case. Spirituality teaches that we are all connected by something larger than ourselves — some may call this God or simply a higher consciousness. When we extend love to ourselves and others, we are honoring that connection.

Honoring this connection to others puts us in a better position to lead lives that are guided by the values of love. This means prioritizing nurturing encounters and relationships with everyone we meet and being courageous enough to connect our values with our actions. 

Take, for example, men who believe that domestic violence is wrong, but who aren't willing to challenge the patriarchy that enables it, as this would mean losing some of the rights and privileges that they enjoy under that system. We can only create a more loving society if we are willing to accompany our loving and spiritual beliefs with actions.

### 5. Experiencing community, in the form of family and friendships, teaches us how to love. 

If you were asked to list the members of your family, where would you start and end? Most people just think of their nuclear family, meaning their parents and any siblings. This is because society tends to emphasize the nuclear family while ignoring extended family — the grandparents, aunts, uncles, and the like, who form a community that each of us is born into. 

Communities are ideal spaces to learn how to love. By disregarding the community of extended family, we lower the chances of children experiencing love. In isolated nuclear families, women might start to depend only on their husbands, while children become totally dependent on their mothers. This creates a setting where abuse and neglect can occur unchecked.

Expanding our idea of family to include extended kin makes it more likely that we'll have other people to rely on for love and affection, even if we don't receive these in our nuclear families. Growing up, the author experienced this situation. Although her household was dysfunctional and she was often shamed and humiliated, having a nurturing grandfather gave her hope and showed her that families could be places of love.

Not all of us are lucky enough to have extended family members around — and even if we do, it's not always the case that our relationships with them are loving ones. However, this doesn't mean that we can't receive the nurturing benefits of community. Good friendships are another way that we can build community and experience love outside of our family homes; they also teach us how to respect each other and work through problems. 

Take a moment to think about your closest friends, the ones you've known for years. You've probably had your fair share of fights and disagreements, but you've learned how to work through them and return to a place of care and understanding. These relationships can guide how you approach all other interactions with family members, romantic partners, or new acquaintances.

> "_The love we make in community stays with us wherever we go."_

### 6. Mutual growth through sharing, communicating, and honesty is at the core of love. 

Say you're having coffee with a friend, and the topic of relationships comes up. You ask your friend what they want in a partner. They describe how they'd like to be treated and outline what they'd want their partner to do for them. 

While it makes sense to expect to get something in our relationships, we often focus on this and ignore another important component: the fact that we also have to give. We all need to receive things like time, care, affection, and attention. And when partners give to each other generously so that both have their needs met, there is shared growth and nurturing in the relationship. 

Unfortunately, shared growth in relationships can be difficult in our society because we are frequently taught that men and women have specific roles. The author uses her own relationships to demonstrate this. In one relationship, her partner believed that a woman's role was to nurture and care for him just as his mother had. In another relationship, her partner began relying on sexist gender roles to make him feel more masculine and dominant. 

In both cases, she never received the love she was looking for. Her partners had not been taught that they needed to give love, only that men and women needed to behave in specific ways. 

Blindly going along with gender roles prevents both men and women from having loving relationships; abandoning these ideas and choosing to learn how to love is an important step for us all. Ironically, one way past gender roles might be to embrace one of them — because women are raised to be caring, giving, and nurturing, they can guide men toward love. But this only works when men are willing to admit that they need to learn how to love and are able to prioritize their partners' needs in the same way that they prioritize their own.

### 7. Believing that romantic love is not something we can control and that it should be easy creates unrealistic expectations. 

When was the last time you watched a romcom, or read a romance novel — a few weeks ago? A few years? Either way, you're probably familiar with the standard storyline: boy meets girl, and they inexplicably fall in love — neither has any control over the situation.

The idea that we don't choose who we love doesn't just exist in romcoms and books; many people expect their own romantic relationships to work that way. But accepting this lack of control stops us from being intentional in love; it prevents us from taking the time to figure out what we need in a partner and what we can offer that partner in return. Being selective in our romantic relationships gives us a better shot at choosing partners who are right for us, and with whom we can grow. 

Believing that we have no choice when it comes to romantic partners isn't the only idea that's jumped out of the screen and into our lives. A lot of people also search for the passion and ease of most romantic relationships depicted in the media, mistaking those things for love and feeling disappointed when they don't last. 

Say two people meet and are instantly attracted to each other. The beginning of their relationship is effortless and filled with excitement. Eventually, they start really getting to know each other and are faced with imperfections and difficulties. Perhaps one partner has insecurities from their childhood, or the other has trouble being open and communicating. But love should be easy, right? So, rather than working through these issues, the pair abandon the relationship and decide it just wasn't meant to be. 

This approach prevents love from developing. We can only experience true love if we see each other for who we really are and commit to doing the work of growing together — even when it's difficult and not like in the movies.

### 8. Final summary 

The key message in these blinks:

**Having truly loving relationships is a matter of being willing to generously perform acts of kindness, compassion, respect, and nurturing for everyone –** **ourselves included. When we do this, we not only improve our interpersonal relationships; we also start building a society in which everyone is valued and cared for.**

Actionable advice:

**Make an effort to build community wherever you are.**

Wherever you find yourself, start extending kindness or greetings to strangers. Show appreciation to those around you. These small but important actions build bridges, and as such, they're first steps in connecting with others and building a community.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Ain't I A Woman,_** **by bell hooks**

As we've just learned, sexism has shaped our ideas of love and is one of the biggest obstacles standing in the way of us building loving relationships and changing society for the better. But it affects a lot more than just our relationships.

To better understand sexism, and particularly how black women experience it, check out the blinks to _Ain't I A Woman_. You'll gain insight into how black women have been doubly oppressed by racism and sexism throughout America's history, and see how this double oppression affected the movements for the abolition of slavery, the establishment of women's rights, and civil rights. Finally, the blinks show how black women's oppression continues today, and suggest some ways that society can start to address it.
---

### bell hooks

bell hooks is a leading intellectual, writer, feminist, and cultural critic whose work focuses on capitalism, gender, and race. At the age of 19, she began writing _Ain't I a Woman:_ _Black Women and Feminism_ (1981) _,_ which has become essential reading for those interested in the topics of racism and sexism. She went on to write more than 30 books, including _Yearning: Race, Gender, and Cultural Politics_ (1990), which won the American Book Award, and _Salvation: Black People and Love_ (2001), which was nominated for the Hurston Wright Legacy Award.

