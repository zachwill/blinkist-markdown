---
id: 553fba136136310007410000
slug: all-joy-and-no-fun-en
published_date: 2015-04-29T00:00:00.000+00:00
author: Jennifer Senior
title: All Joy and no Fun
subtitle: The Paradox of Modern Parenthood
main_color: F5DF46
text_color: 756B22
---

# All Joy and no Fun

_The Paradox of Modern Parenthood_

**Jennifer Senior**

_All Joy and No Fun_ (2014) is a book about the trials and tribulations of raising kids. Senior examines the challenges of parenting while keeping us cognizant of the pleasures and rewards that come with it.

---
### 1. What’s in it for me? Learn the often-uncomfortable truths behind modern parenting. 

Being a parent today is much easier than it was a century ago. For one, our society is free from many of the diseases that, not that long ago, killed or disabled many infants. Then there are the parenting aids, from designer buggies to baby monitors, which make parenting that much easier. And of course, there are babysitters and daycare centers that afford parents more freedom to carry on with "normal" life.

And yet, despite these amenities, modern parenthood is not a bed of roses. Raising a child today can lead to increased stress, lack of sleep and loss of autonomy. Of course, none of this takes away from the joy of raising a child. But no parent should underestimate the hardships involved — hardships that these blinks will explore.

In these blinks you'll discover

  * why being able to work from home might add to your level of stress;

  * what is going through the minds of soccer moms; and

  * why you should give your teenager a little bit of freedom, but not too much.

### 2. Being a parent is a challenging undertaking; knowing what’s in store, and how to handle it, helps a lot. 

Those of us who plan to have children in the near future have a pretty good sense of what it's going to take, right?

Well, not so fast.

No matter how many baby books you've trawled for advice, no matter how many parenting gurus you've consulted, you will never be fully prepared to have a child.

For example, you may think you know all the ins and outs of childcare, but there is far more to it than you likely realize. Yes, you know you'll be sleep-deprived and expected to change dirty diapers — but you'll have other concerns, too.

For starters, today's parents are burdened with a crippling desire to be perfect. This stems from a surfeit of choice; parents get to choose the number of children they want, when they want have them, what kind of birth they want, how they want to parent them. The baby's gender is about the only thing left to chance.

You might be thinking, "That's great!" But, actually, too many options create pressure to do everything exactly right.

Parents want to impress others with the way they raise their family and so feel stressed when they can't quite live up to their own ideals.

The role of children in the home and in society is changing, too.

Childrearing involves more work than ever before: we haul them to soccer practice and music lessons and all the other extracurricular activities that are often expected of them — and then have to pick them up afterward!

Modern parents usually need more time and money than their parents did. It's no surprise that stress and exhaustion are common.

The following blinks shed some light on the most challenging aspects of modern parenthood. It should be kept in mind, though, that the advice below applies largely to the US middle-class not to the poor or the elite.

So start off by taking a look at the first difficulty faced by most new parents.

> _"Becoming a parent is one of the most sudden and dramatic changes in adult life."_

### 3. Parenthood erodes your autonomy. 

Prior to having children, you are a self-determined adult, coming and going as you please. You are free to do the things that make you happy. You want to go out and party? No problem! You want to take a quick weekend vacation? Hop on that plane!

This comes to a rather abrupt end when your child arrives.

With this change, adults lose the daily rhythms that have shaped them and given them joy.

Now that most parents choose to have children later in life, it's harder for them to suddenly alter their deeply entrenched behavioral patterns.

One of the most obvious examples is how newborns disrupt sleep patterns. We spend a lot of our lives perfecting how much sleep we require to function well, and when a child comes along, we lose control over this.

When you start a family, you are also suddenly unable to make reliable predictions about your future, because there's no way to accurately predict your child's needs. Those without dependents can map out their lives and spend time planning what they want to do.

Having a child makes it hard figure out when you'll be free in the future. Merely being around children can even stop you from thinking about the future, because life suddenly becomes a matter of the here and now.

But, it's not all that bad, either. One way to get on top of things is to plan your work around childcare.

Although flexible work schedules might help you juggle both your duties as a parent and your ambitions as an adult, it can also restrict what you are able to do. Working from home whenever you want will likely mean that you spend your time oscillating between tending to your child and tending to your work — an unproductive battle between the two!

It's clear, then, that your autonomy is one of the first things to suffer when you have kids, but what else is threatened?

> _"But we can never choose or change our children. They are the last binding obligation in a culture that asks for almost no other permanent commitments at all."_

### 4. Not only will you personally struggle with parenthood, your marriage can come under fire, too. 

Are you married? Do you plan to have children? Bear in mind: no matter how long you've been married or living in a partnership, your relationship will change forever when a child enters your life.

Even though having children can make it less likely that you will divorce, it doesn't guarantee automatic happiness.

In fact, parenthood can seriously harm relationships.

How?

Well, first, there is the issue of housework. When a child arrives, the amount of housework skyrockets. Often, both parents step up to tackle all the new tasks, but they don't necessarily share the burden equally.

Mothers are typically better at multitasking; for example, they can look after a child and clean at the same time. Fathers, on the other hand, are more likely to focus first on one task, then on another. This often makes it seem like fathers aren't pulling their weight, even if they spend the same amount of time helping out.

And this seeming "inequality" creates conflict.

Additionally, parents are forced to rely on each other more and more for social support.

Before the baby, couples socialize with a variety of people — their friends, their colleagues. But this all changes when the baby arrives; time that was spent socializing suddenly vanishes, and the parents become each other's only social outlet.

This reliance can lead to additional relationship pressure, which in turn leads to more conflict.

Finally, mothers and fathers have different approaches to taking a break from childcare and enjoying some "me" time.

Whereas fathers are often able to switch off from parenting and relax, mothers tend to feel pangs of guilt about taking care of themselves rather than their child. And dad's taking a break may incur mom's resentment.

These are some of the early challenges parents face. But what comes later?

### 5. Over-scheduling your kids and yourself is a big problem. 

Have you ever heard the term _concerned cultivation?_ Maybe not — but you're probably familiar with parents who engage in it.

Concerned cultivation is what (typically middle-class) parents do when they attempt to exercise control over and plan all aspects of their kids' lives.

Many of us have heard the term _tiger moms._ These are obtrusive mothers who force their children, at a very young age, to take up a staggering quantity of hobbies and educational activities — extra math classes, language lessons, sports practice and so on.

These mothers epitomize concerned cultivation: they take frantic hold of every opportunity to develop the education and skills of their children.

Statistics show the rate at which this trend is increasing. Compared with a mother in 1965, a mom today spends 3.8 hours more per week on childcare. She works three times as much as she did in the past, and fathers spend three times as many hours with their children.

But why do some parents push their children so much? Here's one reason: globalization.

The modern world is an unsteady, unpredictable place. No one knows exactly what the future will bring. Jobs in the West that seem steady today may soon be outsourced, and others may be automated or mechanized.

Therefore, in order to secure a good job, parents are desperately trying to encourage their offspring to master as many skills as possible. They want to cover all the bases.

For example, one reason for soccer's growing popularity in the US is its global appeal. American football, on the other hand, appeals almost solely to Americans. So your child clearly stands a better chance of winning an international scholarship by playing soccer than by playing football.

Lining up an array of activities for your child when they're young is one thing, but soon enough a challenge we all dread comes along — adolescence!

### 6. Adolescents are hard to deal with, and it’s crucial to understand why. 

Ask about a parent's baby or toddler and they will happily gush about its cuteness and wonderful personality. Ask a mother or father about their teenager and you will get a rather different response!

Adolescence is so fraught conflict and angst because it's that awkward phase between childhood and adulthood.

Parents often think the ideal way to bring up a teenager is to treat them like an adult, permitting them to make their own choices and enjoy their own freedom. However, this is very risky, as adolescents are vastly different from full-grown adults.

First, they don't react to rational arguments like most adults. No matter how logical a proposition, an adolescent is likely to respond emotionally.

They are extremely hungry for experimentation. When given freedom, they stretch it as far as they can. Allow them to drink alcohol and they'll probably keep at it until they're fall-down drunk.

But smothering them with care and constraint also isn't the way to go.

One of the main issues in modern society is that we try to prevent our teenagers from taking risks and exploring creative outlets. We try to curb their energy and behavior. This is often why they engage in activities that to us seem senseless, such as throwing eggs at people on the street.

Parents also often make the mistake of trying to live a second, vicarious life through their teenage children. This usually produces negative effects.

We all regret little things from our teenage years. When you have an adolescent child, you suddenly feel like you have the chance to stop these things from happening again. Therefore, many parents spend their time attempting to stop their own children from acting in certain ways. Unfortunately, this is usually more restrictive than it is protective.

### 7. The joy and fun of having kids outweighs the trouble. 

So having children presents many a challenge. Why, then, do we want them at all? Well, hand in hand with the hardship is a great deal of fun and joy.

The wonderful thing about children is that they allow you to reconnect with your past. Also, the monotony of adulthood is interrupted when you have kids, which can be very liberating.

Children are a strange force. Their ability to be present in the here and now can be hard to cope with; they want a pizza right this minute, no matter what you say! But childlike behavior can also free you and break your own dull routines. For example, you can get to interact with the world in more childlike fashion, by touching things instead of observing life from a distance.

One other advantage is that children allow you to philosophize. Many adults consider philosophical questions to be unpragmatic, a distraction from their busy schedules. But children's wanting to know why everything is the way it is encourages parents to philosophize more, which is something many of us love about being around kids. They invite us to question our adult assumptions.

So having children can be incredibly enjoyable. But it's more than that: Parenting also forms a part of your identity.

Through raising children, you can explore your potential and add purpose to your life. Children are a reason to get up every morning. Though they'll certainly complicate your life, they'll also simplify it, because you'll have a clear goal: to raise your child as best you can.

In a 2007 poll conducted by Pew Research Center, 85 percent of parents said their relationship with their children contributed most to their sense of personal fulfillment — more than their relationship with their spouse.

Sure, many challenges come with having children. But these can be overcome. In the end, it's the power that kids have to bring joy and fulfillment to your life that makes them worth having.

> _"It's not a full happiness. It's not a full sorrow. It's a full parenthood. It's what you have when you have kids."_

### 8. Final summary 

The key message in this book:

**Nothing can really prepare you for parenthood. The reality is you'll lose autonomy, sacrifice your leisure and face relationship challenges with your parenting partner. But realizing that these challenges are par for the course will also help you remember that, all in all, being a parent can bring you immense joy and enrich your life.**

Actionable advice:

**Remember yourself as a teen.**

The next time you are about to tear your hair out because of your teenager's behavior, stop and think: are you so frustrated because you did similar thing when you were her age? A little bit of empathy might help you keep hold of your sanity!

**Re-prioritize your social life.**

If you're feeling cut off from your social circle because you're caring for your new baby, make a deal with yourself to get out and reconnect with some of your other friends, instead of only those who have kids. Sometimes this little change can give you the lift you need.

**Suggested** **further** **reading:** ** _Selfish Reasons to Have More Kids_** **by Bryan Caplan**

_Selfish_ _Reasons_ _to_ _Have_ _More_ _Kids_ examines the demands of modern parenting and why people today are choosing to have fewer and fewer kids. The author argues that this trend is due to modern parents placing too high expectations on themselves, even when a far more relaxed style of parenting would get the job done just as well and make the whole experience more enjoyable.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jennifer Senior

Jennifer Senior writes on mental health and social science, and is a contributing editor at _New York Magazine_. In addition to writing and public speaking, Senior has appeared on a number of television programs, including _Good Morning America_ and _Today_.

