---
id: 565c7f109906750007000056
slug: all-marketers-are-liars-en
published_date: 2015-12-02T00:00:00.000+00:00
author: Seth Godin
title: All Marketers Are Liars
subtitle: The Underground Classic That Explains How Marketing Really Works – and Why Authenticity Is The Best Marketing of All
main_color: E02D33
text_color: 9C1F23
---

# All Marketers Are Liars

_The Underground Classic That Explains How Marketing Really Works – and Why Authenticity Is The Best Marketing of All_

**Seth Godin**

_All Marketers Are Liars_ explains how telling your customers authentic, meaningful stories about your business helps you sell your products and build a strong, long-lasting relationship with them.

---
### 1. What’s in it for me? Get to know marketers. 

You know how it goes: you see an AMAZING commercial advertising an AMAZING product. You rush to get it, you buy it, you bring it home. The next day it breaks, crashes — or just does not fulfill the promise made about it in the (no longer so amazing) commercial.

Marketing is about spreading ideas. It's about telling stories and, in many cases, lies. It's about tapping into the worlds and minds of consumers — something marketers can be pretty good at. 

That's exactly what these blinks are all about. In fact, in _All Marketers Are Liars_, the author has some great insights on the world of marketing, how marketers think and how they have created stories (and fibs and frauds) for decades, which have had an impact on the lives of many — both for better and for worse. 

In these blinks, you'll learn

  * why spotting your consumers' worldviews is so important (and how Dr. Atkins did it);

  * why people use similar strategies as frogs; and

  * where the important line is between fibs and frauds.

### 2. Marketing is all about telling a story people can believe in. 

A long time ago, people noticed that the sun rose every morning, so they developed a story about Helios and his chariot. This occurred long before the concept of "marketing" existed, but it has something fundamental in common with it: a good story.

Effective marketing is all about telling people believable stories. George Riedel, a tenth-generation glass blower, understands this idea well. His company makes glass products and his wine glasses are particularly popular. He says that all wines have a unique "message" which is translated through the glass out of which a person drinks it.

Riedel's story works amazingly well. Scientific tests have proved there's no difference between his glasses and others, but wine experts and enthusiasts all around the world insist that wine tastes better out of a Riedel glass. 

That's the power of marketing. It can change the taste of a wine. 

Marketing schemes like Riedel's succeed because in today's world, people buy what they want, not what they need. That's how marketers earn their profits. 

Think of it this way: imagine a young woman who buys a pair of Puma sneakers for $125 — which were produced for $3 in China. She doesn't buy them because of their durability or the support they give her feet. She buys them because, once she puts them on, she feels cool. She imagines her life and image will improve slightly once she gets those shoes.

What the Puma marketers _really_ sold the young woman is a story — a story that she's special, hip and fashionable — and people spread stories. They rarely buy a product just for its particular features. So remember, if you can tell a good story, you'll reach them.

### 3. Get a feeling for your customers’ worldview. 

You might assume that everyone wants the same things in life: success, security, good health, love, respect, happiness, etc. But it's not that simple. We don't all want the same things.

Each person has his own _worldview_, which is formed by his values, biases and assumptions. A person's worldview is influenced by his parents, the schools he went to, the places he's lived and the other environments he's been in. Their worldview determines which stories they're going to believe.

For example, if you were cheated the last time you bought a car from a used car salesman, that would affect your worldview the next time you go to another dealership. You'd have a different worldview from a person who has successfully bought three cars from that dealership in the past. 

So customers aren't all the same. Then again, they aren't completely different. 

People from similar backgrounds develop similar worldviews. Your job as a marketer is to find a group of people who have nearly the same worldview and tell a story that speaks directly to them.

For example, there are new mothers whose highest priority is their children's growth and education, bodybuilders who think the next nutritional supplement is the shortcut to a perfect body and environmentalists who think the next scientific innovation will be the one to save humanity. Each of these groups wants to hear stories that agree with their worldview. 

_Baby Einstein_, a division of Disney, told a great story in 2004 when they sold $150 million worth of videos for newborns and infants. The name, along with an aggressive marketing campaign, convinced parents that the videos would make their children smarter. These parents wanted to feel like they were helping their children, even though research later showed the videos had little or no effect on children's intellect.

### 4. Tailor your story to your customers’ worldview. 

So you've spent the time getting a good understanding of your customer base's worldview. What's next?

The next step is to create a story, or _frame_, that matches that worldview. The frame allows you to present your story to your customers in a way that's meaningful. 

_Interstate Bakeries_, the company behind _Twinkies_ and _Wonder Bread_, went bankrupt when their customers' worldview changed: when Dr. Atkins's low-carb diet became all the rage, people no longer wanted to feed things like Twinkies to their children. They wanted something healthier.

During that same time, _General Mills_ took advantage of the shift in worldview. Shortly after the Atkins craze started, General Mills adjusted their story by using 100% whole grains in all of their cereal brands, including _Lucky Charms_. It framed its cereals as healthy, which aligned with the negative view of carbohydrates that prevailed at the time.

The key is to identify a group of people who are open to hearing a new story because of their particular worldview. 

Imagine your boss asks you to design the marketing campaign for a new kind of salty snack. There are plenty of snacks in the supermarket so you need a story that will make yours stand out.

You might target new moms who believe salty snacks are unhealthy for their kids and create a story for them: the snacks will be made from soy, not potatoes. They'll be organic, low-fat and salted with sea-salt rather than sodium. They'll come in a box rather than a bag and you'll sell them in the produce department instead of the snack aisle.

### 5. People only pay attention to new information and they create stories to make sense of it. 

A key part of telling great stories is understanding how the brain responds to new ideas and how we process information.

When people encounter something new, they compare it to what they already know. Humans are similar to frogs in that sense: we only react to changes and ignore anything that isn't new.

How does a frog spot a fly, aim its tongue at it and catch it all in less than a second? It ignores everything static around it and keeps an eye out for change in its environment. In the same way, when you walk into your house, you notice immediately if something has changed.

Humans can't catch flies with their tongues but we _can_ tell right away if the supermarket gets a new brand of beer or the mailman gets a haircut. 

Once people notice something new, their brains start trying to understand it. People naturally search for explanations for what they see because the brain is restless and doesn't like randomness.

So if a window breaks, we might look for an object on the floor: we immediately develop a theory on how the window broke and check to see if it's true. 

_The_ _New York Times_ once ran an article concerning this phenomenon. The article was about people who were convinced that the shuffle feature on their iPods was broken. They were certain their iPods favored some songs over others. But randomness doesn't necessarily dictate that the songs be distributed evenly.

This is why stories are so important: they allow us to make sense of the world. If you want people to engage with the new information you give them, you have to make sure your story is authentic. Authenticity is the thing that makes or breaks your story.

### 6. Only authentic stories have a resounding impact on people. 

We've all judged people based on their appearance before, even if we didn't mean to. We make quick judgements about people and objects all the time, often without realizing it. As a marketer, it's your job to make sure your customers make the right judgment when they're first exposed to your product. 

There's a difference between your customer's _first contact_ with your brand and their _first impression_ of it. People often confuse these two things. The customer's _first contact_ with the brand is simply that: their very first contact, which might not necessarily generate a reaction. Their first meaningful reaction is their _first impression_. 

Imagine you receive a promotional email from an online store. That would be your first contact, but you might not form any opinion on the store. If you buy some shoes from them later and the shoes never arrive, that would create your first impression — and it wouldn't be a positive one!

First impressions are vital, but you never know which aspect of your business is going to generate the first impression. That's why _authenticity_ is so important. 

Your story is authentic when it's coherent relative to all aspects of your business. Your product, your employees and your company's message all need to align with each other. 

That means all of your company's potential first contact points need to resonate with the message you're trying to send. So if your logo and location are cool but your staff is rude or your products are subpar, your story isn't coherent and you aren't authentic.

You won't reach out to a large number of people without a coherent and authentic story. Authenticity is the key to connecting with your customer base.

### 7. Marketers shouldn’t cross the fine line between fibs and frauds. 

It would be unethical to market a speaker cable for $99 if it only worked as well as a $10 one, right? Marketers should be honest and consumers should be rational, right?

Unfortunately, it's not that simple. People aren't always rational when it comes to making purchases. Most of us would like to think we buy products because of their quality or function. We spend our hard-earned money on useful things that make our lives better; we don't waste it on foolish trends.

But it's nearly impossible to avoid buying overpriced products. Almost everyone gives into buying an expensive designer T-shirt or eating out at a trendy restaurant every once in a while. 

Marketers know they can take advantage of their customers' irrational behavior. That's why some come up with _fibs_ to sell their products. 

Fibs are little lies that help the company's story come true. They don't need to have any negative effect on the customer. 

George Riedel, our wine glass maker from before, is a fibber. He's a sort of "honest liar," if you will. He tells his customer that his glasses improve the taste of fine wines. That's not true, scientifically speaking, but it _becomes_ true because the customers believe it. 

Fibs like this are harmless and can even improve the customer's experience. It's another matter when marketers engage in _fraud_. 

Fibs and fraud are not the same thing. When marketers tell overt lies that are harmful to their customers, that's fraud. 

Nestlé committed fraud a few decades ago when they told their customers that bottle-feeding was better than breastfeeding, inadvertently contributing to the death of over a million babies, according to _UNICEF_. If Nestlé had told an authentic story aimed at mothers who had trouble breastfeeding, they could've avoided that tragedy and built a long-term business. Stories need to help the customers, not hurt them.

### 8. Final summary 

The key message in this book:

**Get to know your customers, then craft a special and authentic story just for them. Make sure every aspect of your business — from your staff to your products to your logo — fits in with that story. Use harmless fibs if it helps you and your customers, but don't ever commit fraud against them. When you reach out to people in a meaningful and sincere way, you'll build a relationship that benefits both sides.**

Actionable advice:

**Tailor your story to a small audience.**

Don't try to appeal to everyone or you'll water your story down. When your story targets a more specific audience, it's much more powerful.

**Got Feedback?**

We'd love to hear what you think of our content! Just send us an email at [remember@blinkist.com](<mailto:remember@blinkist.com>) with _All Marketers are Liars_ in the subject line and share your thoughts!
---

### Seth Godin

Seth Godin is the bestselling author of _Purple Cow_, _Linchpin_ and _Poke the Box_. He is considered one of the most influential business bloggers in the world.

