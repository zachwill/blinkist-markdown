---
id: 5899c5d495ee030004e5dd70
slug: all-the-single-ladies-en
published_date: 2017-02-10T00:00:00.000+00:00
author: Rebecca Traister
title: All The Single Ladies
subtitle: Unmarried Women and The Rise of an Independent Nation
main_color: 7A6BB5
text_color: 584D82
---

# All The Single Ladies

_Unmarried Women and The Rise of an Independent Nation_

**Rebecca Traister**

_All The Single Ladies_ (2016) chronicles the history and changing circumstances of single women in America. These blinks explain how cultivating female friendships and seeking alternative strategies to balancing personal and professional obligations help women achieve greater success and independence.

**This is a Blinkist staff pick**

_"A book-in-blinks all about capital-L Love: love of place, love of one's work, and most important, the romance of friendship between a single woman and her female familiars. Come for the history and politics, stay for the feels. The chapter on friendship made me cry."_

– Caitlin, Simplify Co-host & Podcast Lead

---
### 1. What’s in it for me? Understand the struggle of American single women for social equality. 

Single women helped build America. For some 250 years, women free of the obligations of house and children have been essential in helping the country win wars and develop urban centers.

Although single women have made many strides toward social equality in recent decades, there's no question that being a single woman in modern society is still no easy thing.

As companies public and private come to grips with the gender pay gap and media outlets continue to shine a spotlight on women's rights, the future of single women in America is looking brighter than ever.

In these blinks, you'll discover

  * why society still views single women with suspicion;

  * what it was like in New York City being a single woman in the 50s; and

  * why it's never been better to be single than it is today.

### 2. In the 1990s, the Hill-Thomas hearings made workplace sexual harassment a national talking point. 

It seems today that there are more single women in society than married women. Yet this wasn't always the case. Not 20 years ago, when an unmarried woman claimed she was sexually harassed at work, she was rarely taken seriously.

In 1991, a high-profile case showed how being unmarried made a woman untrustworthy.

Law professor Anita Faye Hill testified before a Senate committee against her former employer, Clarence Thomas, soon to be appointed to America's highest court, the Supreme Court.

As her boss years before, Thomas allegedly had made repeated sexual remarks to Hill, referencing lewd acts and the size of his penis.

Shockingly, Hill's status as a single woman would be used against her during the hearings.

Hill's allegations were brushed aside by many who supported Thomas, on the basis that as an older single woman, she was obviously incapable of maintaining healthy relationships with men. They even declared that Hill suffered from erotomania — delusions that powerful men were attracted to her.

The Senate Judiciary Committee eventually dismissed Hill's claims and went on to appoint Thomas to the Supreme Court. As the hearings were televised, the incident became national news. And given the outcome, many American women were outraged.

For one, the Senate committee that heard Hill's testimony was comprised entirely of men. At the time, only two women served in the Senate, with just a few more in the House of Representatives.

Inspired to take action, women across America realized that something needed to change in the halls of power. Just one year after Hill's testimony, an unusually large number of women ran for Senate seats.

Four of these candidates succeeded in winning their race to become senators — not a large number, but it was a start.

### 3. The social upheaval of the American Revolution allowed women to step into traditional “male” roles. 

For women living in the early American colonies, marriage was necessary for survival, as the law permitted single women few rights in society.

In the seventeenth century, unmarried women were forbidden by law to own property. The only exception was for widows who could inherit property from a deceased husband, and such cases were rare. A widow with no assets, however, clearly had nothing to inherit, leaving her reliant on community or family relations.

Single women weren't just marginalized by the law but also scorned by society. Single women were often referred to as thornbacks — a ray fish with a barbed tail and little flesh — underscoring society's negative view of older women who were seen as having little chance of finding a husband.

It wasn't until well after the American Revolution that women began to seek greater independence in society. What exactly sparked this shift in thinking and action?

When men left home in the 1770s to fight in the Revolutionary War, women left behind had to fend for themselves, taking charge of the family property or business. At the same time, new, radical feminist ideas inspired by the French Revolution made their way across the Atlantic and were discussed and debated by American women.

By the mid-nineteenth century, an economic and population boom in the United States further opened social opportunities for women.

Professions such as teaching and nursing needed new talent, and women stepped up. Thus more and more women had the opportunity to support themselves independently, without having to rely on a husband or birth family.

American novelist Louisa May Alcott, for example, remained single her whole life. She worked as a teacher, a nanny, a nurse and a magazine editor before publishing her novels, _Little Women_ and _Little Men._

### 4. Urban centers have allowed single women the freedom to explore new lives and new occupations. 

Parents might find the idea of letting a daughter move to the big city alone as a frightening prospect. But while a booming metropolis might seem dangerous, it provides many positive opportunities for a single woman.

Urban centers throughout history have opened society's doors for young, single women. In the sixteenth and seventeenth centuries, single women moved to the city for jobs, taking on roles as seamstresses, washerwomen and bakers.

During the Industrial Revolution, job opportunities for single women continued to grow with positions in textile factories, as domestic servants, as nurses and secretaries. Though many jobs were tough and unrewarding, many women preferred working hard over living in servitude to a husband back home.

The booming economic times following World War II demanded more women enter the workforce than ever before. For many single women, a job in the city was a chance to turn a life around.

Author Letty Cottin Pogrebin had an unhappy childhood; she was raised by a single mother who had left an abusive husband. After graduating from Brandeis University, Pogrebin moved to Manhattan in 1959.

In Manhattan, Pogrebin found work in publicity within the publishing industry. She rose through the ranks, earning more and more, eventually becoming the official promoter of Helen Gurley Brown's bestselling book, _Sex and the Single Girl_.

It wasn't just the promise of well-paid work that drew women to cities but also the lure of a lively, urban lifestyle. Pogrebin lived a bohemian life, riding through the city on a motor scooter and befriending artists such as playwright Edward Albee and actor Leonard Nimoy.

Today single women still flock to cities to find work, to live independent, exciting lives and to enjoy more social freedoms than are allowed "back home."

> Although only 25 percent of all Americans are single, this number jumps to 40 percent when considering exclusively urban centers.

### 5. Modern single women seek out platonic female relationships to empower and support them. 

Many single women long to find a soulmate. But what if your soulmate isn't a boyfriend or husband but a female best friend?

Women today often seek and forge powerful friendships with other women. These bonds can be as intense as a romantic relationship, without being sexual.

In 2009, the author's two friends, Aminatou Sow and Ann Friedman, met for the first time. After finding they had much in common, they decided to stay in touch, so became friends on Facebook.

Soon the two women began to go out socially on weekends. Eventually, Sow and Friedman started a blog together, writing about shared interests in literature, politics and fashion.

Their friendship grew deeper, becoming a source of trusting emotional support. They sought each other out for advice on personal matters and over career frustrations.

Certainly, they were far more than just "good friends" but they weren't romantic partners — the status of their relationship was a new concept for both of them.

It's true that modern female friendships provide single women with a powerful alternative to the traditional male-female family unit. The number of single women in society is growing, and women are relying on each other more than ever as a result.

The author's friends realized that they could provide each other with the love and support that the men in their respective lives hadn't been able to give.

Sow pushed Friedman to achieve successes of which she had never dreamed, and importantly, wasn't disappointed if she failed — support Friedman simply never had received from a male partner.

### 6. Being single doesn’t have to mean that you’re lonely; you can embrace the freedom being single gives you. 

Many women fear being single and the loneliness that can come with it. But time and time again, single women prove that living independently can be some of the best, most exciting years of a woman's life.

Publisher and journalist Rebecca Wiegand Coale rejects the word "single," pointing out that her years without a partner were her most sociable. Once in a romantic relationship, Coale did everything with her partner. She said that this sometimes felt lonely, as it was always just the two of them.

As a single woman, however, Coale's professional life blossomed. She used her free time to network and meet new colleagues, join sports teams and build new friendships with other single women.

Coale found that she had not just one but many men in her life who were able to fulfill a range of needs and roles. She dubbed this group her "Gaggle," which became the title and inspiration for a blog and book about modern dating that she wrote with her best friend, Jessica Massa.

But what if you're a single woman who is socially introverted? Spending time alone isn't synonymous with loneliness. Single or not, being alone can be a wonderful thing.

New Jersey hairstylist Kitty Curtis remembers being afraid of being alone after the end of a relationship. But to her surprise, Curtis discovered that she loved having time to herself. She didn't have to consider someone else's interests constantly, and that was refreshing.

Curtis decided to follow her dreams and desires, packing her bags to travel around the world, from Mexico to Europe and across Asia. In the process, she built a life that eluded her when she was with her previous boyfriend, as he simply didn't want the same things.

> _"Do you know how rare finding a moment's peace has been for women throughout human history?"_ — Mallory Ortberg, American author

### 7. Single, sexually active women are seen as a threat to men in relationships and to society at large. 

There are plenty of stories about single guys never calling a woman back after a one-night stand. Yet today's single women are slowly but surely learning how to take back control of their sex lives.

The result? Single, sexually active women are now seen by men as the real threat!

As women began to enjoy sexuality for the fun of it, they can become far less needy and insecure in relationships with men.

Frances Kissling, a liberal Catholic and pro-abortion activist, left a ten-year monogamous relationship and decided to take ownership over her sex life. She wanted to be adventurous and not let the fear of pregnancy stand in her way. To do so, she had her fallopian tubes tied, a sterilization procedure.

As a result, Kissling finally found herself on an equal footing with men. She also found that this new footing was indeed threatening for her partners, who were accustomed to being the one who could easily leave or refuse to commit emotionally.

In the media, sexually liberated women are often portrayed as a threat to social and cultural norms. "Hookup" culture on college campuses, where partners meet up for no-strings-attached sex, has caught media attention and has been documented in many articles. Some journalists and readers seem to think that hooking up is a dangerous trend and one that is on the rise.

Studies show, however, that young women today are far more responsible about sex than their baby-boomer mothers. Research by the Centers for Disease Control and Prevention shows that over the past 20 years, the percentage of sexually active teenage girls has dropped from 51 to 43 percent.

### 8. Many women delay marriage to secure professional success before becoming mothers. 

Only recently have women been allowed to pursue higher education. Before the nineteenth century, women were taught to read only so they could read the Bible. The single purpose of women in society was to care for a husband and future children.

Unfortunately, modern women still struggle against this expectation today.

Many women juggle the challenges of managing a family and a professional career. When Barbara Walters retired as a television journalist in 2004, she said that she had had to sacrifice her personal and family life to succeed over her 40-year-long career.

When it comes to chasing ambition and achieving success, single women just have it easier. When Janet Napolitano was nominated as the head of Homeland Security in 2008, Pennsylvania Governor Ed Rendell said she was "perfect" for the role. Why? Because she had no family to distract her from her work.

Although Rendell was roundly criticized for his remarks, they reflect attitudes still widely held in American society.

For women who want to build a career and have a family, marriage delay is a popular strategy.

Senators Kirsten Gillibrand and Amy Klobuchar, for example, and Loretta Lynch, the first African-American to become attorney general, all married later in life. They made sure they were well-established in their chosen professional field before starting a family.

Marriage delay isn't just something a handful of successful women are doing, however. In 2000, only 34 percent of women aged 25 to 34 had never married. By 2008, that percentage had risen to 46 percent.

> _"The millions of women who are staying single play a transformative role in the way we perceive women's relationship to work."_

### 9. While men still hold the reins of power, we must continue to fight to raise all women up. 

Although much progress has been made, the fact is that men still dominate the ranks of the world's most rich and powerful people.

In 2012, only 20 of America's 1,000-largest companies employed a female CEO, according to _Forbes_. And in 2014, only 4.8 percent of America's top-earning CEOs as listed by _Fortune_ magazine were women.

Though these numbers do represent major milestones for women in business, they also show that as a society, America has a long way to go.

The disparity between men and women is also reflected in the gender pay gap. For every 78 cents a woman earns, a dollar goes to a man performing the same job.

Of course, statistics can also be misleading. The road to gender equality is not only long but also complex; there are many other social issues at play.

In other words, success for women is relative and may come at the cost of other social groups.

In 2010, journalist Belinda Luscombe conducted a study that revealed single women living in the city without children earned 8 percent more than men in the same age bracket.

This result, however, is because single professional women were being measured against a larger urban population that includes many poor, immigrant men who are forced to work for very low wages.

Yet when white women are compared to white men with a similar educational background, the woman's salary is always lower.

We should not forget that many women can support a career only with the aid of underpaid immigrant housekeepers and child caregivers.

As women continue to fight for gender equality, other marginalized groups must not be left behind.

### 10. Final summary 

The key message in this book:

**Being a single woman doesn't mean you're sad, flawed or lonely. Today, single women living in cities are leading the way and demonstrating that a woman doesn't need a man to find love and support, follow a successful career and lead a fulfilling life.**

Actionable advice:

**Single? Take advantage of it!**

If you're single and feeling sad, don't forget that being single is a time of great freedom. This is your chance to get out there and explore new adventures, find new friends and live the dreams you thought you'd never have time for.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _We Should All Be Feminists_** **by Chimamanda Ngozi Adichie**

In _We Should All Be Feminists_ (2014), Chimamanda Ngozi Adichie expands on her much admired TEDx talk to address our deepest misconceptions about feminism. By masterfully interweaving personal anecdotes, philosophy and her talent for prose, she explains how men and women are far from being equal, how women are systematically discriminated against and what we can do about it.
---

### Rebecca Traister

Rebecca Traister is a journalist and writer, focusing on working women and feminist perspectives on cultural and political issues. Her work has appeared in _New York Magazine_ and _Elle,_ among other publications. She also wrote the award-winning book, _Big Girls Don't Cry._

