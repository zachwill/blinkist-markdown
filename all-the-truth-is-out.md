---
id: 548a1b3b3363370009840000
slug: all-the-truth-is-out-en
published_date: 2014-12-25T00:00:00.000+00:00
author: Matt Bai
title: All the Truth Is Out
subtitle: The Week Politics Went Tabloid
main_color: E5372E
text_color: B22B24
---

# All the Truth Is Out

_The Week Politics Went Tabloid_

**Matt Bai**

_All The Truth is Out_ details the sudden transformation of political journalism in the late 1980s, as political reporters shifted their focus from policy to the personal lives of politicians. Using the rise and fall of former presidential hopeful Gary Hart as a starting point, it shows how political journalism and politics in general have changed both in form and content.

---
### 1. What’s in it for me? Discover how today’s political coverage became so vapid. 

Who is Gary Hart?

If you don't know the answer, you were probably too young to vote in the US in the 1980s. During that time, Hart was arguably one of the most promising Democratic presidential candidates ever — a political rockstar with a promising future.

Nowadays, however, he's an obscure historical figure, whose reputation has been forever marred by a sex scandal that ruined his campaign and completely changed political journalism forever.

The reverberations of Hart's legacy are felt even to this very day, when politicians' interactions with the press are careful, calculated and without substance. Gone are the days when presidential hopefuls utter a candid word about their policies.

Today's politics is all about dodging blunders and surviving the scrutiny of journalists playing amateur detective.

In these blinks, you'll learn

  * why John Kerry regrets uttering that one fateful sentence during an interview with the author;

  * why Bill Clinton survived his extramarital affair, while Hart didn't; and

  * why JFK would have been eaten alive by today's political journalists.

### 2. Gary Hart was one of the most exciting and intelligent presidential candidates of his generation. 

Politicians didn't always enjoy the rock-star celebrity status that they do today. Before Barack Obama, before Bill Clinton there was Gary Hart, the Democratic presidential candidate who ran for office in 1984 and 1987.

Hart was a special breed of politician whose rise to stardom coincided with a need for political reform. He formally entered the political arena a US senator from Colorado with a strong academic pedigree, and came from a strong Christian upbringing, which inspired him to earn a degree from Yale Divinity School before doing the same at Yale Law.

According to the author, Hart possessed a unique and sharply-honed intellect — one that was different from Newt Gingrich's abilities to recall entire libraries of philosophical texts verbatim, for example, or from Clinton's quirky ability to discuss policy while simultaneously solving a crossword puzzle.

Hart's prodigious intellectual gift was his ability to mix theology and technology with history, culture and politics — to broaden the scope, assimilating all available areas of knowledge and paint a bigger picture of what needs to be done extemporaneously.

Further adding to his credibility, Hart had an uncanny ability to "peer into the future" of policy and politics.

This gift was so well-known that it even earned its own name in Richard Ben Cramer's 1992 book on the 1988 presidential election, _What it Takes_. Cramer called Hart's prophetic quips "_Hart Facts_."

For example, in 2002 Hart predicted that the United States would engage Iraq militarily, that it would prove to be a catastrophe America would struggle to end and which would further exacerbate terrorism. Does that ring any bells?

He also remarked that growing inequality and market recklessness were bound to lead to a depression. Surely enough, we are still recovering from the Great Recession of 2008 to this day.

### 3. Hart enjoyed a meteoric rise through American politics and was a clear favorite leading up to the 1988 presidential election. 

Hart had gained considerable experience during an unsuccessful nomination bid in 1984, and his reformist policies left him well positioned as a front-runner for the Democratic presidential candidacy.

After the Democratic primary in 1987, Hart emerged as a clear front-runner, with double-digit leads in most polls over other Democratic hopefuls such as Rev. Jesse Jackson. In fact, his strongest challengers came from the new wave of young politicians, such as Joe Biden, who aimed to emulate Hart himself. 

These contenders became known as the "new Garys," but despite their attempts to emulate his success, the original Gary easily beat them at the polls.

Despite being the party's forerunner, Hart still considered himself to be an anti-establishment Democrat. Hart's unorthodox approach to his presidential campaign lends credence to this perception. He focused his campaign on big ideas and eschewed political grandstanding, despite the fact that this strategy alienated him from the press.

Hart spent much of his campaign away from interviews and from the barbeques where presidential hopefuls typically schmoozed with big industry. Instead, he toured the countryside, giving speeches at universities and presenting his policy on post-Cold War America and economic reform along the way.

Hart also made it clear to the media that he would only talk policy and not about polling data, nor would he participate in the kinds of personal interviews that could land him on the cover of _Time_ or _Newsweek_.

Obviously, this did little to get on the press's good side, whose appetite for a juicy story only grew as top television commentators such as Jack Germond began referring to Hart as "an unstoppable force in presidential politics."

### 4. Media scrutiny serves a valuable purpose, but not the kind that ultimately derailed and destroyed Hart’s campaign. 

Prior to the 1988 presidential election, journalists didn't even consider delving into the personal and matrimonial lives of presidential candidates.

However, even in those times, a certain level of media inquiry into the lives of a prospective presidential candidate was expected. After all, they should be vetted and scrutinized for the public's benefit.

Hart was no exception to this rule. In fact, journalists who looked into his past during his first presidential nomination campaign in 1984 described Hart as "cool and aloof" and a "loner."

In addition, journalists also discovered both that he had changed his name from Hartpence to Hart and that he had two registered birthdates.

While none of this news was particularly damning — or even interesting — this line of inquiry represents how a new breed of journalists were looking for something juicier than policy discussions.

In 1987, Hart was once again brought under the magnifying glass, and this time it cost him his candidacy:

The first blow came in April of that year, when a reporter at the _Miami Herald_ received an anonymous tip that Hart was having an extramarital affair with Miss South Carolina, Donna Rice.

In order to find out more, a _Herald_ news team staked out Hart's townhouse in Washington D.C., something journalists had never done before. After witnessing both Hart and Rice entering the townhouse, they published their story, thus kicking off a media frenzy that would engulf and ultimately ruin Hart's campaign.

The second blow came at a campaign stop, in which Hart was asked point blank whether he'd committed adultery _before_ the Rice incident. Although he denied it, Hart's mumbling, ambiguous response made it obvious that he had.

These incidents earned Hart a reputation in D.C. as a womanizer, while rumors circulated that his marriage was nonmonogamous.

Eventually, the story reached a fever pitch that caused Hart to rescind his candidacy before the 1988 National Democratic Convention, where he received only a single vote.

> _"Ideas are not all there is to a campaign: human beings choose which ideas will govern."_

### 5. Past presidents’ and presidential candidates’ indiscretions were largely ignored. 

One of the interesting aspects about Hart's capitulation in 1988 wasn't that journalists investigated his private life, but rather that, in the past, the private lives of politicians were seen as distinct from their viability as professionals.

In fact, prior to Hart's downfall, journalists knew about a number of presidents' and candidates' extramarital affairs: Franklin D. Roosevelt, John F. Kennedy and Lyndon B. Johnson, for example, were all adulterers — both _before_ and _during_ their presidencies.

For instance, when the _New York Times_ reporter R.W. Apple was tasked with following president Kennedy on one of his trips to New York City in 1963, no one seemed to care about what he discovered:

Apple was told to hang around the hotel lobby where Kennedy was staying in order to observe the visits from other important politicians. Instead, however, much of Apple's time was spent observing a certain prominent actress coming and going from the hotel lobby, presumably visiting Kennedy.

When Apple reported what he saw to his editor, the reply was simply: "no story there."

Hart, too, was widely known to have been with other women before his campaign after having been separated from his wife on two occasions. He even commented to a journalist at one point that he had a "reformist marriage."

So, why were things so different back then?

The media avoided reporting on politicians' private indiscretions not out of loyalty, but because they didn't think it was relevant to the public interest. Reporters didn't see adultery as especially relevant, that is, unless these indiscretions were somehow related to some larger political context, such as a candidate's economic plans.

They focused instead on more substantial issues, such as whether a politician took bribes or stood up to power. At the time, adultery was simply immaterial.

So why was Hart's indiscretion so damaging, and how was political media coverage changed? Our following blinks will answer these very questions.

### 6. The role of political journalism changed drastically since Watergate. 

The transformation from a kind of journalism which considered JFK's tryst with Marilyn Monroe to be a non-story into the vulturous circus that it is today can be summed up with one word: Watergate.

After the Watergate scandal of the 1970s, there was suddenly no greater prize in political journalism than unearthing the lies of politicians.

The Watergate affair was a political scandal that occurred in the United States during the Nixon presidency, centered around a break-in that occurred at the Democratic National Committee headquarters at the Watergate Office complex in Washington, D.C. During the investigation, it became apparent that the Nixon administration and Nixon himself were involved in the incident, and had even attempted a cover-up.

Ultimately, Nixon became the first president in American history to be forced to resign.

This exposé was published by _The Washington Post's_ Bob Woodward and Carl Bernstein, who were subsequently launched into stardom and financial prosperity and were later played by Robert Redford and Dustin Hoffman in a cinematic re-enactment.

Not only that, but Watergate became the pinnacle career achievement that countless other journalists continue to try to recreate.

What's more, the Watergate scandal brought journalism to the TV which saw the creation of the Washington _pundit_ — those supposed political experts whose opinions are sought out by news agencies all over the world.

TV news had just arrived in 1980 with Ted Turner launching CNN, whose signature program _Crossfire_ hit the airwaves shortly after. This represented the advent of televised debate shows, in which two partisans would spectacularly square off about a particular issue live on air.

In many ways, this precipitated a change in reporting — one in which opinion and analysis held primacy over objective and factual reporting.

### 7. Technological advances now allowed newscasters to go live and shuttle information at tremendous pace around the country. 

The post-Cold War period saw not only great social and economic change, but also technological advances that ushered in a new era of news broadcasting. Specifically, the age of Electronic News Gathering (ENG) had a profound effect on how politics was reported.

Electronic news gathering was the transition from using film to modern videotape at the start of the 1980s. Its effects were profound, allowing news to be broadcast from anywhere as it happened by using satellite technology.

This contrasts sharply from the old approach, in which "going live" meant filming at the scene and handing the physical tape to someone who would then speed off on a motorcycle to the studio. This tape would need to be converted to video, synced with the sound and sent to an editor. And all this before the evening news deadline!

But in 1986 CNN began providing its reports with lightweight "flyaway dishes," which allowed breaking news interviews to be reported live from anywhere.

Thus, the 1988 presidential campaign became the very first to be covered live by a 24-hour news channel.

This not only changed the speed at which the industry could provide news to the public, but also the criteria for what constitutes news. From this point onward, an acceptable story didn't need to be _important_ so long as it was _engrossing_.

For instance, before 24-hour news the stories that made it on air were chosen based entirely on how relevant they were to the public interest. Now, however, what was considered "newsworthy" was simply whatever could capture our attention or be sensationalized by focusing on spontaneous mishaps rather than substance.

For example, Michael Dukakis, who succeeded Hart as the Democratic nominee, isn't remembered for the substance of his policy on the military. He was immortalized instead for a publicity stunt gone wrong during which he was filmed in a ridiculous custom, wearing an oversized helmet in a tank.

So what have these changes meant for American politics and political news coverage today? Our final blinks will answer this question.

### 8. American and world politics changed dramatically after the media spectacle that disgraced Hart. 

This shift in priorities for journalists brought in a new era of politics and political coverage that eventually spread across the globe.

Another renowned political journalist (who was granted the only in-depth character interview with Hart in 1987), wrote in the _New York Times_ that Hart's fall from grace had ushered in an era of _confessional politics_ :

Politicians and their spouses were now being coached by their advisors to disclose parts of their private life that before would not have been open to the public.

For example, weeks after Hart's withdrawal, the Democratic congressman Barney Frank announced that he was gay. The newly fascinated press also turned their sights to other presidential candidates, tailing Democratic governor Richard Celeste and delving into the sex lives of Jesse Jackson and George H.W. Bush.

This shift in the media's interest from policy to personal matters wasn't isolated to the US — it could also be seen in other parts of the world. By 1989 heads of state in both Greece and Japan were forced to step down amidst allegation of sex-related scandals.

This trend has continued unabated, leaving political journalism to now be solely concerned with uncovering lies, inconsistencies or character flaws in politicians. While Hart was essentially shoved to the political wilderness, his name will nonetheless be forever associated with scandal in what has become known as the _Hart Effect_.

Journalists today see themselves as amateur detectives. And while their skepticism towards politicians is indeed valuable for public interest, _New York Times_ editor Hendrik Hertzberg believes that the media has completely discarded discretion along with any sense of context.

The personal interest perspective is a sharp contrast with the sober, boring journalism of 50 years ago!

> _"If post-Hart political journalism had a motto, it would have been: We know you're a fraud somehow. Our job is to prove it."_

### 9. The measure we now use to assess politicians has more to do with showmanship than governance. 

The media's obsessive focus with scandal-finding and superficial, attention-grabbing factoids has meant that presidential candidates are now admired for attributes that have little to do with governance.

This becomes glaringly obvious when we compare Gary Hart's political career with that of Bill Clinton, who, despite his many parallels to Gary Hart, emerged relatively unscathed from his indiscretions.

In terms of policy, they were quite similar. Both Hart and Clinton espoused Vietnam War-era pacifism and pushed towards a technology-based economy. Their private lives were also similar, in that they both experienced a political sex scandal:

Clinton was ultimately impeached during his second term after the exposition of his extramarital relationship with the 22-year-old intern Monica Lewinsky.

However, Clinton did not suffer political ruination as a result, and to this day remains active in American politics. Why?

Clinton — unlike Hart, who was always thought of as an aloof loner — was willing to play to the public. He played the saxaphone on street corners, revealed the kind of underwear he wore on MTV and lied repeatedly about his affair.

We've now reached the point where we confuse actual leadership qualities with the ability to entertain and endure. With the age of presidential debates and pundits' analyses dominating presidential campaigns, what we consider qualities of leadership have been reduced to little more than showmanship.

Public admiration of candidates is now measured by how easily they deflect criticism of their character, and not their ideological or political positions.

This was a great boon to presidents like Clinton, who would become known as one of the most talented statesmen of his time. He had a magnetic personality, but wasn't above telling the people what they wanted to hear even if it differed from reality.

But these are precisely the qualities that are insubstantial when it comes to governance!

### 10. Presidential candidates have become more insulated as a result of the ever-prying eyes of the media. 

But what if a presidential candidate has a great policy approach that he or she wants to share with the public in detail?

Too bad! Today's media has no interest in policy. Even if politicians _wanted_ to detail their policy ideas, they'd have no one to talk to!

As a result, presidential candidates don't even bother explaining their theories anymore. The olden days in which presidential candidates would grab a beer with reporters and go over the nuances of their policy plans are over.

Candidates know that any poorly worded phrase, anything that can be taken out of context or any candid remark could snuff out their careers in no time. Just look at John Kerry, for example, whose presidential ambitions went up in smoke largely due to an interview with the author.

During a four-hour interview, Kerry commented that terrorism would never be defeated absolutely, so we should aim to get to a point "where terrorists are not the focus of our lives, but they're a nuisance."

The media disingenuously took Kerry's quote to mean that he wasn't sufficiently anti-terrorism. None of the coverage bothered to look for context within the entirety of the interview, or what Kerry's anti-terrorism proposal was.

Kerry's aides even confided to the author that their biggest mistake was that interview and that they shouldn't have spoken to him.

No presidential hopeful wants to make a blunder like Kerry's. So, candidates now avoid discussing substantial issues altogether and instead stick to vacuous one-liners.

You can think of Obama's mantras of hope and change, for instance, as the rhetorical equivalent of rainbows and unicorns. 

Obama, much like Clinton, is great at saying things that sound nice but are of little relevance to the real world. Examples abound if we examine the long-term economic or global-political trajectory of America against Obama's political accomplishments.

With every word representing potential ruin, politicians have adapted by shielding themselves with rhetorical armor.

### 11. Final summary 

The key message in this book:

**Nothing has changed the media's political coverage more than Gary Hart's sex scandal from the late 1980s. Not only did this unfortunate scandal represent a major shift in the priorities of political journalism; it also completely transformed the content and quality of political discourse and even our conception of what constitutes good political leaders.**

**Suggested further reading:** ** _A Quiet Word_** **by Tamasin Cave and Andy Rowell**

_A_ _Quiet_ _Word_ explains what lobbyism is, how it works and why it can be dangerous for democracy. The authors reveal the extent of lobbying today, detail different strategies used by lobbyists to influence governments, and offer a solution to help defend democracy.
---

### Matt Bai

Matt Bai is a political journalist who covered the 2004 and 2008 presidential campaigns during his tenure at _The New York Times Magazine_, and is currently the national political columnist for _Yahoo! News_.

