---
id: 54bd256d633234000a770000
slug: alone-together-en
published_date: 2015-01-21T00:00:00.000+00:00
author: Sherry Turkle
title: Alone Together
subtitle: Why We Expect More From Technology and Less From Each Other
main_color: F88F34
text_color: 784519
---

# Alone Together

_Why We Expect More From Technology and Less From Each Other_

**Sherry Turkle**

Technology has changed our lives tremendously — in some ways for the better and in some ways for the worse. _Alone Together_ explains how even though a great deal of new technology, like smartphones and social media, is supposed to bring us together, it actually makes us lonelier in the end.

---
### 1. What’s in it for me? Learn how today’s tech connected world is actually making us more miserable. 

It's never been easier to connect with another person. Today, thanks to digital devices we're living fully networked lives; we're available to chat, answer email or work around the clock.

At the same time there's also been an evolution in robotics, so that robots not only take on difficult or dangerous jobs for us but also care for us — and even have demands of their own.

These blinks explore these two strands of today's digital culture by looking into how technology both enhances our everyday experiences and changes the way we relate to each other — not necessarily for the better.

In the following blinks, you'll learn

  * how a seal-like robot has been designed to take care of the elderly;

  * why many girls use "shrinking" software on profile pictures to appear thinner; and

  * how being constantly available online actually makes us anxious.

### 2. You won’t have to take care of your elderly parents in the future; robots will do this for you. 

Have you ever wondered who'll take care of your parents as they get older? You? Your siblings? Someone else?

Well, many people believe that robots will eventually become our caretakers. In some places like Japan, they may even be a necessity.

Some 25 years ago, Japanese demographers concluded that Japan wouldn't have enough young people to take care of the elderly in the future. So they began developing a robot called Wandakun that would be able to help.

Wandakun is a fuzzy koala that purrs, sings and speaks a few phrases when it's touched. It can serve as a companion to the elderly. Speaking about Wandakun, one 74-year-old man said, "I fell in love after years of being quite lonely...I swore to protect and care for the little animal."

For many people, robots have changed the very notion of "care." We used to think "care" meant caring _about_ someone, but now it often means _taking care of_ someone — something a robot may be able to do.

In 2005, the International Symposium for Contemplative Studies discussed the potential uses of caregiving robots. Speaking of a machine that cuts people's toenails, one attendee said, "That is a caring computer. Or [one that] talks with you if you are lonely. Same thing."

Some people don't think there's an important difference between a real person and a robot performing caregiving tasks. They're both _taking care_, which is enough.

Miriam, a 72-year-old who lives in a nursing home, is quite fond of her seal-like robot Paro, with whom she talks and shares secrets. In the end, Paro doesn't and can't truly care _about_ her, but the nursing staff and scientists who created Paro don't think this is a problem.

> _"We ask technology to perform what used to be love's labor: Taking care of each other."_

### 3. When robots ask us to care for them, the line between the animate and inanimate begins to blur. 

It's not only the elderly and their caretakers who've started using robots as companions. A wider audience is also beginning to accept them. Why?

One reason is that we start to forget that robots are machines when _they_ start asking _us_ for care.

Tamagotchi _,_ a widely popular digital toy in the 1990s, illustrates this phenomenon. Tamagotchis were rather unsophisticated devices, with small screens that displayed an animal-like creature with basic needs, such as eating and sleeping.

They needed to be "taken care of" continuously. Children fed and bathed them, and became quite attached to them. Many children believed their feelings for their Tamagotchis were reciprocated. One nine-year-old said of her "deceased" Tamagotchi, "She was loved; she loved back."

As robots become more lifelike, our ethical understanding of them will also change.

One researcher developed a test to measure how "real" an inanimate object has to be before people feel uncomfortable when it's hurt. For the study, participants were asked to hold three creatures upside-down: a Barbie doll, a Furby (Furbies are bird-like robots that say, "me scared," when held upside down) and a living rodent.

Participants gave the rodent a wide berth and didn't dare turn it upside-down, whereas they didn't mind handling the Barbie roughly. They felt fine doing things like slinging the doll around by its hair.

The Furby, however, presented an interesting case; when the Furby expressed discomfort, most participants felt guilty after 30 seconds or so and turned it back over.

So when robots _feel or act_ more real, many of us can experience an ethical dilemma when asked to "hurt" them.

> _"Demands relate into care, and care into feelings of caring."_

### 4. Robots can keep the lonely company; in some cases, a robot is preferable to a family member! 

We've seen that certain robots can seem "real" enough that we are compelled to consider what they "need."

And for some people, robots are real enough to become actual social companions as well, fulfilling _our_ social needs.

Tim, a 53-year-old man, felt guilty about putting his mother in a nursing home, even though he visits her several times a week. He says he "used to hate to leave her in that room."

Yet Paro, the seal-like robot that is sensitive to touch and can actually understand a few words, has made him feel much better about his mother's social interactions. "She puts it in her lap," he says. "She talks to it. It's...less depressing."

Even though Paro isn't alive, Tim feels his mother is "less alone" now that she has the robot.

And certain robots are more than just "real enough" — some people actually _prefer_ robots to live companions.

For example, you don't have the same responsibilities with a robot that you would with an animal. Robotic pets don't need as much attention as live ones; they don't make waste, and they can't do as much damage to property.

Yolanda, an 11-year-old girl who plays with a robot dog called AIBO, learned this. She initially thought AIBO would be a good alternative for kids who can't have a real pet, but has since decided AIBO might even be _better_. "You can turn [it] off," she said, "and it won't bug you."

So robots aren't just _replacements_ for our traditional companions; some see robots as better than human or animal companions, too.

### 5. As robots become more lifelike, we can get very attached to or even emotionally hurt by them. 

Robots have become an integral part of our culture, and, in the future, may become more and more like humans. Yet what sort of effects will these lifelike robots have on us?

Well, it's possible that we'll become more attached to them. Older people with few friends or companions are particularly susceptible to this, becoming enchanted by robots quite quickly.

Consider the story of Andy, a 76-year-old living in a nursing home, who was struggling to make new friends as he missed his family and ex-wife. He was given a baby robot, called My Real Baby.

He began to interact with it as if it was alive, and even named it after his ex-wife. He now uses the doll to remember the good times he spent with her. He also uses the doll to discuss his divorce.

For people like Andy, bonding with a robot can provide great psychological relief. It can also be easier than bonding with real people, and perhaps even more cathartic.

Our increasing attachment to robots has consequences, however. Children are especially likely to be hurt, for example, if a robot breaks down or malfunctions.

The author observed this in the interaction between a 12-year-old girl named Estelle and her robot, Kismet.

As a human-like robot, Kismet is gentle and soft-spoken, with big eyes and lifelike features. Yet when it suffered a glitch while Estelle was playing with it, she thought that she had somehow done something wrong.

As a result, Estelle felt that Kismet didn't like her, that the robot was, in response to her actions, acting "sullen" (when actually, it had just malfunctioned). When children grow attached to robots that are more lifelike than not, they could get their feelings hurt if something goes technically awry.

### 6. Robots can cause people to neglect, or even lose track of altogether, their human relationships. 

Are relationships with robots always positive? Or perhaps we should ask, are human relationships always positive?

Interestingly, certain people may be prone to neglect their human relationships in favor of robotic ones. Some people may become so involved with their robots that they forget the people around them.

Edna, an 82-year-old who cares for a My Real Baby robot, has been known to neglect her granddaughter Gail when the robot is also present. When Edna and Gail are alone, Edna can focus on her granddaughter; but when the robot arrives, Edna will divert her attention to the "baby."

Edna can become so engaged with her robot that she basically ignores Gail's pleas for attention.

Yet some people may even _want_ to replace their human relationships with robotic ones. Wesley, a 64-year-old who's been divorced three times, is one example.

He believes he always ends up hurting others as he's self-centered, yet with a robot, this wouldn't be an issue. A robot thus provides needed social interaction, without the risks.

Wesley even thinks a robot can provide _more_ for him than a real woman could, because he doesn't have to consider the robot's moods or emotions. He also finds it burdensome when real people grow attached to him, because he fears hurting them.

A robot well suited to Wesley's interests is now being developed, called Roxxxy — the first "sex robot." Roxxxy can't move, but she has warm skin and a pulse. She can talk, and what's more, an owner can even choose from a host of different personalities for her.

Sex robots were originally intended to be a "better than nothing" solution, but for people like Wesley, they may be _better_ than anything else.

### 7. Teens stress over how they are perceived online, and spend tons of time improving their identities. 

These days, people — especially teenagers — are spending more and more time on the internet, creating and building their online "identity."

Developmental psychologist Erik Erikson said that _identity play,_ a term that describes when people experiment with who they are, is an important part of adolescence. Today, there are all sorts of new opportunities for identity play, thanks to technology.

People can create _avatars_, which are a graphical representation of a person's alter ego or character, in video games or virtual-reality environments. They can then use the avatar to explore or "try on" different personalities.

More generally, social media sites offer another platform for identity play. Mona, a teenager, is always updating her Facebook profile, as she fears she doesn't appear interesting enough. Her friend Helen faces similar challenges, and has asked herself, "Who should I friend online? I really want to only have my cool friends listed, but I'm nice to a lot of other kids at school."

Adolescents like Mona and Helen make their decisions on Facebook very carefully. They experiment with their real identities by shaping and reshaping what they reveal and don't reveal about themselves online.

Of course, maintaining an online identity can be stressful, as many people worry about how their image comes across to others.

Many girls now use "shrinking" software on their profile pictures to make them appear thinner.

Boys are also affected. Brad spends a lot of time perfecting his "cool" online persona. He feels he has to conceal parts of his personality, however, such as his love for _Harry Potter_, because it's "uncool."

Brad is afraid that if he doesn't write out all his interests and positive attributes, he won't seem "cool," but if he brags too much, he'll appear vain.

Feelings like this are a normal part of adolescence, but young people are now experiencing adolescence on a broader, more public stage than ever before. This can add extra stress, because any mistake they make is recorded for everyone to see.

### 8. Talking on the phone is less common these days, as people now prefer to text or send emails. 

Did you spend hours talking on the phone with friends as an adolescent? Today teens are more likely to text and message each other constantly, instead of calling.

Even adults tend to use the phone less, partly as they think phone calls now are too demanding.

Randolph, an architect, says, "Now that there is e-mail, people expect that a call will be more complicated...People expect it to take time, or else you wouldn't have called."

Tara, a lawyer, also thinks phone calls must be urgent, or else you "would have sent an e-mail."

Granted, instant messaging and e-mail can be more convenient for scheduling appointments, for example. Leonora, a chemistry professor, schedules all her appointments via e-mail, sometimes months in advance. She's too busy to do it otherwise, and she's also noticed that her friends prefer this too.

Teenagers especially like to text often during the day, considering this sort of communication easy and convenient.

Seventeen-year-old Elaine says she sends each of her six best friends at least 20 messages a day. She also sends an additional 40 instant messages from her computer.

She thinks texting is easier, because "when you can think about what you're going to say, you can talk to someone you'd have trouble talking to [otherwise]."

So people are calling each other less, but many of us are still expected to be available at all times — thanks to our phones.

### 9. With smartphones, we’re expected to be available all the time, yet this can cause stress. 

Can you remember the last time you intentionally went somewhere without your phone?

When you do leave your phone behind, accidentally or intentionally, it may make you nervous. These days, we're expected to be reachable at a moment's notice, at all times of the day.

A phone has become a necessary social item for many teenagers; yet aside from the benefits of texting friends dozens of times a day, a phone can also keep a troubled teen in touch with parents.

Often, this is exactly why parents give their children a phone in the first place: to stay connected in a crisis.

Julia vividly remembers when she was eight and living in New York City during the terrorist attacks of 9/11.

She and her classmates were taken to the school's basement, and kept there for hours just to be safe. Recalling the event, Julia feels that a phone would've helped her a lot, because being able to speak with her mother would've made the situation more bearable.

In an emergency like 9/11, a phone can be an enormous help, but our phones also can cause us stress. We can't ever be truly alone, or have private time to ourselves.

Hope, a real estate broker, often goes hiking in the woods and takes her BlackBerry smartphone with her. Her husband calls her every 30 minutes to stay in touch, and she keeps answering her phone until she loses reception.

She feels relief, however, when she's out of cell range, saying, "Thank goodness. I need a rest."

Being constantly available can be quite a burden, especially when you're trying to relax in nature!

> _"Tethered children know they have a parent on tap, a text or a call away."_

### 10. Online identities can mean freedom for some people, but a prison for others. 

Sharing ideas and meeting people online can be a positive experience. But be wary, as online identities shouldn't take over your real life.

Often when a person is having a hard time in real life, turning to the internet is a relief, a chance to live another life in a digital skin, with an online persona.

A shy person, for instance, might experiment with being more outgoing online. Young people can also practice flirting without the pressure of actually seeing the object of their attentions.

For some people, this can be life-changing, as it was for one young woman with a prosthetic leg. She lost her leg in a car crash, and afterward, it was hard for her to resume a normal life, or have sexual relations.

So she created an online avatar, a character that also had a prosthetic leg, and used this new persona to practice talking about her situation in virtual relationships. This helped her grow more comfortable with herself, and through this, she learned to accept her new body.

Unfortunately, having an online life isn't always positive. Some people's online lives take over their offline realities.

This happened to Adam, a 43-year-old who struggles with his addiction to online games. He works two jobs, but feels them "slipping away" under the pressure of his gaming, as he plays up to 15 hours a day.

He wants to cut back, but says he can only feel relaxed and happy when he's playing. He has little social contact outside of his games, and isn't working toward any personal or professional goals.

People like Adam can become so engrossed in their online identities they forget the rest of the world. Online lives can be liberating, but they also can consume you.

### 11. The public nature of our online lives, and the permanence of online actions, can cause anxiety. 

Do you ever browse Facebook photos of people you barely know? If so, you're not alone.

Following people's lives online is something most of us do. It's also the source of a great deal of stress.

Nineteen-year-old Chris says that "online stalking" has become routine for him and his peers. His friends upload photos to Facebook, and label and tag people in them. This makes it very easy for others to follow what they're doing and who they're with.

Chris looks at photos of girls he finds attractive, and learns a lot about them through their online profiles. Are they popular? Do they have boyfriends? What are their interests?

At the same time, Chris feels apprehensive as he knows other people do the same to him. Anything he does might end up online for everyone to see, which makes him anxious. However, even though he doesn't like the idea of being "stalked," he can't stop himself from stalking others.

The permanence of our actions online is another source of stress. Anything you do online can and will be permanently recorded. A lot of people police themselves heavily when using the internet, because they're afraid anything they say could come back to haunt them.

Teenaged Brad is very wary of the internet for this reason. He never posts anything spontaneously, but chooses his words carefully. He wants to present himself as a calm, resolved and responsible person.

Brad, and many others just like him, don't feel comfortable just "being themselves" online.

### 12. Many people feel overwhelmed by technology, and want to connect on a human level again. 

Do you ever wish you could just get away from technology, if even for a short time?

It's not an uncommon thought. Some people go on self-imposed "media fasts" so they have to reach out to others via phone or even person-to-person, the "old-fashioned" way.

Sometimes we do need a break from the always-on pressures of the internet. When Brad gets too stressed by online social responsibilities, he goes off networking sites and socializes in person instead. He calls friends, and makes dates to meet in cafes or at home.

Brad feels that turning away from social media like this means "sacrificing three hollow conversations [online] in favor of one really nice social interaction with one person." He says this kind of social interaction makes life more meaningful.

Others get away from technology by deliberately leaving their phones at home. Young people, even though they use social media voraciously, do value their private time, too.

Pattie has decided not to carry her cell phone anymore, saying, "It feels good to have people _not_ reach you."

Another advantage of being less connected is that we're not always able to immediately receive bad news. Bad news can be particularly damaging if we hear it when alone, without the support of friends or family.

For example, Hillary was thankful her family couldn't call her immediately when her father suffered a seizure. She was alone, and might have panicked. Instead, she got the news when she arrived home, where her family was there to comfort her and explain the situation properly.

Being connected undoubtedly has its perks, but sometimes we forget that turning off our devices can be beneficial as well.

> _"We expect more from technology and less from each other."_

### 13. Final summary 

The key message in this book:

**Technology is supposed to enrich our lives, but it often makes us lonelier. Robots and other new technologies are** ** _replacing_** **some of our relationships, instead of enhancing them. This can be stressful, especially for children and the elderly, so many people are backing away from technology and trying to reconnect on a human level.**

Actionable advice:

**Feeling overwhelmed? Leave your phone at home.**

You don't have to carry your phone with you all the time. Knowing that it could ring at any moment is stressful, so leave it behind when you want to relax. Don't feel guilty — you deserve some personal time to yourself.

**Suggested further reading:** ** _The Shallows_** **by Nicholas Carr**

_The_ _Shallows_ delves into the seldom-asked question of how technology, and the internet in particular, affects our brains. _The_ _Shallows_ looks at the history of technology and recent studies in psychology and neuroscience to show us how the internet fundamentally rewires our neural circuitry, and why it hasn't delivered the benefits promised by its champions.
---

### Sherry Turkle

Sherry Turkle is a professor at the Massachusetts Institute of Technology (MIT). She's written several books on human-technology interaction, and she's also the founder and director of the _MIT Initiative on Technology and Self_.

