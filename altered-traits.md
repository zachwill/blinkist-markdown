---
id: 5bb5c7f5862c430007d26da9
slug: altered-traits-en
published_date: 2018-10-05T00:00:00.000+00:00
author: Daniel Goleman and Richard J. Davidson
title: Altered Traits
subtitle: Science Reveals How Meditation Changes Your Mind, Brain, and Body
main_color: 2B86D8
text_color: 1C578C
---

# Altered Traits

_Science Reveals How Meditation Changes Your Mind, Brain, and Body_

**Daniel Goleman and Richard J. Davidson**

_Altered Traits_ (2017) takes an empirical look at the art of meditation and details the benefits it has on our mental well-being. It also looks at different types of meditation, as well as the effects of meditative practices on different groups, including experienced meditators, students and even a yogi.

---
### 1. What’s in it for me? Learn how meditation can help you become a better person. 

It's easy to be sceptical about meditation. Given its increasing popularity in the Western world over the past decades, it can be seen as a fad without real substance.

But the good news is that the value of meditation can be determined scientifically. Citing numerous studies — some of which were conducted by the authors themselves — understanding meditation from a scientific point-of-view will satiate curious minds.

These blinks explain how meditation helps to develop positive attributes that will help you become a better individual. We'll explore specific parts of the brain affected by meditative practices and learn how we can use the knowledge to our advantage.

The benefits of meditation are for everyone, whether you're a seasoned meditator or someone who's looking to try it out for the first time.

In these blinks, you'll discover

  * why multitasking isn't helpful;

  * the implications of a wandering mind; and

  * how meditation can help you better process emotion.

### 2. Two types of meditation include focusing on a singular thing and not reacting to thoughts. 

When he was a Harvard graduate student, Goleman, one of the authors, traveled to India to learn about meditation. There are many different types of meditation, but Goleman was advised by one of his meditation teachers, Anagarika Munindra, to study one found in an ancient text called the Visuddhimagga, or Path to Purification.

This type of meditation focuses on a single thing.

Written in the fifth century, the Visuddhimagga is the principal text of Theravada Buddhism, a branch that is widespread among many Southeast Asian nations. It's also the source of _mindfulness_, which teaches you to develop your concentration by focusing explicitly on one thing.

Typically, you begin by focusing on your breath. In the beginning, it will be hard to concentrate on one thing as your mind zips back and forth between thoughts. However, with practice, your thoughts will calm and your mind will become quiet so that the only thing you are paying attention to is your breathing.

Another type of meditation is based on not reacting to your thoughts.

It comes directly from the founder of Buddhism, Gautama Buddha, who lived during the sixth century BC. In contrast to the type of meditation mentioned earlier, this one encourages you to remain cognizant of _all_ your thoughts. What's important here is to refrain from reacting to them.

You need to instantly let go of thoughts as they pop into your mind, instead of hanging on and becoming consumed by them. Over time you will develop _equanimity_, meaning that it won't matter whether the thoughts you have are self-hating or romantic fantasies — they'll simply become passing musings that bear no impact on your core consciousness.

Regardless of whether you prefer the first or second type of meditation, both are equally respected. The main thing to consider is which type will benefit you the most. In the following blinks, we'll take a look at what those benefits are.

> _"Once we glimpse our mind as a set of processes, rather than getting swept away by the seductions of our thoughts, we enter the path of insight."_

### 3. Meditation reduces reactions to emotional cues and stress triggers. 

Picture yourself in an interview with two potential employers who show no signs of empathy or encouragement. After trying to prove yourself as a valuable addition to their company, they ask you to do a bit of arithmetic: Start with 1324, then subtract 17, then minus 17 again and keep going without making any errors.

This is a form of the Trier Social Stress Test — a psychological test that calculates participants' reactions to social stress situations. Participants' heart rates and blood pressure are measured to determine how they handle certain stress triggers.

Meditation is one way to help participants become less responsive to triggers within the Trier Social Stress Test.

In 2012, researchers Paul Ekman and Alan Wallace studied the impact of meditation on lowering social stress levels among teachers. The study found that after being exposed to the Trier test, teachers' blood pressures returned to normal quicker if they'd practiced meditation. In fact, the longer they meditated, the faster their recovery from stress, and it was effective even five months after the meditation training.

In 2016, the author Davidson replicated the Ekman and Wallace study in his lab, with the addition of experienced meditators. On the first day, the participants spent eight hours in meditation, and on the following day, they took the Trier test. In comparison to the group of non-meditators, the meditation group produced less _cortisol_, a stress hormone, when exposed to triggers. Moreover, the meditation group didn't find the Trier test particularly stressful.

Meditation can also reduce your reactions to emotional cues.

In 2017, Davidson continued working with the same experienced meditators in another experiment. This time, Davidson used a scanner to observe the brain activity of participants as they looked at images of injured and suffering people.

The part of the brain analyzed was the _amygdala_, which is involved in emotional processing. Davidson found that the amygdala of meditators were less reactive to the images than those of non-meditators. This is because meditators had a stronger link between the amygdala and the _prefrontal cortex_ — the part of the brain related to planning and other complex cognitive functions. It has been shown that the amygdala-prefrontal cortex connection lowers the effect of strong emotions, whether they're positive or negative.

### 4. Multitasking exhausts the brain, whereas meditation strengthens our mental ability. 

Multitasking is a common phenomenon of the modern world. Nowadays, we're juggling various duties while being bombarded with texts, calls, emails, Facebook notifications and Instagram photos.

Some try to work on their ability to handle several tasks at once, but multitasking is inefficient and mentally exhausting.

In 2009, Eyal Ophir, a communications scientist from Stanford University, conducted a study which found that the brain is unable to multitask. When trying to do many things at once, our attention switches quickly between those tasks and we lose concentration, meaning that we need more time to regain that focus when returning to our original task. The study also found that people who multitask regularly are easily distracted and thus have to use more areas of their brain to concentrate.

Thankfully, inefficient multitasking can be replaced with meditation, which helps to strengthen our capacity for concentration.

Psychologists Thomas E. Gorman and C. Shawn Green ran a study in 2016 that compared the concentration ability of two groups of students. Prior to the test, one group browsed the internet for ten minutes, while the other group meditated by counting their breaths. During the concentration tests, the meditators showed greater improvement in their ability to concentrate, most notably among the students who multitasked regularly.

Another study ran in 2012 by Michael D. Mrazek, who teaches at the Santa Barbara University of California, found that longer meditation reduced distraction and helped students to improve their scores on the grad school entrance exams by up to 30 percent.

> _"Multitaskers are suckers for irrelevancy, which hampers concentration, analytic understanding and empathy."_

### 5. Meditation turns off the default mode our brain enters when we do nothing. 

When faced with a difficult mathematical equation, you probably think your brain needs to fire up in order to solve it. But when engaged in arduous tasks, many areas of the brain _deactivate_. Moreover, when we do nothing at all, these same parts of the brain _reactivate._ So when we're doing nothing, our brains aren't really resting.

In fact, the brain enters what is called the _default mode_. Neuroscientist Marcus Raichle, who worked at Washington University in St. Louis, discovered in 2001 that when we think we're doing nothing, many parts of the brain show high activity.

The brain only makes up two percent of our entire body mass, yet uses up 20 percent of our metabolic energy. Surprisingly, the brain uses a similar amount of energy regardless of whether it's actively focused on reading Kant's _Critique of Pure Reason_ or sunbathing at the beach.

The _posterior cingulate cortex_ and the midline of the _prefrontal cortex_ are the two areas that are activated when we're supposedly not doing anything. Two of these components comprise the brain's _default mode network_ — where the brain is active during rest.

The problem is that the default mode isn't healthy and needs to be switched off. This is where meditation can help.

During default mode, the brain is unhappily distracted and wandering. Not too long ago, researchers from Harvard asked thousands of participants to pay attention to the association between their level of concentration and their mood. The majority reported that they were less happy when the mind wasn't as focused.

This is because our brain likes to revisit our dissatisfactions and anxieties. Through meditation, we can change its impact on our well-being. In 2011, psychiatrist Judson Brewer found that people who meditate regularly showed a stronger link between the _dorsolateral prefrontal cortex_ and the default mode network. This connection is developed through meditation, and it helps to soothe the mind when we aren't engaged in a mentally strenuous task.

### 6. Studies indicate that meditation strengthens the brain, but we must take these findings with a grain of salt. 

Meditation can calm people and help their focus. But what if meditation has a bigger impact, particularly on our brain cells?

There are studies that indicate meditation makes the brain stronger. The first evidence for this came from a 2005 study conducted by Sara Lazar, who worked at Harvard Medical School. Lazar found that meditation made certain areas of the brain grow thicker. The implications of what this would mean for humans has yet to be determined, but the study did suggest that parts of the brain were improving.

Since Lazar's initial study, many more have been conducted. In 2014, neurologist Kieran C. R. Fox from Stanford University meta-analyzed 21 of these studies. Fox found that meditation strengthened three areas of the brain: the _insula_, which is the part responsible for the recognition of emotional and physical bodily processes, the _prefrontal cortex_, which is important for focused attention and the _cingulate cortex_, which helps with self-regulation and impulse control.

Then, in 2016, neurologist Eileen Luders from UCLA found that the brain rejuvenates with meditation, which slows down brain cell death. Of those participants in their 50s, the ones that meditated had brains on average 7.5 years younger than those of non-meditators.

Despite the extensive studies on the effects of meditation on brain activity and general well-being, the findings should be taken with a grain of salt.

While it's true that Luders' study generally showed that the brains of meditators were healthier, the participants practiced different types of meditation, including _Vipassana_, _Zen Buddhism_ and _Kundalini Yoga_.

There are stark differences between these forms of meditation. Some leave the mind completely open, so that you become aware of everything, while others teach you to focus on only one thing. Likewise, some types advise you to control your breathing, while others recommend to let it come naturally. It's therefore hard to determine which aspects of each meditation are responsible for the positive effects on the brain, and further study in this area is necessary.

### 7. Meditation helps to soothe and reduce the risk of depression. 

Western branches of medicine have not yet found an adequate response to psychiatric disorders that aren't responsive to drugs. Perhaps meditation could help?

Some studies suggest that meditation can help to alleviate depression.

One study, conducted in 2000 by cognitive psychologist John Teasdale from Oxford University, indicated that _mindfulness-based cognitive therapy_ (MBCT) was helpful in preventing depression. MBCT is a type of cognitive behavioral therapy that requires the patient to concentrate on one thing.

Another study at Oxford University, this time ran by Mark Williams in 2014, had two control groups: one received cognitive therapy, while the other received traditional pharmaceutical treatment. Results showed that MBCT was most effective when the depression stemmed from childhood traumas. With other types of depression, however, MBCT worked just as well as the drugs.

Alberto Chiesa from the University of Bologna ran a study in 2015 which found that patients who did not show improvements from medication did feel better with MBCT.

Cognitive psychologist Zindel Segal, also from Oxford, provided further evidence to support the positive role of meditation on people with depression. Segal used fMRI scans to demonstrate that MBCT reinforced the insula of patients with depression, meaning they could more easily gain perspective on their lives, instead of feeling overwhelmed by their thoughts and emotions. Additionally, the study showed that relapses were reduced by 35% after MBCT.

Meditation can also help lower the risk of depression.

Pregnant women with a history of depression are vulnerable to experiencing depression during and after giving birth and are often hesitant to take antidepressants while pregnant. Luckily, the study by Sona Dimidjian in 2016 showed that pregnant women could use MBCT to lower the risk of depression.

Furthermore, a 2016 study by S. Nidich in Iowa found that _transcendental meditation_, which requires you to silently repeat chants or mantras, helped male prisoners decrease their depression and anxiety levels.

### 8. Intense meditation can result in high levels of compassion. 

In the autumn of 2002, a Tibetan monk named Mingyur Rinpoche arrived at Madison Airport in Wisconsin. One of the authors had invited the monk to his lab so that he could measure the brain activity of a yogi — the master of meditation.

The author found that neurological changes were more visible in the brain of a yogi.

At the author's lab, Rinpoche was hooked up to an _electroencephalogram_ (EEG), which tunes into brain waves to measure the level of brain activation. Meanwhile, a scholar — aided by a translator — asked Rinpoche to practice _compassion meditation_, which involves focusing initially on feeling compassionate towards people you love, before extending that love to every single living thing in the universe. In between these regular intervals of compassion meditation, the yogi was asked to rest.

The study found that during moments of compassion meditation, the yogi's brain waves spiked to an unusually high level, which continued for the entire one-minute interval. Typically, these unusually high spikes would only be seen when someone accidentally sets off the EEG sensors. But the yogi didn't move a muscle. During the resting phases, the spikes lowered slightly but returned to their high levels as soon as he was instructed to meditate again.

The yogi's brain demonstrated the power of compassion.

In the second part of the author's study, the yogi's neural activity was recorded with an fMRI machine, which allowed for a more precise look at which areas were being activated. This time, it was obvious that the areas associated with empathy were activated. Rather than hitting normal levels, however, activation rose by 800 percent in comparison to when the yogi was resting.

Such an incredible level of neural activity had never before been seen in science. The closest recordings came from schizophrenics, but these were never voluntary, unlike Rinpoche, who achieved supernormal brain power with intent.

### 9. The more you meditate, the greater the benefits. 

There are many different types of meditators around the world. Some meditate sporadically, while others go off to meditation retreats and practice for thousands of hours. If you're a yogi, then meditating over a three-year period is nothing out of the ordinary. Similar to the various types of meditation, the effects are different for everyone.

But even meditating just a little bit will help you see beneficial effects.

In Western society, though most people who meditate are beginners, that doesn't mean they're not already reaping the benefits. In fact, there's evidence to suggest that the amygdala becomes less responsive after 30 hours of _Mindfulness-Based Stress Reduction_ meditation.

Furthermore, seven hours of practicing compassion has been shown to strengthen areas of the brain related to empathy and positive emotions, such as pleasure and enjoyment.

With the aforementioned Gorman and Green study, we've learned that concentration levels can increase slightly with eight minutes of mindfulness. After two weeks of daily eight-minute meditation sessions, enhanced concentration resulted in improved test scores.

The thing is, none of these methods resulted in long-lasting benefits unless they were sustained over a lengthier period.

Therefore, the more you practice meditation, the more benefits you'll receive.

After meditating for thousands of hours, you become less reactive to stress triggers, your brain's ability to regulate emotion enhances and your body releases less of the stress hormone cortisol. Long-term compassion meditation results in higher levels of empathy, which enables meditators to better understand the suffering of others. Additionally, it increases the chances that you would take action to help people in need.

As concentration levels increase, mind-wandering dwindles. This is especially beneficial for people who tend to get caught up in their own lives — reducing their level of self-centeredness and allowing more time for others.

The advantages of meditation are significant, and now that you know there's scientific evidence to prove it, there's little reason not to try meditation to aid your path towards self-improvement.

### 10. Final summary 

The key message in these blinks:

**For a long time, people have celebrated meditation as a source of inner peace, wisdom and tranquility. However, in the last few decades, there's been increasing scientific evidence to back up these claims. Studies have shown that meditation can improve concentration and empathy levels, as well as lower the risk of depression. Further research is required, but it's safe to say that meditation enriches the brain.**

Actionable advice:

**Make time for meditation.**

Most people who say they want to meditate claim they have no time to do so, but this is simply untrue. How much time do you spend on social media each day? You'll probably find out that you've got more spare time than you think. Even if you have a really full schedule, you can always squeeze in some mindfulness meditation. You can practice on the commute to work, while on a snack break or even while doing the groceries. It's all about focusing your attention on the task at hand.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Buddha's Brain_** **by Rick Hanson**

_Buddha's_ _Brain_ is a practical guide to attaining more happiness, love and wisdom in life. It aims to empower readers by providing them with practical skills and tools to help unlock their brains' potential and achieve greater peace of mind. Specific attention is paid to the contemplative technique "mindfulness" and the latest neurological findings that support it.
---

### Daniel Goleman and Richard J. Davidson

Daniel Goleman is an author and Harvard graduate. One of his best-known books is _Emotional Intelligence_ (1995). Goleman's interest in meditation began with a two-year trip to India when he was a student.

Richard J. Davidson is a psychologist from Harvard and the director of a neurological laboratory in Wisconsin. Davidson has been studying the effects of meditation on individual well-being for decades.

