---
id: 5832180c2b89ea0004096a2e
slug: american-lion-en
published_date: 2016-11-22T00:00:00.000+00:00
author: Jon Meacham
title: American Lion
subtitle: Andrew Jackson in the White House
main_color: A86C35
text_color: 754B25
---

# American Lion

_Andrew Jackson in the White House_

**Jon Meacham**

_American Lion_ (2008) tells the story of Andrew Jackson, America's seventh president. These blinks describe Jackson's rise from poverty to the White House, and how he transformed the presidency from a relatively symbolic position into a powerful vehicle for representing the interests of the people.

---
### 1. What’s in it for me? Get to know one of the most controversial presidents of all time. 

In the presidential television drama _The West Wing,_ the characters celebrate an annual Big Block of Cheese Day. The day is celebrated in honor of President Andrew Jackson, who used to put a two-ton block of cheese in the White House foyer and invite the people to come and partake. It was a time not only for citizens to eat but also to ask questions and share their opinions with the president.

This anecdote about Jackson is telling. Although he has been criticized in modern times for owning slaves and dealing harshly with Native Americans, he was wildly popular in his day. In fact, he was one of the most popular US presidents of all time. His political career was based on being a voice for the common man and protecting their liberty from special interests and corruption — a revolutionary idea at the time.

So let's dive into the life of the United States' seventh president, and see how he left his fingerprints on the highest office in the land.

In these blinks, you'll find out

  * about Jackson's tendency to challenge his foes to duels;

  * how an Army troop he led outmaneuvered a larger British one; and

  * why his use of the veto changed American politics forever.

### 2. Orphaned during the Revolutionary War, Andrew Jackson went on to marry the love of his life and establish himself as a prominent lawyer. 

On March 15, 1767, Andrew Jackson was born in the small town of Waxhaw, which straddles the border between North and South Carolina. He grew up with just one parent; not long before his birth, Jackson's father unexpectedly passed away.

The impoverished childhood that followed was never easy, but Jackson nonetheless spent his youth wrestling with friends and taking classes at a local Presbyterian church. And so life went on until 1779, when the American Revolutionary War reached Waxhaw. The war devastated Jackson's world. Before the age of 15, his entire family was dead.

First he lost his older brothers. Hugh fell in battle. Both Andrew and his other older brother, Robert, were found by the British and taken as prisoners of war in 1781. Soon after, Robert fell ill with an infection from which he never recovered.

That same year, Jackson's mother, who was in Charleston taking care of two sick nephews, took ill and passed away. Jackson left Waxhaw and never returned.

Yet by the age of 21, he was rising to fame as a well-known lawyer and courting his soon-to-be wife. He'd acquired his law license in 1787 and quickly built a reputation for himself as a carousing and rowdy, although charismatic, man.

During his first court case, Jackson even challenged the opposing counsel to a duel!

In 1788, he moved to the frontier of the Tennessee territories and became friendly with a well-established family there, the Donelsons. The daughter of the family, Rachel Donelson, was in a difficult marriage at the time and Jackson was happy to protect her from her abusive husband.

Eventually, Rachel petitioned for a divorce and, even before it became official, married Jackson in early 1791. But, despite the romance of the gesture, the choice to wed before the divorce went through would come to haunt the couple later in life.

### 3. Before becoming president, Jackson earned a name for himself as a colorful and violent character. 

Jackson and Rachel Donelson remarried in 1794, once the confusion surrounding her divorce was settled. The couple was absolutely in love and Jackson went to great lengths to protect her honor, a tendency that sometimes led to violence.

In fact, Jackson was generally a deeply loyal man who'd quickly draw a gun on anyone that said a hurtful thing about his wife or friends. This inclination toward violence came into play in 1803, when Governor John Sevier of Tennessee commented on how Jackson, then a General of the Tennessee militia, had been in a relationship with another man's wife. The jab resulted in a shootout on a crowded street.

Thankfully, no one was hurt that time, although a bullet grazed a bystander. But blood _was_ shed in 1806, when a man in Nashville, by the name of Charles Dickinson, made a rude comment about Rachel. To settle this matter, Jackson demanded a duel and, standing 24 feet apart, Jackson let his foe shoot first, taking a bullet to the chest before calmly leveling his gun, returning fire and killing Dickinson in an instant.

This wasn't the only time Jackson got shot. The frontier was a dangerous place and he'd taken fire to his left arm from a man named Jesse Benton after getting into a street fight with Jesse's brother, Tom. As you can imagine, stories like these built quite the reputation for Jackson, but his legacy as an American hero was solidified during the Battle of New Orleans.

During the War of 1812, Jackson defended the South against the British. On one occasion, in New Orleans, he led an army of 5,000 men to victory against a British army that vastly outnumbered his.

He secured victory in part through the controversial tactic of forcing the city's leaders to enact martial law, effectively giving him total control of the city. In the end, the strategy was a success. Only 13 of Jackson's soldiers were killed and a mere 39 wounded.

### 4. Jackson leveraged his military fame into political currency, taking the presidency. 

Following his victory in New Orleans, Jackson was enlisted to battle the Seminole Indians and the Spanish in the Florida peninsula, an important new territory for the American government. In the end, he succeeded in taking Florida, but the battles leading up to this victory were absolute bloodbaths.

Jackson even personally took it upon himself to execute two British subjects, a bold move that angered certain members of congress who saw Jackson as a loose cannon. But regardless of what these men thought, Jackson's military endeavors put him in the political spotlight, which opened new doors.

This hard-won political fame would eventually help Jackson become president, but at a tragic price.

Here's what happened:

Jackson lost his first bid for the presidency against John Quincy Adams in 1824, even though Jackson won the popular vote. He ran again in 1828 and won, despite vicious attacks by Adam's camp, which questioned Jackson's marriage to Rachel and spread rumors about him engaging in an adulterous affair.

As a result of these slanders and the claim that she was unfit to serve as the first lady, Rachel became humiliated and, on December 22, 1828, she had a heart attack and died. Jackson was heartbroken at the loss of his wife but was supported by Rachel's family, especially Andrew and Emily Donelson, who loved Jackson as an uncle and stood by him in the years to come.

So, recuperating from his loss, Jackson put together a cabinet, naming his close friend John Eaton as secretary of war, a choice that would spur an unforeseen scandal. In 1829, Eaton married Margaret O'Neal, shortly after the death of her husband and the surfacing of rumors that he'd killed himself due to Margaret's infidelity.

There were also rumors that Margaret had miscarried while her former husband was serving on active duty in the Navy and that John and Margaret had shared a bedroom before they married. These rumors offered perfect ammunition for Jackson's enemies throughout his first term in office.

### 5. Jackson became president at a time when the rights of Southern states was a pressing political issue. 

Coming into the White House, Jackson had a laundry list of things he wanted to accomplish. But these plans would be complicated by the fact that the nation was in a precarious position when his first term began in 1829.

Jackson viewed himself, above all else, as a representative of the people, someone who could advocate for the rights of the common man against special interests. However, the country was facing pressing issues — namely, rising tensions surrounding the rights of states.

At the core of this battle was South Carolina, a state whose leaders sought the right to nullify certain congressional laws that they deemed unfair. Specifically, they wanted to overturn a tariff law, primarily applied to cotton, that had passed in 1828 and that Southern farmers viewed as an abomination.

Among those in favor of nullification were the influential senator Henry Clay and a South Carolina politician named John C. Calhoun, who'd served as vice president during Jackson's first term. These men saw nullification as an essential right of all states to protect themselves from damaging legislation.

On the other hand, Jackson, and politicians like the legendary senator Daniel Webster, viewed nullification as a tremendously dangerous proposition. After all, Jackson was determined to maintain a unified nation and he saw nullification as a step away from what the founding fathers had in mind when they wrote the constitution.

Jackson didn't lack sympathy for the concerns of the Southern states. He believed that the nation's founders had established an effective process, through both legislative and judicial means, for disputing laws and preventing the federal government from carrying out unconstitutional acts. Despite this rationale, however, the issue of nullification would nag Jackson throughout his presidency.

### 6. Jackson faced other difficult issues like the allocation of federal funds and the resettlement of native people. 

So Jackson assumed office amid a raging battle over states' rights. But that wasn't the only issue he had to deal with. At the time, the nation was still expanding westward, and there was the matter of how infrastructure, like roads and canals, was to be paid for.

When it came to the latter issue, the most pressing question was: Will the federal government pay for the canals and roads that need to be built, or will these costs be passed on to individual states? To this, Jackson had a clear answer.

In his opinion, states should pay for their own construction projects and the federal government should bankroll interstate projects. He was firm on this; he wasn't going to make exceptions or play favorites. Nonetheless, Congress was still shocked when, after passing a bill to help build a 60-mile road in Maysville, Kentucky, Jackson vetoed the legislation.

The Maysville veto, as it came to be known, was a turning point in American politics. Up until then, presidents rarely exercised veto authority. By striking down this potential law, Jackson set a new standard. From then on, Congress would no longer be the ultimate power in American politics. The president was in charge.

This wasn't the only position on which Jackson was consistent. His stance on Native Americans also never wavered. This was major because the issue of so-called _Indian removal_ was a divisive subject. Many treaties had been signed throughout the states and, as westward expansion continued, lots of them were being rewritten.

Jackson was adamant that tribes be moved west and relocated to territory beyond the Mississippi River. He didn't consider it sustainable to allow sovereign lands to coexist within American states — and he tried to justify the removal of indigenous peoples as being in their best interest, which, unfortunately, he may have actually believed.

> The first six American presidents combined used the veto nine times; Jackson used it 12 times.

### 7. Many of the major issues of Jackson’s first term were overshadowed by the ongoing Eaton scandal. 

By challenging the established powers, speaking out against corruption and insisting on using his veto power, Jackson was redefining what it meant to be president of the United States. He was showing people that the president was much more than a figurehead intended to maintain the status quo.

But not everyone was thrilled about the growing power of the president. Especially concerned were the politicians who'd grown accustomed to Congress acting as the main political force in Washington. And such people were constantly attempting to devise ways of limiting Jackson's power.

One of his biggest weaknesses during his first term was the continuing scandal surrounding Secretary of War John Eaton and Eaton's wife, Margaret. In fact, a huge portion of Washington society was snubbing the Eatons as a result of the scandal. Even Andrew and Emily Donelson took part. At diplomatic events, representatives of other nations gave the Eatons the cold shoulder.

However, Jackson remained an incredibly loyal friend and insisted that everyone in the White House show loyalty to the blacklisted couple. It's likely that Jackson was especially sympathetic to the Eatons because of the scandal surrounding his own marriage.

But not everyone was on board with Jackson. In fact, the issue divided his own cabinet, with just Martin Van Buren, his Secretary of State, and William Barry, the Postmaster General, standing by Jackson.

And as you can imagine, his opponents were only too happy to stoke the flames. They wanted Eaton replaced, ideally with someone who was more in favor of nullification and states' rights. With no end in sight, this division got so bad that Andrew Donelson, Jackson's advisor, and Donelson's wife, the White House hostess, were no longer welcome in the presidential mansion because of their poor treatment of the Eatons.

### 8. In a bold move, Jackson cleaned house and began focusing on the nullification crisis. 

Of course, Jackson couldn't ban his family from the White House for very long. He spent many lonely days and nights in 1830 while the Donelsons were in Tennessee. He knew he needed a plan to unify his administration and eradicate this distracting feud and yet still remain loyal to his friend.

In the end, it was Martin Van Buren who had the answer: he suggested to Jackson that he wipe the slate clean by bringing in a whole new cabinet. Both Van Buren and Eaton agreed to step down so that Jackson could let other members go and assemble a new administration that would actually function.

So, by 1832, Jackson had a new cabinet in place and could now focus on the increasingly urgent crisis of nullification. Here's what happened next:

A tariff-reform bill was moving through Congress and, by the end of 1832, the governor of South Carolina, Robert Hayne, was writing secret orders to form a state militia in case things went awry. Meanwhile, Jackson was acutely aware of everything that was happening in South Carolina thanks to Joel Poinsett, an American diplomat who would later serve as secretary of war under President Van Buren.

Poinsett had been funneling Jackson constant reports from the South, keeping him abreast of the situation and letting Jackson know that it was urgent to send arms to the Unionists in the area as it appeared fighting might erupt at any moment.

And a battle did nearly break out between the supporters of nullification and the Unionists, but, as Congress debated the tariff-reform bill at the beginning of 1833, more rational minds prevailed and the tariff rates were lowered.

Nevertheless, despite the passing of this tariff reform, South Carolina would remain dissatisfied for years to come.

### 9. During his second term, Jackson successfully challenged the power of the US Bank. 

While issues like tariffs and states' rights would always plague Jackson, his popularity with voters remained steady. As a result, he won a second term in office in 1832.

But just how committed was Jackson to the voting public?

To find out you need only consider his fight against the Bank of the United States.

Here's the story:

The Bank of the United States was a financial institution that held the country's federal deposits and controlled a substantial proportion of the nation's finances. It was the precursor to the modern-day Federal Reserve.

When Jackson lost his first bid for the presidency, he saw how close certain politicians were with the bank officials who held sway over votes in the House of Representatives and who therefore could influence the outcomes of state and federal elections. Jackson was a firm believer in using his position to act in the best interest of the public and he saw the bank as an institution with too much power. It interfered with liberty and acted only for its own gain; it had to be shut down.

So, in 1833, Jackson vetoed a bill to recharter the bank and started transferring all federal deposits to state banks. His stance against the bank and the veto it resulted in are considered two of history's most significant presidential actions. Both signaled the government's main goal: the improvement of the lives of the many, not the special treatment of the few.

The way he exercised his presidential veto and asserted himself as a representative of the public majority was a new presidential approach. Today, presidents are expected to act as he did, but, at the time, Jackson's opponents likened him to a monarch, saying he had more in common with Julius Caesar than George Washington.

Nonetheless, in the end this was a major victory for Jackson. On April 4, 1834, the House agreed not to recharter the Bank of the United States and to keep the money in the state banks.

### 10. During his second term, Jackson was reprimanded by the Senate and faced two assassination attempts. 

Jackson's battle against the Bank of the United States was a bold move and it provoked a bold response. As a result of his actions, his opponents pushed for the Senate to issue a formal censure of Jackson, charging him with overstepping his powers as president.

People like Senator Henry Clay thought that Jackson's work to strengthen the office of the president was destroying the founders' vision of American democracy. A vote was taken, and censure — essentially a reprimand which would remain on the Senate record books — was approved: twenty-six for; twenty against.

But Jackson wasn't merely attacked in writing. He was also the first president to have attempts made on his life. His first would-be assassin was an unstable former Navy officer who'd been let go after charges of misconduct. He blamed Jackson for his unemployment and attempted to kill him on May 6, 1833, while Jackson was sitting at a table on a steamboat.

Luckily, Jackson's friend Andrew Donelson put himself between the president and his attacker and no harm was done.

A more incredible incident occurred on January 30, 1835, when a mentally disturbed man named Richard Lawrence, who sometimes claimed to be the King of England, drew two pistols on Jackson as he was leaving the funeral of South Carolina representative Warren R. Davis.

Amazingly enough, both guns failed to discharge and Jackson rushed the man, striking him with his cane and pinning him to the ground. What's even more remarkable is that, when the pistols were later tested, both fired without a problem. In fact, the chances of both pistols failing to fire simultaneously were an astonishing 125,000 to one.

### 11. Jackson’s final challenges were preserving his legacy and handling a dispute with France. 

Although he was the first US president to have assassination attempts made on him, Andrew Jackson remained wildly popular throughout his time in office. And, while he may have revolutionized the role of the president during his two terms, he was positive he hadn't done anything unconstitutional or deserving of Senate censure.

So, to ensure his legacy was left unblemished, at least in the Senate records, he formally challenged the censure. In his response to the reprimand, entitled "Protest," Jackson reiterated that he acted solely in the interest of the people and their liberty. Though Henry Clay and Jackson's former vice president, John Calhoun, opposed lifting the censure, another Senate vote was taken in 1837 and it was removed.

However, an additional problem had arisen at the same time as the censure. On the exact very day that Jackson received this reprimand, he also got bad news from France. The country had decided not to honor a three-year-old treaty with the United States and said they would no longer pay a debt of $5 million.

Tensions between the two nations ensued. Jackson issued a statement in response that essentially said he was disappointed in France's decision to abandon the treaty and suggested that Congress approve measures to seek repayment via French property in America. The then King of France, Louis Philippe, responded by demanding an apology from Jackson, a confusing gesture to which Jackson responded by saying that he had no intention of offending the nation.

Eventually, Britain stepped in to settle matters as America threatened a naval blockade against France and the nations drew close to war. Naturally, the British viewed this potential military confrontation as needlessly troublesome and, thanks in no small part to Britain, the King of France finally backed down, admitted that the public was in favor of refusing to pay the United States but that France would honor the debt.

### 12. Andrew Jackson’s legacy is controversial, but nobody denies the impact he made. 

Andrew Jackson wasn't without his flaws. But he left a lasting impression on American politics, even if his vision was too limited to advocate human rights for Native Americans or slaves.

There's no question that he changed politics forever. Indeed, Jackson's biggest detractor, Henry Clay, was so upset by Jackson's empowerment of the presidency that he established a new political party. It was called the Whig Party, named after those in Britain who stood against the monarchy.

However, while many of Jackson's approaches to the presidency became standard practice, there isn't much that can justify his treatment of Native Americans or slaves. Like many people of his time, Andrew Jackson accepted slavery as a fact of life.

He viewed the abolitionist movement as a dangerous threat to American peace and approved measures to keep anti-slavery writings out of the South. Not just that, but when one of his personal slaves ran away, he was happy to pay someone cash to catch and whip the runaway.

And his mistreatment of slaves wasn't the only shameful mark on his legacy. When he left office, his friend and vice president, Martin Van Buren, took over and, in 1838, Jackson's policy of Indian Removal became a horrific and tragic reality for the Cherokee. This travesty, now known as the Trail of Tears, resulted in the forced migration westward of 16,000 Cherokee, 4,000 of whom died along the way.

Though these flaws are inexcusable, Jackson was still a pivotal president who transformed the role of his office and made the way for what the presidency has since become.

He left office in 1837 and passed away at his home on June 8th, 1845, at the age of 78. Jackson was remembered as a charismatic leader who inspired loyalty in nearly everyone who served under him. Many years later, President Harry Truman would cite Jackson's legacy of looking "after the little fellow who had no pull" as inspiration for his own work in the White House.

One might even consider it tragic that a man with such passion for liberty — who dedicated himself to protecting the free people of his time from special interests and attacks on their liberty — never saw freedom as a universal right for all people.

### 13. Final summary 

The key message in this book:

**In many ways, Andrew Jackson was the first modern American president. That's because, unlike his predecessors, he made a point of challenging Congress — an authoritative force at the time — and speaking out on behalf of the American people and their liberty.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Bully Pulpit_** **by Doris Kearns Goodwin**

_The Bully Pulpit_ (2013) follows three intricately linked strands of American history: the life of president Theodore Roosevelt, the emergence of a class of progressive investigative journalists, and the life of William Howard Taft and his complicated relationship with Roosevelt.
---

### Jon Meacham

Jon Meacham is the Pulitzer Prize-winning author of numerous best sellers, including _Thomas Jefferson: The Art of Power_ and _Franklin and Wilson_. In addition to writing, he teaches at Vanderbilt University and the University of the South.

