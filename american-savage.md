---
id: 59672732b238e10006ad57df
slug: american-savage-en
published_date: 2017-07-13T00:00:00.000+00:00
author: Dan Savage
title: American Savage
subtitle: Insights, Slights and Fights on Faith, Sex, Love and Politics
main_color: C43F46
text_color: C43F46
---

# American Savage

_Insights, Slights and Fights on Faith, Sex, Love and Politics_

**Dan Savage**

_American Savage_ (2014) explores some of the most contentious social and political issues in the US today, ranging from religion to gay rights, and from health care to education. It offers a fresh, humorous and unconventional perspective on today's most divisive subjects, while also sounding a rallying cry to fight oppression and stand up for human rights.

**This is a Blinkist staff pick**

_"Podcast fans will no doubt know Savage from shows like_ This American Life _and his own wildly popular podcast,_ Savage Love _. His unflinching, playful style provokes honest thought about gender, orientation, love, sex, and why we all have a blinkered view of what constitutes a healthy relationship. Read, think, enjoy — and never, ever Google "Santorum". "_

– Carrie, Managing Editor of Blinkist Magazine

---
### 1. What’s in it for me? Follow Dan Savage’s quest for a more tolerant United States. 

During his long career as a gay rights activist, Dan Savage has inspired countless Americans to stand up to the hatred and discrimination that the lesbian, gay, bisexual and transgender (LGBT) community face on a daily basis in a country still plagued by intolerance and religious dogmatism.

Through his sex columns and his podcast _Savage Love_, he's challenged many common misconceptions about sex and sexuality, and has helped people shed their inhibitions to lead more sexually fulfilling lives.

In these blinks, you'll learn about some of Savage's more controversial ideas and advice for the sex-deprived, his quest to end bullying of young LGBT people and why American sex education needs an overhaul.

In these blinks, you'll learn

  * why cheating is sometimes a good idea;

  * the real meaning of the word "santorum;" and

  * why American gun laws need to change.

### 2. Using the Bible to condemn homosexuality and justify antigay bigotry is extremely hypocritical. 

Even though the author, Dan Savage, was raised Catholic, he isn't the kind of man the Church welcomes with open arms.

After all, Savage is a candid sex columnist and outspoken gay rights activist who is happily married to his husband, Terry Miller. Meanwhile, both the Church and the Bible itself openly condemn homosexuality.

Antigay Christians often justify their hatred of LGBT people by citing the Old Testament, which says that any man who "lies with" another man "shall surely be put to death."

Savage has met with countless people who tell him they can't help but bully gay people since the Bible says it is wrong to be gay.

The Church also has a long history of persecuting gay people by throwing its significant political power behind antigay legislation. Over the years, the Church has tried to ban gay marriage and same-sex adoption rights, as well as block advances in LGBT civil rights.

What many people don't realize is that they're being rather hypocritical when they focus their attention on one part of the Bible.

The Old Testament also advocates for slavery and the stoning of women who have premarital sex. Yet the majority of people are quick to discount these verses and say that the Old Testament isn't considered a relevant text in modern Christianity.

So there is a choice to be made: people can either accept they are being hypocritical when adhering to one part of the Bible while ignoring others, or we need to agree that the Old Testament is out of touch with the values of modern societies.

Just as we've come to accept that the Bible was wrong about slavery, we should agree that the Old Testament is not an appropriate lens through which to interpret something as complex as human sexuality.

### 3. Some relationships can benefit from extramarital sex, and all partners should be good, giving and game. 

Most people have a hard time accepting the idea that having sex with someone other than their partner might be good for their relationship.

But in certain cases, Dan Savage has recommended cheating as a way to keep a couple's relationship alive.

Since starting his sex advice column in 1991, Savage has received countless letters and e-mails from people seeking his guidance, and it's not uncommon for him to hear from partners who, for a variety of reasons, have been sex deprived for years. For some people, having children can be a libido killer; in other cases, one partner won't know what to do when their loved one becomes chronically ill.

In certain situations, the author has advised seeking sex elsewhere.

One man wrote Savage to say that he loved his wife and kids, but didn't know what to do when his wife told him that she had never desired sex and was tired of pretending to want it. In this case, to both honor the wife's desires and keep the family together, Savage's advice was for the husband to discreetly find sex elsewhere.

In another case, a husband wrote in to explain how his wife's libido had completely died and that he hadn't been able to find any way of getting their spark back. In an effort to keep his "wonderful-yet-sexless" marriage intact, he followed Savage's advice to find sex elsewhere.

As a matter of fact, the man wrote back to say that his wife's libido eventually did come back, at which time he ended his extramarital affair.

However, a more common piece of advice for maintaining a healthy relationship is for partners to be GGG.

This stands for **g** ood in bed, **g** iving pleasure without expecting an immediate return and being **g** ame for anything (within reason). GGG partners are open about their sexual desires and are willing to put in the effort to meet each other's desires.

We can note the effectiveness of being GGG in a study published in the _Journal of Sex Research_, which found that couples experienced higher sex drive and more happiness when partners were attentive to each other's individual desires.

> _"Being game is about being willing to give something a whirl, and happily so."_

### 4. The Church’s view of sex is far removed from reality, and sex education in the United States is woefully inadequate. 

You might not think of the Catholic Church and the US school system as having much in common — yet they both teach damaging lessons when it comes to sex.

For the Church's part, its take on sex has been incredibly counterproductive and is entirely disconnected from reality.

Catholicism essentially sees sex purely as a means of reproduction, and having it for any other reason is considered unnatural. This contrasts sharply with scientific studies that show how important sex is to establish and maintain intimate relationships.

Nevertheless, the Church has persistently refused to accept the use of birth control or abortions, since either would be seen to accept the possibility of sex as a nonreproductive act.

But in reality, 93 percent of Catholics support the use of condoms, and 67 percent see premarital sex as acceptable. Statistics also tell us that Catholic women have just as many abortions as non-Catholics. All of this shows just how far removed the Church is from the reality lived by its followers.

But there's another major institution that transmits views that are just as harmful: schools.

Many schools have a sex education, or sex ed, class that students must take. But a better name for it would be "sex dread," since these classes focus solely on the risks of having sex.

Many sex ed classes will simply promote an abstinence approach to sex, rather than teaching safe sex practices. But statistics show that this approach puts students at greater risk of contracting a sexually transmitted disease (STD) or having an unplanned pregnancy.

Abstinence-only education is the standard in Mississippi and Mississippi leads the United States in cases of teen births and gonorrhea. It also ranks second in cases of chlamydia, and third for syphilis.

In fact, the United States is far and away the leader in teen pregnancies among industrialized countries, and it's clearly due to states that teach abstinence-only sex education.

But the only other options American schools provide are known as "comprehensive sex ed" classes, where teachers instill a fear of sex by skipping over any discussion of pleasure and focusing solely on the risks of sexual activity and the potential of catching STDs.

### 5. Arguments against marriage equality and same-sex parenting are hypocritical and illogical. 

As you can imagine, Dan Savage's husband was none too happy when he found out that the author had invited the staunch antigay marriage advocate Brian Brown over to their house for dinner.

As expected, a lively debate ensued, and it illustrated just how ridiculous the argument against gay marriage is.

At one point, Savage asked Brown why he isn't opposed to divorce since the Church views this as a sinful act.

This led Brown to draw some astoundingly illogical conclusions.

Brown explained that just because you believe something is wrong doesn't mean it should be illegal. Naturally, Savage then asked Brown why he doesn't have the same reaction to same-sex marriage. Brown's confounding response was that gay marriage "cannot exist" since it's not "based upon reality."

Brown holds onto the belief that marriage exists solely for the purpose of uniting males and females so they can produce offspring. This concept is known as the _natural law argument_.

But this argument doesn't hold up very well to scrutiny: for example, what if a couple is infertile, unable to have children or doesn't want children? Does this make their marriage illegal?

Again, we run into hypocritical thinking that shows how illogical antigay bigotry is.

Another fact that flies in the face of conservative Christian propaganda is that same-sex parents are just as good at parenting as opposite-sex parents.

Conservatives often argue that a child needs both a mother and a father, even calling it "child abuse" to deny them this "traditional" upbringing.

The worst antigay bigotry has come from the Christian fundamentalist pundit, Bryan Fischer. His attempts at preventing gay couples from adopting have included calling gay men predators who like to prey on children.

However, the studies show that children of same-sex parents are just as likely to be as happy and functional as those with opposite-sex parents. In reality, there is no evidence to support an argument against the parenting abilities of gay and lesbian couples.

This is why mainstream child health and social service organizations fully support the adoption rights of qualified gay parents.

> _"Gay couples aren't stealing children from straight couples."_

### 6. Gay pride events and BDSM fairs are liberating and promote safe sex, and straight people can benefit from them as well. 

According to some right-wing bigots, all gay people are depraved perverts, and you can witness this behavior during gay pride parades and BDSM fairs.

The Oklahoma state legislator, Sally Kern, uses these examples to support her bigoted argument that homosexuality is a bigger threat to the nation than "terrorism and Islam."

What some people fail to understand is that pride parades and kinky street fairs are liberating displays of freedom and safety.

They show the world that the gay rights movement isn't limited to sexual orientation, it's about celebrating the freedom to be who you truly are.

Consider the example of Reverend Gary Aldridge, one of many people who have died due to _autoerotic asphyxiation_, a sexual practice that involves limiting one's oxygen intake to heighten sexual arousal. If he and others had been less ashamed of their kink, they may have spoken with others about it and learned how to practice it safely.

Similarly, BDSM gatherings are also important, as they allow people who engage in these practices to share skills and make sure people are being safe.

Most BDSM practices are far more dangerous when practiced alone. So it's to everyone's benefit that we destigmatize kinky sex through open fairs and festivals that celebrate sexual fantasies rather than demonize them.

When we look at the popularity of Halloween celebrations, and all the sexy his-and-hers matching costumes, we can see that straight people have a desire for pride parades as well.

Over the years, Halloween has clearly become a time for straight people to flaunt their sexuality in all of its kinky glory.

Just as gay people often become more sexually liberated once they've found the courage to come out, straight people can also revel in pride celebrations that relieve them of society's constrictive norms.

> _"And I would argue that all those leather guys, dykes on bikes, go-go boys and drag queens have actually helped our movement."_

### 7. Contrary to what Christian conservatives argue, being gay isn’t a choice – but remaining in the closet is. 

Can you imagine your sexual orientation being something you decide upon when you wake up in the morning? When choosing your outfit for the day, could you then also choose who you're attracted to?

Sounds impossible, right? Yet conservatives and antigay bigots are still arguing that sinful people are choosing to be gay.

This argument is usually made by _choicers_ to discriminate against gays. Since LGBT people are choosing their lifestyle, choicers believe there's no reason to recognize their claim to equal rights.

This is yet another weak argument, since the reality is that we don't choose our sexual orientation; it would be like telling people not to complain about anti-Semitism because they could just choose not to be Jewish.

There's no internal switch that allows us to choose between being gay or straight. And to emphasize this point, Dan Savage has extended an open invitation to choicer John Cummins, of British Columbia's Conservative Party, to prove Savage wrong by willingly giving him oral sex.

What is a choice is deciding to be a closeted adult. And this, too, has been exposed as being profoundly hypocritical.

It was understandable to decide to be closeted back in the late 1960s, before the LGBT movement gained momentum and at a time when homosexuals faced rampant discrimination in every aspect of their lives.

What remains remarkable is how many relatively recent cases there have been of closeted men who have tried to disguise their homosexuality through harmful practices of bigotry.

One of the most blatant examples is Jim West, a Republican politician who helped kill an antidiscrimination bill that would have protected gays and lesbians. He was later caught prowling the internet for teenage boys and coercing them into having sex.

West, like many others, was in a position where he no longer had to lie. He had options, but chose to live a life of shame and hypocrisy.

### 8. The LGBT movement has helped save lives and encouraged people to stand up for themselves. 

Even Dan Savage has his limits. And he'd certainly had enough when Rick Santorum, the US Senate's third-ranking Republican, compared gay people to child rapists and dog fuckers.

Savage wasn't alone; his readers were pretty upset as well, so they worked together to come up with an appropriate response. Eventually, they found a way to forever link the name Santorum to the dirtiest thing they could think of.

This is how "santorum" has now come to refer to "the frothy mixture of lube and fecal matter that is sometimes the byproduct of anal sex."

This is a good example of how the LGBT movement has taken the initiative to fight back against bigotry.

It shows how people are no longer willing to be considered second-class citizens or tolerate being called sick, sinful or perverse — especially when it comes to cases of LGBT children being bullied.

When a 15-year-old boy named Billy Lewis hanged himself after relentless bullying, Savage and his husband began the _It Gets Better_ project to prevent suicide among LGBT youth.

Many LGBT kids can't imagine a future reality that could possibly make it worth enduring the pain they have to endure in the present. These are the isolated and bullied kids that Savage hoped to reach.

Savage was also angry, just as Merle Miller was when he wrote his groundbreaking 1971 essay "What It Means to Be a Homosexual."

Miller was an American writer and pioneer in the gay rights movement at a time when the gay community was first demanding equal rights. Miller was fed up, and in his essay he directed his anger toward those who were saying "such goddamn demeaning, degrading bullshit about me and my friends."

Miller thus became a pioneer in pushing back against the bigotry and getting people to fight until there's equal protection under the law for all LGBT people.

As we've seen, this bigotry has often come from Christian conservatives. But in the next blink, we'll see how homosexuality isn't the only political issue that causes the religious right wing to mobilize.

> _"Miller, in anger, came to the defense of himself and his friends and helped to change the world."_

### 9. The US health care system and gun control laws are in desperate need of repair. 

It can seem ironic that even though Jesus told his followers to care for the sick, the most religious states in the United States are the ones with the fiercest opposition to the Affordable Care Act (ACA), also known as Obamacare.

But one of the biggest reasons for this is the mistaken idea that the Affordable Care Act is an act of socialism.

This couldn't be further from the truth, since Obamacare actually began as a conservative solution and it doesn't actually provide healthcare for everyone.

That's right, Obamacare first emerged from a conservative think tank; it was also first tested by a Republican governor and was even supported by the health insurance industry. It wasn't until President Obama supported it that conservatives began referring to it as an act of socialism.

But something had to change, since before the ACA, around 50 million Americans were uninsured and tens of thousands of people were dying every year because of it.

Thanks to Obamacare, fewer Americans will die of preventable diseases like tooth infections. But even with the ACA, millions of Americans will still be uninsured and unable to afford health insurance, despite the subsidies the act provides.

Another broken system that is costing numerous American lives is US gun policy. As it stands, there are hardly any laws in place to protect people from the threat of random gun violence.

The United States currently has the highest rate of civilian gun ownership in the world, so it's perhaps no surprise that it also has the highest homicide rate of any democratic industrialized nation. For example, the US homicide rate is six times higher than Germany's.

The problem is, every time there's a shooting and citizens start arguing for tougher gun control laws, the NRA and other gun enthusiasts accuse advocates for gun control of exploiting a tragedy.

So, by insisting that we need to postpone a gun control debate over and over again, it continues to be legal to carry concealed weapons into schools, shopping malls, cinemas and offices.

Looking at the bigger picture, it's clear that there is still progress to be made, and as a start we should at least come together to admit that something has to be done, and that something has to be done now.

> A sick child without insurance is 60 percent more likely to die in the hospital than a sick child with insurance.

### 10. Final summary 

The key message in this book:

**Despite what the Church, conservative Christians and heartless bigots would have you believe, LGBT people deserve equal rights. There are currently more than enough issues causing major societal divisions in the United States, including education curricula, health care and gun laws. So, let's accept that the Old Testament is not applicable to modern societies, and that science is accurate in showing us not only how complex human sexuality is, but also that homosexuality is not a choice.**

Actionable advice:

**Disclose your kinks early on.**

If you're interested in a healthy committed relationship, honesty is everything — and this also applies to your kinky sexual preferences. In fact, your kink cards should be laid face-up on the table after three months, and before any major commitments are made — such as moving in together, getting married or having kids.

This gives your partner the chance to bail if your kink happens to turn them off. And to better your chances of exciting your partner with this new piece of information, make sure to frame the kink like a present that your partner is lucky enough to get to do with you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Sex at Dawn_** **by Christopher Ryan and Cacilda Jethá**

_Sex_ _At_ _Dawn_ argues that the idealization of monogamy in Western societies is essentially incompatible with human nature. The book makes a compelling case for our innately promiscuous nature by exploring the history and evolution of human sexuality, with a strong focus on our primate ancestors and the invention of agriculture. Arguing that our distorted view of sexuality ruins our health and keeps us from being happy, _Sex_ _At_ _Dawn_ explains how returning to a more casual approach to sex could benefit interpersonal relationships and societies in general.
---

### Dan Savage

Dan Keenan Savage is the man and voice behind the nationally syndicated sex and relationship advice column "Savage Love" and its podcast version, the _Savage Lovecast_. A vocal gay rights activist, Savage is also the editorial director for the weekly Seattle newspaper, the _Stranger_, and the author of a number of books including _The Commitment: Love, Sex, Marriage, and My Family_ and _The Kid: What Happened After My Boyfriend and I Decided to Go Get Pregnant_.

