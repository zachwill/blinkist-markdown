---
id: 57d6735a08c9270003b19235
slug: americas-bank-en
published_date: 2016-09-23T00:00:00.000+00:00
author: Roger Lowenstein
title: America's Bank
subtitle: The Epic Struggle to Create the Federal Reserve Act
main_color: 947D59
text_color: 7A674A
---

# America's Bank

_The Epic Struggle to Create the Federal Reserve Act_

**Roger Lowenstein**

In _America's Bank_ (2015), you'll discover the gripping story of the US Federal Reserve, or "Fed." These blinks trace the history behind the development and unification of the American banking system and show the complex web of interests and players that continue to shape the system today.

---
### 1. What’s in it for me? Get the straight facts on America’s top bank. 

The reason the US Federal Reserve exists is straightforward. This centralized banking system helps control the flow of currency among other important tasks, such as dampening the effects of financial crises.

But how was the Fed established? It certainly wasn't conceived overnight, but rather was the result of many years of political and social struggle.

In the wake of the 2008 financial crisis that brought US markets — and many individuals — to their knees, these blinks will show you that the long history of the Fed can shed much light on today's market challenges.

In these blinks, you'll discover

  * why it was so difficult to come up with a plan to establish a US central bank;

  * how Woodrow Wilson's character brought bankers and politicians together; and

  * why today's populist anger against Wall Street echos the anger of the past.

### 2. The populism of Andrew Jackson doused plans for creating a central bank in the United States. 

If a presidential candidate today were to question the US Federal Reserve's right to exist, at best the comment would be considered a joke or worse, the ravings of a lunatic.

As you'll soon see, however, this sort of opinion in the early days of the United States wasn't uncommon.

Between the War of Independence which ended in 1783 and the early 1900s, many people opposed any form of concentrated authority in the United States, including a central bank.

This opposition was based on a fear that with such an institution, the government could exercise an undue amount of control over citizens' lives. 

For instance, in 1791, the Philadelphia-based First Bank of the United States was chartered for 20 years. Anti-federalists, however, strongly opposed this central bank's existence as a sign of government dominance, and blocked the charter from being renewed.

In lieu of a central bank, banks at the regional and state levels were forced to issue their own banknotes. 

During the War of 1812 against Britain, the lack of a regulated central currency in the United States caused fiscal chaos. To better secure their market interests, businesses began to favor both centralized tariffs and regulation, which led to the establishment in 1816 of the Second Bank of the United States. 

Soon enough, however, the hopes for a strong central bank faced yet another hurdle in Andrew Jackson, the seventh American president.

Jackson opposed centralized banking as he saw it as an extension of the wealthy minority's power over the poorer majority. His opinion, which echoed a populist current at the time, was that banking was an elitist, anti-democratic force.

In 1836, to combat what he saw as an unfair and inappropriate amount of economic and political power held by the central bank, Jackson vetoed the charter renewal for the Second Bank of the United States.

His opposition dealt a death blow to the dream of a central bank for the rest of the century.

### 3. A series of market panics and liquidity failures led to a growing call for a strong central bank. 

After the serious economic depression known as the Panic of 1893, the United States entered a period of economic prosperity that for many, seemed as if it would last forever.

This was an illusion, of course. The fantasy was shattered in 1907, when a three-week-long market crisis led the US economy to shrink by some 40 percent.

How did this happen? The rise of _trusts_ was a contributing factor.

A trust is a company which has achieved a monopoly position in a certain market. A cotton trust, for instance, would own a majority — if not all — of the shares of a cotton company trading in an unregulated market. This trust alone would then be able to determine the price of the shares in the market. 

In 1907, the catalyst of the crash was the United Copper Company.

First, the company's owners overborrowed because they misread how much money they would need to corner the market for United Copper Company shares. Even with the borrowed money, however, they lacked sufficient cash to monopolize the market. Soon, they realized they didn't even have enough cash to pay off their debts to their creditors and the banks.

This liquidity failure incited panic in the public at large. People were worried about the security of private companies and public financial institutions. In a mad dash to save what cash was left, people flocked to banks to withdraw their savings.

Unfortunately, the banks too suffered a liquidity problem. They had made investments in unregulated financial companies and didn't have the necessary reserves to pay out depositors.

This signaled the onset of another market panic, and soon the US economy came to a screeching halt.

A central bank would have been able to provide the necessary cash during this period, but of course, the United States at the time did not have a central bank. Regional banks instead had to rely on the goodwill of a few individual financiers, or on banking moguls such as J.P. Morgan.

This scramble caused American regional banks to crave the security of a centralized institution that would help them avoid such crises in the future.

### 4. The Progressive Era marked a political shift that led to more government oversight of banking. 

Though government oversight of banking is commonly accepted today, it was a nascent idea in the early twentieth century.

As you'll soon discover, social and political reform was critical to bringing about this shift in perspective.

The Progressive Era, or years between the 1890s and 1920s, marked one of the largest steps forward for social and political reform. The women's movement pushed for voting rights, and black communities fought to overcome race-based economic disparities, which persisted despite the end of slavery.

In the corridors of power, however, banking reform was the subject for mostly white, American male leaders. Across the political spectrum, there was a consensus that reform was necessary, particularly regarding policies that would increase the government's oversight of the banking system.

More oversight would allow for regulations to break up trusts and monopolies as well as establish a central bank in the United States.

The Panic of 1907 provided the biggest push for banking centralization and banking reform. Before this, most politicians and bankers felt that the market would sort itself out, and that politics had no business meddling. In fact, they felt that the government had no place in business at all.

The crisis, however, showed the fallibility of believing that markets were truly self-regulating. Not only were markets unable to fix themselves, but also the possibility existed that, with a the lack of government oversight, businesses could take advantage of consumers.

The stock market, for instance, was rife with risky schemes. Individuals would bet on stocks without having the assets to back such dangerous investments, and in doing so, were essentially creating an unregulated, even duplicitous, side market.

A series of articles in the _Wall Street Journal_ in 1909 in support of banking reform captured the seed of change that was slowly germinating in this new progressive environment.

> _"Banking reform and the progressive movement each hurtled ahead, like separate freight cars approaching a fateful junction."_

### 5. A group of lawmakers and bankers met in secret to draft plans for a central bank. 

In what sounds like a scene straight out of a movie, a small, secret group of bankers and politicians met on an island off the coast of Georgia in 1910 to discuss the future of banking.

One of them was Republican Senator Nelson Aldrich from Rhode Island. While he had once been staunchly opposed to a central bank, the Panic of 1907 and a study of Europe's central banking model had convinced him otherwise.

Aldrich saw how Europe's bank served as a critical, stabilizing force amid tough financial times. He was especially interested in England's central bank, which had prevented the destabilization of the British pound ever since it was established in 1694.

Another member was Paul Warburg, a German-American banker and expert in the European banking system. Though he was in many ways an outsider to American politics and finance, his European expertise and support for a central bank were important in fashioning the draft for an American bank.

Warburg's and Aldrich's contrasting backgrounds were reflected in their disagreements regarding the extent to which an American central banking system should be regulated. 

Warburg favored strong government regulation, while Aldrich believed a small group of regional banks run by politicians and bankers should provide oversight.

The group sought to appease both bankers and politicians, though this was no easy task given each side's contrasting interests. Bankers felt threatened by government intervention, while politicians disagreed on how much intervention would be appropriate.

In general, Republicans wanted little to no government influence, while Democrats wanted more oversight but were wary of bankers being a part of the regulatory process.

Recalling Andrew Jackson's populist fear of bankers, Democrats in general regarded bankers with suspicion.

With a lengthy back and forth, the island group was able to draw up a draft plan for an American central bank, which we'll explore in the next blink.

### 6. With its three central goals, the Aldrich Plan served as a template for the final Federal Reserve Act. 

The meeting on Jekyll Island off the coast of Georgia resulted in the release of the Aldrich Plan in January 1911.

Though the plan wasn't used in its entirety, it did raise a number of issues that were critical to the final act to create a centralized banking system, called the Federal Reserve Act.

The Aldrich Plan outlined three central goals. The first was uniting state banks, which up to that point had printed their own currency. Under the previous system, interstate commerce and exchange rates between states had been unregulated.

Under the Aldrich Plan, a unified system would establish connections among state banks and make interstate commerce run more smoothly.

The second goal was to establish a standard currency. This would lend legitimacy to the unified bank system by ensuring all banks would share the same currency and that it would carry equal value across United States.

The last goal was the establishment of paper money. Before the Aldrich Plan, businesses had struggled to gather assets and cash in particular. Maintaining liquidity depended on banks making questionable deals with stock market traders, and tough economic times only made such deals less reliable and more difficult.

Under the Aldrich Plan, businesses would more easily be able to borrow cash, and the resulting liquidity would work to soften financial crises before they became economic disasters.

The plan was clearly designed with the interests of both bankers and politicians in mind.

It pleased politicians as the plan increased the government's role in overseeing interstate and national financial transactions, and thus increased the overall accountability of the financial system.

Bankers were also happy, as they could now play a greater role in interstate commerce thanks to increased business opportunities afforded by a larger network of banks.

### 7. Wilson’s position of compromise provided the groundwork for the passage of the Federal Reserve Act. 

Soon after the Aldrich Plan was introduced, in 1913, Woodrow Wilson became the twenty-eighth president of the United States.

Wilson was an admirer of both a strong federal government and laissez-faire economics, and as such, he symbolized a compromise between the economic viewpoints of Republicans and Democrats.

President Wilson would be the man to lead the final push for a national central bank. He was motivated to do so for two reasons. First, he was a strong believer in a powerful federal government; and second, he saw banking reform as a way to curb the influence of monopolies.

Wilson pushed for greater federal oversight of banks while also allowing room for healthy business competition by providing credit to both businesses and individuals.

To him, a central bank symbolized a strong federal authority that would provide easier access to credit by being a "lender of last resort." This meant that a central bank and the US Federal Reserve could lower interest rates in times of need, providing banks with the necessary reserves to keep the economy going.

These convictions helped Wilson to get the _Federal Reserve Act_ passed in 1913.

The act emphasized two further points of development for the central bank. First, the federal currency was to be elastic, meaning it would increase or decrease in value depending on demand.

Second, banks had to utilize financial reserves during times of crisis, as holding on to reserves only aggravates a stressed economy. The central bank would also have reserves available and allocate funds as necessary during a future crisis. 

The act passed through the House of Representatives and the Senate, and President Wilson signed the act into law on December 23, 1913.

### 8. The Fed flexed its muscles during World War I and helped the economies of Britain and France. 

Despite the benefits it offered, the establishment of the "Fed" did not solve all the financial worries in the United States.

Laws require time to take effect, and the Federal Reserve Act was no exception.

The act's main goal was to create the Federal Reserve, a process that had many steps and needed to take into account many different factors.

One such factor was determining where to set up _reserve banks_, or regional banks in major US cities, with the goal of distributing the central bank's power. Bankers identified 12 different cities in which to establish reserve banks, including New York, Atlanta, Chicago, Cleveland and Dallas.

Another factor was how much involvement Wall Street should have on the board of the Federal Reserve.

Paul Warburg was nominated as board chief, a decision not without controversy. Warburg's involvement with financial company Kuhn and Loeb signalled a potential conflict of interest. But he eventually severed ties with the firm and was able to appease his detractors.

World War I, however, soon provided the Federal Reserve an opportunity to exercise its power, especially with regard to its role in domestic and international trade.

During the war's early years, the Fed mostly complied with a Department of Treasury request to minimize interest rates. Low interest rates means more borrowing and, as a result, more trade.

However, the Fed wanted to increase its powers. Warburg and his board asked Congress for the right to issue banknotes. Congress granted the Fed its wish, and in a matter of months, the central bank had doubled the amount of money in circulation.

The Fed's ability to lend money and regulate international trade during wartime signaled a significant expansion of its power. It enabled the United States to assist the economies of Britain and France by exporting food and other supplies. Such positive influence earned the Fed international recognition.

### 9. Today the Fed’s role is still under scrutiny, it’s just a different generation asking the questions. 

History has a tendency to repeat itself. The Federal Reserve today faces many of the same issues it did a century ago, with people voicing concerns over the unbridled power of banks and the inability of governments to adequately control them.

Let's connect the dots between the past and present. The suspicion with which banks are regarded today, for instance, is reminiscent of the Progressive Era, during which the general population felt that banks could and did misuse their power.

The Panic of 1907 confirmed that fear, showing the populace that a lack of governmental control can indeed lead to economic disaster.

Just as businesses in the early 1900s rushed ahead in the pursuit of profits, the 2008 financial crisis was fueled by the delusion that the US housing market could expand indefinitely.

In reality, people were borrowing money to buy homes they couldn't afford, creating an inflated housing market bubble. The eventual pop of that bubble wreaked havoc on the economy and on individuals, and only confirmed the general distrust of big banks and the financial sector.

Social activism surrounding the Fed today echoes past movements. The Progressive Era was witness to many activists, from women to racial minorities to politicians, fighting for certain goals. This included, of course, bank reformers who helped pass the Federal Reserve Act.

Banking controversies are back in the headlines. Remember the Citizens United case heard before the US Supreme Court in 2010? This ruling granted corporations the same legal rights as individual people, yet without the same level of accountability to which individuals are held.

Many activists and political groups continue to petition for more corporate responsibility and for the fair prosecution of wrongdoing in the financial sector.

So remember that the protests against and concerns about the banking sector today are not a new phenomenon — they're as old as the history of the Fed itself.

### 10. Final summary 

The key message in this book:

**Though the United States won its independence in 1776, it wasn't until 1913 that the country established had a central bank, called the Federal Reserve. The goal of the Fed was to alleviate the potential damage of financial crises, a goal that is still fights for amid many conflicting interests today.**

Actionable advice:

**Remember to inquire into the history of an institution.**

The next time you hear people calling for institutional change, pay close attention to the arguments for or against such change. Take the time to look into the history of the institution in question. Change does not occur in a vacuum, and poignant historical context can inform misguided short-term memory.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _When Genius Failed_** **by Roger Lowenstein**

_When Genius Failed_ (2001) follows the rise and fall of Long-Term Capital Management, the world's largest ever investment fund. The book reveals uncomfortable truths about the nature of investment and the fragility of the models we use to assess risk.
---

### Roger Lowenstein

Roger Lowenstein is a journalist who has contributed to the _Wall Street Journal_, the _New York Times Magazine_, _Fortune_ and other publications. He has also written a number of books, including _The End of Wall Street_ and _Origins of the Crash_.

