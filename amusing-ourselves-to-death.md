---
id: 5a3ee96cb238e10006e1ac95
slug: amusing-ourselves-to-death-en
published_date: 2018-01-03T00:00:00.000+00:00
author: Neil Postman
title: Amusing Ourselves to Death
subtitle: Public Discourse in the Age of Show Business
main_color: E52E31
text_color: 991F21
---

# Amusing Ourselves to Death

_Public Discourse in the Age of Show Business_

**Neil Postman**

_Amusing Ourselves to Death_ (1985) explores the detrimental effects the medium of television is having on the content of public discourse. Over the course of two centuries, the United States has moved from being a culture defined by the printed word to one where television and triviality dominate.

---
### 1. What’s in it for me? Learn how modern media is dumbing us down. 

The way we consume information has changed drastically over the last 100 years. Subscription numbers for newspapers, the once-dominant source of information, are in steady decline. Meanwhile, modern mediums like television and the internet are spoon-feeding us tidbits of information designed more to entertain than to inform.

The days of long reads are over; we live in an age of headlines. And with a changed media landscape comes a changed society.

The following blinks look back on how these changes occurred and where we're at now — and, of course, where we go from here.

In these blinks, you'll learn

  * why television isn't like a church;

  * why Abraham Lincoln was such a great speaker; and

  * how television provides a Huxleyan warning.

### 2. Our ideas about truth have evolved in tandem with the evolution of communication media. 

The American spirit can be hard to pin down. It's easy to talk about grand ideas — equality, democracy, our inalienable rights — but, at the end of the day, such noble abstractions say little about the lived, everyday experience of Americans.

A better way to pinpoint the true national mood of the United States is to look at its major cities.

For instance, in the mid-nineteenth century, America experienced an immigration boom, and the melting pot of New York City came to embody the multiplicity of American identity. In the twentieth century, Chicago was the hub of American commerce, representing both industrial and human progress.

Today, the American spirit is most manifest in Las Vegas, the city of entertainment.

Understanding how America got here is a matter of understanding how new mediums of communication give rise to new forms of content.

Early in humanity's history, the most crucial communicative medium was, of course, speech — an exclusively verbal form of communication decipherable by solely aural means. Later, with the invention of the alphabet, language took on physical form; the written word froze the flow of human utterances, and transformed language into something that could be studied and analyzed.

Hence, the invention of writing gave rise to grammarians and logicians, philosophers and physicists, novelists and neuroscientists — all the people who try to think about and make sense of the world.

Now, as we move from typographical to televised representations of the world, a new shift is taking place. Public discourse is no longer based on words, but on images. And this new medium is simply unable to adequately convey serious, intellectual content.

This is worrisome, because the dominant medium of communication of a particular era always comes to define our ideas of truth and legitimacy.

Let's say you just graduated from a doctoral program at Harvard University. You'd expect Harvard to give you a degree, right? Our culture is still a print-oriented culture, so no one's going to believe you if you just _say_ you have a PhD from Harvard. You have to produce written proof.

But as our culture becomes dominated by television, appearances, which are often deceiving, are starting to be taken more seriously than printed truths. Accordingly, our ideas about what is and isn't true — and thus the structure of public discourse — are shifting as well.

### 3. Nineteenth-century America was dominated by the medium of print. 

In America today, the Super Bowl draws more collective attention than any other national event, with roughly 111 million people tuning in — that's one-third of the American population! Books, in contrast, rarely sell more than 1 million copies. They simply don't generate the same kind of hype.

But books didn't always have it so bad.

Back in 1776, Thomas Paine published _Common Sense_, a pamphlet that was printed almost half a million times — and this at a time when the American population stood at about 2.5 million! In other words, eighteenth-century Americans were as crazy about reading as modern ones are about football.

So it's no surprise that, by the turn of the nineteenth century, public discourse in America was dominated by the written word in printed form.

This public discourse began near the end of the eighteenth century, when the emergent medium of newspapers enabled Americans to engage in the national conversation. Much of the population was literate, and the papers were widely read and circulated.

The centrality of the written word in public discourse is further evidenced by the eloquence of eighteenth- and nineteenth-century political figures.

For instance, when campaigning for the presidency in 1860, Abraham Lincoln and his opponent Stephen A. Douglas gave speeches that often lasted between three and seven hours. That's a lot of talking, and all of it was closer in style to written language than to spoken. They employed elaborate metaphors, subtle irony and lots of dependent clauses, figures of speech hardly ever used in the majority of television programs.

Furthermore, in eighteenth- and nineteenth-century America, the general public would have been familiar with the writings of public figures, not with their image.

Between 1789 and 1860, the average citizen probably wouldn't have even recognized the president if he walked by. Politicians of the time were judged by their words, by the positions they took and the arguments they made.

Today, in contrast, we associate public figures with a face. And this is what differentiates a word-centered culture from an image-centered one.

### 4. Telegraphy and photography laid the foundation for the age of show business. 

Up until the middle of the nineteenth century, information could only move as fast as a person could travel, that is, at the speed of a horse or a car or a train. Telegraphy changed things entirely. Suddenly, information could travel across vast distances almost instantaneously, and this unprecedented rapidity revolutionized both communication and ideas about what warranted communicating.

Telegraphy basically turned the prevailing notion of information on its head.

American writer and philosopher Henry David Thoreau once disdainfully remarked that the telegraphic connection between England and America would lead to the communication of trivial information. For instance, one telegraph conveyed the news that Princess Adelaide had the whooping cough.

Thoreau's jest proved to be spot on, for telegraphy utterly changed information by stripping it of its social and intellectual context.

Sure, Princess Adelaide had the whooping cough. But what about the context of her sickness? Did a lot of English people have it, too? What were the short- and long-term implications?

Telegraphy, unlike print media, wasn't good at providing context and analysis. Telegraphs could only carry snippets of information, and this resulted in the quick communication of a great many irrelevant facts. In other words, telegraphic information was the antithesis of the information found in print media like books, pamphlets and newspapers.

Photography emerged around the same time as telegraphy, in the mid-nineteenth century, and it perfectly complemented the flood of telegraphic fragments.

By the end of the century, advertisers and newspapermen alike had learned what everyone knows today: a picture is worth a thousand words. This realization effected another shift in America, specifically that information, previously conceived of typographically, became a photographic phenomenon. In the past, to read was to believe; now, seeing was believing.

Not that photographs actually did a better job of conveying information. Like telegraphy, photography presents facts untethered from context, with no discussion of what the facts might mean.

Telegraphy and photography thereby formed the foundation of the age of show business — a culture full of random information floating free of context. And this culture's apotheosis is television.

> _"Everything became everyone's business. For the first time, we were sent information which answered no question we had asked."_

### 5. On television, the content of public discourse becomes entertainment. 

When the light bulb was invented, many people thought it was nothing but a more powerful candle. When the automobile was invented, many thought it was nothing more than a faster horse. In fact, the advent of any new technology is usually thought of as no more than an extension or amplification of an existing one.

The same goes for television. Many people believed it was some sort of dynamic amplification of the printing press. Well, these people were very wrong; television carries on the tradition that telegraphy and photography began.

And, in this tradition, television is not an arena for serious public discourse. It's a medium of entertainment.

American television is a remarkable visual spectacle, an endless fountain of images that you can cause to pour forth with the touch of a finger. These images are simple and, generally, they seek to gratify the viewer in some aesthetic or emotional way. In short, they're entertaining.

On television, everything is necessarily entertaining. Even professedly serious shows — such as the news — are, quite literally, shows.

The news begins and ends with music, and music often plays as stories are reported. As in film and onstage, this music serves one purpose: to create atmosphere and enforce the themes of the entertainment.

Meanwhile, newscasters report on mass killings and natural disasters in the same blandly enthusiastic way that they might forecast a stretch of temperate weather.

Televised debates are just as bad. The winner is usually the best showman, because, on television, it's more important to make a good impression than a cogent argument.

Then you've got the commercials, which undermine any seriousness that television might pretend to retain. To be gravely watching a report about the inevitability of nuclear war one minute and then to be confronted with the latest offer from Burger King the next is, when you think about it, utterly absurd.

And since television is currently the dominant medium, public discourse is changing in a similarly absurd fashion.

### 6. Television turns religion and politics into entertainment and show business. 

The sudden ubiquity of television soon gave rise to the idea that everything could be televised, even the most mysterious and ineffable aspects of life. Today, television is not only a window into current events but, for some Americans, a means of communicating with God.

Indeed, there are many religious television shows and television preachers. But television transforms the divine — just as it transforms the news — into entertainment.

Religious shows assume that anything that can be done in a church can be done on television, too. However, television so often broadcasts profane events, such as infidelity, violence and other unholy happenings, that it can hardly be reframed as a portal to the kingdom of heaven. Television gives off no aura of mysticism and sacredness, and so no televised sermon or ceremony can offer a nontrivial religious experience.

So, in compensation for its inability to offer meaningful religious experience, television entertains. Most religious shows have elaborate sets, often with floral displays, sparkling fountains and choral groups.

On television, nothing is sacred. So it's no surprise that televised political discussions are also just another form of show business.

Ronald Reagan noticed this back in 1966, saying, "politics is just like show business," a statement that his subsequent election to the US presidency proved right. But Reagan was only right because of the rise of television. On television, politics isn't about taking a firm ideological stance; rather, it's about pleasing the crowd and coming across as an honest, trustworthy, competent individual — even if the person in question is none of these things.

Furthermore, the presentation of political ideas has been heavily influenced by televised commercials. The rise of the 30-second political ad has resulted in our valuing brevity of expression over depth of thought, which, in turn, has encouraged the fantasy that political problems can and should have quick and easy solutions.

> _"In America, we are never denied the opportunity to amuse ourselves."_

### 7. Television affects our notion of education and gives a Huxleyan warning. 

It is hardly an exaggeration to say that, today, children "learn" as much in front of the television screen as they learn in school. One could even say that television is a sort of curriculum, as it is watched on a daily basis for hours at a time by both children and adults alike.

But television offers a completely new philosophy of education, a philosophy that's both limited and limiting.

All television programs, whether intentionally educational or not, must adhere to three principles:

First, they must be comprehensible without requiring viewers to have any previous knowledge. Thus, television does away with the ideas of sequence and continuity, both of which are crucial in education. One cannot be taught algebra in an hour-long program. One needs first to learn to add and subtract and multiply. On television, there is neither time nor space for structured learning.

Second, television assumes that any subject can be immediately understood by anyone who tunes in. No studying is necessary, no memorization, no application of concepts or endurance of hardship.

Third, anything taught on television will always come in the form of a story. Hence, no need for arguments, hypotheses, discussion, reflection or any other kind of reasoned discourse.

So what might we call this new philosophy of education that does away with prerequisites, difficulty and discourse? Entertainment.

But television might, despite itself, be doing society a favor as well. By turning all public discourse into show business and entertainment, it inadvertently broadcasts a Huxleyan warning.

In his 1932 dystopian novel, _Brave New World_, Aldous Huxley described a future where people would adore technologies that undermine their will and capacity to think. In this future, truth was washed away on a tide of irrelevant information and culture was nothing but a sad parody of itself.

Knowing that television turns the content of public discourse, whether political, educational or religious, into entertainment, it becomes harder to dismiss Huxley's dystopian vision.

We should pay attention to the detrimental effects television is having on American society. Otherwise, America may soon come to resemble the world that Huxley imagined.

### 8. Final summary 

The key message in this book:

**Television has brought about the age of show business. From politics to education to religion, it has turned the content of public discourse into entertainment. If we don't start paying attention to how television shapes our cognition and perspectives, our world may become a Huxleyan dystopia that we'll be unable to shape or influence.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Seinfeldia_** **by Jennifer Keishin Armstrong**

_Seinfeldia_ (2016) is an in-depth look behind the scenes of the hit sitcom that changed TV forever. These blinks explain _Seinfeld_ 's creation, history, fan base and the lasting impact it has had on TV history as well as millions of fans.
---

### Neil Postman

Neil Postman, a renowned social critic as well as a theorist of education and communication, was a professor at New York University for more than 40 years. He authored more than 20 books, including _The End of Education_ and _How to Watch TV News_.

