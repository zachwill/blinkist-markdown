---
id: 5a2d721ab238e100064279e7
slug: an-american-sickness-en
published_date: 2017-12-14T00:00:00.000+00:00
author: Elisabeth Rosenthal
title: An American Sickness
subtitle: How Health Became Big Business and How You Can Take It Back
main_color: 3582D9
text_color: 2863A6
---

# An American Sickness

_How Health Became Big Business and How You Can Take It Back_

**Elisabeth Rosenthal**

_An American Sickness_ (2017) takes an honest look at the state of the American health-care system and frankly diagnoses its many ailments. When big business started taking over what were once charitable organizations, things began to go truly wrong. Rosenthal presents valuable information on how to reduce health-care bills and not get taken for a ride by greedy hospitals and over-prescribing doctors.

---
### 1. What’s in it for me? Get a better understanding of the US health-care system. 

Over the last few decades, health care has become a major point of debate in the United States. Politicians of all stripes argue over the price of medicine and different approaches to health insurance, but, though everyone seems to agree that something isn't working, no one seems to have a clear idea of exactly how to fix it.

How did it come to this? In these blinks, we'll dive deep into the US health-care system and explore why the United States approaches treatment of the ill very differently than most other Western nations. We'll look at the history of health care, as well as the current situation and possibilities for the future. There may be a way to save a system that most parties agree is sick.

You'll also find out

  * why prescription drugs are so expensive in the United States;

  * that hospitals make money on tests you might not need; and

  * what you can do to start reducing health-care costs now.

### 2. The American health-care system went from humble beginnings to high profits. 

In the grand scheme of things, the American health-care industry isn't very old. It emerged around 1900, with the introduction of the very first health-insurance policies, which were designed to compensate workers for income lost during illness.

The earliest US insurance companies were nonprofits that aimed to help hospitals get paid while also helping patients save money.

For a long time, Blue Cross and Blue Shield were the only major health-insurance companies. But in the 1950s, a decade during which the number of Americans buying health insurance jumped by 60 percent, it became clear that insurance was big business. Soon, for-profit companies started taking over.

Since then, health insurance has remained an extremely profitable business.

To understand just how big the industry has become, let's look at Jeffrey Kivi, a New York chemistry teacher who has been treated for _psoriatic arthritis_ since childhood. People with this condition have an over-responsive immune system that attacks their skin, and, without regular infusions of a drug called Remicade, their life would be almost unbearable.

Jeffrey's treatments used to cost $19,000 every six weeks, and his insurance always covered it. But then, when Kivi's doctor moved to another hospital, a single infusion suddenly started costing $130,000. Jeffrey was surprised when his insurance company picked up the tab, no questions asked.

It might seem crazy, but nowadays, insurance companies are actually looking for such absurdly high bills.

In 1993, Blue Cross spent 95 cents of every dollar they earned covering medical costs, but in the following years they decided to keep most of that money as profit. That is, until the Affordable Care Act (better known as Obamacare) required insurers to spend at least 80 to 85 percent of every dollar earned on patient care.

This requirement is what made Jeffrey's insurance company so happy to pick up that $130,000 tab. Since they make so much money, they need to spend a lot of it in order to adhere to the rules.

But this is only scratching the surface of what's going on in today's American health-care system.

### 3. Hospitals are being run like any other profit-seeking corporation. 

The origins of many American hospitals, most of which began as charitable institutions set up by religious groups, can be traced back to the nineteenth century.

But today, hospitals are more like major corporations than philanthropic foundations.

The transition from charity to big business took place in the 1970s, when hospitals began hiring business consultants from firms like Deloitte & Touche, which introduced concepts like "strategic pricing." From this point on, hospitals were in the business of manipulating their bills and raising prices in an effort to optimize profits.

More often than not, patients end up bearing the brunt of these price hikes.

In 2014, attorney Heather Pearce Campbell, a patient of the Swedish Medical Center, in Seattle, was pregnant with her second child. A sonogram had revealed an ectopic pregnancy; the embryo was growing in one of her fallopian tubes.

Since this is a potentially fatal complication, she was quickly sent to an operating room to have the tube and embryo removed. The relatively simple procedure went fine, but then the hospital bill arrived — for over $44,000. Not only was this an exorbitant fee, but the bill categorized the procedure as "miscellaneous." It was just another way for the hospital's accountants to maximize profits.

Hospitals also started treating doctors like investment bankers and giving them incentives known as "productivity bonuses." The more they charged their patients, the higher their paychecks.

The business consultants restructured the hospitals as well, so areas that were underperforming, like the dialysis unit, were outsourced to make room for the expansion of more profitable units, such as orthopedic and cardiac.

All of these changes resulted in more costs for the patient. Between 1997 and 2012, overall hospital service fees increased by 149 percent. In 2013, an average day in a US hospital cost $4,300, which is ten times more than a stay in a Spanish hospital.

Wondering why hospitals charge so much is like wondering why bank robbers rob banks. It's not that complicated: they do it for the money.

> ****_The list of hospital CEOs with million-dollar annual salaries is long — even if their hospital is listed as a "not-for-profit."_

### 4. Doctors are a lot like entrepreneurs looking for new revenue streams. 

In 1990, the American College of Surgeons required its members to make a pledge that included these words: "I will set my fees commensurate with the services rendered." Unsurprisingly, this statement was removed from the pledge in 2004.

While it's fair to expect doctors to be reasonably compensated, given their years of intensive education and training, the definition of "reasonable" has recently been stretched to a patently unreasonable degree. Today, America's wealthiest one percent includes around 27 percent of the nation's doctors.

Part of this wealth comes from the new revenue streams doctors have access to, which has turned the profession into a cross between doctor and entrepreneur.

Take ambulatory surgery centers (ASCs), for example. Made popular during the eighties and nineties, these facilities are being run less by hospitals and more by individual doctors and investors. In theory, a stay at an ASC should be cheaper since they don't have to deal with the huge overhead costs of a standard hospital. But doctors have struck gold by charging a "facility fee" for their high-end suites, and with fees of between $5,000 and $10,000 per night, it's not unlike being charged for a room rate at an exclusive hotel.

Some doctors have found their fortune by using the private-practice business model. Many of these are specialists, such as anesthesiologists and radiologists — people who aren't commonly needed yet nonetheless serve very important functions.

They are referred to as NPCs, or _no patient contact specialists_, and during the 1980s most of them were employed by hospitals. But now they've started opening private practices and they work with hospitals through contracts. These contracts are often expensive, which is why a visit with an NPC can be the biggest fee on a hospital bill.

These are just a couple of the methods doctors use to generate revenue. The medical field is a huge business and there are very few doctors who haven't found a way to commercialize their practice.

### 5. Pharmaceutical corporations manipulate patent laws and prices to keep profits flowing. 

Like hospitals, many of the big US pharmaceutical companies can be traced back to the nineteenth century. Back then, they were small businesses selling tonics and potions that succeeded in mixing a little bit of science with a whole lot of fanciful marketing.

Though this formula for success sounds quaint, the sad truth is that it hasn't changed all that much. Prices, however, have.

Vaccines used to be sold for pennies, and antibiotics went for a few dollars. But these days, drug prices are routinely raised as high as the market can bear, and there's little patients can do about it.

For example, a monthly dose for the ulcer-treatment drug Mesalamine goes for around $12 in the United Kingdom. In the United States, patients pay between $700 and $1,200 for the same amount. Even patients whose lives depend on this medication have no choice but to pay full price.

In 2015, former hedge-fund manager Martin Shkreli bought the rights to Daraprim, a medication used in the treatment of HIV. He made headlines and became the poster boy for big-pharma greed when he raised the price of one pill from $13.50 to $750.

It's reasonable to think there should be laws against such actions, but pharmaceutical companies excel at finding various strategies to manipulate patent law.

In order to continue raising prices and profits, new patents need to be secured. And so companies create "new" drugs made of existing, non-patented ingredients and thereby secure a new patent. It's become an easy and standard process.

Mesalamine is an example of a drug that uses non-patented ingredients, and to keep the patent from expiring, the pharmaceutical companies only need to show that they made a "non-obvious" modification to it.

Another popular strategy is to earn a new patent by combining old drugs to create a new drug.

This is how Horizon pharmaceuticals created the painkiller Duexis, back in 2011, which is a combination of ibuprofen, the anti-inflammatory, and famotidine, which coats and protects the stomach lining. By combining these two old ingredients, Horizon only had to spend $9 to produce Duexis, but they can now charge over $1,600 for it.

### 6. Medical-device companies are in the dangerous position of having little competition and oversight. 

In 2006, Robin Miller's brother had a heart attack and during his recovery he required an implantable defibrillator, which is similar to a pacemaker. Robin's brother was uninsured, so Robin agreed to cover the costs. But neither the hospital nor the manufacturer would tell him exactly what these costs would amount to.

This particular case is representative of a general problem. Much of what goes on in the area of medical devices is hidden from customers, and yet these devices are often the single most expensive part of a medical bill.

One of the primary reasons costs are so high is that only a few companies control the entire medical device market, making them a tight-knit oligopoly with very little competition.

Knee and hip implants, for example, are made by either Stryker, Zimmer Biomet, DePuy Synthes or Smith & Nephew, and that's about it. Experts in the field like to refer to this small group as "the cartel."

The result of this limited competition is that many medical devices come with an unreasonably expensive price tag.

In fact, there aren't any wholesale prices at all: the exorbitant costs of devices are determined after various intermediaries negotiate their cut. So the usual breakdown is that 16 to 18 percent will go to sales representatives, distributors get 30 percent and then hospitals add an additional 100 to 300 percent for their cut. In Robin Miller's case, he ended up paying $30,000 for his brother's defibrillator.

What's worse is that the lack of oversight in this market has resulted in medical devices receiving very little safety review. Remarkably, there is next to no regulatory approval or clinical tests required, certainly not the kind given to pharmaceuticals, despite the fact that many of these devices are designed to be implanted.

This lack of scrutiny has led to a number of medical disasters.

When a new surgical clip was manufactured and put to use, it was found to be faulty after it failed to close off a big blood vessel. The patient bled out during an operation that, normally, is simple and safe.

### 7. Hospitals have become greedy conglomerates, eager to earn profits through unnecessary tests and services. 

If you've ever worked in the service industry, you may know that restaurants make most of their profit by selling expensive cocktails and drinks.

As it turns out, hospitals have their own version of this. But instead of booze, they make the real money from testing and ancillary services, such as physical therapy sessions.

For example, patients recovering from a hip replacement are often prescribed a long period of physical therapy that ends up costing a small fortune. Sometimes a patient won't be discharged until undergoing PT, even though research suggests it's not necessary after hip replacements.

Thorough testing is generally a good thing when getting a proper diagnosis, but it's also a growing revenue stream for hospitals that are all too willing to prescribe an unnecessary test. This is why physician assistants and nurses are allowed to order tests before a doctor even takes a first look at a patient.

Björn Kemper's son was complaining of a stomachache, so Björn took him to the emergency room at Florida Celebration Health Hospital. Next thing he knew, a completely unnecessary $7,000 CAT scan was underway.

Prices are also being driven up by the rise of medical conglomerates.

When a hospital suddenly begins issuing outrageous medical bills, it's often a sign that they've joined one of the big conglomerates, which act like health-care monopolies designed to drive competition out of the area.

Conglomerates can raise prices as they see fit, and research shows that towns and cities with them tend to see an increase in health-care costs of between 40 to 50 percent.

One of California's hospital conglomerates is called Sutter Health, and it's comprised of 24 hospitals, 34 outpatient surgicenters, nine cancer centers and thousands of affiliated private practices. In some areas of California, a patient has no alternative but to deal with the overpriced service of a Sutter Health facility.

### 8. Health-care organizations are more interested in money than patient care, something the Affordable Care Act tried to fix. 

In a 2014 study of US customer debt, 52 percent of the overdue debt on current credit reports was the result of unpaid medical bills. In fact, one out of every five Americans had medical debt that was affecting their credit score, and therefore their chances of buying a car or getting a loan or a mortgage.

While we've looked at many of the reasons for massive medical bills, it all boils down to this: health care today is being run like big business.

This change is reflected by the language being used. There was once a time when doctors and nurses — and even insurers — were concerned about "patients," whereas today they deal with "consumers." And rather than "illnesses," some medical consultants speak of "high-value disease states."

The business side of medicine is also apparent in today's lack of research funding.

At the Harvard Medical School, Dr. Denise Faustman was researching treatment for type 1 diabetes, but no one, not even the major foundations dedicated to diabetes research, wanted to pay for her work.

The reason for this? Her possible cure had no potential for commercial profit. The business of health care would rather sell treatment over an entire lifetime than sell a single cure. Fortunately, Dr. Faustman was able to find public funding online to continue her important research.

One of the goals of the Affordable Care Act (ACA) was to shift health care away from being profit-driven and back toward being patient-oriented. Parts of ACA are truly revolutionary in this regard, such as making it illegal to deny patients coverage due to a preexisting illness.

Thanks to the ACA, the percentage of uninsured Americans dropped from 18 percent to 11.9 percent, between 2013 and 2016. But there are still improvements to be made, as Obamacare did little to reduce costs.

> _"Unless you're part of the 1 percent, you're only ever one unlucky step away from medical financial disaster."_

### 9. There are steps Americans can take to reduce the costs of medical care. 

You've probably heard about the refugee crisis, but have you heard of "health-care refugees"? This is the growing number of middle- to upper-middle-class Americans who are moving to other countries in an effort to avoid unbearable medical costs.

The author once met a graduate student with diabetes who was searching for an academic position abroad because she could no longer afford treatment in the United States.

If the United States hopes to reduce the number of fleeing citizens, it needs to make health care more affordable, which it could do by following the lead of other nations with good and affordable health care.

Setting a national fee schedule for certain drugs, procedures and devices is one way the United States could generate reasonable costs. This is how countries like Germany, Japan and Belgium operate. The prices are negotiated by doctors, academics and government personnel, and once they're set they can't be suddenly raised overnight.

America could also introduce a single-payer system, similar to the ones in places like Canada, Australia and Taiwan.

In this system, the government covers most of the money being paid to health-care providers, thereby cutting out the private insurers from most of the basic services. Private insurers would only factor into "extra" treatments, like cosmetic dentistry or laser eye surgery.

This kind of system has been debated over the years, and its opponents have kept it from moving forward by calling it "socialized medicine," which tends to be a damning term in US politics.

But there are things you as a patient can do to keep costs down, and the first step is to speak up and ask questions.

Don't be afraid to ask how much a treatment will cost. Ask about cheaper alternatives and why tests are being done. Is that blood test, X-ray or CAT scan really necessary? It's also wise to clarify where a scheduled procedure will take place, and how the location might affect the cost. And when you're getting a referral to another doctor, make sure they're part of your insurer's network. If they aren't, then ask for one who is.

While some doctors are more focused on profits than patient care, most of them are in the profession because they care. A lot of doctors are on your side and just as angry as you about the state of the health-care system.

### 10. Choose your hospital and insurer wisely, and don’t be afraid to speak up. 

Before trying a new restaurant these days, people tend to check online reviews. This is something you can — and should — do before picking a hospital as well.

Many hospitals in the United States are reviewed on Yelp, and the customer reviews to be found there can give you a good idea of what to expect. _U.S. News & World Report_ publishes a ranking of the country's best hospitals that takes into account their reputation within the medical community. This valuable resource also considers the ratio between nurses and patients and the number of medical mishaps. And then you can always check Medicare's online Hospital Compare program, too.

Once you've arrived at a hospital, there are more steps you can take to keep costs down.

First, when you're being admitted and asked to sign documents, keep an eye out for the one that's asking you to agree to pay costs not covered by your insurer. You can check the "limited consent" clause, which says you're agreeing to those costs "as long as the providers are in my insurance network."

Second, if your bill is unreasonably high, don't be afraid to negotiate.

With prices as inflated as they are, hospitals even allow low-level clerks to approve major discounts. So make an offer or simply let them know if you can't pay. Hospitals don't benefit when they have to send overdue bills to debt collectors, so they'll be willing to negotiate.

Third, always ask for a complete itemization of your bill for transparency.

Also, choose your health insurance carefully.

It can be an overwhelming task, but carefully consider all your options and read the small print. You can also take advantage of the health-care "navigators" that the Affordable Care Act introduced.

If you want to stick with your current doctor, call the office and ask for a list of the insurance plans that he or she accepts.

> _Scientific studies disprove any correlation between price and quality of medical care._

### 11. There are ways to reduce the costs of medications and medical services. 

In a 2015 poll, 72 percent of Americans said they felt the cost of prescription drugs was unreasonably high and approximately 25 percent reported difficulties in paying for their medication. Unfortunately, this problem is even worse for people with poor health.

If you hope to lower your drug expenses, here are a few tips.

First, ask your doctor if there are any cheaper alternatives to your prescribed medications. Oftentimes, there are less expensive, but no less effective, alternatives to brand-name prescription drugs. Sometimes a change in dosage can save money as well, so find out if it's cheaper to take two five-milligram pills instead of one ten-milligram pill.

Second, shop around at different drug stores and online outlets. GoodRx.com, for example, allows you to compare the prices at every pharmacy in your local area. It also provides coupons!

Third, if you really can't afford a drug, you might want to consider buying it outside the United States.

You might be thinking, "Isn't importing drugs for personal use illegal in the United States?" Indeed it is, but it's also true that intercepting small packages of prescription drugs is practically impossible, and the government has largely turned a blind eye to this practice as long as the amount is for three months or less.

Websites such as PharmacyChecker.com can help you verify whether a foreign pharmacy is trustworthy.

Finally, there are ways you can reduce expenses on costly medical services.

Whenever possible, decline tests or services that aren't offered by your insurance network, and always double-check to make sure that a service really _is_ part of your network.

Another general rule is to avoid having blood and other fluid specimens tested in hospital labs. These in-house services tend to be far pricier than commercial labs. So ask your doctor to send them to one of the commercial labs that are in your insurance network.

Big business has seized control of the US health-care system, but you still have a voice. Don't be afraid to speak up and demand reasonable care at an affordable cost.

### 12. Final summary 

The key message in this book:

**The American health-care system is a mess. Patients are routinely forced to pay exorbitant fees on everything from hospital visits and services to medications and devices. But there is still hope. There are many ways you can protect yourself from unnecessary costs, like choosing health insurance wisely, negotiating with hospitals and making sure you know exactly what you're paying for.**

Actionable advice:

**Consider a nonprofit insurance plan.**

Only a handful of nonprofit insurance plans exist, but it could be just the right thing for you. The real benefit of nonprofit insurers is that they have no shareholders, so none of your hard-earned money will line the pockets of its investors. The primary concern of a nonprofit health-insurance plan is patient care.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Patient Will See You Now_** **by Eric Topol**

The medical world is on the brink of a revolution thanks to new and future technology like Big Data health maps and bacteria scanners that can attach to smartphones. Power is shifting from the doctor to the patient, and self-treatment and self-diagnoses are becoming unprecedentedly powerful. _The Patient Will See You Now_ (2015) outlines these changes and what they mean for both you and the healthcare world.
---

### Elisabeth Rosenthal

Dr. Elisabeth Rosenthal, after spending many years as a correspondent and reporter for the _New York Times_, became editor-in-chief of _Kaiser Health News_, a position she still holds. A graduate of Harvard Medical School, she has invaluable experience as an ER physician and extensive training in internal medicine.

