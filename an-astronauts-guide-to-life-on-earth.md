---
id: 54f5baeb646133000a4c0000
slug: an-astronauts-guide-to-life-on-earth-en
published_date: 2015-03-06T00:00:00.000+00:00
author: Chris Hadfield
title: An Astronaut's Guide to Life on Earth
subtitle: What Going to Space Taught Me About Ingenuity, Determination, and Being Prepared for Anything
main_color: 279AC2
text_color: 175D75
---

# An Astronaut's Guide to Life on Earth

_What Going to Space Taught Me About Ingenuity, Determination, and Being Prepared for Anything_

**Chris Hadfield**

Test pilot and astronaut Chris Hadfield was the first Canadian to space walk, and this is his story. He offers insight into life in the space business, from training and lift-off to space research and coming home. He outlines the surprising challenges astronauts face, both off and on this planet, and offers some of the wisdom he gained from leaving our natural home and coming back down to Earth. Even if you never make it to the Milky Way, you'll find that we have a lot to learn from spacemen.

---
### 1. What’s in it for me? Learn what it takes to be an astronaut. 

Have you ever wondered what life as an astronaut would be like? Hours of gazing at beautiful planets and floating around in space? In fact, spacewalking makes up just a tiny fraction of the job.

In these blinks, you'll discover that the biggest part of an astronaut's life is taken up with long periods of pre-flight preparation far from family and home. Obviously, being in zero gravity is loads of fun, but before that can happen, an astronaut has to put in some serious work down here on Earth. Once she's in orbit, even the simplest tasks are complicated, and as for getting home in one piece? Well, let's just say it's hard to stay on target.

You'll also learn

  * that the ratio of time on Earth to time in Space is many months to a single day;

  * why washing your hair isn't easy in space; and

  * how even sitting on a chair can be uncomfortable after an astronaut gets back from space.

### 2. Astronauts must have a wide variety of skills and spend a lot of time training. 

What do astronauts do when they aren't in space? They spend most of their days training, taking classes and studying. So if all you want to do is whip around the earth in a spaceship, you'd hate being an astronaut.

Astronauts need a tremendous range of skills. Some are more obvious, like piloting the rocket, walking in space, repairing pieces of the space station and monitoring the experiments on board.

Since they're away from civilization for so long, however, astronauts also need to prepare for some less obvious tasks. They have to be able to perform basic surgery and dentistry, program their computers, rewire any electrical panels and conduct a press conference. They also have to get along with their colleagues while living with them 24/7 in a confined space!

Astronauts must be prepared to respond to any crisis that could occur in space. When they're orbiting the Earth at 400 km, they certainly can't expect a rescue crew to come get them.

There are many dangerous potential situations. Toxic gas could start leaking or a fire break out, for instance.

Fires aren't hypothetical situations, either. In 1997, an oxygen-generating canister started a fire on the Russian space station _Mir_. The astronauts managed to put it out by throwing wet towels on the canister. Their spacecraft filled with smoke, but thankfully everyone survived.

The ratio of time spent in space vs. time spent preparing for space is quite low: for each day in orbit, you have to train for several months. It takes several years of training before you're assigned to a mission, and then training for a specific missions takes between two and four more.

> _"If you're striving for excellence — whether it's in playing the guitar or flying a jet — there's no such thing as over-preparation."_

### 3. Preparation is essential for life in space – and life on Earth too. 

On a space mission, preparation is everything — no matter how intelligent or experienced the astronaut. So, during training, astronauts spend most of their days studying and simulating experiences they may never actually go through.

Simulations provide more than just practice: they frequently serve as wake-up calls. Trainers specialize in devising unpredictable bad scenarios for astronauts to work out. They practice what to do if there's engine trouble, a computer meltdown or an explosion, to name just a few examples.

This training pushes astronauts to develop a new set of instincts. Instead of reacting to danger with a fight-or-flight response, they're trained to respond calmly and immediately. They must prioritize threats and solve them methodically.

So astronauts prepare for nearly every conceivable problem that could occur between launching and landing. This also helps them build their improvisational skills for the situations they can't predict.

This training is useful back on Earth, too. It's very helpful to have plans for dealing with any unpleasant obstacles life might present.

Chris Hadfield's training became ingrained as a form of mental discipline, not just in space but in his regular life, too. When he gets into a crowded elevator he thinks, "What are we going to do if we get stuck?"

This isn't the same as worrying all the time. It's actually the opposite: planning for bad situations usually gives you more peace of mind.

So though astronauts have many abilities (both physical and mental), their core skills are all about solving complex problems, even with incomplete information and in a hostile environment.

> _"We're trained to look at the dark side and to imagine the worst thing that could possibly happen."_

### 4. Criticism is vital to an astronaut’s survival. 

It's always better to view criticism as potentially helpful advice rather than a personal attack. For an astronaut, criticism is a basic survival skill.

At NASA, everyone has to be a critic. When astronauts work through their simulations, dozens of people watch them, in the hope that they'll catch any flaws. The more the astronauts are critiqued, the safer they'll be.

Every small blunder is scrutinized to make sure it won't happen again. Over the years, hundreds of people will critique an astronaut's performance. It's a regular part of the job.

The goal of all the criticism is to establish a list of _Flight Rules_ that outline the steps to take if any problems arise. These rules prepare the astronauts to face any problems confidently, and they help protect against the temptation to take dangerous gambles.

Criticism should never be personal or involve ridicule, however. Even when you have to offer some harsh criticism, it should never be focused on the individual. You should point out the problem in a cool and detached manner, never teasing the person — even when it's tempting.

These rules are useful in all areas of life, but especially in the space business. If a team faces a serious problem — like a medical emergency or equipment failure — they only have each other to depend on for survival.

And if your crewmates are the only people in the universe who can save you from some sort of threat, you certainly don't want them to dislike you because you've ridiculed them in the past!

So whether you're in the space business or any other line of work, strive to create an environment where people can learn from their mistakes in constructive ways. The whole team should learn collectively from failures, without anyone being made to feel bad.

### 5. Astronauts are physically away from their loved ones for long periods, so they have to find other ways to stay close to them. 

Missions can last for several months. Naturally, the extended absence required for training and missions is hard for astronauts and their families.

From 2007 onward, Hadfield spent six months per year training in Moscow. He also trained in the United States, Japan, Germany, Canada and Kazakhstan. He was only home for about 15 weeks per year, and missed quite a few birthdays and holidays. He effectively became a visitor in his own home.

Astronauts have to anticipate these difficult periods, and try to make it up to their families in advance.

Before his last mission, Hadfield sat down with a calendar and planned for the times he'd be away. He realized he wouldn't be there for Valentine's Day, so he arranged a card and a gift in advance so they'd arrive for his wife on the right day.

When he realized his launch overshadowed his son's 16th birthday, he knew he'd have to do something special. So in interviews he announced that his crew would be lighting the biggest candles — the rocket's engines — to celebrate the birthday, making his son feel happier.

Lots of people have demanding jobs, even if they aren't astronauts. So if you know you can't be around much for your loved ones in the future, try to make it up to them in other ways. You'll feel more connected with them, even when you're away, and they'll be reminded that you love them even though you can't always be there.

### 6. Living on board the International Space Station is a lot of fun, but also very serious. 

The International Space Station (ISS) is as big as a football field and weighs over one million pounds. It has so many modules that you can go a full day on board without seeing another person. So what does life in the ISS actually feel like?

Well, the quality of life onboard is sort of similar to that on a long sailboat trip. It's remote and there's no running water, because water would cohere into blobs, float away and wreck the sophisticated equipment that the ship runs on.

Privacy and fresh produce are also in short supply. Hygiene is very basic — obviously there are no long, hot showers. Astronauts must wash their hair by scrubbing their scalps with non-rinse shampoo, then dry it very carefully so stray hairs don't end up floating all over the spacecraft.

They also need to exercise for two hours a day to maintain their muscle strength. If they don't, they won't be strong enough to even stand when they get back to Earth. It certainly might be tempting to just float around all day, but it'd be very dangerous.

Fun floating aside, an astronaut's work is very serious. The ISS primarily functions as an enormous laboratory.

Astronauts on the ISS spend their time researching ways to venture further into space — and stay alive and healthy while doing so. Their work also applies to other fields, however: experiments and research conducted in space have influenced many fields, including medicine and robotics.

Medical experiments in space have revealed methods for warding off one type of osteoporosis, and some of the machinery used inside nuclear power plants is based on machinery developed for the ISS.

Data gathered on the ISS also helped power Google Maps.

> _"Remove that one variable, gravity, and everything changes."_

### 7. Returning and readapting to Earth is very hard. 

When astronauts have finished their missions, how do they get back to Earth? And how are their lives different once they arrive?

Astronauts return to Earth in the Russian Soyuz spacecraft, but Soyuz landings are notoriously rough.

Since the American Space Shuttle was withdrawn from service in 2011, the Soyuz has been the only way to reach and depart from the ISS.

The return is a very wild ride. The re-entry into the atmosphere is basically a one-hour tumble — nearly everyone who's experienced it has an extreme story about it.

Yuri Malenchenko, for example, said that during his 2008 landing on the Soyuz, the parachute designed to decelerate the re-entry capsule caught fire, and eventually burned up entirely. The crew survived the landing, but they ended up a long way from their intended target, with no one to meet them on the Kazakh Steppe.

They eventually met with some locals who, drawn to the smoke, asked, "Where did you come from?" and "What about your boat? Where did the boat come from?"

Even after you've survived the landing, your body needs time to readapt to gravity again. The general rule of thumb is that it takes a day on Earth to recover from each day in space.

When you return to Earth, you feel like your body has aged a lot. After you get used to floating weightlessly in the air, sitting on a chair and feeling your own weight is very uncomfortable.

Despite the drawbacks, returning to Earth can be a transformative experience. Many people assume that after astronauts have seen space, life on Earth must be dull. For Hadfield, the opposite has been true: he says he returns home energized, with a new and inspiring view of the world.

> _"See, a funny thing happened on the way to space: I learned how to live better and more happily here on Earth."_

### 8. Final summary 

The key message in this book:

**Floating in space is incredibly fun, but an astronaut's work involves much more. If you want to be an astronaut, you've got to spend years preparing, hear hundreds of people criticize you, stay away from your family for long periods and endure much physical stress. If you have any trouble in space, no one can come to your rescue except your crewmates. Despite the hardships, however, it's very rewarding work, and life in space can give you a better perspective on life on Earth.**

Actionable advice:

**Plan for the times you'll be away.**

Even if you aren't an astronaut, you've probably caught yourself sacrificing time with your loved ones so you can work more. It's OK if you have to work, but plan for it. If you know you're going to miss significant dates, or just generally be around less, make it up to your family in other ways. Arrange gifts for them in advance, and send special messages to them. It'll make the distance much easier to bear.

**Suggested further reading:** ** _Awaken The Giant Within_** **by Anthony Robbins**

_Awaken The Giant Within_ argues that, ultimately, we're all in control of our own lives, and that by changing our habits, controlling our emotions and believing in those things we want to believe, we can make our ideal life a reality.
---

### Chris Hadfield

Chris Hadfield has become one of the most experienced astronauts in the world since his selection by the Canadian Space Agency in 1992. He's also served as Director of NASA Operations in Star City, Russia, and he was the Chief of International Space Station Operations from 2006 to 2008.

