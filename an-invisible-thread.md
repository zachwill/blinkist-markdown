---
id: 566ffa83cf6aa30007000043
slug: an-invisible-thread-en
published_date: 2015-12-17T00:00:00.000+00:00
author: Laura Schroff and Alex Tresniowski
title: An Invisible Thread
subtitle: The True Story of an 11-Year-Old Panhandler, a Busy Sales Executive, and an Unlikely Meeting with Destiny
main_color: A92435
text_color: A92435
---

# An Invisible Thread

_The True Story of an 11-Year-Old Panhandler, a Busy Sales Executive, and an Unlikely Meeting with Destiny_

**Laura Schroff and Alex Tresniowski**

_An Invisible Thread_ (2011) recounts the story of the incredible and unlikely friendship between a successful businesswoman and a poverty-stricken young boy — a friendship that ultimately changed both of their lives forever.

---
### 1. What’s in it for me? See the impact a chance meeting can have. 

Life is busy. We hurry to get to work, pick up the kids from school, get groceries and do errands. In our constant hurry we rarely see the people around us. Just think of all the people you pass on the street who don't have homes or schools, or errands to run — who look and live nothing like you do. How would your life change if you stopped and looked at the people you usually rush past?

This is the story of Laura, an advertising executive and one of the authors of this book, and an eleven-year-old boy named Maurice. It's about how their unlikely friendship challenged and changed their lives. It's the story of what can happen if you raise your eyes from the pavement and look at the people walking it.

In these blinks, you'll discover

  * why lunch in a brown paper bag says that someone cares about you;

  * how a holiday recital changed Maurice's view on parenting; and

  * why having a huge dinner table epitomized Maurice's ambitions in life.

### 2. An invisible thread may connect the people who are destined to meet. 

Maybe it sounds cheesy, but some chance encounters seem to be guided by fate. At least that's how it seemed when the author, Laura Schroff, and Maurice Mazyck met on the busy streets of downtown New York City on Labor Day in 1986.

Laura Schroff, Caucasian and single at the age of 35, was a successful advertising executive for _USA Today_. Maurice was an eleven-year-old African American kid on the verge of homelessness.

Laura had plenty of reasons to keep on walking when Maurice asked her for spare change. She didn't have to stop and take him to McDonald's for lunch. After all, she was a busy advertising executive; her schedule was tight, and stopping to treat eleven-year-olds to lunch was certainly not part of it.

Furthermore, the mid-1980s witnessed a dramatic widening of the gap between rich and poor; more and more people were forced to live on the street. Because poverty was so prevalent, it would have been easy for Laura to ignore Maurice and simply continue on her way.

So why did she stop? The only explanation Laura can give is that the hand of destiny somehow guided this chance meeting. There is a Chinese proverb that speaks of an _invisible thread_ that connects those that are destined to meet; in other cultures, it's been called a _string of fate_ or a _thread of destiny_.

Whether it was fate or serendipity, Laura stopped in her tracks that afternoon, and rather than just giving Maurice the spare change he had asked for, she took him to lunch. With this action she began a friendship that would continue for the next 30 years.

> _"When we met we were just two people with complicated pasts and fragile dreams."_

### 3. A tough upbringing nearly killed Maurice before he ever met Laura. 

Before his fortuitous meeting with Laura, Maurice had only known extreme poverty. He'd grown up in an unstable and often life-threatening environment.

Born into a world of criminality and violence, Maurice was neglected by his father, a feared member of Brooklyn's Tomahawks gang. Once, while in his father's care, Maurice, half-starved, was left to scrounge for food in garbage cans.

Maurice's mother, Darcella, was equally tough. During one heated conflict, Darcella repeatedly stabbed Maurice's father as the five-year-old Maurice looked on in shock. 

Maurice's family life wasn't just punctuated with outbursts of violence alone. Drugs, and the horrific effects of addiction that accompany them, were just as prevalent.

Throughout his childhood, Maurice and his two older sisters were dragged from one single-room apartment to another as their mother and uncles scraped together money by selling crack cocaine.

During this time, his mother was battling an addiction to both crack and heroin. This was the standard routine: the adults in Maurice's life did drugs at night while the children slept and, come morning, the adults would sleep it off while the children spent their day out on the streets.

Maurice remembers the times when his mom would shoot up as some of the best. Finally, she would be peaceful and happy — a pleasant break from the feverish anxiety of finding the next fix.

She was indeed very addicted. One time Darcella needed her fix so badly that she just couldn't wait to get home. So, she had the children stand in a circle around her on the subway to shield her from lookers-on as she shot heroin.

Even traditionally happy occasions were tainted by drugs. For example, Maurice's first ever birthday present was a joint, given to him on his sixth birthday by his grandmother Rose. While Rose's six sons helped raise Maurice, all were in and out of jail, often involved in dealing drugs and prone to violence.

### 4. Laura’s childhood and previous experiences taught her the importance of getting a lucky break. 

Laura's own childhood in suburban New Jersey was fraught with tension and chaos, from which she'd longed to escape.

Her father was the kind of alcoholic who is kind and pleasant during the day, but after drinking gets bitter and angry — and prone to violent outbursts. Laura's mother wasn't one to provide much clarity regarding these outbursts; the next morning, she would simply tell the kids to act as if nothing had happened.

And of course, it didn't help that her father's steadiest job was as a bartender.

Her mother and eldest brother, Frank, were the main targets of her father's drunken rages, which, usually ending with broken furniture, left the family terrified and tearful. 

Laura would dream of becoming an airline stewardess and flying far, far away — away from her hometown, away from the yelling and fighting.

Things didn't quite go as planned, however. Laura didn't do very well in school, didn't get into college and failed the exams she needed to pass to become a stewardess. But she did end up getting a lucky break that put her on the path toward success.

Though she failed to get the job she'd initially wanted, she kept on applying to a stewardess program offered by Icelandic Airlines. Despite numerous failed attempts, her perseverance eventually landed her a job as a receptionist.

From there, she went on to interview for a position as a sales representative with _Travel Agent Magazine_. Despite having no prior experience, she used her natural charm and persuasiveness to convince the magazine to hire her.

This one big break brought her into the world of advertising sales, which eventually led to financial success with _Ms. Magazine_ and _USA Today_.

> _**"** Sometimes one good break is all you need."_

### 5. Laura and Maurice’s first meetings were important for establishing trust. 

After Laura took Maurice out to eat for the first time, Maurice thought, quite naturally, that this friendly white lady would be done with him. But Laura couldn't get him out of her mind; she wanted to know more about him. What was his story? What could she do to help?

She initially questioned her intentions; helping Maurice was giving her a sense of fulfillment that her job and single life in Manhattan simply didn't offer. She wondered if she was being patronizing or exploitative by wanting to help Maurice. In the end, though, she realized that her intentions were pure.

Three days after that first meeting, Laura sought out Maurice again, treated him to another lunch and suggested establishing a lunchtime routine in order to get to know each other. The two agreed to meet up for lunch every Monday.

After treating Maurice to some lunches at places like the Hard Rock Cafe and learning more about his life, Laura decided to take their relationship to the next level by offering Maurice a home-cooked meal at her studio apartment.

Laura's friends were initially supportive of the way that she was helping Maurice, but some felt that inviting him to her home was crossing a line. What if his family tracked her down, or Child Protective Services came knocking on her door?

But Laura saw the goodness in Maurice, and realized that they were establishing a genuine friendship; she wanted to demonstrate her trust in him.

So, before Maurice came over, the two of them made a friendship pact: Laura wouldn't betray his trust as long as he didn't betray hers. They shook on it.

After dinner, Laura had a surprise for Maurice: tickets to a Mets game.

> _"Only years later would I be able to take the full measure of what that handshake meant."_

### 6. A baseball game and lunches in brown paper bags were important firsts for Maurice and Laura. 

Going to see the home team on the baseball diamond or getting lunch in a brown paper bag from a parent or guardian might seem like a basic part of any childhood. For Maurice, these were deep, life-changing moments.

Taking Maurice to a New York Mets game was an eye-opening experience — for both of them. Maurice was initially excited when Laura offered to take him to a game, but when she asked Maurice to have his mom sign a permission slip, he didn't show up at the agreed time.

So, Laura decided to visit his home, the Bryant Hotel, just a few blocks away from Times Square.

Laura had never seen such substandard living conditions in her life. The Bryant Hotel was noisy, chaotic and filled with people both young and old. It had an overwhelming stench. Trash covered the floors and graffiti snaked across the crumbling plaster walls. 

When a security guard escorted Laura to Maurice's room, his mother was so intoxicated that she could barely stand up to answer the door. Maurice's grandmother Rose had to sign the permission slip in Darcella's stead.

With the permission slip signed, Maurice was able to go to the baseball game, and the look of joy on his face when he walked into the stadium will remain with Laura forever. But Maurice, being protective of Laura, made her promise that she would never again return to the Bryant Hotel.

As time went on, Laura realized that Maurice would sometimes go without food for days before their Monday meetings. So, she offered to leave a packed lunch for him that he could pick up from her building's doorman on his way to school.

Maurice's sole request was that she put the lunch in a brown paper bag. Why? Because, as he explained to her, when kids at school have brown paper-bag lunches it means someone at home cares about them.

> _"A simple brown paper bag… To me, it meant nothing. To him, it was everything."_

### 7. Holidays in the suburbs gave Maurice hope for a new kind of life. 

Baseball games and brown-bag lunches weren't the only new experiences Maurice had as a result of his friendship with Laura. He'd never left New York City's five boroughs before. So, when Laura took him to visit her sister's family in the suburbs of Greenlawn, Long Island, he witnessed a family dynamic that he'd never seen before.

If you've lived in one-room urban apartments your entire life, seeing how a suburban family lives can lead to revelations. Indeed, Maurice was amazed at the things he saw at Laura's sister's house.

To him, simple things like a yard to play in and unshared rooms for the kids were remarkable. Maurice's life was utterly different: if he wanted to get some time away from his sisters and uncles without leaving the house, he'd sometimes have to sit in a closet.

His biggest revelation, however, was sitting at the big dinner table and seeing everyone happy and engaged in conversation. If someone in Maurice's family ever brought home food, he was used to eating it on the floor all by himself.

Maurice was moved by the experience. On the way home, he told Laura that one day he too would have a big dining room, around which his entire family would eat together. It was the first time Laura had ever heard him talk optimistically about the future.

The holidays in the suburbs also offered Maurice a look at a way of parenting that was totally different from what he was used to.

During the Christmas holiday, Laura and Maurice went to one of her niece's recitals at the local church. When Laura's niece became upset, and started crying, Maurice worried that, if she didn't stop, she'd get a beating, because that's what he was accustomed to seeing.

Instead, he saw her father comfort her and calm her down. It made him realize that not all parenting methods are the same — some are clearly better than others.

> _"I always felt terrible saying goodbye to Maurice, because I knew what he had to go back to."_

### 8. Balancing a personal life with her obligations to Maurice wasn’t always easy. 

Many years into their friendship, Laura met Michael, a man who seemed to be the man of her dreams, someone she would like to marry. Unfortunately, however, this new relationship eventually led to difficulties. Ultimately, she had to choose between her marriage and her friendship with Maurice.

When Laura first met Michael, he seemed like the perfect guy, but there was a catch. On the one hand, he was successful and good to her; on the other hand, he was suspicious of Maurice and refused to even allow him into his house.

Laura eventually moved into Michael's house outside the city, but worried that she was letting Maurice down despite having made a promise to herself that she would never abandon him even if things got tough.

While she and Maurice continued to stay in touch and meet regularly, when she and Michael got married, Maurice wasn't there. She knew that he was missing out on a big part of her life.

Her marriage to Michael wasn't getting off to a good start. He refused to budge on the idea of allowing Maurice to spend time with them, and he didn't want to have a child with Laura. In the end it didn't work out, and their marriage came to an end.

Laura was now in her forties, and had resigned herself to the fact that she would never have a child of her own. Helping Maurice and watching him grow up became all the more important to her. In essence, he was the child she never had. 

While this revelation was profound, it didn't instantly make everything smooth and easy. Maurice and Laura still had one major hurdle to overcome in their relationship.

### 9. When Maurice disappeared, Laura feared the worst. 

Maurice was growing up, but Laura had trouble really seeing him as a budding adult. All illusions were shattered, however, when Maurice met a girl and had a child with her.

Laura voiced her disapproval, believing it irresponsible to have a child while so young. But it didn't matter. Maurice needed to let Laura go in order to leave boyhood and become a man.

One day Maurice asked Laura for money to buy winter coats for his new family, and then suddenly disappeared — for three long years — leaving Laura, with no clue where he might be, to worry and mourn.

In the eight years they'd known each other, Maurice had never asked for money. So what changed? It turned out that he had had a second child, and needed help to support his new family. But he couldn't tell Laura; he didn't want to disappoint her or have her think any less of him because of it.

During his time away from Laura, he tried to figure out his life on his own and to provide for his family without her help. So, he got a job as a messenger and sold bootleg clothing on the side — a business which would eventually put him in the middle of a gunfight, from which, luckily, he managed to escape unharmed without touching a gun.

While this was going on, things weren't going well for Maurice's mother. After years of battling addiction, Darcella was succumbing to HIV. She eventually died, and when Maurice identified her body at the hospital he was surprised at the relief he felt for her. She looked peaceful, as if her burdens had finally been lifted.

The first person he called after the funeral was Laura, who, after having heard nothing from him for three years, was relieved to finally hear his voice. "You're my mom now," he told her.

Laura's relief was immense. After all, the odds didn't seem to be in his favor. By this point, almost all of his uncles and cousins were either dead or in prison.

> _"When I saw Maurice, he looked older, more mature, he was a man now."_

### 10. Maurice and Laura both learned a great deal through their extraordinary friendship. 

On Laura's fiftieth birthday, she decided to throw a big party and surround herself with friends and family. After numerous toasts, Maurice stood up and shared what he'd learned from his friendship with Laura.

Maurice and Laura both believe that they are lucky to have met each other. Maurice likes to say that "the lord sent me an angel," but Laura also believes that Maurice was brought into her life with the help of some higher power or guiding hand.

On her deathbed, Laura's mother told her children that she would still be looking after them even after death. Laura wonders if it wasn't her mother that caused her to stop on the sidewalk that fateful afternoon in 1986 and take Maurice out to lunch.

Having the opportunity to help Maurice grow into an adult taught Laura a great deal about courage and resilience in the face of overwhelming adversity, as well as the true value of money and being happy with what one has.

With Laura's help, Maurice was able to navigate an environment that had entrapped or killed many. As an adult, he went on to get his GED and enroll in Medgar Evers College in New York City. While in college, Maurice learned, from an article he'd read, that there were more black men in prison than in college. This fact struck him with such force that he started a prison-to-college program for the school's Male Development Program, and was eventually hired as the research director for the Fatherhood Initiative Program.

After graduating, Maurice started a construction firm, and is now a proud husband and father of five, with a nice apartment overlooking both the Manhattan and Brooklyn bridges. And yes, he got that big dining room table that he'd always wanted for his family.

### 11. Final summary 

The key message in this book:

**The extraordinary friendship of Laura and Maurice proves that it's possible to break the cycle of violence and neglect that exists in some families. Even the simplest chance encounter can have a far-reaching and positive influence on the lives of those around you.**

**Suggested** **further** **reading:** ** _A Long Way Gone_** **by Ishmael Beah**

_A Long Way Gone_ (2007) is a story of how, as a young boy in Sierra Leone, the author found himself caught in a civil war and recruited as a child soldier. You'll travel alongside during his harrowing journey, eventual rescue and recovery guided through the kindness and grace of loving people.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Laura Schroff and Alex Tresniowski

Laura Schroff is a former ad executive for numerous publications, including _USA Today_, _People_ magazine and _InStyle_.

Alex Tresniowkski is a senior writer for _People_ magazine and author of _The Vendetta_, which inspired _Public Enemies_, the 2009 Michael Mann movie. His other books include _Labor of Love_ and _When Life Gives You Lemons_.

