---
id: 58a9a3438e98580004e55629
slug: anatomy-of-an-epidemic-en
published_date: 2017-02-24T00:00:00.000+00:00
author: Robert Whitaker
title: Anatomy of an Epidemic
subtitle: Magic Bullets, Psychiatric Drugs, and the Astonishing Rise of Mental Illness in America
main_color: CE2939
text_color: B52432
---

# Anatomy of an Epidemic

_Magic Bullets, Psychiatric Drugs, and the Astonishing Rise of Mental Illness in America_

**Robert Whitaker**

_Anatomy of an Epidemic_ (2010) traces the development of mental health medications and the relationship between pharmaceutical companies and the field of psychiatry. These blinks bust the myths we're told about such drugs, and, in the process, may make you question the wisdom of fighting psychological ills with a panoply of pills.

---
### 1. What’s in it for me? Learn why mental illness has become a modern epidemic. 

Do you have friends who take medication for depression, anxiety or other mental health issues? Or maybe you take such medications yourself? Most people will answer yes to at least one of these questions. In fact, the modern upswing in mental health issues is so drastic that it almost constitutes an epidemic. How did we get here?

We're familiar with viral or bacterial epidemics. Indeed, they're the most common. But epidemics needn't stem from infectious diseases. This is the story of how chance, private interests, the pharmaceutical industry, legislators and physicians got caught up in an intricate dance that resulted in a plague of mental illness that's consuming America.

In these blinks, you'll learn

  * how psychoactive drugs were invented;

  * why psychiatry's low status led to mass medication; and

  * that government spending on mental health has almost tripled in 14 years.

### 2. Popular psychiatric medicines were introduced without proper testing. 

It's more than likely that you or someone you know takes a prescription medicine for emotional or psychiatric reasons.

The statistics paint a pretty clear picture:

One out of every eight Americans takes psychiatric medication — including children and infants.

This wasn't always the case: between 1985 and 2008, the yearly sales of antidepressants and antipsychotics in the United States increased nearly fiftyfold, eventually reaching a whopping $24.2 billion.

That's big business today, but to trace this phenomenon back to its beginning, we need to look at how such drugs were introduced.

Today, psychoactive drugs are used to treat conditions such as anxiety, depression and mania. But that wasn't their intended purpose. Originally, doctors were looking for so-called "magic bullets," or miracle cures, to fight infectious diseases. But in this search they inadvertently invented something else.

In the post-World War II era, between 1954 and 1957, researchers stumbled upon different compounds that affected the central nervous system. While testing the efficacy of these drugs against disease, the researchers found that some of them could dramatically curb a person's normal physical and emotional responses without causing a loss of consciousness.

This isn't how drugs are normally developed. Usually, research is devoted to finding a _cause_ for an ailment, followed by the development of a medication that will cure the _effects_ of that ailment.

This is how insulin therapy was developed, once research showed that diabetes was caused by insulin deficiency.

But since the new psychoactive drugs weren't designed with any particular ailment in mind, they weren't tested well before going to market.

The pharmaceutical company Smith, Kline & French tested its new mental illness magic bullet, _Thorazine,_ on less than 150 psychiatric patients before applying for FDA approval. The company's president nevertheless claimed that Thorazine had been thoroughly tested on over 5,000 animals and had "proved active and safe for human administration."

In 1954, it was put on the US market and advertised as a treatment for schizophrenia, anxiety and bipolar disorder.

### 3. Psychiatric medications can cause severe side effects and long-lasting dependency. 

One of the biggest problems with limited testing is that it doesn't allow time for a medication's possible side effects to emerge. And we now know that psychoactive drugs have side effects in spades.

When psychiatric medications are used for long periods of time, they can cause significant and long-lasting changes to a person's neural functions. After just a few weeks on a psychoactive drug, the brain's natural defenses begin to weaken, which is when side effects start to manifest.

The most commonly prescribed antidepressants are selective serotonin reuptake inhibitors, or SSRIs. These result in an increase in _serotonin_, a naturally produced bio-chemical that aids the function of the brain and nerve cells. In excess, however, it can also cause episodes of mania.

Antipsychotic medications, on the other hand, work by blocking a specific neurotransmitter called _dopamine_. But reducing dopamine can have unpleasant side effects, too. Those who suffer from Parkinson's disease also have low dopamine levels, so it's no surprise that people taking antipsychotics experience tremors and impaired motor function.

Other common side effects include memory loss and reduced learning ability, as well as weight gain, suicidal thoughts and apathy. These symptoms are often treated with yet more drugs, leaving many patients with a daily cocktail of pills.

The prescriptions tend to snowball. Say you take an SSRI and end up feeling manic. This results in your being diagnosed with manic depression. So you get treated with an additional antipsychotic drug, which might cause another symptom. And so on. It's not unheard of for patients to take six psychoactive drugs per day.

Evidence of other adverse side effects continues to emerge. For instance, neuroscientist Nancy Andreasen published a paper connecting antipsychotic drugs to brain shrinkage, suggesting a direct correlation between dosage, duration of intake and reduction in brain size.

Another problem is getting patients off the drugs, as withdrawal can send the brain's chemicals into shock.

When people stop taking the SSRI _Celexa_, their serotonin levels fall dramatically. And being taken off antipsychotics can cause dopamine levels to skyrocket. This can lead to a slew of side effects: insomnia, apathy, suicidal thoughts, mania — symptoms that are hard to distinguish from a patient's pre-treatment condition and can result in their being put back on the medication.

Ultimately, withdrawal can result in months of agony, highlighted by seizures and panic attacks.

> _"[Psychoactive drugs] cause the prefrontal cortex to slowly atrophy."_ \- Nancy Andreasen

### 4. As more psychiatric drugs became available, the number of mental disabilities in America skyrocketed. 

As we've seen, sales of psychiatric drugs have risen sharply in recent years. So it stands to reason that diagnoses of mental disorders have risen in tandem with those numbers.

Since 1955, when these drugs were first introduced, the number of mentally disabled people has risen dramatically.

It's also worth noting the rise in mentally disabled people who also qualify for federal assistance, such as Supplemental Security Income or Social Security Disability Insurance. Between 1987 and 2007, this number more than doubled, going from one in every 184 Americans to one in every 76.

The American Psychiatric Association (APA) is responsible for setting the criteria that determine mental illness in the United States and they've established four broad categories:

Anxiety disorders, including phobias and post-traumatic stress disorder (PTSD).

Mood disorders, such as severe depression and bipolar disorder.

Impulse-control disorders, such as attention-deficit/hyperactivity disorder (ADHD).

And, finally, substance-use disorders, including alcoholism and drug abuse.

These categories are so wide ranging that, between 2001 and 2003, the National Institute of Mental Health conducted a massive survey and found that 46 percent of the population met the criteria of at least one mental illness. Most of them met the criteria for multiple diagnoses.

Perhaps more disturbingly, there's been a mind-boggling rise in the diagnosis and treatment of mental illness in children.

In the 20 years between 1987 and 2007, the number of children diagnosed with a mental disorder has increased more than thirtyfold, with some patients being as young as two years old.

As a result, mental illness is now the number one disability in children, and ten percent of all ten-year-old boys take medication for ADHD, while approximately 500,000 children in the United States overall take some form of antipsychotic drug.

### 5. Overprescribing psychiatric medications can leave many patients worse off. 

Remember that these psychiatric medications were intended to be a "magic bullet," a perfect cure that could kill an infectious disease. And yet they've created more problems than they've solved.

Part of the issue is that the mental health community quickly began over-prescribing these medications, even giving them to patients for whom the drugs were never approved.

This especially applies to children, who often receive treatment that includes drugs that aren't FDA approved for their age group and that may lead to severe side effects.

While the drugs can provide short-term relief for some symptoms, they can also cause long-term harm to the brain's chemistry, and this damage can persist long after the original symptoms would have naturally subsided.

Therefore, treatments for schizophrenia and depression can actually make the symptoms far worse. Before medication, someone with schizophrenia may have had episodes that lasted a maximum of six months, offset by extended periods of normalcy. But after treatment, these episodes can become lifelong and chronic.

This isn't the case for everyone. Many people do benefit from regularly using antidepressants.

However, before the overprescription of these drugs became so widespread, many people would recover from their symptoms and go on to lead healthy lives. Today, on the other hand, there's a large number of patients who've been placed on long-term drug therapy and now find themselves chronically ill.

Indeed, rather than offering extensive data in support of psychiatric drugs, research actually shows them to be counterproductive.

Jonathan Cole, often referred to as the father of American psychopharmacology, wrote a paper in 1977 called "Is the Cure Worse Than the Disease?"

Cole reviewed the evidence of long-term damage caused by psychoactive drugs, including studies which showed how at least half of all schizophrenic patients could live fruitful lives without them. He conceded that antipsychotics were not the "magic bullet," or the life-saving drugs, that psychopharmacologists had hoped they were.

Further supporting Cole's paper was a 1998 study by the World Health Organization; it showed that long-term use of antidepressants was associated with an increased — not a reduced — risk of long-term depression.

### 6. When these drugs were introduced, the psychiatric profession was in crisis. 

To fully understand why the mental health field is in such disarray, it's important to consider the impact that these new drugs had on psychiatry itself.

Prior to the drugs' introduction, psychiatrists were not concerned with brain chemistry and neurotransmitters; they were completely focused on Freudian analysis. They saw any mental illness as stemming from unconscious conflicts, often originating in childhood — conflicts that caused emotional problems which were separate from the physical aspects of the brain.

But this changed with the arrival of psychoactive drugs. Psychiatrists shifted their attention to the brain, and they started caring less and less about the life stories of their patients.

This new approach began in the 1950s, when psychiatrists became primarily concerned with identifying symptoms and eliminating or reducing them with brain-altering medication.

An initial sense of hope accompanied the advent of these drugs, but this optimism soon soured. In the 1970s, the severe side effects began to become apparent. Meanwhile, the anti-psychiatry movement kicked into gear, entering the popular imagination with the publication of Ken Kesey's _One Flew Over the Cuckoo's Nest_.

Psychiatrists were also facing growing competition from other professionals, including psychologists and social workers, who treated patients without medication.

Some psychiatrists rejected the medical approach altogether. Some still clung to the Freudian model, and there were even those who regarded mental illness as a sane response to an increasingly insane world. These internal divisions further weakened the profession.

The lack of unity didn't help psychiatry's image. Within the larger medical family, psychiatrists were like a band of unpopular cousins. Despite their fancy new drugs, they were still considered less scientific than other specialists. And they generally earned less than their peers.

So what's a profession to do when it's crumbling and considered a joke by other professions? We'll find out in the next blink.

### 7. Psychiatry successfully re-branded itself in order to appear more medically legitimate. 

After years of inner and outer turmoil, psychiatry eventually struck back.

In the 1970s, the psychiatric profession underwent a radical re-branding. They launched a full media and public-relations campaign to assert their medical legitimacy and enhance their status.

As part of their campaign, the APA planned to publish the third edition of their _Diagnostic and Status Manual of Mental Disorders_ (DSM). This is the guidebook that lays out the diagnostic criteria for every recognized mental disorder.

The first two editions, published in 1952 and 1968, were largely unknown outside the profession, and they still reflected the old Freudian view of mental illness.

To make sure the third edition, known as DSM-III, accurately reflected their new point of view, the president of the APA hired Robert Spitzer, an admired psychiatry professor at Columbia University, to head the task force composing the manual.

Published in 1980, the new and improved DSM-III contained 265 diagnoses, a significant increase from the 182 in the previous edition. And it quickly became a universal tool for a wide array of professions, from insurance companies, hospitals and courts to schools, government agencies and the larger medical community.

It offered something many people were looking for: a consistent guide to psychiatric diagnosis, a tool that allowed different professionals presented with the same patient to discern the same problem.

It did this by breaking down each diagnosis with a list of symptoms and certain numerical thresholds. So, to meet the criteria for a certain mood disorder, you might need to have at least five of the nine symptoms listed.

The main goal of DSM-III, however, was to justify the use of psychiatric medications.

All licensed psychiatrists have passed their medical board exams and thus qualify as doctors with the authority to write prescriptions. But after the introduction of psychoactive drugs, some psychiatrists were starting to call themselves by a different name: _psychopharmacologists_. This terminology was another effort to lend credence to the myth that drugs could fix any mental illness by adjusting the brain's "chemical imbalance."

Their re-branding largely worked as intended, and, as we'll see in the next blink, psychiatry was an especial darling of the pharmaceutical industry.

### 8. Pharmaceutical companies continue to partner with psychiatrists, while the public pays the tab. 

It's no secret that the income of a doctor can rapidly rise with the ability to prescribe pharmaceuticals. Between 1950 and 1970, the income of physicians doubled, thanks largely to the increase in available prescription drugs.

Even the American Medical Association brought in ad revenue from drug manufacturers by selling space in their magazine, revenue that rose from $2.5 million to $10 million in just the ten years between 1950 and 1960.

These ads always paint a rosy picture. But they often don't tell the complete story about the drugs they're selling. For instance, in 1959 it was found that 89 percent of the ads contained no information at all about possible side effects.

The relationship between pharmaceutical companies and the APA eventually grew to include sponsored scientific symposiums.

These quickly became the APA's most popular events. Drug companies would provide lavish dinners and pay handsomely for a "panel of experts" to give testimonials about the effectiveness of certain drugs.

While these panels claimed to be "educational," they were nothing more than well-rehearsed advertisements; if someone dared to talk about things like side effects, they were never invited back. If you were considered a star in your field, perhaps a psychiatrist at one of the top medical schools, you could earn between $2,000 and $10,000 per speech.

The APA was earning good money as well, raking in millions of dollars from various partnerships with drug companies.

Revenues in 1980 were around $10.5 million; by 1987, that number had risen to $21.4 million. At this point, they opened a new office in Washington and were even talking openly about how pharmaceutical corporations were "partners in industry."

Yet, while the APA and its experts were getting rich, the American public was paying the cost.

In 2009, the federal Agency for Healthcare Research and Quality found that the personal costs of mental health services were rising faster than any other medical category.

In 2008, US citizens spent $170 billion on mental health services, twice as much as they paid in 2001. And it's expected to climb to $280 billion in 2015. That's a 240-percent increase since 2001.

Medicaid and Medicare programs pay for approximately 60 percent of these services, so in the end even perfectly healthy taxpayers wind up footing the bill.

### 9. Final summary 

The key message in this book:

**The history of psychoactive drugs is a disturbing one. Today, pharmaceutical companies and the psychiatry profession are complicit in a corrupt relationship to promote these drugs, and there's only scant evidence to back up their claims that what they're peddling will cure mental illness.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Shrinks_** **by Jeffrey A. Lieberman, Ogi Ogas**

_Shrinks_ (2015) tells the story of psychiatry's astonishing development throughout the centuries. These blinks take us on a tour of the discipline's crude past, its strange and shocking therapies and its great improvements.
---

### Robert Whitaker

Robert Whitaker is an award-winning writer who has provided extensive coverage on issues regarding mental health and the pharmaceutical industry. He is the author of several critically-acclaimed books, including _Mad in America_, _The Mapmaker's Wife_ and _On the Laps of Gods_.

