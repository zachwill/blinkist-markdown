---
id: 56a6ae35fb7e87000700001f
slug: and-the-good-news-is-dot-dot-dot-en
published_date: 2016-01-27T00:00:00.000+00:00
author: Dana Perino
title: And the Good News Is...
subtitle: Lessons and Advice from the Bright Side
main_color: B72C29
text_color: 9E2623
---

# And the Good News Is...

_Lessons and Advice from the Bright Side_

**Dana Perino**

_And the Good News Is…_ (2015) walks you through the life and times of Dana Perino. These blinks follow Dana's life — from her early childhood in rural Wyoming to her career on Capitol Hill — and offer applicable advice for success and personal improvement along the way.

---
### 1. What’s in it for me? Get to know Dana Perino. 

What do you really know about Dana Perino? Maybe you've seen her on television or know something about her political stance. But beyond that?

Right — not much.

What did her life look like before she became a famous figure in American politics? Where is she from? How was her childhood? What obstacles did she overcome on her way to success? 

These blinks answer such questions and more. They provide you with Dana's personal life story, describing landmark moments along the way — from living life on a ranch to working closely with president Bush in the White House and going on to achieve fame on the Fox News show, _The Five_.

In these blinks, you'll learn

  * how Dana's parents prepared her for a job in politics;

  * that being tough doesn't exclude being gentle; and

  * what Dana learned from Dick Cheney.

### 2. Spending lots of time on a Wyoming ranch taught Dana to be tough and independent. 

Dana Perino spent much of her childhood in rural Wyoming, on her grandparents' ranch. Her father's grandparents were Italian immigrants who'd grabbed the American dream by the horns; for them, and for Dana, too, the ranch was a symbol of freedom.

Not just that, but living on the ranch taught Dana the importance of always lending a hand. Even though it wasn't always fun, Dana and her sister helped out with all the chores: filling water tanks, opening gates, collecting eggs and keeping the calves fed during winter.

Sometimes Dana had to overcome her fears in order to help out.

For instance, gathering chicken eggs frightened Dana. The coop was dark and smelly, and the hens would peck at her hands when she tried to take the eggs. But she did it anyway. Life in rural Wyoming was rough, and everyone had to pull his or her own weight. Learning to overcome her fears and pitch in proved a valuable lesson later in Dana's life. 

Ranch life taught Dana other important lessons, too. For instance, that it's essential to be both tough and gentle. When she was about eight years old, Dana and her sister were in the truck with their grandfather when they noticed that one of the horses had a broken leg. The huge animal was whimpering and clearly in a lot of pain. 

Dana's grandfather got out of the car, grabbed his rifle and told the two little girls to stay in the truck with their heads down. Dana snuck a peak anyhow, and watched as her grandfather shot the horse. 

As they drove away, he rested his hand on her knee and squeezed it gently, showing her he knew how she felt. It was then that Dana realized that tenderness and strength are like peas in a pod.

### 3. Dana’s parents got her hooked on politics and showed her why America is great. 

The apple doesn't fall far from the tree: in Dana's case, this aphorism holds especially true. In fact, her father, a lover of politics, gave her an early taste for newspapers and government. 

How?

Dana's father subscribed to countless political magazines like _Time_, _U.S. News & World Report_, _National Review_ and _Newsweek_. Naturally, it didn't take long for the young Dana to become obsessed — so obsessed, in fact, that she insisted on her family attending the earliest church service on Sunday so she could get home in time to catch the morning talk shows. 

Furthermore, when Dana reached the third grade, her dad had her and her sister read _The Denver Post_ and _Rocky Mountain News_ every day before he got back from work. Then, before the family sat down to dinner, Dana would discuss two articles with him. Her dad would read the news stories, ask Dana questions and help her sort out her own arguments and responses, thus helping her cultivate skills that would later prove crucial. 

And Dana's mother?

Well, she taught her daughter about the greatness of America. For instance, her mother worked for a national program called Refugee Services, an effort of the Lutheran Family Services. Her job was to help refugees get settled in the United States. Many of these newly arrived families had escaped the Soviet Union, and Dana learned early on about the dangers of communism and that the United States was a safe haven for those seeking freedom. 

Her mother was also the reason Dana first traveled to Washington, DC, a city she would soon grow to love. Dana's mother had a friend who worked for President Carter and this friend took Dana, who was seven at the time, and her sister to visit the White House. It was on their flight home, watching the Fourth of July fireworks over the Washington Monument, that Dana fell in love with Washington.

> _"My dad got me hooked on news. That was a good thing."_

### 4. Dana was a high-achieving student and quickly secured a job on Capitol Hill. 

So, Dana found an early love for politics and, in high school, was elected student body president. Among other improvements made during her term, she altered the weekly schedule to allow more time for tutoring and studying. 

But she soon discovered that her real passion lay in the media. While in college, she got a job on _Standoff_, a debate show broadcast on a local television channel. This went so well that she earned her own show, _Capitol Journal_, a weekly series on political topics and legislative issues concerning Colorado. This show helped her make contacts that proved invaluable later in her career. 

Following her graduation in 1995, she quickly landed a job on Capitol Hill as an intern with a CBS affiliate. However, she felt that the staff was biased against republicans and hostile to conservative views. So she quit and took a job waiting tables. 

During this time her republican convictions deepened. It was in her blood to desire a limited government, a strong military and to value personal responsibility. So, when she heard that the Colorado State Senate was hiring a deputy press secretary she contacted the chief of staff to US Representative Scott McInnis, who had appeared several times for interviews on her show, and asked for a reference.

Instead, he offered her a job in Washington. The only problem was that the job was pretty boring — she would greet people entering the Capitol and answer phones. As a result, she quickly lost interest in the work. Luckily, her strong network soon came to her aid:

She was at a hockey game where she met Tim Rutten, an old friend who now worked in the Senate. He told her that Representative Dan Schaefer of Colorado was searching for a new press secretary. Three weeks later she had the job. 

But despite being highly accomplished by the age of 25, something was still missing from Dana's life.

### 5. After years of hard work Dana finally landed her dream job. 

In August of 1997, Dana got on a plane to Washington, DC. She was 25 years old and oblivious to the fact that the British man sitting next to her would soon be her husband. Nor did she know that this man would give her unwavering support and help her become the presidential press secretary in 2007. His name was Peter, and here's how it all happened:

In 2000, after Peter and Dana got married, an old friend of Dana's called her from the Bush campaign. The team was looking for a spokesperson in California. At that time, however, Dana was living in the United Kingdom with her husband. Luckily, he convinced her that she wouldn't be happy if she didn't give Washington another go and in August of that year they moved back to America. 

From there, Dana filled many positions: spokesperson for the Department of Justice, director of communications at the White House Council on Environmental Quality and, ultimately, her dream position — deputy press secretary. It made Dana the fastest Blackberry typist in the country but also meant working every waking minute. 

By 2007, she was ready to leave the White House. Her heavy workload was putting a major strain on her and a strain on her marriage. But then she was offered a position she couldn't refuse: press secretary to the Bush administration. She stayed on until the presidency ended in 2008, and then found another perfect job on the Fox News show, _The Five_. 

How?

Well, when Obama took office, Dana was out of work. At this time the CEO of Fox News, Roger Ailes, invited her to lunch and told her about his idea for an hour-long discussion show focused on current affairs, representing people with different backgrounds. 

This was _The Five_ and Dana was a perfect fit. With her media experience and time in the White House, she complemented the show's other members perfectly.

### 6. Success means cultivating good habits. 

Congresswoman Susan Molinari, a long time friend of Dana's, once gave her some solid career advice. In return, Molinari said, she wanted Dana to share the advice with others. Passing on information is just one of many rules of conduct that Dana thinks is essential to success. At the top of her list, however, are good manners — because Dana believes that politeness will restore America's lost civility. 

But how did the country lose its manners in the first place?

Well, over the last few decades, many people have grown dissatisfied with the government. This has resulted in a vitriolic political climate, one in which there's little space for civil behavior. Not just that, but a general lack of manners is making it even harder to solve the nation's problems.

Being polite shouldn't just be a personal goal; it should be a goal for the nation. There are a couple of key ways to practice good manners and the first is sharing credit with others. 

Dana learned this lesson from president Bush in August of 2008. The president had just received a call from the CIA, informing him that American hostages, held captive in Colombia by FARC guerillas, had just been rescued. 

However, the mission wasn't just an American victory. In fact, the Colombian Special Forces had been instrumental. Knowing that the Colombian president, Álvaro Uribe, had been taking heat for not doing enough to combat terrorism, Bush gave him credit for the rescue. 

Another essential skill is knowing when to keep quiet and listen. This lesson was passed on to Dana by Bush's vice president, Dick Cheney. 

Here's how:

In an interview, Cheney was once asked why he was often quiet in meetings. He said that staffers, worried that their opinions will come under fire, often say nothing, thus depriving the group of their valuable thoughts. So Cheney makes sure that everyone has had their say before he speaks up. That way, no one gets shut down, and all voices are heard.

### 7. Embrace risk, change and always work to improve yourself. 

In life, it's tempting to keep your nose glued to the trail. No need to seek change if change isn't necessary! However, moving forward sometimes means literally moving, even if that seems like a frightening or impossible thing to do. That's exactly what Dana learned over the course of her own journey: don't be afraid to move!

Nonetheless, Americans are becoming more and more afraid of risk and a fear of moving is increasingly common. So don't get trapped by your worry. Because changing locations can be the only way to advance your career — and if it doesn't pan out, or you're just plain homesick, you can always move back. 

It's also key to give your brain sufficient exercise. Dana came to this realization when Congressman Scott McInnis advised her to read the Review and Outlook sections of _The Wall Street Journal_ every day. She's done it ever since and, in her opinion, these expert editorials have helped her excel at both argumentation and writing. 

Not just that, but it's also exposed her to things she would otherwise never have known about!

And Dana's last piece of advice?

Stay focused on what's positive. In fact, Dana has always known that looking on the sunny side is essential to success. Therefore, it's imperative to overcome the daily barrage of negative thoughts — the thoughts that say we're not good enough. 

For instance, when Dana took over as press secretary from Tony Snow, she admitted to him that she wasn't sure how she was going to fill his shoes. He put a hand on her shoulder and said that she would do better than she imagined. This simple gesture boosted Dana's confidence and allowed her to refocus on the positive.

### 8. Final summary 

The key message in this book:

**With the help of her parents, Dana found an early passion for politics and excelled by following a few key points: she strives to improve herself, shares credit with others and knows when to listen. Abiding by this advice resulted in a long and fulfilling career.**

Actionable advice:

**Replace your gum with mints.**

Cultivating small habits can lead to big achievements. So, even though it might seem silly or trivial, avoid chewing gum in the workplace. Many employers (and employees!) find it distracting or even downright impolite. If you need to freshen your breath, grab a pack of mints!

**Suggested further reading:** ** _Hard Choices_** **by Hillary Clinton**

_Hard Choices_ offers a first-hand account of the trials and impressive diplomatic successes of the early years of the Obama administration. In this telling memoir, former Secretary of State Hillary Clinton places you at the administration's negotiating table where key policy decisions were made.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dana Perino

Dana Perino served as Press Secretary to the George W. Bush administration. She is currently a panelist on the hit Fox News show, _The Five_.

