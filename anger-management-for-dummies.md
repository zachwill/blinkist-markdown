---
id: 57878390c6cd380003a5c5ca
slug: anger-management-for-dummies-en
published_date: 2016-07-22T00:00:00.000+00:00
author: Charles H. Elliott, PhD, Laura L. Smith, PhD
title: Anger Management for Dummies
subtitle: Your one-stop guide to anger management
main_color: E52E41
text_color: B22433
---

# Anger Management for Dummies

_Your one-stop guide to anger management_

**Charles H. Elliott, PhD, Laura L. Smith, PhD**

_Anger Management for Dummies_ (2015) is a guide to dealing with your inner irascibility. These blinks will give you the tools you need to understand and — more importantly — master your anger. You'll learn methods for dissolving stress, dealing with provocations and even stopping temper tantrums in their tracks.

---
### 1. What’s in it for me? Be the boss of your anger. 

Does your own anger scare you sometimes? For instance, did you ever find yourself yelling at your terrified children or your bewildered partner, seemingly unable to stop? Do you go berserk when you're steering your car through heavy traffic and the driver in front of you is less than competent? Or do you worry that your short temper will eventually harm your career?

If you feel that your negative emotions control you, rather than the other way round, these blinks are for you. You'll learn about the many things that nourish your rage and ways to avoid them. You'll get to understand how, ultimately, it's your thoughts about a situation that determine whether or not it annoys you in the first place.

And you'll also discover

  * how to talk to your sulky and provocative teen without lashing out;

  * why balancing on one leg can be an anger management skill; and

  * how to foretell that you're about to have a temper tantrum.

### 2. Anger can be a curse or a blessing depending on how you use it. 

Everybody knows how much damage anger can do. After all, it's a powerful emotion, and if you don't know how to control it, your anger can end up hurting you and those around you.

When under the influence of an angry outburst, people can do things they wouldn't normally do; they might drive like a maniac, hit a loved one or destroy the property of another person. But beyond that, people who are easily upset and prone to violent outbursts are difficult to relax around since they might pounce at any moment.

Nobody wants to be that person, but too many people end up yelling at their partners or some other innocent person because they've suppressed all the rage they feel against, say, their bully of a boss.

And it's not just other people who suffer from your passionate fits. Getting too wound up too often can actually hurt your health. For instance, a perpetual state of anger can produce ulcers and lead to high blood pressure.

So, rage can be a destructive force, but if you know how to harness it, it can also fuel many constructive pursuits. For example, you might be scared to leave a toxic relationship because you're unsure about your ability to survive on your own. But one day, you grow so sick of your situation that your anger becomes stronger than your fear and all of a sudden you're walking out the door.

It's in moments like these that fury is fueling your action, but in a way that's helpful — not hurtful.

Righteous rage has even led people to productively transform their communities. Just take Nelson Mandela, who, enraged by the racial injustice he witnessed in his lifetime, harnessed his emotions to start a powerful movement against the racist, apartheid regime of South Africa.

You can be like Mandela, too. Use your anger to make your world a better place in which to live.

### 3. To control your anger, catch its early signals and strategically calm yourself down. 

Do you ever feel your anger suddenly overcoming you like a rogue wave? Well, you might need to pay closer attention to your body. That's because every sudden wave of anger is preceded by subtle physical cues; controlling your anger begins when you learn to identify those cues.

Just moments before you're overcome by a fit of rage, your body sends you signals that let you know it's coming: you start breathing faster, blushing, sweating and tensing up your jaw and hands.

This means that if you pay careful attention to your body and learn to notice these sensations, you'll have a real chance to calm yourself down before it's too late.

But what should you do once you identify an impending explosion of anger?

First, remind yourself that your peak rage will only last five or ten minutes. This is such a short period that you can easily wait it out. To do so, take a few deep breaths and remember that this feeling will pass. You should start to calm down.

Then, give your brain something else to think about. For instance, you might switch your attention to a vacation you're planning, do something silly like counting the moles on the face of the person in front of you, or try to balance on one leg.

Once you've changed your breathing and switched your attention away, come back to the situation and try to see what's good about it. For example, if you're stuck in traffic and getting annoyed, you could think about it as an opportunity to relax and listen to your favorite album.

Finally, it's easy to react to your mistakes with rage, but you can just as easily see every mistake you make as a lesson in self-acceptance. Just remember, no matter how you're treated by other people — or the world — only _you_ are responsible for your feelings.

> _"In all these cases, the angry people attribute their reactions to what happened to them. They take no responsibility."_

### 4. Brooding feeds your anger, but you can learn to control it. 

Annoying things happen every day — people are rude to you, your car breaks down, maybe your beloved cat throws up all over your favorite carpet. These are all perfect opportunities to get into the unhealthy habit of brooding.

Brooding breeds anger, and an irritating mishap can easily trigger an unstoppable wave of rage if you think about it too much. For instance, say your cat did vomit on your rug.

You might think to yourself, "Why didn't it get sick on the tiles instead of the expensive carpet?" You might even start to wonder if it was deliberate.

From there, you'll easily start thinking about the fallout of the incident, about how long it took to clean up, how it made you late for work and filled your living room with a foul stench. At a certain point, there'll be no turning back and you'll be coasting toward an emotional explosion.

This happens to everyone, and thankfully there's a way to control your brooding. Just confine it to a specific period.

Try brooding for 15 minutes at 6 p.m. every day and write down exactly what you intend to brood over each time. Then, spend this time brooding regardless of whether you feel like it or not.

If obsessive thoughts pop into your head outside of this time, just make yourself postpone them. You'll find your inclination to brood becoming weaker and weaker.

However, setting aside negative emotions like this is easier said than done. So, to help distract yourself from thoughts that feed your anger, use pleasurable activities that engage your mind and draw your attention away from brooding. This might involve taking a relaxing stroll through a museum or along a beach.

The following visualization can also help: picture yourself shooting your negative thoughts into the sky or launching them into a cloud. Then envision them floating farther and farther away.

> _"Beauty and anger just don't mix very well. If your attention focuses on something pleasing and delightful, anger melts away."_

### 5. Squeeze anger out of your life by handling provocations and dealing with the pain of the past. 

Unless you're a hermit, there's always going to be somebody who'll try to pick a fight with you. But you have a choice — you don't have to lash out in return. It's completely up to you to deal with the provocations of others without losing your cool, getting into a shouting match, or worse.

That's because it takes two parties to fight and even if someone provokes you incessantly, hostilities can only break out if you play along. Say your teenager is trying to draw you into an argument to let off some of her angst: She comes home from school upset, responds to your loving welcome with a sneer and slams the door to her room, telling you to leave her alone.

In situations like these, it's essential to remember that it's your daughter's stroppiness that's speaking, not your own. This simple reminder will help you keep your calm.

Next, you can ask yourself whether or not you _want_ to argue with your daughter. If you don't, simply respond peacefully by saying, "It seems like you're upset." By acknowledging her feelings in this way, you'll show her that you understand but are not willing to engage with her petulance.

Simply put, just because someone is being angry toward you doesn't mean you need to respond in kind.

However, in other cases, wounds from the past can exacerbate your feelings in the present. To deal with this type of anger it's essential to address the painful past events and make peace with them.

Say your father used to ridicule you publicly and, as a result, you now find yourself furious whenever a friend playfully teases you. The only way to deal with this and act respectfully toward your friend is to address the hurt you still feel from your father's actions.

To start, admit that the events of the past still affect you and find someone with whom you can talk about them. Then ask yourself how you can reconcile these memories.

### 6. While stress can be bad for your health, you can keep it in check by changing your thought processes. 

What do you frequently say during an angry outburst? Something about telling people to back off, or a warning that you're running out of patience? In that case, it's likely that your anger is caused by stress, a serious situation that needs to be addressed.

But first, a bit more about stress.

There are two types of stressors and both impact your health. First, major stressors. Some things can flip your life completely upside down. These big-time upsets are things like losing your job, getting pregnant or moving home. But there are also loads of minor stressors that are experienced on a daily basis. Think of deadlines, cold sores or a disruptive work environment.

So, there are two categories, but surprisingly enough, the minor ones are just as bad for you as the major ones. That's because they happen every day and therefore make up a sizeable chunk of your overall stress load.

This is important because stress, especially chronic stress, is known to produce all manner of undesirable health outcomes. It can raise your blood pressure, cause depression and exacerbate pretty much every long-term disease.

That makes keeping your stress under control essential. Start by defining some of your stressors as challenges, rather than catastrophes. Some major stressors, like getting pregnant or moving, aren't necessarily bad, they're just scary because they mean huge life changes.

As a result, you can make them less stressful by simply changing your outlook. For example, instead of seeing the need to move home as a horrible unforeseen event, take it as an opportunity to do the best job you can at a tricky task.

Simply determine any potential problems — like repainting your old apartment at short notice — and start brainstorming solutions. Just remember, if you don't learn how to handle your daily stress, it's going to catch up with you one way or another.

> "_Stress has been linked to depression and cardiovascular disease."_

### 7. Sleep, physical exercise and gratitude improve your wellbeing and deter angry feelings. 

Everyone knows how cranky kids get when they don't have enough sleep, and, if you're lucky, they'll be merely grumpy. In the worst case scenario, they'll throw a violent temper tantrum. So, how does this relate to anger management as an adult?

Well, as a grown-up, you likely won't throw yourself on the floor screaming, but sleep deprivation will still increase your irritability. Getting too little rest might set you up for an irate outburst. People generally feel a lot better if their brains are well-rested. After all, a good night's sleep lets you see the world with clarity and optimism.

Here's how to help yourself get the high-quality slumber you need.

First, try to avoid all caffeine and nicotine after dinner. These stimulants prevent you from sleeping well because they activate your nervous system. Second, avoid eating too late in the evening, because a full stomach can disrupt your sleep as well. Then, to keep your mind from spinning around thinking about work, or other stressful scenarios, put your smartphone away.

On top of this sleep routine, getting regular physical exercise is a great way to make yourself feel happier and more relaxed. What's more, it's pretty difficult to be angry after a hard workout.

Once you've made these changes to improve your sleep and deter your anger, adding a _gratitude habit_ can help you be happier and even less prone to rage.

When people are angry, it's often because they're not getting what they want, whether from a partnership, a job, or life in general. One strategy to change such situations is to focus on the things you _do_ have and be grateful for them. Hence, a gratitude habit.

To incorporate this technique you should begin every day by making a mental list of everything you're grateful for, like the roof above your head, the delicious coffee you're about to enjoy or the beautiful sun in the sky.

By focusing on the things you appreciate you'll feel happier about your life overall.

> _"People who appreciate the blessings of life find life to be more satisfying."_

### 8. Final summary 

The key message in this book:

**If you don't control your anger, it'll end up controlling you. Luckily, there are some simple techniques to get a handle on irascibility. Often, simply noticing your thoughts and tweaking daily habits can go a long way in helping you deal with feelings of rage.**

Actionable advice:

**Drive your car and your life more slowly.**

Whether you're literally in the car or in the figurative driver's seat of life, take your time and try not to rush everywhere. Just relax, get comfortable and enjoy the ride with a relaxed attitude. You'll find it's a lot harder to be angry when you're so preoccupied with enjoying yourself.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Improving Your Relationship For Dummies_** **by Paula Hall**

_Improving Your Relationship For Dummies_ (2010) provides simple techniques for couples hoping to boost their intimacy, sharpen their communication and overcome challenges in a productive, healthy way. It offers advice for both new couples that want to start off on the right foot and long-term couples that hope to get things back on track.
---

### Charles H. Elliott, PhD, Laura L. Smith, PhD

Charles H. Elliott, PhD, is a clinical psychologist, a founding fellow of the Academy of Cognitive Therapy and a professor emeritus at Fielding Graduate University. He specialises in therapy for emotional disorders.

Laura L. Smith, PhD, is a school and clinical psychologist, as well as the president of the New Mexico Psychological Association. She has given workshops on cognitive therapy and mental health issues to national and international audiences. Together, she and Charles Elliott have co-authored several books for the _Dummies_ series.

©Charles H. Elliott, PhD, Laura L. Smith, PhD: Anger Management for Dummies copyright 2015, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

