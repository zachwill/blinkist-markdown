---
id: 57a5a69ebed677000322c48d
slug: animal-madness-en
published_date: 2016-08-09T00:00:00.000+00:00
author: Laurel Braitman
title: Animal Madness
subtitle: How Anxious Dogs, Compulsive Parrots, and Elephants in Recovery Help Us Understand Ourselves
main_color: F7E17E
text_color: 5E5630
---

# Animal Madness

_How Anxious Dogs, Compulsive Parrots, and Elephants in Recovery Help Us Understand Ourselves_

**Laurel Braitman**

_Animal Madness_ (2014) is all about the emotional disorders from which animals suffer and the way these problems resemble psychiatric illnesses in humans. These blinks demonstrate how similar we are to our furry friends and how we can improve the mental well-being of all animals.

---
### 1. What’s in it for me? Get a completely new take on animals. 

For a long time, we humans have thought of mental illness as something uniquely human, dependent on psychological traits that animals don't possess. You can't become delusional if you don't have the gift of consciousness to begin with. You can't suffer from melancholia if you don't have something akin to a soul. And to become suicidal — to consciously want to kill yourself — you need to have some concept of life and death.

In the past, many people — including notable scholars — were convinced that animals were just too dumb to ever develop any disorders more complex than some annoying behavioral issues. But over the last few decades, we've learned about the amazing intelligence of many animal species. And now it's time to admit that animals can suffer mentally too. These blinks will introduce you to the puzzling phenomenon of animal madness.

You'll find out

  * about a mortally homesick gorilla;

  * why a loving kangaroo mother hit her babies; and

  * why in the olden days people would hang homicidal elephants.

### 2. Despite what some people think, animals have minds and feelings. 

While a little boy might feel bad when he forgets his cat's birthday, the cat probably won't care. But is it safe to assume that in general, animals don't feel the way humans do?

Some people claim this is definitely the case, because they hold an _anthropocentrist_ view of the world. That means they believe humans are the only beings with a mind, while animals are driven solely by instincts.

The philosopher René Descartes believed that humans were unique in their ability to reason. As a result, he saw animals as mere biological machines that were incapable of feeling or self-awareness.

But this isn't the only school of thought; many people describe the behaviors and experiences of animals as being like those of humans. This view is called _anthropomorphism_.

For instance, say you just walked in on your spaniel chewing your favorite hat. The dog becomes uncharacteristically meek and won't make eye contact with you. Is it because it feels guilty? It's not outside the realm of possibility, and there's nothing wrong with believing this is the case.

After all, there's good reason to think that animals are similar to ourselves, that they have feelings and minds, just like humans. We're closely related to animals, as we know from Charles Darwin, who, over a century ago, pointed out that humans are just another animal species.

What's more, many of our brain structures are the same as those of animals, especially other mammals. As a result, it's likely that our brains produce comparable experiences and abilities. For example, an MRI study found that dogs who were reunited with their human guardians showed elevated brain activity in the same regions that process joy in the human brain.

And animals can be remarkably intelligent too. In fact, experiments have shown that several animal species use tools. Apes use sticks to dig for food and even appear to recognize themselves in mirrors, indicating they have some concept of self.

### 3. Animals have been described as mad, homesick and even suicidal. 

Have you ever heard of the Overtoun Bridge in Scotland? Well, this bridge has a peculiar claim to fame: many dogs have apparently thrown themselves off it. Stories like this tend to spread very quickly because people find the notion of disturbed and world-weary animals particularly mesmerizing.

It used to be that behavioral problems in animals were described as _madness_. The word referred to mental instability in a variety of species, but elephants in particular. Nowadays we know that the aggressive nature of elephants is, in large part, a result of their mistreatment or else sparked by _musth_, the period of surging male hormones which lasts several weeks.

However, in the past, aggressive elephants who killed their closest human caretakers were sometimes held accountable for their violent acts, because people saw them as capable of reason. That means they were punished for their murders, electrocuted and even hanged.

But not all animal emotional problems are so brutal. Another common one is homesickness. Just take John Daniel, the gorilla. He was captured in the wild around 1915 and raised by a loving human family in London where he thrived. But he soon got too big and strong to be safely left alone.

So, his family sold him to a circus representative posing as an employee of a private park, who promised that John would be comfortable and cared for. But alone in the circus, John died within weeks. The cause was thought to be "homesickness."

And finally, animals throughout history have been reported to exhibit suicidal behavior. Many animals show signs of self-harming behavior and some even die of it.

For example, dogs refuse to eat, scorpions sting themselves with their poisonous tails and, for millennia, whales have been seen beaching themselves in masses, just to die on the shore.

Not just that, but in 1901, a circus lion named Rex was found hanging from his cage, choked to death by his neck chain. These cases seem like suicide, but of course whether these animals really intended to kill themselves is simply impossible for us to know.

> _"Every animal with a mind has the capacity to lose hold of it from time to time."_

### 4. Diagnosing mental illness in animals requires close examination of their habits and life history. 

So, animals can suffer from mental illness, but a vet can't play the part of a therapist, asking an elephant about its problems. This means that diagnosing mental illness in an animal requires close observation.

Certain problems are easy to diagnose, like tic disorders. The author met Sunita, a highly strung tigress, in a free-range refuge in San Andreas, California. Sunita was suffering from facial tics that caused her to blink her eyes and twitch her muzzle when stressed out.

But other times you need to look closer to tell if an animal's behavior is actually pathological or has other drivers. For example, one of the tree kangaroos in the San Antonio Zoo seemed to be beating its babies whenever someone approached. However, upon closer inspection it was clear that she was just scrambling to pick up her children and protect them, but her paws were too short to be effective.

That's because tree kangaroos, as their name suggests, usually raise their young in trees where they don't need to pick them up off the ground. So, she was behaving rationally, just in an environment to which she wasn't adapted.

Another thing to consider when diagnosing an animal's problems is its breed. That's because some breeds are predisposed to certain issues. German Shepherds tend to snap at non-existent flies and Bull Terriers often chase their tails.

And finally, looking to the past can help you understand an animal's current behavior. That's because mistreatment can produce a wide range of behavioral issues and anxieties.

The tigress Sunita was always on edge because of a past trauma. She'd been imprisoned by an animal hoarder in an overcrowded room where she was constantly abused by the stronger tigers. This type of persistent stress can lead to permanently elevated stress hormones that damage the brain and cause chronic anxiety.

Uncovering the way in which an animal's past is affecting its present is essential because it makes treatment much easier to provide.

> _"91.5 percent of all farmed pigs have mental disorders, but also more than 50 percent of all lab rats and most zoo animals."_

### 5. Many vets turn to medications to treat animals, but drugs don’t work as a standalone solution. 

Have you ever been at the zoo and joked that an animal staring off into space looks as though it's drugged up? Well, this description might be spot on. That's because it's exceedingly common to give animals drugs like antidepressants and tranquilizers.

In fact, while it's difficult to get hard figures on this because lots of the professionals in this industry are made to sign nondisclosure agreements, evidence points to broad use in both farm and zoo animals. Lots of zoo animals, including bears and dolphins, are given antidepressants and so are household pets.

After all, many of these medications have been cleared for human use through animal trials, so why not use them to treat the same mental problems in animals?

Well, drugs tend to only cover one aspect of the equation. They only dissipate symptoms and their effects stop as soon as administration of the drug ends. So, to sustainably address an animal's issues, it's better to combine drugs with _behavior-modification training_, a method of helping an animal unlearn problematic behavior or learn to behave in a desired way.

If your dog barks like crazy every time a cell phone rings, you might expose it to these sounds over and over, beginning with a very faint noise and slowly increasing the volume. This process will let your dog get used to the ringtone and learn that it is harmless.

But it's also important to understand that while many pet problems might be annoying, they're not disorders. If your dog always wants to be with you and gets into trouble when left alone, it doesn't have a disorder; it's just acting like a normal dog. In cases like this, there's no point in using drugs.

So, drugs can be helpful, but they're not a silver bullet to solve all animal problems. To truly help an animal you need to understand what they require to stay healthy in the first place, which is exactly what you'll learn in the next blink.

> _"Many dog owners are content to meet a dog on human terms but are unwilling to do so on the dog's terms."_

### 6. Animals need companions and activities to keep them healthy. 

Humans need friends and companionship to thrive, and the same is true for animals.

While the majority of animals find this companionship by living with others of their own species, interspecies pairings work as well. For instance, race horses who get anxious are kept with other animals, often goats, to calm their nerves. In many cases, both animals get so attached to one another that they suffer when separated.

But many animals also thrive when cared for by a human companion. The only issue is that they can get pretty possessive. Elephants have been said to get jealous and aggressive if their keeper so much as brings along his girlfriend for a visit. To avoid aggression, it's essential that the animal trusts that its companion won't abandon it.

But for an animal to stay healthy, it also needs to stay occupied. Just like humans, animals need to exert their energy and intelligence to thrive. But they also need to experience joy, like that derived from play.

These needs are even acknowledged by legislators who signed the _Animal Welfare Act_, a US federal law that requires laboratories to give their test animals some kind of diversion. That means the cages of monkeys used for tests should have mirrors, swings and other equipment that make them more habitable for the animals.

In conclusion, it turns out that animals aren't so different from humans when it comes to their mental health. Therefore when we talk about human rights, we must consider animal rights too.

### 7. Final summary 

The key message in this book:

**Animals and humans are much more similar than many people think, and that includes their emotional life. Animals can feel anxiety, stress and even homesickness or ennui. So, to keep animals in good mental health, we need to ensure they have many of the same things that we do, including company and diversions.**

Actionable advice:

**Don't administer drugs unnecessarily.**

Most psychotropic drugs have side effects, are expensive and often are totally unnecessary. So, if your pet seems afraid of something, don't just stuff pills down its throat. Oftentimes a little cuddling and soothing will go a long way to comfort them. Just remember, it's normal for a pet to be frightened sometimes. Of course, if you exhaust all the other options and your pet is still suffering, there's nothing wrong with going to the vet and getting medicine, but it's worth trying to solve your animal's problem without chemicals first.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Genius of Dogs_** **by Brian Hare and Vanessa Woods**

_The Genius of Dogs_ (2013) uncovers the remarkable intelligence of man's best four-legged friend. By first examining human intelligence, the authors go on to explain exactly what makes dogs so smart, which talents they have in common with humans and other animals, and what sets them apart.
---

### Laurel Braitman

Laurel Braitman is a scientific historian and the writer in residence at the Stanford School of Biomedical Ethics. She is a contributor to publications such as the _Guardian_ and _New Inquiry_, a recipient of a TED fellowship and an affiliate artist at Headlands Center for the Arts. _Animal Madness_ is her first bestseller.

