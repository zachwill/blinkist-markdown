---
id: 56654b8ec58abd0007000015
slug: animal-vegetable-miracle-en
published_date: 2015-12-07T00:00:00.000+00:00
author: Barbara Kingsolver, with Steven L. Hopp, and Camille Kingsolver
title: Animal, Vegetable, Miracle
subtitle: Our Year of Seasonal Eating
main_color: 66853F
text_color: 526B33
---

# Animal, Vegetable, Miracle

_Our Year of Seasonal Eating_

**Barbara Kingsolver, with Steven L. Hopp, and Camille Kingsolver**

_Animal, Vegetable, Miracle_ (2007) offers insights gained during the authors' year-long sojourn in the countryside. They lived only on seasonal and local food, and their experiment reveals the right time to eat each vegetable and the importance of investing in the local food made by local farmers.

---
### 1. What’s in it for me? Learn to grow your own food. 

Maybe you live in the city. Maybe you live surrounded by vast fields of green. Either way, you have the opportunity to sow your own seeds, to cultivate and grow them and, finally, to eat them. And it doesn't stop there. You also have the opportunity to stand against the horrific methods of big food corporations. 

Pretty cool to be able to serve home-grown veggies when you invite your friends over for dinner, right? And let's see how happy they'll be when you assure them that everything is pesticide-free. There probably won't be much left over.

In _Animal, Vegetable, Miracle_, the authors pull you into the universe of growing your own food. You'll learn when to sow what and when to expect what to grow. Starting by revealing some truths about today's food industry, they give you an immediate incentive to grow your own veggies and to go hunting for that asparagus yourself — that is, outside the supermarket. 

In these blinks, you'll discover

  * why people still buy the products of big food corporations;

  * what to look for when you go hunting for asparagus; and

  * which plantings to expect during the months of spring.

### 2. The food industry has made people forget all about real food. 

Living in the city for so long has removed us from the process of food creation. Foods now have bizarre, made-up names; foodstuff is imported and exported all over the world. For the most part, we've entirely forgotten about local farmers.

It's true that most people today want products from local, organic farmers. Nonetheless, they tend to purchase those products from giant food corporations.

These corporations use synthetic fertilizers and pesticides, and genetically modify their products in order to produce them cheaply — a far cry from the local, organic food people crave. 

Most people are fully aware that most animals destined for consumption endure horrific conditions before being slaughtered. But as long as prices stay low, people will buy from giant corporations instead of their local farmers' market.

People even complain about the high prices of organic meats and vegetables. But prices are high for a reason: farmers personally tended to those vegetables and took great care of those animals. 

Moreover, the calories we consume today come in forms that are hardly recognizable when compared to real food.

After World War II the US government relied on chemical fertilizers to guarantee a cheap, steady supply of corn and soybeans in order to produce high-fructose corn syrup and hydrogenated oils, and to feed cattle and poultry. 

But that production never slowed down. Today, American farmers still produce 3,900 calories per citizen per day. People are consuming way more calories than they require, often without knowing it.

Finally, genetically modified (GM) plants are almost ubiquitous in the US food supply chain, and are often difficult to avoid.

Genetically modified food does not have to be labelled as such, meaning that, even if you don't want to eat GM foods, the food industry has nonetheless figured out how to get them into your body.

In fact, the people running the industrial farms have strategy meetings to discuss new ways of getting you to consume all these surplus calories, resulting in widespread obesity.

> _"Conventional methods are definitely producing huge quantities of corn, wheat, and soybeans, but not to feed the poor [...]."_

### 3. The springtime is your opportunity to bring real food from your own garden to your plate. 

If you want to eat real food, then you'll have to grow it yourself. But growing your own food isn't the easiest thing in the world, and it will definitely be tempting to buy those delicious-looking greens from your local supermarket in the wintertime. However, if you plan ahead, you can eat vegetables from your own local garden year round.

Spring is the best time to start this garden. Come March, start planting. You can even do this indoors. The author and her daughter, for instance, started by simply planting their veggies and flower seeds under fluorescent lights. When March rolls around again, you might still be eating your winter supply from the last harvest!

The highlight of this month is asparagus, which you can find growing wild. This can be difficult, though. The asparagus spears that you may have seen only look like that for a single day if left unpicked; they grow so rapidly you can almost see it happening in real time. If you pick it, it will simply keep growing.

Later, during the month of April, the climate gets warmer, and everything starts to blossom. So prepare for action in your garden!

The earliest blooms you'll encounter in April are the vegetables that do well in cool weather and don't mind a bit of snow. For example, if you planted potatoes in February, when it was still pretty cold outside, your potatoes are likely to be ready for harvest in April. Other plants in this category are onions, peas and the Cole crops, like broccoli, kale, cabbage and so on.

Spring is also the season for spinach, kale, endive and baby lettuce. Just imagine a plate of greens still warm from the sun, with a handful of walnuts and some goat cheese, and you'll start to truly appreciate the beauty of real food.

### 4. Working hard in the garden means eating great in the summer. 

The summertime can transform each and every one of us into a passionate gardener and cook. All you have to do is reconnect with your gardening roots. 

Though most of us aren't farmers or gardeners, we may still feel some dim nostalgia for a simpler time when people lived off the land. Go ahead and embrace that feeling. Don't let the fact that you live in the city deter you from gardening!

A small balcony is all that's needed to start your own little garden. In fact, container gardening on your balcony can afford you enough space to grow tomato plants, basil or any of your favorite veggies.

If you've made the effort to start a small garden in the spring, then you can reap plenty of rewards in the summer. July is the month of squash (zucchini, crookneck, heirloom), eggplants and cucumbers; August is the "red month," full of tomatoes. Perfect for any of your favorite dishes!

Another benefit of growing your own greens is that you can cook the "fruits" (and veggies!) of your labor. If you cook for yourself, you're more likely to keep yourself and your family healthy, and even save some money in the process.

By cooking your own food, you can more easily control both the quality and the quantity of your ingredients. Cooking can also be quite relaxing, and even therapeutic. Cooking food that you've grown yourself helps you connect with and get to know your ingredients, not to mention learn about the benefits they hold for your body.

Just remember: working hard in the garden and the kitchen brings rewards that make it all worthwhile.

### 5. Autumn brings blood, pumpkin and potatoes into our gardens. 

When the weather changes, our eating habits change with it. We start consuming foods that will give us the warmth and strength to survive the cold of winter. Often, that strength comes from animals.

Although not everyone can raise animals, those who do must know how to do it right. Animal harvesting is a controversial practice, considered by some to be a cruel ritual. However, unlike the mere killing of an animal, harvesting implies planning and respect — an effort to make the animal's life as good as possible before it's slaughtered for consumption. 

Animals in industrial farms, however, are inhumanly killed, not harvested. That is, not a second thought is given to the terror and pain these animals suffer prior to their death. Furthermore, the conditions in which these animals live are beyond deplorable: the animals get little or no exercise, waiting in darkness and discomfort for their final day.

But the harvesting of animals can be done in ways that don't cause fear or pain.

The author recounts a story of how she and her family decided one September day that it was time to harvest some of their roosters. To spare it a painful death, they would grab the rooster by the legs and turn it upside down, causing it to fall asleep. They then put its neck gently onto a block, quickly and painlessly decapitated it and then drained it of blood.

After the animal harvest in September, October brings delicious potatoes and pumpkins.

Potatoes are a great source of carbohydrates, which fill you with energy. They are best planted as soon as the soil can be worked, so you should probably start stirring up in March, around Saint Patrick's Day.

Pumpkins are another marvelous treat for the autumn table. They are the largest vegetable we consume, and can be used in many recipes.

> _"To believe we can live without taking life is delusional. Humans may only cultivate nonviolence in our diets by degree."_

### 6. Open up your freezer in winter, explore its contents and wait for the warm days to return. 

There's not much gardening to be done in the winter. Rather, these icy months invite you to stay indoors and explore what you've stockpiled in your freezer. But are you sufficiently prepared for winter?

There's one thing you should definitely be sure of: that your freezer is stocked with meat and fats, like fish and nuts. Without these nutrients, both your body and mind might suffer.

We need meat and fatty food to keep our bodies warm and energetic, especially during the wintertime. Chicken, beef, turkey or even fish serve your body's metabolic cravings for fatty food and help it maintain its energy stores.

People who live in places with little daylight and colder temperatures are even advised to eat more seafood, which helps the body fight depression. Indeed, neurological studies have revealed that those omega-3 fatty acids in ocean fish counteract the blues. You can also find omega-3 in pasture-finished beef.

Because you won't have any crops from the garden that need to be eaten right away, winter is the perfect time to get creative in the kitchen.

Open your freezer and use whatever vegetables you have. It could be grated zucchini, broccoli and greens for salads or sliced apples for pies, and you'll probably have tons of squash for soups, vegetable pies and stews. You can even freeze pesto for a quick and delicious pasta dish in winter.

You can make pesto out of pretty much any veggie — tomatoes, basil or any other greens. And it's easily stored in small jars or plastic bags. When you want to eat some pasta, all you have to do is grab a jar from the freezer and set it in some hot water. Once unfrozen, mix the pesto with the pasta, add a few olives, dried tomatoes and a bit of parmesan and — _presto_! — you've got a delicious meal.

### 7. Final summary 

The key message in this book:

**Even if you live in the city, you can still experience the joy of gardening. Whether you do it on your balcony or in a community space, gardening will familiarize you with your food, save you money and improve your diet.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Barbara Kingsolver, with Steven L. Hopp, and Camille Kingsolver

Barbara Kingsolver is a novelist, essayist and poet. Her other works include _The Bean Trees, The Poisonwood Bible_ and _The Lacuna_, which won the 2010 Orange Prize for Fiction.

Steven L. Hopp is a teacher at Emory and Henry College, specializing in Environmental Studies.

Camille Kingsolver, a graduate of Duke University, is an advocate for local food. She currently works in the mental-health field.

