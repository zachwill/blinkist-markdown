---
id: 556c718c3236630007ad0000
slug: anticipate-en
published_date: 2015-06-04T00:00:00.000+00:00
author: Rob-Jan de Jong
title: Anticipate
subtitle: The Art of Leading By Looking Ahead
main_color: DF2D34
text_color: AB2228
---

# Anticipate

_The Art of Leading By Looking Ahead_

**Rob-Jan de Jong**

_Anticipate_ (2015) reveals what it takes to become a visionary leader. From Aristotle's three pillars of leadership to practices and mindsets that strengthen your leadership abilities, these blinks show that having a vision isn't something you're born with — it's something you work at.

---
### 1. What’s in it for me? Discover the skills needed to become a visionary leader. 

When it comes to election time, how do the best politicians convince us to vote for them? They display a clear vision of how they will lead us into a better future, and they use speeches and manifestos to persuade us.

Usually it works. After all, who would you want to follow: someone with a clear idea of the future, or someone stuck in the past?

Not surprisingly, vision can also be used in the business world. Leaders who anticipate future business trends, position their companies accordingly and then persuade others to follow suit have a great chance of succeeding. But how do you develop this vision? These blinks will show you how.

In these blinks, you'll learn

  * why, despite its success, many Amazon shareholders are unhappy with their lot; and

  * why you can develop your vision by writing your own obituary.

### 2. Great vision consists of three elements, two of which are Logos and Pathos. 

We all know that a good leader is someone with _vision_ — it's something you hear all the time in business today. But the concept of vision in this context actually dates back to the ancient Greek philosopher Aristotle.

In his book _On Rhetoric,_ Aristotle identified three principles that are vital for a leader to have vision: _logos, pathos_ and _ethos_. For a better understanding of how these principles come together, it's important to take a closer look at the first two, starting with logos.

You might have noticed that logos sounds a little like "logic," and it's for a reason. Logos refers to the ability to examine a situation and come up with a logical strategy to achieve positive outcomes in the future — something every good leader requires!

We find a great display of logos in the vision of Sheikh Mohammed bin Rashid al-Maktoum, who was responsible for turning Dubai into a high-end tourism and finance hub. He arrived at his vision by analyzing Dubai's situation. He recognized that its revenue relied on oil exports, a finite resource that clearly won't last forever, and developed a strategy with the goal of long-term prosperity.

Logos will help you construct your vision, but it alone won't help you achieve it. Every leader needs his supporters, and that's why logos needs _pathos_ to work alongside it.

Pathos describes the ability of a leader to win people over by appealing to their emotions.

Take IKEA's founder, Ingvar Kamprad. He portrayed his vision as a fight against _unfairness_, as only a wealthy few could afford stylish, functional and well-designed furniture. Through the slogan "You do a little, we do a little and together we save a lot," IKEA also promotes togetherness. In this way, IKEA turned their vision into a noble cause, garnering the support that makes them a household name today!

### 3. The third key aspect of visionary leadership is ethos. 

We now know that great vision necessitates both a logical and an emotional side. But there is one more principle that Aristotle describes: _Ethos_, the personal credibility of the leader. So what makes ethos vital in leadership?

People often don't like the idea of change, which can make a leader's task of gaining their trust quite tough. In fact, unless they display personal integrity, they'll have a hard time gaining any trust at all.

This is something that Amazon.com founder Jeff Bezos recognizes. Amazon is the world's biggest online retailer with the vision of delivering excellent customer service as well as innovative products. Despite the site's success, Bezos is constantly under pressure from shareholders to focus on making additional profits.

Nevertheless, Bezos regularly reminds these shareholders that Amazon's mission is to improve customer experience through innovation, and that the company must continue investing toward this goal. And since this has always been his vision, and has always been successful, Bezos's shareholders are prepared to trust him and continue to follow him, no matter what. This is a prime example of the importance of integrity in leadership.

### 4. Stay ahead of trends to develop winning strategies. 

Rationality, emotions and credibility are what make a great vision. But a great vision is of no use if leaders don't have the skills to act upon it quickly.

Imagine your vision existing along a timeline between two points. The first is the _point of surprise,_ which is when you discover a new and original idea. The second is the _point of no return_, when everyone has figured out that same idea and you lose your competitive advantage. 

So, if you want to maximize success, you'll need to make the gap between those points as big as possible by surprising your market sooner. This is exactly how the CEO of CISCO, John Chambers, has kept his company at the forefront of innovation. By constantly searching for trends and disruptions in his industry, he keeps CISCO well ahead of its competitors, often by six to nine years!

Once you've noticed a gap in the market that your fresh idea can fill, you'll need to create a concrete and _measurable_ vision for achieving it. So how is this done?

Let's say you predict that increasing urbanization will affect the way we live in the future.

That's all well and good, but how can you know whether you're right or wrong? The key to making this projection measurable is to make it more concrete, perhaps by saying that our urbanized future will mean that 20 years from now, only 50% of children will have a chance to spend time outside of a city.

This might not end up being true, but the point is that you will be able to determine whether you are right or wrong, gauge your strategies in response to this outcome and tweak them until you get it right.

### 5. Keep your strategies flexible and expect the unexpected. 

Making your vision concrete will keep you on track as you work toward it. But this certainly doesn't mean that you should stick to it uncompromisingly.

Consider the story of Alan Greenspan, one of the most vocal advocates for financial deregulation and free markets. As the chairman of the US Federal Reserve from 1987 to 2006, his desire to create a booming free-market economy led to a drop in interest rates in 2004.

Despite warnings that this could create a wave of unstable borrowing, Greenspan stuck rigidly to his original plan. The result? His policies were a major factor in one of history's largest financial crises. The clear lesson here is that an uncompromising strategy isn't always a smart one.

Humans aren't perfect, nor are our plans — we'll always make mistakes that we didn't see coming.

In order to realize our vision effectively, we need to remain flexible with our strategy and ensure that we listen to others. Leaders must be both bonded to their vision and open-minded along their journey toward achieving it.

One of the best ways to strike a balance between determination and flexibility is by building a strategy that considers unforeseen events and multiple possibilities.

Take a look at Shell during the 1970s and '80s, for example. A strategic planning analyst, Pierre Wack, prepared the company's executives for all manner of possible scenarios, so that they'd be able to limit the damage in even the worst of crises.

As it happened, one such crisis did strike: The oil embargo put in place by OPEC countries in the 1970s led to severe price shocks that in turn caused economic upheaval around the world. Because its executives had prepared for this crisis in advance, Shell suffered far less than its competitors.

> _"Life is what happens to you while you are busy making other plans." - John Lennon_

### 6. A great leader embodies his vision in every action. 

As we learned in the second blink, a visionary leader must be credible, or, in other words, possess ethos. In this blink we'll investigate this principle further, in order to find out what it really means to _walk the talk_ as a leader.

Would you follow a leader who says one thing and does another? Of course not — you wouldn't support someone's strategy if it's clear that they themselves don't believe in it. As a leader, it's vital that you reflect authenticity. You can achieve this by communicating a love for what you do in each of your actions.

Steve Jobs was a master at developing a personal connection with supporters through his authenticity and love of his work. By sharing stories and anecdotes from his own life, from dropping out of college to battling with cancer, he put his vision of innovation into a human context that we could all relate to.

Equally important for a leader's integrity is his _self-awareness._ Only those leaders who display the willingness to learn from their mistakes and failures will be the ones who gain the trust of their followers.

We all have our blind spots. You can, however, boost your own self-awareness by conducting the _obituary exercise_. Ask yourself what you would wish you had accomplished in your lifetime if you were to die today. This is a surefire way to help you reconnect with your core values and life goals. Ultimately, knowing what you stand for is at the heart of visionary leadership.

### 7. Train your leadership abilities by learning to listen and question. 

Despite common perceptions, leadership isn't just something that you're born with. As we've seen, it comprises many different elements, and is based on skills that we can practice. Two of these skills are particularly important in developing leadership abilities.

Learning to listen is the first of these techniques, and it applies a skill that we often undervalue. Think about it: How often do you jump in to express your own opinion when listening to someone else's? This is no way to build supportive relationships!

Switch on your _listening mode_ by consciously preventing yourself from taking over a conversation, and by not dismissing the other person's opinion, no matter what you think of it. Instead, ask questions that will lead your counterpart to freely uncover their reasoning: "Is there anything that surprises you in the progress of this matter?" or "Have your ideas regarding this matter changed in recent years?"

Asking questions in this way contributes to an environment that welcomes dialogue and freedom of opinion. What's more, when worded the right way, questions can also stimulate curiosity and creativity. What, how and why questions encourage higher-level thinking, while closed questions with only yes or no answers have no place in a creative workspace.

So instead of asking "Can we become more productive?", ask "What should we transform to make sure that we become more innovative?" This way, you pave the way for a more considered and informative response.

We should also question our own _assumptions._ In fact, many of our questions will have assumptions of their own built into them. Take our previous example: Asking how you can become more innovative contains the implicit assumption that innovation is a top priority.

If innovation is indeed your top priority, great, but if customer service is a more important issue for you at the time, you'll need to modify your questions accordingly.

### 8. Draw people to your vision with the power of language. 

With a well-rounded vision and solid interpersonal skills, you're well on your way to becoming an effective leader. But there's still one more skill you'll need to practice: communication.

Any leader should be able to communicate the core of his vision in two to three minutes, tops. And that's without the help of a slide-based presentation! Being as succinct as possible will demonstrate your clarity of thought to your audience, and will impress them too.

Another way you can capture your audience's attention is by evoking imagination, creativity and excitement. How? By pointing people's minds towards the future rather than the past. While history can provide us with facts, experiences and reasoning, a truly engaging vision is future-oriented.

For example, German energy policy could concentrate on the past and what has gone wrong over the past decades, focusing on what could or should have been done. But it doesn't. Instead, the focus is on what can still be done in the future. With the goal of increasing carbon-free electricity production to 40 percent of overall consumption by 2025, Germany is today one of the world's leading nations in sustainable energy production.

You'll also have a better chance of gaining your audience's support by using _powerhouse_ verbs. More evocative verbs and language make our listeners more observant and keen. So swap "see" for "discover", "gather" for "mobilize" and "display" for "radiate". These memorable words influence emotions to emphasize the importance of an argument.

For a final example of the power of words in communicating a vision, consider the following experiment: A blind beggar who held a sign that read "I'm blind, please help," received only a few donations. When his sign was changed to say "It's beautiful day and I can't see it," donations increased significantly. Language really does make a difference, so why not give your vision the presentation it deserves?

### 9. Final summary 

The key message in this book:

**To build your vision, you'll need rational thinking, empathy and credibility. Strengthen this vision with measurable and flexible strategies, and make sure you act in a way that embodies your vision. Finally, good communication and listening skills are vital to gaining and maintaining support for your vision.**

**Suggested further reading:** ** _Non-Obvious_** **by Rohit Bhargava**

_Non-Obvious_ is all about detecting important trends, an essential part of running a business. You can't accurately predict the future by looking at obvious, surface-level influences — the important ones are hidden underneath. These blinks offer advice on identifying meaningful changes, while outlining some prominent trends we're likely to see in the future.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Rob-Jan de Jong

Rob-Jan de Jong is a consultant, educator and speaker in leadership development. His clients include TotalOil, ING, Phillips and other organizations and business schools worldwide. _Anticipate_ marks his debut as an author.

