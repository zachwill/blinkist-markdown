---
id: 56c068384f33cc0007000061
slug: anything-you-want-en
published_date: 2016-02-18T00:00:00.000+00:00
author: Derek Sivers
title: Anything You Want
subtitle: 40 Lessons for a New Kind of Entrepreneur
main_color: D3AE6A
text_color: 6E5A37
---

# Anything You Want

_40 Lessons for a New Kind of Entrepreneur_

**Derek Sivers**

_Anything You Want_ (2011) is a guide to realizing your dream business, which is easier than you might think. These blinks will teach you why the conventional ideas of amassing tons of money, consultants and technology are all wrong, and that the real key to success is you and your stellar ideas.

---
### 1. What’s in it for me? Make your business anything you want. 

Stay true to yourself, but be flexible! Make a business plan that makes investors believe in you but keep it simple! Delegate, but don't delegate too much!

Maybe you've already struggled with balancing some or all of these seemingly unbalanceable things when starting your own business. Maybe you're feeling pretty lost. And could use some advice?

In _Anything You Want_, that's exactly what author Derek Sivers will give you. Based on his own personal success story and aiming to unravel the tangled threads of starting a business, he helps you figure out the answer to the question: How do you make your business anything you want?

In these blinks, you'll discover

  * why you don't really need that much money to start your own business;

  * when delegation is good — or when it's too much; and

  * why the author refused to work with some musicians.

### 2. Starting a successful business means striving to improve your ideas while keeping it simple. 

When the author set out in the world of business he never intended to found _CD Baby_, an online independent music store. In fact, he'd previously attempted several other businesses only to end up unsuccessful. 

So, what made this one work?

A key step was realizing that the more you improve your ideas, the higher the chances are that you'll come up with one that's truly successful. The point is that if you've been promoting your "perfect" idea for years and it's still going nowhere, it might be time for a change. That means, if you're not getting positive feedback — and a lot of it — it's probably best to let go of your idea. 

Instead, you should try out new options and work to improve your ideas. Because if you're always improving, you're bound to hit on an idea that people will love. For instance, the author was totally surprised when one of his ideas suddenly caught fire among his musician friends. His hard work had paid off, and he created something that would help both himself and others. 

But coming up with the right idea is just the beginning. The next step is to come up with some simple numbers and build the principles of a business plan. This doesn't have to be a complex, 100-page document with financial projections for the next two decades, but a simple document that lays out how your company will work and how much money it'll take to make it happen. 

The key here is to keep it logical and easy to understand. That's because the more precisely you write, the better people will comprehend and believe in your idea. It's really not necessary to throw around complicated figures. You just need to explain how you'll make money and if it will cover your costs. 

For instance, when the author first set up CD Baby, he only used two numbers: how much it would cost musicians to list their CD on the site and how much they would make on each sale. These simple, fixed numbers were enough to calculate his projected success.

> _"If you're not saying 'Hell yeah!' about something, say no."_

### 3. Define your target audiences and tailor your business to their needs. 

It might be hard to settle on a specific group you want to sell products to, but defining a target market is key to meeting your customers' needs. A good strategy is to develop many small customer groups, rather than one big one, making a flexible and secure business. 

For instance, imagine crafting your product or service to meet the needs of a single client with the hopes that she'll invest more in your company. In doing so you, base just about every decision on her thoughts and desires. 

But what if she suddenly changes her mind and drops out?

Your business would be in ruins because you put the needs of one customer above all the others. So, it's better to work out which group of customers is the most important and how you can satisfy the needs of the majority. 

By doing so, you'll be more flexible and independent, and won't suffer when one client decides to move on. As a bonus, you'll also have access to hundreds of opinions to guide your strategy. 

In any case, you can't expect to please everyone. In fact, as your business grows, you'll likely get lots of requests from all different types of people. When this happens, it's key to remember that you don't need to fulfill all of them to be successful. 

Actually, quite the opposite is true. Just stick to your principles and remember the promises you've made to your customers. So if something feels wrong, don't do it. It's absolutely fine to let some people's desires go unfulfilled. 

For instance, the author constantly got requests from record label execs who wanted their newest talent on the site. But he would continually refuse them since the shop was specifically designed for musicians _without_ a label. That's because, when the site was born 15 years ago, these musicians had no other distribution options.

> _"It's a big world. You can loudly leave out 99 percent of it."_

### 4. You don’t need big money to realize a big idea. 

When most people talk about starting a business, their focus is on securing enough money before really investing in the idea. But having zero funding can actually be a huge advantage. 

In fact, there's nothing wrong with financial limitations. You just need to start with the bare minimum. So, if you're hard up for cash but you've got a grand vision for your business's future, don't worry. Money isn't what makes ideas grow. Being useful to people is, and that should be your main focus. 

Just use the resources available to you, whether personal or otherwise, even if they seem insufficient. For instance, say you want to start a tutoring company but don't have the money to make it happen right away. Don't let go of the idea, just start teaching somebody something today. You'll begin identifying potential customers and be spreading your knowledge. 

In fact, being strapped for cash actually boosts creativity. That's because major funders can buy you the best equipment, hire a top-tier staff and rent the fanciest office, but are those the things you need?

Often, lacking the money for material luxuries helps you brainstorm creative solutions to make things easier, faster and cheaper. In the best cases, you'll even expand your own knowledge. 

For instance, plenty of people are dead broke but have a great sense of style — some are even better dressed than rich people. That's because they only buy items that they truly love that go well with what they have, and which provide myriad styling possibilities. 

So, having little or no money can make your job harder and take longer, but the long-term effect will be a business based on personal knowledge and creative problem solving.

> _"By not having any money to waste, you never waste money."_

### 5. Follow your customers’ needs. 

We all know the saying "the customer is always right" and it's clear that satisfying customers is essential to a thriving business. After all, they buy your products!

Knowing this, you should base decisions on your customers' needs. That's because a good business isn't just about you or making money: it's about the people you serve. So listen to what they want and make it happen. 

For instance, your business began with an idea: one that would make certain people's lives easier. And it's key for this vision to remain central in all your business decisions. So you should be basing decisions on customer feedback rather than your wealthiest investor or your bottom line. Because if you consider what your customers would say about the decisions you're faced with, you'll be putting your company on a path to profitability. 

But if the unthinkable happens and your business is struggling to stay afloat, don't let yourself get trapped in survival mode. Most companies endeavor to constantly grow and expand, but sometimes the best decision is to simply give up.

After all, every business idea sets out to satisfy a need or solve a problem, but what if that need disappears or that problem is suddenly fixed?

For instance, if musicians found an easy way to set up their own online store, CD Baby would lose its entire business base. In fact, the author recognized this possibility, but he didn't worry about it. That's because his goal was to solve a problem and, if the solution was already there without him, he could move on to solving a new problem. 

So, while it can sometimes be difficult to keep in touch with your customers, it's absolutely essential to success. Just remember: as long as you have something valuable to offer, people will be happy to hear about it.

### 6. Share responsibility, but not too much. 

What do most self-employed people have in common? They have no clue how to delegate. 

This can be a major pitfall since developing a delegation mindset is an absolute must if you want to protect your precious resources from depletion. After all, if you start a business yourself, doing everything alone, it can be difficult to suddenly start sharing responsibilities. Many entrepreneurs struggle to even find the time to hire and train employees to take over certain tasks. 

But failing to delegate could lead to something much worse: burning out. 

Therefore, it's crucial to find the time to formally train your employees. For instance, you could develop a training manual. It's easy to do. Simply keep a running list of problems you encounter while starting and running your business along with ways to solve them. As time goes on, employees will learn to solve issues on their own and you'll be free to focus on strategic tasks or other interests. 

So, failing to delegate is dangerous but over-delegating can be a disaster. To stay safe, be sure your delegation is based on trust and control.

Here's how:

First of all, make sure to cultivate a trusting relationship with your employees. Let them know you're sharing responsibilities with them because you have faith in them. Second, put in place some simple control mechanisms. You shouldn't surveil your staff, but you should develop some rules and procedures everyone abides by. 

For instance, the author doled out a lot of tasks, giving his employees a great deal of freedom to choose their work style. But then he got a call from his accountant, saying that a profit-sharing plan was being put in place based on a decision made by a staff person. This particular employee had decided that all the company's profits should be divided among the staff without the author even knowing about it, at which point the author realized he had delegated too much.

> _"I had to make myself unnecessary to the running of my company."_

### 7. Creating your dream job is up to you. 

Plenty of people have dreams of being a CEO at some point in their careers while others envision it being a management nightmare.

How do you feel about it?

If you're in the group that shudders at the thought of being in a conventional CEO management position, but still want to start a business, don't do it. There are going to be tasks that you simply don't want to do — and you shouldn't feel the pressure to fill a position that will make you unhappy. 

For instance, say you opened a restaurant because you love to be in the kitchen. But, as the restaurant grew, you found yourself with less and less time to actually cook. Now your job consists of logistics, management and running a company — a reality you never bargained for. 

The same holds true for your employees. That is, they also need to take pleasure in what they do. To make this a reality, you shouldn't ask your employees what they're good at, but what they enjoy. And remember: you don't need to have conventional positions like Head of Finance and Head of Marketing. You can simply let your employees choose positions and titles based on their passions. 

By doing so, you'll find that there are people excited to tackle even the most annoying tasks!

And, finally, keep in mind that traditional business strategies might not work for your way of doing things. So, whatever consultants or so-called "experts" might tell you, running a business is a creative task that calls for creative strategies. 

For example, if your only goal is to cover costs and you aren't interested in growing, that's totally fine. If you just want to run a small tech startup without any dreams of being the next Steve Jobs or being bought out by Apple, that's your decision. Even things that seem absolutely normal, such as constantly posting to social media, are still decisions you should make for yourself, without fear of judgement.

> _"When you make it a dream come true for yourself, it'll will be a dream come true for someone else, too."_

### 8. Final summary 

The key message in this book:

**Starting a successful business is just a matter of finding a great idea, harnessing the will to satisfy your customers and creatively solving your problems. So, while it might not be the norm, forget about huge sums of venture capital and consultants; just set your mind to making your business dream become a reality.**

Actionable advice:

**Make business choices that work for you.**

The way you make decisions in business is personal. So, when faced with questions about your company's future, make sure you answer them in the way that works best for _you_. For instance, while it might be the norm to start at 9 a.m., you might find that a workday that starts later is better for you and your employees. 

**Suggested** **further** **reading:** ** _The Obstacle is the Way_** **by Ryan Holiday**

**In _The Obstacle is the Way_, Ryan Holiday brings the age-old wisdom of Stoic philosophy up to date. By examining the struggles of historical figures of inspiring resilience, Holiday shows not only how obstacles couldn't stop them, but more importantly, how these people thrived precisely _because of_ the obstacles. Holiday shows how we can turn obstacles to our advantage, and how we can transform apparent roadblocks into success, both in our businesses and our personal lives.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Derek Sivers

Derek Sivers began a career as a musician who endeavored to sell his music online. This simple idea became the basis of _CD Baby_, a company that was the biggest seller of independent music on the web when he sold it in 2008 for $22 million.

