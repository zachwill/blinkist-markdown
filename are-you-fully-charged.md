---
id: 55891da13466610007020000
slug: are-you-fully-charged-en
published_date: 2015-06-23T00:00:00.000+00:00
author: Tom Rath
title: Are You Fully Charged?
subtitle: The 3 Keys to Energizing Your Work and Life
main_color: CF293D
text_color: B52435
---

# Are You Fully Charged?

_The 3 Keys to Energizing Your Work and Life_

**Tom Rath**

_Are You Fully Charged_ (2015) is your guide to eliminating your off days, one positive interaction at a time. From socializing more to sitting down less, these blinks reveal easy-to-implement tips and tricks for generating the mental and physical energy you need, all by finding greater meaning in your life.

---
### 1. What’s in it for me? Give your life a positive charge. 

In our efforts to be happier and more successful in life, we tend to shoot for the moon. We often want to completely rethink the way we live — the problem is, that never works!

There are plenty of reasons why you don't have to completely transform your life in order to feel more charged, to feel those bursts of energy and happiness that we all seek. For example, instead of thinking about how our lives can be happier, we should be thinking about how our lives can be more meaningful. As you'll see, once you have found meaning, happiness will follow.

Taken together, these blinks show you simple ways in which you can use three keys — energy, interactions and meaning — to get fully charged in your professional and personal life.

In these blinks, you'll learn

  * how much sleep top performers get per night;

  * what can prevent those intense cravings for sweets; and

  * why making more money won't make you any happier.

### 2. Stop pursuing happiness, start pursing meaning. 

We all want to be happy. In fact, we want it so badly that we're willing to chase happiness for our whole lives. But few of us realize that the pursuit of happiness is in fact our greatest obstacle to it. The truth is that happiness isn't something you can achieve by seeking it; rather, happiness is a _by-product_ of living a meaningful life.

So where can we find meaning? Though we've been taught to find fulfillment and satisfaction in our careers and wages, these don't have the power to make you happy. US-wide surveys showed that even a sudden doubling of income produced only a nine percent increase in life satisfaction.

Money and success are _external_ motivations, and are not sustainable sources of meaning or happiness in the long run. _Internal_ motivations, on the other hand, give us a powerful way to experience meaning in every action we do.

Think about it: most jobs were created because they help other people, create progress, improve efficiency or produce something people need. You don't need to be the head of an international NGO to change lives — you can find meaning and internal motivation in any situation. If you work in a call center and use your warmth and understanding to help a stranger out in even the smallest way, you'll give both them and yourself a _positive charge._

You'll also double the charge if you can combine your strengths and interests with what others around you need. Remember: meaning does not happen to you, you create it.

### 3. Exchange passive reacting for active initiating. 

Be honest: how much of your day do you spend proactively pursuing your own well-being? Probably not a lot. In fact, you're likely spending far more time responding to external stimuli, such as your smartphone notifications and other distractions. Lapsing into this state of passive reacting can be dangerous.

A study of smartphone users has shown that phones are unlocked an average of 110 times per day, and nine times an hour during peak evening hours. But this is just a fact of modern life, right? Well, this constant activity is more damaging than you might think: a 2015 study discovered that the pressure people feel to respond immediately to a smartphone notification is associated with worse sleep quality, more sick days and a higher probability of mental and physical burnout.

If you want to manage these passive reactions, there are techniques that can help: switch off automatic notifications, and set specific times for checking your emails and social media accounts throughout the day. Not being disturbed by notification alerts, even for short periods of time, will make a massive difference for your productivity.

If you're still having trouble overcoming distraction, try taking a minute to write down the things that seem to waste your time most often, like that new game you downloaded on your phone.

Now that you've eliminated your passive reactions, it's time to start initiating! But be careful — we often tend to mistake certain activities for progress, even when they're not productive at all. If you find yourself keeping busy all day without actually getting anything done, this applies to you!

Spending all day responding to emails might feel like you're getting stuff done, but you're not actually proactively initiating anything yourself! To avoid disguising passivity as productivity, the author reminds himself that instead of considering himself "busy," he simply needs to manage his time better.

If you feel stuck and unable to begin initiating, start with simple things: try striking up a conversation with somebody new. In fact, even short interactions with those around you have great benefits for your well-being. Find out more in the next blink!

### 4. Keep thinking positively and spreading positive energy. 

Exchanges with people around you, no matter how brief, can give your day a positive or a negative charge. So how can you keep your day on the positive side? Always assume people have positive intentions.

Let's say somebody bumps into you and spills your coffee without apologizing. Instead of getting angry, consider whether the person has a serious problem on his mind and didn't even notice bumping into you. It's rare for people to actually have bad intentions!

If you assume people have negative intentions toward you, it'll only make you angry and will give you a negative charge for the next minutes, hours or days. Instead, assume that people only mean the best, and you'll be doing the best for your well-being too!

Another good way to boost your daily positivity levels is by paying attention to how your conversations unfold. A great rule is to keep 80 percent of your conversation positive. You can do this simply by using positive words. Did you know that every negative comment needs four positive ones to make a person feel neutral?

People are more likely to listen and be receptive when most of the conversation is positive. If you're flooding a conversation with negative words, others will most likely shut off. You might think that yelling criticism at somebody will get his attention, but chances are he'll just stop listening.

We can see our own performance improve exponentially when our self-confidence increases, so why not give the gift of encouragement to others! Remember to praise and recognize your peers, focusing on their hard work and successes and helping build their self-confidence. 

But not all compliments are made equal; the most lasting comments are sincere and specific. This will help others see what they do best and believe it. So instead of merely saying "good job" after a coworker's presentation, tell them that they explained it in a sophisticated and engaging way. After all, that's the kind of compliment you'd like to hear yourself!

### 5. Multiply yours and others’ well-being by investing in relationships. 

Another secret to feeling fully charged is to spend time on the right experiences with the right people. Relationships are not only a key to making you happy — they can also boost your creativity and motivation.

It's important to choose your friends wisely, but why exactly? Well, those who you spend time with will influence your habits, choices and therefore your well-being, whether you like it or not. So if you want to grow into a better person, it's definitely worth investing your time in those who care about your development.

Often when we find ourselves swamped with work, it's hard to even consider dedicating time to our social life without feeling guilty. But giving time to your friendships really takes no time at all! You can show your appreciation for those who support you by taking strolls together, or sharing a nice dinner. You'll be surprised at just how much better you'll feel afterwards, as these positive experiences have the power to put your problems into perspective.

Positive experiences also have a lasting value, and this is largely because we remember these kinds of experiences with others even better than we expect to. One study asked participants to estimate how happy they'd be after an experiential purchase, such as a concert, a trip or dinner at a restaurant. Two weeks after the purchase, they were 106 percent happier than they'd expected they would be.

As well as enjoying positive experiences long after they happen, you can also use them to lift your mood in the weeks leading up to them. When planning an activity, share it with others rather than keeping it a surprise; anticipation significantly enhances the experience. Research shows that looking forward to holidays increases well-being for weeks or even months.

Besides refocusing your life on meaning and spending time on your relationships, the keys to feeling your best are eating, moving and sleeping. The next blinks will explain this further.

### 6. Eat right to give your body the positive charge it needs. 

You make a decision with every bite you eat. Food is fuel, and it'll give you the charge you need if you eat right. But with all the bizarre fad diets out there, how can you know which one will actually lift your energy levels?

In the end, it's not all that complicated, nor does it require a total dietary makeover. You can start by adjusting and swapping the core elements of your diet. Avoid fried food, products with processed sugar and carbohydrates. Instead, gradually start building your meals around veggies and whole fruits. Swap your sugary energy drinks for water, tea or coffee.

Large-scale studies show that even a slight increase in protein intake alongside a decrease in carbohydrates has positive effects on your health. An easy shortcut can help you with every day shopping decisions: try to avoid food that has a carbs to proteins ratio higher than five to one.

Next time you're at the supermarket, check the nutritional values to ensure you avoid these foods.

A study done by the University of Missouri confirmed that a protein-rich breakfast increases the level of the brain chemical dopamine, which reduces cravings for sweets. So if you want to eat less sweets, start eating more protein: eggs, nuts, seeds, salmon or lean meat.

Finally, prepare healthy choices in advance to avoid any tempting last-minute decisions. Plan your shopping beforehand and avoid the sections with unhealthy food. Reorganize your kitchen so the healthy food is more visible and easily accessible, keeping it at your eye level or stored in visible places. And don't forget to bring small bags of fruits, veggies or nuts with you when you leave home, in case you feel like snacking.

### 7. Customize your workday to get your body moving more. 

Nowadays, people spend more time sitting than sleeping. In the long term, this can have damaging effects on our health. When sitting, the electrical activity in our leg muscles shuts off, calorie consumption drops to one per minute, enzymes breaking down fat decrease by 90 percent and good cholesterol drops by approximately ten percent per hour of sitting.

Sounds pretty frightening! But these negative impacts are easy to avoid. Simply stretching and standing up a few times an hour can make a huge difference. During exercise, our blood pressure and blood flow increase; this drives oxygen to our brain and allows us to think more effectively. In this way, just moving around can boost your concentration and mood for hours.

A surprising research study discovered that just a 20-minute, moderate intensity workout can improve your mood for up to 12 hours after the exercise. Even walking increases energy levels by 150 percent. The benefits of moving around are clear, so how can you start making it a bigger part of your day?

Study your surroundings for ways to increase your activity. One easy way to do this is by rearranging your home and office to encourage movement instead of convenience. Let's say you often print documents in your office — make sure the printer is not within your reach so you need to stand up every time you fetch your printed papers. 

Measuring your activity also motivates you to move more. During one experiment, people who were assigned to track their activity moved 27 percent more, simply as a by-product of measuring their movement. Use a health-tracking device — like an app or a pedometer — to track and set your activity levels.

According to the author's research, 10,000 steps per day is a good target to set. So, for those of you who say you've got no time to exercise, no more excuses! If you want to be more efficient, add exercise to your schedule and start saving time in the long run.

### 8. If you want to get more things done in less time, invest in your sleep. 

In today's business world, we perceive a proper eight-hour sleep as an unnecessary luxury or a sign of laziness. But don't be fooled! One less hour of sleep doesn't equal one extra hour of achievement or experience.

Insufficient sleep decreases your well-being, ability to think and your productivity, and also harms your health.

Research show that elite professionals sleep an average of eight hours and 36 minutes per night. The same study also indicates that frequent resting improves performance, as it helps prevent exhaustion and promotes full recovery. Harvard Medical School even found that sleep insufficiencies cost the American economy $63 billion a year due to lost productivity.

Your sleep quality is itself influenced by your activity during the 90 minutes before you go to sleep. A 2014 study found that late-night smartphone usage has a significant impact on sleep quality. That's right, your pre-sleep scroll of social media feeds is enough to make you fatigued and less engaged at your workplace the next day.

To ensure stress-free and quality sleep, avoid using electronic devices at least an hour before going to bed — being exposed to bright lights before bedtime also decreases your sleep quality.

Sacrificing sleep has diminishing returns, and won't give you the charge you deserve. So, if you have a lot of work to do, sleep more, sleep well and take regular breaks to keep your performance and efficiency at their highest levels.

### 9. Final summary 

The key message in this book:

**Despite what you've been told, prioritizing your well-being and relationships won't take a toll on your work performance. Instead, you'll become more effective, organized and productive. Learning to start living meaningfully and actively is what will help you live happily in the long run.**

Actionable advice:

**Hide your phone during conversations.**

Having your phone visible when you are around others will immediately decrease the quality of your interactions. This is the so-called _iPhone effect_, and a 2014 study confirmed that the mere presence of a mobile device (even if switched off) made conversations less fulfilling. Another study found that a visible cell phone had negative effects on attention and on people's ability to perform complex tasks. So, leave your phone in your bag and encourage your relationships to flourish.

**Suggested further reading:** ** _Thrive_** **by Arianna Huffington**

_Thrive_ argues that it's time for society to stop thinking of success only in terms of money and power, and redefine it altogether. If we want to truly thrive in our professional and personal lives, we have to create room for well-being, wisdom, wonder and giving as well.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Tom Rath

Tom Rath is a researcher who focuses on the connection between human behavior and health, well-being and business. He is the author of six _New York Times_ and _Wall Street Journal_ best sellers including _How Full Is Your Bucket?_ and _StrengthsFinder 2.0_.

