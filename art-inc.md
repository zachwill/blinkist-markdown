---
id: 545795d03365660008740000
slug: art-inc-en
published_date: 2014-11-04T00:00:00.000+00:00
author: Lisa Congdon and Meg Mateo Ilasco
title: Art, Inc.
subtitle: The Essential Guide for Building Your Career as an Artist
main_color: EA5E2F
text_color: B84A25
---

# Art, Inc.

_The Essential Guide for Building Your Career as an Artist_

**Lisa Congdon and Meg Mateo Ilasco**

_Art, Inc._ is a practical guide for artists who want to earn a living from doing what they love. Art, Inc. presents the business side of the art world in a palatable way, sharing useful advice and practical guidance.

---
### 1. What’s in it for me? Make your art work for you. 

You're making great art but how can you sell it? Sacrificing your worldly possessions and leading a life of struggle until you "make it" may have been the story of some famous artists of the past, but doing the same won't serve you.

As an artist today who wants to be successful, you need to not only have your creative skills, but also a good mind for business. You need to find your voice and your values, as well as know where to advertise, how to market yourself and sell your work. The following blinks will help you do exactly that.

In these blinks, you'll find out:

  * how to find your voice as an artist,

  * how to sell your work and

  * how to present yourself to galleries.

### 2. Being authentic is the best thing you can do for yourself as an artist. 

So many of us want to be great artists. But how do we get there? Study artists of the past, emulate them and their lifestyle? As tempting as it might be, doing the exact opposite is a far better idea.

We're all aware of the "starving artist myth," the idea that in order to create something worthwhile artists need to struggle and suffer like Vincent van Gogh. Although a lot of us cling to this myth, it's a lie.

Being a professional artist doesn't necessarily entail enduring a tough life and trying to make ends meet. It can actually mean being surrounded by people who care about you and support you, such as your family, friends, mentors and artist circle.

What you will need to keep your creativity thriving is passion, talent and hard work. The place you live and the amount of struggle you go through is irrelevant. So forget the starving artist myth and stop mimicking tortured artists of the past.

Often people tell artists that they need to learn from other established painters or experienced teachers, but the reality is that the best art is art that displays authenticity and the true inner workings of the artist who created it. It's the art that reflects your history and the events that made you who you are.

Therefore, if you are copying someone else, you won't be able to innovate. Taking risks, trying different techniques in your work and using different subjects for your inspiration will be far more valuable and worth your while.

The good news is that inspiration is all around you. To stay inspired, try keeping a journal and noting down your observations and activities. You never know when a seemingly insignificant event might become your next source of inspiration.

> _"Much of what separates successful artists from those who struggle is simply their mindset."_

### 3. Being an artist requires you to create a well-run business based on your values. 

So you want to be an artist? Fantastic! But first ask yourself, "Do I want to make a living out of it?" If so, you'll need to be more than an artist; you'll also need to be an entrepreneur.

To start thinking about yourself in a business sense, the first thing you should do is determine what kind of image you want to project. What type of artist would you like to be? What type of art do you want to make and what are your most important values?

If your dream is to work as an illustrator, consider what materials you'll need. In terms of values, perhaps environmental friendliness is important to you, and maybe there are certain companies you wouldn't work for based on your principles.

Regardless of what kind of artist you want to be, getting there can be a long process. However, identifying the steps you need to reach your goal and then following through with these steps will have you well on your way.

Take the illustrator scenario. If your goal is to work freelance for big companies, you'll need to find a name for your business, photograph your work so you can market it, use a high-quality printing lab for printing and come up with an attention-grabbing bio for your website.

As an artist, it can be off-putting to deal with the real world. Yet, this is an essential skill that must not be neglected.

You'll need to get a handle on paperwork, such as filing taxes and applying for business permits. For instance, if you are dealing with physical products such as paintings and sculptures, you'll need a permit to sell these goods. Taking care of these administrative tasks is vital, as not doing so could land you in a lot of legal trouble.

Try to see all these things as taking care of every part of you as an artist; such a mindset will keep you motivated along the way.

> _"Understanding how to run a small business is essential to making a living as an artist."_

### 4. The key to being a successful artist is to promote, promote, promote. 

What is most important in the artistic process? The art itself, right? Actually, as important as creating your art is, whether you're successful or not depends on how effective you are at promoting it. So how can you improve your promotion skills?

Start with the internet, which offers a vast space to promote your work. Make sure you have a good website where people can view your art and read about you as an artist. Make it easy to navigate and have your work readily available for people to view. You'll also want to make sure you have an easy, memorable domain name.

The website of Canadian painter Claire Desjardins, [www.clairedesjardins.com](<http://www.clairedesjardins.com>) is a good example of an attractive website that's easy to navigate.

You may also wish to try blogging, where you can share your thoughts, inspirations and achievements. The main thing to bear in mind when blogging is to be genuine and write about things that interest you.

You can also use social network sites as a promotional tool for your work, but don't get lost in the social network abyss! Many artists fall prey to writing social media posts so often that they start to neglect their actual work.

Aside from using social media, you can also attract the attention of the press. To do this, create a list of blogs and magazines you wish to contact and get the names and email addresses of their editors. But before you approach the press, you'll need a press kit. This includes your contact details, press releases, a bio and your CV. So be sure to have this ready before you send out an inquiry.

If you can perfect the art of promotion, there will be no stopping you.

> _"Promoting your work can be just as creative and fulfilling as making your artwork."_

### 5. Create, copy and sell your art as the professionals do. 

Being an artist in the modern world is actually pretty great as you can enjoy many benefits that previous generations never had.

One main advantage is access to digital technology. Whether you're a photographer or a painter, modern technology is a real asset. One such technology is digital reproduction. By replicating your work this way, you can sell your art to a multitude of people.

If you want to use this strategy, however, you must know your options.

First, decide which images you think your customers will appreciate most and consider your market niche. If you want to sell to renowned galleries, make sure you choose the highest quality paper and ink and limit the number of copies you print to ensure your work remains valuable. You'll likely want your work to stay in the best condition for as long as possible, so use archival acid-free paper and archival ink _._ This will protect your work from disintegration.

If you want your art to reach a larger number of people, however, you may opt for cheaper print methods and a higher number of copies.

However, when selling or publicizing your art, make sure you use the highest quality photographs to show your work.

Now, on to actually selling your art. How do you know how much to charge for it?

How you create your art will help you establish your asking price and the locations you wish to sell it. Your price should cover your costs, which includes the price of printing, materials you used and labor.

If you want to sell to a broad public, you can use online retailers such as _Etsy_, _Big Cartel_ and _Shopify_ or even your own website.

If you are looking to sell to prestigious clients, you have the option of selling your work to them directly or even working for them on commission.

Whichever way you choose to reproduce or sell your work, respect your art and you'll reap the rewards.

> _"Regardless of how you sell your work, make sure your customers are happy — individuals, galleries, and retail shops alike will make or break the overall success of your business."_

### 6. Exhibitions can be a great way to show your work to the world. 

There are many different artists and all have different requirements when it comes to displaying their work.

For many, the goal is becoming a gallery artist, a visual artist who shows her work in exhibitions. So let's look at how to do this.

As you might guess, an essential consideration if you want to be this kind of artist is to find the right exhibition space, as your work must be appropriate for the gallery in which you wish to exhibit it.

Every gallery has its own preferences and direction. San Francisco gallery owner Jessica Silverman, for example, likes to explore "artists and their relationship with the overconsumption of images," so she's interested in how artists stand out from everyone else in the art world.

If you have an idea of the kind of gallery you would like to exhibit in, you can reach out to some potential galleries by using the internet to locate specific galleries, asking your local art community or an art professor.

Once you've pinpointed some galleries you think will be suitable for your work, contact them, but be prepared to wait a while as a reply can take months.

If you're invited to come to the gallery with your portfolio, think about what to take with you. You should show at least two pieces of work in your portfolio and bring a copy of your CV, including your artist statement with an introduction to your work and why you created it.

If you can't find a gallery, don't worry. You can always organize a solo or group exhibition in a café, shop, restaurant or even in your own studio.

Just remember, if you're organizing a group exhibition, you will need to find a location and make sure each artist has the space to display their work. You should also think about how you will promote it by using printed cards, writing press releases or promoting it online.

> _"...Understanding exhibitions and the gallery world will help you to effectively plan ahead for opportunities and make smart decisions."_

### 7. One potentially lucrative path for an artist is to enter the illustration world. 

So we've seen how to get started as a gallery artist. Yet this is just one road an artist can choose to go down. Another rewarding path you can follow is illustration.

The illustration world is largely commercial and you'll need to have a good idea of how exactly you'll sell your work there. To do this, first think about which area you wish to focus on.

There are many options here: you could illustrate for wedding invitations, birth announcements, logos, advertisements and books, to name only a few.

When you've pinned down your niche, research any local jobs and start applying. You might want to advertise your services through your website or social media. You could also contact the art directors of websites you like and take part in design trade shows such as _Printsource_ or _Surtex_. 

If you land a job, you'll receive a briefing of what to illustrate, the number and size of illustrations and their associated deadlines. Be sure to read this carefully, be aware of what is required of you and who will have ownership rights of the final work.

Betsy Cordes, licensing consultant and creative manager, advises illustrators when going over contracts to remember that _they_ should own the copyright to their work. Remember, you're leasing your work for a specific purpose and a specific amount of time only.

If you want to reach for the biggest companies to illustrate for, it might be worth hiring an agent. The agent's job is to use his contacts to promote you and your work through social networking, print media and trade shows. Agents can be useful, but they are an added expense, and no matter how good they are, they can't turn a bad artist into a great one!

Lastly, remember when making decisions and dealing with commercial business to stay true to yourself. That way you can't go wrong.

> _"Illustration is a unique art form that is defined not by its medium, but by its context."_

### 8. Relish your productive and unproductive time. 

Constant movement keeps artists working and in the flow. But taking breaks every now and then is just as important and experiencing peaks and valleys is normal.

If you're experiencing some downtime in your career, it doesn't mean that you have to stop working on anything altogether. You can use your downtime to refresh your portfolio, take up a personal project, make new connections with people in the local scene or even take on a residency. These can be highly beneficial to help you evolve artistically.

You could also look into teaching others, which can be great for expanding creativity and reactivating long forgotten techniques. Plus, sharing your knowledge and experience with others can be stimulating and rewarding. You can look into teaching through galleries or businesses or you can even teach online.

In the ebb and flow of being an artist, sometimes the work can really pile up. When this happens and you have multiple projects on the go, it's a good idea to dedicate some time to organization.

Use a simple to-do list or calendar or try _Google Task_ or _Todoist_. There are many options out there for self-organization. And remember, you can always hire an assistant, if your finances allow.

Regardless of whether you can afford assistance, though, learning to be assertive and saying no to tasks you cannot add to your schedule can make your life easier. You might even find that saying no to one opportunity allows a better opportunity to come in.

Always try to keep balance in mind. You can balance out your work life by making sure you take regular intervals for family, friends, meditating, exercising and eating healthily. These things will help keep you grounded and able to cope in times of extreme stress.

Remember, success means balancing work and your life outside work.

> _"As artists, we can have enormous success, but we also have a responsibility to stay true to our values and our commitments to ourselves, our loved ones and our families."_

### 9. Final summary 

The key message in this book:

**Forget about the myth of the struggling artist. With an entrepreneurial mindset and some promotional and marketing know-how, there is nothing to stop you from taking your art and making a healthy living from it.**

Actionable advice:

**Packaging matters more than you think!**

Package your art with care and consideration. Why would you spend so long creating something only to cover it in shabby packaging? Present your art in the best way you can and don't forget to insure it against theft or damage if necessary.

**Don't sit around waiting for others.**

You shouldn't have to wait for a response from a gallery just so that you can exhibit your work. Instead, ask small cafes and shops if they will show your work or get some artist friends together and find a local space where you can display your work. All it takes is a bit of motivation and incentive to get your work out there.

**Suggested** **further** **reading:** ** _The War of Art_** **by Steven Pressfield**

In _The_ _War_ _of_ _Art_, author Steven Pressfield helps you identify your inner creative battles against fear and self-doubt and offers advice on how to win those battles. An inspirational book for anyone who's had trouble realizing their passion, it offers an examination of those negative forces that keep you from realizing your dreams, and shows how you can defeat your fears to achieve your creative goals.
---

### Lisa Congdon and Meg Mateo Ilasco

Lisa Congdon is an illustrator and has worked for _The Museum of Modern Ar_ t, _The Obama Campaign_ and _Simon & Schuster_. Her work has also been shown in _The Bedford Gallery_ and _The Contemporary Jewish Museum_.

Meg Mateo Ilasco is a creative entrepreneur and author of seven books including _Craft, Inc._ She has been a guest on _Martha Stewart Living Radio_ and her work has been featured in _Real Living_ and _Real Simple_.

