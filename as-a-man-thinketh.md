---
id: 56afa7f7377c160007000064
slug: as-a-man-thinketh-en
published_date: 2016-02-03T00:00:00.000+00:00
author: James Allen
title: As A Man Thinketh
subtitle: None
main_color: F16859
text_color: A6473D
---

# As A Man Thinketh

_None_

**James Allen**

_As a Man Thinketh_ (1903) captures the philosophy of James Allen. These blinks reveal the power that thoughts have over our personalities, our circumstances and our well-being. We discover the benefits of learning to master our minds and direct our thoughts toward the goals we want to achieve.

---
### 1. What’s in it for me? Become aware of your power and take responsibility for your life. 

Do you want to become powerful? Or is there a specific goal you want to achieve, like writing your first book? Typically, advice literature will tell you to pursue your goals by following deliberate steps and taking certain actions: listing your priorities, cleaning and organizing your desk or practicing your networking skills at the next conference. Well, that's the kind of advice you won't find in these blinks. 

Instead, _As a Man Thinketh_ aims to help you realize the power you already possess. Reading these blinks, you'll find out how everything in your life, from your strength of character and your physical health to your artistic achievements and your life circumstances, are determined by your thoughts. And you'll find out how to make the most of this unbounded freedom.

Reading these blinks, you'll also learn

  * why your mind is like a garden; 

  * that your thoughts can make you look older than you are; and

  * what's so great about being a dreamer.

### 2. By mastering our thoughts, we can change our lives. 

Ever sat and wondered what makes us humans what we are? It's a puzzling question, so here's a clue to start you off: man is the sum of _his thoughts_. 

Just as a plant sprouts from a tiny seed, each action we perform is an outgrowth of our thoughts. From these actions, patterns emerge. In turn, these patterns come to constitute our _character_. Our character or personality begins with our thoughts. But why is this important?

Well, have you ever known someone who always seemed to give up on projects or relationships, and at the same time had a pretty pessimistic attitude? You guessed it: those two things — attitude and action — are closely related. Underachievers tend to disappoint themselves because they _think_ very little of themselves to begin with. Attitude problems often snowball and have severe consequences. Yet, there's a very simple solution.

If our thoughts shape our character, it's only logical that changing our thoughts will change our character, too. By changing the nature of their thoughts, pessimistic people might find that they have a bit more to smile about. 

Anyone who attempts to master his thoughts can do much more than eliminate negative character traits. By working on his thoughts the right way, an individual can even obtain what the author refers to as _Divine Perfection_. In other words, by weeding out bad or useless thoughts, a person can bring joy, strength, peace and wisdom to his life. 

Those are some pretty attractive benefits! So why not start mastering your thoughts today? The alternative, after all, isn't pretty. Unmanaged thoughts can make _you_ into your biggest obstacle; negative attitudes literally have the power to destroy the things you cherish and love.

> _"As a man thinketh in his heart, so is he."_

### 3. The world we live in doesn’t just shape us – we shape it, too! 

Ever failed miserably at something and blamed it on the weather, the unpleasant phone call you had with your mother that day or on some particularly nasty childhood experience? Blaming failure on external factors is something we're all guilty of. It might make us feel better, but it only sets us back. 

Our circumstances — that is, all those external factors that impact our lives — are closely linked to our character. But they aren't linked in the way you might first assume. It's all too easy to think that our experiences and our living conditions shape who we are. But the truth is: we shape our world just as much as it shapes us!

Rather than being a mere product of circumstance, our character has considerable influence over what kinds of situations we end up in. 

So, if you end up in jail, it's not because the outside world or fate brought you there. Rather, the thoughts and attitudes you had about the outside world led you to the very situations that got you in trouble. 

Of course, it's not always clear whether character or circumstance exerts more influence on any given situation. We all know those people that have hearts of gold, and yet still face terrible adversity. We also know that many greedy, dishonest people lead satisfied lives, surrounded by wealth and admirers. 

This means that deducing someone's character from her circumstances is impossible. Likewise, we can't always predict the circumstances someone will end up in by examining their character. Many people in jail have unpleasant characteristics. But some of them have admirable sides, too.

> _"Good thoughts and actions can never produce bad results; bad thoughts and actions can never produce good results."_

### 4. Work toward better health and other goals by cultivating positive thoughts. 

Some of us age gracefully, while some of us — to put it lightly! — do not. Bad habits, lack of access to healthcare and unfortunate genes all play a role. But there's one factor we often forget to consider: our attitude. 

We've already seen what a powerful influence our thoughts have on both our character and the conditions we face. It should come as no surprise, then, that our thoughts impact our bodies, too. Unhappy thoughts have a slew of unfortunate side effects: a higher heart rate, poorer sleep, headaches, and yes, wrinkles from all that frowning!

On the other hand, thinking thoughts of joy, lightness and energy are powerful enough to make us feel happier — and younger, too! So if you want to feel better about your body, try directing your thoughts toward feeling better altogether. 

Directing your thoughts is crucial if you want to achieve any sort of goal. Whether you intend to reach new heights in your workplace, your relationships or your spiritual practice, it won't be possible unless you focus your thoughts on that goal.

This means letting go of all thoughts that distract you from your goals. If you notice that a certain thought is standing in the way of your purpose — by, say, making you afraid, pessimistic or uncertain — it's got to go. This isn't easy at first. But your mind is like a muscle; with practice, you can train it to think more effectively, positively and purposefully.

> _"Thought is the fount of action, life, and manifestation; make the fountain pure, and all will be pure."_

### 5. We are solely responsible for our ability to succeed, so dream big! 

In the second blink, we learned that external factors aren't to blame for the way we are or the mistakes we make. This may seem like a great burden, however. Are we solely to blame for our failures? 

This doesn't seem like a very helpful attitude, so let's change our perspective. If we think we're being victimized, we're bound to feel and act like victims, and continue being oppressed. Yet, if we don't let ourselves feel like victims, it's harder for others to make us their victims. Therefore, it's crucial that we understand that our ability to free ourselves lies _in our hands_.

No matter the circumstances we face, we should stay true to our values and ideals. Our highest aspirations — our dreams — are what give us the impetus to turn our goals into achievements. That's why it's so important to keep dreamers around, and to keep your dreams alive, too.

Without poets, painters and composers, our world wouldn't be the exciting place that it is. Dreaming also lets us discover new worlds. Columbus, after all, dreamed of another world. Without this dream, he would never have been able to discover it on his voyages. 

If you master your thoughts, thereby changing your character and your circumstances for the better; if you direct your thoughts to all that you wish to achieve; if you open your thoughts to the dreams that inspire you — then you'll discover that life itself has a new quality: serenity. 

A serene way of being is the mark of someone who has learned to live (and live well with) his thoughts. It takes time to get there, but there's no doubt that it's worth it.

### 6. Final summary 

The key message in this book:

**Thought is key to our lives. Our characteristics, our ability to achieve, the circumstances we face and even our physical health are all shaped by the thoughts we have. By learning to change our thoughts for the better, we can change our own lives too.**

Actionable advice: 

**Start weeding!**

The next time you're feeling tired and grumpy, it's time to do a bit of weeding. Not in the garden, but in your mind! Can you find thoughts that are harming you? If they're doing damage or simply getting in the way, pull them out! This allows you to cultivate your mind, keeping your attitude positive and directed toward the life you want.

**Suggested** **further** **reading:** ** _How to Win Friends & Influence People _****by Dale Carnegie**

The self-help classic from 1936 presents some basic rules for how to make a good first impression on people and win them over. Carnegie's advice is backed up by anecdotes of famous people, such as former US presidents.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### James Allen

James Allen (1864–1912) was a British author best known for his theories about the power of thought. His most famous work, _As a Man Thinketh_, has inspired generations of self-help writers.

