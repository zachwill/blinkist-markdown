---
id: 572603a6f2d1b1000391f2c8
slug: as-we-speak-en
published_date: 2016-05-06T00:00:00.000+00:00
author: Peter Meyers and Shann Nix
title: As We Speak
subtitle: How to Make Your Point and Have It Stick
main_color: CC3029
text_color: CC3029
---

# As We Speak

_How to Make Your Point and Have It Stick_

**Peter Meyers and Shann Nix**

Whether you want to speak more effectively at business meetings or deliver a memorable school presentation, _As We Speak_ (2012) outlines the proper techniques to win over an audience in a matter of seconds. You'll learn how to move and talk with purpose and get your message across.

---
### 1. What’s in it for me? Learn how to become an excellent communicator. 

We communicate all the time, spending a majority of each day chattering with friends or typing out emails or messages. Yet very few individuals love the challenge of standing up in front of an audience and delivering an important speech.

For many of us, the prospect of clammy hands, knocking knees and stuttering speech is enough to keep us far away from any podium or general meeting.

Communication, whether in front of a crowd or one on one, is a powerful tool. As business leaders, we need to convey important information every day to help lead others in doing their jobs well and making important decisions.

These blinks will help you become the great communicator you've always wanted to be. You'll learn how to prepare for and deliver an excellent presentation, by conquering your fears and realizing that effective communication is one of the most valuable gifts you can give your friends and colleagues.

In these blinks, you'll discover

  * why seven seconds is all it takes for an audience to switch off;

  * why peppering an email with "I" is the wrong way to get things done; and

  * how to keep your hands from shaking during a talk.

### 2. During a presentation, focus on feeding your audience’s emotional side with an interesting story. 

Let's imagine that you're about to deliver an important presentation on how to improve company performance. You've spent hours rehearsing and think you sound great; what's more, the content of your talk is top-notch and should be of great benefit to the company.

But as you finish, instead of applauding, your audience sits bored and disinterested.

What went wrong?

One common mistake presenters make is that we assume our listeners will be automatically interested in what we have to say. It's just not true. Instead, we should work to remind our listeners how they can benefit from what we have to say.

Likewise, a presentation can flop when it focuses too much on how ideas can improve the company overall, rather than benefiting the individuals involved. To keep an audience interested, focus on how your ideas will make their day more efficient and help them be more productive.

Another tip for getting a message across is to use stories, rather than relying on numbers and statistics.

Research tells us that the part of the brain responsible for decision-making isn't the rational or analytical, left side of the brain. Rather, decision-making happens on the right side of the brain, the side that processes feelings, emotions, humor and stories.

So if you want to influence someone, tell them an engaging story!

The internet has made this technique even more relevant. Because there is so much information out there, you have to find an effective way to cut through it all. This means shifting the focus of your presentation: don't dwell on _what_ you're telling people, but refine instead _how_ you're telling it.

To make sure you're engaging with your listeners' emotional side (the brain's right side), think about what people will feel as they listen to your presentation. Introduce your idea with a creative origin story; make the whole presentation fun or lively. In sum, do whatever you can to liven up how you communicate your ideas to your audience!

> _"This is, literally, the meaning of the word 'inspire': To breathe life into another human being. This is what it's all about."_

### 3. Keep your audience engaged and attentive by delivering a well-organized, three-point talk. 

If you've sat through many presentations, you probably know that one of the worst things that can happen is to listen to someone stumble through a rambling speech that never ends.

To avoid this, make sure you let your audience know where you are going with your presentation. It's crucial to start clearly.

It takes only seven seconds for a person to decide whether they're interested in what you have to say. Make these seconds count! Skip the pleasantries, such as "Good morning" and "Thank you all for coming." Certainly don't begin with a list of "housekeeping items" such as dull business reports. Such standard patter can be seen as a warning signal that your presentation is going to be boring.

Instead, there are many effective ways to grab your audience's attention.

One is to start your presentation is by opening with the word, "you." Also, statistics and numbers can be good openers, but only if they are shocking or attention-grabbing. Another option is to tell an interesting anecdote or story that creates a bond with your audience.

After you have your audience's attention, then you can go ahead and welcome them and address any needed housekeeping details.

Your presentation's structure should be clear and organized in a fashion that makes it easy for your audience to process the information you're delivering, by making clear what's in it for them.

No matter how much material you plan to cover, divide your presentation into three categories.

In your first category, explain the current situation. This may involve showing the latest quarterly report and the company's current market position.

In the second category, tell your audience where you want to go. Explore targets that should be met or that may have been missed using current methods.

And third, explain how long it will take to reach these goals. Now you share "next steps," that is how your audience can reach goals themselves. This could involve sharing updated recruiting strategies, possible budget cuts or marketing changes.

> _"People are motivated by the desires both to seek pleasure and avoid pain. When you're influencing decisions, you need to address both ends of this spectrum."_

### 4. Pay attention to what your voice is saying about you, because your audience certainly is! 

Unfortunately, even a tight structure and a good opening story won't prevent some members of your audience from dozing off. To keep them engaged, use the most powerful tool you have: your voice.

If you maintain only one tone of voice through your talk, your audience will get bored. Mix things up by routinely changing the volume of your voice and the speed of your delivery.

Also, try to smile. Smiling adds a note of positivity to your voice, and people react well to this. Keeping a smile on your face is especially important when you are giving a face-to-face presentation.

When you're delivering a video or audio presentation, however, it can be particularly difficult to keep people engaged.

In such situations, your voice is still important, but you also need to be clear about where your presentation is headed. Be sure to provide your audience with a clear topic, the goals of the presentation and specifically, when it will end. With this information, your listeners will be more engaged.

Providing clarity and keeping people engaged also applies to effective email communication.

When you write an email, be aware of the _I ratio_. This is the number of times you use the word "I" in your note. Here's a rule of thumb: Use the word "you" ten times for every time you use the word "I."

If you don't, the recipient might feel the email is full of selfish requests that only benefit the sender. By toning down your use of the word "I," you can better help people figure out what you want.

Keep emails short, and write precisely. In fact, the higher up the recipient is in the chain of command, the shorter the email should be. After all, it's likely that the CEO is already quite busy!

> _"Technology on its own won't create a bond, just as a music instrument won't play itself. You have to learn how to play the medium..."_

### 5. Everyone gets nervous during a talk, but make sure your body language doesn’t distract from your words. 

Though we may not like to admit it, we often judge people based on their appearance. And it is certainly natural to feel nervous when speaking in public. With this in mind, here are some simple tips you can follow to show your audience that you're calm and in control.

Shaky hands? Avoid holding loose pages of paper or a laser pointer, as doing so will only make the shaking more apparent. Instead, hold a book or grip a pen while you speak. If your legs shake, try to move around as you speak to get your circulation moving so you will feel stronger.

Excessive sweating? Whatever you do, don't take your jacket or sweater off! This will only show just how sweaty and nervous you are, and might give people the impression that you don't know what you're doing. For a sweaty forehead, make sure you have a cotton handkerchief at hand, and use as needed.

Trembling voice? When you're nervous, your chest and throat might tighten up and block your airflow, making it difficult to speak clearly. Practice breathing from your diaphragm, and not from your chest.

It's okay to use notes during a talk, as long as you remember to make eye contact with your audience after making an important point.

Granted, some people prefer to keep their face down in their notes to avoid looking at the audience. To prevent this behavior, your notes should offer just a basic outline of your main points.

If you use note cards rather than paper, make sure to number them so you don't lose the order. Use a black pen, and write clearly in large script so your notes aren't difficult to read.

But most of all, don't panic if you lose your way in a talk or draw a blank when trying to make an important point. Breathe, have a drink of water, and calmly refer to your notes. People will think that you are taking the time to make sure you're saying the correct thing.

> Do you get red faced during presentations? Wear red clothes and nobody will notice a thing!

### 6. Avoiding tough conversations can make a tense relationship worse; stay positive and have the talk. 

In previous blinks, we explored improving communication in presentations and email. Now let's look at how you can improve communication with colleagues through direct conversation.

Here's a common problem: a conversation that goes nowhere. Such an interaction is when two people talk _at_ each other, with neither person engaging with what the other person is trying to say.

To avoid this dilemma, make it clear from the start of your talk what you hope to achieve with the conversation.

Define your intentions before the discussion even begins. This way you can focus on what you want to accomplish as the conversation proceeds.

Importantly, separate the person with whom you are talking from the problem at hand.

Say you need to talk to a person who shows up late to work every day. Rather than being angry at the person, start the conversation by directly addressing the issue of being tardy, and calmly discuss ways toward finding a solution.

Addressing the issue directly allows you to find the real reason behind a person's behavior. You might find out that the person didn't even know there was a problem, as at her previous job, schedules were flexible.

If emotions do run high during a conversation, try to calm the situation by listening, asking questions and staying positive. The same holds for a situation that might already be tense. Try to ask questions that lead the person into entering your talk with a positive attitude.

Rather than demanding a person communicate with you, you might ask, "I would like to talk to you about your schedule. Is this okay?" Chances are, the person will agree; and you can reinforce positivity by showing appreciation and saying, "That's great. Thank you."

Throughout the conversation, respond positively and show you're engaged by saying things like "I see," and "That makes sense." Likewise, thoughtful noises when there is a pause in the conversation, such as "Hmm," and body language such as nodding and smiling, will also help reduce any tension.

> _"What people see and hear is just as important as what you say."_

### 7. Think of delivering a presentation as giving a positive, useful gift to your audience. 

Everyone has bad days. Unfortunately, it's exactly those days when we also have to stand up in front of an audience and deliver a meaningful presentation.

To ensure that you're in the right frame of mind despite having a bad day, identify your _positive performance preparation patterns_.

We all have unconscious patterns, such as routines we follow before going to bed. There are also things we do before a presentation or meeting, certain actions or movements that make us feel confident and prepared.

To get yourself in the right frame of mind, do things that normally make you feel confident and happy. Perhaps you might enjoy a cup of your favorite tea, or listen to a song that gets you pumped up.

It's also useful to find something that puts a smile on your face. And rather than sitting and stewing with your thoughts, it's good to walk around in a fashion that will emphasize feelings of confidence.

Avoid frantic "double-checking" that you are ready, as this can end up sabotaging your performance.

Don't make yourself crazy by asking yourself, "What if they ask something I don't know the answer to?" Or, "What if all my hard work is unconvincing?" You'll only make yourself self-conscious and generate negativity.

Turn these questions into ones with positive answers. Ask yourself: "How can I use their questions to build trust?" Or, "What's my strongest idea?" By doing this, you're reprogramming your brain to take you forward, not backward.

So instead of being self-conscious, think about what kind of gift you can give your audience.

Everything you do and say sends a specific message. This means that your actions are constantly influencing others. Define the outcome of your speech by putting all of that energy and passion toward making it a great experience — a gift for your audience.

> _"...the intention to give a gift will elevate what you're doing. The intention transforms the action."_

### 8. Final summary 

The key message in this book:

**To truly communicate with others, we must be able to connect with their needs. Whether it is just one person or hundreds of people, a speaker must always think first of what the audience needs. Communication is a gift because our words and actions have the potential to change people's lives. Good communicators take this responsibility seriously.**

Actionable advice:

**Don't forget to breathe!**

Next time you're preparing to stand in front an audience and deliver a presentation, if you're feeling nervous, take a minute to breathe deeply. Stop thinking about yourself and instead, think of what you want your audience to feel and think.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Talk Like TED_** **by Carmine Gallo**

In _Talk_ _like_ _TED_, you'll learn about presentation strategies used by the world's most influential public speakers. Author Carmine Gallo analyzed more than 500 TED talks to identify common features that make these talks so influential and appealing.
---

### Peter Meyers and Shann Nix

Peter Meyers is the founder of Stand & Deliver Consulting Group. An award-winning actor, Meyers also teaches classes in performance and leadership at Stanford University, the Esalen Institute and the International Institute for Management Development in Lausanne, Switzerland.

Shann Nix is an award-winning journalist, novelist, playwright and radio talk show host.

