---
id: 56000472615cd100090000ee
slug: ask-en
published_date: 2015-09-25T00:00:00.000+00:00
author: Ryan Levesque
title: Ask
subtitle: The Counterintuitive Online Formula to Discover Exactly What Your Customers Want to Buy...Create a Mass of Raving Fans...and Take Any Business to the Next Level
main_color: BD202C
text_color: 8A1720
---

# Ask

_The Counterintuitive Online Formula to Discover Exactly What Your Customers Want to Buy...Create a Mass of Raving Fans...and Take Any Business to the Next Level_

**Ryan Levesque**

_Ask_ (2015) is based on a simple idea — that learning what your customer wants is as simple as asking. The trick is asking in the right way. Full of immediately actionable insights into customer behavior, _Ask_ gives you all the tools you need to uncover what your customers really want and give it to them.

---
### 1. What’s in it for me? Give your customers what they want – even if they don’t know they want it. 

Anyone who's ever shopped online or started an online store knows that online businesses are plagued by inefficiency. Visitors to websites usually leave them rapidly; there's nothing they want to buy, so they close the window or move on to another site. 

The problem is that you can't just ask people what they want and then provide them with it. 

That's where the Ask Formula comes in. The Ask Formula is a series of surveys that you can give to prospective customers and current customers. As these blinks explain, once people take your surveys, you'll know what people's biggest challenges are and how you can solve them. 

And solving such problems is what will put your business ahead of the competition.

In these blinks, you'll learn

  * why nobody knows what they want;

  * how to win over people who rejected your first offer; and

  * which email subject headings will get people to open them.

### 2. All too often, surveys fail to deliver what they promise. 

The _Ask Formula_ was conceived as a solution to a problem faced by every business owner: how to learn what customers and prospects want? Knowing what is wanted positions businesses to personalize services and answer customer needs.

So how do you find out what is wanted? Well, you could simply send out a survey. Traditional surveys, however, come with several inherent problems.

First, surveys are irritating. Remember the last time your dinner was interrupted by a telemarketer asking for your opinion on a thrilling topic like new soda bottle brand packaging? Exactly — irritating.

Second, there's no real incentive to answer a survey, since the customers don't have access to the results. In general, surveys are designed by marketers to help marketers.

Other approaches to asking customers fail because people are only good at two things: expressing what they _don't_ want and saying what they already have.

Businesses want to know what people _want_ — but those same businesses are no good at helping potential customers figure out what that thing might be. For example, if you're browsing the shelves at a store and a salesperson asks, "How can I help you?", she's not helping you make a choice. Rather, she's assuming that you already know what you want so that she can help you find it.

But most potential customers have no idea what they want! Just think about the last time you tried to decide where to go out to eat with a group of friends. If you ask them what they want to eat, you enter an endless loop of suggestions and questions, and no decision gets made.

But as a business you still have to find out what people want, even when they can't quite articulate it. The Ask Formula will help you do this by using surveys in a different and unexpected way.

### 3. The Ask Formula starts with getting to know your prospects. 

So, traditional surveys don't work. They annoy customers and don't offer any useful information. So how is the Ask Formula any different? 

For starters, the Ask Formula relies on a _Survey Funnel_ strategy, that is, the combination of steps that transform your prospects into paying customers. 

We'll take a closer look at each step in later blinks, so for now, before you start the funnel, you need to construct a _Deep Dive_ _Survey_. You'll only conduct this survey once, at the very beginning of the process; it uncovers who your prospective customers really are. 

The Deep Dive Survey begins with an email to your list of prospects. These could be current fans or former customers. The email should quickly introduce your company and explain that you'd like to get to know your potential customers better. For example, you might write, "Here at Home Lighting, we want to know who our customers are so we can give them the best service."

Then comes a small sample of survey questions. Remember, your prospects only know what they _don't_ want, so it's not worth directly asking what they want. Instead, you have to coax it out of them with questions like, "What is the biggest challenge you're struggling with right now?"

Next, direct respondents to complete the survey on a website where you can collect the answers.

Finally, collect and analyze the survey data. Look for answer patterns and sort them into _buckets_, that is, similar groups of prospects. For example, if you are a B2B company, one bucket could be companies that earn less than $500,000 annually, another for those with more than $5 million, and so on.

By organizing respondents into buckets, you can learn the natural consumer language to use when communicating with each group. You wouldn't want to write an email to your retiree bucket in the technical language of your software geek bucket!

Once you know who your prospects are, it's time to move on to the next step.

### 4. A great landing page will convince prospects to take the next survey. 

Thanks to the Deep Dive Survey, you should have a good idea of who your prospects are. The next step is getting them to tell you more; then you can actually start solving their problems. The way you'll accomplish this is with another survey.

In order to entice prospects to take the next survey, you need to design a convincing landing page, called the _Prospect Self-Discovery Landing_ _Page_. All key information should be available on this page without requiring readers to scroll down. Here's what the page should look like:

Start with a standout headline or question. As an example, the author's RocketMemory homepage asked, "Is it possible to improve your memory in just three days using these techniques?" 

Then, tell the prospects your goal and explain how the survey will help you achieve it. For example, you could say something like, "We want to help you diagnose your problem by asking a few questions, and then offer you a customized solution."

Increase participation by adding a video to your landing page. An easy way to do this is with a "talking head" video, in which you or an actor explains your company. Slides with voiceover also work.

The key to the video's success is a good _hook_ — a compelling idea that inspires curiosity and desire. To find it, ask your viewers about their struggle. Whether it's losing weight or improving their tennis serve, a good hook will grab their interest, enticing them to take the next survey.

Not everyone is easily persuaded, however. Skeptics can be coaxed with _if-then statements_, like: "If you work with diverse markets but aren't sure how to communicate with all of them, then…"

Finally, introduce the survey to your prospects by hinting at your expertise. Write something like, "For ten years now I've asked my clients a series of questions that helped identify their biggest sales bottleneck."

Once you've won over your prospects, they'll eagerly click the link and begin the next survey.

### 5. Gradually win prospects’ trust with the Micro-Commitment Bucket Survey. 

So, your prospects are hooked and ready to take your survey. Now it's time to deliver your _Micro-Commitment Bucket_ _Survey_. 

This survey starts with small, non-threatening, multiple-choice questions, before moving on to more private questions, such as name and email. It starts this way because people are reluctant to divulge personal information and little questions chip away at that reluctance.

It's important to phrase these questions in a highly conversational way — not a robotic one. For example, "What's your favorite soda brand?" is a far better question than "Which market-leading soda manufacturer do you most prefer?"

Your Micro-Commitment Bucket Survey should start with easily answerable questions — "How old are you?" for instance. Use this data to personalize messages by, for instance, referring to your prospects' age in a follow-up email. 

Finally, use _segmentation_ _questions_ to help characterize your prospects and funnel them into different buckets.

Avoid the temptation to segment by demographic (age, location, etc.) and instead try to ask questions that will segment prospects according to the specific challenges or problems they want to solve. 

For example, you might ask the multiple choice questions, "Which of the following is your biggest challenge right now?" with the answers "Selling to multiple sub markets," "Making paid traffic convert," and "Entering a new market." Each answer gives you insight into your customer's _specific_ problem.

Now it's time to sort the data you've received. In the Deep Dive Survey, you created buckets. Using the Micro-Commitment Bucket Survey, you'll fill and refine these buckets.

For example, if you're a car manufacturer, you could sort prospects first according to their marital status and then sub-segment these buckets with the number of children in each family.

This type of segmentation is invaluable for future promotions, as it helps you introduce relevant products to your customers in a marketing language they understand.

The very last thing you'll do is send your prospects to a capture page, where they'll enter their name and email address.

### 6. Now get ready to make your pitch. 

After submitting their survey, prospects will land on your _Post-Survey_ page, where they'll receive a diagnosis of their problem and a solution to it. 

This page will be nothing but a headline and a video offering personalized feedback to the concerns they shared in the survey. 

If you were a doctor, this would be where you offer your patient a prognosis and walk them through the various treatment options. You wouldn't rush them into surgery or talk about costs! Your goal here is to demonstrate that you understand what they're going through and motivate them to ask _you_ what they should do about it?

That's when you make your sales pitch.

Like your landing page, your sales pitch should also include a video, preferably using the _Problem, Agitate, Solution formula_ : introduce the prospect's problem, reinforce its severity and urgency and then explain its solution.

Start by providing a creatively named diagnosis. For example, you might tell the viewer: "Based on the information you've provided, your marketing bottleneck is caused by the _Cold Traffic Curse_." 

Your diagnosis should immediately spark your prospects' curiosity and make them want to learn more.

Next, explain what your diagnosis means. Demonstrate that you understand their problem and focus on its urgency and severity, while also showing that you care about the outcome. Going with our previous example: "Many websites today face the Cold Traffic Curse, preventing them from converting visitors into customers and threatening business."

Finally, explain your solution by turning education into an offer. Your offer should include the benefit, that is, what's in it for your customers when they purchase; a description of the offer; the discount price or special deal available; and the primary reason why they need to act _now_ (like deadlines or limited availability).

Up to this point, the Ask Formula has helped you identify customer problems so that you can solve their specific problems. In our final blinks, you'll learn how you can make the most of these new conversions.

### 7. After your first offer, the Ask Formula helps you maximize profit. 

The Ask Formula doesn't end with this first offer on the Post-Survey page. Rather, it aims to drive sales well into the future. But how? 

Once you've generated a customer, the real benefit comes from the extended relationship you'll have with that customer as you sell to them over time. This is where the _Profit Maximization Upsell sequence_ comes into play.

Think about your latest visit to McDonald's. They don't make billions in sales simply because people buy burgers. Rather, they ask, "Do you want fries with that?" 

Once someone has already decided to make a purchase, they're in a buying mindset and thus much more likely to spend more money right then and there.

Thus, directly after your prospect makes their initial purchase, present them with one or more _one-click upsell_ opportunities that don't require them to re-enter payment information. 

One option might simply be to sell more of what the customer just purchased, but at a discounted price. For instance, if you sell protein powder, you could offer three discounted tubs of powder with each new purchase.

Another option is to sell a faster, easier result. If you sell a gym membership online, for example, you could target the people who expect fast results and propose a 30-day pass to a fat-burn boot camp in addition to the membership.

Finally, you could offer a solution to a problem that your prospects hadn't even thought about yet but will surely face after getting results from your initial product. To conceptualize this, imagine that you sell a salary-negotiation course, which helps people argue for higher wages. You now have the opportunity to help customers with the new "good" problem of having excess cash.

Your upsell could emphasize the importance of investing their excess money wisely and offer a complementary program focused on investment. This is typically a "good" problem that your clients don't have yet but may be facing soon.

### 8. Finally, rescue sales from non-buyers and keep your customers coming back. 

Unfortunately, only a small fraction of visitors — possibly as low as ten percent — will buy after this first round of surveys. But don't give up on the other 90 percent! The Ask Formula has a special way of following up with these reluctant prospects to win their business.

Email follow-ups offer a constant string of communication that helps optimize and improve the marketing funnel through the _Email Follow-Up Feedback Loop_. There are two types of email sequences: one for non-buyers and another for buyers.

Let's start by focusing on following up with non-buyers, those who didn't immediately buy the offer presented at the post-survey sales prescription. Emails to non-buyers have to be highly engaging in order to rescue a sale.

One way to keep them engaged is to tweak the offer without devaluing the product by offering discount after discount. Try lowering the threshold of commitment — by offering, for example, a one-dollar trial period with the full amount due at a later date.

In addition, urgency emails can be used to remind non-buyers of your offer's time limitation. Knowing that an offer is only valid for another hour could convince some non-buyers to pull out their wallets.

Lastly, if the initial offer failed, give people the opportunity to _pivot_ and choose their own next step by simply starting the Ask Formula cycle again, this time focusing on a different kind of product.

Luckily, you can use a similar process to get more from your buyers, but this time you'll prime them for another purchase by leveraging the joy they got from the product you already sold them.

A good way to entice buyers to open your emails is to put an incomplete thought in the subject line. For example, "Where there's a will there's a…" tickles the reader's curiosity and entices them to read on.

In the body, you should include a link that directs them to your offer; that'll turn these one-time customers into repeat buyers.

### 9. Final summary 

The key message in this book:

**If you want to know what your customers really want, the best thing to do is to ask. The trick is to ask in the right way, using an organized system that pulls actionable information from prospects while also building trust and intrigue. In other words, you need the Ask Formula.**

Actionable advice:

**Nothing-fits-all**. 

There's no such thing as a one-size-fits-all solution. Everyone's situation is unique and thus the solutions that are best for each individual customer will likewise need to be unique. 

**Suggested** **further** **reading:** ** _Launch_** **by Jeff Walker**

With his _Product Launch Formula,_ Jeff Walker has changed the way we sell. In his book _Launch_, he outlines his tried-and-tested strategy for selling products online and building a business that's almost guaranteed to bring success. Walker shows you how he's succeeded by outlining a step-by-step plan for launching your product — even before you know exactly what it is.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ryan Levesque

Known as the "conversion genius," Ryan Levesque has led a successful career in business and has helped businesses in 23 different industries to increase their conversion and drive revenues. This is his first book.

