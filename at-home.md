---
id: 58531b7858b1350004ed177b
slug: at-home-en
published_date: 2016-12-30T00:00:00.000+00:00
author: Bill Bryson
title: At Home
subtitle: A Short History of Private Life
main_color: 608C56
text_color: 4F7346
---

# At Home

_A Short History of Private Life_

**Bill Bryson**

_At Home_ (2010) offers an in-depth look at the history of the home. These blinks walk you through stories that each "take place" in a different room in a house, explaining the history of spaces such as a bathroom or kitchen. Interestingly, you'll explore how each space evolved into the rooms we live in today.

---
### 1. What’s in it for me? Break down the walls of history and learn about the home. 

Did you ever wonder why your house is built of wood and brick? Why you insist on adding salt and pepper to everything you eat? Or how exactly those canned green beans in the pantry came to be?

If such questions keep you up at night, these blinks will be soothing bedtime reading.

We say home is where the heart is; but really, home is where the history lives. These blinks walk you through a "typical" house to discover the curiosities that each room reveals.

In these blinks, you'll also learn

  * why French soldiers had to shoot canned food to eat;

  * why ladies avoided board games for their "sexually stimulating" effects; and

  * why medieval monks were more than a little funky.

### 2. Soldiers once needed to shoot cans open to get at the food inside; in general, food safety was lax. 

Nearly every modern Western kitchen has a cupboard stacked high with a colorful array of cans, filled with foods from olives to peaches to peas.

Yet people didn't always have such easy access to healthy, non-perishable foods. How to preserve food to last through a winter, say, was once a big challenge for families.

In the late eighteenth century, a Frenchman named Francois Appert proposed storing food in glass jars.

Appert's idea was a huge breakthrough at the time simply because the alternatives were poor. Unfortunately, however, the glass jars didn't seal well, meaning that air and bacteria could contaminate the food.

In the early nineteenth century, an Englishman named Bryan Donkin came up with the sealed metal can. He made his cans from wrought iron, however, which was exceedingly heavy and difficult to open.

Just how difficult? Well, some cans came with instructions on how to break them open with a hammer and chisel. Soldiers issued canned food as rations would have to shoot the can or stab it with a bayonet to get at the food inside!

Later cans were produced from lighter materials but were still difficult to open; that is, until 1925 when the can opener was invented.

So while inventors were working on more efficient ways to preserve and consume food from a can, a hungry public was plagued by another problem: food adulteration. In the food industry in the seventeenth century, this was common practice; and as there was little official oversight, no consumer could be completely sure what exactly he or she was eating.

Sugar was commonly "cut" with gypsum, sand or even dust. Tea was often a mix of tea leaves, dust or dirt. Vinegar was "complemented" by sulphuric acid; chalk was a common additive in milk.

Today, luckily, governments enforce food standards, and for the most part, we know what we're putting in our mouths!

### 3. The lack of limestone and timber in America led British colonists to use stone as a building material. 

Have you ever wondered how common materials such as wood or brick became the building blocks of our homes? This terrific story touches on both colonial British and early American history.

Let's start with wood, which owes its use as a building material to the British colonies in North America. When newly-arrived settlers began establishing homesteads, they faced a problem — namely, a lack of limestone. Back in Britain, they would have built their homes from a combination of mud and sticks, held together with lime. But lime was not available in North America and without it, the settlers' early houses were shoddy and fragile. Houses fell down so often that it was rare to find one that had stood for more than ten years.

Faced with this problem, the colonists turned to a far sturdier building material; wood. Unfortunately, timber supplies in the North American colonies were also poor. Many forests had been cleared by the Native American tribes to improve hunting.

Although attempts were made to manage the dwindling number of trees — for example, by cutting off the tops of trees (instead of cutting down the whole tree) so trees could rapidly regrow — it wasn't sustainable to keep building with wood.

All of this led colonists in America to start using stone.

Stone was plentiful in Britain as well, yet it wasn't widely used. Stone posed problems; it was heavy and expensive to transport.

So even though English lands were rich in limestone, a very sturdy building stone, this resource was hardly exploited as it was so expensive to extract and move from place to place.

Thus only the grandest construction projects were built from stone, such as churches and castles. Building a monastery, for example, would require 40,000 cartloads of stone, at the very least!

So if a normal family had no wood and couldn't afford stone, what _would_ they build a home with?

> _"…the woods that greeted the newcomers were not quite as boundless as they first appeared…"_

### 4. The whimsies of fashion affect building materials, too, and London’s bricks had a fluctuating existence. 

We've learned that building materials, like wood and stone, were used to build homes based on availability and affordability. Yet fashion also played a part in determining a builder's preference.

When expensive stone wasn't an option, many English families opted to use brick to build their homes. This was often the case in areas where limestone was in short supply, like in London. What London did have was quantities of iron-rich clay that builders could bake into bricks at a building site, eliminating transportation costs entirely.

The popularity of brick as a building material, however, changed with the American Revolutionary War. As the end of this costly war also meant the end of American taxes being sent to British coffers, the government quickly realized it was short on cash. So in 1784, Britain instituted a brick tax.

As a result, bricks fell out of favor as a popular building material. What's more, using brick to build one's home, especially traditional red brick, came to be associated with poor taste, with prominent figures such as architect Isaac Ware going so far as to call brick an "improper" material for refined homes.

In place of brick, stucco and stone became popular during the late Georgian era, from 1714 to 1830. Many brick houses at the time were "glazed" with stucco, a mixture of cement, lime and water, which made the houses look as if they were made of stone.

Another cosmetic fix was to build a facade of stone to cover up the brick underneath. For instance, the Apsley House in London's Hyde Park, now the townhouse of the Duke of Wellington, was built in such a fashion, its brick structure covered by a layer of solid stone.

Let's now move from the exterior to the interior of a home, to look at the history of the bedroom.

### 5. A nineteenth-century bed was often stuffed with straw and home to rodents and bugs. 

Your biggest complaint about your mattress is probably that it's either too firm or too soft. Regardless of your preference, you probably would have loathed your mattress in the nineteenth century.

Beds of this era were often stuffed with all manner of things, both dead and alive!

While most mattresses were filled with straw, other stuffing materials such as feathers, hair, sea moss and sawdust were also used. Unsurprisingly, it was difficult to keep insects and rodents from cozying up in such bedding. Pests like bedbugs, moths, mice and rats were common in the bedroom; if something rustled underneath your bedspread, more often than not it would be an uninvited bedmate!

In a letter from 1897, an American girl named Eliza Ann Summers wrote to a friend that she'd taken her shoes to bed with her to use as a weapon against the rats.

But having pesky rodents in bed wasn't the only problem with mattresses. Mattresses also had to bear the unsavory connection between the bed and sex, which, along with masturbation, was considered unhealthy or even dangerous.

In the nineteenth century, many people believed that if a woman experienced sexual arousal while conceiving a child or at any time during her pregnancy, the fetus would be irreparably damaged. Women thus were advised to avoid such "stimulating" activities as reading or playing board games.

Sexual mores were restrictive for men, too. It was commonly thought that for a man to release his seminal fluid anywhere other than in his wife's body during intercourse would be to irreparably weaken his body and mind. Masturbation, or _self-pollution_ as it was known at the time, was strictly off limits.

To help men avoid "accidents" while sleeping, in the 1850s inventors came up with the _Penile Pricking Ring_. This ring-shaped device was lined with sharp, inward-pointing pins that would prick a man's penis if it swelled during the night.

So when you tuck yourself into bed this evening, sleep easy knowing that your night's rest is incomparably more comfortable and less dangerous than that of people from centuries gone by!

### 6. Ancient Romans loved taking baths, but medieval thinkers thought dirt brought you closer to God. 

A bath can either be a cozy, soothing retreat from the stresses of life or a time-consuming, albeit necessary, hygienic measure. Yet taking a bath today is nothing like it was in ancient Rome.

Many Romans delighted in spending time in grand bathing halls, a practice that was a lifestyle choice for socializing and not just a necessary habit to keep clean. 

Bathing was so much a part of everyday life that some ancient Roman bath houses even had libraries, barbers, tennis courts and brothels.

Importantly, bathing wasn't just an indulgence of the rich and famous but a pastime that was accessible to people of all classes.

Such a social tradition, however, didn't last. Many early Christians thought that an unwashed man was a holy man.

It was no accident that in 1170, the undergarments of the Archbishop of Canterbury, Thomas Becket, were discovered full of lice on his deathbed. English monk Godric became a saint after making a pilgrimage from England to Jerusalem without taking a single bath.

Events would further solidify medieval society's aversion to bathing. The bubonic plague, which began around 1350, eventually raised awareness of the benefits of good hygiene — yet at the time, people hadn't made the connection between hygiene and disease.

Learned men pondered the scourge, wondering how to stop the spread of deadly disease. Yet they concluded that bathing was _bad_, as a hot bath opens up the pores on a person's skin, making the body susceptible to infection.

So for hundreds of years, bathing was synonymous with disease. People felt much "safer" covered in sweat and dirt, as their pores would remain closed and illness would stay at bay. Skin rashes and persistent itching were just a part of life.

It's no wonder the plague and other infectious diseases ran rampant at the time!

> "_No one really knows quite what the Romans got up to in there, but whatever it was it didn't sit well with the early Christians."_

### 7. We eat salt to survive; we consume pepper because it’s popular, or so said the ancient Romans. 

If you glance at a dining table anywhere in the Western world, you'll see shakers of salt and pepper. But have you ever wondered why this combo became such a permanent table fixture?

Well salt is essential to human biology. For thousands of years, humans have gone to great lengths — from the creative to the violent– to secure salt. This makes sense when you know that, without salt, you'll die.

And although scientists today know much more about the key functions salt plays in our bodies, we've been consuming salt for millennia. The Aztecs, a Mesoamerican people who lived in Central America from the fourteenth to the sixteenth century, would dry urine to make edible salt.

Societies across history have traveled and fought wars in the name of this needed mineral. And people in power often used salt to display power. Henry VIII of England, for example, in 1513 killed some 25,000 oxen and ordered the meat preserved in vast quantities of salt.

Pepper, in contrast, isn't necessary for human survival. In fact, we can live perfectly healthy lives without it.

Yet pepper as a condiment was wildly popular among ancient Romans, who played a major role in establishing its consumption in history. Roman love for pepper drove up its price and status.

This spice was so prized that, in 408 CE when Goth tribes nearly decimated the Roman Empire, the Romans offered the invading army some 3,000 pounds of pepper to call off their soldiers.

In 1468, Duke Karl of Bourgogne ordered 380 pounds of pepper as decoration for his wedding, simply to show off his wealth.

The history of salt and pepper is just one chapter in the many tomes our homes hold. Whenever we eat, sleep or patch a hole in a wall, such stories are waiting to be heard.

### 8. Final summary 

The key message in this book:

**What we call "home" has changed dramatically over the centuries. Domestic spaces and contemporary habits are drastically different than they were previously; living spaces have evolved with human needs and wants.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Happier at Home_** **by Gretchen Rubin**

_Happier at Home_ (2012) is a guide to transforming your home into a sanctuary that reflects your family's personality. By helping you identify both your and your family's needs, this book gives you everything you need to start changing your home and family life for the better.
---

### Bill Bryson

Bill Bryson is an author of many bestselling books, on topics ranging from science to language and travel. He previously worked as a journalist and chief copy editor at British newspapers _The Times_ and _The Independent_. His other titles include _Notes from a Small Island_ (1995) and _A Short History of Nearly Everything_ (2003).

