---
id: 5746b709b49ff200039c4d58
slug: atomic-accidents-en
published_date: 2016-05-30T00:00:00.000+00:00
author: James Mahaffey
title: Atomic Accidents
subtitle: A History of Nuclear Meltdowns and Disasters from the Ozark Mountains to Fukushima
main_color: CDD845
text_color: 55591D
---

# Atomic Accidents

_A History of Nuclear Meltdowns and Disasters from the Ozark Mountains to Fukushima_

**James Mahaffey**

_Atomic Accidents_ (2014) explores the evolution of one of the most fascinating and yet controversial technologies of our times, nuclear energy. These blinks explore the development of nuclear technology and reveal the details behind the tragic accidents that occurred along the way.

---
### 1. What’s in it for me? Understand past mistakes and the future promise of nuclear power. 

When Japan was struck by a powerful earthquake and tsunami, one of its coastal nuclear power plants, Fukushima 1, experienced a catastrophic meltdown. As a result, German Chancellor Angela Merkel changed her previously positive view of nuclear power to one of outright distrust almost overnight.

This disaster was the most recent in the history of nuclear power. Such accidents, as rare as they are, are sufficiently horrific to change public opinion irrevocably. Nuclear power was once seen as an energy source that held the key to a greener future. After Fukushima, however, this is no longer the case.

Through exploring the history of atomic energy, you'll discover how decades of development have led society toward safer ground and more stable ways of harnessing nuclear power. Given the relatively short history of nuclear experimentation, the years ahead just might bring us into a new nuclear era, completely free of nuclear fears.

In these blinks, you'll learn

  * how a decade of technology determined the fate of Fukushima's two plants;

  * why human error played a huge part in the Chernobyl meltdown; and

  * how 300 years, not 30,000, could be the new "safe" number for nuclear waste.

### 2. The discovery of radiation and radioactive elements was both exciting and deadly. 

At the end of the nineteenth century, Nikola Tesla accidently discovered radiation. Then, in 1896, after more research and experimentation, Wilhelm Rontgen published the first paper on radiation.

Soon, scientists around the world were researching this new phenomenon. Scientists Marie and Pierre Curie soon discovered a radioactive element that they named _radium_.

Yet underneath the excitement, radiation held a dark secret: it was mortally dangerous.

The pioneers of radiation research were not aware of its pernicious effects. They would all suffer, in one way or another, from the negative effects of radiation.

Tesla's health deteriorated after repeated exposure to radiation. An assistant of Thomas Edison's died from overexposure; Pierre Curie was weakened from prolonged exposure to radioactive materials.

While new medical applications for radiation were discovered, because people didn't yet know how truly dangerous it could be, those using these new methods didn't take sufficient precautions.

X-ray machines became a widely used diagnostic tool, yet technicians, exposed on a daily basis, suffered from leukemia and cataracts. Today medical professionals use appropriate precautions to protect themselves from excess radiation exposure.

Despite the health risks, radiation still offered many exciting solutions. Radium therapy, in which tumors are exposed to radium, became one of the few effective treatments against cancer at the time.

The "healing power" of radiation, however, was a popular idea and one that foolhardy entrepreneurs tried to cash in on, often with tragic consequences.

Businessman William Bailey created a "medicine" that was essentially water enriched with small quantities of radioactive materials. His tonic, "Radithor," was popular until people started getting sick. Millionaire Eben McBurney Byers, who had consumed large quantities of the tonic, suffered from weakening bones, so much so that his jaw almost completely deteriorated.

Such tragedies changed the public perception of radiation, and are the source of modern society's fear of the phenomenon. But those fears would only deepen as scientists explored a further use of radiation — the nuclear bomb.

> Rontgen noticed that exposure to radiation made him sleepy, and he speculated that he discovered a new sleep aid.

### 3. Research for the nuclear bomb was an experiment on a massive scale with unknown consequences. 

By the late 1930s, the public was well aware of the dangers of radiation and its potential to kill. But despite the risks, the campaign to build an atomic bomb was seen by advocates as a way to end World War II with a minimum of casualties.

Although the United States was the first country to build a bomb, German scientists were close behind. US scientists worked in secret laboratories in Washington State and New Mexico, and the United States recruited some of the most talented scientists of the time for the project. Many of these men, including Albert Einstein, had fled from Germany's Nazi regime.

In Germany, the Nazis established an atomic program at the University of Leipzig. It was led by Werner Heisenberg, a Nobel Prize winner and one of the few world-class nuclear scientists who had not fled the Nazi regime.

But despite the talent of those involved, both groups still suffered fatal accidents in the race for a nuclear bomb.

Two US scientists, Harry Daghlian and Louis Alexander Slotin, prepared radioactive material for the bombs yet didn't take enough safety precautions. As a result, they were exposed to fatal amounts of radiation and were the only US casualties of the atomic bomb campaign.

In the other camp, the facilities in Leipzig caught fire while German scientists attempted to induce a nuclear reaction during a test, killing several scientists.

In the end, the United States was the first to build an atomic bomb, yet officials really weren't prepared for the amount of destruction this new weapon would cause.

When the bomb was dropped in Japan, scientists were shocked that everything flammable within 12 miles of the drop's epicenter caught fire. While scientists knew how much damage the released radiation would cause, they underestimated the bomb's intense thermal energy.

Some 83,000 Japanese would die, many from radiation-induced cancer some decades later.

During World War II, the destructive potential of nuclear power was demonstrated to its fullest. How would this potential be contained and managed afterward?

### 4. Accidents with nuclear bombs during military exercises happen more often than you might think. 

Looking at the devastation caused in Japan, one would think that today's nuclear bombs are closely watched, carefully handled and highly protected. But this is not exactly the case.

In fact, there have been many accidents involving nuclear bombs. Some 65 cases of incidents with nuclear weapons have been documented to date, and that's only bombs belonging to the United States.

For example, airplanes have accidently dropped nuclear weapons they were carrying, or airplanes with nuclear weapons on board have crashed. For a long time the US military tried to keep such events secret, but several incidents eventually were leaked to the public.

Many of these accidents were caused by simple human error, which then triggered a terrible chain of events. Some egregious accidents include a military crew dropping a bomb on a civilian's home, injuring but not killing a family, and a B-52 bomber crash in Southern Greenland.

The particular design of a nuclear bomb intentionally prevents a full-scale nuclear reaction. Indeed, design engineers specifically considered the potential danger of an unintended nuclear detonation before the first US bomb was dropped on Japan in 1945.

For a nuclear bomb to cause a nuclear reaction, several different mechanisms have to be activated. Some of these are switched off during exercises or during transportation. But while many accidents thankfully don't result in a nuclear explosion, these events still leave behind radioactive material that must be cleaned.

These stories show that while mistakes with nuclear bombs are unavoidable, robust design can help to avoid catastrophe. Unfortunately, many nuclear power plants don't follow similar guidelines.

> It took eight months for the US military to clean up the radioactive waste left behind after a plane crash in Greenland.

### 5. The disaster in Chernobyl can be attributed to bad nuclear power plant design and human error. 

In the 1970s, engineers were confident that nuclear power plants could avoid large-scale accidents. Unfortunately, the Chernobyl disaster in April 1986 proved them wrong.

This catastrophic event took place during a routine safety operation. At the time, no technician with an advanced understanding of nuclear physics was on duty. What's more, no staff knew how to fully implement the required safety measures in case of an accident.

Poor decisions by physicist Anatoly Dyatlov led his staff to commit more errors. He instructed personnel to override the official emergency procedures, which essentially made the meltdown worse.

Politics also played a part in the disaster. Soviet engineers worked in isolation from the rest of the world. The Soviet Union was secretive about its technological developments; there was no knowledge sharing with experts from the West, for example.

Consequently, the Chernobyl plant itself was built using outdated technology and posed many problems. Its graphite moderated light-water reactor model, for example, had already been scrapped by Western countries.

To make matters worse, the Soviets didn't immediately release news of the reactor meltdown. Until 1986, the government kept any radioactive leaks as classified information, fearing that such details would cause citizens to panic.

Not only were the immediate areas around Chernobyl affected, but radioactive matter was spread by wind to neighboring European countries, increasing the levels of radiation in the soil and contaminating vegetation.

Yet after Chernobyl, many years passed without a nuclear disaster on a similar scale. Experts perhaps hoped that this was to be the last large nuclear catastrophe of its kind.

> The full impact of the Chernobyl disaster over the next 100 years is difficult to measure, but experts predict as many as 4,000 cases of cancer as a result of radiation exposure.

### 6. The Fukushima nuclear plant was poised for disaster, given its location in an earthquake zone. 

Even though Japan's Pacific coast is a hotbed of major earthquakes, officials still deemed this area an appropriate location for two nuclear power plants: Fukushima 1 and Fukushima 2.

The disaster in 2011 was inevitable for a number of reasons.

The Japanese government, as well as the Tokyo Electric Power Company, ignored scientists' repeated warnings of a potentially serious earthquake in the region. Also, the company failed to improve a coastal wall to protect the plants in case of a tsunami.

While the breakwater along the coast could protect the plants from waves up to 18.7 feet high, the tsunami that followed the 9.0 earthquake were 46 feet high! What's more, a series of aftershocks impeded efforts to reduce the scale of the meltdown.

One of the main reasons that Fukushima 1 failed was age. The plant was built in the 1970s and lacked the latest technological upgrades. Fukushima 2, just seven miles away, was designed in the 1980s; its superior technology helped to prevent a meltdown there. Fukushima 2's generators are air-cooled, for example, while Fukushima 1's generators are seawater-cooled.

Human error, like with Chernobyl, also played a large role at Fukushima 1. An operator took it upon himself to override a computerized safety mechanism that monitored the cooling of the power plant. It's likely that the meltdown could have been avoided had this automatic trigger not been interrupted.

One seemingly minor decision caused a chain reaction which exacerbated an already crisis situation, making disaster inevitable.

> For every person who dies as a result of generating electricity by nuclear means, 4,000 people die as a result of fine particle pollution from burning coal.

### 7. It will take time and money to envision a new future for nuclear power plant design. 

In the 1950s, Admiral Hyman Rickover of the US Navy designed a small-scale power plant to power nuclear submarines. His design would in time come to dominate the civil nuclear industry.

Why was this the case? First, Rickover's design was both efficient and robust.

The challenge of designing within the restrictive conditions of a submarine inspired the admiral to come up with new ideas. His plant generated more fuel than it burned, for one; it also didn't use liquid sodium, which could leak dangerously.

Rickover's successful submarine design inspired civil industry to adopt a similar design. Almost all nuclear power plants operating today are based on some variation of the Rickover plant.

There have been other promising designs, yet market conditions and a lack of investment have prevented these ideas from being realized.

For example, the _direct contact reactor_ (DCR) was a promising alternative to the Rickover design. The goal of the DCR was to be highly efficient; its fuel was molten plutonium. By the 1960s, a mockup was constructed in Los Alamos. Yet at that time, the government's budget for experimental reactors was pulled and the project was never realized.

Engineers also experimented with a _molten salt reactor_. This reactor used thorium, a radioactive metal that both exists in large quantities in nature and isn't fissile — that is, it won't split and start a nuclear reaction on its own. This makes thorium more stable than plutonium or uranium, for example.

Crucially, radioactive waste from thorium is no longer dangerous after 300 years, whereas the waste from uranium fission remains dangerous for some 30,000 years!

The project ran for four years but was then abandoned as the Rickover reactor had by then dominated civil markets.

In sum, it would be unfortunate to stop exploring alternative methods to better produce nuclear energy. The potential dangers of nuclear power will always be with us. Yet nuclear disasters will become less and less probable as technology improves, and engineers learn from past mistakes.

### 8. Final summary 

The key message in this book:

**Nuclear power can be volatile and potentially dangerous. Yet from a history of nuclear mistakes and fatal consequences, we can learn and thus reduce the probability of future disasters. The world's engineers continue to seek ways of harnessing the wonders of nuclear power to benefit the economy and society without damaging the environment or our way of life.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Fukushima_** **by David Lochbaum, Edwin Lyman, Susan Q. Stranahan and the Union of Concerned Scientists**

_Fukushima_ (2014) tells the story of how one of the biggest tsunamis in Japan's history combined with government neglect, corporate interest and propaganda to create the most serious nuclear disaster since Chernobyl. The book was written by the Union of Concerned Scientists, a nonprofit that brings together science and political advocacy.
---

### James Mahaffey

Nuclear engineer James Mahaffey was a senior research scientist at the Georgia Tech Research Institute and has worked for the US Defence Department's Defense Nuclear Agency, the Air Force Air Logistics Center and the National Ground Intelligence Center. He is also the author of _Atomic Awakening: A New Look at the History and Future of Nuclear Power._

