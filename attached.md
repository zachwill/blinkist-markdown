---
id: 53f3489a3162620008a50000
slug: attached-en
published_date: 2014-08-19T00:00:00.000+00:00
author: Amir Levine and Rachel S. F. Heller
title: Attached
subtitle: The New Science of Adult Attachment and How It Can Help You Find – and Keep – Love
main_color: D12A35
text_color: 9E2028
---

# Attached

_The New Science of Adult Attachment and How It Can Help You Find – and Keep – Love_

**Amir Levine and Rachel S. F. Heller**

_Attached_ (2010) is all about how to make your relationships work. This book offers you valuable insight into the science of adult attachment and how to use this insight in everyday life, whether you're in a committed relationship or are still looking for love. It also provides tips and tricks on how to find the perfect partner and reveals why some people just aren't compatible.

---
### 1. What’s in it for me? Discover new ways of thinking about your relationships. 

Have you ever wondered why you feel so uncomfortable when your relationship becomes too intimate? Or why you have so many insecurities and anxieties when it comes to relationships?

_Attached_ provides answers to those very questions and more.

In the following blinks, you'll discover the scientific foundations of romantic relationships and their roots in our evolutionary past. In addition, you'll find how different people actually seek out different kinds of relationships based on their _attachment_ _style_.

By understanding the reasons why people behave in certain ways in a relationship, you'll be able to better manage the difficulties in your own relationship and grasp what you truly need for a successful partnership.

In these blinks, you'll also learn

  * how being in an unsatisfying relationship can actually make you physically ill;

  * what to do when your date still hasn't "made a move"; and

  * how much control you actually have over the quality of your relationships.

### 2. Everybody needs attachment to live a healthy and happy life. 

We're all familiar with that creeping feeling when we know our partner won't be around for a while: _distress_. What is it that causes this feeling?

The distress of being away from our partner is caused by our feelings of _attachment_ for one another.

But what exactly is attachment?

Attachment is a strong bond that two people share with each other, a bond which lasts over time and also forms a special need to keep in contact with one another. It could take the form of a mother-child relationship, or the form of a romantic relationship between two adults.

Interestingly, forming these bonds comes with many benefits. Having a close personal relationship with someone gives us a secure emotional foundation, which then helps us remain calm even in strenuous situations.

Being able to hold your partner's hand during times of stress, for example, gives you support, knowing that this special someone will help you through the trouble. This connection makes even the most distressing situations feel much less frightening.

This was verified in a study that aimed to find out more about the healing effects of attachment by placing female participants in a stressful situation, but giving some participants the opportunity to hold their partner's hand.

Researchers found that when a participant was able to hold her partner's hand, her _hypothalamus_, that is the part of the brain that makes us feel emotional pressure, was less active than when she had to endure the stress alone.

Conversely, those who lack the bonds created by attachment are at greater risk of unhappiness and serious health risks.

In fact, when we find ourselves in an unhappy relationship, we don't only suffer emotionally but also physically.

For example, if you aren't satisfied with your marriage, your partner's presence will actually raise your blood pressure due to the discomfort you feel when you're around them. As long as you are in their proximity, your blood pressure will remain high, which can eventually lead to serious medical problems, such as heart disease.

### 3. The basic need for attachment is in our genes and is influenced by our life experiences. 

If you have ever been in a relationship, then you know that, from time to time, our partners drive us crazy! Yet, in spite of this, we still love them and long for that love to be requited.

But why? Where does attachment come from?

It all boils down to our genes: evolution has programmed us to build relationships. In fact, even right out of the womb, we yearn for attachment. This isn't because we've learned that relationships are a pleasant thing to have, but rather because the desire for attachment is embedded in our genes. 

This desire stretches far back into our collective past, where our ancestors learned that relying on each other was the only way to survive the hardships of life and the constant threat of predators.

Only those who were able to find dependable partners were able to produce offspring, to whom they then passed on the genes that helped them to successfully find reliable mates.

What's more, this genetic predisposition to attachment can be further influenced by our life experiences.

The way parents treat their children actually molds their attitudes toward relationships in general.

For example, when parents form a secure relationship with their child, in which they provide their child with adequate attention and respond appropriately to their child's needs — for example, knowing the difference between their child's behavior when he wants affection or when he's hungry — then it is likely that this person will have secure relationships as an adult as well.

Yet the inability of a child to develop stable relationships, however, is not necessarily the fault of the child's parents.

Our attachments can also be influenced by relationships that we experience later in life. Having a difficult romantic relationship as an adult can likewise influence your attitude toward relationships, and cause later relationships to suffer as a result.

So now we know where our need for relationships comes from. The following blinks will show how our differing needs in relationships inform our behavior.

### 4. People with an anxious attachment style need intimacy and tend to fret about their relationships. 

As you've learned, we all have a strong need for attachment. However, we don't all experience these attachments in the same way: we differ in our attitudes toward relationships and how we think they should function.

These differences help us categorize the different types of people whose attachment style predicts their behavior in romantic situations. Let's start with the first: an _anxious_ attachment style.

Anxious people are preoccupied with their relationships and always worry that their partner doesn't love them enough.

Imagine, for example, that you call your partner while she is at work because you miss her. Yet, rather than getting to hear her voice on the phone, you hear the dial tone as she rejects the call.

You get nervous and worry, thinking that she might not love you anymore! Half an hour later, after you've become sick with concern, she calls you back to apologize, explaining that she was in a meeting and couldn't answer her phone.

Does this feel like the way you or your partner might behave? If so, then one of you has an anxious attachment style, characterized by your need to always have access to your partner and the tendency for you to take their behavior personally.

If this sounds like your style, then you should find someone who has the emotional resources available to provide you with emotional security. Were you to date someone who couldn't handle your needs, then your relationship would provide you no comfort!

You should look for someone that is willing and able to fulfill your need for constant availability. Dating someone with a secure attachment style, that is someone who is comfortable with intimacy (more on this in a later blink), will make you feel deeply loved and understood. They will recognize your needs, and thus always be willing to talk with you about your (many) worries.

### 5. Having an avoidant attachment style means a person craves independence in a relationship. 

Imagine being in a relationship in which you feel totally confined: your partner seems too needy, and you can't understand why it's so important to completely depend on any one person.

Does this sound familiar to you? If so, then you probably have an _avoidant_ attachment style.

An _avoidant_ person is one who tries to maintain their autonomy while in a relationship. People with an avoidant attachment style try to not get too close to anyone, fearing that doing so might mean a loss of independence.

They struggle to recognize the feelings of others, and as such are quite difficult to maintain relationships with.

However, as we've learned, we _all_ have a need for attachment. Avoidants have this need as well, however, they simply express it differently.

When it comes to relationships, they often look for "the one." Because they have difficulties creating compromises based on the personalities and needs of their partner, they create a fixed idea in their head of how their perfect partner should be.

For example, avoidants often find little things about their partners that irritate them, perhaps the way they slurp their coffee or their high-pitched sneeze. By identifying those little "problems," they have an excuse to keep an emotional distance, in their wait for "the one."

To make their relationships work, people with an avoidant attachment style should do their best to think of their partner in a more positive light.

For example, when a relationship runs into problems, avoidants are quick to blame others, which in turn puts a lot of pressure on their partner.

However, this can be avoided, as long as avoidants are willing to put in the practice. They simply need to think of their partner in a positive way, rather than as the source for their woes.

The best way to do this is to first look for the source of conflict _within_ _yourself_, taking great care to see the problem from your partner's perspective.

### 6. People with a secure attachment style “go with the flow,” and are also the most common. 

When it comes to relationships, most of us don't want to settle: we want to find that perfect partner, the person who gives us space when we feel confined and cuddles up next to us when we feel lonely.

Believe it or not, this type of person isn't just the stuff of fairytales! These people are real, and they exhibit a _secure_ attachment style.

The secure attachment style is one in which the person feels comfortable with closeness and intimacy. 

Whereas the other attachment styles struggle with too much or too little dependence, secure people don't really fret it. They can read between the lines and understand their partner's needs without worrying too much (like an anxious person) or being indifferent (like an avoidant person).

In fact, being with a secure partner is the best predictor for a happy relationship.

Judging by the benefits of a secure attachment style, it makes sense that there are hardly any problems in relationships between two secure people. However, it is also worth noting that even just one secure partner can improve the quality of a relationship by mollifying any difficulties that result from another attachment style. 

Imagine, for example, sitting at your desk, knowing that you still have heaps of work to finish but not having enough time left to complete it. As you become more and more worried, your partner enters the room.

He asks you how you're doing, but instead of explaining your problem to him, you tell him off for disturbing your work.

However, because your partner has a secure attachment style, he won't be offended by your harsh words but instead understands that you're simply not in the mood to talk. He reads your signals and easily finds ways to support you, for example, by cheering you on, saying: "You can do it!"

Now you know all about the different attachment styles and how they work. These final blinks will show you how to use this information to create a happy relationship.

### 7. Effective communication will ensure that you find the best partner and keep that partner. 

No two relationships are ever the same. Each comes with its own strengths and weaknesses. So what can you do to guarantee happiness in _your_ relationship?

The answer is simple, whatever your attachment style: effective communication. Indeed, directly expressing your needs and concerns will make it much easier to figure out whether your potential partner is right for you.

Unfortunately, when you start dating someone you don't really know, it can be difficult to interpret the messages they are sending you.

For example, let's say you've been on several dates with your potential partner, and they still haven't "made the move." Many people worry in this situation, wondering whether they should just wait for their love interest to do something or if they should take the initiative.

But rather than worry, it can be a good idea to simply bring up the topic. While you might be afraid that doing so will make you appear too needy, this is a great and effective way to find out about their understanding of your relationship.

Even if they don't react in the way you'd hoped, at least now everyone's expectations are on the table. If you've found someone you _really_ like, it's best to spell out your needs and worries to your partner right away if you want your relationship to last.

However, effective communication doesn't necessarily mean bringing up every single problem or concern right away. Rather, it's about finding a way that ensures your worries won't build up.

The secret to effective communication is to be specific about your problem without assigning blame to your partner.

For example, instead of saying that "It's pathetic that you're still talking about your ex-girlfriend," you could try something like, "Talking about your ex makes me feel sad and insecure. I need to know that you are happy with _our_ relationship."

By articulating your worries without assigning blame, you can feel safe that they'll better understand your perspective.

> _"In a true partnership, both partners view it as their responsibility to ensure the other's emotional well-being."_

### 8. Knowing how to deal with conflicts will make a relationship work. 

Unfortunately, conflict is part of _any_ relationship. But would you have thought that fighting with your partner can actually make you happier?

For a happy relationship, it's not so important how _much_ you disagree with your partner, but _how_ you do it.

There are a few important things to keep in mind whenever you're having a conflict with your partner.

First, be sure not to generalize. For example, a fight over who should do the grocery shopping should not bleed into other arguments, like whose turn it is to do the dishes. Restricting the scope of your argument to a specific topic will make sure it won't escalate.

Second, you need to pay attention to your partner's well-being. One way to do this is to find good compromises on contentious issues so that everyone will be happy with the situation.

Imagine, for example, that you and your partner are discussing your vacation plans. You want to grab your towel and head to the seaside again, but your partner would rather do something other than sitting on the beach all day.

However, it's possible for you to find a happy compromise! For example, you might find a destination where you can have your relaxing beach day as well as something more active, like sightseeing tours.

Moreover, these conflicts can actually be a way for a couple to deepen their attachment bond. Bringing up a problem that has been bothering you for a while will open your partner's eyes to your experiences.

While you might think it's obvious what's troubling you, for example that he never takes out the trash, your partner can't read your mind!

After talking about why you're upset, you'll find yourself relieved that you finally were able to broach the topic, and your partner will be glad that he doesn't have to guess at how to meet your needs and wishes anymore.

In the end, everyone benefits from solving the conflict.

> _"It's always best to assume the best in conflict situation. Expecting the worst...often acts as a self-fulfilling prophecy."_

### 9. Don’t waste your time with a partner who cannot fulfill your needs in a relationship. 

Think back on all those romantic Hollywood films you've seen. It's always the same story: the boy gets the girl and they live happily ever after, no matter how different their needs are. The message is clear: "true love" has the power to make any relationship work.

But this simply isn't so.

A person with an anxious attachment style should not date someone with an avoidant attachment style.

It's correct that sharing true love with your partner is the most important requirement for a long-lasting relationship. However, we also know with certainty that not everyone has the same needs.

When someone with a great need for closeness falls in love with someone with an avoidant attachment style, their relationship will very likely resemble a roller-coaster ride.

Their differing attitudes toward intimacy will put their entire relationship under severe pressure, as these attitudes affect the big questions, like whether to get married or have children. Anxious people often want these, while avoidant people may not.

If you can't agree on these major life decisions, then it will be nearly impossible to find a compromise that would allow your relationship to survive.

The ultimate secret to a happy relationship lies in finding a partner who is able to meet your needs.

Regardless of which attachment style you have, there is one simple way of making sure that you will find someone with whom you can have a happy relationship. As we've seen, it all boils down to effective communication.

Knowing what your partner needs in a relationship, and letting her know what _you_ need, are the most important things!

However, if you are not truly happy, then don't waste time trying to work out the things that cannot be worked out. Sometimes it's best to accept that your wants and needs are incompatible with your partner's, and look for someone with expectations more in line with your own.

_That's_ how to have a happy relationship!

### 10. Final summary 

The key message in this book:

**When** **it** **comes** **to** **a** **long-lasting** **relationship,** **don't** **leave** **anything** **to** **chance!** **You** **need** **to** **figure** **out** **your** **own** **attachment** **style** **and** **then** **try** **to** **find** **a** **partner** **who** **is** **able** **to** **give** **you** **what** **you** **want** **and** **need.**

Actionable advice:

**If** **you** **have** **a** **problem** **with** **your** **partner,** **tell** **them!**

It's no good for anyone if you let a small problem fester over time. This can be an enormous strain on your relationship! Instead, tell your partner when you are unhappy without letting other issues obscure your discussion. This way you can both work on a solution.

**Try** **to** **figure** **out** **your** **own** **attachment** **style.**

If you want to have a healthy and happy relationship, you'll first need to have a better understanding of yourself and your needs. Knowing your attachment style can help you not only to better understand your needs but also better communicate those needs and find a partner who is able to fulfill them.

**Suggested** **further** **reading:** **_Why_** **_We_** **_Love_**

Helen Fisher's _Why_ _We_ _Love_ is not only a report on her latest astonishing research but also a sensitive description of the infinite facets of romantic love. It is a scientifically grounded examination of love that reveals how, why and who we love.
---

### Amir Levine and Rachel S. F. Heller

Dr. Amir Levine and Rachel S. F. Heller are close friends who decided to write a book to help others make better decisions in their romantic lives.

Dr. Levine grew up in Israel and is a psychiatrist and neuroscientist. He is also the principal investigator for a research project sponsored by the National Institutes of Health. Heller graduated from Columbia University with a master's degree in social-organizational psychology, and has worked for a number of management consulting firms, including PriceWaterhouseCoopers, KPMG Consulting and Towers Perrin.

