---
id: 5740bccd2edd1b00032e6bfb
slug: attitude-reflects-leadership-en
published_date: 2016-05-27T00:00:00.000+00:00
author: Leo Hamblin
title: Attitude Reflects Leadership
subtitle: None
main_color: 9B272A
text_color: 822123
---

# Attitude Reflects Leadership

_None_

**Leo Hamblin**

_Attitude Reflects Leadership_ (2015) exposes why the modern world of work is rife with bad bosses. These blinks illuminate the elements of leadership that drive top performance, from knowing the difference between managing and leading to fostering the right attitude in your team. You'll learn that while exceptional leadership is rare, it is something you can learn.

---
### 1. What’s in it for me? Lead by inspiring your employees. 

We've all had bosses who, far from encouraging you to do your work, prefer to do it all themselves. When you did get a chance to show your stuff, they'd hang over your shoulder, correcting every little thing you did.

Chances are, this sort of "helicopter" bossing made you miserable, and what's more, you certainly didn't perform to the best of your abilities.

The truth is, being a great boss — a great leader ‒ isn't about what you do, it's about how you feel, or the attitude you have. The best leaders are those who know how to inspire employees, encouraging greatness in the every day and finding ways to unlock hidden potential in everyone.

The secret is, great leaders aren't born, they're made. Are you ready to become a great leader? These blinks will show you how.

In these blinks, you'll also discover

  * why there is a big difference between leading and managing;

  * why great leaders should offer rewards and not incentives; and

  * why nothing beats a positive attitude.

### 2. Pessimistic bosses bring employees down while inspiring leaders breathe life into a team. 

Ever had a terrible boss? They're the ones who make getting out of bed each day difficult, who fill your commute to work with dread.

Though such bosses hold senior positions in a company, they certainly aren't _leaders_.

Even the most talented individuals can't realize a grand vision without support. Leaders of brands, businesses and organizations need employees, of course — but more importantly, they need people just as determined to succeed as they are.

Employees will only feel inspired by work if their leader is inspired, too.

Imagine your team works around the clock to produce innovative software to improve your product. It's been a long, hard slog, and morale is waning.

What would a good leader do? A good leader would show a positive attitude, no matter what. By reminding employees of their importance as part of a project and even planning a celebratory get-together once the work is done successfully, you can give your team the extra boost they might need.

But if you continue to bemoan obstacles and tear your hair out over how long a project is taking, you'll undermine your competence as a leader. And soon enough, your team will feel and perform poorly, too.

There are no benefits to being a demotivating leader. So why do so many pessimistic bosses have the upper hand in organizations?

One Gallup poll revealed that bad bosses were the top reason for a person quitting a job. Even the employees who do stay onboard often feel disengaged when bad leaders are involved. Another poll saw 70 percent of US workers express feelings of disconnection in their roles at work.

Unfortunately, inspiring leaders are hard to come by in the modern workplace.

> _"Leadership is one of my favorite topics to study and discuss. Take a moment to imagine the world without it. No one would be following because no one would be leading. There would be no pioneers and no discoveries."_

### 3. Management and leadership are not to be confused. Managers measure; leaders inspire. 

Today, the labels "manager" and "leader" are used interchangeably. But management and leadership mean two completely different things in the modern workplace.

Management and _measurement_ go hand in hand. The job of a manager is to control a team and ensure that work is progressing as it should. For a manager to know _when_ and _how_ she should make a change, she needs to _measure_ her team's progress.

For instance, a manager might feel her team's productivity is declining. A poor manager might look at when staff arrives to work, or when and for how long they take lunch breaks. These details could reveal the symptoms of a problem, but won't uncover its real cause.

A good manager knows to measure the salaries of employees against competing companies, and the quality of a company's computers and facilities against the latest models and trends.

With this in mind, a manager can pinpoint the places where making a change would benefit employees the most.

But if a manager is there to track a team's progress, what role does a leader play?

Simply put, leaders are responsible for _inspiring_ employees to achieve more. It goes without saying that good managers and great leaders are a fantastic combination.

So where can we find them?

Often, a boss or a manager was formerly just a great employee. But doing a job well and knowing how to motivate others to do a job well are entirely different things. Managing is a skill in itself and requires the right experience, training and attitude.

Similarly, great employees don't become great leaders simply by receiving a fancy new job title. Being appointed a "chief director" or "head" of something doesn't mean you naturally have the leadership skills to match.

Like management, leadership is a skill — perhaps even an art!

> One thing every manager must measure is return on investment. This will always show you whether what you are doing is working — or not.

### 4. Great leaders stick to their guns when they make a decision. 

Leadership is not something you _have_ but a trait you must demonstrate, over and over again.

In other words, a leader has to take action. And a great leader never hesitates to take the best course of action without compromise.

Naturally, it's vital that you make employees feel that their suggestions and ideas matter. But you must also remember that the final decision lies with you, the leader. Take new ideas on board by all means, but stick to your guns when you know your plan is the correct one.

Say your company is designing a new hair dryer. Based on research, you know customers will respond to a hair dryer with a sleeker design and a more powerful motor. And yet, a team member insists that including an epilator in the design is the best way forward. What should you do?

You _could_ hold meeting after meeting in an attempt to come up with a compromise design. But your best option is to stick to the original plan. As a leader, if you're confident that your idea will work, don't let detractors get in your way. The product's success will speak for itself.

But what if your team seems to fight your every move? If things are getting out of hand, the problem might not be with them but with you. Failure to commit to past decisions can make it difficult for employees to take you seriously in the future.

Let's imagine you've just been named CEO. You're thrilled, and the first thing you do is gush about your plans to inspire your team.

But time passes and after all your promises, you've yet to put anything into action. You begin to notice that your employees lack the motivation to do even the most basic of jobs.

Think about it: if you don't follow through on your decisions, why should they? But if you demonstrate unwavering commitment, you'll see that your team will support you every step of the way.

> _"You can't follow this book or any other book. You would be a follower … Now it's time to lead and take action."_

### 5. Lead your team well by listening, being honest and encouraging employees, even when they fail. 

Let's take a closer look at what it means to make your team feel that their ideas matter to the company.

Great leaders constantly listen. Whether helpful suggestions, strange ideas or grumpy complaints, there's nothing a leader should ignore.

A leader can only make good decisions once she has all the information she needs.

Sometimes the best ideas come from the most unlikely places. In fact, your staff might come up with many great ideas. Unfortunately, you aren't always able to give your team credit for it. This is one of the harsh realities of leadership.

Sometimes, leaders must portray employee ideas as their own. Sound exploitative? It is.

But if you give your staff the respect, care and yes, compensation, that they deserve, they'll accept this compromise as just the way work is. Be honest with your employees and they'll respect you, too.

A leader worth respecting is also one who knows how to deliver criticism well. In this case, brutal honesty isn't always the best policy. Blaming employees for mistakes and pressuring people to perform will only demoralize them. This is the last thing you want.

So how can you address problems productively?

If you ensure your company places a focus on overall improvement and skills development, as a leader you'll be better equipped to address a situation when a valued employee does the wrong thing.

Rather than criticizing, ensure that the employee and the company at large learn from the mistake. The chances are that being treated with respect will inspire your employee to perform better than ever afterward.

> _"Your employees will go to war for you, and business is war."_

### 6. A leader’s attitude determines the attitude of the team she creates. 

Leadership plays an important role in your staff's lives. And identifying leadership potential is crucial when you're hiring new employees, too!

To find the individuals that will give your business the support it needs, you'll need to keep your eyes peeled for the right attitude in a potential employee.

But first, you'll need to embody the right attitude yourself as you recruit. By thinking of staff as replaceable and around for only a limited time, you'll end up with employees who are interested only in a paycheck and nothing else.

But if you want individuals that will personally invest in the work they do, then you're looking for employees you'd never dream of replacing.

The questions you ask in interviews are key. Experiment with questions that reveal how an applicant would engage in their tasks. Present them with hypothetical problems that require a bit more thinking, and you'll gain insight into how they approach work.

Soon enough, you'll know how to spot the staff with an attitude to match yours.

The right attitude is also central to how you reward employees. Let's be clear: incentives and rewards are not the same things.

While incentives make a promise to encourage better performance, rewards are only given in recognition of great performance. An incentive bonus for finishing work before a deadline is a bribe to motivate employees who should be motivated by your leadership alone. A reward for completing a task faster is something employees should earn, and something you can even surprise them with.

With these points in mind, you're equipped to take your leadership skills to the next level. It's now time to show that you've got the attitude of a great leader. Your staff will notice and appreciate it, too.

> _"If management has the belief everyone is replaceable, then they know they're going to replace them at some point, so they might as well hire someone who is beautiful to look at."_

### 7. Final summary 

The key message in this book:

**Leadership is too often confused with management. Bad leaders are often pessimistic and don't know how to effectively inspire employees. Firm decision making, great listening skills, open communication and above all, a positive attitude, are what make a leader exceptional.**

**Actionable advice:**

**It's all right to be selfish!**

Let's face it: business is no cakewalk! Leading a team means you have to make tough calls, but this is part of the job. If a decision you're making falls between an action that will help your business and one that will appease certain employees, know that the business should win, every time. If you've explained your decision as best you can and still can't secure everyone's support, sometimes you have to put yourself first and forge ahead with your plan.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Be a Positive Leader_** **by Jane E. Dutton and Gretchen M. Spreitzer**

_How_ _to_ _Be_ _a_ _Positive_ _Leader_ examines cutting-edge research from the field of positive organizational behavior, in which companies aim to foster both a positive attitude to work and high performance among employees. The research is complemented with vivid examples from real organizations.
---

### Leo Hamblin

Leo Hamblin is a coach and founder of Hamblin Coaching.

