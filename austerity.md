---
id: 595a2344b238e10007cd5077
slug: austerity-en
published_date: 2017-07-04T00:00:00.000+00:00
author: Mark Blyth
title: Austerity
subtitle: The History of a Dangerous Idea
main_color: CAB36D
text_color: 7D6F43
---

# Austerity

_The History of a Dangerous Idea_

**Mark Blyth**

_Austerity_ (2013) cuts through the confusion behind our recent financial crises and reveals what really happens when economists call for a policy of austerity to be implemented. This is when budgets are cut, public funding is slashed and working-class families suffer so that banks can be saved and continue to make billions. Find out what's really going on and who's really being protected when your country gets pushed into austerity.

---
### 1. What’s in it for me? Find out why austerity programs for countries in crisis are bound to fail. 

In the aftermath of the global financial crisis of 2007/2008, a debt crisis hit the eurozone, a crisis that was supposedly caused by massive government overspending in countries like Greece and Spain. Newspapers reported on Greece's ballooning public administration and told outrageous stories of mass fraud — for instance, that legions of people had pretended to be blind so that they could receive benefits.

And, though painful, the solution looked simple enough: these profligate countries needed to whittle down their administrative apparatus, cut benefits where they could, raise taxes and so forth, until they were in a position to pay back their debts.

But, actually, this advice is wrong on several levels. It misplaces the blame, penalizes the wrong people and — laughably enough — doesn't even work.

These blinks explain why.

They also discuss

  * why a "swap derivative" is a dangerous thing;

  * why it's sometimes best for a country to let its banks go bust; and

  * the link between Hitler and austerity.

### 2. Austerity weakens a country’s economy and it hits the lower classes hardest. 

The recent financial crisis, which began in the United States back in 2007, hit Europe hard. Many nations are still struggling to recover, and there are some who think they know the best policy for getting the European economy back on track. This policy is called austerity.

Austerity, however, isn't a one-size-fits-all solution. Though often effective when a single institution finds itself in crisis, it's a dangerous policy when applied to a nation.

Austerity is the process of reducing spending by issuing budget cuts, the goal being to boost an economy's competitiveness and inspire confidence in its businesses.

But we can already see evidence that austerity on a national level fails and weakens the economy, especially when it's applied to multiple countries at once. Spain, Portugal, Greece, Italy and Ireland are all cases in point.

To understand why this is so harmful, imagine that your family has gone over budget. In such a situation, it makes sense to cut back on spending until things return to normal. But imagine that every household in the state did this at the same time. Money would cease flowing into local businesses, and stores and employers — burdened with expenses of their own — would begin to suffer.

To weather the tough times, businesses would need to take out loans and lines of credit, creating even more debt — debt that they'd be unable to pay off until the households in the state begin spending money again.

As you can see, this situation benefits no one — and it's exactly the situation that austerity applied on a national level gives rise to: citizens spend less, the economy shrinks and debt grows.

Furthermore, it's the members of the lower classes who get hit the hardest.

When budgets are cut, it invariably hurts welfare programs, unemployment benefits and the people who rely on social programs to get by.

In times of austerity, the average struggling worker always suffers more than the banker who caused the crisis.

### 3. There’s a long history showing that austerity fails to heal the economy. 

It's commonsensical to tighten your budgetary belt and reduce spending when times are tough; however, when a national budget is slashed across the board, the results are never good.

A quick review of the historical record shows that austerity has routinely failed to restore economic growth.

In the 1920s, during Warren Harding's presidency, the United States experimented with austerity, and the result was the Great Depression.

In 1931, President Herbert Hoover, desperate to balance the budget, tried to increase taxes. But it was too late for such a quick-fix approach, and the country was plunged further into recession. Only when President Franklin Roosevelt abandoned austerity in 1933 and increased spending under the New Deal did the economy finally recover.

Germany was also subjected to a painful period of austerity after World War I and the Treaty of Versailles. This period impoverished and disillusioned large swathes of the population, setting the foundation for Hitler's rise to power.

Sweden, France and Japan also experimented with austerity in the 1930s, and their economies also suffered. Growth only began once they'd reversed the cuts and started making investments.

Some economists will point to successful cases of austerity, but these are highly dubious.

Australia, Denmark and Ireland are often held as positive examples, but with Denmark, one can argue that their budget cuts were always made during an economic boom, not during a recession.

As for Ireland, the country was also experiencing economic growth during the 1980s, helped in part by the devaluation of the Irish pound in 1986, which boosted the demand for the country's exports.

Finally, in Australia's case, it should be noted that there are no records to suggest that any cuts were made to unemployment benefits or capital taxes, as supporters of austerity would have you believe.

### 4. The recent economic crisis was the result of the US banking system and mortgage defaults. 

Between 2007 and 2008, a massive financial crisis swept across the globe, and when it came time to cast blame, many pointed the finger at government overspending.

But the truth is that the US banking system, not government policy, caused the crash.

The banks had become heavily involved in "repo markets" that allowed them to borrow and make short-term loans with one another using housing-mortgage securities.

In order to save some money, the banks would exchange these securities for cash and then buy them back at a very low interest rate. This worked fine for a while, and it would have continued to work had homeowners not begun defaulting on their mortgages, making the securities utterly worthless.

But that's exactly what started happening in 2007. Mortgage defaults skyrocketed, real-estate prices plummeted and banks began losing money left and right.

When the media picked up the story, and the word "crisis" entered the public sphere, people panicked and couldn't withdraw money from their banks fast enough. This only made matters worse. Since the banks didn't have enough cash on hand to cover the withdrawals they borrowed money from other banks, which spread the problem even further.

This proliferation of problems was sparked by two things: specific types of securities known as _CDOs_ and the investment practice known as _credit default swaps_.

CDOs stands for _collateralized debt obligations_, which is a fancy name for what is basically a bunch of seemingly random mortgage securities bundled up together.

Now, these bundles came with an opportunity for investors to use _credit default swaps_ (CDS), which was a way for them to bet against the bank, and receive a massive payout if the owner defaulted on the mortgage.

The mortgage market was considered so safe that insurance companies like American International Group (AIG) sold a staggering number of CDSs to banks around the world — so many that they ended up being unable to pay for them when the defaults began.

And when it became clear that AIG couldn't pay on the CDOs, the investors became desperate and tried to get rid of them at drastically lowered prices. And since no one in the United States would touch them, investors and banks began looking to Europe, where the crisis likewise took hold.

### 5. EU nations were already struggling, but they made their situation worse by bailing out the banks. 

In 2008, most European nations were in no shape to take the blow of a banking crisis. 

This was especially the case for Portugal, Italy, Ireland, Greece and Spain, which the author refers to as the PIIGS nations due to their poor economic policies.

All of the PIIGS nations had been struggling with economic growth. Portugal and Italy faced the problems of low birth rates and an increasingly elderly population; Ireland and Spain were dealing with a bursting property bubble.

Furthermore, industries in the PIIGS nations weren't growing as much as they'd hoped, due to products from core European countries like Germany being in higher demand. So these industries, and the nations themselves, were steadily building up debt.

Adding to their problems was the additional credit they'd received for adopting the euro. Not only were they unable to pay back this credit; the adoption of the euro also reduced their ability to handle crises on their own. Since they could no longer control inflation and exchange rates, they were unable to employ the usual shock-absorbing tactics: devaluing the currency and increasing exports.

Alas, instead of coming up with creative solutions to the crisis, it was decided to bail the banks out, a move that doomed the PIIGS nations to permanent austerity.

The logic behind the bailout was that the banks were "too big to fail," and that, if they went bankrupt, the fallout would be catastrophic.

But the banks were also "too big to bail" since their value had become dangerously high.

For example, in 2008, the combined value of the three biggest French banks was just over three hundred percent of the French gross domestic product! And the same thing had happened all across the EU banking system.

To pay for these bailouts, the PIIGS nations essentially put themselves in a position of permanent austerity, since they would no longer have the money to invest in anything else.

> _"Bailing led to debt. Debt led to crisis. Crisis led to austerity."_

### 6. Ireland has struggled under austerity, but Iceland has thrived after letting its banks go bust. 

Many economists have held up Ireland as a model of how Greece might recover through austerity. But a closer look reveals that Ireland isn't in enviable economic health.

After Ireland spent €70 billion on bailing out its banks, it embarked on an austerity program that cut public-sector salaries by nearly 20 percent and drastically reduced welfare and other social benefits.

Meanwhile, its growth was nearly non-existent. In 2011, growth was below 1 percent, and most of that came from foreign companies taking advantage of reduced taxes and generating wealth that, in the end, doesn't stay in the country.

At the same time, by mid-2012, the unemployment rate had risen to almost 15 percent — a sharp increase from 2007's unemployment rate, which stood at 4.5 percent.

And after three years of austerity, the debt-to-GDP ratio climbed from 32 percent in 2007 to 108 percent in 2013.

To see what might have happened had the nations not bailed out the banks, let's look to Iceland, which was actually in a worse position than Ireland in 2007.

Around that time, the assets-to-GDP ratio of Iceland's banks was nearly 1,000 percent, putting them in a similar "too big to fail" scenario. But Iceland did allow them to "fail" and the results were far better than you might expect.

In many respects, Iceland did exactly what Ireland didn't do: It provided additional support to social benefit programs like welfare, raised taxes on the top earners, lowered taxes on low-to-middle income families, devalued its currency and created capital controls.

Contrary to the catastrophic predictions of the International Monetary Fund (IMF), these actions resulted in Iceland's GDP only dropping 6.5 percent in 2009 and 3.5 percent in 2010. And starting in 2011 a 3 percent growth returned, and continued in 2012, placing Iceland as a top grower on the Organisation for Economic Co-operation and Development list. 

While austerity might be in the best interest of the banks, since it usually entails their being saved from bankruptcy, it isn't in the best interest of the nation or its citizens.

### 7. There are viable alternatives to austerity that help the majority at the expense of the few. 

Rather than looking to countries like Ireland, and singing the praises of austerity's healing abilities, we should look to what real signs of recovery can tell us.

As we saw in the case of Iceland, the first step to helping a nation in the grips of a banking crisis is to let the banks go bust and loosen their control over the economy.

Otherwise, the process of saving the banks drains the state of the resources needed to help protect its people during recovery.

So instead of bailing out the troubled banks, Iceland used 20 percent of its GDP to fund new banks, which took up its domestic operations. It was costly, but far less so than a bailout.

Nations should take a serious look at their banks and ask themselves, "What service are we really protecting with a bailout?" The banking industry doesn't provide an indispensable service like food or energy. What it does is make obscene amounts of money serving as a go-between on transactions between borrowers and lenders.

But letting the banks go bust is only part of the solution; it's also helpful to raise taxes on the wealthy to quickly begin reducing the debt.

In a 2012 study, German economists showed just how restorative a one-time-only tax on the wealthiest 8 percent of its population could be. By collecting 10 percent of net wealth over €250,000, a country could increase GDP revenue by 9 percent!

Similarly, American economists Peter Diamond and Emmanuel Saez suggest that if the United States raised the income tax on the wealthiest 1 percent of the population from 22.4 percent to 43.5 percent, the country would increase its GDP revenue by 3 percent.

If a nation decided to act in the interest of the majority, rather than its few wealthiest citizens, there would be no shortage of creative solutions.

One thing is certain: there's bound to be another financial crisis down the road, and hopefully we can learn from our mistakes and make better choices when that time comes.

### 8. Final summary 

The key message in this book:

**Though the international banking crisis was caused by the private sector, it was the public sector that had to pay when bailouts resulted in cuts to public services and social benefits. As a result, struggling families were hit especially hard while the wealthiest were protected. This is what happens when austerity is used to restore economic stability. But there are alternatives, and if more people understand what happened ten years ago, maybe the next crisis will be handled differently.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Full Catastrophe_** **by James Angelos**

_The Full Catastrophe_ (2015) takes you beyond the headlines on the Greek debt crisis to discover how citizens in Greece and beyond have survived it. Through real-life interviews with people in mountain villages, tourist resorts and in the capital city of Athens, the author lays bare the effects of government budget cuts, austerity policies and endemic corruption.
---

### Mark Blyth

Mark Blyth is a professor at Brown University, where he teaches Political Economy and is known for his criticism of austerity politics. He is also the author of _Great Transformations: Economic Ideas and Institutional Change In the 20th Century_.

