---
id: 54e3d547303764000a9c0000
slug: bad-pharma-en
published_date: 2015-02-19T00:00:00.000+00:00
author: Ben Goldacre
title: Bad Pharma
subtitle: How Drug Companies Mislead Doctors and Harm Patients
main_color: EE8F9F
text_color: 87515A
---

# Bad Pharma

_How Drug Companies Mislead Doctors and Harm Patients_

**Ben Goldacre**

_Bad Pharma_ reveals the shocking truths of the pharmaceutical industry, describing in great detail the ways in which it deceives doctors and patients, and even circumvents standard ethical medical practices, all in pursuit of profits.

---
### 1. What’s in it for me? Learn how pharmaceutical companies harm medicine. 

It would be nice to believe that medicine trials live up to proper scientific practice. It would similarly be great if we could trust that the pills we put in our mouths were effective. But unfortunately, such reputable practices cannot be taken for granted.

In these blinks you'll discover how pharmaceutical companies harm medicine by conducting trials that make drugs look significantly better than they really are. Due to deliberate methodological flaws and the withholding of data, medicine has become as corrupt as any other profit-driven industry.

You'll also learn that

  * pharmaceutical companies often fail to publish unflattering trial results;

  * drug companies persuade doctors to prescribe their medicine; and

  * real-life data can easily be analyzed for decades to come.

### 2. Industry-sponsored studies are likely to produce results that flatter the sponsor’s drug. 

So how do we know how effective drugs are? Often we look to medical trials to determine whether certain medicines are worth taking. Unfortunately, however, not all trials are created equally.

Trials that are funded by pharmaceutical companies are more likely to produce results skewed in their favor.

This bias becomes startlingly evident when you look at the results of Harvard and Toronto researchers' 2010 survey of over five hundred medicinal trials. The survey asked two main questions: Did the results favor the test drug? Were they funded by the industry?

Of the industry-funded studies, 85 percent yielded positive results, compared to only 50 percent of the government-funded trials.

This sort of huge discrepancy indicates that studies sponsored by the pharmaceutical industry produce positive results that cater to industry interests.

In addition, industry-funded trials are often riddled with hidden methodological flaws that make drugs appear better than they are. 

Let's take a look at the multinational pharmaceutical corporation Pfizer, for example, which published several trials on a drug called Pregabalin designed to treat pain. During these trials, researchers measured participants' pain at regular intervals.

However, as is often the case in medical trials, some participants exited the study because they were experiencing the drug's negative side effects.

In these cases, Pfizer decided to take the last measurement of pain severity — while participants were still on the drug — and simply carry that over into the remaining measurements. This technique is deceptive because it disregards the negative side effects.

When the analysis was eventually redone — this time properly — Pfizer's "last observation" method was unsurprisingly revealed to have overestimated the improvement in pain by about 25 percent.

### 3. The pharmaceutical industry creates positive test results by withholding trial data. 

As we saw in the last blink, if medical trials are funded by the industry, it's a safe bet that the results will be positive. But how exactly do industry-sponsored trials nearly always manage to achieve positive results?

One way is to simply not publish medical trials that produce unflattering results. We can see this quite evidently in an investigation published in 2010, whereby researchers compiled the results of all the trials that had ever been conducted on the drug reboxetine, an antidepressant by Pfizer.

Seven trials had been conducted comparing reboxetine against a placebo, i.e., an inert sugar pill. Only _one_, conducted in 254 patients, yielded a positive result, and not coincidentally, it was _this_ trial that was published in an academic journal.

But what about the other six trials? They had almost ten times as many patients, and each of these trials showed reboxetine to be no better than a sugar pill. Yet, these trials were never published. This withholding of trial data fundamentally distorts medical evidence. By preventing you from seeing half of the relevant data, it becomes exceedingly easy to convince you of something false.

This process of withholding negative trial results from publication is called _publication bias_, and it's no small matter. In medicine, research serves a concrete purpose: to save lives, cure illness, and ease suffering.

Withholding trial results puts people's lives at risk.

> _"Every time we fail to publish a piece of research we expose real, living people to unnecessary, avoidable suffering."_

### 4. The commercialization of trials research raises serious ethical problems. 

We like to think of scientific research as being objective and free from commercial bias. However, business and research are only becoming more intertwined, as an increasing number of medical trials are conducted by clinical research organizations (CROs) on behalf of the pharmaceutical industry.

Trials conducted by CROs bring down the cost of testing drugs, since CROs outsource to poorer countries, but this commercialization of research raises some ethical dilemmas.

According to the _Declaration of Helsinki_, an important set of ethical principles regarding human experimentation, trial participants are supposed to come from a population that could reasonably benefit from their results. In practice, this means that a costly AIDS drug, for instance, shouldn't be tested on people in the impoverished parts of the world, where the drug would be out of financial reach for participants.

Yet, in many cases, especially in Africa, drugs are tested on people who couldn't possibly benefit from the drugs' development — simply because it's cheaper.

Moreover, the commercialization and globalization of research trials calls into question the data's reputability.

For example, if a high blood pressure drug is tested on a pool of typical patients in India, those patients would exhibit different underlying characteristics, lifestyle habits and medical access than if the same study was conducted in America or Europe, causing a significant rift in test results.

Would the findings really be applicable to Western patients? In fact, medical trials from developing countries have produced positive results while those conducted elsewhere showed no benefit.

If clinical research is conducted economically rather than ethically, we simply can't be sure of the data.

### 5. The regulation mechanisms that are supposed to guarantee drug safety are largely ineffective. 

Medications don't go straight from the lab to the stores. First, pharmaceutical companies have to send their drugs to _regulators_ for approval.

Regulators are tasked with monitoring a drug's safety, but unfortunately, the regulatory environment is beset with problems.

For starters, government regulators tend to be paid poorly. However, someone with such inside knowledge on regulation is valued by pharmaceutical companies. As a result, regulators are often tempted to leave the regulatory body in favor of the private sector.

Even Europe's top medicinal regulator at the time, Thomas Lonngren, announced in December 2010 that he was leaving his governmental post to work as a private consultant for the pharmaceutical industry.

But people change jobs all the time. What's the big deal?

Well, what happens when governmental regulators, in the course of their everyday duties, are thinking about their future at a drug company? Could they not help a drug company overcome hurdles and approve a drug in order to leave a good impression? 

This conflict of interest makes the existing regulatory mechanisms ineffective. In fact, the standards have been set so low that regulators only require pharmaceutical companies to show that their drug is better than nothing.

When a new drug is released, doctors and patients are interested in one practical question: is it better than the best currently available option? However, the answer is often no.

Of the 197 new drugs approved by the American Federal Drug Administration between 2000 and 2010, only 70 percent boasted data that suggested they were better than other available treatments.

The same problem exists in the EU: to get a license to market your drug, you only have to show that it's better than nothing. In fact, only half the drugs approved between 1995 and 2005 had been studied in comparison with other treatments at the time they were allowed on the market.

> _"Drugs are approved on weak evidence, showing no benefit over existing treatments, and sometimes no benefit at all."_

### 6. There are various ways to conduct “bad trials” in medicine. 

Medical trials aren't perfect. Sometimes they're conducted in unrepresentative patients, or sometimes they're too brief. They can measure the wrong data, be incorrectly analyzed and sometimes the results simply disappear entirely.

On occasion, trial results are outright fraudulent.

One famous case of medical research fraud comes from American anaesthetist Dr. Scott Reuben, who simply never conducted the twenty or more clinical trials on pain he had published over the previous decade. Instead, he made up the data! 

Trials can also fail because they're conducted on unrepresentative "ideal" patients.

In the real world, patients are complicated: they have diverse medical problems and histories or take lots of different medicines, all of which interact with each other in unpredictable ways.

Despite these complications, clinical trials sometimes study drugs in unrepresentative, unrealistically ideal patients, who are both young and also have a clear, singular diagnosis.

Freed from real-life complications, trials in an ideal population might exaggerate the benefits of a treatment or find benefits where there are none, thus making the results completely irrelevant to real-world application. And yet, these kinds of trials are actually routine in pharmaceutical research.

Finally, drugs are often tested against ineffective competitors. One thing that is sure to make a new treatment look promising is testing it against another that doesn't work very well.

For example, a study compared the drugs paroxetine and amitryptiline: paroxetine is a newer antidepressant, and is largely free from side effects such as drowsiness; amitryptiline, in contrast, is a very old drug, known to make people sleepy. In clinical practice, patients take it only at night to account for this side effect.

In this trial, however, both medicines were administered in _the morning_. Amitryptline patients thus reported daytime drowsiness, which made paroxetine look better by comparison.

> _"It feels as if some people, perhaps, view research as a game, where the idea is to get away with as much as you can."_

### 7. Pharmaceutical companies spend billions trying to influence treatment decisions. 

With all the available medicines out there today, how do doctors decide which to prescribe? They don't have the time to read every single peer-reviewed article relevant to their work. This presents a great opportunity for the pharmaceutical industry, which spends billions on marketing (about twice as much as it does on research and development) to convince doctors that their medicine is the real deal.

One way to do this is to appeal to doctors directly. In fact, the overwhelming majority of the industry's promotional budget goes to marketing directed at doctors. About half of that is spent on _drug reps_, people who visit doctors in their offices and try to convince them in person of the superiority of their company's drugs over the competition.

Drug reps are often young and attractive, bringing both gifts and the promise of a mutually beneficial relationship with the drug company they represent.

Drug companies also print advertisements in academic journals. While ads for drugs are supposed to contain "objective" and "unambiguous" information, a study from 2010 found that _less than half_ of the drug ads in leading medical journals referenced a high-quality trial which supported their claims.

Drug companies also fund patient groups, who need money to lobby and support their members more effectively. These groups also can benefit from specialist expertise. Drug companies are happy to oblige, as this gives them an opportunity to establish a positive image for their brand, as direct advertising to patients is prohibited.

Finally, drug companies pay celebrities to mention drugs during interviews. For example, Kathleen Turner, the mom from _Serial Mom_, drops references in interviews to arthritis drugs, and is paid to do so by Wyeth and Amgen. 

Altogether, this forms a powerful marketing mechanism through which tens of billions of dollars are spent each year — as much as $60 billion in the US alone.

By now you've seen what's wrong with advertising and testing within the pharmaceutical industry. Our final blink looks at what we can do to change it.

> Fact: Seventeen of 29 studies found that doctors who see drug reps are more likely to prescribe the promoted drug.

### 8. Randomized trials in routine medical practice could counter many of the serious problems with trials. 

Because the pharmaceutical industry puts so little effort into running effective trials, we really have no idea which treatments are actually the best for many of the most important diseases.

One way to counteract this problem is by establishing randomized trials in routine medical practice.

Let's say you wanted to compare two statins, drugs used to lower cholesterol in the blood, in order to see which is best at preventing heart attacks.

Normally, when a general practitioner sees a patient and prescribes a statin, she simply clicks the "prescribe" button in her software that links to a page where she chooses the drug and prints out a prescription. 

Here, an extra page could be added: "Wait! We don't yet know which of these two statins is best. Rather than choosing one, just press here to randomly assign one or the other to your patient."

The doctor's office would then need to follow up with the patient on the drug's effectiveness.

Using this type of system, the medical community could create a meaningful, anonymized database of health records, which, eventually, could give us a better idea of which medicines are _actually_ best.

Many of the major problems with clinical trials can be addressed with these kinds of simple trials and electronic health records:

  1. Collecting data on real-world patients eliminates the issue of unrepresentative "ideal patients";

  2. It could potentially bring down the cost of trials since they are built into existing medical usage and interaction with doctors;

  3. And, also, these kinds of real-world drug trials could allow for ongoing monitoring and studying of drug effectiveness, adding another dimension of reputability to their results — unlike brief trials that can't possibly represent ongoing drug usage.

### 9. Final summary 

The key message in this book:

**The pharmaceutical industry puts little effort into developing effective drugs, and instead spends billions on marketing. If we want to live in a world in which we are prescribed medicine because it's the best solution to our ailment, then we'll have to seriously rework the way medical trials work.**

**Suggested** **further** **reading: _Cracked_** ** __****by James Davies**

_Cracked_ gives a clear and detailed overview of the current crisis in psychiatric science: malfunctioning scientific standards and the powerful influence of pharmaceutical companies have caused the overdiagnosis and overmedication of people all over the world.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ben Goldacre

Ben Goldacre is a medical doctor, writer and research fellow at the London School of Hygiene and Tropical Medicine. In addition, he wrote _Bad Science_ (also available in blinks) and is best known for his column Bad Science in _The Guardian._

