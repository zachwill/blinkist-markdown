---
id: 5385f4f461623800073a0100
slug: bad-science-en
published_date: 2014-05-27T14:50:48.000+00:00
author: Ben Goldacre
title: Bad Science
subtitle: None
main_color: F0D92A
text_color: 706514
---

# Bad Science

_None_

**Ben Goldacre**

We often swallow scientific-sounding language used in advertisements or on the news without any further thought. But if we analyze it a little, we often find that it's merely pseudoscience. _Bad_ _Science_ shows us that this bogus science can lead to serious misunderstandings, injustice and even death.

---
### 1. What’s in it for me? Learn how to separate real science from cheap imitations. 

Science wields immense power, and its applications are pervasive: it makes our lives easier, improves our health, and extends our life. Yet unfortunately there are many who use science to promote their own agenda — one which bypasses evidence in favor of profit.

_Bad_ _Science_ reveals common tricks used by big players such as pharmaceutical companies, homeopaths and nutritionists to hoodwink the public into buying treatments they don't need.

It also shows how the media concoct misleading stories in the name of science to create a sensation, or worse, scare people into believing the tenuous results from fringe scientists.

These blinks give us the analytical tools necessary to determine whether a piece of science, such as a medical trial, has been designed well enough for us to place our confidence in its results and the researcher's conclusions.

Using these tools, we can lift the veil of superficial, phoney science, which may have nice superscript numbers and references and graphs, but is ultimately just empty talk.

In addition, these blinks will show you:

  * How people portrayed as "experts" in the media are often nothing of the sort.

  * How researchers can bias the results of medical trials in their favor.

  * Why the South African government withheld antiretroviral drugs from people suffering from HIV.

  * Why one mother went to jail because of one prosecutor's flawed and unchallenged statistical reasoning.

### 2. We buy into the junk science myths used to sell health and beauty products. 

Each and every day we are bombarded by advertisements for products that promise to improve our lives in some way. All too often these products are explained in complex and seemingly indisputable scientific language.

We don't have to do much digging to find examples of this. Just think of the health and beauty industry with their claims of how their products "purify" us and make us look more attractive.

For example, there is a detox footbath called _Aqua_ _Detox_, which purports to cleanse your body of "toxins," evidenced by the bath water turning brown after the product is used.

And then there's an advertisement for a face cream made from "specially treated salmon roe DNA," which assumes that salmon DNA somehow nourishes and revitalizes your skin.

Surely the brown water left in the detox bath is the toxins our feet leave behind, right? Wrong. These grand scientific claims are based on absolutely no evidence whatsoever!

Upon closer inspection, the brown color of the water has nothing to do with your feet, but is merely the rust coming from the iron electrodes when the device is switched on.

And that salmon skin cream? DNA is simply too large to be absorbed by skin but even if it wasn't, fish DNA — i.e., _alien_ _DNA_ — isn't beneficial for your cells, and certainly not beneficial for _you_. If you really want to reap the benefits of nutrient-rich salmon, you have to actually eat and digest certain parts of it, not rub it on your skin.

So how do these companies get away with it? In essence, they rely on our misunderstanding of science: we tend to think that it's just too complicated for us. Better to leave that "science stuff" to the people in lab coats, right?

We therefore easily accept the scientific "facts" presented to us without questioning them, leaving advertisers an irresistible opportunity to exploit our ignorance and trust in order to sell their wares.

### 3. Many nutritionists’ claims are fraudulent and misrepresent evidence. 

How many multivitamins do you take every day in the hope that they will make you smarter or healthier, or even prevent some terrible disease? Many people have added multivitamins to their morning routine, but how much actual scientific evidence demonstrates their value?

Not much. In fact, the assertions made by nutritionists often lack scientific rigor, and therefore don't stand up to scrutiny.

One common theme found in nutritional claims is _overextrapolation_, when a finding based on a small-scale trial, perhaps in a laboratory, is deemed applicable on a larger scale — say, for all humans. For example, the academic nutritionist Patrick Holford, lauded by the press as an "expert," once claimed that vitamin C is more effective at fighting HIV than AZT, the prescribable anti-HIV drug.

How did he come to this conclusion? He cited a single paper that showed that when some vitamin C was injected into HIV-infected cells in a petri dish, it reduced the levels of HIV replication. The study didn't even mention AZT, nor had there been human trials!

What's more, false claims like this one can actually cause treatment to be withheld from sick people. For example, vitamin salesman Matthias Rath helped influence the government of South Africa — where a quarter of the population has HIV — to withhold anti-HIV drugs and promote multivitamins, including his own.

He claimed they would reduce the risk of AIDS by 50 percent, and that they were safer and more effective than any anti-HIV drug, basing his claims on a Harvard study involving a thousand HIV-infected Tanzanian women.

The study showed that low-cost generic vitamins — or a better diet — could be used to push back the start of anti-HIV drug regimens in _some_ patients, but Rath distorted the results, claiming that vitamins were the "superior" cure and even adding that anti-HIV drugs worsened immune deficiencies.

These lies had a human cost: one study estimated that if the South African government had opted to give out anti-HIV drugs during this period instead, they could have prevented 343,000 deaths.

> _"In general, you don't absorb things through your skin, because its purpose is to be relatively impermeable. When you sit in a bath of baked beans for charity you do not get fat, nor do you start farting."_

### 4. Some pharmaceutical companies abuse the way science is conducted and reported. 

How reliable are drug trials? You probably think (and hope!) that the results are fair and accurate. Unfortunately, they aren't always as honest as we'd like them to be.

This is partly because drug trials are extremely costly, and often financed by the drug companies themselves. In order to bring a drug to market, it must surmount a number of obstacles: first, it must pass the initial trials to determine safety, then trials to determine efficacy, and finally a larger-scale trial where it is measured against a placebo or comparable treatment. The total cost is close to a staggering $500 million on average.

Because public entities simply can't afford to pay that much, 90 percent of clinical drug trials are conducted or commissioned by pharmaceutical companies. These companies therefore have a massive — and some would say unfair — influence over what is researched, and how it is understood and reported.

One outcome is that results of positive trials are published more often than those of negative trials. This is called _publication_ _bias_, and unfavorable results are often buried.

One example of this is drug companies hiding data that showed that their antidepressant SSRIs were no more effective than a placebo. In fact, there have even been cases in which companies publish the results of a positive trial more than once, to make it seem as if there were corroborating trials supporting their results! For example, one anesthetist compared trial data for a nausea drug only to find the same, slightly reworded results in several studies and journals, thus inflating the drug's apparent efficacy.

And even when drugs are finally brought to market, drug companies can bury risks or side effects. For example, even though SSRIs are known to cause _anorgasmia_ — i.e., the inability to reach an orgasm — researchers simply didn't mention it on the list of side effects.

Even if you read the label carefully, you cannot always know how a drug might seriously affect your life.

Now that we have explored some of the ways in which we can be tricked by bad science, the following blinks will help you to determine what qualifies as "good science."

### 5. Placebos illuminate the way we heal ourselves. 

No one understands why a placebo — a pill containing only sugar — can be used to treat a large range of conditions, such as tooth pain and angina. But somehow they _do_ work.

In part, it is the "theater" of placebos that is crucial to their ability to help our bodies heal. For instance, details such as packaging, price and color all affect our expectations — and thus the outcome — of the treatment itself.

Studies have shown, for example, that a dose of four placebo pills performs better than two, and an injection performs better than a pill. Furthermore, pink placebo pills can make you feel motivated while blue ones relax you.

In another study on the treatment of narrowed arteries, a "sciencey-looking" laser catheter that administered no actual treatment was almost as effective as the real treatment!

The secret to placebo treatments is that the patient _feels_ _like_ they are being treated, and that's all it actually takes to affect results. Because of this phenomenon, real treatments can be compared against placebos to determine their efficacy.

For example, many believe that homeopathy treatments (which are basically just water) work, because they seem to have cured illnesses. Yet when we compare them to placebos in blind, randomized trials, they work no better than the placebo itself.

But although placebos have many benefits, they are also surrounded by ethical issues.

In essence, a placebo is a sham treatment, often little more than a sugar pill. When sick patients are given one in a trial, it could be the case that they miss out on vital treatment and actually get worse.

For example, between 1932 and 1972, the US Public Health Service left 399 poor black men with syphilis under the impression that they were receiving treatment — without actually providing any — just to see what would happen. Sadly, they only got worse, and the government didn't even apologize until 1997.

### 6. Flaws in the design of scientific studies can massively affect the results. 

We tend to place a lot of trust in the results of medical trials. Why shouldn't we? As it turns out, some evidence suggests that they might not always be fair tests.

For example, some trials don't report how they randomize which participants are filtered into the treatment group or the non-treatment group.

In every medical trial there are two groups of patients with a specific disorder: one receives the treatment and the other doesn't. This approach allows researchers to meaningfully test the effectiveness of the drug.

But not all participants are equal! For example, some participants are known as _heartsinks_ : these patients constantly complain about unspecific symptoms that never improve, and are more likely to drop out or not respond to treatment.

If the next available place in the trial is for the treatment group, the experimenter, wanting a positive outcome for her experiment, might decide that this _heartsink_ shouldn't participate in the trial — with the result that they test their treatment only on those with a greater chance for recovery.

This has serious consequences: unclear randomization in patient selection can overstate the efficacy of the treatment by 30 percent or more.

For example, one study of homeopathic treatment for muscle soreness in 42 women showed positive results. However, because the study didn't describe its method of randomization, we can't be certain that the trial was fair.

Furthermore, patients, doctors or experimenters sometimes know which patient is getting the treatment and which is getting the placebo. In order to be effective, tests need to have something called _blinding_ — i.e., the tester shouldn't know in which group an individual patient belongs.

Testers can influence their results through conscious or subconscious communication with patients, just as knowledge of which drugs you are taking can influence the way your body responds to treatment.

For example, trials conducted without proper blinding showed acupuncture to be incredibly beneficial, while other tests with proper blinding proved that the benefit of acupuncture was in fact "statistically insignificant." The difference isn't trivial!

### 7. Statistics can be powerful scientific tools, but they must be used responsibly. 

Nothing is certain. That's why we use _statistics_ — the analysis of numbers and data — to determine something's probability, such as the effectiveness of a treatment or the likelihood that certain crimes are going to happen. When used correctly, they can be incredibly useful.

For example, statistics can be used in _meta-analysis_, in which the results from many similar studies with few patients are combined into a larger, and therefore more robust and accurate test of whether a treatment is effective.

For example, between 1972 and 1981, seven trials were conducted to test whether steroids reduced the rate of infant mortality in premature births, and each showed no strong evidence to support their hypothesis.

However, in 1989 the results were combined and analyzed through meta-analysis, which found very strong evidence that steroids _did_ in fact reduce the risk of infant mortality in premature births!

So wherein lies the discrepancy? The patterns in small studies are sometimes only visible when the data is aggregated.

Yet for all their worth, statistics can be misunderstood and misused, leading to bogus evidence and even injustice.

For example, a solicitor named Sally Clark had two babies who both died suddenly at different times. She was then charged with their murder and sent to jail because of the statistical improbability that two babies in the same family could die of Sudden Infant Death Syndrome (SIDS).

In fact, one key piece of evidence against her was the prosecutor's calculation that there was only a "one in 73 million" chance that both deaths could be attributed to SIDS. However, this analysis overlooked environmental and genetic factors, which suggest that if one child dies from SIDS, the chances of another SIDS-related death in the family are more likely.

Not only that, but the chance that Clark committed double murder was actually twice as unlikely as both her children dying of SIDS, which, when considered with the rest of the evidence, meant that statistics themselves were simply not enough to convict her.

### 8. We are prone to delusions and biases about the information we come across. 

Can you remember the first time you drank coffee? Probably not. But can you remember your first kiss? I bet you can! So why is it easier to remember one event, but not the other?

This is because we have been conditioned to pick up and remember unusual events and forget everything else. Hence the way we remember and process information is necessarily biased, because we don't treat all information equally.

But it's not only our memory that influences our biases; other factors can lead to mistakes in our thinking and decision making.

One such flaw in our thinking is our tendency to invent relationships between events where none actually exist.

For example, consider the fact that improvements in medical conditions can often be attributed not to a treatment, but to the natural progression of an illness, or _regression_ _to_ _the_ _mean_. So if you had an illness and your symptoms were at their peak, and then went to, say, a homeopath for treatment, you would soon be getting better.

We naturally assume that the _visit_ caused the improvement, but in reality our treatment simply coincided with the natural return from extreme illness to normal health.

In addition, we are prejudiced by our prior beliefs and those of the "herd." This was made explicit in one US study that brought together and examined people who supported the death penalty and those who opposed it. In the experiment, one half of each group was given a piece of evidence that supported their belief and a piece of evidence that challenged it, while the other half in each group received contrary evidence.

Interestingly, every single group identified flaws in the research methods for the evidence that challenged their pre-existing beliefs, but dutifully ignored the flaws in the research that supported their view! What's more, this experiment didn't just study irrational buffoons: the results suggest that we _all_ behave this way.

Now that you have the knowledge to understand what qualifies as good science, the last few blinks will explore the ways in which science is misused in the media and the drastic repercussions.

### 9. News stories about science research are dumbed down or sensational, leading to public misunderstanding of science. 

You've probably seen "scientific" stories in the newspaper about things like the "happiest day of the year." The media is full of fluffy stories like these which are passed off as the real deal, while stories about genuine scientific inquiry hardly ever make it into the news at all. Why is this?

The media's problem is that the vast majority of today's scientific advances simply happen too gradually to be newsworthy. There was a time, however, when this wasn't the case: between 1935 and 1975, groundbreaking science was being churned out constantly.

One example of this was the tools used to fight polio. Scientists discovered that polio paralyzes our muscles, thus making it impossible to breathe. To combat this, mechanical ventilation and intensive care were created, both of which saved countless lives.

However, the golden age of scientific discovery is past, and scientific advances are now piecemeal. For example, refinements in esoteric surgical methods and a better understanding of drugs contribute to a longer lifespan, but these kinds of minor changes are slow, and not really all that exciting — certainly not interesting enough for newspaper editors, who prefer to report bold, shocking headlines.

Consequently, many newspaper "science" stories are trivial, wacky and simply published to grab your attention.

For example, you might remember one whimsical essay by a political theorist on the way humans will evolve in 1,000 years' time, which was circulated in many newspapers. These stories claimed that by the year 3000 everyone would be coffee-colored and we would have split into two separate species — one tall, intelligent and healthy, the other short, stupid and unhealthy.

These bold claims fly in the face of evolutionary theory, but did that stop the papers from publishing them? No. In fact, it was later discovered that the story was paid for by Bravo, a men's TV channel, to celebrate its 21st year of operation. Although the story created the air of a scientific investigation, in reality it was merely a publicity stunt.

### 10. The media are compelled to scare people with science stories that lack evidence. 

It's an unfortunate fact that we are drawn to headlines that play on our fears — for example, that we'll all die from some horrible disease or be vaporized by an asteroid impact. The media love to scare us with terrifying stories — luckily, they are often total junk.

Stories that appear to be based on scientific evidence are often neither challenged nor properly investigated. For example, in 2005, newspapers reported that the "superbug" MRSA had been detected in various UK hospitals. Microbiologists from the hospitals, however, found no such bacterium.

In fact, the "expert" that had peddled the story was discovered to have little knowledge of microbiology, and even sold anti-MRSA products from his garden shed. In spite of this lack of credibility, the media were happy to report and promote his views.

One reason why non-experts often get exposure is that the media prefers people with media prowess, even if they aren't the best scientists, which causes false stories to be spread.

For example, British newspapers reported for _nearly_ _a_ _decade_ on research that linked a measles, mumps and rubella (MMR) vaccine to autism in children, largely because of a single anecdotal paper led by the surgeon Andrew Wakefield.

All the large-scale, scientifically rigorous trials showed that MMR was safe. Unfortunately, as is often the case, the academics weren't very good at communicating with the media.

Instead of reporting actual science, the newspapers employed _generalists_, or non-scientists, to write stories to accompany a crusade of emotional parents and patients battling against the political and corporate establishment.

In addition to the nonexistent link between the MMR vaccine and autism, Wakefield also had conflicts of interest and consequently suppressed data that didn't fit his theory. Of course, the media couldn't be bothered to look into this; as a result of their sloppy reporting, fewer people got vaccinated for MMR, and the cases of measles, mumps and rubella shot up.

### 11. Final summary 

The key message in this book:

**Much** **of** **what** **is** **communicated** **to** **us** **as** **"science"** **is** **really** **just** **pseudoscience.** **The** **media** **feeds** **us** **sensation** **packaged** **as** **science,** **big** **pharma** **does** **whatever** **it** **takes** **to** **bring** **drugs** **to** **market,** **and** **charlatans** **insist** **on** **their** **fake** **evidence** **in** **order** **to** **make** **a** **few** **bucks.** **We** **let** **it** **all** **go** **unchallenged.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ben Goldacre

Ben Goldacre is a doctor, a journalist and the author of two books. His Bad Science column in the _Guardian_ attacked alternative medicine and was the starting point for this book, which was shortlisted for the BBC Samuel Johnson Prize for nonfiction.

