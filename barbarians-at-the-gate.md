---
id: 59a2d684b238e10006a12972
slug: barbarians-at-the-gate-en
published_date: 2017-08-29T00:00:00.000+00:00
author: Bryan Burrough, John Helyar
title: Barbarians at the Gate
subtitle: The Fall of RJR Nabisco
main_color: E02D49
text_color: AD2339
---

# Barbarians at the Gate

_The Fall of RJR Nabisco_

**Bryan Burrough, John Helyar**

_Barbarians at the Gate_ (1989) tells the story of one of the largest corporate deals in US history, the leveraged buyout of RJR Nabisco. These blinks provide a gripping portrait of the extreme and extravagant behavior in corporate America during the 1980s.

---
### 1. What’s in it for me? Learn about of one of the 1980s’ most infamous business deals. 

When we think of the 1980s, the yuppie is an archetype that comes to mind for many people. These flashy big spenders, who strove at all costs to maintain their glamorous and luxurious lifestyle, have come to epitomize the excesses of that decade.

Few stories embody these traits and the general atmosphere of the 1980s as well as that of RJR Nabisco and its CEO Ross Johnson. But it's not just a story of one company — it's the story of how a once fairly benevolent business practice, the leveraged buyout, transformed into something much more sinister.

In these blinks, you'll find out

  * why leveraged buyouts became so common;

  * how Johnson rose to his prominent position; and

  * why RJR Nabisco was such an attractive target for a hostile takeover.

### 2. Wall Street’s modern-day modus operandi started out as a work-around to avoid estate taxes. 

Have you heard the term _leveraged buyout_, or _LBO_? Well, by the 1980s, the LBO had become a dirty word, synonymous with corporate greed and the unhinged nature of Wall Street. But at its inception, the LBO was simply a way to preserve family wealth.

These transactions were devised by clever lawyers who were looking for ways to help wealthy business owners skirt estate taxes and pass money onto their heirs. Not coincidentally, LBOs first came on the scene in the late 1960s, when a generation of people, some of whom had built massive business empires, were getting ready to retire.

Because of the way estate taxes function, if business owners wanted to retire and pass their companies on to their heirs, they would have had to pay huge sums of money in taxes.

Under these circumstances, they generally had three options: first, they could pass the company on to an heir and pay their taxes in full; second, they could sell the company, relinquishing control of it in the process; and finally, they could go public, putting the business — and its stock price — at the mercy of the market.

Naturally, none of these three choices were especially appealing. So, a lawyer by the name of Jerry Kohlberg developed a solution, albeit a rather slow-moving and lengthy one.

Say Mr. Big was retiring. His lawyers would found a shell company and bring in a number of investors who would take out massive loans to buy Mr. Big's company out. Mr. Big would still keep a stake in his business, thereby maintaining some control, while the investors would have gotten the target company for a much lower price than if they acquired it at the end of a bidding war with other prospective buyers.

The money for these buyouts would come from bank loans, insurance bonds and the personal funds of the investors. As a result, the investors involved only paid about ten percent of the cost, while getting 30 percent from insurance bonds and 60 percent from bank loans.

In other words, the investors acquired the target company for almost nothing, while the shell company took on a massive amount of debt, the effects of which would be mostly felt by the target company.

> When Kohlberg came up with the LBO, it was known as a "bootstrap deal."

### 3. In the 1980s, LBOs were turned into money-making machines, prompting both impassioned praise and critique. 

In 1982, a company called Gibson Greetings was sold for $80 million to an investment group that had paid just $1 million of the cost themselves. A year and half later, the firm went public and was resold, this time for $290 million.

The primary individual investor had put down $330,000 and had turned it into $66 million; in other words, the LBO craze was in full swing. It was clear to investors how profitable this tactic was and, by 1983, ten times more LBOs were taking place compared to just four years earlier.

So what prompted this frenzy?

Well, there were two main factors. First, the US Internal Revenue Code played a central role by allowing deductions of interest tax, but not dividends. As a result, companies were encouraged to go into debt and pay interest, rather than operate at a profit.

Second, the appearance of _junk bonds,_ speculative investments with a high risk of default, made it possible to raise a massive sum of money in a short period. Junk bonds can be raised in a snap, transforming LBOs from slow, ambling processes into rapid-fire, aggressive takeovers.

Naturally, this change raised eyebrows and LBOs were chastised just as vigorously as they were promoted. The proponents of this tactic said it made firms leaner and increased their value, which was a valid point since an LBO could transform a company. After all, the massive debt burden involved enabled investors to streamline the business without mercy, cutting costs wherever possible.

However, the debt burden also fueled criticism against LBOs. Government officials warned that a leveraged takeover one day meant a bankruptcy the next. Naturally, the losers in such situations were the employees of the target companies, many of whom lost their jobs.

But the original shareholders also suffered; their investment value crumbled as the company took on all the new debt.

But enough about the minute details. Now that you have some background on what an LBO is, next you'll learn about a man who was behind one of the biggest leveraged buyouts of all time.

### 4. A cutthroat businessman with an insatiable appetite for change and luxury transformed the very nature of business. 

In the 1950s, a man named Ross Johnson entered the Canadian business scene at the bottom of the corporate ladder. He would go on to become a prime example of a new breed of businessman, one that eagerly moved from firm to firm, pledging allegiance to investors rather than to a company.

Playing the game this way turned out to be quite lucrative, and Ross was no stranger to luxury — he was fundamentally driven by a love for travel, celebrities and the finer things in life. He never stopped seeking the thrill of buying new properties, eating at world-renowned restaurants and meeting stars.

In fact, he even kept a number of celebrities on his payroll to promote his company of choice at any given time. Good examples are former American Football player Frank Gifford and famed tennis star Rod Laver. To boot, Ross also had a penchant for attending celebrity golf tournaments.

But while he was soaking up the good life, Ross didn't pay much attention to the consequences of his often nefarious business strategies. For instance, he would liquidate entire departments on a mere whim and relocate whole sectors to different cities, simply in the name of a tactical advantage. To put it differently, Ross would happily sacrifice innumerable pawns to help capture a bishop.

So, how did he ascend to such power?

Well, his first position was a sales job in his home country of Canada. He worked his way up the ladder and, in 1963, got a break while working at T. Eaton, an old Canadian department store, under the management of a notorious head of personnel, Tony Peskett. Peskett pushed all his followers to keep business and management in perpetual flux, constantly shaking up companies and the market alike.

While critics of this approach said that such a tactic simply meant pursuing change for the sake of change, leaving behind a trail of chaos and destruction, Johnson personally benefited tremendously by adhering to it. He soon began making crafty moves and was able to climb the corporate hierarchy during the frequent shakeups.

### 5. Johnson’s power grew through a series of mergers. 

Johnson benefited from his relationship with Peskett, but it wasn't long before he was applying his mentor's knowledge on a much grander scale. Johnson's big chance came in 1976 when he became the CEO of the Canadian packaged goods company, Standard Brands. Finally, he was in a position where he called the shots.

In this role, Johnson was already living the high life, but his insatiable thirst for bigger and better things made him set his sights on a merger with a much larger company: Nabisco. In the end, Nabisco technically took over Standard Brands — but in practice, quite the opposite happened.

Back then, Nabisco was already responsible for some of the most popular crackers and cookies in the world, Ritz and Oreo among them. Because of their incredibly successful products, by the time the 1980s Nabisco had settled into an efficient, conservative monolith of a company. Employees never worked past 5:00 p.m. and no one feared for their jobs.

However, when Nabisco merged with Standard Brands, the upheaval practiced by Johnson soon clashed with the Nabisco's established protocols. Rather than spending time in boring meetings, mired with endless presentations, the crew from Standard Brands haphazardly shot out ideas, heckling everyone around them. They even taunted Johnson, who happily encouraged this rambunctious behavior.

Johnson's approach rapidly permeated Nabisco, and the same shift would occur when the firm later merged with RJR Reynolds, one of the world's largest tobacco companies. At the time, RJR Reynolds was one of America's most successful cigarette brands. Its founder had introduced innovations such as the first pre-rolled cigarette, as well as a famous brand of pipe tobacco, and the company dominated the market.

When the firms merged, it was because of the growth opportunities it would afford both of them. However, the flashy behavior of the Northern US company, Nabisco, immediately conflicted with the values of its Southern counterpart, RJR. For example, employees at the RJR factory had never even seen a limousine, whereas it was the preferred mode of transportation for Nabisco managers.

> Nabisco was originally known as the National Biscuit Company, stemming from a merger in 1898 that united most of the bakers in the eastern and western United States.

### 6. A young Wall Street upstart transformed the nature of LBOs. 

So, Ross Johnson was famous for shaking up businesses, but a new kid on the scene would challenge his burgeoning power. His name was Henry Kravis, a Wall Street banker who was making millions through LBOs. Ross had a distinct approach and, while most other traders were closing deals in hours or days, it could take years for him to seal an LBO.

Naturally, Kravis's higher-ups at the investment bank Bear Stearns weren't so keen on this protracted timeframe. For example, Kravis was at one point made the interim CEO of a stationery company during an LBO, prompting a furious call from his boss at Bear Stearns who wanted to know what one of his senior traders was doing at the helm of a crumbling paper company.

Kravis' vision clearly wasn't appreciated, and he soon left the firm, along with his cousin George Roberts, to team up with the originator of LBOs, Jerry Kohlberg. Together, in 1976, the trio formed the infamous private equity company, Kohlberg Kravis Roberts (KKR).

It was through this firm that Kravis played a key role in changing LBOs from innocuous tax workarounds into a powerful vehicle for corporate takeovers. However, Henry and George were much more interested in cutting huge deals than Jerry was. The cousins knew that making a $100-million deal took about as much work as cutting one for $10 billion — and they weren't about to waste their time.

By 1987, all the big players in the LBO world had a pool of investor money to use for LBOs. But the Kravis cousins set their sights on an even bigger pot. By forming a massive investment fund, the pair would show everyone that they had the most power and that the biggest deals belonged to them.

To accomplish this goal, coming off of a massive deal with the Chicago-based Beatrice Foods, KKR began seeking funding for a new war chest. The company even waived management fees of their fund for the first several years to bring in more investors. Pretty soon, they had pulled together $5.6 billion, twice as much money as the competition.

### 7. When Ross Johnson got into the LBO game, his greed resulted in calamity. 

Professionals like the Kravis cousins were largely responsible for refining LBOs — but with so much money to be made, just about everyone wanted in on the action. So, while Johnson didn't have any need to carry out an LBO, he knew that if he didn't, he would look back on the moment as an opportunity lost.

But Johnson knew that his greed would put off any serious players from touching RJR Nabisco, KKR included. As a result, his only option was to go with an outsider in the LBO world, the investment banking company Shearson.

This firm was desperate to snag their own piece of the LBO pie, and they gladly agreed to absolutely insane terms, including handing over a massive cut of the total deal to Johnson and ensuring the maintenance of certain departmental budgets and retirement packages.

Naturally, such concessions would jeopardize the austerity measures on which any solid LBO rests, such as downsizing departments to repay the debt incurred through the deal.

But this wasn't the only issue; involving several LBO novices meant that the deal was handled without the discretion and ruthlessness necessary to succeed. After all, a well-executed LBO is over before anyone realizes it has begun.

To ensure this, senior executives get together with investors and come up with a buyout package. Once everything is in order and they have arrived at a share price offer, the executives put the proposal to a vote of the board.

At this point, the deal is already so far along that the board basically has no choice but to say yes. The management calls for a yes or no vote, and if the board says no, corporate raiders are standing by to swoop in and grab the vulnerable company simply by buying enough of its shares on the stock market.

However, in the case of Johnson's deal, a lack of experience meant that information got out before it should have, resulting in disastrous consequences.

### 8. An initially low offer from Johnson caused other bidders to throw their hats into the ring. 

Johnson's LBO bid was doomed to failure from the get-go. Here's how it played out:

He and Shearson intended to offer a price of $75 a share for RJR Nabisco, $4 per share higher than the company had ever sold for. After the numbers were crunched, this came out to a total price tag of $17.6 billion, double the amount that any bank had ever loaned for a takeover. In fact, it wasn't even initially clear if there was that much money available in the world.

Naturally, when the board heard about Johnson's offer, they insisted on issuing a press statement and opening up an opportunity for other offers to come in. Frankly, the board didn't care for Johnson or his management style and were happy to get rid of him if they could.

As the news spread, offers began pouring in. A special committee was tasked with getting the best deal for shareholders, and two offers stood out:

KKR offered $94 a share, and First Boston, by way of a tax loophole, was prepared to offer anywhere from $105 to $118 a share. But before these offers were made, Shearson got word that Kravis was going to throw his hat into the ring, and changed its bid to $100 a share in an attempt to outbid him.

In the end, it didn't matter; the outlandishly large offer from First Boston prompted a second round of bidding. All involved parties were asked to place another bid and First Boston was asked to show how they could come up with the funds.

Both Shearson and Kohlberg Kravis increased their bids, coming out to between $108 and $109, while First Boston failed to secure the necessary financing. As a result, the battle came down to a dead heat between the two firms. The board was faced with a tough decision: acquiesce to the iron grip of KKR or hand victory to Johnson.

### 9. The LBO deal would be the end of Ross Johnson’s stint at Nabisco but not the end of his career. 

If you were a part of Ross Johnson's gang, you could count on leading a pretty charmed life — but other people were basically irrelevant to him. That's why, when the LBO came before the board, it was already obvious that Ross was utterly indifferent to everyone else. In the end, the fact that he viewed each person in the company as expendable would drive the board's decision against him.

It didn't help Johnson that his management deal became emblematic of corporate greed. All it took was a single _New York Times_ article, which laid bare the lucrative deal Johnson's team was aiming for, to unleash a national cascade of criticism.

Of course, the board saw this media event as just one more blow to RJR Nabisco's already tarnished reputation, and the special committee, therefore, chose KKR, which promised to put the company and its employees above all else.

Just like that, Ross Johnson was done for at RJR Nabisco, and everyone, from rural workers to board members, was happy to see him go. KKR would introduce much-needed order to the company by applying their experience of acquiring companies without dismantling them. So, while the deal wasn't the pot of gold everyone had assumed it would be, it also didn't break KKR with its sheer scale.

And that wraps up the story of Ross Johnson. His legacy is one of corporate greed in excess, but he certainly had a good time along the way and held few regrets about those years.

In fact, because of his talents as a salesman and his plain lack of ethics, he continued to have a thriving career throughout the 1980s in the United States.

While some people's fortunes were made or destroyed with the RJR Nabisco LBO deal, Ross simply entered semiretirement, laughing at the whole affair. He formed a consulting firm with an old friend and, while neither of them actually needed the money, they were happy to entertain themselves by doling out cheap advice to friends.

### 10. Final summary 

The key message in this book:

**Leveraged buyouts began as a clever workaround for wealthy business owners to get out of paying estate taxes, but soon transformed into a vehicle for hostile corporate takeovers. The story of these deals is loaded with powerful characters and driven by distinct differences in approaches that separated the winners from the losers.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _When Genius Failed_** **by Roger Lowenstein**

_When Genius Failed_ (2001) follows the rise and fall of Long-Term Capital Management, the world's largest ever investment fund. The book reveals uncomfortable truths about the nature of investment and the fragility of the models we use to assess risk.
---

### Bryan Burrough, John Helyar

Bryan Burrough and John Helyar are investigative journalists who covered the story of RJR Nabisco's buyout as it happened. Their in-depth research and extensive interviews paint a fascinating picture of a unique period in the history of Wall Street.

