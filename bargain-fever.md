---
id: 543947a93366320008000000
slug: bargain-fever-en
published_date: 2014-10-13T00:00:00.000+00:00
author: Mark Ellwood
title: Bargain Fever
subtitle: How to Shop in a Discounted World
main_color: F5D831
text_color: 756717
---

# Bargain Fever

_How to Shop in a Discounted World_

**Mark Ellwood**

_Bargain Fever_ explores the world of bargains, discounts and coupons, and explains why we'll sometimes go to extreme lengths to find a good deal. Using many illustrative examples, the author presents an account of the history of bargains, explains how they influence our shopping behavior and speculates on what discounts will look like in the future.

---
### 1. What’s in it for me? Discover the secrets behind the discount and learn to use them to your own advantage. 

When it comes to finding a bargain, today's consumer is spoiled for choice. Discounts have become so common that almost no one expects to pay full price for anything — even very popular products.

It wasn't always this way. As Mark Ellwood's deeply researched book, _Bargain Fever_, reveals, the discount is a relatively recent phenomenon that emerged in response to an imbalance in supply and demand.

But there's more behind the allure of the bargain than mere economics. As these blinks show, consumers aren't just _attracted_ to discounted goods, they're _seduced_ by them on a neurological level, chemically driven to purchase products they don't need or want.

As you read these blinks, you'll find out exactly why we find it so hard to resist buying products on sale. You'll learn how stores are able to survive when they offer discounts on everything. And you'll discover how to use the discount system to your own advantage: for instance, you'll learn how to get the latest "It bag" without plundering your bank account.

You'll also learn

  * how Coca-Cola invented the coupon, and why couponing has become a national hobby;

  * the cultural differences in bargaining and discounts;

  * about the elite, secret sales of the high-fashion industry; and

  * about the consequences of living in an all-discount world.

### 2. When you see a bargain, your brain releases the “feel good” hormone dopamine. 

At one time or another, most of us have bought a product we didn't really want or need, simply because it was on sale.

But why is it that we get so excited by the prospect of a discount?

The answer lies in the human brain: the sudden possibility of receiving a discount activates a flow of dopamine, a chemical which is considered part of the brain's reward system. The moment dopamine is released, we feel elated and excited.

However, as we learn to _expect_ a discount, that excitement begins to wane. This was demonstrated in the following experiment. A rat was dropped into a maze in which a small treat was hidden in the center, and its dopamine levels were monitored.

The first time the rat located the treat, its brain released a massive surge of dopamine. However, after it had found a treat a few times, the amount of dopamine released was practically zero.

Why? Because the rat expected to find the treat in the maze's center. In short, the element of surprise had diminished.

Our response to discounts works in a similar way: in order for discounts to prompt a dopamine-fueled rush of excitement, they have to surprise us and exceed our expectations. Say you're visiting your favorite electronics store with the intention of purchasing a new TV, and expecting to pay full price. If you see a "50 percent OFF!" sign as you enter the store, a dopamine surge — and the excitement this produces — is all but guaranteed.

Why, then, do we sometimes find it harder to resist purchasing discounted products than at other times?

If there's too much dopamine coursing through the brain, we're less able to control our behavior. An overabundance of dopamine limits the functioning of the brain region responsible for weighing the pros and cons of any action — the _dorsolateral prefrontal cortex_ (DLPFC).

This dopamine flood can be caused by stress; thus, when we're anxious, we're prone to respond unreasonably to discounts and special offers (e.g., "buy one, get one free") and to make impulsive purchases — even of products we really don't need or want.

> _"So our brains are primed hormonally to react to bargains, in thrall to every squirt of dopamine."_

### 3. The discount was invented 150 years ago, following the introduction of fixed prices in a French department store. 

The history of discounts begins in Paris in 1850, with the opening of one of France's first department stores, _Le Bon Marché_. As a promotional device to drum up new business, in 1869 the store's owner, Aristide Boucicaut, introduced the price tag — marking the first time that retail prices were ever fixed. Prior to this, retail was a business built on haggling.

Once prices were fixed, Boucicaut soon realized that he could appeal to more customers if he cut prices to help move stock at the end of the season. And so the sale was born.

As discounts were becoming an established retail convention, Harry Gordon Selfridge invented a number of discount sales gimmicks, most of which are still used by department stores today.

Selfridge's retail career began in a Chicago mall, where, in 1885, he created the first "bargain basement." Realizing that customers wanted the option of buying less expensive goods than the store offered, Selfridges decided to dedicate the entire basement to discounted merchandise — to great success.

One sales strategy he used was to avoid using words like "cheap" to describe the discounted products, instead choosing descriptors that emphasized the products' _value_. That strategy is still in use today.

Another discount gimmick that emerged around the same time was the _coupon_. In 1886, Coca-Cola attempted to boost sales by handing out free coupons which could be exchanged for a glass of Coke. Their reasoning was that if customers were given a free taste of Coke, they'd be sure to return to purchase more.

> Fact: In 2011, 40 to 45 percent of inventory in the United States was sold with some kind of promotion.

### 4. The coupon system can be exploited by both the customers and the stores. 

Have you ever wondered how the stores that accept your coupons get their money back? Are they just sending them back to the manufacturer and reclaiming the money?

Actually, the process of redeeming coupons is more complicated than that, and involves four parties:

First, the stores collect all the coupons, then — because sorting them is particularly time-consuming — they send them to a sorting company. This company ensures the coupons are directed to the correct manufacturer.

But there's another step in the process before the coupon can be redeemed: the manufacturer employs yet another company to determine which coupons are valid. Only after that does the manufacturer finally issue a check.

The ins and outs of this process suggest that the coupon system is a difficult one to exploit. But this is not the case: both customers and stores take advantage of the system.

For instance, many consumers engage in what's known as _extreme couponing_. They attempt to push the discount system to its limits by using coupons to pay for as much of their shopping costs as possible. Some consumers, in fact, manage to pay _just ten percent_ of the total value of their shopping. Extreme couponing has become so popular that you can even find tutorials, websites and courses online which promise to teach you how to exploit the couponing system.

But it's not only customers that bend the couponing rules. Retailers are guilty of it too: sometimes they'll return unused coupons to the manufacturer to make some extra cash.

At one point, this practice was becoming so common that one coupon manufacturer designed and distributed a coupon for a non-existent product, in order to determine which retailers were to be treated with suspicion.

If the retailers tried to redeem these coupons by returning them to the manufacturer, it would show that they were trying to exploit the system. This was an ingenious plan: they found that two and a half percent of the coupons were redeemed by the stores themselves and not genuine customers, and were able to reveal the cheats.

> _"Buyers who are too greedy or cheat the system will destroy the very sellers on whom they rely."_

### 5. High fashion brands use secretive discount strategies, but customers also get discounted products through resale. 

The reason that discounts are so prevalent today is that the supply of products is outweighing consumer demand. In other words, there are too many things and not enough customers.

Until recently, there were certain industries that managed to avoid this economic fate. High fashion, for instance, used to be more of a cottage industry, and so it wasn't faced with the same risks of overproduction as larger industries.

However, once the high fashion industry began to expand, then in order to make their products more readily available to the general population, they too had to come up with a discount strategy. Their strategy was a unique one: _secret sales_.

Luxury brands, like Prada, Chanel and Armani, now produce on a much larger scale than before, so are available to many more customers. But imagine that one of these brands has made a product that somehow doesn't sell. In that case, a large, bold "SALE" sign in a store window wouldn't be effective — it simply wouldn't suit the image of a luxury brand.

On the other hand, a secret sale, to which only certain loyal customers are invited, is a simple and clever way to sell off old, hard-to-shift stock for a cheaper price while making customers feel like they're elite consumers.

However, what if you don't happen to be part of this elite group? Well, there's another way for consumers to purchase discounted luxury brand products: through the _resale_ of items purchased in the secret sales.

Because so many people nowadays crave the latest accessories, it's become a popular tactic to buy them from second-hand or online stores at a much cheaper price.

The lucky customers who are invited to secret sales usually can't resist buying a few more pieces than they need and then reselling them — on eBay, for example. This is how the average customer can get their hands on top-tier merchandise, like the latest "It bag."

### 6. Attitudes toward discounts are not the same across the globe, but the differences are diminishing. 

We've seen how discounts developed, how they can be exploited and what they do to our brains. But can we actually generalize the bargain phenomenon? Is it the same all over the world?

Actually it's not. Nations that emphasize individualism and those that have more of a collectivist culture have vastly different attitudes toward discounts.

India, for example, is a highly collectivistic culture where the citizens think of themselves as members of different groups, and don't mind being treated differently in accordance with their membership. When they're shopping, they think it's fair that different groups pay different prices for a product. Only within those peer groups do they insist on equality.

However in individualistic cultures, like the United States, the view is quite different: an American citizen simply wouldn't accept that another customer received a discount for the same product while he had to pay full price.

The fear of being cheated is probably one reason that bargaining was once regulated by law in many countries. Coupons, for example, were forbidden in Germany until 2001.

However, due to our globalized marketplace, these attitudinal differences are diminishing.

China used to be a collectivistic nation, and Chinese people thought of themselves first and foremost as part of a group. However, during the last few years, this attitude has changed. Because of China's one-child policy and economic revolution, its people have become increasingly wealthy and individualistic.

To show off their newly acquired wealth and distinguish themselves from others, it has become a trend among rich Chinese to purchase high-quality brands at full price. For example, on average, Chinese tourists visiting Great Britain spend about $2,670 during their visit — more than three times what tourists of other nationalities spend.

### 7. Discounts can have disadvantages, both for people and companies. 

The possibility of saving money on our shopping is certainly an alluring one. But have you ever considered the disadvantages of an all-discount world?

A bargain-oriented world poses challenges for both companies and consumers alike. For companies, surviving in a global marketplace teeming with bargains means that they need to continually adjust their business strategies. For instance, they must come up with ways to replace the money that they lose to discounts.

Take the travel industry. Hotels have accepted the fact that they need to adjust their prices frequently, just to keep up with rival companies and to meet their customers' expectations for discounts.

One solution you've probably noticed yourself is that hotels find other ways to make their money back: they charge extra for using their Wi-Fi connection, and they offer an expensive minibar in each room.

As for the consumers, their energy is drained by being in a constant state of alertness for a bargain, and their time is eaten up by comparing product prices among scores of retail outlets.

Nowadays, because we're always checking other stores and companies to compare offers and prices, in an effort to ensure that we always get the best deal, it's rare that we get to enjoy a relaxed shopping experience.

For example, imagine you just found out that a shirt you bought a week ago is now being sold at a different store with a 50 percent discount. When you notice this, you'll probably feel frustrated, and might even consider the time-consuming act of returning your shirt to the store, only to immediately buy it again at another store at a discount price.

Some companies, however, have decided that shopping with them shouldn't be an exhausting experience. As we'll see in the next blink, these companies have banned discounts from their marketing strategy completely.

> _"An all-discount world is one where buying is a time-consuming process, and sales can even backfire if shoppers feel unfairly treated."_

### 8. Some companies try to stand out by controlling their prices and never offering discounts. 

Now that you've learned about our contemporary all-discount world, you're probably thinking that businesses that don't mark down their prices must have a hard time surviving.

Actually, some companies deliberately insist on an alternative strategy: they keep their prices the same so their customers don't have to worry about whether they could've nabbed the product on sale a few days later.

In fact, in order to distinguish themselves, some companies go to the extent of banning discounts, and maintaining control over their prices. However, to do this, such companies need to control their production, which means they must own their own factories.

Companies that have their own factories, such as _Louis Vuitton_, have a great advantage over the competition: if they notice that a product is selling poorly, then — rather than offering those goods at a discounted price, as other companies would — they can simply instruct their factories to stop producing them immediately.

Moreover, by not giving discounts, companies can stand out because customers perceive expensive products to be high quality ones.

One such company is _Apple_. Its products are usually far pricier than those of its competitors, yet they continue to be bestsellers, and their customers are loyal to their product even though _Apple_ never marks down its prices.

The hypothesis that people perceive expensive products as being higher in quality was demonstrated by a study at Stanford University, in which participants were asked to rate the taste of several wines.

The wines were presented with a range of prices, from $5 to $90. However, there were actually just three different wines, and the researchers came up with prices for them at random.

As you might've guessed, the participants thought the $90 bottle tasted far better than the other, cheaper bottles.

As this illustrates, a company that ensures its products have a fixed, expensive price emphasizes their high quality and distinguishes itself.

> _"In other words, the more expensive it becomes, the more we crave it as a symbol of our own social rank and power."_

### 9. Stores can thrive in an all-discount world by selling own-brand items and personalizing their discounts. 

As if the immense popularity of online shopping hadn't already made things difficult enough for physical stores, nowadays customers will visit those stores to decide which product to buy, only to later search online for a better deal on that same product.

This enables customers to get advice directly from a salesperson, and for them to get hands-on experience of several products while still getting the benefit of online prices.

This practice is so widespread that it even has a name: _showrooming._

But how do the stores keep from losing money this way?

Stores have discovered that selling own-brand goods can prevent showrooming, because those brands can't be found anywhere else for a cheaper price. In fact, fashion stores like _Kohl's_ and _Macy's_ get 40 percent of their income from the sale of own-brand products.

However, it's not just physical stores that have to find new ways to appeal to customers who have an abundance of shopping choices online. Online retailers also must adapt to this climate, which is why many retailers are now using online technologies to _personalize_ the shopping experience, and thus make a greater profit. Increasingly, they're realizing that by tailoring their discounts for the individual consumer, they can sell more products. Quite possibly, this is what the future of discounts will look like.

Since many online stores produce their products several months in advance, they can't simply halt production if a product doesn't sell. But here's what they can do instead:

If they realize that their T-shirts don't sell well in a certain color — e.g., green — they can offer an exclusive discount to those customers who added a T-shirt of a different color to their electronic shopping cart. On adding the item to his cart, the customer will see a message like, "you'll get a 30 percent discount if you let us choose the color of your shirt." Many customers will be happy to get the green shirt if it means that they'll get a discount.

The advantage of this personalized method is that the customers that initially chose the green shirt won't know that the reason it's inexpensive is that it's unpopular.

> _"Buyers are no longer limited . . . Each has the ability to compare costs in the palm of his or her hand."_

### 10. Final summary 

The key message in this book:

**The discount was born in the late nineteenth century when one department store began fixing its prices and there was an imbalance of supply and demand. Today, we live in an all-discount world, which means that stores and retailers must find ways to survive. Consumers, too, must learn to control their hardwired impulse to buy when faced with an irresistible bargain.**

Actionable advice:

**Make fewer impulsive purchases by questioning your desire.**

The next time you see a SALE sign, remind yourself that the strong desire you feel is the result of a dopamine rush in your brain. With this in mind, try instead to rationally decide whether you really need that expensive new jacket or those designer sunglasses.

**Suggested** **further** **reading:** ** _Why We Buy_** **by Paco Underhill**

_Why We Buy_ draws on observations of real shoppers' behavior to understand the way people make purchases. It presents advice on how to design and tweak stores to optimize the shopping experience for customers, and thereby increase sales.
---

### Mark Ellwood

Journalist Mark Ellwood was born in Great Britain but now lives in New York City. He has published articles in the _Financial Times_ and the _Wall Street Journal_, and also works as a TV presenter and producer.

