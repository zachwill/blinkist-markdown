---
id: 5a5ca7c9b238e10007659d4e
slug: barking-up-the-wrong-tree-en
published_date: 2018-01-19T00:00:00.000+00:00
author: Eric Barker
title: Barking Up the Wrong Tree
subtitle: The Surprising Science Behind Why Everything You Know About Success Is (Mostly) Wrong
main_color: E7602E
text_color: B54B24
---

# Barking Up the Wrong Tree

_The Surprising Science Behind Why Everything You Know About Success Is (Mostly) Wrong_

**Eric Barker**

_Barking Up the Wrong Tree_ (2017) explores the divide between the extremely successful and the rest of the pack. These blinks draw on science, statistics and surprising anecdotes to explain the factors that determine success — and how almost anyone can attain it.

---
### 1. What’s in it for me? Find out how to really succeed professionally and socially. 

We live in a competitive world. Growing up, we're told that good grades will get us into good colleges, and only then can we be successful adults. But what if much of what we were taught about attaining success was wrong?

Success is more about breaking rules than following them. About doing what you're passionate about rather than what society dictates. Of course, confidence and hard work are also important, but meaningless unless you have long-term goals to which to couple them. By looking at some examples of real-life success, the following blinks will uncover what it really takes to get ahead of the pack and help you accomplish your life goals.

In these blinks, you'll learn:

  * how to turn your weaknesses into your greatest strengths;

  * why college dropouts on the Forbes 400 list are twice as rich; and

  * why nice guys finish first _and_ last.

### 2. Playing by the rules will only get you so far; creativity is the real engine of success. 

Just about every parent wants his teenager to turn out well-adjusted, conscientious and intelligent, with the grades to prove it. After all, students with these traits often grow up to be reasonably successful. The problem is, an ability to succeed in an environment governed by rules, like a high school, won't prepare a student to rise to the top of the real world.

In fact, the number of valedictorians who transform the world is close to zero. There's a Boston College study to demonstrate this. Researchers followed 81 high-school valedictorians from graduation onward and discovered that these incredibly high-achieving students were rarely visionary in their life pursuits. Rather than revolutionizing the system, they simply settled into it.

How come?

Well, good grades are a stellar predictor of a person's ability to follow rules. However, while school has clear guidelines, life doesn't; it's an unpredictable roller-coaster ride with no clear path. In this environment, rule-addicted academics lose their advantage.

But if valedictorians aren't the most successful people in the world, then who are?

Those who are obsessed; the unruly creatives who can adapt to every aspect of the outside world.

These creatives are driven by passion rather than external rules, and commit themselves to their passion projects with almost religious virtuosity — a clear recipe for excellence.

Just consider a sample of the richest people in the world. Are they conscientious rule followers? Absolutely not!

Some 58 people on the Forbes 400 list either dropped out of college or never even went. Those 58 academic failures have more than double the average net worth of the other individuals on the 400, all of whom attended Ivy League schools.

### 3. Nice guys are highly likely to finish first and last. 

In the 1980s and 1990s, an American doctor named Michael Swango murdered at least 60 patients. While his colleagues suspected him of these crimes, not one of them tried to intervene.

It's just one example of the way bad people seem to get away with everything. But does the world really tip in favor of those prepared to be bad?

Well, one thing is for sure: nice people tend to make less money and receive worse reviews than their meaner peers. Just take a study by the _Harvard Business Review_, which found that men who are low on the "agreeableness" scale make up to $10,000 more per year than those who are very affable.

Not just that, but nice guys who pull more than their weight often end up with worse performance reviews than their lazier colleagues, who make a good impression by flattering their bosses. Research has even found that flattery is so effective it works regardless of whether a boss knows it's sincere.

In other words, nice guys often finish last. Yet, paradoxically, they're equally likely to finish first. For example, Wharton School professor Adam Grant found that engineers, salespeople and medical students who consistently attempted to help others were overrepresented at both the bottom and top of success metrics.

Similarly, "takers," or people who selfishly try to get more while giving less, often end up in the middle, while "givers" are split between the very top and absolute bottom. Interestingly, these same studies found that the highest achieving engineers, students and salespeople were all givers.

It may seem odd, but it also makes intuitive sense; everybody knows a martyr who tries his best to help others only to be used by takers. And we also all know helpful people who rise because others are indebted to them.

To put it simply, it's not always bad to be nice. And by the way, bad people don't get away with everything. That serial killer, Michael Swango, was caught and sentenced to three consecutive life terms in prison in 2000.

### 4. Persevere through tough times thanks to the stories you tell yourself. 

Today, Alfredo Quinones-Hinojosa, also known as Dr. Q, is one of the top brain surgeons in the world. He runs his own lab at America's best hospital, Johns Hopkins in Baltimore. However, his life started out in absolute poverty; as an illegal migrant farmworker, he had to demonstrate true grit, sticking with difficult work throughout his life and never giving up.

It sounds like everyone could benefit from some grit like that, but where does it come from?

Interestingly, it's primarily a product of telling yourself optimistic stories. This is key since studies have found that we say around 300 to 1,000 words to ourselves every single minute. These words can either be positive, like "I can handle this," or negative, like "I can't do this."

Just take an example from the military. Post 9/11, the US Navy SEALS were looking for more recruits. To ensure that a greater percentage of candidates made it through the grueling initiation ritual known as "Hell Week," they began teaching candidates to use positive self-talk. As a result of this practice, the Hell Week pass rates rose by almost 10 percent!

In other words, optimism keeps people going, but true grit is more than positive thinking. It often also depends on the stories people tell themselves about the meaning of their lives.

A good example here is the renowned psychologist Viktor Frankl, who was imprisoned in Auschwitz in 1944. Before long, he found that some people were surviving the camp much longer than others, although they neither appeared physically stronger nor emotionally braver than those who passed away early on.

In observing this phenomenon, Frankl found that what was keeping these survivors going was the meaning they saw in their lives, even during times of tremendous suffering; the people who made it were those who told themselves that they had a purpose beyond themselves. Frankl himself had such a purpose, wanting to survive for his wife. He had imaginary conversations with her all the time.

### 5. Extroverts make the most money, but introverts make the best experts. 

In a working world that's increasingly organized around teams and networking, how far can a lone wolf advance? Do you have to be extroverted to succeed in your career?

Well, if research is any guide, the scales certainly tip in favor of outgoing and popular people. Extroverted workers are much more likely to succeed than their peers, both in financial terms and in career progress.

Just take one study which revealed that the most popular 20 percent of students in a high school class went on to earn 10 percent more in adulthood than the least popular 20 percent.

Beyond that, even the _bad_ habits of extroverts make them more financially successful. For example, extroverts are more likely to go out drinking, but also more likely to bond with others at the bar, potentially forming valuable connections. That could be why, according to other studies, drinkers earn 10 percent more than sober people.

Simply put, having lots of friends is a plus, but it can also serve as a major distraction. That's why introverts are much more likely to become experts in their field. For instance, the author and Olympic medalist David Hemery conducted research which found that 89 percent of top athletes consider themselves introverts, while just 6 percent identify as extroverts.

Why? We know that becoming an expert, regardless of the field, requires 10,000 hours of practice. Simply put, holding together a vast social network doesn't leave extroverts with the time they need to do the hard, lonely work necessary to acquire such expertise. Meanwhile, introverts don't have to worry about other people bothering them, and have plenty of time to hone their skills.

> Research has found that emotionally stable introverts make the best investment bankers.

### 6. Confidence is key to success, but too much of it can make you hurt others. 

When the chess master Garry Kasparov played the supercomputer "Deep Blue" in 1997, the computer executed a random move caused by a software bug. In response, Kasparov assumed that the machine had a plan he wasn't smart enough to understand; he lost his confidence and eventually the match.

It's a clear example of how poor confidence can cause serious problems. Success and confidence have a clear positive association.

Research has even shown that overly confident people have better chances of receiving a promotion than their less confident colleagues, even if the latter are more accomplished. According to other studies, confidence increases productivity and causes people to take on more challenging tasks, thereby making them stand out as shining stars in the workplace.

So, confidence is key, but how can you boost it?

Well, one sure-fire way to make yourself feel more self-assured is being attractive. This correlation likely explains why attractive women earn 4 percent more than their peers and attractive men an extra 3 percent.

In other words, confidence can predict success and often results in increased power, both on the job and in daily life. However, there's also a downside; too much confidence and power can destroy a person's character.

Some studies have found that power lowers empathy and that powerful people are prone to treating others as subhuman. This could be explained by the fact that powerful people need to make decisions that could harm individuals but are, in the long run, the right choices to make. For instance, generals are tasked with leading troops into deadly battles to win wars. If every general were wracked with guilt after making such a call, they would never do what was necessary to win.

And finally, research has also found that feeling powerful makes people more selfish and better at lying. That's because power and confidence reduce a person's concern that he'll hurt others, and therefore lower his remorse when telling a fib. This could explain why increased feelings of power positively correlate with the odds of cheating on a spouse.

### 7. The key to success is working long hours and always pushing yourself to do better. 

If you've read this far, you're ready to succeed and, if you strive for the traits outlined in the earlier blinks, you can. But you won't strike it rich and get famous just by working when the mood hits you. Having a strong work ethic is essential.

The number of hours you spend working will make all the difference between the decent and the incredible. While intelligence will certainly help, once your IQ stretches beyond 120, adding additional points has basically no effect on success. That's why the smart people who succeed are those that work the hardest.

Just take a Harvard study which found that the most successful managers in a number of industries commonly worked over 60 hours per week. The study clearly indicates that a person's success depends on her productivity, and productivity depends on putting in time.

Research has even found that in every complex, professional job, the top 10 percent of employees — those who make the most game-changing contributions — are 80 percent more productive than the average and 700 percent more productive than the employees in the bottom 10 percent.

But putting in long hours isn't all you need to do. Success also depends on how far you push yourself beyond your limits. This is important to remember as people rarely make an effort to improve during their day-to-day activities, work included.

For example, research has found that doctors and nurses don't tend to get better at their jobs over time. Another classic study by Benjamin Bloom considered leading scientists, artists and athletes. It found that one of the greatest benefits of mentorship in these fields was not insider knowledge or emotional support, but the presence of a figure whose expectations would push the mentee beyond their limits.

So, no matter what form you want your success to take, be sure that you're ready to commit yourself to the hard work it will take to achieve. If you do, there's nothing you can't accomplish.

### 8. Final summary 

The key message in this book:

**People tend to think that those who "make it" in life are the smartest and most talented. But the truth is that success is determined by characteristics that are much more within your control, like hard work, motivation and confidence. Because of this, anyone who wants to can enjoy tremendous success.**

Actionable advice:

**Take time to make others happy.**

Did you know that people with happy friends are 15 percent more likely to be happy themselves? You can boost your happiness by doing kind things for those in your life. Try it out by doing a favor for a friend without expecting it to be reciprocated. Doing so will make your friend feel better about you and make you feel better about yourself. It'll only be a matter of time before the happiness of those in your social network boomerangs back at you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Everybody Lies_** **by Seth Stephens-Davidowitz**

_Everybody Lies_ (2017) is about the data collected in vast quantities by computers and over the internet. This data can help reveal fascinating information about the human psyche, behavior and quirks, because, as it turns out, people aren't always so willing to communicate their true hopes and desires to others.
---

### Eric Barker

Eric Barker is a former screenwriter for Walt Disney and Twentieth Century Fox. His popular blog, _Barking Up the Wrong Tree_, offers scientific insights that help readers succeed. Barker's work has been published in the _New York Times_, _Wired_, the _Wall Street Journal_ and _TIME_ magazine, for which he writes a regular column.

