---
id: 562e15cd3864310007150000
slug: battle-hymn-of-the-tiger-mother-en
published_date: 2015-10-27T00:00:00.000+00:00
author: Amy Chua
title: Battle Hymn of the Tiger Mother
subtitle: None
main_color: F16937
text_color: 8C3D20
---

# Battle Hymn of the Tiger Mother

_None_

**Amy Chua**

Amy Chua was born in the United States to strict Chinese immigrant parents who pushed her to work hard and succeed instead of coddling and encouraging her. _Battle Hymn of the Tiger Mother_ (2011) is about her experience of raising her third-generation kids according to her parents' old-school beliefs. Chua offers not only an insightful and often controversial take on parenting, but also a memoir of a very stern yet loving tiger mother.

---
### 1. What’s in it for me? Discover the way of the Tiger Mother. 

If you're a good parent, you know your child needs all the compassion and support you can give them, right? Not according to author and "Tiger Mother" of two, Amy Chua. Quite the contrary, her child-rearing techniques entail endless demands, criticism and, most importantly, the unshakeable belief that her children can succeed at anything they put their minds to. 

In these blinks you'll discover the pros and cons of the authoritarian Chinese style of raising children and how being a Tiger Mom worked out for Amy Chua.

You'll also learn

  * why school plays don't matter to a Tiger Mom;

  * why Chinese parents don't care about hurting their children's self-esteem; and

  * why "fatty, lose some weight" is an OK thing to say to your child.

### 2. Chinese and Western parental mind-sets differ greatly. 

If you're a parent, you want the best for your children. However, Chinese and Western parents have very different views on what "best" means. Here are three primary differences:

First, Western parents want their children to have high self-esteem, while building self-esteem doesn't hold much importance for Chinese parents.

Western parents become concerned about their children's feelings if they lose or fail. On the contrary, Chinese parents expect inner strength from their offspring, rather than believing they'll be easily hurt. If a Western child doesn't do well in school, for example, her parents will often be tactful and try not to hurt the kid's feelings when expressing disapproval. Chinese parents, on the other hand, respond to poor results by demanding improvement, no matter how this may affect their child's self-esteem.

Second, Chinese parents believe that their children owe them everything.

This likely stems from a combination of the Chinese tradition of respecting one's elders and the fact that Chinese parents work very hard to ensure they can give their children a good education. The Chinese perspective is therefore that the children should spend their lives paying back their parents by making them proud. Western parents think otherwise. They believe that as it was they who decided to become parents, it's their duty to take care of their children and that their children don't owe them anything.

Third, Chinese parents feel that they know what is best for their children. 

While Western parents usually ask their offspring what activities they'd like to try, Chinese parents believe it's best to tell children how to spend their time. What their children want for themselves plays a far lesser role, if any.

This means you'll never hear of a Chinese child coming home from school and announcing proudly to her parents, "I got a part in the school play." This is because Chinese parents feel it's best for their offspring to only partake in extracurricular activities in which they can win a medal — preferably a gold one.

### 3. Chinese parents don’t let their children give up so easily, and work hard to prepare them for the future. 

What do you do when you find out your child is not particularly good at something? Western parents will often simply acknowledge a child's weakness, but Chinese parents will do everything in their power to transform it into a strength.

This is because Chinese parents think that there's nothing better for children than to improve the things they aren't good at...yet.

While many Western parents try to avoid hurting their child's self-esteem by, say, avoiding games where the child might lose, Chinese parents teach their children to persevere through challenges and difficult situations.

For instance, Louisa, the author's daughter, was assigned a difficult piano piece to learn, in which each hand had to play different rhythms. Louisa practised for hours and still failed to get it right. When she announced that she was giving up, her mother forced her to carry on. The author wanted to instill in her daughter the belief that she could improve. Sure enough, Louisa was finally able to play the piece, and was so thrilled that she played the song again and again over the following few days.

Another typical aspect of Western parenting is encouraging children to pursue their passions. Yet, Chinese parents see far more value in preparing their children for the future by giving them the necessary skills to succeed.

The author didn't make a single decision for herself throughout her own childhood. Whenever there was a new choice of subjects at school, a new extracurricular activity on offer, or when the topic of which university to attend came up, her parents dictated what was best for her. On one hand, this meant she had no freedom. But seeing how successful she is now, she's grateful for her parent's decisions.

### 4. Chinese parents don’t believe in fun and don’t address happiness while bringing up their children. 

When we see strict parents scolding their children, we tend to think the children are unhappy. Yet Amy Chua was quite happy as a child, even though she was raised by extremely tough Chinese parents.

In fact, to Chinese parents, nothing is fun for children until they are proficient at it. And whenever you want to be good at something, it's going to require considerable practice. While most children want to quit near the beginning of the process — since this is when things are toughest — Tiger Mothers force their children to persevere until they start to like it. 

Take Amy Chua's daughter Sophia. When she started to learn the piano, every exercise took a great deal of effort, but Amy forced her to practice anyway, often for three hours each day. After some time, Sophia became so good at piano that others began praising her for it. This recognition raised her confidence and made the arduous work more fun, which in turn made it easier for Amy to get Sophia to continue with her practice.

Despite the fact that happiness is hardly a primary factor for Chinese parents, their children do say that they are happy.

When you look at the packed schedule of a Chinese child and the immense amount of work their parents pile onto them, you might wonder how on earth they can have a happy childhood. But when Amy looks at Western families, she sees that many break down even though they have far more flexible schedules and openly discuss happiness.

In fact, many grown Western children can't bear being around their parents or, worse, won't even talk with them. Conversely, many Asian children consider themselves happy and are appreciative of their parents, despite their harsh demands. So it seems that seeking happiness might not need to be a priority in order for children to grow up feeling happy and grateful.

### 5. Chinese child-rearing methods often sound harsh and Chinese parents have no problem with being no-nonsense. 

While Western parents would admit to being "strict" if they forced their child to sit down and practice piano for one hour each day, to Chinese parents this single hour would be a mere warm-up.

Strictness is no problem for Chinese parents, and they're far more direct with their children than Western parents. A Tiger Mother has no issue telling her daughter "Hey fatty, lose some weight," for example, whereas Western mothers feel compelled to be tactful by having a conversation about health instead.

But does this really make a difference to the child's health or self-esteem? The truth is, Western children still end up suffering from eating disorders and negative self-image.

When it comes to academia, the author orders her daughters to get straight As, while other parents would just encourage their children to do their best.

In the end, Amy Chua is confident of what her daughters are capable of, and communicates with them without sugar-coating her message. Others are often left trying to persuade themselves that they're not disappointed with what their children have achieved or not achieved.

Another thing that distinguishes Chinese parents from Western parents is that they openly compare their children with others and show favoritism. For instance, Amy Chua was at a Chinese medicine store once and the owner told her about his daughter and his younger son by announcing: "My daughter she smart. Only one problem: not focused. My son — he not smart. My daughter smart."

Even though Amy knew that parental favoritism could damage your child's self-esteem, she never thought it was toxic when she was growing up. Perhaps she didn't mind, since she always did quite well when compared with others.

### 6. Strict behavior is standard across generations of Chinese immigrants, but this has changed with the youngest generation. 

Amy Chua predicts that the youngest generation of Chinese immigrants won't be as successful as their predecessors. The history and pattern of behavior of the three generations of Chinese immigrants explains this.

The first Chinese immigrant generations endured the most challenging phase of their family story and are therefore the most strict and hard-working. Amy Chua's parents belong to this generation, and they were no strangers to hard work. They started with next to no finances, but worked until they became successful in their profession. In their children's eyes they were extremely strict and careful with money, yet everything they did and earned was invested into their children's education and future.

The next generation reaped the benefits of their parents' hard work, and adopted their parenting style.

Amy Chua's generation were high achievers thanks to their parents, often playing piano or violin, attending top universities and becoming business professionals. Although this generation mirrors their parents a great deal, they also tend to be less frugal and not as strict with their children.

But in the most recent generation of Chinese immigrants, Amy Chua sees a different behavior emerging.

This new generation — to which Amy's children belong — will be born into a life of comfort, with friends who get paid for B-pluses, a reward which would be unthinkable for Chinese parents. This new generation may attend private schools and, as they have their individual rights protected by the US Constitution, they're more likely to disobey their parents. In summary, to Amy, this is a generation headed for decline.

### 7. Final summary 

The key message in this book:

**Chinese parents often seem extremely hard on their children, using strict orders and discipline beyond what most Western families would think acceptable. However, this behavior stems from a deep and genuine desire to prepare their children for the future, and to show their children what they are capable of if they work hard enough.**

Actionable advice:

**Improve your children's grades.**

To get children to improve their grades, the Tiger Mom will demand that they work hard. No friends, no sweets and no fun activities are allowed until they complete their homework to the best of their efforts. Only when they are top of the class at everything they do, will a Tiger Mother trust that her children are well-prepared for the future.

**Suggested** **further** **reading:** ** _The Smartest Kids in the World_** **by Amanda Ripley**

_The Smartest Kids in the World_ takes a look at why South Korea, Finland and Poland seem to have the brightest school children and best education systems in the world. At the same time, problems and potential solutions in the US education system are examined.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Amy Chua

Amy Chua is the John M. Duff professor of law at Yale Law School. Her book _Day of Empire: How Hyperpowers Rise to Global Dominance — and Why They Fall_ was an acclaimed bestseller. In 2011, she was named one of _Time_ magazine's 100 most influential people.

