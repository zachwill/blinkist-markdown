---
id: 5a74f698b238e10006541187
slug: be-obsessed-or-be-average-en
published_date: 2018-02-05T00:00:00.000+00:00
author: Grant Cardone
title: Be Obsessed or Be Average
subtitle: None
main_color: 72A7D1
text_color: 486A85
---

# Be Obsessed or Be Average

_None_

**Grant Cardone**

_Be Obsessed or Be Average_ (2016) is a guide to living life to its fullest. Cardone offers his unique take on becoming a driven and passionate business leader with the hunger it takes to rank among the best in the world.

---
### 1. What’s in it for me? Don’t settle for second best in life or business. 

Many of us grew up with the impression that our reward for years of hard work and loyalty would be a nice retirement package that allows us to while away our golden years in a peaceful slumber. In this scenario, the most exciting moments are likely to involve games of bingo. But it doesn't have to be this way.

The only thing that's preventing your wildest dreams from coming true is fear of failure or rejection — and a lack of ambition. But once you begin to tap into the great powers and passion that come with obsession, you can use your fears to your advantage. Then you'll be on your way to unlocking the limitless rewards that come with grand ambitions.

In these blinks, you'll discover

  * J.K. Rowling's secret for success;

  * what the New England Patriots can teach you about ambition; and

  * why haters are a sign of good fortune.

### 2. Become obsessed, and you can have energy and balance in your life. 

Every once in a while there comes a time when you're on the job and feeling depleted of energy. When you're on the verge of burnout, you have two options: you can either take some time off or become obsessed.

The average person will likely take the first option, but this isn't going to help you get where you want to go. If you want to reach the next level you need to get obsessed and always push toward your goals. When this happens, you'll find that you're generating energy, not depleting it.

However, if you do feel exhausted, check in with yourself and reaffirm your purpose. Maybe you've gotten off track?

When the author hit 40, he began feeling tired and stressed. He realized he was constantly crisscrossing the United States to give one talk after another. On days when there was a US holiday, he'd book a talk in Canada, and it wasn't unusual for him to wake up and not know which city he was in.

It dawned on Cardone that all the travel and speaking engagements had distracted him from his real obsession. He didn't start out to be a public speaker; he wanted to be the greatest salesman on earth.

After writing down his purpose and reasserting his goals in life, he immediately felt rejuvenated.

Many people think that being obsessed leads to an imbalanced life, but the author has found that obsession is the key to both unlocking your energy and achieving real balance.

A balanced life isn't about taking lots of time off to relax and hang out. It's about having the career and money you desire as well as good health, a happy family life and strong faith. And for this, you need to work hard each and every moment, which means you need the passion that comes with obsession.

With this energy, you can make the most of every second, like combining family time with fitness and taking the kids to the gym with you in the morning, something the author likes to do.

### 3. By keeping your obsessions fresh, you’ll find new motivation. 

Let's say you know what you're passionate about, but it took you a lot less time than expected to reach the goals you set for yourself. In this case, the question isn't about getting started, but about keeping going and resisting the urge to spend each day kicking back in your hammock.

And this is also where obsession comes in handy.

An exceptional career isn't one that is satisfied by achieving one goal and calling it quits; instead, it involves a whole series of goals, each more audacious than the last.

Having a passionate obsession means you're not one of those people whose goal is to retire so they'll have plenty of time for playing golf. Your goals need to be grand, driving you toward a limitless future. This is a purpose that you feel is part of your very being and makes you excited to get out of bed each morning.

For example, at one point, the author owned 100 apartments, and then his goal immediately became more ambitious: to own 500. Now, as of 2016, he owns 4,500 units.

Once you earn a million dollars, why not set a new goal of a billion? Start thinking of all the charitable organizations you could fund and the safety net you could provide for future generations. With endless ambitions, you can give every day the fuel that will carry you to a truly exceptional life.

Now, if you imagine your post-retirement life on a golf course, your goal might be to achieve that low handicap, but then what? Before long you might end up spending the majority of the day watching TV, and then depression sets in.

If you don't want to be miserable, you need to give yourself a grand purpose that will grow along with your career.

So never settle. Stay active and keep reaching for new heights.

> _"The goals of the obsessed are always a little out of reach."_

### 4. Aim high and worry about the details later. 

No one can predict exactly how things are going to work out. Even billionaires don't know exactly how they're going to make their fortune. Instead, they do what you should do, which is promise ten times more than they think is possible and then force themselves and their team to find a way to deliver on that promise.

So it's important to always aim high and make promises that will make you rise to the challenge.

All the major events in your life, like getting married, having a child or starting your first business require you to dive in and figure out how to swim as you go.

The same principle should hold true for the goals you set for yourself and your business. You can even release products before they're ready, especially if it means being the first on the market.

Often being first is more important than being perfect, as Apple has proven time and again. Nearly every Apple product has had its fair share of flaws, but the company knows that being big, bold and innovative is more important than being delayed in beta testing. This is why Apple is considered one of the best brands ever.

Another organization you should take cues from is the New England Patriots football team.

When Robert Kraft took over as owner of the Patriots in 1995, they were struggling. But that didn't stop Kraft from making bold promises to win the Super Bowl. This inspired the players to give 110 percent each season and each game, and now they've won five Super Bowls since Kraft came on board.

Not only that, the Patriots franchise is worth $3.2 billion, and some of the best seats sell for $500 and corporate boxes for $500,000. Just like the players, the fans are inspired to believe their team is going to go far every year.

As these examples show, it's good business practice to promise the moon and have the confidence that a plan will fall into place.

### 5. Embracing your fears will help you succeed. 

While pushing the envelope can be a driving force for success, it is also guaranteed to create some fear. But don't let this stop you. Fear needs to be understood and embraced since it goes hand in hand with success.

Fear comes in two main flavors: fear of rejection and fear of failure, and both must be dealt with if you want to succeed.

In 2008, J.K. Rowling addressed the graduating class at Harvard University. She said what makes you great in life is being brave enough to fail. So if you never fail, you will never live.

Rowling speaks from experience. She received 12 rejection letters before the first Harry Potter book was finally accepted by a publisher. And it went on to sell around twelve million copies. If she had let the fear of failure stop her, Harry Potter might not exist today, which is unthinkable!

So go ahead and let yourself feel afraid, and know that it's OK because it means you're pushing yourself and moving forward. If you don't feel fear from time to time, it might mean you're getting comfortable and becoming a big fish in a small pond, which should be avoided since it means you're not growing.

If you feel you're stagnating, it might mean you need to move to a bigger pond. Perhaps it's time to open a new branch in another city or explore new business partnerships that will expose you to a new market?

When it comes to beating the competition, fear can also be useful. Embracing fear is all about having the right mind-set, and it's one that can give you a psychological advantage.

For the author, the biggest breakthrough came following the 2008 economic collapse. At the time, everyone was scared about the future, so he decided to use this fear as an incentive to be more aggressive in making sales and more public appearances. So, while others were running scared, he began to expand into more markets, which paid off rather nicely.

> _"The difference between success and failure is staying in the game when others throw in the towel."_

### 6. Stop saving and use that money to grow. 

Whatever your fears may be, success is always going to be measured in growth. This means the smart thing to do is to put your profits and excess energy into expansion. With this strategy, you'll have the best chance of cornering the market.

This strategy also means you should view spending as much more important than saving.

Think of it this way: Money that isn't being used isn't very useful, is it? The only money that matters is money that is helping you grow.

This also makes good tax sense. While profits are always taxed, money that's re-invested in your company is usually not taxed, making these dollars more valuable at the end of the day. So it's wise to always be on the lookout for growth and investment opportunities, including new markets to expand into and new avenues to explore.

The author recommends spending 30 to 40 percent of your profits on expansion opportunities.

But if those expansion opportunities are proving hard to find, then the next best thing is to spend that money on publicity. Generally speaking, money spent on marketing, advertising and social media is never wasted. When publicity works and you become a household name, those previously elusive expansion opportunities will soon be knocking at your door.

Whatever you do, don't try to be a "one-man show." Being a leader means guiding your team to success, and often this means having a very big team, such as Amazon's staff of over 200,000, or Apple and Microsoft, with their 100,000-strong teams.

Currently, around 75 percent of US companies are what the author would call a one-man show, and when you average out their annual profit, it's a meager $44,000.

The author learned the limits of this approach the hard way, spending ten years trying to do it all himself until he realized that being in charge means knowing how to delegate and hire people who help you earn money. That's when Cardone started employing a lot of people, and the company really began to grow.

### 7. Use haters as allies to fuel your obsession. 

With great success comes opposition. It's just the way business works. But since you can anticipate competitors and critics you can also prepare yourself.

Today's social media landscape offers haters plenty of avenues to pick on you, but the best way to deal with their presence isn't to try and shut them up — it's to be thankful for the attention.

Think of it this way: the more haters you have, the more successful you are.

There's an old saying that "there's no such thing as bad publicity," and in this case, haters can indeed be used as free publicity. When someone criticises you, they see you as a leading force in the market. No one picks on an insignificant business. Only those who are important are seen as worthy of trolls and vehement criticism.

To put it another way: If you have millions of Twitter users hating on you, there's a chance you could become the next president!

Haters also have the added benefit of making us more resilient. Think back to all those bullies over the years. For better or worse, they did play a role in making you the person you are today.

Whether it was the jocks or mean girls who picked on you in high school, the bank manager who didn't approve your loan, or the anonymous troll who's calling you a capitalist pig, they might be a pain in the ass but they can also help you to form a thicker skin and fuel your passion to prove them wrong. If used wisely, the words of haters can even result in you making some adjustments and creating a better product.

However, it's not just haters that can stand between you and your obsession. Sometimes, members of your own team can be the ones holding you back, and in the next blink, we'll look at how best to manage problematic employees and create a company-wide culture of obsession.

> _"Massive success is the ultimate revenge."_

### 8. Be obsessive about having the best team possible. 

Which would you consider more important for generating profits, being nice or being effective? Hopefully, you chose the second option since it's a fact of business that, from time to time, you need to make unpopular decisions in order to maintain leadership and ensure that everyone is staying on track.

The best way to maintain control of your staff and business is to control the conversation — literally.

Everyone on your team should be aware that you are both listening and observing them at all times. This means listening in on their sales calls to make sure your team is as aggressive and passionate about moving product as you would be if you were making the calls yourself.

If someone isn't up to your standards, or if the client isn't properly advised, it's perfectly acceptable to enter the conversation and set things straight.

There are other ways to make your team aware that you recognize good work and are personally engaged in every aspect of the business.

The author makes a habit of sending personal messages of gratitude to the individual workers who put all their effort into their work. Even if he's thousands of miles from the office, he'll record a video on his phone and send it to the team. Honest, personal touches like this will ensure total commitment.

But your control really starts by being ruthless when it comes to hiring and firing.

Remember, when you hire someone an agreement is made: you pay them and treat them fairly, while they perform a certain standard of work. So, when an employee fails to hold up their part of the deal and becomes dead weight, you shouldn't feel bad about letting them go and replacing them with someone who will do the job.

These days we no longer expect to work the same job for 50 years. Most people expect to have several full-time jobs over the course of their career. Millennials are especially aware of how to take advantage of short-term contracts in order to build a varied career.

It's your job to build a team that is as obsessive and passionate about your business as you are. With this in place, success is sure to follow.

### 9. Final summary 

The key message in this book:

**Being exceptional means having a growth mind-set. To truly succeed, you need to set high expectations for yourself and your company that may seem impossible. But these big goals are key to motivating a tireless workforce and propelling you toward truly exceptional results.**

Actionable advice:

**Don't waste time on tasks that aren't fuelling your obsession.**

Don't let household chores eat into valuable time which would be better spent pursuing your dreams. Don't hesitate to pay someone else to clean your car, mow your lawn or vacuum your apartment. Surely there are more important things that both suit your skill set and move your plans forward.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The 10x Rule_** **by Grant Cardone**

_The 10X Rule_ (2011) provides clear indications for how to best plan your road to success. These blinks will teach you why this little-known strategy works and how to put it into action, while also giving you the tools you need to become more successful than you ever thought possible.
---

### Grant Cardone

Grant Cardone is a self-made multi-millionaire entrepreneur. He is also the author of the bestselling book _The 10x Rule_.

