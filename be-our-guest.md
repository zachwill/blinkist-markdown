---
id: 553503426638630007ec0000
slug: be-our-guest-en
published_date: 2015-04-22T00:00:00.000+00:00
author: Disney Institute and Theodore Kinni
title: Be Our Guest
subtitle: Perfecting the Art of Customer Service
main_color: A92020
text_color: A92020
---

# Be Our Guest

_Perfecting the Art of Customer Service_

**Disney Institute and Theodore Kinni**

_Be Our Guest_ (2011) reveals Disney's key tenets and principles of outstanding customer service and how following these has helped the company become the successful business empire it is today.

---
### 1. What’s in it for me? Discover why you only ever get the best possible service from Disney. 

What was your dream vacation as a child? A majority of adults (and certainly their children) would say a trip to Disneyland or Disney World.

How has one single company managed to stay so beloved for so long? As the entertainment industry has changed over generations, the one constantly successful group has been Walt Disney.

Granted, the company's films have helped keep it a touchstone for children the world over, but just as importantly, Disney's extraordinary standards of customer service keep people coming back for more.

These blinks show you the lengths to which Disney goes to ensure that everything the company does, from staff (or cast members) expectations to theme park design, is designed to deliver the best experience for each and every customer (or guest).

In these blinks you'll discover

  * what an implementation matrix is and how to use it;

  * why Disney door handles are so ornate and whimsical; and

  * what secret fountains have to do with childhood joy.

### 2. Disney’s service strategy looks at what guests want and delivers high-quality service in response. 

Disney's expert customer service stems from its carefully crafted _Quality Service Compass._

Like the four main points on a compass, the Quality Service Compass has four key elements that help guide every Disney employee in offering quality customer service.

_Compass Point 1_ is the art and science of knowing and understanding customers. In the world of Disney, this is known as _guestology_.

This point is the foundation for every other action on the Quality Service Compass. It involves discovering a guest's expectations — what she likes or doesn't like — and then informing employees (or _cast members,_ in Disney speak) of what those expectations are.

Disney observed guests and identified the main aims of a family theme park vacation, for example. The company discovered that guests wanted to enjoy a fun time that centered around the family, as well as take a break from the exhausting schedule of daily life.

Disney then used this knowledge to modify and mold the perfect experience for its visitors.

_Compass Point 2_ is focused on quality standards. Once you know what your guests want, you can start considering how to ensure you will provide them with the highest quality service.

Disney defines four important quality standards: safety, courtesy, show and efficiency. 

_Safety_ ensures the welfare of all guests, while _courtesy_ means guests are treated with a personal touch. _Show_ considers the environment, making sure all elements of a visit are in harmony, while _efficiency_ makes sure that facilities, systems and employees are working as they should.

Disney cast members know these standards are of the highest importance. Consider a situation in which an employee is about to perform in a show. The employee however sees a child in distress, as she is lost.

Because safety is the first Disney quality standard, the employee would assist the child first before beginning the show.

### 3. Disney provides high-quality service by ensuring that systems are strong and integrated. 

We've explored the first two compass points; let's now look at the final two.

_Compass Point 3_ involves three _delivery systems_ : employees, setting and processes.

Let's expand on these concepts. _Employees_ receive continual performance advice to keep the service they offer of the highest quality. Managers ensure that the _setting_ of the customer experience, whether a hotel, theme park or store, is in line with Disney values. And developing _processes_ that are fault-free is crucial to a seamless customer experience.

Disney ensures that each setting it manages lives up to a guest's wildest imagination. For example, even the doorknobs of a Disney hotel's dining room look straight out of "Alice in Wonderland." These sorts of details make guests feel as if they're in a magical, special place.

Disney also works to design stress-free processes. For example, many guests had a hard time finding their cars in the theme park's enormous parking lot. So Disney came up with a special solution.

Parking lots were filled in a particular order, and a guest's arrival time noted. This way, each family could easily find their car after a long day simply by remembering when they arrived.

_Compass Point 4_ is _integration_, which combines the three delivery systems. While each system needs to be addressed separately, for efficiency, the three systems must be integrated as well.

To do this, Disney employs a special _integration matrix._ The matrix takes a single factor that Disney feels is important and ensures that each of the three elements are examined in this context.

For example, let's take _courtesy_ as an important element and insert it in the integration matrix, which will connect the element of courtesy to _employees_, _setting_ and _processes_.

Managers might ensure _employees_ are trained to be polite and helpful to guests at a particular store; in the store or _setting_, guests might be given "recognition cards" to give to especially helpful employees. The _process_ of allowing guests to name VIPs among their own party, for example, is just another way of expressing courtesy in all its forms.

### 4. A service-oriented purpose acts as a promise to your customers and a mission for your employees. 

Disneyland declares that it is the "Happiest Place On Earth." But why come up with a catchy slogan or message in the first place?

Simply, a theme that outlines a common purpose sets a foundation for the public image of your company.

Such a theme informs guests about what they can expect from your company. With your theme, you make a promise to consumers which you must meet or exceed — or else they won't return.

Disney's theme is, "We create happiness by providing the finest entertainment for people of all ages, everywhere."

With this statement, Disney voices its goal, _to create happiness_, provides information on how to accomplish this, _by providing the finest entertainment_, and defines its target group, _people of all ages, everywhere_.

But slogans also play an important internal role for a company. Devising a common purpose can also function as a mission for your employees, helping to focus their daily activities.

With a well-crafted theme, you create a common goal for all staff, regardless of job title.

Thus a Disney employee doesn't just do a job, but is part of one of the world's leading companies and fulfills an important role when interacting with customers. To underline this concept, Disney doesn't actually use the word "customer" but prefers to use "guest."

We all know that a guest is someone who should be given special treatment. Disney works to encourage employees to welcome their "guests" and cultivate a practice of personalized treatment — and not just deal with "customers" who can easily be seen as faceless year-end statistics.

In sum, formulating a common purpose for your company can inspire both customers and employees, giving your company a more-streamlined focus and encouraging efficiency.

### 5. Consider how customers see, smell and hear the world you’ve created for them. 

Have you ever wondered why you can smell popcorn first thing in the morning at Disneyland? It's to create a cosy, warming atmosphere for your visit!

Smell is an important element in a customer's experience; sight is too. Our eyes contain some 70 percent of all our body's sense receptors, so paying particular attention to what a customer can see is a crucial element in providing exemplary customer service.

At Walt Disney World, public road signs and directional signs are colored purple and red. But why this odd combination? In an experiment, participants were shown flags of different colors; the flags that were most frequently remembered were those colored purple and red.

A customer's experience doesn't stop with smelling and seeing, however. A guest should also never be presented or surrounded by _sounds_ that are unwelcome or annoying. Sounds too should fit in seamlessly with the world that your company has created.

Music is especially important. Music affects emotions, inspiring people to experience joy, or even anger. So ensure your music matches the goals of your store or venue.

At Hong Kong Disneyland, the company uses sound to help inspire employees as they start their day.

The "CostuMagic" system allows an employee to check out a costume, and in doing so, plays the well-known "magical jingle" from Tinkerbell when the process is successful and complete.

This playful sound signals the transition for an employee that they're now in character and ready to greet guests!

So the next time you visit Disneyland, know that the company's not trying to push popcorn for breakfast. The theme park's combination of smells, sights and sounds is all part of a complete experience.

### 6. The things your customers eat, as well as what they touch, help determine their overall experience. 

The dozens of kiosks and restaurants in Disneyland echo the park's many themes. You won't find starched tablecloths and fancy meals in Frontierland, for example.

To maintain the image you want to present to customers, you must consciously mold and adapt everything your company does to be in line with that image.

If customers are looking for entertainment, they don't want their illusions disrupted by everyday, banal things. They're looking for an immersive experience, and that extends even to the food they eat.

Thus Disney's decision to tailor its restaurant menus to the different areas of its resorts. You can chow down on roasted turkey legs in Frontierland, or take a leisurely stroll while nibbling salt water taffy at Disney's BoardWalk. The tastes offered match the setting as well as the preferences of customers.

So far, we've covered smell, sight, sound and taste. Now let's consider a guest's sense of touch.

People receive tons of information from how an object feels. Whether we touch an object with our hands, or feet or even our faces, our perception of our environment is intensified. It makes sense, then, to ensure that guests are surrounded by enjoyable and interesting things to touch.

One of the clever ways Disneyland achieves this is through the use of water. The company plays on people's general love of water, especially children, who love the surprise and excitement of being splashed with water.

Disney theme parks hide fountains in unexpected places, to surprise children when they're suddenly squirted with a stream of water!

> _"...to step through the portals of Disneyland will be like entering another world." — Walt Disney_

### 7. Use surveys wisely to learn more about your customer’s habits and even emotions. 

While surveys can be tedious, customer-focused companies understand that they are a vital tool in understanding a customer's needs and offering a customer exactly what she wants.

To really know who your customers are, you need to collect demographic information. Through the smart use of surveys, you can find out a customer's gender, age, income level and so on, and then apply this knowledge to provide appropriate services.

For instance, Disneyland discovered that 25 percent of its guests come from outside the United States. What's more, the park found that Brazilian guests in particular would travel in large groups and stay close together, often singing together during their visit.

To better serve Brazilian guests, Disney trained Portuguese-speaking employees in Brazilian culture and habits. As well as acting as translators, employees were then better able to accommodate large groups of Brazilians, even encouraging sing-alongs!

Surveys go well beyond identifying who people are; they can also reveal how they feel.

_Psychographic information_ can help you understand a customer's mental state, such as her emotions, needs and wants, and importantly, what she may already know about your company.

Through surveys, Disney has discovered that guests are often excited once they enter a park, are thrilled when riding Space Mountain, and are exhausted from walking at the end of a long day.

Knowing how customers feel means that Disney can promote the thrills of their biggest attractions, offer plenty of places for people to sit down and rest, and ensure that cast members welcome guests enthusiastically the moment they enter a park.

### 8. Disney designed three different solutions addressing the various issues of waiting in lines. 

No matter how thrilling a theme park may be, people hate having to spend ages waiting in line to experience a ride or show.

Disney understands this inherently and has developed solutions to deal with extended waiting times.

The first solution is to _optimize the operation of product and service processes_. This involves managing facilities in ways that minimize wait time; for example, a park could open facilities earlier or later than usual, or open some services only for a select group of customers.

Disney designed a program called Extra Magic Hours, in which every day, one of the four areas in the theme park opens one hour earlier and closes up to three hours later than other areas. What's more, the park limits Extra Magic Hours to guests who are staying at Disney resorts on the property.

A second solution to curbing waiting times is to _optimize the guest flow_.

Disney lets guests explore theme parks at their leisure and according to their own whims. This way, guests can themselves choose how much time they want to spend at each attraction.

To help guests manage their day, Disney sets up a tip board at central locations, listing the major attractions and the corresponding wait time for each. Importantly, the display shows slightly exaggerated wait times, the rationale being that a shorter-than-expected wait is preferable to a longer-than-expected one.

A third solution Disney recommends is to _optimize the queue experience_.

No theme park can do away with wait times altogether. So making sure time spent waiting in line is as enjoyable as possible is essential. Disney offers pre-entertainment to guests waiting in certain lines.

For example, at Jim Henson's Muppet Vision 3D show, guests are entertained by a 12-minute pre-show and can watch the antics of various Muppet characters on a bank of television monitors.

### 9. Final summary 

The key message in this book:

**Building and maintaining a successful business involves paying close attention to customer behavior and acting on their needs and wants. This will ensure your company frequently exceeds customers' expectations and keeps service standards high.**

Actionable advice:

**Be the customer.**

Gain a better understanding of the customer experience by asking yourself exactly what a customer pays attention to when she is in your company space. What does she see, hear, smell, touch and taste? How can you use this information to not only satisfy a customer, but also delight her?

**Suggested further reading:** ** _Legendary Service_** **by Ken Blanchard, Kathy Cuff and Vicki Halsey**

_Legendary Service_ (2014) outlines the principles of great service and describes just how you can implement them in your company. As interacting with customers is a key element in almost any business, following this model is a surefire way to improve your company's performance overall.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Disney Institute and Theodore Kinni

Disney Institute trains companies through seminars, workshops and presentations in the practices and knowledge gained and implemented by The Walt Disney Company.

Theodore Kinni is the senior editor for _Strategy+Business Magazine_ and has written on a wide variety of business topics for companies including IBM, Booz & Company and Prime Resource Group.

