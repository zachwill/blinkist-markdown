---
id: 54195eca6439610008290000
slug: beautiful-game-theory-en
published_date: 2014-09-16T00:00:00.000+00:00
author: Ignacio Palacios-Huerta
title: Beautiful Game Theory
subtitle: How Soccer Can Help Economics
main_color: 83C2C7
text_color: 405F61
---

# Beautiful Game Theory

_How Soccer Can Help Economics_

**Ignacio Palacios-Huerta**

_Beautiful_ _Game_ _Theory_ (2014) shows us how applicable economics is to our daily lives by looking at the fascinating world of professional soccer. By examining compelling statistics and studies on shoot-outs, referee calls, and ticket sales, _Beautiful_ _Game_ _Theory_ offers some interesting insights into the psychology of behavioral economics.

---
### 1. What’s in it for me? Discover the economic thinking behind the penalty kick. 

When a soccer player lines up for the penalty kick, how likely is it that he'll score? Does it depend on the player? Or the goalkeeper? Or something else entirely?

As you'll find out, the answer to those questions is quite intriguing, and has to do with far more than the skill of the players. What's more, these questions aren't just interesting for fanatical soccer fans — they are also highly relevant to the field of economics.

These blinks will offer you insight into the economic thinking that plays out on the soccer field and how abstract economic theories can be verified by the most popular game ever. By examining soccer data, you'll learn about new economic models of human behavior and get a completely new view on the importance of soccer matches.

In addition, you'll learn

  * why you should always choose to kick first during a penalty shoot-out;

  * why the referee always seems to prefer the home team; and

  * how to get the most out of your Rock Paper Scissors strategy.

### 2. The minimax theorem explains and predicts how players act and what strategies they use. 

To appreciate how soccer helps us understand economics, we first have to understand John Neumann's _minimax_ _theorem_. His game theory theorem concerns two-player _zero-sum_ _games_, games in which the positive payoff for one player always means negative payoff for the other.

Minimax assumes that players choose strategies that aim to _minimize_ their opponents' _maximum_ possible payoff — hence "minimax." And since we're talking about zero-sum games, this also means that each player also attempts to minimize his own maximum loss.

For example, in a game of Rock Paper Scissors, only one player can win, and the other will lose. Each player's strategy is therefore to minimize the payoff (the win) for the other.

There are two such strategies: pure and mixed. _Pure_ strategies are strategies whereby a player always chooses the same move (like playing Rock every time), whereas _mixed_ strategies employ variations of Rock, Paper and Scissors.

An important hypothesis of minimax theory is that if it's disadvantageous for the other player to know your choice in advance, then you will benefit from choosing random strategies.

For example, imagine that you are playing Rock Paper Scissors against a long-time friend. If you tend to play Paper most often, then your friend can easily exploit this by pursuing a pure strategy — that is, by always playing Scissors.

However, if one person plays pure and the other plays mixed, the mixed strategy will win two-thirds of the time. On the other hand, if both players play mixed strategies, then both will have equal chances to win.

Thus, if two players randomly play Rock, Paper or Scissors, they should each win 50 percent of the games.

Ideally, then, both players should mix moves, because every _pure_ strategy would only have a 33 percent chance of winning.

### 3. Penalty kicks verify Neumann’s minimax theorem. 

The minimax theorem was introduced as far back as 1928, but had never been verified by empirical data. That is, until the penalty kick in soccer.

But why are penalty kicks considered a minimax game? Just like Rock Paper Scissors, there are only a limited number of strategies, which are chosen independently of the opposition's choice.

The game involves a kicker and a goalie, who both have a strategy (kick left, right, or middle; jump left, right, or stay in the middle). The payoff for the kicker is the goal; the payoff for the goalie is a no-goal. The kicker tries to maximize the likelihood of scoring, while the goalie tries to minimize the likelihood.

Importantly, since it takes the ball only 0.3 seconds to reach the goal, the goalie can't react to the kicker's strategy. Instead, he has to decide on a strategy _beforehand_, meaning that both players choose their strategies independently of one another.

Additionally, because each player must choose from a limited number of strategies — left, right, and middle — we can therefore formalize the players' choices, and evaluate whether they verify predictions made by the minimax theorem.

Indeed, the kickers' strategies prove the minimax theorem's predictions about both players' behavior and the effectiveness of their strategies, namely that they choose mixed strategies, that the likelihood of success is the same for mixed strategies and that no soccer player employs pure strategies.

Interestingly, a player's kicking strategy shows that players automatically create _serially_ _independent_ _sequences_, meaning that they neither always kick towards the same corner nor always change corners. In other words, their shot location is random. 

Just as minimax predicts, this strongly indicates that their choice in strategies is not influenced by previous choices or outcomes.

In fact, of the circa 9,000 penalty kicks in international leagues that have been evaluated, the average scoring probability across strategies is 80 percent, regardless of the order in which the kicker shot left, right or center.

As you'll see in our following blinks, just as soccer can help to verify game theory, it can also help us clarify assumptions about other economic models that were previously considered too vague to evaluate.

### 4. A penalty shoot-out combines economics and psychology in a real-world setting. 

Not only does the penalty shoot-out shed light on game theory; it also provides us with a unique opportunity to analyze the psychological factors in competitive behavior.

Economists and psychologists look at the environment in which decisions take place in order to better understand those decisions. A penalty kick, for example, falls under the category of "tournament settings," characterized by competition. Social scientists use these models to analyze other real-life competitive situations, like job promotions, and study what factors influence who wins.

However, economists and psychologists have found tournaments to be insufficient as analogies for real-life competition, as it's hard to observe and measure the strategies behind real-life scenarios. For example, did a candidate manage to earn a promotion because he had a higher incentive (like paying off his mortgage) or because he was more likeable or less stressed during the interview? The tournament model simply can't provide a satisfactory answer.

This insufficiency is due primarily to the lack of clarity in the observation of strategies and outcomes in everyday situations. The results are already difficult to categorize, and seem too vague to use in explaining psychology or emotions.

The penalty kick, however, offers the ideal way of evaluating psychological influence in tournament settings because of how simple all of the variables are; it reduces the entire scenario to only the outcomes (the result of the shot) and the psychological factors, such as shooting first or second.

Moreover, the randomness of the kicking order in a penalty shoot-out makes for reliable data.

In a penalty shoot-out, both teams are automatically part of a random selection process: the coin flip. Thus, the decision as to who goes first — and 97 percent of all professional players chose to kick first — is randomized.

This randomness adds credibility to penalty-kick data, as randomized data can't be easily tampered with.

### 5. Psychological pressure gives advantage to the first-kicking team in penalty shoot-outs. 

Because both teams perform in front of the same audience and because the result of the penalty shoot-out determines who wins the match (i.e., they have the same stakes), we can ask: does kicking order in a shoot-out effect athletes' performance? The result is astonishing:

Consider that between 1970 and 2014, the winning frequencies by team in both national and international competitions were 60.6 percent for the first team and 39.4 percent for the second.

This is because the standard order of ABABABABAB, where A is one team and B is the other, puts tremendous pressure on the kicker of the second team.

In fact, kickers seem to know this: there are only two exceptions in all documented coin tosses in which the winner of the toss didn't choose to go first.

The first exception was Italian goalie Buffon against Casillas, a goalie from Spain. Buffon won the coin toss and decided that Spain should kick first. Five years later, the same teams and the same players went to a shoot-out, and the Spanish goalie chose Italy as the first kicker.

But how do we know that the disadvantage of the order ABABABABAB is purely psychological? Well, studies have shown that if we change the kicking order to something else, like ABBABAAB, we can eliminate the disadvantage almost entirely.

In order to test this, scientists had professionals from Spain's La Liga perform 200 shoot-outs in the new order, and found that the new order had evened out winning frequency: 51 percent for the first and 49 percent for the second team.

The sequence ABBABAAB is called _Prouhet-Thue-Morse_ _(PTM)_ _sequence_, and answers one important question in studying tournament settings: how can you order a sequential competition between two players such that it is as fair as possible?

The order used today, _Strict_ _Alternation_ (ABAB) gives an advantage to one competitor — one that is completely psychological.

Clearly, psychological factors influence players' performance. Our next blink will explore how pressure can influence the other people on the field: referees.

> _"You don't have to be crazy to be a goalkeeper, but it helps."_

### 6. Social pressure on the referee determines the amount of injury time added at the end of the game. 

One of the fundamental tasks in economics is to analyze how people make decisions. The soccer field provides a perfect environment to study how social pressure affects our decision-making.

For one, audience support of the home team applies social pressure to the referee. Because professional games are attended by up to 100,000 people, this pressure can be overwhelming and can lead to bias.

Consider this recent evaluation of data from Spanish soccer matches, which has even revealed that, when the rewards are higher for winning the game (for instance, at tournament finals), the referee's bias towards the home team increases equally. This indicates that the stakes for the home team has influence on the referee as well.

The statistical mean of injury time is 2.93 minutes in the second half of the game, and this rarely changes if a team leads by more than 1 point or if it's tied. However, injury time varies greatly if it is a close contest.

For example, if the home team is _ahead_ by 1 goal, then the extra time added at the end of a half in order to make up for time lost due to injuries or other interruptions is 29 percent _below_ average — either 1 or 2 minutes.

When the home team is _behind_ by 1 goal, injury time is 35 percent _above_ average — somewhere around 4 extra minutes!

So, when the game is close enough for injury time to matter, that is, if the difference in score is greater than 1 point, then the referee systematically favors the home team, meaning that social pressure can unconsciously influence people whose only job is to remain neutral.

However, there are ways to influence the game without actually being on the field, such as cheering, whistling, booing and hooliganism. In our final blinks, you'll learn how these influences affect other fans.

### 7. Individuals will predictably control their fear of a regular situation, based on their costs and benefits. 

In spite of fear's drastic effects on our behavior, it has only very recently been investigated in economic theory. How does risk change the behavior of consumers? And how do they manage this fear?

Fear is the result of the difference between subjective and objective probabilities of risk — people respond to subjective fear of a situation, not the objective risk. First put into an economic model by Becker and Rubinstein in 2013, it put forward two testable hypotheses:

First, that with the right incentives (benefits), people learn to control their fear (costs). The idea is that your willingness to control your fear is dependent on what economic cost/benefit you expect from exerting that control. Therefore, if we can determine how much potential benefit they expect to receive, we can then measure and predict people's reactions to frightening situations.

Interestingly, when someone expects large benefits from controlling one's emotions (e.g., fear), his or her subjective experience will more closely reflect the objective risk.

Consider, for example, that people who frequently travel on airplanes benefit greatly from controlling their fear that they could crash and die. Their subjective belief of how likely it is that they will crash is closer to the objective, statistical risk of crashing.

However, those who very rarely travel by plane often show anxiety or fear — their subjective belief of the likelihood that they will crash is larger than the actual risk.

Indeed, if you frequently find yourself in a given situation then you are less likely to avoid that situation because of its risk.

For example, data gathered from the Palestinian Intifada between 2000 and 2005 showed that the Palestinian attacks in Israel only influenced the Israelis who occasionally used the goods and services that exposed them to risk, such as restaurants, shopping malls, cafés.

In contrast, the demand of frequent users was completely unchanged by terrorist attacks.

### 8. Hooliganism affects fans differently, making it more difficult to explain using statistics. 

Interestingly, the Becker-Rubinstein model can be verified by soccer data by looking at how violence and hooliganism affect ticket sales. The following data is from 45 seasons in the Spanish League, from 1951-1995. One hundred and sixty seven events were recorded that qualified as hooliganism and 334 matches were singled out in a before/after analysis.

Married people, for example, show the strongest reaction to acts of violence and hooliganism, due to their higher costs of overcoming fear. Indeed, studies found a 40 percent difference in the ticket renewal rate between singles and married individuals after one particularly violent soccer season.

In fact, only 51 percent of married people actually renewed their tickets. Why?

Their emotional investment in their marriage outweighed their interest in their hobbies. The opposite was true for singles, who had no marriage in which to be emotionally invested.

Likewise, single-ticket buyers' attendance dropped by 40 percent after acts of violence. Single-ticket buyers attend games only occasionally, and therefore don't benefit from overcoming their fear of violence at soccer games.

By contrast, single season-ticket holders, are hardly affected by violence. In fact, almost all of them, 94 percent, renewed their season tickets after acts of violences, and 95 percent attended games directly following acts of hooliganism.

All of this data confirms the prediction of the Becker-Rubinstein model that controlling our fears depends on our incentive to do so.

But the second hypothesis of the Becker-Rubinstein model did not prove true: that better educated people are able to more accurately assess objective risk.

If we split season-ticket holders that fall into the categories of high education (5-6 year college degree) and low education (high school degree), we see that the attendance rate among the high education group dropped only from 95 to 93 percent after acts of violence. The attendance of the low education group, however, dropped much more significantly — from 92 to 75 percent.

This data of soccer attendance shows us that maybe more highly educated people are not in fact better at assessing objective risk. However, because of other variables that the study does not take into consideration, this example exposes some shortcomings in attempting to link data to causation.

> _"It is not the risk of physical harm that moves people; it is the emotional disquiet."_

### 9. Final summary 

The key message in this book:

**We** **can** **gain** **a** **number** **of** **insights** **into** **economic** **behavior** **by** **looking** **to** **the** **soccer** **field.** **By** **examining** **the** **strategies** **of** **the** **penalty** **kick,** **a** **referee's** **preference** **to** **a** **particular** **team,** **and** **even** **something** **as** **simple** **as** **ticket** **sales,** **we** **can** **learn** **a** **lot** **about** **the** **world** **around** **us.**

**Suggested** **further** **reading:** **_Risk_** **_Savvy_** **by** **Gerd** **Gigerenzer**

_Risk_ _Savvy_ is an exploration into the way we misunderstand risk and uncertainty, often at great expense to our health, finances and relationships. However, if we better understand risk, we can develop the tools necessary to navigate this highly complex world without having to become an "expert in everything."
---

### Ignacio Palacios-Huerta

Ignacio Palacios-Huerta is Professor of Management, Economics and Strategy at the London School of Economics. In addition, he works for Athletic Club de Bilbao, one of Spain's professional soccer clubs, as the Head of Talent Identification.

