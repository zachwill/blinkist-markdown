---
id: 562e12ee38643100070a0000
slug: becoming-an-exceptional-executive-coach-en
published_date: 2015-10-26T00:00:00.000+00:00
author: Michael Frisch, Robert Lee, Karen L. Metzger, Jeremy Robinson and Judy Rosemarin
title: Becoming an Exceptional Executive Coach
subtitle: Use Your Knowledge, Experience and Intuition to Help Leaders Excel
main_color: 2360B0
text_color: 1E5296
---

# Becoming an Exceptional Executive Coach

_Use Your Knowledge, Experience and Intuition to Help Leaders Excel_

**Michael Frisch, Robert Lee, Karen L. Metzger, Jeremy Robinson and Judy Rosemarin**

_Becoming an Exceptional Executive Coach_ (2012) aims to deepen your understanding of what it means to take on a coaching role. These blinks outline the core elements of coach-client relationships to present a flexible and rich approach to executive coaching.

---
### 1. What’s in it for me? Discover how to be an exceptional executive coach. 

Even people with high-level corporate jobs sometimes need guidance and support. And in these moments, who better to call on than an executive coach?

An individual isn't just born being a successful coach; it takes a certain set of skills and a lot of professional experience. In these blinks, you'll discover the essential steps in a coach's journey to becoming truly exceptional. 

From learning about a client to putting what you've learned into a tangible action plan and reaching an end that pleases everyone, you'll learn what it takes to excel as an executive coach.

In these blinks, you'll also discover

  * how to question in 360 degrees;

  * how one man used developmental goals to stop micromanaging; and

  * how to deal with the demands of hesitant leaders.

### 2. Personalize your style of coaching by learning from experience. 

Remember what it feels like to start a new job? Stepping out on a new career path can be daunting, and many of us look to coworkers to learn from their examples. 

But if you're starting a coaching career, it's a different game altogether. 

Why? Because good coaching means engaging executives in a process of self-discovery, and there's no formula for this.

Instead, it's all about creating your own unique coaching style. Shaping what's called your _personal model of coaching_ helps you go beyond being a cookie-cutter coach to becoming exceptional. 

But what does it take to create a personal model of coaching?

It all starts with self-reflection. What kind of person are you? What do you bring to the table in organizational contexts? Which coaching concepts and techniques would you prefer as a client? 

Note your answers down in full sentences, so you can refer to them later. For instance: "I'd like to have a result-oriented process, where I help my client discover solutions to their challenges."

As you begin coaching, document your experiences in terms of your strengths, your weaknesses and your boundaries as you build relationships with clients. Keep track of the techniques you implement — which ones work and which ones don't. 

Reflecting on yourself and your work will help you discover what makes you stand out among other coaches. The unique aspects of your coaching style make up your _personal model_. 

Though it takes a considerable amount of time, effort and experience before your personal model is fully formed, it's worth the wait. 

Once you've developed your model, you'll have a firm foundation from which you can grow and evolve as you gain more experience and become a more confident coach. 

So how do you start? The first element in your personal model is _engagement management._ Why is it so important? Find out more in the next blink.

### 3. Good engagement management keeps clients, sponsors and coaches on the same page. 

To provide exceptional executive coaching, everyone involved has to be invested. So how is this achieved? 

You need to manage _engagement_ throughout the entire coaching process. This means considering questions such as: How will the contracting be conducted? Who will be involved? What are some possible challenges we'll face along the way?

Whether written or oral, a coaching _contract_ should include the following elements: the steps involved in the coaching process, the time frame, the frequency of coaching meetings and confidentiality agreements. 

Having a clear contract allows for expectations to be set up front, _before_ coaching begins. This is far better than running into problems and misunderstandings later on. 

In general, there are three _parties_ to contracting that share overlapping interests: client, sponsors (typically a client's manager or HR manager) and the coach. All three parties can and should have a hand in shaping the coaching contract. 

Another success factor in managing engagement is _alignment_. Alignment occurs when everyone in the process — the coach, the client and sponsors — share the same vision for engagement in the coaching process. 

As a coach, you are an advocate for greater alignment between your client and sponsors. As an example, Mark, a coach, takes on Cindy, the head of sales, as a client. Mark was called on to coach Cindy as there have been a series of complaints about her behavior. 

However, Cindy's sponsors aren't keen to be as involved in the coaching as Mark would like. Mark reminds them that he needs their engagement. 

Yet he's got to do this without getting frustrated. Instead, Mark should encourage the active participation of sponsors in the process, even if it may be insufficient at times. This keeps the sponsors engaged, which allows everyone to stay on track throughout the coaching progress.

### 4. Client relationships benefit from preparation and an articulated agenda. 

What makes you want to open up to a total stranger? If you're like most people, your openness and engagement depends on the person you're with. 

Coaches should therefore always remember to prioritize their unique relationship with their client above formulaic strategies.

Though each coach has her own particular style, there are several general points to guide a fruitful coaching relationship. 

These points are: _trust_, based on your client's safety and confidentiality; _honesty_, created through your sober, constructive observations; _caring_, which arises through your empathy; and _credibility_, which stems from your professionalism. 

To establish a positive, productive relationship, you need to prepare for each session. Preparation needs to allow for openness and responsiveness, but should also instill a feeling of progress. In other words, you need to be both open and focused. 

This is not an easy balance to strike! To work toward this, you should identify _session goals_ for your client and for yourself, too. 

For instance, your goal could be staying more focused on key issues and not getting sidetracked. As for your client, a goal could be that the session produces at least one possible theme of development. 

Finally, setting an _agenda_ at the beginning of each session is important for a good relationship. At the start, ask your client how she feels, inquire about her actions and thoughts since the last coaching session and then ask her to tell you what she would like to cover.

By sharing the responsibility of setting an agenda, you signal that you have confidence in the client's ability to overcome her own challenges, and establish how you should work together in the future. This helps you present a credible image which reassures and motivates the client, while building your own coaching confidence all the while!

### 5. Collect both quantitative and qualitative data to truly understand your client. 

You can't help someone flourish without having real insight into who they are and what they want. So how can coaches ensure they know their clients better than they know themselves? 

Good coaches gain insights about clients by _collecting data_. This helps a coach not only to understand a client but also how the client is perceived and experienced at work. 

Using several data-gathering methods can be especially helpful if a client has limited self-insight or is really unsure about which improvements or changes to work on.

There are two broad categories of data that should be collected: _quantitative_ and _qualitative_. Quantitative data comes from standardized instruments that measure a client more specifically. This type of data can be collected through _self-assessment_ s completed by the client, for instance. 

Another way to collect quantitative data is to use _360-degree questionnaires,_ which gather feedback from a client's immediate colleagues such as managers, peers and direct reports. These questionnaires are excellent for gleaning the perceptions of a client on a wide range of skills and leadership dimensions. 

Unlike quantitative data, _qualitative_ data comes from personal observations that others have made about the client, as well your own impressions. In executive coaching, the most frequently used qualitative-data gathering method is interviewing a client's colleagues. 

To gain the data you need, you can ask the interviewee to describe the contexts in which he and your client interact, and his observations about what your client does well and where she could improve. 

The goal of this informational interview is similar to other assessment methods, in that you are able to gain a broader, deeper understanding of a client. 

So it's crucial that you design questions by carefully considering which information will be most useful to you and your client.

### 6. The development planning phase is crucial in translating coaching goals into action. 

After gaining some insight about your client, you should have an idea of where change has to happen, and why. 

Your next step is _development planning_. This is where the need for change is translated into tangible and actionable goals. 

These goals change shape as a coaching relationship matures. To begin, goals emerge from _felt needs_, the initial wishes articulated by sponsors or clients. These then develop as _negotiated goals_, explored through a client's history, feedback and self-perception, to ultimately become _designed objectives_, shaped from assessment results and other analyzed and distilled data. 

Ankit, a coach, was hired before a company promoted a manager named Howard, because he was unfortunately prone to micromanaging employees. This was the _felt need_ of the organization. 

Ankit and Howard negotiated that Howard should empower his employees to take more responsibility to overcome his micromanaging. This was the _negotiated goal_. 

Finally, designed objectives are the goals that the client will follow through with even after the coach is gone, in this case: build stronger relationships to support greater empowerment.

Once the designed objectives are clear to you and your client, it is time to create a _written development plan_. 

Typical sections in a development plan include a brief statement about process, client strengths, and each designed objective followed by action ideas for progress toward the objective. 

Development plans are created with the expectation that they will be shared with sponsors, so they need to be clear also to those who have not been part of the process. Strong development plans become an invaluable resource as the coaching process continues.

### 7. Patience and motivational interviewing make coaching senior leaders a little easier. 

Once we achieve success, most of us prefer to give others advice rather than receive it. Coaches often find themselves facing considerable challenges when tasked with coaching successful clients. 

Coaching senior leaders is a challenge that requires both significant skill and flexibility. 

Patience and motivational interviewing make change less threatening for senior leaders, who are often resistant to letting go of a current set of behaviors. It takes more time for such clients to accept change, meaning you have to remain patient, as your credibility may be tested more than it is at lower organizational levels. 

Remember that your patience in responding to a senior client's reluctance also conveys positivity and hope. 

A good way to deal with hesitation is _motivational interviewing_. This method relies on broad, open questions to explore with the client both current and changed conditions. 

A few questions might include asking your client what change means to her, and what the pros or cons of change would be in her particular situation. Also, you might want to explore what she thinks she would need to give up to make a change. These types of questions can make change seem less threatening. 

Also, senior leaders are likely to treat all outside professionals as consultants, so expect to be lumped in this category as well. Senior leaders may demand counsel from you because they may believe that a coach is supposed to help them in all aspects of their job. 

While coaching and consulting can be synergistic, the activities are quite different. In general terms, coaches ask in order to explore, while consultants ask in order to tell. In this situation, coaches need to focus on facilitating discovery. 

If your client is facing a challenge and asking for counsel, try to point out the development potential in the challenge. This will support the client's growth without reducing her responsibility for progress.

### 8. Considerate closure requires planning; don’t let the end be a surprise for you or your client. 

Everything comes to an end, even successful coaching relationships. It's highly recommended that you and your client openly discuss the end of the process, and what it means for the future. 

A considerate closure depends on what you do to anticipate and plan for the end of coaching. While the closure date can change through mutual consent, it is important to avoid letting the final session be a surprise either to the client or to you. 

Targeting a final session makes the ending tangible and prompts discussion of both plans and feelings about those plans. You can use final sessions to ask your client to reflect on a number of issues, such as what she primarily learned through the process, what worked and what could have been better, or how she will focus on development in the future.

Ending coaching is also an opportunity for you to reflect on your own experience, identify gaps in your abilities and consider future engagements. After a session has ended, clients and sponsors can be surveyed more formally about their satisfaction and the results of the process on the client's behavior. 

Evaluation is usually framed in terms of answering the question, _How effective was coaching in achieving the results envisioned?_

In evaluating the impact of intervention, psychologist Donald Kirkpatrick has identified four variables. These are: _reactions,_ which measure client's and sponsors' satisfaction; _learning_, which measures what the client learned from the process; _behavioral changes_, which measure the change seen by others; and _organizational results_, which measure tangible benefits to an organization, such as increased revenue or more customers. 

Try to always create an end process that instills positive closure, confidence in the future and a support structure that encourages continuing development. In other words, give your coaching efforts the positive ending that you and your client deserve!

### 9. Final summary 

The key message in this book:

**Every person is unique, and so is every coaching process. By considering the vital elements of coaching, from outlining goals, managing engagement and encouraging development, you can develop an individual style of coaching that will have a positive and enduring impact on your clients.**

Actionable advice:  

  

 **Be present.**

Make sure to be physically present when your client makes a presentation, or find a way to discreetly join in on meetings. This gives you a real opportunity to observe your client while interacting with colleagues, and you'll gather concrete examples that your client will be able to relate to more easily. 

**Suggested** **further** **reading:** ** _Challenging Coaching_** **by John Blakey and Ian Day**

_Challenging_ _Coaching_ argues that traditional coaching is limited by its therapeutic origins. Blakey and Day introduce a better alternative for the twenty-first century business environment: the FACTS approach. Its emphasis on demanding challenging _Feedback_, _Accountability_, _Courageous_ Goals, _Tension_ and _Systems_ _Thinking_ drives a client to achieve their full potential.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michael Frisch, Robert Lee, Karen L. Metzger, Jeremy Robinson and Judy Rosemarin

Michael H. Frisch, Robert J. Lee, Karen L. Metzger, Jeremy Robinson and Judy Rosemarin are all New York City-based executive coaches and trainers with substantial corporate, consulting and academic experience.

