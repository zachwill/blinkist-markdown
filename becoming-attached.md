---
id: 54101b5e3635620008260000
slug: becoming-attached-en
published_date: 2014-09-09T00:00:00.000+00:00
author: Robert Karen
title: Becoming Attached
subtitle: First Relationships and How They Shape Our Capacity to Love
main_color: 915C59
text_color: 784C49
---

# Becoming Attached

_First Relationships and How They Shape Our Capacity to Love_

**Robert Karen**

This book is about the importance of children's first relationships, especially with their primary caregiver, typically the mother. It offers insights into the ways that attachment can positively or negatively affect children's development, and offers a great deal of scientific research on important findings concerning attachment.

---
### 1. What’s in it for me? Learn how attachment affects a child’s psychological development. 

Theories about parenting and childhood development have always been controversial, especially in recent decades as the field of developmental psychology has grown rapidly. With so much information available, some of which is contradictory, it's difficult to know which findings to trust.

These blinks depict childhood attachment from a psychological point of view. They provide insights from top psychologists in the field and explain some of the most important studies done on childhood attachment. You'll learn how children's first relationship with their primary caregiver has a profound impact on their development. These insights might help you provide a healthier environment for your own children or understand your own childhood experience.

You'll also learn

  * about the controversy of day care and how it's not always harmful;

  * how our relationship to our own parents influences our relationship to our children;

  * how separation from parents for only a few days can negatively affect a child; and

  * how to classify different types of parent-child attachment styles.

### 2. The strong bond between children and their parents or other caregivers is called attachment. 

How much can you remember about your early childhood? Probably not much. But you'll definitely remember the person who cared for you: your _primary_ _caregiver_. In most cases, children's primary caregiver is their mother. The special connection between children and their main caregiver is called _attachment_.

Attachment is a very complex process that children begin in their first year of life. It's a basic biological need for a simple reason: children can't survive on their own.

Attachment develops in different phases. In the first weeks of their lives, newborns don't have any preference for their mothers. But, after a few weeks, they can differentiate between faces and recognize their mother or other primary caregiver. As they continue to grow, they show signs of distress if their caregiver isn't nearby. This is the start of attachment.

Researchers have studied monkeys to gain some understanding of attachment. In one study, they separated baby monkeys from their mothers right after birth and raised them in a cage that had two fake "mothers" made out of wire. One of the "mothers" had a soft cloth wrapped around it, and the other had a feeding nipple. They wanted to see which of the mothers the baby monkeys would prefer.

In the end, the baby monkeys spent most of their time cuddling with the cloth mother, and went to the other one only for food. Their needs for warmth, love and attachment were more important to them than eating.

Although attachment is something that we've all experienced, the study of it is rather controversial. There are many disputed theories, and people within the field often try to push parents into a certain parenting style. In the following blinks, we'll learn more about these different theories.

### 3. The mother is a secure base from which children can explore their environment. 

Think back on your childhood. Did you go to your mother when you were scared of something? No matter how scary a situation was, it was always better when your mom was around, right?

That's because your attachment to your mother (or other primary caregiver) provided a secure base for you.

Children rely on that base as they start to understand the world. Even when they first start exploring their environment by crawling, they stay near their mother. Babies enjoy moving around and looking at interesting things as long as they're not alone. But usually, once their mother leaves, they'll stop exploring and cry until she comes back. They need their attachment figure in order to feel comfortable and safe.

Because the baby monkeys with the wire mothers had provided so much insight into attachment, researchers used them in another attachment experiment afterwards. They moved one baby monkey to a new cage with its wire "mother" and another to a new cage completely alone.

The monkey that didn't have its mother just sat in a corner, terrified, and cried continuously. The monkey who did have its "mother" was scared initially, but eventually felt comfortable enough to leave its mother and explore the cage. Because the mother provided a secure base, the baby monkey could shift its attention to its surroundings.

Like the baby monkeys, toddlers also rely on their mothers to provide a base for them. But what about those situations that all parents fear: like a child running off in a crowded place. Why would any child choose to wander so far from its base?

Well, researchers call this _negative_ _attention_ _seeking_. The children want to test how far they can go before their mothers call them back — they want to test the limits of their secure base. Essentially, it's their way of figuring out how dangerous a situation has to be in order for their mothers to protect them.

### 4. A young child experiences problems when separated from its mother. 

Imagine your young child were so sick you had to leave him in the hospital and weren't allowed to visit. While that might seem strange to us today, it was a reality for many parents just a few decades ago. Back then, hospitals didn't let parents stay with their children for fear of infection. In doing so, they unknowingly caused a great deal of problems.

In the beginning of a child's life, even just a few days of separation can have serious consequences for the child's development. James Robertson, one of the top researchers in the field of mother-child separation, knew this and pushed hard to have hospitals change their policy to start allowing parents to visit their children.

To educate the hospitals on the matter, Robertson released several film reels of real children and their suffering in hospitals. In one film study, a two-year-old girl stayed in a hospital for only eight days, but in that time changed from being happy and well-behaved to resentful and dull. Her parents were only allowed to visit her for 45 minutes every other day.

In the first few days, she cried continuously. After that, she started to ignore her parents when they came for their visit. She continued suffering from anxiety and irritability even after she went home.

Children that young aren't able to understand that their separation is temporary: if their caregiver leaves them for even a few days, they'll perceive it as a permanent loss. If young children are hospitalized and kept from their parents for a longer period of time, the consequences can be even worse. There can come a point where children don't accept their parents; some even stop eating.

Although it took a while for hospitals to come around, thankfully, after learning about these studies, they eventually began allowing parents unlimited visiting time.

### 5. There are three attachment styles that describe someone’s attitude and behavior towards attachment. 

If you've ever studied psychology, you might've encountered the name Mary Ainsworth, who's played an instrumental role in the field of child psychology. In her analysis of attachment, she identified three different attachment styles to classify the bond between parent and child.

The first, _secure_ _attachment_, is the most common. It's characterized by an emotionally available mother and an easy-to-handle child. These mothers respond quickly to their children's needs. They attend to their children in a warm and loving way, and are consistently available.

Children of these mothers can use their mothers as a secure base. They cry the least and are the easiest to satisfy.

The next attachment style is _ambivalent_ _attachment_. Ambivalently attached children are very needy, and worry a lot when their mothers aren't available to them. Mothers in this attachment style usually behave in unpredictable ways. They're available sometimes but completely ignore the child other times. These children have a very difficult time dealing with any separation. They cry often, and it's difficult for their mothers to calm them down again.

The final style is _avoidant_ _attachment_, characterized by very little interaction between mother and child. Naturally, it's unhealthy. Here, the parenting style of the mother is inconsistent. She encourages too much independence, and shows the child that she doesn't want her to be needy or clingy.

An avoidantly attached child and her mother have an inconsistent relationship. The child might sometimes be unresponsive to her mother, but she might also get angry with her for no reason.

The attachment mother and child form in the child's early years tends to stay consistent over the years. Children who are classified with a certain attachment style usually show traces of the same style into their adulthood as well.

### 6. You can examine either the child or the parent to assess the attachment style. 

Now that you know the three attachment styles, how can you determine which one someone has?

To help answer this question, Ainsworth developed something called the S _trange_ _Situation_.

She built a playroom in a laboratory to observe how children would react in an unfamiliar environment. They entered with their mothers (their secure base) and then started to explore the toys. Their mothers would then sit down on a chair and, a few minutes later, a stranger would enter the room. The mothers would then exit the room, leaving the child alone with the stranger.

Typically, the children would start to cry and it wouldn't help if the stranger tried to calm them down. A few moments later, the mother would re-enter the room. Ainsworth would then determine the children's attachment style by their reaction to seeing their mother reappear.

The securely attached children were happy to see their mothers again and calmed down easily.

The ambivalently attached children were more distressed when their mothers returned, and often seemed angry.

Finally, the avoidantly attached children completely ignored their mothers when they returned.

The Strange Situation is not only useful for assessing children's attachment styles but also those of adults. Often, especially in therapy, it's important to understand the way adults were attached to their parents in their childhoods.

The _Berkeley_ _Adult_ _Attachment_ _Interview_ is a questionnaire often used to determine this. It includes questions about the person's childhood, including their family situation, the way they felt about their parents, and their painful childhood memories.

This is especially useful for parents-to-be because a person often passes on the attachment style they had with their parents to their own children. Understanding adults' past relationship with their mother or parents can shed light on their own behavior as parents.

### 7. Parental behavior has a great impact on the attachment style of a child. 

So what causes these differences in attachment styles? Some psychologists have suggested that it's partly genetic. Most research, however, has shown that there's a much more reliable indicator.

The attachment style adults had with their own parents is a good predictor of what style they'll have with their children.

In one study, the Adult Attachment Interview was used on expecting mothers. Researchers used the results of the womens' tests to predict what attachment style they'd have with _their_ children. The predictions were correct in 75 percent of the cases.

For example, one woman seemed to have a rather realistic image of her future as a mother: she expressed excitement, but also fear and worry. Her awareness that difficulties might arise made her much better at dealing with them when they did. She could thus behave in a way that ensured her child would develop a secure attachment to her.

The point is, there's always room for improvement in parenting. When mothers or parents are educated on childcare, it can improve their attachment style with their children.

One research team examined a large group of children born into low-income families who had been identified as irritable. They gave half the mothers free counseling sessions, where their children's behavior was explained to them.

The counseling had a big impact: after the study, 68 percent of children whose mothers had been in counseling were classified as being securely attached, whereas only 28 percent in the control group were. Even if a mother has difficulty with her child, she can improve her attachment relationship if she has the proper resources.

### 8. Day care in early infanthood is not necessarily harmful. 

There will always be debates about the best ways to raise children. The topic of day care, however, is particularly controversial and one of the most-discussed topics in the history of developmental psychology.

For a long time, day care was considered harmful in the first year of infanthood. Discussions about day care first arose when women began fighting for their right to work. Before that, there had not been any need for day care, as women stayed home to care for their children. Opposition to day care was thus an excuse for people to speak out against the women's movement.

Some early studies showed that day care, especially in low-quality community care centers, led to anxiety and aggression in children. Day care opponents also tried to cite earlier findings proving that it was better for children to have only one attachment figure: their mother.

However, if children are put into high-quality care centers, there's no reason to be concerned.

Advocates of day care addressed these concerns by emphasizing the importance of having high-quality day care. They also demonstrated that the problems some children experienced in day care weren't due to the day care itself but to their parent's reasons for putting them there.

Children who are put in day care typically come from houses where the parents are very busy. If the parents create a stressful environment at home, that will affect the child whether or not they're in day care.

As long as caregivers in day care centers can build healthy and reliable relationships with the children, there's no reason that day care should be considered an inherent danger to children, or even infants.

### 9. Parents’ interaction with their children is often affected by their attempt to deal with their own childhood. 

If you're a parent, how did you feel before you became one? If you're not a parent, how would you feel if you knew you had a child on the way? Excited? Scared? Confused?

Well, you should try to be aware of those feelings, because they will influence the way you bring up your child! Your attitude toward parenting, and your own past experience as a child have a big effect on your own parenting style.

People tend to replicate their own parent's behavior — even if they hated it when they were young. Most expectant parents declare they'll never repeat the same mistakes their parents made. Yet many people don't really understand why they were dissatisfied with their own upbringing, which can lead them to repeat their parents' mistakes anyway.

One expectant mother interviewed for a study expressed that she had already thought about methods she would use to control her baby. It was clear from her interview, however, that she'd struggled with her mother controlling her when she was a child, although she hadn't consciously realized that. She was therefore about to unknowingly make the same mistake as her own mother.

When parents aren't able to reflect on their own experience with _their_ parents, they might struggle with dealing with their children at times. Ultimately, a parent's job is to teach his or her children how to navigate the world without them. And children must learn to understand their emotions so they'll know how to behave in different situations.

So when parents can't understand their _own_ emotions, it's harder for them to teach children how to understand theirs. Good parents have to understand themselves before they can effectively raise another person.

> _"The problem we have as parents, is not usually a lack of love [...], but an unwillingness to face who we are."_

### 10. Final summary 

The key message in this book:

**A** **child's** **first** **relationships** **have** **a** **tremendous** **impact** **not** **only** **on** **the** **rest** **of** **their** **childhood,** **but** **on** **their** **adult** **life** **as** **well.** **Children** **need** **a** **secure** **and** **healthy** **relationship** **with** **their** **mother** **or** **other** **primary** **caregiver** **so** **they** **have** **a** **base** **from** **which** **they** **can** **grow** **and** **explore** **the** **world.**

Actionable advice:

**Be** **consistent** **with** **your** **children.**

Children need to be able to rely on their parents to learn right from wrong, and to feel safe in their environment. So be sure to always be there for them and let them know they're loved and safe.

**Suggested** **further** **reading:** **_Selfish_** **_Reasons_** **_to_** **_Have_** **_More_** **_Kids_** **by** **Bryan** **Caplan**

_Selfish_ _Reasons_ _to_ _Have_ _More_ _Kids_ examines the demands of modern parenting and why people today are choosing to have fewer and fewer kids. The author argues that this trend is due to modern parents placing too high expectations on themselves, even when a far more relaxed style of parenting would get the job done just as well and make the whole experience more enjoyable.
---

### Robert Karen

Robert Karen is a clinical psychologist and the author of several successful books on psychology, the most recent of which is _The_ _Forgiving_ _Self:_ _The_ _Road_ _from_ _Resentment_ _to_ _Connection._ He is also assistant clinical professor at the Derner Institute of Advanced Psychological Studies.

