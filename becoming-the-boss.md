---
id: 54cf8171323335000a100000
slug: becoming-the-boss-en
published_date: 2015-02-02T00:00:00.000+00:00
author: Lindsey Pollak
title: Becoming the Boss
subtitle: New Rules for the Next Generation of Leaders
main_color: D92B33
text_color: A62127
---

# Becoming the Boss

_New Rules for the Next Generation of Leaders_

**Lindsey Pollak**

_Becoming the Boss_ presents the potential hurdles for leaders in our modern era and some useful tips on how to tackle them. Pollak explains some fundamental principles that will help anyone to, not only become a leader, but to excel in their new role. Pollak also summarizes some new best practices for managing employees and advises on professional networking and personal growth.

---
### 1. What’s in it for me? Learn how to become a great leader in the modern business world. 

We worship the great business leaders of the past, and rightly so! The likes of Steve Jobs or Bill Gates are role models we should all follow.

However, as well as learning from them, we need to realize that what worked for them in the past may not work today. The modern business world is a unique. First technology is constantly changing: meaning the way we communicate and learn is in constant motion. Secondly the modern workforce is like no other labor force before. Comprised of _Millennials,_ people born between 1980 and 2000, it tends toward tech savvy, self-determined and prepared young professionals, who work many jobs at the same time.

So what does this landscape mean if you want to be a leader?

It means you need to develop leadership skills that focus on the specific nature of the modern world. These blinks will help you do that.

These blinks show

  * why leading others starts by leading yourself;

  * why you should apply for jobs you have no hope of getting; and

  * why you should devote a portion of the work week to untagging yourself from Facebook photos.

### 2. In order to be a great leader, you have to lead yourself. 

What is a leader? Someone who is in charge of a lot of people, right? Sure, but there is another aspect of leadership that is just as important.

In addition to guiding others, you need to be able to _lead yourself:_ meaning that you can structure and organize your life and work as well as you can that of a team.

The following three strategies will help you lead yourself.

First, don't panic! Modern business environments can be volatile. With technological developments and new ideas proliferating all around us, there are bound to be times when you feel afraid. That worry is a normal reaction, therefore learning to handle it is important.

One strategy is to ask yourself, "Do I want this more than I am afraid of it?" Do you want to found your own company more than you fear failing, losing money and moving back in with your parents? If the answer is yes, go for it, as the pros outweigh the cons.

Another way to work on your leadership is to develop your creativity.

If you want to become a successful automobile engineer, it's not enough to just be an expert in your field, as it was before. You should also be familiar with other fields of expertise like computing, design, or even basic business practices. Being familiar with ancillary fields will enable you to see relationships between different information and help you generate creative ideas.

The third strategy is to learn more and work harder.

To do so, you must evaluate your knowledge level. Which skills do you need to improve? Scoping the LinkedIn profiles of people whose careers you admire may help you to determine where to keep learning.

> _"How well you are capable of leading other people begins, first and foremost, with how well you lead yourself."_

### 3. You should build your personal brand by being visible, different and consistent. 

Today, fewer and fewer of us are working for a single company. Most of us have multiple roles and are constantly seeking out more and more.

In order to be a strong contender for new roles, you need to become the _CEO of yourself._ This means crafting your own brand and making sure you stand out among your competitors.

So, how do you create a personal brand?

(1) Be as visible as you can, as people need to be able to recognize who you are. Try making regular appearances at cafeterias, co-working spaces, and, if possible, at media or industry events.

(2) Differentiate yourself from those who offer similar services.

Make it easy for people to differentiate you from others. Do you have a unique selling point? Having areas where you really shine will help you. Perhaps you're an expert in classical literature or in modern China. Having some kind of unique expertise will make you far more attractive to potential clients.

(3) You need to be consistent in order to create a positive reputation; treat people equally and show uniform behavior. Employees value that sort of predictable attitude because it lets them know what to expect.

So, what can you do to promote your personal brand? Introduce yourself in a convincing way in order to set a foundation of confidence and authority. To do so, create an _elevator pitch_. That is, come up with a way to describe yourself to someone in 30 seconds. What important points about you should people know? Doing this will help you effectively sell yourself and your skills to others.

Once you've created your personal brand offline, using these guidelines, you're ready to create your online brand.

### 4. Know which social networks are important for your business, and how to present yourself in them. 

If you want to be an effective leader, you need respect from those around you. In the modern world it can be hard to control when people are constantly tagging you in photos on social networks.

It makes sense, then, that you should make the effort to ensure that no embarrassing photos of you are readily available online.

Obviously, untagging yourself from photos that you'd rather people didn't see on Facebook or other social networks is a good practice. Drunken photos, risque photos or photos in which you are doing something illegal may come back to haunt you in future professional roles. Remember you can always politely request that a photo of you be removed by the person who posted it.

Of course, this doesn't mean that social networks are bad. They can also work wonders for your own brand. You just need to know how to use them.

Social networks are everywhere these days, with some that are independent of your profession and others that depend on your profession. LinkedIn, for instance, is advantageous for almost any profession.

In the entertainment industry, Twitter, Facebook and Instagram are useful tools, as they allow you to promote yourself across a vast crowd.

Since LinkedIn is useful for almost any profession, make your headlines as specific as you can so others can hone in on what you offer. Instead of writing "project manager," you could write "technology project manager / passionate about big data / experienced in startups and Fortune 500 corporations." This way, it's easier for people to quickly learn about you.

When you are satisfied with your profile, ask a friend or colleague to review it and provide you feedback.

### 5. Being a good leader means communicating well with other people. 

If you don't know how to talk to others, you can pretty much give up on being a leader in your career!

To communicate like a leader, there are two things you should know.

First, it's not about you! Contrary to what you might think, leaders are not self-centered. In one study, researchers at the University of Texas discovered that leaders use the word "I" less than non-leaders.

Therefore, be wary of talking about yourself too much. Furthermore, you should focus on your employees in order to keep them motivated. Focusing on yourself makes it harder for you to pick up on the needs of your employees.

Second, avoid _under-communication_.

A survey of HR managers by the US company Accountemps found that lack of open and honest communication was more detrimental to staff morale than micromanagement or heavy workloads. So if in doubt, it's better to communicate too much rather than too little.

Also bear in mind it's not just about how _much_ you communicate. It's also _how_ you do it.

There are multiple ways to communicate information and it's key to gauge the best way for your listener.

If the information is extremely positive, negative, private, or confidential, talk face to face with your employee to avoid misunderstanding. Sharing good news in person can significantly boost your relationship with the employee.

If there's a considerable amount of information you need to get across, use email. The big advantage of email is that information can't be lost.

Should you need a quick response to any information you are giving, opt for either a phone call or an instant messaging tool.

As a leader, knowing what to say to your employees and which channel to choose is vital. The next blink will give you further tips for communicating well.

> _"Every communication is unique and nuances mean everything."_

### 6. Using people’s names, knowing how to handle tough questions, and never gossiping are essential leadership skills. 

None of us are born with perfect communication skills, but anyone can learn how to become a great communicator. Luckily, the necessary skills are fairly straightforward and the following advice will help you.

Let's start with the basics.

One obvious yet often overlooked skill is simply to remember the name of the person you're speaking with! Saying her name is an easy and powerful persuasion tool, as all of us appreciate being addressed by our names. This also means double-checking the names you include in your emails. Making mistakes with names here can be costly and cause people think you don't care about them.

Another basic skill is to make eye contact with the person to whom you're speaking, as it portrays confidence.

In addition to eye contact, your confidence comes across when you respond well to a question to which you have no idea of the right answer.

It's impossible to know the right answer to every question someone might ask you. To handle these situations and build respect and credibility at the same time, you can respond in one of three ways:

  1. Refer to someone else who might know the answer;

  2. Defer the question by saying "I'll get back to you"; or

  3. Ask the person to be more specific. The latter might help you to answer the question yourself in the moment.

Lastly, as all great leaders understand, avoid gossip.

Although gossiping seems to be a universal practice, participating in it can be damaging. Talking about others behind their back, particularly if you are a leader, will make you look disingenuous and will erode your integrity.

### 7. Managers, both new and old, need to be prepared to face the constantly changing business environment. 

You may have noticed a trend lately: managers are younger. Not surprisingly, then, there's quite a few young managers out there who are daunted by their leadership status. But not to worry! The following advice will help you adjust to your new position.

First, it's completely normal to feel insecure about being a manager for the first time. A 2014 study by Deloitte found that only 36 percent of Millennial leaders felt they were ready when they began their role. So, you're not alone.

Next, it's important to make early progress if you want to set up a good rapport with your employees. For example, can you do away with an unpopular "required" report? Minor changes such as this can help, and if you can find a new contact, client or funder, even better!

You should also hone your motivation skills. One effective strategy for employee motivation is to show interest in each employee's personal development. Ask them where they want to be in five years and how you can help achieve their goals.

Next, make sure you are prepared. No matter how much experience you have, the rapidly changing world can catch you if you aren't paying attention.

Notice the technologies your company is using; do you use shared document services, such as Dropbox? Which Customer Relationship Management tool do you use?

Another major shift in the business world is _cultural diversification_. As a manager, you must know how to manage people from different cultures. Thus, cultural awareness is paramount.

One way to learn about other cultures is simply to ask. Usually people from other countries will appreciate the opportunity to tell you about their culture and how business is conducted there.

> _"You're shifting from a level of mastery in your individual contributor role to a level of beginner in your management role."_

### 8. Networking revolves around reaching out to people and using social media effectively. 

Meeting and learning from people who have more experience than you is a great way of advancing your career. How can you get in touch with them? By expanding your network!

More specifically, you need to be able to _network up_. That is, you need to connect to those in your industry who possess the knowledge and experience you would like to have. You can network up by pushing past your networking comfort zone, attending meetings with more senior and experienced people.

Say you want to become an entrepreneur. What about going to a meeting such as the Young Presidents' Organization or the Young Entrepreneur Council, where you can mingle with some of the most accomplished CEOs and business leaders around? Although being among so many experienced and successful people might initially seem intimidating, it will provide you with valuable contacts and insight.

One particularly valuable networking rule is to "always take the meeting."

For example, when you are job-hunting, also apply for jobs that may not entirely meet your expectations. Occasionally, going to a job interview might offer new opportunities that you weren't aware of. There might be another role in the company that matches your interests or experience better, or the company might get back to you in a couple of months if they find a different job that they think is ideal for you.

Another important networking skill is knowing how to use social media in a professional way.

When you want to add professional contacts on LinkedIn, for instance, write a short introduction about yourself, rather than just saying "I want to add you to my professional network." You can mention where you know this person from, why you're adding him and even the projects you are currently working on. The more detail you add, the more likely you are to get his attention.

### 9. Final summary 

The key message in this book:

**The business world is changing rapidly and leadership is changing along with it. In order to be an effective leader, familiarize yourself with leadership fundamentals, such as how to manage employees, use the right networking skills and present yourself in the best way possible.**

Actionable advice:

**To avoid burnout, prioritize.**

On those days when your schedule is bursting at the seams, take 15 minutes to prioritize what is really important. For example, sometimes leaving an important email unanswered for a few hours while you attend a crucial business meeting is better use of your time!

**Manage stress by accepting it and breathing.**

Accept stress rather than resisting it. Resisting it will make it worse. And remember to take deep breaths. You already know this, but do you actually do it? After taking three deep breaths, you'll start to notice how your body is relaxing.

**Don't be afraid when it comes to changing your job.**

At some point in your career, you'll ask yourself whether or not you should stick with your current job or change course. To decide, think back to your childhood dreams. Are you on your way to fulfilling them? If you feel you have gone way off course, you should seriously consider changing your job.

**Suggested** **further** **reading:** ** _Adapt_** **by Tim Harford**

The world is complex and unpredictable, from the inner workings of the humble toaster to the countless problems facing the very existence of mankind. Instead of applying traditional means of approaching problems and managing organizations, we must instead use trial and error. Only by experimenting, surviving the inevitable failures and analyzing the failures themselves, will we be able to adapt to the complex and changing environments we find ourselves in.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Lindsey Pollak

Lindsey Pollak is an expert in workplace issues and the Millennial generation and has consulted for corporate clients such as LinkedIn, where she now serves as an official ambassador. She wrote _Getting from College to Career: Your Essential Guide to Succeeding in the Real World,_ and her advice has appeared in renowned media outlets such as _CNN_, _The Wall Street Journal_ and _The New York Times_.

