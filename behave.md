---
id: 5a9aee2db238e1000882844d
slug: behave-en
published_date: 2018-03-05T00:00:00.000+00:00
author: Robert Sapolsky
title: Behave
subtitle: The Biology of Humans at Our Best and Worst
main_color: DF4F2D
text_color: AB3D22
---

# Behave

_The Biology of Humans at Our Best and Worst_

**Robert Sapolsky**

Humans are complex beings, and human behavior doubly so. Every human act is a result of a myriad of factors, from brain chemistry to social conditioning, that have developed over millennia. In _Behave_ (2017), renowned professor Robert Sapolsky takes a journey into the depths of the human condition, demonstrating the reasons behind the best — and worst — of human behavior.

---
### 1. What’s in it for me? Get a better sense of what causes and influences human behavior. 

Have you ever found yourself asking, "why did I just do that?" Have you ever been shocked by your own callousness or by an inappropriate or vulgar thought running through your mind?

Well, human behavior is influenced by several different factors, some neurological and some environmental.

Some of these influences have developed over thousands of years of human history and civilization; others are chemical processes in the brain that take place milliseconds before a given behavioral act occurs. It turns out that behavior is indeed unique to the individual, but also very much dependent on the societies and cultures that make up the modern world.

In these blinks, we will explore some of the intricate factors that play a part in determining our all-too-human behavior.

You'll also learn

  * why the liberal mind-set is actually just brain chemistry;

  * about the relationship between wheat and patent applications; and

  * which part of the brain is racist.

### 2. To understand human behavior, we must delve into the biology of the brain, culture and history. 

The saying goes that everything happens for a reason, and whether or not you believe this to be the case when it comes to the events that unfold in a lifetime, it's certainly a valid statement when it comes to grasping human behavior.

If we really want to get to grips with the factors that influence humanity's best and worst behaviors, we're going to have to delve deeply into human biology.

Immediately before a given behavior occurs — such as the shooting of a gun — the oldest parts of the human brain kick into gear. We inherited these regions of the brain from our evolutionary ancestors, and these very regions are the ones that process our most basic instincts, like the fear of death. This is just the sort of emotional impulse that might lead a person to pull a trigger.

But, in the seconds to minutes before the fatal moment occurs, the brain has been busy processing sensory data — particularly visual or auditory information — based on its immediate environment. This information from the senses impacts on how we act. In a war zone, for example, heightened sensory awareness of danger makes it much more likely that we'll act aggressively.

However, the brain's response isn't randomly generated. In fact, our behavioral biology is deeply intertwined with human society, culture and history.

Years to decades before a behavior takes place, we'll have grown up in human societies that determine our behavior. Different societies will condition us to behave in different ways; in other words, we are more prone to violence if we were exposed and accustomed to constant violence earlier in our lives.

And if we go back hundreds to thousands of years, we'd find ancestral geographies and ecologies have affected human behavior, for better and for worse.

In short, if we're going to get to the crux of the matter, we're going to have to take an interdisciplinary approach to explain the complex origins of human behavior.

### 3. Two parts of the brain control aggression – and whether or not it's acted upon. 

Right before an aggressive behavior takes place, the brain must make several split-second decisions. Two specific parts of the brain are critical in this process: the _amygdala_ and the _frontal cortex_ ** _._**

The amygdala is located in the largest part of the brain, the cerebral cortex, and is the region associated with aggressive behavior and fear. We know this because brain scans show activity in the amygdala when people are shown images that stimulate anger or fear.

There's also an historical case that suggests just such a link. In 1966, an autopsy was carried out on Charles Whitman, who had murdered his wife and mother before carrying out a mass shooting at the University of Texas.

Whitman's motive was unclear. He'd even left a note next to his wife's body stating he could not "rationally pinpoint any specific reason" for the act.

However, his autopsy found a tumor pressing against his amygdala. In fact, the previously happily married man had told his doctor of headaches and violent impulses prior to the murders, and it's believed that the pressure against his amygdala caused neurological changes that led to his sudden, violent behavior.

The frontal cortex, meanwhile, is responsible for regulating emotions, including aggression, and also controls impulsiveness.

This is demonstrated by the fascinating case of Phineas Gage. In 1848, while working on a railroad construction site, an iron rod punctured his skull and destroyed his frontal cortex.

Amazingly, he survived — but was a completely changed man. He now began to swear, was impatient and was subject to pronounced mood swings. It was later discovered that this was because the frontal cortex is critical in determining what constitutes appropriate behavior and in restraining aggression.

Beyond this one case, there's evidence that large numbers of violent criminals have experienced concussions involving the frontal cortex, and that violent psychopaths generally have less activity in this particular brain region.

### 4. Sensory cues in our immediate environment shape our behaviors. 

Whether it's the sight of a flashing blade or a light touch on your hand, the five senses are constantly sending information to the brain. These signals are known as sensory cues.

It turns out that visual cues, such as people's faces, alter our perception of strangers and our attitudes toward them. Critically, it's been proven that our brains are extremely attuned to skin color.

When images of faces are flashed before the eyes of white participants for one-tenth of a second, the amygdala is more likely to activate if the face comes from a different ethnic background.

Of course, when the face is shown for longer, the frontal cortex's response will rationalize that initial response from the amygdala. The fearful response will be quelled — for nonracists, at least.

But there are nevertheless practical implications to this initial tendency. Shamefully, it has been proven that longer sentences are awarded for the same crime if the defendant has a more stereotypically "African" face, which has led to interesting strategies by defense attorneys.

These include giving their black male clients clunky glasses to wear, as they are more closely associated with white nerds than black criminals. Such a small detail might just be enough to sway a jury.

Equally, auditory cues can unconsciously generate fear.

In a similar face-flashing experiment, music was played. Rap music, due to its association with African-American culture, was found to cause increased amygdala activity. Conversely, white-associated death metal music had the opposite effect.

Knowledge of this fact actually led a black postgraduate student who studied with the author's colleague to whistle pieces by Vivaldi when walking home at night, as he didn't want to appear threatening.

On top of these two types of cues, our behavior toward others is also based on our immediate social context.

It's an observable fact that when men are around women, they're more likely to take risks, as well as buy luxury items rather than spend more on everyday essentials. This male "generosity" could well be seen as the male brain unconsciously giving off mating signals.

### 5. Hormones affect human behavior, but this influence is context-specific. 

It's well known that hormones are chemical messengers that affect different parts of the brain; they form in various glands and are released into the bloodstream. Just think of testosterone, which forms in the male testes or female ovaries.

But the relationship between hormones and behavior isn't as simple as you might think.

Testosterone, contrary to common understanding, does not directly cause aggression, even though studies have shown a clear correlation. For instance, it's well known that castration does lead to a decline in a man's levels of aggression.

Some people might then be tempted to ask: would sex offenders be less likely to re-offend if castrated? But such questions miss the point, because context is a key factor.

Studies have shown that male prisoners measure higher testosterone levels if they display more aggressive behavior.

It's tempting to imagine that the testosterone is at fault, but it's actually the aggressive behavior that causes the increased testosterone secretion.

Nevertheless, since the amygdala is full of testosterone receptors, more testosterone increases aggressive behavior. But it only does so if the individual was already predisposed toward such behavior.

The hormone oxytocin is also interesting, as it's associated with positive feelings, such as trust.

While testosterone increases amygdala activity, oxytocin inhibits it. Therefore, oxytocin levels correlate with prosocial behavior.

This has been observed in studies involving economic games. Increased oxytocin levels in subjects led them to perceive others as more trustworthy. Normally, with low oxytocin, you would expect subjects to distrust deceitful players more as the game progresses. However, when oxytocin was high, subjects still trusted other players regardless of their deceit. Increased oxytocin levels in subjects led them to perceive others as more trustworthy.

But, just as with testosterone, context is critical.

The subjects playing the economic game only had boosted levels of trust if the other players were in the same room. Trust did not increase if their fellow players were anonymous and in another room.

### 6. Childhood and adolescent experiences impact our behavioral development. 

As the brain matures, behavior also changes. Interestingly, even though the brain is 85 percent fully developed in the first two years of life, it's the remaining 15 percent that is essential for determining behavioral development.

Believe it or not, the frontal cortex doesn't finish developing until we're in our mid-20s.

In fact, adolescence is a critical time for brain development. During this time, the immature frontal cortex can negatively influence behavioral traits such as risk-taking or impulsiveness.

Unfortunately, this lack of self-regulation can cause late adolescents and young adults to experience a spike in violent behavior.

Recognition of these biological circumstances means that in some countries, like the United States, the criminal justice system treats young offenders more leniently. There, the Supreme Court ruled in a landmark case that it was illegal to sentence juvenile criminals to life without parole.

Aside from the impact of an immature frontal cortex during this formative period, it's been shown that difficulties in childhood can cause lifelong increases in violence.

During childhood, the human brain has remarkable neural plasticity, which means that a child's brain can absorb information much faster than an adult's. However, this can also result in problems later in life if a child has had repeated negative experiences. Studies show that 33 percent of adults who experienced childhood abuse will themselves abuse their children.

So, if we experience adversity such as poverty or violence as children, this can, in turn, lead to simultaneous neurological overdevelopment and underdevelopment — a very destructive one-two punch.

This happens because adversity results in excessive growth in the amygdala and underdevelopment in the frontal cortex.

And the result is clear. As we've seen, the frontal cortex inhibits the amygdala from making impulsive decisions. So, when the former is underdeveloped and the latter overdeveloped — as is the case with individuals who experience childhood adversity — the outcome is poorer behavioral regulation, as well as a tendency toward violence later in life.

### 7. Cultural factors also explain societal behavior. 

We've seen so far that grasping the physiology of the brain is essential if we want to understand the roots of human behavior.

But there's more going on than neurobiology. After all, if murder is 450 times more likely in Honduras than in Singapore, inevitably, the experience of growing up in one country or the other will have a profound impact on your behavior.

In other words, some behavioral differences are culturally conditioned.

Let's consider the differences between an individualist culture, like the United States, and more collectivist cultures, such as those in East Asia.

Individualist cultures are based on individual rights and personal achievement; conversely, collectivist cultures place the needs of the group above those of the individual.

This plays out in differences in brain activation. Frontal cortices are more likely to activate in Americans when they look at pictures of themselves than when they see those of relatives. East Asians, meanwhile, do not display this impulse nearly as much.

Additionally, such cultural differences affect sensory processing.

If shown an image of a person standing alone in the center of a complex scene, Westerners are more likely to remember the details of the person, while East Asians are better at remembering the scene.

Most obviously, of course, differences in culture result in varying moral systems.

Collectivist cultures tend to place greater value on the needs of the many, which means that more utilitarian moral stances are common. In enacting something like criminal justice, it can be clearly seen that the greater good is held in high esteem.

For instance, collectivist cultures are more willing to imprison innocent persons if it means a riot can be stopped that way.

On the other hand, in cultures like that of the United States, individual rights are more highly valued, and it would be an affront to societal norms if individuals were imprisoned without due process.

### 8. Local ecology and geography influence civilizational and behavioral development. 

Collectivist and individualist cultures across the globe didn't develop by chance; rather, each is dependent on its environment and situation.

That is to say, it's not just cultural background that informs behavior. The ecological and geographical background to this cultural development is also critical, and it's a centuries-long process.

Let's look at how ecology shapes cultural differences in behavior first.

Some East Asian civilizational development depended extensively on rice cultivation, a communal activity requiring collective labor.

In Northern China, by comparison, it's harder to grow rice. There, wheat is grown, which is a much more individual form of agriculture.

As it turns out, divorce is more common in these wheat-growing regions than in neighboring rice-growing regions, and this more individualistic approach to life also means more patents are filed in the north, too.

Similarly, geographical circumstances can explain the United States' propensity toward individualism.

The United States is a country based largely on immigration. Broadly speaking, people who immigrated to the United States often did so because they were seen as outcasts, second-class citizens or even criminals in their native cultures. America represented a fresh start, not only because it offered a path out of their original social context, but a new geographic environment as well.

Additionally, it's important to consider colonial American geographical development. The rapidly growing country had a constantly moving frontier, and it needed immigrants to develop, farm and "civilize" the land. This development cultivated a real sense of individualism, self-reliance and even aggression, and these are common character traits that can still be observed across many of the United States' southern states to this day.

Historically, the region has been pastoral and rural in its geography, which often meant the central government couldn't properly implement the rule of law. As a result, people became accustomed to taking justice into their own hands. Sadly, this has also led to increased levels of violence in the region, which are still in evidence to this day.

### 9. The brain’s neurobiology can inform political views and morality. 

It's no great shock to learn that the brain plays a part in determining political views and morals. But the way it does so will surprise you.

In fact, there's a correlation between neurobiological conditions and whether someone has a liberal or conservative worldview.

One study interviewed people from both sides of the political spectrum. They were asked what they thought about the roots of poverty, and both groups initially proposed similar reasons: poor people were at fault because of their laziness.

However, if given time to rationalize, liberals tended toward a situational explanation. They felt that the system is stacked against the poor.

The author suggests that a similar mentality is also present when non-political topics are under examination. Imagine someone falling over while dancing. Initially, both liberals and conservatives would hold the dancer responsible for his clumsiness. However, over time, liberals would recognize that the difficulty of the dance step was probably also to blame.

More tellingly still, there are observable neurobiological differences between the two groups. Liberals have increased levels of gray matter in their cingulate cortex, the area of the brain associated with empathy. Conversely, conservatives have an increased perception of fear, most likely because they tend to have enlarged amygdalae. Consequently, conservatives are also more anxious in risky situations.

On top of this, neurobiological connections in the brain also inform morality.

If you decide to tell a lie against your better judgment, the frontal cortex will activate. The frontal cortex has to work at full thrust to stop you from being tempted to tell the truth. This happens because simply telling the truth is easier for the brain than strategic deceit.

Conversely, the frontal cortices of very honest individuals do not activate even when they're given the opportunity to deceive. After all, if deception isn't something they'd consider doing in the first place, then there's no requirement for the frontal cortex to restrain the impulse.

### 10. Empathy and compassion aren’t as closely connected as is often thought. 

If you witness a needle being pushed into someone's finger, your response will be physical. You might even find your own hand closing up tightly as an empathetic response. This type of reaction is telling because it shows that empathy is actually closely connected with avoiding pain.

When you perceive others' pain, the _Anterior Cingulate Cortex (ACC)_ is activated. It's a region of the brain that's linked to both the frontal cortex and the amygdala. The ACC is responsible for helping us learn to fear observable bad experiences. These neurological connections indicate that empathy actually has more to do with self-preservation than with a desire to help others.

Levels of empathy are also contingent on sensory factors. Even the visual perception of someone's "race" can affect empathy. Remember that person being pricked by a needle? As we know from earlier blinks, if you see a person of a different ethnic background, your fear-inducing amygdala is more likely to activate, making you less likely to empathize with him as a result.

This, again, has real-world consequences. We now know from an interesting study that if you want to be compassionate, it's best that you don't actually try to empathize.

In the study, two parallel training sessions for volunteers took place. One session was focused on empathy, and the other on compassion.

In the former, volunteers were asked to feel the pain of a distressed subject. The resulting amygdala activation led to anxiety and negative feelings.

In the latter, they were asked to feel warmth toward the distressed subject, and were explicitly told to avoid empathizing.

This time, the amygdala didn't activate, but the frontal cortex did, making the volunteers exhibit positive and prosocial emotions.

This shows that although we might tend to associate empathy and compassion with each other, they are in fact articulations of two different parts of the brain, each activated by different reactions to seeing others in pain.

There's no escaping it: we might like to think that human behavior is easily predicted, but it is a highly complex and multifaceted field. From societal and historical conditioning, right through to the smallest neurological activations, the reasons behind human behavior are manifold and varied.

### 11. Final summary 

The key message in this book:

**Human behavior is linked to both brain chemistry and the society in which we live. Whether it's behaving aggressively or feeling empathy toward others, different parts of the brain are activated when we carry out these behavioral acts. Only by understanding how these behaviors come about can we truly understand what it means to exist and function in a society.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How Emotions Are Made_** **by Lisa Feldman Barrett**

_How Emotions Are Made_ (2017) challenges everything you think you know about emotions. From learning how our brain registers anger, fear and joy to how we think about these emotions culturally, you'll come away with a new understanding of the ways in which emotions are created and how their scope is determined by society at large.
---

### Robert Sapolsky

Robert Sapolsky is the John A. and Cynthia Fry Gunn Professor of Neurology and Neurosurgery at Stanford University. He has also written other highly acclaimed and popular science books including _The Trouble with Testosterone_ and _A Primate's Memoir._

