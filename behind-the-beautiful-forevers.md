---
id: 53fc7fb437316200082b0000
slug: behind-the-beautiful-forevers-en
published_date: 2014-08-26T00:00:00.000+00:00
author: Katherine Boo
title: Behind the Beautiful Forevers
subtitle: Life, Death and Hope in a Mumbai Slum
main_color: EDEB42
text_color: 6E6D1F
---

# Behind the Beautiful Forevers

_Life, Death and Hope in a Mumbai Slum_

**Katherine Boo**

_Behind_ _the_ _Beautiful_ _Forevers_ describes life in the Annawadi slum in India, close to Mumbai's international airport. These blinks tell the story of families who live in squalid conditions but still dream of a better life, even though the odds are overwhelmingly against them.

---
### 1. What’s in it for me? Take a walk in an Indian slum. 

Have you seen the movie _Slumdog_ _Millionaire_? These blinks dive into the same world: the slums of Mumbai.

These blinks will take you on a personal journey through Annawadi, a slum in one of the world's biggest and most complicated cities. Its inhabitants dream of better lives, only to have those dreams shattered by the slightest twist of fate. You'll read about indifference, suicide and corruption, and begin to wonder what you would be willing to do to escape if you found yourself living alongside the Annawadians.

In these blinks you'll learn

  * why one child who lost his hand at work felt the need to apologize to his employer;

  * how hospitals sell slum dwellers the medicine they are supposed to provide for free; and

  * why slum dwellers take such pride in voting in elections.

### 2. In the slum of Annawadi, the smallest coincidence can thwart your attempt to make a better life for yourself. 

All people have hopes and dreams for the future. This is true even for the inhabitants of the illegal settlement — or slum — of Annawadi, next to Mumbai's international airport. But their dreams can be shattered by something as simple as a neighborhood disagreement.

Meet the Husain family, who are fairly well off by Annawadian standards, earning up to eleven dollars a day with their garbage business. Their dream is to buy a plot of land on a legal settlement and build a new home. In fact, they have already paid a deposit for a plot.

But then, one day as they are renovating their home, the Husains accidentally shake a brick wall that they share with a mentally unstable neighbor, Fatima. The only damage is some rubble falling into a pot of rice Fatima happens to be cooking, but since she is jealous of the Husains' "wealth," she claims her home has been destroyed and demands compensation.

The Husains' refusal sends Fatima into such a rage that she sets herself on fire. At the hospital, she receives grossly inadequate treatment for her burns and subsequently dies.

The tragedy is further compounded when several members of the Husain household are arrested and falsely charged with the _murder_ of their neighbor.

Now the Husains begin to see their meager savings disappear.

First, since the local justice system is wholly corrupt, they need to pay various bribes.

Second, the neighbors are also aware of the Husains' supposed wealth, so they demand payment from them just to tell the police the truth about what happened.

Third, when the Husains are jailed before their trial, they lose their income because they can no longer work.

Fourth, they have to sell a part of their original hut to pay their lawyers, further hurting their garbage business.

And finally, since they cannot afford to buy the new plot of land for which they'd paid a deposit, it is sold to another family. The Husains are not reimbursed.

### 3. Diseases and health ailments plague the slum due to the unsanitary conditions. 

Though the Husains are considered well off by their neighbors, they too suffer in the harsh living conditions prevalent in the slum, which spawn a variety of health problems for inhabitants.

Animals are allowed to roam around freely, feeding on what they want. For instance, when Abdul Husain, the family's eldest son, is at work sorting garbage, goats will come up to him to lick the labels off discarded plastic bottles. But because the goats drink out of a nearby lake of sewage, they also suffer from diarrhea, which they then spread around the slum, infecting others.

This sewage lake is — not surprisingly — a source of many diseases. Because it's an illegal settlement, the slum has no sewers, so Annawadians dump all their waste into this foul lake. One time Abdul even spotted 12 goat carcasses rotting in the water. The lake is so vile that when pigs or dogs sleep in the shallows, their bellies get stained blue by the water.

Mosquitoes breed in the lake and then infect the slum inhabitants with malaria. But mosquito bites are not all Annawadians need to worry about, for they are also frequently bitten by rats. Abdul's younger brother Lallu has plenty of rat bites on his cheeks to prove this, and in the humid climate they get infected all too easily.

Finally, the very air in Annawadi is full of sand and gravel blowing in from a nearby concrete plant. Many inhabitants like Abdul's father Karam suffer from asthma, and the constant coughing and sneezing caused by the dust promotes the spread of tuberculosis, which devastates those who suffer from it.

### 4. People living in slums have few to no basic human rights. 

Can you imagine living in a place where basic human rights mean nothing? For the slum dwellers of Annawadi, this affects every aspect of their life.

Workers are so scared of their employers that they might as well have no rights at all. For example, when one little boy loses his hand to a shredder at the garbage recycling plant where he works, he actually apologizes to the owner. He says: "Sa'ab, I'm sorry. I won't cause you any problems by reporting this. You will have no trouble from me." The boy does this because he knows that poor young workers are easily disposed of by plant owners if they threaten to be troublesome.

The police don't respect basic human rights, either, even if those rights are supposedly protected by Indian law. For example, when Abdul is arrested, he is beaten by the police in jail. The police also tries to terrify him into taking out a loan from a moneylender and paying them huge bribes. This would be the only way for Abdul to avoid having the false charge filed against him.

The simple truth is that slum dwellers have fewer rights than other Indians.

> _"The Indian criminal justice system was a market like garbage, Abdul now understood. Innocence and guilt could be bought and sold like a kilo of polyurethane bags."_

### 5. In the slum, corruption is rife and bribes are needed for everything. 

In slums, corruption is omnipresent. Annawadians have to pay bribes all the time and are always likely to be taken advantage of.

One example is the way government funding and charitable donations get misused.

Take the Bridge School, which many children from Annawadi attend. It's funded by the central government through a Catholic charity, and officially it's run by a woman named Asha. But because she's busy with other schemes, it is her wholly unqualified daughter who actually runs the school. What's more, Asha wants the school to be run as profitably as possible, so her daughter only teaches classes on days when the supervisor from the charity comes to check on them.

Indian democracy is also deeply corrupt. Though Annawadians don't really benefit much from participating in the political process, the act of voting in itself gives them hope. This is because the mere act of living in a slum makes them criminals in the eyes of the rest of country, so it is only when voting that they can feel like legitimate Indian citizens with the same rights as others.

But, in order to vote in elections, the slum dwellers need a voter card, which can only be acquired by bribing officials. One group of Annawadians register to vote but still haven't received their voter cards a year later. They try to pay Asha to take the proper documents to the government officials and bribe them so they will have a chance of getting their voter cards, but she refuses.

So no matter how good the intent behind the programs that are supposed to benefit slum dwellers, you can be sure someone will exploit any opportunity to become a corrupt middleman.

> _"But for the poor of a country where corruption thieved a great deal of opportunity, corruption was one of the genuine opportunities that remained."_

### 6. Even health care professionals extract bribes and provide inadequate medical treatment. 

In the previous blink, you learned how slum dwellers face corruption in education and democracy. But this corruption also reaches into their health care system.

A prime example is the way doctors manage to charge patients for services that should be covered by the Indian state.

For instance, when the Husain's neighbor Fatima is taken to the public hospital to treat her burns, she does not get the medication she needs. Medical staff steal medicine and then tell the patient's relatives that it's out of stock, while assuring them that they can buy the medicine from the market right in front of the hospital. The staff then sell the medicine through the market. They take especial advantage of desperate people.

Doctors forge medical papers to cover their tracks in case of police charges and to improve their care statistics. For example, when Fatima dies from an infection she picks up in the hospital ward, her doctor changes her admission papers to say that burns covered 95 percent of her body instead of 35 percent. This way the doctor cannot be blamed for her death, as she would have been a hopeless case on admission.

It's in the hospital that the false charge against the Husain family is concocted, too. Poornima Paikrao, a government officer, helps to craft a false statement in which Fatima claims she had been incited to commit suicide by the Husain family — a very serious crime. Later on, Poornima offers to expunge the statement if the Husain family pays her a bribe.

This dynamic is normal in the slum: the injured and sick go without treatment while others try to take advantage of the situation.

### 7. The slum is an especially dangerous place for children. 

Life in the slum is dangerous and hard for everyone, but it's even worse for children.

Something as simple as crossing the road outside the slum can be a life-threatening experience: children are frequently hit by cars on their way to school because many inexperienced drivers talk on their phones while driving and don't pay attention to the road. And most of the time, when a child is hit, the driver won't even stop.

When children are struck by cars, they can expect blame, not pity, from their parents. For example, when a boy named Devo is knocked down, his teacher does her best to dress the wounds. But when the boy arrives home covered in blood, his mother is furious at him for not being more careful and beats him fiercely. A severe injury would mean a financial catastrophe for his family, so the mother screams: "If the driver had hurt you worse, how would I have paid the doctor? ... Do I have one rupee to spend to save your life?"

This illustrates how in the slum children are completely responsible for their own well-being, and their parents see their every mistake as an undue nuisance.

What's more, if a child in the slum is killed, the death is rarely investigated. In fact, it's often hushed up. For example, shortly after Abdul Husain gets out of prison, he is told that his 15-year-old friend Kalu, one of the slum's homeless garbage collectors and thieves, has been violently murdered. Though bystanders can plainly see he was murdered, the police rule that he died of tuberculosis, and don't even conduct an autopsy.

### 8. Many slum dwellers either try to exploit others or ignore their plight. 

Now that you know something about life in Annawadi, ask yourself: if you were living there, what would you be willing to do to survive and improve your situation? Would you lie, steal and take advantage of those worse off than you?

Many in the slum try to use whatever power and influence they have to extract money from others.

For example, Asha, the teacher supposedly running the school, aspires to become the first female _slumlord_ in Annawadi. Though an unofficial title, "slumlord" denotes an influential person who helps everyone with their problems and mediates disagreements between neighbors — all in exchange for money, of course.

In order to become a slumlord, Asha must be strict and unyielding. For example, when a local man asks her to help him in a scheme to secure loans for his heart surgery, Asha refuses because she considers the bribe he offers to be insufficient: "A dying man," she says, "should pay a lot to live."

Asha also prostitutes herself to government officials in order to influence them — valuable leverage in her bid to become a slumlord.

But not everyone preys on others to get ahead. Some simply choose to ignore the tragedies happening around them.

For example, once the flames are doused, instead of taking Fatima to a hospital immediately, onlookers decide not to get further involved. After all, Fatima's husband can take her when he arrives home in the evening. So she is left lying in her hut.

In the constant struggle to survive, slum dwellers focus only on helping themselves and their immediate family. It seems they just don't have enough energy to be compassionate toward anyone else.

> _"Everybody wants their profit ... They say, if I do this, how much will I make?"_

### 9. Many slum dwellers work hard to get some kind of education, but it often leads nowhere. 

Those who live in slums like Annawadi don't have many ways to get out, but an education is seen as one way to improve their lot.

This is why many children prioritize getting their schooling, despite the difficulties. Take Sonu, a young boy who enrolls in school every year even though he can't attend due to his day job as a garbage collector. He works during the day and studies at night, so that once a year he can go and take the standardized final exams. He optimistically tells his friend: "Educate ourselves, and we'll be making as much money as there is garbage!"

Unfortunately, the quality of the education available to slum dwellers is often dubious, even at college level.

For example, Asha's daughter Manju is trying to become Annawadi's first college graduate. She works hard and divides her time between her housework, her teaching duties at the slum school and her homework for college.

And yet, the education she is getting at the college does not even come close to a Western college education in terms of quality. For instance, although Manju studies English, she can't speak it fluently, nor does she even own a dictionary. Her English classes consist solely of students memorizing the plots of famous books.

But it is doubtful that any college education would help Manju to ascend to higher social strata. This is illustrated well by the difficulties she has in her part-time job selling life insurance. Though Manju completes the sales-agent training with excellent grades, she struggles to make any sales because the slum dwellers can't afford life insurance and everyone outside Annawadi views her as a simple "slum girl," and so won't do business with her.

Thus it seems that though Annawadians try hard to get the best education available, they rarely actually escape the slum.

### 10. For some, suicide is the only way out of the misery of the slums. 

In the previous blinks you learned how tough life is in the slums. Though Annawadians try their best not to lose hope, many still succumb to the everyday pressure and choose the only possible way out: suicide.

One reason for this drastic action is fear. Slum dwellers have almost no protection from criminals, so they know that simply witnessing a crime means they could soon be forcibly silenced.

For example, after the violent murder of Abdul's friend Kalu, an orphan named Sanjay is picked up by the police. Even though there's no evidence he was involved in the murder, Sanjay is beaten and threatened by the police, giving the illusion that they are conducting an investigation into Kalu's death.

But, in fact, Sanjay did witness the murder and is worried that the gang of men who killed Kalu will murder him too. At the same time, he is afraid of the police. Finally, in desperation, he drinks a whole bottle of rat poison and dies.

Another driver of suicide is the miserable way girls are treated. Typically, their families abuse them and beat them until they are married off to other families.

One tragic example is Meena, one of the local girls. Her parents forbid her to go to school and make her devote all her time to housework instead. If her work is deemed unsatisfactory, she's beaten by her parents and brothers.

The only foreseeable end to this misery is that one day she will be married off to another family in the countryside. But Meena dreads this development, because she fears she will be lonely there, and the more traditional customs in the countryside mean she'll be treated even worse. In despair, she too eats rat poison and dies after six days. Another victim of the slum.

> _"'She was fed up with what the world had to offer,' the Tamil women concluded."_

### 11. Final summary 

The key message in this book:

**In** **the** **slum** **of** **Annawadi,** **danger,** **disease** **and** **corruption** **lurk** **behind** **every** **corner.** **Many** **dream** **of** **a** **better** **life,** **but** **often** **they** **find** **there** **is** **no** **escape** **–** **their** **fate** **is** **beyond** **their** **control.**

**Suggested** **further** **reading:** **_The_** **_Promise_** **_of_** **_a_** **_Pencil_** **by** **Adam** **Braun**

_The_ _Promise_ _of_ _a_ _Pencil_ tells the inspiring story of Adam Braun and Pencils of Promise, a charity he founded on just $25, and which has built more than 200 schools in developing countries all over the world. The blinks are divided into simple lessons that show how everybody can find their passion, make the best use of their potential and live a life full of meaning, joy and inspiration.
---

### Katherine Boo

Katherine Boo is an investigative journalist who focuses on themes of social justice and poverty. In order to research _Behind_ _the_ _Beautiful_ _Forevers_, she spent over three years in the Annawadi slum. She won the Pulitzer Prize for Public Service in 2000.

