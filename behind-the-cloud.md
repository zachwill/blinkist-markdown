---
id: 55a38abc3738610007500000
slug: behind-the-cloud-en
published_date: 2015-07-14T00:00:00.000+00:00
author: Marc R. Benioff and Carlye Adler
title: Behind the Cloud
subtitle: The Untold Story of How Salesforce.com Went from Idea to Billion-Dollar Company and Revolutionized an Industry
main_color: 2893CA
text_color: 195B7D
---

# Behind the Cloud

_The Untold Story of How Salesforce.com Went from Idea to Billion-Dollar Company and Revolutionized an Industry_

**Marc R. Benioff and Carlye Adler**

_Behind the Cloud_ (2009) details the rise of Salesforce.com from a one-room apartment start-up to a prosperous global company. Using often unconventional methods, Marc Benioff — the founder and CEO of Salesforce.com — steered his company through financial problems and the dot-com bubble burst, and came out on top.

---
### 1. What’s in it for me? Discover how to turn your business into a multi-million-dollar company and change an entire industry. 

Every year millions of companies are started around the world. The majority fail within a few years. Multi-billion-dollar powerhouse Salesforce.com is truly one of the exceptions. So what makes them different?

All success stories begin with an idea you truly believe in and the courage to go your own way when venture capitalists give you the cold shoulder and markets are in uproar.

But how do you give that fledgling idea wings? By clearly differentiating yourself from others, putting the customer at the heart of your business, and by not being afraid to pick on the big dogs, you can not only pave the way to success, you can also change an entire industry.

In these blinks you'll learn

  * how Salesforce.com put an end to the software era;

  * why standing out from the crowd is so important; and

  * how you become big in Japan.

### 2. Stick to your idea and think big if you want to build a fast-growing company. 

Even the biggest companies in the world start off as mere ideas. But if everyone could snap their fingers and manifest those ideas, the world would be teeming with entrepreneurs. So what is it that made Marc Benioff and Salesforce.com so massively successful?

Let's start with this tip from Benioff: when you notice an idea's potential, don't be intimidated by it, even if it means going at it alone.

The idea for Salesforce.com wasn't the culmination of an arduous journey to seek out a grand new business concept. Benioff was on sabbatical after over ten years as vice president of Oracle when he got his inspiration while swimming with dolphins in Hawaii.

Around this time, the Customer Relationship Management (CRM) company Siebel Systems went public with a software solution that enabled salespeople to track leads, account information and manage contacts. However, it was expensive, flawed and high-maintenance, like many other similar solutions at the time.

Benioff noted the concept and thought he could improve the software using a "Software-as-a-Service" approach, specifically through a cloud computing model, so he contacted founder Tom Siebel about his idea. However, although supportive, Siebel didn't think it would win over a significant part of the market.

But Benioff stuck to his vision and decided to go at it alone. He knew he had to think big. To draw in the best engineers for his new company, he marketed his vision as "the end of software business and technology models." He knew that this benchmark would intrigue any skilled developer.

By March 1999, he had employed three highly experienced application developers who worked with him in a one-bedroom apartment in San Francisco and by that summer, Benioff had ten employees and was out-growing the apartment. To accommodate his promising company, he decided to rent an 8,000 square-foot office space at the Rincon Center.

> _"There won't be enough people or enough hours in the day. So focus on the 20 percent that makes 80 percent of the difference."_

### 3. Advertise your product aggressively from the beginning. 

Marketing: it's the number one way to garner attention when you're still a growing company. If you want to see the same kind of success as Salesforce.com, you must assert yourself either against the market leader or as the new market leader from the very start.

The first thing you must figure out is how you can show the public that you are unique.

Just six months after Benioff founded Salesforce.com, Don Clark from the _Wall Street Journal_ wrote a front page story on the "spawning of a new industry." This did wonders for the company and spread awareness about the "end of software" era. Benioff knew he had to offer a clear message and inform journalists about how Salesforce.com was different from other companies.

Next, Salesforce.com needed branding. And for this, Benioff hired Bruce Campbell, a legendary marketer who created the "no software" logo, which consisted of the word "software" in a red circle with a line through it. Some team members were skeptical, but Benioff felt that their concerns were overruled by the number one marketing rule — stand out from the pack.

He was right: after plastering the logo all over magazines and newspapers, the campaign was recognized as the Hi-Tech Campaign of the Year by _PRWeek_.

But it's not enough to simply stand out from the crowd, you must also pick a fight with the biggest players.

Bolstered by the positive media feedback, Salesforce.com began badgering their competitors. In one instance, they paid actors to pretend to be protesters waving "no software" signs in front of a conference for their main competitor, Siebel User Group. Every visitor was given an invitation for the Salesforce.com launch party and many of them showed up! The press revelled in this David and Goliath story, adding even more buzz to the event.

### 4. Use public events to garner media attention. 

As Salesforce.com expanded, they found it less necessary to concentrate on attacking their competitors. Now was the right time to really market the value of their service.

One of the best ways to do exactly this is to use events wisely to maximize the viral effect.

There are two important marketing strategies that result in the best sales conversions: the first are _editorials_, which are unbiased write-ups in the press. The second are _testimonies_, meaning word-of-mouth created by customers enthusing about their success stories.

Rather than presenting their product to potential investors, Salesforce.com started organizing a road show in a string of cities called _City Tours_, with events featuring keynote speakers, customer presentations and demos. This brought potential customers, journalists and analysts together to share their experiences.

This way of catering to the end user made a marked difference in the company's marketing success. Traditional software companies chased executives who, although they controlled budgets, were less likely to use the product themselves. So Salesforce.com saw more value in celebrating their real customers. They covered events with posters of them, even incorporating them into their presentations. These events became affectionately known as "love fests," and enabled the company to close deals with a huge 80 percent of new prospects.

Lastly, the company began throwing cocktail parties instead of hosting boring product demos.

As the City Tours became more popular, Benioff wasn't convinced they could keep holding such expensive events in every city.

But an East Coast salesman had an idea: he didn't really care about the presentations, but loved the after parties, where he could mingle with customers. So how about just hosting the cocktail parties? Salesforce.com decided to test this in a small venue in New York with only 11 customers and prospects. At a _tenth_ of the cost of a typical City Tour event, the net result was almost exactly the same.

### 5. It’s essential to put the customer at the heart of your business. 

Dealing in enterprise software during the 1990s meant tailored presentations, multiple negotiations and months of expensive work. But as the internet era dawned, Salesforce.com found a far better way to deliver and improve their product: they recognized that the customers themselves should be able to evaluate it.

Benioff understood that potential customers wanted to be hands-on with the software before they signed anything. So he started giving away free trials over the internet, which meant customers could try it without talking to a salesperson. This isn't unusual today, but in 1999 it certainly wasn't standard industry practice.

The software initially cost $50 per month, so the purchase was also less risky for the customer.

Salesforce.com also created "bugforce," a scaled-down database where customers could report bugs and offer new ideas for the software.

With this constant feedback from customers, they were able to rework and refine the software immediately, and respond to customers' needs effectively.

This customer-centric approach helped them more than they could have predicted when the dot-com bubble burst.

The dot-com craze initially boosted a lot of sites. But in March 2001, they, as well as Salesforce.com, were hit hard. Many companies decided to leave Salesforce.com and by October 2001, the company was losing up to one and a half million dollars per month, creating critical cash flow problems.

Their solution was to change from a monthly to a yearly billing plan. Although this was a more conservative approach and restricted the customers' payment plans, over 50 percent of them agreed to it immediately. Within a year, Salesforce.com went from being in the red to making a profit, all while retaining many of their customers. This was only possible because they had earned the trust and loyalty of their customers long beforehand, and the same customers now felt secure about paying upfront.

### 6. Focus on developing one flawless product after the other, not many simultaneously. 

Software companies such as Oracle traditionally tailored their products for each company that used their product. But Salesforce.com focused on a single service to suit everyone.

They did this partly by allowing their users to share their service over the internet.

The developers at Salesforce.com used a technology model they called "multitenancy." This acted like an apartment building where tenants split costs associated with the whole building, but every tenant still had their own lock and could decorate their apartment however they liked. At Salesforce.com, this meant the user could access the software running on the Salesforce.com servers while still working with their own data.

Venture capitalists saw this as a loss of control and feared they would shed customers as a result. But this was never a problem — maintenance became easier because every user received updates or new features automatically. Besides, if they went back on the idea, how could they promote "the end of software" if they began offering exactly what they wanted to destroy?

In addition to multitenancy, Salesforce.com knew they had to make things fast and simple.

Before their small-scale development team started writing the code, they created core principles for the system and laid them out on a whiteboard. They concluded that the system should be fast, because salespeople needed information quickly, and if they ran into roadblocks, they would quit the service. Next, they decided their code should be as simple as possible, which would make identifying and fixing bugs easier.

Their principles paid off: in the first quarter of 2009, the service had a 99.9 percent uptime and over 200 million transactions per day.

One pivotal decision was to make the code public for external developers, giving them free rein to create their own applications. This was put to use by the $300-million healthcare services provider, the Schumacher Group, who used it for over 90 percent of their operational programs.

> _"We could make everything much less complex and far less expensive by sharing our resources."_

### 7. Consider cultural differences if you want to expand your business in different countries. 

Salesforce.com had now laid a solid foundation in the United States, but as the need for CRM was international, Marc Benioff was keen to expand overseas.

So what should you do if you want to be internationally competitive? Choose your headquarters wisely.

Salesforce.com opted for Europe first, and in order to give themselves a promising start, in 2000, they set up their headquarters in Dublin, Ireland. At this time Dublin was attractive to big companies like Oracle or Microsoft, due to the English-speaking population and the favorable tax rate of 12.5 percent.

They also resolved to only hire native speakers from different countries. That way, when a customer from Germany called they would think they were speaking with someone in Germany.

Although they had an office for essential office work, they conducted meetings largely from luxury hotels in London, which was possible as their software was internet-based. Using hotels for meetings projected an image of success to their clients, and it was cheaper than renting high-end offices.

The company also began to tailor their marketing strategy for different markets.

Salesforce's aggressive marketing strategy was hugely effective in the United States and Europe, so now they set their sights on Asian markets, beginning with Japan. After conducting a bit of research, they recognized that they would need to approach Japan rather differently, with a significant re-think on how they positioned themselves. Japanese companies are traditionally cautious about foreign products, and look for something relatable.

Salesforce.com learned this lesson from their direct sales manager, Eiji Uda. Everytime he introduced Salesforce.com to potential Japanese customers, he would throw in a few references to other growing companies like Google or Amazon, with which the Japanese were familiar. Then, when two of the biggest Japanese companies — Canon and Japan Post — started implementing Salesforce.com into their workflow, many more were happy to follow suit.

> _"Remember though, that simply having a website in another language, doesn't mean you have a presence in that market."_

### 8. When it comes to capital and finance, combine thinking outside the box with sensible accounting. 

Getting sufficient funds together is one of the most stressful and draining parts of setting up a business. Unfortunately, even if you're a passionate entrepreneur, the passion alone is not enough to support your new business financially.

Thankfully, though, start-up capital is often closer at hand than you might think.

According to one study, 79 percent of small businesses start out with a serious lack of funds. In an attempt to avoid being one of these companies, Benioff sought out interested venture capitalists. This seemed logical for a fast-growing internet company looking to land a multi-million-dollar investment.

However, no one was particularly enthused by the "no software" model. But rather than quitting, Benioff reminded himself of all the other companies, like Starbucks and Cisco, that got turned down by venture capitalists.

So Benioff went to those who believed in him: his family and friends. To his surprise, he found many people in his immediate circle who totally backed his idea. Remarkably, over five fundraising rounds he managed to raise $65 million between 1999 and 2002, much to the chagrin of the venture capitalists who had rejected his idea. If they'd made an initial investment of $1 million it would be worth one hundred times that today.

Benioff also decided to focus his efforts on raising revenue and expanding Salesforce.com's customer base, rather than streamlining the firm and chasing profitability.

In 2002, Salesforce.com had the goal of becoming a one billion-dollar company, and they decided to go public. However, as the market had only just found its feet after the dot-com bubble burst, companies now had to demonstrate significant revenue. Going against convention, Salesforce.com started to audit _before_ they went public.

By December 2003, after over a year of internal preparation, Salesforce.com increased their annual revenue from $25 million to $100 million. That's more than twice as much as other companies had achieved.

Not long after, on 23 June 2004, Benioff got to ring the bell at the New York Stock Exchange — a personal career highlight.

### 9. Final summary 

The key message in this book:

**Salesforce.com is a solid example of a company who became hugely successful by following a few key guidelines: stand by your vision; know how to position yourself in different markets; keep your customers at the center of your business; pick one product or service and do it in the best possible way; and don't be scared of the big players.**

Actionable advice:

**Don't let expense stop you.**

If you have an idea you believe in, don't let anyone dissuade you with stories of how hard it will be to raise the money to realize it. Instead of chasing down venture capitalists, look to other means to support your idea, like people you know or institutions and organizations that can back you financially.

**Suggested further reading:** ** _Crossing the Chasm_** **by Geoffrey A. Moore**

_Crossing the Chasm_ examines the market dynamics faced by innovative new products, particularly the daunting chasm that lies between early to mainstream markets.

The book provides tangible advice on how to make this difficult transition and offers real-world examples of companies that have struggled in the chasm.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Marc R. Benioff and Carlye Adler

Marc Benioff is the co-founder, chairman and CEO of Salesforce.com. He has written three books on business and is currently a board member of Cisco Systems. He's also a committed philanthropist who's donated hundreds of millions of dollars to good causes.

Carlye Adler is an award-winning journalist and a bestselling writer. She has been published in _Newsweek_, _TIME_ and _Wired_ and has co-authored many successful business and leadership books such as _The Promise of a Pencil_ with Adam Braun and _The Hard Thing about Hard Things_ with Ben Horowitz.

© Marc Benioff, Carlye Adler: Behind the Cloud copyright 2009, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

