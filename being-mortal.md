---
id: 5519d8b76661620007000000
slug: being-mortal-en
published_date: 2015-04-02T00:00:00.000+00:00
author: Atul Gawande
title: Being Mortal
subtitle: Illness, Medicine and What Matters in the End
main_color: BEA443
text_color: 736328
---

# Being Mortal

_Illness, Medicine and What Matters in the End_

**Atul Gawande**

_Being Mortal_ (2014) helps the reader navigate and understand one of life's most sobering inevitabilities: death. In this book, you will learn about the successes and failures of modern society's approach to death and dying. You'll also learn how to confront death and, by doing so, how to make the most out of life.

---
### 1. What’s in it for me? Discover why every one of us will have to get used to the idea of dying. 

According to the axiom, death and taxes are the only two inevitabilities in life. And although we all get used to paying Uncle Sam a portion of our income, very few of us come to terms with the fact that we are, one day, going to die. This needs to be addressed.

Advances in medicine continue to prolong our lives, and the longer our lifespans, the longer our decline toward death. Dying, once relatively rapid, may become a torturously lengthy process. Both as individuals and as a society, we will need to face this reality and learn how to make dying more than something to be dreaded and endured.

In these blinks you'll discover

  * why dignity is important, right up until the end;

  * why care homes need to be more humane; and

  * why considering death now will make dying easier.

### 2. Old age and disease result in a loss of independence, causing reliance on family, medicine and social programs. 

It's unpleasant to think about death and disease. Yet every one of us will eventually be confronted with them, both directly, as our own body ages, and indirectly, as our loved ones age and pass on. Precisely because it's inevitable, it is tremendously important to talk about death and dying.

As we age, our organs slowly lose their strength and efficiency. Our bones, muscles and teeth, for example, lose mass, while our blood vessels and joints harden. As our bodies undergo this process, the heart has to pump harder to maintain blood flow, causing many elderly people to suffer from high blood pressure.

Our brains aren't spared deterioration, either; the brain shrinks in size, often resulting in dementia.

As our bodily systems gradually fail us, we become more prone to injury and disease, and less capable of caring for ourselves.

Muscle weakness, for instance, causes many elderly people to suffer dangerous falls. In the US alone, some 350,000 people per year break a hip due to muscle deterioration.

The process of aging makes it harder to maintain an independent lifestyle. Even daily activities — from grocery shopping to using the toilet — become increasingly difficult.

Eventually, we'll need permanent help from family or medical professionals, which often means spending our remaining years in a hospital or nursing home.

The increased vulnerability that comes with old age is a hard fact, but that doesn't make it any easier to accept. It's important, then, that we consider the realities of aging and death, and find ways to make the experience less painful.

> _"People can't stop the aging of their bodies and minds, but there are ways to make it more manageable."_

### 3. Increasingly, aging and dying take place in hospitals and other medical institutions. 

Modern medicine allows us to now live longer than ever before. In past centuries, diseases contracted after the age of 50 usually resulted in a quick death. Today, however, doctors can help us maintain our health much longer, slowing the body's deterioration and postponing death.

As a result of these medical breakthroughs, the percentage of Americans over the age of 65 has risen from 2 to 14 percent in the last 200 years. In Germany, Italy and Japan the elderly now constitute 20 percent of the population. While it might seem strange, these demographic changes have meant changes in _how_ we die.

Over the course of human history, generations of families typically lived under one roof or in close proximity. As the parents grew older and feebler, the children would take care of them.

This system allowed elderly people to stay, and die, at home, where they could still engage in everyday family life.

On the flipside, those bereft of family often lived out their final days in filthy poorhouses.

Today, doctors and nurses provide the care once provided by the family of the old or ill.

Consider that in 1945 most deaths in the US occurred at home. By the 1980s, however, that number had dropped considerably — only 17 percent of the population died at home. Currently, the majority of deaths take place in either hospitals or care homes. Why the shift?

In part, because families today are more spread out. Adult children often live far from their parents and, understandably, disinclined to open their doors and offer full-time care.

Fortunately, most of today's hospitals and nursing homes provide adequate medical care. Unlike the hellish poorhouses of the past, they provide a perfectly safe and hygienic environment for their residents.

While these institutions provide adequate care, they nevertheless fail to truly understand what we need and long for at the end of our lives.

### 4. Even when we’re old and sick, we want to maintain at least some control over our lives. 

When you're young, you have all the freedom you could ask for. You can live by yourself and play by your own rules, without anyone meddling in your affairs.

While independence is among our fundamental values, the truth is that a point will come in life when complete independence is no longer tenable. When you grow old or sick, you must accept the new limitations imposed by your body and mind. But that doesn't mean you _want_ to give up the values that have guided your entire life.

In old age as in youth, we want to keep our autonomy while feeling that our lives have purpose and worth. Even if old age means we can't do everything we used to, we still want to maintain a little independence.

If you've always been an avid cook, for example, but the troubles of old age make it increasingly difficult to shop for groceries, you'd rather have help shopping than give up cooking altogether.

Our perspective on life changes as we realize that our time on earth is coming to a close.

We begin to enjoy everyday pleasures — like cooking — more than chasing after new experiences. And our relationships with family and friends become increasingly important, too.

In a study conducted by Stanford psychologist Laura Carsten, participants were given a deck of cards describing people they might know — their mother, their favorite author, and so on. They were then asked to rate how much they would like to spend half an hour with each of them.

Younger participants were eager to spend time with new and exciting characters. In contrast, elderly and HIV-positive people opted to spend time with their close family and friends.

Aging often makes you more relaxed and content, and yet most people still fear old age. As will become clear in the next blinks, this is due, in part, to the failure of medical institutions to meet the psychological and emotional needs of the elderly.

> _"It is not death that the very old tell me they fear. It is...losing their hearing, their memory, their best friends, their way of life."_

### 5. Medical institutions and doctors fail to meet the needs of the elderly and dying and often rob them of their independence. 

When most people think of old age homes, they imagine drab rooms full of dullness and depression — and this stereotype isn't too far from the mark. Unfortunately, boredom and loneliness are the norm for the elderly.

Nursing homes and intensive care units often operate on regimented routines that infringe on people's autonomy. People who had previously lived their lives in relative freedom suddenly find themselves bound to a schedule and forced to give up all personal privacy.

For caregivers, it's often easier to make decisions for old people than to assist them in making decisions on their own. Even if a patient, with a bit of assistance, can get dressed on their own, it's often quicker for the nurse to just do it for them. But such time-saving techniques should be avoided. It may be easier for the nurse, but it makes residents feel useless, small and incapable.

While old age homes excel at providing care and safety, they often fail to make residents feel appreciated or at home.

Indeed, doctors and caregivers do a great job providing basic medical care. They wash, feed and dress the residents and make sure everyone is taking the appropriate dosage of their medication.

But that's not enough! As in other stages of life, we want to feel appreciated and understood in our old age.

Often this lack of warmth in old age homes is due to understaffing. Nursing homes suffer a chronic shortage of _geriatricians_, doctors specializing in medicine and care for the elderly.

And with so many duties, doctors and nurses have little time to spend making their patients feel more valued.

As it stands, the only alternative to institutionalized neglect is the family. Yet, managing the care needs of the elderly or the sick is extremely difficult, and even our families might not always make the best decisions for us.

### 6. We often seek the treatment that helps us live longest, even when it makes the remainder of our life miserable. 

Twenty-five percent of the US's healthcare costs is for treatment of people who are in the final year of their life, even though they make up only five percent of all patients. While some of these treatments are important and beneficial, many are hopeless attempts to prolong a fading life. Drawing out death often does more harm than good.

In choosing a treatment, both patients and doctors often succumb to hopeful delusions. Even when all the odds are against them, the patient and their family tend to anxiously await a miracle cure that almost certainly won't come.

Doctors often do little to help people understand how terminal their terminal illnesses really are. Not wanting to crush the hopes of their patients or their patients' families, doctors promote unrealistic hopes. In fact, more than 40 percent of oncologists admit that they've offered treatments, at one point or another, that they believed to be unlikely to succeed.

As a result, terminally ill people often choose dangerous treatment methods whose side-effects — such as debilitating nausea or extreme tiredness — ruin the patient's present well-being in exchange for a tiny chance of having more time later.

This is because we value our time on earth more than our quality of life. In the end, it's better to have had meaningful and enjoyable final days than to suffer the anxiety of desperately trying to extend our lives.

As a study by the American Coping with Cancer project reveals, terminally ill cancer patients who were admitted to intensive care had a much lower quality of life in the last weeks of their lives than those who chose to stop intensive treatment and spend their remaining time at home or in a hospice.

Terminal illnesses like cancer and the natural diseases of old age pose difficult questions for doctors, patients and relatives. When to fight? When to give up? How to die better?

> Fact: "63% of doctors overestimate their patients' survival time."

### 7. Society must manage aging and death so we keep both our independence and our sense of meaning. 

Clearly, modern society's version of medical care fails to meet the needs and desires of those facing death. To age well and to die well, we need to promote services that respect people's autonomy while providing compassionate help and guidance when it is needed.

One such model that does this is _assisted living_, which aims to move away from the rigorous control of nursing homes while essentially providing the same services.

Patients in assisted living facilities are given their own space, where they can create a real "home" for themselves. _They_ get to decide how they spend their time, what risks they are willing to take and how much assistance they need.

While the author advocates assisted living, it's also worth looking into completely alternative conceptions of old-age housing.

We should follow the example of facilities such as the nursing home in New York that strove to lift the spirits of their residents by creating a garden for them. They also arranged for children from the local kindergarten to visit regularly, and even brought in two dogs, four cats and 100 birds!

Amazingly, after two years, the number of prescriptions per resident fell to half of that of regular nursing homes, and deaths fell by 15 percent annually. But how?

Taking care of the children, animals and plants gave residents a new sense of joy and imbued their lives with meaning, thus giving them a reason to continue living.

Likewise, hospice service must also be expanded and improved. For the terminally ill, their final experiences in life are defined by the quality of the hospice care they receive. Nurses, doctors and social workers, rather than focusing on medical treatment, should help people live the fullest lives possible before they pass.

As we develop new ways to manage aging and illness, it becomes ever more important that we are able to honestly communicate our wants and needs to our doctors and our family.

### 8. Doctors need to provide better guidance for people facing death. 

The relationship between doctor and patient is often tricky, and this is especially true when the patient is terminally ill. Doctors are no less human than their patients, and are constantly torn between professional sobriety and natural compassion.

But in order to help sick people make the most of their lives, doctors can't be completely one way or the other.

Rather, doctors should provide gentle, informed guidance for seriously ill patients. They should neither take the role of the authoritarian, dictating to their patients exactly which treatments to pursue, nor should they be purely informative, leaving the overwhelmed patient to their own devices.

A doctor must find the middle ground. They must provide all the necessary information, talk realistically about the available options, discover what's most important to the patient and, finally, offer their own perspective.

For example, doctors should ask terminally ill patients which activities matter most to them — perhaps it's playing tennis, perhaps it's being with their kids — and provide information about the treatment procedures that will best ensure the patient's ability to engage in those activities for as long as possible, even if it means shortening the patient's life.

In extreme cases, doctors should be permitted to assist patients in dying. Though the subject is extremely contentious, doctors should nonetheless at least be willing to talk about assisted death for people who are suffering unbearable pain.

While active help is against the law in most countries, medical advice on the subject may help patients to prioritize what they want and feel.

Now that the processes of aging and dying involve doctors and not just family, it is vital that medical professionals improve their listening and communication skills. That way, they'll be better able to meet their patients' needs.

> _"Patients tend to be optimists, even if that makes them prefer doctors who are more likely to be wrong."_

### 9. Families need to learn to talk about age, illness and death in realistic terms – before it’s too late. 

We often don't want to talk or think about old age and mortality — neither our own or that of our loved ones. Often, we start thinking about death and illness when it's already too late, thus forcing ourselves to make hasty decisions and depriving ourselves of a dignified good-bye.

It's important that you discuss the questions that come with age and disease openly with family and our friends, for example, by considering what "trade-offs" you're willing to make on your sick- or deathbed. You can make this vivid by considering the following questions: What will be most important in your final hours? What makes life worth living?

You don't have to answer these questions immediately; in the process of discussing them, however, what you want from old age and how you want to leave this life will become clearer. These conversations are especially important once it's no longer up to you to decide what to do with your life, i.e., when others must make the decisions for you.

Moreover, taking an early look at the end of your life can be empowering. If you confront your hopes and fears and embrace your mortality, you can continue feeling like the author of your own life right until the end.

Accepting the reality of death lets you choose your "dying role," thus giving you control over what you pass on and what you will be remembered for.

For example, in the final stages of terminal cancer, the author's daughter's piano teacher gathered her remaining strength to give each of her students one last piano lesson. In this way, she made sure her advice as a teacher would live on in her students' music.

By openly confronting old age, disease and death, we might, as individuals, families and societies, be better able to make our mortality a less painful experience, and even find in it a meaning and a purpose.

### 10. Final summary 

The key message in this book:

**Whether we like it or not, we're all going to die. Luckily, we have our entire lives to prepare for our inevitable end. This preparation requires us to think seriously about what is most important to us in life, and to share these values with our friends, family and doctors, while also doing what we can to make life in the nursing home more rewarding.**

**Suggested further reading:** ** _The Checklist Manifesto_** **by Atul Gawande**

Drawing from his experience as a general surgeon, Atul Gawande's _The Checklist Manifesto_ reveals startling evidence on how using a simple checklist can significantly reduce human error in complex professions such as aviation, engineering and medicine.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Atul Gawande

Atul Gawande is a doctor, author, researcher and professor at the Harvard School of Public Health. He has written two other books, _Complications_ (2007) and _The Checklist Manifesto_ (2011).

