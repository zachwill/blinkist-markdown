---
id: 5678640b0bac9b0007000006
slug: benjamin-franklin-en
published_date: 2015-12-23T00:00:00.000+00:00
author: Walter Isaacson
title: Benjamin Franklin
subtitle: An American Life
main_color: B36E37
text_color: 6B4221
---

# Benjamin Franklin

_An American Life_

**Walter Isaacson**

In _Benjamin Franklin_ (2004), you'll discover the life and passions of one of America's most respected and beloved personalities, from his early days as an essay writer to his budding scientific career to finally his inspirational role as one of the founding fathers of the United States of America.

---
### 1. What’s in it for me? Dive into the fascinating, pivotal life of a true American hero. 

Certain people throughout history have been pivotal in transforming their fields of expertise. Henry Ford revolutionized industry; Albert Einstein opened up new realms of physics; Ted Turner helped create modern media.

And yet there are relatively few individuals who have approached and transformed so many different disciplines and fields of study as did the American legend, Benjamin Franklin.

Over his long, illustrious career, Franklin dazzled the science world with his early discoveries regarding the nature of electricity; he was an early pioneer in the American printing industry; and what's more, he was one of the main people responsible for setting the political foundations of the United States.

These blinks walk you through this man's amazing life, from his early days as a sharp yet rambunctious child to his gradual evolution into America's most influential diplomat. If you dream of making an impact in the world, there is much you can learn from such an inspiring figure.

In these blinks, you'll discover

  * why wearing a "leather apron" showed you were a savvy colonial thinker;

  * the famous story of Franklin and a kite; and

  * who exactly was the sharp-tongued letter writer, Mrs. Silence Dogood.

**This is a Blinkist staff pick**

_"I learned a lot from these blinks, not only about the fascinating life of the multi-talented Franklin, but about the struggle for independence and the founding of the United States."_

– Erik, Editorial Production Manager at Blinkist

### 2. Young Benjamin Franklin was rebellious yet highly intelligent, despite having had little formal schooling. 

Benjamin Franklin was born in Boston on January 17, 1705. As a young boy, Franklin already showed signs of independence and inventiveness, evident for example in how he approached swimming.

Wanting to swim faster yet realizing his fingers and toes were preventing him from doing so, Franklin began tinkering with contraptions to help propel him faster through the water. His solution? Fashioning paddles for his hands and flippers for his feet!

His parents planned for Franklin to receive an education and then enter the church. Yet his father Josiah soon realized that his youngest son wasn't fit for the clergy. There are many anecdotes describing young Benjamin's mischievous ways — and none paint him as especially pious.

For example, Franklin thought the grace his father recited before each meal was tedious. So after the meat for that winter had been salted and stored in barrel, Franklin asked his father if he would instead say grace over the barrel, as it would save time!

Franklin spent just two years enrolled at a local school, where he was taught writing and arithmetic. Then, at just 10 years old, he started work as an apprentice. First he worked under his father, and then with his older brother James, who founded the _New England Courant_, the first independent newspaper in Boston.

Eventually Franklin grew tired of working alongside his brother, chafing at his subordinate role as an apprentice — especially since he had managed the newspaper on his own when James was away in England.

So even though he was still a teenager, Franklin decided to strike out on his own instead.

> The surname Franklin comes from Frankleyn, roughly meaning free-men

### 3. Benjamin Franklin’s wanderings took him to London and back, but his dream above all was to write. 

In 1723, when Franklin was just 17 years old, he boarded a sloop heading for Philadelphia.

There he found a job with Samuel Keimer, a printer, whom Franklin thought "an odd fish" but stayed on with him because he enjoyed their lengthy philosophical discussions. It was during this time that Franklin honed the debating skills that would prove so important later in his life.

After befriending Pennsylvania governor Sir William Keith, Franklin was presented with the opportunity to travel to London. He stayed there for almost two years working as a printer's assistant. Yet after a Quaker merchant offered him a position as a clerk in his general store back in Philadelphia, Franklin accepted the position and returned to America. 

Despite moving up in business, his real passion lay elsewhere — in writing.

Franklin knew he wanted to write and had read voraciously while working at his brother's printing shop. John Bunyan's _Pilgrim's Progress_ affected him deeply, and the book's ideas of progress and self-improvement stayed with him for the rest of his life.

Another favourite tome was Daniel Defoe's _An Essay Upon Projects_. Defoe's work argued that it was inhumane to bar women from education and the rights that men freely enjoyed.

Always working to educate himself and refine his writing skills, Franklin began regularly reading essays published in _The Spectator_, a British daily paper. A few days later, he'd write the essays he'd read in his own words, and then compare his work with the original.

Franklin's very first attempts at writing were actually published in his brother's newspaper — these humorous essays were submitted anonymously under the female pseudonym, Mrs. Silence Dogood. For someone his age, Franklin's early work displayed an unusual amount of imagination!

### 4. After founding a group of likeminded thinkers, Franklin’s ideas on community and government took root. 

Franklin's life as a shopkeeper was short-lived, and he soon returned to his old position at Samuel Keimer's printing shop. Yet nothing could sate his growing curiosity and ambition. 

In 1727, Franklin founded a club named the _Leather Apron Club_, later known as the _Junto_. Unlike older men's clubs at the time, Junto members were young tradesmen and artisans who met to discuss current events. 

The Junto became a springboard for many of Franklin's ideas for community improvement, such as a subscription library, a tax for neighborhood constables, a volunteer fire department and an academy — which later evolved into the University of Pennsylvania.

Franklin's thoughts on government and security were radical for the time. In 1747, he conceived of an idea to form a voluntary military, independent from Pennsylvania's colonial government. He felt such a corps was necessary, given the colony's inept handling of the threat from France and its Indian allies.

Many people supported the initiative — some 10,000 people signed up to join the corps and sought to elect Franklin as its colonel, although he turned the position down. Instead, Franklin wrote the corps' motto and designed insignias for its divisions. The corps disbanded in 1748, however, as the threat of war had vanished.

It's possible that Franklin didn't realize how radical it was for a private group to assume a government function. However, the colony's proprietor, Thomas Penn, did. He condemned Franklin's actions as contempt for the colonial government and proclaimed Franklin a dangerous man.

Of course, the real power struggle was still 20 years away. In the meantime, Franklin wasn't yet overly preoccupied with politics — he was focused, instead, on the natural world.

### 5. Franklin’s curiosity led him to make serious scientific discoveries, especially with regards to electricity. 

Greek philosopher Plato said that the unexamined life was not worth living — and no one embodied this statement more than Benjamin Franklin.

Franklin's insatiable curiosity would make him famous. During a visit to Boston in 1743, Franklin observed a performance which included some tricks using electricity. At the time, no one fully understood how electricity really worked. 

Intrigued by what he saw at the performance, Franklin started his own experiments. He soon discovered that there was a connection between electricity and lightning — a phenomenon that most people still considered supernatural. 

He conceived of an experiment in which a person would hold an iron rod on a clear hilltop during a thunderstorm, with the goal of attracting a lightning strike — thus proving not only that a metal object could draw an electrical charge, but also that lightning was indeed electricity.

Franklin described this groundbreaking experiment in a letter to the Royal Society in London. Word quickly spread. In May 1752, a group of French scientists were able to successfully replicate Franklin's experiment.

Meanwhile, Franklin decided to perform the experiment himself, using not a rod but a metal key attached to a kite string. Flying the kite in a storm, Franklin proved his intuition was correct — the key was struck by lightning! He was then able to transfer the charge from the key to a special jar that stored electricity, called a Leyden jar.

Franklin's breakthrough led to the invention of the lightning rod. Yet his further explorations into electricity brought more acclaim; Franklin was the first person to call connected Leyden jars a "battery," for example.

Yet Franklin's work brought him both praise and condemnation. Religious figures like Abbé Nollet condemned his work as an offense to God, while German philosopher Immanuel Kant called him a "new Prometheus," having stolen fire "from the gods." Nevertheless, his star was rising. He was rewarded with honorary degrees from both Harvard and Yale.

> _"What signifies philosophy that does not apply to some use?_

### 6. Franklin crafted an initial plan to unify the American colonies, but the time was not yet ripe. 

After success in scientific circles, Franklin turned his attention to the political arena. Although, as he would soon find out, doing so was even riskier than sparring with lightning.

Since 1747, when Franklin established a voluntary militia, the question of who should finance the war against the French and native tribes continued to stoke tensions between the colonies' proprietors and residents.

After George Washington was sent to the Ohio Valley in 1753 in a failed attempt to remove the French from the territory, the British colonial authorities requested delegates from each colony to attend a conference in Albany, New York _._

Franklin was now a fervent opponent of the colonial proprietors, and, along with affluent Massachusetts shipping merchant Thomas Hutchinson, drew up a plan for a union of colonies which he planned to present in Albany.

But the idea of cooperation between the colonies didn't come easily. In a 1751 letter to his friend James Parker, Franklin wrote how odd it was that six nations of "savages," referring to the native Iroquois tribes, could come together while a dozen English colonies could not.

Franklin's plan detailed a national congress made up of representatives who would be selected by each colony, and a head or "President General," who would be appointed by the king.

The plan also outlined a new political idea which would later become known as federalism. The "general government" would decide on matters such as national defense and westward expansion, while each colony would maintain its own local governing power and laws.

However, Franklin's Albany plan was rejected; colonial assemblies claimed it infringed upon their power.

In Franklin's words, the plan as it was viewed in England was "judged to have too much of the democratic." While Franklin was skeptical of those who ran the colonies, he still saw the American colonies as British. Yet this, too, would soon change.

### 7. Franklin brought the colony’s fight to Britain’s shores, but couldn’t get the proprietors to bend. 

In 1757, three years after the Albany conference and Franklin's failed unification plan, the Pennsylvania assembly raised the issue again. 

It sent Franklin to London to press the issue with the powerful British families that had been granted proprietary rights — that is, ownership — over the colonies. Failing this, he was to talk with the British government directly. Franklin wound up spending five years in negotiations.

A series of meetings in 1757 were initiated between Franklin and the proprietor of Pennsylvania, Thomas Penn. The assembly's main peeve was the proprietors' demand to be exempt from the taxes required to defend colonial land from the French and the native tribes. The assembly thought this was "unjust and cruel."

In the formal complaint, Franklin added a separate jibe. He refused to describe the Penn family as the "true and absolute proprietaries" of Pennsylvania — a slight the Penn family did not appreciate. 

Then there was the question of political power. The assembly had selected a group of commissioners to deal with the native tribes, yet Thomas Penn claimed he was within his rights to veto the assembly's decision. 

Franklin countered, saying that the colony's previous proprietor, Thomas Penn's father William Penn, had awarded the assembly exactly this power in the 1701 Charter of Privileges. In response, Thomas Penn angrily cut off all contact with Franklin, and it took a year before he finally responded to Franklin's concerns.

Penn remained inflexible regarding political decision making, saying that the Pennsylvania assembly could only give "advice and consent." At this juncture, Franklin was convinced that trying to change the proprietors for the good of the colonies was pointless.

After this disappointment, Franklin made the acquaintance of many leading British thinkers, notably philosopher David Hume, and spoke widely on colonial topics. In 1762, Franklin sailed back to America and discovered that the political situation there had deteriorated considerably.

### 8. The Stamp Act was the last straw for the colonies; Franklin saw the need for the British to go. 

Back in Philadelphia, Franklin was shocked at what he saw. Relations between colonial residents and native tribes had taken a serious turn for the worse. A mob of frontiersmen called the Paxton Boys were threatening to come to the city and kill native Indians there — as well as any colonial resident found hiding them.

It was obvious to Franklin that the colonial proprietary government wouldn't be able to handle the situation — so Franklin once again set off for London. This time, he was gone for a decade.

In 1765, as Franklin had already set sail, the British proposed a tax on stamps to finance fighting against the French and their native Indian allies. The "Stamp Act" would irrevocably change both the colonies and Franklin's political perspective.

The tax for many colonial leaders was truly the last straw. A new group of men, Thomas Jefferson among them, decided it was time to remove the British once and for all. 

In October 1765, leaders from nine colonies came together to discuss the situation. For the first time since the Albany conference, the talk centered around uniting against the British colonial powers.

While Franklin initially supported the Stamp Act as a needed, pragmatic step, he soon realized just how much colonial attitudes had changed, and the backlash against him was fierce. In Philadelphia, a disgruntled mob stormed Franklin's house, where his wife, alone, had barricaded herself in, armed with a rifle.

Franklin still held on to the idea of colonial independence, and soon realized that the British Parliament either had to apply _all_ laws to the colonies, or none at all. There was no room for compromise.

When Franklin finally returned to Philadelphia with his new perspective in 1775, he was greeted as a patriot.

### 9. Franklin’s prose started the conversation that led to the Declaration of Independence. 

Franklin realized that they would have to depend upon one another, if the colonies were to claim independence from British rule.

On July 21, 1775, Franklin shared his ideas with the Continental Congress, an assembly of men representing the colonies' interests against Britain. The content of Franklin's draft of the "Articles of Confederation and Perpetual Union" was carefully considered and meticulous in its detail. 

Franklin's idea was that a single-chamber congress would be formed, with its members allocated proportionally based on each colony's population. Plus, rather than a single president, congress would appoint an executive council of 12 members.

Interestingly, Franklin also proposed that the confederation would be dissolved if Britain were to concede to the colonists' demands and make monetary reparations. Like many colonial leaders, Franklin still viewed the crisis as a feud with specific British government ministers and not with the Crown itself. 

But in 1776, a pamphlet was published that would change many minds and prime the colonies to fight for independence. 

_Common Sense_ was written by Thomas Paine, although many at the time thought Franklin had authored it. (Franklin had read it before publication and suggested revisions to Paine.) Paine argued vehemently for the total independence of the colonies from Britain, expanding on the idea that there were no natural or religious grounds for classifying people into kings or subjects.

The pamphlet sold in excess of 120,000 copies — an incredible number at the time — and it helped rally support for a revolution against the colonial powers.

On July 4, 1776, the Continental Congress voted in favor of independence. While Thomas Jefferson eventually penned the declaration, Franklin suggested edits to the document, the most famous being changing Jefferson's "We hold these truths to be sacred and undeniable," to "We hold these truths to be self-evident."

This change exemplifies the influence that humanist thinkers had at the time on Franklin. He believed human rights were not bestowed by God upon a chosen few, but were shared by everyone equally in nature.

### 10. Franklin played a pivotal role in securing France as an ally in America’s fight against Britain. 

With their declaration of independence from Britain, the 13 colonies of America now faced a very formidable enemy. To stand a chance of winning the fight, the colonies would need an ally, namely the French. 

But who would persuade the French to support their cause? Benjamin Franklin, of course. The success of the American revolution was now in his hands.

In late 1776, Franklin traveled to Paris to meet with French foreign minister Comte de Vergennes.

In Franklin's pitch to Vergennes, he highlighted the delicate balance of power between a newly independent America, Britain and France — an issue he knew Vergennes would see favorably. 

If France were to support the Americans, it would be an enormous defeat for Britain, which would lose both its American colonies and possessions in the West Indies. 

Franklin said that if the Americans were victorious, they promised the French could keep the islands in the West Indies. Yet if France turned away from the cause, the Americans might have to accommodate Britain's interests, which of course clashed with those of France.

Vergennes eventually agreed to support the American cause, yet the French dawdled and it wasn't until March 1778 that Louis XVI made the alliance official.

As this happened, crowds of people gathered outside the royal palace gates to see the famous American, who was greeted with cries of "Vive Franklin!" as his coach drove off. The alliance that Franklin had managed to secure meant the Americans stood a chance at defeating the British!

### 11. After the Revolution, Franklin’s talent in diplomacy helped the young United States of America immensely. 

After the fighting was over, it was time to patch up relations with Britain. Once again, Franklin's composure and personality played a central role.

Early on in the fight for independence, Franklin had positioned himself as an isolationist, believing that America should avoid seeking support from other countries. However, he later changed his position and felt that the states should be grateful for France's support.

This position created a rift between Franklin and John Adams. Adams claimed that the French had agreed to an alliance based solely on national interests. In his view, America didn't owe France anything.

But by the end of the 1770s, America's urgent need for money for the war raised the importance of the relationship with France. Without sufficient financial support, the Americans couldn't prevail; and without a American victory on the battlefield, the British wouldn't even consider negotiations.

With France's financial support — thanks to Franklin's tactful negotiations — American troops fought on. In 1781, British General Lord Cornwallis was defeated at Yorktown, Virginia. This marked a victory for independence, and the beginning of peace talks with the British.

As one of the five peace commissioners after the fighting was over, Franklin faced a problem. America had agreed previously to coordinate diplomacy with the French, but the British representatives wanted to meet separately with the Americans.

In the subsequent months, Franklin played a risky game by meeting with British representatives without letting the French know. In these meetings, he proposed to the British that they surrender control of Canada to America, knowing that this was something France didn't want, as it would make America stronger and less dependent on France.

On November 30, 1782, the parties signed a peace treaty whose opening lines declared America to be "free, sovereign and independent." This statement outraged the French.

But yet again, Franklin's masterful diplomatic skills came into play. He not only calmed but convinced Vergennes of America's new independent course.

> _"The United States will never have a more zealous and more useful servant than Franklin." — Comte de Vergennes_

### 12. As a co-author of the American constitution and a national hero, Franklin made an indelible mark on history. 

When the news of the British defeat reached Congress in 1781, there was so little money in the treasury that congressional members had to pay the messenger out of their own pockets. 

As stipulated by the articles of confederation, Congress had very few practical powers and could not levy taxes. Now that the war was over, political change was needed. 

Change came in 1787 with the Constitutional Convention, in which Franklin was a central figure.

A key issue during the convention was how American states should be represented in Congress. With proportional representation, Franklin noted, smaller states were wary of their liberty being compromised; with an equal number of votes, bigger states feared their money would be at risk.

Franklin suggested to split the difference. Congress would consist of both a lower house, for which representatives would be elected on a proportional basis, _and_ an upper house, for which each state would appoint an equal number of senators. 

The lower house (House of Representatives) would govern taxes and spending; the upper house (Senate) would confirm executive officers and handle any questions of sovereignty. In the end, Franklin's proposal was accepted.

But Franklin's work didn't stop there. His last mission in life was trying to end slavery.

In 1760, approximately 10 percent of Philadelphia's residents were slaves. Franklin's views on slavery had evolved over time, so that by 1787 he was solidly abolitionist and became the president of the Pennsylvania Society for Promoting the Abolition of Slavery.

In 1790, Franklin put forward a petition to Congress, which stated that its duty should be to uphold liberty for the American people "without distinction of color."

Yet despite Franklin's efforts, the petition was condemned by those who defended slavery; they argued that the Bible actually sanctioned it.

On April 17, 1790, at the age of 84, Benjamin Franklin died. From his early inquisitive years to his renowned career as a statesman, Franklin made an indelible mark on American history. To honor the great American, over 20,000 people attended his funeral procession.

### 13. Final summary 

The key message in this book:

**The life of Benjamin Franklin shows how one person, through his limitless curiosity and compassionate interest in human progress and liberty, can alter the course of history.**

**Suggested** **further** **reading:** ** _Einstein_** **by Walter Isaacson**

In an attempt to understand what motivated this peerless scientist, Walter Isaacson's insightful biography (2008) delves into Einstein's personal life. And, as it turns out, there are many factors that shaped Einstein — his rebellious nature, his fervent curiosity and his commitment to individual freedom.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Walter Isaacson

Former CNN chairman Walter Isaacson is the author of several bestselling books, including _Einstein: His Life and Universe;_ _Kissinger: A Biography;_ and _Steve Jobs: The Man Who Thought Different._ Isaacson is the president and CEO of the Aspen Institute, an educational organization.

