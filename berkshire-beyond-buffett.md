---
id: 5580661f62323200074a0000
slug: berkshire-beyond-buffett-en
published_date: 2015-06-18T00:00:00.000+00:00
author: Lawrence A. Cunningham
title: Berkshire Beyond Buffett
subtitle: The Enduring Value of Values
main_color: AB222A
text_color: AB222A
---

# Berkshire Beyond Buffett

_The Enduring Value of Values_

**Lawrence A. Cunningham**

_Berkshire Beyond Buffett_ (2014) reveals the core values that define Berkshire Hathaway's corporate culture as established by its founder, Warren Buffett. The book goes on to prove that the investment company's unique view of investing and operating will ensure its success even after Buffett's passing.

---
### 1. What’s in it for me? Learn how a distinctive corporate culture can make a company thrive. 

Some personalities are so influential and awe-inspiring that it's difficult to imagine the world without them. Investment legend Warren Buffett is that sort of personality.

Having molded Berkshire Hathaway, a conglomerate worth more than $300 billion, from the ground up, Buffett is seen as the foundation without which the firm could not stand on its own.

Yet these blinks will show that the core values that Buffett has worked so hard to instill, and the corporate culture that has resulted, are exactly what will keep Berkshire Hathaway successful even after Buffett is gone. And in exploring Buffett's path, you will learn how you too can instill such a rock-solid culture in your own company.

In these blinks, you'll also learn

  * how flipping burgers can actually lead to living your ultimate dream;

  * why junior managers should make more decisions than senior managers; and

  * who's next in line when Berkshire founder Warren Buffett is gone.

### 2. While Berkshire subsidiaries are diverse, they all share common core values. 

Starting from humble roots in 1965, Berkshire Hathaway has grown to become one of the world's largest corporations.

This company's iconic leader, Warren Buffet, rocketed to fame in the 1990s as his savvy stock-picking gained him shares in top companies such as American Express, Coca-Cola and the Washington Post Company.

As a corporation, Berkshire Hathaway's holdings are vast and diversified, and its influence extends over a wide array of commercial, financial and manufacturing interests.

Some of the companies it owns include: GEICO, the second most-popular car insurer in the United States; Burlington Northern Santa Fe, one of North America's major transcontinental railroads; and MidAmerican Energy, a global energy supplier.

And it's not just through Berkshire Hathaway's broad engagement in many different businesses that it demonstrates its ability to diversify. The company's subsidiaries differ widely in almost every measurable category, such as acquisition price, company size and number of employees. 

With such a variety of companies under one roof, a degree of homogeneity might be expected.

Berkshire Hathaway's subsidiaries do indeed share its own unique corporate culture, built on core values that serve to bring unity to such a wide-ranging portfolio.

One of the company's core values is _eternality_. Long-term partnerships are highly valued at Berkshire Hathaway; the firm presents itself to subsidiaries, including many family businesses, as a place where they can find a permanent home.

In this way, unifying traits and commonalities are found in bonds of trust rather than in simple financial metrics. These traits when taken together create the Berkshire Hathaway culture.

### 3. Being budget conscious and keeping promises are the heart of the Berkshire Hathaway approach. 

Berkshire Hathaway's culture is made up of core values, which are spelled out by the individual letters in BERKSHIRE: that is, for every letter, there's a corresponding value. 

Let's explore the acronym's first two letters to learn the firm's two most important values.

The "B" stands for one of Berkshire Hathaway's most essential principles: _budget consciousness_. Consider how the company's stake in GEICO, a car insurer, supports its budget-conscious focus.

GEICO practices budget consciousness through serious frugality and extraordinary operating efficiency. The company's goal to keep costs at an absolute minimum isn't merely to increase profits, however. GEICO in fact passes along most of its savings to customers as lower premiums. This policy in turn attracts more customers, and results in greater total premium volume for the company.

The "E" refers to _earnestness._ The value of keeping promises, earnestness is a characteristic which broadly applies to all Berkshire Hathaway subsidiaries, particularly its insurance companies.

The business philosophy of Berkshire Hathaway subsidiary National Indemnity Company (NICO) is based on earnestness at its most effective. Its philosophy stresses that while an insurance policy is merely a promise, NICO aims to offer promises of the highest quality.

How does the company do this? NICO writes policies that other insurers won't or can't because such policies involve unusual risks for proportional premiums.

In the months after the 9/11 terrorist attacks in the United States, NICO wrote large-scale terrorism policies including a $1 billion policy for several international airlines and a $500 million policy for an overseas oil platform.

NICO can write such policies and stick to its earnestness principle as it has made Berkshire Hathaway's core values its own.

> _"Lose money for the firm, and I will be understanding; lose a shred of reputation for the firm, and I will be ruthless." — Warren Buffett_

### 4. A strong reputation and solid family ties have served Berkshire Hathaway well. 

A good reputation goes a long way, and not just in terms of maintaining a friendship. Even a company's finances can benefit from a company's good rep in an industry.

Importantly, investing in the value of _reputation_ — the "R" in BERKSHIRE — has paid off for Berkshire Hathaway companies.

Successful furniture retailer and Berkshire Hathaway subsidiary Jordan's Furniture earns some $950 of revenue per square foot annually, a number that's nearly six times the industry average.

The company's secret? Jordan's Furniture has a solid reputation based on its unique customer service. Its approach goes beyond offering a wide selection of furniture at good prices with prompt delivery to orchestrating a special experience that the company calls "shoppertainment."

At one Jordan's store, for instance, customers can take a seat in a small theater to watch a flight simulation. At another, customers can stroll along a model of Bourbon Street in New Orleans and even tour a riverboat. These entertaining in-store concepts attract tons of interested customers and in turn generate high sales. Truly Jordan's investment in the value of reputation has paid off!

The "K" in BERKSHIRE, _kinship,_ has also proved valuable for Berkshire subsidiaries. In striving to solidify kinship bonds, Berkshire Hathaway aims to create wealth that lasts generations, in the same way that a tight-knit family values identity and legacy.

Berkshire Hathaway does business for the long-term. This makes family businesses particularly attractive for it, as they are often characterized by powerful bonds such as fairness, mutual respect and trust. And in business, this pays off.

In 1995, Berkshire Hathaway was able to acquire family business RC Willey Home Furnishings for $25 million less than a rival bid.

RC Willey knew that Berkshire Hathaway appreciated the strengths of family businesses and saw value in its financial position and policy of permanent relationships. Thus the benefits of its corporate culture allowed Berkshire Hathaway to seal a deal while also saving money!

> _"It is its culture that enables Berkshire to acquire companies at lower prices than rival bidders."_

### 5. Self-starters and entrepreneurial thinkers thrive under Berkshire’s hands-off management policy. 

As an entrepreneur in the field of acquisitions, Warren Buffett showed how a small business could transform into a huge corporation.

It's no surprise that this entrepreneurial spirit still fuels Berkshire Hathaway's company culture.

Berkshire managers must be _self-starters,_ the "S" in BERKSHIRE. Self-starters are entrepreneurs who have a vision and can run a business on their own.

In fact, among Berkshire's entrepreneurs are several recipients of the Horatio Alger Award, an honor for individuals who have achieved success often in the face of adversity. One award winner was Albert Lee Ueltschi, founder of FlightSafety International.

At just 16 years old, Ueltschi opened a hamburger stand called "Little Hawk" and used the profits to fund his flying lessons. His passion for flying then inspired him to teach others how to fly.

Ueltschi eventually created the world's premier commercial pilot training school, using flight simulators to teach routine patterns and emergency drills. Since 1996, FlightSafety has been part of Berkshire Hathaway.

To encourage a self-starter mentality in employees, Berkshire Hathaway practices a _hands off_ (the "H" in BERKSHIRE) management approach.

Most businesses are often organized bureaucratically, mired in committees and meetings, with multiple layers of reporting to make sure every message and decision is controlled.

In contrast, Berkshire Hathaway's management approach values decentralization and autonomy. Every subsidiary stands on its own, and only the most mission-critical decisions are made at headquarters. Interestingly, Berkshire Hathaway subsidiaries employ more than 300,000 people, while Berkshire's headquarters claims only two dozen employees.

Some Berkshire subsidiaries also practice the 90/10 rule, in which junior managers make 90 percent of the decisions while senior managers collaborate on the rest — in particular for issues which involve unusual risk, require special skills or go beyond the expertise of junior managers.

This hands-off style is exactly what makes Berkshire Hathaway attractive to self-starters. Executives appreciate that they can continue running their businesses independently yet feel secure in a partnership with Berkshire Hathaway at the same time.

### 6. Stay savvy, and keep it simple. Berkshire benefits too from its subsidiaries’ acquisition strengths. 

Over some 50 years, Berkshire Hathaway has acquired dozens of companies, and the value of each has increased over time.

How has the firm managed to do this? Because the company's subsidiaries do a lot of acquiring, and are successful at it, too. They have _investor savvy_, the "I" in BERKSHIRE.

This means that subsidiaries keep an eye out for companies with an organizational culture that fits their own — which enables them to attract a target company without even having to make the highest bid.

Moreover, many Berkshire subsidiaries replicate the firm's approach to acquisitions by attracting companies that also stress core values, such as trust and partnership.

Chemical company and Berkshire subsidiary Lubrizol has made a number of smaller acquisitions, many of which have gained it not only talented scientists and business managers who fit well with its ethical, research-driven culture but also increased research and development capabilities.

You may have noticed that subsidiaries are also of a particular nature. We've seen so far that Berkshire Hathaway companies engage in business in the fields of energy, transport, chemicals, insurance or furniture, as just a few examples.

In sum, the firm's acquisition criteria states a preference for businesses that are easy to understand; that is, businesses that are at their essence, _rudimentary_ — the "R" in BERKSHIRE.

The logic is that such businesses have been around for a long time and are thus well-understood; what's more, such businesses are expected to be around for many years to come, which jibes with the firm's preference for permanence and long-term value.

Such rudimentary businesses usually pose less risk than do new or seemingly exotic industries. At Berkshire Hathaway, it is more important to keep things simple and not lose money, rather than to make money by taking big risks.

> _"Berkshire businesses favor simplicity to flash."_

### 7. Berkshire has always planned with the long term in mind, but there are challenges ahead. 

It's the question on everyone's mind: what happens to Berkshire when Warren Buffett passes away?

Many people fear that losing Buffett could mean the end of Berkshire Hathaway. But keeping in mind the company's core value of _eternality_ — the "E" in BERKSHIRE — Buffett has developed his company's culture and practices in a way that will see Berkshire Hathaway endure long after he is gone.

Since 1993, Buffett has written a number of articles about what Berkshire should be without him, and he's also formalized with the board a succession plan, which prescribes splitting Buffett's job into two separate roles: _management_ and _investment_.

Berkshire Hathaway has since recruited investment managers Todd Combs and Ted Weschler, two men who possess the necessary skills to handle the investment line of Buffett's job and have even surpassed Buffett's investment performance in recent years!

Buffett has ensured that his company has solid candidates for future managers waiting in the wings as well.

The most important trait for a successor as manager and chief executive officer is a commitment to Berkshire Hathaway culture. Thus, as the succession plan outlines, the best candidates are insiders, or those employees who now manage Berkshire subsidiaries.

The strongest candidate is Frank Ptak, chief executive officer at the Marmon Group since 2006. Ptak has over 40 years of business experience.

Yet there's no question that Buffett's successors, whoever they may be, will certainly face challenges.

An upheaval in the firm's subsidiaries is a definite possibility. Buffett's successors will have to vet managers carefully to avoid tense relations and ensure that only the most outstanding managers are selected, while a long tenure of executives is also maintained.

Another challenge is carrying on Buffet's unique approach to acquisitions.

Typically, accountants and lawyers examine a takeover target over weeks or months. In contrast, Buffett sizes people up often in less than a minute, and a deal is reached without delay, sometimes as part of the first phone call!

Of course, not everyone can do business this way! Buffett's successors will have to find their own approach to acquisitions that fit their personal skills best.

> _"You can take Buffet out of Berkshire, but you can't take Berkshire out of the subsidiaries."_

### 8. Berkshire can find a valuable succession lesson in the experience of Marmon Group. 

In the mid-1990s, financial analysts mulled the question whether holding company Marmon Group would fall apart after its founders, Jay and Robert Pritzker, passed away.

Today the financial community is confronting a similar question as it attempts to divine the fate of Berkshire Hathaway, considering that Warren Buffett in 2015 will turn 85 years old.

The similarities between Marmon Group and Berkshire Hathaway are many. Both have pursued a variety of diverse businesses as acquisition targets; both have sought out rudimentary businesses and follow on the whole a decentralized management style.

And both Marmon Group and Berkshire Hathaway were founded and developed by powerful men who left an indelible mark on their respective companies.

Critics who believed that Marmon Group would collapse after the Pritzkers were proven wrong. The firm continued to thrive, acquiring more than 100 companies. And in 2008, Marmon Group became a Berkshire subsidiary, fitting right in because of shared core values.

Frank Ptak, Marmon Group director since 2003, is now chief executive officer. The company continues to operate much as it did decades before, when Jay and Robert Pritzker were still in charge.

Though few things are certain, we can learn much from the Marmon Group case. When Berkshire Hathaway holds true to its core values like Marmon Group did, it will be able to live on and continue to prosper, even after Buffett is gone.

### 9. Final summary 

The key message in this book:

**Budget consciousness, earnestness and kinship are some of the key characteristics that give strength to business endeavors at Berkshire Hathaway, a multibillion-dollar conglomerate led by self-starters and driven by a hands-off management philosophy. By maintaining a strong reputation in many industries, encouraging savvy investing and preferring rudimentary businesses, Berkshire Hathaway founder Warren Buffett has built a legacy that will continue to prosper even when he's gone.**

Actionable advice:

**Build core values with your own company name.**

Berkshire Hathaway showed that one way to create a set of core values that your employees can remember and live by is to build them into your company's name. Berkshire for example took each letter and assigned it a value, such as budget consciousness for "B" and earnestness for "E." Try it yourself! In doing so, you might just discover some key principles with which your company can thrive.

**Suggested** **further** **reading:** ** _The Education of a Value Investor_** **by Guy Spier**

In his bestselling book, Guy Spier recounts his transformation from greedy hedge-fund manager on Wall Street to a successful _value investor_. Sharing the incredible story of his career and the wisdom he acquired along the way, Spier has some surprising insights concerning, what he sees as a false choice between leading an ethical life and a financially successful one. With great admiration, Spier also names the people who were most influential to his professional life, explaining the specific effect each of them had on his mindset and career.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Lawrence A. Cunningham

Lawrence E. Cunningham is the Henry St. George Tucker III Research Professor of Law at George Washington University in Washington, D.C. His writings have appeared in university journals and in publications such as the _Financial Times_, the _New York Post_ and _The_ _New York Times_. Other books include _The AIG Story_ (with Maurice E. Greenberg) and _How to Think Like Benjamin Graham_ _and Invest Like Warren Buffett._

