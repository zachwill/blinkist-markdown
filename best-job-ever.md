---
id: 588d15541a56850004e76d92
slug: best-job-ever-en
published_date: 2017-01-30T00:00:00.000+00:00
author: Dr. C. K. Bray
title: Best Job Ever
subtitle: Rethink Your Career, Redefine Rich, Revolutionize Your Life
main_color: D12F2A
text_color: 9E2420
---

# Best Job Ever

_Rethink Your Career, Redefine Rich, Revolutionize Your Life_

**Dr. C. K. Bray**

_Best Job Ever_ (2016) is a guide to making the career change you need to be happy with your professional life. These blinks walk you through the difficult process of switching jobs, changing fields and trying something you've always wanted to do, but didn't know how to start.

---
### 1. What’s in it for me? Find your ideal job. 

Are you tired of your job? Are you bored, exhausted or dissatisfied with your tasks? Or perhaps you like your tasks, but it's your office and your colleagues that make you dread going to work.

Today, as we rush to find a job and make a living, we make bad decisions and choose a job that doesn't meet our expectations or doesn't fit our strengths. As a result, our discontent grows. We're hesitant to make a change because we fear we won't be able to find anything else, let alone something better — and sometimes we don't even know what we would do instead.

These blinks show you what it takes to make a career change. They're full of effective strategies to help you overcome your fears, discover your strengths and successfully plan a career transition.

In these blinks, you'll also learn

  * why winning the lottery is not a career path;

  * what scoring a goal, landing on the moon and changing careers have in common; and

  * how to try out job opportunities like food samples in the supermarket.

### 2. Many people have unrealistic expectations about work and are dissatisfied with what they do. 

You've probably heard this cliche before: Just find a job you like and you'll never work a day in your life. Well, the reality isn't that simple. After all, work is called work for a reason. What makes it worse, though, is that lots of people have unrealistic expectations about their jobs.

For instance, when asked to describe their dream career, people tend to say things like, "a job that lets me travel a lot," or "one where I make twice as much money as I do now." But such notions are totally off base. Just like winning the lottery, which some people list as their ideal career, the notion of luxurious jobs is a fantasy, with an absurdly low probability of realization.

While some people want the most pampered career they can imagine, others would take virtually anything _but_ their current job. In fact, lots of people are unhappy with their jobs, even though many of them like the field in which they work. Indeed, a 2013 global Gallup survey found that 87 percent of employees weren't satisfied with their careers.

But while lots of people are left unsatisfied by their work, it's not the _type_ of work they do that leaves them feeling unfulfilled. The real issues are characteristics of their workplaces; things like bosses, coworkers, salaries or even office politics.

If this is true for you, it might make sense to look for a different position within the same company or the same role at a different one. For example, if you have a family, but your job requires loads of travel, you might prefer a different role within your organization that keeps you closer to home.

### 3. Making a career change means overcoming your fears and deciding what should be different. 

As you begin thinking about changing careers, you're likely to have thoughts like "you can't do it" and "you'll definitely fail." So how can you get past these thoughts and into the career you desire?

The thing all these thoughts have in common is fear. So to make your career change, you'll need to overcome it. Remember, it's no surprise that your plans are scary — it's a common human reaction to unknown situations, and a career change is a highly uncertain undertaking.

To get your worries under control, start by listing the unknowns. For instance, you might not know whether you'll be able to quickly find another job, or what it'll be like to work at a new company; you might be worried that you won't be able to meet a new set of expectations.

Once you've listed all the unknowns, write down what makes them scary. Then, come up with an action sentence for each fear that begins with the word "despite." As an example, you might write, "I'm afraid of switching careers because it might mean a few months without income. Despite this financial risk, I can change careers because I have some savings."

Writing out your worries and how you'll handle them will help you separate legitimate fears from illegitimate ones, and show you what you can do about them.

After you've gotten this roadblock out of the way, it's time to decide what your change should look like. This is key, as lots of people mistakenly make drastic changes to their entire career when all that's needed is a slight adjustment.

To avoid making that mistake, determine what change or changes to make by considering your current job satisfaction. Ask yourself questions like, _"_ How much do you like the place you work and the people you work with?" and "Are you being paid the same as others with your position?"

By answering these questions, you'll get a better idea of whether you should switch departments, change companies or choose a different career altogether.

### 4. Develop a five-year plan for your career change to lay out your path and reduce your fears. 

Lots of people try to make significant changes in their lives, from quitting smoking to losing weight to stopping nail-biting. But too often, people fail to stick to these plans. That doesn't mean change is impossible, it just shows that it requires a particular combination of short- and long-term thinking.

So once you know exactly what change you want to make, it's time to make plans for the near and distant future. Draw three columns on a piece of paper and label them "One Year," "Three Years" and "Five Years," respectively. Then, in each column, write your ideal employment situation for that time frame.

Continue filling in each of the three columns with your preferred job type, desired salary, the skills or education required and whether your position will be in management or not. Once you've got this information down, form a timeline that starts where you are and ends where you want to be.

This process will get your goals down on paper, but it will also help reduce any fears you might have about finding your ideal job. It will give you some very specific details about the path you want to follow over the next five years, knowledge that's central to quelling your fears.

For instance, after completing this process, you'll be able to start getting the training you know you'll need and searching for positions that meet your requirements. This will allow you to set several small, achievable goals that cut down the fear of a big change and give you a higher chance of meeting your target.

### 5. You can use your network to change careers, even if meeting new people makes you anxious. 

Just as companies have to leverage their resources to make profits, you need to leverage yours. But what's the most important resource at your disposal?

It's the people around you.

Take stock of the contacts in your network and consider how each of them can help you achieve your career goals. Essentially, this is just networking. While lots of people don't enjoy this activity, considering it shallow or selfish, it's actually all about building relationships.

If you approach relationship-building with genuine goodwill, the other benefits of networking will follow naturally. That being said, there is still some strategy involved:

After making a contact, say, at a social gathering or conference, be sure to get in touch with them at least three times per year. This could be as simple as shooting them an e-mail saying that something reminded you of them.

By maintaining contact, you'll establish a rapport that lets you ask for a favor down the line. For instance, an accountant you met at a business fair might know the CEO of a company you're dying to work for.

But meeting new people can be scary and, if you're really apprehensive about it, you should consider preparing yourself beforehand. Begin by generating a few questions you can ask almost anybody, like what their profession is, or whether they've heard of your company.

Then, once you've asked a question and the other person is responding, regain your composure while listening to them and share something relevant about yourself. The goal of this type of interaction isn't to make a deal, it's just to leave an impression through a brief conversation.

Remember, you can always follow up later with a phone call or e-mail that gives more details about who you are and sets up your next meeting.

> _"Courage is simply the willingness to be afraid and act anyway."_ — Dr. Robert Anthony

### 6. You can and should prepare for your career change to ensure it’s done in a carefully calculated manner. 

You know how some supermarkets offer free samples that let you try out different foods before buying them? Well, the job market also has free samples on offer.

In fact, there are many ways to find out what a prospective job really involves, and this information can help you make an informed decision about your employment and career prospects. For instance, if you're considering changing jobs within your company, you might ask to work one day per week in a different role, strictly on a trial basis.

When it comes to positions outside your organization, lots of companies will offer potential hires the opportunity to try a job out for a few weeks to determine if they're a good fit. And you can always take a second job, undergo training in a new field that interests you and talk to as many people as possible about your new career options.

But before embarking on your career change journey, it's important to arm yourself with the right tools. First, make sure you have a financial contingency plan in case things get rocky. Figure out how much money you'll need to get you through the transition, and then be sure to have a little more than that, just in case.

It's also essential to prepare yourself mentally, since your career change might not be all that easy at first. Changes are especially difficult in their initial stages. But don't worry, they get easier with time.

Just take the author, who meticulously prepared his doctoral thesis, thought it was all set to go and was devastated when he was told it needed a lot more work. Instead of freaking out, he kept himself focused on the goal he had set for himself; he believed in it and never quit until his thesis was complete.

The lesson is clear: as you embark on this major life transition, make a careful plan and stick to it.

### 7. Final summary 

The key message in this book:

**Whether your goal is to secure a different job, join a new company or enter a different industry altogether, you have to take stock of what you want from your career change and make a careful plan to achieve your goal. Knowing yourself well and being realistic about what's possible are essential steps in this potentially life-changing process.**

Actionable advice:

**Define your strengths.**

If you know you want a different job but aren't sure what it should be, take some time to consider your strengths and how they could be applied to different fields. Make a list of things you're particularly good at and consider different industries or jobs that require these skills.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The 10 Laws of Career Reinvention_** **by Pamela Mitchell**

_The 10 Laws of Career Reinvention_ (2010) teaches you the career survival skills you need for the twenty-first century economy. No matter what industry you're trying to break into, this book will help you create a successful roadmap to your goal.
---

### Dr. C. K. Bray

Dr. C. K. Bray is a career coach who used to work in the corporate world until he realized he was chasing other people's dreams. Now, he helps people define and achieve their professional goals.

©C. K. Bray: Best Job Ever copyright 2016, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

