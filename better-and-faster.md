---
id: 55e4c089a95dad0009000000
slug: better-and-faster-en
published_date: 2015-09-01T00:00:00.000+00:00
author: Jeremy Gutsche
title: Better and Faster
subtitle: The Proven Path to Unstoppable Ideas
main_color: CE2933
text_color: CE2933
---

# Better and Faster

_The Proven Path to Unstoppable Ideas_

**Jeremy Gutsche**

_Better and Faster_ (2015) outlines a specific set of tools and guidelines readers can use to outmaneuver their competition and attain success in the chaotic and unpredictable modern marketplace. Using countless real-life examples — both cautionary tales and inspiring success stories — Jeremy Gutsche lays out a path for finding opportunities and developing successful business ideas.

---
### 1. What’s in it for me? Get the skills and knowledge you need to succeed in an uncertain market. 

A few thousand years ago, humanity made a bold decision. People stopped being hunters and started being farmers. Most decided that a life of relative comfort and predictability was better than one of risk and constant movement. You might think they made a wise move, but in fact this change led to humanity's becoming lazier and more complacent, something that affects us to this day.

How?

Well take a look at any business industry, and you'll find leaders who embody farmer mentality: people too indifferent, scared or set in their ways to change. More often than not, these people fail. There are others, however, who've clung to the life of the hunter; they're quick to act, ready to be brutal and never scared to take risks. These are the people you ought to emulate. These blinks tell you all you need to know about becoming a hunter, allowing you to succeed in today's constantly changing world.

In these blinks, you'll discover

  * why Red Bull is a success in spite of its taste;

  * how Volvo uses a car's ugliness to sell it; and

  * how Blockbuster went from the top to bankrupt in no time.

### 2. Three “Farmer Traps” – complacency, repetition and overprotectiveness – stand in the way of success. 

Every business leader thinks they have what it takes to succeed. They're smart enough to know what needs to be done and tenacious enough to overcome any obstacle that stands in their way. 

Nonetheless, failure in business is as common as dirt. And that's because the people at the top — especially those in fast-changing industries — frequently fall into one of the three so-called _farmer traps_.

_Farmer trap #1_ refers to complacency, which develops when people rest on the laurels earned from past successes. 

As an example, look at typewriter manufacturer Smith Corona, a former industry giant who became complacent and failed to adapt to the changing marketplace. In 1990, the company pulled out of a joint venture with computer giant Acer, because they believed their business was too strong to be vulnerable to market change. Three years later, Smith Corona declared bankruptcy; they'd been crushed by the emerging computer sector. 

Similarly, _farmer trap #2_ occurs when you repeat actions that brought success in the past and expect the same outcome in the present. 

Blockbuster Video is the perfect example of a business that crumbled because it fell into the Farmer Trap of repetition: in 2009, the franchise boasted 9,000 locations and 60,000 employees. But just a year later, Blockbuster declared bankruptcy: the company had failed to adapt to the streaming market, continuing to direct almost all their resources into franchising and physical media.

Finally, _farmer trap #3_ describes a state of protectiveness. This occurs when companies set up barriers that protect the status quo and inhibit evolution.

For instance, film manufacturer Kodak missed some wonderful opportunities because they were overly protective of their elite status in the analog film market. But that was a big mistake: the company's unwillingness to invest resources into digital innovation led them to declare bankruptcy in 2012.

### 3. Cultivate your “Hunter Instincts” – insatiability, curiosity and a willingness to destroy. 

As we've learned, it's easy to fall into one of the farmer traps. But luckily, it's also possible to avoid them by cultivating the three _hunter instincts_. 

First, don't be complacent; instead, be insatiable. Because insatiability — constantly scanning the horizon for the next opportunity — is the core of _hunter instinct #1_. 

Capital One Bank is a great example of a company driven by this instinct. After the 2008 financial crisis, the bank poured their resources into innovation and experimentation. As a result, they soon boasted a billion-dollar portfolio of low-risk assets. In other words, by focusing on innovation, the bank successfully adapted to the marketplace; meanwhile, most of its competitors suffered a credit crunch. 

Moving on: _hunter instinct #2_ rejects repetitiveness in favor of curiosity — the drive to seek out new and fresh sources of inspiration. How else will you spot new trends and develop your next business success?

After all, innovation happens when people take creative mental leaps, connecting fields in ways others couldn't have imagined. And to make those kinds of connections, you have to be curious. 

Take entrepreneur Ron Finley as a prime example of this principle. For Finley, his Los Angeles neighborhood was a source of inspiration, leading him to success in a variety of fields, from fashion to urban gardening to social outreach. 

Finally, _hunter instinct #3_ reminds us that we need to have a willingness to destroy. Instead of being overprotective and cautious, you have to be ready to abandon your safety net if you want to encounter new ideas and opportunities.

In fact, the willingness to destroy is precisely what explains chef Eric Ripert's decades-long success. He's been able to sustain the coveted three-star Michelin rating for years and years, despite the fact that he constantly throws out old menus and starts again from scratch, fully embracing the new.

So now that we know how to awaken our hunter instincts, it's time to learn how to put them into practice. And that's what the next blinks are all about.

### 4. Determine future business trends by following the six patterns of opportunity, starting with Convergence and Divergence. 

It's time to put your hunter instincts into practice: there are six scenarios, or _patterns of opportunity_, that will allow you to spot new business models and products, giving you a serious competitive edge.

Let's start with two of these patterns: _convergence_ and _divergence_. The former, convergence, is about taking advantage of market chaos to create opportunity by mixing and integrating the right trends. After all, you can create a winning idea by combining multiple services and products.

For instance, the successful iPhone fitness and running app, "Zombies, Run!," employs convergence by combining three things: GPS technology, the lucrative fitness market and the highly popular zombie trend. As you can see, combining several appealing patterns and industries is a great way of providing value to customers. 

The second pattern, divergence, takes a different approach by carving out a competitive advantage through a unique perspective that diverges from the overcrowded mainstream. Designing a product that taps into compelling alternative notions like rebellion, status or luxury will allow you to stand out from the crowd. 

The key to successfully pulling off the divergence approach is marketing; in order to stand out from the competition, you have to turn your greatest weakness into your greatest strength.

And that's exactly what Red Bull did, becoming one of the most successful beverage brands, despite their less-than-palatable product. The company succeeded by using a savvy advertising strategy that communicated an exciting and unique brand. Ultimately, the company proved that with the right marketing plan, you can harness the power of divergence and create successful products and services that stand out.

### 5. Harness the power of momentum by using Cyclicality and Redirection to create a competitive advantage. 

Let's move on to the next two patterns of opportunity: _Cyclicality_ and _Redirection_. These two approaches are all about harnessing the power of momentum, by doing things like, for instance, forecasting trends and figuring out how to take advantage of predictably recurring elements such as retro appeal, nostalgia, economic cycles and generational patterns. 

This point speaks directly to cyclicality, which requires you to foster a sense of history that'll allow you to look beyond the moment and anticipate what's next. This can be a powerful way of carving out opportunity: consider the fact that all sorts of companies — liquor companies and auto manufacturers, fashion brands and food products — have managed to reinvigorate their businesses by tapping into cyclical patterns and taking advantage of nostalgia. 

Just look at the success of Instagram, a product that combines nostalgia with the social media photo-sharing trend. These two simple elements are at the heart of the world-famous app, which imbues user-generated photographs with a vintage-like quality. 

The next pattern, redirection, aims to channel the power of a trend or behavior by refocusing, reprioritizing or reversing it. This is essentially the art of "spin" — turning a potentially negative narrative upside down, using the momentum of a trend to your own advantage and inspiring desire for your product in the process. 

Through well-crafted redirection, you can create an appealing positive narrative out of something more negative. Just look at Volvo: by using the slogan, "They're boxy but they're good," the auto company launched a successful marketing campaign that drew attention to the car's safety features. By playing up the security aspect, Volvo neutralized the car's unpopular design and ultimately increased sales.

### 6. Tap into Reduction and Acceleration – two patterns of opportunity – to carve out a market niche. 

Let's learn about the two remaining patterns of opportunity: _reduction_ and _acceleration_. These are both proven methods for finding a specific market, creating a winning business idea and becoming a better innovator, decision-maker and investor. 

Reduction involves targeting niche audiences with a product or service that stands out because of its extreme simplicity. Doing this gets to the core of a business concept by using crowdsourcing and localization to remove any unneeded parts. 

In other words, by identifying and then skillfully removing all the complex layers of existing products, you can create value and carve out success in a small market. And that will open up many more possibilities. 

As it happens, reduction explains the wild popularity of GoPro, an easy-to-use, wide-angle, two-button camera that has proven irresistible to a specific group of consumers eager to capture their adventurous exploits. 

The next pattern of opportunity, acceleration, relies on the practice of perfecting, exaggerating, reimagining or repositioning key product features as a way of differentiating yourself from the competition. To do that, you have to identify a crucial feature of an existing product or business concept which you then dramatically enhance. In other words, figure out what's great about an idea and then accelerate it; making it bigger, faster or better will engage customers. 

To that end, just look at Dyson: the brand established itself as the world standard for high-end vacuum cleaners after they refined the device to create the most powerful vacuum on the market. It took 5,127 prototypes to get there, but it was worth it: in 1991, James Dyson won the International Design Fair prize for his ultra-powerful and efficient product.

### 7. Identify a “hunting ground” by narrowing your focus and tracking your chosen industry. 

Wake up your "inner hunter." Familiarize yourself with the six patterns of opportunity. Done all that? Great! Now you can start narrowing your focus and zero in on a great business idea.

Of course, finding the nugget of a successful idea is easier said than done. The key is to establish a "hunting ground" and identify a cluster of opportunity — some group of products, services or concepts that have a similar approach. Then, you use the six patterns of opportunity to reframe the central idea and make it special.

But don't limit your hunting ground by only researching and collecting ideas that are obviously similar. Instead, look beyond your immediate market to find clever approaches to similar goals. That's the way to create something truly unique. 

For instance, instead of developing a simple pop-up store concept, use convergence to launch a "Double Business." One artist, Karl Lagerfeld, did just that when he combined a pop-up fashion shop with an art gallery. 

However, no matter what industry you choose to work within, you have to be able to forecast future trends before they actually happen. Luckily, whether you're hunting ground involves movies, fashion, tech, retail, broadcast media or food, you can use the six patterns to find future opportunities. 

In the fashion field, for example, retailers like Zara and H&M have reached success by providing consumers with more choice. In fact, Zara became one of the fastest-growing retailers by both speeding up their production process — they only need 14 days to get a newly-designed product into stores — and by offering thousands of different items at any given moment. 

But what's next? By applying convergence to the fashion industry, we can predict the rise of trends like wearable technology, 3D printed clothing and more. And the firms that figure out how to capitalize on those trends now will be dominant in the future.

### 8. Final summary 

The key message:

**Today's marketplace is far more unpredictable and chaotic than ever before. To be successful in this turbulent environment, you must embrace change. Do so by awakening your inner hunter and exploiting patterns of opportunity. That way, you'll be able to adapt quickly — to not only survive, but to thrive.**

Actionable advice:

**Cyclicality is one of the most persistent and prevalent patterns of opportunity. Whatever your field, you can take advantage of this pattern by following four simple takeaways.**

  1. Expect repetition: look for clues that might lead to opportunity. 

  2. Act quickly: cyclical opportunities can be fleeting. 

  3. Anticipate the certainty of evolution: get a competitive advantage by preparing for what's next.

  4. Look beyond what others already see: major forces and firms can create cascading opportunities. Zero in on those less-obvious opportunities. 

**Suggested** **further** **reading:** ** _The Innovators_** **by Walter Isaacson**

_The Innovators_ explores the social and cultural forces that inspired technological innovation through the history of computers and the internet. By weaving together the personal stories of technology's greatest minds, _The Innovators_ gives you an inside look at how the best and the brightest innovate and collaborate.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jeremy Gutsche

Jeremy Gutsche, one of today's most sought-after keynote speakers, is the CEO and founder of TrendHunter.com. As an innovation expert, he developed a $1 billion portfolio for Capital One Bank and, by providing them with his savvy business insights, assisted over three hundred top brands, including Coca-Cola, Sony and IBM.

