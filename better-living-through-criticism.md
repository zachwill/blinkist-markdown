---
id: 58bd5e340d93aa00045b76dd
slug: better-living-through-criticism-en
published_date: 2017-03-10T00:00:00.000+00:00
author: A.O. Scott
title: Better Living Through Criticism
subtitle: How to Think About Art, Pleasure, Beauty and Truth
main_color: C52B27
text_color: C52B27
---

# Better Living Through Criticism

_How to Think About Art, Pleasure, Beauty and Truth_

**A.O. Scott**

_Better Living Through Criticism_ (2016) explores the role of the critic. From the historical significance of criticism to the future of the digital critic, these blinks are an engaging introduction into an indispensable aspect of art and culture.

---
### 1. What’s in it for me? Embrace your inner critic. 

Imagine, just for a second, what our society would be like without critics. Who would take the time to dive deep into artworks and explain them to the masses? And how would you know which new movie to see at the cinema?

That's where critics come in. Love them or hate them, we need critics to be the judges of culture in society. By delving deeper into the art of criticism, we'll discover the necessary role of the critic and how it fits into societies past and present.

In these blinks, you'll learn:

  * how critics came about;

  * why _Moby-Dick_ only became famous decades after its author's death;

  * and what the digital revolution means for the future of criticism.

### 2. Critics must react to art in the moment, without fear of backlash. 

We're living in a fast-changing culture. From music to fashion, an underground hit can become a worldwide trending topic at breakneck pace. Criticism is much the same way — and that's how it should be.

While artworks themselves last for a long time, with our attitudes toward them changing over years and decades, criticism is fixed in a single moment. Take rock music, for instance. Though the genre was initially rejected by revered music critics, today rock'n'roll is recognized as a pivotal moment in musical history. The first negative critiques of rock music, on the other hand, became irrelevant.

Often, when artists create work that breaks with the status quo and shocks society, criticism of this work changes over time alongside our understanding of it. Nevertheless, it's the critic's job to tackle the problem of examining art in the context of current conditions — even though their opinions and predictions could be contradicted or proven wrong by future cultural shifts.

This is no small task, especially considering the sheer amount and range of works that require the intellectual scrutiny of the critic. Today's critics are no longer just concerned with fine arts enjoyed by the elite. Traditionally "low-brow" forms of art also provide them with a wealth of interesting material to deconstruct. Even superhero movies tell us a lot about cultural attitudes today and are worth examining.

However, our culture is also one that doesn't respond well to criticism. Part of the critic's task is to provoke thought, which often means articulating unpopular or controversial opinions. Backlash is to be expected.

Even the author of this book sparked a heated online conflict over his biting review of _The Avengers: Age of Ultron._ On Twitter, the film's star, actor Samuel L. Jackson, demanded that Scott find a new job, and thousands of fans were spurred on to attack Scott. But he wasn't too concerned. The incident gave him notoriety as a critic and ensured that his job would never be in jeopardy.

Of course, being a critic isn't all about playing devil's advocate. In the following blinks, we'll learn about what makes criticism valuable. But first, let's take a closer look at the intimate relationship between art and criticism.

### 3. Critics are artists and artists are critics. 

Oftentimes controversial critics will be branded "failed artists" by their detractors. However, many game-changing critics were successful as artists too.

Take George Bernard Shaw, for instance. Not only was he one of Britain's greatest playwrights, but also one of the greatest English-language drama critics who ever lived. T.S. Eliot and Samuel Taylor Coleridge are still studied in universities today for their work in literary criticism as well as for their poetry. So although critics and artists appear to be rivals, history shows us that this isn't the case.

On the contrary, art and criticism both draw strength from the same source: human creativity. T.S. Eliot noted that the only difference between criticism and art is that criticism can't exist without having some piece of art to respond to. And yet, one could argue that much influential art arose from the artist's responses to other pieces of artwork.

All art is, at least partially, a commentary on the art that has come before it. Writers are readers, and musicians are listeners. Creative people have a desire to imitate, improve, subvert or experiment with the art they've encountered. In this way, you could even consider the artist an accidental art critic too.

Many filmmakers from the Coen Brothers to Quentin Tarantino combine elements from films that inspired them with their own ideas to create unique visual and narrative experiences. Hip hop and electronic music artists constantly sample melodies, beats and fragments from other music and sound sources. They might be borrowing from existing artworks, but the results are often original and unexpected.

Sampling, references, and mixing and matching influences might feel far removed from traditional critical journalism, but these acts of appraisal provide fascinating reflections on previous artworks and their new significance in contemporary cultural contexts.

### 4. Our tastes are shaped by the communities with which we engage, just as history shapes cultural understandings of beauty. 

What is it about your favorite song that makes you love it so much? Is it the way the chorus just hits you, or that killer rhythm? Or is it because it's the perfect blend of musical qualities that you've learned to love since diving into a particular scene?

While some artworks may have a special impact on us because of our unique personal experiences, we also tend to seek out groups and communities that share our taste in visual art, music, fashion, film, and so on.

This gives us a feeling of affirmation, belonging and community. If you've ever been in a cinema where the whole audience is laughing at a joke on screen, you've experienced this!

For many young people, affirmation like this isn't just found in local communities, but in online ones. Blogs, social media and other channels are all incredibly influential in shaping the tastes of individuals.

Of course, our environments don't shape our taste permanently, as both our interests and our settings are constantly changing. New stages in life can make our old passions seem outdated, unrefined or even plain embarrassing. And, as society enters new stages, our collective understanding of beauty can change significantly.

For instance, in the eighteenth century, philosophers such as Immanuel Kant and Edmund Burke argued for a "subjective universality" that meant that on a deeper level, all humans considered the same things beautiful.

Today, however, we're aware that cultural and commercial forces have a powerful influence over our tastes. It's become a common cultural reflex to think twice and ask critical questions about our initial reactions to works of art.

### 5. Being painfully honest and being accused of bias are all part of the critic’s role in society. 

Sometimes, the truth hurts. Good thing we have critics! While most of us avoid expressing how we really feel to avoid hurting someone's feelings, critics make it their job to speak their mind.

All humans share the desire to express judgment. However, if we all acted upon this desire each time we experienced it, we'd be arguing nonstop. Early civilizations recognized this danger, and the critic was born. It's believed that when humans began to draw, dance and make music, critics took up the role of upholding standards by making critical judgments based on the aesthetic standards of the day.

This made critics both trusted and reviled by the public. They always told the truth, but this wasn't always what the people wanted to hear. Throughout history, critics have been accused of bias when their opinions don't affirm cultural beliefs. This is especially true today, thanks to a global culture filled with diverse and contradictory ideals.

Still, critics wield an unusual power. For creators, this isn't always pleasant. One scathing review is enough to shut down a Broadway show or nip a young author's career in the bud. Even _Moby-Dick_, Herman Melville's literary masterpiece, met with harsh reviews when it was first published, and Melville retreated into obscurity for the rest of his life.

It wasn't until after he died that the reviews, and eventually the book, were rediscovered. If it weren't for the original published reviews, the book would have been entirely forgotten.

Criticism provides us with thought-provoking commentary and valuable historical records. Despite this, modern society is skeptical about whether we need critics at all anymore.

### 6. Sometimes, critics have to make the wrong judgment to spark a conversation. 

Nobody enjoys admitting that they're wrong. But for a critic, sometimes it's necessary to get things wrong first, as this helps all of us get it right in the end.

By offering up their subjective opinions, critics can help us uncover universal truths about art and culture. When others disagree with this opinion, this kicks off discussions which ultimately help us come to a judgment that satisfies most, if not all, of the viewpoints involved.

This is the principle upon which Rotten Tomatoes, a film review score aggregator, is based. By combining many subjective reviews by a wide range of different critics, the site produces a score to capture how society as a whole feels about a movie.

Unfortunately, many contemporary critics attempt to shirk their responsibility for being honest in their critiques, and instead use superlatives and marketing-speak to avoid being wrong.

Words like "captivating" and "mesmerizing" appear all too often in reviews today. These are essentially empty buzzwords. They might help a critic land his name and quote in advertising for the film, but detract from the most important function of critical writing: starting a conversation.

### 7. Critics connect new generations with masterpieces of the past – and bring undiscovered art to new audiences. 

Many of the classic films, novels and albums that we know and love today might not have gone down in history at all if it weren't for critics. In an age where we have greater access to past cultural output than ever before, critics help us work out which works are worth focusing on. Critics are also able to enter into dialogue with their forerunners, correcting their judgments with the benefit of hindsight.

In the 1930s and 1940s, film critics were lamenting the dying art of film as one zany comedy after another seemed to hit the theaters. _New York Times_ critic Frank S. Nugent dismissed the screwball comedy _Bringing Up Baby_ as just an unremarkable film in a collection of similar movies. Later critics offered a different take, thereby revitalizing the film and helping new audiences fall in love with it. Today, _Bringing Up Baby_ is considered one of the best movies of all time.

Something similar might happen to the superhero movies today. While many contemporary critics pan them as formulaic, over-the-top mass entertainment, some of them may later be immortalized as classic twenty-first-century cinema.

Critics can also use their voices to get undervalued art onto our radars. Jazz, for instance, was not always a venerable art form. Elitist critics condemned jazz as a degenerate form of music, and jazz artists were limited to playing New Orleans brothels and Kansas City dance halls.

But, through the efforts of critics in magazines such as _Down Beat_, jazz came to be recognized as a complex art form that could be appreciated intellectually. This paved the way for landmark jazz performances in the world's most prestigious venues, from New York to Tokyo. Today, jazz is widely adored.

### 8. The digital age has created an overabundance of critics. 

These days, everybody is a self-appointed critic. From Yelp ratings to GoodReads reviews, the internet has created a glut of criticism. It's often hard to hear the voices of true critics among the overwhelming noise of amateurs. This development has consequences for our culture.

Name-calling, gut reactions and even rage are the order of the day for online reviewers. This is a far cry from the thoughtful, insightful critical writing that professional critics spend years honing. Despite this, review aggregators like Rotten Tomatoes give equal weight to traditional critics, bloggers and critics on new sites like Reelviews and The A.V. Club.

Students at universities with outstanding curricula dedicated to the study and art of critique are now in direct competition with online comment boards. If they want their voices to be heard, professional critics must publish new writing at an incredible pace.

Despite their lack of education in criticism, today's bloggers are just as, if not more influential than established, educated critics. This has sparked a fiery debate among the critical community as to whether new forms of criticism should be embraced or not. Either way, the young upstarts on the critical scene look like they're here to stay.

### 9. Critics must evolve beyond print to rebuild their discipline online. 

Throughout the history of art and culture, the printed word has been at the center of critical debate about aesthetics. But today, print faces some serious challenges.

The rise of the internet and the accompanying collapse of our attention span has changed print journalism irrevocably. Long-form printed criticism has been superseded by rapid social media commentary. By the time a printed film review is published and read, hundreds of tweets and online conversations have beaten it to the punch.

Traditional media outlets from newspapers to magazines are struggling to survive. Forced to publish content online for free to match their social-media-savvy competitors, they've seen revenues fall to an all-time low. Staff and budgets have been cut, and many publications have faded away altogether.

The age of the critic as the brilliant but controversial provocateur is fast disappearing. Today, the pressure is on to create catchy sound bites or clickbait headlines to provide websites with hits, likes and shares.

It's clear that traditional critics must move with the times. They have a lot to learn from writers who have embraced digital media. Many successful writers today got their start as bloggers and worked their way up to score roles at internet publishing giants such as BuzzFeed.

Though these new media titans look very different to traditional publishers, at heart they possess the same journalistic spirit. As the digital revolution unfolds, they may breathe fresh life into the practice of criticism.

### 10. Final summary 

**The key message in this book:**

Criticism is something we all practice in many aspects of our lives, from the way we view art to our take on how a neighbor is dressed. It happens all the time, and we don't even realize it. But true criticism has to be interesting in and of itself.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _A Curious Mind_** **by Brian Grazer and Charles Fishman**

_A Curious Mind_ (2015) investigates a vital attribute that many of us simply don't value highly enough: curiosity. These blinks explain the vital importance of curiosity, and outline the ways it can improve your relationships with your employees, customers or loved ones — and even help you conquer your fears.
---

### A.O. Scott

A.O. Scott became a film critic for the _New York Times_ in 2000 and was named Chief Film Critic in 2004. In addition, Scott is a distinguished professor of Film Criticism at Wesleyan University and writes for the _New York Times Magazine_ and _Book Review_.

