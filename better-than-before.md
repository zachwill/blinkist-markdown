---
id: 550fef8f666564000a200000
slug: better-than-before-en
published_date: 2015-03-23T00:00:00.000+00:00
author: Gretchen Rubin
title: Better Than Before
subtitle: Mastering the Habits of Our Everyday Lives
main_color: 29B9CF
text_color: 1A7482
---

# Better Than Before

_Mastering the Habits of Our Everyday Lives_

**Gretchen Rubin**

_Better Than Before_ (2015) gives you the strategies to both create and maintain good, healthy habits, and break the habits that don't serve you.

---
### 1. What’s in it for me? Kick your bad habits, and stick to your good ones. 

Mahatma Gandhi once said, "Your actions become your habits. Your habits become your values. Your values become your destiny." And if your destiny isn't reason enough to improve your habits, then nothing is.

The truth is, we all have good habits and bad habits, and we want more of the good ones and less of the bad. But how to achieve that?

Starting with understanding what kind of person you are, and then suggesting specific, simple strategies that you can use this very day, these blinks provide a roadmap to conquering our bad habits and sticking to our good ones.

In these blinks, you'll find out

  * how a Swedish musical staircase convinced people not to take the elevator;

  * why men gain weight after getting divorced; and

  * why some burglars found chocolate in a millionaire's safe.

### 2. In order to take control of your habits you must know thyself. 

Aristotle's imperative — "_Know thyself!"_ — still rings true today. To ditch bad habits or stick to good ones, you need to know who you are. And you'll likely fall into one of these four personality types:

If you're an _Upholder_, you find it easy to respond to your own and others' expectations. You always do everything on your to-do list. However, you tend to dawdle when there are no clear-cut rules or expectations.

For example, if going to the gym is in the calendar, an Upholder will go even if the weather is terrible, or she's tired from work. If it's not in the calendar, though, she'd probably skip it even if it would be a fine day to work out.

If you scrutinize what's expected of you and comply only if they make sense to you, you're more of a _Questioner_. This usually means that when you want to start a new habit, you take forever to gather momentum.

A Questioner wanting to start a gym habit can benefit from exercise apps or other data sources, because when he sees statistical proof that he's losing weight, he'll be more motivated to stick to his gym routine.

If you're an _Obliger_, you have no problem meeting expectations when they're imposed on you, but you find it difficult to impose expectations on yourself. Therefore if you as an Obliger want to go to the gym more, you should get an exercise buddy who'll pressure you into going.

Finally, if you're a _Rebel,_ you resist all expectations, whether they come from you or other people. Authenticity and self-determination are your guides. If you're a Rebel you should refrain from putting things in your calendar (if you have one at all) and just say to yourself you're going to the gym today because you want to, not because it's in the calendar.

Which type are you? Knowing this will help you form new habits.

### 3. Using a calendar and monitoring your habits will make creating and maintaining the good ones easier. 

You know the feeling when you're at a restaurant, and there are so many tasty options of the menu that you just can't decide? The simple truth is that for us humans, making decisions is hard.

That's why when you're trying to pick up a new habit like say, going to the gym, you have to take decision-making out of the equation. If you need to separately decide each and every day whether to hit the weights or not, you'll probably end up skipping most of your workouts. So don't think — just do! Make the decision today to put your workouts for the next months in your calendar, and you'll no longer need to make any other decisions. Just follow your calendar!

What else can you do to make picking up new habits easier?

Start monitoring your behavior.

Consider your diet. In 2010, 70 percent of Americans were overweight. Being overweight is the number one cause of cancer and diabetes, so it's no surprise that the most common habits people would like to adopt are eating less and eating healthier.

One of the most effective methods to form such habits is to keep a food journal in which you write down everything you eat.

Research has shown we're terrible at gauging how much we consume. We also have the desire to consume things in units of one, like one hamburger or one pizza, no matter how big the portion. This can be a considerable problem, but keeping a food journal will help you get your eating under control by tracking how much you've _actually_ eaten.

If you want more exercise, you can buy a pedometer, a gadget that counts your steps. Use this to monitor how much you move each day and strive to increase your number of steps.

One 2003 study showed that the average American takes around 5,117 steps per day, which is only about half as many as we need to stay healthy.

> _"Decide, then don't decide."_

### 4. New beginnings are great for introducing new habits. 

Ever tried to change a nasty habit, like nail biting? It's not easy. Changing our habits is notoriously difficult because our daily routines are so ingrained in us.

But when a major change occurs in our life, like moving house, getting a new job, or even finding new love, our routines suddenly change, or even disappear altogether. Such times are ideal for creating new and better habits!

Consider a study of people who wished to overhaul their diet. A whopping 36 percent of those who were successful had recently moved to a new place.

Another study found that students who wanted to start exercising more or watch less television were much more likely to succeed if they had recently enrolled at a new university.

Research has also found that marriage and divorce can massively impact our habits — particularly our eating and exercise habits. Divorced men, for example, often gain weight; conversely, women tend to gain weight after marriage.

So why do major life changes make it easier to adopt new habits?

Starting afresh alters our perspective on life and makes it easier to create new routines.

Say you're a single parent whose priority has been generating enough income to ensure your children are provided for and will be able to attend college. When your children leave the nest and there's no more tuition fees to cover, what then?

Moments like these can shed new light on life and are often an optimal time to begin forming new habits. For example, now that you have more time and freedom on your hands, why not take that rock climbing class you always wanted to?

### 5. It’s easier to positively shape our lives when we make good habits convenient and bad ones inconvenient. 

Let's face it — most of us are pretty lazy. So if we want to maintain our good habits, we need to make them as convenient and easy as we possibly can.

Say you want to start socializing more. If you have to go to the trouble of making arrangements and calling your friends every time you feel like socializing, you'll often end up by yourself because you can't be bothered. However, joining a group that has regular meet-ups, such as a book club, can make things far easier because it minimizes the effort needed.

Another great way to make something easier is to make it fun.

For example, when the stairs in a subway station in Sweden were transformed into a keyboard that played notes as you walked up them, the number of people who took the stairs instead of the escalator increased by 66 percent!

Conversely, we should strive to make bad habits inconvenient to indulge in. Even a slight inconvenience can have a substantial subconscious effect.

For instance, studies have found that people place smaller portions of food on their plate if they're using tongs instead of more convenient serving spoons.

Another study showed that when the lid of an ice cream cooler was left open in a café, 30 percent of the customers bought ice cream, whereas only 14 percent bought ice cream if they had to open the lid themselves.

We can use this knowledge to our benefit, especially when it comes to altering our daily habits, such as what and how much we eat.

Anne Bass, the millionaire and socialite, was once forced by burglars to open her safe. To the burglars' surprise, they didn't just find money and jewelry inside — they also found chocolate! Bass explained that her chocolate consumption had begun to get out of hand, and this was her way of cutting back.

### 6. Safeguard against temptation and don’t use excuses. 

Do you ever feel like your entire life revolves around resisting temptation? Well, you're not entirely wrong. Research has found that we spend around a quarter of our waking life struggling to resist some kind of temptation. It is no surprise then that often our success in adopting good habits or ditching bad ones depends on our ability to resist temptation.

How can we do this effectively?

We can start by anticipating and minimizing our exposure to temptation. Doing this is essential to breaking bad habits.

The old adage "out of sight, out of mind" is often useful. Jacob Tomsky, an expert on hospitality, observes that alcoholics often request that the minibar be emptied of alcohol before they check in to their hotel rooms.

Or take the story of the Sirens in Homer's _Odyssey_. Odysseus is warned against listening to the haunting song of the Sirens, famous for luring seamen to their deaths. In order to protect his crew, he has them put wax in their ears, blocking out the temptation.

Resisting temptation is also the key to maintaining good habits, for we're often tempted to come up with excuses to avoid them.

Strangely, even if we have a firm habit of doing something we enjoy and that is good for us, we often still try to find loopholes and excuses not to do it.

One of the most oft-used loopholes is the _Moral Licensing Loophole_, i.e., when we treat ourselves to something "bad" — like scarfing a candy bar or splurging on a new pair of shoes — after doing something "good." If you're trying to lose weight, for example, you might indulge in a burger and fries after a workout. But since losing weight has more to do with changing what you eat than with increasing your exercise, such "rewards" are counterproductive.

Remember the Obligers from the first blink? They have an especially hard time with this, as their motivation is to please others. So when no one's watching, it's easy for them to find excuses.

> _"Nothing stays in Vegas."_

### 7. It’s easier to form good habits when you find the right distractions and learn to work without the promise of rewards. 

Why do we always view distraction as a negative thing? When it comes to refraining from bad habits, distractions can be a real blessing.

So the next time you feel the need to fight off unwelcome thoughts and cravings, try to distract yourself.

Why?

Everyone knows that trying to suppress your thoughts or cravings will only exacerbate them, so a far better approach is to shift your focus. That is, to distract yourself.

Research has shown that if we learn to move our attention away from a craving, it will weaken within just 15 minutes.

When used in the right way, distraction can also be a fantastic way to reduce stress and worrisome thoughts. But what should you distract yourself with?

Research indicates that distraction as a strategy functions best when we shift our attention to something fun and absorbing, rather than something stressful or arousing. So, opt for _Kung Fu Panda_, not _Schindler's List._

Another surprising fact when it comes to our habits is that rewarding good behavior, which might seem like a positive thing, can actually make enforcing good habits trickier.

We often assume that rewards make it easier to form new habits. The opposite, however, is true. Doing things in order to be rewarded can be highly addictive, and if the reward is later withdrawn, what's the point in behaving in that way anymore?

Studies have shown that rewarding children for good behavior is not in many cases only futile, but can have the inverse of the desired effect.

In one study, two groups of children were given Magic Markers to color with, with one group being rewarded for using them and the other not. The result? The group that received the reward was actually _less_ likely to continue coloring afterward.

> _"The reward of a thing well done is to have done it." — Ralph Waldo Emerson_

### 8. Pair good habits together, and give yourself a treat now and then. 

We've seen how rewards aren't always useful when we want to stick to our good habits. Luckily, there are other approaches we can use. One is called _pairing_.

Pairing is simply taking two activities, one that you enjoy doing and one that you want to do but find a bit more difficult, and making them dependent on one another. This makes it easier to keep up good habits.

Let's say you want to maintain your habit of running more, as well as reading more books. Why not get an audio book and resolve to only listen to it while you're running. This way, if you want to find out what's going to happen in the next chapter, you need to get your running shoes on!

If you take some kind of medication, try placing your tablets next to your coffee machine and only have a cup of coffee when you've taken your meds. As long as drinking coffee isn't a habit you're trying to break, this kind of pairing is a great way to ensure you take your medication when you make your morning brew.

Next up, let's look at _treats_.

Creating new habits, like working out more, eating less or giving up smoking, can be hard. It's therefore important to give yourself a lift here and there without encouraging the need for a prize to keep up the good work. The solution is to treat yourself now and then.

Treats differ from rewards in that we don't need to earn them — they're just something we give ourselves. They can be something as simple as a five minute stroll in the sun or smelling a beautiful flower. The main thing is that it's a spontaneous gift, rather than a planned one, and it's not given in return for any pre-determined accomplishment.

### 9. Final summary 

The key message in this book:

**You can create and maintain good habits, as well as steering clear of bad ones, by employing some simple strategies: Distract yourself, use treats instead of rewards, know what sort of person you are and make your good habits convenient!**

Actionable advice:

**Make a** ** _if-then_** **list.**

This consists of making a list that says "if Y happens, then I will do X." It's a sort of plan that stops us from having to make those knee-jerk decisions which often lead to poor decisions. For example: "If I finish work early, then I'll go running."

**Set up for a new habit ahead of time.**

To make new habits as easy to establish as possible, set yourself up. For example, if you want to go running in the morning, lay out your running gear the night before.

**Suggested** **further** **reading:** ** _The Power of Habit_** **by Charles Duhigg**

_The Power of Habit_ explains how important a role habits play in our lives, from brushing our teeth to smoking to exercising, and how exactly those habits are formed. The research and anecdotes in _The Power of Habit_ provide easy tips for changing habits both individually as well as in organizations. The book spent over 60 weeks on the _New York Times_ bestseller list.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Gretchen Rubin

Gretchen Rubin is the author of the _New York Times_ bestseller _The Happiness Project_, a book that has been translated into over 30 languages and instigated a movement of people wanting to live happier lives.

