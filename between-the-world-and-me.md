---
id: 5681641b704a88000700001c
slug: between-the-world-and-me-en
published_date: 2015-12-28T00:00:00.000+00:00
author: Ta-Nehisi Coates
title: Between the World and Me
subtitle: None
main_color: A62138
text_color: 8C1C2F
---

# Between the World and Me

_None_

**Ta-Nehisi Coates**

_Between the World and Me_ (2015) is an open letter to the author's 15-year-old son about the realities that face black men in America. Filled with personal anecdotes about the author's personal development and experiences with racism, his letter tries to prepare young black people for the world that awaits them.

---
### 1. What’s in it for me? Spend a few minutes inside the reality of a black writer in the United States. 

The problem of racism is deep seated in U.S. We can see it most clearly in the murder of black people by police officers, an event that happens over and over again across the nation. (And, over and over again, the police officers walk away unpunished.)

These shootings lead to campaigns and protests from people angry that such things still happen in the twenty-first century. Yet, although they are the most observable instances of racism in the U.S., the problem, of course, goes much deeper. In fact, almost every aspect of public life is steeped in racism. 

These blinks, based on the thoughts of one of America's most insightful writers, will give you an idea of what it's like to be black in the U.S.

These blinks will also show you

  * why the American Dream is a white dream;

  * why the school system hammers out creativity in black students; and

  * why changing such an unequal society will be hard.

### 2. Ta-Nehisi Coates is a black man born and raised in Baltimore, Maryland. 

Author and journalist Ta-Nehisi Coates was born on September 30, 1975 in Baltimore, Maryland. Throughout his life Coates has lived with the typical fears that black people have in the United States. But a couple experiences stand out as being the most formative.

The first occurred in 1986, when Coates was standing outside a market after school. Across the street, an unknown boy called him over. He said nothing to Coates; he simply pulled out a gun from his ski jacket, brandished it, and then put it back.

This fleeting moment solidified the notion that, because he was black, he was constantly subject to the threat of spontaneous and unexpected violence directed at him. 

The second experience involved an acquaintance, Prince Jones, whom he met at Howard University, an _HBCU_, short for historically black colleges and universities. 

Jones's mother came from poverty, but she worked hard and "made it" in America. No expense was spared when it came to her son, who was a father and engaged to be married. By all accounts, Jones's future was destined to be a happy, middle-class life. 

However, one night, while driving to his fiance's house in Virginia, Jones was followed across state lines by a D.C. cop — the same cop who would go on to gun Jones down outside his fiance's house. 

The cop, a known liar, claimed Jones was trying to run him over, and simply returned to work after being absolved of any wrongdoing.

Coates understood then that even taking the middle road — keeping your head down and working hard to succeed — wasn't enough to guarantee your safety, peace or happiness as a black American. 

These events weighed heavily on Coates, and the birth of his son gave him newfound cause to tackle these problems. As a writer, he reflects upon the fears he has for himself, the black community and, above all, his son.

> _"To be black in the Baltimore of my youth was to be naked before the elements of the world."_

### 3. Reading Malcolm X and going to Howard University helped Coates face the realities of racism and race in the U.S. 

Early on in his life Coates understood that the education he received in school was irrelevant to him. However, he did find some truth in the books he read outside of school.

Reading Malcolm X and listening to his speeches was pivotal for Coates's development by exposing him to a reality that school did not. Malcolm X, the black civil rights and human rights activist, didn't sugarcoat the truth or advocate a platform amenable to the agendas offered by the white system.

Malcolm's ideas, such as his belief that blacks should retaliate, eye-for-an-eye, against a white racist society, set Coates on the path toward his re-education, and served as an antidote to the white education system.

College, too, offered Coates a substantial view into the way the world really works.

Coates attended Howard University, a private liberal arts university focused on getting its students into law school. But, behind this education, which one could find at any school, the author soon discovered _The Mecca_, i.e., the university's ethos and ideology. It comprises everyone who has passed through Howard's doors and lecture halls, and contributed to the construction of a positive identity for blacks in America, and not simply identities that are reactions to whites.

Notable Howard alumni include writers Toni Morrison and Zora Neale Hurston, Supreme Court Justice Thurgood Marshall, activist Stokely Carmichael and poet Amiri Baraka.

Most importantly, The Mecca is an opportunity for young black people — who had been systematically denied access to educational institutions for a large part of American history — to receive a well-rounded education.

### 4. The first reality of being black is having a black body. 

What constitutes the unbridgeable gap between whites and blacks in the U.S. today? According to Coates, one undeniable fact is the reality of having a black body.

In the United States, white bodies and black bodies are treated fundamentally differently. Whites, however, are blind to this difference. 

Ultimately, whites will never be able to truly understand the black experience, as it is one they can never live themselves. Whereas black people can't even walk down the street without the fear of being profiled by the public or law enforcement, both of which implicitly equate blacks with criminality, whites, as a group, have no such experience. 

The experience of being an American, therefore, is fundamentally different depending on the color of your skin. 

An integral part of the difference in perception lies in the racism against blacks that continues to this day, despite the progress made in recent history. While this racism can be see in many aspects of life, it's most clearly manifested in a couple of forms: 

The first is police brutality, which has come to the fore most viscerally in the recent killings of unarmed black boys and men by police officers. These include the deaths of Eric Garner, Michael Brown and twelve-year-old Tamir Rice to name only a few.

The second is in the general quality of life and incarceration rates of black people, neither of which can be separated from the other. 

Compared to white people, the amount of black people in prison is immensely disproportionate. But what is it that causes such a high rate of incarceration among blacks? What do we find when we look beyond the statistics?

We find a lack in the quality of life, exemplified by a lack of community resources, i.e., community centers and public programs. We also find poverty and the presence of drugs, problems that affect black communities more than white ones, and which are driving forces for crime.

### 5. The American Dream was built upon the backs of black bodies, so it’s no wonder that racism continues today. 

The American Dream is the belief that anyone has an opportunity to succeed in America. This feel-good belief about life in America appears to be a universally positive thing. But the American Dream isn't for everyone, as it relies on the continued subjugation of black people. 

Racism didn't end with the Emancipation Proclamation or the Civil Rights Movement. Rather, it's woven into the very fabric of America. The historical and continued subjugation of black people is what has made and makes the American Dream possible.

Slavery was a common practice in the colonies before the Revolutionary War. In fact, many of the "founding fathers" owned slaves. But, after the war, the slaves brought from Africa became the foundation of American wealth. All in all, 1.5 million slaves were brought to the states, most of whom were enslaved in the South.

The Confederacy's defeat at the end of the Civil War in 1865 brought an end to slavery, not to racism. During the period of Southern Reconstruction, black people faced increasing discrimination and violence that perpetuated a segregated and fundamentally unequal society known as the Jim Crow South.

Then came the Civil Rights Movement, culminating in the Civil Rights Act of 1964, which outlawed discrimination based on race, color, religion, sex or national origin. But the Civil Rights Act was not enough to end institutional or individual racism in the United States.

Since the 1960s, institutional racism has continued much as before. Just like in the past, black people today are racially profiled by law enforcement officials; in the eyes of the media and law enforcement, black people are implicitly associated with violence and criminality, and more likely to be treated accordingly. 

Similarly, a lack of state and federal funding to provide safe and vibrant communities has damaged blacks as a class for generations. This lack of resources can lead people to resort to crime to survive. 

For every American dream, there are thousands of African-American nightmares.

### 6. On the streets and in school, blacks must fend for themselves. 

The author recalls two different systems of fear while growing up: the streets he would traverse to get to school, go downtown and come back home; and the schools, where he spent most of the day being taught information that did not pertain to him or his fellow students at all. 

For poor black communities, the streets are a place where fear is rampant. To survive, people need to figure out some way to navigate the intricate labyrinth of the "law of the streets." 

Communities have numerous gangs that control different parts of the city, and they are a constant source of fear for kids going to and from school. People map out their paths throughout the day, trying to avoid running into gang members if possible.

Yet, as Coates points out, even these gang members are afraid: they're afraid of the white world. All their displays of power are reactions to white institutions, including police, politicians and those in control of funding. 

Like the streets, the school system also shackles black life. The standard, i.e., white, education system does not relate to the lives of black kids. Coates compares it to a drug that dulls curiosity and makes kids passive. Though Coates himself was a curious child, Baltimore's school system didn't foster curiosity, but instead just kept him busy. 

He recalls sitting in French class, realizing that he didn't know any French people. To him, France was like a distant galaxy, and learning the language had zero practical application in his life.

What brought the author out of this deadlock was a re-education, sparked by his immersion in books written by blacks, for blacks and about blacks — books that detailed the lives of black people in America and that were an antidote to the numbing lie of schools and the false bravado of the streets.

> _"If the streets shackled my right leg, the schools shackled my left."_

### 7. The next generation of blacks will have to confront the racist society they live in. 

How do you prepare your children for realities you can't protect them from? How do you prepare them for the fear and hate that people will inevitably feel toward them, without them losing hope in the world? 

For Coates's son, a number of recent events have made the reality of life as a young black man abundantly clear. 

The 2012 shooting of Trayvon Martin, an unarmed 17-year-old boy from Florida, by neighborhood watch volunteer George Zimmerman was one such example. Martin was on his way to a house in a neighborhood where several houses had been robbed recently. Zimmerman noticed him, thought he looked suspicious, and called the police.

Soon, the two were involved in an altercation during which Zimmerman shot Martin in the chest. Zimmerman, who was injured during the alleged altercation, was acquitted of second degree murder and manslaughter.

Then there was the 2014 shooting of the unarmed, black 18-year-old Michael Brown by white police officer Darren Wilson. After his death, Brown's reputation was dragged through the mud, and his death seemingly justified by the fact that he had stolen cigarillos from a local convenience store.

His death sparked social unrest that continues to this day, and his killer faced no charges.

The shooting of unarmed black people is but an extreme case of a daily injustice that exists in the United States. Blacks, and specifically black men, are perceived and portrayed as criminals, thus making the violence against them seem justified, or even necessary.

But are things changing for the better?

Though progress has indeed been made in terms of racial justice since the abolition of slavery, institutional racism still exists. 

Following these killings, Coates didn't want to offer his son consolation where there was none. It just didn't make sense to try to cover such deep, historical and ongoing wounds with a band-aid. Rather, his son must simply join the struggle to create a better world.

### 8. The American Dream is a white dream sustained by a racist society that needs to change. 

As mentioned previously, the American Dream was built and sustained on the backs of black people, first by slavery and then by an exclusionary racist society. Updating the Dream is not the way to change American society. Instead, society must abandon the myth altogether in order to become more inclusive and free. 

The American Dream is a white dream, completely blind to racial injustices, and the institutions that sustain the American Dream — the education system, the media, the authorities responsible for law and order, etc. — are likewise blind to these injustices.

No matter how it is formulated, the American Dream will not be as available to blacks as to whites. After all, if the country's institutions view you as a criminal, how can you hope to rely on them to achieve the Dream?

While the individuals who succeed at achieving the American Dream need not be white, the problem is that the Dream is itself a white dream. Success is defined by white society, and blacks who manage to succeed even with the odds against them must still play the role given to them by a white society: that of the "successful black." 

To Coates, the problem isn't just the American Dream, but the structure of "dreaming" altogether.

While at Howard University, Coates came face to face with the diverse reality of black life. He had been searching for a singular essence of blackness, but what he found at Howard was that blackness wasn't a monolith. There were Christians, Muslims, black students from Africa, students from all across America, who all had unique interests across various disciplines. 

This awakening made him realize the impossibility of supplanting the American Dream with a "Black American Dream," as there is no singular black experience. The solution is not to fight a dream with a dream, but to oppose these myths themselves.

### 9. Final summary 

The key message in this book:

**Black Americans face a completely different reality than white Americans: a reality shaped by a long and continued history of subjugation. Between violence, poverty and institutional neglect, black Americans face an uphill battle in achieving success. The author's approach to preparing his son and the current generation of young black people for these realities is to tell it like it is and dismantle the myth of the American Dream.**

**Suggested** **further** **reading:** ** _The New Jim Crow_** **by Michelle Alexander**

_The New Jim Crow_ (2010) unveils an appalling system of discrimination in the United States that has led to the unprecedented mass incarceration of African-Americans. The so-called War on Drugs, under the jurisdiction of an ostensibly colorblind justice system, has only perpetuated the problem through unconscious racial bias in judgments and sentencing.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ta-Nehisi Coates

Ta-Nehisi Coates is a national correspondent for _The Atlantic_ and the author of _The Beautiful Struggle_, a memoir that explores Coates's relationship with his father Paul Coates. In 2014, he won the George Polk Award in Journalism and, in 2015, he won the MacArthur Genius Grant.

Image Ta-Nehisi Coates: John D. & Catherine T. MacArthur Foundation

