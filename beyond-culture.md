---
id: 59a95ddfb238e10005b9a23a
slug: beyond-culture-en
published_date: 2017-09-25T00:00:00.000+00:00
author: Edward Hall
title: Beyond Culture
subtitle: None
main_color: C2AB57
text_color: 756835
---

# Beyond Culture

_None_

**Edward Hall**

_Beyond Culture_ (1976) explores how people across cultures display such diverse patterns of behavior, from resolving conflict to perceiving the passage of time. These blinks highlight the contrasts among cultures, showing us why we need to look beyond our culture to better understand other people.

---
### 1. What’s in it for me? Learn to understand your culture better and then transcend it. 

Globalization has torn down barriers. Today it's not unusual to see people from Asia, America, Africa, Europe and Australia living in a single neighborhood in London, for example. You can jump on a plane in New York and be in Paris in a few hours; you can send a text from Sydney to Buenos Aires in a second.

As a global society, we have overcome the world's physical and technological barriers to connect easily with one another. Yet one hurdle to connecting remains, and that's culture.

In these blinks, you'll learn that no matter how many barriers we've conquered, culture is still a divider, determining how we act and think in groups. Even today we easily misunderstand behaviors that are unfamiliar, and this often leads to confusion, or worse. Here you'll learn how to recognize the practices that blind you and importantly, transcend bad habits and open your mind to other cultural norms.

In these blinks, you'll also learn

  * what the German language and programming languages have in common;

  * why your Brazilian colleague won't have an excuse for being late; and

  * why Pueblo Indians consider Western schools to be poor.

### 2. Your actions and thoughts are shaped by the culture in which you grow up. 

Human beings are cultural by nature. Regardless of where a person is born and raised, her culture — the ideas, customs and social mores of her community — will inevitably have an effect on the way she acts and thinks throughout her life.

From birth, we begin to learn from the people around us. In this way, a person's actions are changeable, as they suit the cultural context in which the person exists. Over time, learned actions develop into ingrained habits. Eventually, these habits become second nature, almost automatic.

By the time we've reached adulthood, these learned actions have become internalized, unconscious behaviors, specific to the culture in which we were raised.

A good example of this can be found in the way people greet each other. While the Japanese bow, Inuits rub noses. Such behaviors are taken for granted within each culture and are performed automatically. Both actions, however different, convey respect or gratitude, yet only when performed in the context of each respective culture.

So what other practices does a culture carry with it? Different cultures usually speak different languages. Some researchers have argued that the language a group speaks has a big effect on the way a group thinks.

The Sapir-Whorf hypothesis, formulated in 1929 by anthropologist Edward Sapir and linguist Benjamin Whorf, supposes that the way people see the world is significantly influenced by the language a people speaks.

In English, you can tell someone, "It rained yesterday." Yet from that statement, it's unclear whether you know it rained because you got caught in a downpour; whether you saw puddles on the ground last night; or whether someone told you that it had rained, and you were just passing on that information.

In contrast, the Hopi of Arizona encode such details through verbs in its spoken language. This communicates to the listener not only information, but the source of the information.

### 3. You perform learned cultural rituals on a daily basis – often without even realizing it. 

"Hi Steve! How are you? How are the kids?"

"Hi Kate! I'm fine. Billy and Jane are heading to summer camp soon. How are you?"

Does this dialog sound familiar? Small talk is a great example of a _cultural pattern_. When you chat with an acquaintance at a cocktail party, for example, you perform actions in a familiar and predictable sequence — such a cultural pattern is also called a _ritual._

We perform rituals daily, whether working, buying groceries, even dating. For example, while sitting in a public library, one of the author's students noticed a pattern of courtship between fellow students.

Typically, a boy would enter the library first, placing his books in front of him on a desk. A girl would sit near a boy she was interested in. The boy would "break the ice" by asking the girl a trivial question, perhaps what she was studying. The girl would respond briefly, then eventually build up the nerve to start a longer conversation.

The sequence of actions that people perform together differs from culture to culture. Settling disputes is one particular practice that is often culturally determined.

A person from England or America, for example, might first offer subtle verbal hints that something is wrong. He might then send a message through an emissary, before directly confronting the other party. If none of these actions resolve the dispute, he might resort to legal action.

People from Latin America or Mediterranean cultures, on the other hand, see handling disputes much differently. In general, people from these cultures try to avoid confrontation with coworkers or family members unless they feel that they must engage directly.

They do this because there is a risk of entering into a feud with the other party, through which a cycle of revenge can be sparked.

### 4. Different cultures have different ways of communicating, and each has its pros and cons. 

Have you ever struggled to plan an event with people from different cultural backgrounds? This situation isn't uncommon and results from cultural differences in communication.

In short, different cultures have different ways of communicating. Some communicate _explicitly_ while others communicate _implicitly._

Cultures that communicate explicitly include those in Germany, Switzerland, the countries of Scandinavia and (although to a lesser extent) the United States. In the context of these cultures, plans are typically set clearly and plainly, using words.

The downside of such communication is that a message must contain all the necessary information so there can be communication at all. This can slow things down, as messages are long and complex.

Other cultures, however, rely more on implicit communication. This means that a lot of communicated information is embedded in context and the body language of the people involved.

In Asian cultures, for example, people are on the lookout for verbal symbols or physical gestures as part of a conversation, and such gestures are easily understood by the group.

Using implicit communication means that less attention can be paid to spoken words, which speeds up communication considerably.

There are pros and cons, of course, to both styles. Explicit communication is slower, requiring more spoken information and longer messages. But the upside is that meanings can be changed quickly.

Think of it like a computer program. Once a program becomes outdated, it can simply be updated by rewriting a bit of code.

Implicit communication, in contrast, is faster in the moment but much slower to change overall. Physical gestures, in particular, rely on historical tradition for meaning. Gestures can't take on new meanings quickly, but spoken language can.

If a culture is stable for a long time, people become more able to efficiently communicate, often through developing implicit signs to speed things up. But if a culture is changing rapidly, communication remains explicit, as it allows for more flexible communication.

### 5. Cultural differences shape the way you walk, and the way you perceive time. 

We've looked at differences in styles of communication between cultures. But there's more. Even the way you walk down the street is influenced by your cultural background!

Not only do people from different cultures talk differently; they move differently, too. In fact, each culture comes with particular ways of sitting, standing, dancing and moving around.

One of the author's students filmed people walking down a street in two different states, New Mexico and Arizona. Examining white Americans and Pueblo Indians in particular, the student found 15 distinct differences in how members of each culture walked.

Culture doesn't just affect how we talk or move our bodies. Remember the Sapir-Whorf hypothesis from an earlier blink? Cultural practices affect the way we think, too. You might be surprised to find out that people even hold a different perception of time depending on cultural background.

In Northern Europe and America, people view time as a straight line, moving forward into the future. Such a view leads people to schedule work hours strictly, setting deadlines for specific tasks.

People from cultures in the Middle East and Latin America, in contrast, tend to focus on the present moment. They often prioritize tasks on the fly, based on what is most pressing at that moment. For people in these cultures, time is flexible, and deadlines are seldom hard or fast.

Differences in the perception of time can certainly explain many cultural differences. For instance, being late to an appointment is much more tolerated in Latin America than it is in the United States.

In Latin America, it is understood that you might need to prioritize an issue that you feel is more pressing than your appointment. Changing plans last minute, however, is seen as rude in the US and Northern Europe, as people from these cultures expect you to plan your schedule in advance.

> For Americans, being five minutes late requires a small apology; 15 minutes late requires an apology plus an explanation; 30 minutes late is an insult to the person with whom you're meeting.

### 6. You see the world through the lens of your culture, which can lead to a lot of misunderstanding. 

Because we view the world through our unique cultural glasses, we expect other people to act and think the way we do. It's no surprise then that there are many misunderstandings between cultures.

Actions deemed "appropriate" are in particular a sensitive area between cultures. You may be startled or even offended by the actions of a person from another culture, especially when the gesture clashes or conflicts with what you see as correct or acceptable.

Japanese hotels offer a useful example. Here it's common practice for hotel staff to move your luggage to a new room without asking your permission. Staff will do this if your room is needed urgently by another party (for example, a large family).

For the Japanese, this is a completely normal practice, and even connotes familiarity and a sense of inclusion for the guest who is being moved. Yet American and European guests, when faced with such a situation, are often shocked and insulted. Why? People from Western cultures tend to associate space with private ownership and personal status. A stranger moving your stuff is just wrong.

It can also be hard to understand the way institutions work in foreign cultures. Styles of education vary considerably from culture to culture, for example.

In Western cultures, children are trained to get ready for the job market, thus schooling is competitive and task-driven. Regular exams measure student achievement and awards are given to those who excel.

In contrast, children of Pueblo Indian descent are educated by peers and role models, spending time with them and absorbing their knowledge. This system is informal, and children play more than study before their working lives begin.

Pueblo Indians prefer this method, feeling that the Western system is unfair to children and therefore damaging to society.

> For Inuit workers in a factory in Alaska, the idea that people worked fixed, eight-hour shifts seemed ridiculous. For them, the tides signaled the hours of work.

### 7. It takes a lot of work to understand another culture, but it’s worth it. 

Our world is becoming increasingly connected, which means you're more likely to meet or work with someone from a culture different from your own.

For this reason, it's more important than ever that we learn to understand the way culture affects people's behavior.

Yet doing so isn't as easy as you might think. Understanding other cultures is difficult, often because it requires knowledge of a culture's particular historical and social context.

In Japan, for example, people are expected to use an appropriate degree of politeness depending on their relationship to a person — whether the person is a superior, a teacher, a friend or family.

This practice stems from Japan's feudal history. Until recently, social standing was determined by a person's status and wealth. People from lower ranks were required to show respect to people from higher ranks. Today's degrees of politeness is just one consequence of the country's former social organization.

Importantly, we also need to be able to see beyond our cultural lens. Because culture is so deeply ingrained, it can be hard to realize that the way you view things isn't the "only" way.

One method to better understand foreign cultures is to better understand your beliefs, even those that you might not have ever questioned.

People in Western cultures, for example, believe in competition and individual freedom. We expect people to "be themselves" and seek to stand out from the crowd. But this belief contrasts with those of other cultures, where people feel more comfortable sticking to the norm.

So what's the best way to discover cultures beyond your own? It's simple: interact with people whose cultural backgrounds differ from your own.

When you meet and interact with people from other cultures, your experiences will help you recognize differences in points of view and identify new perspectives on beliefs and behaviors you might take for granted. For that reason alone, this can be a rewarding, fulfilling experience.

### 8. Final summary 

The key message in this book:

**From the way we talk and walk to how we resolve conflicts and view the world, our cultural backgrounds determine how we behave. By interacting with people from different cultures, we're better able to recognize and understand contrasting behaviors and communicate with individuals of all backgrounds.**

Actionable advice:

**Ask questions first before you judge someone's behavior.**

The next time you're working or socializing with someone from a different cultural background and are confused by something the person did, resist the temptation to judge the behavior by your own cultural yardstick. Instead, consider how this behavior might make sense within the context of the person's culture. Better yet, _ask_ the person why he did what he did! This will help you handle such a situation more sensitively in the future, and you might even learn something new.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Culture Map_** **by Erin Meyer**

_The Culture Map_ provides a framework for handling intercultural differences in business and illustrates how different cultures perceive the world. It helps us understand these differences, and in doing so improves our ability to react to certain behaviors that might have once seemed strange. With this knowledge, we can avoid misunderstandings and maintain conflict-free communication, regardless of where we are in the world.
---

### Edward Hall

Edward Hall (1914-2009) was a renowned American anthropologist and cross-cultural researcher. He received his PhD from Columbia University, conducted groundbreaking field research across Europe, Asia and the Middle East, and authored a number of lauded books on culture and communication.

