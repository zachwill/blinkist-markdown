---
id: 57753eef3588860003e8240e
slug: beyond-measure-en
published_date: 2016-07-06T00:00:00.000+00:00
author: Margaret Heffernan
title: Beyond Measure
subtitle: The Big Impact of Small Changes
main_color: 91CFAE
text_color: 496958
---

# Beyond Measure

_The Big Impact of Small Changes_

**Margaret Heffernan**

_Beyond Measure_ (2015) shows that transforming a struggling company into a thriving one is a simple matter of making small systemic changes that empower people to speak up, collaborate and share. Discover you can stop your company from being controlled by one overworked CEO and make it into an innovative powerhouse where ideas can flourish.

---
### 1. What’s in it for me? Achieve results by working with the soft, human side of business. 

In the age of big data, many companies are obsessed with metrics. If there aren't any numbers, if something can't be measured, then how can one know what action to take? Well, some things are beyond measure.

Take relationships, for example. Could you quantify the relationship you have with your family or loved ones, with your friends? Probably not, and, as you probably know, businesses are full of relationships that can produce magnificent outcomes if they work well. So, let's take a look at how these interpersonal relationships work and what companies can do to achieve greatness through small changes.

In these blinks, you'll find out

  * what social capital is and how it helps businesses succeed;

  * why working more hours leads to less productivity; and

  * about the amazing results of bringing in an outsider.

### 2. Creative conflict within companies can be learned through practice and supportive structures. 

You've probably been there before. You're sitting in a business meeting, and someone proposes a business decision you disagree with. And because you don't want to seem hostile, you hold your tongue.

But avoiding debate is never helpful, and there are ways your company can change this unproductive habit.

The key is creative conflict, a skill that, like all others, can be learned through practice.

With creative conflict comes a willingness to disagree and challenge the general consensus so that new and creative ideas can be developed. But what many people don't know is that there's a _talent_ to productive disagreement.

Brooke Deterline is a constructive conflict specialist, and she is always astounded by how her clients assume they'll be able to productively manage group discussions on their first attempt. It's particularly surprising since people recognize that other skills, such as learning a language, a musical instrument or public speaking, take time and practice.

And when it comes to creative conflict and the ability to effectively express your opinion and meet conflict head-on, it is still practice that makes perfect.

The first key point people need to realize is that they must prepare and gain supportive allies before they take on authority figures at the workplace.

Deterline coached one of her clients, Luke, through this process when he had to stand up to his aggressive boss while negotiating a contract.

The first step was simulating how the situation might play out; they considered how his boss would react to different statements and devised appropriate counter-reactions for Luke.

Then, Luke reached out to his colleagues within the company for advice and support. This allowed him to strengthen his position further so that he could engage with his boss in a productive conversation.

So, by the time Luke had the actual negotiation with his boss, he was confident. He knew what he needed to say and how to react appropriately to whatever his temperamental boss threw his way.

> _"Buried under the dragon's foot is always a gem — something to be learned from conflict."_

### 3. Social capital is a prerequisite for constructive conflict and it can be built through small actions. 

Have you ever been part of a group project where everyone involved was unfamiliar with one another? If you have, then you probably noticed that discussions were mostly unproductive, because everyone was hesitant and nervous around each other.

To combat this kind of situation, and enable constructive conflict, it's important to establish _social capital_ within your company.

The concept of social capital isn't new. Anthropologists who studied early communities coined the term to explain why certain tribes thrived and succeeded — it's what people in a community feel when they can rely and depend upon one another for help, and it remains a great survival tool for companies today.

And you will find that when you combine creative conflict with social capital, your company will enter into a cycle of prosperity.

This happens when people engage in creative conflict. The experience allows coworkers to bond with one another and improves their relationships. Naturally, this creates even more social capital within the company, which in turn builds more trust so that people feel more confident to engage in future creative conflict.

But creative conflict isn't the only way to build social capital.

One company used a different kind of creativity to help improve collaborative efforts between departments. The CEO launched a company-wide project in which each department was charged with making a short film that highlighted the work of a different department.

They weren't expected to make an Oscar-winning movie, nor did they have to: people only need to take small actions in order to build social capital.

And the project worked perfectly. The whole company gathered in a movie theater to watch the results, and the movies not only turned out to be creative and original but also got the previously uncooperative company members talking and bonding with one another.

### 4. Working too much reduces productivity, cognitive ability and health. 

Working longer hours should mean that more work gets done, right? Actually, that might not be the case. Over 100 years ago, scientist Ernst Abbe discovered that reducing the workday from 9 to 8 hours _increased_ overall productivity.

Even now, studies continue to show that overworked employees are less productive, but companies remain reluctant to change their ways.

Since Abbe's discovery, many studies have shown that people working over 40 hours a week are less productive and more prone to making mistakes, all of which results in a less efficient company.

Yet these companies remain largely blind to these facts. Apart from industries like aviation or truck driving, where long hours can lead to deadly accidents, many continue to believe that workaholics make the best employees.

This holds true even in the face of compelling evidence. For example, while 50 percent of all mergers or acquisition deals fail, few people question whether or not this could be due to exhausted CEOs making poor decisions that could be avoided by working more reasonable hours.

Working too much can lead to what's known as tunnel vision. When people are tired, their ability to think and reason diminishes, and they often become stubborn in their decision-making, refusing to adapt to changing circumstances or even recognize existing errors.

Naturally, under these conditions, employees become increasingly inefficient in their work, which unfortunately results in having to work even longer hours!

What's worse is that being overworked can lead to health problems as well.

In 2012, the Finnish Institute of Occupational Health showed that working 11 hours a day doubles a person's risk of depression.

Furthermore, employees who work more than 55 hours a week start to show signs of losing their language and problem-solving skills by middle age. All this in addition to the slower reaction times that overworked people tend to have.

> _"A half hour walk can prove wildly more productive than staying late at work."_

### 5. Companies can find innovation by working with outsiders and connecting to the world. 

You might think that the analytical mind of a scientist and the abstract mind of an artist would make for an unproductive pairing. But such a team can actually make some astounding breakthroughs.

This is exactly what happened at Roche Pharmaceuticals. Team leader Matthias Essenpreis was at a loss when his group was trying to improve the company's diabetes products.

He knew his team needed an exciting new perspective, so Essenpreis came up with an unorthodox idea: to invite an artist to work with the team.

It was an effective strategy. While his team of scientists approached problem-solving in a logical way, the artist, Kelly Heaton, took a more holistic approach. Rather than focusing on the technical aspects, the artist took on the perspective of the diabetes patient.

She was also a great communicator and her fresh perspective allowed her to ask the right questions when the team was stuck.

As a result, in 2003, Roche developed diabetes packs containing an innovative new device that allowed patients to simultaneously administer insulin and measure their blood sugar.

Thinking outside the box can also lead organizations to breaking boundaries and connecting with other businesses.

Traditionally, companies tend to look inward; they create protective policies and focus on safe products to maintain a comfortable status quo. These days, however, they are discovering that true innovation comes when companies consider themselves as interactive parts of a bigger corporate picture.

We can see this change of perspective in ARM, a company that specializes in semiconductor and software design. When ARM started, it was a tiny company of only 12 employees.

In order to grow and increase production, ARM learned to collaborate with much larger firms that had thousands of employees. Rather than struggling on their own, they grew by becoming a part of other companies' teams.

Even today, many of ARM's employees spend most of their time working within other organizations.

> _"I couldn't say whether it's that we go out, or we let them in, but either way, there's just no real boundary between us."_ \- Tom Cronk, general manager at ARM

### 6. Flat hierarchies enable more innovation and prevent bad leadership. 

Companies that don't collaborate are often less creative than companies that do. Indeed, collaboration is, in some ways, the name of the game. And that's why companies hoping to innovate should adopt a _flat hierarchy_.

A flat hierarchy is a company structure that eliminates the levels of middle management that separate employees from executives.

Paul Harris, CEO of South Africa's FirstRand Bank is a proponent of flat hierarchies. For Harris, part of this structure's appeal is a matter of efficiency.

Vertical hierarchies, with their long chain of managers, are needlessly complicated: the flow of information from top to bottom can often get stuck in middle management, leaving people lower on the ladder clueless as to what their bosses want them to do.

But more importantly, flat hierarchies give every member of a company the opportunity to contribute creative ideas, which makes for a much more innovative environment.

When FirstRand Bank introduced a flat hierarchy system, the idea of introducing an electronic payment method and giving clients the ability to use their cell phones to transfer money, easily made its way from idea to implementation and is now a service for every FirstRand Bank customer.

The thing is that, when one person has too much power, it's often a recipe for disaster.

Research in this area shows that when people become powerful they also become worse at listening to others, failing to take into account the opinions of their employees. They get so preoccupied with pursuing their own agenda that the ideas, emotions and perspectives of other people simply fall by the wayside.

Many of the traits can be traced back to the fact that these powerful leaders tend to feel that all the responsibility rests on their shoulders. Therefore, they are reluctant to seek help or assistance, fearing it will be perceived as weakness.

Implementing a flat hierarchy is one way to avoid ending up with a bad leader.

> _"I believe that the more power you give away, the more you have, because when people are empowered they… will not let you down."_ \- Paul Harris, CEO FirstRand Bank

### 7. Final summary 

The key message in this book:

**Having a successful company isn't just about generating billions of dollars in profit; it's about fostering creativity, innovation and open communication. And you can achieve this with some small but important changes — like cutting down on work hours, flattening hierarchies and encouraging people to voice their opinions. Simple changes like these can make all the difference.**

Actionable advice:

**Lead from where you are.**

It doesn't matter whether you are the CEO of a company or a regular employee. Look out for opportunities to lead your team to success. For example, practice voicing your opinion as often as possible. This may result in some annoyance at first, and you may make mistakes, but your team will thank you in the long run for getting the ball rolling and showing that you care about the company.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Small Move, Big Change_** **by Caroline L. Arnold**

In _Small_ _Move,_ _Big_ _Change,_ you'll learn why it is we so often fail to follow through with the changes in our lives that we so desperately want to make. Author Caroline Arnold explains that we fall short because we're not crafting our resolutions in a way that is actually achievable. Instead, we should focus on "microresolutions" — small, easy-to-keep commitments that add up to big change.
---

### Margaret Heffernan

Coming from a multicultural, Cambridge-educated background, Margaret Heffernan went on to become the CEO of many corporations, including _InfoMation_, _ZineZone_ and _iCAST_. She is also the prize-winning author of _A Bigger Prize_ and _Willful Blindness_.

