---
id: 548a150d3363370009630000
slug: beyond-outrage-en
published_date: 2014-12-23T00:00:00.000+00:00
author: Robert B. Reich
title: Beyond Outrage
subtitle: What Has Gone Wrong with Our Economy and Our Democracy, and How to Fix It
main_color: F28130
text_color: A65821
---

# Beyond Outrage

_What Has Gone Wrong with Our Economy and Our Democracy, and How to Fix It_

**Robert B. Reich**

_Beyond Outrage_ provides a sobering analysis of what has gone wrong in American politics and economics. Looking at the distribution of wealth and income imbalance, it convincingly argues that we must wrest government from the hands of the regressive right.

---
### 1. What’s in it for me? Learn what is wrong with the American economy and why the tea party is not the solution. 

We all live in a democracy, which is supposed to mean that the people are reigning. But when you think about it, do you really think that you and your fellow ones are the ones who decide where America is going? Probably not: in a recent poll, 77 percent of the US citizens polled said that too much power is in the hands of a few rich people and corporations.

Something has gotten out of balance in the American democracy and economy, the game has become rigged against the average US citizen while it favors the rich and global corporations. In this book, you'll learn about what has gone wrong, how the Tea Party movement is making it worse and what can be done about it.

You'll also find out:

  * why the rich are effectively paying less income tax than the middle-class;

  * what motivates the oil industry to donate $150 million every year on political campaigns; and

  * why Henry Ford paid his workers three times more than other factory workers were paid.

### 2. Government policy over the last 30 years has allowed the gap between rich and poor to widen dramatically. 

Have you ever wondered why, despite the fact that the American economy has grown consistently over the last three decades, your wage hasn't changed a bit?

In fact, the average American worker's monthly pay has grown by only $280 in thirty years. We have to ask ourselves, who is benefitting from all of this economic growth?

While the working and middle classes have seen very little change in their wealth, the rich have enjoyed the lion's share. Indeed, over the last few decades the gap between rich and poor has grown at a terrifying rate.

In the '60s and '70s, the top one-percent earned somewhere between nine and ten percent of the total income nationally. In 2007 that number had more than doubled at 23.5 percent. Even more frightening is the fact that the wealthiest 400 Americans are now wealthier than the 150 million lowest earners put together!

Moreover, the average wage of CEOs has gone from being "only" 40 times that of their average workers to 400 times over the same 30 year period.

So what caused this startling rise in inequality? In essence: tax cuts that mainly benefit the wealthy.

The United States saw several tax cuts for the rich under both the Ronald Reagan and George W. Bush administrations. Looking at the period between 1958 and 2008, you notice that the average _effective tax rate_, i.e., the rate people actually pay after all the tricks and loopholes have been exploited, has dropped from from 51 percent to only 26 percent for the top one percent.

What's more, the rich also earn a great deal of their income in the form of _capital gains_, that is, profits that come from stocks, bonds or real estate. Capital gains are taxed at only 15 percent instead of the 35 percent for wages in their tax bracket.

Meanwhile, the income tax on the average middle-class worker has increased from 15 to 16 percent.

> Fact: Sixty-six percent of Americans say that there should be a more even distribution of wealth.

### 3. Under the pretense of creating jobs, the government defends corporate interests in return for donations. 

Anyone who follows American electoral politics knows that presidential campaigns are expensive. Political ads, staff and tour busses all come with a huge price tag.

Take a deep breath and let this sink in: the 2012 general election in the US, the most expensive in American history, cost $2 billion.

So where do politicians get all that cash? Mostly, it comes from the deep pockets of big corporations.

Yet, as they say, there's no such thing as a free lunch. In return for donations, businesses expect to get something in return.

And indeed they do! In fact, large corporations have vast influence in Washington. The oil industry, for example, spends around $150 million _each year_ on political campaigns. In return for their "generosity," they expect the government to create laws that benefit their business.

These benefits could come in the form of tax breaks, promises not to sign environmental protection bills or contracts for cushy government jobs. Just imagine that the tax breaks for the oil industry earn them an estimated $2.5 billion a year — well worth their initial investment!

Politicians defend their coddling of big business by pontificating about the job opportunities these companies create for Americans.

This claim, however, doesn't ring true; most of the jobs created by today's corporations are not in the United States, but overseas. Why?

The answer is economic: corporations are driven only by the profit motive, and have no interest in helping the American middle class. Unfortunately for Americans, cheap labor is found elsewhere.

According to a 2012 survey by _Wall Street Journal_, the 35 largest American corporations created 333,000 jobs abroad, while only creating 113,000 in America itself from 2009 to 2011. Apple, for example, employs only about 43,000 people in the US — yet it has nearly 700,000 workers abroad, mostly in China.

As we'll see, shrinking tax revenue means that the government has less money to sustain public services, which has dire consequences.

> _"We may have democracy or we may have great wealth concentrated in the hands of a few, but we can't have both." — Supreme Court justice Louis Brandeis_

### 4. The public sector is declining due to vast budget squeezes which mainly affect the poor. 

Do you get excited about tax season? Not likely! No one likes paying taxes, and many people actively try and reduce the taxes they pay by voting for politicians who advocate tax cuts.

Yet, campaigns to slash taxes have an obvious consequence: worse public services. Lost tax revenue means government must cut basic public services that we all need, such as schools, highways, hospitals, safe bridges, police or public transportation.

For example, spending on education and infrastructure has seen a dramatic drop from 12 percent of GDP in the '70s to just three percent in 2011.

Although these cuts everyone, they're especially damaging to the poor.

In 2012, for instance, the Republicans in the House of Representatives decided to cut $3.3 _trillion_ from low-income programs over the next ten years. This translates to a drop in health care coverage for 14 to 28 million low-income people and a 17-percent reduction in food stamps.

These cuts are even more dramatic when you consider that, according to a study of the Agricultural Department in 2011, nearly one in four children belonged to a family that could not afford sufficient food at some point in the year.

People sometimes counter this by appealing to the generosity of America's wealthy elite, pointing to its long tradition of charity. Yet, today's seemingly charitable donations are often not designed to actually help the poor!

Most philanthropy from the wealthy is directed at cultural institutions, like operas and art museums, or at the universities from which they graduated. These are more investments in their lifestyles than true charity.

In fact, only 10 percent of all charitable donations actually help the poor or support organizations that do so!

> _"Without a government that's focussed on more and better jobs, we're left with global corporations that don't give a damn."_

### 5. Declining middle class wages have a dramatic consequence for the American economy. 

In 1914, the famous automobile magnate Henry Ford did something revolutionary: he decided he would pay his workers three times the wage a typical factory employee earned at that time.

Although this might seem crazy, there was a huge upside. Ford employees could now afford Ford cars, thus turning his workers into customers. And his idea worked! After only two years, Ford's profits doubled.

It's this basic deal between American employers and workers that made the American economy so strong: employers paid workers enough to live a decent life, and their paychecks in turn kept the economy growing.

This was the key to America's economic success and growing middle-class. Armed with a decent income, Americans could spend their earnings on American-made products, which in turn created even more jobs and higher salaries.

In the late 1970s, however, the deal was broken. Tax breaks for the wealthy gave them a much larger share of the economy at the expense of declining wages for the middle class.

This was especially true after the 2008 recession. For example, if you started to work at General Electric in 2002, you would earn between $21 to $32 per hour. In 2012, after the crisis, they earned only $12 to $19.

The decline in middle-class wealth isn't something we should ignore, and in fact has serious consequences for the health of the American economy.

In order for an economy to grow, it needs healthy middle class people to constantly purchase new products. It can't rely on spending of the wealthy, because they eventually reach a point at which there is nothing else for them to purchase. They already have everything, and would rather save money at that point.

And if money goes unspent, then the economy cannot grow; it can only stagnate.

By this point, you've seen how favoritism towards the wealthy elite has damaged the American economy. We'll now look at one of the major forces in the United States working to further dismantle the few existing social programs.

### 6. The populist Tea Party movement is regressive, radical and very dangerous. 

The anti-government and tax-cutting policies we've seen aren't just driven by the wealthy and powerful. They also find supporters among the "normal" American population, who have organized themselves into a group known as the _Tea Party Movement_.

Tea Party members are often associated with the Republican Party, and consider themselves to be conservatives. In actuality, they're quite regressive.

True conservatism is about conserving the good parts of government and society that we already have. In contrast, the Tea Party wants to abolish the public achievements made over the years, hence taking us _back_ in time _,_ preferably to the US of the 1920s.

It was in these times that the US government felt comfortable letting the free market reign — a time when there was no Social Security, labor laws, unemployment insurance or minimum wage.

Moreover, people who identify with the Tea Party are ideologically distinct from moderate Republicans. They are, at their core, an angry white minority who, according to political analyst Michael List, come primarily from poor, rural, heavily conservative areas in the Southern — i.e., former Confederate — states.

Consequently, their political positions differ from Republicans in certain areas. A 2011 CNN poll among Republicans revealed considerable ideological splits. For instance, Tea Party members were twice as likely than other Republicans to believe that abortion should be illegal under every circumstance or that gay marriage should be categorically forbidden.

Indeed, the Tea Party is ideologically extreme, immune to science and unwilling to compromise on anything. Their strategy is simple, yet decisive: they'd stop at nothing to achieve their ends.

Just look back to 2013, when House Republicans, with the aid of the Tea Party, forced the government to shut down at tremendous cost to American taxpayers because they were unhappy with Obama's budget.

### 7. The Tea Party’s stance on social services resembles life in prehistory. 

The regressive right has become obsessed with a return to the old and "pure" America, in which the government had no teeth and citizens were left to their own fate. The America that they want to recreate also had a different social mentality, one shaped by the unfortunate ideas of _Social Darwinism_.

Simply put, Social Darwinism holds that the principle of survival of the fittest — that only those who are strong deserve to survive — will lead to a strong society.

The concept of Social Darwinism was developed in the 1880s by a Yale professor of political and social science William Graham Sumner. He believed that, in order for civilization to progress, it must be totally free. Social classes would have no obligation to one another, and those too "unfit" to survive under free market capitalism would die out.

These callous sentiments are well represented in Tea Party ideology. They believe that government should do as little as possible to help the needy, as doing so creates unnatural dependencies and disrupts the process of natural selection.

Let's look at some of Ron Paul's views to get an idea of how Social Darwinists think:

In a Republican debate in 2011 on Obama's healthcare plans, he was asked how he would handle a young patient who had fallen into coma without having previously purchased health insurance. Of course, without insurance, he couldn't pay the treatment.

His response: "That's what freedom is all about: taking your own risks." In essence, if he died, it would've been his own fault — survival of the fittest.

By now, you've seen just how frightening contemporary social and economic policy can be. Our final blinks will offer you some advice on what you can do to change it.

### 8. Defeating the Tea Party and making positive change requires active citizenship and political engagement. 

If you've read this far, then you should be thoroughly terrified by these recent political and economic trends. So what can you do to enact change?

For starters, you can get more involved in the political process. Citizenship means more than just checking a few boxes on an election ballot. No positive change will come unless the right people mobilize, organize support and make it happen. The politicians in Washington, regardless of their intentions, need someone to push them to do the right things.

Heed the words of Franklin D. Roosevelt. During Roosevelt's 1936 re-election campaign, a woman listed all the problems that he needed to fix. He replied: "Ma'am, I want to do those things, but you must make me."

So how do you make them? You'll have to take action! But you can't make a difference on your own; you have to join forces with others or, even better, start your own movement!

Unfortunately, politicians and media won't pay attention to average individuals. But they will pay attention to thousands of people that have come together for a common cause!

You don't necessarily need formal authority or a fancy title in order to get people energized and organized. You only need strong motivation and an infectious passion for change.

There are dozens of examples throughout history — such as Martin Luther King Jr., Mahatma Gandhi and Nelson Mandela — of individuals whose sheer desire for change inspired great social and political movements.

What's more, you need to seek out the people who disagree with you. When you only talk to people who share your views, then you have no hope of changing anything!

This means stepping outside of your ideological bubble and engaging with your opponents in order to persuade them.

> _"Moral outrage is the prerequisite of social change. But you also need to move beyond outrage and take action."_

### 9. We should increase taxes for the rich to finance investments in education and infrastructure. 

So you know now _how_ you can accomplish change. But _what_ sorts of changes should we implement? Here are some points that you might want to consider:

Blessed with their vast wealth, the rich should be expected to contribute a larger share to society. In practice, this means paying higher tax rates than those who have less.

The fact that the 400 richest Americans pay less income tax than most of the middle-class workers due to the tax loophole created by capital gains is simply intolerable, and must be changed.

Moreover, the super-rich should pay the same income tax rate as they did in the '50s and '60s: 70 percent on income over one million dollars.

Another idea would be to levy a two-percent surtax on the assets of the richest 0.5 percent of Americans. Each of these individuals has more than $7.2 million in wealth, meaning they possess an unprecedented _28 percent_ of all the nation's wealth! Such a tax would generate around $70 billion per year in revenue.

So what do we do with all this additional revenue? We should invest it into public goods, especially in those which are woefully underfunded but are also crucial to the country's future: education and infrastructure.

Kids are packed like sardines in the classroom, schools can't afford textbooks or science labs, teachers receive a pauper's pay: lots of schools desperately need money, and this lack of funding puts the education of America's children at risk.

Meanwhile, we drive on unsafe roads and our subways, buses, ports and airports are sorely in need of repair. After examining the existing American infrastructure, the American Society of Civil Engineers gave it an overall grade of a D — hardly something to brag about.

As you can see, there's still lots to do, and we are in strong need of active citizens who want to bring progressive change. It doesn't matter _how_ you do it; the important thing is _that_ you do it.

### 10. Final summary 

The key message in this book:

**The political and economic game is rigged against the American middle class. Big money and the regressive right have taken over in Washington and have set the American economy on a crash course. Luckily, with the right attitude and policy initiatives, we can put things back on track.**

**Suggested further reading:** ** _The Spirit Level_** **by Naomi Klein**

_The Spirit Level_ provides a detailed explanation of how inequality is responsible for many of our present-day problems, including violence and mental illness. It provides detailed explanations and studies to support this and shows how inequality not only hurts the poor but everybody in a society.
---

### Robert B. Reich

Robert B. Reich is a Professor for Public Policy at the Goldman School of Public Policy at the University of California. He has worked for three presidential administrations, last serving as Secretary of Labor under Bill Clinton, and has also worked as an adviser for Barack Obama. In addition, he has written twelve books that champion liberal causes.

