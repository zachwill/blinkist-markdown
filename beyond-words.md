---
id: 56e6d43b9854a500070000b0
slug: beyond-words-en
published_date: 2016-03-15T00:00:00.000+00:00
author: Carl Safina
title: Beyond Words
subtitle: What Animals Think and Feel
main_color: 9BCA5B
text_color: 4C632D
---

# Beyond Words

_What Animals Think and Feel_

**Carl Safina**

_Beyond Words_ (2015) is an explanation of the animal world's propensity for thought and feeling. These blinks walk you through the complex societies of the natural world and explain how animals think, experience real emotion and learn.

---
### 1. What’s in it for me? See animals in a whole new way. 

Humans have always lived side by side with animals. We have feared them, worshipped them, domesticated them, even brought them into our homes as pets. But how much do we really know about what they feel and think? And can they think and feel in the first place?

Answering this question is harder than it might seem. First, we must learn to see animals as unique creatures, with different needs, minds and feelings than us, and then start viewing them on their own terms. Only then can we truly get to know them. Interestingly, there's a lot of research that shows that they can think, feel and communicate in complex ways. You'll never see animals the same way again.

In these blinks, you'll learn

  * how wolves became dogs;

  * how to get sex if you're a chimpanzee; and

  * how killer whales communicate.

### 2. We share some characteristics with animals, but their minds are entirely different. 

We all know the term "puppy dog eyes" and most of us have felt a surge of emotion at the sight of a pooch wearing this seemingly sad expression. But can dogs really feel emotion?

Actually, _anthropomorphism_ and _anthropocentrism_ are huge roadblocks to seeing animals as they truly are. That's because scientists are trained to observe and aren't meant to attribute human characteristics to other beings, or in other words to anthropomorphize them. 

A scientist could say that an elephant stood between her child and a hyena, but would shy away from saying she did so to protect the infant. After all, there could be another reason. 

And anthropocentrism?

That's the idea that humans are uniquely capable of feeling and thinking. This is major, because if other animals can't feel or think like us, then why should they have any rights?

So, a better way to view animals is by acknowledging that they have minds, albeit minds of their own. For example, thanks to their superb hearing, an elephant will notice an approaching truck or a herd of animals long before a human does. As a result, they might move way before their handler even spots something coming. Failing to acknowledge this fact makes it easy to misinterpret elephant behavior. 

Therefore it's key to acknowledge the differences between humans and animals but also those between all creatures on an individual basis. That's because every human is the same but different — and that's also true for all other species. 

For instance, humans have a long common history with other animals and therefore share common roots, but we have also diverged from them in a variety of ways. Nonetheless, we still share goals like those of survival, reproduction and finding food as well as shelter. Given all of these commonalities, the idea that animals can't think or feel is totally absurd.

But how can we prove that they can?

> _"Only humans have human minds. But believing that only humans have minds is like believing that because only humans have human skeletons, only human have skeletons."_

### 3. Most experiments and theories can’t uncover what animals are thinking. 

If someone asked you to design an experiment to determine what thoughts were running through an animal's head, what would you do? 

Well, one popular theory for testing the animal capacity for thought is called the _theory of mind_, or the ability to determine what others are likely thinking, and then to respond accordingly.

Here's how it developed.

In 1978, researchers showed a group of chimpanzees films of humans getting cold or failing to reach a banana. The chimps were then given a choice between different pictures that fit the problem. For instance, a picture of a heater to warm up or a picture of a stick to reach a banana. The chimps failed to select the correct image, ostensibly proving that they lacked a theory of mind. 

But evidence of chimp behavior in the wild contradicts this. That's because, in natural conditions, where only the alpha male has sex with the females, chimpanzees lower in the ranks will sometimes challenge the leader's dominance. They do this knowing that it will prompt the alpha male to cause a scene, creating a diversion during which the lower ranking male can copulate with a female behind the alpha's back. 

So, this tells us that chimps know how the alpha male thinks and how he'll respond to their actions. But it doesn't tell us much about _what_ the chimpanzees are thinking. 

In fact, other experiments and explanations have also failed on that front. For instance, self-awareness can be tested by placing a dye mark on an animal's forehead and having it look in a mirror. If the animal recognizes itself and attempts to remove the mark, it is thought to be self-aware.

However, this test only proves an understanding of reflection — that the mirror image is not in fact you. It doesn't prove anything about self-awareness. 

Even looking at an animal's brain is unhelpful. For example, although an albatross's brain is extremely similar to a human's, its intelligence is nothing like ours. The bird simply needs different skills, like knowing how to find its way through several hundred miles of stormy weather.

### 4. Domestication is key to understanding certain animals and our relationships to them. 

As you might know, the domesticated dogs of today are descended from wolves. But you likely don't know how this process of transformation occurred. Actually, dogs domesticated themselves — here's how.

Surprisingly enough, it wasn't the selective breeding of humans that domesticated dogs, but the adaptation of the animals themselves to the human settlements that provided them with food. This made it advantageous for wild dogs to be friendly to humans, as the friendly wolves got more food, bred more and eventually became what we know as dogs. 

But while they gained safety and food, they lost independence. For example, if a dog is presented with a locked box, it will look for human help, while a wolf will attempt to open it. And that's not all that changed about the animal; this process even transformed their appearance.

Experiments on foxes in Siberia have found a genetic link between friendliness and floppy ears and curled tails. The friendly foxes were those most selected by humans, and they therefore had more offspring, making floppy ears and curly tails more common. 

But selective breeding can also occur with zero human intervention. For instance, as we learned earlier, among chimpanzees, the dominant male holds a monopoly on reproduction. He therefore aggressively protects his position.

However, the close relatives of the chimp, the bonobos, are a different story. Among this species the females rule and it is the friendly males, not the aggressive ones, which are preferred for breeding. As a result, the species has become friendlier over time. This is essentially an example of developing a characteristic temperament by selective breeding. 

So, while domestication is not well understood with regards to animal intelligence, what we do know about it helps explain the human limitations to understanding animal thoughts and feelings. Just think of how some people assume dogs are stupid because they are loyal even when abused. But this isn't a matter of poor intelligence; the dog simply knows that being loyal to the one that feeds it will help it survive.

### 5. The brains of other mammals are similar to those of humans. 

Did you know that the basic structure of your brain is very similar to that of other mammals?

This surprising fact says a lot about animal thought because having brains and hormones comparable to those of humans makes other mammals act, feel and think much like we do. In fact, animals actually demonstrate cognition, the ability to perceive and acquire knowledge.

For example, elephants consciously learn which plants to eat from older elephants by observing them and tasting the foods they eat. So, while our experience of the world might differ from theirs, we certainly don't have a monopoly on consciousness. 

However, science has long argued that animals can't be conscious because their cerebral cortex is simply too small. Yet there are humans like Roger, a 30-year-old who lost 95 percent of his cerebral cortex to an infection, yet still knows who he is. 

And brains can even teach us about how animals feel. For instance, the hormone oxytocin is essential to bonding in humans as well as most other mammals. So, if it's blocked in an individual's brain, they're likely to be more solitary. The fact that the same is true for humans and animals suggests a similarity both in our brains and our feelings. 

But what about the animals with tiny brains? Can they feel too?

Actually, brain size is a poor indicator of animal intelligence. A better one is how similar an animal's brain is to ours. That's because a larger brain, while it means more computing power, isn't necessarily helpful. For example, tuna have smaller brains than dolphins but are just as good at hunting.

Large brains _are_ necessary for being social and cooperating with others. That's because when individuals are responsible for the survival of a group, a bigger or _social_ _brain_ is needed. Just think of the tactics employed by chimps to assert dominance. 

In the case of animals that _do_ have social brains, like primates, elephants, whales and dolphins, it's hard to deny that they're thinking and feeling like humans when their brains so closely resemble ours.

> _"Species who have the most complex societies develop the most complex brains."_

### 6. Elephants live in organized societies, cooperating and caring for each other. 

If you've ever observed elephants you know that they organize by group, a clear indication of their complex society. In fact, elephants even understand intra-group relationships, like which baby belongs to whom. This is called "understanding third-party relationships" and it's key to functioning as a group.

Not just that, but they're a species led by a matriarch who holds the knowledge and history of the group. When she dies, another female will take her place, the group will divide or merge with another. Furthermore, as it's essential for elephant families to remember where watering holes and food sources are located, they require adaptable brains, capable of learning.

So, when elephants are born, their brains are only 35 percent as heavy as an adult elephant's brain, giving them plenty of room to develop. Compare that to 90-percent brain weight at birth in most other mammals!

Elephants are also cooperative and when lost will seek out others. In fact, they can even feel the emotions of another elephant.

For instance, when an elephant is hurt, another will feed it, showing that they understand such situations and respond with compassion. Just as in humans, this behavior releases oxytocin in the elephant's brain, rewarding them for helping others. 

But how did this organized elephant society develop?

It's all due to their long process of rearing offspring. Male elephants over 30 experience _musth_, an annual period of increased reproductive hormones that makes them aggressive and competitive for months at a time. On the other hand, although females come into heat from age 11 onward, it's only for four days at a time. If impregnated, they'll carry a child for two years after conception and nurse it for another two. That means each female elephant is only ready to mate on four days every four years, easily explaining the competition among males!

So, making a baby elephant is hard work and those babies need to be protected. Since testosterone-charged adult males disrupt the peace of the group, young males remain subordinate in matriarchal families and leave them when they are grown.

### 7. Human actions have shaped animal consciousness. 

Can you imagine someone hunting you down with a gun? Most of us would be terrified. So it's no surprise that that's how animals respond, too. 

Orcas, a species that have been systematically hunted — even with machine guns — dramatically decreased in number in the twentieth century. These so-called "killer whales" hunted the same fish that fishermen prized and were therefore seen as competition. On top of that, their young were captured to stock aquariums.

As a result, killer whales began avoiding certain locations and routes. But while North America largely banned their killing or capture in the 1970s, they continue to be hunted in Greenland and Indonesia to this day. 

Elephants also have to deal with human predators. Ivory poachers have drastically reduced the African elephant population, which used to cover the entire continent. The ivory trade was banned in 1990, but animal habitats and food supplies have still declined. In Kenya, the human population has quadrupled over the last 40 years while the elephant population declined by four-fifths. 

This change has caused the elephants to adopt unique responses to people. For instance, elephants are terrified of the spear-wielding Maasai, a semi-nomadic ethnic group of Kenya and Tanzania, because these tribespeople have often hurt elephants when trying to protect their own cattle. 

So, when elephants living close to the Maasai were presented with three different T-shirts, one belonging to a Maasai, another to a different local and one to a researcher, they only reacted with fear to the Maasai shirt, literally smelling the danger. 

The same goes for voices: the animals were relaxed in the presence of English-speakers, but responded with fear to the voice of a Maasai, meaning they know not all humans are the same. 

Nonetheless, neither elephants nor killer whales commonly show hatred for people. In fact, elephants are generally friendly to humans and the only instances of killer whales harming a person occurred in captivity. Orcas have even been reported to aid humans in need, protecting them from other predators.

> _"Penning an Icelandic whale with Pacific Northwest residents might be the killer whale equivalent of putting a Neanderthal mammoth hunter in a cell with three Japanese waitresses."_

### 8. Wolves live in complex societies and are capable of generosity. 

So, elephants' brains are comparable to those of humans and they therefore have similar intelligence. But what about other animals?

Well, it used to be thought that a pack of wolves was led by an alpha male. However, every member of the pack is crucial, from the alpha female he's breeding with to their pups. That's because the alphas need their offspring to hunt and need a partner to rear those pups. 

This is especially important as wolves are one of the few animals that hunt prey larger than themselves, like elk, a task that demands cooperation and teamwork. Every member of the pack is necessary, as some run faster while others are heavier and more capable of bringing down prey. Not to mention that this process must happen in a flash, before the dangerous antlers of an elk can do any damage. 

So, the alpha pair need to be confident leaders who enable the team. Leaders who are too aggressive often lead to a lack of cooperation among pack members. Not just that, but such alphas are in danger of being overthrown by other wolves in the pack. For instance, when a fight breaks out every wolf takes a side and the alphas risk losing their position if they've been too aggressive with the others. 

Alright, now you know wolves are organized, but did you know that they can also be generous?

For example, a group of wolves was reintroduced to Yellowstone national park in 1995 to restore the natural balance of elk. The best killer among them was a wolf named "Twenty-One" and in all his time hunting in the park he never once killed a rival wolf. He would even let cubs beat him, demonstrating an awareness of how his actions would be perceived by others. 

He would always prove stronger in fights and, as the loser would naturally decline in status, Twenty-One's territory and status were secure. Therefore he had easy access to mating and food and had no reason to kill his rivals.

### 9. All wolves are different, but historically they’ve been marked as evil. 

We all know that every person is different and the same is true for animals. However, personality in animals goes largely unacknowledged and it's an area of study that requires more focus. 

For instance, "Oh-Six," the granddaughter of Twenty-One, was an even more capable hunter. She could bring down two elk all by herself, a feat unheard of until that point. Naturally, her pack flourished. But during a hard winter, Yellowstone proved too small a hunting ground and they left the park. 

Oh-Six died during the transition along with some of her pack and children. The pack was lost without her, and after days of frantic searching for their leader, they broke apart over leadership battles and poor decisions. It was Oh-Six's leadership and individuality that made her pack so strong; when they lost her they lost a teacher. 

So, wolves can express individuality, but plenty of other animals can understand the individuality of species other than their own, even of humans. For example, ravens who are tagged by researchers will recognize the person who marked them even years later. 

The birds respond to these people with loud screeches and attacks. As a result, most researchers now wear masks when catching ravens to avoid being hunted down. 

But despite having individual personalities, animals — wolves especially — are mired in problems caused by humans and their prejudice. For example, wolves have long been symbols of evil and fear in literature. They represent uncivilized dangers, a lack of law and order. As a result, during the European Middle Ages, wolves were hunted by members of the church who viewed them as the devil's dog. 

Thankfully, not all humans treat animals this way, and indigenous peoples have often experienced better outcomes with wild animals like wolves, lions and tigers which they respect. For instance, for a long time in Siberia, tigers would not kill humans, but that all changed with the arrival of settlers who didn't honor the animals in the same way.

### 10. Killer whales have a highly developed society, but one that’s divided. 

If you only looked at their teeth, killer whales would resemble a seafaring T. Rex. But get to know them and you'll see that they're cooperative, generous and pose no threat to humans. 

Why?

Killer whales live in extremely complex social systems in which mothers play an essential part. For instance, families live with their matriarch and, while they'll mate while socializing with other families, will continue to travel with the matriarch afterward as she's responsible for memorizing the routes and best feeding locations. 

To communicate, each family uses unique calls but also shares certain common ones with other families whose calls unite them in a _pod._ Multiple pods form _clans_, which also employ a specific set of calls. Clans socialize with other clans with the same calls to form a _community_ which will not interact with other killer whale communities. 

But just how long do matriarchs lead their clans?

Actually, they can remain in their position long after entering menopause. This is a rarity in the natural world as, generally, animals who stop reproducing struggle to survive. But with killer whales, elderly matriarchs play a crucial role in the survival of the young. 

In fact, male killer whales under 30 are three times more likely to die if their mother does because they rely on her for food. Even female whales over 30 are 2.5 times more likely to die when their mother does. 

Another fun fact about killer whales is that they enjoy sex! That even goes for those who have entered menopause and whales of the same sex. Many killer whales will even masturbate by rubbing against boats or arouse other whales by rubbing against them. 

So, there are lots of interesting commonalities among killer whales, but they also differ from one another enormously. Different communities keep their distance and those who live off fish don't socialize with those who eat mammals. 

They're still seen as a single species with different types, but the fact that they uniquely self-segregate and don't interbreed indicates that they're actually a different species altogether.

### 11. Killer whales communicate in complex ways, as do other animals. 

So killer whales can communicate with each other, but the fact that humans can't understand them is actually a result of our own poor hearing. Sound travels better in water than in air, so killer whales are vocal creatures, and we just don't have the ears to hear them properly. Orcas can communicate across distances of 150 miles using sound to send messages and locate fish!

Human visual perception is also lacking, which means that what we call visible light is just a fraction of the light that exists. Many animals see more of it or just view it differently than we do — sometimes "seeing" with senses other than sight. Interpreting sounds can be considered a _form_ of vision that even humans can learn. 

For instance, American campaigner Daniel Kish has been blind since he was one, but he gets around using clicks, like a sonar. He simply clicks in a manner that resonates in the space around him, thereby producing an image of the environment in his mind. He's so skilled that he can even ride his bike in traffic. 

So, clearly communication is more than just words. Just take elephants, who can "talk" by making vibrations at frequencies that humans can't even hear and can barely sense. These amazing animals pick up rumbles made by other elephants over a distance of miles — vibrations unnoticed by humans. Even scent is a form of communication, like the way that flowers speak with insects through their various smells. 

And if it seemed like the natural world couldn't get any smarter, think again, because animals can even learn human languages. Dolphins in Hawaii have demonstrated an understanding of syntactical differences after learning some human language — they could discern between the spoken human command to receive a ring from person A to give to person B and the opposite. 

If that's not a clear sign that animals are using their heads, then what is?

### 12. Final summary 

The key message in this book:

**Every animal is capable of thinking and feeling, but judging other animals based on human experience can cause their abilities to appear limited. The truth is that animals have complex and profound mental capacities, a variety of skills and feelings — just like us.**

Actionable advice:

**If you think something is specific to humans, think again.**

Plenty of things that were once thought of as special to humans have been shown to exist in other animals. For instance, the ability to teach used to be thought distinctly human, but just as cats deliver live prey to their young to show them how to hunt, killer whales instruct their calves to follow them. So give animals more credit, keep your eyes peeled and you might just notice that animals are more like you than you think!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Genius of Dogs_** **by Brian Hare and Vanessa Woods**

_The Genius of Dogs_ (2013) uncovers the remarkable intelligence of man's best four-legged friend. By first examining human intelligence, the authors go on to explain exactly what makes dogs so smart, which talents they have in common with humans and other animals, and what sets them apart.
---

### Carl Safina

Carl Safina holds the endowed chair for nature and humanity at Stony Brook University and is also a staff member of the Alan Alda Center for Communication Science. He has published in _National Geographic_ as well as the _New York Times_ and hosts a PBS series.

