---
id: 559bc55d3966610007650000
slug: big-weed-en
published_date: 2015-07-08T00:00:00.000+00:00
author: Christian Hageseth
title: Big Weed
subtitle: An Entrepreneur's High-Stakes Adventures in the Budding Legal Marijuana Business
main_color: 80943F
text_color: 54612A
---

# Big Weed

_An Entrepreneur's High-Stakes Adventures in the Budding Legal Marijuana Business_

**Christian Hageseth**

_Big Weed_ (2015) is a first-person account of entrepreneur Christian Hageseth's success in the evolving business of legal marijuana. He reveals all the essentials you need to know about a future in this new market, and explains a bit about the basics of marijuana that any budding entrepreneur should know.

---
### 1. What’s in it for me? Learn how to do business in the newly legalized marijuana industry. 

Stoners. Drop-outs. 4/20 and Bob Marley. When you think of marijuana, what comes to mind?

Few think of the cultivation and sale of _Cannabis sativa_ as a legitimate and legal business, yet with the decriminalization of marijuana in many U.S. states, that's just what it's become.

In fact, with the right strategy, a budding entrepreneur can make it big in the pot business. Based on the experiences of entrepreneur Christian Hageseth, these blinks tell the story of one man's foray into the world of weed and shows how you too can turn kind bud into green cash.

In these blinks, you'll discover

  * why you can't sell bud and expect a bank to give you a business account;

  * why there's more to product research than just smoking up;

  * how learning the facts about marijuana will make you a better businessperson.

### 2. Public pressure and the potential for increased tax revenue has pushed pot into legal territory. 

In the 1920s in the United States, the federal government prohibited the sale and consumption of alcohol. Yet Prohibition was a disaster; the greater public flaunted the law at every turn and the government lost millions in tax revenue. The laws were eventually repealed.

Today, marijuana is America's bathtub gin. While many law enforcement officials continue to demonize pot, public support for the decriminalization of marijuana has been steadily rising. 

Despite prolific warnings over the dangers of using drugs, it's clear that the so-called war on drugs in America has been a failure. Not only have thousands of people been incarcerated for essentially non-violent crimes but also drug policies have destroyed families and lives.

Yet the benefits of a drug like marijuana are many. It can act as powerful medicine to ease the suffering of the chronically ill, for example. Proponents thus say pot should be made legally available to such groups. Why deny chemotherapy patients medicine that could ease their suffering?

Separately, politicians have started to understand just how much money can be made from legalizing the sale of marijuana.

In the few U.S. states where the drug is now legal, tax revenue is filling state coffers. In 2009 alone, Colorado, a western state that has led the charge for legalization, collected some $3 million in taxes, fees and associated costs.

Public pressure, combined with the backing of select politicians, has led to huge changes in marijuana policy. While many states have passed statutes allowing marijuana only for medical use, states like Colorado also have legalized pot for recreational purposes.

This more liberal attitude toward marijuana is sure to increase, giving rise to a "new world of marijuana." Yet rather than being a business controlled by gangsters and drug cartels, it will be a normal industry with brands, business strategies and tax accountants.

> _"Capitalism had ridden in on a big, green, sticky horse and saved the day."_

### 3. Early efforts to make marijuana “evil” have persisted; many still hold a negative view of pot. 

Attitudes to marijuana in the United States have relaxed in recent years. But this doesn't mean that everyone believes that it should be legalized; many think drugs across the board are dangerous.

Why has this negative opinion of marijuana persisted in society?

One of the main issues with regard to legalization is that after so many years of being classified as an illegal drug, marijuana is still associated with street crime and criminals.

If you ever used marijuana or had friends who did, the likelihood that they purchased it on the street from a dealer is high. And the fear persists that marijuana is a "gateway" drug. If you buy drugs from criminals, what's to stop you from accessing and experimenting with other, more dangerous, drugs?

These are some of the main reasons why so many people still campaign against legalization. The author himself ran into this when he set up his own, _legal_ _marijuana_ business. 

Many friends and business associates were skeptical. After all, they said, wasn't marijuana just a vice for stoners and criminals?

Interestingly, marijuana's inclusion on the list of illegal substances was actually done for political reasons.

Even though marijuana has been a part of human society for hundreds of years, in the twentieth century, political forces found reasons to curb its use. (A marijuana pipe was found even in the gardens of legendary playwright William Shakespeare!)

Politicians, looking for a scapegoat for societal problems and eager to protect their own economic interests, worked to ban marijuana. They believed that criminalizing the drug and disseminating anti-marijuana propaganda was a vote-winning and money-making tactic.

What is more surprising is that such campaigns continued despite growing scientific research that clearly showed marijuana wasn't the societal evil it was billed as.

In 1972, a Senate commission report revealed that marijuana did not in fact lead to physical dependence. What's more, the overwhelming majority of people who used marijuana did not then move on to other harder drugs.

This report was, however, ignored.

### 4. Selling cannabis is more than just opening up a head shop. You need a proper business strategy. 

Since marijuana has for so long been illegal, early attempts to profit from legalization have been unorthodox, to say the least.

Many marijuana sellers simply find a shop, buy some stock and try to sell it. And most fail.

Achieving success in the pot business requires you to think exactly how you would in any other business niche. You need to come up with an effective business strategy.

The author began his business with a clear goal in mind: to make marijuana America's new consumer product, branded and marketed intelligently.

In addition to marketing, you will also need to keep tight controls on the growing process.

Marijuana plants aren't easy to cultivate, as they require lots of light. Yet lots of light leads to heat and humidity, which is actually bad for the plants. So it makes sense to purchase an air conditioning system for your grow space. This in turn amounts to a great deal of energy use!

To lower your energy costs, the best approach is to think in broad terms from the outset. One large air conditioning unit costs less to run than a number of smaller ones. To make a large unit worth the expense, however, you need to invest in a lot of plants.

The author found this out the hard way. After three failed harvests, he increased his production size and found just the right balance for success.

Since marijuana is still treated with suspicion, as a pot entrepreneur you need to work on creating a culture in which it's more normalized and accepted. The author learned this firsthand after a trip to the Netherlands, where drug laws are harsher than in the United States but the culture more accepting of marijuana use.

> _"The struggle going forward is about normalizing marijuana. It's about making it part of our cultural landscape again, as okay as every liquor store you pass every day."_

### 5. A wake-and-bake lifestyle won’t bring you success. Do your homework and know your market. 

So you're ready to light up and be a marijuana entrepreneur. Think about how great it would be to sit around and "test" your product, all day!

Not so fast. Before you inhale, consider your business. You need to inform yourself about every single aspect of the market if you want to stand any chance of actually making a profit.

First, learn as much as you can about your product.

Marijuana can be used in many different ways. Although smoking is the most well-known method, it isn't really the best. Inhaling the smoke of burning plant material means toxins are going directly into your lungs.

Some better alternatives include vaporizers, which heat up the pot until the plant's cannabinoids become airborne and can be inhaled. Another alternative is to bake the drug into cookies or other food items, for a delayed effect. However, this is harder to dose.

Next, get to know your market. Create a brand to market your product effectively. As in any industry, if you want to sell successfully and keep customers coming back, you need to have a ubiquitous, trusted brand.

The author created his own brand, Green Man Cannabis. With the desire of becoming the market leader in the industry, he took care to brand every area of his business effectively.

Finally, you need to understand your customers.

The author's goal is to introduce marijuana to a wider audience while at the same time demystifying the product. To that end, he's building a large-scale marijuana growth facility to educate others.

The Green Man Cannabis Ranch, which will include a terrace, atrium, open greenhouse and a shop, will be a place where customers can better understand his brand and his product.

And once you've put in the hours and work to grow your business in a smart way, sure — go ahead and kick back occasionally and enjoy the benefits of your product!

### 6. Federal laws on marijuana clash with many state laws. Be prepared for obstacles in doing business. 

The legal marijuana business is a business like any other. But there is one area in which legal marijuana sellers are actually prevented from participating in what should be normal business practices.

While most business owners can open a bank account to safely store their profits, legal marijuana business owners cannot.

As marijuana is still illegal in many U.S. states and according federal statutes, and any money deposited in U.S. banks is backed by the federal government, the feds can deny anyone involved in an industry they deem illegal from holding or depositing funds in a bank account.

For those involved in the legal marijuana businesses, this is a real headache. Cold hard cash is your currency for almost everything, including paying for electricity and gas — even your taxes. It's time-consuming as well as risky, as you'll be carrying serious amounts of cash around with you often.

It will probably take some time to overcome such discrimination on both on the state and federal level toward those in the legal marijuana industry.

Police officers, politicians and judges were taught almost all their life that marijuana is bad. Thus it may be a while before these groups will be able to let go of their prejudices, even when marijuana is legalized.

Keeping marijuana illegal however causes more problems than it solves. The so-called war on drugs has massively increased the number of law enforcement agencies and prisons around the country. Some 800,000 Americans annually are arrested and sentenced for marijuana crimes, which begs the question of how prisons are able to cope with all these inmates.

Unfortunately, the criminalization of marijuana and all drugs is also a business for many industries, and powerful lobbies have an interest to keep things they way they are.

So if you plan to get into the legal marijuana business, be prepared to face a number of obstacles.

> _"We are not criminals, but the law is forcing us to behave like them."_

### 7. The marijuana economy will evolve rapidly as attitudes change and federal interests grow. 

The legal marijuana business is still in its early days, yet with time, it will flourish while also changing significantly.

The tobacco industry is now barred from selling legal marijuana, but this won't always be the case. Big agriculture at some point will get involved, industrializing cultivation quickly.

Thus the days when you'll be able to pick up a pack of marijuana cigarettes at the corner store are probably not so far away! But whether that pack will be natural and organic is another question.

The forces of capitalism will not be long in molding this nascent industry, as larger, better capitalized firms buy out smaller, local dispensaries and push national brands.

A comeback for hemp is also a possibility. German car manufacturers BMW and Mercedes-Benz use hemp for car door panels; this is coming soon to the United States. As hemp becomes more ubiquitous, marijuana's bad rep may also fade.

And although decent-quality pot will be readily available for consumers in this expanded market, serious connoisseurs may seek out higher-quality products — another business opportunity.

Today's pot is far stronger than the weed groovy kids were smoking at Woodstock in the 1960s. Marijuana then contained about three percent THC (pot's psychoactive component) compared with up to 25 percent today.

We probably won't see a trend for stronger marijuana, but instead for consistently dosed servings. We know that one ounce of vodka has the same alcoholic effect as five ounces of wine and 12 ounces of beer, for example.

One joint, however, can't be compared to another. This will certainly change, if not just to make recreational use more measurable and predictable.

Finally, we will also see a change in U.S. federal policy. While legalization today is not a top federal priority, with greater demand and a need for increased tax revenues, the call for legalization will grow.

### 8. Final summary 

The key message in this book:

**The push to legalize marijuana will continue to evolve in the United States. Already, the public's perception of marijuana culture has changed for the better; law enforcement too is grappling with new attitudes and newer laws. As marijuana becomes a serious and legal business opportunity, society may take a more accepting and constructive look at how to market and use the drug responsibly.**

Actionable advice:

**Consider what a marijuana tax could do for your town.**

The next time you're involved in a discussion on legalization, think about the dollars and cents of both sides of the argument. Are the associated costs of keeping marijuana illegal greater or less than the benefits your community could reap were it to collect taxes on legal marijuana use?

**Suggested further reading:** ** _Chasing the Scream_** **by Johann Hari**

_Chasing the Scream_ (2015) gives a riveting account of the first hundred years of the disastrously ineffective War on Drugs. Weaving together fascinating anecdotes, surprising statistics and passionate argumentation, Hari examines the history of the War on Drugs and explains why it's time to rethink addiction, rehabilitation and drug enforcement.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Christian Hageseth

A serial entrepreneur, Christian Hageseth is also the founder of _Green Man Cannabis_, a highly regarded legal marijuana company that has twice won the industry's most prestigious prize for growing the best weed in the world.

