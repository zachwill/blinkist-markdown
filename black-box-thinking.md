---
id: 568a66f18944a6000700002d
slug: black-box-thinking-en
published_date: 2016-01-04T00:00:00.000+00:00
author: Matthew Syed
title: Black Box Thinking
subtitle: The Surprising Truth About Success (And Why Some People Never Learn from Mistakes)
main_color: C42D27
text_color: C42D27
---

# Black Box Thinking

_The Surprising Truth About Success (And Why Some People Never Learn from Mistakes)_

**Matthew Syed**

_Black Box Thinking_ (2015) explores the ways in which failure, despite all the shame and pain associated with it, is actually one of our greatest assets. Full of practical tips on how to develop a healthy, productive relationship to failure, _Black Box Thinking_ will put you on the path to success.

---
### 1. What’s in it for me? Learn how to extract the positive from failure. 

Failure. Whether you failed to pass an exam, failed to woo someone you found attractive or failed to whip up a great meal for your friends, failure is failure. And it's one of the most annoying and scary things out there.

But maybe failure isn't just annoying and scary and counterproductive. Maybe it's also the opposite of those things. These blinks show how failure can be used to your advantage — how it's in fact key to progress and success.

In these blinks, you'll discover

  * why prosecuting innocent people isn't a rare event;

  * how the invention of the ATM sprang from failure; and

  * how good you are at learning from your mistakes.

### 2. People are afraid of failure because it compromises their self-esteem. 

Children have a hard time admitting their mistakes. It's practically automatic for them to deny doing things like drawing all over the walls, even when the evidence — the marker in their hand and the ink on their fingers — is indisputable. But are we that much different when we're all grown up?

Not really. In general, people are highly averse to admitting that they've made a mistake. In fact, we hate _admitting_ that we've made an error more than we hate making mistakes themselves!

A look at the criminal justice system makes this very clear. 

In 1984, the advent of DNA testing enabled prosecutors to prove guilt beyond doubt. You'd think that this fool-proof technology would work the other way around, too — helping wrongfully convicted people prove their innocence. Unfortunately, it usually didn't work that way. In most cases law enforcement simply wouldn't admit that they'd made a mistake. 

Take the case of Juan Rivera, a 19-year-old with a history of mental illness. In 1992, he was accused of raping and murdering an eleven-year-old girl, and sentenced to life in prison. Thirteen years later, a DNA test proved Juan's innocence. But prosecutors wouldn't budge, and it took another _six_ years for his release. 

So why is it so hard to admit mistakes? Well, admitting error compromises our self-esteem, especially when it's about something important.

Those prosecutors from the Rivera case weren't necessarily bad people. They may have simply wanted to cover up their mistakes.

Perhaps the hardest part of admitting mistakes is the first part — admitting to _yourself_ that you've made one. This is especially true when the mistake is a big one, like sending an innocent person to spend 13 years in prison. Admitting such a horrible mistake instantly compromises your self-esteem, making it difficult to even live with yourself? 

So, in all likelihood, the prosecutors truly believed that Rivera was guilty, and that there was some explanation for the negative DNA test that didn't rule out guilt.

> _"It is only when we have staked our ego that our mistakes of judgement become threatening."_

### 3. Failure certainly hurts, but it’s a necessary precursor to improvement. 

As you saw in the previous blink (and no doubt learned from some experience in your own life), it's extremely difficult to admit mistakes. But failing face and understand failure has consequences: it hinders our ability to succeed.

Failure is more than personal shame; rather, it's an indication that something is wrong. And when you know that something is wrong — be it your personal attitude or the way a company is organized — then you have an opportunity to fix it. 

Think about it like this: when playing basketball, every missed basket is, technically, a failure. You obviously made a miscalculation, a mistake. Maybe you're holding the ball incorrectly, using too much force or jumping awkwardly. Each time you miss the basket, you know that you've somehow failed to shoot a perfect shot. 

It's by adjusting your behavior according to the feedback you get from failure that you improve, and ultimately succeed. All of those missed shots provide you with crucial information about how to get things right next time — how to hold the ball, how to jump — so that you can eventually score. 

Nature works in the same way. Species evolve over the course of hundreds of thousands of years, with each generation passing on the mutations that made survival easier. It's as if each species makes a record of the things that _almost_ got it killed in order to ensure that future generations are better prepared for danger. 

A team of Unilever biologists employed a similar method when attempting to design a nozzle that wouldn't get clogged. In total, they produced 449 designs, taking the best design from each series until they eventually developed the most effective nozzle.

### 4. If you can’t admit your mistakes, then you’ll never progress. 

Imagine a world in which _no one_ admitted to or learned from their failures. In such a world, mistakes would be repeated again and again, with drastic consequences.

It's often obvious whether someone succeeded or failed: a patient either lives or dies, a plane either lands or crashes. The subtlety lies in the explanation: was this failure due to a mistake, or not?

But it's not always obvious that a change in action would lead to a change in outcome. Would the patient have lived if treated differently? Would the plane have crashed if landed elsewhere?

It's precisely this vagueness that makes it so easy to shirk responsibility for mistakes. But if you can't admit mistakes, how will you learn to do better next time?

In the medical profession, mistakes are so unacceptable that doctors and nurses rarely admit to making them. As a result, mistakes are repeated, ultimately at the expense of the patient's health. Studies estimate that at least 40,000 people die per year in the United States due to medical mistakes.

In some fields, however, failure is simply impossible. As a result, these fields make virtually no progress.

For example, pseudo-sciences like astrology haven't progressed at all in centuries. The assumptions underpinning astrological predictions are simply too vague to be falsified.

Another great example is bloodletting, which was a common medical practice before clinical trials became standard in the nineteenth century.

Doctors would drain blood from patients, in an attempt to cure or prevent diseases. Although this only weakened the patients when they most needed their strength, doctors nonetheless employed this practice for over 1700 years. They had no idea they were literally killing patients, as they'd never bothered to test the practice.

So far, we've focused on the negative impact of not owning up to mistakes. In the following blinks, we'll explore how to put your failures to good use.

> _"Self-justification, allied to a wider cultural allergy to failure, morphs into an almost insurmountable barrier to progress."_

### 5. To learn and develop, you have to subject your theories to failure. 

We tend to see the world as simple, and easily understandable. As a result, we rarely feel the need to test our theories. But this deprives us of the opportunity to see if these theories are true or false!

The world is big and scary, so it makes sense that we'd look for simple explanations wherever we can find them. Think back to the practice of bloodletting: medieval doctors thought that patients who died were simply doomed from the beginning. Such patients were so far gone that _even bloodletting_ couldn't save them.

Though it may be hard to admit, the world isn't simple. Difficult situations often stem from numerous causes. Simplifying things only prevents you from actually making sense of the world by testing your theories.

The medieval doctors never tested the validity of bloodletting because they had no reason to. They already "knew" why the patients died — at least, they thought they did.

But giving ideas an opportunity to fail makes room for new ideas and progress. No matter how reasonable an idea may seem, you can never be certain of its validity if you don't put it to the test.

One way to test a theory is to perform a _randomized control test_ (RCT), in which you test something against a control group that helps make the cause of failure clearer.

For example, if you wanted to test the efficacy of bloodletting, you could gather ten patients, each suffering from the same illness, and split them into two groups: the bloodletting group and the control group. The bloodletting group gets a bloodletting procedure; the other group gets no treatment.

If everyone in each group dies, then you don't have enough information to make an informed judgment about bloodletting. However, if everyone in the bloodletting group dies, but half of the control group survives, then you have to admit that bloodletting is not only ineffective, but downright harmful.

### 6. Failure inspires great solutions and helps fine-tune a complicated processes. 

Failure can be annoying. But it can also inspire you to see problems in a different light. And with this new perspective comes new solutions. 

Often, great ideas arise when there's a specific problem — that is, when something has failed. The failure itself is what drives you to find a solution, and in this way, failure can function as a driver of progress. 

Think about the ATM, for example, conceived one day when John Shephard-Barron forgot to go to the bank to pick up some cash. In other words, he failed at having cash when he needed it. But through his failure, a new solution arose: a money-dispensing machine that is open when banks are closed. 

In addition to catalyzing novel solutions, failure is also a way to fine-tune complicated processes, as it helps us discern a problem's component parts.

The more complicated the process, the harder it is to fine-tune. Complexity makes it harder to see exactly _where_ things went wrong.

Say you want to help improve education throughout the African continent. How do you know if your aid is making a difference? Looking at grades alone doesn't tell you much, because the problem is simply too large to know what causes a certain change. 

However, by allowing yourself to fail on a small scale, it becomes easier to discern which strategies are working, and then apply them on a larger scale. 

For example, in Kenya, a group of economists wanted to improve the quality of local schools. They started by recording the grades at different schools, and trying out various things, to see if they improved scores.

Their first idea was to deliver free textbooks. However, they soon discovered that schools without this aid performed just as well. So, they tried several other approaches. Finally, they stumbled upon a solution that actually helped improve grades: de-worming medicine.

Once a small-scale solution like this is developed, it can be tested on a wider scale.

### 7. Reaching your full potential requires embracing failure. 

If you want to take full advantage of failure, it's not enough to understand intellectually that failure is helpful. You also need to build a positive relationship with it. 

If you can't handle failure — if you run from it instead — then you'll end up failing more than is necessary.

In fact, a fear of failure can cause people to create unnecessary barriers to success.

For instance, the author recalls some of his classmates, the "cool kids," who used to go out partying the night before exams. These students were so afraid of not living up to expectations that they decided to do things to take the edge off of potential failure. If they did fine on the exam, then all was well. But if they ended up failing, they could blame it on the night of drinking.

Obviously, this is a terrible way to improve. You have to be willing to fail, and take responsibility for that failure, in order to develop — because failure is a valuable teacher. But no teacher in the world can help you if you're not willing to listen.

Learning from failure means spending time and effort thinking about your mistakes. Unfortunately, people would rather bury their heads in the sand than face their failures. This is a major problem, because our attitude toward failure often determines our success.

We see evidence for this in an experiment performed by a team of psychologists at Michigan State University. The experiment divided children in two groups: those who believed they were born intelligent, and those who thought they could get smarter with practice.

Each group was assigned tasks with increasing difficulty, designed so that children would eventually fail. The experiment revealed that children who believed they could improve were able to use these failures to progress in subsequent tests. The other children, the ones who believed their intelligence was fixed, simply gave up.

### 8. Final summary 

The key message in this book:

**It's hard to admit to mistakes. But if you want to meet your full potential, then you not only have to recognize that you make mistakes but embrace them as part of your path to success. Indeed, without failure, there is no progress.**

**Suggested** **further** **reading:** ** _Mistakes Were Made (But Not by Me)_** **by Carol Tavris and Elliot Aronson**

Through studies and anecdotes, these blinks explain why, when we make mistakes, we often come up with self-justifications instead of admitting the mistakes to ourselves. It also shows how detrimental these self-justifications can be to personal relationships, medicinal care, the justice system and even international relations.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Matthew Syed

Matthew Syed is a British journalist and Oxford alumnus, as well as a three-time Men's Singles Champion at the Commonwealth Table Tennis Championships. He is also the author of _Bounce_.

