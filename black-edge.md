---
id: 59d26759b238e10005435bbc
slug: black-edge-en
published_date: 2017-10-03T00:00:00.000+00:00
author: Sheelah Kolhatkar
title: Black Edge
subtitle: Inside Information, Dirty Money, and the Quest to Bring Down the Most Wanted Man on Wall Street
main_color: F2E330
text_color: 736B17
---

# Black Edge

_Inside Information, Dirty Money, and the Quest to Bring Down the Most Wanted Man on Wall Street_

**Sheelah Kolhatkar**

_Black Edge_ (2017) tells the real-life tale of greed and financial crime on Wall Street during the 2000s. It describes large-scale, illegal insider trading at SAC Capital Advisors, a hedge fund founded by star investor Steve Cohen. SAC maintained a culture of trading on inside information, but while some traders at SAC were convicted of insider trading, US authorities could never stop Steve Cohen himself from making his millions — and he was never convicted of any crime.

---
### 1. What’s in it for me? Discover how Wall Street fat cat Steve Cohen got rich through illegal insider trading. 

It's common knowledge these days that Wall Street has a lot to answer for. After the events of the 2007-2008 financial crisis, one would hope that governments would crack down on the excesses at the heart of global finance.

Sadly, that simply has not been the case, and the injustice of this state of affairs is perfectly personified by Steve Cohen, a Wall Street investor who made billions of dollars by attaining a "black edge" over fellow investors.

Cohen procured illegal, or "black," insider information about companies' performances in order to gain an advantage over fellow investors. To the outside world, he was miraculously lucky, always betting on the right companies at the right time. In reality, as the investigative journalist Sheelah Kolhatkar discovered, he was almost certainly utilizing an unfair advantage to surpass his rivals.

In these blinks, you'll learn

  * how the hedge fund Cohen founded systematically used illegal insider information to manipulate stock prices;

  * how a drug to help Alzheimer's patients made Cohen $273 million; and

  * why, after multiple criminal proceedings, Cohen has managed to remain a free man.

### 2. Steve Cohen was a talented trader and was blessed with early success, but faced charges of insider trading. 

In 2008, in the midst of the worldwide financial crisis, US federal agents were in the process of taking down Raj Rajaratnam, a Wall Street titan, when they discovered something fascinating.

Rajaratnam was illegally using inside information from companies; he traded on this information and profited from it massively. The agents were intrigued at how one name kept cropping up during interviews with Wall Street insiders: Steve Cohen.

Was there an even larger case waiting to be uncovered? Rajaratnam, it turned out, was small fry — the investigation was only just getting started.

So who is Steve Cohen? Let's start from the beginning.

Steve Cohen was born in 1956 and grew up in a middle-class family on Long Island, New York. From a young age, he was fascinated with finance.

As a student at the famous Wharton School at the University of Philadelphia, he read the _Wall Street Journal_ every morning and followed the stock market. He was talented too: he played poker with his fellow students and made a lot of money doing so.

In 1978, fresh out of Wharton as a 21-year-old, he landed a job at Gruntal & Co., a New York brokerage firm. Even then, his skills shone through: in one afternoon, he made $4,000, a huge figure in 1978.

Cohen was successful, making $5 million to $10 million a year. But it didn't take long for the first signs of wrongdoing to appear as he faced charges of insider trading.

In 1985, the Securities and Exchange Commission, or SEC, looked into Cohen's transactions. Cohen had received inside information through a friend about an imminent takeover of electronics company RCA by General Electric. Cohen invested heavily in RCA shares and made $20 million in profits when the takeover was announced.

Even though the criminal case was later dropped, it strongly indicated Cohen had a somewhat unorthodox approach to trading.

### 3. Cohen built up his own investment firm, SAC Capital, which systematically sought inside information. 

Within 14 years, Cohen moved from being a lowly junior trader to a revered Wall Street personality. It would only be a matter of time before he wished to break free of Gruntal and venture out on his own.

So it was that in 1992, Cohen founded his own investment firm, SAC Capital Advisors, using his own initials for the company name. It was a hedge fund that profited by investing money gathered from different individuals and institutions.

Cohen began with $23 million and nine employees, and met with astronomical success. In just three years, SAC had quadrupled in size to $100 million. After that, SAC doubled each year, surpassing $1 billion in assets by 1999.

Cohen's pockets were overflowing. But just how did he manage to make so much so quickly?

Cohen bet on short-term movements in stock prices. Each day, he gathered information about the market, bought large numbers of shares and then sold them when their price rose.

But it eventually became more complicated than that. In fact, SAC systematically attempted to receive and trade on inside information.

By the late 90s, SAC was finding it more difficult to profit from short-term trading, and Cohen sensed that things needed to be turned up a notch.

Until that point, his traders were unaware of the industries or companies whose shares they were trading. In response, Cohen started to hire traders with a "fundamental edge," that is, traders with deep knowledge, expertise or personal connections in a particular industry.

SAC wanted people who knew and would reveal valuable intelligence — in other words, inside information.

For instance, if a potential hire lived in the same neighborhood as an industry executive, this was a bonus. Such "coincidences" would be opportunities for making personal connections.

Every possible connection was mined in a bid to build up profits.

### 4. SAC was accused of manipulating stock prices and a culture of seeking inside information became embedded. 

By the mid-2000s, Cohen was one of the richest men in the world. He had a personal fortune of nearly $10 billion. He allowed himself a few luxuries with this cash, and often splurged on expensive artwork.

But something seemed off. SAC's huge profits seemed just too good to be true, and there simply had to be something shady going on.

It was no surprise to anyone, then, when, in 2006, SAC Capital was accused of manipulating stock prices. The first to point fingers were Biovail, a Canadian drug manufacturer, and Fairfax, a Canadian insurance company.

They accused SAC of spreading false and negative reports about their performance and business practices, which in turn caused their stock prices to drop. Traders at SAC were able to make huge profits when they bet against these companies' success.

For instance, Fairfax employees claimed to have received nighttime calls from anonymous voices that whispered that Fairfax was fraudulent. Additionally, anonymous websites appeared making comparisons between Fairfax and Enron, a firm which had itself collapsed after massive fraud revelations.

The accusations were all unfounded, but they were still noticed by the regulatory authorities, the SEC and the FBI.

Simultaneously, at SAC, the shady culture that purposefully sought inside information continued.

SAC's approach was to bet on short-term stock-price movements, especially of the type that occur after specific events, such as companies' profit announcements. SAC managers pushed their traders to talk to their contacts and leverage them to get relevant information before any official announcements.

A favored way to get valuable information was using so-called expert networks, such as Gerson Lehrman Group. These networks are designed to connect investors with company executives. Technically, in these paid-for "consultations," the executives can't share inside information.

However, they did drop valuable hints, and the information flowed back to SAC traders. Needless to say, they put this knowledge to good use.

> "When it came to innovating financial crime, hedge funds were like Silicon Valley."

### 5. In 2008, SAC used research into Alzheimer’s to benefit in massive insider trading. 

Almost 5 million people have Alzheimer's disease in the United States alone. The disease causes severe memory loss and, unfortunately, no effective treatment has been discovered.

A breakthrough in this regard could potentially result in millions of dollars in profits, so any development is sure to attract Wall Street's attention.

When two pharmaceutical companies, Elan and Wyeth, tried to develop Alzheimer's medication in the 2000s, Mathew Martoma, an SAC trader, naturally wanted to learn more. The new drug was to be called Bapineuzumab, or Bapi for short.

Martoma set about building strong relations with Dr. Sidney "Sid" Gilman, who was involved in bapi's development as chair of Elan's safety monitoring committee. He had also signed a confidentiality agreement related to all aspects of bapi's development.

Nonetheless, Martoma was very persuasive. The two spent hours on the phone and before long, Martoma had pushed Gilman into discussing the confidential bapi trials.

It was this information, which Gilman eventually provided to Martoma, that SAC used to great effect.

Thanks to the calls, Martoma was initially very confident that bapi would be successful. Therefore, Cohen and Martoma gathered more than $700 million in Elan and Wyeth stocks.

The final bapi test results were due to be released on July 28, 2008, at the International Conference on Alzheimer's Disease in Chicago. Gilman was to be keynote speaker and one of a tiny circle of people who knew the highly confidential results in advance.

Well, theoretically at least. He had also shown them to Martoma.

Martoma realized that bapi was a dud as far as investment was concerned — it was suitable only for a small number of patients.

Therefore, on July 20, Martoma called Cohen and they agreed to quietly disburden themselves of stock. They even started _shorting_ stocks, meaning that they sold stocks before the prices dropped and then repurchased the stocks at a lower price. In doing so, they held onto the stocks but also took home the difference between their initial selling price and subsequent, lower purchase price.

By the time the bapi results were officially released, Elan and Wyeth share prices had plummeted, making Martoma and Cohen a cool $276 million in the process.

### 6. By the late 2000s, the US regulatory authorities began to hone in on insider trading. 

SAC's business model was heavily and clearly based on insider trading.

Even so, in the late 2000s, insider trading was common practice at Wall Street hedge funds. Unlike closely monitored big banks, the relatively new hedge funds operated below the radar of the regulators and could get away with murder.

But that was soon to change.

In 2009, the FBI began to investigate SAC's obscure business practices in secret.

In particular, the FBI were searching for suspicious trades — and their key target was Cohen himself. But their strategy was to approach implicated junior SAC analysts, then push them to provide evidence against their managers. The idea was that they would gradually gather evidence about higher-ranking traders until they finally got to Cohen himself.

They soon found Jonathan Hollander, a former junior analyst at SAC and just what the FBI were after. He had traded stock in Albertsons, a supermarket chain, and a friend of his had leaked information to him about an impending takeover.

Hollander was the FBI's first link, and they hoped for a clear path to Cohen from there. But they found that Cohen had created a security mechanism to protect himself. He asked his analysts to rate potential trades on a scale from zero to ten, thereby ensuring that he wasn't himself trading directly with inside information.

At about the same time, the U.S. Securities and Exchange Commission (SEC) started to investigate, too.

They were looking into SAC's spectacular success when trading the Elan and Wyeth stocks just prior to the announcement of the results of the bapi trials. It was suspicious to say the least.

At that critical moment, on November 19, 2010, the _Wall Street Journal_ published an article detailing the FBI's and SEC's investigations. The traders had been forewarned and became very alarmed, and quickly set about destroying their hard drives. The investigation's cover had been blown.

### 7. In 2011, SAC trader Mathew Martoma and Dr. Sidney Gilman came to the attention of US authorities. 

Once the _Wall Street Journal_ had revealed what the FBI and SEC were up to, the authorities had no choice but to move quickly. Otherwise, they ran the risk of Wall Street traders destroying all evidence of insider trading before they could get to it.

Just a few months later, in May 2011, US authorities had their first breakthrough, and placed Mathew Martoma and Dr. Sidney Gilman on their suspect list.

The SEC had identified Gilman as the putative bapi trial results leaker months earlier, but they remained unsure who his contact at SAC was. As a result, the SEC subpoenaed Gilman's phone records, and eventually identified a mysterious phone number.

It was Martoma's, one of the portfolio managers at SAC.

The investigators also discovered that Martoma had contacted Cohen shortly before SAC sold off its Elan and Wyeth shares. On top of all that, Martoma was already on the FBI's database as being "known to the Bureau." They were closing in.

Things unraveled quickly when they started digging on Martoma. For starters, Mathew Martoma wasn't his real name — it was Ajai Thomas. They also found that, as Thomas, he'd had to leave Harvard Law School rather suddenly.

In his second year of classes, Martoma had struggled to cope with course work. In an effort to get a coveted legal clerkship, he'd actually forged the grades on his transcript. It was after this that he'd changed his name.

Not long after the FBI breakthrough, their agents confronted the pair at their respective homes, detailing their suspicions of insider trading.

But Cohen remained the FBI's ultimate target, and they needed the pair to cooperate — they needed their help in getting the real dirt on Cohen.

### 8. In late 2012, Mathew Martoma was arrested and SAC paid a record fine to settle charges of insider trading. 

When, in 2011, FBI agents pulled up in front of Mathew Martoma's house, they had strong suspicions but no actual evidence of insider trading. In those circumstances, all they could do was ask questions and chip away at his resolve.

However, a year later, that had changed. This time, when the agents arrived for the second time, they weren't there to shoot the breeze. They arrested him on the spot.

However, the FBI's initial interrogation of Dr. Sidney Gilman and Mathew Martoma didn't proceed so well.

When asked about scientific details of the bapi trials, Gilman could recall every detail. But when it came to questions about his relationship to Martoma, he stonewalled investigators and said he couldn't recall any details. Meanwhile, Martoma pled the Fifth Amendment so as not to incriminate himself; after all, he was facing a prison sentence of up to ten years.

To investigators, Martoma's move seemed odd, as he could easily have reduced his potential sentence if he implicated Cohen — but he didn't. Possible factors could have been that Cohen was covering Martoma's legal costs, or that Martoma feared retribution if he crossed Cohen.

In August 2012, Gilman finally agreed to cooperate fully. He admitted that he had given Martoma inside information about the bapi test results.

With that confession, the FBI had enough evidence to arrest Martoma. But they still couldn't press charges on Cohen and SAC.

Cohen, though, was aware of the danger. And so, in the spring of 2013, SAC agreed to fork out a record fine of over $600 million to settle the cases of insider trading.

After all, from Cohen's perspective, it made sense to settle before the situation worsened. And there was still the risk that Martoma would testify against him. All the legal issues would simply dissolve, he thought, if he wrote a check.

But it wasn't over yet. The SEC was still keen to charge Cohen himself.

> "The $616 million he had paid the SEC was nothing, Cohen seemed to be saying. He could find that in the cushions of his Maybach."

### 9. SAC agreed to pay another fine and Martoma was sentenced to prison – but Cohen walked free. 

Finally, there was another development in the case. The SEC got their hands on an email that potentially connected Cohen to another case of insider trading at SAC.

In the summer of 2008, Mike Steinberg, an SAC trader, received inside information about the computer company Dell's disappointing business figures, which weren't yet public. Steinberg used the information to bet that Dell's stock price would drop, and when it did, he netted a cool $1.4 million. It was an email that Steinberg had sent to Cohen about this information that the SEC now had in its possession.

Therefore, to settle this additional case, SAC agreed to pay a record fine of $1.2 billion in July 2013.

The SEC's ultimate aim was to use this information to convict Cohen of insider trading. After all, he had received Steinberg's email and sold Dell stocks immediately afterward.

However, Cohen's lawyers were canny. They argued Cohen only read about ten percent of his 1,000 or so daily emails, and so had probably never seen the incriminating note. Because it wasn't clear that he had acted on the email from Steinberg, convicting Cohen was difficult; it simply couldn't be proven.

At this point, since the SEC didn't want their whole case to come crashing down, they dropped this line of inquiry. This main case was settled with SAC for another $1.2 billion.

Soon after, in the fall of 2014, Mathew Martoma was sentenced to nine years in prison. He was repeatedly given the opportunity to cooperate and testify against Cohen since, in doing so, he could have reduced his sentence significantly. But he revealed nothing, and we still don't know why.

Cohen, however, was himself never convicted. To rub salt into the wound, he has since made more money than ever before. In April 2014, he rebranded the tarnished SAC Capital Advisors brand as Point72 Asset Management and simply kept going. Despite their continuing investigations, US authorities have drawn a blank.

Cohen walked away a very rich man indeed. In fact, in 2014, Cohen earned $2.5 billion.

### 10. Final summary 

The key message in this book:

**Steve Cohen was, and remains, a gifted investor. But his investment firm, SAC Capital Advisors, was built on a business model that involved illegal insider trading. Some of Cohen's traders were convicted of insider trading after years of investigation — but Cohen himself was never found guilty of any criminal charges. He continues to make billions of dollars and remains active in the financial sector.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Spider Network_** **by David Enrich**

The Wild Story of a Math Genius, a Gang of Backstabbing Bankers, and One of the Greatest Scams in Financial History.
---

### Sheelah Kolhatkar

Sheelah Kolhatkar is a staff writer at the _New Yorker_. She has also written for the _Atlantic_, the _New York Times_ and _Time_ magazine. She is a public speaker and commentator on business, economics, Wall Street, regulation and financial crime. Before becoming a journalist, Kolhatkar was an analyst at two New York hedge funds.

