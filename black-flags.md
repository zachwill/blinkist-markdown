---
id: 59bf9cd7b238e10005c1fbc7
slug: black-flags-en
published_date: 2017-09-21T00:00:00.000+00:00
author: Joby Warrick
title: Black Flags
subtitle: The Rise of ISIS
main_color: DF2B2B
text_color: 911C1C
---

# Black Flags

_The Rise of ISIS_

**Joby Warrick**

_Black Flags_ (2015) tells the origin story of the infamous terror organization known as the _Islamic State_ or _ISIS_. These blinks detail a history that begins with a fledgling insurgency in Iraq following the US invasion in 2003 and ends with one of the most powerful and frightening terrorist groups of all time.

---
### 1. What’s in it for me? Uncover the roots of ISIS, the world’s most brutal terrorist organization. 

The Middle East has been the site of lingering, complex conflicts for decades, but ISIS has presented the world with a new kind of evil that even al-Qaeda considers too extreme.

How, then, did such a group come into existence?

The ideology that would eventually give birth to ISIS is Salafi Jihadism, a fundamentalist ideology espoused by extremists professing to speak for all Muslims. Although the roots of this ideology can be traced back to the Egyptian Muslim Brotherhood movement of the 1950s, the political repression and imprisonment meted out to its followers over subsequent decades means that today's jihadis bear little resemblance to their moderate predecessors. In the 1950s, many sought change via democratic means, but the insurgents of today operate only by the sword.

These blinks will explain why this disturbing change occurred.

In these blinks, you'll learn

  * how one man's imprisonment led to the birth of the world's most-feared terrorist group;

  * why the US-led invasion of Iraq caused an increase in terrorism; and

  * how the Syrian Civil War was the final piece of the puzzle allowing for the founding of ISIS.

### 2. ISIS can be traced back to the release of Abu Musab al-Zarqawi from a Jordanian prison in 1999. 

Have you heard the name Zarqawi? This al-Qaeda leader shaped the course of recent history.

He was born in 1966 in the city of Zarqa, Jordan, earning him the name "al-Zarqawi," or "the one from Zarqa." The vicious terrorist network that he went on to lead would eventually form the basis of ISIS, after a complicated journey that begins in the mid-1990s. Here's the story:

Earlier in that decade, Zarqawi went to Afghanistan to join the "holy war" against the pro-Moscow government installed by the Soviet Union. Following his return to Jordan, he was arrested on March 29, 1994 and locked up in al-Jafr prison with 12 other men, all of whom had been found in possession of illegal weapons that they intended to use in a terrorist attack on an Israeli outpost. They had planned to avenge the killing of 20 Muslims by a Jewish extremist on February 25 that year.

However, prison did little to weaken Zarqawi's resolve or that of his fellow Islamist inmates. Rather than spending their time in jail with the common criminals, the group pored over the Koran, hardening their hatred for those they saw as enemies of Allah — namely the United States and Israel.

Then, in 1999, King Abdullah II of Jordan released Zarqawi — at that time the most dangerous inmate in al-Jafr — along with many of his associates. King Hussein had passed away and his son, Abdullah, was hellbent on repairing the damaged relations between the kingdom and various political factions.

It was high time to pacify the Islamists, and he offered a gift: the release of 16 Muslim Brotherhood members from prison. Little did the king know that, while in prison, Zarqawi had become a father figure to those around him. Upon his release, this leadership paid dividends as Zarqawi found himself surrounded by a group of loyal followers who would unquestioningly obey his every word.

### 3. Following his release from prison, Zarqawi formed a series of terrorist training camps. 

Just six months after his release, Abu Musab al-Zarqawi walked into the departures terminal of Amman's Queen Alia International Airport, where he was immediately confronted by Jordanian intelligence officers.

When Zarqawi told them he was simply on his way to raise honeybees in the mountains of Pakistan, the lie was obvious, but the officers had nothing on him and had to let him go. In actuality, he was retreating to Afghanistan, where al-Qaeda would put him in charge of a terrorist training camp.

Upon arrival, Zarqawi went to meet Osama bin Laden, the founder of al-Qaeda. While bin Laden initially refused to see Zarqawi, doubting his trustworthiness, he eventually recognized that the newcomer could help extend the reach of al-Qaeda.

He let Zarqawi run a training camp for Islamist volunteers from Jordan; al-Qaeda would cover the initial expenses and oversee Zarqawi's success from afar.

However, that all changed when US troops invaded Afghanistan in 2001. Zarqawi's men were suddenly in dire need of a safe haven, and the northeastern mountains of Iraq were the perfect choice. The training camp rapidly moved there.

Zarqawi went to work building a terrorist network in this new country. Soon after, in 2002, the then little-known Zarqawi came up on the radar of senior Bush administration officials after being implicated in the assassination of the American diplomat Laurence Foley in Amman, Jordan.

In fact, despite his lack of name recognition in the West, Zarqawi had been hard at work in the Middle East. By uniting with the Iraqi terrorist group, Ansar al-Islam, he had built a miniature Islamic theocracy.

At the time, the United States knew exactly where he was, and CIA spies recommended bombing his terror camp. But the Bush administration, already in the process of preparing for war with Iraq, feared that an early strike could set things off before they were ready. This hesitation cost them a golden opportunity.

### 4. As Iraq descended into chaos, Zarqawi capitalized on the opportunity. 

In 2003, with the US invasion of Iraq well underway, there was still no proof of the threat that had launched the war. After months of searching, no weapons of mass destruction had been found, and America couldn't show any connections between Saddam Hussein and al-Qaeda terrorists.

Despite this, President Bush declared victory in May 2003, just two months after the war began. However, while the United States appeared confident in its success, America couldn't even maintain security in Iraq itself.

Right after the invasion, the country was hammered by a series of bombings. The first, a car bombing on August 7, 2003, targeted the Jordanian embassy in Baghdad, killing 17. Twelve days later, the UN building in Baghdad experienced another blast. This bombing was the deadliest attack on a UN facility in history, leaving 22 dead, including Sergio Vieira de Mello, head of the UN mission in Iraq. All the evidence pointed to Zarqawi's Ansar al-Islam terrorist network.

But how did the group wreak such havoc?

Well, by this point, Zarqawi had transformed his small cadre into a powerful and developed terrorist network. Ironically enough, while the United States didn't have hard evidence to support its initial invasion, it was obvious that post-war Iraq was now harboring terrorists. In late 2003, the Bush administration finally admitted that Iraq currently contained many more al-Qaeda-style terror groups than it had before the invasion.

The country was on a fast track to lawlessness and anarchy, a context in which Zarqawi found the freedom to do his work and a litany of powerful allies to support him. In fact, his group was well received by thousands of war-embittered Iraqis and even garnered support from across the Muslim world. It became so appealing that former captains and sergeants from Saddam Hussein's army joined the terrorist network.

Zarqawi had built a massive terror network in under a year. He was now capable of executing large-scale attacks one after another.

### 5. Zarqawi allied with al-Qaeda in Iraq despite differences of opinion. 

In January 2004, Zarqawi sat down to write a letter to Osama bin Laden, asking for a favor. Zarqawi's organization had been expanding and had already planned major attacks in Iraq, but if it could secure the resources and formal support of al-Qaeda, it could do much more damage.

Zarqawi hoped that bin Laden would jump at the opportunity, but instead, the leader was again reluctant. Zarqawi's brutal slaying of Arabs and innocent bystanders went too far for the al-Qaeda founder.

Nonetheless, Zarqawi steamed ahead, executing barbaric terrorist attacks across Iraq. In 2004, he was responsible for horrifically murdering the American radio technician Nicholas Evan Berg.

Berg was in Iraq to launch a radio tower repair business. But before he got very far, he was kidnapped by Zarqawi's men. Then, on May 8, a military patrol noticed an object hanging from a freeway overpass. As the patrol approached, it became clear that they were looking at a human torso on the end of a rope. It was Berg, hanging above his severed head, which lay neatly on a blanket below him.

Two days after this discovery, a video was released, depicting Zarqawi himself decapitating the prisoner. It was made clear that Berg was killed because he was American. The video became well known, with Zarqawi soon revered as the "sheik of the slaughterers."

And this wasn't an isolated incident; Zarqawi's group carried out and recorded dozens of executions in addition to suicide bombings that killed foreigners and Arabs alike.

Now, al-Qaeda teamed up with Zarqawi to launch _al-Qaeda in Iraq_ or _AQI_. Why did bin Laden change his mind?

Well, the truth is, bin Laden never liked Zarqawi. However, with the attacks of 9/11 already three years in the past, Zarqawi was offering bin Laden a desperately needed "win." So, in an audio recording broadcast on Arab media channels, bin Laden offered his official approval for a partnership with Zarqawi, thereby forming the new organization.

### 6. Zarqawi’s barbarism crossed a line, leading to his assassination. 

On January 30, 2005, Iraq elected a National Assembly for the first time in history. Zarqawi naturally denounced the election as collaboration with America and decried all the participants as sinners.

He went out of his way to sabotage the polls. Zarqawi's men carried out a number of attacks, specifically on Sunni political candidates and polling stations in Sunni districts, killing at least 44 people. In the end, Zarqawi got what he wanted; the Sunnis were too terrified to vote, and the election was invalidated.

However, his penchant for barbaric crimes against westerners and Muslims alike also meant that Zarqawi's enemies were growing in number. Ordinary Muslims were disgusted by the group's brutal approach, and Iraqi citizens made near-daily reports to law enforcement about the activities of the terrorist network.

So, by the fall of 2005, US special forces led by General McChrystal were chipping away at Zarqawi's command structure. In the process, hundreds of terrorist lieutenants were killed or captured.

But Zarqawi's biggest blunder was probably his attack on three Jordanian hotels frequented by foreign diplomats. These suicide bombings took place on 9 November 2005, killing 60 people, including 38 Arab wedding guests. The brutality of this slaughter solidified Jordan's resolve, and the country became determined to fight the terrorists alongside US forces.

In part because of this mounting pressure, Zarqawi was finally located. The intel that sealed the deal came down from a high-ranking Zarqawi insider who had been detained by Jordanian intelligence forces. In interrogating him, Jordan discovered that Zarqawi had a religious advisor, Sheikh Abd al-Rahman, an Iraqi imam who was living in Baghdad. The pair would meet about once a week, so all the US forces had to do was locate the imam and follow his movements.

This careful reconnaissance eventually paid off on June 7, 2006 when American fighter jets bombed Zarqawi's safe house. The terrorist leader was severely injured and died shortly after US ground troops arrived on the scene.

### 7. As Syria imploded in 2011, Zarqawi’s weakened terrorist organization found a new home. 

Initially, it seemed like Syria would be immune to the agitation of the Arab Spring revolutions that swept across North Africa and the Middle East in 2010. But in 2011, that fantasy vaporized as calamity erupted in Syria.

The country's president, Bashar al-Assad, refused to heed calls for reform and instead opted for violent retaliation, shooting peaceful protesters. Assad showed no intention of stepping down and instead allowed a full blown civil war to break out; chaos was inevitable and, one by one, the security institutions of the Syrian state began to fall.

Naturally, Zarqawi's followers saw a chance to pounce on another weakened country. It was a desperately needed opportunity for them. Five years after their leader's death, the once prominent terror organization — now renamed the _Islamic State of Iraq_ or _ISI_ — was becoming irrelevant.

US commandos had destroyed the organization, killing and capturing innumerable terrorists. As a result, ISI was resource-poor, had few fighters and no place to hide. In Syria, they found solutions to all of these problems.

Syria looked a lot like Iraq had nine years earlier; it was a lawless land with few barriers to the free movement of weapons and fighters. Not just that, but the terrorist group now had a powerful new leader, Abu Bakr al-Baghdadi, a man who painted a stark contrast to Zarqawi.

Baghdadi, unlike his predecessor, was no violent troublemaker. He was a highly educated Islamic scholar and college professor, which enabled him to interpret Sharia law to give religious cover for violent acts of terror.

He came from Iraq's al-Badri tribe, which laid claim to a direct ancestral line to the Prophet Muhammad. These ample qualifications made Baghdadi perfect for the role of _caliph_, an essential symbolic asset for an organization that sought to found an Islamic state.

### 8. The jihadist organization grew into a fully fledged army and, in 2013, Islamic State became official. 

In 2012, as the Syrian civil war continued to escalate, thousands of religious Muslims from the Sunni Arab countries of the Persian Gulf and North Africa put their bodies on the line for the jihadists who were resisting the Syrian tyrant, Assad. Huge numbers of sympathetic Arabs donated money as well as supplies and, in secrecy, Arab governments donated weapons to the cause.

Meanwhile, ISI was growing in size. Empowered by the steady stream of money and weapons, not just Arab Muslims, but radicalized Muslims from across the world entered Syria from Turkey to join ISI and their Syrian offshoot. Thousands of young Muslim men were even making the journey from Western Europe.

As a result, by 2013, the best-armed and best-trained fighters in the conflict belonged to Baghdadi. His organization would soon be capable of defeating four Iraqi army divisions and claiming a third of the country's territory.

As the group's power grew steadily, the formation of ISIS became official. On April 9, 2013, Baghdadi named this new organization the _Islamic State of Iraq and al-Sham_ or _ISIS_, the final word referring to the "Levant," or eastern lands of the Mediterranean from Turkey to Syria, Lebanon, Jordan and Israel.

The new group resembled the government of a nation state with departments for social media, logistics, finance, training, recruitment and much more. It wasn't long before the city of Raqqa, Syria was declared ISIS's new capital, and on July 4, 2014, the group declared its intention to establish a worldwide caliphate under a new name, the _Islamic State_.

Baghdadi had dramatically expanded his ambitions to rule over limited Middle Eastern territories and, by late 2014, ISIS was in control of massive territories in both Iraq and Syria. They were perfectly poised to continue their fight for expansion.

### 9. Final summary 

The key message in this book:

**The prominent terrorist organization ISIS was forged after a decades-long story of unrest and insurgency in the Middle East. The formation of this infamous terror group owes much to a single man, Abu Musab al-Zarqawi.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _ISIS_** **by Michael Weiss and Hassan Hassan**

_ISIS: Inside the Army of Terror_ (2015) charts the rapid rise of the Islamic State in the Middle East, from its early beginnings to its self-proclaimed caliphate in Iraq and Syria. Grippingly told, the story of ISIS's domination over al-Qaeda in Iraq and its slow but ruthless push in Syria also shines light on the failings of the West in dealing with this fanatical yet disciplined jihadi group.
---

### Joby Warrick

Joby Warrick is a long-time reporter for the _Washington Post_ and winner of the Pulitzer Prize for journalism. His other titles include _The Triple Agent_.

