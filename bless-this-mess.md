---
id: 5e3f7a0a6cee070006b5f8e7
slug: bless-this-mess-en
published_date: 2020-02-10T00:00:00.000+00:00
author: Rev. Molly Baskette and Ellen O’Donnell, PhD
title: Bless This Mess
subtitle: A Modern Guide to Faith and Parenting in a Chaotic World
main_color: None
text_color: None
---

# Bless This Mess

_A Modern Guide to Faith and Parenting in a Chaotic World_

**Rev. Molly Baskette and Ellen O’Donnell, PhD**

_Bless This Mess_ (2019) is a parents' guide to raising Christian children in a bewildering age. Combining spiritual wisdom with the latest thought in child psychology, the authors aim to show that there's no reason to think of the terms "progressive" and "Christian" as contradictory when it comes to child-rearing.

---
### 1. What’s in it for me? Learn how to raise progressive Christian children in changing times. 

Molly Baskette and Ellen O'Donnell are mothers who have read a lot of books on parenting without discovering much that addressed their own particular situations — that is, parents trying to raise Christian children who are also courageously progressive. Many of the books they encountered gave short shrift to sensitive topics like drugs, alcohol, and sex; those that did touch on these issues often did so in an off-putting, authoritarian manner. 

So they resolved to pool their knowledge and write their own parenting book together _._ Baskette brought her years of experience working as a minister in some of the most progressive churches in the US, and O'Donnell provided the clinical expertise she developed working as a child psychologist over the years. 

These blinks draw on their combined wisdom to provide an overview of parenting fundamentals that embody both good Christian and progressive values. They'll also give you advice on how to navigate the nuances of sensitive topics that other guides either ignore or turn into black-and-white morality tales. 

In these blinks, you'll find out

  * how to raise children capable of moral reasoning;

  * why Christians should fight for social justice; and

  * why you should never have "the Talk" with your children.

### 2. Instead of trying to remodel your children, adapt your parenting style to better suit their personalities. 

For too long, Christian parenting has relied on one all-too-familiar proverb: "Spare the rod and spoil the child." Fortunately, most of us disagree with that piece of "advice" these days — and the Bible has more to say about child-rearing than just that.

We can find an alternative attitude worth imitating in another Biblical proverb: "Train children the right way, and when grown, they will not stray." What makes this statement especially interesting is that the language of the original Hebrew version seems to recommend raising our kids "in accordance with their own character." 

This nuance is significant. Raising children with, rather than against, the grain of their characters involves discerning each child's uniqueness — and then changing our own style of parenting accordingly. In other words, we need to tailor our parenting methods to fit our children, rather than trying to force them into complying with a one-size-fits-all method.

Taking this advice to heart entails considering a number of important questions: Who are these children God has given us? What are their individual strengths? What are their inclinations? And what can we, as parents, do to work with their God-given temperaments?

One of the problems we can run into when addressing these questions is _ego-involvement_. This term describes parents' tendency to over-identify with the successes and failures of their children. And while it's only natural that we root for our kids — wanting them to do well and meet their goals — we must be careful not to cross the line by replacing their dreams with our own. In short, we must avoid being too ego-involved.

In practice, this might mean learning the difference between wanting your daughter to receive a good education for her own benefit, and wanting her to go to Harvard to satisfy your ambitions. Alternatively, it might mean learning that your son will never be the academic type, and that working with his hands is what will make him happiest in life. 

Relinquishing control over your children's aims for the future can be difficult, but soul-searching of this kind is the first step in starting to change our methods of child-rearing. By separating our kids' true temperaments and inclinations from our own imaginary ideals, we can begin to formulate new parenting strategies from the ground up.

### 3. Create a home environment that fosters your child’s sense of independence and self-motivation. 

While reformulating our parenting methods might seem like a daunting task, there are a lot of places we can look to for guidance. One of them is a psychological school of thought known as s _elf-determination theory_. 

According to this theory, everybody is born with an inner reserve of motivation — a fact that in-the-know parents can harness for the good of their children. 

How? Well, the answer has to do with the idea of _autonomy,_ which is central to this school of thought. If you want your children to show drive, the theory goes, then you need to empower them to make their own decisions. That enables them to become _autonomous_, which means they have the freedom and ability to choose their own course of action. 

In theory, this might sound abstract, but in practice, it's simple. You support autonomy when you give your children choices. By allowing them to make decisions for themselves, you teach them to rely on their inner drive and to trust in their own independence. 

So the next time your son insists on wearing shorts in the middle of winter, you might want to try explaining the factors that make pants a more suitable choice.

But what if he continues to insist on wearing the shorts anyway? Well, that brings us to the next point to remember: Supporting our children's autonomy doesn't mean letting them do whatever they want. To safeguard their autonomy, we also need to provide them with _structure_. 

In this context, structure refers to limits and consequences. Providing our children with structure means communicating our values to them, explaining to them that their actions have repercussions, and justifying our thinking when we feel compelled to insist on certain behaviors. 

Returning to the previous example, if your son refuses to change out of shorts on a cold day, you may have to resort to issuing an ultimatum. But you should make clear that this decision is based on concern for his welfare.

This point leads into the final idea we can borrow from self-determination theory: _involvement_. This means helping your children feel valued, warm, and secure — assuring them that they're both connected to and loved by the people around them.

If your son refuses to wear pants to school, show involvement by taking the time to ask him why. Maybe his pants have drawn unwanted attention from other kids, or maybe he just finds it easier to run around in his shorts. What matters, in this instance, is showing that you care.

### 4. Replace simplistic moral assertions with the concept of moral ambiguity and the cultivation of empathy. 

Jesus took nuanced moral stances in a time of clear-cut absolutes and stood up on behalf of social outcasts. To enable our children to do the same, the first step we can take is to introduce them to the concept of _moral ambiguity_. This is a form of ethical thinking that deals in moral gray areas, rather than in black-and-white notions of good and evil.

The problem is that children are familiar with the notion of good guys and bad guys from an early age — it's a trope they encounter in nearly every form of storytelling. This makes it even more important that we help them to realize that goodies and baddies are exactly that: just a story, a fable.

So if your child asks you which TV character is the good guy and which is the bad, congratulations! You've been given a teachable moment. Take the opportunity to explain to your child that we all make good and bad decisions, that none of us are good or bad all of the time, and that all of us are equally loved by God.

Another way to develop your children's moral reasoning is to ask them to imagine why "bad" people — like criminals and bullies — might behave the way they do. What kind of influences or experiences might have shaped them?

This brings us to another, equally important aspect of our children's moral development: the cultivation of empathy. Remember that Jesus commanded us to love everyone, even our enemies. To follow this commandment, we need to be able to soften our hearts to those who have wronged us. This requires understanding the emotions, circumstances, and motivations behind their behavior. 

So how can we help our children to develop their sense of empathy? Well, we can start by becoming more empathetic ourselves. One way to do that is to be kinder and more forgiving regarding the judgments we make about strangers. 

This is a habit we can practice anytime, anywhere — and we can practice it with our children, allowing everyone to grow together as an empathetic family. For example, you can use your imagination to empathize with the person who just cut you off in traffic. Invite your children to help you think of a good reason for the driver's behavior. Who knows, maybe they're rushing to a hospital emergency room!

### 5. Teach your children that their actions should be consistent with your family values, even when there’s no reward. 

Our children are ours, but they're also their own. In other words, they aren't carbon copies of us, and they can't — and won't — follow our advice for every situation.

So how do we ensure that they adopt the values we cherish? Well, the first step is simple: define those values, and make them known.

To do this, start by taking some time to write out your family values. These might include things like keeping promises, being truthful, and standing up for others. Once you've arrived at a comprehensive set, enlist the creative powers of your children to write, design, and ornament a _family mission statement_ summarizing your family values. To keep it fresh in everyone's mind, display the statement in a prominent location in your home. 

The next step is to link these family values to your child's process of decision-making. Make a habit of this, and in time your children will learn to look to your family values for moral guidance without instruction. 

One way of doing this is to draw out the consequences of your child's options in any situation, comparing them to a specific family value. For example, you might say to your daughter, "If you don't visit your grandmother, she'll be very hurt — and you know we care about each others' feelings in this family."

This approach has the benefit of fostering your child's sense of autonomy and competence, by allowing her to make her own decisions and demonstrating trust in her ability to weigh conflicting options. By making the consequences of her actions clear, you're also employing one of the three fundamental aspects of parenting: structure.

However, when thinking about consequences, it's important to distinguish between providing structure and providing rewards and punishments. As parents, we often rely on promised rewards and threatened punishments to influence our children's behavior — but studies have shown that they're rarely effective over the long term. 

One reason for this is that although the threat of, say, missing out on dessert might make your son clean his room this week, it's not going to make him internalize the notion that he should generally clean up after himself. That means that once the threat of punishment is gone, so too is the desired behavior — and with it, your son's clean bedroom.

Rewards are the flipside of this strategy, which have been shown to be even more unhelpful over time. Giving your daughter pocket money in exchange for reading, for example, can end up making her feel that reading books is only a chore to be endured in exchange for some reward! In an instance such as this, outlining the benefits and values associated with reading is the wiser option.

> _"Natural rewards are usually enough."_

### 6. When it comes to money, we should teach our children to be both generous and sensible. 

In the Sermon on the Mount, Jesus explicitly warned us that money and material possessions are unimportant and can even hinder our spiritual growth. In fact, he advised his disciples to give away everything they owned.

In today's world, how can we can begin to live a life more aligned with this teaching? 

Don't worry, you don't have to sell your home and donate all the money to charity! There are less extreme steps your family can take to become more generous and less materialistic. One of them is to adopt the Biblical practice of _tithing_. This means donating 10 percent of your income to other people, charities, or your church.

Of course, giving is good for its own sake, but it's also beneficial to your family's well-being. Research in social science indicates that generous people are happier, healthier, and more grateful on average. This phenomenon is known as the _paradox of generosity_.

So both the personal benefits and the moral obligations of generosity are clear. But giving away 10 percent of your income is easier said than done if your budget is tight. To become more generous, you'll have to think carefully about how you're using your money, so you can have enough to give away. And here's where you can help your children to start thinking about it, too. 

It's helpful to begin by distinguishing between _wants_ and _needs_. Once we've identified unnecessary expenses, we can cut them out, freeing up more money to give to others. Unsurprisingly, children often find it fun to identify the wants their parents can give up. Indulge their mischievousness this once, and they'll remember the principle for good!

Although your children will benefit from just discussing money, it's even better to start allowing them to manage some finances of their own. This will help to prepare them for financial independence in the future. So encourage them to get jobs when they're teenagers, and give them an allowance when they're younger. 

Whatever the amount is, consider making it a figure divisible by three. That way, they can save a third, spend a third, and give a third away in the form of donations and gifts.

Of course, there are other things we can give besides our money. In the next blink, we'll look at how we can encourage our children to give their time and energy to improve the lives of others.

### 7. We can follow Jesus’s example by teaching our children to care about others and urging them to pursue social justice. 

Although we might be inclined to think of social justice as an entirely modern concern, the pursuit of kindness, fairness, and equality has a much longer history. In fact, Jesus repeatedly calls on us in the Bible to try to fix society's ills.

To heed this call, our children must develop a sense of concern for the people around us — especially those in need. Unfortunately, many parents assume that their children will simply adopt a caring attitude as they mature. But if you don't show your children that it's important to be kind, they're probably not going to magically start thinking that it is. 

Research bears this out. When parents are asked what they want most for their children, the majority say they want them to be kind — but when children are asked what their parents most want for them, the majority think it's merely to be happy.

So how do we go about fixing this disconnect? It's simple: Instead of just asking our kids how they feel, we can make a habit of asking them how they think they're making others feel. That means asking your child to think about how excluding a classmate from a party might upset her, as well as inviting your child to think about how a kind action might cheer someone up.

You can put your values into practice on a larger scale by getting your family involved in social justice campaigns and community initiatives. Talk to your children about your culture's legacy of oppression, so that they can counteract the present-day effects of such injustice. If you live in the US, for example, consider discussing the Civil Rights Movement, as well as the persistence of racism today.

But encouraging mere tolerance for others is never enough. Explain to your children that everyone deserves to be loved and respected, no matter how different they are. In general, the best way to teach kids to value diversity is to expose them to a diverse environment at a young age. If they live in a monoculture, how can they begin to understand diverse ways of life, much less celebrate them?

This is something a good church can help you with. If you have a number of churches in your area, make sure you take the diversity of the congregations into account when deciding which one to attend.

> _"Jesus made a point of reaching out to people who were different, invisible, or meant to be shunned."_

### 8. Nuance is paramount when discussing sensitive topics like sex, drugs, and alcohol. 

For too long, parents have delivered advice on drugs, alcohol, and especially sex in the form of the dreaded "Talk" — a one-off conversation in which everything kids need to know about these sensitive topics is supposedly revealed. More often than not, this Talk is stilted, ineffective, and rife with unhelpful moral absolutes. So what might a progressive, Christian alternative to the Talk look like instead?

For a start, the Talk would stop existing in the singular. Replacing it with a series of casual but meaningful discussions is a far more effective strategy if we want to help our children make safe, informed decisions.

Conversations about sex and the body, for example, should begin at an early age by teaching children the proper names for their body parts. One of the best ways to allow talks about sex to arise is simply to take seriously the questions your children ask you, whether they're posed in a spirit of innocence or of mischief. 

As children grow older and you begin to discuss sex in more detail, avoid talking in terms of inflexible absolutes if at all possible. This is unhelpful and can even backfire. For example, kids who are told that abstinence is their only option are not more likely to practice abstinence — and are actually _more_ likely to fall pregnant and contract STDs.

Instead of delivering decrees, make use of the fundamentals of parenting: autonomy, support, structure, and involvement. Take the time to talk to your kids, and empower them to make safe and informed decisions, bearing in mind the potential consequences of their actions. For example, you could help them weigh the pros and cons of various forms of contraception without suggesting they need to be having sex.

When it comes to alcohol and drugs, a nuanced approach is also vital. 

For example, you might want to consider serving your children some wine (diluted, if you like) along with their food on special occasions. Taking an approach like this toward alcohol teaches them that drinking and drunkenness are two very different things, with profoundly different implications for our well-being.

Explain to your children that just as there's a difference between one beer and six, there's also a significant difference between marijuana and cocaine. If we simply tell our children that all drugs are equally bad, we're omitting important information that could help them to avoid the most sinister substances down the line.

### 9. Small changes involving ritual, prayer, and the Bible can help bring God into the life of your family. 

For Christians, Sunday is meant to be a day of rest — but, sadly, this aspiration is becoming increasingly unrealistic in our fast-paced modern world.

With all the demands life makes on our time, attention, and energy these days, how can we hope to bring religion into our daily lives?

Well, if we want an example of how a holy day of rest might be incorporated into our lives, we can take inspiration from the Jewish celebration of the sabbath, which is known as Shabbat. 

On Friday night, observant Jews stop working, light a candle, bless their food and wine, and sit down together to enjoy a meal. For the next 24 hours, they refrain from any type of work. This might feel like a luxury, but if you do observe the Sabbath, be careful not to regard it as a personal indulgence. Sabbath is a gift God has given us, and observing it is one of the Ten Commandments.

Another way you can bring religion into your family's everyday life is by encouraging your children to talk to you about God. Don't worry if your theological knowledge is limited — try to answer from your own experience as much as you can.

To help with specific religious questions you find hard to answer, consider buying a children's Bible for your family. You could even read it yourself, if your knowledge of Biblical stories has grown rusty over the years.

Another helpful ritual is praying together as a family. Doing this is beneficial for a number of reasons. For one, children who pray with their family learn that their parents are human too, with their own worries, hopes, and memories. By hearing their parents voice their concerns and aspirations, children learn that adults aren't invincible — they're people who require love and understanding like anyone else.

Prayer can also help us to cultivate both empathy and gratitude. Empathy can be developed by praying for those suffering misfortune in faraway places, and by trying to understand what the victims of various disasters must be going through. A grateful attitude can be cultivated by remembering to thank God for the good things in your life, including the food on your table. 

Overall, the most significant thing you can do is the most obvious: go to church with your family. Christianity is fundamentally communal, and a tightly-knit church can give you the neighbors God wants you to love.

> Families that eat together regularly have children who tend to be less prone to anxiety.

### 10. We can overcome worry by trusting in prayer and encouraging our children to confront their fears. 

As parents, if we let ourselves worry about everything that troubles us, we might be in a constant state of panic. We want the best for our children, but we know there's so much in the world that could harm them.

Luckily, prayer can sometimes act as an antidote to anxiety. One reason for this is that praying involves recognizing that some things are beyond our control — that no matter how much we worry, there will always be things we can't change. When we pray to God about something that's troubling us, then, we're acknowledging the limits of our own power as well as the futility of worry. After all, if only God can fix whatever's worrying us, what use is our own troubled brooding?

Of course, anxiety isn't just an adult problem. Children can suffer from it, too. What should you do if that's the case? 

The first thing we should bear in mind is somewhat counterintuitive: psychological research tells us that helping your child to avoid anxiety-inducing situations is very often a bad idea.

That's because anxiety thrives on avoidance. Children who never learn to confront their fears never learn that their fears can be overcome. Instead of facing their fears and learning that many anxieties are irrational, kids can come to believe in the scary stories their minds construct more and more.

For example, if you have a daughter who's very shy, part of you may want to allow her to stay in her room when family friends come over — but that might just worsen the problem. Instead of realizing that nobody is going to stare or laugh at her if she comes down to say "hello," she'll be left with the conviction that staying in her room was the right thing to do in order to stay safe. 

In situations such as this, the best strategy is to be kind and understanding, but also firm and instructive. You could try telling your shy daughter that you understand her worries, but that they're very improbable. Encourage her to come down, for at least a few minutes, and see how it goes.

### 11. Final summary 

The key message in these blinks:

**There's no reason to think that Christian parenting and progressive parenting can't go hand-in-hand — on the contrary! We can make use of the Bible to help our children tackle social injustice, learn how to give generously, and live in a way that is safe, independent, and spiritually rewarding.**

Actionable advice:

**If your child does something kind, point it out, and tie their actions to their personality.**

This makes them more likely to identify with the trait you ascribe to them, and to repeat actions of the same sort in the future. For example, if your daughter shares her chocolate with a younger brother, you could say, "Well done, that was very kind of you, and you made your brother very happy. You're a very considerate girl."

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _How to Raise Successful People_** **, by Esther Wojcicki**

You've just learned how to raise grounded and kind Christian children in the twenty-first century. Esther Wojcicki, an educator and award-winning journalist known as the "Godmother of Silicon Valley," tackles a similar issue from a secular point of view in _How to Raise Successful People_.

What are the values we should be passing on to our kids? How can we teach them to be independent? And how do we strike the right balance between raising kind children and raising successful children? Wojcicki brings her years of experience as an educator to explore these and other equally important questions. To discover her answers, check out our blinks to _How to Raise Successful People_, by Esther Wojcicki.
---

### Rev. Molly Baskette and Ellen O’Donnell, PhD

Rev. Molly Baskette is the senior minister at First Congregational Church of Berkeley, California, and has written books on a wide range of topics — including public confession, coping with grief, and the revitalization of churches. Her recent works include _Standing Naked Before God_ and _Real Good Church_. Ellen O'Donnell has a PhD in clinical psychology and is a child psychologist at Massachusetts General Hospital for Children and Shriners Hospital for Children in Boston. She also works as an instructor at Harvard Medical School and has authored a number of scholarly articles in academic journals, such as the _Journal of Youth and Adolescence_.

