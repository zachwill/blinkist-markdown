---
id: 55477b5736663200078a0000
slug: blinkracy-en
published_date: 2015-05-07T00:00:00.000+00:00
author: Ben Hughes and Sebastian Klein
title: Blinkracy
subtitle: None
main_color: 6AB852
text_color: 3E6B30
---

# Blinkracy

_None_

**Ben Hughes and Sebastian Klein**

_Blinkracy_ (2015) is all about an innovative organizational approach based on empowering employees and eliminating the need for managers. With insights from the Berlin-based startup Blinkist, which restructured its own workplace using this model, these blinks describe how you can implement it at your own firm.

---
### 1. What’s in it for me? Get rid of managers and pointless meetings at your company. 

Do you work in a coal mine?

Probably not. More likely, you work in an office somewhere, surrounded by cubicles and laptops.

But then, why are most companies still run as if they were coal mines, with rigid hierarchies that offer very little leeway for employees to decide what they do and how they do it?

Because most companies are stuck in the past.

Enter the future of organizational management: Blinkracy. This is the experimental organizational structure we implemented here at Blinkist, precisely because, after thinking about the way most companies are run, we realized there was a better way. And although Blinkist has since moved past Blinkracy, it can still teach some valuable lessons.

In these blinks, you'll learn

  * why Eastman Kodak went bankrupt;

  * why a company doesn't necessarily need any managers; and

  * how you can get rid of pointless meetings in your company.

### 2. Classic command and control organizations are antiquated, dysfunctional and ineffective. 

Like most companies in the world, your workplace is probably organized according to a rigid hierarchy: Employees do whatever their bosses tell them. In turn, these bosses have their own bosses. So in effect, mandates from the highest tiers of management trickle down to each employee.

This top-down hierarchy is called _command and control_ (C&C), and it's based on the antiquated idea that companies work best when a hoard of uneducated minions carries out the orders of one genius leader (like Rockefeller or Vanderbilt). Companies have been organized in the same non-motivating way ever since the coal mines and factories of the Industrial Revolution.

But today, these old business structures don't suit the fast-changing business landscape. Why fast-changing? Well, a Yale University study showed that the average American company's lifespan has decreased from 67 years to a trifling 15.

That means businesses have to be capable of adapting — and quickly!

Eastman Kodak learned this lesson the hard way: The iconic photography company, founded in the late 19th century, the heyday of C&C, didn't respond fast enough to the rise of digital cameras in the 1990s. And as a result, they filed for bankruptcy in 2012.

As you can see, rigidity is a major disadvantage of C&C. But it's not the only one! Poor talent management and poisonous office politics also undermine these kinds of hierarchical workplaces.

Why?

Well, in C&C systems, a few managers have the authority to hire and fire at will. So people often get promoted not because of merit but because they play golf with the boss.

To be successful, however, organizations need to have the most capable and skillful people filling each role. And that's why having a culture of promotions based on connections — not skills — damages a company's prospects.

Although C&C is a dysfunctional system, it's still the dominant way of organizing the workplace. So is it any wonder that 71 percent of American employees dislike their jobs?

There has to be a better way of organizing companies without descending into anarchy.

> _"Command and control systems are unpleasant for all, except the cigar-smoking, top-hat wearing, monocle-wiping owner at the top."_

### 3. Forget about titles – instead, structure your organization around roles. 

We don't expect the Director of Marketing to get involved in accounting. And we'd be surprised to learn that an HR associate was responsible for the product design of a new launch. Under C&C, if something's not in our title, we simply don't do it.

But most people are capable of more than their job titles suggest. And that's why it's beneficial to structure a company around _roles_, not titles.

To set up a role-based organization, start by listing every single task that needs to be completed in order for the business to succeed. Then, logically cluster tasks together to form a role.

Centering roles round tasks illuminates each employee's responsibilities. For instance, "cold calling customers" and "maintaining customer database" might be combined together into a role called "B2B Sales."

And tasks such as "maintaining coffee supplies" and "ensuring adequate printer paper" would be assigned to the role called "Office Administration." As you can see, this system guarantees that each task has an owner, since even mundane chores are delegated.

The main difference between a role-centered structure and traditional, title-based hierarchies has to do with the fact that one person can have several roles, as long as they have the skills, the time and the resources.

Furthermore, combining roles promotes efficiency, creates opportunities for growth and promotes open communication.

In terms of efficiency, let's say you're skilled in the arenas of both customer service and product design. In most organizations, these are two completely different departments and positions within them are filled by different people. But when you structure your company around roles, one person can be responsible for both tasks, if they have the time.

Role-based organizations also create opportunities for employees to grow more organically. Because instead of waiting for formal promotions, people can incorporate new tasks into their role when necessary.

And since roles aren't connected to one specific person, it's easier to discuss problems without anyone feeling defensive. For example, saying something like, "the B2B sales role isn't being fulfilled," is far less personal than, "the sales manager isn't doing her job."

> _"The process of defining roles makes the expectations and responsibilities crystal clear."_

### 4. Replace the traditional department structure with circles – a more fluid and flexible way of organizing your company. 

In our personal lives, most of us are part of many different circles: We have family, classmates, colleagues, yoga buddies and so on. These circles overlap; their compositions change constantly; and sometimes they disintegrate.

Now imagine if you had that kind of flexibility at work!

Well, you can! Within a firm, _circles_ encompass several roles working together toward a broader purpose that aligns with the company's overall goals.

Let's say the circle Marketing and Sales has the explicit goal of increasing sales by 20 percent. And the roles Customer Service, B2B Sales and Telesales belong to that circle and work together to achieve its goal.

Well, if the company changes its overall strategy, then circles can change their individual goals accordingly, incorporating new roles and removing others. So Marketing and Sales might respond to a company-wide call to focus on brand awareness by adopting the goal of increasing web traffic by 50 percent. And this new web-traffic goal might mean switching out the Telesales role for a Web Developer role.

It's important to note that one role might be part of several circles. For instance, the Customer Service role might belong to the Marketing circle, the IT circle _and_ the Customer Service circle.

Another big advantage of circles is that they can be created or closed in a single meeting, very much unlike C&C departments. Imagine a company decides to cancel a line of products, Product X. In a traditional office, the department associated with Product X would disband and some employees might be laid off.

In a circle structure, though, the Product X team would go and join already existing circles, or create new ones. And employees could either keep working in the same role as before or adopt another.

But there is one circle that's permanent — the _lead circle_, which is responsible for the business's overall goals and vision. And if possible, all circles should include at least one member of the lead circle, thereby ensuring that each circle adheres to the company's broader vision.

> _"Departments are so 1984, circles are the new black."_

### 5. Restructure your company’s meeting culture and start getting things done. 

Here's a question that's probably hard to answer: When was the last time you thoroughly enjoyed a meeting?

Unfortunately, traditional C&C meetings tend to be ineffective and time-consuming because they're centered round an unstructured jumble of topics — everything from the company's strategic plans to the brand of tea served in the kitchen. Even worse, these conversations often don't produce many results.

But companies can discard this undisciplined approach. Start by implementing weekly _tactical meetings_ within each circle. These should be centered around the here and now — focused on achieving the circle's goals. (It's important to note that this is _not_ the time to brainstorm goals.)

Whenever there's an issue that can't be resolved in one of these tactical meetings, it should be removed from immediate discussion. Then, stakeholders should schedule a meeting centered round that specific issue.

Imagine the Telesales role needs specs from the Product circle in order to draft a new sales script. It would be pointless for the entire Marketing and Sales circle to spend time discussing this. Instead, the Telesales role should set up a separate meeting with someone from the Product circle.

Each circle should also hold _governance meetings_ focused on higher-level strategy every one to three months. These meetings are all about _what_ to achieve (as opposed to _how_ to achieve it) and they present a good opportunity for rearranging goals and roles.

Governance meetings are also the time to discuss _tensions_ — that is, recurring roadblocks and potential improvements. For example, someone might suggest fixing the product specs tension by having the Telesales role attend Product circle tacticals. If everyone agrees, this task is added to that role's responsibilities.

Finally, the firm should hold weekly _company-wide touch points_ to give circles the opportunity to connect. Here, each team gives a two-minute summary of their activities, the goal being to align the business as a whole.

### 6. A Blinkracy improves productivity by eliminating the need for managers. 

Would kids ever mow the lawn or dust the living room if they weren't told to? Probably not. But here's the thing: In traditional C&C workplaces, employees are treated like children, with managers playing the role of Mom and Dad. But they aren't children! They're skilled, capable adults.

A Blinkracy is different. Within this innovative organizational structure, circles are self-directed, which makes managers unnecessary. Instead, a decentralized task management process supports productivity.

It starts with _recurring tasks_, which refer to each role's concrete responsibilities on a daily or weekly basis. The group tracks these tasks with a _checklist_ during each tactical meeting, but only discusses tasks in-depth when problems arise.

For example, having each team member complete five customer-calls might be on the Marketing and Sales circle's checklist.

Then there are _projects_, which are larger and non-recurring tasks, like finding a new customer service representative. These need to be completed in order for the circle to achieve its goals.

The progress of each one of these projects should be briefly reviewed during each weekly tactical. At this point, the circle also determines "next actions" for the week ahead. For instance, next actions for finding a new customer service representative might be:

  * Draft and share job ad for feedback.

  * Post job ad online.

As you can see, it's a clear structure. And that's exactly why a Blinkracy eliminates the need for managers. Since both recurring tasks and on-going projects are internally defined and directed with the checklist, there's no need for managers to bark orders or pass down instructions.

The checklist structure also ensures that all tasks are completed, or, at the worst, put in the backlog to be handled later. This way, tasks simply can't fall through the cracks.

Now you have all the building blocks of a Blinkracy! You know about roles, circles, well-structured meetings and decentralized task management. Want to try implementing this within your own organization? The final blink will tell you exactly how to start.

### 7. To implement a Blinkracy at your own firm, start by testing the model within one department. 

Imagine you're painting the kitchen red. You'd probably experiment with the bold color on one wall first, to make sure you've selected the right shade before moving on.

Well, you should follow the same principle when you're restructuring your workplace: Implement the new model in just one area of your business first, so that you know the reality matches your expectations.

But even before you do that, start by defining your vision. Hold a strategy meeting with your executive team to set the company's goals for the coming quarter. You'll need these goals in order to define specific tasks. And in turn, these tasks will form roles and establish circles.

And once you've established your goals, choose a department in which to test this new organizational model. This is your company's first circle: Guide them through their first governance meetings to define tasks, set roles and articulate a purpose based on the company's broader goals.

At Blinkist, the content team was the first to try the new model. And at their first governance meeting, they established 50 discrete tasks (for example, "schedule content publication"). Then, these tasks were divided into six different roles, such as Recruiting and HR and Content Selection. The circle also defined a few larger projects, like "Growing content to twelve publications/week" and "Recruit content contributors."

Like a bold red paint job, this major organizational change may generate resistance and skepticism within your firm. But don't listen to the naysayers — just try it out and see if it works. If it does, employees will see how it benefits them and become evangelists of the new system.

That's exactly what happened at Blinkist: The experiment was a success and other teams noticed how happy the content team was with their new model. Soon enough, we opted for a full-scale rollout and adhered to Blinkracy for many years.

That's all, folks! Now you know all about this innovative organizational model and understand how to implement it within your own organization. The next move is yours.

### 8. Final summary 

The key message:

**Classic command and control organizations are antiquated, dysfunctional and ineffective. It's time to try an innovative new model, one that empowers employees, facilitates teamwork and produces results.**

Actionable advice:

**Each meeting should have a facilitator and a secretary.**

Since there aren't any bosses, there needs to be someone at every meeting who can facilitate discussion and curb crosstalk. Additionally, in case of absences and for future reference, meetings should be documented and shared.

**Suggested** **further** **reading:** ** _Reinventing Organizations_** **by Frederic Laloux**

_Reinventing Organizations_ discusses why companies around the world are getting rid of bosses, introducing flat hierarchies and pursuing purpose over profit. And ultimately, by adopting a non-hierarchical model, these organizations _thrive_.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ben Hughes and Sebastian Klein

Sebastian Klein is co-founder of, and Editor-in-Chief at Blinkist. He was formerly a consultant to the Boston Consulting Group and a serial entrepreneur. He specializes in knife skills and once rode a bicycle across Kuala Lumpur with no handlebars.

Ben Hughes is the Head of Content at Blinkist. He is a former management consultant, with expertise in corporate strategy and psychology. In 2013, _Finland Licorice Weekly_ named him in their "Top 50" and he currently holds the record for most comments on a Google Doc, an incredible 439 comments on just three pages.

Blinkist is a Berlin startup specializing in distilling complex concepts from great books into easy-to-understand fifteen-minute packs.

