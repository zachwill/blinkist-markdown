---
id: 553ffdf56233390007170000
slug: blood-feud-en
published_date: 2015-04-30T00:00:00.000+00:00
author: Edward Klein
title: Blood Feud
subtitle: The Clintons vs. the Obamas
main_color: ED342F
text_color: BA2925
---

# Blood Feud

_The Clintons vs. the Obamas_

**Edward Klein**

_Blood Feud_ (2014) tells the story of how two of the most influential families in the United States, the Obamas and the Clintons, came to despise one another, and how this mutual hatred has led to a long and cruel history of manipulation, back-stabbing and broken trust.

---
### 1. What’s in it for me? Discover the animosity that rankles between past, present and (perhaps) future presidents. 

Modern politics are all about spin and slick PR, but we all suspect that behind the scenes it's a whole other story. From time immemorial, politics have been a cut-throat business — literally! Plenty of feuds between powerful families throughout history were acted out with armies and assassins.

However, in the modern world, these quarrels aren't settled on the battlefield, but in the media or on campaign trails. Rivals (at least in the United States) no longer try to kill each other, but that doesn't mean they don't fight dirty.

These blinks take you on a journey through the most bitter feud in current US politics, between an earlier two-term president, the current one and perhaps the next one too. If you want to understand American politics, they are essential reading.

In these blinks you'll discover

  * how responsibility for the Benghazi incident was used to score points;

  * why Bill Clinton hinted that Obama would be more suited to serving coffee; and

  * why Obama said that Reagan was a better president than Clinton.

### 2. The animosity between the Clintons and the Obamas runs deep. 

At a Wellesley College class reunion in 2013, Hillary Clinton, according to one attendee, aimed heavy criticism at Obama, calling him "incompetent and feckless," and going so far as to say "you can't trust the motherfucker."

What inspired this vitriol? Was it simply a lack of confidence in Obama's skill as a statesman? Far from it. It was revenge.

Let's take a trip back to the 2008 presidential election. During the course of the campaign, Bill thought Obama had essentially put a "hit job" on him by implicitly calling him a racist. Furthermore, Obama snubbed Clinton by implying that Ronald Reagan had had a great impact in a way that "Slick Willie" had not.

Bill, who wanted to be remembered alongside FDR and Reagan as a great president, was deeply wounded by these remarks.

Moreover, Bill begrudged Obama for never seeking his advice during his campaign. Even George W. Bush had reached out to him!

Bill's disdain for Obama has developed into a visceral hatred. In fact, he has even said that he hates Obama more than any man he's ever met.

Hillary's resentment of Obama is similarly intense. It all started in the 2008 primaries after the Obama campaign said that she couldn't be trusted, implying that political calculation — and not conviction — was her sole motivation.

The Obamas share this disdain, feeling that the Clintons are racially insensitive and morally bankrupt. According to Senator Ted Kennedy, Bill has said of Obama, "A few years ago, this guy would be getting us coffee!"

Michelle has held a grudge against Hillary ever since she became aware of Hillary's aggressive, over-the-top attack on Edward Brooke, the first African American ever to become a US senator, during her time at Wellesley College.

The Obamas also feel that Bill spends too much time flying around on his private jet, dubbed "Air Fuck One," and feel his shady deals with shady businessmen, such as Nebraska multimillionaire Vinod Gupta, are done in return for campaign contributions, compromising his integrity.

> _"I'm not sure what Bill and I expected from the Obamas, but there was bad blood between us from the start." — Hillary Clinton_

### 3. Obama realized his 2012 re-election bid hinged on the Clintons’ support. 

If we don't like someone, it's easy enough to simply screen their calls and hope we don't run into her on the street. It's different in politics. In fact, sometimes you have to jump into bed with your enemy, no matter how much the thought may sicken you.

In August 2011, things weren't looking good for Obama. According to a Gallup poll, only 41 percent of voting-age Americans thought Obama was doing a "good job." Similarly, a Pew poll showed that of the 52 percent of independent voters who voted Obama in 2008, only 37 percent wanted him re-elected.

Moreover, Oprah Winfrey, whose vocal support for Obama during the 2008 election campaign had played a major role in his success, felt cheated by the presidential family. In return for her support — which had cost her many Republican viewers — she had been promised privileged access to the White House.

She kept her end of the deal, but her access to the White House never materialized. And so, in 2011 she announced she wouldn't be supporting Obama this time around.

Without a popularity boost from the "Oprah effect," what was Obama to do? It was time to call Bill Clinton.

Two of Obama's advisors were split on whether it was wise to enlist Clinton in the campaign.

Valerie Jarrett, Obama's closest advisor, argued fiercely against reaching out to Clinton. He was, she argued, impossible to manage, highly manipulative and would ask for far too much in return.

In contrast, advisor David Plouffe argued that it was absolutely necessary to use Clinton's popularity in order to mount a successful attack on the Republicans. He considered it a necessity. Furthermore, Clinton could help attract the white, working-class voters whom Obama had trouble reaching.

In the end, Obama sided with Plouffe, but Jarrett had nonetheless managed to convince him that, after the election, he could simply abandon any deal he had made with Clinton.

### 4. Bill Clinton’s support helped Obama get re-elected, but it came at a cost. 

Over a game of golf, Obama convinced the former president to help him seek re-election. Clinton was delighted with this opportunity — not only would it secure Obama's support for Hillary in 2016, it would also give him an opportunity to ultimately plan his treachery against Obama.

Clinton's main contribution was convincing people that the faltering economy wasn't Obama's fault. This was critical, as 56 percent of eligible voters at the time deemed Obama's economic policies, the so-called _Obamanomics_, a failure.

Clinton's words on the subject of economics meant a lot. During his tenure as president, he had kept the budget in check and even left behind a surplus.

He leveraged this trust in his expertise. For example, at a campaign stop in Virginia he answered critics of Obama's economic policy with claims that it usually takes ten full years to recover from the economic crises that follow ruptured housing bubbles. If you took this into account, Obama was _ahead_ of the curve, not _behind_ it.

But it didn't take long for Clinton to start using his influence to promote his own ends. The first opportunity came at the 2012 Democratic National Convention, where Clinton would give the nominating speech to boost Obama's campaign.

Clinton designed his speech to portray Obama as a centrist Democrat with a firm belief in responsibility and the virtues of opportunity, rather than the left-leaning, tax-and-spend Liberal he actually was. By portraying him in this light, Clinton implicitly compared Obama's presidency to his own and reinforced the Democratic policy platform the Clintons represented. In other words: while he was advocating for Obama, he was also implicitly advocating for Hillary.

As Newt Gingrich and others have pointed out, Clinton's speech was very anti-Obama when you read between the lines. It implied that Clinton, by working with the Republicans, had kept the economy running and made positive reforms for welfare — all of which were lacking under the Obama presidency.

### 5. The Benghazi controversy deepened the feud between the Clintons and Obamas. 

On 11 September 2012, a group of Islamists attacked a US diplomatic post in Benghazi, Libya, killing four American citizens, including ambassador Christopher Stevens. While it isn't entirely clear what transpired on that day, it is clear that Obama was poised to put Hillary Clinton in the firing line to avoid taking responsibility for those deaths.

In essence, Obama forced Hillary to play along with a cover-up story she knew might harm her in the future.

He wanted Hillary to issue a State Department release saying that the attack had been a spontaneous reaction to a blasphemous video online that demeaned the Prophet Muhammad. This was a complete fabrication, but Obama was willing to take the risk. If it became known that the attack was actually carried out by a terrorist organization, then one of Obama's key arguments for re-election — that he had ended the War on Terror — would be undermined.

Hillary knew she would have to play along, despite the fact that public knowledge of her inaction on increased terrorist threats could destroy her chances in 2016.

When she testified in front of the Senate Committee on Foreign Relations on her role in the affair, she lost her temper, saying that it didn't matter why the four Americans had lost their lives. This shocked many and was a serious blow to her image as a composed stateswoman.

As things dragged on, the Clintons became convinced that Obama was trying to pin the responsibility for the Benghazi tragedy on Hillary.

For example, during the 2012 vice presidential debate against Paul Ryan, Joe Biden said that the State Department had not told the White House that the Benghazi compound was in need of increased security measures. Statements like these put all the blame on the State Department, i.e., Hillary. They also infuriated Bill, who became convinced that Obama would never uphold his end of their bargain and support Hillary in 2016.

> _"You never tell somebody in politics to go to hell unless you can send them there" — Lyndon Johnson_

### 6. The Clintons exacted their revenge once it became clear that Obama would not support Hillary in 2016. 

Obama faced a number of difficulties during his second term. Not only was Congress rejecting all his proposals, but the Clintons, who felt Obama wouldn't keep his implicit promise to support Hillary in 2016, had also begun to exact their revenge.

Obama made revenge easy with a series of blunders, such as the roll-out of Obamacare, whose webpage had promised that purchasing health insurance would be as easy as "buying a TV on Amazon." Yet nothing worked. The page crashed continually, the policy explanations were all wrong and the sign-up process didn't function properly.

The Obama administration had also promised that people would be able to keep their current insurance plan, which turned out to be untrue. According to a report by NBC, Obama had _known_ that somewhere between 40 and 67 percent of Americans would actually lose their coverage.

Bill Clinton saw this as an opportunity. In an interview with the magazine _Ozy_, he called Obamacare a "fiasco." He said further that the president should honor his promise to the American people, and implied that Hillary would never have made such an amateurish mistake.

Then there was the civil war in Syria, which was only becoming more violent, and which Bill used as an opportunity to say that Obama lacked leadership.

In August 2012, Obama declared that incontrovertible evidence of chemical weapons use by Assad against his own people would be seen as a "red line" that necessitated military intervention. It didn't take long before reports came in that this was indeed happening. Yet Obama did nothing.

The Clintons used this inaction to their advantage. Hillary told the _New York Times_, for example, that she had argued for more decisive action. And at an event with John McCain, Bill declared that the president would look like a "wuss" and a "fool" if he did nothing.

In the end, Bill had succeeded in using the Obama presidency to garner political capital and position Hillary for a shot at the 2016 presidential election. Now it's up to her to seize the opportunity.

> _"That's the story of the Obama presidency. No hand on the fucking tiller." - Hillary Clinton_

### 7. Final summary 

The key message in this book:

**While the Clintons and the Obamas may seem like the friendliest of allies, there's a vicious struggle for power going on behind the scenes. Fueled by broken promises and back-stabbing, these two political families share a deep, mutual hatred for one another.**

**Suggested** **further** **reading:** ** _Hard Choices_** **by Hillary Clinton**

_Hard Choices_ offers a first-hand account of the trials and impressive diplomatic successes of the early years of the Obama administration. In this telling memoir, former Secretary of State Hillary Clinton places you at the administration's negotiating table where key policy decisions were made.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Edward Klein

Edward Klein is the author of the _New York Times_ bestseller _The Amateur,_ as well as former foreign editor of _Newsweek_ and a contributing editor to _Vanity Fair._

