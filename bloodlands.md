---
id: 5563c09b6461640007d10000
slug: bloodlands-en
published_date: 2015-05-28T00:00:00.000+00:00
author: Timothy Snyder
title: Bloodlands
subtitle: Europe Between Hitler and Stalin
main_color: B22D25
text_color: B22D25
---

# Bloodlands

_Europe Between Hitler and Stalin_

**Timothy Snyder**

In _Bloodlands_ (2010), author Timothy Snyder tells the tragic story of the people caught in the crossfire between Nazi Germany and the Soviet Union during World War II. The victims of the "bloodlands," or territories that after the war became the Eastern Bloc, were pushed and pulled by two ruthless powers and treated like pawns both before the conflict and afterward.

---
### 1. What’s in it for me? Learn the terrible fate of those caught between Germany and the Soviet Union in World War II. 

The horrors committed by Nazi Germany during World War II in Europe have been well-documented, and war victims have been commemorated and remembered in countless memorials and books.

What is often overlooked, however, are the horrors perpetrated by Stalin on his own people in the Soviet Union as well as on people living in bordering countries to the west.

In modern-day Poland, Ukraine, Belarus and the Baltic states, Russia's Stalin and Germany's Hitler unleashed wave after wave of terror as they battled over these "bloodlands" for ideological and military dominance.

These blinks show you how both dictators and their criminal regimes killed millions of people and tore up the map of Europe for generations.

In these blinks, you'll discover

  * why Stalin deliberately starved millions of his citizens;

  * why those who resisted the Nazis were then shot by the Soviets; and

  * how pre-war Poland was completely different than post-war Poland.

### 2. Under Stalin's forced farm collectivization, millions of people starved to death. 

The year 1933 was a difficult one in the West, as people lost their jobs and suffered with poverty and hunger during the Great Depression. For some people in Eastern Europe, however, life was a series of crises, as government policies led to a disastrous famine and millions of deaths.

This same year, the leader of the Soviet Union, Joseph Stalin, completed a five-year economic plan begun in 1928 to industrialize the mostly agrarian country. Part of this plan included the _collectivization_ of farms.

Collectivization policy stipulated that individual farmers must move from their small holdings to larger farms, to work the land together. Many farmers refused, so the government passed laws that gave collective farms legal advantages over smaller, private farms.

Among other things, collective farms were granted the right to vote to take seed grain away from private farmers. Thus farmers were virtually forced to join collective farms to survive.

The goal of collectivization was to make agriculture more efficient, but once farmers moved from their own holdings to larger collective farms, the incentives to work were less. Farm machinery at the collective farms were also outdated and faulty; what's more, the winter of 1931 had been especially rough. Farmers weren't able to meet their quotas, and people were dying of hunger.

But Stalin still demanded that the quotas set in his economic plan be met, refusing even by 1932 to accept that collectivization was a failure. He ordered farms that had missed quotas to hand over grain and livestock — leaving starving peasants with literally nothing to eat — to the state.

Seed grain that farmers were saving for the following season was also seized, pushing an already precarious situation into a full-blown crisis. Particularly in the Ukraine, famine was widespread.

By the end of 1933, an estimated 5.5 million people had died of hunger in the Soviet Union. Of those, 3.3 million alone died in Ukraine.

### 3. Stalin brutally persecuted minority groups and people labeled as class enemies in Soviet Russia. 

While Stalin's forced collectivization and economic plans took their toll on the population, they also helped to spur rapid economic growth in the Soviet Union.

Yet as many people had become dispossessed as a result of such policies, Stalin lived in fear of challenges to his rule in the form of social uprisings. So he decided to take measures against anyone he feared might act against the state.

Affluent farmers (or _kulaks_ ) became a focus of state persecution, labeled as "class enemies" during a wave of _dekulakization_ between 1929 and 1932. Very few farmers had been happy about the state policy of collectivization, but wealthier _kulaks_ had been strictly opposed to it.

As "kulak" was an official state term, officials could apply it to anyone they wanted to. Farmers identified as kulaks were arrested and deported — and often executed. All in all, about 380,000 people were sentenced to death during _dekulakization_.

Another group that Stalin believed posed a potential threat were ethnic minorities inside the Soviet Union.

Stalin was especially concerned with the Polish minority living in Russia's western territories, fearing that Poland and Germany could attack the Soviet Union from the west. In order to discredit this potentially powerful minority, Stalin decided to blame the 1933 famine on Soviet Poles.

Huge numbers of Poles were sentenced as criminals and sent to forced labor camps or were killed outright. Between 1937 and 1938, the state executed some 85,000 Polish people in Soviet Russia.

Polish people weren't the only group to be targeted. The government also persecuted thousands of Latvians, Estonians and Lithuanians, accusing them of being spies. Some 274,000 people were killed in waves of "national cleansing" in the early 1930s.

Thus even before the outbreak of World War II, life was not easy for those living in the bloodlands.

Unfortunately, the worst was yet to come.

### 4. Hitler and Stalin agreed to invade Poland from the west and east, wreaking havoc on the population. 

Before Nazi troops invaded Poland, Stalin's government was the only threat to the Polish people living in the bloodlands. This all changed in 1939.

In a move that shocked the world, Hitler made a pact with Stalin, his ideological enemy. Both states agreed to invade Poland; Germany from the west, Russia from the east.

Nazi Germany invaded Poland without warning on September 1, 1939, and Poland's allies, France and Britain, did nothing, leaving the country to fend for itself. Germany's assault was rapid and the Poles suffered terribly from the first days of the invasion.

Nazi soldiers were told that Poland wasn't a real country, and that Polish people were subhuman. In many cases, soldiers used Polish prisoners of war as human shields, or killed unarmed civilians.

Many Poles fled east, fearing Germany's approach from the west. On September 17, however, the Soviets entered the war. Poland was then trapped on both sides.

Some 500,000 Soviet soldiers crossed into Poland, many hoping to eventually fight Nazi Germany too, despite Stalin's pact. Importantly, the Soviet government believed that Nazi Germany's initial assault had made the Polish state obsolete, leaving the land free for the taking.

So the Red Army occupied the east of Poland while Nazi Germany occupied the west, just as Hitler and Stalin had agreed. And while Nazi soldiers mistreated Polish nationals, the Red Army too joined in, slaughtering helpless civilians or unarmed soldiers.

Neither Nazi Germany nor the Soviet Union had declared war before invading Poland; their collective action neglected the larger, international war.

Tragically, the Poles would only continue to suffer after the invasions.

### 5. The Soviet Union worked to crush the Polish resistance and sent in secret agents to identify fighters. 

The Red Army wasn't the only fighting force that invaded Poland. Stalin also sent in agents of the NKVD, essentially the Soviet Union's secret service.

Stalin wanted to absorb eastern Poland into the Soviet Union as quickly as possible, and the job of NKVD officers was to squash any potential resistance.

The Soviet leadership quickly labeled certain professions as dangerous, and made sure that agents went after any Polish person who held such a role — military veterans, foresters, civil servants and policemen, among others. Many Polish citizens were deported as a result.

In February 1940, NKVD agents rounded up some 14,000 people on Stalin's orders. Those arrested were placed in freight trains and shipped off to forced labor settlements in Kazakhstan or Siberia. More deportations followed. All in all, some 50,000 people died from the stresses of travel alone.

Educated Poles were also seen as a threat. The NKVD knew that Poland's political and social fabric would disintegrate without an educated class, so they identified and arrested anyone in it.

Some 25,000 Poles took part in resistance groups. The NKVD quickly penetrated these organizations, yet their existence alone gave Soviet leaders an excuse to crack down even harder on Polish intellectuals.

At the time, some 97 percent of prisoners in Soviet labor camps were educated Poles. Conditions in work camps were terrible, and many laborers died during their time in the camps.

So using a vast network of surveillance, Stalin successfully managed to crush the Polish intellectual class and other groups he considered dangerous.

Nazi Germany's system of suppression was different, however, but it was just as cruel.

### 6. Germany viewed ethnic minorities in the occupied regions as subhumans, and treated them as such. 

Hitler added some 20 million Poles, 6 million Czechs and 2 million Jews to the war empire of Nazi Germany when he began his occupation of Poland. By the end of 1939, Germany had become Europe's second-largest multinational state, after the Soviet Union.

Hitler however couldn't deport these "undesirables" to a distant labor camp like Stalin could, so he had to find other methods of suppression.

The Nazis wanted to deport Polish Jews from the occupied territories in 1940. Unable to find a suitable permanent location, the Nazis decided to order Jews to live in certain regions designated as "ghettos" in occupied Poland as a temporary solution.

The Nazis also forced Jews to wear yellow stars as a form of identification, and subjected them to humiliating regulations. Jews sent to the ghettos could bring little if no private property with them, were forced to live in crowded rooms and had limited food. Health conditions in the ghettos were horrible.

Between 1940 and 1941, 60,000 Jews died in the Warsaw ghetto alone.

Any Poles identified as belonging to the educated classes were sentenced to death and shot publicly. Polish-Jewish elites were initially spared, and were put in charge of implementing Nazi policies in the ghettos. Yet non-Jewish Polish elites were still considered a political threat.

In early 1940, Hitler ordered that all Polish people in any sort of leadership position had to be eliminated. This included people who belonged to the educated classes, the clergy as well as anyone politically active. By the end of the year, the Nazis had killed 3,000 Poles they deemed politically dangerous.

Yet unlike the Soviets who valued secrecy in their executions, the Nazis had no problem killing victims in public.

### 7. When the Nazis turned against the Soviets, there was no pity. Nazi soldiers let civilians freeze and starve. 

As we've seen, life was difficult for ethnic minorities in Stalin's Soviet Union. Many thought that Nazi Germany would offer some sort of salvation; yet when the Nazis showed up, things just got worse.

When Nazi Germany broke the pact it negotiated with Soviet Russia in 1941, the goal was to clear the way for the creation of a new German _lebensraum,_ or living space for the German race. Little concern was shown for the inhabitants of these soon-to-be bloodlands; Hitler considered the people living to the east to be Germany's natural enemies.

The end result of such an invasion was articulated long before the fighting started: Hitler wanted to expel or exterminate inhabitants of the east and replace them with Germans.

Nazi troops started their attack on the Soviet Union on June 22, 1941. Germany hoped to achieve a rapid victory — or _blitzkrieg_ — in the first nine to 12 weeks, so troops proceeded quickly, taking over regions of what had been Lithuania, Latvia, Estonia, eastern Poland, Belarus and parts of Ukraine.

The army seized food supplies in the conquered territories, leaving the civilian population to starve. Prisoners of war were barely fed, and German soldiers took their clothing as well to prepare for the coming winter.

Germany also enacted a general policy of forced starvation for civilian populations, resulting in millions of deaths.

Once Nazi troops reached Leningrad, fighting stalled and troops dug in. Of the 3.5 million people living in the city at the time, some 1 million starved during the German siege.

In Nazi camps for prisoners of war, the death rate for Red Army soldiers over the course of the war was 57.5 percent. All in all, some 3.1 million Soviet prisoners of war were killed during the period.

### 8. Nazi Germany began to recruit men into forced labor as military losses mounted. 

While Nazi German forces made significant progress in their initial push against the Soviet Union, victory still wasn't in sight by the fall of 1941, prompting Hitler to change his strategy.

Hitler had believed that in attacking the Soviets, Germany would gain supply lines and raw materials for its war machine; yet instead of solving economic problems, the push only created new ones.

While the Nazis expected the Soviet Union to collapse in a mere three months, the Red Army did fall back but had yet to surrender. The Soviet system was still intact and German troops had yet to reach the capital, Moscow.

What's more, even though Nazi soldiers had killed many Soviet soldiers, the Red Army (reflecting the population of the Soviet Union as a whole) was enormous and had a much larger pool of manpower from which to pull.

German forces meanwhile were taking on considerable losses as the _blitzkrieg_ continued. Officers were forced to draft more and more soldiers from Germany, which was causing problems back in the homeland.

This situation forced Hitler's hand. At the start of the _blitzkrieg_, Nazi soldiers killed all young adult males in any conquered territory, as they were seen as a potential threat. Now these threats were rehabilitated into necessary forced labor for the Germans.

So German troops recruited roughly 1 million men from the starving populations in prisoner-of-war camps to serve in the army of the occupied territories. Some were even forced to dig the trenches that would then be used as mass graves for executed Jews.

Others were given jobs as police to hunt down Jews or serve as guards in Nazi concentration camps.

Nazi Germany thus decided to spare the lives of some citizens in the bloodlands out of necessity. Yet the Jewish populations of these regions knew no such mercy.

### 9. Germany’s “final solution” for Jews shifted from deportation to mass extermination. 

As Nazi Germany's fortunes shifted, so too did plans for the Jewish people living in the occupied territories to the east.

While the Nazis originally planned to deport all Jews from the newly expanded German Reich, ideas gradually evolved into something far more sinister. Over the course of the war, Germany's _final solution_ for the Jews shifted from deportation to mass extermination.

By the end of 1941, there were more Jews living under German occupation than ever before. The German government had discussed various options for deportation, including a plan to move all Jews to a huge ghetto in the Lublin district of Poland. This was rejected, as Lublin was too close to areas where ethnic Germans lived.

In February 1940, Hitler asked Stalin if he'd take the Jews living in German-occupied territories to the Soviet Union, but Stalin refused. A further plan to move Jews to Madagascar couldn't be implemented because the Allied British navy controlled the sea.

By this point, there were roughly 5 million Jews living under German rule, and the collapse of the Soviet Union was nowhere in sight. So Hitler changed his plans significantly.

By 1942, Jews living in the western regions of the German Reich had already been gathered into ghettos. Special forces followed the Nazi army into newly conquered territories, hunting down and rounding up any Jews in the area.

Nazi officials then began building concentration camps in the western areas of the German Reich, with the goal of imprisoning Jews — many of whom would be worked to death — within the camps.

The Nazi strategy in occupied Poland was even more inhumane. Officials constructed huge facilities that existed solely to implement the systematic killing of Jewish people.

Overall, 6 million Jews were killed in World War II, and some 1 million alone were murdered at the death camp in Treblinka.

### 10. Resistance to German occupation was virtually impossible, and rebel uprisings were few and short-lived. 

It was only a matter of time before armed resistance groups came together in the German-occupied territories. Resisting the Nazis was extremely dangerous, however, and often resulted in death.

It was difficult for people in the bloodlands to fight their foreign occupiers. If they opposed the Nazis, for instance, they risked strengthening the Soviets.

The Poles and those living in the Baltic states were oppressed by both Germany and the Soviet Union before the war even began. Many people felt that resistance was futile, as they were victims of geography, trapped between two vicious powers.

In the Baltics, many people felt it was useless to fight the Nazis as it would only help the Soviets. So they chose to endure the war and to hope for a speedy end to the conflict.

Jews, however, suffered much more under German occupation than under Soviet occupation. It was nearly impossible for Jewish groups to build any sort of resistance movement, as in the ghettos, they were tightly observed and controlled. Any show of resistance risked death.

People who chose to fight were mercilessly hunted down. Some partisans did see a few successes, but most uprisings were quickly suppressed. Moreover, German soldiers were told not to discriminate between partisans and civilians during uprisings, which led to countless civilian deaths.

When the Jews of the Warsaw ghetto risked an uprising in 1943, the German response was brutal.

High-ranking Nazi leader and the man most responsible for the policies of the Holocaust, Heinrich Himmler ordered the ghetto to be destroyed. Some 13,000 Jews were killed in the uprising, many of whom were burned alive when Nazi soldiers set fire to their homes.

Of the remaining 50,000 survivors of the ghetto, nearly all were sent to death camps.

### 11. The Soviets persecuted both partisans and collaborators as they gained more ground in the west. 

As the Red Army made gains against the German forces, a few partisans eager to see the backs of the Nazis tried to collaborate with the Soviets.

Soviet soldiers under Stalin's orders were no saviors, however. Partisans more often than not were fighting for independence; yet Stalin wanted instead to annex the territories for the Soviet Union.

Resistance early on was a suicidal affair as the German forces were so superior. Yet as the war wore on and the Red Army marched west, many partisans saw their chance.

So in secret, some partisans briefed the Soviets troops on the positions of the German army. In exchange for this information, the Soviets gave little back, however; they encouraged the partisan groups to fight yet never promised aid or assistance outright.

The reality was that Stalin saw any partisan as a potential threat. He believed that a fighter who resisted a German occupation would eventually resist a Soviet occupation, too.

Ultimately, when the Polish Home Army attacked German troops in Warsaw in 1944, the Red Army was nearby but didn't intervene.

While the Poles had passion, the Germans had superior numbers and firepower. Half a million Polish lives were lost. And when the Red Army finally did enter the fray, they disarmed and arrested the few remaining resistance survivors.

The Soviets also viewed anyone who collaborated with the Nazis as enemies. Partisans were considered a threat because they resisted foreign occupation, but non-partisans were a threat because they _didn't_ resist — they were too compliant with German rule.

With such a policy, Stalin could essentially persecute absolutely anyone he wanted. So Soviet power went after anyone considered an undesirable, just as was standard before the war.

### 12. The Soviets abused or deported Germans living in territories the Reich lost at the end of the war. 

The war still wasn't over by February 1945, but Germany was nearly defeated.

Allied powers preemptively agreed that post-war Germany would be confined to a small area in the middle of Europe, and all Germans living abroad would be resettled there.

The Allies had to conquer Germany before they could enact this plan, however. So meanwhile, the Red Army entered German territory from the east, taking vengeance on the civilians there, raping women and press-ganging men on their march to Berlin.

The Soviet Union had released roughly 1 million of its own prisoners to serve as soldiers. So criminals in uniform continued to commit more crimes against Poles and Hungarians in the frenzied march.

The violence increased as soldiers neared Germany's original borders. Soviet soldiers were long familiar with the brutality of Nazi troops, and were out for revenge. Furthermore, even regular German farmers were wealthy by Soviet standards, and many poor soldiers believed that Germans had started the war in the hopes of stealing even more wealth.

German women suffered greatly, with rape common regardless of age. German men who tried to protect them were beaten or killed. About 520,000 Germans were seized for forced labor, and some 185,000 of those prisoners died.

Any Germans still living in the territories the Reich lost after the war were deported. About 3.8 million individuals were forced to leave their homes. The conditions on the trains in which they traveled were terrible, and some 400,000 Germans died from hunger or illness along the way.

### 13. Thousands of people died during the deportations that followed the end of World War II. 

With Nazi Germany's unconditional surrender on May 8, 1945, the war ended. 

Yet the suffering in the bloodlands wasn't over. Stalin had overseen large numbers of deportations before the war, and with its end, deportations continued.

Stalin wanted to secure more territory for the Soviet Union with the fall of Germany. Soviet troops in 1940 had already annexed Estonia, Lithuania and Latvia, as well as the eastern half of Poland during the German invasion.

In the summer of 1945, the leaders of the Soviet Union, the United States and Britain convened in Potsdam. Stalin insisted that Soviet control of the territories in Eastern Europe was necessary to prevent further German aggression. The Allied powers agreed with Stalin.

The Allies decided that the Baltic states should stay under Soviet control, and that the borders of Poland should be moved westward. So eastern Poland was ceded to the Soviet Union and the formerly German-occupied regions in the west were given to the new Polish state.

Stalin continued deportations in the Soviet Union's newly annexed areas, as he wanted a homogeneous society to better control dissent. He feared that people living in these new territories wouldn't be as loyal as those who'd already been living under the Soviet system. Once again, anyone identified as a potential threat was rounded up and deported to Siberia.

Poles living in the newly annexed Soviet lands were deported to the west. Between 1943 and 1947, some 700,000 Germans, 150,000 Poles, 250,000 Ukrainians and 300,000 Soviets citizens lost their lives amid these forced deportations.

### 14. Final summary 

The key message in this book:

**World War II was a time of terrible atrocities, yet nowhere was suffering greater than in the bloodlands of Eastern Europe. Millions of people in this region stuck between Nazi Germany and Soviet Russia lost their lives, as these two warring powers invaded and subjugated local populations. Many in the bloodlands had no hope of resistance, and only tried to endure and survive.**

**Suggested further reading:** ** _The Sleepwalkers_** **by Christopher Clark**

Christopher Clark's _The Sleepwalkers_ takes a fresh look at the outbreak of the First World War, focusing on the alliances established among Europe's nations in the years leading up to 1914. In his compelling and masterful account, Clark examines the decisions, both big and small, that led to the outbreak, and investigates the common belief that the war was an inevitability.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Timothy Snyder

A professor at Yale University, Timothy Snyder specializes in European history and the Holocaust and has written several award-winning books, including _The Reconstruction of Nations_ and _The Red Prince_.

