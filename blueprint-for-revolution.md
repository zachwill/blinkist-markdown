---
id: 58ecade8511add00045ae2ae
slug: blueprint-for-revolution-en
published_date: 2017-04-13T00:00:00.000+00:00
author: Srdja Popovic and Matthew Miller
title: Blueprint for Revolution
subtitle: How to Use Rice Pudding, Lego Men, and Other Non-Violent Techniques to Galvanize Communities, Overthrow Dictators or Simply Change the World
main_color: F2E349
text_color: 736C23
---

# Blueprint for Revolution

_How to Use Rice Pudding, Lego Men, and Other Non-Violent Techniques to Galvanize Communities, Overthrow Dictators or Simply Change the World_

**Srdja Popovic and Matthew Miller**

_Blueprint for Revolution_ (2015) is your guide to starting a social movement that inspires people to come together and make real change happen. These blinks use historical anecdotes to detail a variety of nonviolent techniques that can be used to apply political pressure, fight oppression and diminish fear.

---
### 1. What’s in it for me? Discover tried and tested nonviolent means to bring about revolutionary change! 

The word "revolution" usually stirs up images of barricades and armed struggle. It doesn't have to be this way, however. From Gandhi's successful struggle against the British to the overthrow of Serbian dictator Slobodan Milošević, nonviolent revolutionary movements have proven effective in changing the world for the better.

The following blinks will take you on a journey through successful nonviolent strategies that political activists like author Srdja Popovic have used to overthrow dictators, stand up to power and fight back against oppression. It doesn't matter if you're a hardened activist or simply a concerned citizen — the methods outlined here can be used by anyone, anywhere, to bring about democratic change.

In these blinks, you'll learn

  * what the "pillars of power" are and how to shake them;

  * how to use humor as a revolutionary tool; and

  * why nonviolent actions are always more effective than their violent counterparts.

### 2. Start a revolution by picking a battle you can win. 

During its infancy, any revolutionary movement is relatively unknown to the public, which makes it difficult to build a following. That's why movements need to make names for themselves before they can draw in the crowds. The most effective strategy is picking small, winnable battles.

Some of the most powerful nonviolent revolutions in recent history were launched in precisely this way. Just take Gandhi's long march to an independent India. It began with a smaller journey that has since been dubbed the _Salt March_ of 1930.

This action was a response to the high taxes on salt imposed by the British Empire. Knowing that salt was a necessity for everyone, Gandhi began a month-long march to the sea, at the end of which he would extract salt from the water.

By the time his journey reached its end, Gandhi had been joined by 12,000 others. The British were caught off guard and were forced to drop the salt tax. Launching his movement in this way gave Gandhi access to momentum and fame that helped him win bigger fights down the line.

So, small victories are key, but what about speeches? Although many believe you must have inspiring speeches to affect social change, it's about far more than that. Just take Harvey Milk, an American politician and the first openly gay person to be elected to public office in the United States.

As he began his political career, Milk thought that the way to acquire followers was to give speeches about the things that mattered to him. However, this strategy didn't pan out, and he lost two elections.

Learning from his failures, he revised his strategy and began campaigning about something that everyone in his home city of San Francisco cared about: the dog poop that plagued the city's parks. In the end, the campaign was hugely successful. Milk made a name for himself, and in 1977 he was elected to local government.

### 3. Successful political movements use inspiring visions to promise a better future. 

Movements are built to inspire action, but that's easier said than done. To really get people moving, you need a vision of the future that your followers can support. In the case of the Serbian _Otpor!_ movement, this dream focused on openness to the world, a powerful aspiration that led to the overthrow of the country's dictator, Slobodan Milošević.

Here's the story:

After the fall of communist Yugoslavia, Milošević rose to power in Serbia, installing an authoritarian regime. The dictatorship banned all foreign music and began a propaganda campaign against Serbia's neighbors.

Otpor! was born in this volatile climate. The young members of this group were set on being able to explore and enjoy the cultures of other countries as they had under communism. They envisioned a Serbia that was open to the world.

While their movement never held political office, they did help overthrow Milošević in 2000. As a result, Serbia is now a much more open place.

A similar example can be found in the South Asian island nation of the Maldives. There, the opposition's vision of pulling low-income citizens out of poverty gave them the boost they needed to rise to power.

In 2008, when the Maldives were preparing for their first democratic election in 30 years, the political party opposed to the Maldivian dictator, Maumoon Abdul Gayoom, was searching for a vision that would galvanize voters. To get a better sense of what this vision might be, one member of the group, Imran Zahir, took a trip through the country.

During his travels, he passed through several remote islands populated by elderly people with nothing to do but stare at the ocean. They had no jobs and were completely dependent on their children for food and medicine.

It was immediately clear that they needed financial assistance and healthcare. In the end, a campaign was mounted for a Maldives with a public welfare system for pensions and health care. By presenting this vision to voters — along with a free serving of rice pudding — the opposition attained victory in the 2008 elections.

### 4. To topple a dictator, identify and challenge the structures that support him or her. 

In 1973, a political science professor named Gene Sharp introduced a cutting-edge theory. He postulated that every regime is propped up by a handful of supports that he called the _pillars of power_. If you apply sufficient pressure to one or more of these pillars, the regime will topple.

The same logic can be applied to any institution. Just take large corporations. Their pillars of power are shareholders and the media outlets that influence stock prices through their reporting. Or consider a small African village, for which the pillars of power might be tribal elders.

Institutions like political parties are supported by multiple pillars of power from favorably viewed leaders to friendly news sources.

But in the case of dictators, the single most important pillar of power is economic. After all, every despot needs money to amass his armies and spread propaganda. When the bank runs dry, the dictator is unable to defend his regime and it becomes vulnerable to collapse.

That's why the first thing a movement should do is identify the sources of financial support the dictator draws on, and neutralize them. For instance, Syrian dictator Bashar al-Assad gets most of his money from foreign investments. If a movement could force foreign firms to close operations in Syria, Assad would become vulnerable.

Some nonviolent activist groups in Syria tried to expose the way these businesses were cooperating with the regime, attempting to earn them some bad press that affected their profits. The activists hoped that eventually the firms would decide that doing business with Syria did more harm than good to their bottom line.

Unfortunately, this effort was cut short by the vicious civil war that erupted.

### 5. Humor is a powerful tool. 

Anyone who enjoys a good laugh knows that humor is a near magical force; that's why activists love to use comedy to build the resistance.

How?

By cleverly deploying humor, activists can ridicule a regime, counteracting the fear it instills in its citizens. Just take a famous stunt by Otpor!, in which the group placed a baseball bat and an old oil barrel that had been painted with Milošević's face in the middle of a crowded street. Next to the display was a sign that said, "Smash his face for a dinar" (a small denomination in the local currency).

It didn't take long for people to line up to take a swing. But the best part wouldn't come till later when the police arrived.

Normally the cops would arrest the organizers, but they were nowhere to be found. So, the police had two choices: either arrest the innocent people striking the barrel, risking total outrage, or arrest the barrel itself.

In the end they took the latter route and the next day Belgrade was plastered with photos of two police officers arresting an old oil barrel. All of a sudden, Milošević's fearsome police didn't look so terrifying anymore.

But why does humor work so well? Primarily because it's hard for a regime to respond to pranks.

In October 1987, the communist government of Poland celebrated the seventieth anniversary of the Russian Revolution. The Polish opposition group _Solidarity_ decided to hold a rally during the display. They decided to ridicule the communist regime by manically displaying their "love" of communism. The streets rapidly filled with people wearing all red clothing and holding banners painted with bombastic communist language.

Naturally the authorities were upset, but they were stumped about how to react. After all, they couldn't arrest people for celebrating communism.

Or consider the Russian town of Bernaul, where citizens held a protest in the city's center using Lego figures. The toys even held little signs emblazoned with protest messages, and the stunt garnered a great deal of media attention.

> _"The human race has unquestionably one really effective weapon — laughter. Against the assault of laughter, nothing can stand." — Mark Twain_

### 6. Oppressive measures often backfire. 

Authoritarian regimes regularly try to force the compliance of their subjects. But whether they dish out punishments for disobeying orders or simply make a calculated effort to intimidate, there are times when such shows of brute force work against those deploying them.

Just consider the famous _Saffron Revolution_ in Burma, which began following a brutal act of oppression by the military regime.

On 19 September 2007, 400 Burmese monks began a march to protest against Burma's military government. While protest was banned in Burma, nobody thought that the army would violently repress the monks since they were considered the highest moral authority in the entire country.

Unfortunately, they were wrong. The army opened fire on the monks, killing them by the dozen, arresting countless others and later sentencing thousands in the courts.

The regime already had a reputation for violence, but this had clearly crossed a line. The people of Burma were spurred into action, rose up and kicked off the Saffron Revolution. Although it was eventually violently suppressed, the revolution paved the way for open elections eight years later.

Or consider the small Serbian town of Subotica, where activists turned the community against their oppressor to remove him from power. It all began at the height of Milošević's rule, when Subotica was ruled by a sadistic police officer named Ivan.

Ivan, who was built like an Olympic wrestler, had a reputation for viciously beating any members of Otpor! he found. One day, members of the movement put posters of Ivan's face all over town. The posters read, "This man is a bully! Call this man and ask him _why he is beating our kids_."

Before long, everyone in town — even those closest to Ivan — was carefully avoiding the police officer as well as his wife and kids. They had effectively become outcasts, and pretty soon the beatings stopped. Ivan's reign of terror was over and it was all thanks to a handful of homemade posters.

### 7. Nonviolent revolutions are always more effective than violent ones. 

When most people hear the word "revolution," they imagine an absolute bloodbath — a moment when violence is both inevitable and necessary. However, a wealth of historical knowledge and data shows us that nonviolent revolutions are actually more effective in terms of producing positive change.

That's because peaceful revolutions are more likely to create vibrant democracies. Just take a 2011 study penned by Erica Chenoweth and Maria J. Stephan, which considered statistics from 323 revolutions between 1900 and 2006.

The study found that the chances of success for nonviolent revolutions are double those of uprisings that use violence. Countries that experiences peaceful forms of resistance had more than a 40 percent chance of remaining democratic five years after the conflict ended.

For countries with violent revolutions, that number dropped to just 5 percent. Furthermore, countries that experienced nonviolent revolutions had only a 28 percent chance of having a civil war within a decade of the revolution, while countries with violent resistances had a 43 percent chance of such a conflict.

But the numbers aren't the only reason nonviolence is more desirable. It also has a unique ability to inspire action and draw in many people. After all, armed rebels, with their big guns and thuggish appearances don't exactly inspire trust. People are much more likely to join a group of joyful, ordinary people who are fighting for their rights than a militant group hell bent on taking up arms.

Also, while violent revolutions can only be joined by people who are fit, healthy and strong, almost anyone can participate in a nonviolent campaign, even the elderly and children. For these reasons, nonviolent revolutions amass much larger followings than their violent counterparts and derive tremendous power from mass support.

> _"By placing confidence in violent means, one has chosen the very type of struggle with which the oppressors nearly always have superiority." — Gene Sharp_

### 8. Final summary 

The key message in this book:

**Protest movements are built on powerful visions for a future that ordinary people can believe in and work toward. If you want to change the world, you can do it without violence. Propose a compelling path forward, work creatively and get out in the streets to make it happen.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Engines of Liberty_** **by David Cole**

_Engines of Liberty_ (2016) is an exploration into the influence citizens can have on government, and the changes that can be brought about through activism, the spreading of information and the mobilization of one's peers. When it comes to the big issues of our time, like gay marriage, guns and human rights, it's passionate citizens who are speaking up for what they believe in and bringing about change.
---

### Srdja Popovic and Matthew Miller

Srdja Popovic is a Serbian activist and a leader in the Otpor! movement that helped topple the Serbian dictator, Slobodan Milošević.

