---
id: 5d1ca9856cee070007ea188e
slug: blueprint-en
published_date: 2019-07-04T00:00:00.000+00:00
author: Nicholas A. Christakis
title: Blueprint
subtitle: The Evolutionary Origins of a Good Society
main_color: 1D61D6
text_color: 164AA3
---

# Blueprint

_The Evolutionary Origins of a Good Society_

**Nicholas A. Christakis**

_Blueprint_ (2019) explores the psychological traits that all humans share. Examining the evolutionary underpinnings of our social behavior, these blinks shine a light on our ancestral past and investigate how love, cooperation and friendship came to be indispensable items in our social tool kit.

---
### 1. What’s in it for me? The social ties that bind. 

In a world of Trump, Brexit and populist politics, it's easy to focus on what divides us. Every day, whether on social media or in our newspapers, commentators line up to explain how our identities are different and how incompatible we all are. But what if all this discourse is simply a vanity of small differences? What if we are actually genetically predestined to be universally similar, in our relationships, our friendships, and even in our cultures? 

Let's take a drive through our evolutionary road map, and learn about the common humanity that unites us all, regardless of nationality, gender, religion, or race. Drawing on cutting-edge insights from his social science laboratory, Professor Nicholas A. Christakis takes a look at how our evolutionary past has shaped our present, and examines the psychological cornerstones of every human society throughout history. 

Read on to discover

  * what shipwrecks can teach us about evolution;

  * where romantic love comes from; and

  * how humans are genetically designed for culture.

### 2. Humans come ready-made with a blueprint for social behavior. 

We don't always need words to understand one another. As a young boy, Christakis was one of the only Greek children to arrive on the Turkish island of Büyükada. Nevertheless, the author and his younger brother quickly made friends, and spent a long summer rampaging around the island with the local boys. They even waged war on rival groups, using pinecones as weapons. In later years, the author reflected on this cross-cultural friendship group. How did it work so well, despite their linguistic and cultural differences?

As a researcher of social behavior, the author concluded that these childhood friendships were made possible by a mental manual of social skills, instincts and tendencies, which guide the behavior of every human being on the planet. In other words, there is a universal blueprint for social behavior encoded in our genes. These instincts help us form societies, which can be as small as a group of Turkish and Greek schoolboys united in their search for adventure, or as big as sovereign states composed of hundreds of millions of people. This collection of universal social tendencies, which the author calls the _social suite_, includes the capacity for love and friendship, as well as teaching and learning from others. 

Unfortunately though, the _social suite_ also includes a tendency to favor your own "group." 

A 2011 study, for instance, found that five-year-old children wearing a red T-shirt consequently liked and favored other children also in red T-shirts, and discriminated against those wearing different colors. This prejudice occurred even when the children were told that the colored shirts had been allocated at random. Studies like these demonstrate that humans have an affinity for "likeness" — no matter how small we perceive that likeness to be. 

Of course, even if we identify with people similar to us, we don't consider them all to be the same. Each of us are born with the ability to develop and recognize individual human identities. Just consider the almost universal human practice of using personal names. Although this might seem inconsequential, recognizing individuals is the bedrock of other human traits, such as love and friendship. After all, if we were unable to discriminate between two individuals, we'd have no way of preferring one person over the other, or repaying favors that our friends do for us.

> _"The presence of our fellow humans — people we must interact with, cooperate with or avoid — has been as powerful as any predator in shaping our genes."_

### 3. The fortunes of shipwreck survivors show the social suite in action. 

The nature versus nurture debate is almost as old as science itself. And for good reason — it is often fiendishly difficult to tell whether any given behavior is a result of our genes or our environment. Unfortunately, social behavior is no different. This begs the question: how can we be sure that the social suite of human behaviors are really inbuilt evolutionary adaptations, rather than spontaneous responses to the situation we find ourselves in?

To know for sure, we would have to conduct life-long experiments in which humans were raised in an environment without any pre-existing society or older people, and see how these unfortunate guinea pigs interacted with one another. Of course, ethical considerations make these sorts of studies impossible. Shipwrecks, however, may provide a close substitute for these experimental conditions. Those who survive shipwrecks, for instance, often find themselves washed up on deserted islands without any trace of established human society or infrastructure. 

Let's take a look at the _Invercauld_ and the _Grafton_, two ships that crashed in 1864 on different sides of Auckland Island, off the coast of New Zealand. Neither group of survivors knew the other was there, and crucially, their survival strategies were very different. The survivors of the _Grafton_ exhibited almost the full social suite of behaviors, with everyone helping one another from the moment their ship ran aground. The Captain's Mate, for instance, would have died if the rest of the crew hadn't worked together to pull him out of the water with ropes. 

In contrast, the survivors of the _Invercauld_, within days of coming ashore, left behind the weakest man among them to die. From here, the fate of the _Invercauld_ deteriorated further. The men continually split up, deserted the weak and the sick, and even ate one person. By the time the _Invercauld_ survivors were rescued, only three out of the 19 who had survived the initial wreck remained alive. 

And the _Grafton_ community? They continued as they had begun — working together and cooperating to survive. Not only did they remain as one group, which demonstrated their close social bonds, but they also left no man behind. The survivors even set up a makeshift school while they waited for rescue, thereby engaging in teaching and learning — an important social suite behavior. This adherence to typical social behaviour definitely paid off: all the men who made it to shore also made it off the island.

These two very different communities clearly show the evolutionary advantage of the social suite in boosting people's chances of survival. Furthermore, it seems some humans really do have a blueprint for social behaviors, like cooperation, teaching, and helping others, which guide their behaviour even outside of their normal environments.

### 4. When it comes to relationships, love is universal – and monogamy is useful. 

What aspects of sexual relationships are universal? Until recently, the author assumed kissing was universal. But it turns out he was wrong about that. The Tsonga people of Mozambique, for example, do not kiss. In fact, upon hearing of this strange practice, the Tsongas questioned why two people would want to share saliva and all its accompanying germs. This got the author thinking: which traits of human sexual relationships _are_ present across all cultures?

The answer, quite simply, is love.

In scientific terms, "love" describes a deep emotional connection to one's partner, beyond feelings of a mere sexual nature. Some experts believe that the development of love within couples was an evolutionary accident. In their opinion, humans originally only felt an emotional affinity for their children. However, over time, this love for our offspring extended to our mates, too. This process — where one evolutionary adaptation is repurposed into another — is known as _exaptation_. In this respect, the evolution of human romantic love has much in common with the evolution of flight amongst birds. Birds, it is believed, first grew feathers to keep themselves warm, but later these were repurposed to help them fly. Similarly, humans initially felt love only for their offspring, but later redistributed it to include their sexual partners too. The evolutionary explanation for this romantic love may be that it helped ensure a family stayed together during periods of pregnancy and child-rearing, thus improving the chances of their offspring's survival.

Interestingly, it's only in the last two thousand years that monogamy has overtaken polygyny as the most widely practiced form of romantic relationships. How can we explain this switch? Well, anthropologists believe that monogamy confers particular advantages on societies. In monogamous communities, for instance, every man can have a partner. In contrast, in a polygynous set-up, many men are inevitably left without wives. Crucially, these unattached men, without hope of a family and children of their own, tend to feel less invested in the future. This bleak outlook makes them more likely to engage in antisocial behavior such as violence, theft and rape. Their bad conduct then destabilizes the whole society; diminishing its resources and making it less productive. We can see evidence of this behaviour in countries like China, where sex-selective abortion practices have created a schewed gender ratio, resulting in more men than women. Sure enough, evidence suggests that these 'leftover' young men, tend to live more violent lives and die younger than their married counterparts.

### 5. Friendships help people through rough times and are expressed in a variety of ways. 

On December 17, 2015, 15-year-old Zavien Dobson was sitting on his porch with three friends in Tennessee, when a car pulled up and opened fire on them. Zavien didn't hesitate. He threw himself on top of his female companions and died in a hail of bullets. Thanks to his actions that day, each of his friends survived this random and senseless attack. In light of Zavien's moving sacrifice, the author began wondering: why do we feel such love for our friends? 

Hearteningly, evidence suggests that friendship is a universal feature of nearly all human societies. Furthermore, it seems that the vast majority of cultures share the same core essentials of friendship ⁠⁠– namely, affection, trust and mutual aid. Another universal trait of friendship appears to be the acceptance of our vulnerability. You may not mind being teased by a friend, for instance, because you trust that they mean well. 

But not all friendship traits are universal. While disclosing personal information and regularly socializing with one another are seen as important aspects of friendships in the United States, these attributes are not common in all cultures. In other regions of the world, physical contact rather is a typical expression of friendship. In 2005, for example, many Americans were surprised to see the US President George W. Bush holding hands with Crown Prince Abdullah of Saudi Arabia. To Saudi Arabians, however, it was a natural gesture of friendship. 

Clearly, friendship comes in many shapes and sizes. But beyond its various expressions around the world, why do humans engage in it at all? 

Experts believe that the ability to develop and maintain friendships gave our ancestors an evolutionary advantage. In the earliest human societies, a lack of food, or bad weather, illness or injury were constant threats to you and your offspring's chances of survival. And if you wanted to survive these precarious circumstances, it was important to have relationships with people you could rely on to help you out in times of need. These needed to be people who weren't always expecting something in return. In other words, you needed genuine friends. This is still somewhat true today. In poorer American communities, for instance, people are more likely to call on their friends for childcare, loans and home repairs than their middle-class counterparts.

### 6. Technological progress has opened up new ways of studying cooperative behavior. 

In 2005, Amazon launched a software system that enabled them to hire tens of thousands of part-time workers to complete minor online tasks. This massive platform is known as Amazon Mechanical Turk, and its creation allowed the company to recruit workers, record their contribution and pay them accordingly. Before long, social scientists realized they could use the Turk system to set up artificial "communities" of users and see how they responded, individually and as a group, to tasks set by the researchers.

For the author and his laboratory, this vast online apparatus represented an exciting opportunity to explore the social suite of behaviors. He and his team set out to learn whether Turk users would still display these supposedly universal social behaviors online, or whether this brand new environment would prompt them to behave in brand new ways. 

When it came to one of the most distinctive human traits of all — cooperation — the author's studies were illuminating. One experiment saw his team set up 40 different social networks, with Turk users randomly dropped into each one. Each person in every network also had a unique set of neighbors. Every round, the researchers gave one individual from each of the networks a sum of money. They were told that they could either keep this money for themselves or give it to their neighbor. If they did the latter, then the gift to their neighbor would be doubled — meaning their neighbor would be left significantly better off, and they would be left poorer. However, they were also made aware that, in the next round, their neighbor could choose to reciprocate and give _them_ money, whereupon the amount would also be doubled. By structuring the experiment in this way, the researchers cleverly gave each network a choice — cooperate and potentially make more money, or choose not to cooperate and make much less.

So, what did Turk users choose to do?

In those networks where _everybody_ donated to their neighbors, cooperation was the norm. However, in the groups where just one person began to keep their money for themselves, defection spread like wildfire. This just goes to show that, while cooperation seems to be a natural human behavior, it is fragile and will perish under certain conditions.

### 7. Many other species show the same social tendencies and behaviors that we do. 

Humans and animals share many similarities. Up until 1964, scientists were stumped as to how to repair damage caused to heart valves by cardiovascular disease. That was until a French surgeon called Alain Carpentier tried replacing them altogether, with valves transplanted from pigs. Incredibly, his technique worked, and is still used today. Clearly then, our organs have a lot in common with those of pigs. But what if animals are similar to us in even more subtle ways?

As science progresses, we are increasingly faced with an uncomfortable truth: that the very same animals we eat and experiment on might share many features of our social suite. Evidence suggests, for example, that elephants have friends, gorillas have their own language, and rats feel empathy. Not convinced? Then just consider the parallels between capuchin monkeys' social behaviour, and our own. These South American primates put their fingers into other monkeys' mouths and allow them to gently bite down on them, thus exhibiting the apparently human trait of accepting vulnerability in front of one's friends.

Observations like these indicate that we share many traits with other animals. But how did this come to be?

The answer lies in a process known as _evolutionary convergence_. This describes the phenomenon whereby different species arrive at the same evolutionary adaptation separately. Birds and bats, for instance, both evolved separately to have flight. Similarly, humans and some other species — such as elephants, whales and apes — have all evolved separately to share many social traits, like cooperation, recognizing individual identity and even social learning. The reason for this convergence is that all of these species, including our own, share almost identical _environments_. Of course, it is not the plains of the Savanna or the oceans that we share, but our _social_ environments. 

Animals like humans, elephants and apes have evolved in the presence of other members of the same species, with whom we needed to live, interact, and get along with to survive. This meant that those individuals who were more social, and exhibited behaviors such as cooperation, trust and friendship were better adapted to their environment, and thus more likely to pass their social genes down to the next generation. In turn, the most social of these offspring were also more likely to survive, and this natural selection continued until an optimal type of social behavior emerged. It is this optimal behavior that seems to comprise the social suite, which has been arrived at separately by many different species around the world.

### 8. Humans conquered a hostile planet through a combination of culture and genetics. 

The human race is scattered across the world. As a species, we've made our homes almost everywhere, from the freezing temperatures of the Arctic to the humid Amazonian rainforests. But how exactly have our genetics helped us survive these hugely variable environments? As it turns out, our genes have enabled us to prosper by endowing us with the ability to develop culture.

In an evolutionary sense, culture refers to knowledge that's transmitted from person-to-person within a group, which then influences individuals' behavior. Furthermore, culture is an evolutionary adaptation in itself, in that natural selection has equipped us with genes that can create culture. Our genetics give us relatively long lives, for instance, providing plenty of chances for information to be passed on between generations. Humans also have several psychological features that seem tailor-made for culture, like our desire for conformity between individuals and our tendency to mimic the behavior of older individuals. Extremely young children, for example, have been found to regularly copy every action they see an adult performing, no matter how trivial the behavior might be. 

Another important aspect of human culture is that, much like the process of natural selection itself, it can evolve and become better adapted to the environment to which it must respond. In the same way that genetic mutations may lead to survival advantages, great ideas may be implemented rather than mediocre ones, thus become part of a group's ongoing culture. 

But culture isn't just optimized for survival — it's absolutely crucial.

Just consider the many European adventurers who got lost on expeditions to faraway places. Without cultural knowledge of their environment, they all too often perished. Their clothes fell apart, their equipment became useless and their food and water supplies dwindled. In the most extreme cases when several explorers got lost together, they sometimes even resorted to cannibalism. In other words, these Europeans were like fish out of water; they simply did not have enough information about their environment to survive. For this same reason, the only explorers who made it out alive were typically the ones who contacted local people — natives who could locate food and water, cook potentially deadly plants and who may have known the medicinal qualities of local flora too. The knowledge these natives possessed of their specific culture meant that they knew how to survive, and could share this invaluable information.

### 9. Final summary 

The key message in these blinks:

**Every human being on the planet, regardless of where they come from, shares a fundamental set of social tendencies and preferences known as the social suite. The ability to cooperate, learn and form loving relationships with one another are all part of this suite. These traits have been central to our ability to survive. But the unique success of the human species also depends on our aptitude for developing culture. Without it, we are bound to perish.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Better Angels of Our Nature_** **, by Steven Pinker**

Now that you've learned the secrets of our common humanity, why not take these ideas to the next level? By checking out the blinks to _Better Angels of Our Nature_ by the renowned linguist Steven Pinker, you'll learn how we can unlock the best of human nature and use it to create a better society. 

With its unflinching look at humanity's history of brutality, this book-in-blinks explains why we're driven to use violence and, more encouragingly, why we're increasingly choosing not to. So, discover what might lie at the end of the path set by our social blueprint, and head over to the blinks to _Better Angels of Our Nature._
---

### Nicholas A. Christakis

Nicholas A. Christakis, MD, PhD, MPH, is the Sterling Professor of Social and Natural Science at Yale University. In 2009, _Time_ magazine named him as one of the world's most influential people. Elected to the American Academy of Arts and Sciences in 2015, Christakis also runs the Human Nature Lab, where his team researches a wide variety of social behavior.

