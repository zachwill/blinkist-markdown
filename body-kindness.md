---
id: 5d00b1f46cee070007f2853f
slug: body-kindness-en
published_date: 2019-06-13T00:00:00.000+00:00
author: Rebecca Scritchfield
title: Body Kindness
subtitle: Transform Your Health From the Inside Out – and Never Say Diet Again
main_color: F99AA1
text_color: 945C60
---

# Body Kindness

_Transform Your Health From the Inside Out – and Never Say Diet Again_

**Rebecca Scritchfield**

_Body Kindness_ (2016) offers a refreshing approach to your overall physical and emotional health — one that allows you to tune into your own unique body and discover what is best for your well-being. This approach does away with one-size-fits-all dietary and exercise rules that often introduce a lot of added stress and anxiety into our daily lives. With small, easy-to-implement steps, you can learn how to boost their health and happiness, without the rules and rigidity.

---
### 1. What’s in it for me? Discover a healthier lifestyle based on self-compassion, rather than strict rules. 

If you've ever tried to keep up with some new exercise plan or stick to that new diet everyone's talking about, there's a good chance that you started out strong but eventually drifted away from it. Don't worry; it happens to most of us. And the reason why is that most of these self-improvement concepts are riddled with more rules and guidelines than any reasonable person can put up with for more than a couple of weeks.

What's really harmful is all the stress and guilt that comes with trying to measure your food, count your calories or, god forbid, miss out on a scheduled workout. If this sounds all too familiar to you, then you may enjoy hearing about _body kindness_, a different approach to feeling better about yourself — because isn't that what dieting and exercise are supposed to be about?

As author Rebecca Scritchfield explains, this approach is about putting aside strict rules and figuring out what is going to work for you based on your unique circumstances and what's important to you. You'll find plenty of tips and advice on how to relax, let some joy into your life and live healthier at the same time.

In these blinks, you'll find out

  * how some diets can turn into food phobias;

  * why you probably need more sleep than you're currently getting; and

  * how making a "PACT" with yourself can curb your negative thinking.

### 2. Generating happiness through good life habits can instigate an upward spiral in your life. 

When you think about the factors that contribute to happiness, do things like money, youth or a slim figure come to mind? Well, according to psychologist Sonja Lyubomirsky, these factors only contribute to around 10 percent of a person's overall happiness. Your daily choices account for 40 percent, while the remaining 50 percent comes down to your genetic makeup.

The 40 percent that makes up your daily choices is a pretty significant factor to have control over, nonetheless, making the right choices requires a great deal of willpower. But the good news is that you can make it easier on yourself by developing good habits.

If you repeat a healthy behavior often enough, you can turn that behavior into a habit that becomes second nature. In other words, you'll reach a point where you won't even have to think about it — it'll just come naturally to you.

For example, let's say you want to be less frivolous with your time at home after work. Maybe your routine now is to absentmindedly waste time checking your Instagram feed or anxiously scamper around the house looking for something to do.

Well, this time could be made more productive if you develop the habit of coming home, changing your clothes and doing 30 minutes of yoga. This would be a win-win scenario as far as your happiness is concerned. It's not only a healthy practice; it would also help quieten down your mind after a busy day at work, and it would allow you to enjoy your evening more consciously.

The positive effect that good habits can have on your life is something the author calls _spiraling upward_.

The easiest way to understand this concept is to see it in action. Let's say you want to increase your creative output. For instance, you could start spending 30 minutes doing some creative writing after breakfast. This will both spark your imagination and let you start the day with the positive feeling of being productive.

Now, that positive feeling often carries over into another good decision, such as a willingness to do some exercise or a feeling of being energized enough to tackle some household chores. This is how a good habit can get you spiraling upward.

Thus, the more positive habits you have in place, the better you'll feel. This, in turn, will increase the chances that you'll continue making good choices on how to spend your time throughout the day — and that will lead to even more good feelings!

> _"If you find yourself caught in a pattern of choices that you feel bad about, something isn't right."_

### 3. Strict diets can create a toxic relationship with food, so try a more reasonable, balanced approach. 

If you feel constantly worried about what to eat, you're not alone. There's no shortage of magazine articles, blog posts and social media conversations filled with fearful warnings about what you should and shouldn't eat. So it's no wonder that people obsess over their diet to the point that they're at war with food — constantly fighting over the question of whether or not something is healthy or unhealthy.

But here's the irony: What's really unhealthy is all that anxiety over what you eat! In fact, obsessing and being fearful of food is downright toxic.

With so much being said about what's healthy and what isn't, certain foods have ended up in very distinct categories. Kale, for example, is healthy, while cupcakes are unhealthy. But all this means is that if you have one cupcake at a birthday party, you can end up feeling racked with guilt.

And when that guilt turns into obsessing about which foods are considered okay, it can turn shopping and eating into a nightmare. Is this carrot organic? Should I only buy gluten-free bread? Can I eat any dairy at all? Am I getting enough antioxidants? The questions are endless.

Ultimately, being overly controlling of what lands on your plate isn't healthy because in the best case scenario, it adds to your levels of stress and anxiety, and, in the worst case, it leads to eating disorders like anorexia. Eating can be one of the great pleasures in life, so it should never have the toxic emotions of guilt and fear associated with it.

But if that's not convincing enough, there's also the fact that forcing strict dietary rules upon yourself can often backfire.

The author knows a woman named Susan who tried to cut bread from her diet at a time when she was obsessed about avoiding carbs. But not unlike the proverbial forbidden fruit, the more she tried to avoid bread, the greater her cravings became. The struggle became so intense that she even avoided certain social events where free appetizers of bread slices might be found sitting on the table. The temptation would have been too much!

Fortunately, Susan decided to adopt a healthier approach to her diet, and with the author's help, she allowed herself to have bread with one meal per day. This balanced approach not only removed the fear over an unavoidable food item; it also provided a sensible plan to avoid overindulging in it.

> _"Legalizing bread and her other forbidden foods gave Susan the comfort and confidence that she could eat all kinds of food in feel-good ways."_

### 4. Exercise is one of the best things you can do for your body if you do it for the right reasons. 

Even if exercise isn't your favorite thing to do, there's a good chance you've had at least one workout that left your mind feeling clear and your body feeling a certain lightness and energy that only comes after physical exertion.

There's a good reason people feel this transcendence after a workout; it's because exercise is among the best things you can do for your health.

Here is a small sample of the many health benefits that have been found to come with regular exercise: decreased risk of chronic diseases like heart disease, diabetes and cancer, as well as reduced symptoms of depression.

Why does it provide these benefits? Well, during exercise, you're pushing your heart and lungs to work faster, which means you have more blood pumping through your body. As a result, your body is more effectively distributing nutrients and nourishing cells throughout your body, including the ones in your brain.

But despite all the benefits, 30 percent of Americans exercise only once a week or less, while the average American moves around for about 20 minutes per day! What this means is that, to feel healthier and happier, we need to find better ways of motivating ourselves to exercise.

Often, the key to effective motivation is finding the right reason to do something.

For example, if your reason for exercising is about superficial concerns like achieving the perfect body, this can easily lead to discouragement rather than motivation. That's because it can be impossible to live up to the images of chiseled abs and perfectly sculpted bodies we're bombarded with in ads and on TV. Once you feel you're working toward an unachievable goal, you're likely to throw in the towel.

Instead of focusing on body image, the more effective motivation is to focus on _body kindness_, which is all about how healthy and beneficial exercise is for your emotional and physical well-being. And a great way to improve your chances of staying motivated is to find a coach or instructor whose work is grounded in this approach.

To help you keep the philosophy of body kindness in mind, you can develop a mantra, such as reminding yourself each morning that doing some exercise today will be an expression of your love and appreciation for your body and mind.

In the author's experience, this body kindness approach to exercising has resulted in her clients exercising more often and with far more enjoyment.

### 5. Sleep is important for physical and emotional well-being, so create a restorative sleep sanctuary. 

Are you one of those people who can wake up after just six hours of sleep feeling perfectly energized and restored? Well, if so, you can consider yourself special! Research shows that only 5 percent of the human population carries the genetic variation that allows them to fully recuperate after just six hour of sleep. Meanwhile, the other 95 percent of us require significantly more.

Getting the right amount of sleep is about more than just getting some shut-eye; it's vital to the well-being of both your mind and body.

The benefits of sleep are numerous, not the least of which is that it helps you learn new things and enjoy more emotional stability. When you sleep, your brain has enough downtime to sort through the information you've acquired over the day. It then stores the important information and discards the rest. That's right; sleep is basically a tidying-up session for the brain.

While the brain is busy decluttering, studies have shown that we experience a series of sleep cycles, with each one lasting around two hours. Researchers also believe that we generally need to experience around four or five of these cycles per night in order to feel fully restored when we wake up. This is why most people need eight to ten hours of sleep.

One of the most important parts of the cycle is what's known as deep sleep, or slow-wave sleep, as this is the time when the body regenerates and produces important hormones that regulate metabolism.

Another cycle is known as REM, or Rapid Eye Movement sleep, which is the lighter period of sleep when dreaming takes place. Your dreams are important even if you never remember them because they strengthen memory and assist in the brain's processing of emotions.

Lots of people can experience times when sleeping is difficult, but there are steps you can take to get a better night's sleep.

First off, turn your bedroom into a _sleep sanctuary_ by taking anything out of your bedroom that isn't necessary for sleeping. TV? Computer? Take them out and make space for them elsewhere.

Next, make sure you have a clean and comfortable bed by washing your sheets regularly and investing in a mattress that is as hard or soft as you need it to be in order to have a long, deep sleep.

Finally, always get to bed early enough to have the eight to ten hours of sleep you need, and get your mind ready to rest by staying away from all electronic devices at least an hour before you intend to sleep.

With these simple practices, you should find it easier to get the sleep your body needs.

### 6. If you act mindfully, negative thoughts don’t have to lead to negative actions. 

There's a good chance you've tried certain diets or exercise regimens that didn't stick. Or maybe you've been to nutritionists and therapists but stopped when you believed they weren't doing you any good. With enough of these experiences, you may start feeling hopeless and decide that you're done with self-improvement plans altogether.

If such thoughts sound familiar, don't worry. Having them can happen to the best of us, and it doesn't mean that you can't follow through on whatever life changes you hope to make.

A lot of self-help gurus will advocate for using the power of positive thinking, but there is a more effective way of dealing with the negative thoughts we have. The trick is to remind yourself that thoughts aren't necessarily true, and they don't need to be acted upon.

So, the next time you recognize a self-critical thought, like telling yourself you'll "Never stick to an exercise plan, so don't bother trying," remember that this is just an unsubstantial, intangible thought. It doesn't have to have any power over you unless you decide to act upon it — and there's no need to do so!

In other words, just because you _think_ something doesn't mean you should let it control your behavior. Effective people doubt themselves all the time, especially when it comes to a challenging exercise goal, but they don't let that stop them.

Many people think that they're too restless for meditation or that they're going to hate a party they're obliged to go to. But instead of letting these thoughts get in the way, they find that meditation changed their lives and they met some amazing people at the party. If you let negative thoughts control your actions, there's little chance you'll discover all the potential for new and wonderful things in life.

A great tool for removing negative thoughts of their power over you is to make a _PACT_ with yourself by practicing Presence, Acceptance, Choice and Taking action.

_Presence_ means having awareness and noticing a negative thought for what it is when it pops up.

_Acceptance_ is about not mindlessly reacting when negative thoughts arise, and instead observing the thought, accepting it as natural and considering what may have triggered the thought.

_Choice_ is recognizing that you are not beholden to act according to your thoughts and feelings, and can instead choose to do what is kind and positive for you and your body.

_Taking action_ means leaving your negative thoughts in the dust and following through on the act of kindness — using it to spiral upward to the next positive thing.

### 7. Have more fun by making the mundane meaningful and by remembering to laugh. 

Do you ever look around you and think, Why is everyone so serious? The author has not only had this thought; she's even gone so far as to suggest there's an ongoing _fun famine_, which is characterized by a general lack of silliness and too many people just going through the motions of mundanity.

If you feel like your life could benefit from making the mundane more meaningful, here are some tips for doing just that.

Let's start with grocery shopping. Everyone needs food and water to survive, but the routine business of getting these things can easily become tedious. But it doesn't have to be. After all, this is food we're talking about! So instead of always buying the same old boring stuff, why not turn shopping into a treasure hunt for the tastiest, freshest and most delicious things you can find?

There are all sorts of ways to get engaged and involved with shopping, such as making a point of buying at least one type of food that you've never tried before, searching out new places to buy your groceries from or hunting for the best bargains in town. When it comes to food, there are endless possibilities, so why not treat yourself to a little adventurousness?

Another way to keep your day-to-day life more meaningful is to always be on the lookout for opportunities to laugh.

You may think laughter is just an involuntary reaction to something humorous, but aside from being fun, it's also one of the healthiest things you can do! Indeed, the act of laughing exercises numerous muscles and it also produces pleasure-related hormones like dopamine and serotonin, while reducing stress-related hormones like cortisol and epinephrine. Ultimately, this adds up to laughter being excellent for your body by creating deep states of relaxation.

In order to benefit more from laughter, you can try to look at things in a more lighthearted way.

For example, let's say your morning pot of coffee tips over onto the breakfast table, and as you go to grab a towel, you find your child adding milk to the coffee by pouring it onto the table as well. One response to this might be anger, but it's also a pretty comical situation, and you might find that laughter can make cleaning up the mess a whole lot more enjoyable.

### 8. A personalized plan that addresses what you value is an effective way to lead a healthier life. 

Now that you have some ideas for how to be kinder to your body and bring more joy into your day-to-day life, let's see what it looks like when you bring these ideas together into a master plan.

With a good plan in mind, you can have a strong foundation to rely upon — one that can provide ways for you to practice body kindness in every aspect of your life.

A good way to start your own personalized plan is to create a _mind map_. All this requires is a sheet of paper and something to write with. Then, simply draw separate sections, such as large circles, to signify the different areas of your life. These often include things like work, exercise, nutrition and your social life. Now, in each of these sections, you can make some notes about improvements you'd like to make through the use of positive actions.

One of the author's clients, Sheryl, made a plan to help deal with different aspects of her life. These included an overwhelming amount of work, the loneliness she was experiencing after getting divorced and having her kids grow up and leave the house. These issues were causing Sheryl to come home from work feeling exhausted, and she would distract herself with fast food and binge-watching television.

To find an effective plan, the author worked with her client to identify what was most important to Sheryl. Since she valued her friends and her social life, they made a weekly meeting with one or more friends part of her master plan.

Not falling into the trap of being overworked and exhausted was also important, so part of the plan also became making sure she left work on time and got to bed by 9:00 p.m. with a book to help her fall asleep. Then, for an extra dose of energy and body kindness, they set a goal for Sheryl to exercise three times a week.

It's important to note that no part of this plan was unrealistic. It consisted entirely of relatively small, doable steps that added up to a big shift toward a healthier lifestyle, with Sheryl giving herself more kindness and a lot more joy.

### 9. Self-compassion is essential to body kindness, so speak to yourself as you would to a good friend. 

You've probably heard the saying "no pain, no gain," which suggests that self-improvement requires fighting and pushing your way through discomfort. This can lead to people thinking it's okay to be hard on themselves and to regularly dole out self-punishment, but it doesn't have to be that way.

Being harsh with yourself is far more likely to bring about exhaustion and cause you to give up rather than persevere. And that's why practicing regular self-compassion is a far better method for success.

Everyone has self-critical thoughts, whether it's comparing themselves unfavorably to others or wondering if they're doomed to always make the same mistakes. Self-compassion, on the other hand, is about taking the time to speak kindly to yourself. You can do this by making sure you acknowledge your achievements, instead of only dwelling on your mistakes, and by appreciating your strengths, rather than dwelling on your weaknesses.

For example, let's say you're out of work and looking for a job. If another day goes by without an interview, you might feel like beating yourself up and calling yourself useless. But rather than giving in to that impulse, you can be kind and truthful by reminding yourself that searching for a job is a difficult task, no matter who you are.

In fact, you can think of self-compassion as a way of looking after yourself and making sure you're taken care of — especially when times are tough. And when you do this, you'll find that you'll experience fewer symptoms of stress, anxiety and depression, while feeling more optimistic — all the key benefits of body kindness!

If you've been out of practice with self-compassion, you may not remember what this voice even sounds like. To get back in touch with it, just talk to yourself as you would to your best friend.

Ask yourself, if your best friend was struggling to find a job or having difficulty managing her career while raising her kids, how would you speak to her? What tone of voice would you use? What words of wisdom would you offer? Would you call her lazy and be judgmental? Or would you remind her that she's always shown impressive strength and perseverance?

If she's still your friend, then you probably spoke kindly and offered support, and that's exactly the kind of tone and spirit that you should offer yourself, especially when times are tough. Rather than kicking yourself when you're down, remind yourself that you've rallied your strength to overcome hardships before and that you can do it again. No matter what situation you find yourself in, you deserve to support and admire yourself for the amazing job you've done to get this far.

With a solid master plan and a renewed commitment to self-compassion, you'll be on your way to reaping the rewards of a more meaningful and joyous life.

### 10. Final summary 

The key message in these blinks:

**Body kindness is a compassionate approach to health and fitness that replaces stressful, strict, short-lived diets. By creating a personal plan tied to your unique values and circumstances, you can find ways to work less, be more well-rested, eat the foods you love and experience more joy in life. By reducing the power of negative thoughts, learning to laugh more and practicing more self-compassion, you can use the tools of body kindness to take conscious control over your happiness.**

Actionable advice:

**Rediscover which activities you feel most passionate about.**

If you're feeling a bit adrift in life, here's a quick exercise that can help you re-familiarize yourself with your true passions in life.

Imagine if you had one day to do whatever you wanted in life — no limits! Now, start brainstorming ideas and create a list of everything you come up with.

Then, read over the list and put a star next to the tasks that get you really excited and energized. These are probably the kind of activities you need to spend more time on. Get cracking on that list, and think about how you can add more joy and excitement into your life by simply doing the things you love to do!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Body Is Not an Apology_** **, by Sonya Renee Taylor**

The previous blinks had some good tips on bringing more self-love into your life, along with ways of reducing guilt around food and exercise. If you'd like to follow that up with more ideas on self-image and how to reduce those negative feelings, then we recommend the blinks to _The Body Is Not an Apology._

Here, you'll find more advice on how to practice what author Sonya Renee Taylor calls "radical self-love" and get to the root of where feelings of guilt and body shame come from, as well as how we can gain a healthier relationship with ourselves.
---

### Rebecca Scritchfield

Rebecca Scritchfield is an experienced fitness counselor, health counselor and nutritionist. She developed the _body kindness_ approach to health and well-being, which forgoes the usual diet restrictions and focuses on personalized self-care instead. She has appeared on CNN and the _Today Show_, and she has contributed articles to _The_ _Washington Post_, _Oprah Magazine_ and many other publications.

