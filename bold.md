---
id: 55127db1336565000a000000
slug: bold-en
published_date: 2015-03-26T00:00:00.000+00:00
author: Peter H. Diamandis and Steven Kotler
title: Bold
subtitle: How to Go Big, Create Wealth and Impact the World
main_color: E27C3D
text_color: 965329
---

# Bold

_How to Go Big, Create Wealth and Impact the World_

**Peter H. Diamandis and Steven Kotler**

_Bold_ (2015) is a guide to creating wealth by using today's most impactful, cutting-edge tools: exponential technologies. Using real-life examples and step-by-step guides, the blinks explore how to transform start-up concepts into billion-dollar companies.

---
### 1. What’s in it for me? Discover the power of exponential growth and how to tap into it. 

A fundamental question that all companies face is how to grow their business, and grow it fast. So what's the trick?

You've got to be _bold_. A bold company is a company that has unlocked the power of exponential growth.

These blinks explain how new technology enables new kinds of exponential growth. The emergence of a global online community has opened up possibilities for growth that, a mere 20 years ago, was the stuff of dreams. Crowdsourcing, crowdfunding, and other ways of tapping into this community's power can rocket a startup from the doldrums to dominion in no time at all.

In these blinks you'll learn

  * how Clarence Johnson designed the United States's first military jet in only 143 days;

  * how to get other people to pay for building a swimming pool; and

  * how to make your plants tell you what they need.

### 2. The exponential growth of technology is democratizing the power to change the world. 

Some 66 million years ago an asteroid wiped out all the dinosaurs, leaving the ruling of the world to a new group of animals — our furry little mammalian ancestors. Currently, another monumental event is rocking our modern world: the rise of _exponential technology_.

And like that fateful asteroid, exponential technology will wipe out today's dinosaurs, i.e., all lumbering, innovation-resistant companies.

Our understanding of exponential technological growth was first formulated by Gordon Moore, founder of Intel, who noticed that the number of transistors on integrated circuits consistently doubled about every 12 to 24 months. This trend of exponential growth is called _Moore's law_.

To understand how Moore's law affects our world, consider that our smartphones are a _million_ times cheaper and a _thousand_ times more powerful than a supercomputer from 1970.

When a medium or technology is digitalized — like when photography changed from images on physical film to images stored as ones and zeros — its growth rate becomes exponential. After digitization, photography achieved such profuseness and popularity that companies were soon willing to fork out huge amounts of money to get in on the action. Facebook, for example, bought Instagram for a cool $1 billion.

Exponential technology, in addition to bringing us neat gadgets like smartphones, is democratizing the power to change the world.

3D printing, for example, is furthering the democratization of manufacturing. Thanks to 3D printing, manufacturing a new car design no longer requires the building of an entirely new factory. With the right resources, almost anyone can do it. Today, most cars have 3D printed parts, and an entire car can now be printed on one site in one day.

On a smaller scale, some experts think that 3D printers will be a sound investment for individual households. A 3D printer can quickly manufacture enough products to pay for itself, and if we each owned one, it would irreversibly change the world of manufacturing.

### 3. Exponential technologies offer the world’s biggest business opportunities. 

These days, getting ahead as an entrepreneur means knowing which exponential technologies are on the verge of wide availability.

One such technology, _Artificial Intelligence_ (AI), will soon be ubiquitous in our daily lives. And some of these machines are already well on their way to near-human sophistication.

For example, Ray Kurzweil, the co-founder of Singularity University, is developing AI with natural-language understanding. In other words, he's teaching computers to understand subtle nuances in both spoken and written languages. His success would have profound consequences.

If we could ask complex questions of our machines, whose responses would be formulated based not only on logical intelligence but on emotional intelligence as well, AI's ability to understand the world might equal or even surpass that of humans. In fact, Kurzweil estimates that machines will be outsmarting us by 2029.

Another exponential technology verging on ubiquity is _sensor technology_. A sensor is any device that detects information, e.g., temperature or vibration. When connected to a network, these sensors can then transmit this information to us.

As the price of sensors decreases, the only limit to how we might put them to use is our imagination.

And with each technological advance, the number of sensors increases. To get an idea of how many sensors there are, imagine taking all the world's iPads and iPhones and laying their sensors out on the ground. In 2012, the total area covered by these sensors was equal to that of 2,000 football fields; in 2015, it will be three times larger. In other words, there will be enough sensors to cover half of Manhattan.

With the growing availability of sensors comes new opportunities to innovate. On a small scale, for example, there are now kits that enable your houseplants to "tweet" when they need to be watered, and beer mugs that can tell you how much you've had to drink during Oktoberfest. The possibilities are endless.

### 4. Skunk methodology helps entrepreneurs finish cutting-edge projects in record time. 

In 1944, German fighter jets ranged the skies above Europe, and the US lacked the technology to help the Allies fight back.

Then came Skunk Works, a small cadre of engineers and mechanics with freedom to design military jets however they saw fit. It was a resounding success.

In a mere 143 days, Skunk Works presented the United States with its first military jet; they were seven days ahead of schedule. Normally, contractors can't even get their paperwork signed in that amount of time!

The team went on to build some of the world's most famous aircraft, including the U-2 and Nighthawk stealth fighters.

These developments helped the United States maintain their position of power during the Cold War and inspired many companies that wanted to be innovative and bold to create their own "skunk works." Steve Jobs, for instance, built the first Macintosh by leasing a small building and filling it with 20 brilliant designers.

_Skunk methodology_ facilitates bold, exponential entrepreneurship by setting a clear goal, isolating the team from corporate bureaucracy and then supporting a state of _flow_, i.e., that state of consciousness in which people feel, and thus perform at, their best: a state of complete absorption in the work at hand.

The man who devised Skunk Works, Clarence "Kelly" Johnson, worked by a set of rules that gave his team a clear goal (like saving the world from the perils of Nazism). He also provided them with the isolation they needed to work well. Unbound by corporate convention, his team experimented unrestrainedly, and achievements, failures and insights rapidly succeeded each other.

The ability to quickly develop good ideas is exactly what skunk methodology is all about, and entrepreneurs hoping to use it need to give themselves the freedom to completely engross themselves in their work; doing so will help them enter a state of flow, which will enhance their concentration and drive them to push the limits of innovation.

### 5. To go big with your idea, you’ll have to win people over with “super-credibility.” 

To pull off any kind of grand, bold idea, you're going to need support. You'll need to surround yourself with many people who don't just believe in your idea, but find it so exciting that they want to become involved.

And to win people over, your idea will need _super-credibility_.

To get an idea of what this takes, consider that the author and his partner Eric Anderson devoted three years to developing the credibility of their company, Planetary Resources, which specializes in asteroid mining. They kept the company a secret during this time and did their utmost to give their idea credibility.

One way they did this was by gathering a group of billionaire investors willing to put their money and names behind the project. By the time the company launched, these super-smart businessmen, with the help of some highly qualified space engineers, had made the project not only credible, but super-credible.

But it doesn't necessarily take billionaires and brainiacs to bestow super-credibility upon an idea or project. These three simple steps can get you there: start with those who've already seen you succeed, slow down and stage your idea.

Back when the author was in college and without investment connections, he still managed to turn his bold dream — opening the space frontier — into a reality.

He was frustrated that NASA was such an insular agency, only offering military-industrial jobs. So, he founded Students for the Exploration and Development of Space (SEDS).

His success with SEDS gave him enough credibility for the next step: slowing down enough to take the time to organize a conference where the idea of creating an International Space University (ISU) could be discussed.

What finally gave the project super-credibility and made ISU a reality was the way in which information was presented. At the conference, a detailed plan of what ISU would actually look like was laid out by the much more experienced (and credible) advisers to the project. It's crucial that you do your research and present it well.

### 6. Exponential entrepreneurs need to be passionate and optimistic. 

If you want to learn about the mindset required to build world-changing, multibillion-dollar companies, it's necessary to look at successes from the past. By turning to some of the most remarkable entrepreneurs of our time, we can learn what kinds of psychological strategies exponential entrepreneurship requires.

Take Elon Musk, for example, the serial entrepreneur who reinvented banking and built four multibillion-dollar companies — PayPal, the automotive company Tesla Motors, the aerospace company SpaceX and the power generation company SolarCity. He is relentless in pursuing long-term goals and always approaches new projects with enthusiasm.

Musk considers the crucial secrets to his success to be his passion, his purpose and his optimistic approach to his own creativity.

After studying business and physics in college, he failed to land a job at Netscape, which he considered to be the only interesting internet company around. But instead of letting this get him down, he simply began to learn coding himself and started Zip2, an application that allowed companies to post content online, such as directory listings, maps, and so on. This start-up was so successful that Compaq eventually bought it for $307 million.

Another example of the do-it-yourself optimist is Sir Richard Branson. A notorious rebel known for his boldness, Branson maintained an indomitable attitude that led to countless adventures. He founded both Virgin Music, which he turned into one of the biggest record companies in the world, and Virgin Airlines.

Branson is driven by his pursuit of fun, and this has become his driving principle when starting businesses. Maybe that explains why he's started some _400_ new business ventures!

His rationale is that, if something is fun for him — like a quality airline that makes people say "wow" — then it's probably going to be fun for everyone else.

### 7. Crowdsourcing lets the online crowd do your work for you. 

Making your fledgling start-up grow exponentially can be a difficult endeavor. Luckily, there are some great tools available to you. New communication technology, for example, is connecting people all over the globe and opening up a world of possibilities.

Another one of these tools is _crowdsourcing_, i.e., taking a function once performed by employees and, after making an open call for assistance, outsourcing it to a large and often anonymous network of people.

These networks began to take shape when unemployed designers, looking for things to do, started hanging around on the internet. In fact, these cyber job-hunters were the genesis of crowdsourcing.

In 2000, two 19-year-olds realized they could tap into this crowd of talent by hosting weekly T-shirt design competitions. They'd award the winner $100 and then sell shirts bearing the winning design on their site. Within a few years their venture, Threadless.com, was earning more than $20 million per year.

Crowdsourcing has since diversified into all kinds of commercial applications, from 99designs, which allows users to submit a design need — for instance, a new logo — and then provides a platform for the crowd to compete for business, to Gengo.com, which offers crowdsourced human translators.

For only a few thousand dollars, crowdsourcing can help an entrepreneur turn an idea into a multimillion-dollar company — and all from the comfort of the entrepreneur's home.

Consider the entrepreneur Simon Clausen, who built an antivirus company called PC Tools by crowdsourcing the first iteration of his antivirus app. An Indian company charged a thousand dollars to make the program, and he certainly made his money back; before he sold it, his company reached a valuation of $100 million.

Today, platforms for crowdsourced work are myriad. For example, if you need any small task completed, such as turning your hand-drawn sketches into digital art, check out the online marketplace Fiverr. For larger "macrotasks" that require specific skillsets and can't be broken up, you might try your luck with Freelancer.com.

### 8. Crowdfunding is one of the greatest capital-raising tools available to today’s entrepreneurs. 

Until recently, many of the world's big challenges were off-limits to the average person, since tackling these issues required a large amount of capital. Today, however, you don't need to be wealthy to be successful as an entrepreneur. You just need to know how to use the tools available to you.

_Crowdfunding_ is one such exponential growth tool that lets anyone tap the resources of potential supporters and customers.

Crowdfunding platforms emerged in the mid-2000s, and were used primarily by musicians and filmmakers who were looking for ways to fund their creative projects without relying on a major label or studio that might compromise their creative visions.

In essence, crowdfunding entrepreneurs simply ask potential investors in the crowd for financial assistance. This can come in many forms: donations or loans (to be repaid with interest) or money (perhaps in return for stock or some other kind of incentive).

With more than 700 crowdfunding sites on the web today, it has become a significant economic engine for practically every kind of project.

Even nontraditional projects, such as +Pool, are earning huge amounts of money via crowdsourcing: +Pool raised more than $270,000 on the crowdfunding platform Kickstarter. Their project? To build a swimming pool that floats in the middle of the East River in New York.

Crowdfunding is especially useful for entrepreneurs who are starting community-focused products and services. Having the community in mind allows them to fast-track projects with nothing more than a simple prototype to show off to potential investors.

A common thread running through most crowdfunding projects is that the product or service aims to solve a problem or to tell a story relevant to a larger community. If you do one of those two things, you'll be able to get funding for almost anything, from films and art galleries to up-and-coming NGOs and soon-to-be developed pieces of useful hardware.

### 9. In order to tackle bold challenges, you need to build a committed and capable community. 

The crowd isn't the same as a community. The crowd represents every potential customer or supporter out there, whereas your community consists of those you pull _from_ the crowd and with whom you develop a working relationship.

With the help of a committed and capable community, you can tackle challenges that would be impossible to tackle alone.

Take Kevin Schawinski, for example, a PhD in astrophysics who was identifying galaxies by searching through the data of Sloan Digital Sky Survey, a project aimed at exploring and mapping the universe. In order to aid him in his project, he developed an online community of astronomers, called Galaxy Zoo, which has since gathered 250,000 interested people wanting to contribute to real science by going to the website and classifying galaxies.

TopCoder, a company that hosts development contests, is another example. They post problems on their site, and then eager, competitive coders compete for prizes by submitting the most accurate solution in the least amount of time. The TopCoder community doesn't solve trivial problems, either. In fact, they've solved problems for some impressively sizable companies, such as Best Buy.

But how do you _build_ a community? To do this, you'll have to play the role of a _conversational caretaker_.

Communities should reinforce people's sense of identity by inspiring their participation in a meaningful conversation. As a conversational caretaker, your responsibility is to facilitate that conversation — a conversation about exactly what your community is going to look like.

Building a community means identifying your mission, being clear about what you stand for and then making sure that your community's members are engaged in its mission. But you also have to make sure they're actually engaging in the conversation.

Rather than having a large group of mostly inactive lurkers, you should handpick the early members of your community and engage these people in the community-building process, asking for their input and feedback in order to create the most meaningful opportunities to participate in the conversation.

### 10. Final summary 

The key message in this book:

**Success in entrepreneurship requires you to have an eye for the future. Today, that means appreciating exponential technologies and their implications for individuals, businesses and societies.**

**Suggested** **further** **reading:** ** _Abundance_** **by Peter H. Diamandis and Steven Kotler**

_Abundance_ explains why our current predicament is not as gloomy as we believe it to be and presents a compelling case for ways in which we could have a future marked by abundance and not scarcity. It takes readers on a whistle-stop tour of transformative technologies, their key players and a glimpse of how these technologies could be employed to solve many of the resource problems we face today. Above all, this book reminds readers that it's an interesting and exciting time to exist.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Peter H. Diamandis and Steven Kotler

Peter H. Diamandis, founder of the X Prize Foundation and co-founder of Singularity University, is an engineer, entrepreneur, physician and space enthusiast. He also co-authored the bestselling book _Abundance_, with Steven Kotler.

Steven Kotler is a journalist, entrepreneur and author, who, in addition to co-authoring _Abundance_, has published articles in _The New York Times Magazine_ and _Wired_.

