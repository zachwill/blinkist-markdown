---
id: 546be0543435300008900000
slug: bonk-en
published_date: 2014-11-21T00:00:00.000+00:00
author: Mary Roach
title: Bonk
subtitle: The Curious Coupling of Science and Sex
main_color: None
text_color: None
---

# Bonk

_The Curious Coupling of Science and Sex_

**Mary Roach**

_Bonk_ offers a humorous look at how scientific researchers go about investigating one of the most important yet taboo topics of all: sex. Describing everything from the history of sex research to the many contraptions scientists have developed to give men's penises a lift, _Bonk_ is as informative as it is hilarious.

---
### 1. What’s in it for me? Learn all the ins and outs of sexual intercourse. 

Could you imagine having sex in a lab knowing that scientists were observing you and your partner from behind the mirrored glass? What if an investigative journalist was right there with the scientists, taking notes for her soon-to-be bestseller?

As you'll soon discover, sex research isn't easy, and it's not any easier for journalists to investigate it. But Mary Roach came up with a solution: to participate in the research herself and in doing so gain access to the firsthand experience of investigations into sexuality. And thus _Bonk_ was born.

These blinks will not only give you an insight into the fascinating history of sex research, they'll also show you how sexual gratification can solve everyday problems — from curing a bad case of the hiccups to extending your lifespan.

In these blinks, you'll discover

  * why Viagra doesn't work for women;

  * which unconventional operations have helped over 250,000 men achieve an erection; and

  * how even spinal cord injuries can't stop people from achieving orgasm.

### 2. It’s very difficult to get funding for sex research. 

Considering how valuable sex is to our culture and even to our very existence, it's surprising how difficult it is to study. Sex happens every day all over the world and yet the scientific community seems reluctant to dive into the topic. But why?

The fundamental problem is that it's hard to procure funding for research projects that don't lead directly to the development of new drugs. While a vast amount of studies search for pharmaceutical solutions to increase women's sexual desire, it's difficult to get funding for the research to answer questions regarding the health benefits of masturbation.

Take one University of Chicago study that aimed to evaluate the therapeutic value of a vibrator called _Eros_. Roach asked one of the researchers why they couldn't conduct their research by simply giving women the vibrator and letting them masturbate, to which the researcher quipped, "Can you imagine if I tried to get funding for a study that had masturbation in the title?"

Researchers who want funding to investigate these kinds of intimate questions have to get funding from the sales of sex toys themselves, such as _Vibrating Port-A-Pussies_ and _Mr. Fred Jelly Dongs_.

Compounding this problem is the fact that the gatekeepers of family values hinder research efforts on taboo topics. We see this, for example, in the way politicians make funding decisions based on pressure from religious leaders.

As we've seen, it's quite difficult to scientifically examine masturbation, at least formally. However, it's even more difficult for journalists to gain access to the labs in which women are masturbating. To get around this, Roach herself became a test subject. By persuading her husband Ed Rachels to participate with her in a study in which sexual intercourse was measured with ultrasound, she was able to get a first-hand look into the science of sex.

> _"Masturbation is a touchy area."_

### 3. Scientists have always wanted to learn more about sex, despite the taboos. 

The history of science has revealed time and time again that censure cannot stop the passion of discovery. The same applies to sex research.

The 1920s, similar to the 1960s, were a racy decade in terms of sex research. At the center of it all stood the female orgasm, as it was commonly associated with health and happy marriages. In fact, many writers even provided tips on how to achieve it.

Dutch gynecologist Theodor Van de Velde became famous for authoring the manual _Ideal Marriage: Its Physiology and Technique_. The book, which went through more than 40 editions, was full of practical advice and had 25 pages devoted to sex positions.

During this period, the first laboratory-based experiments for the physiological examination of individuals engaged in sexual intercourse were established by the gynecologist Robert Latou Dickinson. In his investigations, he tried to develop experimental settings to find out via electric stimulation, for example, where the tip of the penis made contact during sex.

Although the 1920s were followed by a period of conservatism, Dickinson's work did not disappear from the history of science. In fact, his work inspired Alfred Kinsey, who published the results of his surveys of thousands of Americans from the forties and fifties concerning their sex lives in two best-selling volumes.

Kinsey was so dedicated to sex research that he even made secret recordings of 30 of sexual encounters on a mattress in his attic, which were later dubbed the "attic tapes."

William Masters and Virginia Johnson went even further, building an "artificial-coition machine" equipped with penises made from plastic and outfitted with a camera that allowed for easy observation and recording.

By varying the machine's thrusting frequency, the length of the artificial penises and other parameters, they could thus systematically study the nature of female orgasms under reproducible circumstances.

Our next blinks will look at sex research as it relates specifically to men.

> _"[Sex researchers'] lives are not easy. But their cocktail parties are the best."_

### 4. Blood must remain in the penis in order for it to stay erect. 

Many men suffer from _impotence_, i.e., the inability to achieve or maintain an erection. But let's not forget the other men and women who also suffer: their sexual partners! Given how widespread and frustrating the problem is, it's not surprising that many different methods to help men achieve and maintain erections have been developed over the years.

One of the most passionate researchers in erectile science is urological surgeon Dr. Geng-Long Hsu, who runs the Microsurgical Potency Reconstruction and Research Center located in Taipei. He is convinced that his surgical procedure, which ties off and removes some of the veins of the penis, can help up to 90 percent of men with erectile dysfunction.

Here's a bit of background information on his rationale: in the Middle Ages, people thought that erect penises were filled with pressurized air. But thanks to Leonardo da Vinci's anatomic studies, this erroneous belief could finally be put to rest.

After cutting open the corpses of hanged criminals, da Vinci discovered that erect penises weren't full of pressurized air at all: they were full of blood.

However, pumping blood into the penis isn't sufficient to achieve a full-blown erection. In order to maintain an erection, blood has to stay in the penis. And since blood arrives to the penis through the arteries and leaves through the veins, fewer veins means less outflow and longer-lasting erections. This is where Dr. Hsu's surgical operation comes into play.

Unfortunately, however, statistics have shown that the effects of such vein-removing operations only lead to short-term successes, since the body will simply grow new, larger veins in response. For this reason, most researchers — apart from Dr. Hsu — no longer promote such operations.

> _"Blood is the backbone of a stiff penis."_

### 5. Drug-based treatment only works if the penile tissue still functions. 

As it turns out, removing veins from the penis isn't the only way to treat erectile dysfunction. Dr. Hsu's approach attempts to sustain erections by trapping the blood there. However, there is another way to enhance erections by regulating blood flow.

Rather than _inhibiting_ the blood that flows out of the penis, it's possible to _increase_ the blood flow into the penis. That's the goal of the ultra-popular drug _Viagra_, the blue pill that became well known due to its massive publicity campaign in 1998.

The idea is simple: inside the penis are two cylindrical chambers, each filled with smooth-muscle erectile tissue. When this tissue is relaxed, it can expand, thus filling with blood and causing an erection. This relaxation is triggered by an enzyme that is activated when the brain perceives sexual stimuli.

Drugs like Viagra work by curbing the effects of an enzyme called PDE5, which inhibits the relaxation of the smooth-muscle tissue.

However, drug-based approaches for treating erectile dysfunction depend on the functional integrity of the patient's smooth-muscle tissue. So what are people to do in those cases in which the tissue itself is damaged?

In these cases, patients can opt for another surgical approach: penile implants.

There are two different types of implants: malleables and inflatables. With malleables, the penis must be bent into an erect position, and with inflatables, the penis has to be pumped up manually.

In spite of their somewhat cumbersome nature, over 250,000 penile implant operations have been carried out worldwide. These implants not only allow patients to achieve an erection but also ejaculation and orgasm.

### 6. A spinal cord injury won’t necessarily stop you from achieving orgasms. 

As is often the case in medical science, insights into the nature of orgasms are usually the result of coincidence. For example, in 1948, doctors tried to procure the sperm of paralyzed veterans via electric stimulation in order to impregnate their wives.

As it turns out, these _electroejaculations_ had unforeseen beneficial side-effects: several hours after the stimulations, paralyzed patients experienced a reduction in their leg spasms!

But how is it possible for paraplegics who can't feel anything "down there" to become aroused and even have orgasms?

For a long time, the general assumption was that people whose spinal cord injuries had occurred higher than the point at which the nerves from the genitals enter the spine couldn't have orgasms since the nerve impulses from the genitals wouldn't be able to reach the brain.

However, according to large surveys, 40 to 50 percent of men and women with such spinal injuries do, in fact, experience orgasms. But how?

The only thing that definitively excludes the possibility of orgasm is when the sacral nerve roots at the base of the spine are completely destroyed. That's because the sacral reflex arc belongs to something called the _autonomic nervous system_, i.e., nerve systems that control the function of our internal organs and cannot be controlled voluntarily.

Luckily, when the spinal cord is injured, the pathways of the autonomous nervous system often remain undamaged. In most cases, only the pathways responsible for voluntary muscle control — e.g., the movement of our legs — become blocked.

Therefore, unless their spine has been damaged in a very specific way, paraplegics can still achieve orgasms even if they can't feel or control their lower bodies!

Moving away from male erections, the following blinks will take a look at interesting aspects of women's sexuality.

### 7. Despite the structural similarities between the penis and the clitoris, Viagra doesn’t work for women. 

While men's sexual troubles — as far as they can be treated by drugs — have been more or less vanquished with the advent of medications like Viagra, a huge amount of money is now going toward the study of widespread female sexual disorders.

However, as you will soon see, the issues the pharmaceutical industry faces seem to be much more complicated regarding women's sexuality.

After the major success of Viagra, pharmaceutical group _Pfizer_ turned its focus on women. The idea that Viagra could solve the problem of women's _low libido_, i.e., low sex drive, didn't seem at all unrealistic due to the fact that men's penises and women's clitorises share important anatomical features.

In the womb, both male and female fetuses have genitals that are closer to a clitoris than to a penis. Over time, the male "clitoris" develops into a penis, while those of developing females stay more or less the same.

What's more, both the penis and the clitoris enlarge during arousal. In fact, aroused clitorises can double in size! The mechanism at work here is essentially the same issue of blood flow as in the case of penile erections.

And since a man's sexual arousal and his erection are inextricably linked, Pfizer therefore concluded that women's low libidos could likewise be managed by increasing the blood flow into the clitoris with drugs like Viagra.

The fundamental assumption that the blood flow to the clitoris would be increased turned out to be correct. But, surprisingly, after taking Viagra, women didn't really notice the same changes in their arousal. Roach states that, much more than men's sexual arousal, that of women lies not only in the physiological but also in the psychological.

### 8. A woman’s libido is largely driven by certain hormones such as testosterone. 

In the previous blink, we learned that women's sexual arousal is much more influenced by psychological factors than men's. In fact, there's even one case of a woman who can achieve orgasm by just thinking — without using her hands!

Today, it's a well established fact that hormones influence women's libidos. A study by M.A. Bellis and R.R. Baker demonstrated that women are more likely to have sex with their _lovers_ on the day of maximum fertility — that is, at the peak of their hormone levels — while sex with their _husbands_ is randomly distributed throughout the month.

When they hit their hormonal peak, women also tend to more often and more easily engage in socially risky sex than they otherwise would. In fact, their high hormone levels cause them to be perceived as more attractive than they would appear with a lower hormonal level.

Testosterone accounts for the largest increase in women's libidos. Unfortunately, contraceptive pills cause an increase in a certain protein that binds to testosterone, thereby taking women's testosterone out of commission and lowering her libido.

Plus, tinkering with hormones can have long-lasting consequences. One study found that, even when women stop taking hormonal contraceptive pills, their testosterone levels won't recover.

While such a study might be sobering, it doesn't necessarily mean the end of arousal for women on the pill. That's because arousal doesn't necessarily have to be achieved _before_ sex.

On the search for great sex, researchers William Masters and Virginia Johnson found that homosexual couples did it best. The explanation was simple: they took their time and didn't look for the most direct ways to orgasm (as do many heterosexual couples). Thus, they became more and more aroused over the course of having sex; in other words, they produced arousal through the act of sex.

Well, we made it this far without undue focus what many consider the most important aspect of sexual intercourse: the orgasm. Our final blink will examine the medical benefits of sexual climax.

> _"Hormones are nature's three bottles of beer."_

### 9. Self-stimulation doesn’t just feel good – it’s actually good for you. 

How astonished would you be if your doctor told you that you should masturbate more often? It's precisely this astonishment, this taboo attached to masturbation, that might be the reason why he wouldn't consider it. However, as you will see, orgasms, and thus masturbation, are highly beneficial to our health.

Consider one unfortunate Israeli man who in 1999 couldn't stop hiccuping for four entire days. He tried everything from household remedies to drugs, but nothing worked. He couldn't fall asleep or concentrate on his work. Finally, on the fourth day, he had sex with his wife. And although the hiccups continued all the way through the act, they finally stopped after he ejaculated.

While such stories are a amusing, they are also scientifically important — so much so that this particular case was published in a report entitled "Sexual Intercourse as a Potential Treatment for Intractable Hiccups." The authors of the report suggest that unattached hiccupers might try masturbation as a viable remedy!

Moreover, as two Rutgers University researchers Barry Komisaruk and Beverly Whipple have concluded, orgasms can have many other health-related benefits than simply curing a bad case of the hiccups. They assert that "people who have regular orgasms seem to have less stress and enjoy lower rates of heart disease, breast cancer, prostate cancer, and endometriosis."

In fact, orgasms seem to lead to a longer life in general. According to a study by G. Davey Smith, men who had two or more orgasms per week over a span of ten years were 50 percent less likely to die than those who had orgasms less than once a month.

The same holds for Catholic priests living lives of celibacy. Compared to non-celibate protestant pastors, the risk of an early death is higher among the celibate Catholics.

Indeed, psychologist Dorvey Butt puts it best when he says that the orgasm is "the most basic form of physical exercise."

### 10. Final summary 

The key message in this book:

**The scientific knowledge about sex we have today was gained by courageous researchers who had (and have) to face disrespect and stubborn resistance. Despite the taboos surrounding sex and masturbation, the orgasm plays a critical role in both our happiness and our health.**

**Suggested** **further** **reading:** ** _Sex_** **_at Dawn_** **by Christopher** **Ryan** **and** **Cacilda** **Jethá**

_Sex_ _At_ _Dawn_ argues that the idealization of monogamy in Western societies is essentially incompatible with human nature. The book makes a compelling case for our innately promiscuous nature by exploring the history and evolution of human sexuality, with a strong focus on our primate ancestors and the invention of agriculture. Arguing that our distorted view of sexuality ruins our health and keeps us from being happy, _Sex_ _At_ _Dawn_ explains how returning to a more casual approach to sex could benefit interpersonal relationships and societies in general.
---

### Mary Roach

Mary Roach is the author of several critically acclaimed books such as _Stiff: The Curious Life of Human Cadavers_ (also available in blinks) and _Spook: Science Tackles the Afterlife_. In addition, her writings have appeared in numerous publications such as _Outside_ , _Wired_ , _National Geographic_ and the _New York Times Magazine_.

