---
id: 5ac3680fb238e10007c07d5a
slug: bored-and-brilliant-en
published_date: 2018-04-06T00:00:00.000+00:00
author: Manoush Zomorodi
title: Bored and Brilliant
subtitle: How Spacing Out Can Unlock Your Most Productive and Creative Self
main_color: 3BC8E4
text_color: 206E7D
---

# Bored and Brilliant

_How Spacing Out Can Unlock Your Most Productive and Creative Self_

**Manoush Zomorodi**

_Bored and Brilliant_ (2017) posits that the constant distractions of modern life — from smartphones to advertisements to email — are depriving us of a crucial resource: boredom. When we're constantly busy or being entertained, we have no time to process information or let our minds wander. This not only makes life more stressful; it harms our creativity, making it harder for us to come up with brilliant ideas.

---
### 1. What’s in it for me? Unplug and embrace the benefits of boredom. 

When was the last time you came up with a great idea? Did it hit you when you were reading Twitter or group texting some friends? Or maybe that genius thought struck you during a two-hour session of YouTube binge-watching?

No? Well, if you, like the author, realized that the last time you had a brilliant thought was the last time you were bored — whether you were taking a long, aimless walk or an extended shower — you're not alone.

In fact, our brains need boredom in order to get truly creative. So how can you unplug from all the digital distractions and benefit from that rare human emotion of boredom?

These blinks show you the way. They explain how boredom fuels creativity and give you some pointers for improving your digital habits. Finally, you'll be given a challenge that'll help you unplug entirely — at least for a little while.

In these blinks, you'll also learn

  * that mobile games aren't just a waste of time;

  * how to improve your memory; and

  * the right way to take a fakeation.

### 2. Our avoidance of boredom might be hurting our creativity. 

It's Sunday afternoon and you've got nothing to do. Truly nothing — the internet is down, you're all alone, your phone is dead and there aren't any books. You get the picture.

Does this sound like the most mind-numbingly boring and awful thing ever?

To give you a sense of the level of dislike we have of boredom, consider a study conducted at the University of Virginia. In the experiment, participants were exposed to three types of stimuli: music, images and mild electric shocks. After some time, the participants were asked whether they'd be willing to pay to stop the shocks, and 75 percent of them said they would.

The participants were then given 15 minutes to think things over. They were left alone and given a button that, if pressed, would shock them. Here's the weird part: one-third of the participants who'd said they'd pay a fee to make the shocks stop decided to press the button in an attempt to keep the boredom at bay.

So we prefer the pain of minor electrocution to the agony of boredom. This is a shame, because boredom is a boon.

For starters, it can boost creativity.

Think back to the last argument you had. Did you manage to deliver quip after biting quip, utterly vanquishing your opponent? Or was it later, when you were alone, that you came up with those perfect comebacks?

When we're engaged in the moment, our _executive attention network_ kicks into action. Although this makes us more ready and alert, it inhibits and controls our attention, making it harder to cook up great ideas. It's only in moments of boredom that this network switches off, and our brains shift toward creative thinking.

You see, our brains don't just shut off when we're bored. They're still very active, using about 95 percent of the energy that an engaged brain requires. The difference is this: when bored, we become less focused and our mind starts drifting, shuffling through old memories and reflecting on the present and the future. We create unexpected connections, which lend themselves to creativity.

So boredom isn't all bad — but it sure is rare these days.

### 3. Technology offers great convenience – but this convenience comes at a price. 

Take a walk down the street, or spend a few minutes on the subway or a bus, and you'll see them: hordes of people with their heads lowered, each gazing at the screen of some device. Sure, smartphones and tablets are great — you're probably using one right now! — but they're also altering the way we think and act.

For one, we no longer read or process information as we used to.

When investigating why people were having difficulty getting immersed in books or other long texts, writer Mike Rosenwald made a discovery: the internet has altered the way we read.

Pre-internet, people read linearly. Today, our reading is utterly nonlinear — we scroll and skim and follow links to other stories. And all this skipping, skimming and scrolling is affecting our ability to engage with long stretches of text.

An ironic case in point is that only 30 percent of the people who began Rosenwald's article about his research managed to read it to the end.

But it's not only the internet that's at fault; screens are also reducing our reading comprehension.

Anne Mangen, a professor at the University of Stavanger in Norway, conducted a study that had some participants read a mystery story on an e-reader and others read the same story in a bound book. The emotional experience of reading was similar for all participants; however, those who read the story in book form were much better at answering questions about the chronology of events.

In a similar vein, digital photography, and our incessant picture-taking, is affecting our memory.

These days, people take dozens of pictures per day, attempting to capture and broadcast every moment. But it's been shown that the moments we try to keep this way actually become harder to remember.

In an experiment, Linda Henkel of Fairfield University had subjects tour a museum. They were told to take photos of some objects. Others, they were instructed to merely observe. The result? Participants had a much easier time remembering the details of objects they'd only observed and hadn't photographed.

### 4. Our attention is valuable, and businesses put a lot of effort and resources toward capturing it. 

What do drug dealers and people in the tech sector have in common? No, the answer isn't, "Too much money." It's that they both call their customers "users."

This is no coincidence. Many of today's devices and platforms are as addictive as any drug — and companies want it that way, for our attention is their most valuable asset.

Indeed, companies know how to exploit our instincts so that we remain riveted.

Have you ever heard of the _endowed progress effect_? Well, here's how it works:

We tend to favor tasks that we believe we're close to completing. The human brain has evolved to long for completion, and the endowed progress effect makes people feel that they're on the verge of it.

This is why LinkedIn, among other companies, displays a profile "completion" bar. By constantly reminding you that you're almost done, the site increases the likelihood of your sticking around and actually setting up a profile.

In the opinion of digital marketing expert Nir Eyal, such subtle manipulation is a kind of mind control, which can be as addicting as alcohol, cigarettes or any other drug. Eyal argues that between 2 and 5 percent of technology users become severely addicted; reason enough, he claims, to mark digital devices and products with a special label: potentially addictive.

But many modern companies do more than exploit our preexisting instincts and deep-seated habits; they seek to create completely new habits, too.

Have you ever thought about why mobile gaming companies limit the length of play or the number of lives you have? Well, we value scarce resources — so having little play time or limited chances to play makes us value the game. It also forces us to play again and again, which is habit-forming.

Companies have even found new ways to give comfort. People don't like uncertainty, which is why Uber, right after you request a ride, shows you precisely where your driver is.

The comfort derived from this experience makes users more likely to use the service in the future.

> _"The currency of information is attention."_

### 5. There are a range of benefits to unplugging. 

You've probably seen "no laptops" signs in cafes by now. There is, in fact, a fast-spreading no-tech trend, with people organizing technology-free retreats and some live music acts even forbidding the use of phones.

But what good can pocketing your phone really do?

Well, for one, disconnecting from technology can help you connect with others.

In a Virginia Tech study, participants were put into pairs and asked to talk with each other for ten minutes. Some people chose to put away their mobile device; others put it on the table or held it in their hand. And the presence or absence of these devices made a big difference. Empathy levels were reported to be higher when mobile devices were absent. Even if the discussion partners knew each other well, the presence of a device drove empathy below the level of device-free partners who were complete strangers.

Or consider Laura Norén, an NYU professor who decided to act on research from Princeton and UCLA that indicates that taking handwritten notes improves information retention. After noticing that students seemed less engaged when typing, word for word, what she said in class, she put a classroom ban on laptops. And this dramatically improved the students' ability and willingness to join in class discussions.

Taking a break from technology is similar to taking a break from work, which most companies now know is absolutely crucial.

BCG Consulting learned this the hard way. Yes, it was a top management-consulting firm. Yes, new employees were always committed and eager. But there was a problem. The work was intense, and consultants were on call every day, at all hours, and those initially chipper and excellent workers usually burned out in under five years.

So economist Leslie Perlow proposed a solution: select a midweek day on which consultants wouldn't be allowed to work or make any team communications. She got a team of consultants to take a free day, and, being utterly unused to having any time for themselves, they initially panicked.

But this solution proved remarkably effective. BCG's management backed it, and the consultants both recovered in their personal lives and improved their teamwork.

So taking a break from technology, or from work, certainly has its benefits. It's up to you to actually take that break.

### 6. Mobile games can be a help or a hindrance – it all depends on how they’re played. 

Most of us have done it. We install a game on our phone, thinking it might be a fun, occasional pastime, but before we know it, we're totally hooked, playing whenever there's a free minute. Whether it's Candy Crush, Clash of Clans or Two Dots, these games are often regarded as a waste of time.

But that's an oversimplification. Depending on how we play them, mobile games can actually be beneficial.

According to Jane McGonigal, an author and game developer, there are correct and incorrect ways to play games. It's best to play them in brief spurts, and to make meaningful connections between the game and reality. Doing this can, according to McGonigal, improve your mental state.

On the other hand, you shouldn't get so immersed in the game that reality falls away. If you do this, you may be using the game to escape your real-life problems, which, more often than not, only makes these problems worse.

So choose your games wisely. McGonigal believes that some games can reduce stress and anxiety, so if you struggle with these feelings, playing the right game occasionally for about ten minutes can have very beneficial effects.

It's equally important that we teach our children healthy gaming habits, rather than banning games entirely.

Consider computer teacher Joel Levin, who introduced his daughter to Minecraft when she was four years old. The game allows players to explore a randomly-generated world, and Levin watched as his daughter interacted with each new world.

It struck him that the game would be an excellent teaching aid. Indeed, with its help, his daughter had already learned to spell her first word. So Levin introduced his class to Minecraft. It was such a hit that he eventually developed an educational version with the game's creators.

Today, MinecraftEdu helps students in more than 7,000 classrooms, spread across 40 countries, learn math, history, social studies and much more.

When approached correctly, games have the potential to do plenty of good. So it's important that we teach, and model, healthy digital behavior — and that's what the next blink is all about.

### 7. Young people are particularly vulnerable to technology, and they must be taught to use it responsibly. 

Kids like excitement. And there's a lot of excitement available these days, from mobile games to flashy ads to YouTube videos. But the drawbacks of nonstop excitement and stimulation are myriad.

Modern-day technology has such a powerful effect on childhood development that many tech giants don't let their kids use any devices at all. Steve Jobs, for example, wouldn't let his kids use an iPad.

And there's a good reason for these strict policies.

Consider the research of Dr. Mary Helen Immordino-Yang, who studies the way youths interact with social media. Her findings show that children who frequently use social media tend to be both less empathic and less adept at creatively solving real-world problems.

It's also been shown that social media affects how young people form opinions.

In one study at UCLA, Lauren Sherman created a simulated Instagram feed and had individual participants from a group of youths rate the pictures. But here's the twist: Sherman would sometimes shuffle around the number of likes that each photo had. No matter how she shifted the pattern of likes, the photos with more likes were always the most popular with the youths.

So how can we counteract these negative side effects?

Well, one technique is to ban technology completely, which is what Waldorf schools attempt to do. Before seventh grade, students at Waldorf schools aren't exposed to technology at all. Oddly enough, this has made them particularly popular with Silicon Valley CEOs, who also tend to believe that technology hampers development.

However, there's another way to do things: give guidance and let children establish their own limits.

This method certainly worked at Longacre Leadership Camp in Pennsylvania, where a staff member named Matt Smith conducted a daring experiment. Instead of following the lead of most summer camps and banning technology, Smith decided to allow it after the first week of camp.

At first, technology use shot up — but after this initial surge, campers began enforcing their own limits.

And they not only monitored themselves; they monitored each other. Campers would be called out for overusing their headphones, for instance, or for texting outside of quiet time.

If we can guide them and set a good example, there's no reason our children can't form healthy relationships with technology.

### 8. Become a healthier and more mindful technology user with the Bored and Brilliant challenge. 

So how can you set a good example for others and form better technology habits?

Why not start by taking on the _Bored and Brilliant challenge_? It's a weeklong program that encourages better technology-usage habits, with a new mini challenge for each day of the week. Here's what the first four days look like:

Day one is about self-observation. Over the course of a normal day, take note of your digital habits. There are two apps that can help you do this: Moment, for iOS, and Space, for Android. Both will show you how many times you unlock your phone per day, and how much time you spend on the device. At this point, you just want to get an idea of your typical device usage, so there's no need to alter your behavior yet.

On day two, you should avoid using your device when you're on the move. Whether you're on a walk, on your commute or standing in an elevator, don't give in to the urge to take out your phone. And don't do it by halves — really don't use it at all. No podcasts, no music, nothing. Leave your phone in your pocket or purse and observe the world around you.

Day three is photo-free day, which is pretty straightforward: just don't take any photos at all. This will both help you occupy the moment and make it easier for you to remember your experiences later. If this is too easy, try not to share or like photos, either.

On day four, you should delete an app. You know the one — you overuse it, especially when you've got a down moment. And don't cheat — really delete it. And it has to _stay_ deleted. Once it's gone, you'll be free to enjoy all those moments that you used to waste!

Beginning the challenge is the hardest part. But once you've done that, you can begin the second part of the challenge, which is all about creating healthier digital habits.

### 9. Improve your digital habits during the last three days of the Bored and Brilliant challenge. 

Now that you've taken steps toward breaking your unhealthy digital habits, it's time to form some new, positive ones. Once you've established these habits, you'll be ready to tap into all your hidden reservoirs of creativity.

It's day five, which means it's time for you to take a _fakeation_ — that is, to designate a window of time during which you'll be completely unplugged from all digital devices and distractions. You should use this time either to focus on a project you've been meaning to complete or simply to enjoy a bit of notification-free peace and quiet.

Now determine how long this fake vacation needs to be, whether that's half an hour or the whole day.

Before unplugging, set up an auto-response message on your email, letting people know that you're otherwise occupied and can't respond immediately. Do this even if your break ends up being very short — and then do the same for the other channels through which people tend to contact you.

On the next day, day six, your challenge is pretty simple. All you've got to do is go to a public space and spend a period of time watching and listening (without any digital devices, of course). If you don't like the idea of lurking around one place, go take a long, aimless stroll.

Pay attention to the buildings, the people, the sounds and smells. This won't only calm your mind and excite your creativity; it may even open your eyes to something you'd have otherwise missed altogether.

Day seven's challenge brings together all the work you've put in so far.

On this day, you should pinpoint an issue in your life that makes you feel anxious or confused. Once you've done this, spend 30 minutes alone, with no distractions and no interruptions. Then get a sheet of paper and fill the page with ones and zeros. Make the numbers as small as possible, so that it takes a while.

Then, when all this mindless activity has made you terribly bored, you should grab a new sheet of paper and start writing possible solutions to the issue you identified earlier.

If all goes well, your creativity will have been sufficiently stimulated by all this boredom and you'll cook up a solution that you would never have thought of otherwise.

Don't think the Bored and Brilliant challenge will solve your problems overnight; it won't. However, it should provide you with the tools necessary for improving your digital habits and your relationships to your devices.

### 10. Final summary 

The key message in this book:

**Technology is incredibly helpful, but it can do a great deal of harm, too. By being mindful of how we interact with our devices, and by creating space to be bored, we can break the control that technology too often exerts. Furthermore, children need to be taught to use technology responsibly. This doesn't mean keeping it from them; rather, it's a matter of teaching them what healthy usage looks like. We can improve our own digital habits, and become better role models, by taking on the Bored and Brilliant challenge.**

Actionable advice

**Do some quick meditation.**

Chade-Meng Tan, former Jolly Good Fellow at Google, developed a simple four-step meditation designed to make people calmer and more successful. Here are the steps:

    * 1\. Think about a person you care about

    * 2\. Think "I wish this person happiness."

    * 3\. Keep this in your head for three breaths, in and out

    * 4\. Do this every day and your wish for the happiness of another will actually bring _you_ happiness

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Unplug_** **by Suze Yalof Schwartz**

_Unplug_ (2017), a useful beginner's guide to meditation, makes clear that meditation isn't just for hippies and monks; rather, it's a practical exercise that anyone can use to become calmer, happier and healthier. Get all the basic info you need to start a daily meditation routine, as well as a number of supplemental tools that will aid relaxation and improve your practice.

This is a Blinkist staff pick

_"These days, we are so busy at work and in our private lives that it becomes more and more difficult to truly relax. These blinks explain why meditation is the best medicine to fully recharge your batteries."_

– Robyn, German Production Manager at Blinkist
---

### Manoush Zomorodi

Manoush Zomorodi is a journalist, radio host and author. She's best known for her award-winning podcast, _Note to Self_, which is about technology and people's relationship to it.

