---
id: 57cdee212b2b0d0003fa2a2b
slug: born-for-this-en
published_date: 2016-09-09T00:00:00.000+00:00
author: Chris Guillebeau
title: Born For This
subtitle: How to Find the Work You Were Meant to Do
main_color: E32D3A
text_color: A12029
---

# Born For This

_How to Find the Work You Were Meant to Do_

**Chris Guillebeau**

_Born For This_ (2016) is your guide to finding the career of your dreams. These blinks will help you discover your true passions while providing the tips and information you need to earn money from your genuine interests.

---
### 1. What’s in it for me? Find the work you were born to do. 

Everyone wants to win the job lottery. We all want to get paid for doing work we love so much that it doesn't even feel like work.

But you need to be exceptionally talented to make a living creating works of art or conducting cutting-edge experiments in a lab, right? Well, that's not necessarily the case. A great job is waiting for everyone, including you — it's the job you were born to do. You have all the right skills for it and it brings together your greatest interests. All you need to do is to find this job, but outdated career advice may be holding you back from doing so.

That's where these blinks come in. They'll help you update your career roadmap and will tell you where to start searching. 

In these blinks, you'll also learn

  * how to build a small business alongside your main job in just a few weeks;

  * why you don't need to settle for one of the jobs that are already out there; and

  * what a 100 People Project is and how it can help your career.

### 2. The way to your true calling isn’t clear-cut, and a one-size-fits-all plan won’t help you find it. 

Some people have their entire life mapped out by the time they enter kindergarten. But for most of us, finding our life's work takes a bit more effort, especially since career paths aren't always straightforward and predictable.

So, if you don't know what you want to do with your life, don't sweat it — just start experimenting. This is a good strategy because careers develop in unforeseeable ways and every job or project that grabs your attention could be a step toward your true calling.

Say you follow your dream and join the circus. After a few months, you start to realize that you're not so into cleaning up after elephants. So you transfer to the box office to sell tickets, but this new job, a bit too pleasant and a bit too clean, quickly gets boring.

Done with the box office, you decide to get an office job outside the circus. Your new job is just starting to get on your nerves when you meet a client who owns a clothing line. Suddenly you realize that you were born to design circus-themed T-shirts!

The point is, you don't need to subscribe to a seemingly established career path. While some people are staunch believers in the notion that careers _do_ follow strict scripts, these assumptions are invariably based on other people's experiences.

For instance, a common rule is that if a career opportunity comes your way, you should seize the chance because it might be your only one. In reality, abiding by such strict rules will undermine your creativity and obscure the path to your dream job. After all, just because something worked for someone else's career doesn't mean it will for yours.

That's why it's important to _flip the script_, that is, to rewrite traditional career advice to match your needs. You might ditch the rule about jumping at your first career opportunity and write a new one that says, "If you don't feel good about a job, pass it up." There will always be other options.

> _"Business opportunities are like buses. There's always another one coming."_ \- Richard Branson

### 3. Your ideal job should be the perfect mix of joy, money and flow, with working conditions that match your needs. 

Imagine you had the chance to make one wish and you chose to win the job lottery, but it's still up to you to specify exactly what you want. What does your ideal job entail?

For starters, it should have the right mix of joy, money and _flow_. After all, the ideal job is one that pays you for doing work you love. If working with children is your greatest joy, second only to explaining things, then being a school teacher is perfect for you.

But, of course, enjoying your work isn't the only important factor; there's also the issue of money. Even if you don't want to be rich, it's hard to enjoy your life if you're constantly stressed about making ends meet.

And finally, your ideal job should let you experience _flow_, a mental state in which you're completely immersed in an activity, forget about time and do your absolute best work. All three of these ingredients are important for everyone, but the difference lies in how much of a priority each one is to any individual.

So, joy, money and flow are central to determining your ideal career, but your working conditions are too. You might love the content of your work, but if your day-to-day conditions are poor you'll just wind up stressed out and unhappy.

To prevent this from happening, it's important to determine the conditions that are right for you. For example, how important is it for you to set your own schedule? Could you be happy working a 9-to-5 job?

Then consider what type of social environment suits your needs — do you want to work solo or on a team? Do you prefer working in a shared office or at home?

Lastly, there's _reporting_ and _accountability_. In other words, do you prefer working autonomously or are you fine with being managed? How do you feel about your actions being monitored?

Once you've answered all these questions, you'll have a stronger sense of what kind of job is right for you. Now it's just a matter of implementing a strategy that helps you attain it!

> _The closer you come to your ideal intersection of these three qualities, the happier — and more successful — you'll be._

### 4. Conquer your fear, plan for risk and develop backup plans. 

Could fear be holding you back from following your dreams?

If it is, you'll benefit from boosting your confidence and taking more risks. To do so, start by listing everything that could go wrong and estimating how likely any given outcome is. You might find that many of your fears are either harmless or so improbable that there's nothing to worry about, so you don't have to be afraid to make your move!

Next, make sure that a fear of missing out isn't guiding your career decisions. After all, it's a terrible basis for making decisions and will actually distract you from what matters.

Say you have an interview at a company's headquarters. Upon arriving, everyone looks tense and unhappy — but the interview goes great and they end up offering you a position.

If you were desperate, you might feel compelled to accept, scared that it would be your only chance, even though the workplace environment looked downright scary. If you let your fear of missing out on this one opportunity guide your decision, it might end up making you miserable.

And finally, prepare yourself for risks, make backup plans and build a safety net.

How?

Whenever you make a decision, consider the possible outcomes and determine what you'll do if they come true. For example, say you want to send some bold advertising e-mails promoting your business. You can make an _if-then sketch_, which would go like this:

If the first recipients take the bait, go out and celebrate! But _if_ they don't, _then_ send a tame mailer with a printable coupon to the next prospects. Just make sure these backup plans are very specific to help you put them into place.

Additionally, three general measures that make up your _career insurance policy_ will help you control any damage if things go wrong — say, if you lose your job.

First, maintain a network of supporters, that is, people who can help you build a career, find customers or get back on your feet. Second, secure an additional source of income to ensure that losing one won't be the end of the world. And finally, always spend less than you earn to maintain a financial buffer.

> _"The more we can make rational decisions based on the information currently available to us, the better we'll become at assessing risk."_

### 5. Identify your skills and weaknesses, figure out how they can meet a demand and present your ideas to others. 

When hypothesizing about suitable careers, people tend to overfocus on things they've studied. For instance, a data scientist might only think of jobs that involve doing statistical computations. To widen your scope, make an inventory of _all_ your skills and weaknesses.

To do so, simply make two lists. One list should detail the things you do well — and that means _everything_, not just the skills related to your education. The second list should include everything you hate or struggle to do. This is crucial because your greatest weaknesses are unlikely to become your strongest skills.

After completing these lists, you'll get a first glimpse of the type of work you were made to do. The next step is to determine how you can make money by employing these skills to meet a demand and connect with prospective clients.

And this might be simpler than you think. If the people you talk to every day are already telling you what area they need you to apply your abilities, you're already on the right track. For example, you might find that coworkers always turn to you with relationship trouble or laptop malfunctions, which could point to an area where you can provide something that people are willing to pay for.

Then, once you've identified this area, you're ready to begin your _100 People Project_, which works like this:

Say you discover you're great at giving weight loss advice. Your next move is to choose 100 people from your contacts list and social media, get in touch with them and offer them free 15-minute consultations. This will improve your skills while connecting you with potential clients for your future business.

But remember, no matter what other talents you have, it's essential to keep up your writing and speaking skills. After all, you'll always need them to persuade others of yourself, your ideas and your products.

> _"You can't go wrong by helping people solve universal, everyday problems like losing weight."_

### 6. Build your own business while keeping your day job. 

Starting a new business is exciting, especially if it's one that's closely linked to your skills and passions. And the best part is, you can launch your dream business without quitting your day job. It's possible to build a small business in your free time, as long as you do so systematically.

In fact, loads of people who now earn their living from their passion initially built their business in their spare time as a _side hustle_. And just a single hour a day spent planning over the course of a few weeks is enough to launch a small business. But, naturally, you need to be careful not to waste your time.

So don't search out that one big business idea; it's elusive and you don't need it. Instead, answer specific questions: What kind of product or service are you going to provide? Who are your customers going to be? Which of their problems are you going to solve? What will your budget look like? How can you market your business?

These questions are perfect because you can answer them in detail in your daily hour devoted to your new business. Then, once you're done planning, you can start spending your daily hour executing your plans, one step at a time — for instance, by advertising your service on a social network.

You'll always be on the safe side if you follow this strategy. You'll be able to rely on the income from your day job and won't have to depend on your new business to succeed. As a result, you can experiment with your idea, learning a lot in the process while still paying your bills.

And if your business _does_ work out, it will give you a second source of income that will make you even more financially stable. That said, if you really want to quit your 9-to-5 and work on your business full time, you should do it — but only if your business is generating enough money to keep you afloat.

> _"You don't have to quit your job to start something on the side, and it doesn't have to take over your life unless you want it to."_

### 7. Create your own job or become your own agent. 

Plenty of job-finding strategies seem like a good idea initially, but prove to be a colossal waste of time. And, truthfully, most of them are. So, if the well-worn paths don't lead you to career success, just create your own job.

After all, consider all the hours and energy you can invest in updating your LinkedIn profile or attending job fairs, only to come up empty-handed. These strategies simply aren't very effective because thousands of smart, qualified applicants are also using the very same ones. This means lots of competitors, some of whom might be a better fit for any given position.

So, a better idea is to invent the position that fits your skills perfectly and find a company that's willing to create it. Some people even create their own titles. For instance, Tony Bacigalupo nominated himself to be the _Mayor_ of _New Work Cities_, his coworking space. Just remember, this strategy is best used to search for a job within a company.

But maybe you want to make a living from your creative practice. If so, just become your own manager and promoter.

If you hide in your basement and paint all day, people won't know who you are, regardless of how amazing your work is. That's not to say you shouldn't harness your inspiration to build a compelling body of work; but the art world has changed, and now you need to do much more than that.

Writers, musicians and visual artists used to rely on a publisher, record label or patron to distribute their art, and would pay them a share for the service. But today, _you_ can represent your own work.

Start by reaching out to your fans and building an audience. Just do so through the best platform for socializing with people who appreciate your work; it might be through social media, by attending conferences or any other preferred method of putting yourself out there.

And finally, it's essential to figure out how to get paid. You might make money by selling paintings, through crowdfunding or even blog ads.

> _"Focus (...) on making something that matters and connecting with people who get what it's all about."_

### 8. There’s nothing wrong with pursuing multiple passions. 

Are you struggling with the question of what to do with your life? Well, if you are, it might be because you think you need to decide on a single, focused pursuit once and for all.

In reality, you don't need to pick a niche and stick with it for your whole life; instead, you can adapt your career to your changing needs and interests.

After all, you never signed a contract saying you'll do the same thing for the rest of your life. And that's a good thing, because life is seasonal — your needs and interests will change as you do. If you're raising a child, you might appreciate the regular schedule and steady income of a 9-to-5 job. But during other phases, you might prefer the nomadic lifestyle of a travel writer.

A happy life is based on the ability to choose, but you also might want to focus on more than one skill over the course of your career. It's not necessarily to the detriment of your career to pursue multiple interests.

So how can you best transition between multiple jobs?

One good way is to use _workshifting_, which is deliberately switching your complete attention back and forth between different projects. For instance, someone might focus on his landscaping business during the spring and summer while spending the winter months writing screenplays. Another person might work as a boxing trainer during the afternoons while offering relationship coaching in the evenings.

And these combinations aren't difficult to attain; these people have simply integrated change into their regular work schedule, allowing them to switch quickly and efficiently from one job to another.

Just remember, there are lots of ways to live a fulfilling life, so find your passions and devote yourself to every one of them.

### 9. Final summary 

The key message in this book:

**Finding the work you were made to do is never a straightforward journey. So, keep an open mind, use all the skills at your disposal and follow all your passions no matter where they might take you.**

Actionable advice:

**Do something different.**

The next time you make a New Year's resolution, promise yourself that, at least once a year, you'll break free from routine and do something different. It might be to take that yoga teacher training course you've always wanted to, or sell some special treat of yours at the park on a weekend. Regardless of whether it's the beginning of a new business or not, chances are you'll meet interesting new people, have new experiences and build your confidence — all while having lots of fun along the way.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The $100 Startup_** **by Chris Guillebeau**

_The $100 Startup_ is a guide for people who want to leave their nine-to-five jobs to start their own business. Drawing from case studies of 50 entrepreneurs who have started microbusinesses with $100 or less, Guillebeau gives advice and tools on how to successfully define and sell a product, as well as how to grow your business from there.
---

### Chris Guillebeau

Chris Guillebeau is an American entrepreneur, nonfiction writer and blogger. He's known for his blog _The Art of Nonconformity_ and his book of the same name. In addition to his books and blogging, he has written travel guides and business literature under the brand name _Unconventional Guides._

