---
id: 58c7e76bb57da400041c50ea
slug: born-liars-en
published_date: 2017-03-14T00:00:00.000+00:00
author: Ian Leslie
title: Born Liars
subtitle: Why We Can't Live Without Deceit
main_color: 2EC3E6
text_color: 196C80
---

# Born Liars

_Why We Can't Live Without Deceit_

**Ian Leslie**

_Born Liars_ (2011) uncovers the truth about lying and the important role it plays in our lives. Far from being some undesirable glitch in the human system, lying has not only made us smarter but saved many lives and become an essential ingredient to our overall well-being. In these blinks, you'll learn all about the history and neuroscience of fibbing, why it might be impossible to detect every lie and how central mendacity truly is to being human.

---
### 1. What’s in it for me? Learn why everybody lies – and why it’s not necessarily a bad thing. 

It begins when we're very young. Our parents, and all other authority figures, tell us not to lie. Indeed, truthfulness is ingrained in our culture. The Ten Commandments instruct us not to bear false witness — an imperative that is repeated by secular institutions as well. And yet most of us lie anyway. Sometimes we do it cunningly; other times, to spare a friend's feelings. Often, we lie without even knowing it ourselves. It's almost as if falsehood were part of our DNA.

And perhaps it is. We are literally born liars. We learn to twist the truth early on, a skill that helps us survive in human society. As we get older, we continue to deceive ourselves and each other — not out of spite but, perhaps, out of habit and necessity. So let's take a closer look at lying and learn why our brains make us do it, even when we don't want to.

In these blinks, you'll learn

  * That even a nine-month-old baby can tell a lie;

  * how lies can sometimes benefit our health; and

  * that most of your memories are built on lies.

### 2. Lying is an essential part of who we are. 

Even if you consider yourself an honest person, you probably tell a lie every now and then. Or, to be more precise, you utter a falsehood with intent to deceive. These lies may not be malicious. Indeed, they often aren't. Lying is simply something we're hardwired to do.

We lie for a number of reasons, most of which stem from our social nature.

It isn't easy being a social species. You have to keep track of dozens of relationships, predict how your actions will affect others and how best to react to the actions and reactions of those around you. Just thinking about it is exhausting!

When our ancestors started becoming more social, they began developing bigger brains to cope with these proliferating interpersonal demands. Bigger brains helped them make better decisions, which in turn reinforced their developing intelligence. This narrative is the basis of the "social intelligence" hypothesis proposed in 1976 by the academic Nicholas Humphrey.

Relatively speaking, it wasn't long before we discovered that deceit is an invaluable tool in society. Say a certain caveman needed more food. It wasn't long before he realized that he could hide what he'd received from the others and then say he'd never been given his share in the first place.

Society always presents us with rivals and competition, difficulties that we learned to brush aside with a few apposite untruths.

In the 1980s, primatologists Richard Byrne and Andrew Whiten discovered that primates also lie to get ahead.

Two young chimpanzees were once observed digging for food. When they noticed an older chimp approaching, they quickly sat back, scratched their heads, relaxed and began pretending like nothing was going on. Once the older chimp was out of sight, they immediately got back to digging.

This kind of deceit requires intelligence.

To be able to convince that older chimp, the two youngsters had to have good timing and needed to pick the right gestures and posture to make it believable.

According to Byrne and Whiten, human intelligence directly evolved from these scenarios of successful deception.

In other words, lying is an essential part of how we came to be.

### 3. As we grow older, we learn when to lie. 

Oddly enough, we don't even need to be taught how to lie; it's just something that starts happening. Unless you were an only child, you probably recall cooking up some story about your brother or sister and telling it to your parents — just to get your sibling in trouble.

But we actually begin to lie when we're infants. Initially, it's innocent enough. For instance, a nine-month old baby may fake laughter in order to be part of the fun that other giggling babies are having.

At around age four, however, we become better liars.

You may have noticed that three-year-olds will usually confess immediately when you ask whether they took the last cookie. A four-year-old, on the other hand, is much more likely to lie or blame someone else or pretend that he didn't take it.

This change is what psychologists call the _theory of mind_.

Around age four is when we start to understand that what's happening in our mind is different from what's going on in other people's minds — and that our mind is inaccessible to others.

So the three-year-old will tell the truth because she assumes that her accuser already knows the reality of the situation. But a four-year-old is starting to realize that the accuser can't read his mind, and that he might be able to avoid getting caught if he says the right thing.

As time goes on and we become more social, we learn _when_ to lie.

At school, we start receiving a great deal of feedback from other people. And this teaches us a lot about lying.

First of all, we learn that if we're constantly lying, we can easily lose all credibility.

We also learn that some lies come with bigger risks than others. The potential consequences of lying to a close friend, for instance, are much worse than lying to a complete stranger.

This experience provides us an understanding of when to lie and when not to lie, which we carry on into new environments, like the workplace and our adult relationships.

### 4. It’s hard to spot a liar, and we can get away with lying more often than we think. 

Some people grow up to be better liars than others. When lying, some people are shifty and anxious, while others are smooth, confident and persuasive. As a result, there's no foolproof way to spot a liar, though there are signs to look out for.

One way to spot a fib is to pay attention to facial expressions, which usually signal someone's true emotions and are generally hard to control.

For example, a smile that raises the cheeks is an expression of happiness. So if your coworker says she's sad that you didn't get the promotion you were after, and you see her cheeks rise for a moment, then she might be trying to conceal her true happiness.

However, the best liars are keenly aware of their facial expressions and they know which one to show at just the right moment so as not to get caught.

By and large, though, we can get away with lying more often than we think.

As psychologist Emily Pronin of Princeton University points out, in any given social situation, the advantage is on the liar's side.

Only we know what we're thinking and others can only see our facial expressions. Therefore, if you're telling someone how lovely his boring vacation photos are, you're the only one who knows how you truly feel. And your clueless pal, with only your smiling face and friendly words to go by, will tend to believe that you are in fact enjoying yourself.

Most people take things at face value. They believe that a smile is a smile, and forget that any number of different thoughts might be going on behind that smile.

And just as we tend to overestimate how good we are at detecting the lies of others, we also overestimate how good others are at detecting our lies. In reality, most people will think our expressions are sincere, just as we assume theirs are.

So, when we tell that lie, we're often safer than we think.

> _"Pinocchio's nose remains a fairytale."_

### 5. Machines are not a foolproof way to spot a liar. 

Okay, so people are lousy at spotting lies. But we have machines for that purpose, right?

Most of us are familiar with the famous polygraph machine. What most of don't know is that it's actually quite flawed.

The polygraph works by monitoring several physiological features, such as blood pressure and respiratory rate, and marks the results on a scrolling piece of paper.

All of this works under the assumption that when you're lying, your blood pressure will drop, and be noted by the machine. And even though it's been widely used by the police since the 1920s, it was never 100 percent accurate.

There's more than one reason for a suspect's blood pressure to drop, especially when they're innocent and worried about being falsely imprisoned, and this natural anxiety is exactly why polygraphs aren't used much anymore.

But the polygraph isn't fully accurate for another reason, too — a reason that makes the invention of a foolproof lie detector extremely improbable: liars sometimes truly believe their own lies.

When we bring up a memory, we're not just pressing play on a recording; we're actively constructing a scene in our mind that can be influenced by what is going on around us at that moment.

So your mom might go on and on about how much she liked that purple shirt you always wore as a child, until finally you start to remember it as being purple, even though it was actually green.

These false memories can be implanted during an interrogation as well, when the interrogator uses loaded language, such as: "We know you did it, so just admit it, you attacked him with the knife."

This can go on for hours, and the pressure can make the suspect remember things that never happened and then confess to a crime he never committed. And since the delirious suspect has come to believe the false confession, any lie detector would only confirm that he's telling the truth.

This is what happened in the 1999 case of the "Norfolk Four," who were convicted of raping and murdering nineteen-year-old Michelle Moore-Bosko after falsely confessing to the crimes.

### 6. It is also human for us to lie to ourselves. 

The way our memories can change might lead you to wonder whether we ever perceive anything objectively.

Our brains perform a lot of functions, and they can also deceive us.

After all, we don't just perceive reality exactly as it is; our brain is constantly filtering reality, giving us our own unique perceptions. So a person who is color blind and deaf in one ear will have rather different perceptions than someone with perfect hearing and a more traditional color sense. 

Our brains even alter some perceptions on purpose. For instance, they tend to make hills seem steeper than they actually are. This becomes more exaggerated as we get older, and when we're unwell or we're carrying a heavy load. It's one of the brain's ways of protecting us from injury.

It is also common for us to lie to ourselves to protect our beliefs.

We consider our beliefs to be important — one of our defining characteristics — and so we often tell ourselves that information that contradicts those beliefs is untrue.

This is called _cognitive dissonance_, and it allows us to ignore the facts rather than reexamine our beliefs.

It allows people to go on thinking they're just enjoying life and blowing off steam by getting drunk everyday, rather than admitting they're an addict.

We also tend to be delusional and believe we're more skillful than we really are.

When a group of college students were asked about their driving skills, 88 percent claimed that they were in the top 50 percent of that group, which is obviously a mathematical impossibility.

However, this self-deception does have its benefits, especially when it comes to innovation.

Society relies on progress and progress relies on people defying the odds and making the previously unimaginable a reality.

To do this, the idealistic dreamers need to ignore the realistic doubters. In other words, they need to deceive themselves.

But self-deception shouldn't be taken too far.

Wars begin and societies are destroyed when leaders on both sides ignore reason and think they're going to win.

> _"In a sense, deception begins the moment you open your eyes."_

### 7. Some lies save lives; others help sell things. 

So lies serve a lot of purposes and are a central part of our lives. But did you know that they're powerful enough to help heal the human body?

In 1944, Henry Beecher was a Harvard Professor and a doctor helping Allied troops in Anzio, Italy. One day an injured soldier arrived needing an urgent operation, but Beecher was all out of morphine.

In desperation, one of the nurses injected the soldier with diluted salt water, telling him it was morphine. And, strangely enough, deception did the trick. The soldier immediately calmed down, and barely flinched from pain during the operation.

What Beecher witnessed is what we now call the _placebo effect_ — when the _belief_ that you're receiving treatment proves as effective as real treatment.

This is actually a powerful medical tool, though exactly why it works remains a mystery.

Similarly, no one knows why the same drug delivered by two doctors — one confident, one nervous — will be more effective when administered by the confident doctor.

However, there's no doubting the power of the placebo, which is why it's still widely used today.

According to a recent study by the Oregon Health Sciences University, between 35 and 45 percent of all prescribed medications today are placebos.

This practice is so popular that it is also used to make our food taste better.

In 2006, an advertising executive and former comedian named Hunter Somerville decided to reinvent a classic shredded wheat cereal with a simple marketing campaign.

He took the square shaped cereal, rotated it by 45 degrees so that it resembled a diamond and renamed it accordingly: the brand new _Diamond Shreddie_!

The campaign worked so well that people even claimed that Diamond Shreddies tasted better than the old cereal, even though nothing had actually changed.

The new image simply acted as a placebo, making people believe that they were being treated to something new.

### 8. There’s no clear answer to the morality of lies. 

Okay, so our culture is steeped in lies. But there's one question we haven't addressed: "Is lying wrong?"

Many people have been raised to believe that lying is immoral. But neither the Bible nor secular philosophers provide a definitive answer on this point.

True, one of the Bible's Ten Commandments is "Thou shalt not bear false witness against thy neighbor." But there are other instances in the Bible where lies are presented as acceptable.

For instance, the Egyptian midwives lie to the Pharaoh in order to save the Hebrew children. According to the Bible, this act of deception was performed with God's approval.

In 1785, the German philosopher Immanuel Kant wrote that lying should be considered unequivocally wrong since it robs the liar of his human dignity. However, Kant later expressed uncertainty; he wondered whether it wasn't okay to tell a friend that you liked his work even if you didn't, so as not to hurt his feelings.

Ultimately, how people feel about lying depends on their upbringing.

In the early 2000s, Chinese-Canadian professor Kang Lee rephrased the question by asking why some lies are deemed OK, and others aren't.

To get a better sense of where people draw the line, he presented groups of Chinese and Canadian children with a series of different scenarios.

In one story, a child performs a good deed, and then lies to her teacher about having done it.

The Canadian children said the child shouldn't have lied. But the Chinese children said it was just fine.

Kang Lee decided that these differing opinions came from the different values that the Chinese and Canadian children had been taught.

Canadian culture values personal achievement, so lying about performing a good deed is not only dishonest; it's harmful.

And since Chinese culture values humility and self-effacement, they see this lie as something good in and of itself. It shows that the person is truly humble.

In the end, the morality of lying remains, and will likely continue to remain, a matter of perception.

### 9. Final summary 

The key message in this book:

**It's time to change the way we think about lying. It isn't some evil character flaw that bad people are infected with. It's a very human trait that has served us throughout our existence and can continue to be a useful tool — especially when you don't want to hurt someone's feelings.**

Actionable advice:

**Be skeptical of your own knowledge.**

Part of being honest with yourself is admitting that you might be wrong about what you think you know. If you replace "I know" with "I think," you'll be more open to discussion and, in turn, better able to contribute to the progress of the human race.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Curious_** **by Ian Leslie**

_Curious_ is all about one of the most fundamental forces for our success as well as our perception of the world around us: our curiosity. The book offers a unique look into how curiosity works, what you can do to nurture it and what sorts of behaviors stifle it.
---

### Ian Leslie

Ian Leslie lives in London, where he writes for a variety of UK and US publications. He is also a writer and performer for the BBC Radio 4 comedy show _Before They Were Famous_. His second book, _Curious: The Desire to Know and Why Your Future Depends On It_, was published in 2015.

