---
id: 5799f1f8e0bc1b000321bcf2
slug: born-to-run-en
published_date: 2016-08-05T00:00:00.000+00:00
author: Christopher McDougall
title: Born to Run
subtitle: A Hidden Tribe, Superathletes and the Greatest Race the World Has Never Seen
main_color: E65C2E
text_color: 993D1F
---

# Born to Run

_A Hidden Tribe, Superathletes and the Greatest Race the World Has Never Seen_

**Christopher McDougall**

_Born to Run_ (2009) delves into the human capacity for long-distance running. First-hand accounts, an encounter with a secretive ultra-running tribe and cutting-edge research combine to argue for the idea that we may well be born to run.

---
### 1. What’s in it for me? Find your inner marathon runner! 

Every year millions of people all over the world run marathons. You might think they're all completely crazy or that you could never run a marathon yourself. Many of us secretly fantasize about being marathon runners if only this were possible.

However, it doesn't take a freakish, superhuman physique to cover 26 miles and 385 yards at a clip. You just need some training and the right attitude to get yourself off the couch.

In these blinks, you'll learn

  * why dogs can't run marathons;

  * why most running shoes hurt us, rather than help us; and

  * why the best runners run like kindergarteners.

### 2. The human body is evolutionarily well-adapted to long-distance running. 

When you think of fast sprinters, your mind probably jumps to cheetahs or horses. But when it comes to running long distances, it's actually _Homo sapiens_ who take the lead.

The reason? It's in our physiology.

For one, humans are able to dissipate heat more rapidly than other animals.

Most other mammals don't have the right glands in their skin to cool down via sweating, so their main method for releasing body heat is through breathing.

When four-legged animals want to run fast, they break into a gallop. This method of running, though quick, restricts their breathing rate because the moving leg muscles squish the animal's lungs like bellows. As a result, when running fast, most animals can only breathe at a rate of one breath per stride.

This works fine for them — until they reach the critical limit at which they heat up faster than they can cool down. Then they have to stop running in order to survive.

We humans, on the other hand, prevent overheating by sweating through our skin. As a result, the human breathing cycle is not determined by our need to cool down, making it more efficient at procuring oxygen and maintaining endurance.

A second key factor that makes humans master runners is that we move on two legs instead of four.

When early humans began to walk upright — thus freeing their hands to use tools and reach higher hanging fruit — it allowed their throats to open and chests to expand. Though this development came at the expense of sprinting speed, this new posture and the increase in air capacity enabled them to maintain running over long distances.

Finally, our Achilles' tendon is the third trait distinguishing humans as runners.

Some 95 percent of human DNA correlates with that of our close genetic relative, the chimpanzee, but even these primates don't have this flexible, rubber-band-like cord of collagen tissue in their lower leg. As it's stretched, the tendon stores energy until it's ready to be released when the leg propels the body forward. This maximizes our endurance because it takes us less energy to spring from one step to the next.

> _"If you don't think you were born to run you're not only denying history. You're denying who you are."_

### 3. Running shoes actually do more harm than good. 

As we've seen, we have many biological traits that work to our advantage as runners. Sometimes, though, we actually hinder our natural gifts with inventions that are intended to help. A key culprit here is running shoes, which are guilty of stabilizing the foot _too_ well.

When you run, your foot rolls inward in a process called _pronation_ that acts as a built-in shock absorber for the lower leg. Unfortunately, pronation has been demonized as being responsible for _runner's knee_, a painful and common ailment.

As a result, we now see a fleet of pronation-alleviating shoes on the market, although a mere three percent of the population have a medical need for special shoes.

For most people, encasing the foot in a shoe is a lot like having a plaster cast on the leg. The shoe limits the foot's full movement, causing the muscles to atrophy and the foot to lose strength.

The resulting imbalances throughout the body put excessive strain on certain muscles and joints and can lead to injury, which is why taking running shoes out of the equation might actually be better for you.

Consider Alan Webb, the greatest mile runner in the United States, who suffered from foot injuries while still in school. Part of the problem was that Webb had flat feet with low arches, which are not well-suited for running. Thanks to various barefoot running exercises, however, his feet strengthened, and his arches rose dramatically — so much so, in fact, that his size 12 feet now fit into size nine or ten shoes, and his foot injuries abated.

Another problem with running shoes is that they mask the discomfort caused by harmful impacts against the ground.

Thickly cushioned shoes fail to alert runners to the painful shock waves that reverberate through their bodies as they pound the pavement, thus preventing them from adopting less harmful running forms.

Barefoot running, on the other hand, forces the runner to adapt a natural, and ultimately more comfortable, gait.

> "_Covering your feet with cushioned shoes is like turning the smoke alarms off." — Barefoot Ted_

### 4. To run faster and longer, restrict your protein intake and switch to a vegetarian diet. 

OK, so now that we've gotten rid of the shoes, what's next?

Sorry to say it, but your bacon habit might have to bite the dust if you want to become a champion runner.

Despite Western society's current vogue for diets based around lean meats, cutting out meat entirely is probably your best bet.

If you need evidence, just look at some of history's greatest endurance athletes, many of whom were vegetarians.

Japanese marathon-running monks, for instance, would complete 25,000-mile ultra-marathons on a daily provision of miso soup, tofu and vegetables.

Or take Percy Cerutty, coach to some of the greatest runners of all time, who strongly advocated a vegetarian diet. He often pushed his clients through triple sessions on a simple diet of raw oats, fruits, cheeses and nuts.

Elite ultra-runner Scott Jurek took it even further and switched to a vegan diet. Despite being told that he wouldn't recover from workouts and would be more susceptible to injuries, he proved his doubters wrong and performed better than ever.

A vegetarian diet is especially well-suited for running long distances because foods such as fruits, vegetables and whole grains help you extract the maximum nutrition from the fewest calories. As a result, the body is spared from carrying and processing any additional bulk.

Furthermore, your body digests proteins much more slowly than it does carbohydrates. That means that when you eat meat, you're wasting a lot more time waiting for the food to be processed by your digestive system. Think about it this way: less meat means more time to train and fine-tune your running skills.

And don't worry about being malnourished on a meat-free diet, because a diet of grains, legumes and vegetables possesses the amino acids essential for muscle production. They allow for recovery while keeping you ready to run at any moment due to their comparatively brief digestion time.

Now that we have the right equipment and diet, let's move on to the next step in becoming a skilled runner: the right training!

### 5. Form and pace are essential to long-distance running. 

If you've ever watched Olympic sprinting and marathon events back to back, you'll know that they might as well be two completely different sports.

For instance, the ideal sprinting posture, with a straightened back, steady head, and large, forceful strides, is astonishingly different from the way Kenyan athletes choose to run — and, given that Kenya is an elite nation in long-distance running, we might want to pay attention to how they do it.

Ken Mierke, an exercise physiologist and world champion triathlete, set out to discover the Kenyan secret. After watching hours of footage of barefoot Kenyan runners, he discovered that these world-class athletes actually run like kindergarteners.

The key was moving the legs in smaller contractions to enable quicker foot turnover. This technique increases efficiency and fosters endurance that is ideal for running long distances.

Ken came up with a creative, if unusual, idea to help athletes adapt to this form: he set metronomes to 180 beats per minute and attached them to his athlete clients, instructing them to match their pace to its tempo.

After five months, a 60-year-old client who'd been a runner for 40 years in the top 10 percent of his age group saw significant improvements in his trial time. His running résumé proved that the improvements weren't simply the gains of a beginner and that anyone could apply the metronome method to their own running.

Once you've got your form down, the next thing to master is the art of pacing.

In order to achieve this, start by trying to stay below the aerobic threshold — that is, the point at which you begin breathing heavily.

This will help you utilize your fat stores instead of burning through your sugar reserves.

This is important because the average person has enough fat stored to run very long distances. Unfortunately, the mistake that many runners make is running too quickly and using up their sugar tank, which is far more limited than the fat. Pacing will help you to capitalize on the fat reserves and keep you running longer and further.

### 6. The Tarahumara tribe excel at long-distance running because they see running as part of their identity. 

The Tarahumara people live in settlements scattered among the canyons of northern Mexico. What's their connection to running?

"Tarahumara" roughly translates to the "running people." They're aptly named, because they are well-known for their ability to regularly run 200 miles in a single session. Not only this, but they also avoid the running injuries typical of the modern world.

This elusive tribe lives in isolation and shuns outsiders, the reason for which stems from their mistreatment at the hands of Spanish colonizers. Since that time, the Tarahumara have preferred to stick with one another and experience the natural joy of running.

To find out more, the author enlisted the help of a local to track down the secretive tribe. After a marathon drive and two-day hike, they finally met face-to-face with Arnulfo Guinare, the most respected Tarahumara runner.

Thankfully, despite his stand-offish reputation, Arnulfo welcomed them in and shared the secret to being an outstanding runner.

The lesson from the Tarahumara was simple: learn to love to run.

By creating a mind-set and culture based on the belief that running is an indispensable human skill, they've made it hard to be a part of the Tarahumara and _not_ enjoy running. It's seen as an ancestral necessity that makes them who they are as a people.

Not only do they run for the sake of their tribe, but they also run for themselves.

But this passion for running isn't limited to only the Tarahumara people; it's something many of us can relate to. Do you remember running around with total abandon and delight when you were a child? The fact is, running can be a blast. Although we're often conditioned to lose this sense of pleasure, the Tarahumara have not forgotten this feeling — and it's something you should relearn, too.

### 7. Final summary 

The key message in this book:

**Many people are unaware that we humans have a host of innate traits that make us excellent long-distance runners. One of the most important things you can do for your inner runner is to make sure you don't hinder these inherent gifts — go barefoot and remember the natural joy of running!**

**Actionable advice:**

The next time you're thinking about throwing away $100 on a pair of flashy running shoes, think again. Save the money and your health by opting to go without them entirely. Run on a flat sole like a true Tahamuran if you want to improve your running prowess.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Sports Gene_** **by David Epstein**

_The Sports Gene_ takes a look at the physiological traits that are beneficial in various sports, and at their hereditary background. It also examines why people in certain parts of the world have evolved in their particular way, and how this is beneficial in the realm of certain sports.
---

### Christopher McDougall

Christopher McDougall is an American author, TED speaker and journalist who has written for _Esquire, New York Times Magazine_, _Men's Journal_, _New York_ and _Outside_. He's also acted as a contributing editor for _Men's Health._

