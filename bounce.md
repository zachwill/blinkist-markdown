---
id: 530c992634623100088e0000
slug: bounce-en
published_date: 2014-02-25T08:00:00.000+00:00
author: Matthew Syed
title: Bounce
subtitle: The Myth of Talent and the Power of Practice
main_color: B3CF32
text_color: 5B6919
---

# Bounce

_The Myth of Talent and the Power of Practice_

**Matthew Syed**

In _Bounce_ (2011), Matthew Syed explores the origins of outstanding achievements in fields like sports, mathematics and music. He argues that it is intensive training, not natural ability that determines our success, and people who attribute great performances to natural gifts will probably miss their own chance to succeed due to lack of practice.

---
### 1. What’s in it for me? Learn what really separates the winners from the rest of us. 

"You're so talented!" is a common exclamation of praise in our culture. But is it really accurate?

_Bounce_ looks at the science of attaining high performance, and the steps needed to get there. You'll find out, for example, why at the end of the day Mozart wasn't all that special.

You'll also understand how even trivial things, like sharing a birthday with someone successful, can inspire you to succeed.

Finally, you'll also come to understand that most dreaded of occurrences: choking when the stakes are highest, despite thorough preparation. Thankfully, you'll also learn about tools to avoid it happening to you.

### 2. If you want to excel, 10,000 hours of training will take you much further than your natural abilities. 

Mozart is considered by many to have been the greatest composer who ever lived. Traditionally, most people would assume outstanding achievements like his are due to natural abilities, or even divine inspiration or fate. This assumption holds especially true for child prodigies like Mozart who already had the world mesmerized with his musical talent at the age of six.

But looking more closely at the phenomenon of child prodigies, we find that in fact they had to practice for thousands of hours before showing their so-called prodigious talent. In fact, scientists studying the phenomenon have found that typically a prodigy's training begins at a very early age and that they compress endless hours of practice into their young lives.

For example, when the six-year-old Mozart toured Europe to display his precocious piano skills, he had already undergone 3,500 hours of musical training. If you compare this to other pianists who have practiced for as long, Mozart's performance wasn't all that exceptional.

It seems then, that outstanding abilities come from vast amounts of rigorous practice rather than natural talents.

This is illustrated by a study of young violinists' concerts, where the only factor directly linked to the students' level of achievement was the amount of time they had spent practicing seriously: while the star performers had practiced for an average of 10,000 hours, the least skilled students only had 4,000 hours under their belts. What's even more telling is that there were no exceptions: all of the best-performing students had devoted great efforts to practicing, and all of the students who had practiced for 10,000 hours belonged to the best-performing group.

It seems that no prodigious talent can exist without thorough practice.

### 3. To master new skills, keep challenging yourself and try to learn from your failures. 

Imagine a child pianist practicing to play her favorite songs by ear. She may spend many afternoons practicing at the piano, but it's likely that as soon as she gets close to the original, she'll work less and less hard to improve further as her performance is already good enough.

This is actually typical for most people: they don't practice purposefully after a certain point. When we try to pick up a new skill, we only work on improving our performance until a certain point. That point could be the level of our peers, a requirement set by a piano teacher or simply the level where we're good enough to derive pleasure from our performance.

After this point, we tend to simply keep practicing what we already know, without challenging ourselves further. In essence, we switch to autopilot, and though we may continue practicing, we actually improve very little.

Top performers, however, differ in this aspect. They aren't content with performing tasks they have already mastered, but instead strive to constantly improve and develop skills beyond their current level. Such attempts to perform tasks that are out of their reach force them to focus intently, and also send strong signals to their brains and bodies that they need to adapt.

So how can you practice so that you keep improving and striving for new skills? One way is to accept failure and learn from it.

Naturally, learning any task that's out of your current reach will entail failure. However, this is not a bad thing, as failure provides you with feedback. It teaches you which skills you need to master the task, and shows you which skills you have at your disposal: your strengths and weaknesses. This information allows you to adapt your training regimen.

To become a champion, you must keep striving for skills that are out of your reach, accepting and learning from failures. It's this kind of training that will transform you.

### 4. Intensive practice changes the way your brain works, making it more effective. 

One of the most convincing manifestations of the power of practice can be seen in table tennis player Desmond Douglas. For a long time, he was the top player in the UK, famous for his lightning-fast reactions. Yet, when scientists ran tests on the reaction speeds of all the English national team players, Douglas turned out to be the slowest. So how can someone with long reaction times in general react quickly when it comes to ping-pong?

The answer lies in the two changes that intensive practice brings about in the way your brain handles a specific task.

First, after years of experience in a field, an expert's brain has learned to "read" complex situations typical for that field. It has been primed to quickly extract the relevant bits of information from this familiar setting. 

Therefore when playing table tennis, Douglas' brain could instantly spot the relevant visual cues to predict the ball's trajectory. This left him with more time to react than a less experienced player.

Second, an expert uses different parts of the brain to perform a task than a novice does. This is because when you learn a new skill like table tennis, you need your conscious mind to monitor your every move in an unfamiliar setting. Thus a part of the brain called the _prefrontal_ _cortex_, responsible for conscious control, is very active.

Once you've mastered the skill, conscious control is no longer required as the various actions have become automated. This means that in your brain, control of the movements has been turned over to other areas.

In the realm of table tennis, this would mean that when a player has mastered the wrist movements necessary for forehand topspin, his mind is free to concentrate on things like legwork or tactical considerations.

Now we understand how Douglas became a fast player with slow reactions: intensive practice changed the way his brain functioned in table tennis.

Next, let's look at how your own attitude and state of mind can have an impact on your chances of success.

### 5. If you believe success is determined by your innate talents, you will fail to strive for glory. 

Imagine a top-notch marathon runner, who always finishes her races in the top ten. If you had to explain her success, would you say it was due to her being a natural runner? Or is it because she has practiced unwaveringly for years?

If you'd opt for the first explanation, psychologists would say you probably have a _fixed_ _mindset_, meaning you believe success depends on something you can't change, like genetics. This kind of mindset is very harmful.

If someone with such a fixed mindset is ever labeled as "ungifted," they will no longer bother to practice or try to improve. They will feel that since they obviously lack the talent to succeed, there's no point in wasting time on arduous practice.

On the other hand, if someone with a fixed mindset believes that they are "gifted," they still won't bother to take the necessary steps to truly excel, because they feel they will succeed anyway thanks to their innate abilities.

Consider Darius Knight, a promising table tennis player who was praised so much for his extraordinary talent that he reduced his training intensity. The consequence? His results plummeted until a new coach got him to focus on working harder.

A fixed mindset can also make people give up too easily when learning something new, because they take even small setbacks as evidence that they're not suited to the task.

This was demonstrated by a study where children had to solve puzzles of increasing difficulty. When the children who had a fixed mindset encountered the first difficulties, they began to doubt their intelligence and eventually gave up entirely. At the same time, the other children rose to the challenge — they tried harder and became better solvers when the puzzles became more demanding.

It seems clear then that children should be praised for their commitment, stamina and enthusiasm rather than their talents, for the latter can lead to a fixed mindset.

### 6. Great ambition to succeed can be sparked by the most trivial of circumstances. 

When it comes to the world of professional golf, for a long time South Korea was not really on the map. However, after South Korean golfer Se Ri Pak won the LPGA Championship in 1998, the number of South Koreans on the LPGA tour multiplied. This is not a coincidence.

When people find a similarity — even a trivial one — to a successful person, it increases their confidence in their own chances of success and motivates them to try harder. In this case, South Korean golfers were inspired by their compatriot's success. This phenomenon is called _motivation_ _by_ _association_.

This effect follows from the very basic human desire to belong, which makes us identify with people even if there is just a random similarity between us and them.

This was illustrated by an experiment where undergraduates had to work on unsolvable math puzzles. Before the task though, the students had to read a report written by a fictional but supposedly successful mathematics graduate. When the graduate's birthday on the report was tweaked so that it matched the birthday of the student reading it, it resulted in those students persevering on the puzzle some 65% longer than their peers. This trivial similarity was enough to make them believe in their mathematical abilities more, and try harder.

Other trivial things can spark our motivation too, and they need not be similarities to successful people. Many highly successful people have, in retrospect, found that a trivial incident ignited their desire to excel — for example, an insult or a seemingly meaningless assignment. For professional football player Mia Hamm, that incident came when her coach told her that she had to mentally "switch on" her motivation to strive for the top every day, and demonstrated this point by turning off a light switch in the room.

### 7. If you’re not convinced you’ll win a competition, you won’t be able to put in your maximum performance. 

Right before a competition, top athletes go to great lengths to convince themselves that they will win. This is true even if the athlete in question has just suffered a string of defeats.

To an external observer, this kind of conviction may seem irrational. But in fact the point of this conviction is not its veracity.

You see, in a competition, even the slightest shadow of a doubt in your ability to win will make you more likely to fail. This is because doubts make you nervous and can cause your muscles to quiver or tighten, which could cause a golfer to miss a crucial putt or a gymnast to lose his balance.

Doubts will also distract you when you should be concentrating on your performance. Thus, for example, a football player riddled with doubts will be less likely to spot important cues and signals from teammates.

Finally, doubts can also cause you to be so nervous that your mind "draws a blank," meaning that you temporarily forget something crucial. This phenomenon is familiar to many people in, for example, public speaking.

To perform optimally, you must first get into the right mindset, because your mind can greatly influence your physical state. A good example of this is the placebo effect, where people experience an improvement in health that cannot be explained by the medical procedure they underwent. For example, when severely injured soldiers were injected with saline solution, their pain subsided as long as they thought they were getting morphine.

For top athletes, the mere belief that they are in unbeatable form focuses their mind and allows their body to perform better. It improves their concentration, helps them remain calm under stress and enhance their motor control.

Next, let's look at why some people choke when under pressure, and what can be done about it.

### 8. When we are under pressure and don’t want to fail, our brains make us act cautiously and deliberately. 

Imagine you're at a party, holding a glass of red wine filled to the brim. To your horror, you realize that to greet the host you have to cross an extremely expensive white carpet. So what do you do? Probably, you'll slow down and focus on each and every step. But why?

It's important to understand that the brain is made up of two systems:

The first is called the _explicit_ _brain_ _system._ It is rather slow, and it is activated when we try to consciously control our movements, for example, when we are performing a tap dancing routine for the first time and need to memorize the steps and control each movement of our feet.

The other system is called the _implicit_ _brain_ _system_, and it is active when we perform tasks automatically, without concentrating on what we're doing. It allows us to control our movements quickly and fluently, and it can even process multiple tasks simultaneously.

Once someone has mastered a task, that task is taken over by the implicit brain system, meaning that they are then free to focus on other tasks.

But when people are under pressure, they often revert to the explicit brain system and begin to monitor every single movement they make.

Of course, important tasks are prone to inducing pressure, especially if failure in them would cause unpleasant consequences. This is why people sometimes behave strangely when they're doing something very important; they are afraid of failing.

For example, as you carry that wine glass across the precious carpet, you're afraid of spilling it and angering your host, so you revert to the explicit brain system that you normally only use when learning completely new skills, and hence walk very slowly and deliberately.

### 9. Avoid choking under pressure by convincing yourself that an event isn’t important. 

Many top athletes have been in this nightmare scenario: They are facing a career-defining contest or competition, and are extremely well-prepared, but at some point in the event, their performance deteriorates to that of a beginner. But why does this occur?

Thousands of hours of training and practice has prepared these experts to perform complex tasks. In fact, the tasks have long since been handled by their implicit brain system, so they are done automatically and can be performed simultaneously.

The phenomenon of failure when the stakes are highest is called _choking_, and it occurs when the expert has to perform a complex but familiar task under great pressure. This happens because as you know, under pressure the brain transfers control of any task to the explicit brain system, which unfortunately cannot do many things at once. Thus the expert is no longer able to perform complex tasks, and chokes.

To avoid choking, athletes have to convince themselves that the competition is irrelevant, as this will lower the amount of pressure they feel.

The author, for example, before facing a big competition, tries to put things in perspective by thinking of things much more important than the contest, like his relationships, health or his family. This helps him feel less pressure, which allows him to use his implicit brain to perform the tasks.

Therefore it seems that in order to excel, top athletes must practice as though their chosen sport was the most important thing in the world, but then downplay this importance when the stakes are highest.

### 10. Final summary 

The key message of this book:

**Achieving success in a field isn't about having the right genes, but about practicing deliberately and relentlessly. Practice and constantly challenging yourself will transform the way your brain processes the task at hand. But your attitude also counts — you must trust that practice will make you a master, and learn from your failures.**

Actionable ideas in this book:

**Praise your children for their efforts, not their talent**

If you want your children to make the most of their potential, do not praise them for their talents, but rather compliment their commitment and perseverance in practicing.

**Put things into perspective to avoid choking**

The next time you find yourself scared stiff by an imminent test or competition of some kind, you can soothe your nerves by putting it in perspective: how important is it compared to the most meaningful relationships in your life?
---

### Matthew Syed

Matthew Syed is an award-winning sports journalist who writes columns for _The Times_ and also works as a commentator for BBC Sports. As a table tennis player, Syed was the English number one for almost ten years and played in two Olympic Games.

