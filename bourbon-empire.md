---
id: 570a1fde8b5d6e000300012e
slug: bourbon-empire-en
published_date: 2016-04-15T00:00:00.000+00:00
author: Reid Mitenbuler
title: Bourbon Empire
subtitle: The Past and Future of America's Whiskey
main_color: FBDC32
text_color: 7A6B18
---

# Bourbon Empire

_The Past and Future of America's Whiskey_

**Reid Mitenbuler**

_Bourbon Empire_ (2015) takes you on an exciting journey through time, revealing the complex history of America's famous corn-based whiskey. Learn how this tipple survived the dry period of Prohibition, numerous corruption scandals and competition from overseas spirits while making its mark in politics and society.

---
### 1. What’s in it for me? Learn the history of a true American spirit and become a bourbon expert. 

Greedy lobbyists and US congressmen on the take. Thirsty frontiersmen with stills in backwater swamps. Partying city dwellers willing to risk their lives for a drink. The element that connects them all is bourbon, a distilled spirit that is intimately tied to the history of America.

From bootleggers and bureaucrats to blue-collar workers, bourbon has fueled both greed and thirst. These blinks take you on the adventure that established American whiskey as a potently symbolic drink, with its roots deeply embedded in the nation's culture. You'll learn which whiskey is worth your cash and how capitalism tarnished the myth of the small bourbon distillery forever.

In these blinks, you'll discover

  * which conditions make American whiskey officially bourbon;

  * why when the British left, rum dried up and whiskey became popular; and

  * how the US government tried to poison America's thirst for booze.

**This is a Blinkist staff pick**

_"Pour yourself a glass of bourbon and read or listen to these blinks. They really give you a sense of the long and gritty history of this most American of spirits."_

– Erik, Editorial Production Manager at Blinkist

### 2. Bourbon whiskey is the embodiment of the American spirit and storied history. 

How do you like your bourbon whiskey? Served on the rocks, with a twist? Or perhaps just a shot after a long day at work? However you enjoy bourbon, know that it has a long, storied history as the drink of choice for the American people.

There are many reasons why bourbon is a uniquely American spirit.

While bourbon is technically a whiskey, this type of spirit can only be called "bourbon" if it meets the following conditions: It must be distilled in the United States; the grain mixture must be at least 51 percent corn; and it must be aged in new oak barrels.

These rules to protect bourbon's unique character came about through careful lobbying by the liquor industry. In particular, Lewis Rosenstiel, head of one of America's largest liquor companies, wanted to give bourbon a special classification to secure it as a distinctively American product.

Through this classification, Rosenstiel knew he could make bourbon a more desirable export product. He spent millions of dollars lobbying overseas, even sending every US embassy a case of bourbon!

In time, his efforts paid off. In 1964, the classification rules were made law and bourbon was on its way to international fame.

While politics made bourbon the spirit it is today, its history with American drinkers runs much deeper.

North American whiskey appears as far back as the seventeenth century when the fiery spirit was supplied to frontier soldiers fighting Native Americans. This whiskey was of questionable quality, but producers at the time weren't concerned about quality ingredients or proper distilling procedures.

But as settlers began to move into the western territories, new and different corn and grains were discovered and added to the mash recipe. This certainly helped the flavor of this originally simple spirit: what once tasted like gasoline began to refine into what we know as bourbon today.

Thus, bourbon whiskey was on its way to becoming the "American native spirit."

### 3. The bourbon market today is a great illustration of the fight between large and small businesses. 

Many popular stories about bourbon whiskey are of families following noble, centuries-old distilling traditions, unconcerned with modern technology. But the reality of the whiskey business tells quite a different story.

The truth is that small, family-run whiskey stills were getting squeezed out of business as far back as the eighteenth century.

Between 1789 and 1797, when George Washington was president of the United States, he tried to introduce a tax on whiskey that would benefit big producers while harming smaller ones. People who lived on the frontier, many of whom made their own whiskey for sale, rebelled against the tax, arguing that it would ruin them.

The protests were successful, and Washington gave up on the tax. But it was only a matter of time before big companies would take over the distilled spirits business.

Today, large producers disguise their influence by releasing products under many different brands, which at first glance appear to represent small, family-run businesses and artisanal traditions.

Even though store shelves are packed with dozens of different whiskeys and bourbons, some 75 percent of these are made by the same _Big Four_ producers: Beam, Schenley, Stitzel-Weller and Heaven Hill. These four companies are constantly fighting for market leadership.

So are you fooling yourself into thinking that there are tons of small brands that differ in quality and value?

In essence, the whiskey industry is taking advantage of the power the alcoholic spirit has to make us feel connected to past traditions, to relive the spirit of the American frontier.

By manipulating this desire, the Big Four market products offering you a break from today's chaos and a retreat to a simpler time. But really, these companies represent nothing less than hardened American capitalism in the modern world.

So we now know about today's whiskey industry. But what were its origins?

> _"Don't believe 90 percent of the tales you read on whiskey bottles, but don't forget to enjoy them either."_

### 4. The history of bourbon began 400 years ago, with an army distiller in a Virginia swamp. 

Captain George Thorpe was America's first whiskey distiller. Some 400 years ago in a Virginia swamp, he became one of the first thirsty men to distill corn mash into alcohol.

This early whiskey came out tasting more like paint thinner than the bourbon we know today. It's no wonder that this tipple faced tough competition from the far more palatable Caribbean rum.

When the British were still in control of North America, rum was cheaply available, as British territories in the Caribbean provided an easily accessible source of sugar cane from which to distill rum.

Rum was so popular that it could even be used to trade for goods or British-backed credit. This trade in alcohol provided healthy tax revenues for the British government, and soon led people to see rum as all that was bad about British rule in America.

When the American Revolutionary War began in 1775, the supply of cheap rum came to an end. And with sugar imports cut off, rum stocks were quickly depleted.

Whiskey could be made with local ingredients, so it quickly became popular — so much so that it came to represent a symbol of national freedom.

Even George Washington had his own whiskey distillery, producing around 11,000 gallons a year!

### 5. The lucrative whiskey industry inspired criminality and poor quality; then Prohibition shut it down. 

You might think, after independence, it would be smooth sailing for whiskey producers and drinkers. But the whiskey industry was about to enter a troubling period in its history.

By 1875, newspapers were filled with stories of scandal, corruption, bribery and tax evasion going on in the whiskey industry.

What became known as the _Whiskey Ring_ was a conspiracy involving hundreds of people selling cheap, low-quality whiskey disguised as a superior product. Dozens of distilleries across the country were involved, and most were evading federal taxes.

The Whiskey Ring conspiracy even reached the White House. To his dismay, President Ulysses S. Grant discovered that his secretary and friend, Orville Babcock, was one of the ringleaders!

This scandal resulted in the arrest of hundreds of whiskey producers, and it damaged the reputation of the whiskey business for a long time. But things got even worse in the twentieth century.

In 1920, the Eighteenth Amendment was passed, banning the production or sale of alcoholic beverages and ushering in the era of Prohibition. This ban lasted for 13 years.

People soon forgot what a quality product tasted like; illegal "bootleg" booze began to appear, accepted by desperate customers despite its low quality.

Things even turned deadly, as the government began to meddle with the supplies that bootleggers were using to manufacture illegal whiskey.

For instance, alcohol was still needed for medical purposes. To make sure these products weren't used for consumption, the government deliberately added poisonous chemicals to the mix.

Unfortunately, this didn't stop bootleggers from making booze or thirsty consumers from drinking it, either. By 1925, alcohol-related deaths had increased to 4,000 per year.

And even though Prohibition was eventually repealed, its effects on the whiskey industry lasted much longer than the time it was enforced.

### 6. Following the repeal of Prohibition, the industry fell firmly into the hands of the Big Four. 

Prohibition's dry spell ended in 1933, and afterward many Americans celebrated with a glass of whiskey. But they didn't realize that the world of whiskey had been forever changed.

Before Prohibition, both large and small distilleries operated across the United States. Over half of these distilleries were forced to close after 13 long years of being unable to sell alcohol legally. Even companies that did try to restart their businesses after repeal struggled and often failed.

The companies that did succeed in the market after Prohibition were the Big Four. These companies had the resources to control the industry by shutting down the competition and buying up or squeezing out of the market smaller rival brands.

These companies also had the capital to roll out extensive marketing campaigns, the beginning of a rebranding effort to change the bad reputation distilled spirits had that led to Prohibition in the first place. Instead of a low-quality libation tied to violence and corruption, whiskey now was a luxurious beverage for refined connoisseurs.

Interestingly, given that there was a shortage of whiskey after repeal and that whiskey as part of its refinement process needs a few years in barrels, companies began to explore the production of new varieties of spirits in the 1930s.

Distillers like Stitzel-Weller started using wheat to make a lighter spirit that could be sold quickly without needing to age too long.

After years of fierce competition, the Big Four came together during World War II to help the war effort with a program called "Cocktails for Hitler," to produce industrial alcohol.

Indeed, once the United States entered the war, many distilleries diverted their alcohol for industrial purposes. The Big Four even collaborated and shared techniques to assist in the fight.

This newfound cooperation didn't last long, however. Soon after the war was over, the Big Four were once again fighting among themselves for industry dominance.

> _"Heritage was important, but repeal also offered a perfect chance to shed parts of history that didn't fit in with the future."_

### 7. When vodka arrived in the US market, bourbon fought back by gunning for the luxury niche. 

Bourbon in America traditionally was a drink for blue-collar workers. While rich people might enjoy bourbon on occasion, the spirit was primarily seen as the preference of soldiers and hard-working men. But all this started to change when vodka arrived on the market.

Vodka was first imported to the United States in 1946. Early on, the spirit was mostly popular in neighborhoods and cities with large Eastern European populations. But soon cocktail culture began to grow, with one drink emerging as a popular favorite: the Moscow Mule, a mix of vodka, lime and ginger ale.

American baby-boomers saw vodka as a "hipper" alternative to bourbon, now associated with old-fashioned dads and "square" uncles — an image they wanted to avoid. Vodka was easier to drink and also easier to mix with other ingredients in cocktails. What's more, alcoholics liked vodka more as it was essentially odorless, and couldn't be detected on the breath.

After being sold in the US market for just 30 years, vodka even began to outsell bourbon, the traditional American spirit.

Something had to be done, so the whiskey industry came up with a plan: to rebrand bourbon as a luxury drink.

As vodka's popularity continued to rise and bourbon's star fell, Maker's Mark, a small distillery in Kentucky, decided to try something new. It changed its domestic image into an international brand, focusing on becoming an expensive, luxury drink for discerning tastes around the world.

This marked a new trend that many other distilleries followed. And it worked! Many Europeans, unaware of bourbon's humble beginnings, considered bourbon a fancy, imported spirit and boosted sales.

Soon, whiskey producers were selling their products for higher prices internationally.

Today bourbon is a drink that is enjoyed by people from all walks of life. So when you savor your next sip, now you can enjoy bourbon's complex history along with its complex taste!

### 8. Final summary 

The key message in this book:

**Bourbon is the quintessential American spirit, yet its history is storied and often unsavory. From its humble beginnings as a fiery distilled spirit for frontiersmen to its bootleg status during the years under Prohibition, this corn-based whiskey was often synonymous with corruption, tax evasion and even murder. Yet over time bourbon's image has changed, to become the internationally popular alcohol it is today.**

Actionable advice:

**Take a moment and relax.**

Today's modern life is often chaotic. If you want to make a connection with the past, get yourself a high-quality bottle of bourbon and relax. A great bourbon takes many years to age, so take the time out to savor what it means to make a spirit this refined **.**

**Got feedback?**

**We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!**

**Suggested further reading:** ** _Meet Your Happy Chemicals_** **by Loretta Graziano Breuning**

_Meet Your Happy Chemicals_ (2012) provides a detailed introduction to the four chemicals responsible for our happiness: dopamine, serotonin, endorphin and oxytocin. The book explores the mechanics of what makes us happy and why, as well as why some bad things make us feel so good.
---

### Reid Mitenbuler

Living in New York, Reid Mitenbuler writes about whiskey and US drinking culture for _Slate, Whiskey Advocate, Saveur_ and _The Atlantic_. _The Bourbon Empire_ is his first book.

