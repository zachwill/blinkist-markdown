---
id: 5e68d02b6cee070006d37ad9
slug: boys-and-sex-en
published_date: 2020-03-12T00:00:00.000+00:00
author: Peggy Orenstein
title: Boys & Sex
subtitle: Young Men on Hookups, Love, Porn, Consent, and Navigating the New Masculinity
main_color: 753A8E
text_color: 753A8E
---

# Boys & Sex

_Young Men on Hookups, Love, Porn, Consent, and Navigating the New Masculinity_

**Peggy Orenstein**

In _Boys & Sex _(2020), acclaimed journalist Peggy Orenstein asks young men all about their sex lives — and gets the uncensored answers. Drawing on two years of interviews with young men, academics, psychologists, and sex educators, Orenstein offers an unfiltered yet informed perspective on young men's relationship to sex, from the pleasurable to the problematic.

---
### 1. What’s in it for me? Get teenage boys’ uncensored perspectives on sex. 

Adolescent boys don't exactly have a reputation for being communicative — especially when it comes to talking about sensitive topics like sex. Nonetheless, over the course of two years, hundreds of adolescent American boys engaged in conversation with award-winning journalist Peggy Orenstein, talking openly about their sex lives. 

Their frank discussions touched on a range of topics, including hookup culture, gender expression, sexual orientation, and the #metoo movement. They also tackled triggering subjects like hardcore pornography and campus sexual assault. The result? A compelling picture of contemporary teen masculinity and sexuality.

These blinks lay out Orenstein's most salient findings, presenting you with a snapshot of teen male sexuality in the United States. The landscape of sex, sexual culture, and masculinity is rapidly changing. These dispatches from the frontlines of adolescent male sexuality illuminate a rarely mapped portion of that landscape. 

In these blinks, you'll learn

  * how explicit pornography is affecting IRL sexual encounters;

  * what college hookup culture is and how it works; and

  * what teen boys secretly wish their parents would tell them about sex.

### 2. Society still celebrates a stereotypical image of masculinity, and this stereotype is negatively impacting male health. 

Over the course of two years, journalist Peggy Orenstein asked hundreds of teenage boys the same question: Which qualities does the "ideal woman" possess? She was expecting to hear a litany of standard stereotypes — blonde hair, big boobs. But the boys' answers surprised her.

When describing this ideal woman, they used words like "leadership," "ambition," and "intelligence," among other positive descriptors. Oddly enough, it was when asked to describe the "ideal man" that the boys stuck to stereotype, listing qualities like emotional reserve, strength, and silence.

**The key message here is: Society still celebrates a stereotypical image of masculinity, and this stereotype is negatively impacting male health.**

The stereotypical image of the strong silent male is tangibly, and negatively, affecting male mental health. Why? Well, _emodiversity_ — the capability to feel and express a spectrum of emotions — is key to healthy mental functioning. And the pressure to suppress emotions severely limits many boys' emodiversity.

A 2018 survey of over 1,000 US adolescents found that young men who strongly identified with male stereotypes were a staggering six times more likely to report harassing girls and bullying other boys. Moreover, they were more prone than other males to binge drinking, risk-taking behavior, and depression.

As these worrying statistics show, when men ignore their feelings, the consequences can be extremely negative. Yet boys are strictly socialized not to acknowledge their feelings.

The process of gender socialization starts at home. Studies consistently show that both mothers and fathers deploy less varied emotional vocabulary and display less empathy when talking with sons than with daughters.

And it continues at school. Psychologist Judy Y. Chu has shown that preschool-aged boys are often still comfortable showing their need for emotional connection with others. By kindergarten, however, most boys have learned to hide that need. And by the age of 14, the majority of boys are afraid that sharing their feelings with their male peers will make them look weak.

The 2018 survey found that girls believed there were "many ways to be a girl." Boys, on the other hand, reported feeling pressured to be strong, silent, and unemotive. Sometimes this pressure came from adult authority figures — people who discouraged the boys from expressing sadness or frustration by telling them to "man up." Sometimes it came from peers who applied feminized slurs, like "pussy" and "bitch," to boys who did express emotion.

The image of the strong, silent male isn't something to be celebrated; it's a toxic gender stereotype that is harming young men.

### 3. Early exposure to pornography is shaping boys’ sense of pleasure, arousal, and sexual self-esteem. 

People don't discuss it. People barely even acknowledge it. It's as taboo as taboo can be — and yet, online, it's as common as cat gifs. 

We're talking, of course, about pornography.

Since the advent of high-speed internet, websites like PornHub — think YouTube, but for porn — have proliferated. For today's adolescent boys, vast quantities of explicit pornographic material are but a click away. What's more, on average, most young boys are exposed to pornography long before they first have sex — or even begin to masturbate.

**The key message in this blink is: Early exposure to pornography is shaping boys' sense of pleasure, arousal, and sexual self-esteem.**

Pornography isn't always harmful. And cultivating healthy sexual curiosity in adolescence is both natural and positive. But the fact is, millennial and Gen Z boys will be the first to go through adolescence with such easy access to so much pornography — and the evidence suggests that it's doing more harm than good.

Not only is pornography more available than ever before, it's far more explicit. In a US survey, boys were found to be three times more likely than their fathers to have watched hardcore pornography featuring potentially triggering themes such as BDSM, gang rape, and double penetration.

The boys Orenstein interviewed all accessed online pornography regularly. As they became sexually active themselves, they found the conventions of "IRL sex" hard to reconcile with those of online pornography. They reported worrying about the size of their penises and about whether they'd be able to perform the acts they'd seen online. And they said that engaging in those same acts often led to regret and shame when they were less pleasurable than expected — hardly surprising, considering that extreme sexual acts can be unpleasurable even for professional performers. 

Some struggled to get sexually excited by real-life partners. After years of refining the features that aroused them — right down to nipple shape and color — through search filters, they had trouble getting turned on by any features that fell outside those parameters.

Lastly, and most disturbingly, while all the boys Orenstein spoke with understood the need for consent, many admitted to watching "unwilling" pornography — where the female actor resists or is coerced into a sex act before "succumbing" to its "pleasure."

It's time parents and educators frankly addressed pornography when discussing sex with boys. Pornography isn't necessarily harmful — but, when it warps real-life sexual expectations and encounters, it certainly can be.

### 4. Hookup culture doesn’t allow for emotional intimacy. 

Dating apps, booze-fueled one-night stands, awkward mornings after — today's teens and young adults have seemingly "swiped right" on _hookup culture_, **** a culture that encourages casual sex but frowns on emotional attachment or commitment.

But this culture comes with real costs. 

**In this blink, the key message is: Hookup culture doesn't allow for emotional intimacy.**

For many of the boys Orenstein spoke to, casual hookups weren't emotionally rewarding. Then again, emotional intimacy wasn't the point. In fact, in heterosexual hookup culture, staying detached and casual is key. Sociologist Lisa Wade says this culture calls for "compulsory carelessness." The goal here is sex without vulnerability or attachment. Boys who failed to keep it casual — for example, by expressing feelings for their partner or doubts about their own performance — reported being ostracized and ridiculed by their male peers.

Why are boys so keen to avoid attaching emotion to sex? Perhaps because the stereotype of the sexually active — not to mention sexually confident — college-aged male is so persistent. Couple this stereotype with the notion that feeling or expressing emotion isn't manly, and mix in apps that make hooking up as easy as ordering a pizza, and you've got the recipe for hookup culture.

Worryingly, although perhaps not surprisingly, an emphasis on transactional sex encourages boys to treat girls as sexual objects. During sex, college-aged boys tend to be unresponsive to their partners' needs. In a recent Online College Social Life Survey, 51 percent of girls reported that they climaxed in their most recent hookup. Meanwhile, 81 percent of boys reported climaxing. What's more, after such encounters, many boys admitted they were cold and distant toward their partner in order to preserve their masculine image.

Finally, many boys reported feeling pressure to perform sexually, despite their relative sexual inexperience. To overcome their sexual self-doubt, they drank heavily before hooking up. Alcohol has been shown to affect judgment and impede the ability to read social cues — like a potential hookup partner's hesitance, for example. Binge drinking can lead boys to make poor decisions and engage in sexual misconduct. Worst of all? The consequences of those decisions are borne by their sexual partners — or victims, as the case may be — as well as by the boys themselves.

Sex doesn't have to be a deep emotional experience. But a culture that doesn't allow for _any_ emotional intimacy within sexual encounters negatively impacts adolescent boys and their sexual partners alike.

### 5. For gay, queer, and trans boys, sexual exploration and expression is still fraught – and that needs to change. 

Across the United States, tolerance is trending — particularly when it comes to LGBTQI+ acceptance. In 2004, for example, only 31 percent of surveyed Americans said they supported same-sex marriage. By 2016, that percentage had jumped to 61.

But while tolerance is increasing, it isn't universal. LGBTQI+ teens are still more likely than their straight peers to be affected by mental-health issues, domestic violence, and sexual assault. What's more, a lack of openness and education around non-penetrative sex can make it difficult for queer teens to explore and embrace their sexual identity during adolescence.

**The key message in this blink is: For gay, queer, and trans boys, sexual exploration and expression is still fraught — and that needs to change.**

In the United States, same-sex marriage is legal and television series like "Queer Eye" attract millions of viewers. But that doesn't mean life is easy for queer boys. A nationwide trend for LGBTQI+ acceptance may not concretely improve the lives of LGBTQI+ teens living in conservative or evangelical parts of the country. When Orenstein spoke to gay boys from these backgrounds, they all reported feeling unsafe exploring their sexuality.

In rural, conservative, or evangelical parts of the nation, Orenstein found that gay boys are turning to hookup apps like Grindr — often while they are still underage. Why? In some isolated communities, Grindr is the only way to connect with others of the same sexual orientation. For closeted teens, the app offers a "safe space" for exploring their sexuality anonymously. Ironically, in searching for a "safe" space for sexual exploration, many gay adolescent boys engaged in risk-taking behavior: lying about their age, for example, or meeting strangers for sex.

Trans boys also faced challenges when it came to sexual exploration. The trans boys Orenstein interviewed were frustrated by the general lack of information about non-penetrative sex. They reported feeling doubtful about how they, or their prospective partner, could experience sexual pleasure. These doubts caused them to feel embarrassment and anxiety with prospective partners.

The truth is, while there is growing support for LGBTQI+ rights, queer-friendly modes of sex are often left out of the conversation. Sex education in the United States tends to focus on penetrative sex, with an emphasis on reproduction.

Inclusive sex education would certainly benefit LGBTQI+ teens. But, more than this, open and inclusive education about the many, many ways in which people can find sexual pleasure would benefit _all_ adolescents.

### 6. There is a double standard when it comes to the sexual conduct of white boys and black boys. 

From hip-hop to streetwear, black culture is prized by teen boys. But are black teen boys themselves prized in society?

Young black boys face the same struggles with toxic masculinity as their white peers. But the way their blackness is perceived — both by their peers and by society — can further complicate their sexual expression and experiences.

**In this blink, the key message is: There is a double standard when it comes to the sexual conduct of white boys and black boys.**

Many of the college-aged black boys Orenstein spoke to felt accepted within the college environment. But they wryly noted that sometimes they felt accepted because of stereotyped qualities ascribed to them by their white peers — like sexual prowess, confidence, coolness, and athleticism — rather than the qualities they prized in themselves.

These stereotypical attributes fit neatly within a gender-conforming image of the "ideal man." However, the black boys Orenstein interviewed all noted that, in the eyes of their white peers, these same qualities could also be perceived as threatening. Their confidence could easily be interpreted as aggression, their athleticism as physical menace. 

Racist stereotypes of black males as sexual predators have persisted in America for centuries. The black boys Orenstein spoke with all voiced the opinion that, were they to be reported for sexual misconduct, they would face harsher punishment than their white peers.

Statistics on college-campus sexual assaults in America aren't broken down by race. However, there is evidence that, at majority white institutions, black men are reported for sexual misconduct at proportionately far higher rates than white men. They also show that white girls are more likely to report a black boy for sexual misconduct than a white boy. Black girls, on the other hand, were both proportionately more likely to experience sexual assault than their white female peers _and_ far less likely to report any assault.

Claims of sexual misconduct should always be taken seriously — regardless of the alleged attacker's race. And girls should always feel safe reporting sexual assaults. But the black boys Orenstein spoke with didn't feel safe, either. They reportedly hesitated to hook up with girls at parties — even girls who expressed explicit sexual interest in them. Racist stereotypes, coupled with structural inequality, limit their chances for productive, healthy sexual experimentation.

### 7. Many adolescent boys aren’t correctly obtaining sexual consent. 

The night is young and so are you, and you just want to dance, dance, dance. You've never been to such a good party before. All your best friends are here, and you've had two — or is it three? — stiff drinks. And now, suddenly, an attractive stranger grabs your hand and drags you to the dance floor.

You dance, you talk, you end up going home together. Now what? Well, your answer to this question might depend on your gender.

**The key message in this blink is: Many adolescent boys aren't correctly obtaining sexual consent.**

A 2016 survey by research group Confi asked 1,200 college students what they would expect to happen if they danced with someone at a party — then went home with that person. 45 percent of men said they would expect to have sex, compared to 30 percent of women.

When it comes to sexual signals, there is a perception gap between men and women, as the story of the comedian Aziz Ansari illustrates. In the wake of 2017's #metoo movement, a 22-year-old woman using the pseudonym Grace recounted an uncomfortable sexual experience with Ansari. The two had gone on a date, he'd asked for sex, she'd refused, he'd pressed her — and, eventually, she'd given in. After this story went public, many other women shared similar experiences of being pressured into giving consent.

Let's be clear: coerced consent is not consent. But do teen boys understand this? Theoretically, yes. But they aren't applying theory to practice.

In 2015, sociologist Nicole Bedera surveyed over 1,000 adolescent boys about consent. All the boys understood the concept of enthusiastic consent and that there were situations in which consent couldn't be given. Yet, during the lead-up to their most recent sexual encounter, only 13 percent had engaged in a conversation about their intentions. And the majority of these conversations were initiated by the respondents' partners.

Boys also tended to stretch the concept of consent to accommodate situations where verbal consent had not been obtained. They interpreted nonverbal cues, like smiling or making physical contact, as signs of sexual attraction. To some boys, these cues even _constituted_ consent.

It seems that, when there is an expectation that sex will take place, boys can become careless with the concept of consent — or obtain it through coercive means. This carelessness can have traumatic consequences for their sexual partners. Parents and educators have a duty to tell teen boys that they are never entitled to sex, and that coerced or nonverbal consent isn't consent at all.

### 8. Boys can be sexually vulnerable, too. 

Boys will be boys. Under the cover of this well-worn phrase, boys can get away with just about anything — from pulling girls' ponytails on the playground to getting into fistfights. 

But there's one thing they can't get away with: being vulnerable. 

Society conditions us to see men as aggressors, not victims. And it encourages men to act as aggressors, not victims. But, as usual, these stereotypes don't tell the whole story.

**This blink's key message is: Boys can be sexually vulnerable, too.**

Sexual assault on college campuses is a serious problem. But the victims of sexual assault aren't always female. A 2017 study of students at Columbia University found that 22 percent of students had experienced a sexual assault in their time on campus. Of that 22 percent, 80 percent were female. The rest were male.

In the Columbia study, many male victims reported experiencing unwanted sexual advances, inappropriate contact, or groping. Some also reported penetrative assault, where they were physically incapacitated or verbally goaded to engage in penetrative sex with their assaulter. In 60 percent of these cases, the assaulter was female.

In the aftermath of sexual assault, boys often find their trauma isn't taken seriously. Why not? Because of toxic masculine gender norms that suggest men always want sex, no matter the context. Boys that described their assaults to their peers were met with congratulations rather than sympathy.

The disparity between boys' and girls' experiences of assault is particularly acute in cases of statutory rape. A male adult teacher has an affair with an underage girl? He's a pervert who deserves prison time. A female adult teacher has an affair with an underage boy? Lucky boy!

The idea that young boys always want sex, and therefore can't be victims of sexual assault, is enshrined in American culture. From _The Graduate_ to _American Pie_, movies portray relationships between older women and adolescent boys in a positive — even comic — light.

But the reality is far darker. In the United States, up to one in six boys will experience sexual assault before he reaches the age of 18. Yet our narrow image of masculinity doesn't allow us to imagine a male victim — a male who is powerless, vulnerable, or sexually unwilling. Boys who are the victims of female abusers are especially antithetical to the masculine norm.

As long as our notions of masculinity don't allow for male victims, we'll continue to disenfranchise male assault survivors.

### 9. Parents and educators need to talk openly and regularly with boys about sex. 

It's hard to know what kind of men today's boys will grow up to be. After speaking with a broad cross section of adolescent boys, Peggy Orenstein unearthed many positives in contemporary boyhood. Today's boys are informed about consent, and they're progressive when it comes to embracing feminism and LGBTQI+ rights. At the same time, they're taught outmoded ideas about masculinity, socialized to repress their emotions, and exposed to more graphic pornographic content than any generation that came before them. Moreover, they live in a country whose president bragged about "grabbing women by the pussy," and where a high-profile judge was elected to the Supreme Court after brushing off serious allegations of sexual assault.

How can parents and educators help these boys become good men — men who respect themselves, their sexual partners, and the people around them?

**The key message in this, the final blink, is: Parents and educators need to talk openly and regularly with boys about sex.**

As well as talking openly with boys about sex, parents and educators need to expand the parameters around sex as a discussion topic. Sex isn't just penetrative sex. Sex can mean touching your partner for pleasure, engaging in oral or anal sex, or digitally stimulating your partner — and that's just for starters!

More than that, sex isn't just a physical act. Talking about sex with boys means talking about consent, respect, gender socialization, gender expression, and sexual orientation. It means talking about how they can physically and emotionally fulfill their partner's wants and needs — and encouraging them to get in touch with their own wants and needs, too.

If you're a parent or parent figure, don't fall into the trap of giving your boy "the talk." Your obligations to teach him about sex don't start and end with one discussion. Make sure you discuss sex, and all the emotional and ethical issues around it, frankly and regularly.

If all of this strikes you as potentially awkward and uncomfortable, well, it probably will be — especially if you've also been socialized to repress your emotions and avoid discussing sex with kindness and honesty. But, if we want young men to value honesty and emotional openness, we need to model it for them ourselves.

Today's boys are filled with potential, yet beset by challenges. If they are to meet those challenges and reach that potential, we must guide them — by communicating, by caring, and by having the courage to face our own discomfort.

### 10. Final summary 

The key message in these blinks:

**When it comes to sex, today's teen boys are navigating complicated terrain. Some elements of today's teen sexual culture, like on-demand explicit pornography and a proliferation of hookup apps, are utterly contemporary. Others, like male stereotypes around sexual performance, present age-old problems. To overcome toxic masculinity — and thrive sexually and emotionally — adolescent boys need to learn emotional openness and cultivate vulnerability.**

Actionable advice:

**Have an IRL discussion about online porn.**

Talking sex with an adolescent boy? Don't neglect to bring up masturbation. It's a healthy way to explore sexual desires. But when teen boys rely on internet pornography to get aroused, they may only connect arousal with the extreme acts they see online. The author recommends encouraging teen boys to explore masturbating without pornography as well. When boys use their imaginations to get aroused, they start to get a sense of what they find sexually exciting outside the parameters of pornography.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Girls & Sex, _****by Peggy Orenstein**

Learned a lot from _Boys & Sex_? Why not get the other side of the story with the blinks to _Girls & Sex_, from the same author. Like adolescent boys, adolescent girls are navigating the complex world of contemporary sex and relationships. 

Peggy Orenstein interviewed hundreds of girls to find out how girls are engaging in sex, and what they really think about it. Touching on topics that many parents and educators still find taboo — think masturbation, pornography, and casual sex — this is an up-to-the-minute and totally unfiltered insight into the sexual experiences of today's teen girls.
---

### Peggy Orenstein

Peggy Orenstein is an award-winning journalist, _New York Times_ best-selling author, and expert on gender and sexuality. Her book _Girls & Sex _broke new ground with its frank examination of the sex lives of contemporary American girls, and her TED talk on girls' sexual pleasure was a viral sensation.

