---
id: 583226832b89ea0004096a83
slug: braiding-sweetgrass-en
published_date: 2016-11-24T00:00:00.000+00:00
author: Robin Wall Kimmerer
title: Braiding Sweetgrass
subtitle: Indigenous Wisdom, Scientific Knowledge, and the Teachings of Plants
main_color: E5B249
text_color: 806329
---

# Braiding Sweetgrass

_Indigenous Wisdom, Scientific Knowledge, and the Teachings of Plants_

**Robin Wall Kimmerer**

_Braiding Sweetgrass_ (2013) offers a profound and insightful look at the relationship between humans and Mother Earth. With the growing concerns about climate change, deforestation and the depletion of our natural resources, it is more important than ever to reevaluate how we treat the world around us. Find out how the traditional practices of Native Americans can help us make the world a better place for future generations.

---
### 1. What’s in it for me? Learn how the fate of humanity is interwoven with the fate of the environment. 

Our environment is in peril. Global warming; the death of pollinators like bees; beach erosion; plants and species going extinct — the list of tragedies and threats goes on. Yet not all these phenomena are new; we can find parallels for the current crisis throughout human history. 

In these highly personal, memoir-like blinks we explore not only how the colonization of the Americas endangered both the continent's indigenous peoples and indigenous species but also how the wisdom of Native Americans could help us restore our environment.

In theses blinks, you'll learn

  * what sweetgrass is and how it's faith is intertwined with the Potawatomi people;

  * that reciprocity is a guiding principle among the Potawatomi people; and

  * how learning to give back could be the salvation of our environment.

### 2. As part of a Native American family, the author was raised in two very different worlds. 

Like many Native Americans, the author, Robin Wall Kimmerer, has experienced a clash of cultures. Modern America and her family's tribe were — and, to a certain extent, continue to be — at utter odds.

Kimmerer is Potawatomi, which, like many other Native American tribes during the nineteenth century, suffered through terrible conditions and harmful government policies as the United States expanded. Many tribe members suffered tragic deaths as they were forced on deadly marches to relocate to new lands.

Kimmerer's grandmother was one of the Potawatomi who was given citizenship and legal protections as a landowner in the state of Oklahoma.

Kimmerer spent a good deal of time with her grandmother, and she even attended Potawatomi gatherings. But, for the majority of her childhood, she lived in upstate New York. As she grew up, the cultural differences between the Potawatomi and modern American society became very clear.

One significant difference was how people treated nature, especially the food it provides. Often Kimmerer would go out to a nearby field and pick wild strawberries after school. The author sees these kinds of offerings as the world's _gift economy_ — things that are given to us without the expectation of any payment in return.

But it is part of Potawatomi culture to show gratitude for gifts like this by offering reciprocation.

For strawberries, this means going back to the fields after berry season ends to find seedlings and prepare new plots of land to plant for more to grow.

With this form of reciprocation, humans form a mutually beneficial relationship with nature that's not unlike a bond between two people: They take care of each other not because they have to but out of love.

However, she found out first-hand that modern America doesn't practice the gift economy.

Growing up, Kimmerer had a small job picking strawberries at a local farm where the owner strictly prohibited the eating of any strawberries without paying for them. So if she wanted to enjoy any of the farm's fresh strawberries she would have to return most of her money right back to where it came from.

### 3. The decline of sweetgrass, the sacred plant, mirrors the history of Native Americans. 

Another of nature's gifts is the aromatic herb sweetgrass, which plays an integral role in traditional Potawatomi life.

This stems from Potawatomi mythology and the tale of Skywoman, an angelic figure who descended from the heavens to spread life across the land. And the very first plant that Skywoman brought to life? You guessed it — sweetgrass.

This plant is considered sacred. For spiritual rituals, tribespeople braid it to look like Skywoman's hair, and, for day-to-day use, they weave it into baskets.

The act of basket-weaving actually has a very spiritual meaning for people as well: By using sweetgrass to make something new, the act itself is seen as honoring Skywoman, the creator.

Unfortunately, sweetgrass is becoming harder and harder to find due to the introduction of European plants and weeds. These plants, the gift of European colonists, are characteristically invasive and rapidly take over lands that were once home to sweetgrass. Though sweetgrass has been around for millennia, it's currently at risk of vanishing altogether.

You may have noticed that the history of sweetgrass parallels that of the Potawatomi people themselves. Just as sweetgrass was displaced by the arrival of foreign plants, native tribes across the land were uprooted from their ancient homes by a growing number of colonists.

Sadly, the indigenous culture and language were also being forcibly supplanted. An untold number of children were torn from their families and placed in government schools where they weren't allowed to speak their native language or engage in any of their traditional practices.

All of this did lasting damage to both the land and the tribes. Reversing this damage is a matter of reevaluating our relationship to Mother Nature and the world in which we live.

And, as we'll see in the following blinks, we can do this by taking a closer look at what we can learn from indigenous cultures, and how we can incorporate this knowledge into modern-day life.

### 4. The relationship between humanity and nature should be based on gratitude and reciprocity. 

There is a lot to be learned from indigenous culture, especially the way in which they've developed societies that are based on reciprocity — the infinite and cyclical relationship humans have with one another and the world around them.

These cycles are actually an inherent part of being human.

Paula Gunn, an influential anthropologist, has described how reciprocity plays out in the lives of women:

It begins with the _Way of the Daughter_ — when a young girl is taught the ways of the world by her parents. The girl learns how to take care of herself, grows into womanhood and enters the next stage: the _Way of the Mother_. Here, she passes on the love and knowledge she learned from her parents onto the next generation of children.

In the third and final stage, the cycle of reciprocity is completed as women grow older and assume the _Way of the Teacher_. During this period, women act as the role models to whom people in the community, including parents, turn for advice.

This is the kind of loving and caring relationships that we need to spread into the world at large.

It's the attitude that Kimmerer put to use when she came across a local pond that was so polluted that large amounts of algae were forming and causing birds to get stuck and trapped. For over twelve years she cared for the pond in a motherly way, visiting it regularly, removing the algae and making sure it stayed clean.

This kind of caring creates its own cycle. With the pond clean and clear, the birds can thrive. There's also clean water to swim in and, as the pond runs downstream, other ponds and lakes end up healthier, too.

This behavior is quite different from how modern society treats nature today.

With practices such as mining non-renewable resources, there are no reciprocal benefits; the resources are irreparably depleted, and, in the process, both the well-being of the planet and the miners is harmed.

### 5. In order to maintain a successfully sustainable environment, we must act in harmony with nature. 

Only a few decades ago, people began changing their attitudes toward the environment and becoming more accountable. This is good, and it's led to good things, like increased and widespread recycling standards. But there's still a lot that needs to be done.

For the Potawatomi, sustainability is the essential goal, and this is always achieved through reciprocity.

This is something the European colonists didn't understand. After arriving in the Americas, they were confused to discover that the Native Americans only harvested half of their rice crops. But they weren't wasting the rice; they were practicing reciprocation.

By leaving half of the rice unharvested, they were giving back to the land that was providing for them; other animals could graze the land, eat the rice and make sure that the ground would be well-seeded for next season's harvest.

This is called an _honorable harvest_ : a sustainable way of farming that means you only take what you need to survive and leave the remainder as a sign of gratitude _._

Unfortunately, reciprocity and sustainability are not a main concern in our food and farming policies.

The closest a few states have come to approaching this mindset is to create rules that only stipulate things you can't do, such as fishing non-adult trout. And generally, the only penalty for breaking the rules is a fine.

The honorable harvest, on the other hand, is not an agreement between the state and its law-abiding citizens; it's a set of harmonious principles shared by two sets of living things, humans and nature. We agree to take only what we need and leave enough for nature to regenerate, and in turn nature continues to provide food for us.

With this kind of reciprocal relationship in mind, we can begin to rethink how we approach sustainability. Rather than simply putting your waste paper in the correct recycling bin, consider the ways you can reciprocate the many gifts that the world's trees have given you.

Since deforestation is a serious threat to the planet, you can inform yourself and others about the dangers and even take part in local tree-planting programs.

### 6. We can achieve sustainability and reduce harmful practices by looking to traditional methods. 

Alongside her firsthand knowledge of Potawatomi traditions, Kimmerer is also a professor of environmental biology. This background provides the author with a unique perspective on how the world works since it combines multiple points of view that allow for greater understanding.

She put this knowledge to use in her botany class after she saw that her students weren't responding very well to the traditional academic approach.

To get them engaged, she decided to add a healthy dose of practical Native American teachings. And it worked. The students became more attentive, and now the first class in her course takes place in the garden, where students are taught about the agricultural technique known as _the Three Sisters_.

But more than just an interesting legend and farming technique, the Three Sisters shows us how traditional methods can still be used today to help crops thrive without harmful modern techniques.

The Three Sisters is a method of beneficial combination planting that has its origins in the mythological tale of three sisters who came to a village seeking shelter during a winter storm.

While the villagers didn't have much to offer, they did share what little food they had with the sisters. To show their gratitude, the sisters revealed themselves to be the embodiment of corn, beans and squash, and provided the village with a bounty of their seeds.

These seeds also reveal themselves to be perfectly suited for being planted and growing next to one another as each one helps the others:

The fast growing corn has stems that provide vertical support for the beans to wraps its leaves around; these leaves then help trap moisture and encourage the corn to continue growing; and the baby sister, squash, provides defense, keeping harmful insects away from the beans and corn with its sharply pointed leaves.

But instead of sustainable techniques, like planting foods that naturally help and protect each other, we practice unsustainable techniques such as spraying gigantic cornfields with toxic insecticides.

Some of these sprays harm other animals and kill the bees that play a vital role in pollinating the plants we rely on.

> _"I envision a time when the intellectual monoculture of science will be replaced with a polyculture of complementary knowledge."_

### 7. To protect our future, we must teach the next generation the importance of gratitude and respect. 

Another threat that is related to the poor way in which we've treated the world's resources is climate change. In the face of this danger, it is more important than ever to change our priorities.

Much of our hope lies in how the next generation recognizes the importance of caring for the environment, and it's our responsibility to make sure they grow up with a sense of gratitude for nature.

There's one simple way we can help: Many schools pledge allegiance to the flag every morning, but imagine another pledge where children promise to show gratitude toward nature and the land they live on.

Native American schools are already using this revolutionary idea by having children recite a _thanksgiving address_ to thank Mother Earth for providing us with food, water and shelter.

We could have a whole generation of children begin each day with a sense of gratitude, and instead of feeling the desire to consume more things, they could feel the need to give back to nature.

Such a morning address would also help instill an attitude that will motivate them to go out and change the world, rather than simply complain about the way things are. And if we're going to fight against global warming, we need people who are willing to take action.

In New England, for instance, we need people who will protect the maple trees.

These trees do more than provide delicious syrup for your pancakes; they also provide wood for fireplaces and remove CO2 from the air. But if climate change continues at its current pace, in 50 years New England will be too warm for the trees to exist.

New Englanders need to stop complaining and start getting politically active if they hope to save the maple trees that have provided so much for them. People could help by raising awareness and helping political groups that lobby the government to issue higher carbon taxes, which can force businesses to start changing their ways.

The lesson of the Potawatomi is a simple one: only by giving back in the present will we be able to continue taking in the future.

> _"If what we want for our people is patriotism, then let us inspire true love of country by invoking the land herself."_

### 8. Final summary 

The key message in this book:

**One essential element of many indigenous cultures is the reciprocal relationship between humanity and nature. This means that when we take gifts given to us by nature, we should reciprocate the gift in the form of gratitude or giving back. By treating nature as if it were a loved family member, we can create a sustainable environment for future generations to enjoy.**

Actionable advice:

**Plant a garden!**

It's not only a cheap and easy way to grow your own food (it can be done on any windowsill); planting a garden can help instill a deeper understanding of how reciprocity between humans and nature works.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Hidden Life of Trees_** **by Peter Wohlleben**

Trees are engaged in countless complex cycles and they constantly struggle for water, light and their own survival. This struggle has led to some astonishing abilities: trees communicate with one another, give each other assistance, collaborate with fungi and other creatures, have memories and have even developed their own version of the internet!
---

### Robin Wall Kimmerer

Robin Wall Kimmerer is a writer, scientist and professor in the Environmental Sciences and Forestry Department at the State University of New York. The founder of the Center for Native Peoples and the Environment, she is also the author of _Gathering Moss: A Natural and Cultural History of Mosses._

