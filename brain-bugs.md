---
id: 538eee243761300007000000
slug: brain-bugs-en
published_date: 2014-06-03T11:45:00.000+00:00
author: Dean Buonomano
title: Brain Bugs
subtitle: How The Brain´s Flaws Shape Our Lives
main_color: None
text_color: None
---

# Brain Bugs

_How The Brain´s Flaws Shape Our Lives_

**Dean Buonomano**

_Brain_ _Bugs_ explores the inner workings of the human brain – both its incredible capabilities and its major flaws. Drawing on a wealth of examples and current research, the author illustrates how the brain's many blind spots and weaknesses lead us to make foolish decisions, recall false memories, and fear the wrong things. He also offers a number of suggestions for managing our brain bugs.

---
### 1. What’s in it for me? Find out how the brain works – and why it sometimes doesn’t! 

Have you ever wondered why remembering names or significant numbers, such as birth dates, is sometimes so difficult?

The answer is actually quite simple.

Even though the human brain is, in some aspects, like a computer, it doesn't store information in the same way.

In a computer, the storage and retrieval of information is a simple process. The brain, on the other hand, works quite differently: it stores information by associating different concepts.

So while a computer would have a hard time recognizing a particular face — a simple task for the human brain — it will certainly beat you when it comes to storing names and birth dates.

This is just one of many interesting features of the human brain. It's capable of fascinating feats, but at the same time it's home to some interesting, and sometimes strange, bugs — as the example of Beth Rutherford illustrates.

Rutherford asked her church counselor to help her to deal with stress. This counselor "helped" her to recall being raped by her father several times, being impregnated twice and being forced to abort the fetuses using a coat hanger.

As a result, Rutherford's father had to resign from his job — even though a later medical examination revealed that Beth was still a virgin at age 22. How are such false memories planted in the mind?

_Brain_ _Bugs_ explains how this can happen, as well as exploring many other interesting facts about how the brain works and why it fails.

In these blinks, you'll find out:

  * What the brain and the internet have in common;

  * Why computers find it hard to recognize faces, while humans have a difficult time doing basic math;

  * Why amputees often feel pain in their missing limb, and why some people believe that one of their hands is controlled by another being;

  * Why smokers often don't seriously consider the proven dangers of smoking;

  * And, finally, why certain spiritual beliefs originate in the brain.

### 2. The structure of our nervous system is somewhat comparable to a computer. 

Despite the many obvious differences, we often liken the human brain to a computer.

Why?

Much like a computer, the brain is an information-processing organ.

A computer consists of physical components (hardware) and the programs that run on those components (software), and the same is true of the human nervous system.

It's composed of hardware — the organs, such as the brain, spine and nerves — and it's programmed to direct our behavior and our thoughts, just like software.

Not only are there similarities between the computer and the brain, but the neurons and synapses of the human brain constitute a large network — much like the World Wide Web.

Neurons are information-storing cells, and a group of them is comparable to a web page, in that the individual cells are linked to one another via synapses.

Furthermore, just as a web page which contains no links to other pages will be very difficult to find, neurons which have no — or just a few — connections to other neurons will be hard to find, too.

In one study, for example, subjects were presented with images of famous politicians and actors. While the subjects looked at the images, a researcher observed the patterns of neurons activating in their brains.

What did they find?

Every image corresponded consistently with its own specific pattern of neurons. In other words, just like web pages, our neurons and the groups they form are focused on specific content.

Moreover, the human brain contains approximately 90 billion neurons and 100 trillion synapses, and each neuron is linked to many other neurons. The World Wide Web is similar: it comprises 20 billion web pages, with one trillion links.

However, as you'll learn in the following blinks, there are many major differences between the computer and the human brain.

### 3. A computer calculates numbers; the brain uses associations. 

You've probably browsed the web recently, clicking links here and there, only to be asked at some point to prove that you're not a computer. Suddenly you're presented with a series of letters or numbers to transcribe.

This is called a CAPTCHA, and the distorted characters it presents are not particularly difficult to read — if you're human.

While computers are designed to perform numerical calculations at incredible speeds, they have trouble with pattern recognition. In contrast, the human brain is a sophisticated pattern-recognition machine, but struggles with mathematical calculations.

Why is this?

Whereas a computer performs precise calculations, the brain connects related concepts with one another.

For example, computers are able to solve logarithms to many decimal places in the blink of an eye. Human beings, however — even extraordinary mathematicians — will never be able to perform calculations as quickly.

This is because the human brain is _associative_ — as illustrated by the following study.

A subject is shown a series of words and nonwords, and told to quickly decide which of them is an actual word. If the word "butter" is displayed, the subject's response time is 0.5 seconds. But if the word "bread" is shown before that, the person takes just 0.45 seconds to react.

Why?

The subject associates "bread" and "butter," which accelerates the thinking process.

In contrast, a computer would have to check the order of the letters to determine that it's seeing a real word. By its very design, a computer cannot identify any association between words.

However, computers have a certain advantage over the brain: they don't make mistakes.

As long there are no problems with its hardware and software, a computer performs exactly as it's programmed to perform. However, if there _are_ problems, the results can be disastrous for the computer — even to the point of total failure.

Our brains, on the other hand, _never_ function perfectly. Indeed, neurons have a failure rate of 15 to 50 percent. However, the associative nature of the brain means that it can compensate for the loss of some neurons without a problem.

### 4. The brain’s plasticity allows us to adapt to our environment. 

Generally speaking, the hardware in our computers doesn´t change. Rather, the computer in front of you advances only when a new operating system is installed.

The brain, on the other hand, is "plastic" — which means that its hardware develops, and not just its software. This "upgrade" is an automatic process: the development is not programmed, but rather is dependent on environmental conditions.

For instance, our learning is automatic. Consider, for example, that an infant is unaware that a furry, four-legged, purring creature is a cat. Yet when the child is told that such an animal is called a "cat," the child will immediately understand, as the neurons for the word "cat" and those for the image of a cat become connected to one another.

Another way that the hardware of our brain develops is that certain areas responsible for specific parts of the body can grow and adapt whenever necessary.

Every part of the body corresponds to an area on our "cortical map," and this map can change depending on our needs.

For example, a blind person often has superior hearing and sense of touch to a person who can see perfectly well. In other words, their remaining senses improve to compensate for their lack of sight.

In more technical terms, because a blind person no longer needs their cortical areas for visual processing, their cortical map changes. On the one hand, the blind person's visual cortex begins to assume non-visual tasks, and on the other, those areas responsible for hearing and touch expand because they are rendered more important by the lack of sight.

The brain's plasticity is what enables humans to adapt to their environment. But, as we'll see in the next blink, this ability can also cause some strange problems.

### 5. Brain dysfunction and injury can cause wide-ranging mistakes in our body-awareness. 

As we've seen, the brain and its flaws have a great influence on how we see and feel the world.

But they are also behind our self- and body-awareness, so that — for instance — we're able to recognize ourselves when we look in a mirror.

Sometimes, however, the brain is dysfunctional, and, in such cases, certain problems in our body-awareness can develop.

One such problem is that the brain fails to adapt the cortical map. Consider, for example, a neurological disease known as _phantom_ _limb_ _syndrome_ : When people have a limb amputated, they might continue to sense the missing limb — even to the extent that they feel a phantom _pain_.

Why does this happen?

Although the limb itself no longer exists, its corresponding neurons _do_, so they can be activated spontaneously. After some time, the cortical area responsible for the phantom limb might shrink, and — as a result — the syndrome will cease.

Other similar neurological diseases are caused by brain injuries. For example, some people experience _alien_ _hand_ _syndrome_ — the feeling that one or more of their limbs are controlled by someone else. Even though the limb is intact, such people simply cannot gain mastery over their "alien" hand.

The effects of this syndrome are bizarre: for instance, when the affected person opens a drawer with her "normal" hand, her "alien" hand slams it shut. Sometimes this person might even hurt herself unwittingly with the alien hand — for instance, using the alien hand, she might pull her own hair.

Another example of such disorder is _Capgras_ _syndrome._ A person with this disorder believes her friends or family members — even though she recognizes them — are actually imposters playing a role.

Other sufferers of this disorder don't trust their own reflection in the mirror, convinced that the person they see looking back at them is a stranger.

So far, we've looked at how the brain works and how this can lead to brain bugs. In the following blinks, you'll learn all about the most important of those bugs.

### 6. Our memories aren't always correct. 

The flaws in our brains (our "brain bugs") can have wide-ranging consequences — and not only for ourselves.

Often, the ultimate decisions of a criminal court are based on the testimonies of witnesses. While this might seem fair, the problem is that our memories can be erroneous.

Why?

First, whenever we recall a memory of some event or other, its content is subject to change as we tend to continually update and overwrite the specifics of our memories.

This can be a positive thing. For instance, whenever we see someone we know and haven't seen for a while, we update our memory of their appearance so we can maintain an accurate mental image of them.

But this process can also have a negative effect: a memory can be altered so drastically that it becomes an entirely false memory.

This was demonstrated by a study in which children were asked if they'd ever experienced certain "special" events — like a surprise birthday party or a ride in a hot air balloon.

The researchers — who were aware of the children's actual experiences, having interviewed the parents beforehand — listened as the children began to recall experiences they'd never had, talking about them as if they were real. In this way, the children's autobiographical memory was altered radically.

Second, whenever we are uncertain of our memories, we tend to make erroneous connections.

In another study, children were shown a video in which a female teacher is being robbed by a man. Also depicted in this video is a male teacher reading a book.

Once the children had watched the video, they were shown photos of various people and asked to determine whether one of these people was the thief, and, if so, to point to the correct photo.

In fact, a photo of the thief was not included in the "line up." However, the innocent male teacher _was_ included, and 60 percent of the children incorrectly identified him as the thief.

> _**"** Complaining that you have a bad memory for names or numbers is a bit like whining about your smartphone functioning poorly underwater."_

### 7. Our fears are often misleading. 

Of all the emotions, fear is one of the most powerful because it aids our survival.

But while fear is not considered a "brain bug" — because it helps to protect us from danger — our fears often have no basis in reality.

Consider, for example, that, in the United States, 3,200 people were killed as a result of terrorism and 180,000 were victims of murder between 1995 and 2005.

In contrast, in the same period in the United States, there were 300,000 suicides, 450,000 automobile-related deaths, and six million deaths linked to heart disease.

So while the fear of terrorism is widespread nowadays, our lifestyles, and their associated diseases and accidents, are far deadlier.

In other words: we often fear what we shouldn't, and we don't fear what we should.

Why?

One possibility is that we tend to fear whatever is not under our control. So while we feel that we have some control over our health, we believe we don't have any grasp over the actions of potentially violent strangers.

Another possibility is that, in former times, having a strong fear of wild animals and the unknown could mean the difference between life and death, and this trait remains active in humans. Today, however, stress — for example — is a far bigger problem than being attacked by a sabre-tooth tiger, so our sense of danger is clearly in need of an update.

While some fears are innate and hardwired, other fears are learned.

How?

Firstly, via conditioning. For instance, a child who is bitten by a dog might continue to fear dogs throughout her entire life.

Secondly, we learn many things by imitating others' behavior, and this extends to fear. When a child witnesses her parents' fear towards a particular thing, she might internalize that behavior and adopt the same response.

For example, if a mother's fear of spiders is obvious to the child, the child might develop the same fear — whether or not the spider is actually dangerous.

> **Reflect** **on** **your** **fears.  

** Whenever you find yourself in the grip of fear, remind yourself that we're hardwired through evolution to fear certain things that are actually no longer a relevant threat.

### 8. Our brains sometimes deceive us. 

Can you estimate the age of particular people correctly?

In one experiment, subjects were instructed to estimate the age of, first, the actor Brad Pitt (50), and second, American Vice President Joseph Biden (71). Then another group of subjects were asked to perform the same task, but this time the order was switched.

The result?

When the researcher asked for estimates of Pitt's age first, the estimated age of _both_ men was lower than in reality. But when Biden was presented first, the subjects estimated the age of both men to be two or three years older.

As this experiment illustrates, the brain can sometimes deceive us, making it difficult for us to estimate accurately.

For example, in one study, doctors had to estimate the probability that a woman with a positive mammogram actually has breast cancer

The researcher informed the doctors that (a) the probability of a woman having breast cancer is one percent; (b) there's a 90 percent chance that a woman with breast cancer will have a positive mammogram; and (c) women who _don't_ have breast cancer have a nine percent chance of having a positive mammogram ("false-positive").

Most doctors estimated the probability of a positively tested woman actually having breast cancer at more than 80 percent. But they were wrong.

The correct answer is actually just ten percent.

How is the figure calculated?

In a sample of 1,000 women, 990 will be healthy, but 89 (nine percent of 990) will receive false-positive test results. Furthermore, ten of the women actually suffer from breast cancer, but only nine receive positive mammograms.

Thus, in this sample, 98 women were tested positively, but only nine of those were correctly identified (approximately ten percent).

In addition to influencing how we reason, the brain sometimes deceives our senses. For instance, the brain is responsible for optical illusions, which we've all experienced at some point. For example, if you stare at a waterfall for a while, then turn your gaze toward a rock, it will appear as if the rock is falling.

Now that you've learned about several brain bugs, the following blinks will show you some of their consequences, and present a few ways of dealing with them.

### 9. Advertising exploits our brain bugs. 

As primatologists have observed, chimpanzees imitate the dominant members of their group **.** In one case, for example, when a dominant male had injured himself and developed a limp, the young chimpanzees began to imitate his movements.

But it's not just chimps who engage in this behavior: humans do it too. They also try to imitate successful people.

This particular brain bug is exploited by contemporary advertising. Just turn on the TV for a moment and watch a few commercials. You'll notice that the actors are usually good looking, and are portrayed as being admirable.

For example, until the 1950s, it wasn't uncommon for actual doctors to be used in cigarette commercials. Why? If the viewing public saw a doctor — a role model in perfect health — smoke, people would get the impression that smoking couldn't be that dangerous.

Or consider the example of using celebrities in advertisements. Celebrities have a high social status — they're rich and successful — so they're effective role models. Hence they're the perfect choice as actors in commercials, even though the advertised product and the celebrity's success are completely unrelated.

Another way that advertising exploits the brain's flaws is that it creates associations in our minds, between, on the one hand, a product, and on the other, certain values and mental triggers.

For example, companies use slogans and music in connection with their product, so that you automatically think of the product whenever you hear a certain song on the radio, or you hear a particular sentence — for example, Ford's slogan "Feel the Difference" and the range of cars the company sells.

Another example is the diamond and its associations with eternal love. Although it's often thought of as an old tradition, it's only since the mid-twentieth century that diamonds have been used in engagement rings — the result of an effective marketing campaign in which the rings were shown in Hollywood movies and an association was forged between the naturally tough diamond and enduring love.

### 10. Brain bugs influence our beliefs. 

One major difference between humans and animals is that humans always want to know _why_ certain things happen or exist.

And, thanks to the incredible capabilities of the brain, we're often able to arrive at an explanation.

However, when we can't find a scientific explanation, we tend to look for an irrational, sometimes supernatural one.

Why?

Because the brain has a direct influence on our relationship to spirituality.

This has been demonstrated by a study in which brain-tumor patients took a personality test before and after brain surgery. This test (the Temperament and Character Inventory) includes questions about the existence of spiritual forces and spiritual connections to other people — questions which relate to the personality trait, and self-transcendence.

After surgery, the study found that only one of the traits had changed: self-transcendence. When a section of the parietal cortex was removed, self-transcendence increased.

However, this also works the other way around: religious belief can suppress our rational mind.

Religion plays a specific role in human societies — it keeps a group together. In former times, it has even been essential to human survival: members of a religious group often share and abide by the same ethics, and accept such rules because they believe that otherwise they'll be punished by an all-knowing supernatural force, such as an almighty god.

Holding such beliefs, however, can have negative consequences on our ability to think rationally. Robyn Twitchell, for example, was just two years old when he died, having endured a five-day period of intense physical pain.

On the evening that the child began crying and squirming in pain, his parents were convinced that it wasn't necessary for him to be taken to hospital, as their prayers would bring him back to health. However, an autopsy confirmed that his condition — an obstructed bowel — could've been corrected with surgery.

Unfortunately this is not an isolated case: due to religious fanaticism, at least 172 children died in 1998 in the United States, 80 percent of whom could've been saved.

### 11. To some extent, we can debug our brains. 

Have you ever done your grocery shopping when you were hungry? If so, you probably noticed that you bought more food than you would if you were full. This is because our brain wants us to satisfy our hunger immediately. Of course, this situation can be easily avoided by making sure we shop for food only when we've recently eaten.

This is an example of _priming_, and it's a way to influence our decision-making positively. Technically speaking, priming refers to instances when the processing of a stimulus depends on a previous stimulus.

For example, in one study, subjects were allocated either positive words (e.g., "encourage") or negative ones (e.g., "disturb"), and then told to construct sentences from these words.

Before the experiment could continue, however, the subjects were instructed to talk to another researcher, who was deep in conversation with someone else.

Those participants primed with positive words either didn't interrupt the researcher at all, or they interrupted after some time. On the other hand, those primed with negative words interrupted the conversation much more quickly. When primed with positive words, we're far more polite, it seems.

Another approach to debugging our brains involves implementing regulations at a societal level that help to nudge us toward making the "right" decisions.

One of these is the _choice_ _architecture_ approach, where regulations guide us towards good decision making without restricting our freedom. An example of this is that cigarettes must legally carry health warnings, guiding people towards _not_ _smoking_, yet giving them the freedom to smoke if they wish.

Another example is establishing norms that guide people toward "doing the right thing" for society.

For instance, organ donors are always in demand. Yet, even though most people don't care whether their organs are used after they die, they don't register themselves as donors because they're unaware of its importance.

But if there were a regulation that made organ donation a social norm — with the choice to opt out — the attitude toward it would change and general awareness of its importance would increase.

As this shows, brain bugs are a two-way street: not only do they hold a great influence over us, but we have the ability to influence _them_.

### 12. Final summary 

The key message in this book:

**Though** **the** **human** **brain** **is** **capable** **of** **amazing** **things,** **it** **has** **many** **flaws,** **which** **the** **author** **calls** **"brain** **bugs":** **Our** **memories** **are** **imprecise** **and** **sometimes** **false;** **we** **fear** **the** **wrong** **things** **as** **a** **result** **of** **evolution;** **our** **sense** **of** **time** **leads** **us** **to** **make** **only** **short-term** **decisions,** **and** **our** **senses** **are** **easily** **deceived.** **However,** **although** **there** **are** **many** **different** **types** **of** **brain** **bugs,** **it** **is** **possible** **to** **learn** **how** **to** **handle** **some** **of** **them.**
---

### Dean Buonomano

Dean Buonomano is a neuroscientist and professor of psychology and neurobiology at the University of California. Widely recognized as an expert on the brain's sense of time, Buonomano has received plenty of media attention, and has been interviewed for many scientific journals and newspapers, such as _Die_ _Zeit_ , _Newsweek_ and _Scientific_ _American_.

