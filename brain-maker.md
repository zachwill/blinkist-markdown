---
id: 563783016234650007f20000
slug: brain-maker-en
published_date: 2015-11-06T00:00:00.000+00:00
author: Dr. David Perlmutter with Kristin Loberg
title: Brain Maker
subtitle: The Power of Gut Microbes to Heal and Protect your Brain – for Life
main_color: 62804C
text_color: 4E663D
---

# Brain Maker

_The Power of Gut Microbes to Heal and Protect your Brain – for Life_

**Dr. David Perlmutter with Kristin Loberg**

_Brain Maker_ (2015) explains the connection between your health and your microbiome — the bacteria in your gut. It shows you how even diseases that affect the brain such as Alzheimer's are actually influenced by your microbiome. Importantly, you'll learn how making some simple dietary decisions can help you keep disease at bay and make yourself healthier.

---
### 1. What’s in it for me? Find out how a troubled tummy could lead to disease, and how you can avoid it. 

It is said that the way to a person's heart is through his stomach. It turns out that the way to increased health and disease prevention may be through that exact same spot. 

Alzheimer's and autism, depression and obesity are all diseases that could potentially be stymied, if not cured, by the right balance of miraculous little creatures that live in our guts — known as the gut microbiome.

It turns out that the health of your brain is closely linked to the health of your gut. Gut bacteria provides your body with valuable vitamins and helps to ward off infection, not to mention other nasty bacteria that would rather trigger inflammation and lay siege to your system.

The good news is that you can have a say in which group of bacteria prevails, and in doing so, join a whole new world of disease prevention by being smart about what you eat.

Reading these blinks, you'll also discover

  * how the chemicals in your plastic notebook could make you fat;

  * how good gut flora may curb autism spectrum disorder symptoms; and 

  * why a tasty, turmeric-heavy curry can make your brain grow.

### 2. Your gut microbes can have a considerable impact on the size of your waistline. 

The human gut is colonized by tons and tons of bacteria. A thought that's not only strange, but also perhaps a little gross. Yet here's another strange fact: these colonies play a huge role in your health. 

Let's start with how much you weigh. Interestingly, the type of bacteria you've got in your gut can determine whether you stay slim or become obese. 

Two groups of bacteria are accountable for some 90 percent of your gut's microbiome — _firmicutes_ and _bacteroidetes_. 

While scientists don't know the "ideal" ratio of these groups, they do know that when you have more firmicutes than bacteroidetes, you can suffer from increased inflammation and potentially from obesity.

Firmicutes are expert at extracting energy from the food you eat, which means that they help you consume more calories. In contrast, bacteroidetes aren't so much involved with calorie extraction but work to break down plant fibers and starches.

Harvard researchers examined the connection of obesity and a person's microbiome, focusing on two groups: people living in Western countries and in Africa. Considering that obesity is virtually non-existent in Africa, any differences in microbiome were viewed as notable. 

And what researchers found was indeed notable. Africans had more bacteroidetes in their gut, while Westerners had more calorie-extracting firmicutes. So having more firmicutes may be at least partly responsible for the obesity epidemic in the West.

Your microbes not only help to keep you slim (or fat) but also help support your liver. Many foods contain environmental toxins; it's the liver's job to get rid of these once in your body. 

Yet a healthy gut can also support the liver in its work, which is why the gut is often called the body's "second liver."

Gut microbes help to neutralize toxins that reach the intestines, acting as a first line of defense. In doing so, the microbes take a bit of pressure off the liver, keeping it healthier!

### 3. The body’s inflammatory response protects you, but it can go haywire and cause damage, too. 

When an insect bites you, often the skin around the bite gets red and may itch. This redness is called inflammation; a process the body sets in motion to protect you and keep you healthy. 

Yet sometimes inflammation can get out of control and actually be harmful. 

Most people think that inflammation is just a patch of irritated skin or a swollen, sore throat — but it is often much more. The body's inflammatory response is there to support the immune system and help it fight infection or injury. 

But when this response persists without reason, it can lead to a wide range of illnesses such as diabetes, cancer, asthma, arthritis or even multiple sclerosis. 

But what exactly would trigger excessive inflammation in the body?

Some genes can contribute to excessive inflammation; yet for this to happen, these genes need to be "switched on." Thankfully there are things that you can do, such as getting enough sleep and eating healthily, that will keep these bad genes quiet while kicking into gear the more helpful genes. 

Too much blood sugar can also increase inflammation in the body, as high blood sugar levels can be toxic if your cells aren't able to process it. This situation can lead to _glycation_, a state in which sugar binds to proteins or fats, which causes a buildup of _advanced glycation end products_ (AGEs). And AGEs trigger inflammation.

So while a little inflammation can help you fight disease, too much can have negative consequences when it comes to your body and your health. 

But did you know that your gut microbiome can also trigger inflammation that potentially leads to mental health problems? Read more in the next blink.

> _"Death begins in the colon." — Elie Mechnikov_

### 4. If your gut and its microbiome aren’t in tip-top shape, your brain might be in trouble, too. 

Did you know that your brain and your gut are actually connected? If your gut microbiome is imbalanced to the point where it no longer is assisting the immune system in protecting the body, your brain could be in peril, too. 

Your gut is mostly busy absorbing nutrients from food during the day. While it's performing this task, it must be protected from potential pathogens that could harm you. As a means of defense, the gut has a protective layer of cells that are responsible not only for absorbing nutrients but also for blocking harmful bacteria. 

If this cell layer becomes compromised, the gut's defenses are weakened and potentially harmful bacteria could wreak havoc in your body. Having an inflamed, "leaky" gut can thus lead to other, more serious illnesses. 

Research now shows that an inflamed, leaky gut can potentially lead to a so-called _leaky brain_. This rather horrifying idea means that the brain, previously believed to be securely protected by the blood-brain barrier, can actually be exposed to harmful bacteria from the body, too. 

If bad bugs get into your brain, this can lead to brain inflammation. And when your brain suffers from inflammation, a lot of damage can be done before you even know that something is wrong.

Why? The brain doesn't have its own pain receptors; so unlike an inflammation of the skin (which you can see and feel), an inflammation of the brain is a silent sufferer. 

Yet brain inflammation is serious business. This symptom can lead to illnesses such as Alzheimer's disease or other severe neurological diseases, such as Parkinson's and depression. 

Even disorders like autism may be caused by what's happening in your gut, as we'll see in the next blink.

### 5. The development of autism spectrum disorder is believed to be strongly influenced by gut bacteria. 

Autism spectrum disorder (ASD) is a perplexing condition. Scientists still don't know what causes it, and while many therapies attempt to address ASD symptoms, there's no known cure. 

Examining the gut microbiome, however, may help us to better understand and treat ASD. 

ASD covers a wide range of conditions that are expressed during early brain development. People with ASD often avoid eye contact, prefer to be alone, have difficulty expressing their needs and display repetitive behavior, among other behaviors.

One hypothesis is that problems in the gut microbiome might contribute to the development of ASD. 

So far we've explored how issues in the gut can affect the brain and lead to illness. In the case of ASD, a similar situation may be at play. If brain inflammation exists during childhood, this might disrupt brain development, potentially resulting in ASD.

Studies have found that many people with ASD possess a particular composition of gut bacteria that is typically linked to a heightened inflammatory response in the body. 

Jason, diagnosed with ASD, was given multiple courses of antibiotics as a baby, which may have compromised his natural gut microbiome. When Jason was 10 years old, doctors did a stool analysis and found that he had almost no beneficial _lactobacillus bacteria_ in his gut.

Fortunately, the symptoms of autism can be alleviated by healing the gut microbiome.

One treatment method is to give a patient oral probiotics and vitamin supplements, in order to cultivate a healthier gut microbiome. 

For Jason, this therapy led to some success. After only three weeks, he showed a decrease in anxiety symptoms. He was even able to tie his own shoes for the first time.

A new treatment called _fecal microbial transplant_, or _stool transplan_ t, has shown promising results. In the treatment, a doctor extracts stool bacteria from a healthy gut and transplants it to a patient's colon. This practice has proven effective at improving a patient's compromised gut microbiome.

Now that you know how important your gut microbiome is, how can you best care for it?

### 6. Too much fructose in the Western diet has led to a rash of diet-related diseases. 

It's hard to say no to the delicious taste of sugar. Yet a particular type of sugar, called fructose, is today ubiquitous in prepared foods and drinks — and is wreaking havoc with our health. 

Fructose, found in processed and sweetened foods like soda and candy, is one of the most consumed sources of calories in Western nations. 

While fruit naturally contains some fructose, a fizzy soda is saturated with it. One 350-milliliter can of soda has around 140 calories from fructose sugar, whereas a medium-sized, fresh apple has only 70.

Fructose has the lowest _glycemic index_ (GI) of all the sugars, which means it has no immediate negative effect on blood sugar and insulin levels. Yet studies show that consuming too much fructose is related to _insulin resistance_ — a condition in which the glucose-processing ability of insulin is compromised. This can contribute to diabetes and hypertension. 

Consuming a fructose-heavy diet can also stress out your liver (as well as compromise your health overall), as this organ in particular is responsible for metabolizing fructose — primarily into fat.

Another substance that can be harmful to your health is gluten. Gluten is a protein found in grains and grain products; it gives elasticity to dough, for example. And it's everywhere, in your pizza and pasta, even in your ice cream and cosmetics.

And while only a small percentage of people suffer from celiac disease — gluten intolerance — many more people may have an averse yet often undetected reaction to gluten, known as _gluten sensitivity._

Gluten sensitivity can increase the body's inflammatory response, which as we now know, can lead to a wide range of diseases. If you feel you may have gluten sensitivity, it's best to avoid it in the foods you eat.

> _"Our bodies haven't evolved to healthily manage the prodigious amounts of fructose we consume today."_

### 7. As a society, we use too many antibiotics and are exposed to many toxins that could harm our health. 

In 1928, Scottish biologist Alexander Fleming discovered penicillin. One of the most important breakthroughs of the twentieth century, Fleming's discovery ushered in the era of antibiotics.

Unquestionably, the use of antibiotics has saved many lives. Yet today we take too many antibiotics, and in doing so, we're compromising both our gut microbiome and our overall health.

In 2010, doctors in the United States prescribed some _258_ _million_ courses of antibiotics — and the U.S. population is just over 300 million people. Often such prescriptions were for ailments that antibiotics can do nothing against, like the common cold.

Another major — and unnecessary — use of antibiotics is in agriculture. Farmers often pump antibiotics into healthy animals, simply to make them larger and fatter.

The excessive use of antibiotics is risky, as bacteria can quickly adapt and become resistant. Therefore the more antibiotics we use, the greater the chance that bacteria will evolve to develop a resistance.

Scientists have observed this in strains of the _staphylococcus aureus_ bacteria. Now resistant to a majority of common antibiotics, these bacteria can cause serious infections that can be fatal.

Antibiotics can also disrupt your gut microbiome by killing healthy bacteria — giving an opportunity to harmful bacteria to take up residence instead.

Along with antibiotics, many environmental chemicals can be damaging to our bodies and health. Of some 100,000 chemicals approved in recent decades in the United States, only 200 chemicals have been thoroughly safety-tested.

One example of a problematic chemical is _Bisphenol-A_ (BPA). Invented in 1891, BPA was used then both to alleviate menstrual problems and as a growth drug for cattle. However, it was found to trigger cancer and its use as a drug was banned.

Yet since the 1950s, BPA has been used in the creation of certain plastics; you can find BPA in many items, from notebooks to cash receipts. Scientists have found that BPA exposure can not only disrupt the body's hormonal balance but also alter the gut microbiome.

> _"The next time you think you or your child needs an antibiotic, I encourage you to weigh the pros and cons."_

### 8. Choosing fermented foods and fasting every season is good for you and your body. 

What do wine, yogurt, sauerkraut and black tea have in common? They're all _fermented_ food products. 

Humans have benefited from the process of fermentation in food for some 7,000 years, and nearly every culture has some kind of traditional fermented dish or drink. It's no surprise, as fermented foods are often good for your health. 

So what is fermentation? This organic process converts carbohydrates, like sugars, into either alcohol and carbon dioxide or organic acids. For fermentation to happen, either yeast, bacteria, or both are needed. 

It's during fermentation that bacteria produce the valuable vitamin B12, for example. One particularly useful fermentation process is called _lactic acid fermentation_. Here sugar is converted to lactic acid, which increases the number of beneficial bacteria (often called probiotics) while safeguarding food from harmful bacteria, and spoilage.

An easy way to consume probiotics is by eating unsweetened yogurt, created when milk undergoes _lactic acid fermentation_. 

Probiotic bacteria strains have many health benefits. They help increase the availability of vitamins, reduce inflammation and decrease the level of harmful bacteria in your gut. 

And when you eat fermented foods that contain beneficial bacteria, your body can absorb them more readily and thus benefit from them more than when you take probiotic supplements alone.

The practice of fasting can also help boost your health. Fasting was first mentioned in ancient Indian Vedic texts, and people have been exploring the benefits of fasting for over 3,000 years.

There are different ways to fast: you can restrict your caloric intake for a period of time, or you can not eat at all for 24 to 72 hours, as some examples. This latter practice is called intermittent fasting. 

Health benefits from fasting include increased insulin sensitivity, slowing the aging process and switching the body into fat-burning mode to lose weight.

Fasting also promotes positive changes in gut bacteria. In one study, the restriction of calories encouraged bacterial growth connected to a longer life, while reducing the amount of bacteria associated with a shorter lifespan.

### 9. Two natural substances that can work wonders for your health are turmeric and coconut oil. 

Do you enjoy a spicy curry? Good for you if you do, as this means the yellow spice, turmeric, is already part of your diet.

Turmeric is a popular Chinese and Indian seasoning that is often a component of curry powder, giving it a distinctive yellow color. But it's more than just a spice — turmeric is also good for your body.

This spice is chock-full of anti-inflammatory and antioxidant qualities, and can even increase your brain's cell count.

Even though Chinese and Indian practitioners have been using it for thousands of years, turmeric was discovered by Western medicine only recently; research regarding its benefits is ongoing.

Importantly, turmeric contains curcumin, an organic compound that can improve glucose metabolism, or the stabilization of blood sugar in your body.

So if you're not a huge fan of curry, you might want to consider taking turmeric or curcumin supplements to reap the compounds' benefits.

Another healthy "super food" is coconut oil.

Coconut oil has powerful anti-inflammatory properties, and is believed to be able to prevent and possibly even heal neurological diseases, such as Alzheimer's disease.

If you want to introduce coconut oil to your diet, you can use it as a cooking oil instead of canola oil, for example. Or you can consume it directly, one to two teaspoons of pure oil every day.

While there are plenty of supplements available on the market, if you want to explore a more natural approach to health, taking turmeric and coconut oil are excellent options.

### 10. Final summary 

The key message in this book:

**Your gut has far more to do with your health than you know. An upset gut is one of the main culprits in obesity and is linked to diabetes, dementia and even potentially autism. But you can improve the state of your gut microbiome and in turn your health by making simple adjustments to your diet, such as eating more probiotic foods.**

Actionable advice:

**Favor the filter.**

Chemicals like chlorine are bad for your gut microbiome, so you might want to consider filtering your water. This is simple to do and needn't be expensive. Although you may want to look at more involved solutions like filtering your shower water, you can also just purchase a water filter jug to keep filtered drinking water on hand in your refrigerator.

**Try a probiotics chaser.**

The next time you need to take antibiotics, follow them with a course of probiotics. These are found in yogurt, sauerkraut or even probiotic enemas. Antibiotics kill bacteria en masse in your gut, including the good ones. This makes room for all kinds of bacteria that you don't want, potentially resulting in diarrhea or prolonged inflammation. Probiotics can help you recover by introducing new, healthy bacteria into your system.

**Suggested further reading:** ** _Grain Brain_** **by David Perlmutter**

_Grain Brain_ (2013) outlines how what we eat can cause or mitigate serious brain disorders like anxiety, ADHD and depression. Eating well is crucial to your brain's functioning well, and these blinks tell you why.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dr. David Perlmutter with Kristin Loberg

Dr. David Perlmutter is a neurologist and the president of the Perlmutter Health Center in Florida. He is the author of the bestselling book _Grain Brain_. Additionally, he has written for the _Huffington Post,_ the _Daily Beast_ as well as for medical publications such as the _Journal of Neurosurgery._

Kristin Loberg is a writer and editor, and has been a contributing author for many bestselling books, including _Rule #1_ (with Phil Town), _The End of Illness_ (with David Agus) and _Grain Brain_ (with David Perlmutter). Loberg also holds writing workshops at the University of California at Los Angeles.

