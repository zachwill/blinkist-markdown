---
id: 5e5f8d266cee0700069aea17
slug: brain-wash-en
published_date: 2020-03-05T00:00:00.000+00:00
author: David Perlmutter MD, Austin Perlmutter MD, Kristin Loberg
title: Brain Wash
subtitle: Detox Your Mind for Clearer Thinking, Deeper Relationships, and Lasting Happiness
main_color: None
text_color: None
---

# Brain Wash

_Detox Your Mind for Clearer Thinking, Deeper Relationships, and Lasting Happiness_

**David Perlmutter MD, Austin Perlmutter MD, Kristin Loberg**

_Brain Wash_ (2020) is a no-nonsense handbook for living a calm and content life in a world that's designed to deliver the opposite. Medical doctors David and Austin Perlmutter unpack how our modern society manipulates our brains. Then they lay out a powerful 10-day bootcamp for breaking these bad patterns and building healthier habits.

---
### 1. What’s in it for me? A crash course in practical brain science. 

Many of us live with a wealth of pleasures right at our fingertips. With fast food, next-day delivery, and on-demand streaming media, we usually get what we want as soon as we want it. So why are so many people feeling anxious, depressed, and disconnected?

In these blinks, you'll learn why unhappiness is so common, and what doctors David Perlmutter, MD, and Austin Perlmutter, MD, prescribe as the cure. Drawing on the expertise of this father-and-son duo, you'll discover how our culture hijacks your brain and pushes you to make decisions that keep you from feeling your best.

You'll learn to avoid these pitfalls by building healthier habits with an in-depth, 10-day bootcamp program the Perlmutters developed to rewire your brain. With these blinks as your guide, you'll cultivate the tools necessary to craft a life centered on smart choices and long-term happiness.

In these blinks, you'll learn 

  * what early humans can teach us about nutrition; 

  * how a houseplant can keep you healthy; and

  * why your phone's screen may be keeping you awake.

### 2. Our modern world is great for instant gratification but terrible for long-term happiness. 

Clicking, scrolling, snacking, buying — our daily routines are made up of thousands of tiny actions, all designed to deliver a bit of pleasure. So, obviously, everyone should be happy, right?

Unfortunately, statistics show that the opposite is true. Since the 1990s, the number of antidepressant prescriptions in the United States has gone up more than 400 percent. During the same time period, the suicide rate rose in nearly every state. And, today, more than one in four Americans suffer from insomnia.

**The key message in this blink is: our modern world is great for instant gratification but terrible for long-term happiness.**

So how'd we get here? The answer has to do with our brains.

For millions of years, our ancestors lived in a dangerous and unstable world. Food was often scarce and predators lurked around every corner. To cope with these pressures, and to help us stay safe, our brains evolved to value certain things, like social acceptance and energy-rich foods. 

Due to technological advancement, our contemporary lives are much easier. However, our brains haven't caught up. The older parts of our brain, such as the amygdala, are still wired to seek the same rewards.

The problem comes when big businesses exploit these survival instincts for profit. This is increasingly common. Just consider the modern diet. Grocery stores could offer only the healthiest fare, but, instead, many push a variety of sugary sweets, salty snacks, and all sorts of high-calorie, nutrient-poor options. 

We love these foods because they satisfy our primal instincts — our age-old preference for foods packed with energy. Businesses love these foods because they're easy to sell. The downside is, this exchange only leads to short-term gratification. In the long-term it leads to chronic illnesses such as obesity, diabetes, and heart disease. 

The same problem occurs with social media. Companies like to keep us clicking, swiping, and liking because it generates advertising revenue. Our brains like it because it provides the primal rewards of attention and social acceptance. However, the whole ordeal comes at the expense of deeper meaningful relationships. Even with our constant connection, the majority of Americans report feeling lonely.

We can call this pattern of valuing instant gratification over long-term happiness "disconnection syndrome." To overcome it, we must rewire our brains. In the next blinks, we'll start to learn how.

> _"What were once valuable adaptations that helped us survive have now become entry points for exploitation."_

### 3. The brain is malleable and susceptible to change, for both good and bad. 

It's 1848 and railroad worker Phineas Gage is in trouble. A disastrous explosion has pushed an iron rod straight through his skull. Amazingly, the accident doesn't kill him. However, it does damage his brain and change his life.

Before the accident, Gage was affable and friendly. Afterwards, his personality shifted. He was impulsive, mean, and had a fiery temper. For many years, Gage's behavior made him a social outcast. But, as time went on, his brain began to heal. At the end of his life, he was once again acting gentle and kind.

**In this blink, the key message is: the brain is malleable and susceptible to change, for both good and bad.**

Luckily, big, dramatic events like the one Gage experienced are pretty rare. That said, everyone's brains are constantly changing in many small ways throughout their lives. Every time you have a new thought, feeling, or experience, your brain cells, or neurons, form new synaptic connections. This is called neuroplasticity. 

The connections between neurons in your brain help determine your overall thought patterns and personality. The more you use certain connections, the more robust and influential they become. For example, consistently having dark or anxious thoughts makes it easier to have dark or anxious thoughts in the future.

This patterning becomes especially important when you consider the relationship between different parts of the brain.

In simple terms, our brain has three main parts. First there is the brain stem, which oversees automatic bodily functions like breathing. Next is the limbic brain, which contains the amygdala, and is responsible for processing emotions like fear and excitement. Finally, there is the cerebral cortex. This last part is the most recent evolutionary addition to the human brain. It moderates functions like reflective thinking, advanced problem solving, and complex planning. 

In a healthy brain, there is balanced communication between the limbic brain and the cerebral cortex. For instance, if we see a predator, the amygdala provides a rush of fear. Then, our cerebral cortex can take over and assess the level of danger and provide a plan of action. Problems arise when this communication is weak, and the impulses of the amygdala are allowed to run wild. When this happens, we become impulsive, reactive, and self-centered. 

Part of rewiring our brains to have healthier habits requires strengthening the good connections between the cortex and the amygdala. The next blink will delve into this topic in more detail.

> _"Understanding the story of the prefrontal cortex and the amygdala is key as we move ahead to untangle the question of how modern influences modify our health and happiness."_

### 4. The risk and reward structures of our brains are dangerously overstimulated. 

Imagine life 12,000 years ago: no TV, no smartphones, no strawberry ice cream. The most exciting activities around are foraging for food and running from predators. 

Our bodies evolved in this environment, and that means our minds did, too. Unfortunately, the result is that our brains are mostly ill-prepared for the sensory overload of the modern world. 

**The key message in this blink is: the risk and reward structures of our brains are dangerously overstimulated.**

Let's examine how this works. When you engage in an activity like eating a sweet snack, your body responds by releasing dopamine. This neurotransmitter kicks off a chain reaction that triggers your brain's centers for pleasure and memory. Essentially, you get a boost of joy and remember to do it again.

This system works wonders when you're on the savannah and sweets are rare. However, when you're surrounded by candy bars and fast food, it goes haywire. These days, we constantly engage in dopamine-releasing activities. We do it so much that those reward pathways build a tolerance, and we're never satisfied.

Complicating the issue of too much pleasure is the opposite problem: too much stress. When you experience a difficult or frightening situation, your body releases cortisol. This hormone sets off your fight-or-flight response. Your muscles tense, your heart rate spikes, and the amygdala section of your brain takes control. This is great in life-or-death situations, but, over time, it weakens your prefrontal cortex, priming you to make more impulsive decisions.

Going about your daily life, you cycle through these two processes over and over at an unnatural pace. Think about it. Nearly 80 percent of adults start their day by checking their smartphone. That's a boost of dopamine first thing in the morning. Then, it's off to a stressful day of work where traffic, meetings, or other stressors spike your cortisol levels. At the end of the day, a high-carb meal and evening of digital media jack up your dopamine again.

All of these highs and lows add up and your brain struggles to find balance. The outcome of this overstimulation is a super-charged limbic brain wired to overreact to stress and then seek instant gratification to calm down. It's a vicious cycle.

In the next blink, we'll take a closer look at how our digital devices exacerbate these problems.

### 5. Overuse of social media can severely limit your happiness. 

You wake up, you check your phone. You make breakfast, you check your email. You drive to work, you check your phone again. 

Sound familiar? Don't worry, you're not alone. Over the past decade, the internet and social media have become a constant presence for most people. In fact, a recent report found that 16 percent of adolescents feel addicted to their phones. But what are the side effects of all this connectivity?

**The key message in this blink is: overuse of social media can severely limit your happiness.**

Digital-communication technology can be extremely useful. However, it can also wreak havoc on your brain. In particular, it can overstimulate the reward system we previously discussed. Every Facebook like, Instagram post, or funny tweet floods your brain with dopamine. As you become accustomed to the regular hits of stimulation, you become addicted.

This process may even result in physical changes to the brain. Several studies have found that many individuals with internet addictions have weakened _anterior cingulates_. This section of the brain helps moderate impulse control. This structural change could contribute to our habit of mindless scrolling and refreshing as we seek that next hit.

Our internet addiction may even weaken our friendships and inflame our feelings of social disconnection. The average internet user has more than eight social media accounts. That's a lot of ways to keep in touch! However, interaction through these online spaces doesn't offer the same benefits as face-to-face interaction.

On the contrary, using all these networks may make us feel more isolated. A recent study from the University of Pennsylvania asked more than 100 students to limit their use of social media to ten minutes a day for three weeks. At the end of the intervention, students who successfully constrained their use reported feeling significantly less lonely and scored lower on tests of depression.

Could such an experiment have a similar effect on you? Try reining in your own compulsive use of social media and see how it feels. Limit your time spent online and try to be mindful of endless scrolling. You may find disconnecting from the internet helps you reconnect with your surroundings.

### 6. Fostering empathy is crucial to fighting disconnection syndrome. 

You're walking down a busy street. On the road, cars speed by with few gaps in between. At the corner is an old woman trying to cross. She's struggling with two bags of groceries and could clearly use a hand. What do you do?

If you're like most people, you stop and help her navigate the intersection. Maybe you even carry her groceries home. Humans are inherently social, and our ability to work together is one reason our species has survived. Sadly, the modern world may be eroding that capacity.

**In this blink, the key message is: fostering empathy is crucial to fighting disconnection syndrome.**

So what exactly is empathy? Specialists usually define it in one of two ways. The first is "affective empathy." This is the ability to feel the pain or emotions of others. When you watch a sad movie and cry along with the characters, you are experiencing affective empathy. The second form is "cognitive empathy," also known as "theory of mind." This is the ability to see the world from someone else's point of view. 

In contrast to empathy is narcissism. Narcissistic people are highly self-centered, selfish, and often completely disregard the feelings of others. Narcissistic tendencies have been linked to aggression, domestic violence, and other antisocial behavior.

Over the past decade, scientists have noticed a disturbing trend. Narcissistic behavior is becoming more common than empathetic behavior. One study from the University of Michigan found that since the year 2000 students are about 40 percent less empathetic than in the past. 

What does this have to do with the brain? Well, the rise in narcissism could be connected to the ways the modern world has rewired our neurons. We already know that things like an overstimulated reward system and overuse of social media can weaken the link between the prefrontal cortex and the amygdala. Recent science also suggests these same processes could limit our ability to feel empathy.

This is bad news for those suffering from disconnection syndrome. A decreased ability to feel empathy limits our ability to connect with others and form deep relationships. It can also contribute to our already overworked stress response, since those with narcissistic personality traits tend to produce significantly higher levels of cortisol.

Clearly, the modern world puts us in a tough situation. It may seem like there's no solution. Luckily, that's not the case. There are steps we can take to break these vicious cycles. We'll start looking at some concrete strategies in the next blink.

> _"It is our interconnectedness, our interrelationships, not just among people but also among all living things, that sustains us and provides resilience against adversity."_

### 7. Fight disconnection syndrome by reconnecting with nature. 

In the early 1900s, English author E. M. Forster wrote a short story called "The Machine Stops." This disturbing tale describes a dystopian future where humanity lives in complete indoor isolation. The characters rarely see the sun and they only communicate through digital devices.

Today, a bit more than a century after the story's publication, reality has a great deal in common with that bleak fictional world. How much, exactly? Well, one survey found that Americans only spend about 5 percent of their day outdoors. As one journalist put it, most of us suffer from a "nature deficit disorder."

**The key message in this blink is: fight disconnection syndrome by reconnecting with nature.**

Today, the majority of the world's population lives in cities. So it can be easy to forget that for most of history humans lived as part of nature. **** We breathed fresh air, walked through forests and fields, and basked in sunlight. While the conveniences of the modern world are valuable, research has consistently shown that experiencing these elements of nature is important to our overall health and well-being.

Just how important is it? Very! A 2018 survey of 2,000 Canadians discovered that nearly 90 percent of respondents felt happier, healthier, and more productive after spending time outside.

Even more amazingly, a report published in the medical journal _Science_ found that merely seeing plants could improve surgery outcomes. Patients with hospital rooms with views of the forest recovered more quickly and required less pain medication than those with no view of the outdoors.

Why is interacting with nature so important? Many scientists believe that being surrounded by natural beauty can significantly lower our levels of stress.

Take a stroll through the woods and notice how it puts the world into perspective. Even a brief walk can help you forget about the demands of your job or the persistent attention-seeking of your digital devices. These moments of quiet will help reduce the amount of stress hormones like cortisol in your brain.

Furthermore, regular exposure to sunlight encourages our body to produce vitamin D. **** This hormone is essential for our health, but, in particular, it stimulates the brain's ability to make serotonin, a hormone associated with happiness. Try working a few moments of sunbathing into your day. Even just sitting in a park or having a short picnic could vastly improve your mood.

Of course, what you decide to eat on that picnic is important as well. In the next blink, we'll cover how food can help in the fight against disconnection.

### 8. The artificial modern diet is bad for your waistline and your brain. 

Take a look around your kitchen. What do you see? Colorful boxes of sugary cereal, a couple cans of soda in the fridge? Maybe a carton or two of leftover takeout from last night?

Thousands of years ago, your options would have looked much different. In fact, the modern menu is full of foods that would be unimaginable to our early ancestors. But are all these new options good for us? The truth is: probably not.

**The key message in this blink is: the artificial modern diet is bad for your waistline and your brain.**

When early humans first evolved, they relied on hunting and gathering to fill their plates. This means our ancestors ate a huge variety of foods and their diets changed along with the seasons. The modern diet is significantly different. With the invention of agriculture and modern food processing, we eat much more food but, in general, it's much less nutritious. 

One important difference is the huge amount of sugar in the modern diet. While sweet snacks were uncommon in prehistory, our current meals are packed with carbohydrates and added sweeteners. Unfortunately, most of the time, we don't even realize it.

In 2016, researchers at the University of North Carolina tried to trace just how much our food is artificially sweetened. They looked at more than a million different products sold around the United States. Shockingly, they found that nearly 70 percent of foods had been altered to include extra sugar.

All this sugar is bad news for our brains. For one, diets packed with refined carbs can lower the production of BDNF, a protein essential for creating strong neurological connections. Worse still, high blood-sugar levels are associated with degenerative brain conditions such as dementia and Alzheimer's. A study from 2018 even found a link between excessive sugar consumption and depression.

Of course, what we eat can have positive effects as well. Stick to a diet low in refined carbs and packed with healthy fats from olive oil, nuts, seeds, and red meat, and you'll lower your risk of depression. Scientists believe this is because these snacks contain loads of tryptophan. This amino acid helps the body produce serotonin, a chemical essential to feelings of joy and contentment. 

Eating right is only the first step toward developing a healthier brain. Other habits, such as sticking to a regular sleeping schedule, are just as important, as we'll see in the next blink.

> _"The first step to dietary change requires understanding what you're actually eating, how you're being manipulated to eat the wrong things, and how those dietary choices are affecting your brain."_

### 9. A good night’s sleep is essential for maintaining brain health. 

Your eyes are heavy. Your muscles ache. Every step feels labored, like your shoes are full of lead.

This is how it feels to start the day after a night of no sleep. And while your body may feel physically bad, chances are, your brain is doing even worse.

**Here's the key message in this blink: a good night's sleep is essential for maintaining brain health.**

Every human has an internal clock, also known as the circadian rhythm. This natural cycle evolved when we lived in nature. It helps us sleep at night and wake up with the sunrise. However, the modern world, with its electric lights and 24/7 lifestyles, disrupts this pattern.

Now, we stay up much later and spend much less time sleeping. Studies show that a full third of American adults sleep less than the recommended seven hours a night. This condition, sometimes called a "sleep deficit," has major impacts on our brain. Decades of research have taught us that regularly missing bedtime can increase your risk of Alzheimer's, weaken your memory, and lead to an overall decline in cognitive functioning.

Why is snoozing so important? One reason is that sleep acts as a natural "shampoo for the brain." When you hit the hay, a structure called the glymphatic system kicks into high gear. This system clears your brain of all the excess molecular junk that builds up during the day. This includes inflammatory chemicals like kynurenine and beta-amyloids, both of which are linked to ailments like depression.

Additionally, missing sleep weakens the prefrontal cortex and gives more control to the amygdala. When the amygdala is allowed to run wild, we experience more impulsive behavior and overreact to stress. As a result, a sleep-deprived person will make poor choices such as overeating or indulging in sugary snacks.

So how can you ensure you get enough shut-eye? Don't reach for the sleeping pills right away. These can leave you groggy in the morning and further upset your circadian rhythm. Instead, try limiting your exposure to electronics like computers and smartphones before bed. The screens on these devices give off harsh blue light. Soaking up this wavelength past sunset can keep your body from producing melatonin, the brain's natural sleep aid.

Now that we've established the importance of rest, it's time to look at another habit that is essential for brain health: exercise.

### 10. Regular exercise can seriously improve cognition. 

Let's take a trip to northern Tanzania. Here, we'll find the Hadza, a modern community of hunter-gatherers. Each day, members of this group spend their time searching the savannah for food — and all that foraging adds up. A typical day includes more than five miles of walking.

Humans were built to move. A traditional hunter-gatherer lifestyle is great for our bodies. That's obvious. However, lately, science has shown us that physical activity is also crucial for our brains.

**This blink's key message is: regular exercise can seriously improve cognition.**

Unlike the Hadza, most people don't get enough physical activity. Doctors usually recommend that healthy adults get at least 30 minutes of exercise a day. In the United States, only 5 percent of adults hit that threshold. This sedentary lifestyle contributes to the low moods and foggy thinking associated with disconnection syndrome.

Exercise can support brain health in many ways. To start, regular physical activity activates the prefrontal cortex, giving a boost to the part of our brain that handles problem-solving, planning, and other executive functions.

A study from 2011 shows this in action. A group of 171 children were given tests to measure their critical thinking and math skills. Half the children exercised before the tests; the others did not. The results were clear: those that did a bit of cardio scored much higher. Researchers believe this is because aerobic exercise increased blood flow to the prefrontal cortex.

Aside from raising your test scores, exercise can also lift your mood. Many people report feeling great after a hard workout. After all, we've all heard of the famed "runner's high." Yet science shows the long-term effects of exercise are just as impactful for our outlook on life.

Many studies have established a strong connection between an active lifestyle and a positive state of mind. Just how strong is this connection? Well, a report published in the journal _Psychiatry_ tracked the habits and moods of 40,000 adults for more than a decade. It concluded that even just one hour of physical activity a week could significantly reduce the risk of depression. 

It doesn't matter which type of workout you choose. You will experience the cognitive benefits of exercise as long as you get moving. Make it a habit to build a little movement into your routine. Whether it's gardening, jogging, or lifting weights, the boost to your brain will be the same.

### 11. You can retrain your brain by practicing mindfulness. 

Whirring, glitching, freezing up — an overstimulated mind can act like a computer with too many programs open.

When the distractions and stress of the modern world overload your central processing unit, it's best to eliminate the excess noise. Sometimes, a little bit of silence is necessary to reboot your brain.

**The key message in this blink is: you can retrain your brain by practicing mindfulness.**

Mindfulness and meditation are increasingly trendy topics in both popular culture and within the scientific community. According to the Centers for Disease Control and Prevention, the number of American adults participating in yoga doubled between 2012 and 2017. Even more impressively, the number of adults practicing meditation tripled over that same period.

Why this huge jump in popularity? Most likely because people feel that mindfulness practices like meditation can provide a much-needed break from the constant buzz of contemporary life. And many recent studies back up these feelings with hard science. We now know that practicing meditation can have measurable effects on the structure and functioning of the brain.

For instance, in 2011, Harvard researchers led participants in an eight-week program in mindfulness training. At the end of the program, an MRI scan revealed that the students had developed significantly denser concentrations of gray matter in their prefrontal cortex. Such density is correlated with better focus, sensory processing, and planning abilities.

Routine meditation is especially beneficial for handling stress and controlling the rash, impulsive side of our brain. Just take it from Dr. Yi-Yuan Tang at Stanford University. In multiple studies, he demonstrated that daily meditation leads to increased connectivity within the prefrontal cortex and relatively tamer activation in the amygdala. Such structural changes mean practitioners generally have more control over emotions and fewer bouts of anxiety and depression.

Integrating mindfulness into your day isn't difficult. Even just 12 minutes of meditation can increase blood flow to the prefrontal cortex. That means you can get all those brain-boosting benefits in the span of a coffee break. 

Your practice doesn't have to be complicated, either. Beginning meditators can start with the simple act of deep breathing. To do this, just sit comfortably on a chair or on the floor. Close your eyes and concentrate on your breath as it goes in and out of your lungs. Breathe slowly so that each inhale takes about 20 seconds. 

Don't worry if your mind wanders. Mindfulness is a skill. With a little practice each day, you'll soon get the hang of it. In the next blink, we'll go over a ten-day plan to get you started.

### 12. Committing to the Brain Wash program sets the stage for long-term change. 

By now, you understand how the modern world encourages behaviors and habits that negatively affect our brain's functioning. You know that everything from your sleep schedule to your nutrition to your digital devices play a role in making you feel depressed, drained, and disconnected. 

Unfortunately, there is no quick fix to disconnection syndrome. However, you can reclaim your health by sticking to a few fundamentals. Dr. David Perlmutter, along with his son, Dr. Austin Perlmutter, have put together a ten-day Brain Wash program to get you started. 

**The key message in this, the final blink, is: committing to the Brain Wash program sets the stage for long-term change.**

Each day of the Brain Wash program asks you to focus on one aspect of your brain's health. As you follow the program, keep practicing what works until you've rebuilt your routines from the ground up.

Day one: Start with a digital detox. Cut unessential technology from your life, and block any apps that are mostly used to waste time.

Day two: Practice empathy and gratitude. Reflect on the positive aspects of your life and write down five things you are thankful for.

Day three: Reconnect with nature. Take the time for a short walk outside, have a picnic in a park, or spend an afternoon tending to a garden.

Day four: Detox your diet. Take stock of what you eat and make a plan to improve your habits. Eliminate processed foods and look up a few recipes that incorporate fresh, healthy ingredients. Additionally, consider which vitamins you could add to your daily intake.

Day five: Spruce up your sleep schedule. Remove any digital devices from your bedroom and cut out caffeine after 2:00 p.m. Set an earlier bedtime and stick to it. 

Day six: Embrace exercise. Engage in a bit of physical activity like a brief walk or a trip to the gym. Think about how to make it a habit. Maybe set up a schedule or find a friend to keep you on track.

Day seven: Medicate with meditation. On this day, take 12 minutes to try deep-breathing meditation. 

Day eight: Strengthen your social circle. Part of disconnection syndrome is feeling lonely. Fight back by having dinner with a friend, calling family members, or volunteering at a local organization. 

Day nine: Take stock. Look back at the previous days and consider what worked and what didn't.

Day ten: Move forward. You've come a long way in just ten days. Now it's time to make it stick. Notice how much better you feel and make a promise to commit to your new routines.

> "_You are on your way to a better life. You've begun to make significant changes that are affecting you right now on many levels, including your mood, metabolism, and brain function."_

### 13. Final summary 

The key message in these blinks:

**The modern world is designed to knock your brain off balance. Poor nutrition, inadequate sleep, and constant digital distractions set us up to be impulsive, depressed, and disconnected from what matters most. However, by concentrating on building better habits — like eating fresh, reconnecting with nature, and turning off our phones — we can rewire our brain to deliver long-term happiness.**

Actionable advice:

**Be realistic about change.**

It's easy to get caught up in the desire to overhaul your habits. Pushing for big changes right away can lead to burnout, relapse, and disappointment. Approach the Brain Wash program with an eye toward sustainability. It's better to make a few little changes you can stick to than a big change you abandon.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Brain Maker_** **(2015) by Dr. David Perlmutter**

You just learned how our instant-gratification society is seriously rewiring our brains for the worse. Delve deeper into the science behind our nervous system with our blinks to _Brain Maker_, also by Dr. David Perlmutter.

This book-in-blinks examines the often overlooked role that the microbiome plays in influencing our brains. Pulling facts from cutting-edge research and his own clinical experience, Dr. Perlmutter spells out why what we eat can have such a massive impact on how we think and feel.
---

### David Perlmutter MD, Austin Perlmutter MD, Kristin Loberg

David Permutter and his son Austin Permutter are a pair of board-certified medical doctors. Austin holds an MD in internal medicine from the University of Miami. David is a neurologist, a fellow at the American College of Nutrition, and the author of many acclaimed books, including _The Grain Brain Whole Life Plan_, _The Grain Brain Cookbook_, and _Raise a Smarter Child by Kindergarten_.

