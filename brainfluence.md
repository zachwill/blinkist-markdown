---
id: 5563ba5f6461640007b40000
slug: brainfluence-en
published_date: 2015-05-27T00:00:00.000+00:00
author: Roger Dooley
title: Brainfluence
subtitle: 100 Ways to Persuade and Convince Consumers with Neuromarketing
main_color: 2388B0
text_color: 1E7596
---

# Brainfluence

_100 Ways to Persuade and Convince Consumers with Neuromarketing_

**Roger Dooley**

_Brainfluence_ (2012) explores the unconscious thoughts and motivations that influence our decision-making process, and offers tips and tricks on how savvy marketers can exploit them. By understanding the mechanisms that cause us to buy (or not buy), you can increase your sales while keeping your customers happier.

---
### 1. What’s in it for me? Learn some neuromarketing tips that can help you sell more. 

What does it take for a business to have excellent sales? Easy: hire confident sales staff, train them in how to force your product onto the customer and you'll be sure to sell tons. Right?

Well, it isn't quite that simple. There are psychological mechanisms at work in the world of sales, and one of the keys to great sales performance can be found in the world of neuroscience. In recent years, scientists have discovered more and more about what makes us tick, and each bit of this valuable information can be used to sell things.

These blinks show you some of the best tips from neuroscience that you can use to sell more products.

In these blinks, you'll discover

  * why speaking into someone's right ear is an effective sales strategy;

  * why your best sales technique might simply be to use a picture of a baby; and

  * why selling is all in how your product smells.

### 2. Reduce the feeling of pain during the buying experience and even tightwads will buy. 

We've all had the experience of buyer's remorse, that feeling of regret after having made a purchase becomes. Sometimes it's more than just a feeling — shopping can cause real pain.

Indeed, purchases themselves can activate the brain's pain center. In a Carnegie Mellon University and Stanford University experiment, subjects were presented with cash before being placed in a functional magnetic resonance imaging (fMRI) machine to record their brain activity. They were then offered items at a certain price — some items were offered at a bargain and others were a raw deal.

Interestingly, researchers could accurately predict whether or not the subject would buy an item or keep the cash just by looking at his or her brain scans and seeing how much pain they were feeling.

However, it's not just the amount of money we part with that's important for the brain's pain center, but also the context. For instance, losing 75 cents to a vending machine can be far more aggravating than spending thousands on a car.

So if you want to sell to even the most miserly tightwads, you'll need to minimize the pain they feel when they buy. But how?

Above all else, you should make the price seem like a bargain, or at least appear fair. If you're selling a $120 annual membership to your gym, you can make this amount seem smaller by selling it for "only $10 per month" or "33 cents per day."

Similarly, appealing to important needs over unnecessary pleasures is a particularly good strategy for selling to tightwads. For example, a Carnegie Mellon University study began by using a survey to situate participants along the "Tightwad-Spendthrift Scale" and then offered massages, both in terms of a massage for pleasure and also as a way to relieve back pain. The results showed that tightwads were 26 percent less likely than the spendthrifts to buy a massage for pleasure, but when the massage was described as relieving back pain, tightwads were only 9 percent less likely to buy it.

> _"Pricing and the product itself need to be optimized to minimize the pain of paying."_

### 3. Captivate all the senses, especially smell. 

When making your sales pitch, it goes without saying that appealing only to your potential customers' rationality is not enough — you also need to pull the right emotional strings. However, you'll have even greater success if you appeal to a more visceral part of the human experience: this means you have to sell to all five senses.

Singapore Airlines is a good example of this principle in action. They've combined a number of sensory triggers as a means of developing their brand image. Their flight attendants, for instance, are impeccably dressed in uniforms that match the aircraft's color scheme. They also all wear the same perfume, which is likewise used in their hot towels and other services.

Their mission to make every sensory element appealing has consistently put Singapore Airlines at the top of travelers' preference rankings according to Martin Lindstrom, author of _Buyology and Brand Sense_.

But of all the senses, smell is the most important for high sales. We can see this demonstrated in an experiment in which customers evaluated identical Nike shoes, one in an unscented room and another in a room with a floral scent. Astoundingly, 84 percent of participants in the scented room rated the shoes as superior.

This is because, according to Lindstrom, 75 percent of our emotional reactions are generated by smell.

In addition, our sense of smell contributes to how we recall memories and process information. For instance, one test found that simply changing a shampoo's fragrance was enough to make buyers think that it foamed better, rinsed out more easily and left their hair glossier! Unsurprisingly, these changes also encouraged them to buy more.

So what does all this mean for you? You should be aware of how clients and customers experience the place where you sell. Every type of business has a signature smell, like the smell of leather in a shoe store or coffee in a café. Ask yourself: Does my business smell like it should?

### 4. Want to make your ad more effective? Put a baby on it. 

People who have been in the ad business know that the right picture is worth a thousand words. But what kind of picture qualifies as the "right" image? Clearly some are more effective than others.

For starters, putting a face on your ad will attract attention. But a baby's face will attract even more! This was demonstrated in a recent study, which observed high levels of activity in the participants' medial orbitofrontal cortex, the area associated with emotion, just 150 milliseconds after having seen a photo of a baby.

As it turns out, we really are wired to respond to baby faces, and even baby-like characteristics in adults. The reason for this is likely evolutionary: Babies are highly vulnerable, and increase their chances of survival if they tug at the emotions of all adults, not just their parents.

Studies have even shown that men prefer females with baby-like features. Likewise, women, depending on their stage of ovulation, may prefer more masculine or baby-like faces.

So, if you want to attract your viewers' attention, an easy way is to just add a picture of a baby.

And while you're adding faces to your ads, make sure that they're looking at what you want the viewer to focus on. According to the Australian usability specialist James Breeze, when someone in an ad is looking at something, we look at it too.

In other words, if the baby face is looking at us, then we'll look right back at the baby face. But if the baby face is looking at _something else_ — your headline, a product image, key information, etc. — then that's where the viewer will direct their attention.

By using pictures of people, you can motivate your viewers to actually spend time reading that snazzy copy you've crafted, rather than letting your ad simply become part of the background.

### 5. Generate more sales by building and rewarding the loyalty of customers. 

Every businessperson's dream is to spend less and sell more. One of the best ways to do this is to cultivate your customer relationships in a way that inspires loyalty. Not only are loyal customers cheaper to sell to, they're also more likely to buy.

One way to boost loyalty is to draw people's attention to other possible circumstances. In other words, drawing attention to disadvantageous alternatives to their current life — whether it's the company they work for or a product they're thinking of buying — actually increases people's loyalty to their current situation.

We see this tendency in research conducted at Northwestern University and the University of California, Berkeley. The study found that subjects who reflected upon what their world would be like if the US hadn't come into being demonstrated higher levels of patriotism than subjects who reflected upon what their world _is_ like because the US _did_ come into existence.

That means that a counterfactual reflection — imagining the world without the US — was more influential than a factual reflection.

Similarly, if you get a customer to imagine her relationship with another company, she will better appreciate her relationship with _your_ company, and feel encouraged to remain loyal.

If, for example, you can get her to imagine that other companies' customer service pales in comparison to yours, she'll suddenly become more appreciative of your great service.

It's also important for you to reward your loyal customers, because rewarding loyal customers is beneficial to you too.

Indeed, loyalty programs _do_ work and keep your customers engaged. While there are many different types of loyalty programs, one of the best kinds is the old-fashioned punch card system. With this system, customers see their progress towards their rewards with each purchase, which in turn motivates them to continue using your products and services, and stay loyal to your brand.

If you keep your customers continually engaged and loyal, you won't have to spend more on marketing to attract new customers. It's a cheaper way of selling more.

### 6. If you want to generate more sales, speak into the customer’s right ear when schmoozing. 

The best marketing happens in person, as it allows you to engage multiple senses.

One weapon in your multisensory marketing arsenal is _schmoozing_. While some believe that it's best to get right down to brass tacks, idle small talk can have a big payout too.

Consider this game often used in experiments called the _ultimatum game_, whereby two subjects are given a sum of money. One subject decides how to split the money between them, and the second can either accept the split or reject it. But there's a catch: If the split is rejected, then both people walk away with nothing. Most of the time people will reject an offer they feel is unfair.

Researcher Al Roth introduced a twist to this game, whereby the subjects talk to one another before playing. This little bit of small talk caused fair offers to rise by 83 percent, and only 5 percent of games post-small talk resulted in failure.

So what does this mean for you? Even if you're just chatting about the kids, golf or the weather, a little schmoozing goes a long way to build mutual respect and trust, and increases the likelihood of reaching a mutually satisfactory deal.

After you've schmoozed your potential client, you can make the most of your conversation by speaking into their right ear. Seriously.

Dr. Luca Tommasi and Daniele Marzoli from the University Gabriele d'Annunzio in Italy found that humans prefer information spoken into our right ear, and that requests spoken into the right ear are more likely to be successful.

In their studies they observed many people in noisy nightclubs, and found that the majority spoke into each other's right ears. When they put the theory to test, they found that they had more success bumming cigarettes if they spoke into right ears.

So, the next time you're at a networking event or a dinner party, sit to the right of whomever you want to be schmoozing with!

### 7. Sell more by surprising your customer’s brains. 

Why is it that children are so good at captivating our attention? Because they surprise us! They paint their faces blue, wear saucepans on their heads and hide in unexpected places. If you want to sell more, you'll need to capture people's attention in a similar way — not by hiding in unexpected places, but by showing customers unexpected things.

Researchers in the UK have discovered that the _hippocampus_, a small component in our brains, predicts what will happen next. It does this by automatically recalling a sequence of events in response to a single cue. And when the unexpected comes, we react.

You can use this element of surprise to your advantage. Good copywriters often do this by substituting an unexpected word in a familiar phrase. For example, instead of "a stitch in time saves nine," they might write, "a stitch in time saves money."

Of course, you don't have to use words. Surprising images and designs work just as well.

This strategy for garnering attention is timeless — even Shakespeare used it! But in his case, he usually created surprise by misusing words rather than substituting them. He would take, for instance, the noun "God," and turn it into a verb with a phrase like "he godded me" (meaning, he treated me like God).

Shakespeare's misuse of words increases brain activity in the reader or viewer, which, according to researcher Neil Roberts, is among the primary reasons for his works' enduring appeal.

Similarly, you could modify your copy to make it more eye catching, or perhaps "brain catching." For example, if you run a coffee shop, why not tell your customers to "coffee it up" rather than simply saying "time for coffee" to get them thinking?

The right words — or rather, the wrong ones — can tap into your customers' emotions and hold their attention long enough for you to convey your message. The kicker? It won't cost you a dime.

### 8. Final summary 

The key message in this book:

**We like to think that our purchasing decisions are formed through intellect and rationality. But this just isn't true. Rather, whether we buy or don't buy is determined just as much by our senses, emotions and unconscious mind.**

Actionable advice:

**Bundle your products.**

If you're having a hard time closing the deal, try putting your products together in a bundle. Because customers can't easily calculate the value of each component, they can't easily evaluate the fairness of the deal. Think about it: Can you calculate the worth of the components of your car? The leather seats, sunroof, air conditioning, etc.?

**Suggested** **further** **reading:** ** _Buyology_** **by Martin Lindstrom**

Day in and day out we're bombarded by thousands of brand images, logos and commercials enticing us to buy their products. However, only _some_ ads actually motivate us to whip out our wallets. Why? Using cutting-edge neuromarketing methods, _Buyology_ answers that question and explores the hidden motivations behind our purchasing decisions.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Roger Dooley

Richard Dooley is an entrepreneur and marketer, and founder of the marketing consultancy Dooley Direct LLC. He is also the author of the blog _Neuromarketing_. 

[Roger Dooley: Brainfluence] copyright [2011], John Wiley & Sons [Inc. or Ltd. as applicable] Used by permission of John Wiley & Sons [Inc. or Ltd. as applicable] and shall not be made available to any unauthorized third parties.

