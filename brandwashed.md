---
id: 53b3b8de3135350007000000
slug: brandwashed-en
published_date: 2014-07-01T00:00:00.000+00:00
author: Martin Lindstrom
title: Brandwashed
subtitle: Tricks Companies Use to Manipulate Our Minds and Persuade Us to Buy
main_color: 5CC4E0
text_color: 326B7A
---

# Brandwashed

_Tricks Companies Use to Manipulate Our Minds and Persuade Us to Buy_

**Martin Lindstrom**

_Brandwashed_ explains the different psychological effects that influence our buying decisions and shows how marketers use them to sell their products. _Brandwashed_ reveals the marketing tricks of the world's largest companies, which play an increasingly important role in our everyday lives. Lindstrom's exposé will help you to avoid manipulation the next time you go shopping.

---
### 1. What’s in it for me? Find out how easily we’re tricked into brand loyalty. 

As the tricks of marketing become increasingly more sophisticated and subtle, detecting them becomes more and more difficult for consumers.

Enter _Brandwashed_, in which author and former marketing insider Martin Lindstrom guides you through the common (and not-so-common) tricks of contemporary marketing, with the aim of empowering you as a consumer and helping you to make far more rational buying decisions.

This book will teach you the ways in which everyday brands attempt to trick you into purchasing their merchandise. It will also show you how companies design their advertising to exploit your psychological weak spots, so that while you might believe you're making a rational choice during a shopping trip, you're actually unconsciously directed towards certain products.

By reading these blinks, you'll begin to learn how you can stop yourself being duped in this way. Knowing the tricks of the advertising trade will help make your next shopping outing a better, more rational experience.

You'll also find out:

  * why people can't resist peer pressure and why bestseller lists are a very effective way to activate this behavior,

  * why fear is the most persuasive buying factor,

  * why fatty foods can be more addictive than cocaine,

  * why lipgloss is addictive and

  * how some of your product choices as adults are formed in the womb.

### 2. The products and brands children are exposed to determine their preferences as adults. 

Why is it that many adults still purchase the same juice they drank as a child? And why can some adults easily recall the advertising jingle for the cornflakes brand their mother used to buy?

As children, we're exposed to many products and brands, and these make a lasting impression on us well into adulthood.

In fact, we begin forming these preferences even _before_ we're born. While in the womb, a fetus is able to perceive sounds from the external world. So, if a mother likes a specific tune, she'll share with her baby the positive emotions she has while listening to it. And because unborn children are able to recognize melodies — including the jingles of advertisements — hearing or recalling such melodies later in life will trigger the same positive feelings.

Then, once we're born, the media bombards us with hundreds of brands every day, in web advertisements, in video games and on TV.

In fact, a Nickelodeon study found that children are exposed to so many ads that by the time they're ten years old they will have memorized 300–400 brands. Of these hundreds of brands, children will form relationships with some that last well into the future.

Such relationships are often formed because children believe that brands help them to establish friendships with others. For example, one preschooler in a 2009 study demanded to have LEGO because he believed that otherwise none of the other children would want to play with him, or even like him.

The final reason for our preference for certain brands is that children believe everything in their family life is the norm. Therefore the products their parents buy become their favored products well into adulthood. For example, children who see that their parents purchase a particular brand of orange juice again and again will come to think of it as the "normal" orange juice.

Later in life, the adult will associate brands such as this with positive, nostalgic feelings. For instance, the brand will remind him or her of a comfortable childhood home, and the warm affection of a beloved family. This is precisely why we continue to purchase the same brands in adulthood.

In the following blinks, we'll learn about several other factors that influence our behavior as consumers.

> _"Recent studies have shown that by the time they are 36 months old, American children recognize an average of 100 brand logos."_

### 3. Fear can lead us to make irrational decisions. 

Many people think that because they enjoy watching horror movies or going on ghost walks, they enjoy being scared.

But, this isn't _real_ fear. When people are actually afraid, when they fear for their lives or their safety, they do whatever they can to escape the threat.

And often this means that we act irrationally.

Why?

Fear is a biological reaction to danger: our most important motivator is our survival, so our brains are wired to make us act quickly when we're threatened.

More specifically, an area of the brain responsible for fear — the _amygdala_ — can override the part of the brain responsible for rational behavior (the prefrontal cortex), which reacts much more slowly.

And so when we feel threatened, we act quickly to maintain our safety, but in ways that are often not very sensible.

For example, whenever there's a global flu pandemic, we try to protect ourselves by using hand sanitizers, which seems like a sensible reaction to have. Yet we do this without thinking: there's no proof that they actually help, but because we're afraid of the threat to our survival, we rush to use them anyway.

Moreover, we're not only scared of actual danger, but also of a future "_feared_ _self_ " — a version of ourself we really want to avoid.

For instance, no one wants to ever be described as appearing tired, unfit or unclean. So, to avoid this, people act in irrational ways — especially when buying products.

In one 2008 study, consumers were shown a future _feared_ _self_ version of themselves, which proved the most successful way to persuade them to make a purchasing decision. Just think, for example, of the huge amounts of money that people spend on cosmetics, just to ensure that others don't see them as unkempt or unattractive.

> _"Clearly, fear is a powerful persuader, and you'd better believe that marketers and advertisers know it and aren't afraid to exploit it to the fullest."_

### 4. People can become addicted to products and shopping. 

Can you imagine a life without your smartphone, your favorite coffee or lipstick? Many people can't, and some can even become truly addicted to such products.

This addiction can range from being unable to put your phone away for more than an hour, to very serious problems like spending so much on shopping that you bankrupt yourself.

In fact, this addiction is a widespread problem in modern society, with many people considering themselves addicted to a certain product. For example, one study found that 34 percent of Stanford University Students admitted that they were addicted to their smartphones.

Moreover, this kind of addiction has exactly the same effect on the brain as other, more recognized addictions, such as drugs or love, which is, technically speaking, an addiction to your lover.

For instance, a study found that when young Americans aged 18–25 heard the ringing or felt the vibrations of their iPhones, their _insula_ — a region of the brain — was activated. This region is also activated when we are in love.

Shopping addiction, on the other hand, works in a slightly different way. When they shop, a shopping addict's brain is flooded with dopamine, a chemical that makes them feel high and gives them a feeling of well being.

Soon after the experience, however, dopamine levels return to normal and the addict's mood will drop.

Having experienced such a high, they will want naturally to do it again — and soon. But in order to keep feeling a "rush," they will need to increase their "hit" each time, which means that the amount they purchase increases each time they go on a shopping binge.

### 5. Our need to fit in means that we are heavily influenced by peer pressure. 

When somebody stares at something behind you, do you turn your head around to see what they're looking at?

If so, you're not alone — most of us tend to follow group behavior. Moreover, this is a very natural process: we tend to observe what people around us are doing and try to do the same ourselves.

This phenomenon is known as _peer_ _pressure_. But what's behind it?

Humans are a social species. We want to be part of a community, so in our evolution we've developed an instinct to do whatever we can to fit in with the tribe.

For instance, in one study 14-month-old babies were trained to play with specific toys, and then untrained babies were brought in to watch them at play.

Two days later, when the researchers brought the toys to the untrained babies, they knew what to do with them because they'd closely observed how the other babies played.

Furthermore, this need to fit in means that people feel very uncomfortable when they're left out. In one experiment, a group of 200 people were told to walk around a room, but only ten of them were instructed to walk in a specific direction. After a while, all 200 participants were walking in the same direction, because everyone wanted to fit in with the group as a whole.

Finally, peer pressure means that we want what other people have.

For example, in another experiment, cookies were offered to people gathered in a room. At first, just one in five of them took a cookie. Then researchers sent a person into the room to take a cookie without being offered one. Suddenly everyone in the room began taking cookies.

Such behavior is due to people's fear of "missing out," and so when one person takes something for herself, the entire group is likely to follow.

### 6. The warm feeling of nostalgia is good for our health, but it can make us remember the past falsely. 

As we grow older, we tend to remember the TV shows we watched as children in a positive light. Even though they might've been cheaply made cartoons, we're likely to think of them almost as works of art.

The reason for this is _nostalgia_ _–_ an extreme and sentimental longing to return to a past period.

But why do we have this longing?

Firstly, nostalgia is important for our well-being. Recalling the past in a positive light — or _rosy_ _remembering_ — is a way to protect ourselves from painful memories that could limit our future actions.

For example, if women consistently remembered the pain of childbirth in intricate detail, they'd probably choose not to go through the process again.

But if they only remember the positive aspects of the experience — such as the joy of a newborn baby — they may be willing to go through it again.

Secondly, thinking about "the good old times" can dramatically improve one's mood, and even strengthen relationships.

In a 2006 study, for example, students were asked about their social competence — such as being able to offer emotional support for friends, and building relationships — and about how often they have nostalgic thoughts. It turned out that those students with a tendency for nostalgic thinking had far stronger social skills.

However, there's a dangerous side to nostalgia. Sometimes it can lead us to remember situations and events that never actually occurred.

In a famous experiment, researchers convinced people that they had a childhood memory of seeing Bugs Bunny at Disney World. Yet this is an impossible situation, as Bugs is a Warner Bros. character.

However, by making the participants feel the warm glow of nostalgia, the researchers were able to manipulate their memories.

Now that you've learned about the factors that influence our brand preferences, it's time to look at how marketers use this knowledge to manipulate our purchasing decisions.

### 7. Because children carry brand preferences into adulthood, companies target children from a very early age. 

Nowadays children are surrounded by marketing — whether it's the commercial break between their favorite TV series, a teddy bear with a company logo or the funny game on the back of their pack of cornflakes.

Companies target children in this way because they know that capturing their attention at an early age can mean that the child becomes a loyal, lifelong customer.

But how exactly do they manage to appeal to kids?

One effective way to influence children is to use entertainment to make them desire a certain product. For example, Kellogg's created an iPhone racing-car game for their "Apple Jack" cereal, in which children collect cereal icons for extra points.

This strategy has many advantages. Firstly, the games help companies to get around legislation that prevents them from advertising directly to kids on TV. Secondly, children who play such games act as "viral marketers," spreading awareness of a brand among their peers as they play together.

Once a company has managed to make their brand appeal to kids, they then want to ensure that these children stick with their brand into adulthood.

To that end, companies aim to get kids to use "grown-up products" as soon as possible.

Even though children are more likely to stick with the brands they favor in early age, brands try to ensure this by targeting children with adult or teenage products.

For example, cosmetic firms, such as Bonne Bell, offer cosmetics to girls as young as seven years old. One study found that their campaigns seem to be working: from 2007 to 2009, the percentage of girls aged eight to 12 who already use mascara regularly almost doubled from ten to 18 percent.

Another (slightly more shocking) example is the birthday parcel that R. J. Reynolds _Tobacco_ _Company_ sends to teenagers on their eighteenth birthday, which contain coupons for their menthol cigarette brand "Kool" and CDs of new rock bands.

### 8. Companies play on people’s fears by creating products that dramatize the dangers that surround us. 

As we've already learned, people behave irrationally when they're scared — a fact that firms use to their advantage.

For example, the firm Broadway Security created a commercial in which a mother is depicted preparing dinner in the kitchen, oblivious to the fact that there's a stranger nearby watching her children playing in the yard.

By tapping into people's fear, the company managed to get people to rush out to buy the company's security equipment. In fact, sales for their alarm systems increased by ten percent.

Because they were gripped by fear, these customers didn't stop to think rationally. If they had, they'd have realized that the crime rate was _declining_, and that they were actually safer than ever!

Furthermore, to make their advertisements even more effective, companies often play on a combination of fear and guilt.

That's because when people feel guilty _and_ afraid, they're more likely to act on their fears.

For example, Thai Life Insurance made a video in which a man driving a car suddenly realizes that he should spend more time with his teenage son. However, the moment he has the realization, a bus smashes into his car, killing him immediately.

This advert is effective because people are not only scared of dying, they also feel guilty that if they die their children will be left alone.

Firms also play on people's fear of becoming something they don't want to be.

For instance, those suffering from negative traits, like obesity or baldness, are always looking for ways to get rid of their issues. Companies play on this by advertising products that make their problems seem far worse.

Most of these adverts show someone with an issue that makes them really unhappy before it is solved by the product. For example, a commercial for Flonase allergy spray shows a miserable woman trapped in her house while her family is having fun outside.

But after taking the spray she's able to overcome her problem, and finally go outside to join the others.

### 9. Products are designed to evoke a physical and emotional addiction. 

Addiction to shopping and products can make people act in irrational ways. Companies are well aware of this and so design their products to be as addictive as possible.

For instance, food companies use special ingredients to make their products addictive.

In one study (using rats), researchers found that food that's high in fat and sugar is very similar to cocaine in terms of its effect on the brain. Both are responsible for a release of dopamine, and, over time, we require increasing amounts of both to achieve the original level of satisfaction.

In the study, some rats were addicted to food, others to cocaine. Incredibly, the effects of addiction on the brain in the obese rats lasted seven times longer than in those addicted to the drug.

For another example of just how addictive food can be, consider the woman in New Zealand who became addicted to Red Bull to the extent that when she abstained she showed classic withdrawal symptoms, such as shaking.

It should come as little surprise that Red Bull can be so addictive, since one can contains 27 grams of sugar!

However, it's not just food companies whose products are filled with addictive ingredients. Take, for example, lip balm, which often contains menthol — which is used sometimes in cigarettes to make them more addictive — and phenol, a carbolic acid that interferes with the lips' ability to moisten themselves. As a result, using lip balm actually _dries_ the lips, which, in turn, necessitates the use of more lip balm.

The product is made even more addictive by the ritualistic act of applying it. This means that the constant application of lip balm becomes almost automatic after a while, and people start to apply it without conscious thought.

### 10. Marketers create an artificial peer pressure to sell their products. 

We all know "must-have" products: whether it's a handbag, a make of car or a luxury chocolate bar. The reason people have such a strong desire for these products is that no one wants to be left out of a good thing.

Companies are aware of this and therefore go to great lengths to convince customers that their product is the one thing they _absolutely_ _must_ _have_.

But how exactly do they generate such a buzz?

One way is through customer reviews, as people are far more likely to purchase something when they know how others feel about it. Customer reviews are the perfect way to see this.

For instance, a 2008 study found that almost half of the American consumers who had shopped online at least four times a year, and who spent a minimum of $500, needed to consult between four to seven customer reviews before they felt comfortable making a purchasing decision.

Although many people are aware of the fact that approximately 25 percent of reviews are faked, they still want to believe customer reviews.

Another way to create peer pressure to buy a product is through bestseller lists. Such lists create the impression that the products were preapproved by experts, so people automatically believe that they must be good.

For example, on the iTunes start page, there are categories like "What's Hot" and "What we're listening to." The records that are listed there are very often _not_ the approved choices of music experts but rather the result of negotiations with record labels who pay for the start page positions.

Nevertheless, people believe in the expertise of those rankings and so it's very likely that well-placed records become actual bestsellers, because people don't want to miss out on what appears to be trending at the moment.

> _"When it comes to the things we buy, what other people think matters. A lot. Even when these people are complete strangers."_

### 11. Marketers have many different strategies for creating a nostalgic feeling with their products. 

One of the main tricks companies use to sell their products is to appeal to the sense of warmth we get from nostalgic feelings.

For instance, many companies bring back slogans or commercials from the past. In 2009, for example, Heinz revived a 1970s advertisement in which mothers were shown serving their children baked beans, with this slogan: "Sometimes when I'm feeling sad my mum will read the signs. She knows that beanz meanz Heinz."

With this strategy, the company creates a connection between the product and the nostalgic memories of that time. This connection doesn't even require people to have actually consumed Heinz Baked Beans in the past, because their brains associate their memories of the product with all the other positive feelings they had at that time.

After this relaunch, "Beanz meanz Heinz" was voted the most popular slogan by Advertising Hall of Fame.

In addition to reviving past commercials to trigger nostalgia, companies also represent their products as being imperfect. Food companies, for example, try to make their food appear cosmetically flawed because it reminds consumers of a time when food was produced manually, and without artificial ingredients.

Most people like to think of a time when food was fresh rather than mass produced, convinced that everything was much healthier and better at that time.

Food companies and retailers use imperfect details in their representations of their products, so that they'd appear to have been produced individually.

For example, the author saw the luxury chocolate Ghirardelli sold as chunks, packed in brown paper bags with old-fashioned handwriting on it. This created the impression of individual production and cutting.

However, when he bought two pieces of the chocolate it was clear that they were totally identical, in form and size. Although the chocolate chunks were mass produced, the company had gone to great lengths to disguise this fact!

### 12. Final summary 

The key message in this book:

**Advertisers** **and** **marketers** **know** **your** **psychological** **weak** **spots.** **They** **use** **this** **knowledge** **to** **their** **advantage,** **designing** **products** **to** **be** **addictive,** **tapping** **into** **your** **feelings** **of** **fear** **and** **nostalgia,** **and** **playing** **with** **your** **need** **to** **belong.** **By** **learning** **about** **these** **tricks** **in** **detail,** **you** **can** **begin** **to** **make** **better,** **more** **rational** **buying** **decisions.**

Actionable advice:

**Hack** **your** **nostalgia** **at** **the** **supermarket.**

The next time you're shopping for your usual breakfast juice, ask yourself if you prefer a certain brand only because of nostalgic feelings and habit. If so, it could be a great day to try a new brand of juice, and perhaps even discover a better one!

**Beware** **of** **dramatizing.**

Try not to make spontaneous buying decisions after watching a frightening commercial. If you're afraid, you're more likely to act irrationally. Marketers may be dramatizing an issue just to make you buy their products.
---

### Martin Lindstrom

Martin Lindstrom is a Danish author and a successful marketing expert for some of the world's largest companies. He's currently a consumer advocate and has written six books about brand marketing and buying behavior. Lindstrom also writes columns for several journals such as the _Harvard_ _Business_ _Review_ and was voted one of the World's 100 Most Influential People by _Time_ magazine in 2009. His previous book, _Buyology_, was a _New_ _York_ _Times_ bestseller.

