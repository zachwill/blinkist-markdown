---
id: 532838033635380008340000
slug: brave-new-war-en
published_date: 2014-03-18T09:17:25.000+00:00
author: John Robb
title: Brave New War
subtitle: The Next Stage of Terrorism and the End of Globalization
main_color: B48345
text_color: 694C28
---

# Brave New War

_The Next Stage of Terrorism and the End of Globalization_

**John Robb**

Modern technology and globalization have made it possible for one man to wage war against an entire country and win. Although it might seem unbelievable, it's not.

Technological advances like the internet have made it possible for groups of terrorists and criminals to continuously share, develop and improve their tactics. This results in ever-changing threats made all the more dangerous by the interconnected nature of the modern world, where we rely on vital systems, like electricity and communication networks, that can be easily knocked out. _Brave New War_ (2008) explores these topics and gives recommendations for dealing with future threats.

---
### 1. What’s in it for me? Find out how vulnerable your supposedly safe and pleasant life is. 

These days you can hardly pick up a newspaper without reading about a recent terrorist attack. It seems like we're more vulnerable than ever before and, in the following blinks, you'll find out why many states, despite their immense military resources, consistently lose battles to small cells of terrorists.

You'll also discover why the safe life that nation states provide today is totally dependent on a number of systems that could topple like dominoes if struck on the right spot.

Finally, you'll also come to understand why terrorists can't be stopped by undermining civil liberties and trampling on the privacy of citizens.

### 2. Large, resourceful nation-states can no longer dominate warfare. 

For the past four hundred years, most wars have been fought between two or more nation-states for control of a geographic area. In such conflicts, the largest states with the biggest military capacities tended to prevail.

But with the advent of nuclear weapons and the growing global interconnectedness of the world, larger states no longer dominate warfare so overwhelmingly.

Since nuclear weapons were developed in the mid-twentieth century, it has become increasingly unlikely for two developed nations to come into direct conflict. This is due to the doctrine of _Mutually Assured Destruction,_ according to which no state with nuclear weapons can be attacked without the attacker also being devastated in a matter of hours. Large armies became essentially useless in the shadow of these weapons.

Secondly, nation-states have become more interconnected through trade, among other things, meaning that any conflict would hurt their economies. At the same time international bodies like the UN work to uphold peace by refusing to legitimize most conflicts, further decreasing the value of large armies.

Another factor decreasing the advantage of countries with large armies is the trend toward _proxy wars,_ which are not fought by the actual states, but by proxies such as guerrillas.

Both large and small states have waged proxy wars when they could not directly engage each other. For example, the United States used guerrillas to fight against the Soviet Union in Afghanistan, while Iran and Syria used Hezbollah terrorist proxies to bomb a US Marine barracks in Lebanon.

Guerilla warfare means avoiding large battles in favor of small-scale attacks that wear down the enemy gradually, negating the advantage of large armies, which can be bled dry this way.

Over the past half century, the advantage of large nation-states in warfare has been gradually eroded to the point that large armies in theaters, like Vietnam, Iraq and Afghanistan, have struggled greatly against guerillas.

### 3. New technologies, like the internet, are weakening the power of the nation-state. 

Ever since the concept was first established in the treaty of Westphalia in 1648, the sovereign nation state has been the most powerful entity in global society. States have asserted control over their economies, security, people and communications, while crushing possible rivals for this control, like religions, tribal loyalties and empires.

But the emergence of new technologies has begun to erode the power of the state. The most obvious example of this is the internet, where people can share ideas openly and buy goods and services from almost anywhere on the planet. This means states can no longer control their economies or their citizens' access to new ideas.

The consequence of such new technologies is a decline in the power of nation-states.

Consider, for example, the realm of security: protecting citizens from attacks has always been a priority for nation-states. But today, new technologies have led to sophisticated and powerful terrorist and insurgent networks that are hard to uncover and have the potential for inflicting enormous damage. To stop such ever-present threats, the state would need to dedicate huge amounts of its resources to security: resources it doesn't have.

To make up for this gap, the private sector is increasingly charged with providing security services which can be seen, for example, in the number of private security firms contracted in Iraq and Afghanistan to protect politicians and businesses.

The nation-states' inability to provide security for their own citizens is clear evidence of their eroding power caused by new technologies.

### 4. Criminal and terrorist organizations are deliberately undermining the nation-state. 

The weakening power and security problems of the nation-state can be seen in its constant struggle to keep terrorists, drug cartels and international gangs at bay. This phenomenon is likely to continue.

Across the globe, various criminal groups are likely to continue gaining power. These groups can have many forms, including terrorists and insurgents, as well as international gangs engaging in human smuggling, counterfeit goods, drugs and so forth. These groups are known as _global guerillas._

They're able to thrive because of the huge and growing global black market, currently worth between $1 and $3 trillion, and growing at seven times the rate of the legal economy. This expansion is fuelled by new technologies like the internet, which greatly eases cross-border trade.

Although each global guerrilla group has their own separate agenda, they do occasionally share a goal and can work together.

One of the main desires of the vast majority of these groups is to undermine the state.

In the past, this goal meant taking over and replacing the current state power, but that's not the aim anymore today. Instead, global guerrillas want the state to fail because a non-functioning infrastructure gives them a rich array of opportunities: weak or failed states provide terrorists and insurgents with a constant stream of disillusioned supporters, and their feeble public institutions and laws allow criminals to thrive.

Often, the groups want to totally destroy the entity of the nation-state. This can be seen with Al-Qaeda, which wants to destroy all the nation-states in the Middle East and replace them with an Islamic empire.

Across the globe, the nation-state is increasingly under attack from global guerrillas who aim to hollow it out.

In the following blinks, you'll discover what tactics global guerrillas are using in their battle against the nation-state and how successful these strategies are.

### 5. Global guerrillas maximize the damage they inflict by targeting society’s vital systems. 

In 2004, Iraqi insurgents decided to sabotage an oil pipeline in their country. After careful planning, they proceeded to blast a hole into it, creating an enormous oil spill. It took the authorities in Iraq a week to fix the pipeline, by which time the oil lost had cost the struggling Iraqi economy over $500 million in export revenues. The mission had cost the insurgents a mere thousand dollars, so their effective return on investment was an astounding multiple of 250,000.

This is a prime example of a tactic increasingly used by global guerillas: _systems disruption._ Systems disruption involves deliberately targeting key points in the processes vital for a society. This does not necessarily mean oil, but could also be areas like transport, electricity and communication networks. Rather than trying to maximize casualties, which was their previous tactic, systems disruption allows the groups to inflict the most damage on their target states with the least cost to themselves. 

This tactic is so effective because states depend on interconnected systems. The electricity network, for instance, is interconnected to many other systems like the transport and communication systems.

Such interconnected systems are vulnerable to collapse because there's a point in the entire system which, if destroyed, will bring the whole system down with it. This is what's known as the _systempunkt_. And once it's knocked out, it will unleash a so-called _cascade of failure,_ a chain reaction that destroys all the other systempunkts, too.

This was illustrated by the Iraqi insurgents' operation: by punching a single hole in the pipeline, they brought the entire Iraqi oil industry to a standstill. Even such small operations, if carefully targeted, can paralyze entire states and undermine their credibility in the eyes of their citizens.

### 6. Like software developers, global guerillas develop their tactics and weapons via open-source networks. 

Over the past decade, the development of software has been greatly accelerated through the use of _open-source networks_. In such networks, one person or group may develop a piece of software and then share it with other people so they can also work on improving it. An example of this is Apache, a dominant web server developed by hordes of programmers collaborating through open source networks.

Just like software designers, such open-source networks are also increasingly used by global guerillas to share and develop their tactics. This is known as _open-source warfare_ (OSW), where potential tactics, targets, weapons and so forth are openly discussed between various groups, with each group testing, improving on and sharing successful ideas. This is made possible by the internet, where global guerrillas from all over the world can share ideas and form mutual strategies.

The emergence of OSW has made it very difficult to stop global guerillas because the networks can easily contain hundreds of people and are constantly evolving. In the past, security services could stop terror cells by infiltrating them or removing their leadership, but OSW means that there is no clear group leader to kill, and any information gained through infiltration will rapidly become obsolete.

For example, in 2006, US forces killed the Iraqi insurgent leader Abu Musab al-Zarqawi and celebrated this operation as a crucial step in defeating the insurgency. But due to OSW — it wasn't. While Al-Zarqawi had been an important commander in the early stages of the insurgency, by 2006, the group had already moved on and no longer needed overall commanders. Al-Zarqawi had become a mere figurehead and his death stopped nothing.

### 7. The internet has provided warfare with a “long tail,” allowing even small global guerilla groups to become threats. 

After coalition forces invaded Iraq and overthrew Saddam Hussein's regime, an insurgency against the occupying forces and the new government of Iraq ensued. Though official figures suggested that between one and three thousand insurgents were being killed or captured each month, the insurgency continued. How could they do this with such losses?

To understand this phenomenon, it's first important to look at an analogy from the realm of business, where globalization and the internet have revolutionized the way that markets work.

For example, consider that a typical large bookstore in the US carries about 130,000 titles, while a global online bookstore like Amazon carries more than a million. The difference has a huge effect, as Amazon generates over 50 percent of its book sales from the "niche" titles outside the 130,000 that regular bookstores carry.

This illustrates the fact that, due to globalization and the internet, markets now have _long tails_, i.e., it isn't a few dominant products that capture the majority of the market, but numerous less-dominant products that share it.

Similar long tail effects can be seen in insurgency and warfare too: instead of just a few dominant groups, there are many tiny groups and factions spreading their message over the internet, seeking supporters and sharing ideas. So instead of facing just a few large homogenous terrorist groups, society must now deal with countless tiny ones.

The insurgency in Iraq is a prime example of this. At the height of the insurgency, there were at least 75 separate groups of insurgents, each operating based on their own ideals, e.g., loyalty to Saddam Hussein, or for tribal or religious beliefs. Each group occupied its own niche, but they all worked toward their common goal of fighting coalition forces.

This splintered nature of the groups explains why coalition forces kept being attacked by insurgents, no matter how good the numbers looked. There were always more insurgent groups ready to fight even if one was knocked down.

In the following blinks, you'll learn what society must do to prevent global guerrillas from succeeding and destroying our way of life.

### 8. The traditional approaches to security by nation-states are inflexible and ineffective. 

The nation-state has long provided security for its citizens through centralized bodies like the military, the police and security services. Today, however, this method is becoming less and less effective.

That's because the threats we face change constantly and it's difficult for our existing security systems to keep up. It seems we're always one step behind the global guerillas, learning from previous attacks but never anticipating the next ones.

This phenomenon is similar to the _black swan_ concept presented by Nassim Nicholas Taleb: we have a tendency to trick ourselves into thinking we can predict enormous, unexpected events, and thus are always completely surprised by such inherently unpredictable events.

For example, no US government agency was prepared for the September 11 attacks, and if they had been, the attacks would have been stopped. Nevertheless, in the aftermath, people began to feel that the attacks were predictable and so security measures in airports were tightened in an effort to prepare for future attacks. But the next attack probably won't be like 9/11 because, as we've learned, systems disruption will be a more likely tactic.

In the face of flexible and adaptable global guerrilla groups, security agencies have started to resort to highly questionable and unworkable tactics. Some people feel that many nations are turning into police states as security agencies tighten their grip to combat terrorist and criminal organizations.

For example, the United States' National Security Agency (NSA) analyzes the personal data of individuals, both domestically and internationally, while its other agencies have been reported to use "enhanced interrogation techniques" and torture.

Such legally and ethically questionable tactics do nothing to stop terrorists, but they do hurt the legitimacy of the state, especially in nations like the US that pride themselves on being moral leaders.

These developments demonstrate that we can no longer rely on the state to keep us safe. We must look for new strategies, especially in terms of keeping our vital societal systems safe.

### 9. To deal with the threats of the future, we need to decentralize our vital systems. 

As we have seen, today's society is too inflexible and interconnected to effectively deal with the threat of global guerillas who can inflict massive damage with little cost to themselves.

Fortunately, there is a remedy: decentralizing our vital systems. This would make them more robust in the face of an attack.

Currently, we don't know where the next attack will come from, and we cannot hope to protect all our vital systems from system disruption. If, however, we decentralize the systems, separating them and making them more independent, any attack will be less destructive, as it won't be able to cause a cascade of failures.

Using _platforms_ is one example of how this can be achieved. These are tools that allow many people two-way access to a system so they can act both as users and producers in the system. The internet is a prime example of a platform where people can produce and upload software, or download software made by others.

Let's take a look at the electricity grid, one of society's vital systems. This principle can be applied by, for example, allowing all users to input power from their own source, most likely from solar panels. The soaring number of producers would make the network far more robust, as knocking out one producer would only have a negligible impact.

This kind of decentralization would stop systems from collapsing even if we have no idea what the target of the next attack will be.

### 10. Final summary 

The main message of this book is:

**The future threats nation-states face will take advantage of our dependence on highly complex and interdependent systems. At the moment, terrorists and criminals can cause immense damage by targeting such systems at a low cost to themselves. And the nation-state, the entity which we have relied on to combat threats to our security, is simply too inflexible and weak to do the job.**

**In the future, to combat the increased flexibility and decentralized nature of criminal groups, we must become more fluid and autonomous ourselves.**

Actionable advice:

**Be careful of which economy you are contributing to.**

Across the globe, the black market economy is growing rapidly. And participating in this illegal economy, whether by buying drugs or counterfeit clothing, can help finance the very groups that threaten our society. Consider that the 2004 attacks on the transport network in Madrid were funded by the sale of ecstasy and hashish: a harsh reminder of what you may be contributing to if you are considering dealing in the black market.

**Don't take vital systems for granted — be prepared for their disruption.**

Our society is heavily dependent on certain networks, like the electricity, transport and communication networks. Many of us take these systems for granted, thinking they will always be there. But, in fact, these systems will increasingly be the targets of attacks, so we should ensure that we are prepared to survive and thrive without them.
---

### John Robb

John Robb is a military analyst and high tech entrepreneur who now advises corporations and governments on future terrorist threats.

He has written several articles on war and military strategy, and blogs on _Global Guerrillas_.

