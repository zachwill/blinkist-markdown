---
id: 5a6c4741b238e100076e8489
slug: braving-the-wilderness-en
published_date: 2018-01-29T00:00:00.000+00:00
author: Brené Brown
title: Braving the Wilderness
subtitle: The Quest for True Belonging and the Courage to Stand Alone
main_color: 43AAC4
text_color: 296878
---

# Braving the Wilderness

_The Quest for True Belonging and the Courage to Stand Alone_

**Brené Brown**

_Braving The Wilderness_ (2017) challenges common notions about what it means to belong. It links feelings of unbelonging to feelings of anger and unrest, both in the United States and abroad. Brené Brown uses a potent combination of scientific research and storytelling to reveal what it means to truly belong. This includes remarkable tales of pain and suffering that show just how far people are willing to go to gain a sense of belonging.

---
### 1. What’s in it for me? Learn how to feel less like an outsider by better understanding yourself. 

Every day, we're reminded of how divided the world is becoming. There was once a time when we thought modern technology was going to bring us all closer together — that a global community would form around common goals and ideals. But the internet has mostly made it easier to find others who already share our opinions. As a result, we tend to surround ourselves with like-minded people, a fact that's only led to greater division.

No one wants to feel isolated or alone, so it's tempting to join a group, even if it happens to be a militant faction. Yet these groups often only serve to increase the feeling of loneliness, especially when they offer no personal connections. This is why the real solution lies within. Only by looking inward and asking the tough questions can we find the peace that comes with truly belonging and knowing our place in the world.

In these blinks, you'll discover

  * how a permission slip got Brené Brown a guest spot on _Oprah_ ;

  * the tools you need to make it through your personal wilderness; and

  * how tragedy can make you stronger.

### 2. We spend our lives looking for true belonging, and it can be painful to go without it. 

From childhood to adulthood, we're all trying to find our place in the world — somewhere where we fit in perfectly and don't stand out. In other words, we're all searching for a sense of _belonging_, which is not only a desire but one of our primary needs.

If we don't find this feeling of belonging, it can be deeply painful.

When the author, Brené Brown, moved to New Orleans, racism in the country was far worse than it is today and schools were still segregated. Due in part to her name, Brown found herself unwelcome in both black and white circles. Brown's middle name, Cassandra, was considered African-American by her white classmates, so she was teased and never invited to their birthday parties.

Brown did get invited to some parties by her African-American friends, but their parents were often shocked to see a white girl show up. Even though this was long ago, Brown can still recall the pain of being a four-year-old and feeling as though she didn't belong.

But even worse is feeling as though you don't belong in your own family, which can become dangerous.

When Brown was a teenager, her family moved back to Houston. At her new school, she wanted desperately to be part of the Bearkadettes, the school's cheerleading team. In preparation for the tryouts, Brown went on a liquid diet that lasted two weeks. She practiced her routine non-stop. But, despite the fact that she nailed her performance, Brown was told she just wasn't Bearkadette material.

This was particularly heartbreaking for her since her mom had been a Bearkadette and her dad was once the star of the football team. Both parents voiced their disappointment, so she not only felt rejected by the cheerleading squad; she felt rejected by her family as well.

Brown carried this feeling into adulthood, and, to fight the pain, she began drinking.

Later, she joined Alcoholics Anonymous, but this support group brought little comfort. Her AA sponsor told her that she didn't really fit in with the group and that she should try the Co-Dependents Anonymous group instead, which helps people form healthy relationships. But guess what — they didn't want her either and recommended she look elsewhere.

The problem was that Brown had too many addictions. Drinking and getting into co-dependent relationships were just the tip of the iceberg. These problems could be traced back to the rejection she felt as a child, and the pain of being made to feel she didn't belong just continued to pile up.

### 3. True belonging means belonging to yourself and no one else. 

Rejection isn't the only thing that can cause a feeling of unbelonging. Sometimes one can be welcomed into a club or sorority and still feel like an outsider; other times, one may feel self-loathing for how much effort one has put into trying to please others in order to gain acceptance.

When you think about it, we spend an awful lot of time and energy trying to belong, either to something or to someone. But what the author has come to realize is that true belonging happens when you stop seeking the acceptance of others, when you no longer try to belong to them.

In 1987, Brown met Steve, the man she would eventually marry. At the time, Brown was in full self-destructive mode; partying, drinking and smoking. But Steve was special; he saw through all this and recognized the woman she truly was. Thanks to Steve's support and recognition, she slowly became true to herself and quit the self-destruction.

This relationship helped Brown start the healthy part of her journey toward self-belonging. Thirty years later, the journey would take her to a life-changing appearance on _Oprah_, where she explained her self-belonging strategy.

Brown showed Oprah how she started to write herself permission slips, explaining that her very first one was to allow herself to be goofy, to enjoy life and to have fun. This is how Brown's self-belonging strategy led to her being able to turn her life around and have a great time. Oprah was so captivated that she ended up calling Brown back for a second episode.

At the end of her first appearance on _Oprah_, Brown had the chance to meet one of her idols, the poet and activist Maya Angelou. Angelou offered some advice Brown remembers still. She said, "Do not be moved, Brené," which meant that she shouldn't ever change to accommodate another person's opinions.

Six months later, Brown was in Chicago to speak at an event that requested she wear "business attire." At first, she went along with it and dressed up, but she felt phony in the fancy outfit. So she quickly changed into her normal outfit of jeans, shirt and clogs, and, when the event was over, she received the highest rating of all the speakers in attendance.

> _"True belonging is about breaking down the walls, abandoning our ideological bunkers, and living from our wild heart rather than our weary hurt."_

### 4. Belonging requires bravery and trust while you journey through the wilderness. 

What is it about the wilderness that captures our imagination? Whether it's the Bee Gees singing "Voice into the Wilderness" or Jon Krakauer writing about adventure in _Into the Wild_, the wilderness has served as the muse for countless writers, musicians and philosophers.

For many of these artists, the wilderness is both scary and tempting, and the author uses it as a metaphor for the hurdles that must be overcome to achieve personal growth.

The metaphorical wilderness is any unknown territory that lies outside your comfort zone. It can be anything you're not used to or any place where you're forced to confront your uncertainties and vulnerabilities.

So your wilderness might involve unknown people, an unfamiliar situation or working up the courage to either say no or to say yes.

While everyone's path through the wilderness will be unique to them, all paths are bound to be challenging. As the mythologist Joseph Campbell wrote: "Your path is created with every step you take."

Your personal trip through the wilderness might take the form of intense introspection, solitude or spiritual learning. These will be the activities that lead to self-discovery and personal development. In fact, being able to venture into the wilderness alone is what will provide you with your sense of real belonging.

However, you can't enter the wilderness without first learning some "braving skills."

To successfully find your way, you need to know how to listen and you must be willing to engage in difficult and painful conversations. Rather than being controlled by fear and defensiveness, you need to be led by your curiosity and a sense of joy.

You will also need to practice trust — not just of others, but of yourself as well. This means making yourself, and the things you value, open and vulnerable to the actions of others. It takes courage to show trust and be vulnerable, but these are also abilities you can develop.

The author identified seven traits that make a person trustworthy. They are: being reliable, owning mistakes, respecting boundaries, keeping confidential information safe, making decisions with integrity, being non-judgmental of others and, finally, being generous.

Think these over and ask yourself: How much do I trust myself — and others?

### 5. By surrounding ourselves with unchallenging opinions, we’ve caused a loneliness epidemic. 

If you're at all familiar with the news these days, you've surely noticed how divided people are. Ideologically, politically and in matters of religion, people are more at odds than ever. But are these factions really improving our sense of belonging?

We certainly cling to groups in the hope of feeling less alone.

In 1976, only 25 percent of Americans lived in a county where the vast majority of residents all voted for a particular presidential candidate. In 2016, 80 percent of US counties voted either overwhelmingly for Donald Trump or overwhelmingly for Hillary Clinton. This shows that, over the past 40 years, people have surrounded themselves with like-minded individuals

But this is just another example of how we've gotten ourselves stuck in a "feedback loop," as the journalist Bill Bishop puts it.

Today, people only engage with TV shows, blogs, newspapers and magazines that reflect the opinions they already have. Rather than being challenged, we remain stuck in a cycle that validates our own views and further disconnects us from any outside information.

Belonging to a certain group is like being inside a bubble. And once inside that bubble, people tend to feel lonelier than ever.

This is why the number of Americans describing themselves as lonely has more than doubled since the 1980s. According to Julianne Holt Lunstad, a professor of psychology and neuroscience at Brigham Young University, chronic loneliness is a bigger health hazard than obesity, pollution or excessive drinking. Her research reveals that it increases the likelihood of premature death by 45 percent!

So, instead of groups, we should seek places and experiences that unify us and avoid the ones that have the opposite effect.

Brown has two kids, Ellen and Charlie, and they've often told her that certain restaurants and certain friends' houses leave them feeling lonely. Eventually she and her family realized that, in these places, meaningful connections never happened.

But other places and experiences do have the power to connect us and instill a sense of hope, especially events that revolve around art and music. To find belonging, we should seek such places, not factions.

### 6. To decrease loneliness, focus on firsthand experiences and don’t give in to anger. 

Once you begin your journey toward belonging, you'll find that it's a path filled with contradictions.

In order to deal with these you should focus on what you see and experience firsthand, not what you hear on television.

If all your information comes from one news channel, you might start thinking that all Democrats or all Republicans are selfish morons. But that's not how it is in real life. A day may come when a family emergency arises and you need a ride to the hospital — and the person most willing to help you is a coworker who votes differently than you do.

Things like this happen every day, and such human solidarity is more truthful than the political rhetoric used to divide people.

It's also important that we be civil to others, especially when we seek to correct what we consider to be their misguided views.

Brown grew up in a family where hunting was a normal weekend activity, and one day she was talking about teaching her son how to hunt game birds. A stranger overheard her and immediately accused her of supporting the National Rifle Association (NRA). Brown's first instinct was to yell at the stranger for making such an unfounded assumption. But instead she took a deep breath, smiled and gracefully explained that it's quite possible to support responsible gun ownership while opposing the NRA.

Another tip for uniting rather than dividing is to show neighborly compassion in times of happiness or sorrow, even if you don't know the person that well. This might mean holding hands with a stranger at a memorial event or cheering along your neighbor's favorite sports team, even if you don't normally follow sports.

Finally, remember that being courageous doesn't mean you can't remain vulnerable.

Jen Hatmaker is a friend of Brown's and a Christian community leader who supported the civil rights of the LGBTQ community, even when it meant being severely criticized by other Christians. Despite the hostility, she stayed open, graceful and compassionate to her critics, making her a true inspiration.

> _"The word "paradox" cuts right to the heart of what it means to break out of our ideological bunkers, stand on our own, and brave the wilderness."_

### 7. Instead of letting your anger turn into bitterness, use it to better understand yourself. 

Do you carry grudges? If you do, you know that staying angry at someone for a long time can be exhausting. That's because bottling anger up inside you builds resentment and diminishes your capacity for joy.

There is better a way to deal with anger, but it requires you to be vulnerable and have tough conversations with yourself. Unfortunately, we as a society tend to avoid this method.

After the 9/11 attacks, there was a healthy period of collective grief, but then, rather than putting this energy toward catching the mastermind, politicians channeled the nation's anger toward an enemy that was more ideological than anything else.

Rather than having meaningful discussions about religion, race, identity or any of the reasons behind the attack, Americans united around feelings of patriotism and put American flags on their houses and cars. But, as we now know, mere patriotism won't keep a nation united for long.

It would be better for us to learn how to express anger in positive, meaningful ways.

In a TED Talk from 2015, Kailash Satyarthi, a Nobel Laureate, explained how we all secretly keep our anger bottled up because we're told that it's bad to express it or let it out. But by repressing it, we're increasing the likelihood that it will come out in the form of violence, revenge or oppression. Hence the alarming number of mass shootings and racial violence in the United States.

However, anger can be a powerful force for bringing about a positive change in society, especially when it's used to challenge misguided beliefs.

When the author Antoine Leiris lost his wife in the 2015 Paris attacks, he spent the following days writing a letter entitled "You Will Not Have My Hate." In the letter, he explained to the killers that, though the love of his life had been stolen from him, he and his 17-month-old son would go on living their lives in peace, and not give in to anger or hatred.

By refusing to let bottled-up anger consume him and turn him into a suspicious, fearful person, Leiris defied the terrorists. He decided he would go on living with love in his heart. In other words, Leiris was true to himself, and created a way of belonging that wasn't reactionary.

### 8. Final summary 

The key message in this book:

**Every human being has an innate need to belong. A whole lifetime can be spent on the journey to find this feeling of belonging and, along the way, a person can change a great deal. That's why, in the end, belonging is really about finding out who we really are and accepting this discovery.**

Actionable advice:

**Disagree and debate without dehumanizing others.**

The next time you find yourself disagreeing with someone on social media, be self-aware and avoid dehumanizing insults. It's become commonplace for people to defend the politics or ideologies they believe in by calling other people hurtful names, but this isn't how real debate operates. You can disagree with Ivanka Trump without referring to her as a "bitch," or argue the pros and cons of Obama's politics without referring to him as an "idiot."

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _I Thought It Was Just Me (but it isn't)_** **by Brené Brown**

_I Thought It Was Just Me (but it isn't)_ (2008) is a guide to the visceral and thoroughly human emotion of shame. These blinks explain this complex feeling, discuss how it arises and describe ways in which empathy and connecting with one another can help humans heal.
---

### Brené Brown

Brené Brown is a research professor at the University of Houston and she's spent over 15 years studying a range of feelings, such as vulnerability, courage, empathy and shame. Her TED talk, _The Power of Vulnerability_, has received over 30 million views worldwide and she's written four best-selling books, including _Rising Strong_ and _Daring Greatly_.

