---
id: 584e849c3a41200004701628
slug: brazillionaires-en
published_date: 2016-12-15T00:00:00.000+00:00
author: Alex Cuadros
title: Brazillionaires
subtitle: Wealth, Power, Decadence, and Hope in an American Country
main_color: EBD62F
text_color: 6B6215
---

# Brazillionaires

_Wealth, Power, Decadence, and Hope in an American Country_

**Alex Cuadros**

_Brazillionaires_ (2016) exposes the true story behind Brazil's tumultuous economy. By tracing the rise and fall of billionaires like Eike Batista, these blinks take you through the country's history of inequality and corruption, and explain how the nation's politics and business have become inseparable.

---
### 1. What’s in it for me? Understand the shade and sunshine of Brazil’s economy. 

Though the largest country in South America and the fifth largest in the world, Brazil is, to many of us, a mysterious place. It's name tends to conjure clichés — samba music and bikinis. But, of course, there's much more to it than that.

With its checkered history, corrupt and bureaucratic government and widespread poverty, Brazil is, economically speaking, relatively backward. Yet, at the same time, it is a country with masses of natural resources and a key position in the southern hemisphere.

In the early aughts, it rose to international prominence, along with Russia, India and China, as one of the BRIC countries. These "Big Four" were seen as the potential future powerhouses of the world economy. And while China is still more or less viewed this way, Brazil's prospects seem much bleaker. So what happened?

These blinks help explain how everything for Brazil seemed to be coming up roses — just to end in a mess of thorns.

In these blinks, you'll learn

  * how brands like Budweiser, Burger King and Heinz are in the hands of Brazilian billionaires;

  * Why the example of Eike Batista perfectly explains Brazil's economy; and

  * how corruption has been a staple of Brazilian business since the nineteenth century.

### 2. For most of the population, life in Brazil is filled with struggle to acquire even the most basic services. 

When you think of Brazil, your first thoughts might be of Rio's famous Christ statue, Carnival, endless beaches and colorful samba dancers. But the reality isn't so patently picturesque.

For most Brazilians, it can be a struggle just to get the most basic things in life.

Every city in Brazil has _favelas_, neighborhoods filled with makeshift shacks built by poor residents in bits of open space — under a bridge, in an old parking lot, wherever. You can even find them in the alleyways tucked between skyscrapers.

But the favelas are just one facet of the problems Brazilians face. Others include clogged roads, a poorly functioning public school system and hospitals that often cause more health problems than they cure.

Even though Brazil's constitution guarantees free public healthcare, if there's a health crisis and you don't have private insurance, you'll likely wait an entire day in the emergency room.

There is also an astounding amount of bureaucratic red tape and paperwork for basic needs such as opening a bank account or getting a phone connected.

And trying to cancel one of these services can be even worse. When the author tried to switch his internet provider, for example, he ran into such resistance that he had to file an official complaint with the government's telecom agency.

Making things more difficult is the fact that all the paperwork needs to be stamped and notarized before it can be submitted to the government — a process that requires hours of waiting.

Therefore, getting even the most basic tasks accomplished in Brazil can send you into a Kafkaesque world of endless waiting, where filling out paperwork just leads to more paperwork!

The situation is so dire that every year it takes the average Brazilian company 2,600 man-hours to prepare its taxes.

For these reasons, there are _despachantes_, people whose job is to help others navigate all this red tape, a task which usually involves becoming friendly with the right bureaucrat.

### 3. Despite Brazil’s economic success in recent years, inequality and corruption continue to be considerable problems. 

Though Brazil has more than its share of problems, the country _has_ managed to make impressive progress.

Only a generation ago, the country was impoverished and struggling under the dictatorship which ruled the country from 1964 until democracy returned in 1990.

Since Brazil has adopted democracy, popular exports like coffee, sugar, soybeans and beef have helped make its economy the seventh biggest in the world. The nation also produces more oil than Norway.

And then there's 3G Capital, a corporation run by a trio of Brazilian billionaires who have come to own some of the most iconic American brands, including Budweiser, Burger King, Heinz and Kraft.

Yet despite these economic advancements, the rich and poor are still widely divided, especially in the treatment they receive from the law and the government.

Those who have resorted to minor crimes such as petty theft or selling marijuana often await trial behind bars and are sent straight to prison once they're convicted.

It's only those who are wealthy enough to pay for appeals and hire lawyers that can get their sentences suspended. Good attorneys can submit requests that take advantage of loopholes in the legal system and slow down the process while their clients stay out of jail.

This is what Paulo Maluf, a longtime politician and former mayor of São Paulo, the biggest city in the southern hemisphere, has been able to do.

Over the course of his 50 years in various offices, Maluf has been accused of astounding acts of corruption: In 2011, a Brazilian Supreme Court justice cited evidence that suggests he skimmed $19 billion from public works in just four years as mayor.

Though Maluf denies all accusations, he's also wanted by the international police organization Interpol for such crimes as corruption, conspiracy, money laundering and falsifying documents.

But, thanks to the Brazilian justice system, he's a free man and spent a mere 40 days in jail in 2005.

> _"'For my friends, anything. For my enemies, the law.'"_ Old Brazilian saying.

### 4. Brazil’s politics have a long history of corruption, bribery and favoritism. 

It goes without saying that Paulo Maluf isn't the only corrupt official in Brazil. In fact, the country has an established system of patronage, where officials distribute favors and titles among their friends — which is how many of Brazil's wealthy businessmen rose to power.

Patronage is a centuries-old tradition. When Portugal's King João VI fled Europe to escape Napoleon's advancing forces, he arrived in Brazil in desperate need of money. So, in 1808, to generate revenue, he sold Brazilian titles to his Portuguese friends in the royal family.

Since then, the deep roots of corruption have remained a fixed part of Brazil's culture, especially when it comes to business.

In 1964, the military overthrew the government, and about five years later they created a secret government agency called Oban. Established as a program to uncover subversive agents, Oban functioned by uniting Brazil's top businessmen and politicians.

Their methods were extreme: During interrogations, suspects were given electric shocks and violently beaten. One victim of Oban was Dilma Rousseff, who would later go on to be president of Brazil.

But the businessmen had little choice but to go along with the torture that was going on around them. One banker reasoned that, "It was either us or them." And cooperating with Oban also meant that the businessmen could keep themselves in good standing with the politicians that controlled Brazil.

Back then, the economy was struggling and the government credit that came from supporting Oban could keep a business alive.

And this cooperation between business and politics is still popular today; the most successful businesses are run by those who join forces with the powers that be.

Bribes are considered such a standard part of doing business that companies actually put themselves at a competitive disadvantage by _not_ taking part in these schemes.

Indeed, bribery is so widespread that economists calculate its effects account for the loss of $20 billion — one percent of the country's GDP.

### 5. Eike Batista’s experiences symbolize the country’s tumultuous economic transformation. 

Despite its reputation for corruption, Brazil's economy has long shown potential, which can be seen in the story of Eike Batista.

Batista was born in 1957 to a wealthy family led by his father, Eliezer Batista da Silva, who was the minister of mines and energy in the 1960s and president of the state mining company, Vale.

Eike began his business career in the early 2000s by buying the rights to iron ore deposits just as demands from China's steel factories sent prices skyrocketing.

But there was another reason investors were crazy for Batista. As the saying goes, people don't invest in products, they invest in people; and Batista was seen as a charming playboy, one with a former _Playboy_ cover girl for a wife and brilliant mind for business.

With the help of his family, Batista surrounded himself with Vale's best engineers and it seemed he had a golden touch for picking the right people and the right projects.

While he didn't have a track record to prove himself, he did hold plenty of promise, and in July of 2006 his appeal to money managers resulted in his mining company, MMX, raising $400 million and having the largest IPO Brazil had ever seen.

Batista's rise continued to be spectacular. Between 2007 and 2010 he went from not even being mentioned in the _Forbes_ list of the world's wealthiest people, to jumping from $6.6 billion to $27 billion. Finally, in 2012, with a net worth of $30 billion, Batista was ranked the eighth richest in the world.

But all the while, he was also getting rich by selling an image of success.

He was raising billions for new start-up companies while also winning the favor of politicians who were looking for a secure way to transfer massive amounts of public money.

Behind the scenes, however, things weren't perfect. Batista's oil fields were actually underperforming and being labeled as "prospective," a term given to oil reserves that may or may not be profitable.

### 6. Behind Batista’s success was an economic bubble waiting to burst. 

Batista was seen as the face of Brazil's future prosperity, an image he was happy to embrace. But in 2012, the same year he appeared on the _Forbes_ list, his net worth dropped by $14.5 billion in just three months.

The problem was this: both Batista's ventures and Brazil's economic prospects were built on a very unstable bubble.

One of Batista's mistakes was refusing to listen to anything he didn't want to hear. He labeled any employee who questioned the success of a project as a _calça curta_ — the literal translation is _short pants_ — but it was his way of calling someone a little boy, or small minded.

By 2013, the analysts at _Standard & Poor's_ were warning Batista that his petroleum company, OGX, could be broke by the end of the year.

But it wasn't just Batista; the entire Brazilian economy was on the decline.

With the poor forecasts, foreign investors were losing hope and holding back, turning those predictions into a self-fulfilling prophecy; the loss of trust also meant Brazil's government had to borrow money from foreign banks at much higher interest rates.

And things only got worse. For 2013, Batista's companies reported $10 billion in losses and his personal net worth dropped below zero. Then, in May of 2014, Batista resorted to selling his homes, hotels and expensive toys to get whatever cash he could.

Meanwhile, Batista's problems were being echoed by Brazil's own sociopolitical woes.

President Dilma Rousseff was impeached for using government-run banks to help finance the government. While these methods were nothing new, the country had grown so frustrated and resentful that Rousseff became the scapegoat and was officially removed from office on August 31, 2016.

Unfortunately, the outlook isn't improving, as the current political uncertainty is accompanied by economic turmoil.

More money continues to leave Brazil than comes in, and China isn't buying as much iron and soybeans as before; plus, the IMF is predicting the economy will shrink by eight percent between 2015 and 2017.

As we've seen, these troubles are so deeply rooted in Brazil; only time will tell if the nation can find a way around them.

### 7. Final summary 

The key message in this book:

**There's a fascinating story behind the economic success of Brazil, the epic rise of its billionaires and their subsequent downfall. Due to a history of corruption that has persevered for centuries, success does not reach everyone equally, and those who claim to have the country's best interests in mind often have ulterior motives.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Self-Made Billionaire Effect_** **by John Sviokla and Mitch Cohen**

_The Self-Made Billionaire Effect_ (2014) reveals the secrets behind the world's most successful companies and entrepreneurs. These blinks show that it isn't luck, age or external factors that got some of the world's wealthiest people where they are today. Find out how self-made billionaires became masters of duality by integrating imagination and design, and juggling opposing ideas.
---

### Alex Cuadros

Alex Cuadros is a writer whose work has appeared in the _Washington Post,_ the _New York Times, Bloomberg Businessweek,_ the _Nation_ and _Mother Jones_. He spent over five years living in São Paulo, Brazil, to research his first book, _Brazillionaires_.

