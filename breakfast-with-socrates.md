---
id: 50d72b5ee4b045383aa45e23
slug: breakfast-with-socrates-en
published_date: 2013-01-13T00:00:00.000+00:00
author: Robert Rowland Smith
title: Breakfast with Socrates
subtitle: The Philosophy of Everyday Life
main_color: CB2837
text_color: 941D28
---

# Breakfast with Socrates

_The Philosophy of Everyday Life_

**Robert Rowland Smith**

_Breakfast with Socrates_ (2009) whips you through a normal day with commentary from history's most venerated thinkers, explaining exactly how their major contributions to philosophy, psychology, sociology and theology impact your daily routine: wake up with Descartes, brace yourself for a world of Freudian conflict, and when you go to work, either submit to Marx's wage slavery or embrace Weber's work ethic. Argue with French feminists and then slip into a warm bath, bubbling in Buddha's heightened consciousness. Finally, end the day by drifting away into Jung's collective unconscious.

---
### 1. Philosophy arms you to make wise, practical decisions in a complex world. 

Perhaps you've always thought that philosophy isn't your cup of tea. Think again. Philosophy is not only about asking big questions like, "What is the meaning of life?" It's about asking the right questions, big and small.

Philosophy is as concerned with "How much should I tip the waiter?" as it is with "Does God exist?" It's about looking at the world around you and being able to make wise decisions throughout your everyday life.

In fact, the original Greek word "_philosophia_ " literally means "love of wisdom." Wisdom is the skill of assessing a complicated world and making sound judgments through reflection, i.e. asking the right questions in different situations.

Making _wise_ decisions in your everyday life is philosophy.

A wise example in the real world: it would be wise to consider where, how and when your spinach was grown. Was it grown in a country that has had a recent salmonella outbreak? Were loads of fertilizer and pesticides used? When was it picked? How much longer will it be healthy to eat? These are questions a wise person asks to make a practical decision in the supermarket.

However, wisdom should not be confused with cleverness. Cleverness is primarily concerned with triumphing over one's opponent in an argument. Sure, you may have won an argument with your partner, but would it have been wiser to walk away in the first place? Are you happier with winning or living peacefully?

Ultimately, you will be happier with philosophy in your life. Ask yourself the big questions sometimes, but ask the small questions always. Love wisdom. Practice reflection. Live philosophy.

**Philosophy arms you to make wise, practical decisions in a complex world.**

### 2. Descartes: If you’re awake, you’re thinking, and therefore you must exist. 

Even though we do it every day, waking up can be quite a shock — we can wake up with a wicked hangover, after too little rest, or next to the wrong lover.

But why does waking surprise us so? It is because in the moment before you awake, you are, by definition, asleep. You are not conscious of the fact that you are about to wake.

Philosophers have dwelled for hundreds of years on the subject of consciousness.

Clearly, consciousness is complicated. How can you be certain you are awake? How can you know you're not dreaming about being awake? How do you know that everything around you is not an illusion meant to trick you?

In the 1630s, French philosopher René Descartes set out to answer these very questions. He committed to doubt absolutely everything and took nothing for granted, starting with consciousness. Descartes arrived at the basic understanding that he could doubt everything except the fact that he doubted. And since doubting meant thinking, he knew he was thinking. And since thinking meant living, he knew he was living.

Descartes then famously concluded,"_Cogito, ergo sum_ "_–_ I think, therefore I am _._

Even if you think you are asleep or dead, you are still thinking and therefore you are alive. You exist.

This is Descartes' proof of existence.

This is why waking each morning is deeply philosophical. It is a daily embrace of consciousness and a re-affirmation of your own existence.

**Descartes: If you're awake, you're thinking, and therefore you must exist.**

### 3. Freud: The morning routine is a fierce psychological competition between the ego and superego. 

Certain of your own existence, you perform a series of morning rituals: shower, shit, shave, dress, eat and rush out the door. As always, however, preparing to leave for work is more complicated than we imagine.

At the beginning of the 20th century, Sigmund Freud, the famous Austrian neurologist, immersed himself in the newly evolving field of psychoanalysis, a way of studying the unconscious mind. According to Freud, our minds consist of different forces constantly struggling with each other:

The _ego_ represents the part of an individual's mind that aims mainly at living comfortably in routines without excessive stress or surprises.

The _superego_, on the other hand, demands we interpret what the rest of the world demands from us. The superego makes us feel obligated to shave, speak politely, wear appropriate clothing, and generally follow society's rules.

Getting ready to leave home in the morning is always a turning point in the ego/superego conflict. We prepare to abandon the calm, routine pleasures of the ego for the external regulations required by society and enforced by the superego.

The outside world will often require you to abandon your routines, present you with surprises and impose rules on your behavior and appearance. The subway may be jammed, your hat may be blown into a puddle while you hail a taxi, and when your boss chides you for being late you'll have to respond politely.

Consequently, every button you fasten and each shoelace you tie are in fact a surrendering of the ego to the superego. With each compromise, your superego takes you one step closer to the door.

**Freud: The morning routine is a fierce psychological competition between our _ego_ and _superego._**

### 4. Nietzsche: Abandon the herd’s fantasies, master your destiny and join the ranks of supermen. 

Every day, billions of human beings commute in herd-like unison to toil in offices and complete series of arbitrary bureaucratic procedures under oppressive fluorescent lights.

Perhaps you're sitting on the train reading this right now. Look to the right. Look to the left. Surrounded as you are by comrades of the herd, this is the perfect moment to consider the ideas of 19th-century hypercritical German philosopher Friedrich Nietzsche. He challenged us to ask ourselves, "What if I had to live this life over again? Would I be able to stand it?"

What parts of your life would be unbearable?

Nietzsche asserted the _doctrine of two worlds_ meaning that, in addition to reality, we invent a fantasy world to escape the drudgery of our real lives. While these fantasies are certainly consoling, Nietzsche said they are simply symptoms of weakness. We live vicariously through them, and thus do little to bring our dreams to reality.

The herd marches on in near-perfect, hallucinatory unison.

Nietzsche believed the only way to escape the herd was to abandon the fantasy altogether. Without it, you must master your destiny by living for each moment and seeing the world for what it is.

He called those able to do this "_supermen_."

Supermen do not conform or submit to the fantasy demanded by the herd but instead embrace their individual non-conformity in all its rugged reality. They aren't afraid to be different.

They abandon their fantasies and vow to live so that they would happily relive every moment.

It is only when you _know_ that you would happily relive each moment of your life that you are the master of your destiny. Master your destiny. Become a superman.

**Nietzsche: Abandon the herd's fantasies, master your destiny and join the ranks of supermen.**

### 5. Marx: Work is wage slavery. You have nothing to lose but your chains. 

You have disengaged from the commuting herd and arrived at the location of your employment. As you cross the threshold, what sort of agreement are you getting yourself into? What is work really about?

Classically defined within the capitalist system, work is the exchange of one's labor for money.

The nineteenth-century German philosopher and economist Karl Marx literally revolutionized the modern understanding of work through a key insight:

Fundamentally, Marx came to the conclusion that it's not an accident, nor the result of laziness, ability, or nature, that the average worker remains poor while the elite stay wealthy. In fact, the rich are effectively masters of the average worker, the wage slaves. The rich earn more than their labor is worth, actively prospering from the desperation of wage slaves, who are known as Marx's _proletariat_.

The proletariat consists of the _working class_ within the capitalist structure. _Capitalists_, owners of the majority of assets, are the dominant class. 

Marx inspired revolutions around the world for decades with the claim that wage slaves, having "nothing to lose but their chains," should revolt against capitalists.

What do you have to lose?

**Marx: Work is wage slavery. You have nothing to lose but your chains.**

### 6. Weber: If you work hard and live simply, you will become wealthy. 

Shortly after Marx, one of sociology's founders, German Max Weber, offered competing ideas about the nature of work.

While Marx believed that the wealthy are slave-masters who exploit the average worker, Weber believed that the rich benefit from the capitalist system because they were once poor but became wealthy through hard work and frugal living.

Weber called this phenomenon the "protestant work ethic." The idea made America and much of Western Europe very wealthy.

Far from being slave-masters who exploit the average worker, Weber contended that wealth is the result of twin virtues: hard work and self-denial.

He postulated that many people believe that hard work and frugality brings them closer to God, who rewards them accordingly with wealth.

You could certainly duck out early from work, head to the mall and blow your week's wages on designer clothing, lattes and a fancy dinner, but your poor work ethic and spendthrift ways will ultimately lead you to poverty and further away from God. 

So put in that overtime and have dinner at home instead of spending all your money after work.

**Weber: If you work hard and live simply, you will become wealthy.**

### 7. Learn from French feminists: Let go of your gender and live in peace with your partner. 

Even the most tranquil relationships include some conflict. It is inevitable given the emotional and social investments involved. This makes it very difficult for each person to remain objective, unemotional and rational. Tensions are easily inflamed.

People often blame their partner's faults on traits attributed to maleness and femaleness. Men are assumed to be insensitive and angry, women as emotional and overly dramatic. It can often seem as if the row is between mankind and womankind themselves.

In the 1970s, a group of female French feminists addressed this peculiar conflict in a unique way: their target was not men but the idea of gender altogether.

Known as _essentialists_, they argued that there is no inherent quality that separates women and men. While physiological characteristics set them apart, there is no _metaphysical_ or essential difference between the two. Any metaphysical difference would have to be more profound than mere differences in genitalia and hormones.

So when you're arguing with your partner, you may unwittingly assert that they are fulfilling gender stereotypes: "Craig, you're so unemotional. Can't you just FEEL something?" or "Veronica, can't you just think rationally and not get hysterical?"

A related trap is an unwitting statement that your partner is _not_ fulfilling his or her gender role appropriately: "Craig, why can't you be strong?" or "Dammit, Veronica, why can't you just be more understanding of my needs?"

These traps are real but avoidable if you are aware of respective gender stereotypes and avoid demonizing your partner with them.

You may even be able to forget gender entirely and treat each other as equal human beings, regardless of societal expectations.

**Learn from French feminists: Let go of your gender and live in peace with your partner.**

### 8. Buddhism: Meditation releases you from suffering. 

After a long day, you may enjoy meditating in the warm waters of your tub, conjuring up Eastern images of candles, oils, low lights, flowers and serene relaxation.

Contrary to this image, Buddhist meditation is not about maximizing relaxation; rather, it is about _heightened consciousness_. In the Buddhist tradition, meditation means allowing yourself to live completely in the moment, being intensely aware of everything in your vicinity.

You are conscious of each inhalation and exhalation. What's the difference in temperature you feel as you exhale? Inhale? How does it feel as the water slips between your toes? How does the cool, smooth tub feel on your back?

Meditation is the process of training your mind to stay focused in the moment and to not drift off to other things going on in your life.

Your thoughts are inevitably drawn to wants and desires: a promotion at work, a new lover, a vacation fantasy. Your desires, however, inevitably result in suffering. You become attached to things and people. Attachment eventually creates suffering and enslavement. You begin to need the things and people to which you are attached to be happy, instead of being happy with what you have.

Meditation relieves you from needless suffering by holding your focus in the moment, where it is impossible to desire anything other than what you have.

The heightened consciousness you experience during meditation releases you from the slavery of superficial desires, and thus from unnecessary suffering.

Lower yourself into the tub's warm embrace, close your eyes, and focus all your attention on this moment and no other.

**Buddhism: Meditation releases you from suffering.**

### 9. Jung: Dreaming connects you to the collective unconscious, the world’s soul. 

The end of your waking hours is near. As you drift into deep slumber, consider the final element of your day: sleep itself. Sleep has confounded humanity for thousands of years, and, considering that we sleep for about 20 years of our lives, it's certainly an activity worth thinking about.

There are many competing ideas about what our exciting nocturnal scenarios, or dreams, mean. Freud's colleague and psychoanalyst rival, Carl Jung, believed that not only do we have a _personal unconscious_ but a combined human dream state as well. This idea obviously requires deeper exploration.

Jung believed that humans have subliminally drawn together a reserve of memories from the dawn of mankind. This is the _collective unconscious_.

However, Jung thought this collective unconscious was more than just combined memories; it is the world's soul, and when you dream, the world literally dreams through you. Every human that ever lived has contributed to the collective unconscious, and we are all connected to each other through it.

When you fall asleep, you're no longer an independent actor but part of a massive telepathic network of billions of humans unconsciously experiencing and contributing millions of years' worth of memories to the world soul.

Good night.

**Jung: Dreaming connects you to the collective unconscious, the world's soul.**

### 10. Final summary 

The key message in this book:

**Philosophy arms you to make wise, practical decisions in a complex world.**

The questions this book answered:

**Why is philosophy important?**

  * Philosophy arms you to make wise, practical decisions in a complex world.

**How do ideas from the greatest thinkers impact my everyday life?**

  * Descartes: If you are awake, you are thinking, and therefore you must exist.

  * Freud: The morning routine is a fierce psychological competition between our _ego_ and _superego_

  * Nietzsche: Abandon the herd's fantasies, master your destiny and join the ranks of supermen.

  * Marx: Work is wage slavery. You have nothing to lose but your chains. 

  * Weber: If you work hard and live simply, you will become wealthy.

**How can philosophy relieve my suffering and reconnect me to the world?**

  * Learn from French feminists: Let go of your gender and live in peace with your partner.

  * Buddhism: Meditation releases you from suffering.

  * Jung: Dreaming connects you to the collective unconscious, the world's soul.
---

### Robert Rowland Smith

Robert Rowland Smith is a former Oxford Philosophy Fellow who currently writes for the _Independent_ and _London Evening Standard_. He has been featured in _The Sunday Telegraph_ and _Observer_ magazines, and on _BBC Radio_ and the delightful _Philosophy Bites_ podcast. Smith lives in London and regularly delivers lectures regarding the philosophy of everyday life.

