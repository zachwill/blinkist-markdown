---
id: 575a6c035b96b60003675a05
slug: breaking-the-page-en
published_date: 2016-06-17T00:00:00.000+00:00
author: Peter Meyers
title: Breaking The Page
subtitle: Transforming Books and the Reading Experience
main_color: E52424
text_color: B21C1C
---

# Breaking The Page

_Transforming Books and the Reading Experience_

**Peter Meyers**

_Breaking The Page_ (2014) explores the new possibilities ebooks offer to readers. Throughout these blinks, you'll learn about the differences between traditional books and ebooks, and why we need to rethink what a book _is_ in order to make the most of the powers of the digital wor(l)d.

---
### 1. What’s in it for me? Become an ebook enthusiast. 

If given the choice between two titles that cost the same amount, would you rather have a digital or printed book?

For most people, the answer would emphatically be the latter, as most people prefer hard copies over ebooks. Why?

Because ebooks are not being used to their full potential. Theoretically, we could add features to ebooks that would change the way we read forever. And yet today they are still inferior replicas of their printed counterparts. These blinks help explain a few of the ways in which we could use ebooks to revolutionize how we read.

These blinks also explain

  * what a reading dashboard would look like;

  * why it could be smart for ebooks to come with a framed print; and

  * why you shouldn't use silly cartoons in a religious text.

### 2. Ebooks can improve the reading experience in some genres. 

Do you read ebooks? If you do, you know how different they are to "normal" books.

At first, many thought that ebooks would be just another fad. But now it's safe to say they're here to stay. But while they won't dethrone classical books, they will begin to dominate in some areas.

One reason ebooks have become so popular is that the reader's attention nowadays is scarce: he wants the most relevant information and he wants it now. But traditional books aren't the easiest to skim through, and it's difficult to hone in on the information you want.

Things are different with ebooks. If designed in the right way, they make it much easier for the reader to search and access the content he wants. If, for example, different formatting is used to highlight the most important parts of the text, even hurried readers know what to look at immediately.

However, ebooks aren't well-suited to all forms of content. Fiction, whose content is mostly dependent on text, will largely stay in the traditional format. But non-fiction books will be able to take advantage of all the new opportunities that ebooks offer.

In traditional books, there is relatively little flexibility outside of arranging blocks of text. Although it's possible to add illustrations to certain pages, it's harder to do and the options are limited.

That's not the case with ebooks — in fact, there's the opposite problem. On the screen, large chunks of text are hard to read and follow, so you need to add more media — videos, photos, designs, and so on — to make the book more readable. Ebooks are thus an exciting medium for authors who play with layout or who have skills in areas other than writing.

They are also a work in progress. In the following blinks, we'll examine the state of ebooks today and see what opportunities they hold for the future.

> _"In the religious wars over which will prevail — print or digital — I have become somewhat of an agnostic. (Perhaps Universalist is a better way to put it.)"_

### 3. Ebooks shouldn’t be mere replicas of printed books. 

How do you feel when you open a novel you've been looking forward to for ages: turning the first crisp page, hearing the crack of the spine, taking in the first lines?

Now compare that feeling to opening an ebook.

A pretty big letdown in comparison, right? That's because many ebooks mistakenly try to replicate printed books.

For example, ebooks have covers, title pages, copyright information and a table of contents just like a printed book. And while the presentation of these elements works in a traditional book, it doesn't in an ebook.

Take ebook covers: digital libraries currently feature thumbnails that are nothing more than scanned-in and reduced images of the original covers, which makes them pretty tough to read. So rather than merely shrinking the images of book covers, ebook covers should be specially designed for ebook libraries, which would significantly improve browsability.

Then there's the copyright information, which can make a negative impression on the reader when too prominently placed.

Finally, a traditional table of contents is jarring for ebook users. Instead of just being a list of chapters, it should enable the user to navigate the content more easily and pick out what they need.

So what can be done to make ebooks more exciting?

We need to think outside the book.

One of the main problems with ebooks is that users can't look through their bookshelves to see what they have and haven't read already.

So, why not bridge the digital and physical world by making every ebook come with a print or a magnet? That way you could put the print on a shelf or the magnet on your fridge to keep track of what you've read.

The crux of the matter is: we should be thinking about books in new ways. Unfortunately, companies like Amazon, which are more concerned with profits than the reading experience, are currently setting ebook standards.

What we need instead is for publishers and authors to take a central role in making ebooks better suited to reading.

But what can they do?

> _"Throughout the long history of media, it's far more common for new technologies to coexist with, rather than cannibalize, old ones."_

### 4. Ebooks can drastically improve the reading experience and incorporate other kinds of media. 

Ebooks can reach out to the new generation of readers who feel limited by the narrow scope of traditional books. And with the flexibility of ebooks, we can design a reading experience optimized for all kinds of readers.

Specifically, ebooks can be designed to facilitate the three different types of reading:

_Skimming_, when readers get just enough information to make snap judgments.

_Grokking,_ when the reader is able to understand the key ideas and concepts of a book.

_Mastering,_ when readers delve as deeply into the book as possible.

Traditional books only facilitate the latter, but ebooks can help all three.

One solution might be to turn the chapters of a book into a virtual stack of cards. The first thing the reader would see is a very brief outline of each chapter, which would make it easy for her to skim through the cards. If she sees something that interests her, she could stop on a card and flip it over. This would give her the key insights of the chapter, allowing her to grok. Finally, if she wants to learn even more, she could click through to the whole text and master the content.

There's something else ebooks can do that traditional books can't: moving images. But of course, they have to be incorporated properly.

For example, you need to strike the right balance between the video and the text, making sure that neither dominates. Let's say you're explaining a dry topic like the theory of relativity. By adding a video, the topic could become easier to understand and thus more interesting to the reader. But if you hide the video after multiple lines of technical text, the reader won't read far enough to get any benefit.

Also, you need to make sure the tone matches the subject matter. If your topic is sensitive — say, if you're talking about a war or religious issues — don't go for silly cartoons.

> _"Video activates our emotional interest; text then takes over and tells the story in its methodical fashion."_

### 5. Ebooks need new kinds of libraries that allow customers to find what they want quickly. 

Have you looked at your ebook library recently? Was it a useful experience?

The truth is that many people find their ebook library pretty useless: it's hard to navigate and uninspiring.

A _reading dashboard_ could change all that.

The reading dashboard doesn't organize titles in lists but as clusters. Books could be sorted by clusters such as "Fitness" or "History," which would allow readers to find what they are looking for rapidly.

But using topics isn't the only way to organize our libraries. We could also cluster around _book modules._

Book modules help solve the problem of too much content and not enough time. We simply don't have a chance to read through everything to find the piece of information we need. Ebooks can remedy this by separating books into easy-to-find modules. Say you want some tips on how to have a successful interview. Rather than having to read a whole book on communication, you could easily find and read the specific module you need.

Along with creating better ways of sorting through ebooks, we should also work at providing the customer with better _upfront information_.

At the moment when you're browsing to buy a new ebook, you can read a little bit about the book: the title and subtitle, the blurb, a few reviews. But this information is often just an attempt to market and sell the book, and customers have learned to take such sales pitches with a pinch of salt.

We need to change this so people can choose the book they need as quickly as possible. To do so, authors need to write more candidly about what people can expect to learn from their book, instead of publishers listing the three reasons why their book will change your life.

If customers find their ebooks easier to maneuver, they have more time to do what they want to do: enjoy reading their books.

### 6. Instead of promoting sharing with others, ebooks should help people share with themselves. 

Almost every ebook has an option to share an element on social media. However, doing so is usually disappointing because the content can't be shared very effectively.

For example, when you use a Kindle, you can share content from an ebook on Twitter or Facebook. Theoretically, this allows users to become active readers. But while one person might like the content, another person might not find it meaningful — or even just think of it as a distraction.

Another problem is that algorithms are often behind the presentation of what you share. As a result, your actual shared message might end up being a picture or comment you didn't choose. So, like an algorithm, the share won't have any personality.

Instead of focusing on sharing with others, we should design ebooks to facilitate _sharing with ourselves_, meaning making it easy to contemplate what we read.

One great way of doing that would be to simplify taking notes and linking different passages together. Just imagine reading about, say, different exercise programs, and being able to mark and connect ideas as you would with Post-Its in a traditional book. That way it'd be easier for you to retain the content you read, and you could even link it with information you read in similar books.

That's not to say sharing with others is bad. But often it's no more than a distraction that doesn't benefit the recipient much. Instead, ebooks should concentrate on improving the connection between the reader and the content.

If ebooks take advantage of all the new possibilities the medium has to offer, they can transform the way people read around the world.

> _"Suddenly I have shifted from a receptive, contemplative mental state to a performative one. I am no longer a reader; I am now a writer."_

### 7. Final summary 

The key message in this book:

**Ebooks won't render the book as we know it obsolete. Instead, they'll offer a wide range of new opportunities to allow traditional books to coexist with them. However, to fulfill their potential, ebooks need to adjust their design and have more to offer than just their low price.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Read a Book_** **by Mortimer J. Adler and Charles van Doren**

Since _How to Read a Book_ was first published in 1940, the blank sheet of paper that faces you when you start an essay or report has been replaced by the blinking cursor in a blank Word document. No matter: this classic bestseller, revised in 1972, is still a great guide to tackling a long reading list, extracting all the relevant information and organizing your own conclusions. Be the boss of books with this effective approach to reading and understanding texts of all kinds.
---

### Peter Meyers

Peter Meyers has written for the _New York Times_, the _Wall Street Journal_, _Wired_ and the _Village Voice_. He founded the company Digital Learning Interactive and is a pioneer in multimedia textbook publishing.

