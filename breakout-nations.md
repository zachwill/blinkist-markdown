---
id: 528229ac33346400081c0000
slug: breakout-nations-en
published_date: 2013-10-23T09:14:54.000+00:00
author: Ruchir Sharma
title: Breakout Nations
subtitle: In Pursuit of the Next Economic Miracles
main_color: None
text_color: None
---

# Breakout Nations

_In Pursuit of the Next Economic Miracles_

**Ruchir Sharma**

Although many emerging markets across the globe grew at historically high rates over the past decade, for many nations these growth rates are unsustainable and are the result of a unique set of conditions in the global economy.

_Breakout Nations_ (2012) examines the factors likely to determine which emerging markets can take advantage of changing economic conditions and become breakout nations.

---
### 1. To identify the next breakout nations, look at the current, local conditions in emerging markets. 

International investors are always trying to identify emerging countries that will deliver high growth on their investments. But finding countries that do this consistently is difficult. Consider, for example, that since the 1950s, only two emerging economies — Taiwan and South Korea — have managed to maintain an annual growth rate above 5% for five consecutive decades.

This is why investors are attracted to economies with the potential to beat growth expectations, the so-called _breakout nations_. A breakout nation is an economy that grows faster than its economic rivals so that it competes with nations richer than itself. Economies are measured using _per capita income_ and this figure is used to place nations into different income brackets. For example, a nation in the $5,000–$10,000 bracket that is able to compete with economies in the $10,000­–$15,000 bracket is a breakout nation: it punches above its weight.

So how can you identify such breakout nations?

First, forget long-term forecasts. Economic circumstances change radically every decade or so, so any forecast beyond that is automatically obsolete.

Second, don't follow the crowd. When everyone else is already investing in a country, opportunities there will already be overpriced due to increased demand.

The key to discovering the next breakout nation is to look for developing or emerging economies that are healthy and adaptable enough to take advantage of future economic circumstances.

Take a closer look at each emerging market to identify the local strengths and weaknesses. For example: What are the political conditions like? Is the leadership making the right economic reforms? Is the economy balanced and robust, or too reliant in one particular area? Is the domestic market strong enough to maintain high growth rates?

**To identify the next breakout nations, look at the current, local conditions in emerging markets.**

### 2. China’s high rates of growth are unsustainable as natural economic barriers slow its economy. 

Over the last decade, China's economy has grown at an unprecedented pace, enjoying growth rates in double figures. The speed of this growth combined with China's massive population has turned the nation into an economic powerhouse, which consumes huge amounts of commodities and exports all manner of manufactured goods.

Some have bet on China continuing to grow at its current pace. Yet, in reality, China is reaching natural economic barriers that make high growth rates harder to achieve.

These barriers are not unique to China; the richer a country becomes the harder it is to grow at a fast pace. A larger economy requires more absolute growth every year to maintain its current percentage growth rate. To continue double-digit growth, China would need to consume a third of the global trade of commodities, such as minerals and oil. This is unsustainable.

China is also faced with internal barriers to growth, such as the problem of structural inflation caused by a wealthier workforce. China's growth was possible because of a pool of rural, young, low-skilled and cheap labor. As more of these laborers are moving into the cities to work, the pool is drying up. As the supply for cheap labor declines, its price increases, making China's products more expensive.

Another barrier to China's fast growth is its government's concern in maintaining social order. Rising inflation is pushing up the cost of living for the average citizen. Inequality is also increasing as the rich benefit from higher property prices. This situation could lead to social unrest, so the government will be forced to deliberately slow economic growth.

**China's high rates of growth are unsustainable as natural economic barriers slow its economy.**

### 3. Many emerging economies benefitted from rising commodity prices over the last decade, but this bubble will soon burst. 

Almost all _emerging economies_ have grown over the last decade, with behemoths like China and India as the most prominent examples. So where did this growth come from? It is largely the result of a price 'bubble' in the commodity market.

In order to engineer growth, Western economies lowered interest rates, encouraged borrowing and spent heavily. Much of the capital released found its way into the hands of investors and speculators who poured it into the commodity market expecting, rightly, that commodity prices would go up as China increased its demand for raw materials to fuel growth. Prices rose further still as speculators hoarded commodities like gold to take advantage of future price increases.

The effects of the bubble were highly polarized: For the main commodity exporters, such as Brazil and South Africa, the high prices brought in wealth, allowing them to grow at a fantastic pace. However, the same bubble has been damaging to countries reliant of manufacturing industries, as the cost of the raw materials needed to make their products have skyrocketed.

Similarly, whilst the poor are hurt by rising food and energy prices due to the bubble, wealthy speculators have pocketed huge profits. Such rising inequality has led to much unrest.

This situation will soon end. China's demand for commodities will decrease as its economy slows, and the West is cutting its spending to bring down its high debt levels. For innovative manufacturing economies, the bursting bubble will be helpful due to the falling cost of raw materials. For the commodity exporters, the situation will be bleaker as the price of exports plummets.

**Many emerging economies benefitted from rising commodity prices over the last decade, but this bubble will soon burst.**

### 4. Economic prosperity depends not on a nation’s political system but on the stability and dynamism of its leaders. 

Economists have often argued whether a democratic or an authoritarian system is better for securing long-term economic growth. Some point to examples such as South Korea, which had an authoritarian government for much of its history but has still been the 'gold medalist' for growth, securing high growth rates for five consecutive decades. Others highlight Japan, which grew tremendously with a democratic system after World War II.

The truth is that the political system is largely irrelevant. Governments of either form are just as likely to achieve high growth rates or to produce economic failure. Of the best performing economies over each of the past three decades, 52% were democratic whilst 48% were autocratic.

What is vital for achieving high rates of growth is a political leadership that understands economic rules and has the stability to put reforms in place.

A pro-active leadership with the right economic knowledge can transform a stagnant economy in a relatively short space of time. Emerging economies such as Indonesia and China were transformed by leaders who understood what was required to build growth. Reforms such as sweeping away corrupt practices, bringing debt under control and encouraging competition lead to a healthier economy — one more attractive to investors.

Stability is also important: An unstable leader may feel unable to implement divisive but important reforms for fear of being toppled or voted out. They may also feel the need to promote policies popular with sections of the population to maintain their support, even when such decisions aren't economically sensible.

**Economic prosperity depends not on a nation's political system but on the stability and dynamism of its leaders.**

### 5. An abundance of natural resources in an emerging market can lead to an unbalanced and uncompetitive economy. 

Most emerging economies are currently riding on a wave of high growth rates. Many of these, like Brazil, Russia and South Africa, are rich in natural resources and have profited from high prices in the commodity market. Yet, despite this success, such nations can suffer from a _resource curse_. A nation can become too dependent on its natural resources to an extent where the other areas of the economy suffer.

One of the ways in which a resource curse can damage an economy is currency appreciation. The influx of money from the export of natural resources makes the currency — and therefore other exports — more expensive. This is the problem in South Africa: when the price of gold increases, their currency's value also climbs, making the South African manufacturing industry less competitive globally.

Another detrimental effect of an overreliance on natural resources is that it makes an economy hostage to their export value. When the export value is high, the incentive to invest in other areas of the economy is low, leading to even more reliance on commodities. But when their value falls, the nation's economy collapses as there are few alternative areas of economic strength to lean on.

Corruption and cronyism can also be a problem. When so much wealth is derived from one area of the economy, a tiny but incredibly wealthy oligarchy can develop. In Russia, politicians hungry for commodity profits collude with oligarchs to prevent free competition in commodities. This leads to a powerful, unmovable elite class, skimming off the cream of the nation's commodity profits, damaging the overall competitiveness of the economy.

**An abundance of natural resources in an emerging market can lead to an unbalanced and uncompetitive economy.**

### 6. The consistent success of South Korea is an example of how an emerging nation can secure long-term growth. 

Only two emerging markets have achieved a growth rate of over 5% for five consecutive decades: Taiwan and South Korea. Whilst Taiwan may struggle over the coming years, South Korea shows every sign of maintaining this level of success. The nation is a perfect example of how an emerging market can succeed in the long run.

One reason for the extraordinary success of South Korea is that its firms are extremely competitive and innovative, investing heavily in research and development. This allows them to compete in markets across the globe, just as South Korean multinationals like Samsung do. Other emerging markets, most notably Mexico and South Africa, have their own multinational firms, but their reach is mostly regional, not global.

Another key element for long-term growth is having the right attitude: proactively chasing success, as South Korean firms do. Not satisfied to merely maintain their current position, they are constantly adapting and transforming themselves in search for new markets. This exploration of new markets has produced great results. For example, South Korea has been the only manufacturing nation to have succeeded in expanding its market share in China over the last decade.

The attitude of the South Korean population has also helped the country grow. Although the country is typically conservative and conformist, South Koreans are prepared to embrace change when it's needed. One example of this occurred in 1998 when the nation was hit by the Asian financial crisis. The population quickly mobilized itself to make the necessary changes to get the economy back on track. Some even donated jewelry to help the cause.

**The consistent success of South Korea is an example of how an emerging nation can secure long-term growth.**

### 7. The next explosion of economic growth is likely to come from “frontier economies.” 

One group of nations that did not follow the same patterns of economic growth as other emerging economies over the last decade were the _frontier nations_. Nations in this category include Nigeria, the Gulf States and Sri Lanka. This 'forth world' differs from other emerging markets because frontier economies don't follow the established orthodox economic rules. This leads to wildly varying growth rates that depend heavily on the differing local conditions in each country. Investors are therefore required to conduct in-depth local research to get a true understanding of the economy.

Frontier markets are not defined by lack of wealth: included in this group is Qatar, the world's richest nation with a per capita income of $100,000. What does characterize these economies is their disregard for the expected rules of running an economy.

This unpredictability spooks investors. If an emerging market ignores the expected rules, it might be relegated to the frontier list, as happened to Argentina in 2009 after it put controls on the movement of capital, damaging investor confidence.

In frontier nations, even a slight economic or political change in the right direction can dramatically transform an economy and push the growth rate skyward.

Consider Sri Lanka, which after years of civil war is now enjoying a renaissance. After the end of conflict, the government has invested heavily, generating growth in the former war zone. The country is also geographically well-placed to take advantage of China's and India's growth. Over the coming years, frontier nations with much growth potential like Sri Lanka could become breakout nations.

**The next explosion of economic growth is likely to come from "frontier economies."**

### 8. As the economic conditions change, some nations are in a promising position to become breakout nations. 

The unique economic conditions that have seen almost all emerging markets grow at once are coming to an end. In the future we will see a more uneven situation, with many emerging nations suffering a slowdown. Others will, however, beat expectations and rivals, and grow to become breakout nations.

So who stands the best chance of becoming a breakout nation in the future?

The frontier markets of Sri Lanka and Nigeria both have political leaders who could provide stability and the necessary reforms to unleash high growth rates.

Turkey's relatively rich manufacturing economy has for a long time looked to Europe for inspiration, and could now breakout if it exploits its position as the largest Muslim economy in the region to trade with other Muslim countries.

The stable Eastern European nations of Poland and the Czech Republic also stand a good chance, especially considering their rather low levels of debt compared to other nations on the continent.

Finally, the highly competitive manufacturing economy of South Korea, although relatively rich, still has the potential and economic vibrancy to continue to beat its growth expectations.

These predictions are far from certain as economic conditions can change quickly, but at the moment these economies do look promising.

**As the economic conditions change, some nations are in a promising position to become breakout nations.**

### 9. As the global wave of economic growth slows, confidence in Western economies, especially in the United States, will return. 

George Orwell once remarked: "Whoever is winning at the moment will always seem to be invincible." At the present time, China and the emerging economies of the East and the South seem to be winning. While low growth rates and high debt levels plague the West, emerging economies are enjoying some of the fastest growth in history. Because of this, there is a fear in the West that the global balance of power is shifting from the United States to China.

But these fears are largely misplaced. China's economy remains only a third the size of the United States' and the unique set of economic circumstances that generated the high growth rates in emerging markets is ending. The future will see many emerging markets struggle to adapt to a changing global economy, whilst the economies of the West, especially in the United States, will re-emerge from their current plight. 

The current high commodity prices hurt Western economies that are major importers whilst benefitting many emerging markets, the major exporters. The high prices will soon fall, benefitting the West, whose raw material costs will become lower.

The major advantage that many western economies have over their emerging rivals is their competence and capacity for research and development. The United States, for example, has a highly competitive economic system where companies are forced to invest heavily in developing new products to stay ahead of the competition. This makes them more adaptable in discovering and exploiting new markets, which is particularly important in the rapidly growing and changing software and information technology industries.

**As the global wave of economic growth slows, confidence in Western economies, especially in the United States, will return.**

### 10. Final summary 

The key message in this book is:

**The period of growth which characterized the last decade is over. The emerging economies that have grown the most, such as Brazil and China, will begin to slow down. In the new, more volatile economic conditions, a new wave of breakout nations will emerge. The nations in the West, which have seen their relative prosperity decline, will reassert themselves on the global economy.**

The questions this book answered:

**How do you identify the next breakout nations?**

  * To identify the next breakout nations, look at the current, local conditions in emerging markets.

**What changes will affect the global economy over the coming years?**

  * China's high rates of growth are unsustainable as natural economic barriers slow its economy.

  * Many emerging economies benefitted from rising commodity prices over the last decade, but this bubble will soon burst.

**In an emerging market, which local conditions are most suited to achieving economic growth?**

  * Economic prosperity depends not on a nation's political system but on the stability and dynamism of its leaders.

  * An abundance of natural resources in an emerging market can lead to an unbalanced and uncompetitive economy.

  * The consistent success of South Korea is an example of how an emerging nation can secure long-term growth.

**Which nations are likely to achieve economic success in the coming years?**

  * The next explosion of economic growth is likely to come from "frontier economies."

  * As the economic conditions change, some nations are in a promising position to become breakout nations.

  * As the global wave of economic growth slows, confidence in Western economies, especially in the United States, will return.
---

### Ruchir Sharma

Ruchir Sharma is head of the Emerging Markets Equity team at Morgan Stanley Investment Management. He physically spends one week per month in emerging markets to gain key insight and a hands-on understanding of local conditions.

He has written articles for Newsweek, The Financial Times and The New York Times.

