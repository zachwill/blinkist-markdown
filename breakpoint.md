---
id: 53aa8ca23933310007360000
slug: breakpoint-en
published_date: 2014-06-24T00:00:00.000+00:00
author: Jeff Stibel
title: Breakpoint
subtitle: Why the Web will Implode, Search will be Obsolete, and Everything Else you Need to Know about Technology is in Your Brain
main_color: 48A6E8
text_color: 30709C
---

# Breakpoint

_Why the Web will Implode, Search will be Obsolete, and Everything Else you Need to Know about Technology is in Your Brain_

**Jeff Stibel**

This book deals with the structure, function and development of networks. Drawing on specific aspects of biological, technical and virtual networks, such as the brain and the internet, the author suggests that these networks, however different they may appear, actually have a lot in common. He believes that if we learn how the organic network of the brain works, we can apply those findings to the internet and make it intelligent.

---
### 1. What’s in it for me? Learn how the internet could become like a brain. 

Why do networks often perform so much better than the individuals within them? Can a network get too big for its own good?

The answer to these questions and more lie in this book. The author also explains how a network's size measures up to its quality and how, oddly enough, the human brain actually gets smarter as it loses capacity.

The book also explores what happens when we apply this knowledge about organic networks to technical networks, such as the internet. Although it has grown tremendously over the last two decades, the internet's growth may finally be hitting a decline. If so, we'll enter a period where, like our brains, the internet shrinks but gets smarter — maybe even smart enough to act like a brain itself, capable of intelligence and consciousness.

In these blinks, you'll find out

  * why a relatively stupid ant can build a complex nest,

  * why Google is like a brain and

  * why a termite beats a dinosaur.

### 2. Networks make the lives of creatures easier and more efficient. 

Do you ever feel like you want to abandon your life and civilization and move to a secluded desert island? Tempted as we are by such thoughts at times, isolation isn't all it's cracked up to be. 

After all, species that live in _networks_, i.e., organized communities characterized by the division of labor and support systems for one another, are much more successful than those that don't.

A whopping 99.9 percent of all the species that have ever lived are now extinct, but the ones that have survived continuously and been the most populous tend to be networking or "social" species, such as bees, ants and humans. And although only a small minority — 3 to 5 percent ­– of all species are social, they play a dominant role across all habitats on land.

Why are social species so much more successful than those that aren't? Because the collective intelligence of a network is far greater than an individual's intelligence.

In other words, combining our varying skills and abilities clearly works better than going alone.

Let's use an analogy: the individual ingredients of a cake — eggs, flour, baking powder, etc. — taste anywhere from average to disgusting on their own. But if you mix them the right way, you create a delicious cake.

Or consider the ant. Although not the most intelligent of Earth's creatures — its brain has a mere 250,000 brain cells versus a frog's 16 million — it can achieve a lot in a network, building complex nests and creating rudimentary agriculture and public health systems.

Feeling less into the idea of leaving your life behind and going into isolation yet?

### 3. Every network goes through three stages of development. 

Every network, whether an ant colony or human brain — which is a network of neurons — goes through three stages of development.

The first is _growth_. Once a network is founded, its growth will go slowly at first and then suddenly spike upwards. The human brain, for example, develops at an astounding speed in the womb: fetuses create 250,000 brain cells a minute. This explosion happens as the network attempts to consume all the resources that surround it: because if it doesn't use the resources, something else will.

After a period of growth, a network will enter its second stage of development: the _breakpoint_. Growth will continue until a point when it's no longer beneficial for the network — this limit is the breakpoint.

But a network's growth doesn't just gradually come to a halt: the only way to identify the breakpoint is to exceed it. Ever wonder how we know what temperature bread needs to be baked at? The simple answer is trial and error: somebody somewhere burned loaf after loaf of bread before figuring out the limit.

Once a system realizes that it's gone beyond its limit, it goes through a pruning process. To use the ant example again, once an ant colony becomes too congested, fertile ants will be sent out of the colony to reproduce elsewhere.

The third and final stage of a network is _equilibrium._

When successful networks reach their breakpoint, they undergo a small decline in size — and this decline continues until they reach their ideal size, i.e., their equilibrium. This size will vary depending on the environment: a sea slug's brain only needs 18,000 neurons to function while a cat's needs 1 trillion.

> _"Social networks after the breakpoint are highly successful both in the short term (the life of the network) and the long term (the survival of the species)."_

### 4. The quality of a network is much more important for success than its size. 

Size isn't everything. Just think about the dinosaurs: despite their massiveness, they succumbed to extinction while much smaller species, such as insects, spiders and rodents, survived. And the same holds true of networks.

A tiny network is pretty useless — just imagine a colony of termites with only two or three members. There has to be a certain amount of growth in a network to get it to an optimal size . Yet a network that's too large can also be a disadvantage.

Once a network has reached equilibrium, additional growth is not helpful and can even be dangerous. For example, our brains have grown to a size where they consume 20 percent of all our energy. If it were to grow larger and our hearts and lungs were to stay the size they are now, our brains would soon run out of nutrients and oxygen, and we'd die.

Once a network has reached equilibrium, it will concentrate more on quality than quantity.

When we're five years old, our brains have 1,000 trillion neural connections — too many, as a matter of fact. As we age, our brains get rid of many of these connections until we're left with 100 trillion as an adult. By eliminating the useless connections, the brain strengthens the ones we use most often and it becomes more efficient.

So after it reaches a point where growth is no longer necessary, the brain becomes more efficient rather than just looking for ways to grow. This development in quality is what makes networks, such as the brain and the ant colony, truly intelligent.

In conclusion, growth and expansion are very important for efficient networks — but beyond the breakpoint, quality is more crucial for success.

> _"When the brain stops growing and reaches a point of equilibrium in terms of quality, it starts to grow in terms of quality."_

### 5. A network’s success hinges on communication and self-organization. 

What are the secrets of a successful network?

First off, self-organization and decentralized leadership are vital for a successful network. The best networks, whether an ant colony or the internet, don't require centralized leadership.

There might be a queen in a colony of ants, for example, but her role is to lay eggs, not to micromanage what everybody does. The colony is self-organized in that every ant knows what they need to do with little instruction from a central power.

Wikipedia is a great example of an incredibly successful network with no overall leader or controller.

Based on crowd-sourced material, the site is growing at such a rapid pace that it now contains 22 million articles in 285 languages, much more than all the well-known printed encyclopedias (e.g., Britannica, Cambridge, Americana) combined.

Second, communication is at the heart of every successful network, as the members of any given network need to be able to pass on information to each other.

Ant colonies, for example, communicate via pheromones. The task that each of them decide to carry out is based on information they receive from other ants. Similarly, human beings use language to communicate information about the division of labor and other vital interactions and things.

As a matter of fact, language is so important to us that Sigmund Freud once declared: "The first human who hurled an insult instead of a stone was the founder of civilization."

That's because, ultimately, the self-organization of a network is largely possible thanks to an effective means of communication among its members.

### 6. The internet works the same way as a biological network. 

We've already seen how an organic network develops and what it needs to be successful. Now let's see to what extent the internet lives up to its definition as a network.

Well, first there was a phase of rapid growth: in 1993, there were no websites; in 2002, there were 20 million; and in 2012, there were 600 million. This is an unbelievable amount of growth in just under two decades. How unbelievable? If we were to apply this growth rate to that of a newborn baby, the baby would be so tall at the age of ten that it would be able to touch the moon!

The internet has already reached its breakpoint. This is due in no small part to the fact that there is too much content and too many websites on the internet for it to be user-friendly. 

Indeed, the amount of people using their PCs to surf the net is shrinking. In 2012, there were 4 percent fewer people browsing the web on their computers than the previous year. The amount of time people are spending on the net is also decreasing: in 2011, it was 72 minutes per day and in 2012 it was only 70. People have instead moved on to using the simpler, quicker and more user-friendly apps.

Another limit to the web is energy consumption. Estimates show that the internet will eventually consume 20 percent of the world's power — a drain so huge that it could either collapse the grid or force it to grow so much that it accelerates climate change.

The web needs to slow down to reach equilibrium. If it manages to hit this point, it'll start improving its quality and become more meaningful for society.

### 7. The development of the internet is bringing it ever closer to our brain. 

Just what can the network of the web help us achieve?

In order to answer that question, let's turn to the rather strong similarities between the human brain and the internet.

Just look at a search engine like Google. When you enter a search term, the engine chooses the result based on the importance of a website, which is "directly proportional to how many other websites link to it … and not just the number of the links but also the quality of those links."

This process resembles the way our brain searches. Our most important neurons are linked to the most other neurons. When we want to recall information, the most linked neurons become active first, just as the most linked websites appear at the top of Google's results list.

In the future, the link between our brain and the internet will probably get stronger. For instance, at the moment, we're the ones who ask the internet what we want via a search. But one day, the internet may be able to predict what we want before we need to ask.

This could be made possible by directly connecting the brain to computers. Scientists have already connected a computer to the brain of a paralyzed woman. When she wants to move a robotic arm or switch something on, she can tell the computer to do it with her mind.

To help computers go a step further, we need to give them real "intelligence." If we were to do this, we could make the knowledge of the internet a link to our own brain.

With time we're getting closer and closer to achieving this. A computer called Spaun as already been endowed with a similar neural network to our own. If we ever get there, the internet will become — like the brain and the ant colony — a truly intelligent network.

> _"The internet is an ant colony is a brain."_

### 8. Final summary 

The key message in this book:

**Networks** **are** **some** **of** **the** **most** **powerful** **entities** **on** **earth.** **To** **get** **to** **their** **optimum** **level** **they** **must** **go** **through** **three** **stages;** **growth,** **breakpoint** **and** **equilibrium.** **At** **the** **moment,** **the** **internet** **is** **reaching** **breakpoint.** **When it** **hits** **it and proceeds to reach equilibrium,** **it** **will** **become** **one** **of** **the** **most** **phenomenal** **networks** **on** **Earth.**

Actionable advice:

**Don't** **kill** **that** **ant.**

If you look at the sheer complexity and intelligence of an ant colony, you'll start to see the small creatures in a new light.
---

### Jeff Stibel

Jeff Stibel is a brain scientist and entrepreneur. He is CEO of the Dun & Bradstreet Credibility Corp and currently serves on the boards of the Brown University Entrepreneurship Program and the University of Southern California's Innovation Institute.

