---
id: 546a08c03034360009500000
slug: brick-by-brick-en
published_date: 2014-11-18T00:00:00.000+00:00
author: David Robertson with Bill Breen
title: Brick by Brick
subtitle: How LEGO Rewrote the Rules of Innovation and Conquered the Global Toy Industry
main_color: 5696CE
text_color: 41719C
---

# Brick by Brick

_How LEGO Rewrote the Rules of Innovation and Conquered the Global Toy Industry_

**David Robertson with Bill Breen**

_Brick by Brick_ looks at the failures and ultimate success of one of the world's most beloved brands: LEGO. By examining the management strategies that brought LEGO back from near-disaster, _Brick by Brick_ outlines how innovation can be managed responsibly.

---
### 1. What’s in it for me? Manage innovation intelligently by following LEGO’s example. 

In today's rapidly changing, hypercompetitive economy, innovation is a crucial ingredient in the recipe for long-term success. Companies that are unable to innovate demonstrate a rigidness that causes them to snap under the pressure from market changes, and long-term success often means showing a willingness to be creative.

However, as you'll soon find out, innovation can be both a blessing and a curse: the radical, unchecked innovation that we often hold to be so inspiring sometimes comes with a lack of focus that can cause an entire business to unravel before our very eyes.

Such was the case with the famed Danish toy company LEGO. But as you'll find out in these blinks, the lessons learned by the LEGO company in their turbulent quest to balance innovation and their bread-and-butter would prove crucial to their long-term success. Not only that, but these lessons can be applied to any business model that aims to produce controlled, measurable innovation.

In these blinks, you'll find out

  * what led to LEGO's near-bankruptcy in 2003;

  * why it's sometimes better to think _inside_ the box; and

  * why LEGO hires T-shaped people.

### 2. LEGO’s initial success resulted from shifting focus from creating individual products to developing a system of play. 

Everyone has heard of LEGO. You probably even grew up playing with those distinctive, multi-colored bricks in your home! But have you ever thought of LEGO as the multinational corporation behind the bricks — an economic force to be reckoned with on the world stage?

Founded in 1932 by a former carpenter named Ole Kirk Christiansen in the small town of Billund, Denmark, LEGO (which roughly translates from Danish to "play well") originally specialized in building wooden toys for kids.

Kirk followed a principle of constant innovation, so it's no surprise that the LEGO company was the first toy company in Denmark to experiment with plastic toys.

LEGO's big turning point would come when Kirk's son, Godtfred Kirk, followed the advice of one retailer, who suggested he build an entire _system_ of play rather than building individual products. As a result of that discussion, Godtfred decided to scrap the vast majority — 90 percent — of LEGO's portfolio to focus on what would become one of the most iconic toys of our time: the interlocking brick.

This new unified toy-production system meant that a brick from one set was compatible with bricks from any other set. This meant, for example, that kids could combine LEGO model buildings from one kit with LEGO model cars, streetlights, traffic signs and train tracks from other kits, allowing kids to create all kinds of interesting, novel contraptions.

Decades before the rise of Apple's suite of connected i-branded products, LEGO had taken a holistic approach to its product portfolio, linking everything together via the ubiquitous brick.

Not only was LEGO's new system fun; it also allowed LEGO to increase its share in the market and opened up a whole new world of product development.

### 3. By the late 1990s, the LEGO company was a booming business facing a crisis. 

Like every company, LEGO has seen its fair share of ups and downs. The period between the late 1970s and mid-1990s are now considered the "golden age" for LEGO's sales.

The invention of LEGO's modular brick was akin to striking gold. In 1991 the company saw an 18-percent jump in sales, despite the meager four-percent growth in overall toy sales that year. By the mid-1990s LEGO had grown into a group of 45 companies on six continents with nine thousand employees.

But LEGO's booming sales lured executives into complacency, causing the company to miss out on opportunities for innovation that would help them adjust to a changing toy ecosystem:

In the 1990s, LEGO's analog style of play was becoming increasingly out of touch with kids, who were turning to VCRs, video games, cable TV, and computers for their trusted sources of entertainment. Further complicating LEGO's market share, the group's patent for its interlocking bricks expired in 1988, thus allowing cheap imitators to flood the market.

All of these factors combined to put a stop to LEGO's remarkable period of double-digit growth, which finally plateaued in 1993. By this point, the group had reached the end of its natural growth cycle, and could no longer sustain growth without changing its portfolio.

It was clear that the company would have to start branching out into unfamiliar territory if it wanted to drive further growth.

The crisis LEGO faced in the 1990s demonstrated how even the most successful and beloved companies can become thwarted by their own complacency; small improvements to existing products aren't always enough to drive sales. As you'll soon find out, LEGO has some valuable lessons to offer us regarding what _not_ to do in this situation.

### 4. LEGO’s attempts at innovation in the 1990s severely backfired, leaving the company on the brink of bankruptcy. 

In the mid- to late-1990s, LEGO was in trouble. So, in 1998 they finally brought in an external consultant to help develop new innovation initiatives and turn the company around.

Exploring new directions to take your company is fine and good, but as LEGO quickly learned, haphazardly investing in various innovative approaches can weaken the brand overall.

Take _LEGO Explore_, for example, a new product line meant to appeal to toddlers. Designed by a satellite office in Italy that had little to do with LEGO's Danish headquarters, LEGO Explore did away with many distinctively LEGO features — and in the process alienated a lot of fans.

Moreover, LEGO's attempt to diversify was too broad. They tried to expand into too many branches simultaneously, without actually developing the necessary expertise to make these ventures successful.

Just look at the company's LEGOLAND venture, for example, which grew from a single location in Billund to four over the course of a few years.

While the speed of their expansion is impressive, LEGO lacked both the management know-how and the deep pockets necessary to ensure their success in this area. As a result, the new parks quickly went into the red. Each location cost 1.5 billion Danish Krones to build and sustained operating losses of 300 million Danish Krones in their first year alone.

Ultimately, the innovation strategy adopted by LEGO in the late 1990s caused more harm than good, and almost drove the company into bankruptcy.

Although the company was technically profitable from 1993 to 2002, the owners were depleting their family fortunes at the rate of almost $0.5 million _per day_, _every day_, over the same period.

A change in strategy was sorely needed. In the following blinks, you'll learn about LEGO's amazing turnaround and the principles that allowed it to restructure its company, direct innovation and become the success story that it is today.

> _"The result [of LEGO's innovations efforts] was a cascade of runaway innovations that either bombed or imploded."_

### 5. Innovations ought to grow organically out of the culture and history of the company. 

So what did LEGO do to turn things around? It became clear that one of the major problems with LEGO's initial push for innovation was that it had lost focus, thereby alienating core customers and straying from the company's area of expertise.

Thus, the very first corrective measure that management took was to scale back its expansion into adjacent industries.

In its rush to diversify, LEGO had adopted an "everything-goes" company culture that encouraged developers to pursue any and all market opportunities that were remotely connected to the brand.

As a result, it was producing toys that lacked LEGO's original "do-it-yourself" charm, while also lacking the sophisticated marketing and technological expertise of electronic or action-oriented toys. The result was a series of half-baked products that performed disastrously.

One example of this was _LEGO's Galidor_, an action figure that was released along with a tie-in television show in 2002. However, LEGO executives didn't have the faintest idea how to run a television show. Unsurprisingly, it flopped, and the action figure, too, soon faded into obscurity.

Next, the management instituted controls that would balance innovation and growth with the overall image of the brand. One such control was the establishment of LEGO's "Design Lab," comprised of company veterans who would ensure that new projects had enough "LEGO DNA" to strengthen the brand rather than weaken it.

As a result, the company was able to focus on new areas of innovation while still respecting the system of play that makes LEGO unique.

_LEGO Games_, for example, signaled the company's entry into the board game industry but with a distinctly "LEGO" twist: players must first construct the board out of bricks before they can play.

By refocusing efforts on building upon the original selling-points of the brand, LEGO managed to carve out a distinctive niche in the toy market with room to grow.

### 6. Tighter focus allows for more productive innovation. 

LEGO's initial innovation efforts weren't only problematic because they strayed from the brand — they also faced dismal returns on investment.

In order to focus development, LEGO management began tasking development teams with producing ideas that were not only innovative, but also profitable.

For instance, in the early days of their post-2003 restructuring, the management set up a short-term, concrete goal of 13.5 percent return on sales. No matter how passionate designers were about a new toy, if they couldn't convincingly forecast a 13.5 percent return on investment, then the project would be dismissed outright.

In addition to the strict profit expectations, designers also had to work within tighter budgets. If project developers couldn't come up with ideas that stayed under LEGO's specified manufacturing cost, then they would face consequences at their annual review.

Luckily, their new strategy made a huge impact on the company's direction: by focusing only on projects that were bound to reap significant profits, LEGO managed to increase its return on research and development.

What's more, something surprising happened on the developers' side, too: they gradually discovered that resource restrictions, the kind that meant they had to think "inside the box," actually spurred _more_ creativity!

Apple designers had a similar realization when Steve Jobs' insisted that the iPhone should have only a single control button.

Faced with a budget shortfall, LEGO's solution wasn't to abandon innovation altogether. Instead, they simply shortened the leash, so to speak, on their developers, making sure that their innovative ideas met the requirements necessary to keep LEGO profitable.

> _"You had to stay within your [full manufacturing cost] model...otherwise, you were dead." — Henrik Weis Aalbaek_

### 7. Innovation should take place within a structured process that allows for development at different levels. 

Explosive diversification wasn't the only mistake LEGO made in their frantic attempt to turn things around. In their push for innovation, they also lost the balance between nurturing their core product and developing a diversified portfolio.

As you now know, LEGO moved too quickly, and in the process gave preference to costly top-line ventures with high start-up costs while neglecting the very product that captures LEGO's essence: the brick.

Research shows that, so long as a company has a strong core business, it can successfully move into one adjacency every five years. Contrast this with LEGO's approach, in which it moved into _five_ adjacencies in a single year, all while neglecting its core business.

In order to better coordinate its resources and innovation efforts, LEGO developed a three-pronged structure that gives people enough space to create as well as enough direction to deliver:

The primary component of the business is _Product and Market Development_ (PMD), responsible for the brick. PMD develops the minor product innovations to their primary line, such as expansion kits and thematic categories, that serve as the foundation for the company's financial stability.

Next, the _Concept Lab_ is responsible for speculative development. This is where all of the "never-before-seen" concepts that move the LEGO brand into new directions are cooked up. It was in this division that the LEGO Games lived, for example.

Finally, the company set up _Community, Education and Direct_ (CED) as a way to communicate directly with LEGO's fan and market base.

Whereas PMD and Concept Lab are concerned with the core and paradigm-breaking parts of the business respectively, CED develops the cross-platform marketing opportunities and community engagement that keep their fan base interested and excited.

By adopting this tripartite structure, LEGO ensures that innovation happens simultaneously across all levels of the business.

Moreover, the structure serves as a sort of safety net: if one component struggles during a tough quarter, the entire company won't fall like a house of cards.

### 8. When fostering open innovation, develop a set of direction-setting tools, then get out of the way. 

Crowdsourcing is among the most powerful strategies available to corporations in our Internet Age. One of LEGO's many successes is the way it managed to create a framework with which to harness the innovative potential of its fanbase while avoiding the often unruly consequences of crowdsourcing.

When engaging in open innovation, LEGO sets a fixed direction, but stays flexible in the execution. For instance, when developing its customizable robot line _Mindstorms NXT_, the company turned to its hard-core adult fans, many with backgrounds in robotics.

However, even though they were working with feedback from adults, the project managers never lost sight that the robots were ultimately intended for _kids_, and as a result made sure not to create anything overly complicated.

Crucially, outsiders and insiders share equal responsibility for the ultimate outcome, even though they often have divergent expectations.

For example, despite being given significant freedom for ideas, the volunteer developers involved in the Mindstorms NXT were nonetheless expected to adhere to the same quality, safety and cost standards that govern the process of innovation for employed designers.

By setting clear boundaries and creating the expectation of shared responsibility, LEGO managed to benefit from outside innovators while at the same time limiting any liability that they could pose. Not only that, but LEGO's insistence on quality and cost standards signaled a certain professional respect towards its innovators, thus fostering a sense of co-ownership.

### 9. Collect a diversified team of “T-Shaped People,” counterbalanced by a focused group of managers. 

By this point, we've focused mostly on how LEGO altered its product and innovation strategies to ensure success. But how has LEGO's restructuring affected its recruitment and management strategies?

LEGO now recruits what some call _T-Shaped People_ : individuals who combine an expertise in a particular area (the vertical leg of the T) with a broad knowledge extending across various other disciplines (the crossbar of the T).

LEGO has greatly benefited from hiring these T-shaped employees, as their cross-discipline backgrounds means they tend to think differently and challenge each other.

Take, for example, the chief designer of LEGO Games, Cephas Howard: toy design is his vertical, but because of his history in media, he also has relevant expertise in marketing and sales, making him a more well-rounded innovator.

Yet, while creativity brings innovation, it's possible to have too much of a good thing. Just think back on the runaway development that LEGO underwent in the late 1990s!

So, in order to counterbalance these highly creative "T-shaped people," LEGO has also hired strong managers whose primary focus is to maintain the discipline of the team and shape all that creative energy into something material.

LEGO brings it all together through its product development teams, which put project managers, designers, marketers, engineers, modelers and communications people in the same room.

Each member is expected to contribute at all stages of the project, rather than solely for the parts for which she is primarily responsible. This approach prevents the work process from devolving into a hierarchical assembly line and encourages constant communication.

By bringing people of differing talents, backgrounds and responsibilities together in a cohesive group with a clear direction, LEGO manages to foster creativity and inspiration without losing sight of its goals.

By now, you've seen exactly what it is that almost brought LEGO to its knees as well as what steps this iconic company took to turn things around. So, what lessons can we learn from the LEGO restructuring process?

### 10. Every innovation makes a difference, even the small ones. 

When people think of innovation, they often think of revolutionary ideas that completely change the way the game is played forever.

The go-to example for this is the case of Steve Jobs at Apple, whose radical ideas such as the iPad and iPhone literally changed the way we go about our daily lives.

Closely tied to this idea of radical innovation is the belief that innovation comes from spontaneous, speculative ideas. That's why companies like Google institute policies that give developers the freedom to devote a whopping 20 percent of their time at work to projects of their own choosing.

LEGO, however, understands that game-changing ideas aren't the only kinds of innovations. Even small day-to-day improvements made to an already-solid product can have a great effect on profitability.

While break-the-mold ideas like Mindstorm NXT or LEGO Games have generated large profits for the company, the group's most profitable lines are the decidedly unsexy basics that have mythologized LEGO: the figurines, the houses, the bricks.

The company rightly places as much emphasis on funding research and development in these bread-and-butter products as it does on the speculative projects that aim to change the game or crack open new markets.

And as LEGO demonstrates, innovation can become a regular part of the work flow, and can even be systematized with the right approach.

Managers, for example, are asked to fill in an "innovation matrix" for each project, which outlines the degree of creativity that developers are expected to bring to the table, depending on the overall project goals.

Or just look back on the company's three-part system to manage innovations at all levels. In doing so, LEGO demonstrates that it values all forms of innovations equally — not just the kinds that put you on magazine covers.

### 11. Sometimes the best strategy for growth is not thinking “outside the box.” 

When you're stuck in a rut, it seems like the one thing everyone will tell you is to "think outside the box." But as LEGO has shown us, sometimes it's better to stay _inside_ the box, and use these limitations to your advantage.

When LEGO thought "outside the box" in the late 1990s, the results were disastrous. LEGO's misadventure actually demonstrates the innate danger of "out-of-the-box" thinking: when all of the focus is on creativity, companies can easily lose sight of what brought them their initial success in the first place.

In contrast, LEGO's policies after its restructuring clearly show us the virtues of thinking "inside the box" — or brick, as the case may be. The strict cost and profit standards that the management imposes on its developers, for example, are more than unnecessary restrictions from a money-hungry company. They actually serve to _spur_ creativity, as designers work to produce more from less.

By setting restrictions, creating a system and narrowing the directions in which ideas have the freedom to develop, companies can actually channel creativity in a way that generates the most productive results.

This is certainly the case in LEGO's management of personnel, both employees and volunteers: by allowing creative leeway within clearly-defined limits based on quality, safety and cost, the company is able to generate controlled bursts of innovation that do not risk capsizing the company.

And by setting realistic goals, the company is able to determine just how much innovation is actually needed in order for the project to profitably succeed.

So, the next time someone tells you to think "outside the box," take a moment to think of LEGO, and reconsider whether it might be better to climb back into it.

### 12. Final summary 

The key message in this book:

**LEGO's history shows us that, sometimes, innovation isn't just about thinking "outside the box." In fact, LEGO's brief brush with near-bankruptcy and quick turnaround show how incremental, planned and disciplined innovation can create profits just as effectively as radical, paradigm-shifting ideas.**

**Suggested further reading:** ** _Good to Great_** **by Jim Collins**

_Good to Great_ presents the findings of a five-year study by the author and his research team. The team identified public companies that had achieved enduring success after years of mediocre performance and isolated the factors which differentiated those companies from their lackluster competitors.

These factors have been distilled into key concepts regarding leadership, culture and strategic managemen **t.**
---

### David Robertson with Bill Breen

David C. Robertson is a former _McKinsey & Company_ consultant who currently teaches at the Wharton School of the University of Pennsylvania.

Bill Breen is a founding member and senior editor of _Fast Company_ in addition to the co-author of _The Responsibility Revolution_ and _The Future of Management._

