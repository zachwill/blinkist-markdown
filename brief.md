---
id: 55c7c6436664360007420000
slug: brief-en
published_date: 2015-08-11T00:00:00.000+00:00
author: Joseph McCormack
title: Brief
subtitle: Make a Bigger Impact by Saying Less
main_color: 5C8095
text_color: 3C5361
---

# Brief

_Make a Bigger Impact by Saying Less_

**Joseph McCormack**

_Brief_ (2014) is a guide to having more impact and influence by saying less. These blinks show the reader the power of brevity in the information age, explain the barriers to being brief and give helpful instructions on how to improve your communication while saving your own time and everybody else's.

---
### 1. What’s in it for me? Find out why you talk too much. 

Why is it so hard for us to be brief and to the point when we talk? You can hardly open up an email or go into a meeting without the guarantee of a long-winded, unfocused and forgettable bunch of information.

Actually, there are a lot of reasons why we struggle to be succinct in our communication. These blinks go through them all, and then explain how you can counter each one so that you're known as the concise, business-like person in the office. Once that happens, people will always want to know what you have to say.

In these blinks, you'll learn

  * how many distractions an average worker faces per day;

  * how Steve Jobs mastered succinct storytelling; and

  * why brevity is also crucial in small talk.

### 2. Be heard in today’s distracting world by making your point quickly. 

Every day we're swamped by information, distracted by innumerable people and things vying for our attention. In this fast-paced, information-centric world, time has become a valuable commodity: those who can't grab attention and make their point quickly are easily ignored.

Why's it so hard to get people's attention?

Because we simply don't have the mental capacity to take in all the information we encounter. We're confronted with so much data that it's impossible to make sense of it all.

For example, according to the software developer Atlassian, the average professional receives 304 emails per week. Not just that, the venture capital firm Kleiner, Perkins, Caufield and Byers' annual Internet Trends report found that people check their phones 150 times _per day_. That means the average worker gets interrupted every eight minutes by their phone alone!

Because of these modern diversions we struggle to take in the rest of the information with which we're presented. It's no surprise that research has shown the average attention span has dropped from 12 seconds to eight over the past five years.

As a result, everyone expects things faster. To get _your_ ideas across in this information overload you'll need to hold your audience's focus and that means making your point quickly.

How?

By speaking in attention-grabbing headlines. For instance, instead of slowly building information in a presentation, announce your findings right away.

Headlines are effective because business people can become impatient when speakers aren't straightforward. For example, picture 300 executives at a non-profit fundraiser on a Wednesday night. The keynote speaker goes 30 minutes over and by the time he's done the room is half empty. In this way, the inability to communicate quickly could cost you not just people and money but also respect and your reputation.

But if successful people want everything to be concise and have no tolerance for long-winded explanations, then why's it so difficult for us to get to the point?

### 3. Tackle the unconscious obstacles to brevity. 

It's all too common for someone to promise, "this will only take a minute" only to speak for half an hour. Why's it so hard for us to stick to our word on this?

It's not just a love of hearing our own voices that makes it hard to be brief. The difficulty stems from subconscious reasons like our confidence and comfort level. For instance, if someone considers themselves an expert on a topic they can get stuck on technicalities and over-detailed explanations. And what use is expertise if you can't offer a clear and comprehensible explanation?

A similar thing happens when people feel comfortable: often we say too much in familiar contexts or company. For example, we've all had that experience when a brief chat with a colleague during a coffee break turns into a marathon when our co-worker details his weekend plans. Situations like this eat up your work time and make you think twice about showing interest.

So feeling at ease makes us talk too much. Here's how confusion and complication do the same.

Sometimes when people haven't organized their thoughts before speaking they end up thinking out loud. In this situation, even if your brain is working a mile a minute, the people around you will only receive blurred, indecipherable messages. This commonly occurs during brainstorming sessions. Your great ideas shouldn't go undeveloped because others can't follow your point.

But what about ideas that are intrinsically hard to follow?

For instance, some people believe there are concepts so complex they can't be simplified. Say a customer wants to know why his delivery can't be expedited. You explain all the logistical details of the shipping schedule and when you've finished the customer wants nothing to do with you. By explaining too much you risk losing not just your customer's patience, but also his trust.

Therefore, brevity is key. So teach yourself to be brief with the following four techniques.

### 4. Simplify ideas by using BRIEF mind maps to outline information. 

We all know how difficult it is to be on the receiving end of a conversation with those who overexplain, underprepare and unnecessarily complicate information. Want to ensure you never put another person in that uncomfortable position again? Come prepared with an outline and you'll be amazed at how clear, organized and detailed your presentations can be.

Outlining presentations is easy using _BRIEF_ _mind maps_ — visual outlines that organize information around a core topic.

Here's how they work:

BRIEF stands for _Background_, _Reasons_ or _Relevance_, _Information_ for inclusion, _Ending_ and expecting _Follow-up_ questions. BRIEF mind maps are diagrams that help you express your ideas with brevity. The key to drawing one is taking your time.

Begin with a _brief box_ with a strong headline describing your central point. For instance, if you're updating a group on a project your headline could be: The project is on time.

Then detail the last conversation you had with the person or group you're addressing. So in this example, you might say, "Last week we decided the anticipated costs were $30,000 and the timeline for completion was 30 days."

Once everyone is updated, tell them why you're meeting now and why the information is relevant. Maybe plans changed and you need to invest further resources to stay on schedule.

Now present the key information of your presentation. For instance, "By investing an additional $5,000 we can complete the project four days early."

When concluding, summarize what you've presented and outline the next steps on the timeline: if we increase funding we can finish the project ahead of schedule.

Before your presentation, consider any potential audience questions and try to answer them. For example, can we expect any additional expenses? Are there any unforeseen risks?

The next time you're presenting information keep Nike's example in mind and "Just do it." Just say it, quickly, clearly, and get it done.

> "It takes time to be ready to say less."

### 5. Make your ideas pop with the power of pictures. 

You know the classic saying, a picture is worth a thousand words? When it comes to brevity it's truer than ever. Clear and memorable visuals appeal to everyone and are a great tool to help people remember a message. Today's technology-centric world of screens and interactive media has usurped the text-based world of yesterday.

In fact, studies have shown that 65 percent of the population are visual learners and that people tend to recall about 80 percent of what they see. To put it in perspective, people only remember about 30 percent of what we read and just ten percent of what we hear!

Here's how you can use pictures to communicate your ideas: infographics, videos, graphs, charts, illustrations and animations can communicate your message six times more effectively than using words alone.

Take an example from _USA Today_ founder Al Neuharth, who redefined journalism by seizing on the new way people read newspapers. By using shorter stories and plenty of visuals he remade his medium for a fast-paced world in which people don't have the time to read lengthy articles.

### 6. Exchange corporate-speak for an engaging story. 

Now that you know a few strategies to organize information and present it briefly you're probably ready to try them out. But before getting on stage you'll need one more critical technique: storytelling. Because a well-structured narrative provides a direct line to your audience, which fosters instant clarity with a personal connection.

Storytelling is easy with a narrative map that keeps your story concise.

Just like mind maps, narrative maps are structured around a central point — this time, a story. From the central idea you elaborate a _setup_ or _challenge_, an _opportunity_, an _approach_ and a _payoff_. Just as with the BRIEF map, begin by writing down the central point then fill in each respective aspect, going clockwise from twelve o'clock.

One of the greatest examples of narrative mapping is Steve Job's presentation on the first generation iPhone. Job's well-organized and highly strategic story explained that the current smartphones were neither smart nor user-friendly, and conveyed the extraordinary impact the iPhone would have on its industry.

In Job's narrative map the _challenge_ was that smartphones were not intuitively smart enough. The _opportunity_ arising from this challenge was to build a phone that was both smarter and easier to use than any other. The _approach_ to building it was to design a user-friendly device that made calls while enabling users to surf the web and listen to music. Finally, the _payoff_ was the way Apple solved the problem: by creating a game-changing product, the iPhone.

By introducing his breakthrough product with a compelling narrative, Jobs launched iPhones onto the market.

But it's easy to make mistakes while telling a story. Turning business stories into fairytales is one of them. To avoid this error when mapping your narrative, stay away from fables, myths, satire and any other esoteric forms. Instead, do what Jobs did and stick to a straightforward story that details why, how, who, when, where and therefore _what_.

### 7. Employ active listening to turn monologues into controlled and balanced conversations. 

Sometimes being brief can feel like a conversation killer but actually the opposite is true. To convey your ideas with brevity is to have a meaningful, controlled conversation.

In order for _you_ to do this you'll need to be disciplined and learn to talk about what's truly important to your conversation partner. You can learn what matters to others by employing active listening and asking thoughtful, intentional questions.

The _TALC_ method will show you how:

TALC, an acronym standing for _Talk_, _Active Listening_ and _Converse_, is a technique for keeping conversations on track by putting your ideas in line with those of your partner to guide the conversation in interesting directions.

How does it work?

Begin by letting the other person speak and express themselves. But be prepared to respond with a clear point when they're done. As the other person talks, be certain to listen attentively and show your interest. You can do this by avoiding multitasking and by listening for key words, names, dates and other facts that will help you choose an interesting way to respond. Actively listening in this way is key to understanding your partner's mind-set and values.

As you employ this technique, stick to building up a single conversation. Instead of raising a new topic, try offering a comment or question that serves as a bridge from the topic your partner raises. Keep your responses brief and be mindful of when to stop speaking.

Conversing with brevity is all about understanding what your conversation partner cares about and appropriately responding. So being a good conversation partner means actively listening in order to genuinely respond, not just waiting for your chance to speak. Treat conversations like a game of tennis by reacting to the move your partner makes, instead of like a game of golf where you wait for your turn.

### 8. Brevity signifies respect – show people you care by thinking about their time. 

Being brief isn't just a tool for the office, it's essential to proper etiquette and good manners. In short, it's about respect. If you stay considerate of the precious time of others by being brief in meetings, presentations and even on social media it will make a big difference that people are sure to appreciate.

For example, meetings can be a huge time sink. Just consider the fact that the average CEO spends about 60 percent of his working hours in meetings! So keep meetings quick by setting time limits and assigning moderators to enforce them.

How?

Google's technique is to project a timer on the wall of their conference room that counts down the time left until the meeting ends.

When it comes to presentations, try opening by asking "Why?" This defines the issue at hand from the start and helps tackle the most pressing questions first. So don't save the best for last. Grab your audience's attention from the beginning!

And don't forget social media. Remember, the most effective posts are around 80 characters, and visuals are five times more engaging than text alone.

But that's not all: being brief is also essential to effective small talk.

It's easy to go on autopilot in casual contexts and say whatever pops into your head. But it's just as easy to lose others' respect or damage your reputation by sharing too much.

Briefly telling someone good news will highlight the success and pique their interest. For instance, simply telling your boss that your project is under budget is more impressive than explaining the details of every expense.

And by sharing _bad_ news quickly you minimize the pain of hearing it. Being succinct means divulging only the most meaningful facts and avoiding negative comments. Remember, if you feel badly about something or frustrated with someone it's better to say nothing at all.

Whether at the office or after work, be brief and you'll be polite.

### 9. Final summary 

The key message in this book:

**The modern world is full of distractions; if you want to communicate effectively it's necessary to be brief. Understanding the subconscious factors that make us drag out explanations, practicing strategies for planning presentations and learning the art of storytelling will transform your communication skills by giving them direction and meaning.**

Actionable advice:

**Prepare for your boss's ubiquitous "How's it going?" each day.**

Many of us hear this question from our boss every day and being able to answer it briefly and effectively can mean a lot. Remember, your boss isn't interested in the details of your work but in the progress you've made and the accomplishments to prove it. If you can't think of anything concise, responding with "nothing new" is better than detailing your last three weeks of work.

**Suggested further reading:** ** _Talk Lean_** **by Alan H. Palmer**

_Talk Lean_ will teach you how to express your thoughts in a direct, candid, yet courteous manner. The author shares effective, easy-to-apply tips for having a productive conversation that helps achieve your goals.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Joseph McCormack

Joseph McCormack is an expert on corporate storytelling and transmitting information through narrative. He has provided communications consultation to a wide range of professionals from military leaders to senior executives of companies like SAP, MasterCard and Harley-Davidson.

Joseph McCormack: Brief Copyright 2014, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

