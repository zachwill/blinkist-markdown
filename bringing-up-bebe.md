---
id: 57e6c3f0afd7bf0003b7052d
slug: bringing-up-bebe-en
published_date: 2016-09-28T00:00:00.000+00:00
author: Pamela Druckerman
title: Bringing Up Bébé
subtitle: One American Mother Discovers the Wisdom of French Parenting
main_color: 287CBF
text_color: 1D5B8C
---

# Bringing Up Bébé

_One American Mother Discovers the Wisdom of French Parenting_

**Pamela Druckerman**

It might sound too good to be true, but in France, babies and children sleep through the night, eat their vegetables and do what their parents tell them. In _Bringing Up Bébé_ (2011), Pamela Druckerman, an American mother living in Paris, reveals the French parenting secrets she uncovered in her time abroad.

---
### 1. What’s in it for me? Improve your parenting skills the French way. 

If you were to ask a French person about the French style of parenting, they would would probably say it's nothing special. After all, parenting is just parenting, right?

Still, as anyone who's ever been to France will have noticed, French children are just exceptionally well behaved and independent, even from a very young age. No whining about things they can't have, no dinner table tantrums and no confusion as to who's really the boss.

So how do the French do it?

These blinks outline the lessons learned first hand from an American mother who had a child in France. As you'll see, by adopting a few simple rules, you can not only make your child behave much better, but also carve out more time for yourself and your needs.

In these blinks, you'll learn

  * why there are no kids' menus in France;

  * what marshmallows have to do with raising children; and

  * why you should embrace gender differences.

### 2. Babies sleep through the night if they can soothe themselves back to sleep. 

Sleepless nights are inevitable for new parents, right? Not necessarily! French children tend to sleep through the night, in some case starting when they're just a few weeks old.

What's the secret of parents who raise these quiet babies? It's in how they respond to them.

Babies make a lot of noise at night, but their cries don't always mean they need something. In fact, babies naturally wake up every few hours because they sleep in short cycles. Sometimes they wriggle or cry just because they're trying to get _back_ to sleep.

Also, it's actually rare for a baby to need food during the night; digestion interferes with their sleep, just like it does in adults.

In 1993, the journal _Pediatrics_ published a study by Teresa Pinella and Leann Birch instructing new mothers on how to soothe their babies. They came up with three rules:

  * don't rock the baby to sleep;

  * only feed them during the night if you've already tried swaddling them, patting them, walking them around and changing their diapers; and

  * learn the difference between whimpering and real crying.

Pinella and Birch also found that among parents following these instructions, 38 percent of their babies could sleep through the night after just four weeks, compared to just seven percent of those who didn't receive the instructions.

You also need to wait before responding to your baby. Don't intervene right away when they cry. Give them a chance to fall back asleep on their own — babies often aren't fully awake when they start crying, so you might make it worse if you pick them up.

This doesn't mean you should ignore a crying baby, but you should make the pause longer over time. Pause for just a few seconds with a newborn and increase the time interval until you're eventually waiting a few minutes.

Babies need time to learn how to get back to sleep by themselves, and it's your responsibility to help them develop that autonomy.

### 3. Develop your children’s taste for healthier food. 

Children can be picky eaters, especially in the United States. American children often end up eating only a very limited amount of food. But not so in France!

Children can be trained to eat virtually anything. But when the author visited a range of seafood, Italian and Cuban restaurants in the United States, she found that most offered them the same items: hamburgers, chicken fingers and pizza. 

French restaurants, on the other hand, rarely have a "kids' menu." The French want their children to experience a variety of different cuisines and develop their palates, so they don't have them eat from a separate menu.

In France, a typical kindergarten lunch might be red cabbage and fish in a dill sauce, followed by a brie-like cheese and a baked apple. Children are even served delicacies like foie gras, sardine mousse and goat's cheese.

It's important to expose children to a wide range of textures and flavors, which is why French children are constantly being given foods prepared in different ways. Leeks, for example, can be steamed, pureed, creamed or baked.

French children aren't expected to like everything — but they _are_ expected to _try_ everything. According to the French, if you try something enough times, you'll end up liking it. The author spoke to one French mother who makes her daughter take at least one bite of everything on her plate.

This is part of why the French often give their children three-course meals. And three-course meals don't just expose them to more tastes; they also educate them about nutrition.

The first course might be a vegetable soup or salad, for instance. The child wouldn't be allowed to move onto the main course until she has eaten some or all of her appetizer.

Kids can be trained to eat anything, even vegetables. Just remember the motto: "You have to try at least one bite."

### 4. Strict adherence to mealtimes makes children healthier and more self-disciplined. 

How many times have you eaten a sandwich at your desk during work, or grabbed a snack on the way home? We tend to enforce mealtimes on kids, without always following them ourselves. But the French know that mealtimes are about more than just staying slim.

Fixed mealtimes help reduce calorie intake, so they reduce the risk of obesity too. Nearly all French children eat four times a day: at 8 a.m., midday, 4 p.m. and finally at 8 p.m.

Snacking between meals is usually forbidden. So if a child wants a chocolate bar, they have to eat it during one of the designated times: probably at 4, which is thought of as the "snack time" in France.

Only 3.1 percent of French five-year-olds are obese, compared to 10.4 percent of American five-year-olds. That difference only increases as they get older.

Stricter mealtimes also mean fewer tantrums. Tantrums tend to happen in the street or the supermarket, when a child wants a treat. French children, on the other hand, learn from an early age that it's futile to try because they only get to eat during set times.

Children also become more patient and resilient when they learn to delay their gratification. Walter Mischel, a famous behavioral psychologist, did an experiment on this phenomenon in the 1960s.

Mischel had the children sit by themselves in a room with a marshmallow on the table in front of them. He told them that if they waited for 15 minutes without eating it, they would get to eat two marshmallows.

Mischel then interviewed the group several years later after they had become adults. The children who'd been able to resist the temptation of the marshmallow had done much better in life.

Strict mealtimes aren't just about health; they're also about self-control.

> _"The French seem collectively to have achieved the miracle of getting babies and toddlers not just to wait, but to do so happily."_

### 5. You don’t have to abandon your own needs when you have children. 

When American couples have children, they tend to view their child's birth as the end of their previous life and the start of a new one. American parents are under a lot of societal pressure to put their family's needs before their own — whether in terms of passions, money or sex.

In France, it's considered unhealthy to sacrifice your sex life for your children. In fact, the French believe that the loss of sex drive is a psychological disorder. It's viewed as a form of depression and is treated as such.

Hélène de Leersnyder, a pediatrician, says that French parents don't view sex as an occasional privilege, but rather a basic human need.

So don't be afraid to send your children away when you need some space from them; in France, it's not considered shameful to want a few days to yourself. It's common for schools to take children away on week retreats, even in primary school.

The author spoke to one French couple who takes a ten-day, child-free holiday every year, while the children stay with their grandparents.

Birthday parties are also a chance for a break in France. You let other parents care for your child for the day, and maybe share a glass of champagne with them when you come to pick them up. The author describes it as free babysitting, followed by a cocktail party.

It's okay to admit that children and adults inhabit different worlds. Don't feel guilty when you want some time for yourself. Learn how to take pride in letting yourself detach, give both you and your children some well-needed space, and just relax.

### 6. French parents embrace gender differences. 

These days, people are doing a lot of work to break down the gender divide. French women do too, but they have a different take on the issue. They believe that we need to embrace certain gender disparities, and celebrate our differences.

So French women don't expect men to match them in their child caring skills. According to Debra Ollivier, the author of _What French Women Know_, French women are content to view men as different kinds of people who simply aren't as good at buying diapers or scheduling appointments with the pediatrician. French men aren't under pressure to be perfect like American men are.

French women even like to poke fun at their partners' shortcomings, which makes French men more relaxed and less demoralized unlike American men who are constantly pressured to be the world's best dads.

Moreover, the author believes that keeping score in household chores fosters bitterness.

American women usually expect men do to an equal share of housework and feel resentful when they don't. According _The Bitch in the House_, the best-selling American anthology, most women secretly keep tabs on what their husbands have or haven't done around the house. This just contributes to feelings of anger and pent-up aggression.

The author spoke to several French women about the issue of men doing less work around the house, and none of them felt any resentment about it. They admitted to feeling overwhelmed sometimes, but the important part is the fact that their stress doesn't mutate into hostility toward their husbands, creating a negative atmosphere at home.

The American ideal of equal parenting might sound nice in theory — but according to the author, the French view just makes life easier for everyone.

### 7. Don't smother your children, but learn how to really say “no.” 

The term _helicopter parenting_ has gained a lot of currency in the United States in recent years. It refers to modern parents' tendency to hover around all aspects of their child's life, inhibiting their ability to think for themselves and develop their autonomy.

This isn't how it works in France.

Learn how to chat with your friends while your children do their own thing. In France, it's unusual to see parents hovering around their children in a park, guiding them down slides or pushing them on the swing. Parents sit and talk with each other while the children play by themselves.

That doesn't mean kids can do whatever they want, of course. Parents still have a few rules, like "no hitting" or "don't leave the playground."

And when a child breaks an important rule, French parents let them know. French parents know what it takes to _really_ say "no:" you have to have full confidence in your own authority, and you have to believe that your child will follow your orders. If you believe that, they will too.

Even more free-thinking parents take pride in being strict, because the idea of "strictness" only applies to very serious or dangerous behavior, not something small like taking an extra cookie.

So learn to tell the difference between small problems and big ones. When your child breaks a serious rule, make sure they listen when you tell them off.

With this approach, you'll end up having to discipline them less and less, because they'll understand that when you get angry, you really mean it.

Parenting is about playing a certain role, not about sharing power with your kids. Give your children the freedom to explore and make their own mistakes, but don't forget your position of authority. _You_ have the final say.

> _"When I talk to parents in Paris, what I hear all the time is 'C'est moi qui décide' — it's me who decides."_

### 8. Final summary 

The key message in this book:

**Children need space to develop their autonomy, but they need a certain framework to do so — and parents are the ones to provide it. So don't be a helicopter parent. Give children freedom but set strict boundaries, and allow your kids to develop their palate by exposing them to a variety of foods. Ultimately, your job as their caretaker is to lead them toward becoming independent.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _The Happiest Baby on the Block_** **by Harvey Karp**

_The Happiest Baby on the Block_ (2002) tackles one of the biggest issues new parents will face: the constant wails and tears of their baby. These blinks explain why your baby's survival depends on your responding to its cries, and how, by triggering the calming reflex, you can make your baby feel calm and safe.
---

### Pamela Druckerman

Pamela Druckerman is a former staff reporter for the Wall Street Journal and has written op-eds for _The_ _Guardian_, _The_ _New York Times_ and _The_ _Washington Post_. She is also author of _Lust In Translation: Infidelity from Tokyo to Tennessee_.

