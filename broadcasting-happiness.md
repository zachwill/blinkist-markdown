---
id: 5681b2f5704a88000700003c
slug: broadcasting-happiness-en
published_date: 2015-12-29T00:00:00.000+00:00
author: Michelle Gielan
title: Broadcasting Happiness
subtitle: The Science of Igniting and Sustaining Positive Change
main_color: F97F4C
text_color: AD5835
---

# Broadcasting Happiness

_The Science of Igniting and Sustaining Positive Change_

**Michelle Gielan**

_Broadcasting Happiness_ (2015) reveals the huge impact positivity has on productivity, teamwork and well-being. These blinks explain how you can communicate positive thoughts to send ripples of happiness through your community, your workplace and within your relationships.

---
### 1. What’s in it for me? Turn on your personal positive broadcasting tower. 

With radio and television, communications entered a new era: the era of broadcasting. Information, in the form of news or entertainment, could suddenly be "broadcast" or sent out into the world, for anyone with a receiver to pick up and listen to. 

Yet it's not just radio programs or TV series that are regularly broadcasted — people broadcast their own messages every day. A manager broadcasts her ideas to a department; a teacher broadcasts knowledge to his students. 

In these blinks, we'll take a closer look at how we can use this idea of broadcasting to promote positive change in our communities and a happier mind-set in ourselves. We'll learn how to use communications strategies to encourage well-being and success in others as well as in ourselves, by being our own personal broadcasting station and broadcasting happiness!

In these blinks, you'll learn

  * how CBS reporting on the 2008 crisis actually make viewers happier;

  * how reminiscing about times past helped seniors improve their eyesight; and 

  * which four words help you deliver bad news in a good way.

### 2. Your memories of past experiences and situations determine how you approach future challenges. 

When you see the word "job," what is the first thing you think of? Whether a funny memory from your very first position or an unpleasant event from a recent job, each person has _flash memories_ that can shape the way you think about a particular subject or theme. 

Words trigger different flash memories in different people. The word "success" might make one individual reflect on a ceremony where he won an award, while another person might think of the moment she finally finished a huge project. 

These thoughts aren't simply memories, but a reflection of each individual's attitude toward success. 

In this fashion, positive flash memories can actually create positive attitudes. But how do we create positive flash memories? Simple: by creating positive experiences! 

That's exactly what Sunnyside High School superintendent Dr. Richard Cole decided to do. His school was notorious for its poor graduation rate, which by 2007 had fallen to 41 percent. 

Cole knew that both students and teachers wallowed in a culture of low expectations. To combat this, he worked to give everyone at the school an attitude makeover. 

How did he do this? By sharing success stories wherever and whenever he could. Bulletin boards, newsletters and school announcements were filled with good news from the student body. This step was the beginning of a seven-year shift toward creating a positive learning environment. 

By 2014, the school's graduation rate had shot up to 89 percent. After leaving school, students no longer described their time at the school as miserable, but remembered classes and teachers fondly. 

In other words, the student body developed positive flash memories that would serve each of them well as they moved on to college or a career.

### 3. Broadcasting positive messages through storytelling can be so powerful it can improve your health. 

Humans have been telling stories to each other from our very early days — the detailed narratives drawn in ancient caves prove this. Today, mass media is our "cave," telling modern human stories. 

Of course, if you've ever watched the news, most stories sure don't leave you feeling upbeat or happy. 

While negative storytelling can bring you down, positive storytelling can energize you — even improve your health. But wait — doesn't that sound too good to be true?

It isn't. in 1979, Harvard professor Dr. Ellen Langer put the power of positive storytelling to the test in an experiment called, "Welcome to the year 1959!" 

She invited a group of 75-year-old males to attend a weeklong retreat. Yet at the retreat, the men had to pretend it was 1959 — when they were just 55 years old.

During the week, the group could only share stories about the work, pastimes, relationships and experiences they'd had up to the age of 55. Participants were also required to complete intelligence, memory and strength tests before and after the retreat. 

The results after the retreat were surprising. All the participants' scores improved significantly after a week of sharing stories. Even the men's eyesight improved by some 10 percent! 

Wouldn't it be great if news broadcasters could also give us a dose of positive storytelling? Or with all the violence in the world, is that even possible?

As a news anchor for CBS during the 2008 financial crisis, the author witnessed how the station lost half its viewers as a relentless stream of bad news drove people away. She wanted to inform the public about current events in a way that was optimistic but not dishonest; she didn't want to artificially sugarcoat the news, just to make it palatable.

In collaboration with positive psychology expert Dr. Martin Seligman, the author pioneered CBS's _Happy Week_, a program that offered positive, solution-oriented perspectives on even the worst of the world's news. 

CBS received more positive emails from viewers after this week of programming than it did in the entire previous year!

### 4. How do you build a positive team? By encouraging positive talk and letting optimism be contagious. 

Do you share good news freely? Too often we keep positive thoughts or events to ourselves. But why do we do this? 

There are a couple of reasons. Perhaps we aren't sure that people really care about the great weekend we had, or we don't want to bother a friend who may have a lot on her mind. 

But keeping mum about good things prevents us from tapping into the true potential of positivity.

The truth is, simply being optimistic isn't enough. You've got to _broadcast_ your happiness to others. When you do, it will boost their optimism, too — and this is a powerful thing. 

Just think of how great a work shift can be when all your coworkers are sharing the same bubbly, energetic mood. The workplace in particular is a space where broadcasting happiness is a vital tool. 

Yet companies rarely realize this. In cooperation with _Training_ magazine, the author performed a cross-industry study which revealed that 31 percent of employees felt they held in general a positive attitude but _didn't_ share their positive thoughts with coworkers. 

Think about it: if employees started sharing their positivity, these workplaces would be transformed! 

Of course, there are employees who do broadcast their emotions, yet they're just more likely to share negative experiences. A University of California study revealed that people who openly express emotions frequently are also those who hold pessimistic views about life and work. 

To keep the cranks from bringing your group, organization or team down, you've got to shake out and bolster your shy optimists and turn them into broadcasters.

By carrying out surveys or even taking part in casual conversations, you can uncover those positive broadcasters, and encourage them to let their positive voices be heard — and watch everyone's spirits lift!

### 5. Broadcasting motivating thoughts is one way to prime your team to cooperate effectively. 

Collaboration can be quite challenging. Each person on a team approaches work differently, which can often come to a head during collective tasks. Some team members might be super-motivated, while others have different priorities. Some are organized, while others are sloppy. 

By _priming_ your team to cooperate better, you can make the best of your team's strengths and weaknesses. 

This is where the power of positive words comes into play. Researchers at Stanford University uncovered the possibilities of priming with a "language test," that wasn't really a language test at all. 

Half of the participants before they were handed the test were shown words with negative connotations, such as "rude" or "impatient." The other half were shown words with more positive associations, such as "respectful" and "calm." 

The first group turned out to be just as unpleasant as the words primed them to be. These people interrupted the researcher 50 percent more often during the test, for example; while the second group behaved cordially and rarely interrupted — primed by the words they were shown.

Another study furthers the power that positive priming can have on individuals. 

Participants were asked to complete a test on general knowledge. One group was allowed to contemplate the characteristics of a stereotypical "intelligent professor" before the test, while another group considered a "hooligan." Interestingly, those who were primed by the professor scored highly on the quiz, while those primed by the hooligan scored lower.

### 6. Bad news is inevitable, but sharing it the correct way always improves the situation. 

Bad things can happen to anyone, at any time and anywhere. And the best thing to do when something bad does happen is to _talk about it_. 

Remember, this doesn't mean you're broadcasting negativity! There are ways to share problems with others in ways that make the situation better, not worse. 

Stick to the four Cs: social _capital_, _context_, _compassion_ and _commitment_. 

Social _capital_ describes the physical ways we connect with people we talk to. Body language and eye contact help to make both parties feel they're being listened to and understood. 

_Context_ means speaking about the real situation concretely and honestly, rather than hiding behind abstracts. This encourages you to tell the full story behind your dilemma, while the other person will be better able to grasp the challenges you're facing. 

_Compassion_ is one of the most important Cs. Before sharing your opinion on a situation, ask yourself: Did I really listen to my friend? If you don't understand why a person's problem is such a big deal, try asking them more questions to help you imagine what it would be like to be in their shoes. 

Finally, _commitment_ is vital in communication. By showing how willing you are to help someone, you'll create a genuine connection, rather than just exchanging platitudes and canned advice. 

Keep the four Cs in mind the next time you've got bad news to share or are listening to a friend who does. This will keep both of you in an optimistic, solution-oriented mind-set. 

But what happens if someone's negativity is just too much?

If it's clear that more talking won't help, then feel free to make a strategic retreat. When away from the negative person, take time to reflect on the problem and identify points to bring up next time. 

If you have the chance to discuss the dilemma with the person again, bring another positive-minded person with you. This will shift the balance in the discussion toward optimism, creating a better environment for listening and sharing advice.

### 7. Final summary 

The key message in this book:

**Harness the power of flash memories to shape your attitude toward workplace challenges. By creating a positive perspective and sharing it with others, you can make a huge difference not only in your working environment but also in your personal relationships.**

Actionable advice:

**Tap into your flash memories!**

Say you want to succeed in business. Write down the word "business" and then note which flash memories come up when you think about that word. Perhaps your flash memories are "hard work," "taxes," "cooperation" and so on. Working with flash memories can lead you to formulating more precise life goals, as well as revealing any subconscious assumptions, strengths and weaknesses on a certain topic.

**Suggested** **further** **reading:** ** _Rising Strong_** **by Brené Brown**

_Rising Strong_ (2015) is your guide to picking yourself up and dusting yourself off after a failure — and to becoming stronger, braver and kinder because of it. Whether you dream of being an entrepreneur or maintaining a loving relationship, these blinks supply you with the three vital steps to dealing with any struggle.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michelle Gielan

Bestselling author Michelle Gielan is a journalist and featured professor in Oprah's Happiness course. Her work reporting on the 2008 financial crisis as a national CBS news anchor ultimately led her to found the Institute for Applied Positive Research.

