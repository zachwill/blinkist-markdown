---
id: 53eb4a1436343300077d0000
slug: buddhas-brain-en
published_date: 2014-08-12T00:00:00.000+00:00
author: Rick Hanson
title: Buddha's Brain
subtitle: The Practical Neuroscience of Happiness, Love, and Wisdom
main_color: D6A047
text_color: 8A672E
---

# Buddha's Brain

_The Practical Neuroscience of Happiness, Love, and Wisdom_

**Rick Hanson**

_Buddha's_ _Brain_ is a practical guide to attaining more happiness, love and wisdom in life. It aims to empower readers by providing them with practical skills and tools to help unlock their brains' potential and achieve greater peace of mind. Specific attention is paid to the contemplative technique "mindfulness" and the latest neurological findings that support it.

---
### 1. What’s in it for me? Improve your quality of life by learning to control your emotions and thoughts. 

After reading these blinks, you'll

  * **Be happier.** You'll gain better control over your thoughts, desires and emotions, which will allow you to become a more positive person.

  * **Stop exacerbating misfortunes.** While it's inevitable you'll encounter some pain and suffering in life, you'll no longer make it worse by dwelling on it.

  * **Become a more compassionate and loving person.** You'll develop more empathy toward others and consequently form deeper, more trusting relationships with them.

### 2. Our minds can change the structure of our brains and influence the way we feel. 

At any given time, what we _feel_ is the result of the brain and mind interacting with each other.

But what exactly distinguishes the brain from the mind?

Our _minds_ are our mental processes — our thoughts, wishes and feelings. In contrast, the _brain_ is a highly complex bundle of synapses. So, while the mind is intangible, the brain is physical.

For instance, the brain contains chemicals called neurotransmitters that cause us to feel certain emotions, such as happiness.

However, our _conscious_ experience is the result of a close interaction between brain and mind. In other words, they form an integrated system.

For example, the neurotransmitter dopamine can make us feel excited and energized — sometimes uncomfortably so.

Let's say that you get a promotion at work, and that your brain responds by releasing lots of dopamine. Whether this will cause you to feel happy or anxious depends on your mind, whose job it is to _interpret_ this flood of dopamine.

But the mind's power doesn't end there: it can also alter the brain's physical structure.

Whenever you experience or feel something, this causes neurons to interact — or "fire" — with each other. Over time, these neurons physically change as enduring physical connections are created between them.

This is known as _Hebb's_ _Rule_, which is neatly expressed in the phrase: "neurons that fire together, wire together."

An example of this process is the simple act of laughing with friends. Doing this will establish new links in the brain between the neurons responsible for your memory of that moment and those triggering feelings of joy — proof that our mental processes can alter the structure of our brains.

Here's another example. London cab drivers need to memorize complex street maps and routes. Because of this, they develop a larger than average hippocampus — the key area of the brain activated when memorizing and then recalling how to reach a certain location. In other words, their experiences alter their brains.

> **Fact:  

** Your brain is three pounds of tofu-like tissue containing 1.1 trillion cells, including 100 billion neurons.

### 3. By engaging in self-reflection we can lead happier, more fulfilling lives. 

Many of us often feel like victims of our circumstances, and that our unhappiness and dissatisfaction with life is a foregone conclusion. What we don't realize is that if we actively reflected on these mental processes, this self-reflection would lead to a happier, more prosperous life.

_Self-reflection_ is the active endeavor of trying to understand our mental processes to gain better control over them — processes such as the emotions, thoughts and wishes that we experience daily.

A good example of self-reflection is thinking about what makes us happy. As you'll recall, mental processes can change our brain's structure, so positive self-reflection will result in positive structural changes in our brains.

For instance, more than 2,000 years ago, a young man named Siddhartha — later called Buddha — reflected deeply on the sources of both his happiness and suffering. By doing this, he was able to embrace happiness and free himself from suffering — a psychological transformation which, he believed, was accomplished by focusing on the positive aspects of his life.

Since the brain develops and adapts to whatever it focuses on, if we focus on the positive, we teach the brain to take in good experiences and incorporate them into our lives.

Of course, some people are convinced that being self-reflective means being self-indulgent. But this is not the case. By actively reflecting on our lives, we enable ourselves to become better people, for both our own sakes and those of the people around us. If you pursue a path of self-improvement you'll see wholesome changes to your personality and life, which will make you a more friendly and good-humored person.

For instance, if you develop a more positive outlook on life, your stress levels will diminish, which in turn will improve your interpersonal relationships. Just imagine, for example, how much your relationship with your significant other would improve if you weren't always fretting about your future.

So, how exactly do we do that? As the following blinks will show, it isn't impossible to become a happier person who is free from suffering.

### 4. Suffering is a product of evolution; it aids our survival and affects us today. 

As you might already know, our suffering derives from a useful evolutionary trait — it tells us when something is dangerous. In our modern world, however, sometimes these dangers are not so clear.

_Suffering_ describes all the feelings in life we don't like and would rather not experience. It's a blanket term for everything from the frustration that comes from just missing your bus to the profound anguish of losing a loved one.

Let's look at the role of suffering in more detail.

In essence, suffering is Mother Nature's way of keeping us alive. These negative feelings are informative — they tell us what we should avoid. Positive feelings, in contrast, suggest the things we should seek out.

These negative or positive feelings arise from very old parts of the brain, which we share with our evolutionary ancestors. Those ancestors had to decide whether to approach something or avoid it. For example, those who survived to pass on their genes to the next generation avoided snakes, and approached banana trees for sustenance instead.

Additionally, our brains are biased toward avoiding harm: negative memories are preferentially stored over positive memories in our brains, so that we don't ever approach that snake again.

Such _approach/avoidance_ _behaviors_ continue to play a major role in our lives today — but with one added burden: we now have to decide whether to approach or avoid abstract mental states.

We avoid not only snakes, but also embarrassment or shame. And we approach not only bananas, but also self-worth.

But while these approach/avoidance behaviors aid our survival, they can also make us unhappy.

Approach behaviors, for instance, are not completely positive; they too can involve suffering. Consider the many occasions when we can't attain the things we approach, desire or pursue, or when our desires cause us long-term suffering.

For example, eating a large dessert after a big meal might feel like just the right thing to do at the time, but can ultimately lead to suffering!

### 5. Some physical discomfort is normal, though we often make it worse than it needs to be. 

Unfortunately, pain is impossible to avoid entirely. However, even though physical and mental discomfort is unavoidable, our suffering arises mostly from our _reactions_ to these discomforts.

We usually experience discomfort on two separate levels. The first level can be likened to being struck by a dart — for example, accidentally touching a very hot plate, or being rejected by a romantic interest.

However, most of our suffering comes from a "second dart" that _we_ throw at _ourselves_. This is the second level of pain. "Second darts" refer to our own reactions to a painful event, be it physical or mental.

For instance, stubbing one's toe on a table leg would be a (very painful!) "first dart." The second dart would be the feelings of rage and the impulse to blame someone else ("Who moved that chair?").

This vengeful emotional state is the true suffering we feel, and it's caused by our reaction to the first dart.

However, some suffering doesn't even need a first dart as a trigger. This is because suffering always involves strong physical reactions that can trigger further "second dart" discomfort.

How?

Suffering kicks the _sympathetic_ _nervous_ _system_ (SNS) into overdrive. This triggers a flood of adrenalin and an increase in heart rate. That's why the first dart of being rejected by your crush can feel as uncomfortable as a root canal.

Then comes the second-dart reaction, increasing SNS activity, which causes more second-dart reactions in turn, such as being upset about feeling depressed. Things snowball: the body is constantly in a state of emotional stimulation, which is physically and mentally exhausting.

For example, if you're anxious about the speech you have to give tomorrow, that feeling of anxiety can remain for hours — or even days — after you've delivered your speech and the source of the first dart has vanished!

### 6. The key to happiness lies in certain ways of thinking. 

Did you know that each of us has the means to promote his own well-being, cause positive neurological effects and help reduce unhealthy desire? All we need to do is learn a few new ways of thinking.

The first one is _mindfulness_.

_Mindfulness_ is the state of complete awareness, and it can be generated by meditation. Meditation is key because it helps us to control our attention — which is why we're so aware of our bodies when doing yoga. Moreover, meditation actually increases gray matter in the brain regions that control attention.

So, how does mindfulness promote well-being?

It does this by stimulating the _parasympathetic_ _nervous_ _system_ _(PNS)_.

We stimulate our PNS when we're mindful. The PNS is like a cousin to the SNS, in that it reduces the heart rate and makes us feel calm and restful. In this way, being mindful helps us to feel happy and more relaxed.

The second approach is to think about _wholesome_ _intentions_, as these result in positive neurological effects in all regions of the brain.

Intentions are a kind of desire, and an example of a wholesome intention would be thinking about yourself as strong and powerful. Indeed, by actively thinking "I am strong, it's good to be strong," you can trigger many brain regions (from the limbic system to the prefrontal cortex) to release neurotransmitters that make you feel good.

Lastly, practicing _composure_ helps us to control our emotions and reduces any unhealthy desires we may have.

_Composure_ is a state of control that acts much like a circuit-breaker: when we're composed, we're able to sever the link between feeling good because of something in particular, and having an endless craving for it.

Practicing composure entails understanding how things make you feel, and realizing that your emotional feelings can be separated from your desires.

A good example is success. Success makes us feel good, but an endless craving for it can cause us to feel dissatisfied, and even depressed. Composure enables us to recognize that we'll be happier if we don't long constantly for it.

> _"I make myself rich by making my wants few."_

### 7. Meditation leads to greater insights about oneself and, ultimately, more wisdom. 

So far we've seen that meditation involves focused concentration. But that very concentration is not only a means for gaining awareness, it's also the driving force behind the generation of important insights.

Such insights about ourselves and the world around us are fundamental to gaining _wisdom_ — which is defined as the quality of having enduring, good judgment.

While meditating, our attention is intensely focused. This cuts through all the noise of our everyday lives, and illuminates that which is important to us. In turn, this leads to insight.

For much of our lives, we exist in a kind of "forest" of ignorance. By meditating, we develop a sharp knife that can help to clear the path to understanding. The insight we generate sharpens that knife, and the focus we practice gives it power.

When it works, meditation can bring about a singleness of mind. In this state, our senses are unified, all distractions are forgotten, and we can experience a deep immersion in the object of our attention.

In fact, studies on the brains of experienced meditators discovered a link between singleness of mind and high-frequency gamma waves. These waves are believed to underscore the sense of awareness and mental acuity experienced after meditating. This finding indicates that meditation can help us to become more wise.

In order to gain the benefits that singleness of mind can provide, you could try a focused meditation technique, such as yoga.

> _"I heard a story once about a Native American elder who was asked how she had become so wise, so happy and so respected. She answered: 'In my heart there are two wolves: a wolf of love and a wolf of hate. It all depends on which one I feed each day.'"_

### 8. Meditation can help you to attain a more calm and relaxed sense of self. 

Our _sense_ _of_ _self_ is the "I" with which we experience the world, and it's what we believe makes us who we are. Unfortunately, it also leads to some of our greatest feelings of suffering.

We suffer, for example, whenever we take things personally, or crave approval or affirmation from others.

But when we let go of this strong sense of self and immerse ourselves in the world without ego, we feel a sense of peace and fulfillment.

For example, when we don't internalize approval or rely on approval for our own benefit, we lead happier lives.

In fact, both Buddhist monks and death-row inmates achieve peace by relinquishing the "I," adopting instead the mantra: "No self, no problem."

However, a strong sense of self often comes in useful, and — through meditation — we can learn when to take things personally and when to step back from ourselves. Also, a sense of self helps us to maintain continuity in our experiences from one day to the next, and to distinguish ourselves from other people.

But sometimes our sense of self grows beyond our control, which leads to suffering.

This happens, for example, whenever we identify with things, such as "This is _my_ laptop," or "_I_ am this belief." But because everything in the world ultimately comes to an end, our over-identification with things means that eventually we experience loss and, subsequently, suffering.

In other words, by identifying with things we make their fate our own. Therefore it's best not to rely on our sense of self unless we really have to.

Luckily, meditation can help us to choose when we should and shouldn't exercise our sense of self.

For instance, in meditation we can reflect on things without using personal pronouns. So, instead of "I am thinking of birds," you could think, "thoughts of birds are arising."

Now that you've learned how to defeat suffering in favor of happiness and wisdom, how do we foster love instead of hate? The following blinks will explain.

### 9. Our brains are hardwired to love. 

Have you ever wondered why we love? The short answer is that our capacity to love — a concept that encompasses a wide range of feelings associated with kinship and cooperation — is in our DNA.

Indeed, the structure of the human brain has, during our evolution, developed for love.

As mammals, we have much larger brains than other species, such as reptiles. In large part, this is due to the need of mammals to manage their offspring and social relationships. For instance, primates' brain size is related to the sociability of the species: the more social the species, the larger their brain.

Furthermore, the size of the human brain has tripled in the past three million years, and much of that growth is devoted to facilitating good interpersonal relationships. More specifically, the brain has developed certain neural networks that express feelings of love, such as altruism and forgiveness.

Moreover, the brain has powerful neurochemistry that facilitates bonding among humans — including oxytocin, a neuromodulator that promotes caring.

All of this suggests that our large capacity for love has been, and continues to be, a fundamental part of human survival.

For instance, you can find romantic love in all human cultures, which suggests that love is a deep-seated trait in our biological makeup.

The biological basis of love is evident in the way that feelings of love help to generate trust in communities and play a role in the formation of partnerships for sexual reproduction. Therefore feelings of love are crucial in helping humans reproduce.

Moreover, as we evolved and our brains grew larger, our childhoods grew longer. Because our brains were more complex, it took longer for them to fully develop after birth.

Due to this, it became important for parents to develop strong bonds with their offspring, in order to care for them and ensure their survival.

### 10. With a greater sense of empathy, one can become a more compassionate and loving person. 

Have you ever wondered how you could be more compassionate to those around you? Or have you ever considered how empathy could help you to develop a stronger bond to your loved ones?

Well, empathy and compassion are closely related: indeed, empathy is the _foundation_ of true compassion.

To have compassion for others means caring deeply about them. When we are empathic toward others, we gain insight into their perspective. For example, by truly caring about your friends' inner feelings, you'll be more reassuring to them, and more compassionate.

On the other hand, when our empathy is lacking, there are often harmful consequences. For instance, children who grow up around caregivers who show insufficient empathy are often insecure as adults, and struggle to form strong relationships.

And when a child grows up with absent parents, he can struggle to relate to people. Unfortunately, this can perpetuate a cycle in which, as an adult, he harbors deep prejudices and becomes an exploitative person — he may in turn become an absent parent to his children.

Yet there's more to empathy than compassion. Being empathic can open you up to deeper, more successful relationships with others.

This is because empathy can help to soothe and diffuse interpersonal conflicts — as, for example, when a good friend criticises you.

If you show that friend empathy, you'll attempt to address and understand her concerns. As a result, she'll probably appreciate your empathic approach, and thus be less critical and more understanding of you.

Additionally, empathy helps to eradicate any caution we may feel when it comes to being close to others. It's a fact that most psychological pain occurs in close relationships and during our childhood years, because it's in these situations that our memory network and emotional reactions are least controlled by the prefrontal cortex — the decision-making part of the brain.

For example, we're more likely to get hurt as a result of putting our trust in a deceitful person when we're young. In contrast, as adults we simply wouldn't trust that person in the first place.

However, by being empathic as adults, we gradually erode this wariness and distrust, and consequently enable ourselves to establish deep connections with others again.

### 11. Final Summary 

The key message in this book:

**This** **book** **aims** **to** **show** **the** **reader** **that** **anyone** **can** **change** **their** **life** **for** **the** **better** **by** **changing** **their** **mental** **habits.** **The** **structure** **of** **our** **brains** **does** **not** **remain** **the** **same** **throughout** **our** **lives,** **but** **actually** **develops** **in** **tandem** **with** **our** **minds.** **By** **understanding** **and** **using** **meditative** **practices,** **we** **can** **help** **to** **transform** **our** **brains** **and,** **in** **doing** **so,** **achieve** **happiness,** **love** **and** **wisdom.**

Actionable advice:

**Throughout** **your** **everyday** **life,** **try** **to** **be** **mindful.**

There are many ways to do this. You could actively focus on your breathing while performing daily activities. You could try to nurture a feeling of calm when you're around other people. Or you could take a moment to reflect on aspects of your daily routine, such as meal time — for instance, focus on the wheat in your bread, and imagine it growing in a field, being threshed, and so on.

**Use** **mental** **imagery** **during** **interpersonal** **conflicts.**

If you are in an argument and someone is shouting at you, imagine that you are a firmly rooted tree and that their words are a light wind that is gently blowing past you. Mental imagery activates your right parietal lobe, which induces a sense of wholeness and well-being.

**Suggested further reading:** ** _The Art of Happiness_**

_The Art of Happiness_ is based on interviews of His Holiness the Dalai Lama conducted by the psychiatrist Howard C. Cutler. The combination of Tibetan Buddhist spiritual tradition with Dr. Cutler's knowledge of Western therapeutic methods and scientific studies makes it a very accessible guide to everyday happiness.
---

### Rick Hanson

Dr. Rick Hanson is a neuropsychologist, meditation teacher and a senior fellow at the Greater Good Science Center of the University of California, Berkeley. Hanson's previous book, _Hardwiring_ _Happiness_, is a best seller, and has been translated into 14 languages.

