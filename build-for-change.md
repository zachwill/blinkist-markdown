---
id: 568ae29bde78c00007000000
slug: build-for-change-en
published_date: 2016-01-06T00:00:00.000+00:00
author: Alan Trefler
title: Build For Change
subtitle: Revolutionizing Customer Engagement through Continuous Digital Innovation
main_color: 2EADE8
text_color: 1F749C
---

# Build For Change

_Revolutionizing Customer Engagement through Continuous Digital Innovation_

**Alan Trefler**

_Build for Change_ (2014) sheds light on the changing relationships between customers and businesses. By explaining the inner workings of customer loyalty and highlighting the importance of new technological developments, these blinks equip businesses with the tools they need to create a powerful and sustainable customer base.

---
### 1. What’s in it for me? Position your business to capture the customers of tomorrow. 

The times they are a-changin' — at least they are for businesses. For generations, companies have maintained a rather cosy relationship with their customers: companies advertised and marketed their wares and, depending on whether they liked their products, customers would buy them or wouldn't. Basically, that was it. 

But these days, social media and a permanently connected society means that the relationship between business and customer goes both ways. No longer will a disinterested or disappointed customer simply refuse to buy your product; they will now actively try to bring it down. Social media can destroy a poorly run business in a matter of weeks. 

So what should modern businesses do? These blinks show you how companies can design themselves to please the ultra-demanding and often angry customers of today. 

In these blinks, you'll discover

  * why businesses should be scared of Generation D;

  * why you should bring your IT department to the center of your business; and

  * why most new IT systems cost too much and fail too often.

### 2. The coming generation of customers could spell the end for many companies. 

Whether you know them as twenty-somethings, Generation Y or Millenials, one thing is certain: young people today are a very different breed from their parents. Marketers have truly struggled to win them over in recent years, which has earned them another, rather appropriate name: _Generation Content._

Generation Content, or simply Generation C, are entitled, fickle and notoriously difficult to please. If a company makes a single misstep, they're liable to lose this generation's loyalty completely. Nokia learned this the hard way during their desperate campaign against smartphone supergiant Apple. 

By bombarding customers with their criticisms of the iPhone ("You can't replace the battery!", "It breaks easily!"), Nokia hoped to recapture their old customers. But their plan failed miserably. 

Generation C appreciated Apple's innovation and sleekness, and didn't appreciate Nokia telling them that they were wrong. So they turned away from Nokia, and the company flopped. 

This is one example of many where Generation C proved to be a nightmare for businesses. But the worst is yet to come: _Generation D._ The next round of young people won't just turn away from companies they don't like — they will seek them out and destroy them. 

Generation D is all too aware of the power that the internet provides them. If a product displeases them, they can easily rally hundreds, thousands or millions of other users around their cause. So what rubs Generation D the wrong way? Three things above all: poor service, a product not living up to its advertising or products that are disappointing compared to their competitors. 

Companies that fail to stay in Generation D's good books risk going under during the _customerpocalypse_, which, simply put, is a company's total annihilation. A customerpocalypse occurs when a company sees their customer base disappear before their eyes. If you're keen to avoid that with your business, read on to find out how best to deal with Generation D.

### 3. Data is only useful when used thoughtfully – don’t take it for granted! 

These days, data is king! Many savvy businesses are aware of the important role that Big Data will play in their customer relationships. Some even see it as their key to success. Yet, Big Data simply isn't enough to inform the future of a company. Why?

Big Data is nothing more than a snapshot of the past. Users' past searches, transactions and requests can tell you a lot about who your customer was, and can tell you something about who they are at the moment, but tell you very little about their future desires, hopes and needs.

The true key to a company's success is their ability to fulfill a customer's need. Most businesses realize this, but some still fail to recognize that _needs change_. Take Sony, for instance. Once the market leader in personal music devices, Sony's winning products like the Walkman and Discman were wiped off the map before they could do anything about it. What happened?

Apple had entered the market with a new approach to meeting modern customer needs. Their MP3 player responded to the desire for constant access to music with new possibilities for accessing music online — the iPod and iTunes store were born. 

Sony, meanwhile, had taken their success for granted and hadn't even tried to see if their customers' interests and desires were changing. And what a costly mistake that was!

The lesson here for businesses today is that Big Data is useful, but is not to be taken for granted. If you really want to use data to your advantage, you've got to know how to work with it. And that's no easy task! 

Take Vodafone: their approach is all about careful judgement and constant improvement. Vodafone uses information about past customer behavior to create personalized "daily specials." These specials are a shrewd blend of forecasting and trial and error. And it works — Vodafone continues to grow and maintain a huge market share.

### 4. Win over Generation D by designing your processes to suit and support them. 

Data brings us one step closer to winning the favor of the Generation that demands personalized customer experiences and undivided attention. But what they want most of all is _ease_. 

Digital technology affords businesses many opportunities to streamline their processes. Rarely, though, do businesses use this opportunity effectively. 

Many companies optimistically believe that simply adding an electronic element to their process will please young customers. If only it were so easy! 

Consider the bank BB&T, who moved their account creation processes online, hoping it would boost customer satisfaction. This was actually a rather senseless move, considering that customers still had to fill out forms just as lengthy as the ones in bank branches, and had to wait just as long for approval. Unsurprisingly, customer satisfaction dropped significantly. 

BB&T is just one of many companies guilty of an _inside-out_ approach. This approach typically features departmentalized processes with firm divisions separating marketing, customer service and HR departments. Such a disjointed approach simply cannot achieve the quick, efficient and reliable customer service that is expected of modern-day companies. 

These companies need to switch things up and become _outside-in_, and they can do so by focusing first on what the customers want, then designing a seamless process to provide it to them. 

After their disastrous first attempt, BB&T made the shift to _outside-in_. 

They designed a process where all aspects of the customer account department blended together. Whether a customer wanted to set up their account in the bank, online, over the phone or all of the above, an automated process was cued. 

Customers at BB&T can now create accounts faster and much more easily. BB&T has cut costs for back-end processes, customer satisfaction is at 90 percent and applications abandoned halfway have been reduced by half. One BB&T executive even noted that the increase in business was equivalent to opening 75 brick-and-mortar bank outlets.

### 5. The traditional approach to building IT systems is failing companies. 

Do you know how to code? For most of us, coding sounds like something that belongs in _The Matrix_. IT experts, with their knowledge of complete foreign languages comprising numbers and symbols, may even seem like an alien species. This is precisely the divide that causes problems in so many technological systems today. 

The majority of companies designing their IT systems witness a clash between the businessmen who come up with product designs and the developers hired to build them. Business owners take it as their responsibility to work out what they want from their IT system. Yet, most have little to no knowledge as to how such systems operate, and their plans are often hopelessly complicated or impractical. 

These plans are then delivered to the IT experts, who themselves have little idea of what customers and operators need from the IT system. So, the developers just build the plans as they were presented to them. Unsurprisingly, most systems created this way end in failure. 

Many become what's known as _zombie systems._ This is a flawed IT system that desperately needs a change in design but never receives one, simply because the communication rift between the businessmen and the IT professionals is never resolved. The system is left hobbling along, never quite doing the job effectively and often breaking down. 

Sometimes, a zombie system can lead to a _manual system._ This occurs when frustrated operators do away with a complicated system and use rudimentary manual processes instead, doing the best they can with elementary excel systems or multiple computer screens to run a process a little faster. 

This is a slight improvement, but is still far from smooth. Even so, it is possible to create technological systems that work — and it all starts with teamwork. Find out more in the final blink.

### 6. Teamwork between developers and businesspeople is what makes technological systems effective. 

If you want to avoid the technology issues described in the previous blink, it's time for your business to completely rethink its approach to designing digital systems. Naturally, this starts with bringing your IT and business teams together. 

A truly modern company is founded on great communication between businesspeople and IT people. For inspiration, we need only take a look at the world of botany. When scientists want to develop a plant that's hardier and more bountiful, they take parents from two different species to breed together. 

This is called _hybrid vigor_, and you can do it in your business too. To create stronger, more effective internal processes, allow outstanding workers from the business and IT sectors to combine their specialist skills. Several businesses have already successfully implemented _innovation centers_, where teams from both worlds come together to create efficient, capable and reliable IT systems. 

If you want to take your processes to the next level, you could even employ a _Chief Process Officer (CPO_ ) to oversee the whole process. Telstra, the Australian telecommunications company, boasts a very strong customer service process that they owe to a redesign in the executive branch overseeing customer service. 

By creating specialized roles for these individual process- and customer-focused tasks, Telstra is able to turn their research on what customers need into highly efficient processes. And it works! Customer satisfaction continues to increase and Telstra has witnessed a 70 percent decline in the time from order to delivery.

### 7. Final summary 

The key message in this book:

**The rise of Generation D threatens to destroy any businesses unable to keep up with their high expectations. Businesses that fulfill ever-changing customer needs through constant innovation and flexible, dynamic internal processes will succeed. The rest will bite the dust!**

Actionable advice: 

**Change comes from within.**

No progress is possible without a change in company culture. Ensure that your businesspeople, your customer service personnel and your developers feel like they're playing on the same team, combining their specializations to work toward the same goal. This way, your company will be ready for any challenge that Generation D throws at it! 

**Suggested** **further** **reading:** ** _Running Lean_** **by Ash Maurya**

_Running Lean_ presents you with a fail-safe strategy to bringing your new product successfully into the market. By promoting a methodology of clever testing and planning ahead, the book gives you a step-by-step guide in building a business model that works while saving time, money and effort.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Alan Trefler

Alan Trefler is the founder, CEO and Chairman of Pegasystems. He was chosen as the American Business Awards' Software CEO of the Year in 2009 and was named Public Company CEO of the Year in 2011 by the Massachusetts Technology Leadership Council.

© Alan Trefler: Build for Change copyright 2014, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

