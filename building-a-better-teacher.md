---
id: 5485c1fd6331620009030000
slug: building-a-better-teacher-en
published_date: 2014-12-08T00:00:00.000+00:00
author: Elizabeth Green
title: Building a Better Teacher
subtitle: How Teaching Works (and How to Teach it to Everyone)
main_color: None
text_color: None
---

# Building a Better Teacher

_How Teaching Works (and How to Teach it to Everyone)_

**Elizabeth Green**

In _Building a Better Teacher,_ you'll discover what exactly makes a good teacher and how the art of teaching is practiced in the United States. It examines pervasive problems in the current system and offers theories on how these can be addressed. With impressive detail, the book takes a look at what makes certain schools successful and how we can all help improve our children's – and the nation's – future.

---
### 1. What’s in it for me? Learn how teaching works and how we can improve it. 

What makes a good teacher? A simple question, but one that is actually difficult to answer.

Are good teachers made, or born? Can we define the qualities of a good teacher, or do you just know it when you see it?

Many researchers have attempted to uncover what it is that makes a truly great teacher great. These blinks offer an inside look into this research as well as the history of _pedagogy_, or the study of education, and how it has changed over time.

In these blinks, you'll learn about the challenges faced by the education system in America, and why it's lagging behind the rest of the world — even though some of the best pedagogical research comes from the United States.

In the following blinks, you'll also learn:

  * why a burger from McDonald's proved the value of education to an investor;

  * why strict discipline is counterproductive to effective learning; and

  * why math is important for today's pupils now more than ever.

### 2. The American education system employs a huge number of teachers, yet is under great strain. 

Teachers make up the majority of the American workforce. Before we delve into the specific details of the American education system, you first should understand just how big it is.

In 2008, a study conducted by the American Bureau of Labor Statistics concluded that the teaching profession claimed the highest number of employees. In the United States, there are 180,000 architects, 185,000 psychologists, 952,000 lawyers, 3.3 million blue-collar workers — and _3.7 million_ _teachers_.

Moreover, this number actually needs to _increase_. Three million additional teachers will be needed between 2013 and 2020, as the baby-boomer generation retires.

Despite a huge teaching workforce and America's status as a world power, its education system is still falling behind global standards. This is especially true in mathematics.

In fact, a study commissioned by the U.S. government found that the average Japanese student could outperform nearly _98 percent_ of American students in math.

Further research by the _Dallas Times Herald_ concluded that out of eight countries that were examined in the study, Japan ranked first in math while the United States ranked last.

The results of these studies were alarming to both politicians and businessmen, particularly because of the way the U.S. economy is changing, shifting from relying on production (such as the auto industry) to relying on information products (such as software and telecommunications).

This shift means that math, science and engineering are more important now than ever.

The _Third International Mathematics and Science Study_ (TIMSS) compared the education systems in the United States, Germany and Japan in the 1990s and found vast differences in teaching methods.

Thus if we want to improve the American education system, we need to start by improving our teaching.

> _"More people teach in this country [America] than work at McDonald's, Wal-Mart and the U.S. Post Office combined."_

### 3. There are two schools of thought on how to improve the American education system. 

Most people would agree that we should improve our teaching methods to improve education overall. But how, exactly?

Well, there are two schools of thought on education reform. The first focuses on improvement through _accountability_.

The accountability argument suggests that children are suffering because teachers don't have enough incentive to excel at their jobs. U.S. President Barack Obama used this idea as the basis of his education policy during his campaign for president.

According to the accountability argument, American education is failing because teachers all receive equal treatment regardless of how effective they are. They receive the same pay raises, evaluations and job protections.

In 2009, Obama stated that the problem isn't that there are good and bad teachers, but rather that we can't identify the good from the bad.

Proponents of the accountability argument call for continual teacher evaluations and student testing. They believe this data can determine which teachers should be rewarded or punished, and how their careers should advance.

The other argument for education reform is based on _autonomy_. While the accountability argument calls for more government intervention in education, the autonomy argument calls for the opposite.

Proponents here say that teachers are professionals just like lawyers, and they should be treated as such. They'll only improve if they can be trusted, respected and given the freedom to do their jobs.

The _Chicago Teachers Union_ believes in autonomy and argues that the education system in Finland proves the value of it.

Teachers in Finland are highly trusted and respected, and they aren't subjected to a rating system. They have a lot of freedom and they work together to develop curriculum and manage a school. As a result, teaching is a top career choice Finland.

Both of these arguments for reform have merit, but still stem from a fundamental misunderstanding of how teaching works.

### 4. Initially, educational research didn’t focus on the actual process of teaching. 

While educational research has been conducted in some form since the 1900s, it's only recently that research has turned its focus to the process of actually teaching.

The study of education is called _pedagogy._ Early pedagogical researchers, however, weren't very interested in the topic at all. In fact, Edward Thorndike, one of the founders of pedagogy, only became interested in the field because it was the best job he could find after studying psychology.

Thorndike's research was focused mostly on psychological issues in schools, rather than education itself. Like Thorndike, most other early pedagogical researchers ignored the practice of teaching simply because they found it boring.

This began to change as the number of teachers rose, and by 1948, there were nearly 1 million teachers in the United States. With the increase, teacher training also became a lucrative business for schools and universities.

Yet the first real attempt to scientifically study teaching resulted in the _process-product paradigm,_ developed by education researcher Nate Gage. The process-product paradigm is based on the idea that you can compare the "process" of teaching with its "product" of student learning.

By studying the process and product, researchers could determine which teaching practices were effective.

Gage performed his experiments in a rather formal setting, by bringing teachers into a lab to instruct real students. He videotaped the lessons and tried to control certain variables, such as whether a teacher used a chalkboard. He wanted to see what methods yielded the best results.

The results of his studies were not especially helpful, as he found correlations between arbitrary and sometimes contradictory variables. For example, a teacher walking around correlated with the students paying more attention, but so did a teacher that kept still and only moved her hands.

Clearly, what made a good teacher was still somewhat ambiguous. This led researchers to assume that good teachers were simply born that way.

### 5. Education reform grew with more reliable studies and a commercial interest in teaching. 

Did you know that one of the biggest breakthroughs in educational research didn't come from the classroom, but from the hospital?

Medical research help shed light on the challenges teachers face, in particular, the research of academic Lee Schulman. Schulman was another researcher who fell into pedagogy after leaving a different discipline, in his case philosophy.

Schulman was fascinated with _epistemology_, the study of knowledge. He researched complex problem solving by observing how doctors would diagnose patients in controlled experiments.

His results showed that skilled doctors actually drew several conclusions straight away. They didn't first assess symptoms, then specific signs before diagnosing a disease; instead, they identified several diagnostics at once. It turned out that decision making in medicine was even more complicated than the textbooks made it seem.

Schulman then extrapolated his findings to teaching and uncovered just how complex the profession was also. Like doctors, teachers had to make quick decisions in circumstances that were always changing. They had to decide in the moment whether to give the student an answer or wait for them to come up with it on their own, for instance.

Pedagogy advanced further as businessmen became more interested in investing in education. Poor teaching didn't only affect the future of students but also the future of big business.

Alfred Taubman, the CEO of fast-food chain _A &W_, learned this with the release of a particular product. He had introduced a "one-third pounder" cheeseburger at the same price as McDonald's "quarter pounder" in order to compete with them. Testing showed that the A&W burger beat the McDonald's burger in both value and taste, but it failed to sell. Why?

Well, market testing also revealed that customers felt they were being cheated — they thought they were being sold less meat for the same price. Astoundingly, customers thought that a third was smaller than a fourth, because three is smaller than four!

This made Taubman realize how badly education reform was needed in the United States, so he began investing in it. Other business tycoons, such as Microsoft's Bill Gates, have also shown great interest in the subject. Commercial interest in teaching and education reform has helped it develop further.

### 6. While leading educational research is produced in America, it’s implemented in other countries. 

Even though the United States has lagged behind in international academic rankings, a lot of successful foreign education systems are built largely on American research.

Most of the best teaching theories have originated in the United States, from researchers like Magdalene Lampert. Lampert received her Ph.D. from the Graduate School of Education at Harvard University, and chose to go into elementary math teaching after graduation.

Lampert and other teachers took an elementary school called Spartan Village in Michigan and turned it into a teaching laboratory. They used their academic knowledge to try and isolate the best teaching practices and become the best teachers they could be.

Their first step was videotaping all lessons so they could better assess lesson plans. This was the 1980s, so the technology was still quite new.

The Spartan Village teachers eventually perfected what became known as "This Kind of Teaching" (TKOT). TKOT was focused on understanding the students' perspective by analyzing their confusion, and showing them the path to an answer rather than the answer itself.

Interestingly, while the United States was at the forefront of teaching research, findings were being implemented elsewhere, in countries like Japan.

The first comparative study of classroom teaching differences between the United States and Japan was called the _TIMMS_ study. The TIMMS study revealed that even though Japan had a strong education system, the teachers themselves were not always so great.

Informal interviews with Japanese teachers showed that they used similar methods to American teachers before education reform began in the 1980s. They had subjected the students to rote memorization in math classes, for instance.

More startling, however, was the revelation that Japanese education reform had been based on articles published by Magdalene Lampert and other Americans! The United States was producing useful pedagogical studies, but it wasn't benefiting from them.

### 7. Some charter schools have succeeded in implementing policies of accountability and zero-tolerance. 

So how can we push for education reform? Some teachers have taken this matter into their own hands, by starting their own schools.

_Charter schools,_ which started in 1991, have changed the way schools operate. A charter school receives part of its funding from its school district, but it is run by management that is independent from the district, usually a group of teachers or activists.

Charter schools are run like businesses, or "educational start-ups." They're called charter schools because a founding charter lays out the school's goals when the school is formed.

One charter school, the _Academy of the Pacific Rim_ (APR), states that students have to meet certain academic targets or the school would shut down, for instance.

Like businesses, however, charter schools soon became obsessed with statistics. They compile huge amounts of data, called _quantitative metrics_, and use these metrics to determine the best teaching methods they can use.

Early charter schools believed strongly in accountability and _zero-tolerance_. APR founder Doug Lemov was particularly obsessed with accountability. He believed that charter schools were successful because they were driven by students' results. After APR opened, he attended Harvard Business School so he could create diagnostic tests to measure the needs of his students.

Discipline was also thought to be critical to the success of charter students, especially as most charter schools operated in rough, inner-city areas. Lemov thought a lack of discipline caused other schools to fail, so he instilled nearly militant-like rules at APR. One student was sent home for having the wrong shade of blue sports shorts, for instance.

APR was generally successful, but the school's policy of zero-tolerance didn't work at many other schools.

### 8. Harsh discipline doesn't benefit a student, but social problem solving does. 

Are strict rules and harsh discipline the keys to successful education? No, they aren't.

In fact, harsh discipline is _counterproductive_ to learning. Discipline can make a student focus, but it also gives him anxiety and low self-esteem.

Rousseah Mieze, a APR graduate who later became a teacher, said the strict environment of APR shaped his negative self-image. When he was in college, he'd still imagine his former teachers saying things like, "You don't work hard enough. You don't belong here."

Psychologists who study discipline have also found that harsh disciplinary actions can breed feelings of resentment, and can inhibit learning overall. Suspension and expulsion, for instance, are harmful.

Yet suspension and expulsion were initially thought to improve a learning environment because the action removed the disruptive students. A 2008 study by the American Psychological Association disproved this, finding that schools with high suspension and expulsion rates had the _worst_ environments for learning.

_Social problem solving_ is a much better alternative. Social problem solving sees discipline as an opportunity to learn, rather than a punishment.

Proponents of TKOT helped developed this method of resolving conflicts. When a conflict broke out between students, teachers viewed the conflict like a mathematical mistake and helped the students solve it on their own.

In social problem solving, conflict is resolved collectively and in its natural setting.

One teacher, for example, had a student named Jamal who struggled with anger. He came from a chaotic lower-class home, and often came to school disheveled and smelling badly. During one incident, he lashed out at a group of girls who had been teasing him for his smell.

Shannon, the teacher, solved the issue through mediated discussion with the students. The girls learned about Jamal's difficult home situation and he learned how his aggression had upset them.

The conversation changed the students' behavior, and Jamal himself said he gained a "different view on life" because of it.

### 9. A combination of accountability and teacher development is the key to better teaching. 

It might seem tempting to simply hire lots of teachers, try them out, keep the best ones and fire the rest. That idea is dangerous, however.

Accountability alone won't lead to better teaching. Many theorists have tried to isolate the traits that make a good teacher, to then support those traits and reward those teachers. The Hamilton Project, for instance, was an initiative that tried to use statistics to correlate teachers' attributes with their success.

Yet the project itself was unsuccessful. The Hamilton Project found that a teacher's education level and certifications had no relationship to students' performance. It seemed the only way to tell if a teacher was good or bad was to let them start teaching.

Using statistics to determine a teacher's career trajectory is also dangerous, as an individual teacher's scores can vary dramatically.

In New York City, for example, 31 percent of the English teachers whose performance scores were in the bottom 20 percent in 2007 moved up to the _top 40 percent_ the following year.

If those teachers had been fired in 2007, New York would've lost nearly 400 teachers who were destined to dramatically improve.

The best method for improving teaching utilizes both observation and teacher development. Evaluations can offer a good overview of a teacher's performance and also help them become more effective. Teachers benefit from having an observational rubric and a set of criteria for evaluations.

The Bill Gates Foundation developed the _Protocol for Language Teaching Observations_ (PLATO) to offer helpful evaluations for language teachers. PLATO has also been used as a development tool. It allows teachers to examine their teaching styles together, by watching videos of their lessons and collaborating in workshops.

Observations can help a school and its teachers improve, but teachers must be able to engage with feedback to improve their teaching skills.

### 10. Final summary 

The key message in this book:

**Good teachers aren't simply born, they perfect their craft over time. Teachers need a chance to practice and improve, especially now as the American education system lags behind international standards. If education in the United States is to raise its standards, we need to nurture our teachers through a combination of accountability and development methods.**

Actionable advice:

**Don't discipline children too harshly.**

It's certainly tempting to punish or suspend children that behave badly. That might fix the problem in the short term, but it actually inhibits a child's overall learning. It's much more effective to solve conflicts through social problem solving. When children can engage with a problem in a safe environment, their behavior is more likely to change for the good.

**Suggested** **further** **reading:** ** _The Smartest Kids in the World_** **by Amanda Ripley**

These blinks examine the education systems of three countries — South Korea, Finland and Poland — which have done especially well in the international test for scholastic aptitude known as PISA. They also investigate the problems of the current US education system, and how they might be fixed.
---

### Elizabeth Green

Elizabeth Green is the cofounder of _Chalkbeat_ , a non-profit education news organization. She was also a former Spencer Fellow at the Columbia School of Journalism, and has written for major publications, such as _The New York Times_.

