---
id: 5713abe15236f20007c16f59
slug: building-social-business-en
published_date: 2016-04-18T00:00:00.000+00:00
author: Muhammad Yunus
title: Building Social Business
subtitle: The New Kind of Capitalism that Serves Humanity's Most Pressing Needs
main_color: B7843E
text_color: 85602D
---

# Building Social Business

_The New Kind of Capitalism that Serves Humanity's Most Pressing Needs_

**Muhammad Yunus**

_Building Social Business_ (2010) is a guide to social businesses, that is, companies that do good. These blinks explain everything you need to know about what social businesses are, how they work and what you need to start your own — and start changing the world for the better.

---
### 1. What’s in it for me? Learn what a social business is and how you can start your own. 

Although capitalism has brought prosperity to many parts of the world, far too many people have been excluded from its benefits and live a life of crushing poverty. But what can we do to make the world a better place? Well, a great first step is to build a social business.

Charity, social entrepreneurship, nonprofit organizations, nongovernmental organizations and so on are all meant to support a good cause — but a social business is something entirely different. 

In these blinks, you will learn what a social business is all about and what differentiates it from other social enterprises. You will be given advice on how to come up with a concept for your own social business and how to get it started. 

You'll also learn

  * how poor Bangladeshi people run a bank;

  * why a social business actually should aim for profits; and

  * why capitalism is incomplete without social businesses.

### 2. There’s a different way to do business that’s about solving society’s problems rather than getting rich. 

You've likely already heard of the term "social business", but what does it really mean?

Fundamentally, a social business isn't based solely on its bottom line. Most businesses, private ones in particular, are all about maximizing profit; on the other side of the coin are nonprofits that rely primarily on philanthropic donations. But a social business is distinct from both.

The primary aim of a social business is to solve social, economic or environmental problems. To accomplish this goal, such businesses employ regular business methods like the production and sale of a product or service that makes the venture self-sustaining.

For instance, Grameen Danone makes and sells affordable yogurt products loaded with micronutrients, and their overriding goal is to combat malnutrition among children in Bangladesh. 

But it's not quite that simple. In fact, there are two forms of social business: _Type I_ and _Type II_. A Type I social business produces profits but does not dispense dividends — that is, they don't regularly pay out a percentage of their profits to the company's owners. 

So, while Type I social businesses are still owned by investors, all the profits are reinvested in the company, resulting in neither a gain nor a loss for the investors. Grameen Danone is an example of one such Type I social business, as they operate without dividends. 

On the other hand, a Type II social business operates like a typical for-profit company. However, unlike a Type I social business, it is owned by poor people rather than wealthy investors. As a result, the profits of the business go directly to low-income people and, therefore, serve a social function — namely the alleviation of poverty. 

For example, the Grameen Bank is a Type II social business. It's owned by the poor of Bangladesh and provides loans to low-income citizens.

### 3. A social business is distinct from other charitable and socially oriented organizations. 

There's a lot of confusion surrounding social businesses, and discussions about them are likely to feature terms like social entrepreneurship, NGO, social enterprise and foundation. However, these ventures are all different from social businesses. 

For starters, a social business is not the same as social entrepreneurship. Social entrepreneurship is all about one person — the entrepreneur — and his or her idea. So, while such initiatives can have a social impact, they can be driven by non-economic, charitable or business intentions. 

So, while the entrepreneur in question might have a social vision, his social entrepreneurship could take many forms and is thus different from a social business — which you now know is an economically stable business with a social mission and no loss or dividends. 

Second, contrary to what many may assume, a social business is not a nonprofit organization. Consider a foundation, a type of a nonprofit organization. While they might have a social agenda, they are distinct from social businesses because they're not economically sustainable; they don't generate profit or any income, they simply distribute charitable donations. 

On a similar note, nongovernmental organizations, or NGOs, are also distinct from social businesses, since NGOs are purely non-profit organizations with charitable goals. They run on philanthropic donations and, therefore, lack economic sustainability. Furthermore, they spend much of their time and energy on fundraising, which makes it difficult for them to expand in any sustainable fashion. 

So, you're now interested in a social business, but how can you start your own? That's what you'll learn next, and it all begins with forming an idea!

### 4. Start your social business by identifying a problem that you think you can solve. 

Think of a standard, profit-maximizing business. When founding such a company, entrepreneurs generally seek out a gap or niche in the market that they can fill. Their main intention in doing so is to earn as much money as possible. 

However, when launching a social business, it's essential to start by identifying a social issue that you want to solve and are uniquely suited to tackling. As a social business, you'll by definition be serving a social function, which makes turning a profit a requirement, not a goal, for your enterprise. 

So, to identify a social problem, start by asking yourself what is needed in the world and what bothers you most? What causes these needs to arise? 

Then ask yourself, considering my skills and talents, can I build a social business that helps solve such problems and fill such needs?

For instance, take Cure2Children, which collaborates with the Grameen Healthcare Trust. This social business was founded by a doctor and an entrepreneur who both saw a need for health care in impoverished nations. 

Specifically, they saw a need to treat the genetic disease thalassemia, a condition that affects approximately 100,000 children around the world today. Dr. Faulkner, the doctor, learned how to treat thalassemia and the pair had their idea. 

After identifying a need, choose the one problem that you think you stand a chance at solving. After all, you can't save the whole world at once. Keep in mind that you're new to the game, just learning how to run a social business, and that it's important to start simply. 

After all, if you dive head first into one of the biggest and most complicated problems facing the world, you'll likely struggle for months or even years just to get up and running.

### 5. Attract investors with a reasonable business plan, and look to for-profit companies when building your social business. 

Now that you've got an idea, the next step is to secure funding. But how exactly?

Well, it's important to come up with a reasonable business plan to attract potential investors. This plan should include a detailed five-year budget and forecast for operationalizing the business, complete with both cost and revenue structures. 

In addition, the budget you generate should show that your idea is viable: revenue should cover all costs and preferably leave you with a small surplus for rainy days. 

But it's also important to demonstrate that your cash flow is balanced on both a weekly and monthly basis. After all, you might need more cash in April than in August, and if your revenues aren't balanced enough to account for the difference, your business could be toast by the end of the summer. 

Finally, it's a good idea to build your social business around the structure of a for-profit company. While this might seem surprising at first, here's why it's actually very helpful:

Social businesses are not currently recognized as a legal category of business, which means that you will likely be stuck with running a for-profit company that makes a social goal the core of its operations. However, social businesses operating under this structure risk lawsuits, as some countries have laws that require for-profit businesses to maximize profits for their owners. 

While this risk can be reduced by having your investors sign a document certifying that they cannot receive profit beyond their original investment, the approach is still not ideal. Then again, this solution is better than using the structure of a nonprofit, since such organizations are heavily scrutinized due to the tax exemptions they enjoy.

### 6. Capitalism has meant prosperity for some, but it is incomplete and inequitable without social businesses. 

As World War II was finally coming to a close, global prospects appeared bleak. Poverty and hardship were widespread across the globe. 

As the years passed, capitalism advanced, resulting in positive changes for many — but not for everyone at once. In the decades since the Second World War, many economies have done extremely well, particularly those of Europe and North America, significantly increasing the wealth and quality of life of hundreds of millions of people. But still more people were left impoverished in the wake of this boon. 

So, in attempting to address the gap between the rich and poor, the United Nations set the Millennium Development Goals, which included the aim of cutting the global poverty rate in half. What followed were great improvements for the poor. 

For instance, in 1991, the poverty rate in Bangladesh was 57 percent, but by 2005 it was as low as 40 percent. While this number is still too high, it continues to drop by about two percent annually. 

But beyond that, it's clear that the current capitalist system is failing. Just take the aftermath of the 2008 financial crisis, in which a food crisis struck, leaving millions of people starving. The prices for rice, corn and wheat had gotten too high for poor people to afford. Such crises are a direct result of a world system that doesn't take into account the needs of all people, but instead only the lucky, wealthy few. 

And that's why social businesses are so essential. Social businesses don't disregard the capitalist system, but add the important aspect of doing social good to the work of sustaining a business. Just take the social business Grameen Veolia Water, or GVW, a joint venture of Grameen and Veolia Water. 

GVW strives to increase and improve access to clean drinking water in rural parts of Bangladesh. For instance, in the village of Goalmari, they provide clean and affordable water to approximately 20,000 people!

### 7. Final summary 

The key message in this book:

**Social businesses are a distinct type of company that make the world a more equitable place. These businesses place a social, economic or environmental goal at the core of their operations by letting go of the demand to turn a profit for investors.**

Actionable advice:

**Ask around to find a problem to solve:**

If you're having a hard time deciding on an idea for your social business, just try bouncing ideas off people. Keep asking yourself who you think needs help and remember that it could be anyone from the disabled to children or the elderly. Simply by talking with people about the problems they encounter, you're sure to find something deserving of your attention. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Clay Water Brick_** **by Jessica Jackley**

_Clay, Water, Brick_ explores the author's unusual business career in connection with stories of successful micro-entrepreneurs all over the globe. These blinks reveal the strategies of entrepreneurs who make something out of nothing while making a difference in struggling communities.
---

### Muhammad Yunus

Muhammad Yunus was born and educated in Bangladesh before becoming a professor of economics at Chittagong University. He is the founder and director of Grameen Bank in Bangladesh.

