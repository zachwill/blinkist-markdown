---
id: 553f5ee16136310007240000
slug: building-your-business-the-right-brain-way-en
published_date: 2015-04-28T00:00:00.000+00:00
author: Jennifer Lee
title: Building Your Business the Right-Brain Way
subtitle: Sustainable Success for the Creative Entrepreneur
main_color: 7FDFBB
text_color: 447865
---

# Building Your Business the Right-Brain Way

_Sustainable Success for the Creative Entrepreneur_

**Jennifer Lee**

_Building Your Business the Right-Brain Way_ (2014) offers a practical translation of the basics of entrepreneurship into a language creatives can easily understand, through focusing on the strengths of the right hemisphere of the brain, such as as creativity, emotion and visualization.

---
### 1. What’s in it for me? Don’t let your left-brain dominate your business; harness the power of your right-brain too! 

Are you more left-brained or right-brained? That is, do you look at problems analytically and rationally, or instead, creatively and emotionally?

Regardless whether you trend right or left, the key is not to let either side of your brain completely own the other. Even in business, where left-brain thinkers tend to rule, you can't possibly hope to succeed without a good dose of intuition, creativity and empathy — things the right side of your brain controls.

These blinks discuss not only how you can ensure your "right-brain" has its say in your business, but also how creative people can tackle left-brained tasks — so you too can successfully juggle mundane administrative work while thinking creatively about your next business project!

In these blinks, you'll find out

  * why customers are like honeybees and you are the honey;

  * how to know when to offload administrative tasks; and

  * why you should think of your products as gifts.

### 2. Use both sides of your brain for a better business. 

Countless business books stress the importance of strategic thinking, analytical skills and detailed planning. Sounds sensible, right?

Well, entrepreneurship isn't strictly about just being sensible or rational. There's no reason for you to only focus on your left-brain qualities when creativity and other right-brain qualities are _just as vital._

Just as an artist crafts his masterpiece, the entrepreneur crafts his business model. Building a new solution or new product takes a ton of imaginative thinking, and the process isn't linear.

A truly creative process is one that winds along a path of trial and error, of sudden inspiration and "eureka" moments.

Think about how a painting takes shape. At first, there's just a vision in the artist's head. That idea is sketched, and the canvas is filled with paint and life. Maybe one color doesn't fit, and it's painted over. The forms and shapes that appear trigger new inspirations. In the end, the final product is almost always different than the artist's initial vision.

That's exactly what happens when you start tinkering with your business or start a new one. Indeed, some of the most important business skills are located in the right hemisphere of your brain! 

It's widely accepted in neurological studies that that traits like intuition, non-linear thinking, creative problem solving, empathy and imagination mainly take place in the right hemisphere of your brain, while the left hemisphere is more responsible for structural and logical thinking.

While accounting, scheduling and strategic planning is obviously of value in business, right-brain skills are important too. Just think about marketing messages and how they are based often on making an emotional connection with a potential customer — certainly the strength of the right-brained talent!

### 3. Let your passions guide the way you attract customers. 

Let your brain's right hemisphere run wild for a bit, and imagine your business as a flower, one that is blooming and filled with nectar to entice honeybees, or specifically, potential customers. 

A flower's blossom sprouts from _within_, and that's exactly how you should see your business: a great business idea begins with your inner passion.

When artist Melissa Gazzaneo travelled to Bali to help her mother teach English to children, she discovered that children there loved her handmade arts and crafts. As Melissa grew more and more passionate about the idea of helping children pursue their creativity, she realized she could build a business around her idea.

And that's exactly what she did, turning her passion into a business that made bags from recycled gear while at the same time, raising funds to deliver art supplies to disadvantaged children around the world.

Now that you know the source from which your business should bloom, how do you attract honeybees? Easy: by communicating your passion with a _core message_.

This should be nothing more than an authentic expression of who you are and what you stand for. Melissa's core message, "Repurposed for a Purpose," says loud and clear that providing aid is her key motivation in leading her business.

But what good is a core message if nobody hears it? Consultant and facilitator Laura Burns took this to the extreme by tattooing her core message on her chest, "Married to Amazement."

Granted, you needn't take your commitment this far — think about your own way of communicating your core message to customers clearly and in a way that's as unique as is your product!

But if you're concerned that your business isn't reaching potential customers worldwide, don't worry. It's not important to attract all the honeybees in the world, just the ones with whom your core message really resonates. These are after all the customers who are more likely to understand the value of your product.

> _"The more you can bring your full, authentic self into your work, the more you will attract the right people."_

### 4. Connect directly with your customers and tailor your communication to them specifically. 

Unlike flowers, business owners don't have to hang around waiting for honeybees — we can go out and find them! The more people you connect with, the wider your pool of supporters will be.

In your network of supporters, everyone has value: some may become your customers, some may help encourage business decisions and others may assist in spreading your message.

So how do you make these connections?

Personal and direct communication is what gets people listening. Imagine that selling your product to a customer is like convincing your sweetheart to take you out for a date. Write every email newsletter like a love letter, complete with a touching headline. 

Parent-coach Sheila Pai starts her promotion emails with the introduction, "Dear Hardworking Parent, Parenting is hard, hard, HARD work." In doing so, she shows empathy and understanding for her customers' feelings and needs.

Talking to people and spreading your message is of course great. But you can increase your impact when you address exactly the right people with the right information at the right moment.

Imagine you're launching a new coaching program. You can start by building excitement with tantalizing teasers, perhaps recording a behind-the-scenes video that reveals preparations for the course.

When your launch date approaches, widely open the doors and invite your audience to take action. You could publish a Q&A covering the most important information in your course, dish-out event invitations and make sign-up forms available online.

After your first session, you can use weekly reminders and gentle nudges to keep your offer in the heads of those interested and to attract late-comers. This way you'll be drawing people in at every stage.

### 5. Switch from selling to serving by crafting your product like the gift it really is. 

Remember the last surprise gift you received? How good did it make you feel? A gift comes from the heart, and sharing a gift is way more fun and gratifying than simply selling something.

The author once had a college friend with whom she shared a passion for getting each other just the right gift. Whenever they saw a perfect goodie, they grabbed it and surprised each other just because it brought them joy.

That's the kind of mind-set which helps you empathize with your customers and anticipate their wishes and needs.

Life coach Kerri Richardson for a long time had an awkward feeling that she was "begging for money" when it came to her work. A shift in her mind-set from selling to sharing helped her become more customer-oriented. Kerri now sees her training sessions as worthwhile opportunities for people to meet like-minded people and to get support to realize their dreams.

Just think: when you give a gift, you're considering basically the same questions as when you design a product. Who is the recipient? Why is this a good gift for this person especially, and how will she benefit from it? Is the price appropriate? And, how can you wrap the gift so it looks good?

Just like a present is more fun wrapped in brightly colored paper and ribbons, so too will your product benefit from you paying attention to the little details.

For example, when selling a handmade leather bag, you could gift a unique accessory, or even offer a free subscription to your online leatherwork courses.

### 6. Use your creativity to diversify your income streams. 

Most entrepreneurs are driven by their passions. But chances are that they're also in business to earn a living — in other words, to make money.

Service-oriented business models, such as coaching, swap time for money in a direct way. The same goes for artisanal work, where you sell an individual product that you make with your own labor. Such models eventually reach a financial ceiling.

In these time-for-money models, there are three paths to making a profit: attract new customers, sell more to existing customers or raise your prices.

As a coach, you might offer a face-to-face hour session and receive payment afterwards. However, the number of sessions you provide depends on how much time you have. You can't serve an endless stream of customers, for example, because you don't want to overwork yourself!

On the other hand, there's also a limit to the amount of money people are willing to pay. What should you do? Think creatively!

You could turn individual coaching sessions into group sessions, for example. This would allow you to increase your number of customers, earn more income and even allow you to eventually lower your prices to attract even more customers.

Jane, a holistic health consultant, offers a good example of creative problem-solving. In ten hours, she was able to create a six-module home study course that she delivers online. From this point forward, she is able to earn money with almost no additional hours or labor.

But what if you offer products and not services? As a leather artisan, you could begin to offer services, perhaps in the form of group lessons in leather work or by distributing patterns or tutorials.

> _"Let's cut straight to the chase — you're in business to make money, too, right?"_

### 7. Focus on the tasks you love, and get support in handling the rest. 

Even if you're passionate about sharing your gifts with the world, there still might be something stopping you.

Any guess what that might be?

If you're put off by the idea of administrative work that is time-consuming but still "needs to be done," don't despair. Nobody expects you to shoulder all the work alone — so why not simply ask for help?

In your dream business, you might have the urge to control everything yourself. But in the long term, you'll burn out and lose your creative energy.

Before this happens, try to determine which tasks you should outsource. Ask yourself the following questions: What are you _not_ good at? What takes too much time? What comes easy but bores you? What drives you crazy?

For a long time, fiction-writing coach Beth needed to do everything on her own, until the point when she found herself overwhelmed with work. Beth then bit the bullet and hired an assistant to manage her online tasks. This allowed Beth to focus again on the work she truly cared about.

There are several options for you in recruiting support. When it comes to administrative tasks, bookkeeping or web management, you can hire outside specialists for fair payment.

Or instead of hiring someone, you could team up and create a strategic partnership with any of the countless creative entrepreneurs who are caught up in similar situations.

Life coach Tiffany Han, for example, collaborated with 12 artists. Together they worked as a team to create a product package, combining art prints with a coaching exercise. In doing so, they not only developed an attractive product but also minimized their marketing workload, as they were able to draw on a much bigger network.

### 8. Planning doesn’t have to be boring! Think long term to see patterns and prepare for the unexpected. 

Your head is probably bursting with ideas for new courses, innovative creative solutions or unique art pieces. It's no wonder that you lose focus from time to time! But if you find yourself off-track more often than you'd like, consider doing some _long-term planning_.

This doesn't mean you need to bury yourself in Excel spreadsheets. You can just as easily get your hands dirty and sketch out your work horizon and income plan using a huge wall calendar and colorful markers.

For example, the author usually launches one new seminar program every quarter. For each launch, she notes down the main tasks, deadlines and expected earnings on her wall calendar, so she can keep everything literally in sight.

Writer and web designer Mari Pfeiffer uses mind maps to visually document the important tasks in her business processes. With the actual task placed in the middle of the map, the branches around it explore how often the task occurs, what processes it requires, who needs to be involved and which questions typically arise.

Consistently mapping out your work this way can also give you insight into patterns. You may see that a lot of tasks recur periodically; these are the ones you can plan to execute with less effort next time. Documentation of previous work can even be used as templates for upcoming projects.

The author once was unexpectedly invited to present an offer for a workshop with a popular live broadcast show. The task seemed impossible, until she remembered to look at her old sketchbooks, and was able to craft a proposal using bits from earlier work. Her proposal was a hit and it earned her extra cash and customers with hardly any effort.

No matter which approach you take, give it your own creative twist and adapt your routines as you see fit — so you can keep both sides of your brain happy!

### 9. Sustain your success by caring for yourself. 

Know the saying, "When it rains, it pours?"

The author sure does. Her decision to move to a new home was put on hold when her boyfriend had a bike accident _and_ her dad took a bad fall and spent weeks in the hospital.

Your left-brain tendencies may aspire to control everything, but it's simply not possible.

The right-brain alternative is to let go of the urge for control and face life with ease. This way, you'll be able to sustain the business that you worked so hard to establish.

If doing things "with ease" sounds vague to you, don't worry — here are three helpful strategies to put you in the right frame of mind.

The first strategy is to seek out _simple solutions._ The author herself would often get stuck in her own complex plans, and achieve nothing. Finally when she stopped overthinking and simply jotted down some notes for a book, she found herself on the road to two major projects!

The second strategy is to _take time off_. It's as straightforward as blocking off some space in your schedule for relaxing, perhaps in the form of work-free evenings.

And the third strategy is to create a work environment that _cares for you_. Start by sticking small positive notes on your desk to give you strength when times get tough, and then create your own specific routines that make you work well and feel well, too.

Yoga teacher and life coach Sharon Tessandori places self-care at the top of her business agenda. When swamped with emails and admin work, she takes a seat on her yoga mattress first and centers herself before taking it all on.

Like Sharon, remember that you are the most important person in your business. So why not make things work for you?

### 10. Final summary 

The key message in this book:

**Creative thinkers who want to turn their passion into a business work best when they use the strengths of the right hemisphere of their brain. By turning an entrepreneurial idea into something that is visual, playful and deeply human, you'll be able to sustain the imaginative spark that drew you into the world of business in the first place.**

Actionable advice:

**Paint your plans with the right markers!**

The next time you want to sketch out a plan for your new product launch or campaign, use bold and colorful markers. A marker's thick lines make it hard to get obsessed with small details, and will keep you focused on broader concepts. Save your ballpoint pens for filling in the details afterward! 

**Suggested** **further** **reading:** ** _Everybody Writes_** **by Ann Handley**

_Everybody Writes_ (2014) gives you invaluable advice on how to create great content, from using correct grammar to crafting engaging posts, tweets and emails. With just a handful of simple rules, these blinks will help you gain a better understanding of how to use the right words to keep customers coming back for more.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jennifer Lee

Jennifer Lee wrote the bestselling guidebook, _The Right-Brain Business Plan_. Previously a consultant for Fortune 500 companies, she has since founded a coaching institute to guide artists and creatives to realize their full potential.

