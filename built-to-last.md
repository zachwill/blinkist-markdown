---
id: 510bf0ebe4b07f2ad1aa89f5
slug: built-to-last-en
published_date: 2013-01-13T00:00:00.000+00:00
author: Jim Collins
title: Built to Last
subtitle: Successful Habits of Visionary Companies
main_color: F03B3D
text_color: BD2E30
---

# Built to Last

_Successful Habits of Visionary Companies_

**Jim Collins**

_Built to Last_ (1994) examines 18 extraordinary and venerable companies to discover what has made them prosper for decades, in some cases for nearly two centuries. This groundbreaking study reveals the simple but inspiring differences that set these visionary companies apart from their less successful competitors.

_Built to Last_ is meant for every level of every organization, from CEOs to regular employees, and from Fortune 500 companies to start-ups and charitable foundations. The timeless advice uncovered in this book will help readers discover the importance of adhering to a core ideology while relentlessly stimulating progress.

---
### 1. Visionary companies can teach us through their enduring success. 

So-called _visionary companies_ have a lot to teach us. They are companies with long track-records for success and widely admired as the crown jewels of their industries. What's more, their success is enduring — they prosper even as great leaders retire and individual hit-products become obsolete.

To properly study and learn from these companies, the authors first had to identify them by surveying hundreds of prominent CEOs for the names of companies they considered visionary. The 18 most commonly mentioned firms — including such venerable names as the Walt Disney Company, Marriott Hotels, and Merck — were included in the study. The visionary companies were then paired up with _comparison companies_ : firms that shared similar products and markets but which, while not being outright poor performers, were called "visionary" far less often in the CEO survey.

Both groups of companies were then examined across their considerable life spans (the average founding date lay in the 1890s for both groups). Based on massive amounts of data from interviews, annual reports, financial statements, news articles and many other sources, all aspects of these corporations were studied, ranging from their ownership structures to their cultures.

To understand the extraordinary success of the visionary companies, consider this fact: if you had invested a dollar in their shares in 1926, that dollar would have been worth $6,356 by 1990. Compare that to $955 if you had invested in the comparison companies, and only $415 if you had invested in the general market, and you'll see just how impressive the visionary companies' performance is.

No wonder that all manner of Fortune 500 companies have been fascinated by the findings of this study.

**Visionary companies can teach us through their enduring success.**

### 2. Visionary companies are like machines that constantly produce great products and leaders. 

Contrary to what most people believe, the success of a visionary company is not dependent on great ideas.

The founder of Sony, for example, had no specific idea of what products his company would make. He actually held a brainstorming session _after_ founding the company to evaluate business ideas ranging from sweetened bean-paste to miniature-golf equipment.

Bill Hewlett and Dave Packard also had no specific idea in mind when they founded Hewlett-Packard (HP). They experimented with almost farcically diverse ideas, such as automatic urinal flushers and bowling foul-line indicators.

Hence, it seems that great ideas are not necessary for the start of a visionary company.

Nor are high-profile, charismatic leaders. While visionary companies did have superb individuals at the top of their organization, they were often down-to-earth, reserved and modest people.

But then, what is the secret of enduring success? Many comparison companies had great ideas and strong leadership, yet they all fell behind the visionary companies eventually. Why?

Instead of focusing on a single product or a single leader, the visionary companies studied built themselves into outstanding organizations that constantly churned out great ideas and great leaders. The real creation of the founders was not a product at all but the company itself; constantly advancing independently of any one person or idea.

Think of a clock on the wall. Having one great idea or visionary leader is like getting a glimpse of that clock and being able to tell the time in that instant. But building an organization that constantly generates great ideas and leaders is like building your own clock: a reliable machine.

**Visionary companies are like machines that constantly produce great products and leaders.**

### 3. Visionary companies are driven more by a core ideology than profits, but they still prosper. 

Visionary companies have a higher _purpose_ for their existence than to merely chase profits. Together with the companies' _core values_ — enduring tenets that guide their every decision — this purpose forms their _core ideologies_ : a set of stable principles that guides the company through generations, much like the "truths" of the American Declaration of Independence.

Consider for example the pharmaceutical company Johnson & Johnson (J&J). In 1935, the CEO, Robert W. Johnson Jr., wrote out the company's core ideology in a document called "Our Credo," which listed the company's responsibilities: first to their customers, second to their employees and so forth. Finally, fifth and last on the list, after all the other responsibilities had been fulfilled, Johnson said that shareholders should receive "a fair return."

Likewise, most visionary companies studied were not primarily after profit. Nevertheless, while some ideologies may seem soft or idealistic, visionary companies managed to find a way to stay pragmatic in their business decisions and make profits without ever wavering from their core ideologies.

A core ideology is important not only when visionary companies prosper but also when they hit upon trouble. For example, when Ford faced a dire crisis in the 1980s, instead of just fighting fires, its management team stopped to discuss and clarify what the company stood for and how they could espouse the values of the founder, Henry Ford. Ford's comparison company, General Motors, made no such effort.

Though every visionary company studied had a core ideology, their content varied greatly. What counts is not the content of the ideology but rather that an authentic ideology exists and is rigorously acted upon. 

**Visionary companies are driven more by a core ideology than profits, but they still prosper.**

### 4. Visionary companies preserve their core ideologies while relentlessly stimulating progress and improvement. 

The real heart of what makes visionary companies so successful is that while they jealously guard the permanence of their core ideologies, the manifestations of that core ideology are always open for change and progress. For example, Wal-Mart's drive to "exceed customer expectations" is a stable element of its core ideology, but the customer-greeters at the entrance to their stores are a practice that can change. Similarly, Boeing's core ideology is to be a pioneer in the field of aviation, but building jumbo jets is a manifestation of that ideology that can change.

This flexibility demonstrates how visionary companies refuse to abide by the so-called _tyranny of the OR_, whereby a company must choose between staying true to its core ideology _or_ stimulating progress. Instead, visionary companies use the _genius of the AND_ — experimenting _and_ developing — while still adhering to their core ideologies.

Visionary companies have their core ideologies to guide them, but they are also relentless in their efforts to continually improve their products, business and organization. They never settle and never become complacent. Consider the founder of the Marriott Corporation, J. Willard Marriot, who lived by the motto "Keep on being constructive, doing constructive things, until it's time to die… make every day count, to the very end." This sounds rather depressing, but is also a great commitment to constant progress.

Just like their core ideologies, this drive for progress is innate and unquestioned in visionary companies. Progress is stimulated both by setting bold goals and by creating concrete mechanisms that encourage people to innovate and improve.

**Visionary companies preserve their core ideology while relentlessly stimulating progress and improvement.**

### 5. Visionary companies use big hairy audacious goals to stimulate progress. 

To drive progress, visionary companies often set themselves extremely bold objectives — so-called _big hairy audacious goals_ (BHAGs) — to which they commit utterly and completely. BHAGs are so ambitious that they often seem unrealistic, especially to outsiders. Nevertheless, they're also clear and tangible enough to energize and focus the organization.

A well-known example of a non-corporate BHAG is the one set by John F. Kennedy in 1961 when he proclaimed that the U.S. would take a man to the moon and back safely by the end of the decade. At the time, this was an almost ludicrously bold commitment, but it did get the U.S. moving vigorously forward.

Boeing set many BHAGs during its history, including its commitment to developing the 747 jet. Boeing pursued this goal single-mindedly, without ever even considering the possibility of failure. The CEO stated that they would complete the jet even if it consumed the entire company, which it nearly did: at one stage roughly 86,000 people — some 60% of their workforce — were laid off as sales of the plane did not meet expectations.

Similarly, Thomas J. Watson Sr., the founder of the Computer Tabulating Recording Company, set a BHAG by renaming his company — which sold coffee grinders and butcher scales — to reflect his ambition for global status. The new name was audacious at the time: International Business Machines (IBM).

BHAGs often take on lives of their own. Just as the space program continued after Kennedy's death, the visionary companies studied pursued their BHAGs even as new CEOs and directors came and went. Once a BHAG was achieved, new ones were set — always in line with the company's core ideology.

**Visionary companies use big hairy audacious goals to stimulate progress.**

### 6. Visionary organizations are almost cult-like – new recruits either thrive or leave. 

Visionary organizations pursue their core ideologies so single-mindedly that their corporate cultures are almost cult-like. For example, new employees quickly find themselves socializing primarily with their colleagues, and they are encouraged to be secretive about the inner workings of their companies.

Employees often become completely immersed in the core ideology. Consider IBM, for example, where future managers in training would rise and sing songs from an IBM songbook:

"March on with I.B.M.,

Work hand in hand…"

Similarly, the Walt Disney Company expected its employees to live and breathe its core ideology of wholesome family fun. For example, men with facial hair were not accepted as employees at theme parks, and anyone heard uttering a four-letter word in the presence of Walt Disney himself was fired immediately — no exceptions.

There is not much room in visionary companies for people who do not meet their tough expectations and standards. New employees often find that either they fit right in and thrive, or they perform poorly, are unhappy and exit the company quickly. In this regard, there are no compromises at visionary companies.

Conversely, because the employees are confident and can be counted on to adhere to the company's core ideology, they can also be given the leeway to experiment. This stimulates progress and enables the company to avoid the dangerous _group-think_ endemic in many cults.

Note though that visionary companies are not personality cults, centered around a charismatic CEO or founder but rather around the core ideology of the company. Though charismatic personalities can also drive passionate work, such "cults" invariably collapse when the person leaves.

**Visionary organizations are almost cult-like — new recruits either thrive or leave.**

### 7. Visionary companies produce a continual stream of high-caliber leaders. 

While the visionary companies studied often had outstanding CEOs at their helm at one time or another, what was even more impressive was their ability to continually produce such high-quality leaders.

The organizations focused hard on cultivating managerial talent within the company so that new leaders could be counted on to continue in line with the company's core ideology. At the same time, visionary companies engaged in timely succession-planning to ensure continuity in leadership even if something unexpected were to happen.

Consider for example the General Electric Company (GE), whose most famous CEO is without a doubt, the legendary Jack Welch. But actually thanks to the company's fervent emphasis on internal management training and CEO succession, GE has enjoyed _a century_ of Welch-caliber CEOs. In fact, more GE alumni have gone on to become executives of American corporations than the alumni of any other company. And Welch himself outlined his plan for succession _seven years_ before retiring, though even this seems last-minute compared to Bob Galvin, the former CEO of Motorola, who began planning for the next generation a _quarter-century_ before finally leaving.

In contrast, the comparison companies often hired external CEOs who were unfamiliar with the company and who sometimes began steering it in new, wholly ill-conceived directions. Also, the CEOs at comparison companies were often near-tyrannical and engaged in very little succession planning, which left gaping holes in the companies' leadership when they left. Some comparison companies even had CEOs who actively _hindered_ succession planning and sabotaged would-be candidates. These companies then stumbled when the troublesome CEO finally left.

**Visionary companies produce a continual stream of high-caliber leaders.**

### 8. Visionary companies stimulate evolutionary progress by encouraging experimentation. 

Charles Darwin discovered that evolution is a series of successful "experiments" in which slight variations are introduced to a species and the strongest new variants survive. Similarly, the visionary companies studied understood the need to stimulate a similar _evolutionary progress_ within their businesses. They encouraged their employees and management to experiment with new ideas, products and practices, some of which became great successes.

Consider for example J&J's famous Band-Aids. They were born when an employee put together some surgical tape and gauze to quickly bandage his wife's fingers after she accidentally cut herself with a kitchen knife. When he mentioned the idea to the J&J marketing department, they embraced it and eventually, Band-Aid products became the company's best-selling category.

Or consider 3M, which directed its employees to use 15 percent of their working time to work on any pet projects they felt like. Two such projects by two separate employees eventually collided to produce the famous Post-It Notes. This would never have happened if 3M hadn't actively encouraged experimentation and allowed its employees to continue with their pet projects, even when early market studies were negative. Contrast this with 3M's comparison company, Norton, which actually discouraged the pursuit of opportunities outside of its traditional product lines.

One aspect of evolution is that some — or even most — variations fail; the same is true in business. J&J experienced some very prominent failures too, for example, its colored casts for children with bone fractures. The casts quickly turned hospital bed sheets into something resembling modern art and threw hospital laundries into chaos.

Visionary companies understood that failed experiments are the necessary price to pay for evolution and must not be punished lest further experimentation be discouraged.

**Visionary companies stimulate evolutionary progress by encouraging experimentation.**

### 9. Visionary companies don’t just talk – they take concrete actions to implement their values. 

While many companies claim to adhere to idealistic values, encourage experimentation or embrace constant progress, very little is seen in practice. The visionary companies studied, on the other hand, managed to translate their values into reality by creating concrete mechanisms that affected the daily lives and decisions of employees.

3M did not merely say, "We want our employees to be more innovative." Instead, it implemented several mechanisms to encourage this idea, one example was allowing employees to use 15 percent of their time on pet projects and dictating that 30 percent of each division's annual sales must come from products less than four years old.

Likewise, visionary companies did not merely talk about constant improvement; rather, they created mechanisms to ensure it. Wal-Mart, for example, spurred constant growth with so-called "Beat Yesterday" ledgers, which were used to compare each day's sales to those of the year prior. Similarly, Hewlett-Packard instituted a grueling process of ranking its employees annually to stop those who gained a high status from just coasting.

The visionary companies also took concrete actions in the long run. They invested far more than comparison companies in creating new technologies and business practices, training and developing their human capital, as well as in supporting research and development.

For example, when Merck wanted to become a force in medical research, it deliberately modeled its labs on academic ones and allowed its researchers to publish their findings in academic journals — very unusual for private companies at the time. It also decided the product-development process should be driven by research rather than marketing as it was in many other companies. This attracted top scientists to Merck's labs.

**Visionary companies don't just talk — they take concrete actions to implement their values.**

### 10. Final summary 

The key message in this book:

**Visionary companies are able to attain their phenomenal success by staying true to their core ideologies while still relentlessly pursuing progress. A company's core ideology comprises its core values but also its purpose, meaning the reason it exists beyond profits or shareholder value. To supplement their core ideology, visionary companies also stimulate constant progress by setting bold goals and by instituting grassroots mechanisms to realize their policies.**

The questions this book answered:

**Why should we study visionary companies?**

  * Visionary companies can teach us through their enduring success.

**What are the main principles that visionary companies use to thrive?**

  * Visionary companies are like machines that constantly produce great products and leaders.

  * Visionary companies are driven more by a core ideology than profits, but they still prosper.

  * Visionary companies preserve their core ideology while relentlessly stimulating progress and improvement.

**What are the specific policies and mechanisms that visionary companies use to preserve their core ideology and yet stimulate progress?**

  * Visionary companies use big hairy audacious goals to stimulate progress.

  * Visionary organizations are almost cult-like — new recruits either thrive or leave.

  * Visionary companies produce a continual stream of high-caliber leaders.

  * Visionary companies stimulate evolutionary progress by encouraging experimentation.

  * Visionary companies don't just talk — they take concrete actions to implement their values.
---

### Jim Collins

Jim Collins is an American author, lecturer and consultant, who, among other things, has taught at the Stanford University Graduate School of Business and is a frequent contributor to _Fortune_, _Business Week_ and _Harvard Business Review_. His other book, _Good to Great_, has sold over four million copies.

Jerry I. Porras is an academic and business analyst. He is the Lane Professor Emeritus of Organizational Behavior and Change at the Stanford University Graduate School of Business. His primary interest lies in finding methods for aligning companies with their core purpose and values.

