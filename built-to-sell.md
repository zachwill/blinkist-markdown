---
id: 541c23f26431620008050000
slug: built-to-sell-en
published_date: 2014-09-22T00:00:00.000+00:00
author: John Warrillow
title: Built to Sell
subtitle: Creating a Business That Can Thrive Without You
main_color: 2C96DB
text_color: 2273A8
---

# Built to Sell

_Creating a Business That Can Thrive Without You_

**John Warrillow**

_Built to Sell_ details key strategies for growing a small service company and preparing the business for a future sale. These blinks illustrate these insights by telling the story of Alex Stapleton, owner of a marketing agency, and his advice-giving friend Ted Gordon, who is a successful entrepreneur.

---
### 1. What’s in it for me? Learn how to build a company that’ll be attractive to potential buyers. 

If you're an entrepreneur with a small service business, you might dream of selling your company some day. So what can you do to structure your business to prepare it for a successful acquisition that gets you the best possible price? These blinks answer these questions through the story of Alex Stapleton, who wants to sell his marketing agency. Alex built up a small company with only a few employees, but wants to have more time with his family and is quite stressed by his work. Ted Gordon, an old friend and entrepreneur who has sold businesses successfully in the past, guides him through the process.

In addition, you'll learn

  * why you should always have more than one salesperson;

  * what it takes to get your management team to stay with your company for a long time; and

  * how to find a broker that'll get you the best possible deal on your company.

### 2. Business success lies in specialization in a single service. 

Let's say you're an entrepreneur who's just launched a service business, like a design agency. How can you make it successful? Should you concentrate on one service or take any business you can get — even offers requiring services that aren't your speciality?

Some people would accept every offer. But that's not the right strategy for long-term success.

Here's why: A focus on one service allows clients to see the company's core strengths. As a result, people will come to you with specific problems that you can solve better than anyone else.

When clients can't go to other firms to receive the same quality of service, you're positioned to negotiate higher prices.

Furthermore, when the results are good, clients will recommend you. Word of mouth will earn you new business.

Here's an example: Alex's business used to fulfill a broad range of requests — designing brochures, copywriting and search engine optimization. Taking a friend's advice, Alex decided to concentrate on designing logos. He excelled at the work and his clients loved the product. As a result, Alex became a top logo designer.

There's another benefit to specializing: It allows you to hire the best people for the job.

Why? Firms that provide multiple services have to hire experts for different fields. Smaller firms can't afford to hire as many experts. Instead, they hire generalists to execute different tasks.

However, since the employees don't concentrate on a single field, they can't deliver the best possible product. The quality never matches what's produced by other firms, which can employ experts.

Luckily, there's a solution for smaller companies: by specializing, they can hire a few experts in a single area.

Specializing helps your company produce excellent work, which puts you in a better position to negotiate with clients.

And, perhaps more importantly, your company will run smoothly without too much oversight from you, because everyone will be responsible for discrete tasks.

> _"Clients will never know you're serious about specialization until you say no to other work."_

### 3. For long-term success, build a business that can run without you. 

When you start a business, it's natural to feel ownership. And when a company's young, the founder is often involved in almost every aspect of the operation. But in order for the business to grow, customers shouldn't associate the company entirely with its founder.

What happens when you as a founder "become the business" in this way? When starting a company, you bear all the risks, so it's understandable that you'll want to handle many aspects of the operation on your own. Maybe you think that's the best way to ensure that your employees don't make mistakes.

So you meet with every client and oversee all financial negotiations. As a result, your clients get used to talking about everything with you in person. For them, you represent the whole company.

But this approach will lead to problems. When the company's roster of clients grows, you won't be able to lead the business effectively.

If you spend all your time with clients, you won't be able to focus on developing strategies for further growth. You probably won't have time to manage employees or concentrate on things like financing.

For example, Alex, our logo designer, struggled with a tight schedule because he met with every client in person. As a result, he didn't have time to sit down with employees and discuss each customer's specific needs. And since employees don't tailor their work for each client, Alex's company lost a lot of time when work had to be edited again and again.

Plus, since Alex had to attend each meeting, it was difficult for him to go on vacation or take time off from work.

As you can see, although it's important for a founder to have an overview of business activities, the day-to-day operations should operate smoothly on their own. In a sense, the company's founder should be replaceable.

### 4. Hire at least two salespeople who know how to sell your specific service. 

It all comes down to the bottom line, as the saying goes. Thus, a solid sales team is _crucial_ to your business. Because having employees dedicated to sales not only generates money, it also frees the founder to actually lead the business.

It's vital to build a sales team comprising people who understand your service because they represent the firm to outsiders.

How do you do that? Hire more than one salesperson.

There are two reasons for this:

First, healthy internal competition motivates employees to do their best work. By competing with each other for commissions, they'll generate more business.

Second, by having multiple salespeople, you're minimizing risk in the event that someone gets sick or leaves town.

Even smaller companies should have at least two good salespeople who understand how to sell their specific service.

Finding such people can be tricky: For example, if you offer a standardized service, you need people who are good at selling products, not services.

Take the case of Alex, who wants to specialize in logo design. This is a service, but he wants to sell it as a product with a standard process — meaning, his company follows the same production steps with each logo. As a result, he needs employees who are good at selling products.

Typically, experienced service salespeople rely on _consultative selling_. That means they ask a lot of questions to find out what the clients needs. Consequently, clients expect them to provide individual solutions for their problems.

But if you want to sell a standardized service, you can't customize the product for each deal.

That's where product salespeople come in: These employees can influence the customer and make him believe that the standard product package is the perfect solution.

Which is precisely what's required for selling a standardized service.

So once you have a smoothly running company that can operate independently of its owner, you can start thinking about how you'll sell the business. How do you go about that? Who do you sell it to? And how do you get the best deal?

### 5. Overreliance on one big client puts your business at risk and makes it unattractive to potential buyers. 

Most entrepreneurs dream of one day selling their companies. But to pull off a sale and get the best deal, it's essential to show potential buyers that you have a growing business with growing profits.

One way to grow quickly is by finding big clients, but be careful about relying on them excessively.

Why? Well, if you're dependent on one big client, you'll run into financial trouble anytime there's an issue with the client's payment. For example, 40 percent of Alex's revenue came from MNY Bank. But one month, they were late on their payment, which made it difficult for Alex to pay monthly salaries and rent.

Here's another reason you shouldn't rely on one big client: It puts you in a bad position to negotiate. From the perspective of a buying firm, that's another major drawback.

Why is this a disadvantage? Well, consider what happened when MNY Bank wanted to have something edited between Friday and Monday. Alex couldn't risk losing a major client, so his designer was forced to work over the weekend. Eventually, the designer quits because of the unbearable working hours.

Or another scenario: When Alex made the decision to specialize in logos, he took a big risk. If MNY Bank didn't want logos, he would lose almost half of his revenue.

As you can see, when your business is dependant on a bigger company, it limits your strategic decisions. This is especially unattractive to buyers, who want to develop a company and use its potential.

No buying firm would want to acquire a small company that's wholly dependant on a bigger company, especially because you'll always have to do what the big company wants.

If you don't let big client companies force you into an overly dependant business relationship, your business will be far more attractive to buyers.

> _"Relying too heavily on one client is risky and will turn off potential buyers."_

### 6. Offering a standardized service will save you time, lead to better cash flow and make your company attractive to buyers. 

In business, the customer is always right — but there are limits. Although every client might want a customized service, your small business will be better off if you offer _standardized services_.

That's because customized services are costly. From the first meeting to product delivery, the whole process can take months. And during that time, you're not paid.

That's what happened to Alex. He had to edit some brochures for MNY Bank repeatedly because the client kept being dissatisfied with the results. It took ages for Alex to deliver the right product. And he wasn't paid until he did. Thus, he struggled to find reserves to pay his monthly expenses.

And there's another benefit to standardized services: They give your business a better _cash flow_. Cash flow is the amount of money a firm has available in a certain period. It's the difference between revenues and expenses, and it helps determine a company's profitability.

With standardized services, you're paid upfront because the client knows what to expect from the final product. And that leads to positive cash flow, because you have money now and not in the future.

Additionally, a positive cash flow is attractive from the perspective of buying firms, because it acts as a buffer if the company encounters larger unexpected expenses.

Besides fostering positive cash flow, standardized services make it easier to estimate how long it'll take to deliver a product, since you aren't dependent on the customer. That way, you can better calculate how to allocate your time and resources. A calculated daily business is very attractive to buyers, because they can see that the company runs smoothly on its own.

Finally, by limiting the number of times you meet with clients to tailor the work to their needs, you're saving your company a lot of time.

### 7. Create incentives for managers to stay in the company even after it’s sold. 

Up for a challenge? As an owner, it's up to you to ensure that managers will want to stay with the company for a long time — even after it's sold.

Why is this so important? The purchasers will want to know that the firm will continue to succeed after its acquisition. And the management team plays a crucial role in that.

In Alex's case, he wanted to leave the company after the sale. That meant his management team had to run the company alone.

Alex had three managers in charge of different departments. One was responsible for sales, one for designers and one for coordinating with the clients. If Alex left, those three managers would be the most important people in his company, because they oversaw all the daily tasks.

If they stayed on, the business would continue to run as usual, so as the owner, Alex had to create incentives to retain them.

One option is to give management equity, in the form of _stock options_. Stock options give managers the right to buy a specific amount of the company's stock and therefore profit from its success.

Stock options are a huge motivational tool because they make employees feel more invested in the company. However, stock options aren't the best idea for smaller firms because they're complicated to set up.

So what's an alternative? Long-term incentive plans that reward performance and loyalty are a better choice.

For example, Ted told his friend Alex that he sets targets for his managers. When they reach their end-of-year targets, he pays them a bonus. He then matches the bonus with a sum he puts aside into a special fund. After three years, the employees are allowed to withdraw one third of the money — and so on, for each year after that. Thus, Ted ensures that management stays at the company.

### 8. The right broker will help you sell the company for the best price by arranging negotiations with different potential buyers. 

Ready to sell your company? Make sure you don't overlook a crucial factor: _the broker_.

The broker is the bridge between you and buyers. If he recommends you to the wrong companies, you won't get a great price.

What should you expect from your broker? The right broker will arrange discussions with different potential buyers. Consequently, the competition will result in a better price.

Another benefit: you get to choose between buyers.

Some companies buy for strategic reasons. That's ideal for you because they're more likely to pay a higher price, as well as give your business resources to grow.

For example, Alex's broker suggested they talk to printing companies, which might want to use his logo design business as a Trojan horse to get new clients — clients who would need printing services for their new logos.

But other companies buy only for financial reasons. They want to see a return on investment, so they only care about quick financial success. These firms consider your price and the return they'll get on it. They know they can buy any other potentially profitable firm, meaning they don't care as much about your uniqueness or strategic outlook.

So, a broker can play a major role in the successful sale of your company. But how do you choose one?

It's best to select a medium-sized broker firm.

If it's too big, your deal might not be so important for them and the broker won't invest much time and effort to find the right buyer.

On the other hand, if the firm is too small, the broker might not be respected by potential buyers. In that case, the negotiations also wouldn't have a satisfying conclusion.

Also, the right broker may have experience in your industry. Thus, he'll know the landscape of competitors, the potentially interested firms and so on.

> _"Brokers come in all shapes and sizes. The ideal is to find a broker for whom you will be a meaningful account."_

### 9. Think big and write a carefully worded plan to present your company favorably to potential buyers. 

It's a big decision to make, but when you're ready to sell your business, you need to start by writing a plan detailing your business model and expected cash flow. Bigger companies will use this to decide whether to acquire you, so it should present your firm in the best light.

How should you do that? First: Think big!

Make a three-year plan detailing what your company could achieve with unlimited resources.

Why does this work? Well, the buying company will be larger than you are. It will have more employees, more resources and more money to run the business.

With this added support, your company could achieve things it couldn't on its own — including faster growth.

This is a crucial point: The buying company wants to grow — _that's why it wants to acquire you_. So tell them what's possible and don't be too modest.

Here's another thing that will present your company favorably to buyers: If your company concentrates on a standardized service, use words like "customers" and "business" rather than "clients" and "firm" when writing your plan.

These small changes will make a big difference for buying firms. The word "clients" is associated with a normal service business and the word "customers" more with standardized businesses.

Here's why that matters: Service businesses are often acquired with a three- to five-year _earn out_. That means only a small amount of money is paid upfront. The rest comes later, if the company meets its goals. If not, the owner receives less money.

The acquisition process for product businesses is different. The owners get the full amount of money upfront, without any earn out. This process involves less risk for the owner.

If you're an owner looking to sell your company, you probably want as much money as possible, now — not in a few years. Improve your chances with a well-written and savvy business plan.

### 10. Final summary 

The key message in this book:

**Building a specialized service business attracts potential buyers. By hiring a robust sales force, growing a diverse roster of clients and creating strong incentives to reward management loyalty, you're building a company that can be sold successfully.**

Actionable advice:

**Standardize your service.**

When you sell a standardized product to someone, the process is straightforward: They pay and you deliver.

That's so much simpler than the back-and-forth that takes place when you're dealing with a customized service, which can sometimes take months to deliver, from first meeting to final product.

Standardizing also ensures that you're paid immediately, which saves you the complicated financial issues involved with having to remain afloat until the client is satisfied enough with the product to deliver a payment. With cash coming in upfront, you'll be able to keep your finances organized.

**Suggested** **further** **reading:** ** _The_** **_E-Myth_** **_Revisited_** **by Michael** **E.** **Gerber**

In a revised and updated version of his famous book _The_ _E-Myth_, author Michael Gerber cuts through various myths about what's involved in starting a small business and how to make a business successful. Walking you through every stage of how to build a business, _The_ _E-Myth_ _Revisited_ highlights the important difference between working _in_ your business and working _on_ your business.
---

### John Warrillow

John Warrilow has launched and sold four companies on his own. He also devised The Sellability Score, a tool which evaluates how attractive companies are to potential buyers. _Fortune_ and _Inc._ magazines rated _Built to Sell_ one of the best business titles of 2011.

