---
id: 559bbd4f3966610007010000
slug: bulletproof-diet-en
published_date: 2015-07-07T00:00:00.000+00:00
author: Dave Asprey
title: Bulletproof Diet
subtitle: Lose up to a Pound a Day, Reclaim Energy and Focus, Upgrade Your Life
main_color: E95C2F
text_color: 9C3E1F
---

# Bulletproof Diet

_Lose up to a Pound a Day, Reclaim Energy and Focus, Upgrade Your Life_

**Dave Asprey**

_The Bulletproof Diet_ (2014) shows you how to hack your body to optimize your health, lose weight and increase your mental functioning. How? Maintain a diet filled with the right proteins and fats, and learn to exercise and sleep in the most effective way possible.

---
### 1. What’s in it for me? Discover the secrets behind the world’s best diet. 

Put butter in your coffee.

Seriously.

That's one of the major discoveries in Dave Asprey's _Bulletproof Diet_ — discoveries that have made it one of the most talked-about healthy eating breakthroughs today.

The simplest way to explain the Bulletproof Diet is that it's high in fat and vegetables, moderate in proteins and carbohydrates, and low in toxins and starch. But becoming bulletproof is more than that. It's about understanding how your body works and how you can maximize its potential. It's about learning why you _should_ eat fats — as long as they're the right ones — and why fasting can be OK for your body if you do it right.

These blinks will explain all that and much more. After reading them, you'll know why people are really putting butter in their coffee, and why you should too.

In these blinks, you'll learn

  * which coffees have dangerous mold on them;

  * why grass-fed meat is superior; and

  * how eating more fat can make you think more quickly.

### 2. Most coffee contains mold toxins, which are harmful to your health. 

A lot of people enjoy a cup of coffee before they run off to work in the morning, but they probably aren't aware that coffee contains _antinutrients_ : compounds that prevent your body from absorbing nutrients. They're found in seeds, plants and beans and can be quite detrimental to your health.

Most people are exposed to low doses of these antinutrients, like mold toxins, daily. They are in nearly everything we eat but are difficult to detect and invisible to the naked eye.

Coffee beans naturally carry these mold toxins. In fact, one study of coffee beans grown in Brazil found that over 90 percent of the beans were contaminated with mold before they were processed. A further study found that nearly 50 percent of brewed coffees contain mold.

Mold has become such a big problem in the coffee business that several governments, such as South Korea and Japan, have implemented regulations that ban coffee with a high amount of toxins. The United States and Canada, however, haven't set any such limits so it's likely that coffee sold in those countries carries a greater degree of mold.

High levels of mold toxins can result in many serious health problems, including cardiomyopathy, cancer, hypertension, kidney disease and even brain damage.

So pay careful attention to the coffee you buy. Cheaper coffee isn't just made from lower quality beans — it's also more likely to contain harmful toxins.

And if you think drinking decaf will protect you, think again! It actually contains more mold than caffeinated coffee because caffeine naturally acts as an antifungal defense mechanism. It deters mold and other organisms from growing on beans.

To avoid mold-exposure, it's best to buy your coffee from shops that use their own roasters, if possible. You should also try to buy single-origin coffee instead of blended coffee.

### 3. Drink your coffee with a bit of butter. 

You don't have to give up coffee entirely, however. If you drink it the right way, it'll help make you _bulletproof_!

Caffeine actually has many health benefits: it blocks inflammation to the brain and regulates your insulin sensitivity, which helps in weight loss. What's more, you can intensify these benefits when you drink your morning cup of coffee by adding in some unsalted butter from grass-fed cows.

Why would you do that?

You get 3.4 times more antioxidants out of your coffee when you switch out your milk for butter. This is because milk has a negative effect on polyphenols, the good antioxidant found in coffee — it makes them less likely to be absorbed.

Butter also contains butyric acid, which lowers inflammation and heals your gut.

Still not convinced?

A study on rats found that coffee combined with a high-fat diet led to decreases in body weight, fat and liver triglycerides.

Another reason to add fat to your coffee is that it helps you reach _ketosis_, a metabolic state where the body burns fat for energy instead of sugar.

Some people try to reach ketosis by avoiding carbs, but another trick you can use is to add _C8 MCT oil_, or medium-chain triglyceride oil (MCT oil), to your coffee. It's made from coconut or palm oil and has 18 times more medium-chain triglycerides than regular coconut oil.

Adding butter or MCT oil or even both to your coffee will help your body burn more fat as an energy source.

The author studied this by experimenting with his own diet. He tested himself the day after eating a sushi dinner with two cups of rice and found that his blood ketone level was 0.1. A ketone level of 0.6 indicates ketosis.

After drinking a cup of _bulletproof coffee_, however, his blood ketone level reached 0.7 within 30 minutes! People on low carb diets have to restrict their carb-intake for three days before they can reach the same level.

MCT oil is so strong that it allows you to reach ketosis even after consuming carbs. Be careful though! Increase your MCT oil intake slowly, or you'll risk making yourself sick.

### 4. Harmful bacteria in your gut can cause insulin resistance and inflammation. 

You can also become bulletproof by encouraging the growth of certain bacteria in your digestive tract. The microbes in your intestine — your _gut biome_ — play an important role in your health and your diet has a big impact on them.

Studies of mice have provided a great deal of insight into the ways our bodies gain and lose weight. Your weight isn't just determined by the calories you consume or burn — it's also affected by the bacteria in your gut.

If bacteria from the guts of obese mice is inserted into thin mice, they overeat by ten percent and become more resistant to insulin. And like mice, heavy and thin humans also have very different gut bacteria.

People who are naturally thin have more bacteria from the _bacteroidetes phylum_, also called _thin people bacteria_.

You can generate more of it by eating foods that contain _polyphenols._ They're found in brightly colored vegetables like peppers and carrots. Coffee is the richest source of polyphenols, however. Chocolate contains polyphenols too.

You should also eat more _resistant starches_, like white rice and starchy vegetables such as sweet potatoes, carrots and pumpkins. These starches can radically change your gut biome.

These starches are "resistant" because they're more resistant to digestion. Your body can't break them down, so you can consume them without causing your insulin levels to rise, which would result in blood sugar problems.

Because they can't be broken down, resistant starches are still intact when they arrive in your colon. Several studies have found that helpful bacteria in the colon thrive on resistant starches and produce a short-chain fatty acid called _butyrate_ when they're digested. Butyrate is vital to a healthy gut and brain, which is another reason that butter is so good for you.

Resistant starches are also found in less common foods, like green banana flour, plantain flour and potato starch.

### 5. Eat the right fats in order to lose weight. 

There's a huge misconception that eating fat will make you gain weight, thanks to a man named Ancel Keys. In the 1950s, Keys argued that saturated fat causes heart disease. It was later revealed that he manipulated his research data to prove his theory correct, but the fallacy he spread is still alive and well today.

Think of it this way: a high-performance car needs high-octane fuel to reach high speeds, because high-octane fuel stores more energy per gallon than low-octane fuel. Your body and brain work the same way.

Instead of fuel, you need essential fats like omega-3s, which your body can't produce on its own. Moreover, fat is crucial to our bodies: it's in our brain and all of our cells and organs. Your body can't function without high-quality fat.

Fat is also the basis for _myelin_, which lines your nerves and allows electricity to flow efficiently in them. You literally think more quickly when you have more myelin, because it allows your nerves to transmit messages faster. Your body also burns fat more efficiently and forms more healthy cell membranes when you consume the right fat and avoid excess carbs.

So what are the right kinds of fat?

Mary Enig, a nutritionist, developed two ways to understand fats. The first is to examine the length of the fat molecule. The shorter the molecule, the rarer and more anti-inflammatory it is. That's why the Bulletproof Diet suggests you consume more short- and medium-length fats like MCT oil.

The second way to understand fat is to measure how stable it is. Stable fats like saturated fats oxidize more slowly because the molecules have less space for oxygen. Oxidation is the culprit behind inflammation and the effects of aging.

Bulletproof oils and fats include MCT oil, ghee, cocoa butter, krill oil, avocado oil, coconut oil, sunflower lecithin and grass-fed butter.

### 6. Maintain a moderate amount of high-quality protein in your diet. 

Like fats, different proteins also have different effects on your body — especially when it comes to your immune system, inflammation and muscle gain.

Protein is vital because it maintains your muscle mass and bone density. In fact, protein is so critical to your survival that your body has powerful mechanisms that keep you from eating too much or too little of it.

However, protein isn't simple. A lot of people assume that all protein is healthy, and that assumption has allowed the processed-food industry to stuff their products with low-quality proteins like gluten and soy.

In fact, in one study of the nutrients from high- and low-quality meat, grain-fed meat was found to be so low in omega-3 fats that it didn't even qualify as a meaningful dietary source!

So buy organic, grass-fed meat. It has more nutrients and fewer toxins than grain-fed or conventional meat. It also has more antioxidants, omega-3s, trace minerals and vitamins.

You should also look for fat that appears more yellow — this color indicates that it's full of nutrients. Grass-fed meat has been shown to have higher levels of carotenoid, which makes its fat a darker shade of yellow.

Don't assume that organic meat is just as good as grass-fed meat, however. Organic grain-fed meat is certainly better than conventional meat but it still contains mold toxins (from the food cattle eat) and hormones that contribute to obesity.

Excess protein can also cause inflammation because it's more difficult to digest than carbohydrates or fats. It's harder for the body to turn proteins into glucose, which provides you with energy.

That's why you crave sweets sometimes: your liver needs glucose to process protein more efficiently. Bulletproof protein sources include wild fish with low concentrations of mercury (think haddock, anchovies, sardines and trout), grass-fed beef and lamb, pastured eggs, hydrolyzed collagen, gelatin, and clean whey concentrate.

### 7. Avoid foods that don’t have any real nutritional value. 

One of the keys to becoming bulletproof is to avoid foods that harm your body. So stay away from _kryptonite foods_, like processed meals and sugar. They provide little to no benefit and they make you fat, sluggish and weak.

Processed food and other food that's been chemically altered doesn't satisfy your hunger. It actually makes you want to eat more.

These foods don't satiate you because they don't have enough _macronutrients_ — carbohydrates, proteins or fat. They might be tasty but that's because of their chemical effect, not the food itself.

_Monosodium glutamate_ or MSG is the most common artificial flavor in processed foods. It was created in Japan during World War Two to make food taste better. People added it to low-quality or even spoiled foods.

MSG causes your cells to send signals to each other so they become activated and overexcited. And when your cells die or get damaged, your brain sends signals requesting more energy, which triggers a headache, mood swings or craving for sweets — the fastest sources of energy.

When you give in to your craving for sweets, you only harm your body more. Did you know that sugar and cocaine trigger the same reward centers in the brain? A large amount of sugar can decrease the dopamine receptors in your brain, which makes it harder for you to feel energy and pleasure when dopamine is released. This process also happens to drug addicts!

Most people know what a "sugar crash" is but they don't know where it comes from. A sugar crash doesn't just throw off your focus and lower your energy — it lowers your blood sugar too. When you consume sugar, your blood sugar level rises until your body pumps in so much insulin that it decreases.

Eliminating sugar from your diet is one of the best things you can do for your overall health.

### 8. Intermittent fasting boosts your metabolism and sharpens your focus. 

You can't be afraid of hunger if you want to be bulletproof. Fasting can be very beneficial if you do it in a healthy way.

There are a few ways to fast effectively. _Intermittent fasting_ traditionally meant eating all your food in a shorter period of the day, usually within six to eight hours. In recent years it's become quite popular, and for good reason: it helps fat loss, prevents cancer and builds your muscles.

_Alternate-day fasting_, a form of intermittent fasting, has been proven to help prevent chronic diseases, reduce triglycerides, and cause significant improvements in several markers, such as _LDL cholesterol_.

The downside of alternate-day fasting is that it requires you to skip breakfast and refrain from eating until after 2:00 p.m., which is difficult when you have to work.

The author wanted to find an alternative to alternate-day fasting, which is how he developed the Bulletproof Intermittent Fasting — you start the day with a coffee and don't need anything more till the afternoon.

Why? Because the fat in bulletproof coffee is very satiating. It also triples the influence of a major physiological mechanism called _mammalian target of rapamycin,_ or mTOR, which increases protein synthesis in your muscles and helps build them.

As mentioned before, the Bulletproof Coffee diet also helps you go into ketosis, even if you've consumed carbs the day before. And you focus better and have higher energy when your body is running on ketones, because your blood sugar level remains steady during ketosis.

So if you want something to go with your coffee and butter, it's best to eat a combination of protein and fat, like poached eggs or smoked salmon and avocado. Eating fat and protein together prevents food cravings by giving your body extra energy to break proteins down into amino acids.

### 9. Do high-intensity workouts at spaced-out intervals. 

Becoming bulletproof isn't just about your diet, of course. Exercise is also crucial, although not all exercise is helpful.

You aren't necessarily healthy just because you can do sports. After all, some athletes drop dead during marathons! Extreme chronic cardio exercise actually strains the heart and damages its muscles. Walking and bike rides, on the other hand, aren't challenging enough to be good exercise.

Exercise is immensely beneficial when you do it right. Ultimately, exercise should be brief, intense, infrequent, safe and purposeful. If your exercise doesn't meet these criteria, you aren't maximizing its benefits.

Weight training is the best form of exercise, as long as you keep each workout under 20 minutes. Do it until your muscles fail. Try the seated row and the chest press, for example.

The more muscular you are, the more resilient you are to fatigue, diseases, pathogens and toxins.

Typical cardio workouts like running or cycling aren't the healthiest way to exercise. Try _high-intensity interval training_ instead: run the fastest you can for 30 seconds, then rest for 90 seconds. Repeat for 15 minutes.

Another benefit of high-intensity training is that your body will produce more of the performance anti-aging hormone HGH, which your body makes to keep you looking young. High-intensity workouts are actually the best way to produce HGH.

Exercising more isn't always good for you, however. In fact, overtraining can be very harmful.

It's critical that you give your body time to recover and rebuild muscle after you exercise, whether you're weight lifting or doing high-intensity interval training. So rest from between two and ten days after each workout. Four to seven days is ideal.

Finally, if you're not experienced with free weights, start with machines first, because pushing yourself to the point of muscle fatigue will increase your risk of injury.

### 10. Your diet has a big impact on the quality of sleep you get. 

Besides eating and exercising well, sleep is the last key to becoming bulletproof. A good night of sleep actually increases your ability to complete cognitive tasks by 50 percent!

Sleep also makes your skin healthier, keeps you looking young, optimizes your insulin secretion, improves your athletic performance and encourages healthy cell division.

You don't have to sleep long hours to feel refreshed, either. In fact, studies have shown that healthy people don't need more than six and a half hours of sleep a night.

Sleep is a crucial part of the Bulletproof Diet because there's a direct link between the sleep you get and the food you eat. When you boost your health and make yourself bulletproof, you can sleep less and still get all the benefits of a good night's rest.

One good way to improve your sleep is to eat fat at dinner. It can be grass-fed butter, animal fat, coconut oil or MCT oil. Fat is a long-burning fuel for your mind and body, so eating these fats before sleep gives you a stable stream of energy.

It's also a good idea to eat fish or krill oil at night, because they contain _DHA_, an omega-3 fatty acid.

Studies have shown that fish oil containing DHA helps you produce _serotonin_, the neurotransmitter that promotes feelings of happiness and lowers the stress hormones that can interfere with sleep.

Finally, eating raw honey before you go to bed also improves the quality of your sleep.

### 11. Final summary 

The key message in this book:

**The Bulletproof Diet isn't just about losing weight. It's about having a higher energy level and better focus too. If you consume the right fats and proteins, do high-intensity workouts, sleep right and fast intermittently, your body and mind will function as well as you look!**

Actionable advice:

**Prepare yourself for sleep.**

If you want to get the best quality sleep, avoid the following activities in the two hours before you go to bed: bright lights, violent movies and exercise. Go to bed in the right mental frame and you'll get much more rest.

**What to read next:** ** _The Big Fat Surprise_** **,** **by Nina Teicholz**

If you still find the idea of putting butter in your coffee off-putting, and you're not convinced that fat belongs in a healthy diet, let journalist Nina Teichholz wipe away any lingering doubt.

In _The Big Fat Surprise_, she challenges the notion that saturated fat is bad for your health and digs deeper into how this erroneous notion first took hold in America, in the end revealing it for what it really is: a big fat lie.

So if you want to learn more about how to think about fat — including what you can learn from a Kenyan tribe about lowering your blood pressure — head over to the blinks for _The Big Fat Surprise_, by Nina Teichholz.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dave Asprey

Dave Asprey is a _New York Times_ bestselling author and the chairman of the Silicon Valley Health Institute.

