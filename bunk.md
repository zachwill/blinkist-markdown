---
id: 5b16bf9fb238e10007002516
slug: bunk-en
published_date: 2018-06-07T00:00:00.000+00:00
author: Kevin Young
title: Bunk
subtitle: The Rise of Hoaxes, Humbug, Plagiarists, Phonies, Post-Facts and Fake News
main_color: AD7044
text_color: 7A4F30
---

# Bunk

_The Rise of Hoaxes, Humbug, Plagiarists, Phonies, Post-Facts and Fake News_

**Kevin Young**

_Bunk_ (2017) takes a look at the history of the American phenomenon of the hoax and identifies its inextricable relationship to racial stereotypes and US history. It also explains how the notion of the hoax has transformed since the early twentieth century and operates within the contemporary landscape.

---
### 1. What’s in it for me? Learn why America is so caught up with “alternative facts.” 

Among the many buzzwords uttered on our screens in recent years, "fake news" and "alternative facts" are two of the more incessant ones. America is currently witnessing an era of hoaxes, and it seems that everything is subject to interpretation.

Despite becoming more aware of the permeance of trickery within modern society, the origins of the hoax, and how it's become so widespread, remain a mystery. Young unpacks this American phenomenon and explores its form and function throughout history.

Understanding where hoaxes come from and how they work may help us to better navigate the web of lies and fabrication present in contemporary politics, arming us with the confidence to debunk stories that are fake and morally corrupt.

In these blinks, you'll find out

  * about the first ever case of "fake news";

  * who pretended to be a black woman; and

  * about a fabricated Pulitzer Prize-winning story

### 2. The hoax is characteristic of the American narrative. 

We're all aware that reality TV shows are not representative of real life. Created to hoax — that is, to trick and deceive — this is a phenomenon not only prevalent in American television but characteristic of American culture as a whole.

In fact, hoaxing dates back to the nineteenth century and has been instrumental in the development of American history.

The earliest case of what we would refer to today as "fake news" was the Great Moon Hoax of 1835. Richard Adams Locke, the editor of the New York newspaper _The Sun_, published several articles claiming signs of life on the moon. Within these articles were a number of quotes misleadingly attributed to South African astronomer Sir John Herschel. Locke was aware that Herschel would be difficult to contact, giving the editor great comfort in knowing that his hoax wouldn't be revealed.

Understandably, the news was excitedly received by many Americans. The nation was still young at the time, struggling to identify itself due to a lack of tradition and history. The distribution of fake information came to be seen as a trait of the American narrative and a counterpart to the American ideology that you can choose to be whatever you want to be.

Nowadays, the American hoax has become a cultural phenomenon. Propagated by the internet, hoaxes have spread further across American culture. The extent of this can be seen in the _Washington Post's_ decision to stop tracking online hoaxes in 2015, as it seemed its audience no longer cared whether the news they were reading was true or not.

This lack of care for credible sources reached new heights in November 2016, when Donald J. Trump, a man who shares an ambiguous relationship with the truth, was elected President of the United States. During his campaign, Trump portrayed himself as a self-made man, despite being born into one of the most privileged families in America, gave inconsistent messages, exploited social divisions and was the owner of a fake university. Yet, some Americans didn't seem to mind that this is who would be running the country!

It marks a dangerous time when hoaxes start to pervade politics, and as such, we should begin to understand its origins and the ways in which hoaxes actually function.

### 3. The hoax disregards the truth and instead gives us what we want. 

Whether concerned with ghost stories, alien abductions or plagiarism, the function of a hoax is to excite the audience.

Throughout history, the success of a hoax wasn't measured by its credibility but by how much interest it could evoke.

In addition to the Great Moon Hoax, 1835 also witnessed another American hoax: the showman P. T. Barnum introduced to his spectators a blind, black woman by the name of Joice Heth and insisted that Heth was George Washington's first nursemaid — a claim placing her at 161 years old.

This hoax took place during a time when the new nation was starting to build its history and identity, and Barnum capitalized on the growing cult status of the country's first president.

He also used the abolitionist sentiments of that period to his advantage, raising money in a purported effort to free Heth's family from slavery.

Barnum's hoax also gave people the chance to become the judge: he encouraged them to see and touch Heth to determine whether he was telling the truth.

Heth passed away in the year following the spectacle, and Barnum held a public autopsy in front of many excited spectators. It was revealed that Heth was not 161, but rather a 79-year-old slave the showman had purchased and trained for the show. Barnum even suggested that his efforts be associated with abolitionism, despite having exploited Heth for his own gain. Thus, Barnum's hoax revealed the paradox of slavery, foretelling the racism that exists at the heart of American hoaxing.

As we saw with Barnum, a hoax is successful when it appeals to our desires.

At the beginning of the 1860s, William Mumler, the inventor of _spirit photography_, claimed that his camera could capture spirits and ghosts unseen by the human eye. Meanwhile, the movement of Spiritualism, which started in 1848 when the Fox sisters from New York claimed they could contact spirits, was becoming a popular phenomenon.

One early practitioner of Spiritualism was the First Lady Mary Todd Lincoln, who wanted to reach out to her deceased second son, Willie. Indeed, Lady Lincoln can be seen in one of Mumler's famous spirit photographs.

The spirit photography hoax never proved that spirits are real but instead thrived on the exploitation of grieving persons who wanted nothing more than to contact their deceased loved ones.

In many other cases, the purpose of the hoax was not only to meet the wishes of the audience but also those conducting the hoax. As we will see in the next blink, such wishes are more often than not intertwined with the history of racism.

### 4. The origin of the hoax is related to racism and white supremacy. 

It should come as no surprise that the hoax and the notion of race both entered public consciousness at roughly the same time, during the age of Enlightenment in the middle of the eighteenth century.

Most hoaxes are built on implicit or explicit racial judgments and have played a hand in perpetuating racist and white supremacist ideology.

If we refer back to the previous blink, Heth was just one of many sights at what was essentially Barnum's human zoo. In another display entitled "What is it?" Barnum placed animal hide on a black man and presented him as the missing link between our ancestors and the modern human, referring to Darwin's theory of evolution in _On the_ _Origin of Species,_ published only months earlier in 1859.

The success of Barnum's human zoo mirrored the interests of the time, which were concerned with categorizing everything that exists on earth — especially humans — within a racial hierarchy. While the backdrop of the abolitionist movement forced Americans to confront their enslaving ways, hoaxes like Barnum's allowed white people to reaffirm their racial superiority through light entertainment.

Ultimately, hoaxes reveal more about racism and white supremacy than initially intended.

One of the most astounding hoaxes to date was by Africana studies instructor Rachel Dolezal, who was president of the National Association for the Advancement of Colored People chapter in Spokane, Washington. Dolezal's trickery came to an end in 2015, when her parents revealed that she was pretending to be a black woman by twisting her hair and darkening her skin.

Despite the controversy, Dolezal continues to identify as black, even after confirming that she was born white. Her belief in the right to call herself black is a clear example of white privilege, one which is enabled by the hoax.

> _"You could go so far as to say the hoax is racism's native tongue."_

### 5. The most dangerous aspect of the hoax is that it can erase cultural history. 

People are sometimes quick to dismiss the hoax as nothing more than harmless trickery, but this underestimates how dangerous it can be.

We learned earlier that hoaxes can sustain long-held racist perspectives, though they can also erase the narratives of those they mean to depict.

In the mid-1990s, the United States was introduced to poetry written by Araki Yasusada — a supposed survivor of Hiroshima — before it was revealed as a hoax. The fake poems were published in a book called _Doubled Flowering_, copyrighted by American poet Kent Johnson. Johnson claimed that his roommate was the author of the book and had entrusted him with the manuscript.

In _Double Flowering_, Yasusada's fake biography implies that there were no avant-garde cultural movements in Japan, so he turned to the West for inspiration. Moreover, the poems include American cliches about Japan, further perpetuating the magical otherness of the oriental foreigner.

Hoaxes can also restrict useful discussions on cultural issues. From 2000 to 2004, Anglo writer Tim Barrus wrote three phony memoirs under the name "Nassijj," an imaginary Navajo. Barrus even assumed the identity of a Navajo to sign books and accept prizes, until _LA Weekly_ revealed the _Navahoax_ in 2006.

By making up the name "Nassijj" and assigning it as Navajo, Barrus falsely rewrote Navajo history, thereby eliminating the real culture in the process. Similar to the Japanese poems, Barrus's memoirs perpetuate a racial narrative — the suffering of Native Americans — as he recounts his time taking care of his dying adopted son.

Barrus, and other hoaxers who pretend to be Native American, create an alternative story that effectively erases the real cultural history of indigenous people and other minorities, removing the need for important conversations concerning change for, or conservation of, these communities.

With this in mind, we can see just how powerfully problematic hoaxes can be.

### 6. In the twentieth century, the hoax transformed from a show of wonders into the manifestation of horror. 

Though Barnum's hoaxes were shrouded in racial prejudices, the intention was to wonder and delight the audience. But by 1891, the year the showman passed away, the world was being introduced to high culture through the establishment of museums, and in shows such as Barnum's, the freaks were replaced by exotic animals.

Since then, the hoax has shifted from a presentation of wonder into a more sinister phenomenon relating to America's obsession with horror.

October 30, 1938, marked the day Orson Welles broadcast an adaptation of the H.G. Wells novel _The War of the Worlds_ on the Columbia Broadcasting System radio network. Audiences weren't told that it was a work of fiction, and the announcement of aliens taking over the planet led to a country-wide panic.

The H.G. Wells hoax played on our fears and anxieties and was an indication of the modern form they were beginning to take.

In 1980, the front page of the _Washington Post_ featured an article by Janet Cooke entitled "Jimmy's World." It was about an eight-year-old African-American addicted to heroin, and in 1981, Cooke won the Pulitzer Prize for her story — despite it being completely false.

Several decades later, we find ourselves asking how such an established publication could overlook such blatant racial stereotyping. "Bad, ain't it" and other similar speech patterns were attributed to Jimmy, details adding to the false representation of the "ghetto" which Cooke claimed he came from.

Cooke's fabrication is representative of the wide net that hoaxes now cast, containing some of society's darkest horrors. In this particular case, an eight-year-old heroin addict being a product of modern America is the horror.

> _"If it is a wish, the hoax is a wish corrupted."_

### 7. We live in the Age of Euphemism, where the truth is replaced by notions. 

While some of us may refer to the present as a _post-factual era_, the author suggests that we're living in the _Age of Euphemism_, where people don't really mean what they say.

The hoax began to hit its stride from the 1990s onward, when the American narrative was changing drastically.

The emergence of the age of information has simultaneously given rise to the age of _disinformation_, or _faction_ — where fact is combined with fiction.

At the beginning of the 2000s, the _New York Times_ reported on America's efforts to track down weapons of mass destruction in Iraq but never examined whether the government's claims were truthful. They realized later they had inadvertently supported America's decision to go to war.

A large part of the blame lies with the internet, as the medium allows for hoaxes to spread instantaneously and ubiquitously. Think back to the _Gay Girl in Damascus_ blog that went viral: believed to belong to an American-Syrian lesbian, it was revealed to have been written by a white American man named Tom MacMaster.

Ultimately, however, the blame falls on the nation's narrative crisis, which celebrates spectacle over truth.

Much like the audience going to see Barnum's show, modern spectators of the hoax aren't too concerned about the truth.

To illustrate just how far this has invaded our collective consciousness, let's look at the American President: People didn't seem to care that Trump was ambivalent to the truth, not while they were being entertained by his antics. During his campaign, for example, Trump claimed that climate change was a hoax created by the Chinese.

Worse yet, his indifference toward truth is also representative of the correlation between the hoax and racism. The success of his fabrications with large swaths of the public is a result of racist tendencies in American society and reflect the superiority felt by many of Trump's supporters over minority groups.

Though what hoaxes reveal might be hard to stomach, we need to address these stories and understand the narrative crisis in order to start conversations that will bring change to the current structure of society.

> _"Bunk doesn't care if it's real or not — it just expects you to accept it."_

### 8. Final summary 

The key message in this book:

**We are currently living in the age of disinformation, meaning that the need to understand hoax is more imperative than ever. The hoax perpetuates racial stereotypes and erases the history of those it masquerades. Its history can be traced to the start of the twentieth century, gathering steam throughout the decades to be transformed into a manifestation of fear.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Stamped from the Beginning_** **by Ibram X. Kendi**

_Stamped from the Beginning_ (2016) offers a powerful examination of the modern history of racism in the United States, including where racist ideas originate and how they spread. In particular, the author looks closely at how the presidential campaigns and administrations of Richard Nixon, Ronald Reagan and Bill Clinton have helped propagate racist thought and had a detrimental impact on America's black communities.
---

### Kevin Young

Kevin Young is the poetry editor for the _New Yorker_. He has written ten poetry books, including _Blue Laws: Selected & Uncollected Poems 1995-2015_, and nonfiction works such as _The Grey Album: On Blackness of Blackness_, regarded as a "Notable Book" by the _New York Times_.

