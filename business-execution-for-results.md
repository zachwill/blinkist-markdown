---
id: 595a238eb238e100069fb380
slug: business-execution-for-results-en
published_date: 2017-07-06T00:00:00.000+00:00
author: Stephen Lynch
title: Business Execution for RESULTS
subtitle: A Practical Guide for Leaders of Small to Mid-Sized Firms
main_color: C0262C
text_color: A62126
---

# Business Execution for RESULTS

_A Practical Guide for Leaders of Small to Mid-Sized Firms_

**Stephen Lynch**

_Business Execution for RESULTS_ (2013) is a guide to building a better business. These blinks offer a practical plan for setting appropriate goals and performing the necessary analyses to create a winning business strategy that will lead your company straight to the top.

---
### 1. What’s in it for me? Find the right tools to build a successful business. 

It's easy to come up with a grand, ambitious goal, like developing and marketing a new drug for the pharmaceutical industry or positioning yourself as a leading architect known throughout the world. Making it a reality, though, is a whole different ballgame. To attain your goal, you will likely have to work very long hours for a quite a few years.

But even hard work does not always yield fruit. Start-ups often go bust after three to five years of continued efforts, while many dedicated business professionals end up spending long hours in the office without having anything to show for it in the end.

This happens because success must be carefully planned. These blinks will show you that you'll need short-term, intermediate and long-term goals, and also identify the right tools to reach your goals.

You'll also learn

  * how to stay excited about big hairy audacious goals;

  * how to recognize the best opportunities in your business environment; and

  * how to create a focus for your company that will ensure its success.

### 2. Define ambitious goals that will show the path to success. 

****Have you heard the term _Big Hairy Audacious Goals_, or _BHAGs_? It was coined by Jerry I. Porras and Jim Collins in their book _Built to Last_. To qualify as a BHAG, a goal needs to meet four criteria:

It needs to be _very_ big, take years to accomplish, be a little bit elusive in terms of the best way to attain it and must be easy to identify if it's been achieved or not.

Consider Wal-Mart. Following World War II, the company's founder, Sam Walton, set a remarkably bold goal; he wanted to turn his tiny variety store in Newport, Arkansas into the most profitable one in the entire state.

Since he was primarily selling cheap T-shirts and fishing rods at the time, this was a pretty audacious goal. It also wasn't something he could achieve in a matter of months, and it wasn't immediately clear how he could set about doing it. Finally, in terms of verifying his potential accomplishment, it would be easy to check the size and profit of his growing business against other stores in Arkansas.

By 1948, Walton had achieved his incredible vision — his store was the most profitable of any variety store in Arkansas. From there, he just kept setting goals and, decades down the line, by 2002, Wal-Mart had become the largest company on the face of the earth.

So, do as Sam did and identify your own BHAG. But a BHAG is not necessarily about growing your company; it can also be related to a specific product or project.

In 1934, Walt Disney set out to create the very first animated, feature-length film. No one in the industry believed in this bold project because animation was only considered suitable for short films.

History proved them wrong. Disney reached his goal when _Snow White and the Seven Dwarfs_ was released in 1938.

People loved it. And even though Disney's original goal hadn't been revenue, the film did generate massive returns.

Next up, you'll learn about two key concepts that are at the foundation of your business.

### 3. Build commitment and keep your team on track with a couple of core ideas. 

Imagine your dream is to build a cathedral. To achieve this incredible goal, would you hire people who simply know how to lay bricks or people who are wholeheartedly committed to building a stunning architectural masterpiece. Probably the latter, right?

After all, having a unifying purpose is essential to forging commitment and building a thriving business. This central motivation is called a _core purpose_ and it's key to reaching your goals.

For instance, as an architect, your core purpose might be to beautify cities or to build environmentally sustainable houses. Simply put, your core purpose infuses your work with meaning, while gathering energy and solidifying commitments in the process.

This is an important factor since the more committed a team is, the more likely they are to achieve extraordinary results. In fact, the employee research firm ISR found that companies with an engaged workforce are more productive and profitable overall.

So, your core purpose is a fundamental aspect of achieving your stated goals, but so are your _core values_. These values constitute your company's moral compass; they ensure consistent decision making and guide your hiring and firing practices. In other words, your core values dictate how your company operates.

Say you run a taxi company. Your core values might be summed up by the phrase, _the right route is the right route_. Such a value will indicate that your team doesn't take detours to jack up fares.

By stating this up front, you'll keep your team on solid moral footing and ensure that they are committed to making the right decisions.

But your values will also guide who is on your team in the first place. If it turns out that an employee doesn't believe in the values you stand for, it is clearly the right decision to let them go.

### 4. Scan your environment to make informed decisions. 

Imagine you've invested every last drop of your ingenuity into creating a deli that only sells broccoli jam. Your business could be perfectly organized and seamlessly run, but it wouldn't matter if nobody wants what you're selling.

So, how do you determine which products will sell? By scanning your environment with a _PEST_ analysis.

This acronym, which stands for _Political, Economic, Social_ and _Technological_, is a tool to define external factors that will impact your industry.

To begin with, consider political circumstances that might impact your business, both today and down the line. From there, ask yourself, given the political opportunities and threats you can identify, which strategies should you adopt to succeed both in the present day and the future?

For instance, in the 1960s, Sam Walton analyzed the environment Wal-Mart was entering. He realized that the US government was making a number of infrastructural investments, one of which was the creation of more roads. He therefore knew that it would become easier for customers to reach his stores and that it was a good time to open new locations.

Next, you should consider economic factors that'll influence your business and ask yourself the same question about the economic climate as you did about the political one. Sam Walton remains a good example as, in the years following World War II, he saw the economy surging and knew it was an opportune time to launch a new business.

From there, it's essential to take into account social elements that'll impact your company. To do so, consider how people, your employees and, in particular, your customers act and think. Ask questions like, is the population getting older or younger?

When Walton was starting out, it was clear that more people were having children. He saw that the baby boom would eventually result in more potential customers and was another cue that the time was right to grow.

Finally, consider technological changes that might occur and how they could affect your business. Which technologies are currently trending? If a new technology like, say, flying bicycles was to hit the market tomorrow, how would it affect your business?

Now that you've asked the right questions and set the stage, you're ready to make some bigger decisions — which is exactly what you'll learn about next.

### 5. Thriving businesses stand out and remain focused on reaching their goals. 

If you want to stand out in a crowd, all you've got to do is dye your hair pink and spike it into a mohawk. But standing out in the world of business requires a bit more thought.

To do so, you need to identify your _value discipline_, the thing that makes you different from the competition and guarantees you a leading role in your industry. There are three different value disciplines to choose from and it's up to you to pick the one that fits you best.

First, there's _operational excellence_, which basically means that you have low costs. Second is _product leadership_, which is all about creating the newest and best products. And third, there's _customer intimacy_, which implies that you produce solutions that are perfectly tailored to the problems your customers face.

But regardless of which approach you choose, all three of these value disciplines should keep you focused on the unique difference that makes you stand out. Let's return to Sam Walton. Back in the early 60s, he began building his empire and decided to focus on offering the low-cost option.

Specifically, he made the decision to spend some of his own savings to be able to offer rock bottom prices to his customers. His goal was for Wal-Mart to be the store that everybody could afford; in the end, that's exactly how the company differentiated itself and became such a resounding success.

Then, once you've chosen your value discipline, you can use it to identify your _core activities_. These are the strategic actions you take to keep yourself on the path toward your goals.

Imagine you decide to focus on low prices as Walton did. Other factors like product development and customer service will soon become less important, as they're not essential to reaching your goals. Identifying this will allow you to deliberately allocate fewer resources to these unnecessary elements and instead focus on getting better deals from wholesalers to lower your retail prices. By doing so, you'll be syncing your core activities with your value discipline.

Now that you've got the basics in order, you're ready to build the messages you need to bring in prospective customers and beat out the competition.

> _"There is nothing so useless as doing efficiently that which should not be done at all."_ — Peter Drucker (renowned management thinker and author)

### 6. To ensure trackable progress, use clear targets and key performance indicators. 

You might be thinking, what's a _good_ goal to set for your business? Well, it's actually a difficult question to answer, since words like "good" are quite abstract and can mean entirely different things to different people.

This is precisely why it's crucial to come up with concrete ways of measuring your business's progress — and numerical targets are an ideal method. Numbers make progress more tangible and, therefore, easier to monitor.

Say two of your targets in the coming years are to build two more branch offices and acquire ten more resellers. Once you've set such goals, you can track the numbers you're putting up and have your whole team monitor the progress.

But that's not all. You'll also need to identify your _key performance indicators_, or _KPIs_. These all-important markers of success keep track of performance in essential areas and help you predict financial success.

For example, if you're in the construction business and notice a correlation between higher numbers of proposals and an increase in income, you might want to track the number of building proposals you submit as a KPI.

Or, if your industry is pharmaceutical sales, you might track the number of weekly physician's visits that your sales representatives make because you know it has been a key indicator of your business success in the past.

### 7. Use meetings, especially strategic reviews, to keep your business moving forward. 

By now you have some strong strategies for business success, but if you want to reach your BHAGs, you'll need to be serious about maintaining progress. In that fight, losing focus and wasting time are your biggest enemies — so here are some tactics to overcome them:

First, conducting meetings is central to maintaining forward momentum, as they establish a clear rhythm for your business. While the idea of holding many meetings can be daunting, in general, they give you time to pause and reassess your direction; these regular moments of reflection can quickly become essential to a thriving business.

One meeting that's particularly important is the _quarterly strategic review_. It should happen every three months to give your team the opportunity to look back on the work you did and assess the last quarter. It's a time to consider what you've achieved, review new projects on the docket and take a look at any surprises that came up.

By assessing all these factors, you can double-check that everything you're doing is in line with your goals. From there, you can use this information to perform what's called a _SWOT_ _Analysis_, which is a convenient way to map the current status of your _strengths, weaknesses, opportunities_ and _threats_.

For instance, one of your strengths might be an expert on your team, while a weakness could be your failure to make enough sales. An opportunity might be the bankruptcy of a key competitor, which opens the door for you to bring in more customers, and a threat might be a pending merger between companies that, together, could poach many of your customers.

From there, you can use your quarterly review and SWOT analysis to plan for the future and choose the projects you'll be launching in the next quarter.

Imagine you identify a few threats and weaknesses during your SWOT analysis. It's probably in your best interests to address these in the next quarter; then, once you've gone through damage control, you can look at which new projects you want to take on to stay on top of your overarching, ultimate goals.

### 8. Final summary 

The key message in this book:

**Creating a successful business requires a winning strategy that enables you to set the right goals and actually reach them — but that's easier said than done. To pick the right strategy, you'll need to do some careful thinking and analysis first, after which you'll have to monitor your progress carefully.**

Actionable Advice

**Involve your entire team in strategic research.**

When analyzing your business, including the PEST analysis, ask everyone in your team for their opinion. With more minds involved, it'll be easier to identify the most important aspects in need of consideration and pin down a winning strategy. So involve everyone you can in this process and don't go it alone — you might miss something important.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Value Proposition Design_** **by Alexander Osterwalder, Yves Pigneur, Greg Bernarda, Alan Smith, Trish Papadakos**

_Value Proposition Design_ (2014) is a comprehensive guide to designing compelling products and services. Real value comes from empathizing with customers to find out what everyday jobs and tasks they need help with. However, coming up a product that helps customers complete these jobs and tasks is only the beginning.
---

### Stephen Lynch

Stephen Lynch, in addition to being a developer of successful business strategies, is a former police officer, bodybuilder, DJ and a pharmaceuticals sales manager, just to name a few of his past careers. He's also the cofounder of RESULTS.com, a platform that offers software and consulting services to help businesses thrive.

