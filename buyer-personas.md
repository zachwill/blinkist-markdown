---
id: 55b62a283464390007210000
slug: buyer-personas-en
published_date: 2015-07-27T00:00:00.000+00:00
author: Adele Revella
title: Buyer Personas
subtitle: How to Gain Insight into Your Customer's Expectations, Align Your Marketing Strategies, and Win More Business
main_color: DB2C31
text_color: A82226
---

# Buyer Personas

_How to Gain Insight into Your Customer's Expectations, Align Your Marketing Strategies, and Win More Business_

**Adele Revella**

It's hard to sell to people that you don't know. _Buyer Personas_ (2015) gives you the tools you need to truly understand your customers — their motivations, their quirks, their reservations — so that you can fine-tune your messaging to become a maximally effective seller.

---
### 1. What’s in it for me? Learn everything you need to know about your customers. 

Do you run a blog or an app? Or maybe you work for a marketing company or in sales? If you work in any of these fields (or countless others), the success or failure of your undertakings depends on how well you connect with customers.

If your messages chime with people, you will gather sales, shares and subscriptions; if you create a message that no one cares about, however, you'll fail. If only there was a way of finding out exactly what customers want.

Well, there is! These blinks show you how to create a buyer persona, a complete story about your customer, what they want and need, and what they think about when buying. In short, you'll never miss out on customers again.

In these blinks, you'll discover

  * why a company deliberately made their pocket calculators heavier;

  * why the best customer interviews require little or no preparation; and

  * what the Five Rings of Buying Insight are.

### 2. Knowing your customer helps you provide them with what they’ll buy. 

Imagine you're selling sofas — big, expensive, cumbersome sofas. Now imagine you've taken your product around to tiny, high-rise rental apartments in the inner city. How many do you think you'll sell? Probably none.

Not knowing your customers' wants and needs is a recipe for disaster. So when you enter a new market, it's important to conduct in-depth research to understand your customers.

If you don't do your homework, your prized products may end up gathering dust instead of bringing in revenue. Surprisingly, this is exactly what happened to Apple when they launched the 3G iPhone in the Japanese market.

In 2007, Japanese consumers bought 5 million mobile phones. The following year, when Apple introduced their 3G phones, they sold only 200,000 units — a mere four percent of the Japanese market share — and sold mostly to established Apple users. So what caused this massive failure?

Japanese consumers were used to shooting videos and watching TV programs on their phones, and iPhone 3G didn't even have a video camera! Apple failed to realize that buyers in the Japanese market had different needs than those in the US and Europe, and suffered for it.

To learn about your customers' needs, you have to ask them directly. Unlike Apple, the Turkish-appliance company Beko conducted interviews with prospective customers before launching their dryer in the Chinese market.

As a result of these interviews, Beko's marketing team learned that a large portion of Chinese consumers believe that "there is a spiritual component when garments are exposed to the sun."

So Beko designed their dryers for the Chinese market by making it possible to stop the drying cycle halfway through, allowing customers to dry their garments in the sun. Their diligence was rewarded; Beko dryers are selling very well!

### 3. Buyer personas help you truly know your customer. 

Next time you're at the supermarket, look around and see if you can identify what the patrons are using to evaluate products. Different customers have different criteria. For example, when selecting eggs, some customers will search for organic eggs, while others just want the cheapest ones they can find.

If you are an egg producer, you need to understand _why_ different customers behave the way they do, in order to fulfill their needs and create product messaging that resonates with them.

To help you discover "why," you need to build _buyer personas_ — detailed descriptions of different types of potential customers. Buyer personas catalogue customers' needs and problems, identify their thinking patterns and pinpoint what types of solutions they are interested in.

To return to egg manufacturers: If they know which customers want organic eggs and which want eggs for cheap, and if they know what motivates these preferences, then they can send the right message to the right customer, instead of broadcasting one message to all customers.

Often, knowing a customer's buyer persona will tell you things about their buying habits that you otherwise would never know. To get an idea of how this works, consider this real-life example:

Famed marketer Regis Mckenna was asked to create a buyer persona for a client who sold pocket calculators. So he watched how the customers selected their calculators from among numerous options.

He found that, when customers were comparing models of differing weights, they chose the heavier calculators. Why? They associated heft with quality.

McKenna took his findings to his client and told them to go against the common assumption that smaller calculators were better. Instead, they should make their calculators _heavier_ than their rivals'.

The client did — and their sales increased.

But buyer personas don't come to you by some stroke of luck. Uncovering buyer personas takes time and money. So if you're going to do it, you want to do it right. Our following blinks will show you how.

### 4. Skeptical stakeholders can easily be convinced to implement buyer personas. 

People often prefer the known to the unknown. And usually, they don't like to venture outside their comfort zone. It's no surprise, then, that you're likely to run into resistance from stakeholders when you want to try new things in the marketing department. This is equally true when it comes to building buyer personas for your product.

Internal stakeholders, such as management and shareholders, each have their own reasons for rejecting buyer personas. Some think the project will be a cumbersome time sink, and that the resources invested in identifying the personas will never be recouped.

Others argue that they _already_ know their customers. Why should they spend money to confirm something they already know?

Luckily, convincing doubters to adopt buyer personas isn't that hard if you have the right arguments.

First, set up a meeting with the unconvinced stakeholders. During the interview, ask them to pretend to be a customer as you ask them some questions.

Ask them things like, "When did you discover that you needed the kind of solution we offer?" "How did you evaluate the competing solutions?" and "Why did you choose _ours_ over our competitors'?"

As they answer, look for holes in their arguments.

More than likely they will say something like, "We chose your product because it's the best in the market. It has the best features for the best price." When this happens, stop the roleplay and ask, "If this were true — if you _really_ offered the best features at the best price — then wouldn't you be more successful than your competition by leaps and bounds? But you're not (which is why we're doing this exercise in the first place), so maybe you're being a tad optimistic!"

These interviews will make it apparent how little stakeholders actually know about their customers and products. It shouldn't be hard to convince them that they need buyer personas.

> _"As much as we may like to think of ourselves as open-minded, most people's initial response to a new idea in resistance."_

### 5. Use an internal or external database to reach your target customers. 

Once you've gotten the internal stakeholder onboard, you need to actually create the buyer personas. This work begins with setting up interviews with potential customers. But how should you reach out to them?

First, you need to find your customers, and that will require the help of your sales team.

Although the interviews and analysis will be conducted by the marketing department, the people on your sales team are the ones who know most about your potential customers. They have the database of all the people who have bought your products, or at least inquired about them (even if they ended up going with your competitor). Within this database you'll find many people who are perfect interview candidates.

But while the sales department's databases will certainly be helpful, chances are that you will need to find interviewees from external sources as well.

You can't necessarily count on your sales department's database to be perfectly curated. For example, it may contain incorrect e-mail addresses, obsolete telephone numbers and so on.

Furthermore, external sources, such as qualitative research agencies, can help you not only to reach the right people but to engage people who have no previous experience with your company or your products. Getting their feedback can help you expand your reach.

But what if you sell B2B solutions? Who will form the basis for your buyer personas? If you work in a B2B environment, you deal with two people: the person up top who signs the deals and the person below who does all the research.

It is this researcher who does all the work to determine the specifics about what their company needs. As someone who is well acquainted with their suppliers and their buying processes, this person is the one whose opinions matter most.

### 6. Unearthing helpful answers requires your asking good questions and listening well. 

The key to learning the most from interviewees is to spend more time listening and less time speaking. Ask your question, and then get out of the way.

In a buyer persona interview, the only question you need to prepare for and think about beforehand is the very first question you ask. Ultimately, your first question will be about the first time the interviewee realized that he faced a problem that required him to seek out a new solution.

You might say, for example, "Tell me about the morning that you discovered you needed a new email-marketing solution? What happened?"

Your interviewee's answer probably won't be the whole story. While he might give you the most straightforward answer, a little probing may help you get a better sense of the situation. To ask those probing questions, however, you'll need to listen well to how he answers.

Let's consider the interviewee's response to our hypothetical question. He may respond that he was motivated to find a new email-marketing service because he wanted his campaigns to be more effective and that he wanted to measure returns on investment for the marketing strategy.

Here, the interviewee talked about the _benefits_ of the solution he decided on, but didn't actually answer your question. You want to know what _caused_ him to look for solutions, so that you can uncover what the customer really wants.

To find these causes, use the interviewee's own keywords when you ask your follow-up questions.

You can say, for example: "Let's take a step back for a second. You mentioned that you needed to measure marketing ROI and effectiveness. What suddenly made this a priority for your business?"

This line of questioning should lead you to a more direct and substantial answer about why the customer started looking for a solution in the first place.

> _"Listening is as powerful a means of communication and influence as to talk well." - John Marshall_

### 7. Analyze your buyers’ insights to discover their needs and how to accommodate them. 

There is an old Egyptian saying that goes like this: "Do you know so and so? Yes. Did you hobnob with him? No. Well, then you don't know him." The same applies to your buyers. You can't presume to know them if you never talk to them.

To understand buyers' behavior, you have to talk to them and uncover the _Five Rings of Buying Insights_. Doing this will tell you everything you need to know about your buyers' decisions.

The first ring is _Priority Initiative_. You want to discover why some buyers decided to look for your solutions and why others remain satisfied with the status quo. You can find the Priority Initiative through the kinds of questions we outlined in the previous blinks.

The second ring is _Success Factors_ — the results that your buyer persona expects to achieve from purchasing your solution. Is she looking for greater efficiency? Cost reduction? Or something else altogether?

The third ring is _Perceived Barriers_. Here, you want to identify any reasons a buyer might have to regard your solution as suboptimal, as well as other concerns she might have. For example, your potential buyer might be concerned about maintaining her customers' privacy, or maybe she perceives your company as unreliable.

Then there's the _Buyer's Journey ring_. You want to learn who and what influences your buyer, and how she evaluates options in her selection process.

For example, if you're a B2B organization, you may realize that the CEO at your prospect's company interferes in each big-buying decision. This information can help you prioritize your marketing investment.

The final ring is _Decision Criteria_, which outline the critical aspects of your potential buyer's ideal solution as well as her expectations. You want to know things like whether the buyer is looking for an "easy to use" solution or for the most comprehensive service possible.

Once you've identified the answers to these questions, you will finally have a complete buyer persona.

### 8. Organize your interview data in order to develop actionable insights. 

So, after gathering the data, what's next? Now comes the careful analysis. Remember: you aren't just looking for data that confirms what you already believe. Rather, you want to stay as objective as possible in order to discover what the customer _truly_ needs.

So where do you start?

Combine all of the information from your interviews into a single story for each ring. If you've interviewed 20 buyers, you don't want to be left with 20 different results. Rather, you want to aggregate your discoveries into one single discovery.

Of course, this doesn't mean jumbling all your information together. You still have to divide your data into each of the five corresponding Rings of Buying Insight.

Separate the data for each ring onto its own sheet of paper. On each sheet, find a memorable quote, identify who said it, and then add a clear headline that highlights what the quote refers to.

For example, when writing out the Decision Criteria Ring, you might have identified this interesting quote from a potential buyer: "I don't want to waste money having to create a solution especially for us. Instead, I want something that already exists, but which we can easily repurpose for our own business needs."

In the next column, write the source of this quote: "Tony, Head of Marketing." And in the next column, write a headline for this quote so you can easily understand what it means. In this case, you might use "ease of usability."

Once you've completed this exercise for each of the five rings, you will have organized the answers to all your interview questions. With these answers, you can uncover the essence of your buyer's story in a concise way.

### 9. Create a message that tells the buyer what they want to hear. 

"We have the best solution." "We're the cheapest." " Our solution will save you money." Do these kinds of messages inspire you to buy? It's more than likely that these are the kinds of messages that you completely ignore. This is because the messages don't speak to _you_ and _your_ unique concerns.

With the results of your interviews, you are now equipped to address your buyers' unique, primary concerns.

In the course of your interviews with your buyers, they revealed to you what it was that inspired them to search for a solution, which barriers could prevent them from adopting your solution and, finally, which features they're looking for in a solution.

In short, you will have your buyer's complete story, containing every conceivable insight about their buying habits.

But once you know what your customers need, you then need to connect what they want to hear with what you have to say.

A good place to start is by cataloguing all your solution's capabilities and selling points in a "_Capabilities List_."

Then, use your previous analysis of your buyers to catalogue a list of buying insights in a "_Buyer's Expectations_ " list.

Put the two lists side by side and look for where they intersect, that is, where your capabilities meet the buyer's expectations.

For example, if "durability" is one of the defining characteristics of your product, and if the buyer wants a product that will endure, then you should match these together.

Write a sentence about each of these matches — things like, "We're flexible and able to adapt our solution to your individual needs." This way, your creative-marketing and sales teams will know which message they need to convey.

With this final piece of the jigsaw puzzle, you can finally take your buyer persona and apply it to the real world, ensuring that your product is marketed to the right customers — those people who are looking for exactly what it offers.

> _"Fully 60 to 70 percent of content churned out by business-to-business (B2B) marketing departments today sits unused."_

### 10. Final summary 

The key message in this book:

**Selling to customers requires you to know them. You need to know what motivates them to buy, what they need in a product and how they view your solution. By identifying their Buyer Persona, you will have the tools you need to develop specialized marketing to sell to the customers who want your products most.**

Actionable advice:

**Prepare for the interview in advance.**

If you want to make the most from your buyer interview, you'll need to be prepared. Check your interviewee's Linkedin profile to learn about them and their background. Ask your sales team to give you more info about them: Have they dealt with this person before? And what is their relationship to the business you want to sell to?

**Suggested** **further** **reading:** ** _Purple Cow_** **by Seth Godin**

These blinks explain why traditional marketing no longer works, and why to be successful you need to build "Purple Cows," remarkable products and services that stand out of the crowd. They also explain how you can reach your target market once you've found your own Purple Cow.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Adele Revella

Adele Revella is the CEO and founder of Buyer Persona Institute, and a leading authority on buyer personas. In addition to running the Institute, she is also a marketing and business leadership speaker, consultant, blogger and workshop facilitator.

[Adele Revella: Buyer Personas] copyright [2015], John Wiley & Sons [Inc. or Ltd. as applicable] Used by permission of John Wiley & Sons [Inc. or Ltd. as applicable] and shall not be made available to any unauthorized third parties.

