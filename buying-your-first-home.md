---
id: 57347c4ce236900003290214
slug: buying-your-first-home-en
published_date: 2016-05-19T00:00:00.000+00:00
author: Ilona Bray, Alayna Schroeder, Marcia Stewart
title: Buying Your First Home
subtitle: Nolo's Essential Guide
main_color: 35BCB5
text_color: 20706C
---

# Buying Your First Home

_Nolo's Essential Guide_

**Ilona Bray, Alayna Schroeder, Marcia Stewart**

_Nolo's Essential Guide to Buying Your First Home_ (2014) tells you everything you need to know when you're in the real estate market for the first time. These blinks lay out the dos and don'ts of buying your first home, from getting a successful inspection, to negotiating the price and making the right offer.

---
### 1. What’s in it for me? Learn how to purchase your first home. 

First job, first kiss and maybe even a first hangover; these are all memorable moments in a person's life that might mark the start of another chapter when growing up. But there is one major step that really represents a person's coming of age: buying your first home. This is a hugely significant choice that will stay with you for a long time — and perhaps even the rest of your life.

So it's no wonder that buying your first house is a highly stressful decision.

These blinks invite you to forget the fear of taking the plunge, offering up plenty of insightful advice for you to find the right home at the right price.

In these blinks, you'll discover

  * how to act in cold and hot markets respectively;

  * what the lamps in a house can tell you, before you put in an offer; and

  * how you should behave during the first visit to a home that you're interested in purchasing.

### 2. Buying a home is a great choice and there’s no reason to avoid it. 

Just about everyone has thought, at one point or another, "I'd love to own a home." But beyond the idea of having a house that is yours alone, investing in real estate is a one-of-a-kind opportunity: it's a chance to control a large, valuable asset by paying a small lump sum up front as a down payment.

For instance, it's common to be able to buy your first apartment for about $100,000 just by putting $15,000 on the table and taking out a loan for the remainder. Not only that, but real estate is a relatively safe investment. While property prices drop during economic downturns, they don't plummet irreversibly like stocks can.

For example, according to the S&P/Case Shiller Home Price Index, in 2013, the average appreciation in value of American homes in the time since they were purchased was 12 percent.

So, it makes good financial sense to buy a home — but it also has a psychological impact. After all, if you're a lifelong renter, you know that having a temporary living situation can prevent you from investing in nice furniture or taking the time to decorate your place. So, you leave the walls white and that smelly carpet is there to stay.

But when you move into your own place, everything changes — you suddenly feel an urge to personalize every detail.

Another plus of home ownership is that having no landlord means you don't have to wait for someone else to fix your leaky pipes. You also don't have to fear rent hikes and the threat of eviction; all that stress just falls away.

But even with all those upsides, people always find excuses to avoid home ownership, and it's important to overcome them. For instance, you might think, "I have such a good deal at my place right now." Sure, your rent might be cheap, but buying your own home means that it will, at least eventually, belong to you.

In other words, your rent might be low, but would you rather it was cheap or free? Because free is what it'll be when you're the owner.

### 3. Make sure to choose the right neighborhood and decide between an old house or a new one. 

You've probably heard that when it comes to real estate, the three essential rules are: "location, location and location!"

The neighborhood you choose has an impact not only on your quality of life, but also your home's resale value. So, the easy choice is an area that suits your personality, gives you a good sense of well-being and also makes your everyday life convenient.

But it's also important to pay attention to _conformity_. Buyers who are attracted to a particular neighborhood tend to have a certain type of house in mind and won't pay attention to unique homes. For example, people who are looking to buy in small-house suburban areas won't be interested in big houses in the same area because they can't afford them. As a result, those houses will appreciate more slowly.

Once you've decided on the neighborhood, you'll be faced with another big question: are new houses necessarily better?

The answer is sometimes, but not always. After all, if you're into modern design and lifestyle, or simply can't imagine a house without an induction cooktop and light shafts, you'll want to go for a brand new house.

But remember, "new" doesn't necessarily mean "in perfect condition." In fact, some developers simply do shoddy work, while others don't finish on schedule, failing to compensate their buyers and even going bankrupt before the house is finished!

On the other hand, old houses have their own problems. For starters, they tend to fetch a lower price at resale than brand new homes and often come burdened with replacement costs like those for water heaters and leaky roofs.

In addition, old homes are generally less energy efficient than newer ones. So, you'll also be left paying additional costs like modernizing your windows to avoid losing heat during the colder months.

And finally, the layout of older houses is usually based on the tastes of a previous era. In other words, you might not like the size of the rooms or the lack of electrical outlets.

### 4. A mortgage is your ticket to financing your home. 

If you're like most people, you don't have millions of dollars lying around just waiting to be invested in real estate. But don't worry, there are other ways to pay for your first house.

The most common, a _mortgage_, is a loan given for the express purpose of buying real estate. In such loans, the property acts as collateral for the money. So, if you take out a loan to finance your house and fail to make the mandatory payments to the lender, they can take possession of the house to recover their investment.

While that sums up the basic idea, there are actually different types of mortgages. For instance, you might be offered a mortgage with a fixed or adjustable interest rate; the difference between the two is that the rate of the latter changes over time.

However, most of the significant differences between mortgages are caused by the identity of the borrowers. For example, if you earn a decent, steady salary and can prove that you pay your bills on time, you'll qualify for a lower interest rate.

Similarly, you can also receive better loan conditions by putting forward a bigger down payment — doing so proves that you're less likely to default on your mortgage.

In fact, the down payment is crucial to your greater financing strategy. For instance, a few years ago, you could still finance 100 percent of a home purchase with a mortgage. But after the recent housing meltdown, many houses plummeted in value just after their mortgages were granted; the result was huge losses for lenders.

As a consequence, lenders of today tend to feel safer financing just 80 percent of the purchase and require the borrower to come up with the remaining 20 percent.

### 5. For a successful visit to a prospective home, you should dress accordingly and ask the right questions. 

So, you've identified a few properties you're interested in. What's the next step?

Visit them!

But before you do that, remember that when you're looking over a house to buy, _you're_ being evaluated as well. The seller's agent will search for clues about your financial status. For instance, being too well dressed or wearing lots of expensive jewelry might signal that you can _definitely_ afford the full asking price.

That's a good reason to ditch the suit and tie and dress casually — that is, if you intend to negotiate the price.

Once you've taken a look at the place, and if it seems promising, it's time to start asking questions. The first important thing to ask is how long the house has been on the market; this is a key indicator of its value.

After all, while it depends on the market you're in, if the property has been on offer for longer than a few weeks, it might be overpriced, which gives you leverage in negotiations. Another important question is to determine the cost of utilities like electricity, water and gas. This is especially important if they're unusually high and your budget is already being stretched thin.

Once you've gotten these questions answered, you'll want to take a more careful look around. Specifically, you want to look out for any signs of past damage, which means looking for water issues, especially in the attic and basement.

For instance, if you see moisture on the walls, you might be looking at a costly repair and a struggle to get insurance coverage. Additionally, you'll want to check out the furnace and air conditioning as they might be old and obsolete, adding another expensive repair or replacement.

And finally, ask about the neighbors. This one is crucial because neighbors _can't_ be fixed or replaced. Take a peek next door and get a sense of their noise level and general attitude.

### 6. Try to identify potential problems yourself and hire an independent inspector to follow up on them. 

Hypothetically speaking, if the home you're about to move into was once a drug lab, would you want to know?

Well, while such information disclosures can be helpful, you should tend to approach them skeptically. In the United States, sellers are legally obligated to let you know about any important facts concerning a property, especially ones that may negatively affect its value. However, depending on where you live, such legal disclosures will be more or less detailed.

For instance, California requires sellers to let a potential buyer know about everything from a leaky roof to any deaths that occurred on the property within the previous three years!

But you can't expect the same amount and quality of information in all states. As a result, these disclosures remain an incomplete and uneven source.

For this reason, independent inspectors can and should be assigned to give a place a thorough run-down. Sellers sometimes conduct expert reports on houses, but you shouldn't trust these blindly.

Such reports might be months old and might not include new problems that _you_ can discover. You should only trust professional inspectors that you personally commission.

But before you hire your own inspector, it's a good idea to try to spot big issues yourself. If you can identify major problems, you'll be able to get the right expert to assess the severity of the situation.

Say you find a house you want to buy. You should first check to see if the roof sags; if it does, you should hire an expert roofer to determine any potential foundation issues or whether the roof simply needs replacing.

Similarly, if during your initial visit you turn the faucets to their maximum and notice rust-colored water or generally low pressure, a plumber could be called in to see how old the pipes are, how likely they are to leak and whether they need to be replaced.

### 7. To avoid scams, look past the glossy finish applied by realtors. 

Real estate agents are experts at making total dumps look like lovely homes. You should always be on the lookout for the traps they set.

In fact, to get the maximum return on a property, sellers and their advisors will gladly give a home a quick and superficial makeover. The most common industry trick is to empty the place out and rent furniture, artwork and flowers to dress it up before you visit. It may be shocking, but these simple additions, known as _staging_, can increase the selling value by tens of thousands of dollars.

But don't worry, it's easy to spot this trick by looking for rooms that have less furniture than they actually need, or if there are few functional objects and lots of decorative ones. For instance, in a laundry room you should see detergents and cleaners, not just loads of flower vases.

And pay special attention to lamps. Turn them all on to see if they actually work or if they were just placed decoratively — maybe even in corners with no electrical outlets.

Another common situation to walk into is one where the owner never lived in the place, but just remodeled it to sell for a profit. This is called _flipping houses_ and it might present a situation that seems financially attractive to you.

For example, the owner might have laid a new floor or installed a brand new air conditioner, which means you'll dodge those future expenses. But since the owner never lived in the place, he might not have had a huge incentive to do great work. As such, it's hard to know for sure that the condition of the home is as perfect as promised.

In situations like this, you'll want to bring in a real estate inspector. His job is to appraise such situations and make sure that all the permits necessary for the remodel were properly issued. After all, you don't want to discover after closing on a house that your swimming pool was installed illegally.

### 8. Professional inspections are essential before making an offer. 

Do you have reservations about hiring an inspector to assess a property you're in love with? Well, you shouldn't — inspections are par for the course. In fact, purchase contracts often include a clause that states the buyer can pull out of the deal if he's displeased with the findings of an inspection.

That's why most buyers set up a general inspection to get an overview of a house's condition. There's also a secondary inspection often required by the mortgage lender: a pest inspection. That being said, this isn't a legal obligation. For instance, in the United States, the buyer can decide the number, type and extent of the inspections they want to order.

In fact, there isn't really a "standard" inspection. That means the price of one can vary from $200 to $600 depending on the size, sale price, age and other characteristics of the property.

So, to avoid an unpleasant surprise, it's best to specify your expectations. For example, if you want the inspector to get under the house and look at the foundation, you should confirm it beforehand and be prepared to pay an additional fee.

But it's also important to check the professional qualifications of the inspector you hire. After all, you don't want to bring someone in that isn't licensed to do the job you're hiring them for.

And remember, you can't rely on your inspector to check absolutely everything. What you _can_ do is make sure he does a thorough job. Your average house is made up of 60,000 pieces, many of which are hidden by others.

So, if the seller has copies of past inspections, it's a good idea to use them as a guide. You can bring them to the inspector and can plan on spending time with him while he looks things over.

If he takes an hour and claims to have checked the whole place, he might well have rushed and missed an important detail. It often takes at least a few hours to get a truly complete overview.

### 9. If you like the place, it’s time to make an offer. 

Assuming you've found a place you like, that's within your price range and for which you've been pre-approved for a mortgage, you're ready to make an offer. Naturally, the trickiest part of this is deciding on how much.

To decide on this magic number, you have to strike the right balance: if you offer less than the seller expects, you'll face rejection; if you offer too much you'll be overpaying.

So, to find a happy medium, you need to consider whether the market is _hot_ or _cold_. A hot market is one that's lively and a cold market is one that's stalling. In a hot market it's common to see sellers set very low prices and let potential buyers bid each other up.

For instance, the San Francisco Bay Area is generally considered a hot market because of the Silicon Valley tech boom. In this market, demand is so high that buying a house means making an offer that's well beyond asking, just to beat out the competition.

On the other hand, in a cold market you can often safely offer a price that's below asking that the seller will accept, as they're desperate to sell the property.

But the market isn't all you should consider; the seller's personal position is also relevant when deciding on a price. So, it's in your interest to learn as much about her current situation as possible.

For example, if the seller just inherited the house, doesn't live in it and just wants to get rid of it, you can expect her to take a lower price. Or, if the house is an investment for its owner and rents are falling, she might also be willing to part with it at a discount.

### 10. Final summary 

The key message of this book:

**If real estate isn't your strong suit, buying your first home can seem like a scary and insurmountable task. But, as long as you do your homework, it doesn't have to be. By gathering as much information as you can about both the property and seller, you're sure to get the home of your dreams for the right price.**

Actionable advice:

**Don't rush into a deal.**

Say one day, when you're writing out your rent check, you suddenly realize that you're throwing money away and that it makes much more sense to invest in a home. That may well be true, but your realization shouldn't make you run out and buy the first house you can find. Realistically, you want to allow yourself six months to a year to study the market, neighborhoods and purchasing process. Just remember, the average home buyer looks at 10 to 12 houses before making a decision.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** **_Zillow Talk_** **by Spencer Rascoff and Stan Humphries**

_Zillow Talk: The New Rules of Real Estate_ (2015) gives the reader all the tools they need to buy, sell or rent a home. From the conundrum of whether to rent or buy, to when to sell and how to boost the value of your property, these blinks shed light on the perennially important dilemmas of real estate — the biggest investment of your life.
---

### Ilona Bray, Alayna Schroeder, Marcia Stewart

Ilona Bray is an attorney and the author of several books on real estate, including _The Essential Guide for First-Time Homeowners_ and _Selling Your Home: Nolo's Essential Guide._

Alayna Schroeder is a California-based lawyer, with legal experience ranging from corporate law to the US Peace Corps.

Marcia Stewart is the author and editor of multiple Nolo real estate books, including the best seller _Every Landlord's Legal Guide._

