---
id: 5398570a37373100071a0000
slug: buyology-en
published_date: 2014-06-10T13:24:28.000+00:00
author: Martin Lindstrom
title: Buyology
subtitle: Truth and Lies About Why We Buy
main_color: F7D631
text_color: 786818
---

# Buyology

_Truth and Lies About Why We Buy_

**Martin Lindstrom**

Day in and day out we're bombarded by thousands of brand images, logos and commercials enticing us to buy their products. However, only _some_ ads actually motivate us to whip out our wallets. Why? Using cutting-edge neuromarketing methods, _Buyology_ answers that question and explores the hidden motivations behind our purchasing decisions.

---
### 1. What’s in it for me? Understand what makes you buy what you buy. 

What was your last big purchase? Do you remember _why_ you bought it? Why you chose that particular brand over the others? How rational was your decision-making process?

If we're honest with ourselves, we admit that our purchasing decisions often aren't based on a rational comparison of a product's pros and cons, but rather a gut-level decision we can't quite explain. So what exactly motivates us to buy?

And why is it that some people prefer Pepsi to Coke or Ruffles to Lays? While we might chalk up the distinction to mere personal preferences in _taste_, a closer examination reveals that the answer is a lot less cut and dry, and goes a lot deeper.

_Buyology_ examines what's actually going on in our brains that causes us to open or close our wallets. It'll show us that current methods employed in market research, such as questionnaires, don't necessarily work because what _we_ say we want and what our _brains_ say we want are often at odds.

Instead, marketers should look to _neuromarketing_ — i.e., marketing based on information we get from sophisticated neuroimaging machinery — in order to produce the best marketing strategies possible.

In these blinks, you'll learn

  * whether subliminal advertising _really_ works,

  * why sex _doesn't_ sell,

  * what Oreos and the Catholic Church have in common and

  * why there's simply no way of grossing out smokers enough into kicking the habit.

### 2. Mirror neurons influence our buying decisions without our control. 

Why is it that whenever someone else yawns we have the uncontrollable urge to do the same? Or why is it seeing someone else's beaming smile puts a smile on our face as well? It all comes down to _mirror_ _neurons_.

In 1992, scientist Giacomo Rizzolatti conducted a study on a type of monkey called macaques, and was astonished to find that the animals' premotor neurons lit up _both_ when reaching for a nut _and_ when watching another macaque do the same.

These are mirror neurons at work, and evidence even suggests that the regions of our brains that contain mirror neurons are _equally_ stimulated whether we're performing an action ourselves or merely watching someone else do it.

Essentially, we reenact in our minds whatever we observe others doing.

Companies seek to exploit our mirror neurons with their advertising to entice us to buy. Because our mirror neurons respond to "targeted gestures," i.e., images of someone sipping a soft drink or lacing up a new shoe, this makes them invaluable to marketing. The attractive models on Abercrombie & Fitch bags, for instance, trigger our mirror neurons with the promise of a perfect body.

But mirror neurons don't always act alone: they often work in concert with dopamine, a pleasure hormone, in order to create the happy feeling that causes us to buy. This is why we feel so great after a little "retail therapy," even if that rush of dopamine causes us to buy more than our rational minds would otherwise allow.

The rush we get from making purchases can be explained by our evolution. We consider purchases to be an indicator of an increase in social status, which in turn increases our reproductive chances. Thus, our bodies' survival instinct floods our brains with dopamine to urge us to buy the latest car model or a fancy new handbag in order to increase our chances at reproduction.

### 3. Somatic markers affect the way we perceive products. 

Imagine you're at the store buying peanut butter. Do you choose Skippy, Jif or Peter Pan? Most likely, you decide without a moment's hesitation — though you probably can't explain why.

This is a result of our brains' _somatic_ _markers_, or shortcuts that trigger automatic responses. Whenever we make purchasing decisions, our brains process a plethora of thoughts and ideas, and condense them all into a single response. Rather than start the process anew each time we're faced with the same decision, our brains create shortcuts based on a lifetime of experiences.

In fact, in one recent study, a German research group discovered that more than 50 percent of our purchasing decisions are based on spontaneous — and therefore unconscious — reactions. That's because our brains have already created our decision-making map with somatic markers.

Interestingly, somatic markers cause us to prefer certain brands over others. For example, researchers found that consumers' preference for Andrex over Kleenex toilet paper was linked to Andrex's labrador puppy mascot. While it might seem strange, cute puppies make us think of a young family, and even toilet training, and therefore strengthen the brand with these conceptual links.

This also explains why we often turn to German-made kitchen appliances when faced with a purchasing choice: we associate Germany with technological excellence.

Unsurprisingly, somatic markers are an effective marketing tool. Advertisers often try to create associations between two wildly different things in order to encourage our somatic markers.

The author cites the power of paint color as an example. In order to aid a struggling bank, the author convinced its manager to paint his institution — and everything in it — a vibrant pink color. About three months later, business was booming. Why? Customers associated that pink color with their childhood piggy banks.

> _"All the positive associations the subjects had with Coca-Cola (...) beat back their rational, natural preference for the taste of Pepsi."_

### 4. Marketers increasingly use fear to sell us their products, and it works. 

Although it's certainly cunning in a sense, appealing to our mirror neurons or ingrained somatic markers is relatively harmless. Unfortunately, the same can't be said for _all_ marketing techniques, some of which exploit our negative emotions to make us buy more.

In fact, fear can be an extremely persuasive marketing technique. When we are stressed or afraid, we seek solid foundations and pleasant experiences — perhaps in the form of purchases — in order to get the sweet rush of dopamine that accompanies it. The dopamine, in turn, causes us to want to shop more.

For example, if a particular brand of lingerie or shaving cream can ramp up the fear of dying alone while simultaneously offering us an immediate solution to this fear, then we're more likely to buy it.

A particularly poignant example of this principle in action can be seen in the 1964 presidential candidate Lyndon B. Johnson's iconic "Daisy" commercial, in which a child plays with daisies right before a nuclear explosion erupts behind her. The association is obvious: vote Johnson or perish in nuclear war.

In order to study the effectiveness of this commercial, political strategist Tom Freedman recently examined voters' _amygdala_, the part of the brain that controls fear, as they viewed the commercial. The results? A noticeable increase in activity in the amygdala. It should be no surprise that Johnson won that election.

What's more, fear-based somatic markers can even associate certain products with the _absence_ of negative experiences. For example, diet pills and computer security software create the link between the absence of their products with undesirable consequences, thus enticing us to buy in order to avoid negative experiences.

More famously, Johnson & Johnson's _No_ _More_ _Tears_ _Baby_ _Shampoo_ promises to help avoid the painful childhood memories of burning eyes in the tub. And who'd want to risk burning their baby's eyes?

### 5. Subliminal messaging is used quite often in marketing and it induces us to buy. 

Paranoia surrounding _subliminal_ _messaging_ — i.e., the use of visual, auditory or other sensory messages that are picked up only by our subconscious — has been rampant since 1957, when the first subliminal advertising was employed in a startling study. Despite the fact that the study was outed as a fake, the National Association of Broadcasters nevertheless banned subliminal messaging.

However, if subliminal messaging includes _anything_ that subconsciously entices us to buy a product, then it's still very much present in modern marketing. Consider, for example, the sweet smell of freshly baked cookies wafting from the kitchen of the house you're viewing, or that new car smell while taking a car for a test drive, or Gershwin's piano rolls playing in the background as you shop around for a new suit. These forms of sensory stimulation all elicit explicit subconscious responses.

Less subtly, Philip Morris, the company that owns the tobacco brand Marlboro, actually _pays_ bars to outfit their decor with color schemes, ashtrays and other symbols alluding to the Marlboro logo.

So these subliminal messages are everywhere, but do they actually work? Neuromarketing studies show that they do. One recent experiment found that something as simple as briefly seeing happy or grumpy faces can affect how much we're willing to pay for a product.

In the experiment, test subjects were shown one of the two faces, and then asked to pour themselves a beverage and decide how much it was worth. Those who had been shown the beaming, happy faces poured significantly more and were willing to pay double that of those who had been shown the frowning faces.

These results suggest that even something as small as being greeted with a smile by your cashier can have a remarkable impact on sales.

> Good marketing appeals to your subconscious.

### 6. Counterintuitively, disclaimers and health warnings actually increase sales. 

Gone are the days when doctors recommend their favorite brand of smokes to their patients. Instead, whenever smokers stop into the convenience store to buy cigarettes, they're met with health warnings ranging from honest to downright gory. Yet, in spite of these warnings, approximately 15 _billion_ cigarettes are sold each day throughout the world.

Knowing this, we have to ask ourselves: Do these disclaimers even work?

In short: no, not really. In fact, it seems that warning labels on cigarettes actually have _zero_ effect on subduing smokers' cravings. In one study, volunteers were exposed to images of cigarette warning labels, then asked to rate their urge to smoke. According to brain scans of the volunteers, the warnings did nothing to affect their cravings on a neurological level.

In fact, warning labels themselves actually work _against_ their intended purpose. That same study found that cigarette warnings, rather than simply repulsing people, actually stimulated the brain's "craving spot," or _nucleus_ _accumbens_.

The author and his research team set up a similar experiment in which they showed a particularly repulsive anti-smoking ad to a group of volunteers. In the ad, a seemingly gregarious group of people sits together smoking cigarettes, but their cigarettes emit globs of greenish fat instead of smoke. The fat oozes onto their clothing, the table and the floor, but the group doesn't seem to notice at all.

The point is clear: smoking causes nasty wads of fat to circulate through your bloodstream, clogging your arteries and damaging your health. Yet, surprisingly, this disgusting ad did not repulse smokers into eschewing their bad habit. Instead, their brains focused on the friendly atmosphere of these sociable conversationalists, and their desire for a cigarette actually _increased_.

So, rather than _discouraging_ the habit, cigarette health disclaimers actually _promote_ it!

### 7. Strong brands use some of the same strategies as major religions to ensure loyalty. 

What do Coca-Cola and the Catholic Church have in common? The answer may be quite surprising: they employ many of the same strategies for creating — and maintaining — loyalty.

For one, brands associated with quasi-religious rituals are _stickier_ than other brands, i.e., they help us form an emotional connection to the brand.

Consider the Oreo cookie. Some like to pull the cookie apart and lick the filling from the middle while others prefer to dunk the whole thing in a glass of milk. Either way, everyone has their own ritual for eating the cookies, and thus Oreo itself has become as much a ritual as it is a cookie.

Furthermore, strong brands, like major religions, profess their personal mission that distinguishes them from others. IBM's mission, for example, is to provide "Solutions for a Small Planet." Bang & Olufsen, on the other hand, claim to have the "courage to constantly question the ordinary in search of surprising, long-lasting experiences."

Also, like religion, brands produce an "us vs. them" mentality to ensure brand loyalty. Whether it's Coke vs. Pepsi or Visa vs. Mastercard, strong brands define themselves by creating a contrast with their competitors. This strategy, while perhaps controversial, indeed attracts fanatic followers and makes us loyal.

Brands also use iconography in the form of logos similar to those found in religions. Consider Nike's "swoosh" and McDonald's "golden arches": these logos form powerful associations between a product and its symbol, not unlike the religious feelings we associate with images of an angel or a crown of thorns.

In fact, we even find similarities between strong brands and religious references in the way our brains respond to these messages. For example, one neuromarketing study found that the volunteers' brain activity while viewing images from strong brands — such as iPod, Harley-Davidson, and Ferrari — and religious icons was nearly identical. This suggests that our emotional engagement with a strong brand closely resembles our spiritual attachments.

### 8. Do sexual references in advertising work? Not the way we think they do. 

Surely you've heard that "sex sells," and the prevalence of sex in advertising certainly suggests that it's true. Consider a National Airlines commercial, in which a sexy stewardess promises that she'll "fly you like you've never been flown before." Too subtle? Okay, consider the perfume brand Vulva, which claims to have captured a "beguiling vaginal scent."

Imbuing advertisements with sexual references is an age-old and thriving technique, but is it effective marketing?

In short, no. Sexual references don't actually make a product more marketable. This was made clear by one study in which two groups watched different shows punctuated by commercial breaks — one watched sexually explicit scenes from _Sex_ _and_ _the_ _City,_ while another watched the distinctly unsexy _Malcolm_ _in_ _the_ _Middle_. Surprisingly, the _Sex_ _and_ _the_ _City_ viewers were _less_ likely to remember the advertisements than those who had watched _Malcolm_ _in_ _the_ _Middle._

In fact, sexual content in advertising effectively blinds viewers to actual products. MediaAnalyzer Software & Research conducted a study in which they showed print ads — ranging from highly sexually suggestive to incredibly bland — to a group of volunteers, who were then asked to indicate where their eyes naturally migrate on the page. Unsurprisingly, the sexual content garnered most of the attention, but it came at a price: viewers actually ignored brand names and logos.

The study dubbed this phenomenon the _Vampire_ _Effect_ : these racy images were sucking attention away from the actual advertisement.

However, some sexual content is indeed good marketing, but mostly for its shock value and surrounding controversy. Take American Apparel, for example, whose ad campaigns are characterized by young models in seductive poses. Although the company has been criticized for pornographic and degrading content, their sales have never been higher.

Even here, it's not the _sex_ itself that sells, but rather the _controversy_ surrounding it. There's no such thing as bad publicity!

### 9. Neuromarketing has the power to fundamentally change the way we conduct market research. 

As we've learned, most consumer choices are unconscious, making traditional surveys an inadequate tool in market research. However, by using neuromarketing, companies can better predict a product's success.

In one study, volunteers watched the shows _Quizmania_, _The_ _Swan_ and _How_ _Clean_ _is_ _Your_ _House?_, and were then asked to rate the likelihood of watching them again later. Traditional questionnaires showed that _Quizmania_ was least likely to be watched, while _The_ _Swan_ and _How_ _Clean_ _Is_ _Your_ _House?_ were neck and neck.

Brain scans, however, told a different story mirroring the shows' later performance: _How_ _Clean_ _Is_ _Your_ _House?_ enjoyed the most success, followed by _Quizmania_ and, finally, _The_ _Swan_.

Furthermore, neuromarketing can help rid marketing of techniques that are ineffective or backfire. For example, had Nationwide Annuities had access to neuroimaging data, they might have reconsidered airing a commercial that featured Kevin Federline, Britney Spears' ex-husband, working a shift at a fast-food restaurant.

The tagline read _Life_ _comes_ _at_ _you_ _fast_, implying that you should invest with Nationwide Annuities, lest you too go from riches to rags. Actual neuroimaging data from fMRIs, however, showed that the commercial was actually _scaring_ _away_ potential customers.

Finally, neuromarketing enables companies to discover their customers' true motivations and thus adapt their products and marketing techniques accordingly. In one study, volunteers were asked to rate their enjoyment of various wines. The catch, however, was that one of the wines was presented twice — once with an expensive label and once with a cheap one.

According to brain scans, brain activity flared up in the _medial_ _orbitofrontal_ _cortex_ — the part of the brain in which pleasure is perceived — when participants were presented with the expensive wines.

This study suggests that a higher price can actually _increase_ our enjoyment of a product even when everything else about it remains the same. So, if you want to sell more wine, it may actually be worth it to up the price!

### 10. Final summary 

The key message in this book:

**We** **are** **often** **totally** **unable** **to** **accurately** **judge** **our** **purchasing** **decisions,** **as** **most** **of** **these** **decisions** **happen** **unconsciously.** **In** **order** **to** **better** **understand** **consumers,** **marketers** **will** **need** **to** **look** **directly** **into** **the** **brain** **itself** **by** **employing** **neuromarketing** **research** **techniques.**
---

### Martin Lindstrom

Martin Lindstrom is a Danish writer and branding expert, and was listed as one of _TIME_ magazine's 100 Most Influential People in 2009. In addition to consulting top firms, Lindstrom writes for _FAST_ _Company_ and _TIME_ magazine, and has authored numerous other best-selling books.

