---
id: 5734586de2369000032901e8
slug: campaigns-that-shook-the-world-en
published_date: 2016-05-17T00:00:00.000+00:00
author: Danny Rogers
title: Campaigns that Shook the World
subtitle: The Evolution of Public Relations
main_color: 20A092
text_color: 177368
---

# Campaigns that Shook the World

_The Evolution of Public Relations_

**Danny Rogers**

_Campaigns that Shook the World_ (2015) tells the history of public relations through a series of groundbreaking campaigns, work that inspired a revolutionary shift in communications. These blinks will show you how to create a campaign that packs a serious punch and leads your company to success.

---
### 1. What’s in it for me? Learn why PR is still relevant and effective in today’s internet age. 

If you read a magazine, flip through a newspaper, surf the internet or even chat online, you've no doubt come across a pithy message from some kind of communications campaign. They are everywhere in the public sphere. But some are quickly forgotten while others stick in our heads for years. Why is that?

Great PR tells a compelling story about a brand that speaks to who we are and what we desire. These blinks select real-life examples to explain the various tricks and tools of the PR trade, so you too can build a campaign that will shake the world and tell a story no one will ever forget.

In these blinks, you'll discover

  * how Margaret Thatcher's aggressive campaign won voters over;

  * how Dove started a conversation about self-esteem while selling beauty products; and

  * why the Rolling Stones chose Jovan Musk over sex and drugs.

### 2. Traditional PR strategies have been turned upside down by a revolution in digital communications. 

Many people think that public relations professionals are shadowy spin doctors who use their manipulative talents to convince the media and consumers of things they wouldn't otherwise believe.

But how true is this statement? In reality, this cliché about PR is long outdated.

The ubiquity of social media and internet communications mean that creating outrageous political or corporate spin is difficult to do without being called on it. Facebook, Twitter and other social media platforms give users the power to create and influence the flow of information, essentially reducing the control business or political leaders have over a brand or message.

Just imagine if the events described in the fairy tale, the Emperor's New Clothes, were to take place in the digital era. All it would take to unmask the sham would be a single tweet, gone viral!

In our information age, any hidden truth will eventually be revealed, one way or another.

So what does this mean for today's brands?

It's a good thing. With no choice but to be honest, even influential figures such as the British Royal Family have found ways to boost their public image. Rather than cultivating the polished but stiff persona of decades past, today's Royals are authentic and charming.

PR teams are also grappling with radical change. Consider the difference between Tony Blair's 1997 election campaign and Barack Obama's in 2008. While Blair's team focused on currying favor with print newspapers, Obama's group harnessed the power of digital channels, including online ads, YouTube videos, Facebook posts and emails, to reach voters — particularly those younger than 25 years old.

This example highlights a power shift from editorial media to shared media via digital services, from social media to blogging. But the shift also begs the question: will it threaten the relevance of public relations?

> _"Yes, this book does confirm the demise of 'spin', but even PR, as most people know it, may itself be on a life-support machine."_

### 3. More media platforms mean even more avenues through which campaigns can communicate. 

As you flip through a magazine in a doctor's waiting room, you might notice a QR code placed in an ad inviting you to visit a website. A separate article might tout the latest online videos or tweets from politicians and celebrities alike.

Forms of media once separate now mix and blend in a large melting pot of multimedia communications. Paid advertising mingles with editorial content, while social media messaging has broken down all boundaries. The name for this phenomenon is _converged media_.

A great example of converged media is Dove's _Evolution_ video, released in 2006. The video addressed society's warped perception of beauty as a result of photoshopping in ads and editorials.

The video went viral, causing widespread debate and receiving extensive media coverage. Dove's video even won an award at the Cannes Film Festival in France the following year.

It's not just types of media that are converging but also the people using such media. Modern campaigns are blurring the boundaries that previously separated political and commercial messaging from entertainment.

Former British Prime Minister Margaret Thatcher's 1979 election campaign was the first of its kind to use emotive messages to sway voters instead of purely rational arguments, typical of political messaging at the time. While she did receive criticism for attempting to "sell politics like soap powder," her approach certainly had a hand in her winning the election.

Rolling Stones frontman Mick Jagger, in contrast, marketed his band's 1982 world tour just like "a presidential-style media campaign," said Jagger's PR guru, Alan Edwards. The musician held formal media press conferences in Europe, a total of ten meetings in two days!

While converged media offers the benefits of synergy and broader communications opportunities, it also poses challenges, as campaigns become more complex to manage.

> "PR [...] has almost totally converged with other communications disciplines."

### 4. Despite a tech revolution in communications, public relations is still relevant in brand marketing. 

So in the wake of such revolutionary changes, what is the state of public relations today?

PR is certainly not irrelevant, as it still can expand the reach of a brand campaign for little cost. A single, well-placed news article can bring your brand message to a town, nation or even worldwide. What's more, such placement is practically free.

In short, a clever PR strategist can generate cost-effective, positive publicity for a brand.

As part of her 1979 campaign, Margaret Thatcher's team purchased just 20 billboards across England. Sure, the campaign could have purchased more, but it turns out that they didn't need to.

Why was this the case? A lively discussion in the press of the campaign slogan, "Labour isn't working," brought the campaign successfully into millions of households — for free.

Similarly, the PR team behind the London 2012 Summer Olympics used calculated strategies to boost ticket sales. In doing so, they were able to sell 11 million tickets well within the timeframe the campaign had set.

Good public relations is not only cost effective but also offers an additional strength: storytelling.

Integrated, converged media campaigns can be a challenge to manage, but if your communications team starts with a strong narrative, the story practically sells itself. The London Olympics team was able to create a powerful, cohesive narrative over a six-year campaign, before the games began.

How did they do this? The team chose five positive themes, from engaging young people to presenting London culture in a positive light. They then ensured every single media release and any messaging was tied to at least one of these five themes.

Today the concept of storytelling — the foundation of great public relations from the very beginning — has become a buzzword within the communications industry.

### 5. Winning campaigns balance an authentic message with a strategic focus. 

Before you begin to prepare your project, you need to understand the two fundamental elements of any successful public relations campaign: _authenticity_ and _strategy_.

_Authenticity_ is a must-have in any winning campaign. Presenting your product or personality authentically trumps by a longshot any meticulously molded but essentially artificial image.

Consider former British Prime Minister Margaret Thatcher. Although she came across to the electorate as aggressive, this didn't harm her appeal. Why? Because she truly was an aggressive individual, tough and stubborn! This gave her campaign a sense of honesty, which voters admired.

At the same time, your campaign needs to be _strategic_. But would this undermine an authentic, honest approach? It might, but doesn't have to. Strategy ensures your campaign reaches the audience you want, at the precise moment you need it to.

Let's say you're looking for a romantic partner. Your success is almost wholly dependent on the decisions you make before you put yourself out there, such as selecting a dating website that matches your lifestyle and demographic. An athletic millennial looking for someone who enjoys club music won't have much luck on a dating site for single retirees!

Now let's imagine that this same millennial finds a website that suits his demographic and quickly finds a potential partner. That is, until he lets slip that he had a stylist do his profile picture and a ghostwriter provide his personal description!

In other words, being strategic is crucial — as is keeping your strategy _behind the scenes_. Successful campaigns can balance strategy and authenticity to create a cohesive, appealing and foolproof brand image.

### 6. Complement and combine media channels so your campaign can make a powerful impact. 

The strongest tool for any modern campaign is the intelligent use of digital media.

Some tools allow direct communication between companies or personalities and a target audience, while others create millions of word-of-mouth advertisers ready to spread your message among their fans and followers.

During the 2008 election campaign, Barack Obama's PR team knew how to use these tools well, as they worked to create a grassroots movement of dedicated, first-time voters.

The team combined messaging on Facebook, videos on YouTube, emails, text messages and online advertising with traditional outreach tools, such as radio advertising and phone calls, as well as old-fashioned knocking on doors.

The result? Some 13 million supporters banded together to spread the word and eventually, bring Obama to the White House. This goes to show just how effective delivering a message through a range of channels can be.

Ideally, a message's strategic placement can inspire a chain reaction, spreading it through more channels and reaching more people. In Dove's 2004 Campaign for Real Beauty, the company displayed billboard ads displaying curvy women, with an innovative interactive component. Each billboard included a live-updating poll that tallied the public's votes via text message on whether the model was "fat" or "fit."

With its campaign, the Dove brand simultaneously inspired an inquiry into the public's perception of beauty, while inspiring a lively debate in the international press.

Importantly, the value of the resulting extra publicity was 30 times the costs of purchased media space!

### 7. Strategic partnerships, whether with stars or the press, will take your brand story to the next level. 

Iconic brands such as athletic gear maker Nike and drinks giant Pepsi didn't make it to the top alone. These companies have basketball great Michael Jordan and pop legend Britney Spears to thank for boosting their brand internationally.

Celebrity endorsements offer an effective way to supercharge your brand. But such endorsements are more than just a lucrative PR opportunity or extra revenue for Hollywood stars.

A mutually beneficial partnership between a brand and a personality, organization or even another brand can be a valuable addition to the _story_ your campaign is telling.

To ditch their bad-boy image in the 1980s, the rock group Rolling Stones entered into partnerships with prestigious but non-controversial brands, such as Jovan Musk and TDK cassette tapes.

The message? Sex, drugs and rock and roll were "out," and clean living and a professional reputation were "in."

David Beckham's endorsements also reflect an attempt to position the star footballer as a global icon. Beckham's PR advisor Simon Fuller ensured that Beckham signed deals only with A-list brands, such as Pepsi, Adidas and Armani.

But brands and celebrities aren't the only partnership opportunities out there — a mutually beneficial relationship with the press can work wonders for your campaign.

British Prime Minister Margaret Thatcher proved this by forging strong personal relationships with leading journalists, who in turn rewarded her with positive coverage without her even having to ask.

Entertainment PR whizz Alan Edwards used the same strategy to generate positive press for the Rolling Stones and David Beckham.

### 8. Brands that fight for a meaningful cause won’t succeed unless the campaign demonstrates integrity. 

As new technologies continue to shape the art of communication, so too do new audience members. Today's young people, for example, are highly conscious of how and why they buy.

Inspired by today's conscious consumption trends, younger people buy from brands that don't just have appealing marketing but also demonstrate social, environmental and ethical value.

By linking your brand with a worthy cause, you can turn your campaign into a movement of its own.

Dove again provides a great example. While on one level, the company was advertising its range of beauty products, on another level it was challenging mass media's representation of women and the modern conception of beauty.

In doing so, Dove told a story of its brand as one that is relevant _and_ truly cares about critical issues facing society.

But even the most moving PR campaigns are liable to backfire if they don't also show integrity.

Even Dove's campaign faced calls of hypocrisy. Its parent company, Unilever, also owns Axe and Lynx, two brands notorious for overtly sexist marketing. This seeming duplicity undermined the credibility of Dove's campaign.

Our hyperconnected world means any brand story is subject to intense public scrutiny. Companies that simply say they have values won't win; only firms that honestly live those values will.

In other words, you've got to walk the talk to succeed!

### 9. A brand can’t keep consumers in the dark. A living brand needs to engage buyers and be honest. 

In earlier decades, all a company had to do to turn someone into a customer was come up with a great marketing campaign and watch as people walked right into the trap.

This is no longer the case. While brand marketing is still important, customers today are some of the most effective brand ambassadors.

The meteoric rise of social media has turned online personalities into engaging brand storytellers. Think of all the online forums that discuss the pros and cons of just about any product, from cameras to insurance.

How often do you google a product before making a purchasing decision? Getting a second opinion has become second nature, and now, the advice of real friends can be accessed in real time via Facebook chat or other platforms.

It goes without saying that brands able to successfully seduce net-savvy customers will reap the benefits of this word-of-mouth marketing. Yet if you want to take your millennial customer base to the next level, however, then you'll need to bring your brand to life.

_Living brands_ make positive contributions to society, engage stakeholders respectfully and justify decisions eloquently and confidently in public. If your company fights for a cause, people will certainly consider your efforts worth talking about, whether on social media or between friends.

Chipotle's "Food with Integrity" campaign, for example, highlighted the negative impact of industrial farming on animals, farmers and the environment. The US-based Mexican restaurant franchise combined this campaign with a general promise of using healthy, sustainably-produced ingredients in its stores, a decision that has boosted the company's business overall.

Chipotle as well as other companies know that sweeping issues under the rug or keeping consumers in the dark is no longer an option. Brands must deliver all that they promise and make a worthwhile contribution to society, too.

> "Organizations must stop simply talking the talk and start walking the walk."

### 10. Final summary 

The key message in this book:

**Public relations campaigns that are focused only on making a client look good are long outdated. Today, PR needs to tell a brand story that reflects the good a company does not only for the customer but also for society. Campaign strategies driven by authenticity and integrity, and boosted through the savvy use of multimedia and partnerships, will make your PR campaign a winning one.**

Actionable advice:

**Communicate strategically.**

As an effective PR professional, everything you say has a reason. Stay aware of your objectives to create messages that help you reach your stated goals. Ask yourself: What is the target group? What are the group's expectations? How can you reach out to this group? This strategy can also apply to other situations, such as asking for a promotion or raise.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading: Newsjacking** **by David Meerman Scott**

_Newsjacking_ (2012) is about the best new way to get media attention — not by planning ahead but by reacting quickly and cleverly. These blinks not only explain how this new form of generating news works, but also how you can and should react to it, and how to use it to attain the best possible media coverage.
---

### Danny Rogers

Journalist Danny Rogers is a media and marketing expert, reporting for over two decades on communications issues. He has worked as an editor with publications such as _Campaign_, _Media Week_, _PRWeek_ and _Brand Republic Group_.

