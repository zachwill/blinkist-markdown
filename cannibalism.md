---
id: 59930cbeb238e100050b8918
slug: cannibalism-en
published_date: 2017-08-18T00:00:00.000+00:00
author: Bill Schutt
title: Cannibalism
subtitle: A Perfectly Natural History
main_color: 966745
text_color: 755F2E
---

# Cannibalism

_A Perfectly Natural History_

**Bill Schutt**

___Cannibalism_ (2017) offers a scientific, historical and cultural approach to the understanding of, well, cannibalism. These blinks explain why animals eat their own, why it's become so taboo among humans, and why it could come back.

---
### 1. What’s in it for me? Get a taste of how cannibalism works in animals and humans. 

Cannibalism — it's everywhere! Consider the vampire and zombie craze in movies and TV, with _The Walking Dead_ becoming the most-watched cable TV show of all time, or the wild success of cult horror films like _The Silence of the Lambs_, starring the liver-loving murderer Hannibal Lecter. In our language, too, we have many references to cannibalism — a woman who uses men for sex is a "man-eater"; "eating someone" is popular slang for performing oral sex, and even babies aren't spared — we like to say they're so cute we could just "eat them up."

Despite all this, cannibalism is in many ways one of our last taboos. So let's take a look at the history of human cannibalism and how our views have changed throughout history, but also examine how it works in other animals. It might be less strange than we think.

In these blinks, you'll also find out

  * why cannibalism has evolutionary benefits;

  * how we can guess at the taste of human flesh; and

  * which cannibalistic practices are performed by quite a few humans today.

### 2. Most people consider cannibalism appalling and unnatural, but research shows it’s quite natural. 

Certain things are absolutely taboo in most human societies, and the images you conjure up when you hear the word "cannibalism" are likely pretty loaded. But this practice has an interesting place in human history and deserves a closer look.

Simply put, _cannibalism_ is defined as an individual consuming all or part of another individual of the same species. It includes behavior like scavenging and even certain reproductive processes through which tissues, such as the skin or uterine lining, are consumed.

However, despite the centrality of such practices, until recently cannibalism was considered highly abnormal in nature. It was thought only to arise from extreme conditions like starvation or captivity.

This conception transformed in the 1970s with new research done by Laurel Fox, an ecologist at the University of California at Santa Cruz. Fox discovered that cannibalism is a perfectly normal response to all kinds of environmental factors.

She also found that it's much more widespread than had been previously believed. Cannibalism occurs in every major animal group — even those thought of as herbivores, like butterflies.

However, this behavior depends on a variety of factors from population density to shifts in environmental conditions.

Cannibalism is common in _nutritionally marginal areas_ that experience overcrowding, increased hunger and a lack of nutritious alternatives for a given population. Conversely, it's extremely rare in situations with adequate and predictable supplies of food.

In other words, cannibalism is usually a result of specific conditions, and in the next blink, we'll learn which ones.

### 3. Cannibalism can play an evolutionary role. 

Now you know that the risk of cannibalism increases with hunger and a lack of access to alternative forms of nutrition. But there's more to it than that.

In 1980, the ecologist Gary Polis made some more general observations on the nature of cannibalism. Based on his work, scientists have since found an evolutionary explanation.

Here's the logic.

Polis found that immature animals tend to be eaten more often than adults as they represent an easy source of nutrition. As a result, infanticide is the most common form of cannibalism.

While it might seem ill-advised to eat the next generation, such practices make perfect sense since these young animals are a valuable source of relatively defenseless nutrition. Just take fish, for whom cannibalism is the rule, rather than the exception.

Fish commonly consume both the eggs and young of their species, even those they have themselves produced. After all, fish eggs, larvae and _fry_, or young fish, are vast in numbers, tiny in size and extremely nutritious. The fact that they're also non-threatening and easily collected makes them a prime food source.

So, cannibalism provides easily accessible food when it's needed, but this practice serves another evolutionary function in certain species: it speeds up their developmental processes. A good example is the flour beetle, which derives a reproductive advantage from cannibalism. Cannibal flour beetles have been found to produce more eggs than non-cannibals.

Or consider the sand tiger shark, which practices cannibalism in utero with its siblings. There is usually around 19 sand tiger shark embryos or fetuses per pregnancy, each at different stages of development. The larger of them consume any remaining eggs, as well as the smaller embryos, until just two remain.

These sharks derive both the nutritional benefit of cannibalism and earn the valuable experience of killing for survival before they are born.

### 4. Environmental factors can necessitate cannibalism, but the practice has drawbacks. 

What do a lack of access to alternative sources of nutrition and overcrowding have in common? They're both examples of _stressful environmental conditions_ that are prone to producing cannibalistic behavior.

Look at chickens: these birds are packed by the thousands into cramped poultry farms. The crowded, inadequate and stressful conditions prevalent in such spaces often cause them to misdirect their foraging and pecking behavior into attacking their neighbors.

Or take hamsters, a popular pet for kids. These domestic creatures suffer from captivity-related stress, like that caused by a small cage, excessive noise, a damp environment, or living in proximity to their natural enemies, dogs and cats. Stresses like these readily lead hamsters to cannibalize their own young.

Of the 5,700 species of mammals out there, just 75 have been known to practice some type of cannibalism. This overall low ratio is likely a result of the generally low number of offspring produced by mammals and the high level of parental care they engage in relative to other animals.

While cannibalism is not an everyday behavior in chimpanzees, occasional cannibalism does occur among them. Not just that, but certain researchers believe that, as humans encroach into the areas surrounding chimpanzee preserves, the animals will experience rising population density and increased competition for fewer resources, thereby prompting an increase in cannibalism among our closest evolutionary ancestors.

However, while there are certainly ample examples of this behavior, cannibalism in nature does inevitably present problems. For starters, it can increase the transmission of dangerous diseases, because parasites and pathogens are often species-specific, having evolved ways of overcoming a certain animal's immune defenses.

So animals who eat their own are at a higher risk of getting a disease than those who eat other species. The Fore people of New Guinea are a great example. They practically went extinct because of their ritual of eating the brains and other tissues of their dead kin. These corpses had been infected by kuru, an incurable and highly contagious neurological disease.

### 5. Real-life cannibals exist today, and you might even know some of them. 

By now you know a lot about how cannibalism plays out in other species, but what about humans like the Fore?

Well, while most modern people consider eating other humans to be grotesque, lots of cannibals beg to differ. Just take Armin Meiwes, who, in 2001, killed and ate Bernd Brandes, a 42-year-old computer technician who asked to be eaten.

The pair first met in an online chatroom, then in Meiwes' house in Rothenburg, Germany. At that in-person meeting, they decided to cut off Brandes' penis and eat it raw. However, finding it tough and chewy, they ended up feeding it to Meiwes' dog. Brandes later died from the combined effects of blood loss, pills and alcohol.

From there, Meiwes stored Brandes' remains in a freezer, eating them over the course of several months and describing the taste as "like pork; a little more bitter."

Then there was Issei Sagawa, who murdered and consumed a Dutch student in 1981. He got away with the crime because of his family connections and said the flesh of his victim tasted like raw tuna.

But much more common than that, is a form of cannibalism practiced primarily by white, middle-class women. It's the consumption of their own placenta, either raw, in smoothies, Bloody Marys, or as jerky. There are even companies that turn placenta into easy-to-take nutritional supplements.

How come?

Well, some midwives and alternative health advocates claim that the placenta has therapeutic benefits, like the ability to replenish nutritional loss caused by pregnancy and delivery. However, there's little official research to support such claims.

The author tried some placenta for himself and likened the taste to dark meat or organ meat. Yet he also described it as entirely different to anything he'd ever tasted. The flavor was strong, although not overpowering, somewhat like the chicken gizzards he ate fried as a college student.

### 6. Western cannibalism taboos may have originated in the Christian tradition and played out through storytelling. 

The first generally accessible scholarly text on cannibalism was published in 1975 by the British historian, Reay Tannahill. Called _Flesh and Blood_, this work theorized that the Judeo-Christian belief that the dead needed a complete body in order to be resurrected is at the heart of the cannibalism taboo.

However, there's more to the human distaste for cannibalism than religion. There's also a cultural process by which people distinguish between "insiders" and "outsiders" — a distinction often based on diet.

Just take the British, who came up with the derogatory nickname "frogs" to describe the French, who enjoy the legs of these jumpy amphibians as a delicacy. Similarly, Western colonialists labeled the inhabitants of the lands they invaded "savages" or "primitives" to justify their own land grab. And frequently they described the locals as cannibals.

Western scholars and casual readers alike were subjected to a 500-year indoctrination that told them little of the genocidal mistreatment of native populations. Rather, they were taught that Columbus and other European explorers were brave men who had fought off raving hordes of subhuman cannibals.

Then, by the dawn of the seventeenth and eighteenth centuries, the Western taboo on cannibalism was easily perpetuated through fairy tales. Just take the French courtier, Charles Perrault, who wrote the classic versions of _Little Red Riding Hood_ and _Snow White_.

In Perrault's _Snow White_, the wicked queen eats what she believes to be the organs of her dead stepdaughter, Snow White. But the benevolent hunter whom the queen told to murder Snow White has spared the young girl and given the queen meat from a boar instead. Or consider Perrault's _Little Red Riding Hood,_ in which the wolf really does murder the grandmother, cutting her up and storing the meat, which Red Riding Hood eats without knowing its origin.

Then, of course, there's the famous _Hansel and Gretel_, collected and written down by the German Grimm Brothers. In this tale, an old woman makes plans to eat little children.

All of these stories offer examples of evil, maniacal cannibals. The horror they inspire was used to perpetuate the threat of cannibalism and terrorize children into behaving. __

> _"When he is fat I will eat him . . . Let Hansel be fat or lean, tomorrow I will kill him and cook him." — old woman_ in Hansel and Gretel

### 7. Although humans evolved cultural norms that make cannibalism unacceptable, it might resurface. 

Alright, so Western culture has a _long_ history of considering cannibalism taboo. But where did such proscriptions come from?

Well, the father of modern psychoanalysis, Sigmund Freud, said that the rules associated with taboos were produced to prevent people from regressing into the violent, animalistic urges of the past.

But despite such ideas, some non-Western cultures, like the Chinese, or the Fore people of New Guinea, have participated in a variety of cannibalistic activities. T'ao Tsung-yi, a Chinese writer from the Yuan Dynasty, which lasted from 1271 to 1368, wrote that children's meat tastes best, followed by that of women and then of men.

Naturally, that was hundreds of years ago, and the modern world is a different story. Given the hegemonic nature of Western culture, it's unlikely that any ritual cannibalism is practiced today.

However, that could all change. As humans begin to face more serious environmental stressors, cannibalism could become more widespread. Warning signs of such a shift are all around us. Just take the desertification that's ravaging states like Texas and California, where the period between 2012 and 2014 was the driest on record in 1,200 years!

At the same time, huge swathes of China, Syria and central Africa are turning into desert, while Kenya, Somalia and Ethiopia, three of the poorest countries in the world, are suffering the worst drought in 60 years.

The results are famine, reduced access to freshwater and rampant political conflicts, all of which constitute environmental stresses on the human race. You already know that cannibalism is a natural response to severe stress in nature and, for humans, this is especially true during times of famine and war.

According to sociologist Pitirim Sorokin, cannibalism caused by famine occurred 11 times in Europe between 793 and 1317, as it did in ancient, Greece, Egypt, Rome, Persia, China, India and Japan. The truth is, there might not be much that can be done to prevent cannibalism from happening again, especially in the poorest and most vulnerable nations.

### 8. Final summary 

The key message in this book:

**The great taboo of cannibalism is a natural occurrence, often caused by environmental stresses. While human society has developed a strong distaste for eating our own, human cannibalism could resurface in the future.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Should We Eat Meat?_** **by Vaclav Smil**

_Should We Eat Meat?_ (2013) helps you navigate the ethical dilemmas behind your hamburger with a broad and objective assessment of meat production and consumption. Should you stick to grass-fed beef or take up veganism to save the planet? These blinks will give you all the facts you need to make your own informed decision.
---

### Bill Schutt

Bill Schutt is a research associate at the American Museum of Natural History and a professor of biology at LIU Post in New York. He wrote _Dark Banquet: Blood and the Curious Lives of Blood-Feeding Creatures_ and is the co-author of the novel _Hell's Gate._

