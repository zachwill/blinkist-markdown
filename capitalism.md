---
id: 5c112b0c6cee070007f43f29
slug: capitalism-en
published_date: 2018-12-14T00:00:00.000+00:00
author: James Fulcher
title: Capitalism
subtitle: A Very Short Introduction
main_color: 1F9C83
text_color: 156958
---

# Capitalism

_A Very Short Introduction_

**James Fulcher**

_Capitalism_ (2015) chronicles the history of the dominant socioeconomic system that society runs on today. From its humble beginnings in medieval Europe to its present global dominance, capitalism's history is marked by its dynamic — and sometimes unstable — nature. Nevertheless, its influence on how society has developed over the last 200 years is paramount to understanding the modern human condition.

---
### 1. What’s in it for me? Discover the inner workings and history of our modern capitalist society. 

Capitalism; it's the lifeblood on which most of the world's economies and societies are run.

We all have varying degrees of knowledge about what capitalism is, but its history and how we got to where we are today aren't as well known.

From why it first took in England, or how it led to the financial crisis of 2007-2008, capitalism's influence on humanity throughout its 200-year history cannot be understated. And by combining capitalism's history with an understanding of its basic mechanism of using money to make more money, we can begin to predict how it might affect humanity in the future.

Although capitalism was initially restricted to Europe and North America, the fact that its implications are now omnipresent in nearly every corner of the world makes the understanding of it more pertinent than ever.

In these blinks, you'll find out

  * how capitalists use money to make more money;

  * why refugees helped capitalism come about in England; and

  * what the subprime mortgage crisis is, and how it helped cause the Great Recession.

### 2. Capitalism is a socioeconomic system where money is used to make more money. 

The majority of the world runs, to varying degrees, on capitalism. But while there is no monolithic form that governs everyone who lives under a capitalist system, there are a number of common principles that all varieties of capitalism share.

Put simply, capitalism's most characteristic feature is the investment of money to make more money. This sort of invested money is known as _capital._ Those who use money in this way are _capitalists_, and the excess money made out of this process is _profit._

Capital can be anything that can somehow be transformed into money. Take homeowners, for example. They can choose to sell their homes for money, rent it out to others, or use it as collateral for a mortgage.

Another characteristic of capitalism is its reliance on _wage labor_. Combined with capital such as machines, buildings and raw materials, labor is necessary for the production of goods and services. In return for their work, laborers receive wages from employers.

Wage labor isn't only important for production under capitalism, however — it's integral to capitalist _consumption._

As individual workers cannot produce all the goods or services they might require, such as food, or a place to live, they have to purchase it. This drives demand for the goods and services resulting out of capitalist production, thus providing jobs for wage laborers in the first place!

Wage labor therefore constitutes a fundamental interaction between capitalist production and consumption.

Finally, capitalist consumption and production all take place within the confines of capitalist _markets._

Unlike in pre-capitalist times where much of humanity only consumed what they produced — and produced what they themselves consumed — today's dynamic capitalist markets allow the consumption and production of practically anything, as long as there is demand for it.

Markets can take the form of the traditional marketplace, grocery store or, more recently, online spaces, where people buy and sell goods electronically.

Within markets, capitalists seeking to make more profit than others generates _competition_. This is a key tenet of capitalism.

Competition can take many forms, including cost reduction. One way of reducing costs is to lower workers' wages. Another might be to increase innovation through creating machines that can replace human labor.

By lowering costs, capitalists can sell their products for less money than their competitors, thus generating more profit for themselves.

### 3. The roots of capitalism can be found in medieval Europe, particularly in England. 

While it's true that capitalism in its modern industrial form first became fully-fledged in nineteenth-century England, its roots can be found in medieval Europe.

It's impossible to identify a single cause behind the advent of conditions that allowed for capitalism in Europe. However, one determining factor that brought together various possible causes was the lack of a cohesive and all-dominating elite throughout the continent.

Unlike in the advanced societies of, say, the Roman Empire, or China's medieval imperial dynasties, medieval Europe's political fragmentation laid the groundwork for capitalism, particularly through its _feudalism_ and _multi-state structure._

European feudalism allowed for the development of markets and wage labor — both key features of capitalism — in ways that slave-based societies or self-sustaining peasantries couldn't. This was because economic producers were more flexible under feudalism. As opposed to slaves, producers had a limited amount of freedom but, unlike self-sufficient peasantries elsewhere, they had to provide a surplus to their feudal lord.

This meant that feudal societies were able to make comparatively easy transitions to capitalism. The surpluses that feudal peasants paid their lord was simply replaced by the payment of money. This, of course, meant peasants needed to make money as wage laborers.

Europe's _multi-state structure_ was also a condition that helped capitalism develop later on. With multiple jurisdictions ruling over a relatively small continent, entrepreneurs could move from place to place depending on where entrepreneurial opportunities were more fruitful.

This multi-state structure was even more important when it came to refugees escaping the Counter-Reformation in the sixteenth century. Refugees who fled to England from countries like Belgium and France brought financial innovations with them, and these innovations formed part of the foundation for capitalism in England to thrive.

One of these innovations constituted the origins of the modern corporation and found its roots in sixteenth-century Antwerp. There, merchants had figured out a method of financing their ventures by spreading risk throughout a large pool of investors.

In England, meanwhile, the seeds of capitalism were also being sown. In the sixteenth century, large enterprises in areas like coal mining, and massive growth in the small-scale production of clothing and household goods meant that wage labor was becoming increasingly widespread and, with it, the consumption of goods in competitive markets.

All of these conditions would create a fertile ground for the growth of capitalism in nineteenth-century England.

> _"[Elizabethan England's] openness to refugees contributed significantly to the development of capitalist production in England."_

### 4. Industrial capitalism started out anarchic, but quickly became increasingly managed. 

As schoolchildren, most of us learn about the progress made during the Industrial Revolution of the eighteenth and nineteenth centuries. We're told how it was responsible for so many of the technological breakthroughs we're completely dependent on today, such as mass transport and electricity.

But central to industrialization was the advent of industrial capitalism. And it began in England.

In the early stages of the Industrial Revolution, capitalism was anarchic in its nature. Levels of production and consumption were expanding so rapidly that state regulators found it hard to keep up with regulating markets, and organized labor found it hard to keep up in terms of organizing in opposition to their capitalist managers.

The newly empowered and rapidly wealth-accumulating capitalist class embraced the lack of regulation in this environment as it meant they were free to develop their entrepreneurial endeavors — and increase their profit — with little interference from the state.

These sorts of beliefs went hand in hand with the increasingly trendy philosophy of _liberalism,_ which placed the freedom of both individuals and markets as paramount to human liberty.

Meanwhile, amongst this anarchic environment, new cities, factories, roads and railways were springing up at lightning speed. But lack of state interference was ironically creating much more interference than capitalists had bargained for.

The unregulated market forces that kept wages down and prices up were having a dehumanizing impact on wage laborers' lives. This lead to rioting, strikes and public disorder. What's more, _machine breaking_ becoming commonplace. All of these things disrupted capitalist production.

Slowly but surely, radical labor movements became more organized and demanded rights for their members, challenging the dogma of unregulated capitalism.

By the mid-nineteenth century, anarchic capitalism began giving way to a more managed form as the state stepped in to restore order by imposing more market regulation.

Key to this period was the awakening of class consciousness. This was exhibited by mass workers' parties, increasingly balanced relations between employers and unions, and the state's intervention in regulating the economy.

To give laborers more control over their lives and work, for example, voting rights were extended to all men in 1867.

The founding of the modern welfare state was also key to the success of this more managed form of capitalism. Social policies that make up the norm of modern Britain, such as free healthcare and secondary education, were put into law in the 1940s. Before then, it was the market that set a price on healthcare and secondary education.

While the market still had a role to play, the state had in fact managed to reel in the more dehumanizing aspects of the earlier anarchic forms of capitalism. This form of state-managed capitalism would improve living conditions for millions.

But all this would begin to change in the 1980s.

### 5. In the 1980s, capitalism transformed into a remarketized form known as neoliberalism. 

In the 1970s, state-managed capitalism began to run into trouble.

One of the main reasons for this was the rapid increase of international competition. The growth of free trade meant that capitalist enterprises, now faced with competitors around the world, had to find ways to further reduce costs. This involved lowering wages, outsourcing production to countries with cheaper labor costs, or replacing workers with increasingly productive machines.

All of this was disastrous for employer-union relations, and governments found it increasingly difficult to resolve industrial disputes and regulate markets that were becoming more globalized.

When governments failed to resolve such issues, they began to lose the support of the public, and faced electoral defeat. If governments themselves couldn't compete against international market forces, they would have to try and embrace them.

Another factor contributing to the demise of managed capitalism was a shift in public opinion of individual rights. Whereas collectivist concerns such as social welfare and equality had once dominated political discourse, these began to be replaced by concerns for the individual's well-being.

Instead of being more concerned with low unemployment rates, the mood was shifting toward favoring lower tax rates and prices.

Both of these factors came together to form a perfect storm that managed capitalism couldn't withstand. With the election of the Conservatives in Britain led by Margaret Thatcher in 1979, a new, remarketized _neoliberal capitalism_ slowly began to engulf the world. It heralded a return to capitalism's more anarchic beginnings.

Thatcher's government quickly went about dismantling managed capitalism. Unions and employers were no longer consulted on political policy. Furthermore, unions' abilities to organize were severely restricted, and any defiance of these new limitations was punished by stripping unions of their assets.

Thatcher also began to inject market forces by privatizing nationalized industries. As a result, by 1992, two-thirds of British state-run companies had been put into private hands.

Thatcher's neoliberalism changed the political landscape in such a way that even the British Labour Party adopted it as their overall philosophy. The Labour government under Tony Blair in the 1990s and 2000s, for example, continued Thatcher's remarketization of British healthcare by allowing private financing of hospital construction and restructuring hospitals into corporate-like foundations.

While this new world of neoliberal capitalism allows for increased consumer choice and individual freedom, its decimation of public services means that people's lives are less secure.

For example, without empowered unions, laborers struggle with job security. And with the sale of public housing to the private sector, rental costs are much more susceptible to brutal market forces.

> _"As managed capitalism developed, the freedom of the individual was diminished in the name of greater equality, but in a remarketized capitalism, equality and security have been sacrificed to freedom and choice."_

### 6. While displaying differences, capitalism’s remarketization had similar effects in Sweden and the US. 

As neoliberalism became the ruling capitalist model in the UK under Margaret Thatcher, the same international market forces and increased individualism caused neoliberal dominance elsewhere.

Sweden, for example, also succumbed to neoliberalism, albeit less so than in the UK.

The Swedish Social Democratic Party governed from 1932 to 1976 and, by doing so, established an omnipresent welfare state, high taxes and relatively healthy labor-employer relations. This meant that Sweden had one of the most managed capitalist systems in the world.

But, as in the UK, problems began to emerge in the 1970s. With the rise of international competition and global economic integration, as well as a burgeoning individualist ideology, industrial conflict increased and the egalitarian collectivism that had allowed for high taxes and expansive welfare programs decreased. The time was ripe for remarketization.

So it happened. And it was led not by a Thatcherite party, but by the Social Democrats. In the 1980s, unable to continue funding their expansive welfare programs in the face of international competition, benefits were cut, taxation decreased, the investing of private capital in state-owned sectors was permitted, and the financial sector was deregulated.

All of this meant that by 2011, inequality in Sweden grew more than in any other OECD country in recent decades. But, as its capitalism was so well managed before remarketization began, Sweden is still among the most egalitarian countries in the world and is still a top spender when it comes to education and healthcare.

The same cannot be said for the US.

Even before the swing toward individualism in the 1970s, US society already honored the importance of the individual and entrepreneurial spirit in what is popularly known as the American Dream.

Still, a managed capitalism was to be found in pre-1970s America, particularly after the Great Depression of the 1930s. During this time, expansive welfare programs were put into place by Franklin D. Roosevelt's New Deal legislation.

Unions were even empowered during this time, and a National Labor Relations Board was set up to allow class compromise between laborers and employers.

But, as in the UK and Sweden, this was not to last. Remarketization in the US began under the Reagan administration in the 1980s with expansive tax cuts. Additionally, industries such as air travel, railways, and telecommunications were either deregulated or privatized.

The result of this? By the end of the 1980s, income inequality had returned to levels not seen since the 1920s. At the same time, in order to remain competitive in international markets, US manufacturing increasingly moved to cheaper markets such as Mexico.

Perhaps most significantly, the remarketization and subsequent deregulation of the American financial sector set the scene for the global crisis that international capitalism has faced since 2007.

### 7. The financial crisis of 2007-2008 was the culmination of remarketization policies. 

The remarketization that took place in powerful capitalist economies such as the UK and the US changed the way capitalism in the rest of the world worked. Because of this, in addition to increasing international competition and deregulation, two other factors would come together to create the perfect storm that would be the 2007-2008 global financial crisis and subsequent Great Recession.

The first of these was the financialization of capital, constituting a shift toward capital being invested in high-risk and potentially high-profit financial contracts on the stock market as opposed to in the traditional capitalist production of goods and services.

Take _futures,_ a financial contract where one commits to buying shares or commodities at their current price at a fixed point in the future. If the price has risen after the fixed period, futures investors can make a profit by selling the shares at this new, higher value. But of course, the value can also go down during this fixed period, leading investors toward a loss.

Such high-risk, speculative financial contracts are typical of what British academic Susan Strange terms _casino capitalism._

And due to intensified financial deregulation since the 1980s, such financial contracts have been able to be easily bought and sold across borders with little government oversight.

This financialization of capital contributed to the second factor that would set the 2007-2008 crisis in motion: the excessive growth of debt.

The prime example of this is a key component of the American _subprime mortgage crisis_, where easy credit combined with speculative financial instruments would lead to disaster.

So how did it happen?

The housing bubble that burst in the mid-2000s had previously guaranteed a safe return on investments into housing. Egged on by big banks with the prospect of hefty commissions and bonuses, mortgage brokers increasingly handed out loans to less worthy creditors.

In what became known as subprime mortgages, brokers gave out mortgages to those with bad credit ratings who would previously never have qualified as they had no proof they could pay back their mortgages.

These subprime mortgages were then sold off to banks, who subsequently packaged them into financial instruments that would be sold on to larger financial institutions.

But as soon as housing prices began to fall, mortgages stopped being paid back, and the whole system began to collapse. Huge financial institutions such as Lehman Brothers had themselves borrowed money from outside the US to cash in on the housing boom, and in the process had acquired debt forty times their capital.

On September 15, 2008, Lehman Brothers was no longer able to pay back its debtors. It filed for bankruptcy and set into motion a chain reaction worldwide that would exacerbate the already fragile global economy.

### 8. The financial crisis of 2007-2008 led to questions on how such crises can be avoided in the future. 

After the collapse of Lehman Brothers, further bankruptcies followed, faith in investment banks was shattered, and credit became scarcer. The Great Recession was now truly underway — international trade, employment, wages and consumption fell all over the world.

The effects of the crisis were hard for anyone to ignore. But sadly, lessons haven't been learned. The low interest rates put in place after the crisis, while helping to solve economic woes in western nations, have led to increased borrowing in developing countries. In China, Brazil and Turkey, for example, debt-to-GDP ratios have increased far beyond 2007 levels, meaning that something similar to the subprime mortgage crisis might happen again.

This begs the question: If the very economic policies that helped avert deepening the 2007-2008 crisis are laying the groundwork for other problems, what can be done to avert further capitalist crises?

It won't be an easy task. When it comes to reforming and regulating banks, not much has been done since 2008. There is the argument that banks will simply move to another less-regulated country if confronted with the prospect of increased regulation and less opportunity for profit.

So we may have to accept that crises are an inherent feature of capitalism itself, seeing as capitalists will always seek new ways to make money from novel investments.

But for many, this isn't a sufficient answer. With capitalism's endless desire for growth being a direct contributor to climate change, for example, reforming capitalism has become an existential issue that humanity will be forced to confront.

However, just as in the banking sector, proposed reforms to industries that directly contribute to climate change are met with fierce resistance, as increased regulation leads to decreased short-term profits. Of course, in the not too distant future, disaster stemming from climate change may shut down entire capitalist economies, thus affecting long-term profit.

But with the political left at a weak point in its history, viable alternatives to capitalism aren't obvious. Therefore we'll have to rely on capitalism's dynamic nature to help reform its more destructive tendencies.

Perhaps a focus on reforming the remarketized, neoliberal capitalisms of the West and pushing for a move back toward its state-managed post-war variants would be a step in the right direction.

After all, less neoliberalized regions such as southern and southeastern Asia were much less affected by the Great Recession. Perhaps these dynamic regions will have a large role to play in the future of capitalism.

### 9. Final summary 

The key message in these blinks:

**Capitalism is the dominant socioeconomic system the world runs on today. It's all about investing money to make more money, and relies on a combination of wage labor, competitive markets, consumption and production to function. Although capitalism began in Europe, it has now spread to most of the world. And while its beginnings were fairly chaotic, state intervention managed to regulate capitalism's state excesses up until the 1980s. The undoing of such intervention led to the financial crisis of 2007-** **2008, and poses questions about how capitalism might continue into the future.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Socialism_** **by Michael Newman**

_Socialism_ (2005) is a dash through the history of the term after which the book is named. Socialism has played an important role over the past 200 years of human history, but its original goal of achieving an egalitarian society has, in recent decades, been somewhat forgotten. This book is a thorough tour of socialism's history. It's also an exploration of the various ways the word has been implemented and a guide to ways we might use it in the future.
---

### James Fulcher

James Fulcher is a fellow at the University of Leicester, where he teaches sociology. Aside from _Capitalism,_ he also co-authored Oxford University Press' _Sociology_ textbook, now in its fourth edition.

