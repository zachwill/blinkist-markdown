---
id: 5a2dd4fdb238e1000a9f2ce2
slug: captivate-en
published_date: 2017-12-13T00:00:00.000+00:00
author: Vanessa Van Edwards
title: Captivate
subtitle: The Science of Succeeding with People
main_color: 2A8CD1
text_color: 206A9E
---

# Captivate

_The Science of Succeeding with People_

**Vanessa Van Edwards**

_Captivate_ (2017) is your guide to human behavior and social success. Whether you're trying to connect with others at home, at work or out in the world, Van Edwards set about breaking down the mechanics of how to capture people's attention and engage in meaningful interaction.

---
### 1. What’s in it for me? Master the science of social interaction. 

Today, we're whelming in a flood of content and clickbait — a cascade of Facebook and Instagram posts, emails and chats, news both fake and real. In this environment of digitized interaction, good social skills are becoming rarer, and therefore more valuable. And that's what these blinks are all about: improving your social skills.

Regardless of whether you're going to a networking event or out on a date, these blinks will help you understand how to hack social interactions. You'll learn about real techniques that will help you improve your people skills and become absolutely captivating in a range of social situations.

You will also find out

  * how to make a great first impression with the _triple threat_ ;

  * what OCEAN tells us about basic human personality traits; and

  * about the brain-synchronizing power of storytelling.

### 2. Control social situations by focusing on your strengths and the people who matter. 

Imagine you've been invited to a party or networking event. The problem is, you consider yourself a somewhat socially awkward person. To succeed in this foreign and uncomfortable environment, you'll need a strategy to gain control over the situation. In other words, you'll need a _social game plan_.

Keep in mind that different people do well in different social contexts. Indeed, the point of coming up with a social game plan is to avoid the contexts that you dislike. After all, nobody would expect a pitcher to also be a star second baseman — and the same goes for social contexts: few people thrive in every situation.

Simply put, do your best to avoid the social situations you most abhor. If you try to fake your way through them, you might find yourself in trouble; fakeness is something people can easily sense.

Want proof? Well, a survey done by Science of People, the author's lab, tested the ability of participants to identify a fake smile. In the end, 86.9 percent of the 4,361 participants successfully identified the fake smile.

So knowing where you're comfortable is important, but it's also necessary to decide precisely whom you'll focus on. Remember, you don't have to please everyone. Focus on the people you want to reach. 

The author learned this lesson when one of her YouTube followers complained, in a comment, that her casual attire made her appear unprofessional in her videos. The author was initially concerned about the comment and even considered re-shooting her videos. But pretty quickly she realized that she was looking to connect with a particular type of student for her courses; she wanted people who could focus, not those who would be easily distracted by her clothing.

This helped her realize that she didn't have to appeal to everyone. She alone could define the group of people relevant to her work and use her brainpower to draw them in.

### 3. Use your hands, posture and eye contact to make a good first impression. 

Have you ever encountered someone who is absolutely magnetic? Such a person can enter a crowded room and instantly capture everyone's attention. But these people weren't necessarily born charismatic. In fact, making a great first impression is a skill that almost anyone can learn.

Here's a handy tool to get started. It's called the _triple threat_, and you can use it to hack people's trust. It's made up of three simple nonverbal components: hands, posture and eye contact.

Hands are important to the equation because they can reveal a person's intentions. Most modern people don't check to see if a new acquaintance is carrying a weapon, but this ancient instinct is still alive. That's why it's key to keep your hands visible. It's no accident that we shake hands to greet someone and that criminals are told to put their hands up by the police.

The second aspect, posture, is essential for positioning yourself as a successful, confident person whom people will be attracted to. According to Carnegie Mellon University researchers Bill McEvily and Akbar Zaheer, confidence is everything in a job interview. It even matters more than the candidate's reputation, personal history and skills!

To ensure your posture is truly stellar, use the _launch stance_. Simply pull your shoulders back and down, push your head and chest forward and slightly up and let your arms fall gently away from your torso.

Finally, eye contact is a great way to tell someone that you want to be her ally — that you respect her, that she matters. That's because eye contact sparks the production of _oxytocin_, a hormone that helps build trust and facilitate bonding.

So take a tip from Allan Pease, a body-language expert. In his book, _The Definitive Book of Body Language_, he says that to build a connection with someone you should maintain eye contact for between 60 and 70 percent of the conversation.

### 4. Decode people’s feelings by studying their subtle facial expressions. 

It's pretty common for people to feel one thing and say something entirely different. But by decoding the unconscious facial expressions that all people make, you can determine what they really believe. Dubbed _microexpressions_ by the American psychologist Paul Ekman, these subtle facial gestures flicker across a person's face before she can control them, revealing her true feelings.

There are seven microexpressions to look out for: _anger, contempt, happiness, fear_, _surprise, disgust_ and _sadness_. We'll explore each one in turn.

The first, anger, gives itself away by a tense mouth and pinched eyebrows. It strikes when a person is displeased or annoyed, maybe because of a confrontation or argument. As it dawns, it tends to generate two vertical lines between the eyes, forcing the eyebrows together and down. Meanwhile, the mouth clamps shut, tight-lipped, or opens wide, ready to shout.

Contempt is defined by a raised mouth skewed to one side, in what people commonly call a smirk. Because of this, contempt is often mistaken for the smile of happiness, which it definitely is not. This microexpression can easily be produced when someone is told no, or when they generally disagree with or dislike another person. People usually don't smile in such situations, so pay attention to context when determining which microexpression you're witnessing.

Third, proper happiness results in a big fat smile. The cheeks lift up and wrinkles form around the eyes. This one is easy. You'll know true happiness when you see it. After all, it shows across a person's entire face.

Next up, you'll learn about the other microexpressions and how to identify them.

### 5. Identifying more complex microexpressions can give you even more information. 

Now that you know about the first three microexpressions, it's time to learn about the others. The fourth is fear and it's marked by wide-open eyes, raised eyebrows that pull together, horizontal lines on the forehead and a slightly open mouth.

To correctly identify this microexpression, pay attention to the eyebrows. The best way to see whether a situation is safe or whether you should flee is to open your eyes wide — so if someone's eyebrows and upper eyelids are way up, they're probably afraid. Furthermore, the slightly open mouth of the fear microexpression allows for extra inflow of oxygen, preparing you to run if necessary. Keep in mind that literally any situation that a person considers dangerous, from finding a spider to encountering a bully, will produce fear.

Next up is surprise, evidenced by lifted eyebrows that pull apart and a marked drop of the jaw. Surprise is somewhat akin to fear, except the eyebrows pull apart rather than together and the mouth opens wider.

It's important to learn to spot the difference, as this microexpression can be quite revealing. For instance, if a colleague demonstrates surprise when you talk about being taken off of a project, you can be pretty sure he didn't know about the change until that moment.

The next microexpression is disgust, which can divulge much more than a person intends. It's characterized by a wrinkled upper nose, raised cheeks and a tight lower lip, with the upper lip lifted. Disgust displays itself when a person feels displeasure with something — say, the taste of his sandwich. 

For example, say you're interviewing a candidate for a job. You mention that the position requires lots of paperwork, to which the interviewee responds with a clear expression of disgust. Spotting this will help you determine if he's right for the job.

And, finally, sadness is clearly distinguishable because of its frowning expression; the eyelids droop, the eyebrows pinch together, the lower lip puffs out and the corners of the lips go south. This microexpression shows whenever disappointment is felt and it indicates that a person is upset or even on the verge of tears.

### 6. Get to know others better by looking out for five major personality traits. 

Have you ever intentionally tried to change your romantic partner? If you have, you probably didn't succeed. Indeed, altering others can be exceedingly difficult. So, instead of trying to change people, why not learn to spot personality traits _before_ you befriend them?

It's easier than it sounds. In fact, there are only five major personality traits: _openness, conscientiousness, extroversion, agreeableness_ and _neuroticism_. They make up the acronym OCEAN, and each is present to a certain extent in every person.

These traits are often referred to as the _Big Five_ and they were identified by Dr. Lewis Goldberg in the 1980s. The first, openness, is evidenced by how you feel about and approach new ideas — by how curious and adventuresome you are. For instance, if you jump at the opportunity for travel adventures, or constantly rearrange your furniture, you're likely to be quite open. Conversely, if you're more into daily routines, your openness is probably limited.

The second, conscientiousness, is about getting things done. Signs that you're conscientious are a love for to-do lists and a strict schedule. However, if lists overwhelm you and you're more into talking about big ideas than seeing them through, you probably rate lower in this category.

Third comes extroversion, a description of how you feel around others. To figure out where you stand just consider this question: Do you draw your energy from solitude or from groups? If you gravitate toward social time, chances are you're an extrovert; if you lean toward alone time, you're likely not.

The fourth trait is agreeableness, which corresponds to your ability to work with others. This includes your capacity for trust and how well you get along with those around you. If you go out of your way to please others, you're probably an agreeable person. People who are less agreeable tend to approach others pragmatically, rather than caringly.

And, finally, neuroticism is marked by a tendency to worry and a susceptibility to mood swings.

Keep these traits in mind and use them to assess those around you.

### 7. Use specific types of stories to connect with others. 

Have you ever heard of _neural coupling_? It's a mechanism in your brain that makes it possible for you to conjure up, say, the smell or taste of a delectable chocolate mousse, even when no such treat is at hand. Imagining the dessert replicates the experience you had the last time you enjoyed it.

This same mechanism comes into play when listening to another person's story. When someone tells you her personal story, you're easily able to imagine it and therefore connect with her.

This is substantiated by a study in which researchers used brain scans to track the respective neural activity of a storyteller and his listener. In the end, they found that as the speaker told the story, the listener's brain began to display similar patterns to those of the reader's. In other words, the two brains began to sync as their activity became more and more alike.

So storytelling is powerful. To get yourself started with it, you can create a _story stack_. This is a collection of stories that contain _trigger topics_, _sparking stories_ and _boomerangs._

The first type of story, trigger topics, are about generic subjects that anyone can talk about. Good examples are the latest news, weather, traffic or anything that people can easily relate to.

The second, sparking stories, are those with depth, or at least more depth than the weather. These stories are characterized by personal anecdotes that prompt a more substantial reaction. At their best, they make people laugh, cry, groan or display another strong emotion.

And finally, a boomerang, as its name implies, is a story or question that brings the conversation back to the person you're speaking with. You can use this type of story at points in a conversation when you want the other person to speak. For instance, say you just shared a sparking story with a colleague about this time you spilled a cup of coffee on a celebrity. An appropriate boomerang would be to ask your colleague if she has ever met a celebrity and what it was like.

### 8. Sharing vulnerabilities connects people, and asking for advice is a great way to appear more personable. 

Benjamin Franklin, one of the founding fathers of the United States, once asked a political opponent of his to lend him a book that Franklin knew he owned. The rival agreed, Franklin borrowed the text and, after reading it, returned the book with a thank-you note. Interestingly, from then on out, the other politician was much nicer to Franklin.

In fact, it worked so well that it's since been dubbed the _Franklin effect_. This term refers to how asking favors or advice can connect you with people by making you appear more vulnerable and therefore more human.

In other words, asking a favor of someone will increase the chances of that person liking you. It may sound counterintuitive, but studies have actually verified the effect. Just take an experiment done in the late 1960s by the researchers Jon Jecker and David Landy. In their study, they asked participants to take a number of tests, offering them money in exchange.

At the end of these tests, one of them would approach several participants and ask if they'd consider returning some of the money, claiming the study was poorly funded. Later, the participants were asked how likeable these test administrators were. Surprisingly, those who were asked to return the money had a more favorable opinion of the researchers.

How come?

Because the researchers made themselves vulnerable by saying they lacked funding. In the process, they made themselves appear more human and easier to relate to.

Having said that, you shouldn't just run around asking any stranger for money! A better way to show vulnerability is to ask for advice. Doing so reveals that you don't know everything and shows that you require the help of others. For instance, you might ask for book or restaurant recommendations or ideas for holiday gifts.

> _"Vulnerability is sexy — it shows we are relatable, honest, and real."_

### 9. Final summary 

The key message in this book:

**Social awkwardness and anxiety can hamper your career, limit your dating life and generally hold you back. But there's a way around these difficulties. By learning a few simple tricks — like how to recognize microexpressions and how to identify personality types — you can leave that socially uncomfortable persona behind and become a shining social star.**

Actionable advice:

**Use the napkin trick to avoid sweaty handshakes.**

Have you ever been nervous at a networking event? If so, you've probably experienced the much-dreaded sweaty palms and the terror of having to shake other people's hands with them. To avoid this fate, grab a drink and wrap a napkin around your glass. Hold your drink with your shaking hand and it'll stay dry — happily ready to meet and greet.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Charisma Myth_** **by Olivia Fox Cabane**

_The Charisma Myth_ (2013) defies the popular notion that charisma is inherited, arguing instead that everyone can cultivate their own charisma, and in doing so can have a more positive attitude, find more success, and handle obstacles more successfully.

Using wide-ranging examples of charismatic people, from state leaders to CEOs to employees, the book also outlines the different styles of charisma and how to practice demonstrating each, and offers some useful tools and exercises with which to improve their psychological well-being.
---

### Vanessa Van Edwards

Vanessa Van Edwards is a hacker of human behavior. Having struggled with a number of social situations throughout her childhood and adolescence, she decided to put the newest scientific research on human behavior to the test. Her behavior lab, Science of People, endeavors to teach people how to succeed in every conversation they have.

