---
id: 572c67fdd77ba60003517065
slug: case-interview-secrets-en
published_date: 2016-05-11T00:00:00.000+00:00
author: Victor Cheng
title: Case Interview Secrets
subtitle: A Former McKinsey Interviewer Reveals How to Get Multiple Job Offers in Consulting
main_color: CAA74D
text_color: 7D682F
---

# Case Interview Secrets

_A Former McKinsey Interviewer Reveals How to Get Multiple Job Offers in Consulting_

**Victor Cheng**

_Case Interview Secrets_ (2013) is a practical guide to nailing your interview at a big consultancy, such as Boston Consulting Group, McKinsey or Bain and Company. These blinks offer a crash course on the questions you'll be asked and how you should answer them.

---
### 1. What’s in it for me? Nail your upcoming case interview. 

Once you've passed through the eye of the needle and have been invited for an interview at your favorite consultancy, the time is ripe to consider how to become the successful candidate for the job.

In most cases, a job interview at a consultancy is not just another job interview with an assortment of questions about your endeavors and prior experiences. You have to be ready to answer questions that can be both quantitative and qualitative, and that can take the form of a case, such as _what's the current market size for product X?_

And that's where these blinks can make a big difference. They'll help you get an overview of the different types of questions that you're likely to encounter and, more importantly, how to answer them in order to get the job offer you're looking for.

In these blinks, you'll learn

  * how to perform complex arithmetic estimates, precisely and quickly;

  * when proxies come in handy; and

  * why you should expect that your interviewer might act somewhat aggressively.

### 2. Even if you are a world champion mathlete, you’ll need to practice hard for your interview. 

Have you ever gone for an interview at a major consulting firm? If so you'll know that it won't just be a conversation about your qualifications; rather, it will entail solving hands-on business cases. These are called case interviews and they tend to feature quantitative questions that come in two types.

The first is _math questions_ — problems that involve some sort of arithmetic, percentage calculation or interpretation of data. An example of such a question would be, "if the total innovation capital of company A is X and it were to grow at five percent per year, which of the following is the minimum annual growth of human capital necessary for the firm to represent more than half of total innovation capital within ten years?"

Questions like this can be daunting. But the key is to answer them as quickly as possible, which takes lots of practice. No matter how much of a math whiz you are, your brain is a muscle that needs regular training to perform optimally.

In fact, even candidates with PhDs in physics or the highest marks in college math classes can fail at these questions if they don't practice enough. So, it's essential to take on daily drills that improve your speed and confidence.

But how?

One way is to practice old interview questions. For instance, you can become faster by searching for the types of questions asked by firms like McKinsey or the Boston Consulting Group. By doing so, you'll begin to "automate" your reasoning and be able to respond to such questions in a snap.

But keep in mind that test questions are always changing, and it's important to get the most recently administered tests so that you're up to date.

Now that you know about math questions it's time to learn about the second style of quantitative questions: computational-level estimates.

### 3. Complicated problems can be solved quickly by breaking them down and using rounding. 

So what exactly is a _computational question_?

It's a problem wherein you're given a few facts and tasked with using them to make estimates. Answering such problems successfully requires precise arithmetic using large numbers — no easy task.

But as a matter of fact, performing complex mathematical equations in your head can be done precisely and quickly if you break them up. For instance, you might be asked: "You have a market of 3 million customers, assume a 15 percent market share and revenue of $200 per customer. What is your total revenue?"

To break this down you could first find the total value of the market, which is 3 million buyers x $200 = $600 million. Then, to calculate 15 percent of 600 million, you can break the 15 percent into 10 percent and 5 percent to find that 10 percent of 600 million = 60 million and 5 percent of 600 million = 30 million. Now all you've got to do is simple addition: 60 million + 30 million = 90 million.

But arriving at an exact number isn't always necessary. In fact, in many cases you'll simply need to reach a rough estimate, and you can do so by cleverly rounding numbers. This is helpful because rough estimations will let you perform equations much more quickly.

So, take a variation of the above question in which the market is 42 million customers, the market share is 12.7 percent and revenue is still $200 per buyer. What's the total revenue?

Obviously, there's no way to arrive at a precise answer rapidly in your head, and this is where precise estimates come into play:

Begin by rounding 42 million buyers to 40 million. Then, round 12.7 percent up to 15 percent. It's important that you round this number up to counterbalance your rounding down to 40 million. Now you can just solve the problem like before to come up with $1.2 billion in revenue, a close approximation of the precise answer of $1.07 billion.

### 4. To make big-picture estimates, find a proxy for what you’re estimating. 

So, rounding can help you solve complex math problems easily — but what about determining a product's market size?

It's a piece of cake, and here's how:

First, remember that the purpose of such questions is to see how you approach them. In fact, arriving at the correct answer is more or less irrelevant. With that in mind, your next move is to find _proxies_, that is, other factors that can help you arrive at your answer.

Say you're asked the question, how many hamburgers does the average drive-thru sell per day? In this case, your proxies will be things that help you calculate hamburger sales. For instance, your initial proxies might be the number of cars that visit the establishment daily and the average number of burgers each car orders.

But once you've found your proxies, it's important that you carefully identify and keep in mind their limits; after all, a proxy is merely a rough estimate to guide your prediction. For instance, during peak hours, it might be that the drive-in can't serve all its potential customers. It's essential to account for this imperfection and break sales into peak and off-peak times.

During peak times, sales are limited by the number of hamburgers the staff can churn out; to determine this, you need further proxies like the number of counters and the average time an order takes. But for off-peak times, your calculations will only depend on the number of cars and the average size of their purchase.

For example, say you make the following assumptions: every day has four peak hours and eight off-peak hours. A hundred cars pass through during every peak hour and 30 cars visit the establishment during every off-peak hour. The drive-in has two counters and each car takes three minutes to make the average purchase of two hamburgers. Therefore, all off-peak customers can be served given the shop's 40 car per hour capacity.

Given these assumptions, you can determine off-peak sales by multiplying 8 hours x 30 cars x 2 hamburgers per car = 480 hamburgers per day. Then you can find peak hour sales by multiplying 40 cars x 2 hamburgers per car = 320 hamburgers, which brings the daily total to 800.

### 5. At an interview, act like a professional consultant and treat your interviewer like a client. 

If you find yourself at a case interview, you might be confused as to why interviewers ask you to make so many obscure estimations. How could somebody possibly determine how many hamburgers a drive-thru sells?

Interviewers test prospective employees with these kinds of questions to simulate what clients will ask you if you become a consultant. In essence, case interviews are test runs for your future job as a consultant — and you ought to act like it. To do so, you need to know what your interviewers are looking for and why they ask the questions they do.

For instance, if you know someone who has already had an interview at a big consultancy, they might have mentioned that the interviewers can be aggressive at times, constantly asking you to make estimations.

Why?

Because that's what clients do. As such, the best way to approach an interview is to think of the interviewer as a client and yourself a professional consultant. Simply answer the questions as you would to best represent your company's reputation.

But consultants don't just need the ability to answer questions with laser precision and demonstrate strong analytical skills. They also need top-notch interpersonal skills. In fact, even if you answer a question perfectly, if you're nervous and fidgety you likely won't get the job. Clients simply won't trust recommendations from a nervous consultant.

Say a consultant advises a client to let go of 3,000 employees while the consultant himself is sweating and shaking. Would the client really want to take this pivotal advice from such an anxious source? Probably not, which is why it's crucial to emanate confidence.

Now that you've found this important piece of the puzzle, you can easily approach case interview questions like "how can I boost my profits or turn around my failing business?"

### 6. Use different frameworks to answer qualitative questions. 

So, you've learned how to tackle quantitative questions. But your interviewer might also throw some general business questions your way, like "why am I losing money?" or "should we merge with this company?"

Handling such questions is no problem as long as you utilize _frameworks_. For instance, you can use the _profitability framework_ to conduct a quantitative analysis. It works by breaking profit into two groups based on its components: revenue and cost.

Once you've done this you can do the mathematical operations one at a time to find the core of the problem. Say you are asked "why is the company losing money?"

You simply break profit into _revenue_, the price per unit x the number of units sold, and _cost_, the cost of production per unit x units sold. You can even split the cost per unit into fixed and variable costs. By utilizing this framework you'll be able to see the business quantitatively. Doing so might lead you to discover that the variable costs are too high, causing an increase in total cost and, therefore, decreasing profit.

But figuring out that variable costs are too high isn't much help if you don't know why. To figure that out you'll need the _business situation framework_.

You can use this framework to analyze a client's business, industry and market. It dissects these qualitative facets by uncovering information on four sub-segments of business: _customers, product, company_ and _competition._

The first, customers, is about who buys the product and whether the base breaks into segments with different needs. When it comes to a product, you need to define what is being sold as well as how it's packaged and produced.

Defining the company means identifying its distribution channels, organizational structure and other relevant parts of the business. And finally, competition is about how much market share competitors hold and whether they're doing things that your client's company isn't.

By applying this framework you might discover that your client's firm has high costs because of, say, a new package design that is overly complicated and wastes lots of material and money.

### 7. Final summary 

The key message in this book:

**Most interviews at consultancies break down into two types of questions: quantitative questions and cases. Keeping a few simple tricks in mind, you'll be well prepared to tackle both of them and land your dream job at a major consultancy.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Career Playbook_** **by James M. Citrin**

_The Career Playbook_ (2015) is based on interviews with top professionals as well as a survey of thousands of graduates and young professionals. It offers valuable advice for launching and building a strong career; by acquiring the right skills, building the right network and presenting yourself well, you'll be on your way in no time.
---

### Victor Cheng

Victor Cheng is a former McKinsey interviewer and consultant who has helped countless aspiring consultants prepare for their case interviews via www.caseinterview.com.

