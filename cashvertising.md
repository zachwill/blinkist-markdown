---
id: 55b76d2d3235390007210000
slug: cashvertising-en
published_date: 2015-07-28T00:00:00.000+00:00
author: Drew Eric Whitman
title: Cashvertising
subtitle: How to Use More Than 100 Secrets of Ad-Agency Psychology to Make BIG MONEY Selling Anything to Anyone
main_color: E02D31
text_color: AD1818
---

# Cashvertising

_How to Use More Than 100 Secrets of Ad-Agency Psychology to Make BIG MONEY Selling Anything to Anyone_

**Drew Eric Whitman**

_Cashvertising_ (2009) shows how you don't need a million dollar ad campaign to bring in the customers. All you need to know is how and why we make buying decisions. Full of instantly actionable tips, this book tells you everything you need to know about how to turn your ads into profits.

---
### 1. What’s in it for me? Find out how to sell porn to the Pope. 

Sometimes you open a magazine, so eager to read the cover story that you ignore all distractions. But then, as you search for the story, an ad catches your eye and completely draws you in. Every copywriter wants to write an ad like that — and yet, few do. So what's the secret of these happy few?

The good news is that you don't need outstanding creativity or superb writing skills to produce marvelous ads. Instead, there are effective tricks everyone can learn. These blinks, for instance, will explain how to write an irresistible headline in just three steps and where to place your ad for maximum effect.

The most important thing to know, however, is what actually drives people to buy. So what's the psychology behind highly successful ads? That's the main focus of these blinks.

If you read on, you'll learn

  * why fear is the best friend of every copywriter;

  * why long copies won't scare away your clients; and

  * how to sell a mouse smoothie.

### 2. The key to great advertising is to appeal to your customers’ core desires. 

Imagine that someone has invented a cucumber and mouse smoothie for cats, and you're the lucky person who gets to write the ad. How in the world will you sell this weird concoction to millions of picky cat companions?

The secret: setting off the right _triggers_. But what are they?

Every person is wired with eight desires that marketers would be foolish to ignore. These desires, the _Life-Force 8_ (LF8), helped us survive in the past, and they're still hard-wired into our brains.

We wouldn't make it through the week without the first three desires: to stay alive; to enjoy foods and beverages; and to be safe and free of pain.

Naturally, we also yearn for sexual companionship and comfortable living conditions.

Finally, we're social beings, which is why we desire to protect our loved ones, long for the approval of those around us and strive to excel at what we do.

Good marketers are able to transform these desires into money. In fact, consumer researchers have found that these eight basic desires drive more sales than all other human wants combined. But why?

Desire is a form of tension that compels you to act if a need is left unmet. For example, if you're hungry, you feel a tension that drives your desire for food. This in turn leads to actions that satisfy your desire. Our actions always follow this pattern: tension leads to desire leads to action.

Marketers can benefit from this simple biological process by understanding and appealing to the L8 in a clever way.

So how do you sell that cucumber and mouse smoothie? Appeal to the cat owner's desire to protect her cat. Tell her how the smoothie will protect the cat from going blind due to its high amounts of taurine — a substance that cats really need but can't produce themselves.

Now you understand these basic triggers; our following blinks reveal how marketers use them to sell you their products.

> _"When you create an advertising appeal based on any of the LF8, you tap into the power of Mother Nature herself."_

### 3. Fear-inducing ads have four simple ingredients. 

Let's start out with a freaky fact: it doesn't matter how often you wash your sheets; they're still the perfect breeding ground for hideous dust mites who lay their eggs in your pillows and mattresses. Imagine how they crawl over your skin while you sleep, nibbling at your skin flakes and perhaps crawling up your nose, inducing a nasty allergy attack.

Scary, isn't it? Not to mention gross.

As it turns out, making effective ads that employ the fear factor is as easy as following this four-ingredient recipe:

First, crank up the fear. The best kind of fear is the kind that really "scares the hell out of people." For example, by describing the dangers and habits of mites in lurid detail, you've successfully horrified your listeners, meaning they'd do just about anything to stave off these microscopic monsters.

But you don't want to create _too_ much fear. If you do, people will be paralyzed, like a deer caught in the headlights. Indeed, too much fear causes inaction.

Second, the ad must offer a specific recommendation for overcoming the threat. Fortunately for us all, there is a solution for the mite problem: Bloxem® super anti-mite mattress covers and pillowcases with special pores that prevent mites from making mattresses their home.

Third, your recommended course of action must be perceived as effective. Going back to the mite problem, your advertisement would have to demonstrate how the pores of the sheets are actually much too small for the mites to penetrate.

Finally, the consumer has to believe that he or she is actually capable of carrying out the recommended course of action. Fortunately, buying mite-proof sheets is as easy as pulling out your wallet.

These ingredients don't work by themselves. To use fear effectively, you need to use all four parts of the fear recipe.

### 4. People buy to satisfy their egos, something marketers can easily exploit. 

When was the last time you bought something in the hopes that it would make you look better? We all do that from time to time.

That's because we want to showcase whatever traits we have that are regarded by society as desirable. Remember: according to the LF8, we all want to be appreciated by those around us. 

In our pursuit of appreciation, we strive to possess (or feign possession of) highly valued traits — things like attractiveness, success, intelligence and sexual prowess. This drive is so powerful that most of us are willing to spend lots of time, effort and money to improve our public image. Just imagine all the people at the gym who hate being there but love looking athletic.

Marketers can use this desire to their advantage. For example, exercise can be painful, and many customers would rather buy a better image than put in the work. They'll also want to buy things that convey wealth, youthfulness or other desirable traits.

And that's where you come in: if customers want to buy a good image, you have to provide it!

Think about your product or service. Does using or owning it convey desirable characteristics? If so, invoke those characteristics in your ads. The classic example is the high-end sports car that few people can afford. If that's what you're selling, emphasize its exclusivity and luxuriousness.

It's possible that your product doesn't automatically trigger such associations. In that case, your ad will have to create the association.

Remember those people who want to look athletic? If you're selling running shoes, you need to sell an athletic image that complements them. For example, you could film a muscular runner wearing the shoes as he outstrips his competitors, crossing the finish line with effortless strides.

All of a sudden, your product is associated with athleticism and prowess.

### 5. Authority boosts your ad’s credibility. 

How can you convince people that _your_ products are better than your competitors'? You may say it a thousand times in your ads, but will potential customers actually believe you? Getting them to believe means earning their trust, and that requires authority.

One way to create authority is to transfer it from other people or institutions to your products. There are certain institutions — the church, the scientific community, national agencies — that people have learned to trust. If you can get one of these institutions to publicly support your product or service, then their authority is effectively transferred to you.

For example, if you're selling alarm systems that police themselves recommend as the safest solution on the market, no customer will question it, as police are generally seen as very trustworthy.

Similarly, celebrity testimonials can boost your product's credibility, as celebrities are highly influential in certain demographics.

Take the brand Proactiv, for example. It's just one of many companies that sell acne products. However, because Jessica Simpson have them a testimonial — a Hollywood celebrity with perfect-looking skin — young girls are more likely to believe in Proactiv's effectiveness and, consequently, buy the product (or have their parents buy it for them).

If you can't get an actual endorsement, you can still create authority by using symbols, images or ideas. For example, generally accepted images of medical or scientific authorities have the power to produce and transfer authority as well. You don't really need to hire an actual doctor to praise your product's effectiveness; convincing your customers can be as easy as hiring a model and throwing them in a white lab coat!

Marketing specialists have employed this time-tested trick for ages. In fact, its effectiveness was recently scientifically proven by the Institute for Propaganda Analysis.

### 6. Different kinds of products require different kinds of ads. 

Why do real estate ads look so different from ads for ice cream or cosmetics? The answer lies in our psychology.

Social psychologists have discovered that we don't make all buying decisions the same way. Rather, the more relevant a decision is for our personal lives, the more likely we are to ponder our options.

To describe this phenomenon, researchers developed the _Elaboration Likelihood Model_.

According to the model, we spend lots of time and energy on decisions concerning expensive purchases, like cars and houses. In such cases, a wrong decision could significantly influence the course of our lives, so we need to carefully consider all arguments for and against that purchase.

When making decisions of this magnitude, we use the brain regions that control reasonable and logical thinking.

In contrast, buying decisions for products with lower relevance, like food or gas station trinkets, are made quickly and intuitively. In these cases, we use those parts of the brain responsible for emotion and intuition, and we're more influenced by superficial images and cues, like colors and smells.

Different ads work better depending on the type of buying decision the customer faces.

For buying decisions with high personal relevance — e.g., if you're selling real estate — successful ads will speak to the logical parts of the consumer's brain. In other words, you need to provide facts, statistics and other evidence.

It's easier to influence consumers when products have lower personal relevance. For instance, while a customer might prefer a certain shower gel, chances are they're willing to try other brands as well.

All it takes to change their minds is to speak to the emotional parts of their brains. A flashy ad full of colorful, pleasant images, catchy slogans and/or celebrity testimonials could do the trick. That doesn't mean you can leave out facts and arguments entirely. You still have to provide some data to satisfy our fundamental need for information.

### 7. Create the perfect headline in just three steps. 

60 percent of all people who read ads skip over everything _but_ the headline. Thus, the words you begin with are critical for your ad's success. Luckily, there are some tricks you can use to entice people into reading on.

First, put the biggest benefits in the headline — the things that add value to your prospects' lives. The one thing your prospects care about is: What will this product do for me? Consequently, benefits are the most convincing argument for your product, and you should mention them first.

For example, if the benefit of a Rolls-Royce Phantom Coupé is that it offers luxurious comfort in all climates, then this should go in the headline.

Second, make sure that you're addressing the right audience. You can have the best product in the world, but if the right people don't know about it, then it won't sell. So make sure you communicate your offer to prospects that are actually interested in products like yours.

Imagine you own a bakery and want to sell your new fudge-filled pastry. Instead of saying, "Come in and sample our latest masterpiece!" you should say something like, "Attention chocoholics: Sink your teeth into our 9.5 pound devil's food volcano pie — absolutely free!"

This second ad speaks to chocoholics, and these are exactly the kinds of people you want to bring into your shop.

Finally, there _is_ such a thing as a perfect opening headline. Good headlines grab people's attention and motivate them to continue reading. To achieve this, try using some of these magical words: "free," "new," "how" or "just released."

For example: "Free book reveals how you can write covert ads that practically force people to send you money!" Wouldn't that pique your interest?

> _Unless your headline sells your product, you have wasted 90 percent of your money._

### 8. Use images in your ads – but not just any images! 

Images can make or break an ad. Look at the ads in your favorite magazine. Do you notice many faces?

It's no coincidence that many ads use pictures of the human face, and there are several good reasons to use them in your ads.

If you run a service business, perhaps as a consultant or a private nurse, putting your own face on the ad will give it a personal touch. This way, your company is no longer some faceless abstraction; instead, it'll be associated with a real person, thus enhancing your ad's credibility (unless you look like Peg-Leg Pete, of course).

Furthermore, portraits provide a nice way of highlighting your key message: just put your photo at the top of your ad and insert that message in quotation marks or a speech balloon.

When using portraits in your ads, you can make them more effective by using two simple tricks.

First, make sure the person is looking directly at the reader; this will grab attention.

Second, a smile is almost always a good idea. Smiles not only make a good impression, but also cause the observer to smile back. Smiling makes us feel better, and it's never a bad idea to have prospects associate good feelings with your ad.

Even if you decide against using a portrait, there are still reasons to use pictures of humans or animals in your ad. That's because people will look longer at pictures they like. So, if you had to guess, what kinds of pictures do people like?

We'll answer this question with the results of a study conducted by the Gallup Research Bureau, which surveyed 29,000 readers of 20 different Sunday newspapers. The readers were asked which photos they would prefer, and the pictures were ranked in the following order: children and babies, mothers and babies, groups of adults and, finally, animals.

### 9. The right colors can make all the difference. 

There are more flavors of ice cream than we can reasonably count, and yet, most of us prefer the same few varieties. Same goes for colors: we can discern several million hues, and yet most of us prefer the same colors and color combinations.

Many experiments have been conducted on the subject, and researchers have learned enough to rank our generally preferred colors. Most people prefer the color blue, followed by red and then green.

Also important is the way colors are combined. Generally, we prefer them to be complementary.

Researcher Daniel Starch demonstrated this in an experiment in which 32 men and 25 women had to rate different color combinations. The results showed that people's three favorite color combinations are blue and yellow, blue and red and, lastly, red and green.

So people have different color preferences. Why should you care? Well, people prefer ads in their favorite colors — that's why!

People like to look at ads that appeal to their personal taste. So when creating ads, you should take into account the color preferences identified by researchers and apply them to your ads, rather than relying on the personal taste of your ad designers.

What's more, color ads not only get more attention, but also trigger a 60-percent increase in close reading compared to black-and-white ads.

Finally, color can affect other senses, such as taste. For example, cans of Dr Pepper Snapple Group's Barrelhead Sugar-Free Root Beer used to have a blue background. When they changed it to beige, people said it tasted more like good old-fashioned root beer, which is exactly what Dr Pepper had had in mind. This, despite the fact that the recipe never changed.

So care about the colors you use in your ads, and pay attention to the color preferences of your prospective customers. It might make all the difference!

### 10. Don’t be afraid of longer copy. It won’t scare your clients away. 

People today seem busier and less patient than ever before. For that reason, many marketers believe that no one has the time or inclination to read long copy. Instead, they would advise you to keep your copy short. While this sounds totally reasonable, it's not actually true. But why?

Long copy, as long as it's well written, satisfies all kinds of readers. If people are really interested in your product, they'll likely want to learn more about it. The more information you provide in your copy, the more likely you are to convince them to take action.

Car advertisements demonstrate this well. They often show flashy new cars speeding through the city, eliciting envy and admiration in the viewer (emotions that appeal to the desire for social approval). However, they also provide lots of technical information about features and crash test results. With each new argument, readers and viewers become more and more convinced that _this_ car should be their next big purchase.

Even if your readers don't need that much text, it's not the length of the copy that will dissuade them from buying. As long as the headline is good enough and they're interested, they'll buy anyway. By providing catchy _and_ detailed copy, you can satisfy anyone who's interested in what you're selling.

Interestingly, studies have shown that longer copy gets even better results online. According to the web-consulting firm User Interface Engineering, people prefer sites with a few long pages to sites with many short ones, despite what these people may say about hating to scroll around on a webpage.

If that isn't enough evidence for you, consider this: Online-Learning.com decided to try out some copy on their website that was four times longer than before. The result? Five percent fewer people left the website after seeing only the homepage, and enrollments increased by 20 percent.

> _The only reason for using short copy is when there isn't much to say. - Maxwell Sackheim_

### 11. There is a science behind the right ad placement. 

People spend lots of time thinking about where to place their ads. Will it get more attention on the top of the page, on the left side or on the right, or at the bottom? Don't worry: recent studies have found all the answers.

While marketers may have very strong opinions about where to place your ads, the truth is, it doesn't matter much. In fact, Starch and the National Magazine, among other researchers, have all concluded that it is the ad itself that matters most.

However, that doesn't mean that you can place your ad _anywhere_. It doesn't matter whether you place it on the left or right side of the page, true. But it does matter _which_ page you place your ad on.

We can gain some insight into this concept by looking at the research of the consultancy firm Starch INRA Hooper, which examined the success of more than 10,000 single-page four-color magazine ads appearing on inside pages.

The highest "noted" — that is, seen and recalled — results were achieved by advertisements appearing on the inside front cover. Up to 25 percent higher scores can be achieved by placing the ad opposite a table of contents. The back cover gives you 22 percent higher scores than inside placements, and the inside back cover still gets six percent higher scores than other random placements inside.

Despite all this, if your ad still isn't getting enough attention, try putting a white frame around it.

You can't always afford to buy a full page for your ad. But you _can_ generate a similar effect with a little wit. One way is to frame your ad in white space.

Researchers Poffenberger and Strong conducted numerous experiments on the subject, and discovered that half-page ads framed in this way get 76 percent more attention than a standard half-page composition.

You now have the tools and the know-how. Go put it to use!

### 12. Final summary 

The key message in this book:

**In recent decades, consumer psychologists have gained insight into what consumers really want and what kinds of advertising entice them to buy. Once you learn the simple tricks that get us ready to buy by activating our primeval desires, you'll no longer need super expensive ad campaigns.**

Actionable advice:

**Make buying as easy as possible for your customers.**

There are a lot of barriers to overcome when you're trying to sell, and you don't want to create more for yourself by making customers feel unsure. Put your customer at ease by providing your exact address, phone number, email and web address. Offer them a longer guarantee than your competitors, so they know their investment is safe. And finally, just tell them: "It's easy to order!" They'll believe you.

**Suggested** **further** **reading:** ** _Confessions of an Advertising Man_** **by David Ogilvy**

_Confessions of an Advertising Man_ is a collection of advice and techniques for building successful advertising campaigns and agencies. Written in the era of Mad Men, the book is still considered essential reading in the advertising industry, but also provides advice for aspiring managers in any business.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Drew Eric Whitman

Drew Eric Whitman has worked for the direct marketing division of the largest ad agency in Philadelphia, and is now a consultant and teacher on the psychology behind consumer behavior. His work has been used by huge organizations, such as the Advertising Specialty Institute, American Legion and Texaco.

