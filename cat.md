---
id: 587a825fa8cb0a000443a1ad
slug: cat-en
published_date: 2017-01-16T00:00:00.000+00:00
author: Katharine M. Rogers
title: Cat
subtitle: None
main_color: 8C6C38
text_color: 73592E
---

# Cat

_None_

**Katharine M. Rogers**

Cat (2006) offers a lovingly detailed look at the biological, historical and cultural history of our furry feline friends. Each blink looks at a different stage of development, chronicling how the relationship between humans and cats has evolved from horrific superstitious misunderstandings to domesticated bliss. Meow!

---
### 1. What’s in it for me? Learn how cats went from prehistoric felines, to harbingers of evil, to pouncing their way into our hearts and homes. 

Do you share your home with a cat? If you do, you probably know that cats make wonderful roommates — they're tender, playful and just a tad diva-like.

But mice would tell you a different story: they'd say that you're feeding and pampering a cold-blooded killer with menacingly astute senses.

And then there's the mysterious behavior. Sometimes, seemingly out of nowhere, your cat appears on a shelf, and rather than greeting you with its usual kitty kindness, it fixes you with a sphinx-like, unwavering stare.

These blinks will tell you all about these seemingly incongruous aspects of cats and our relationship to them. You'll learn how cats evolved over millions of years, how they came to live with us as pets, why they make such incredible hunters, and why our superstitious ancestors feared and fought them.

You'll also find out

  * about the link between a sexist zoology book and prostitutes;

  * why one wounded woman was accused of being a shapeshifter; and

  * just how far back your cat's ancestry goes.

### 2. Cats have evolved over millions of years and domestication occurred only a few thousand years ago. 

Maybe you have a cat curled up on your lap right now. The way it naps and happily purrs throughout the day can make your feline friend seem rather unassuming.

Yet today's cuddly cats actually have a fascinating backstory involving 30 million years of evolution.

The first cat, known as _Proailurus_, weighed around 20 pounds and spent most of its time hanging out in trees. It had a long slender body that resembled today's civet, and, compared to the modern cat, it had more teeth and a less complex brain.

Then, following 10 million years of evolution, _Pseudaelurus_ emerged. While this species still had a longer body, it had teeth similar to modern-day cats.

From Pseudaelurus, two different relatives emerged: the sabre-toothed cats, which became extinct (along with their prey) around 10,000 years ago, and _Felinae_, the ancestors of our beloved cat.

Unfortunately, fossil records aren't perfect and it remains unclear how these first felines evolved into the domesticated cat. But fossils that _have_ been found reveal that its close relative, the wildcat, has been around for as long as two million years.

As for domestication, while the ancient Egyptians were close with their pet cats, widespread domestication as we know it today didn't take place until the eighteenth century.

However, before humans took cats into their homes, we already had an ongoing, mutually beneficial relationship; they often lived close to our settlements and helped keep them free of rodents.

But then, around 2000 BC, the Egyptians began a new and close relationship with their cats, which they documented in paintings on tomb walls that date back to 1450 BC. These paintings capture domestic scenes of family members and their cats sitting nearby, being fed or sometimes even being held on a leash.

After the ancient Egyptians, it wouldn't be until around the seventeenth-century in Europe that the status of the cat would start to shift from being a helpful mouse catcher to that of a cherished pet.

### 3. Equipped with keen senses and sharp teeth, cats make superb hunters. 

We all know that cats are great at catching mice, but did you know that they have some of the most specialized features of any meat-eating animal?

One reason for their status as a superior hunter lies in their extraordinary senses.

Let's start with their eyes. Cats' eyes are big and round with pupils that are so flexible they can be contracted down to a narrow slit or a small dot, or expand into a circle so large that it covers most of their outer eye. This gives them the ability to see perfectly whether it's a bright sunny day or a dark and cloudy night. In fact, a cat can hunt in _one-sixth_ of the light that a human eye requires in order to see.

But when cats hunt, sight isn't their only advantage — they also have an incredibly acute sense of hearing that helps them navigate and detect prey. The cat's outer ears can swivel around in all directions, amplifying every little sound, such as the scurrying feet of a mouse, telling it exactly where it needs to go.

Then there's the cat's sense of smell, which is around 30 times more sensitive than ours. So, even if there's no light at all, and not a sound being made, a cat can still sniff out a mouse in hiding.

Clearly, all these traits make the cat a deadly and feared predator. But we haven't even gotten to its teeth, which serve as an inescapable trap for its prey.

The canine teeth of a cat are razor sharp, and when they grab hold of their prey, these teeth clamp down between the victim's vertebrae, instantly paralyzing them.

Compared to a human, a cat's radar-like senses can make it seem practically supernatural. In the next blink, we'll take a closer look at how some of the qualities that make a cat a superior hunter also gave it a bad reputation among our more superstitious ancestors.

### 4. People throughout history believed that cats were mischievous and even related to witchcraft. 

If you have a fear of cats, don't feel too ashamed — people have a long track record of associating cats with some pretty shady behavior. 

In previous centuries, cats were sometimes seen as magical beings, even bearing the label of sinister creatures that were tied to witchcraft.

Some of these ideas likely stem from the fact that cats behave differently from other domesticated animals. Unlike dogs, cats are independent and self-reliant. They certainly aren't eager to please. These traits haven't always been appreciated.

From the Middle Ages to more modern times, some people have seen cats' unwillingness to obey commands, their stealthy maneuvering and their active nocturnal life as unholy behavior.

Beginning in the sixteenth century, these unique characteristics, along with the belief that cats could mysteriously disappear and reappear out of nowhere, got cats branded as agents of witchcraft. Anyone who kept a cat would often be suspected of being a witch and put on trial, especially if they were friendly with their cats or were seen speaking to them.

Eventually, cats were also seen as the creatures that a shape-shifting witch could transform into when she wanted to conceal her identity.

This is what Elizabeth Morse was accused and found guilty of in the seventeenth century; Morse supposedly transformed into a white cat-like creature to attack her neighbor. According to the story, the neighbor had fought back and left marks that were discovered on Morse's body the following day.

All these superstitions resulted in cats often being horrifically abused throughout the years.

In France, during the seventeenth and eighteenth centuries, cats accused of being devils were hung from maypoles. And Christian communities in this same period even burned cats alive in the hopes that such rituals would lead to an abundant harvest and chase away evil spirits.

Over time, people became less superstitious, and as you'll learn in the next blink, there are other, less sinister, views about what cats represent.

### 5. The cat has been seen as a symbol of female sexuality and prostitution. 

Tom Jones famously sang the song, "What's New, Pussycat?", and over the years, other feline-related words such as "kitten" have been used to describe attractive women. But where and why did all this start?

Believe it or not, cats have been associated with femininity since ancient Egypt, where the goddess Bastet had feline features and represented maternity and sexual allure.

It's possible that this all started due to cats being soft, pretty and graceful, which are traditionally traits associated with attractive women. And this hasn't changed; the image of a cat is still considered feminine, while the image of a dog is mainly considered to be masculine.

This representation can be seen in many works of art as well.

Take Francesco Bacchiacca's _Portrait of a Young Woman Holding a Cat_. Dating back to 1525, the painting depicts a young woman cradling a cat in her arms. With her sly glance toward the viewer, the wide-eyed cat serves to represent the woman's burgeoning sexuality.

For centuries, cats have also symbolized prostitution. Since the 1400s, female prostitutes have been called "cats" due to the animal's natural sense of self-preservation. Known for their time spent cleaning and grooming themselves, "cat" was seen as a fitting name for the female prostitutes that prettied themselves to make a living.

The naturalist and writer, Alphonse Toussenel, made direct, and rather unfavorable, comparisons to cats and prostitutes in his 1855 book _Zoologie Passionelle_. Here, he referred to both as being animals who care for themselves, like to have loud orgies and are unsuited for long-term relationships.

### 6. Today, we consider cats to be individuals and celebrate their independence and intelligence. 

Cats have indeed come a long way in just the past few centuries alone. And the good news for them is that, these days, they have come to be more like an adopted family member than a symbol of something sinister.

Today, many people regard cats as individuals. In the United States, there is even a movement to stop people using the term "pet owner," which implies that cats are property, and start using the term "guardian."

As society's views on human gender and sexuality have evolved, our view on cats has also become more complex; they are no longer reduced to simple characterizations or strictly associated with female sexuality.

In fact, we've come to celebrate and admire cats for their independent attitudes and their ability to take care of themselves.

Instead of being intimidated or fearful, people respect cats for not caring about our instructions and for being less dependent on others than we are.

These traits have come to be seen as implying a certain intelligence that has been celebrated in books and literature.

Rudyard Kipling, the author of _The Jungle Book_, also wrote a book called _The Cat That Walked By Himself_, in which a cat uses charm and intelligence to convince a woman to let it share the safety of a cave with her and her child. The cat is let in after it makes a clever promise to entertain the baby by doing things like purring and catching mice — which obviously come naturally to a cat.

Here's hoping we continue to give these fascinating animals and their long and varied history the respect they deserve.

> _"As we have become less comfortable with hierarchical order, we expect cats (as well as dogs) to be equal companions. . ."_

### 7. Final summary 

The key message in this book:

**Cats took many millions of years to evolve into the beloved animals they are today. Only a few hundred years ago, humans were quite wary of cats: we suspected them to be mischievous or morally depraved creatures, and we horribly mistreated them. Only recently have we come to appreciate cats for the remarkable creatures they are.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Genius of Dogs_** **by Brian Hare and Vanessa Woods**

_The Genius of Dogs_ (2013) uncovers the remarkable intelligence of man's best four-legged friend. By first examining human intelligence, the authors go on to explain exactly what makes dogs so smart, which talents they have in common with humans and other animals, and what sets them apart.
---

### Katharine M. Rogers

Katharine M. Rogers is a professor emeritus of English literature at the City University of New York, and has written extensively on eighteenth- and nineteenth-century literature. As an author, Rogers has shown her interest in a wide range of subjects with books such as _Meet the Invertebrates_ and _Port — A Global History_. Rogers is now retired and currently lives in Maryland, where she indulges in her passion for national history.

