---
id: 57f51c7122172d0003082d31
slug: catch-me-if-you-can-en
published_date: 2016-10-05T00:00:00.000+00:00
author: Frank Abagnale and Stan Redding
title: Catch Me If You Can
subtitle: The True Story of a Real Fake
main_color: 29A1CF
text_color: 1F799C
---

# Catch Me If You Can

_The True Story of a Real Fake_

**Frank Abagnale and Stan Redding**

Frank Abagnale's story of swindling millions of dollars from unwitting victims while posing as a pilot, lawyer and professor is the stuff of legend. In _Catch Me If You Can_ (1980), he tells his tale.

---
### 1. What’s in it for me? Step into the exciting, dangerous life of a world-class con man. 

Would you like to be a pilot? Fly all over the world and earn a ton of money? It's not easy; becoming a pilot takes years of study and training.

But what if there were a better way? What if you just bought a uniform and pretended to be a pilot?

As crazy as it might sound, that's what Frank Abagnale did in the 1960s. He wanted to travel the world for free, so he faked his way into Pan Am; he wanted a lot of cash, so he found ways to swindle banks. In short, Frank Abagnale was an expert con artist.

But how did he do it and get away with it for so long? From an early age, Frank pulled the wool over everyone's eyes, swindling, pretending and making a killing along the way. These blinks show you how for years nobody could catch him — that is, until they did.

In these blinks, you'll learn

  * how a gas card introduced Frank to the world of swindling;

  * how a con artist can fake a college degree; and

  * how the long arm of the law finally caught up with Frank.

**This is a Blinkist staff pick**

_"_ _I loved these blinks for their glimpse into the incredible life of con artist Frank Abagnale. Although he was a swindler and criminal, the part where he is thrown into solitary confinement still made me feel for him."_

– Clare, Editorial Quality Lead at Blinkist

### 2. Frank Abagnale’s early life was innocent until his taste for women ignited his criminal instincts. 

In 1948, Frank W. Abagnale Jr. was born in Bronxville, New York, to an affluent family. His father ran a stationery store in New York City, so the family was financially secure, and Frank's early childhood was happy.

But things started to unravel when Frank became a teenager.

Frank's father bought him an old Ford when the boy was 15 — a dream come true for any teenaged boy.

Naturally, girls started noticing Frank because of his car, striking the spark that ignited what became Frank's lifelong avocation — chasing after beautiful women.

Working as a part-time warehouse clerk, however, didn't provide the kind of income Frank needed for gas and to take girls out to dinner. So he did what many a teenager would do — he asked Dad for a credit card to cover car expenses.

His trusting father gave Frank his personal Mobil card, but told him that, while he would help him out within reason, most of the burden of paying off the debt on the card would be Frank's responsibility.

Frank intended to be honest and do the responsible thing, until he discovered that he could charge things other than gas to the card. He could purchase a new set of tires, for example, or pay for other services. That discovery led Frank to scheme his way into "earning" $3,400 over three months in a series of scams.

How did he do this? Frank would charge a service on the card, but instead of buying tires or new windshield wipers, he'd convince the gas station attendant to hand over the value of the purchase in cash instead.

When his father ultimately received the bill and Frank's schemes came to light, his mother sent him away to a boys' reform school.

By the time Frank returned home, the family's fortunes had suffered a tragic reversal. His father had lost his business and was now working as a postal clerk. Though Frank's father seemed content in his new job, it was a blow to Frank to see his family's fortunes destroyed.

So at the age of 16, Frank ran away from home to set off on his own.

### 3. Frank’s first schemes included cashing fraudulent checks and pretending to be an airline pilot. 

You might think the career options for a 16-year-old runaway might be limited. But Frank was never one to let anything stand in the way of getting what he wanted.

But what did this teenager want? Frank hungered after a combination of luxury and adventure.

With $200 in his checking account, Frank set off for New York City where he soon discovered that as a high-school dropout, options for well-paying jobs were slim to none.

While the best job he could get with his credentials was as a truck driver's helper at $2.75 per hour, Frank soon discovered that cashing phony checks was an easier and more lucrative way to make money.

Again, Frank's taste for women motivated his early criminal plans. To attract more female attention, Frank asked himself: Which profession is high-earning and irresistible to the ladies? Almost immediately he envisioned a tall, handsome and uniformed airline pilot. Luckily, Frank had discovered early on that his height made people think he was much older than he was.

So Frank decided the natural course of action would be to become an airline pilot — or at least _pretend_ to be one.

He figured that a pilot would never be suspected of being a crook or swindler. Even more appealing was the fact that professional pilots could fly virtually anywhere for free, a useful perk in evading the law as he pursued other swindling projects.

So Frank got himself a pilot's uniform and, in the following months, studied to hone the "skills" required for his new career choice.

Pretending to be a student doing a school report on future professions, Frank contacted a real pilot and pumped him for all kinds of insider information. Frank also studied library books and visited airports, hanging around other industry professionals to pick up mannerisms and professional jargon.

After a few months of preparation, he put on his suit and reported to "work."

### 4. Lax regulations and a talent for observation (plus a generous portion of luck) enabled Frank to keep up his con game. 

Given the increased levels of security and the wealth of digitized information now available, it's improbable that Frank could have pulled off such a con game as successfully today as he did in the 1960s.

Let's take a look at the circumstances that helped him get away with his scam for so long.

Lax airport policies worked in Frank's favor at the time. Long before terrorist hijackings were a threat, security at airports was minimal for both passengers and professionals.

Frank's scheme was to pretend to be a Pan American World Airways (Pan Am) pilot and _deadhead_ around the world. Deadheading was an industry practice that allowed pilots to walk on to almost any flight, claiming the need to arrive at a certain destination for work.

Special seats on flights were reserved specifically for this purpose. Frank walked on to hundreds of flights in this fashion, traveling around the world.

He also used his identity as a professional pilot to stay at hotels that airlines reserved for staff. Of course, he'd just send the bill to his "employer," Pan Am!

Frank's keen eye for detail and ability to learn quickly from what he saw were central to his success.

For instance, he kept a notebook that contained industry-insider phrases, technical data, names and backgrounds of airline people he had met, phone numbers and other miscellaneous info.

Such trivia was invaluable in keeping Frank's con going. The fact that he knew the fuel consumption per hour of a 707 airliner or that planes flying west do so at even-numbered altitude levels while planes flying east cruise at odd-numbered levels, helped maximize his persuasiveness as a real pilot.

Frank noted names and details about other pilots, too. If he were deadheading on a flight with three pilots from carrier National Airlines, for example, he'd sneak a peek at his notebook and then ask, "How's that Irishman Tom Cooper doing? One of you've got to know him!"

They would, of course. Such exchanges reinforced the validity of his status and kept Frank in the clear as he continued his game.

### 5. Frank faked being not just a pilot but also a lawyer, a professor and even a doctor. 

Frank didn't limit himself to just one profession; he was diverse in his impersonations and managed to convince most people in each of his cons.

In Georgia, Frank posed as a hospital doctor, but the way that it transpired was a bit of a fluke. When filling out an application for an apartment in an expensive, exclusive neighborhood, he wrote down "medical doctor" as his profession.

A nosy neighbor who also happened to be a doctor found out they "shared" a profession and wanted to befriend Frank, which forced Frank to continue the con.

It wasn't long before the neighbor asked Frank if he would do him a favor and take a position as a temporary shift supervisor at a hospital while management looked for someone permanent. Reluctantly, Frank agreed. But to his delight, he found he could get away with doing practically nothing at the hospital.

Whenever there was an emergency, Frank would give the responsibility to one of the many eager interns, who adored Frank because they felt like they were getting real experience!

After playing doctor for nearly a year, Frank tried his hand at being both a lawyer and a professor.

While still posing as a pilot, Frank faked a transcript from Harvard University and took the Louisiana bar exam. Having actually passed the bar, he landed a job in the Louisiana State Attorney General's office.

Unfortunately for Frank, this con came to an end after a colleague who had actually graduated from Harvard kept pressing Frank about his time at the university. Frank had to give up the act and move on.

Now in Utah, Frank applied to be a summer teacher at a local university, claiming to have been a sociology professor at the City College of New York. Again with faked transcripts and letters of recommendation, he got the job.

Just as he'd been adored as a doctor, his students loved him as a teacher. He would simply read one chapter ahead of time, and lecture his students on societal problems and crimes — based on his engaging experiences.

When the summer term ended, the school's professors were so impressed that they told him they'd try to lure him back for a permanent position!

### 6. After years of exciting and lucrative con jobs, Frank was finally caught and jailed. 

How long could Frank possibly keep up these wild adventures, conning his way into millions of dollars and living the high life? At the ripe age of 20, he decided to retire in Montpellier, France.

Yet Frank's plans didn't go as he expected.

Soon after settling himself in the southern French city, the French police caught up with him.

It turned out that law enforcement all over the world had been on Frank's trail for years. While he wasn't aware of the extent to which he was being tracked, Frank nonetheless kept on the move, changing his name and location often as a way to protect himself.

When the French police finally found him, he was sent to prison in Perpignan for one year.

While a single year behind bars might sound like a small price to pay for years of illegal activity, the prison in Perpignan was almost medieval. Frank sat in solitary confinement, his cell no more than five feet in each direction. No light, no bed — his toilet just a bucket.

Frank lived on bread and water. The cell floor soon became covered in feces as his jailers would only empty out the bucket every few weeks. His health began to deteriorate, his arms and legs developed sores and scabs. He was also severely malnourished.

In time, Frank was extradited to Sweden on separate charges. There, however, the police treated him more humanely and after doing time in Sweden, he was eventually extradited to the United States to face charges at home.

In the years following, Frank had his ups and downs, his straight life a mere shadow of the high life he'd formerly led. Once he'd served his sentence in the United States, his criminal record made finding and holding down a job a struggle.

Although he contemplated returning to the con game, Frank instead decided to approach a bank and offered to show employees how to detect false checks — a skill he had mastered in his teens.

Gradually, Frank became known as a white-collar crime specialist. His skills and first-hand knowledge were soon in demand by banks, airlines and other high-risk businesses.

Today, Frank Abagnale teaches at the Federal Bureau of Investigation, a curious "end" to a career of deceit; training future agents how to detect and apprehend con artists like himself.

### 7. Final summary 

The key message in this book:

**Frank** **Abagnale** **was a con artist in the 1960s whose astounding prowess at deception almost defies belief. By meticulously observing and studying the environments and people around him, Frank spent years posing as a pilot, doctor and lawyer, until he was eventually caught and jailed. He now spends his days a reformed man, training FBI employees in how to catch con artists like himself.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Confidence Game_** **by Maria Konnikova**

_The Confidence Game_ (2016) reveals exactly how con artists can strike it rich by taking advantage of some major flaws in human nature. Find out why people believe incredibly unlikely stories and ignore incriminating evidence, and discover how basic human trust and optimism can be used to a con artist's advantage.
---

### Frank Abagnale and Stan Redding

Frank Abagnale, known as a former confidence trickster, is now a professional consultant and lecturer at the US Federal Bureau of Investigation's Financial Crimes Unit.

