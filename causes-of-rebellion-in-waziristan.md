---
id: 566fe13dcf6aa30007000023
slug: causes-of-rebellion-in-waziristan-en
published_date: 2015-12-16T00:00:00.000+00:00
author: Khalid Aziz
title: Causes of Rebellion in Waziristan
subtitle: None
main_color: E6B95C
text_color: 806733
---

# Causes of Rebellion in Waziristan

_None_

**Khalid Aziz**

_Causes of Rebellion in Waziristan_ (2015) takes the reader on a journey into the rocky terrain of this tiny South Asian region whose geopolitical influence reaches far beyond Afghanistan and Pakistan. These blinks explore how the region became such a hotbed of insurrection and what can be done about it.

---
### 1. What’s in it for me? Understand the political situation in Waziristan. 

How much do you know about the political situation in Afghanistan? Probably very little. What about Pakistan? Probably even less. Now what about Waziristan? In all likelihood, you've never heard of it. And yet Waziristan's political situation is considered by many to be a centerpiece of international security. 

_Causes of Rebellion in Waziristan_ details the various attempts that have been made to stabilize this region. The book gives a comprehensive overview of a neglected subject, proposing solutions that we can try to implement in the future and discusses the various parties who continuously impede progress.

In these blinks, you'll learn

  * how Waziristan influenced political developments in Afghanistan;

  * how people there explain the wars in Afghanistan and Iraq; and

  * why the war in Waziristan is considered asymmetrical.

### 2. The little-known region of Waziristan is a strategic locus for the Taliban and jihadists. 

Have you ever heard of Waziristan? Many people haven't. However, this 5,000-square-mile expanse of tribally ruled mountains and barely arable land between Pakistan and Afghanistan is key geopolitical territory.

Why?

Because its strategic position has majorly influenced political developments in Afghanistan. For instance, Waziristan's close proximity to several Afghan districts allows the people of Waziristan, known as the _Wazirs_, and everyone else in the region to enter and exit Afghanistan with relative ease. 

In fact, in the past few years there have been countless attacks waged on Afghan soil by organizations like the Taliban, which, after assaulting US troops in Afghanistan, retreat to the mountains of Waziristan to recruit new fighters and plan their next foray. For instance, in January of 2007, over 40 trucks and buses full of armed fighters came into Afghanistan to fight US troops and Afghans before retreating to Waziristan. 

But why does the Taliban have so much power in this territory?

Because Waziristan has continually resisted the control of Pakistan, of which, legally, it is a territory. So, while the Pakistani government has made repeated attempts to assert authority over the Wazir tribes, the groups have resisted these attempts time and time again. 

For instance, in 2004, the United States convinced Pakistan to attempt to gain control of Waziristan. However, the tribes and foreign jihadists stood their ground against the military campaign and the Pakistani army couldn't maintain their grasp on the region. As the Pakistanis lost their footing and authority, the Taliban swooped in and became the new leading force in Waziristan.

In recent years, however, as the Pakistani military began moving back in, the situation on the ground has only worsened.

### 3. The various wars and conflicts in the South Asia have drawn in many nations. 

Knowing what you now know about Waziristan, it should come as no surprise that the US-led invasions of Afghanistan and Iraq have had a strong impact on the region. But how did these conflicts begin?

Actually, it's still disputed whether the Afghan and Iraq wars were waged because of resources or as an attempt to mitigate Islamist forces. In fact, over the years a variety of conspiracy theories have been proposed. 

Nonetheless, many people are of the opinion that the United States attacked these countries to gain control over oil reserves. That's because American negotiations with Afghanistan for new pipelines had gone poorly and the country was instead signing deals with an Argentinian company, Bridas, to tap its massive oil and gas fields. 

Yet, if you were to ask representatives of the US government, they'd say the intervention was an attempt to stop Islamist groups like Al-Qaeda and the Taliban from gaining more control of Afghanistan and the surrounding areas. The United States also maintains that they're committed to aiding Afghanistan in building a stable administration that can maintain power over Islamist forces. 

No matter who you believe, though, one thing is certain: the Taliban is a battle-hardened force that opposes the US presence in Afghanistan. For instance, although droves of foreign troops have entered Afghanistan in recent years, groups like Al-Qaeda, the Taliban, the tribes of Waziristan and jihadists are still fighting back. In fact, they regularly recruit new Wazir fighters using Islamist rhetoric and jihadi principles to turn them on foreign invaders like NATO, the United States and even the Pakistani army.

Not just that, but the Pakistani military isn't exactly looking out for Pakistan. Due to supply deficiencies, the Pakistani army has always relied heavily on aid, training and equipment from Western nations like Britain. In return, they fight for causes that are not necessarily in the best interest of their country.

### 4. The Pakistani military didn’t own up to its shortcomings in Waziristan until it was too late. 

Over the last few decades, the Pakistani military has made attempts to enter Waziristan and quell the ever-expanding threat of terrorist groups, jihadists and the Wazir tribes. However, they've been unable to keep the situation under control. In fact, the United States has repeatedly alleged that the Pakistani military _supports_ jihadists. 

While Pakistan has denied the allegations, their credibility suffered sorely. For instance, Pakistan has repeatedly claimed that it works to prevent attacks. At the same time, however, jihadists carried out countless terrorist attacks in Afghanistan after making their way through Pakistani military checkpoints. Not just that, they even returned through those same checkpoints after killing people in Afghanistan!

Naturally the United States wanted some explanations. But instead of admitting that they were overwhelmed and undermanned, the Pakistani military and government acted like they had everything under control. 

And when the Pakistani military finally did admit their failure, they only succeeded in giving more power to the tribes. That's because, for years, the Pakistani military had attempted to pacify the region. But in 2006 it owned up to its failures and cut deals with the Taliban and the Wazir tribes, thereby ceding power to them in North Waziristan. 

As a result, terrorist attacks on US forces close to Waziristan tripled. No longer occupied with fighting the Pakistani military, the terrorist militias could now focus on foreign forces.

### 5. The war in Waziristan is a war of belief and religious rules are being imposed on the citizens of the region. 

To understand the situation in Waziristan, it first helps to understand that the conflict being fought there is not a typical war — in fact, it's asymmetrical. 

When countries wage war against each other, differences in military strategy and equipment often bring decisive advantages to one side or the other. But the situation in Waziristan is entirely different. 

The Pakistani military is facing an enemy that's unlike any they've been trained to fight: terrorists and insurgents that employ unconventional tactics in an area as familiar to them as the backs of their own hands. As a result, the war is being fought differently than most. But what does this mean for the people living in the region?

Actually, the people of Waziristan are forced to adhere to extremely strict rules. For instance, the insurgents require Wazirs and those living in the adjoining districts to abide by sharia law. As a result, judges are not allowed to preside over certain cases, men are forbidden from shaving their beards, music and TV are banned and women are forced to wear burkas.

And those who don't follow the rules?

They are either killed or forced into submission, despite vehement protests. 

This strict control makes sense considering that this war of conviction is really a war for the minds of the public — meaning it's not about who will rule the state, but about convictions and beliefs. Essentially, the side that prevails in convincing the public that they are in the right — that they are the true fighters — will be the winners in the long run. Therefore, the Taliban isn't fighting for symbolic citadels or cities, but for control over the minds of the people.

### 6. Waziristan’s social problems and its issues with Afghanistan have to be addressed. 

Today Waziristan faces innumerable problems — from conflict with Afghanistan due to the security threats in the region to deep social issues like high unemployment and poor access to education. 

With so much to fix, where do you even start?

Well, what first needs to be addressed are the problems between Waziristan and Afghanistan. For instance, instead of letting the military serve as the main representative in the area, there should be strategic attempts to mobilize the society at large to keep the Taliban out of the region. For this to happen the military would need to pull back and let civilian authorities address the problems, instead of foolhardily trying to stop violence with more violence. 

But that's just the beginning. Waziristan also needs to invest in a variety of social developments in the coming years. In fact, if the Pakistani government has real hopes for peace in the region, they'll need to start large-scale programs for unemployed young men. 

That's because current projections of unemployed young men in Waziristan stand at a shocking 80,000! These people are especially prone to recruitment by the Taliban, which holds out to them and their families a means of survival. 

Not just that, but it's also necessary for there to be an overall increase in political development in Waziristan. For such a process to be most successful, it should be open to all political parties and not just the internal tribal forces vying for power. 

And finally it's necessary for general development to occur. A good place to start would be building roads, extending internet and phone networks to the area and training a police force. 

While none of these changes will be easy, they're the only way for Waziristan to prevent itself from falling even further into disrepair — and the only hope for a stable future.

### 7. Final summary 

The key message in this book:

**Waziristan is a small region key to the conflicts in South Asia. The area has a high concentration of jihadists and proxy-fighters due to a weak Pakistani military and a general lack of aid to the region.**

**Suggested** **further** **reading:** ** _Behind the Beautiful Forevers_** **by Katherine Boo**

_Behind_ _the_ _Beautiful_ _Forevers_ describes life in the Annawadi slum in India, close to Mumbai's international airport. These blinks tell the story of families who live in squalid conditions but still dream of a better life, even though the odds are overwhelmingly against them.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Khalid Aziz

Khalid Aziz has more than three decades of experience in various management positions within the federal and provincial governments of Pakistan. In 2005, he founded the Regional Institute of Policy Research & Training, in Peshawar.

