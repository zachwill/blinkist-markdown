---
id: 53bbf86d6165630007020000
slug: challenging-coaching-en
published_date: 2014-07-08T00:00:00.000+00:00
author: John Blakey and Ian Day
title: Challenging Coaching
subtitle: Going Beyond Traditional Coaching to Face the FACTS
main_color: EC2F39
text_color: A12027
---

# Challenging Coaching

_Going Beyond Traditional Coaching to Face the FACTS_

**John Blakey and Ian Day**

_Challenging_ _Coaching_ argues that traditional coaching is limited by its therapeutic origins. Blakey and Day introduce a better alternative for the twenty-first century business environment: the FACTS approach. Its emphasis on demanding challenging _Feedback_, _Accountability_, _Courageous_ Goals, _Tension_ and _Systems_ _Thinking_ drives a client to achieve their full potential.

---
### 1. What’s in it for me: Find out why coaching needs an update for the twenty-first century. 

Coaching became popular in the 1980s as a direct consequence of the "war over talent." As the economy became more knowledge based, the development and retention of top talent became necessary for success. So companies started hiring coaches to ensure that their employees reached their full potential.

But have you ever asked yourself: from where did coaching appear?

Coaching developed from existing support-oriented professions like counselling and psychotherapy that are based on certain core-principles, such as being non-directive, and helping clients find their own solutions. These principles are effective for helping people in need of care, but are they effective in the case of coaching?

_Challenging_ _Coaching_ argues that this approach might have led self-obsessed financial traders to focus only on their own personal rewards — with catastrophic results for their companies, and even for the financial market as a whole.

So what's wrong with the old approach?

Traditional coaching tends to fuel leaders' egos with praise, rather than challenging them with honest feedback. And instead of focusing on accountability and the bigger picture, traditional coaches just help the client fulfill their personal goals, and stand back when they see their client breaking their own values.

_Challenging_ _Coaching_ believes this has to change.

In the following blinks, you'll find out:

  * how traditional coaching works, 

  * what its original inspirations were, 

  * how those roots are holding it back, and 

  * what a better alternative looks like: the FACTS approach, which moves away from myth-based coaching to a model that actually works.

### 2. Traditional coaching’s original influences were mainly support oriented. 

Even though it might seem to have existed forever, the coaching profession is actually still in its teenage years. When it first emerged in the 1980s, it was still very much a toddler, and easily influenced by its older peers — like psychotherapy and counselling.

This influence means that coaching shares a core foundation with support-oriented counselling disciplines where the client gets unconditional positive backing.

For example, active listening and powerful questioning are the two skill sets that are fundamentally required for counselling, mentoring, therapy — and coaching. These skills are considered necessary for developing an understanding of a client's needs — which is the starting point of all progress — and helping them find their own answers, which is key to self-determination, a major value of support-oriented disciplines.

But when coaching was born, it knew none of all this. So, in search of orientation, coaching took its biggest inspiration from _person-centered_ _therapy._

Person-centered therapy was developed by Carl Rogers, one of the most influential therapists of the twentieth century, renowned for his humanist methodology. His non-directive approach is based on the fundamental belief that a client already possesses the vast resources necessary for development. The therapist's role is to help the client find their own solutions by creating a safe space for growth using empathy (seeing through another's eyes), congruence (being completely open and honest) and unconditional, non-judgmental positive regard. These principles have become the foundation for traditional coaching models.

For example, the popular Co-Active Coaching model significantly overlaps with Carl Rogers' principles: the client is considered naturally resourceful, the agenda comes from the client, and the relationship is an alliance designed for the client's growth.

Another example is the well-known GROW model, which also shares its main assumptions with Rogers: The client is considered able to find their own solutions, empathy is focused to create a safe space, and understanding is developed through non-directive questioning.

### 3. This support-orientated foundation led to three core coaching principles. 

The newborn coaching profession was able to derive its methods and ethos from the theoretically established and practically proven foundations of psychotherapy and counselling. These predecessors have influenced coaching down to its core, and coaching literature widely agrees on three support-orientated _principles_ that are emphasized as fundamentally important:

  * _The_ _nondirective_ _approach._ This principle assumes that the client — and not the coach — is the expert of their own situation: the client has the answers to her own problems, and the coach merely helps uncover them. The coach's role is therefore to ask questions, listen, reflect, offer support and empower the client rather than problem-solve for them and give advice.

  * _Respecting_ _the_ _client's_ _agenda._ This principle states that the coach must resist directing the client's agenda — even if they think that doing so would help. Fundamentally, it is always the client who decides which topics and areas are to be worked on — not the coach.

  * _Building_ _rapport._ Traditional coaches believe that effective progress can only happen on a foundation of strong empathy between the coach and client. The building of such a bond is assumed to lead to the fundamental trust and rapport necessary to create a safe space to grow.

But while psychotherapy has gone through many paradigm shifts since its creation, and has constantly updated its core principles, coaching still holds on to its founding principles. Are these still up to date? Or is it time for change?

Read on to find out.

### 4. These core principles limit progress and bear great risks. 

We've now learned about the origins of traditional coaching. But what if its fundamental principles are flawed to their very core? What if these principles need to be _challenged?_

_FACTS_ _coaching_ believes that traditional coaching's core principles hinder progress.

To begin with, the nondirective approach is a myth: even with limited interactions, a coach inevitably influences the client. And if a client is stuck, the nondirective approach will leave them there far longer than necessary. By abandoning the nondirective approach, the coach could use their expertise to make suggestions that could stimulate new solutions.

Then there are the limiting effects of sticking to the client's agenda.

For example, clients sometimes avoid difficult issues that must be tackled to overcome other problems. By moving beyond the client's agenda, a coach is empowered to challenge those issues head on.

Finally, even if building rapport is key to the coaching relationship, it should not get in the way of clients reaching their full potential.

Rapport is fundamental for helping the often sensitive and dysfunctional clients who require psychotherapy and counselling. Coaching, however, often deals with robust people who can be pushed to their limits. This is why coaches must be allowed to take clients out their comfort zones to break through growth barriers.

By limiting itself to these principles, traditional coaching can lead to the drastic consequences of collusion, irrelevance and self-obsession.

An overly supportive, non-judgemental coach _colludes_ with the client. This coach aligns with the client's world view and fails to offer feedback from a different perspective — even when the client has got things wrong.

And if coaches always hold on to the client's agenda, this can lead to _irrelevant_ work. Coaching sessions become time-killers instead of a push for the right outcome for the company as a whole.

When these two approaches are combined, clients learn to focus solely on their own situation, and can become _self-obsessive_. When leaders focus too narrowly on their own agenda, this often leads to negative consequences for the wider organization — as we'll see in a later blink.

### 5. FACTS-based coaching is fundamentally more effective than traditional coaching. 

Have you ever had a teacher who became a good friend? Who filled your lessons with laughter and joy? That's great — but for the sake of your progress, it might have been better if you had felt challenged rather than entertained, and had seriously tackled your weaknesses instead of focusing on fun. The same logic can be applied for coaching.

FACTS-based coaching aims to find the right balance of challenge and support, in contrast to traditional coaching, which focuses too much on support. This lack of challenge leads to limited action, because taking risks could mean disrupting the cosy coach–client bond.

So what can be done?

The key to growth is to provide ambitious challenges with equally high levels of support. Not challenging the client will only lead to small results, as there are no big goals to reach, and just giving clients huge challenges without support will only lead to stress and failure. Growth happens when the coach sets goals that challenge the client and supports her in reaching them.

One way FACTS-based coaches challenge their clients is by leaving the comfort zone of traditional coaching for areas of new growth.

Coaching conversations usually take place in a "zone of comfortable debate," because beyond that, things feel tense and harder to control. But to tackle issues at their heart, it's necessary to enter the ZOUD: the "Zone of Uncomfortable Debate." Being able to enter and sustain the ZOUD without damaging the coach–client relationship is a key component of FACTS-based coaching. To do so the coach must be challenging yet respectful, empathetic yet growth orientated.

Now that we've explored what's wrong with traditional coaching, let's explore the core concepts of the FACTS approach.

### 6. F: Coaches must overcome their fear of giving Feedback. 

There are three main reasons why traditional coaches don't provide honest feedback:

1\. Feedback might have been experienced as judgmental. We've all felt angry when someone has criticized us, which can make us reluctant to make suggestions of our own.

2\. The coach is not confident in his or her feedback skills. Not all coaching training courses include feedback training, which leaves a lot of coaches lacking a core skill.

3\. Finally, the coach does not feel they have the client's permission to give feedback.

But not giving challenging, honest feedback is a risk — because feedback is _quintessential_ for the client's progress.

Why?

Firstly, feedback can uncover "blind spots." Another person's perspective is sometimes the fastest way of bringing an issue to light, even if it's something simple: that piece of broccoli between your teeth is something you can easily solve, but only if you know about it!

Feedback can also uncover so called "no go" areas. These are issues that clients are sometimes aware of, but prefer to ignore. Having somebody to keep you on track can jolt these issues back into the forefront of your mind, and help you tackle them head on.

Finally, feedback is a powerful tool for preventing bad decisions. Confident business leaders are often surrounded by employees who are afraid of giving them negative feedback. Coaches can then play the role of delivering the honest, bad news.

So how can a coach overcome their initial fear of feedback?

1\. By ensuring that feedback is non-judgemental: by critiquing the client's behavior instead of their personality, coaches can increase the receptivity of their feedback.

2\. There are also well-established process models for providing feedback that coaches can learn. One example is the four stage approach: Observe the facts, judge their impact, invite the client to a conversation about your observations and assumptions, and agree on future action.

3\. Finally, just ask if you may! If your client is not open to feedback, it's better to find out sooner rather than later.

### 7. A: Clients must be held Accountable for their commitments. 

During the oil spill crisis in Louisiana, BP's CEO Tony Hayward found himself at the eye of a political storm. Giving his testimony before the US Congress, he distanced himself from the company's mistakes and struggled to recall any of the events he was asked about. His behavior provoked fury among not only the direct victims of the oil spill, but also the public at large.

This kind of behavior has led society to demand greater transparency and accountability on a governmental and economic level. Political parties and major public debates have even formed based on these issues — for example, the Pirate Party and Snowden's NSA revelations both revolve around this topic.

One key element of these new public demands has been the accountability of business leaders. But what business leaders often lack is somebody to actually _hold_ _them_ _accountable_.

This is where coaches come into play.

Coaches must move beyond traditional models where clients hold themselves responsible to a new model of holding their clients responsible on a personal and corporate level.

For example, specific commitments must be agreed upon in contracts. If major decisions at BP had been committed in writing to their respective decision makers, the chain-of-command wouldn't have been as unclear as it was revealed to be when the crisis struck. And maybe the decisions would have been made differently in the first place if somebody's name was attached to them.

But beyond this, coaches need to hold their clients accountable to the larger commitments their organization has made — for example, their organization's mission statement, corporate values and social responsibility agenda.

### 8. C: Courageous Goals are crucial for thriving in the current business environment. 

In May 1961, US President John F. Kennedy stood before Congress and proclaimed that the United States "should commit itself to achieving the goal, before the decade is out, of landing a man on the moon and returning him safely to the earth." But a traditional coach would never have agreed with Kennedy on setting this goal.

Why?

Because traditional goal-setting processes inhibit great outcomes.

Traditional goal setting uses popular guidelines like SMART (Specific, Measurable, Agreed, Realistic, Timed) and PRISM (Personal, Realistic, Interesting, Specific, Measurable), which focus on rational, realistic goals.

While this focus does lead to reliable outcomes within a predetermined range, its aversion to risk inhibits transformation on a bigger scale: aiming only for "realistic" goals prevents the leap of faith necessary for huge innovations — like taking man to the moon.

The truth is that setting courageous goals leads to real transformation — which is not only desirable, but necessary.

Why?

Because in a global and fast-changing economy, businesses need to be more flexible than ever before. And to successfully transform their organizations, clients need the creativity and courage to tackle the unknown — resources which can only be unleashed by setting bold goals.

Traditional goals might be to "increase sales by ten percent in the next quarter," but that's not enough to satisfy today's big players. The late Steve Jobs is famous for wanting to "put a ding in the Universe" at Apple; Facebook set out to connect the whole world; and Amazon is trying to sell everything to everyone. These companies aren't simply naive: they are courageous.

> _"I love it when you challenge me, so challenge me more!"_

### 9. T: Tension is essential for optimal performance. 

Alexander Karelin felt pretty relaxed. At the 2000 Olympic Games he was the three-time defending champion of Greco-Roman wrestling, and had never lost a single match in his 15 years of international competition. His opponent in the finals, Rulon Gardner, had never finished higher than fifth in any international wrestling competition, and was arguably perceived as the easiest opponent Karelin had ever to face in a final match. But although Alexander Karelin had won 887 wrestling matches, he lost this one. There was no cheating, no injury, no bad luck — nothing out of the ordinary.

So why did he lose?

Some thought he was too nervous. But after more than 80 victories and three Olympic finals with more threatening opponents, this is highly unlikely. In fact, the opposite might be true: he was probably not nervous enough.

This agrees with the FACTS perspective that _tension_ is necessary for optimal performance.

Traditional coaching's techniques and values, having emerged from the supportive counselling disciplines, focus on reducing stress and tension. The traditional coaching approach to Karelin's final match would have been to worry about the potential pressure he was feeling, and do their best to dissipate it.

But countless psychological findings confirm that everybody has an optimal level in between anxiety and comfort where they enter a state of flow and peak performance. Every athlete can confirm that a certain level of adrenalin is needed to bring their best performance to the table.

And what triggers the production of adrenalin?

FACTS coaching aims to maintain this optimal level of tension by pushing the client to their limits. And because high-achieving people often operate optimally in higher levels of tension than coachs, the tension has to be calibrated to the client, and not to the coach.

The most successful professional athletes and their coaches often state "keeping the tension" as their major challenge for sustaining excellence. And if you don't aim high enough, this tension is lost in advance.

### 10. S: System Thinking helps prevent individuals bringing down entire companies. 

We've all heard of the bank employees who took down the whole bank. Nick Leeson is one of them. A former broker at Barings Bank, the United Kingdom's oldest investment bank, he earned Barings £10,000,000 in his first year as general manager of future markets, which translated into a £130,000 bonus for himself. Motivated by his bonuses, Leeson started investing in increasingly risky trades — which led to the demise of the entire bank. By the end of 1995, his investments totalled a loss of £827,000,000, and Barings had to declare bankruptcy.

What went wrong?

Leeson narrowly focused on his individual goals, which led to a lack of _system_ _awareness_.

Clients are particularly prone to focusing chiefly on their own performance — because coaches traditionally encourage them to do so. Leeson's only motivation was to push his personal outcomes, thinking that this is what was expected of him. What he didn't realize is that the risks he was taking were threatening not only his own fate, but that of the whole system.

What could have saved Leeson and Barings?

A FACTS-trained coach.

It's a FACTS coach's job to raise a client's awareness of the system as a whole. This should be done by repeatedly asking questions that question the wider implications of the clients actions — without offering an opinion and avoiding to take the moral high ground. The goal is intelligent growth — not moralizing.

Nick Leeson was eventually captured and sentenced to six and a half years in prison. His actions remain a warning of how single individuals can ruin whole organizations. Keeping this in the big players' minds without inhibiting their potential is a sensitive task — but FACTS coaching offers the tools for the challenge.

> _"What shaped business coaching as it developed was a focus on individual wants, not organizational needs."_

### 11. Final summary 

The key message in this book:

**Traditional** **coaching's** **therapeutic** **origins** **prevent** **it** **from** **yielding** **optimal** **results.** **Coaching** **needs** **to** **face** **up** **to** **the** **FACTS** **and** **offer** **honest** **Feedback,** **make** **sure** **its** **clients** **are** **held** **Accountable,** **set** **Courageous** **goals,** **find** **and** **maintain** **the** **perfect** **Tension** **and** **pay** **attention** **to** **the** **System** **as** **a** **whole.**
---

### John Blakey and Ian Day

John Blakey and Ian Day both have extensive experience coaching board-level leaders all around the world.

