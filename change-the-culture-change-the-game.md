---
id: 55000313646439000a020000
slug: change-the-culture-change-the-game-en
published_date: 2015-06-01T00:00:00.000+00:00
author: Roger Connors and Tom Smith
title: Change the Culture, Change the Game
subtitle: The Breakthrough Strategy For Energizing Your Organization and Creating Accountability For Results
main_color: 997945
text_color: 66512E
---

# Change the Culture, Change the Game

_The Breakthrough Strategy For Energizing Your Organization and Creating Accountability For Results_

**Roger Connors and Tom Smith**

_Change The Culture, Change The Game_ (2012) demonstrates how to implement a culture of accountability within your organization. You'll discover how to help encourage a shift in thinking to get the game-changing results you want and explore the steps needed to sustain such changes.

---
### 1. What’s in it for me? Discover how to energize your organization’s culture. 

Too many leaders and management consultants try to take short cuts when tasked with shaking up the culture of an organization. Just by stating, "We want a higher profit margin," they think that employees will jump to action and align their work with this broad and poorly conceived goal.

It just doesn't work that way!

These blinks show you that to change a culture, you have to integrate that change from the very top of your organization to the very bottom. And focusing on results and goals is just one small part of the puzzle. You have to consider all the elements on the table — company experience, employee beliefs and ingrained practices — and then ensure that what you change is actually sustained.

Yet in doing so, you'll find that your organization will hum along like a well-oiled machine, and processes that were previously bogged down now run with precision and ease.

In the following blinks, you'll discover

  * how General Motors overhauled its corporate culture and stemmed massive losses;

  * why changing a culture is a transition and not a transformation; and

  * why one bookstore lets employees give tokens to each other.

### 2. Use the results pyramid to achieve your goals. Define your firm’s experiences, beliefs and actions. 

Managers shape a company's culture every day in the experiences they create for employees. Employees in turn learn how things are done, and an organizational culture establishes itself.

Yet not all organizational cultures are healthy. In some cases, they can even do more damage than good.

This is why leaders need to focus on creating a culture that is beneficial for the company as a whole, from the lowest levels all the way to the top. But how is this done?

The first step is to define your goals and a strategy to reach them. For example, "Make decisions. Take risks. Move fast. Be accountable," was the strategy General Motors put forward in 2009 to change its culture and stop the company from losing money once and for all.

How did GM decide on this route? To shape your culture and help your organization win, you have to understand the _results pyramid_.

There are three key components that make up the results pyramid: experiences, beliefs and actions. Stacked on top of each other, the components all contribute to the end result, or the achievements of your organization.

In short: experiences promote beliefs, beliefs impact actions and actions generate results.

Alaris Medical Systems had a bad rep on Wall Street and was criticized for its inability to execute on otherwise quite good ideas. This was the case, that is, until the company turned to the results pyramid as a guide to change its culture.

Alaris executives discussed with every manager how to create the right experiences to foster the desired beliefs, which would in turn produce effective actions, and ultimately achieve the company's desired target results.

Once the program was introduced, every employee at every level was motivated, optimistic and determined to get results and execute. Within six months, Alaris had completely turned their reputation around!

### 3. A culture of accountability will see your organization perform at the highest level. 

So what kind of culture exactly should you be thinking about implementing within your organization?

The key is in the last line of General Motor's reformed strategy: "Be accountable."

Accountability is vital to a strong organizational culture, and it is shaped by every action an employee performs. In organizations, a thin line separates great companies from poor ones. In general, there are two modes of acting: _above the line_ and _below the line_. Only one promotes accountability.

By acting _above the line_, we can move toward accountability in four simple steps. First, we have to _see it_, by considering other people's viewpoints, communicating honestly and candidly, exchanging feedback and hearing the difficult truths that expose the real circumstances at hand.

Second, we need to _own it_ by accepting the goals and priorities of the organization's mission as one's own. Only then are we able to s _olve it_, by asking what else we could do to get the results we want.

Finally, we've got to _do it_, and perform the tasks we said we would, focus on our priorities and be reliable and trustworthy. 

When everyone in an organization decides of their own free will to take _above the line_ steps, you'll have a _culture of accountability_.

While your company should strive for above the line actions, _below the line_ actions will only harm your organizational culture.

When we act _below the line_, we refuse to accept responsibility and instead become obsessed with blame games, cycles of finger pointing and employees playing the victim.

Of course, acting below the line is also human. But by remaining below the line, you'll tread in dangerous waters regarding accountability.

Accountability shouldn't make the question, "Who's accountable for this?" sound like a punishment. It should instead be empowering. Accountability isn't about "getting caught" or failing, but should be approached as playing a starring role in reaching a solution.

### 4. To shift your culture, you have to shift the experiences, beliefs and actions of those in it. 

In a world where being first means everything, you can't take your time implementing a culture change. But equally importantly, you can't take shortcuts, either!

Too often, leaders try to change the way people act to get the results they want without working to change the way people think or what they believe. So while employees might appear involved, they won't actually be committed to the company's cause.

This means that only the top of the results pyramid is being addressed — actions and results — while the pyramid's two fundamental elements — experiences and beliefs — are being neglected. 

To better understand why this doesn't work, let's call R1 your current results, as produced by the current culture, C1. Then let's call R2 the desired results that you'll get with C2, the new culture.

It's impossible to get results with your current culture, C1. So to create C2, you have to create new experiences, E2, and new beliefs, B2. Only these can effectively shape new actions, A2, to generate improved results, R2, as your new culture, C2, emerges. 

Creating a shift in experiences and beliefs takes more effort. But it will yield substantial, long-lasting changes that will then create a firm foundation for the results you want.

This process occurs on different scales and at different speeds for different companies. Think of assuming a new approach to your organization's culture as a _transition_ and not as a _transformation._

An optical retailer wanted to test a new organizational culture, and so the company ran a pilot in a few retail stores. After two months, business improved more than 5 percent.

The change in culture had led to the desired R2; results, as employees were thinking and acting differently with regard to daily work. Soon thereafter, the retailer took the change company-wide.

### 5. Make sure everyone in your organization is on the same page before you try to push for change. 

How can you get everyone in your organization to rally around those key R2 results?

Simply: _alignment_.

Think of alignment as the guiding beliefs and deliberate actions that an organization applies to a clear and common goal.

When people are all on the same page, following the same goals, there's less stress, decision making is faster and more efficient, and other processes across the organization also speed up.

In this way, alignment is vital for effective cultural change.   

  

Still not clear on why this is the case? Think about what happens when you have confusion about your goals. This can kill the momentum in your push for change.

Why? Because nobody's aligned. They don't know where to push, or in which direction to go.

For instance, fast-food chain Fast Grill wanted to improve its profit margins so the company could expand. During a meeting, the author asked Fast Grill's top management what key results the company was aiming for.

They all replied "profit margins." Seems aligned, right? But when the author followed up and asked what the exact margin goal was, everyone in management gave different answers!

Fast Grill management's confusion led to poor execution, below the line behavior, and thus slow change.

To avoid this situation, ensure that everyone's on the same page and totally aligned!

Remember: alignment is a process, not an event. It is something you must constantly work to achieve.

### 6. Leading a culture change requires responsibility, responsiveness and facilitation. 

If you want to guide a culture change in your organization, there are certain skills you'll need as a leader. Having such skills are key to accelerating the process and enhancing your overall leadership.

So what are these skills?

The first thing you'll need to take your organization from C1 to C2 is the _skill to lead the change_. In other words, you can't just assign the responsibility to another department. As a leader, it's up to you to keep abreast with everything going on as part of the change at every level.

When you're leading a change on such a major scale, you'll no doubt receive criticism. To deal with this, you'll need the _skill to respond back._ There are five steps you should take in response to feedback, to ensure everyone is aligned toward C2 _._

First, you must identify the beliefs you want others to share. Second, you need to communicate this belief; and third, you should portray the experience you're going to deliver to your employees. Fourth, you need to ask for feedback on the planned experience, and finally, enroll employees in providing feedback as the plan progresses.

The final necessary skill to lead a change is the _skill to be facilitative._ You want meaningful dialogue about the culture change in your organization? Then you've got to encourage dialogue, teamwork and collaboration.

Sony VAIO wanted to improve repair-related customer satisfaction scores by 15 percent over the previous year. Steven Nickel, VAIO Service vice president, energized the organization around R2 by opening the door to an ongoing dialogue.

Many team members who had never spoken up in meetings before enthusiastically shared numerous improvement ideas, and VAIO Service ultimately exceeded its goal.

### 7. Sustain your culture change by continuously integrating it in all meetings, systems and practices. 

Making all these plans and implementing a change means nothing if you can't sustain it!

There are three simple steps to ensure you integrate C² practices into the everyday life of your organization.

First, list all the meetings, policies and procedures that the organization has. Second, find where you can integrate a culture change into your organizational systems. And finally, apply the changes into the activities you've selected.

Eastside Health Plans offers an example of this three-step process. After the Eastside management team evaluated the first two steps in the integration process, they arrived at a plan. The team took early steps to align their organizational systems as well, from administration to meetings to human resources.

For the first time in 25 years, leaders began to create significant experiences that suggested the company would not tolerate waste. Teams were formed to address waste as well as inefficiencies and administrative costs within the organization.

These teams ultimately achieved line-item cost savings of over $200 million!

If you can integrate a culture change effectively, your employees won't see it as just another annoying management program. Instead, it will become an enjoyable part of life at work.

For example, one of the author's daughters took a job at a local bookstore and shared with him how much she enjoyed the culture at her new job.

When asked what she enjoyed the most, she explained that whenever an employee saw another employee doing a good job, they could give that employee a token, to earn free products from the store.

In fact, the store was one of the father's clients, and one that had integrated a culture of accountability into their organization so successfully that the culture became a positive aspect of an employee's day!

### 8. Final summary 

The key message in this book:

**Take your organization to its full potential by implementing a culture change and fostering accountability across all levels. By considering relationships between experiences, beliefs, actions and results, steps can be taken toward building and then sustaining a new organizational culture.**

Actionable advice:

**Take it step by step!**

To help ensure accurate alignment around key decisions with everyone in an organization, the _leadership alignment process_ is a useful guide. Step one is _participation_, where you get the appropriate people involved. Step two is _accountability_, where you identify those who will make decisions. Step three is _discussion_, in which you make sure that people are able to speak up and are heard. Step four is _ownership_, where you promote the decision as your own. Step five involves communication, to ensure that your message is consistent. And finally, step 6 is follow-up, where you check in with your organization and test for appropriate alignment across all areas.

**Suggested further reading:** ** _How_** **_to_** **_Be_** **_a_** **_Positive_** **_Leader_** **by Jane E. Dutton and Gretchen M. Spreitzer**

_How_ _to_ _Be_ _a_ _Positive_ _Leader_ examines cutting-edge research from the field of positive organizational behavior, in which companies aim to foster both a positive attitude to work and high performance among employees. The research is complemented with vivid examples from real organizations.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Roger Connors and Tom Smith

Roger Connors and Tom Smith are the co-founders of leadership training and management consulting company, Partners In Leadership, Inc. Together they've written three _The_ _New York Times_ bestselling books.

