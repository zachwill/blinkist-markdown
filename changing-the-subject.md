---
id: 588276811543940004f0e2d7
slug: changing-the-subject-en
published_date: 2017-01-23T00:00:00.000+00:00
author: Sven Birkerts
title: Changing the Subject
subtitle: Art and Attention in the Internet Age
main_color: E2BE36
text_color: 7D691E
---

# Changing the Subject

_Art and Attention in the Internet Age_

**Sven Birkerts**

_Changing the Subject_ (2015) takes a critical and concerned look at how the internet is affecting our lives. In these blinks, you'll find out how our constant connection to the digital world is causing us to lose our individuality, our attention span and our intellect. Are we headed for one giant hive mind, or can we put down our phones for a moment and reconnect with our own emotions?

---
### 1. What’s in it for me? Reflect on how the new digital world influences your experiences and selfhood. 

Right now, at this moment, your smartphone surely isn't far away. You may be holding it in your hands; it may be in your pocket or purse; it may be on your desk — and wherever you go, you will doubtless take it with you.

The internet and social media are now so pervasive that many of us can't imagine a day without them. In fact, they shape our whole experience of life — our individuality, our perceptions, our habits.

Given this pervasiveness, it may be time to raise a few critical questions.

In these blinks, you'll reflect on how the digital world influences _you_. You'll learn how it influences your intellect, how it decouples you from reality and how it can even affect your neuronal functioning.

You'll also learn

  * why turning off the navigation system is the better way to go;

  * how Wikipedia, YouTube and Google hinder you from gaining knowledge; and

  * why you should make time to read a good novel.

### 2. Easily accessible information makes our experiences less gratifying and our lives less meaningful. 

It's pretty amazing to think about just how much information is available on the internet. Equally amazing is the fact that, nowadays, the answer to nearly any question is literally at our fingertips.

But having all this knowledge so readily available comes at a price.

First off, this convenience can make acquiring knowledge through first-hand experience seem less relevant and meaningful, which, in turn, can make life feel less gratifying.

Consider GPS. While this tool is very helpful when you find yourself trying to navigate a new city, it also keeps us from becoming familiar with our surroundings. We come to rely on it, instead of maps and our intuition.

Prior to the advent of GPS, people studied and interpreted maps and used their imaginations to picture the landscape around them.

This allowed you to see for yourself a variety of different routes and how far you needed to travel. You could then immerse yourself in the details so you'd know where you're going. And when you finally got there, you'd experience a substantial reward that following GPS just doesn't provide.

There's a sense of gratification that comes with acquiring your own knowledge and putting it to use.

The author used to experience this gratification by discovering new music. He would study the liner notes of albums he liked, reading about other artists who played on them, and then search for good radio stations and get recommendations from people he trusted.

All of this would take time. But when he finally did hit musical pay dirt, the discovery would feel truly rewarding. And it wasn't merely gratifying; it made music more personal. The extra work and effort made him feel connected to the musical scenes and artists that were around.

In order to feel this kind of gratification, you need to feel like you've earned it by working for the knowledge you've gained. But this doesn't happen in today's digital age, with music services like Spotify and Pandora that just expose you to everything, one click at a time.

> _"It is through the earning, the work of achieving, that we establish our psychological claim on our experience."_

### 3. The digital world makes us less self-sufficient and keeps us from confronting our feelings. 

Having gratifying experiences in life is important. But it's not all about worldly accomplishments and adventures. Being connected to your inner self is equally important. And with social media providing a constant connection to the outer world, it's becoming ever more difficult for us to foster _internality_, a space for self-reflection and self-realization.

This is because the need to be connected to the outside world at all times hinders our ability to be self-sufficient.

There are any number of instant-messaging apps and social-media platforms that collapse the distance between you and the rest of the world. Many regard this as a great thing, a wonderful gift. But by allowing yourself to be available 24/7, and expecting others to be equally available, you can end up with no time to focus on yourself and your own thoughts.

As a result, we're constantly waiting for responses and looking for approval from others rather than looking inward and finding out what we think. It also becomes harder for us to feel at peace spending time alone or to feel comfortable taking things slowly.

Instead, we feel restless, anxiously awaiting feedback from the outside world. This reliance on external approval separates us from our individuality and the real facts of our situation.

Many people have become so comfortable living in the digital world that they can't imagine what it would be like to live without immediate access to the internet.

But when you're constantly in this other world, it's hard to stay present in the moment, to live in the physical world occupied by your body.

These days, when people feel bored, lonely or helpless, they immediately turn to their smartphones. They try to escape these feeling instead of doing the healthy thing, confronting them and attempting to understand where they're coming from.

In other words, people look to their smartphone as an easy fix — an escape hatch from real feelings or personality issues that need to be worked out.

Of course, constantly escaping to the digital world creates other problems. After a while, people start to feel a sense of incompleteness and anxiety.

### 4. Our digital fixation is negatively impacting the way we process information. 

It's not uncommon to hear someone complain about not having time to sit down and read a book these days. And it's not at all improbable that the rise of the internet and the ubiquity of portable devices are what's eating up all our time.

It takes focused attention to read a book. The internet, in contrast, works by distracting us, by shifting our attention from link to link, giving us little time to think or reflect on a topic.

As we've gotten in the habit of shifting our attention from one page to the next, our behavior has also started to change. We no longer read with the intent to understand. We rush to get to the next topic by skimming and quickly scrolling through a page.

The digital world has kicked us into high gear, leaving us with neither time for ourselves nor time to concentrate. Even when someone is walking from one appointment to the next, he's likely to be answering an email or quickly scrolling through a news website in an effort to ingest as many stories as possible.

As a result, we've become satisfied with just reading headlines and highlights. We don't dive into the full articles and books that offer the information that allows us to think, reflect and form our own opinions.

All these digital interruptions and distractions may even be changing our neuronal functions.

In 2006, a team of scientists studied the brain behavior of cab drivers and how they were affected by technology. Before taxis were equipped with GPS, cab drivers had to memorize all the streets, routes and alternate routes a city had to offer. And since cabbies had to practice all this memorization, their brains developed a larger-than-average hippocampus, the part of the brain that handles how we remember our geographical environment.

So, clearly, our behavior can change our brains — for better or worse.

Just as our brains grow when we use them, they stagnate when we don't. When we constantly distract ourselves from having deep thoughts, we neglect an important part of our brains that can suffer as a result.

### 5. By using the internet as a source of knowledge, we lose the context and meaning of information. 

Another part of our lives that has transformed in the digital age is how we learn. Whether it's picking up a new skill or finding out about a new subject, there's a good chance you've gone to Wikipedia, YouTube or some other result from a Google search.

This isn't always the best way to learn and become well informed. To learn about a subject on Wikipedia or through a YouTube tutorial is often to strip it of its context. 

Let's say you've always wanted to learn more about the French Revolution.

The information presented on a Wikipedia page, or any other online-knowledge platform, shouldn't be taken at face value. Sites like these don't provide the kind of context that comes from a source that offers a critical and independent point of view. This objective viewpoint comes only from an author who has thoroughly researched the subject from all angles and prepared the information in a comprehensive, thus meaningful, manner.

So to really understand the French Revolution and get a full picture, you would need to read multiple books that offer different viewpoints.

Unfortunately, the internet simply doesn't present information this way.

Google has a universal library project that hopes to scan and compile every book in the world into one massive cross-referenced database.

Having all this data just one click away might seem exciting, but by chopping all these books up into chunks of data that link to one another, the very idea of a "book" is destroyed. A book is no longer a cohesive and independent statement from an author; it's just a collection of isolated info bits.

Further, by combining all this information, the very thing that gives it meaning is removed. There's no context.

We ought to remember that just because we _can_ do something doesn't mean we _should_ do it.

> _"Author and text, thinker and thought — these shape the foundation, the matrix, of who we are and what we know."_

### 6. Reading novels can help us reflect on our positions in the world and fuel our imagination. 

Paper seems to be on the verge of disappearing from our lives. Even textbooks are being digitized and students are using tablets in classes as early as elementary school.

But neglecting the printed word is knocking our inner lives off balance.

It's no secret that the publishing industry is struggling. Many publishers are downsizing and only concentrating on books with a built-in audience, and there are fewer book reviews being published than ever. But, most of all, starting at a young age, people are simply reading less and less.

However, reading books, especially novels, is one of the best ways to improve your thinking and your intellect. They not only invite us to reflect on and contemplate life; they also spark our imaginations.

When you read something that isn't on a digital screen, it engages your ability to focus and transport yourself into another world. And this allows you to see yourself and your own world with clarity so that you can better understand who you are as an individual and where your emotions come from.

There are few things as powerful as a good novel to engage the imagination. But reading a novel takes hard work. You need to give it your full attention. You need to take the author's words, process them and recognize the intent behind them. Otherwise, your imagination won't fully engage.

Such rigorous reading is challenging in and of itself. It becomes more difficult when you're reading from a device. The digital world distracts us. It creates a barrier that makes it hard for our imaginations to connect with our intellect, to completely immerse ourselves in the author's world.

So do yourself a favor — put down the device, pick up a book and nourish your mind.

### 7. Final summary 

The key message in this book:

**Our involvement with apps and the internet is stronger than ever. We check our email the moment we wake up and stay glued to our screens until we fall asleep. And our constant urge to be digitally connected is diminishing our experiences and deteriorating our ability to focus. If we want to retain our ability to make our own informed decisions and not shy away from dealing with our emotions, we need to put down the smartphone, pick up a book and spend more time reflecting on what it means to be alive.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Reclaiming Conversation_** **by Sherry Turkle**

_Reclaiming Conversation_ (2015) reflects on how we interact with one another in our increasingly digitized world. Constant interruptions, leaving messages unanswered and lack of interest have all become the norm in a world rife with mobile devices and screens. But is this what we want? And if not, what can we do about it?
---

### Sven Birkerts

Sven Birkets is an essayist, literary critic and champion of the importance of reading. He is the author of many books, including _The Gutenberg Elegies_, published in 1994, which issued an early warning about how technology could adversely affect our reading habits.

