---
id: 583d59f1e536360004b64ebd
slug: chaos-monkeys-en
published_date: 2016-12-01T00:00:00.000+00:00
author: Antonio Garcia Martinez
title: Chaos Monkeys
subtitle: Obscene Fortune and Random Failure in Silicon Valley
main_color: 76B5B1
text_color: 446966
---

# Chaos Monkeys

_Obscene Fortune and Random Failure in Silicon Valley_

**Antonio Garcia Martinez**

_Chaos Monkeys_ (2016) offers a revealing peek at the deceitful schemes and billion-dollar dreams that drive start-ups in Silicon Valley. Take a tour of companies like Google and Facebook, and find out what it takes to be successful as well as what goes on backstage at some of the world's most dominant start-ups.

---
### 1. What’s in it for me? Behold the new economy in Silicon Valley. 

The last few decades have witnessed the dizzying ascent of many companies in Silicon Valley, California. These companies are now among the most influential in the world; it would be hard to exaggerate the enormity of the role they play in the lives of most westerners.

But, like many success stories, it's not all rainbows and sunshine.

A closer look at how these companies actually work reveals a stormier picture.

In these blinks, you'll find out

  * why foreign workers for tech companies put up with really bad conditions;

  * how Google has left most of the work to non-humans; and

  * why being smart isn't necessarily the most important thing for Silicon Valley success.

### 2. Immigration to Silicon Valley is so tough that immigrants sometimes resort to fake marriages. 

When Europeans were trying to reach America in the eighteenth century, many paid for their passage by submitting to several years of indentured servitude. While tech geeks seeking work in twenty-first century America don't have it quite _that_ bad, they still face a pretty rough deal.

Being allowed to work in Silicon Valley isn't easy for an immigrant, and the process often leads to exploitation.

One solution is to obtain an _H-1B visa_ : a nonimmigrant visa for foreign workers. However, only a limited number of these are available. In 2013, 200,000 foreign workers hoping for employment in Silicon Valley applied for an H-1B visa, but only 16,000 were approved.

And the ones lucky enough to be approved are then totally at the mercy of their boss.

If they're fired or the company shuts down, their visa is no longer valid, a state of insecurity that often results in their feeling forced to accept a smaller salary than their American colleagues.

As a result, the corporations are the big winners. They get highly skilled employees who will work for less, and these employees have to endure five years of this exploitation before they can apply for a green card, which gives them better rights and permanent-residence status.

It's no surprise that immigrants want to avoid all this. And the easiest way to do that is with a fake marriage.

The US immigration system is friendlier with those who are trying to start a family. In fact, immediate-relative or family-preference visas account for two thirds of all successful immigration applicants.

Surprisingly, for this method to work, you don't even have to marry an American citizen.

Stanis Argyris is a Greek computer specialist who wanted to join the author's start-up team in 2008. So Argyris married a Turkish woman who, under a student visa, was studying at Stanford University. Now Argyris could obtain a student-spouse visa, stay in the country and finally work at the author's company.

### 3. Google earns amazing amounts of money through its automated advertising system. 

Sure, life would be a lot easier if you could print your own money. But since this is a highly punishable crime, the next best thing would be to create a company like Google.

One of Silicon Valley's greatest success stories, Google is a sensational money-making machine.

Every year, Google generates around $70 billion in revenue, a number so staggeringly large that you might be wondering, "How can a search engine generate that much cash?"

For starters, it offers answers to many of life's pressing questions, from "What's the best camera under $300?" to "Who's a good divorce lawyer in the area?" But Google also generates ads for companies related to your searches — and earns money every time someone clicks on one.

The amount of money Google earns from a given click depends on the keyword being used and how many companies are bidding on it. A popular one like "insurance" can be worth as much as $54 per click.

Perhaps even more remarkable is that Google hardly breaks a sweat dealing with all the bidding companies since they have an ingenious way of letting computers do the work for them.

This, of course, makes good sense. There are billions of different keywords people search for, and figuring out how to sell individual ad space for each one would be virtually impossible.

To solve this problem, Google holds an automated, instantaneous and lightning-quick auction.

It starts the moment a search is initiated: Google's computers quickly scan through all the pre-placed bids that advertising companies have made for this keyword.

When this happens, two things are being taken into consideration: the highest bids and who has the best chance of being clicked on. The ad that best combines these two criteria will be featured at the top of the search results.

This is why multiple searches for the same term might produce different results. If you search for "London Furniture" today, the winning bid might go to the handcrafted furniture company Swoon Editions; next week, however, it might go to the luxury furniture providers Unum Design.

### 4. Investing in a start-up is risky business, so the companies compensate with specials deals. 

If you're a thrill-seeker looking for a risky adventure, you might try bungee jumping or whitewater rafting. Or you could evade travel and the risk to your body by investing in a start-up.

Chances are, you won't have any trouble finding an opportunity, since start-ups are always looking for risk-friendly investors.

A start-up begins its search for funding with what's called a _seed round_. This is when the company's entrepreneurs collect money from friends and family, while hoping to land big-time investors.

These early investors are in a risky position for two reasons: The company could either fail and they'll never see their money again, or the company could become too successful and they'll end up with a microscopic share.

Let's say a friend offers $100,000 in seed money, and that this original loan is turned into an equity investment after the company, which has done well, decides to go public. And let's say the start-up is valued at $10 million: since 100,000 divided by 10 million equals 0.01, the friend who invested $100,000 would now be the proud owner of one percent of the company.

Obviously, one percent isn't a great outcome, so to help prevent the phantom of such scenarios from scaring off potential investors, start-ups offer special deals.

One attractive offer is to place a cap, or a maximum amount, on the company's valuation. This prevents an investor's share from getting too small.

Let's return to the friend who invested $100,000, and say the company agreed to place a $3-million cap on their initial valuation. This would give the friend 3.3-percent ownership when the company launches. Even if the company were to eventually be valued at $100 million, the friend's initial percentage would grow alongside the company.

While this offer can make one risk seem less troublesome, the fact that the company can still fail remains an unavoidable concern.

> _"Entrepreneurs bandy about their cap number as if it were a real company valuation, but it is purely hypothetical."_

### 5. Bill Gates and Steve Jobs got their start by engaging in some duplicitous dealings. 

Many parents dream of having children who'll grow up to be both successful and truly kind. These parents might _not_ want their children to enter the start-up world. There, success is most definitely not the result of niceness.

As we'll see, both Bill Gates and Steve Jobs used rather ruthless methods on the path to riches.

Coming from a wealthy Seattle family, Bill Gates dropped out of Harvard University before starting a small tech company called Microsoft in 1975.

Around this time, some trusty connections at IBM told Gates that the computer giant was looking for a new operating system.

And Gates knew a guy perfect for the job: the IT genius Gary Kildall. But to Gates's surprise, Kildall refused to agree to the terms of IBM's contract.

This is when Gates stopped playing nice. Knowing Kildall was sitting on a gold mine, he had a Microsoft programmer copy Kildall's operating system and rename it IBM's Disk Operating System (DOS).

After Gates registered the copyright under his own name, the innovative system, which made it possible for computer hardware to be interchangeable, became the property of Microsoft. Hence the well-known acronym: MS-DOS.

As a result, both Kildall and IBM saw not a cent of the billions that began pouring Gates's way.

Steve Jobs was also known for his unkindness, even when he first started out.

Jobs began as an unpopular employee at Atari, the company behind the table-tennis-like video game, Pong. In 1975, the company's CEO, Nolan Bushnell, offered a reward of several thousand dollars to anyone who could create a single-player version of Pong.

Eager to win, Steve Jobs enlisted the help of his friend Steve Wozniak, who could solve just about any technical problem. Jobs pushed hard until Wozniak finally cracked the problem. Jobs won the prize money.

Then Jobs told Wozniak that the prize was only $700 — not a few thousand — and handed him just $350 for his troubles.

### 6. Launching a start-up requires obsessiveness and the will to never give up. 

Okay, so billionaire tech giants like Gates and Jobs also _do_ have a reputation for being rather smart. But if you want to make it in the world of Silicon Valley, there are other qualities that trump intelligence.

One of these qualities — perhaps the most important when launching a start-up — is obsessiveness.

Before the author founded AdGrok, a start-up that optimizes GoogleAds, he was at the bottom of his PhD class, even having to retake a number of exams.

He envied many of the computer programmers around him, people who were far brighter and more talented than he was.

Though not as intelligent as some, Martinez _was_ obsessive. And this is the quality that brought him success in Silicon Valley.

While working on the AdGrok project, he was so focused that nothing else mattered. Life was simply one tech event or company retreat after another; he worked ridiculous hours each day; and he watched his daughters growing and changing on Skype rather than in person.

Work eclipsed his life. He gave up family life. He gave up his hobbies. He stopped reading books and watching movies, and removed all distractions until AdGrok launched.

It's not a pleasant way to live, but Martinez was rewarded for his obsessiveness. In 2011, AdGrok was purchased by Twitter for over $10 million.

Another extremely important quality that can help launch a start-up is a refusal to give up.

Some people, like the author, develop this quality early on. Martinez attributes his success to having made it through a tough childhood that included a sadistic older sister, a bullying father and frequent schoolyard fights.

By persevering through a harsh childhood, Martinez developed a never-give-up attitude that he later applied to the time he spent on the Wall Street trading floor and the endless challenges he faced during AdGrok's launch.

### 7. Facebook’s determined leadership and obsessively loyal employees met the challenge of Google Plus. 

The kind of determination and will-to-win shown by Martinez is contagious. And, in the history of start-ups, there is perhaps no better example of an obsessively dedicated team rising to a challenge than what happened at Facebook's headquarters in 2011.

That was the year Google challenged Facebook's status as the premier social platform with the launch of Google Plus.

It was a serious challenge. Google had lost some of its most talented employees to the younger start-up, and now it was time to finally go on the offensive. Google already had many popular products, like Gmail and YouTube, that promoted Google Plus. And when it launched, its design, photo- sharing features and ad policies were all superior to Facebook's.

But Facebook wasn't just going to roll over; their reaction was immediate and showed that the company had cultivated a loyal and dedicated team of employees.

All they needed was the right motivation, and that's exactly what their leader, Mark Zuckerberg, provided.

Once he heard about Google Plus, he immediately put the company on "Lockdown," ordering all employees to remain on-site and work 24/7 until the threat was neutralized.

Zuckerberg isn't a naturally gifted speaker, but to rally his employees, he rose to the occasion and delivered a rousing speech that demanded Facebook's quality be improved in record time. For the climax of the speech, Zuckerberg quoted the Roman senator Cato the Elder by declaring, "Carthage must be destroyed!" Obviously, Google Plus was Carthage to Zuckerberg's Rome.

His employees responded. They turned the challenge into an event by whipping-up Roman costumes and posters and quickly redesigning Facebook's functionality and photo-sharing options to be in line with Google Plus.

So, in the end, Facebook's customers saw little reason to leave. And Zuckerberg showed that a company with a loyal corporate culture eager for motivation can rise to the toughest challenges.

### 8. There are good reasons why Facebook’s security team is kept out of the spotlight. 

Some of Facebook's major battles have been widely reported on. But, behind the scenes, there is a constant series of skirmishes, and a team of employees who are always in the fray, protecting Facebook's customers.

If you have Facebook, you probably use it to keep in touch with friends and play games and stay abreast of current events. But you're probably unaware that, while you're checking out photos and chatting with pals, Facebook's security team is shielding you from disturbing content.

Online security is one of the biggest challenges for internet start-ups, and security teams must review all ads and be on the lookout for an array of online threats: scammers, sexual predators, pornographers and so on.

Since this work is done largely in the shadows, it is a thankless job. It tends to be recognized only when something goes wrong or slips through the cracks, in which case the security is thrown to the wolves.

In fact, most of the time people get angry about the work they do and call them out for censorship when they accidentally flag something like a breast-feeding photo.

Since users often forget to show gratitude for the accurate and beneficial flagging of harmful content, and perhaps also to make up for their usual anonymity, Facebook's security team created a Facebook group called _Scalps@Facebook_. Here, the photos and profiles of the criminals they have taken down are listed alongside a description of the crimes they've committed.

But aside from this group page, there are good reasons for Facebook to keep the team's work under wraps. The biggest reason is that if people knew the sordid details of all the many crimes and attempted crimes, it could very well deter people from using Facebook.

Many users are already concerned about their online privacy and safety, and if they were notified of every sex offender and scammer that was caught on Facebook, they might think twice about using it. While the security team would be honored, parents would likely make sure their children stayed away altogether.

In the end, it's another example of the many exciting — and unsettling — things that are going on behind the scenes at Silicon Valley.

### 9. Final summary 

The key message in this book:

**Silicon Valley can be a ruthless and competitive place. Yes, friends have been known to stab each other in the back to reach the next rung on the ladder of success, but Silicon Valley offers some fun and inspirational stories as well. For those willing to sacrifice everything for a shot at becoming the next start-up millionaire, Silicon Valley might be just the right place.**

Actionable advice

**Develop your grit.**

If you think you would like to launch a start-up but feel uncertain whether you have the necessary backbone, start by trekking across a continent, or sailing across an ocean. If you make it, you may well have developed the resilience to make it in the world of start-ups, too.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Disrupted_** **by Dan Lyons**

_Disrupted_ (2016) demystifies the culture and practices of tech start-ups by taking a revealing, behind-the-scenes look at Boston's HubSpot software company. After 25 years as a technology journalist, Dan Lyons was fired from _Newsweek_ and accepted a new job at a start-up. These blinks follow Lyons's bumpy and humorous journey as he tries to navigate a weird new world filled with candy walls and other bizarre instances of HubSpottiness.
---

### Antonio Garcia Martinez

Antonio Garcia Martinez worked on Wall Street as a trader for Goldman Sachs before starting his own company, AdGrok, and then becoming a manager on the Facebook Ads team. He lives on a sailboat near San Francisco.

