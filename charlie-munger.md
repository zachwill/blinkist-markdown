---
id: 577faf66c088090003fe9ed5
slug: charlie-munger-en
published_date: 2016-07-13T00:00:00.000+00:00
author: Tren Griffin
title: Charlie Munger
subtitle: The Complete Investor
main_color: C4482C
text_color: AB3F26
---

# Charlie Munger

_The Complete Investor_

**Tren Griffin**

_Charlie Munger_ (2015) is your guide to understanding the strategies and principles that for years have guided Warren Buffett's financial partner, Charlie Munger, considered one of the world's most successful investors. These blinks introduce you to the Graham value investing system that Munger uses, explain the importance of patience and courage, and show you how to make smart decisions using interdisciplinary knowledge.

---
### 1. What’s in it for me? Hone your investing skills by better understanding Charlie Munger’s savvy strategies. 

Here's your once-in-a-lifetime opportunity to sit down and pick the brain of one of today's most successful investors. These blinks offer a personal invitation to enter the talented mind of Charlie Munger, legendary investor and financial partner of Warren Buffett.

As the vice-president of Berkshire Hathaway, Munger has done quite well as an investor — to put it mildly. These blinks give you the secrets to his success, outlining in clear detail the steps you too can take to hone your knowledge and become a smart, worldly and successful investor just like Munger.

You'll also learn

  * why value investing is like waiting for the bus;

  * why you should get to know the whims and worries of Mr. Market; and

  * why "don't just sit there, do something!" is the wrong inspiration for an investor.

### 2. Great investors like Charlie Munger and Warren Buffett dedicate lots of time to learning more. 

Have you heard of Charlie Munger? He's the legendary investor and financial partner of Warren Buffett, a household name when it comes to smart, successful investing.

But that's not all. Munger is also vice-president of Berkshire Hathaway, where Buffett is CEO. It should come as no surprise that Munger and Buffett have guided Berkshire Hathaway more than successfully!

Berkshire Hathaway is the fifth-largest public company in the world. Its activities are broad, with interests in sectors such as insurance, finance, energy, utilities, rail freight transport, manufacturing and services. The company also is the sole owner of fast food restaurant chain Dairy Queen, clothing manufacturer Fruit of the Loom and private jet company NetJets, to name just a few.

Berkshire Hathaway also holds considerable stakes in financial services firm American Express, technology giant IBM and The Coca-Cola Company.

On the New York Stock Exchange (NYSE), Berkshire Hathaway common stock is listed with the trading symbols BRK.A and BRK.B.

In December 2014, a Berkshire Hathaway Class A share sold for over $220,000, the highest priced per-share trade on the NYSE. In 2015, the annual revenue of Berkshire Hathaway was $200 billion.

Which begs the question: how did Berkshire Hathaway become such a success?

The company's success was built on the brilliant investing strategies and sharp minds of Munger and Buffett. For Munger, his success as an investor stems from his unwavering desire to teach himself new things. Indeed, he is the perfect example of his belief that successful investors must be learning machines.

Munger and Buffett dedicate 80 percent of their working day to _reading_, making acquiring tons of knowledge a key part of their work. Munger says he wasn't born a business genius but honed his investment savvy through countless hours of reading and absorbing information daily for years.

> _"His children describe him as a 'book with a couple of legs sticking out.'"_

### 3. Munger follows an investing system that focuses on simplicity and builds on what you already know. 

Investors like Munger, Buffett, Irving Kahn and Seth Klarman all swear by one method: the _Graham value investing system_, invented by Benjamin Graham, an American economist and professional investor.

What makes this system such a popular choice among the world's best investors?

This system is based on _simplicity._ The ultimate goal of the Graham value investing system is only doing things within your competence. As Buffett likes to say, investing is simple, but not easy.

Rather than getting into complicated investments that you might not fully understand, accept the limits of your knowledge and focus on sure bets. Doing so will secure you far better results.

In following the Graham system, Berkshire Hathaway sorts its potential investment opportunities into three baskets: In, Out and Too Tough.

The _In_ basket holds worthwhile potential investments and is the smallest basket. The _Out_ basket contains uninteresting opportunities; the _Too Tough_ basket holds opportunities that look great but are currently outside Berkshire's competence.

If you follow the Graham value investing system, you also focus on buying shares for less than their future earning potential. But in doing so, you need a healthy dose of patience waiting for them to appreciate.

Seth Klarman likens investing to sitting at a bus stop and waiting for a bus even though you have no idea when it'll show up. It's your job as an investor to focus on long-term results to make the best of your investments. You have to be ready to wait until undervalued shares regain their value.

You've also got to wait for the right moment to make a new investment. Unlike baseball, there are no strikes in investing. There's no need to hit the ball with every pitch. Just make sure you're ready to swing when the right ball comes along!

In other words, great investing requires a realistic approach, not to mention patience and courage, too.

### 4. Four simple ideas help a savvy investor stay cool headed and keep an eye on long-term opportunities. 

Munger believes that just about anyone can follow Graham's value investing system, as its guidelines are so simple and easily applied.

Munger himself follows four important principles, easily adopted by any value investor.

First, treat owning a share as _ownership in a business_. You won't understand the value of your shares unless you truly know the company in which you are investing. Any valuation of a share must start with a valuation of the core business.

Second, buy at a discount to give yourself a _margin of safety_. A margin of safety is the difference between the share's current market price and its intrinsic value, that is, its future cash flow.

Think of this as following cars on a highway. By maintaining a safe distance between your car and the car in front of you, you can predict or react to sudden movements more easily. Drive too close, and you could misinterpret a move and crash. The same goes for investing — rather than trying to predict future price jumps, prioritize your financial safety and buy at a bargain price.

Third, always stay on the right side of the market. A Graham value investor knows how to spot mispriced assets and recognize the strange, even bipolar, behaviors of "Mr. Market," the personification of the investing herd. Sometimes he's depressed and sells assets at a bargain price, while other times he's ecstatic and pays way more than he should.

If you can spot them, investor mistakes such as these are golden opportunities for you.

And fourth, stay rational. This is harder than it sounds. It's your job to remain unemotional when selecting investment opportunities. Investing according to your mood is foolish and at worse, dangerous.

To ensure your rational side is always in control, create a checklist that helps you divide tasks into simple blocks. This allows you to check in at every step and ensure that emotional errors aren't lurking in your process.

As Munger says, rationality is something you've got to foster. Putting in the effort to stay cool is worth it as you'll avoid making senseless mistakes.

### 5. Above all, a value investor needs to cultivate patience and courage to go bravely against the pack. 

It's just not enough to have a great investment strategy; you also need to cultivate personality traits that will keep you on the path to success.

To review, Munger believes patience and courage are essential traits for any great investor.

The best investing opportunities come around when the market is fearful. This is important to know!

The thing is, however, that predicting when the market will get jitters is impossible. All you can do is wait for a bargain to show its face, and spend the time while you wait gaining useful information about other investment opportunities.

In other words, a "don't just sit there, do something!" approach clashes with the philosophy of Graham value investing. Yet waiting can be challenging if you associate waiting with being unproductive!

But moving around shares too frequently also means higher taxes, fees and expenses, so don't let your itchy trading finger get the best of you! Be disciplined and stick to the waiting game.

A Graham value investor must also be brave. Sometimes it's hard to make a bold move when everyone else is holding their cards close to their chest. But remember, it's market folly that creates the best investing opportunities for those who are courageous. Fight the tendency to follow the crowd and you may outperform the market.

Indeed, this strategy is like playing poker. Not everyone can win a game; it's just not possible. The same goes for investing — it's mathematically impossible for every investor to outperform the market.

You've got to keep an independent mind and know when to split from the pack. In this way, the worst times for other investors become the best times for Graham value investors.

And finally, remember that nobody is perfect — not even Charlie Munger or Warren Buffett! We all make mistakes. But by cultivating discipline and courage, and pushing yourself to learn new things every day, you'll always improve and your investments will thrive.

### 6. Apply wisdom from a range of different disciplines to help you make better investing decisions. 

The Graham value investing system isn't the only framework Munger follows. He also channels the principles of _worldly wisdom_ in every investing choice he makes.

Worldly wisdom is a unique framework that makes use of a set of _mental models_, or ways of thinking, across a range of disciplines.

Psychology, history, mathematics, physics, philosophy and biology — Munger believes that these all have wisdom that stems from each discipline's unique way of seeing the world.

By examining behavior through these different lenses, a savvy investor can spot similarities and patterns where other less-insightful investors won't.

So how do you cultivate interdisciplinary wisdom? Spend time learning about different fields, but not just by collecting loads of facts and information.

Instead, focus on the core ideas of each discipline — why do people in each field study what they study? How do they structure knowledge? And how do they use it? These questions will help you find the wisdom in just about any social or natural science.

Once you've done this, you're ready to start relating these nuggets of wisdom to each other. This is what will help you make decisions as an investor.

For instance, say the price of a product has recently increased, yet the company is still able to sell the product in increasing numbers.

Doesn't this event break the economic rule of supply and demand? Sure, but it also fits into a model one finds in psychology, that of a Giffen good — a product that a customer desires more when prices rise because it makes the good an exclusive or luxury item.

Having worldly wisdom can bring a competitive advantage. Had a narrow-minded investor observed this price increase, he might have decided to abandon the assets tied to the product. A worldly, wise investor would recognize the product as a Giffen good, go against the crowd and stick to his investment.

### 7. Final summary 

The key message in this book:

**Charlie Munger is a highly successful investor, but not because he's tapped into some mysterious type of investing magic. By following his guidelines of continuous learning, simplicity, patience, courage and worldly wisdom, you too can boost your success in investing.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Berkshire Beyond Buffett_** **by Lawrence A. Cunningham**

_Berkshire Beyond Buffett_ (2014) reveals the core values that define Berkshire Hathaway's corporate culture as established by its founder, Warren Buffett. The book goes on to prove that the investment company's unique view of investing and operating will ensure its success even after Buffett's passing.
---

### Tren Griffin

Tren Griffin is a senior director at Microsoft and the author of _The Global Negotiator_, an acclaimed guide to negotiating on an international level. Griffin is also a former partner at Eagle River, a private equity firm specializing in software, telecommunications and other tech investments.

