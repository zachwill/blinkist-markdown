---
id: 553506e86638630007090100
slug: chasing-the-scream-en
published_date: 2015-04-24T00:00:00.000+00:00
author: Johann Hari
title: Chasing the Scream
subtitle: The First and Last Days of the War on Drugs
main_color: 3686D1
text_color: 29669E
---

# Chasing the Scream

_The First and Last Days of the War on Drugs_

**Johann Hari**

_Chasing the Scream_ (2015) gives a riveting account of the first hundred years of the disastrously ineffective War on Drugs. Weaving together fascinating anecdotes, surprising statistics and passionate argumentation, Hari examines the history of the War on Drugs and explains why it's time to rethink addiction, rehabilitation and drug enforcement.

---
### 1. What’s in it for me? Find out how the War on Drugs started, and why there’s been no victory. 

Like the War on Poverty and the War on Terror, the War on Drugs originally sought to beat an "undeniably" evil foe — drugs. But like so many other costly wars, it turns out that the War on Drugs was waged against the wrong enemy.

Drugs have always been controversial, especially today because legalization and decriminalization are favored more and more across the globe.

These blinks explore the beginnings of the War on Drugs and its recent developments. They explain how we've reached a position where fighting against drugs is perhaps less advantageous than simply accepting them as part of life.

In these blinks, you'll find out

  * why heroin addicts Billie Holiday and Judy Garland were treated so differently;

  * how cracking down on dealers leads to more gang power; and

  * why addiction has less to do with drugs than you might think.

### 2. The War on Drugs was born in America and pushed onto other countries. 

The effort to curb illegal drug use and crack down on dealers, otherwise known as the War on Drugs, has come to seem so natural that we're unfazed by stories about major drug busts in the news. Stacks of confiscated money and drugs and weapons: we barely bat an eye.

But the beginnings of this "war" were surprisingly different from how it is carried on today.

As late as the early twentieth century, those drugs we've come to categorize as illicit were actually freely available in some form throughout the world.

For example, you could leave the pharmacy with a bulging bag of medicine that incorporated drugs like heroin and cocaine. A refreshing sip of Coca-Cola contained ingredients extracted from the coca plant, the source of cocaine, and fashionable department stores in Britain even sold tins of heroin for high-society women.

That all changed, however, in 1914, when the United States began prohibiting the sale and use of drugs. But why the sudden change?

The outbreak of WWI, combined with rapid industrialization, drove Americans to seek an outlet for the anxiety and aggression caused by their fast-changing world.

Drugs — tangible objects that could be destroyed — seemed the perfect scapegoat for the less tangible ills of modernity, such as class tension, displacement and changing customs.

But the eventual hard-line, worldwide prohibition of drugs was due to the concentrated effort of one man: Harry Anslinger, first chief of the US Federal Bureau of Narcotics, from 1930 to 1962, and primary proponent of the War on Drugs.

During his tenure as Bureau chief, Anslinger noticed that drugs kept flowing into the US even as he zealously cracked down on them. He suspected that communists were deliberately smuggling drugs into the country, hoping to use drug addiction as a means of undermining America's military and economic strength.

So, in the 1950s, he took his case to the United Nations, where, by leveraging American geopolitical dominance, he successfully convinced the other member nations to adopt prohibition policies.

> Fact: When Thailand refused to ban opium smoking because it was a longstanding tradition, Harry Anslinger's team threatened to cut off American foreign aid until the country complied.

### 3. The War on Drugs was not originally meant to prevent addiction, but rather to suppress racial minorities. 

Modern misconceptions about the War on Drugs cropped up from the very beginning.

Indeed, we're often told that the purpose of the War on Drugs was and is to protect addicts from further self-destruction and to prevent others from getting addicted. This belief is strengthened by campaigns such as "Just Say No," for example, which was prevalent during the 1980s and 1990s.

Such campaigns lead people to assume that the War on Drugs was waged for noble reasons.

But, in fact, when the War on Drugs was launched in the United States in 1914, its champions didn't use addiction and social harm as their justification. Rather, they saw it as a way to suppress racial minorities.

Harry Anslinger gave a number of public interviews in which he blamed the increase in drug use entirely on black people, once going so far as to argue that "the increase [in drug addiction] is practically 100 percent among Negro people."

This racist belief was reflected in the discriminatory policies of the police as they cracked down on drug use.

Consider, for example, that Billie Holiday and Judy Garland were both prominent heroin addicts. While Holiday, a black woman, was regularly harassed by Anslinger's agents, Garland, a white woman, received Anslinger's assistance. He kept law enforcement out of the picture as she recovered from her addiction.

Many whites in the US preferred not to acknowledge that racial tensions in the country were bound up with structural racism and poverty. It was easier — and less uncomfortable — for them to naively believe that anger among African-Americans was caused by "foreign" drugs, and that by quelling the import and use of these drugs, the country could once again bring its minorities back into docile acceptance of the status quo.

By tapping into the American public's fears and prejudices toward racial minorities, the War on Drugs was able to garner widespread support.

> _"You can be a great surfer, but you still need a great wave. Harry's wave came in the form of a race panic."_

### 4. Ironically, the War on Drugs effectively created the modern drug-related crime industry. 

When Harry Anslinger and his colleagues began their crusade against drug use, they expected that much of the drugs circulating on the streets would simply disappear.

As it turns out, however, when a popular product is criminalized, it doesn't simply vanish. Instead, people seek out other, illegitimate ways to obtain it. This is especially true of drugs, which can cause intense physical and psychological cravings, driving addicts to try to obtain them at any cost.

In fact, the most notable effect that criminalization has had is the creation of criminal networks that control the supply and distribution of illegal drugs.

The sale of illegal drugs, it turns out, is incredibly profitable. Morphine, for example, before being criminalized, cost two to three cents per grain; after it was criminalized, gangsters charged as much as a dollar. Addicts weren't in a position to barter, either, so they paid what they had to pay.

On a smaller scale, the extortionate prices of drugs meant that addicts had to start engaging in petty crime in order to get their next fix. Indeed, the modern perception of a junkie — a person who spends their days engaged in desperate theft, prostitution and other criminal activities — began with the War on Drugs.

As drugs became criminalized, serious drug addiction became a crippling condition. Whereas cheap, controlled drugs had allowed addicts to live relatively normal lives, the new, hard-line approach to drugs meant that addicts would often have to quit their jobs and abandon other obligations to support their habit.

By criminalizing drugs, the modern War on Drugs effectively created the very enemy it would spend much of the twentieth century fighting.

Now that you understand more about the War on Drugs, the following blinks will look at how it has unfolded over the years.

### 5. Cracking down on drug dealers actually increases violence, rather than reducing it. 

You would think that, the longer the War on Drugs raged on, the less drug-related crime there would be. But that's not the case, because drug-related crime doesn't work like other criminal activities.

If you arrest a large number of murderers, for example, you see an immediate correlation in homicide statistics. Similarly, if you arrest a large number of violent racists, you will see a drop in hate crimes. Drug dealing, in contrast, doesn't actually subside when drug dealers are arrested.

Take the case of the New York City cop Michael Levine. Through a long-term surveillance operation, Levine identified 100 drug dealers working a notorious block in Manhattan. Within two weeks, he had nabbed 80 percent of them.

And indeed, this diminished dealing on that block for a few days. But within mere weeks, as new dealers moved in to fill the void in the market, drug activity rose back up to normal levels.

In other cases, cracking down on dealing actually increases the rate of violent crime, including homicide. This is because when authorities arrest the higher-ups in criminal organizations, they leave behind a gap in the power structure that rival drug gangs fight to fill.

In fact, prohibition has created a paradigm in which increasingly sadistic violence is not only normalized, but rewarded.

Drug dealing is a risky business. At every stage, from cultivation to transport to sale, the product is vulnerable to seizure. If someone were to steal their product, gangs can't turn to the law to rectify the situation.

So, the gangs engage in their own preventative measures. They cultivate a reputation for being particularly brutal in order to inspire fear in rival gangs and interlopers.

To counteract this, rival gangs respond in kind, becoming more brutal still. This leads to an ever-increasing cycle of sadism.

> _"Prohibition...creates a system in which the most insane and sadistic violence has a sane and functional logic."_

### 6. Drugs can’t cause addiction without the help of individual susceptibility. 

The inability of those waging the War on Drugs to understand their role in the cycle of violence is a major reason for the war's failure, but it's not the only one. Another reason is our misconception about the nature of addiction.

People erroneously believe that usage leads to addiction — that if we consistently use a drug, we'll get addicted to it. But there's compelling evidence to the contrary.

For example: Do you know anyone who suffered a serious injury and was prescribed opiates to manage their pain? Depending on the severity of their injury, they may have relied on powerful drugs for quite a while. And yet, did they get addicted?

If consistent usage results in addiction, then hospitals ought to be disgorging droves of post-surgery opium addicts. But that's not what's happening.

In fact, a study conducted by the _Canadian Journal of Medicine_ found that patients with significant exposure to opiates weren't more likely to get addicted than anyone else.

What this suggests is that no drug is addictive in itself. Rather, addiction is the combination of potentially addictive substances and addiction-prone individuals.

In many cases, an individual's susceptibility stems from trauma during childhood. For example, two-thirds of injection drug users suffered childhood trauma, be it sexual or physical or verbal abuse, or the death or disappearance of a parent.

More generally, drug addiction is likely to afflict people who suffer from isolation, or have lost a sense of connection with others.

Historically, rates of addiction soar in societies undergoing processes of societal breakdown and displacement. The deindustrialization that hit American cities in the 1970s and 1980s is a classic case. As jobs dwindled, communities began to fall apart.

In such cases, addiction serves as a surrogate for missing human bonds. It doesn't matter whether it's drugs, alcohol or gambling — as long as it gives you relief or a sense of meaning, you'll return to it obsessively.

As we've seen, however, the War on Drugs is not working. Our final blinks will look at potential alternatives.

> _"...addiction isn't a disease. Addiction is an adaptation. It's not you — it's the cage you live in."_

### 7. By decriminalizing the possession of drugs, governments can help addicts get the support they need. 

Looking at the failures of the War on Drugs, we're left wondering how to best combat addiction.

One good way to start would be to decriminalize drug possession. This would both lessen the stigma of addiction and allow addicts to seek the help they need.

When they know that they won't be arrested for reporting drug use, people are more likely to be honest about their drug history. This, in turn, allows authorities to provide addicts with the resources they need.

Furthermore, decriminalization allows authorities to approach addicts as actual human beings in need of help, rather than as criminals needing discipline.

The Swiss government, for example, has set up injection centers where addicts can come to get their daily fix in a sanitary, supervised environment. As a result, addicts don't have to hustle every day to satisfy their addiction, allowing them to keep their jobs and support their families.

Similarly, in 2001, Portugal decriminalized possession of less than ten days' worth of any drug. Instead of harassing and arresting addicts, police officers and drug authorities now play the role of consultants, educating addicts on safe practices and directing them to resources when they want to quit.

Moreover, as a way to integrate recovering addicts back into society, the government offers a tax break to anyone who employs them.

But you're probably thinking: Without a disincentive to use drugs, wouldn't more people use them? And wouldn't that lead to more addiction? Not necessarily.

In Portugal's case, decriminalization actually led to a _decline_ in drug use. For example, the rate of drug injection, a severely harmful way to take drugs, dropped from 3.5 injections per thousand people to 2.

Compared to Spain and Italy, which are still entangled in the War on Drugs, Portugal alone reported a decline in drug use.

> Fact: If they receive enough support to survive their addiction, addicts will naturally stop using drugs after about ten years, regardless of whether or not they receive treatment.

### 8. Legalizing drugs increases tax revenue and weakens criminal organizations. 

****Over the past few years, the subject of decriminalization, whereby people can use (but not sell) drugs, has been a regular subject in global news. However, the most effective solution would be to legalize drugs outright.

By legalizing drugs, governments are better able to limit access to them than when they're criminalized.

Think about it: When was the last time you heard of dealers selling alcohol on street corners or at schools? Probably never.

But these places are prime locations for selling weed or pills.

This is because the sale of alcohol takes place in stores owned by people who have an incentive not to sell to anyone underage. Marijuana dealers, on the other hand, have no reason not to sell to teenagers, since their product is illegal either way.

Despite what we might intuitively believe, legalization would place another, stronger barrier between teenagers and drugs.

Additionally, legalizing drugs would give governments worthwhile economic rewards. In the United States, for example, legalizing drugs would save the government $41 billion annually — money that is usually squandered by arresting, prosecuting and jailing dealers and users.

And if the government were to tax other drugs as they tax alcohol and tobacco, they would earn an extra $46.7 billion per year in taxes.

Altogether, that's $87.7 billion more every year that could be put toward support services for recovering addicts, or toward other causes.

Finally, legalization will seriously weaken drug-related criminal groups worldwide. Legalization would put gangsters out of a job and give pharmacies and stores another product to vend.

Gangs would have no choice but to shift their business to less profitable markets, making them less able to terrorize communities.

Of course, addicts don't want to live debased lives. If given the choice, they would gladly buy their drugs from a clean, trustworthy store rather than from a shifty dealer in a dark alleyway. It's important to remember that drug use doesn't constitute a moral failure — and that those struggling with addiction are human beings, just like the rest of us.

### 9. Final summary 

The key message in this book:

**A century after it began, it's clear that the War on Drugs has done nothing to stanch drug addiction. In fact, a closer look at the War on Drugs reveals that it has exacerbated, not ameliorated, the violence and turmoil associated with drug addiction. It's time to take a new direction.**

**Suggested** **further** **reading:** ** _The New Jim Crow_** **by Michelle Alexander**

_The New Jim Crow_ (2010) unveils an appalling system of discrimination in the United States that has led to the unprecedented mass incarceration of African-Americans. The so-called War on Drugs, under the jurisdiction of an ostensibly colorblind justice system, has only perpetuated the problem through unconscious racial bias in judgments and sentencing.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Johann Hari

Johann Hari is an author and journalist who has contributed to publications such as _The New York Times,_ the _Los Angeles Times_, _The Guardian_ and _The New Republic_, among many others. He is also the author of _God Save the Queen?_, a humorous critique of the British monarchy.

