---
id: 548f03fb6532350009f90000
slug: cheap-en
published_date: 2014-12-31T00:00:00.000+00:00
author: Ellen Ruppel Shell
title: Cheap
subtitle: The High Cost of Discount Culture
main_color: F2DE30
text_color: 736917
---

# Cheap

_The High Cost of Discount Culture_

**Ellen Ruppel Shell**

_Cheap_ is an investigation of the prices we _truly_ pay for low cost items. It outlines the history of America's obsession with finding bargains and cheap goods. _Cheap_ also explains how humans are irrational, and how companies manipulate us into buying things we don't need or even want. Our thirst for cheap products is hurting the entire globe; we need to educate ourselves, stand up and do something about it.

---
### 1. What’s in it for me? Learn just how expensive “cheap” can be. 

This book in blinks represents a milestone in our understanding of how today's global marketplace works. It is a detailed analysis of what is going wrong in the world, and what we have to do to fix it. _Cheap_ takes you on a journey from America's giant shopping malls and outlets to low-wage factories in East Asia. The author explains how we have destroyed quality, durability and craftsmanship, and exchanged them for an easy and cheap hit.

In these blinks you'll learn

  * why low prices are harming our economy and culture;

  * why people take it upon themselves to drive ridiculously long distances to buy cheap, poor quality products;

  * why we feel compelled to buy bargain-basement items even though we know they are not worth our money; and

  * how pricing experts manipulate us to make us buy more than we want.

### 2. The economy has completely changed over the last two centuries. 

Two centuries ago, if you wanted to buy a dress you wouldn't go to an urban superstore, but instead head to the nearest seamstress and have it made. If you wanted some milk, you'd go to the local farm, not the shop on the corner.

Needless to say, everything about the way we shop has changed since then. Why?

Well, it began with the invention of _mass-production_.

Mass-production was first developed to manufacture weapons. Before the Industrial Revolution, guns were time-consuming to produce: A gunsmith had to manufacture each individual piece, and the finished product was always prone to misfiring.

Simeon North, an American entrepreneur, was the first to come up with a way to mass-produce high-quality guns. He split up the production process by having several semi-skilled workers make the different interchangeable parts, rather than having one man make the entire gun. This increased the quality of the guns, and made them much faster to produce.

This new style of manufacturing and the development of factories led to many other changes in society. More and more people left their villages and migrated to cities, where there were more work opportunities in mass-production. This urbanization, in turn, further accelerated the development of that mass-production.

In traditional villages people knew the members of their communities, so they knew who made or produced what they were buying. In cities, however, residents needed a constant supply of cheap goods, like food or clothes — and there wasn't a local farm where they could buy their milk.

The economy had to change as drastically as production and society itself to accommodate these swift developments.

### 3. A new style of shopping developed after the Industrial Revolution. 

Before the Industrial Revolution, each product was the result of someone's backbreaking labor. Things we take for granted today — such as stoves or wheels — were very expensive and difficult to produce.

Thanks to a few entrepreneurial pioneers, we eventually adopted shopping in convenient and cheap stores.

There was an increased demand for a new kind of store as cities kept growing. People needed easier access to their products, and an entrepreneur named John Wanamaker created one of the earliest department stores, Oak Hall, in Philadelphia.

Oak Hall was based in a large space, and sold mass-produced clothing at a range of prices that appealed to people from various economic classes. It was highly successful. Wanamaker even used Oak Hall to invent some new shopping traditions, like the first January White Sale, where products were sold at cheaper prices to prevent the inevitable post-Christmas drop off in sales.

Other entrepreneurs, such as Frank W. Woolworth, soon followed suit. Woolworth opened stores that offered inexpensive goods, but economised by providing less attention to customers than smaller shops had traditionally offered.

Pioneers like Wanamaker and Woolworth ended up changing the way prices and customer service worked. Workers interacted very differently with shoppers in superstores, malls and discount shops.

Previously, shop clerks had been highly educated, and they often knew their customers quite intimately. By contrast, sales clerks in the new stores received only brief training, and any interaction with customers was supposed to be done as quickly as possible.

Stores also introduced price tags, shopping cards and cashiers to speed up the shopping process.

### 4. Humans are very irrational, especially when it comes to shopping. 

A few decades ago, economists developed a model of the perfect shopper: _Homo economicus._ _Homo economicus_ was an utterly rational man who made logical choices. But he doesn't exist.

Our brains are simply not wired to always act rationally. We often make illogical decisions when looking for a bargain, for instance. Have you ever done something like driving five miles to a convenience store because its cheese is nine cents cheaper than the cheese at the store by your house?

Rationally, you spend more money traveling than you save on the cheese. That still might not stop you, however. Why?

Well, we make irrational decisions like this because our brains seek to avoid loss — whether that means clinging to a bad relationship or wasting gas to save money on cheese.

Humans actually rely far more on morality than rationality when making decisions. This is a legacy of our evolution.

Experiments have shown that monkeys have the same tendencies as we do. Capuchin monkeys, for instance, have a strong understanding of fairness. If a capuchin monkey is given a cucumber, it'll happily eat it. However, if it sees that another monkey is given a grape, it'll start screaming and throw away the cucumber because it knows a grape is a better treat. It feels it's been cheated.

Humans behave in the same way. If we think someone is treating us unfairly, we get upset. We won't buy a product if we think the price is unfair, even if it's actually reasonable.

Behavioral studies have shown that when we make decisions about shopping, we're much more guided by morality and experience than logic or economics. Like the monkeys who throw away their cucumbers, we don't want to feel that someone is ripping us off.

### 5. Companies fool us into making rash purchases by setting prices in a strategic way. 

When you go to the supermarket or mall, you'll see the same price levels over and over again. We're so used to prices like $9.99 or $109.99 that we don't stop to wonder how the sellers choose those exact amounts.

Pricing is very complex. In fact, it has it's own field of study within economics, and human behavior plays a major role.

Our brains are much more comfortable with numbers like one, two, five and ten, because we have ten fingers and five on each hand. Young children, for instance, can multiply one, two, five and ten much more easily than any other numbers.

Pricing experts know how to exploit this. We wouldn't question why a new computer is $999.99 instead of $1,000. We just accept that $1,000 is an understandable price, and $999.99 is only a cent below it. However, we _would_ start questioning the price if it was $968.54. We'd wonder how the shop calculated such a strange amount.

Prices can influence us into making completely irrational decisions. Have you ever gone into a shop just to pick up some milk, but ended up buying some hairpins because they were only 99 cents? Or maybe it was a new sponge for 59 cents? You find yourself thinking you might as well buy these small things, because you might need them later, or you might not find a deal like that again.

We've all experienced this — it's exactly what retailers want. There's a reason those hairpins are 99 cents rather than a dollar. It makes us feel like we're getting a good bargain, even if we aren't.

We get fooled easily in the moment when we see these sorts of prices. Even if we don't really need the hairpins or the sponge, we'll buy them anyway without much thought.

> _"Price in and of itself has no meaning at all."_

### 6. We love and seek out bargains – even for things we don’t need. 

Have you ever bought something you knew you didn't need just because it was cheap? Did you think something like, "Well, I wouldn't normally get this, but now that it's on sale . . . "

You certainly aren't alone. In the last few decades, we've developed a very striking desire to find good deals. When we see a bargain, we'll go for it, even if we know we don't need the product.

Imagine you buy two cheap plastic bracelets that were on display by the cash register, just because they were a two-for-one special. You know they can't be high quality, and might break within a few days. But even if someone told you that, or pointed out that the trinkets are worth less than the plastic bag you're carrying them in, you'll probably still feel good about them. Your brain will tell you you did well, because you avoided the worst thing — paying the full price.

Even though we keep getting more access to cheaper products, we still think we're paying too much. Most of us buy simple things like laundry detergent or toothbrushes at discount stores every week. Even though we feel good when we find bargains, we still consider the prices to be too high. We're always demanding that stores lower their prices, and we get upset if they raise them even a tiny amount.

This feeling that we're being cheated by overpriced items is completely wrong. In fact, prices have only declined over time, and we're paying much less than we did a generation ago, even after the financial crisis. Adjusting for inflation, we're actually paying about 52 percent less for appliances, 18 percent less for food and 32 percent less for clothes compared to the 1970s.

So instead of buying random goods because we think we're getting a good deal, we should try to understand how pricing and products are changing over time.

> _"The total distance that Americans travel to outlet malls each year equals about 440,000 circumnavigations of the globe."_

### 7. Low prices and retail culture impede innovation and hurt workers’ rights. 

So now that we've seen why we lust after inexpensive goods, let's see how cheap prices, bad quality and retail culture are hurting us, our economy and our education.

Our constant hunt for profits saps creative innovation in the United States. When society focuses on making quick money, we see a decline in creativity. Why?

Well, many companies don't want to spend time and money creating new things, because they aren't guaranteed to bring short-term profits, and might even bring deficits instead. This is reflected in our education system. Businesses aren't looking for intelligent workers who value innovation — they just want cheap and disposible employees.

This does immense harm to the country: In 1950, 50 percent of all PhDs were conferred in the United States. In 2010, it was only about 15 percent.

The quest for cheap goods and high profits is also hurting workers, and taking away their hard-earned rights.

The average wages for workers today are so low that their fathers would've laughed at them. Americans have been forced to accept much lower salaries because of technological advances and globalization.

Firms can now simply transfer their offices and factories to lower-wage countries, so they don't have to spend as much as they would in the United States. Caterpiller, who make tractors and other heavy equipment, is just one example.

In the 1970s and 1980s, Caterpiller set a gold standard for salaries and benefits, but they slashed their wages after being hit by overseas competition. They reduced their hourly pay from about $40 per hour including benefits, to $13–$18, plus only $9 in benefits. The workers were forced to accept this, or the firm would've moved abroad.

Our culture of cheapness hurts everyone. We all need to work together against it.

### 8. Someone always has to pay for low prices. 

China wasn't considered the factory of the world until about 50 years ago. Before then, American goods weren't produced in countries like India or Bangladesh, but at home. Buying local wasn't a virtue — it was a necessity. Why?

It's all about shipping prices. If American companies had paid to ship goods from Asia 50 years ago, 20 percent of the total cost of production would have been sunk in transportation. It wasn't worth it, even though wages were much lower in China.

This changed with the introduction of shipping containers, which caused global shipping costs to plummet. Shipping containers reduced delays, theft and spoilage of goods. They also reduced shipping costs by over _90 percent_.

Moreover, container ships could be managed by a very small crew. Today, you only need about a dozen people for a ship of 6,000 containers, slashing costs. Most dollar stores and other bargain shops owe their very existence to this cut-price shipping.

As you might imagine, this has had drastic consequences for people living in countries that produce cheap goods.

When companies first began moving their production to low-wage countries, many thought it would bring prosperity to workers there. The opposite has proved true.

Many countries, such as China, have deliberately suppressed wages so that overseas capital won't be deterred from investing still more money. Instead of trying to improve their social systems, or the living and working conditions of people in factories, governments have just exploited the workers even further to improve the market. China's current society is a combination of capitalism, socialism, feudalism and slavery.

> _"Containers made the transport of goods so cheap as to be essentially free."_

### 9. We need to start shopping more responsibly, and think of the social problems caused by consumption. 

So what can we do to fix this mess? It can certainly seem overwhelming, so how can an individual do anything to help?

Well, the good news is that everyone can work to make the world a better place. Consumers need to shop more responsibly, and try to stay aware of what they're supporting with their money, but stores and chains can also contribute.

Chains like Walmart are well-known to be horrible places for employees. They have low wages, high turnover rates and long working hours. A few supermarket chains, however, are trying to be more responsible with their products, employees and customers.

The chain store Wegmans is one example. They try to keep their prices affordable, but they also spent twice as much on training for their employees as other stores. They offer a wide range of benefits for both their customers and employees. They sell local food, and provide health insurance and a generous retirement plan for their workers.

Positive changes can help dismantle harmful ideas and power structures. Instead of going for every bargain you see, look for better quality products. Avoid traveling unnecessary distances for cheaper goods, and be more aware of the consequences other people have to face to bring us our low prices.

This might mean that some cheap stores will make less money, or that companies will have to start selling higher quality products. But this is exactly what we need if we're going to forge a healthier relationship with the things we consume, and better working conditions around the planet.

Consider this your wake-up call. Everyone can help by changing the way they shop!

> _**"** What matters has never been and never will be cheap."_

### 10. Final summary 

The key message in this book:

**Since the Industrial Revolution, society has become obsessed with producing and consuming cheaper and cheaper products. Big companies trick us into buying things even when we know we don't need them, and we readily go along with it. Our overconsumption is hurting the planet and our global community. Individuals and companies alike need to start being more mindful with their consumption choices if we want to reverse this change.**

Actionable advice:

**Don't buy those hairpins just because they're on sale.**

When you catch yourself absent-mindedly throwing extra things into your shopping cart because they're cheap and you might need them later — stop. Think about what you _really_ need. Quit buying small things out of habit — that's exactly what those companies want, and you're only hurting yourself and others. Be more mindful with your shopping decision, and try to consume only what you truly need.

**Suggested further reading:** ** _Bargain Fever_** **by Mark Ellwood**

_Bargain Fever_ explores the world of bargains, discounts and coupons, and explains why we'll sometimes go to extreme lengths to find a good deal. Using many illustrative examples, the author presents an account of the history of bargains, explains how they influence our shopping behavior and speculates on what discounts will look like in the future.
---

### Ellen Ruppel Shell

Ellen Ruppel Shell is a journalist and a professor of science journalism at Boston University. She's a correspondent for the _Atlantic Monthly,_ and has written two other successful books: _The Hungry Gene: the Science of Fat and the Future of Thin_ and _A Child's Place: A Year in the Life of a Day Care Center._

