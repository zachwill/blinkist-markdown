---
id: 5637687762346500070f0000
slug: childhood-disrupted-en
published_date: 2015-11-02T00:00:00.000+00:00
author: Donna Jackson Nakazawa
title: Childhood Disrupted
subtitle: How Your Biography Becomes Your Biology and How You Can Heal
main_color: 29A3CC
text_color: 196680
---

# Childhood Disrupted

_How Your Biography Becomes Your Biology and How You Can Heal_

**Donna Jackson Nakazawa**

What happens when a child who's experienced trauma grows up, and yet never outgrows that trauma? _Childhood Disrupted_ (2015) reveals the deep physiological and emotional consequences of the stress that shapes us both as children and as adults, and explains how we can recover from our childhood experiences and help our own children.

---
### 1. What’s in it for me? Discover how stressful experiences in childhood can have a huge impact on our health as adults – and what we can do about it. 

We all have painful or traumatic memories from our childhood. Whether it was being in the middle of a hateful divorce, the death of a favorite pet or even something as terrible as losing your parents, the pain of these experiences can stay with you for the rest of your life.

To a certain extent this is a normal part of growing up. A little adversity helps us grow into healthy, resilient grown-ups. But there are limits. As these blinks will show, children who have a history of what psychologists call ACEs or _Adverse Childhood Experiences_, are more likely to suffer from physical as well as psychological problems later in life.

Luckily, there are ways to deal with this. By understanding the underlying mechanisms of stress and how it affects us we can learn to be better parents and help children get the childhood they need to become happy adults. 

In these blinks you'll learn

  * how stress is handled by the body;

  * why forgiveness is so important; and

  * how you can make your brain better at dealing with stress.

### 2. Our physical experience of stress has two stages. 

Some of us experience stress once a week, while others face it almost everyday. Though we experience stress in various forms and at varying levels, it's always a physical experience. In fact, we have mechanisms to deal with stress built right into our bodies. 

The human body responds to stress in two ways: getting _ready_ for it, and _relaxing_ after it. Whenever you experience a stressful event, your body prepares you for it by increasing your alertness. This is achieved when the hypothalamus in your brain triggers the pituitary and adrenal glands in your body. Your immune system is fired up, your pulse quickens and your muscles tense. Yes, this is your _fight-or-flight response_.   

  

Just imagine you're lying in bed in the quiet of the evening, when, suddenly, you hear a clatter on the stairs. Eyes wide, heart pounding, you listen carefully as your body stiffens. Within seconds, your body has prepared you for possible danger.

And another few seconds later, you realize that the noise was just your son heading back up to his room after grabbing a midnight snack. Your muscles and your body relax, the adrenal and pituitary glands as well as the hypothalamus decrease their activity, and the production of stress hormones ceases. 

These lightning-fast responses reveal just how capable our bodies are of dealing with stress. And yet, it's widely believed that any form of stress is bad. In fact, most of us will go to great lengths to avoid stress. The truth is that moderate forms of stress can actually increase your ability to handle stressful experiences in the future.

### 3. While chronic stress is definitely debilitating, moderate stress can actually make you stronger. 

Did you have an overprotective parent who tried to shelter you as much as possible during childhood? Today, overprotective parenting is advised against, and quite rightly so. Still, the instinct to protect your children from stressful situations makes a lot of sense. 

Children are highly sensitive to stress. So much so that stressful childhood experiences can haunt them in the form of chronic stress during their adult lives. Remember the two-step stress system we learned about in the previous blink? Well, it's this very system that's disrupted when children experience more stress than they can handle. 

For instance, a child who sees her parents fight with each other, or who is subject to verbal or physical abuse from her carers, is highly likely to experience difficulty when faced with stress as an adult. However, not all stressful experiences are damaging in the long run. 

Researchers at the University of Buffalo performed a study on sufferers of back pain. They found that individuals that experienced normal, healthy childhoods were just as likely to be disabled by their back pain as participants that had extremely traumatic childhoods. 

But those who had only experienced moderate stress during childhood had the least problems with back pain out of all three groups. Why? Well, it appeared that people in this category had become resilient as a result of their experiences. 

Of course, there's a very fine line between stressful experiences that are character building and those that are traumatic. Unfortunately, it's often the case that negative experiences during childhood can have a surprisingly significant impact on your adult health.

### 4. Negative childhood experiences can have a big impact on your adult health. 

The experiences you live through as a child have a direct influence on the way you develop as a person. Situations with toxic levels of stress can subsequently do considerable damage to children in their most vulnerable developmental stage. Psychologists refer to such situations as _Adverse Childhood Experiences (ACEs)._

ACEs are situations in which children are faced with terrifying, unpredictable or constant stressors. During these situations, children lack the adult support they require in order to deal with them safely. Kat, for example, was just five years old when she found her mother's body after she was killed by Kat's father.

Stephen, on the other hand, had parents who were investment bankers. They expected him to show incredible genius from a young age, just as they did. When he couldn't live up to their expectations they started putting him down over anything, even losing a flip flop or forgetting his Spanish book.

While both situations are clearly very different, both are ACEs that led to health problems during adult life. Kat suffered from severe rashes all over her body, as well as chronic physical pain. Her joints became swollen and inflamed, and her blood test revealed an astonishingly low white blood cell count.

At first it seemed that Kat suffered from a bone marrow deficiency. But a few weeks before her 35th birthday, her doctor uncovered the relationship between the emotional stress in her childhood and her debilitating physical pain. 

When Stephen was an adult, he suffered from attention deficit disorder and was diagnosed with depression. On top of this, he experienced crippling anxiety in situations where he was expected to perform his best. He isolated himself, refusing to go out with friends, and developed an immune disease that caused premature baldness. 

Stephen's childhood experiences continued to plague him as an adult not only in the form of health problems, but as mental health issues. It is now known that experiencing ACEs can physically decrease the size of an individual's brain. Find out more in the next blink.

### 5. ACEs can diminish the brain and its ability to cope with stress. 

ACEs don't just lead to health problems — they also have a major impact on the structure and function of the brain. Studies using magnetic resonance imaging have revealed that greater experience of ACEs correlates with noticeable decreases in brain volume. 

More specifically, the prefrontal cortex and the amygdala are significantly smaller in sufferers of ACEs. These two areas are vital in decision making and fear processing. These regions are effectively responsible for how we deal with emotions. The smaller they are, the less capable we are of handling stressful situations. 

It's also known that ACEs can even cause neurons to die more rapidly than normal. Unsurprisingly, this leads to significant disruptions in brain development. Consider two boys who both start with 4,000 neurons in their brains (of course, there are billions of neurons in the human brain, but let's say 4,000 for illustrative purposes). Let's say one child doesn't experience any significant ACEs, while the other does. The latter loses more than half his brain cells as a result, and is left with 1,800 neurons at 12 years old.

This is sufficient for normal functioning at this age. But when the two boys go into adolescence, both will lose approximately 1,000 neurons as part of the developmental process. The luckier boy is left with 3,000, while the other only possesses 800. 

At this point, the differences in neuron count will drastically shape the lives that each boy lives. The former can look forward to a happy and healthy life, while the other is immediately at greater risk of developing depression, anxiety or eating disorders. 

The impact of ACEs and childhood stress on the brain is immense, but research shows that women are even more likely to be affected by this than men. Read on to find out why.

### 6. Women are more likely to be affected by ACEs than men. 

Research suggests that women are much more likely to be affected by ACEs than men. Why? Well, there are several plausible explanations. One widely accepted theory points to the differences in the hormonal makeup of female bodies. 

Women have far more _estrogen_ and _glucocorticoids_ than men. Estrogen helps the immune system produce antibodies, but is also able to increase the presence of autoantibodies, which attack your own physical systems. Glucocorticoids (GCs) such as cortisol regulate inflammation that is caused by these autoantibodies. 

When girls experience ACEs, their GC count drops, leaving their system unprotected against inflammation. Estrogen is still high, especially in puberty. Subsequently, autoantibodies can begin to attack the body without any control. This unfortunately leads to debilitating autoimmune diseases such as rheumatoid arthritis, lupus or thyroiditis. 

Moreover, females experience more stress than their male counterparts as they grow into adults. Not only do women and girls constantly have to be on the watch for not being too fat or too flat, or not being sexy or attractive enough, but they are also paid less for the same work and have more responsibility and less security while having children. These societal stressors place an incredible burden on the female system, which just isn't built to cope with such huge amounts of stress.

So, whether you have a daughter or a son, it's clear that as a parent your role in minimizing the amount of ACEs that your child experiences is incredibly important. At the same time, it's a daunting task. So how should you approach parenting? Find out next.

### 7. Children are highly sensitive to the stress of their parents. 

Stress, as most of us know, is highly contagious. Think of how agitating it can be to sit near a nervous colleague! This phenomenon is experienced even more powerfully by children, as a study at the University of California confirmed. 

In the study, mothers and babies were first checked together to get a baseline state and then separated. Then the mothers had to give a prepared five minute talk to an audience and afterward answer their questions for five minutes. 

There were three groups. The first group received positive feedback from the audience, the second received negative feedback and the third had no interactions with the audience at all. After the speech, mothers from the negative feedback group showed more cardiac stress and reported feeling more negative.

When the mothers from the negative feedback group returned to their babies, the babies' heartrates rose, showing that they immediately picked up their mother stress. The emotional state of the parents can also impact the development of the child.

If a parent is depressed or anxious, their children are likely to experience physical pain like headaches or stomachaches by the time they are aged five to seven. Why does this happen?

Often it's because depressed parents are too overwhelmed with their own struggles to support their children through their emotional problems and anxieties. In a child, unresolved worries can turn into chronic stress, leaving the body in fight-or-flight mode long after a stressful situation has passed. 

Though many adults suffer the consequences of chronic stress, there is hope. Find out in the next blink how a recovery from ACEs is possible.

### 8. Recovery from ACEs is possible with meditation and forgiveness. 

The impact that ACEs have on our health may be dramatic, but it's not irreversible. Fortunately, there are ways to repair the damage that stressful childhood experiences have caused our minds and bodies. The first of these is meditation. 

We often hear about the benefits of meditation and mindfulness, but does it really work? In the case of ACEs, studies have shown that meditation can be incredibly effective. 

One study showed that participants in an eight-week mindfulness program suffered less anxiety and depression. What's more, they showed an increased density of brain tissue in the hippocampus at the program's conclusion. From an emotional and physiological standpoint, eight weeks of meditation training was enough to create significant progress. So what makes meditation so powerful?

By focusing on your breath, letting go of your thoughts and relaxing your body, meditation allows you to stay in the present moment, rather than worrying about the past or the future. But for beginners, this is no easy feat. You can make meditation easier by performing it in the same space, at the same time of day. Choose somewhere where you can get comfortable, but also remain awake and alert. 

Another tool that's equally helpful in recovering from ACEs is forgiveness. Despite what you might think, forgiveness isn't something that benefits the person forgiven — it's something you do for yourself. In forgiving, we let go of things in the past that cause us to suffer in the present. 

Forgiveness is also something we can train ourselves to do alongside meditation. There are many forgiveness systems and practices available today online. The author recommends the four-step forgiveness meditation by James Gordon. 

As adults, we have the opportunity to heal ourselves from the damage of ACEs. But as parents, we also have the chance to prevent our children from experiencing such damage in the first place. Or do we? Find out next.

### 9. Positive associations and personal growth are central to safe and healthy parenting. 

Parenting is one of the hardest jobs on the planet. There's no such thing as a perfect parent, so why strive to be one? Instead, focus on recovering and growing from mistakes or difficulties that you might have experienced yourself. This way, you can make room for new positive memories that will stay with your children for the rest of their lives.

The author herself suffered from several autoimmune diseases, which led to a difficult period for the whole family. Despite this, her children are now able to replace the negative associations they had with their mother during this time with positive ones. 

Instead of associating their mother with gloomy trips to the hospital, the children have new, positive associations with their mother from family hikes, beach holidays and baking together in the kitchen. When parents confront their own issues and begin to make a positive recovery, children will begin to see them more positively too. 

As a parent, it's also important to remember that healthy challenges help children to develop new strengths. When your child faces a minor challenge, such as forgetting to study for a test, don't rush to solve the problem for them. Certainly don't make pessimistic remarks about their mistakes either! Instead, a response along the following lines works best: "You do have a problem, but I'm sure you can handle it! How could you solve this?" 

In this way, you'll show your child that you have faith in them to solve problems, boosting their confidence and self-esteem. Furthermore, you'll allow them to experience tricky situations themselves, so they'll learn and grow. 

Just as children benefit from learning to solve challenges in the present, parents benefit from learning to solve the problems of their past. By understanding your ACEs and taking steps to recover from them, you can avoid passing them on to your children, ensuring you and your family are happy and healthy in the years to come.

> _"Better parents make better kids."_

### 10. Final summary 

The key message in this book:

**Stressful childhood experiences have a powerful impact on an adult's health, both physically and mentally. But recovery from ACEs is possible, and a healthy approach to parenting ensures children experience challenges that help them grow.**

Actionable advice:

**Take the ACE survey:**

The first step to start your healing journey is to take your ACE score. You can take the test online. It includes ten questions and reports your score, so that you can see how you're dealing with your ACEs.

**Suggested further reading:** ** _The Gifts of Imperfection_** **by Brené Brown**

_The Gifts of Imperfection_ offers an accessible and engaging walk through the ten principles that you can follow to live a more fulfilling life, defined by courage, connection and compassion towards others. Filled with relatable anecdotes and actionable advice, the book is a useful resource for readers both young and old.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Donna Jackson Nakazawa

Donna Jackson Nakazawa is an award-winning science journalist, public speaker and the author of _The Last Best Cure_, which describes her own journey to health despite several autoimmune diseases.

