---
id: 55db33ae2647b10009000074
slug: chinas-second-continent-en
published_date: 2015-08-27T00:00:00.000+00:00
author: Howard French
title: China's Second Continent
subtitle: How a Million Migrants are Building a New Empire in Africa
main_color: CD3931
text_color: B2322B
---

# China's Second Continent

_How a Million Migrants are Building a New Empire in Africa_

**Howard French**

_China's Second Continent_ (2014) is about the mass wave of Chinese migrants who have relocated to Africa in the last few decades. These blinks trace the origins of this migration and outline the profound impact it has on both regions, Chinese-African relations and the world at large.

---
### 1. What’s in it for me? Discover how Africa has become China’s main interest. 

Despite being separated by thousands of kilometers and cultural differences, China and the African continent are more closely linked than one might think. Over the last decades, the world's most populous country has started to take a keen interest in Africa, but it's not only the Chinese government that has become involved — many Chinese people have actually decided to move to Africa to pursue a better life.

So what does this mean for the people of Africa and the Chinese migrants? While it might seem ideal that an economic superpower like China has embraced a continent that still has much economic development ahead of it, the full story isn't quite as straightforward. It's a story that plays out on many levels: geopolitical, economic and on a personal level as well.

In these blinks, you'll discover

  * how there will be more people in Africa than Asia by 2050;

  * why a Chinese farmer moved to Mozambique; and

  * how the main streets of Dakar, Senegal can feel more Chinese than African.

### 2. China has benefited greatly from globalization, but still faces many social problems. 

Everyone has seen the phrase "Made in China" stamped on a product at some point. China might seem to be the world's factory, but it hasn't always been that way. Chinese products actually began spreading in the 1970s, when China started reaping the benefits of the first wave of globalization.

Today, China is the fastest-growing economy in the world, thanks largely to the Western companies that have relocated their production there after the '70s. It was more profitable for such companies to produce goods in China because of the country's large pool of human resources and its different political climate.

In fact, 40 percent of the world's economic growth over the last two decades has come from China alone. Over the same period, the Chinese economy has maintained an average annual growth rate of 10.2 percent — and it's no longer satisfied with just being the world's factory. China is now stepping up as a major player in international politics and is ready to shape the global market.

Economic progress hasn't improved the lives of all Chinese people, however, and many still want to leave the country. Several of the Chinese people interviewed by the author described the intense social pressure they're under at home. Overpopulation, the one-child policy (which limits couples to having only one child) and harsh competition in the workplace have left many Chinese people frustrated with their society.

Hao, a Chinese man who started his own farm in Mozambique, was very clear about his dislike for the social system he left behind. He and many others have emigrated from China, where they can't get ahead because of widespread corruption and the extreme gap between rich and poor. These migrants relocate so they can have more opportunities for advancement, but also to find new homes for their families.

With its rapid growth, China needs a new place to expand to, and it's found the perfect spot: Africa.

### 3. The Chinese government has already been strategically strengthening its ties to Africa for the last 20 years. 

For the last several years, Africa hasn't received much attention from the West. Few Western delegates pay regular visits, but Chinese politicians do, and often.

Countries across Africa are at a critical point right now. African economies are growing quickly and soon the continent as a whole will overtake China's growth rate. Africa's demographics are also changing: it is predicted that by 2050, there will be more people in Africa than in Asia.

This rapid growth presents problems, however. Corrupt governments are unlikely to help their countries develop smoothly, leading to high risks of unemployment, hunger or poor urban development. Large companies can also exploit people more easily if they don't have legally authorized means of protection.

If the countries of Africa play their cards right, they could be the determining force of this century. If they falter, they could plunge themselves into even greater poverty and corruption.

The Chinese government is well aware of Africa's potential, which is why they've been strengthening their ties to the continent since the mid-1990s.

In 1996, Jian Zemin, China's head of state at the time, began courting Africa on a visit aimed at fostering greater Chinese-African cooperation. Years later, he promised to send $5 billion in aid to develop hospitals, schools and other public institutions. He also started encouraging Chinese companies to expand overseas and invest in Africa.

Today, nearly one-third of China's total revenue from abroad comes from Africa. China has invested a great deal in Africa and it's paying off. But there's another important factor that influences the relationship between China and Africa, one that's often ignored in international politics: the Chinese migrants who've resettled in Africa.

### 4. Chinese migrants have had a profound impact on Chinese-African relations. 

There are currently over one million Chinese people living in Africa and they've built a strong network of contacts back home, encouraging ever more and more to make the same move. So what exactly is the appeal?

As the social situation in China deteriorates, many are starting to view Africa as their only hope. A lot of Chinese people who move to Africa want to establish new lives for themselves and their families and they can do that with greater ease if, for example, their sons marry African women.

Take Hao, the Chinese farmer who moved to Mozambique, promised his son to a young woman in a neighboring village, in hopes of having his grandchildren work on the farm he'd opened there. Chinese immigrants (in particular those who marry into the society like Hao's son) have a major influence on the locals.

Chinese migrants are the main factor that determines how African people view China and its rapid expansion into their territory. They have a much bigger impact than the hospitals funded by the Chinese government or the stadiums built by Chinese workers.

Why? 

Because migrants form everyday contacts with the locals. These relationships are much more tangible than the funding behind public buildings.

Migrants aren't always welcomed with open arms, however. Locals in many African countries have started to complain that Chinese migrants are taking over their livelihoods, as more and more streets become filled with Chinese shops and Chinese workers.

In some countries, migration even plays a role in elections, as people hold protests and demonstrations against what they see as Chinese colonization. Locals usually want to be more involved in Chinese businesses, but the business owners often just get more workers from China. Naturally, this has caused tensions between the groups.

### 5. Chinese migrants and African locals both have prejudices against each other, but know their partnership could be very beneficial. 

It's not uncommon to hear Chinese migrants in Africa make racist statements like "blacks are lazy." Africans, on the other hand, often complain that the Chinese are taking over their communities and lack basic respect for their culture. These prejudices exist for a number of reason.

One reason is that Chinese migrants are often uneducated and uninterested in their host cultures. Many of the migrants the author interviewed lacked basic knowledge about the African countries they were living in. They tended to know little about their cultures, sometimes not even knowing what language they spoke. Hao didn't even know which countries border Mozambique.

The locals are often ignorant too, however. They tend to see Chinese people as ruthless conquerors out to steal their resources and not interested in giving anything back.

One of the main problems Africans have with Chinese migrants is that they claim they're fostering growth in Africa, but most of their profits are sent back home. There are many Chinese businesses in Africa that conduct their operations with little or no involvement of locals. When they _do_ hire Africans, businesses pay them less than what their Chinese workers earn.

But both sides are aware that they stand to gain a lot from each other in the future. Despite their differences, a lot of Africans believe that Chinese migrants can bring more educational and technological advances to the continent, which could bolster Africa's political power worldwide.

Chinese people also recognize that Africa can provide them with immense human resources; there will also be greater markets for their products if African countries prosper.

There are still many obstacles to forging closer Chinese-African relations, but both regions are increasingly recognizing the potential of their partnership.

### 6. China often exploits African resources, which causes a lot of political tension in African communities. 

Mozambique is one of the most fertile countries in the world. Its soil is so rich that the jungle would take over cropland in a matter weeks if it wasn't kept at bay. Local tribes have cultivated this land for centuries, but many now feel the Chinese are stealing it.

Many territorial disputes like this one stem from legal problems and a disregard for the rights of tribes who use the land. In most Western countries, one has to buy land from the state, but in Mozambique, tribes have farmed for generations without having state-sanctioned ownership of their land.

This means that farmers like Hao can "buy" land that has entire communities farming on it, then expel them. This creates a lot of animosity between the groups: locals hate the farmers who steal their livelihood while the Chinese migrants view locals as barbaric inconveniences.

And it's not just the land that causes disputes. In Zambia the conflict is over copper — a resource that a number of Chinese firms have made a fortune off of in recent decades.

Copper mines are harmful to the environment and they employ Zambians in very dangerous working conditions for low wages. Some mines don't even provide workers with protective gloves and goggles.

The terrible work conditions have caused a number of deaths and serious injuries, prompting Zambians to hold protests against them. In 2006, for example, a Chinese manager opened fire on a group of miners protesting against their supervisor, injuring six. The previous day, a Chinese manager was beaten up by several Zambian miners over a dispute about payment.

Chinese migrants in countries like Zambia have provoked a lot of hatred, which is likely to grow in the coming years unless the current state of affairs changes soon.

### 7. Chinese businesses and workers are rapidly increasing their presence and power in African economies. 

Not all Chinese migrants are wealthy business owners. Many of them are used to what the Chinese call "eating bitter": doing very hard work to sustain themselves and their families. The economic differences among Chinese migrants also have an effect on their relationships with the locals.

In Senegal, Chinese migrants have taken over nearly all of the country's trade in recent years. As recently as 15 years ago, if you walked down the main streets of Dakar, you'd see a beautiful scene with small cafes and restaurants. The same area is now overrun with shops selling cheap mobile phones, jewellery and other tourist souvenirs that break right after you buy them.

Local businesses have died out as these shops have proliferated, and many Senegalese people are taking to the streets to protest. A lot of them have lost their jobs and feel that the Chinese are invading.

The situation is even worse in Liberia. The Liberian state was created by the United States, ostensibly to set a democratic and Christian example for the rest of Africa. It still receives aid from the United States today, but much of it gets lost in corrupt government agencies and doesn't actually reach the Liberian people. Unemployment is high and illnesses like malaria and yellow fever abound.

Chinese migrants have started building hospitals to garner favor with Liberians, but they still tend to employ mostly Chinese workers. Crime rates are high, so Chinese business owners often fear their products will be stolen or their buildings looted and destroyed.

There's little public education in Liberia and many ill people go untreated because they can't afford the hospital fees. So, even with the increasing investment from China that Chinese migrants bring, it has done little to improve the actual conditions for local residents, and the situation in Liberia is still dire.

### 8. In the more corrupt African countries, the Chinese government makes backroom deals that don't benefit local populations. 

Mali is one of the poorest countries in the world. The Malian government is highly corrupt, and often makes secret deals with the Chinese government. These deals don't benefit the local people at all — only the leaders' themselves benefit.

The Chinese government often takes advantage of corrupt African governments in this way to secure more business deals and resources. Beijing hasn't established itself as a big creditor or business partner for Africa; rather, they have gained their influence through corrupt methods like paying off government workers to negotiate behind closed doors. They also don't involve any NGOs or other organizations that monitor negotiations and protect the interests of locals.

The Chinese government _has_ sponsored some important projects in Mali, like the construction of hospitals and other important infrastructure. In exchange, the Malian government has allowed them to acquire great tracts of farmland, at which point they have kicked local farmers off the land, forcing them to rent their own land back from its new Chinese owners.

All in all, such backroom deals rarely benefit the average person. China and many African countries have an unfortunate thing in common: large-scale corruption. Even big projects like the construction of new factories aren't always good news for local industries.

And when projects _are_ completed, Chinese workers often return home, leaving the locals with shoddy buildings they can't repair because they aren't familiar with the foreign technology used to build them.

China's strategy of establishing themselves as Africa's go-to business partner for expensive and useless business deals will certainly affect the way Africans view China in the coming years.

### 9. The Chinese government is building a new kind of empire in Africa. 

When you hear the word "empire," you might imagine the once-sprawling political and colonial spheres of Britain or France. The Chinese government isn't using that strategy in Africa, but the scope of their control and influence is still starting to resemble that of an empire.

In the past, empires could only be established through force. European powers colonized Africa by swooping in with their militaries and asserting economic and political domination. They oppressed the locals, then took their resources and sent them away.

This was done without any care for local populations, leading to horrific political systems like apartheid and leading to a series of revolutions.

China is taking a different route in Africa. The constant stream of new opportunities there ensures that African countries will keep depending on their Chinese partners and Chinese migrants. China will continue to increase its control in the market, buying more and more land to feed and sustain their population there.

Africa's dependence on China also means that African governments and companies will have to keep accepting the corrupt deals that China uses to expand its power.

China is using these deals (and other methods) to build a new kind of empire in Africa. It's an empire of interest — no other country in the world is more invested in Africa than China. As China's influence in African politics and economics grows, the corruption in the two regions will continue to intertwine.

China's new empire will have a massive impact on Africa's future, and the future of the world at large. Africa's population is growing very rapidly and its economies are in a great state of flux. The rest of the world will have no choice but to respond to these major Chinese and African developments.

### 10. Final summary 

The key message in this book:

**Though it's quite different from the traditional colonial strategies employed by Britain and France, China's present-day actions in Africa still resemble those of an empire. The Chinese government has managed to assert control of key sectors of African economies through backroom deals and the flood of Chinese migrants relocating to the continent. The growing Chinese influence in Africa will have a profound impact on both regions, and on the rest of the world as well.**

**Suggested further reading:** ** _From the Ruins of Empire_** **by** **Pankaj Mishra**

In _From the Ruins of Empire_, author Pankaj Mishra examines the past 200 years from the perspective of Eastern cultures and how they responded to Western dominance. The book charts in detail the colonial histories of Persia, India, China and Japan in the nineteenth century to the rise of nation-states in the twentieth century. Select stories of cultural figures help to humanize the often violent clashes of cultures, showing the powerful influence of individuals in the course of history.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an e-mail to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Howard French

Howard W. French is a professor of journalism and photography at the Columbia University Graduate School of Journalism. He served as a foreign correspondent for the _New York Times_ for 23 years and has been nominated for the Pulitzer Prize.

