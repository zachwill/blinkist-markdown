---
id: 5a23a8bfb238e100062d55ad
slug: chinas-super-consumers-en
published_date: 2017-12-05T00:00:00.000+00:00
author: Savio Chan and Michael Zakkour
title: China's Super Consumers
subtitle: What 1 Billion Customers Want and How to Sell it to Them
main_color: A6212B
text_color: A6212B
---

# China's Super Consumers

_What 1 Billion Customers Want and How to Sell it to Them_

**Savio Chan and Michael Zakkour**

_China's Super Consumers_ (2014) is the definitive handbook for foreign companies who want to sell their products on the Chinese market. These blinks walk you through the opportunities and challenges in this vast and varied country and share valuable information about how to succeed in its unique business context.

---
### 1. What’s in it for me? Understand how to reach the biggest consumer market in the world. 

When we think about the Chinese economy, we tend to think of it as a big factory — one great manufacturer of cheap goods and trinkets. While this is indeed partly accurate, it is far from the whole story. With a population of well over 1 billion people, China is also a huge market for selling goods, but those who wish to do so must first understand the uniqueness of China's consumers.

In the following blinks, we intend to do just that. You'll develop a deeper understanding of how the Chinese market and Chinese consumers work, think and act. We will look at what sets China's consumers apart from consumers in the West and the lessons that anyone hoping to break into the Chinese market needs to understand.

You'll also find out

  * how e-commerce is one field where the Chinese are really the world's super-consumers;

  * why marketing to Chinese consumers involves more than attracting individuals; and

  * how the West can reach 200 million Chinese customers in 2020 without even going to China.

### 2. Retail opportunities are numerous in China, but dramatically different from their Western equivalents. 

Over the past two decades, Chinese culture has transformed, and the country's laws have changed along with it. This is big news for foreign firms, as legal structures in China used to hamper consumer culture.

These days, however, reforms have resulted in the rapid construction of new department stores and malls across the country. The current context presents a prime opportunity for foreign firms to sell their products.

But before your company dives into the Chinese market, it's important to know about the significant ways that Chinese department stores differ from their Western counterparts.

First, Chinese department stores play the role of landlord to retail brands that rent space on a floor and design a miniature store for themselves in that space. By contrast, Western department stores are merchants who hold inventory and do their own merchandising. In the Chinese model, brands act as retailers, which presents a challenge for foreign companies lacking experience in this arena.

Second, the Western distinction between wholesale and retail isn't the norm in China, since no corporate buyers exist in Chinese department stores. Rather, each brand is responsible for determining the product mix to order, delivery, merchandising and staffing.

When it comes to Chinese malls, the differences are more subtle but still important. For instance, unlike in the West, where brands and retailers in malls can remain in business despite poor performance, Chinese mall owners can terminate a lease if a retailer underperforms.

Not only that but unlike the typical two-level malls of the United States, Chinese malls usually have four to six levels, with the most expensive rents on the first floor and the least expensive on top. The difference is justifiable since foot traffic is dramatically lower on the top levels.

Finally, apparel, footwear and accessories tend to sell very well in Chinese malls, while electronics, bedding and home decor fall flat. The reason for this is that in China, malls have become centers of youth culture, just like in 1970s and 80s America. These young Chinese shoppers frequent malls to buy items like clothing that help them shape their identities.

### 3. China has surpassed the United States as the biggest and most important e-commerce market on Earth. 

It's no secret that China is rapidly developing in terms of its economy, culture and technology. In fact, transformations that typically occur over five to seven years in more developed economies can happen in just a year in China. And this difference is even more pronounced in the world of e-commerce.

For instance, according to a recent study conducted by the magazine _Focus Money_, in China, shoppers make 8.4 online purchases per month compared to just 5.2 in the United States, 4.3 in the United Kingdom and 2.9 in Germany. Not only that, but China now has more online shoppers than the United States has inhabitants!

In no small part, this trend is because of the Chinese firm _Alibaba_, the world's largest e-commerce company, which has a hand in 80 percent of all online purchases in China. The company utilizes a platform called _Tmall_, where 135,000 foreign and Chinese brands sell directly to 600 million Chinese consumers.

What's more interesting for American firms is that a recent study by the Boston Consulting Group found that 50 percent of Chinese consumers prefer American products to their Chinese equivalents, and are willing to pay for the difference. As a result, American brands can often sell their products at a premium.

That all being said, there are definite drawbacks to the Chinese world of online shopping. For one, brands that sell in China need to register every piece of their intellectual property in the country. If they don't, they'll quickly find that another firm has claimed it as their own. China abides by a "first-to-file" rule for intellectual property rights rather than the Western approach of "first-to-use."

And second, the Chinese e-commerce market can be difficult for luxury brands. Just 5 percent of luxury purchases in the country are made online. Consumers are wary of being sold fakes and want to be able to feel and touch the luxury craftsmanship before shelling out the big bucks.

> _"What is the most commonly spoken language on the internet? It's not English, it's Mandarin Chinese and its sister languages and dialects."_

### 4. The foreign companies who succeed in the Chinese market do so through careful planning and strategy. 

So, China is now the world's largest consumer market, which means foreign firms need to reassess the outdated idea of China as a mere mass producer of low-quality products. More specifically, rather than focusing on manufacturing and exporting their goods to China, foreign companies need to incorporate the six _megaprocesses_ of the supply chain into their strategy. These are _planning, buying, making, storing, distributing_ and _selling._

One company that has been exceptionally successful in this regard is the Italian luxury apparel firm, Ermenegildo Zegna. While Zegna enjoyed healthy profits in China in the past, it was only by adapting its entire supply chain strategy that the firm became the single best-selling luxury men's clothing line in the country.

To accomplish this incredible feat, the firm applied analytical approaches and best practices across all six megaprocesses, while staying mindful of the key differences in the Chinese market. It proceeded patiently, gradually opening new stores and building up its brand identity. It also emphasized its top-quality products and service. Having enjoyed a five-fold increase in revenue over just three years shows that it's clearly doing something right!

Many retail companies also struggle with problems in their Chinese supply chains.

Their issues tend to be related to things like poor quality products and unpredictable supply. To avoid these problems, you need to have clear answers to the following questions in relation to your company:

First, what's your strategy for things like procurement, IT, purchasing and managing relationships with suppliers?

Second, what processes do you need to execute this strategy? Is the human capital in your organization sufficiently informed about China to carry out this strategy?

Third, is the technology your company uses to support these operations sufficiently robust to be used in the Chinese context?

And finally, what metrics will you use to measure your success?

If you have solid answers to each question, you will vastly improve your chances of having a problem-free supply chain.

### 5. Different Chinese markets require distinct approaches to advertising. 

The first ever Chinese television commercials for cars aired in the 1980s, employing a standardized and boring format; the ads featured a photograph of a car against a blue background with text to the effect of "We are now selling Toyotas. Please call this number."

It's difficult now to even imagine such ads and, in the current climate, Chinese marketing couldn't be more different. Today, finding the right message for your target audience is essential, and when it comes to marketing in China, there are a few key things to keep in mind.

First off, China comprises many different kinds of markets. Branding and messaging by Chinese companies are focused more on reliability, benefits and value when targeting emerging markets, but more focused on customer relationships and emotional connections when targeting developed ones.

A great example is Lenovo, the largest vendor of smartphones in mainland China since 2014. The company chalks its success up to its ability to segment Chinese consumers and tailor marketing to each group. To do so, Lenovo varies its branding according to the market it's targeting, and specifically based on whether the market is mature or emerging.

For instance, Lenovo's products in a mature market are branded as international goods, with an English logo and tagline as well as a higher price point, and the company positions itself as a global brand. After all, in this market segment, the company competes with brands like Apple and Samsung.

However, in developing markets like those in small cities, villages or rural communities, Lenovo sticks strictly to Chinese in its logos, ads and collateral, clearly positioning itself as a Chinese firm.

Another important thing to remember is that the creative factor in Chinese branding is different from its Western counterpart. For example, in the West, creativity in advertising revolves primarily around the values of an individual, often resulting in attempts to validate a consumer's self-image.

But in China, creativity in marketing is attached to collective cultural values like harmony and happiness, as well as the notion that the brand meets Western standards of prestige and quality. Simply put, for Chinese consumers, the group acceptance of a product is an important selling point.

### 6. Chinese luxury consumers fall into different categories, each with its own needs. 

Chinese consumers now make up over one-quarter of all luxury purchases worldwide. That means one out of every four luxury watches, handbags, cars and necklaces makes its way into the home of a Chinese consumer.

To break this incredible volume of purchases down, you can think about Chinese luxury consumers as belonging to three distinct groups, each with different purchasing habits.

The first group is the _Chinese nouveau riche_. These are the business and political elite who have, over the past two decades, amassed incredible wealth; they're sophisticated, well traveled and experienced consumers. For this group, material luxury goods are old news and what they want instead is novel, exclusive experiences, like courtside basketball seats and tickets to movie premieres.

Next, we have the _gifting group_. This is an important segment because gift giving is core to Chinese culture. The consumers in this group buy luxury goods on a tremendous scale, both in China and abroad, as a means to build and maintain social relationships. Because of the importance of this practice, this group also isn't very sensitive to price; they see their purchases as an investment to gain social capital.

And finally, there are _middle-class consumers_. This is by far the largest group both in terms of population and potential. These consumers are more price-conscious than the other two and also value product quality and prestige.

Since it's so full of potential, we'll focus on this last group. To truly capitalize on this segment of middle-class luxury consumers, companies should play on the desire for quality and functionality.

A case in point is Brooks Brothers, a luxury American clothing brand. The company has had incredible success selling in China, with 40 percent of its stores now located in China's largest cities. So how did this come about?

The firm carefully crafted a position for itself as the producer of exceptionally well-made clothing at a reasonable price, perfectly suiting the desires of middle-class consumers. Beyond that, it also hammered home the excellent value of its products, which is another important factor for this lucrative market segment.

> Chanel, Louis Vuitton and Prada are the three most desired luxury brands in China.

### 7. Chinese tourists represent a growing and lucrative prospect for travel and real-estate companies. 

A mere decade ago, the Chinese government only allowed its citizens to travel to a handful of other countries. Today, things are different. Chinese nationals regularly board flights to over 150 nations worldwide, and this explosion in Chinese tourism naturally implies new opportunities for foreign travel companies.

For instance, in 2001, under 10 million Chinese citizens traveled to foreign countries. By 2014, that number had grown to 100 million, and it's estimated to reach 200 million by 2020.

In addition, the way they travel has also changed. Up until the mid-2000s, Chinese tourists tended to travel as part of small, highly regulated group tours because of their lack of foreign travel experience and their tight budgets. But today, group tours are exceedingly uncommon, as Chinese travelers spend more money per person per trip than tourists from any other nation, at roughly $7,000 on average.

Those that do opt for group tours have higher standards, expecting more lavish accommodation, food and itineraries.

Besides seeing the world, another major motivation for the wealthier Chinese travelers is to purchase foreign real estate. This is only natural, as increased travel has given wealthy Chinese consumers a better understanding of the global property market.

As a result, many Chinese families who can afford to do so are seeking to turn their liquid assets into foreign real estate and lead binational lives.

In fact, Chinese citizens are the number-one foreign buyers of real estate in many major cities like New York and Los Angeles. And Chinese involvement in foreign real estate is only increasing; the luxury real estate broker Sotheby's reported that, in 2009, 5 percent of their real estate sales were to Chinese investors — by 2014, that number had soared to 35 percent!

### 8. Final summary 

The key message in this book:

**The Chinese market holds incredible potential for foreign firms, but the country exists in a context unlike many others. To succeed, foreign companies need to understand the motivations behind China's growing mass of consumers. By studying the nuances of this group, your company can succeed in the Chinese market.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Alibaba's World_** **by Porter Erisman**

_Alibaba's World_ (2015) tells the story of Chinese e-commerce company Alibaba, which was founded in 1999. _Alibaba's World_ offers valuable insights into leadership and business acumen that can help you put your company on the path to success.
---

### Savio Chan and Michael Zakkour

Savio Chan is the CEO of US China Partners Inc., a consulting firm that helps organizations design and implement their consumer strategies in China. He has been featured in the _New York Times_ and _Forbes_, and serves as a member of the National Committee on US-China relations.

Michael Zakkour writes on business in China for _Forbes_, the _Wall Street Journal_ and _Harvard Business Review._

© Savio Chan and Michael Zakkour: China's Super Consumers copyright 2014, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

