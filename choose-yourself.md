---
id: 54033ed56464630008000000
slug: choose-yourself-en
published_date: 2014-09-02T00:00:00.000+00:00
author: James Altucher
title: Choose Yourself
subtitle: Be Happy, Make Millions, Live the Dream
main_color: 69B5C3
text_color: 3F6D75
---

# Choose Yourself

_Be Happy, Make Millions, Live the Dream_

**James Altucher**

Author James Altucher explains that after the 2008 global economic crisis, you can't wait to be chosen; you have to _Choose_ _Yourself._ This means you have to take full responsibility for your own success and happiness by reclaiming control of your aspirations and dreams. To do this, the book gives you both tools and effective practices to stay physically, mentally, emotionally and spiritually healthy.

---
### 1. What’s in it for me? Become more decisive and carefree. 

After reading these blinks, you'll

  * **Stop worrying about pointless things.** You'll no longer spend time and energy worrying about things you can't change, and you'll instead focus on more creative and meaningful ventures.

  * **Stop letting other make decisions for you.** You'll lose the fear of rejection that makes most people passive pawns of others' decisions. Instead, you yourself will take control of achieving your dreams.

  * **Stop surrounding yourself with the wrong people.** You'll learn to distance yourself from people who just drain your energy and make you angry.

### 2. The American Dream of a steady middle-class job and “keeping up with the Joneses” is over. 

If you were to ask your parents about their ambitions as they were growing up, many of them would probably tell you, "I wanted to get an education, get a steady job and then buy my own house."

From the end of World War II until the global financial crisis in 2008, the population of the United States was living _The_ _American_ _Dream._

What was behind this dream? In a word: _work_.

Early in this period, as men went off to fight in Europe, many women entered the workforce. Yet when the war was over and soldiers came home, women continued to work. For the first time, both men and women in significant numbers were wage earners, putting food on the table and paying the bills.

With two people working, families had more disposable income than ever before. This meant that they could purchase cars, bigger houses and new appliances, experiencing a standard of living of which their parents could only have dreamed.

However, behind the shiny facade of the American Dream was something sinister: a marketing strategy designed to get people to spend money. Americans were increasingly encouraged to express themselves with their cash, showing off their successes by owning more stuff than their neighbors.

Since the advent of 2008 financial crisis, however, the American Dream has come to an end.

Steady, middle-class jobs are a thing of the past. To reduce costs, companies have either outsourced jobs to developing countries or simply replaced employees with technology.

Today, many jobs are only temporary, and offer a worker no security or freedom. Millions of people encouraged to invest in their education, then find a steady job and live the American Dream are now either unemployed, or overqualified and underpaid for what they do.

It's a devastating story. But should we be sad about it? As you'll see, there's a positive side to the death of the American Dream, as it has led to a new, promising era.

### 3. We are now in the “choose yourself” era, in which you must exert control over your life. 

So the American Dream is over. But that's not a bad thing. We are now moving into a new era of personal freedom.

The previous era of working and spending was dependent on other people controlling your life.

To succeed in the American Dream, you needed someone else to "choose you." For example, if you wanted a better job, you had to keep your head down and work hard, hoping your boss would notice you and single you out for a promotion.

This was true even in the creative industries. For instance, if you'd written a good play, book or TV show, you'd have to wait to be chosen by the gatekeepers — the industry executives — as only they could turn your idea into a successful reality.

Today, the world is a different place. You no longer need anyone else to make you a success. Instead, you can — you must — _choose_ _yourself_.

Since the only jobs for you in the corporate world are temporary ones, you have to step outside this realm and empower yourself.

But how?

Aside from self-confidence, all you need is a smartphone or a laptop with an internet connection, and you can do whatever you want. If you've written a book, you don't need a publisher or an agent; publish the book yourself online. If you've created a TV show, you don't need to pitch it to others; post it on YouTube to reach your audience.

By choosing yourself, you also avoid falling into the trap of just chasing money. The basis of the American Dream was the need to earn more and then buy more. As long as the work you did paid for this lifestyle, it simply didn't matter that you were essentially unhappy.

The new era is different. We have the freedom to do what we want, and if we make money doing it, then we will be happy!

> _"We're taught at an early age that we're not good enough. That someone else has to choose us in order for us to be."_

### 4. Our fear of rejection means we are forever waiting on someone else to decide our fate. 

Have you ever stared across a coffee shop at someone you thought could be the love of your life?

If you have, chances are you did nothing about it. But it's not just you: most of us would rather wait for the person we desire to come and talk to us.

Why? Because, above all, we fear rejection, and we'll do anything to avoid it.

This fear of rejection is behind our feeling that we have to let others "choose" us. Most of us simply believe we aren't good enough, and that to succeed, we must let others (teachers, bosses, publishers and so on) decide for us.

To better understand this attitude, Yale psychology professor Stanley Milgram performed a study.

Milgram instructed a few students to use the subway and ask sitting passengers to give up their seats for them. The result? An unbelievable 70 percent of passengers gave up their seats — without asking any questions.

As this suggests, most people are prepared to let others determine their choices for them.

We must stop thinking this way. Instead of fearing rejection and letting others choose for us, we should make our own decisions and empower ourselves to choose.

The alternative? We let others decide our future — in which case, we expose ourselves to rejection and become unhappy when we _are_ rejected.

This very thing happened to the author, who'd developed a TV project called III:am (for HBO) which he put a great deal of work into. Yet to get it made, he depended on one person in a TV company to say "yes."

Ultimately, the company said "no," the project came to sudden stop, and all of that work went to waste.

This is no way to live one's life! You have to make yourself the only person who can control your dreams.

But it's easy to _say_ that you're not going to let anybody control your dreams. Doing it is a lot more difficult. So how do you get over the fear of rejection? How do you "choose yourself"? Find out in the following blinks.

> _"Everyone is an entrepreneur."_

### 5. To choose yourself, you have to take care of both your physical and mental “bodies.” 

So, you're fired up and ready to start living in the "choose yourself" era!

But how do you start? It can be quite daunting, to go out and sell yourself — and face rejection — on a daily basis.

To prep yourself for this, day in and day out, you have to develop a solid foundation.

This means taking care of your four "bodies": the _physical_, the _mental_, the _emotional_ and the _spiritual._

Let's look at the first two: physical and mental.

Our physical body is, obviously, an essential part of our life, allowing us to thrive. For that reason, we need to stay healthy and fit.

Looking after one's physical body is straightforward: get enough sleep, around seven to nine hours a night; eat just two meals per day, not three; get regular exercise and fresh air; and so on.

Caring for your mental body, on the other hand, is a little more tricky.

Most of us waste mental energy by compulsively overthinking and worrying about things we can't control. This is an easy habit to develop, and the consequences are serious: worrying about or fearing things you have no control over will lead to burnout.

To rein in this problematic habit, you have to train your mind to work toward positive ends. And the best way to do this is to train your _idea_ _muscle._

Try daily to read or skim certain chapters from four works on a variety of subjects (ranging from celebrity memoirs to scientific tomes).

Then note down _at_ _least_ 10 ideas. The content of these ideas is not important — they could be grand ideas, like solving world hunger; or more modest ones, such as a list of ways to please your partner. The main thing is that you generate ideas and set them down.

You'll soon find yourself with no time to worry about pointless things, as you'll be too tired from generating useful ideas!

### 6. A solid foundation for choosing yourself is having healthy spiritual and emotional “bodies.” 

In the previous blink, we learned that having the confidence to "choose yourself" depends on having healthy physical and mental bodies.

But that's not all you need. There are two more bodies you have to nourish: your emotional and spiritual bodies.

Your emotional body is important: to "choose yourself" and thrive, you have to eradicate negativity and be positive.

But how to achieve this? Stop surrounding yourself with people who make you angry. We all know the types — these are the kind of people you complain about with your partner or friends, saying "You won't believe what she said this time!"

Maintaining a relationship with this sort of person is a drain on your energy and weakens your emotional health. Therefore you need to gradually distance yourself from people like this.

It is, however, important to be kind. As you try to dissolve your negative relationships, try not to be rude. Instead, greet the person warmly if you see her around, but don't allow yourself to engage any further.

By removing this barrier to thinking positively, you'll nourish your emotional body and increase your ability to thrive.

And finally, the spiritual body. A healthy spiritual body comes from existing only in the here and now.

Most of us spend much of our daily lives "time traveling," that is, either regretting the past or worrying about the future, events we can't do anything about. Time traveling like this leads to burnout and inhibits us from realizing our full potential.

To combat this, make an effort to be in the moment, continually focusing on the _now_.

Pay attention to your surroundings. If you're in a city, take time to look at buildings, exploring their architectural whims, their particular designs. Doing so will keep your mind rooted in the present and stop you from drifting toward the past or future.

### 7. Establish daily routines to improve your physical, mental, emotional and spiritual “bodies.” 

Now that you know what to do to build a solid foundation, there's nothing stopping you from getting out there and thriving in the "choose yourself" era.

Right?

Well, not quite so fast. As enthusiastic as you might be at the start, it's often difficult to stick to a plan.

So how can you stay on task, keeping your four "bodies" healthy and thriving?

Simple. We need to build habits through daily routines.

Changing your life doesn't begin with changing external circumstances, such as jobs, friends and so on. Indeed, these changes happen toward the _end_ of the process.

Rather, we change our lives by first changing _ourselves_. But how?

The best way to make this internal change is to create a daily routine in which you do something "good" every day. This routine could include such simple maxims as, "No TV or junk food!" or "Think of 10 people you're glad to have as friends."

Another good habit to establish is to help others as much as you can. Helping others releases large amounts of the hormone _oxytocin_ into your bloodstream. This wonderful chemical (also released during orgasm and childbirth) helps keeping you healthy and calms you.

Oxytocin flows through you when you help someone out, or even express gratitude. You can also produce this hormone simply by hugging someone, giving a gift — or even "liking" a friend's Facebook post.

In fact, at the height of the 2008 financial crisis, the author himself went to Wall Street and handed out gifts of chocolate to traders and bankers. Not only did this help boost the author's oxytocin levels, but it also gave the bankers a shot of _phenylephylamine_ — a hormone released by eating chocolate and falling in love.

> _"You build that house by laying a solid foundation: by building physical, emotional, mental and spiritual health."_

### 8. Searching for your life’s purpose is pointless and stressful, and can lead us to pseudocide. 

Be honest with yourself. Are you afraid that you haven't yet found your true purpose in life?

The fear that comes from not knowing what to do with our lives is a great source of stress. Yet there's no reason to worry: some of the world's most successful people didn't find their true purpose until much later in life.

Colonel Sanders, of the fast-food chain Kentucky Fried Chicken, didn't make a cent from his special recipe until he started franchising when he was already 65 years old.

And it was only after Peter Roget was forced to retire from his job as a physician at 70 when he was inspired to complete and publish his now-famous thesaurus.

This suggests that stressing over your "purpose" is quite pointless. Indeed, some people _never_ find their purpose. For that reason, it's far more important to enjoy yourself and your life and do whatever work pleases you.

Many people never realize this. They search for purpose, or strive for success, and when the pressure gets too much, they try to escape.

This desire to leave it all behind, disappear and start a new life is called _pseudocide._

But while it may be possible to start over, doing so won't make you any happier. Your internal problems — the same stresses and pressures — will still exist.

So, instead of aiming to disappear, find a way to _truly_ free yourself from the stresses of life. And one way to do this is through the following meditation exercise.

Imagine that you're homeless, with no goals for the future, and carrying zero baggage from your past. Try to imagine this situation in as much detail as possible.

This exercise can help you to truly forget about any stress you might feel, and to momentarily experience what it's like to be "reborn," with fewer troubles and anxieties.

### 9. Choosing yourself requires cutting out the middleman and getting the most value from your sales. 

Getting into the right mind-set and maintaining a strong foundation and healthy bodies is all well and good. But how can you make a living from it? How do you succeed financially in the "choose yourself" era?

Let's look at the example of Bryan Johnson, the entrepreneur behind the multimillion-dollar credit card service _Braintree._

Johnson previously worked for a credit card processing company, visiting restaurants and businesses to ask them to switch to using his company's services.

He hated this job, especially the fact that he was working for someone else. So, Johnson decided to do it alone, doing essentially the same job but working for his own company.

What can we learn from this?

First, in the "choose yourself" era, working for someone else is no fun. Second, you have a perfect opportunity to cut out the middleman: don't work for a company that provides a certain service or product — instead, provide it yourself. If there's demand for what you offer, you stand a good chance of making your company a success.

Johnson's success depended heavily on his ability to sell himself, which is one of the most important factors in this new "choose yourself" era.

One of the best ways to sell yourself is to always consider the bigger picture. It isn't about getting the best price for every sale (whether you're selling a product or your entire business) but getting the best _value_ from it.

For example, the author worked on creating movie websites for New Line Cinema for just $1,000 per movie — 200 times less than he would usually charge.

Why?

Because he knew that by working for such an illustrious company, the best designers in the world would flock to join his company. While it wasn't the best negotiated sale, the author's company gained a lot of value from it.

### 10. The only way you can succeed in the “choose yourself” era is to be honest. 

For the longest time, the maxim, "crime doesn't pay," seemed idealistic and out of step with reality. Fraudster Bernie Madoff, as just one example, conned people out of an incredible $65 billion in 2008.

Although Madoff was arrested before he ever got his hands on the money, for many people his "success" confirmed that the easiest way to earn lots of money was to lie and cheat.

But in the "choose yourself" era, this isn't true. In fact, the only way to succeed today is to be honest.

Of course, being dishonest may work in the short term. But regardless of how successful you are, the moment you're exposed as a liar, your luck will have run out. No one will ever trust you again! (Surely no one will invest with Madoff these days.)

Honesty _compounds_ itself — it grows exponentially. But what does that mean?

By being truthful and fair, you win people's trust. Once you've done this, they'll want to stay in business with you, help spread the word about your company, and so on.

Moreover, if you fail, those who trust you will be more likely to help you start over again, allowing you to build your business over the long term.

Yet if you're not a completely honest person, learning how to become one isn't easy. So how can you become more honest?

Try incorporating these habits into your daily routine.

First, don't get angry. Anger comes from wanting others to do want _you_ want them to do. Because you hold unrealistic expectations for other people, you're basically lying to yourself. To stop this, whenever you feel anger toward another person or group, observe exactly how you feel. Then, try to turn it around so you blame _yourself_ rather than someone else.

Second, don't gossip. Ridiculing or mocking someone behind their back is a dishonest thing to do, and whatever you say about that person will undoubtedly get back to him or her in the end.

> _"Choose to commoditize your labor or choose yourself to be a creator, an innovator, an artist, an investor … an entrepreneur."_

### 11. Final summary 

The key message in this book:

**The** **American** **Dream** **is** **dead,** **an** **era** **where** **you** **had** **to** **wait** **for** **your** **superiors** **to** **chose** **you** **for** **promotion;** **today,** **you** **must** **_choose_** **_yourself_** **.** **This** **means** **reclaiming** **control** **over** **your** **dreams** **and** **taking** **full** **responsibility** **for** **your** **success** **and** **happiness.** **Choosing** **yourself** **requires** **that** **you** **maintain** **a** **healthy** **emotional** **and** **physical** **foundation** **as** **well** **as** **establish** **habits** **and** **routines** **to** **help** **you** **to** **stick** **to** **your** **plans.**

Actionable advice:

**Only** **do** **those** **things** **that** **make** **you** **happy.**

Most people go through their lives doing things they don't want to do, which makes them stressed and unhappy. You should avoid this! Do whatever you think works best for you, and make sure that the hours you work, the clients you take on and the job you do is based on your desires.

**Believe** **in** **yourself!**

Nothing is more important in the "choose yourself" era than self-empowerment. This can be hard since to varying degrees, we've learned to feel worthless. The solution is to develop a mantra: every day, tell yourself that "you love yourself" over and over again. Soon, you'll begin to notice that your happiness and confidence are increasing.

**Suggested** **further** **reading:** **_The_** **_Startup_** **_of_** **_You_** **by** **Reid** **Hoffman**

_The_ _Startup_ _of_ _You_ is a guide to how you can leverage strategies used by start-ups in your own career: being adaptable, building relationships and pursuing breakout opportunities. In a world where entire industries are being ravaged by global competition and traditional career paths are fast becoming dead-ends, everyone needs to hustle like an entrepreneur.
---

### James Altucher

James Altucher is a bestselling author, successful entrepreneur, podcaster, chess master and blogger. He has co-founded more than 20 companies and regularly contributes articles about technology and investments for _TechCrunch_, _The_ _Wall_ _Street_ _Journal_, _The_ _Financial_ _Times_ and _Forbes_.

