---
id: 519de75ee4b0a17389a8ac0d
slug: civilization-en
published_date: 2013-11-13T14:30:04.958+00:00
author: Niall Ferguson
title: Civilization
subtitle: The Six Killer Apps of Western Power
main_color: CB2D29
text_color: 99221F
---

# Civilization

_The Six Killer Apps of Western Power_

**Niall Ferguson**

There seems to be a crisis of confidence in the West. In the face of the rising power of China, and with a seeming lack of interest in its own history and civilization, many fear that the West has somehow lost it way.

_Civilization_ aims to explain why the West grew so powerful and dominated the rest of the world. The answer lies with six _killer applications_, which enabled the West to overcome the rest. Yet vital questions arise: Has the West forgotten these killer apps and will this lead to its collapse?

---
### 1. Western nations have grown exponentially from relative poverty to world dominance over the past five centuries. 

If you had been able to travel around the globe in the fifteenth century, you would have been more impressed with the civilizations of the Orient than with those of Europe. The societies in the East were more culturally and technologically advanced than their European counterparts. Europe was in fact a relative backwater, plagued by conflict among its many states. While China was able to mobilize its workforce to build great architectural projects like the Forbidden City, Europe was struggling with dreadful sanitation and violence.

Yet towards the end of the century, the nations of Europe had begun to grow economically and militarily. Over the next 500 years, the balance shifted overwhelmingly in favor of the West.

The development of the Western empires is one of the most obvious examples of this phenomenon. In 1500, the Western nations controlled just 10% of the world's surface and 16% of its population. By 1900, this ratio had risen dramatically: just under two thirds of the world's land and peoples, not to mention 79% of global economic output, were under the control of the Western empires.

Increasingly, the West's domination stretched into other spheres. Western religion and Western science spread across the globe, with, for example, economic debate having long been dominated by the ideas of three Europeans: Adam Smith, John Maynard Keynes and Karl Marx. Even Western fashion became the widespread norm.

With this domination, living standards in the West rose compared to those elsewhere. By 1990, the average American was seventy-three times richer than the average Chinese — an extreme reversal of their fifteenth century statures.

**Western nations have grown exponentially from relative poverty to world dominance over the past five centuries.**

### 2. Western civilization developed six killer applications, which enabled it to dominate the rest of the world. 

Historians have long theorized why a few Western nations managed to transform themselves from a position of relative poverty to world dominance. Some have argued that their imperialism, through which they subdued and exploited the rest of the world, was the key. Yet empires have existed throughout history, and many Eastern civilizations, such as the Chinese and Ottomans, had huge empires too. Hence, this and the majority of the other explanations for Western domination are too narrow and simplistic.

The truth is that Western civilization accomplished a series of six _institutional developments_. Institutions are crucial because they frame the values and norms in which a society operates; hence advances in those institutions carried great importance. These six developments (or, to use the language of the modern, computerized world, _killer applications_ ) propelled the West forward. The rest of the world, lacking these developments, was left behind.

An example of such an app is the development of rational _scientific thinking_, which enabled technological advancements and improved military hardware, among other things.

Another example is the spirit of _competition,_ which arose from the constant wars and battles between European nations, and later formed a basis for the development of free market capitalism.

The remaining 'apps': _property rights_, _medicine_, _work ethic_ and _the consumer society_ all had similarly profound and powerful effects.

These six killer apps developed in different areas of the West at different times, but all of them contributed to the creation of ideals crucial to the improvement of human society, among them democracy, political freedom and capitalism. It is important to note though, that some developments weren't so positive; mechanized war, ethnic genocide and the slave trade were also products of Western civilization.

**Western civilization developed six killer applications, which enabled it to dominate the rest of the world.**

### 3. A spirit of ruthless competition, developed in Europe, was vital for the development of capitalism and Western power. 

Due to its geography, Europe is a fragmented continent. For much of its history, its many states were constantly at war with each other. Between 1500 and 1799, Spain was at war with foreign enemies 81% of the time, England 53% and France 52%.

Even though life was dreadful for the populations living in the war zones, this constant warring provided the launch pad for future Western dominance. Competition between states led to technological advances in military hardware as each one tried to gain the upper hand by developing new weapons and defenses.

Even more crucially, the need to fund the fighting led to the development of the key capitalist financial institutions, such as the bond market, the public company and the bank. The competitive spirit also ran through society itself with the various social groups constantly fighting for power and influence.

This competitiveness led the European states to seek fortune and power elsewhere in the world. European explorers and merchants took to the seas to find new markets and trade routes, and they ruthlessly exploited the resources they discovered. Their military strength and desire for wealth allowed them to dominate the local, often much larger, societies they encountered. These conquests brought new raw materials and opportunities, propelling the European societies forward.

Compare this to the strict hierarchical society of China, which lacked competitors from outside of their huge, centralized nation. Without social mobility or any incentive to seek overseas expansion, their society became stagnant and inefficient. The Chinese lacked the crucial killer application that had developed in the West: a spirit of competition.

**A spirit of ruthless competition, developed in Europe, was vital for the development of capitalism and Western power.**

### 4. The development of scientific principles gave the West a crucial advantage over other societies. 

In the seventeenth century, European scholars were busily engaged in discovering the secrets of the natural world. This was the beginning of a scientific revolution, which saw the creations of modern disciplines such as biology, mathematics and astronomy. These developments were almost entirely European in nature.

Why?

The scientific revolution was in part kick-started by the Reformation of the Christian faith in the fifteenth century, which saw Protestants split from the Roman Catholic Church. As opposed to Catholics, Protestants were encouraged to read the bible for themselves, leading to a rise in literacy. At the same time the development of the printing press allowed ideas to be easily circulated. Together these two developments created an environment where new ideas could be easily read and spread.

Western rulers were quick to understand the benefits that these new ideas could provide for their states. A prime example of such a ruler is the Prussian King, Frederick the Great, who embodied many of the new scientific and cultural ideals. This helped him create a liberal society based on rationalism rather than the religious superstition that plagued many other societies. He also applied these scientific theories to war, developing one of the strongest armies of his day.

These principles failed to take off outside the West. In the Islamic Ottoman Empire, for example, religious oppression clamped down both on scientific thinking and the printing press, effectively halting scientific progress and the dissemination of new information. This resulted in the once powerful empire crumbling in strength. While the modern methods of the Prussian artillery became feared across the world, Ottoman forces were still hurling stone cannon balls at their enemies.

**The development of scientific principles gave the West a crucial advantage over other societies.**

### 5. The model of representative government based on property rights and the rule of law provides the strongest societies. 

The discovery and colonization of the Americas provided a perfect experiment between two Western models of political and social organization. The British in North America and the Spanish in South and Central America developed two diametrically opposed systems.

There can be no doubt which system proved most successful. By the twentieth century, the United States had become a world superpower with a stable, democratic constitution. The nations to its south, however, suffered centuries of instability following independence.

In the British colonies, land was shared out among the settlers. Even those who left Britain incredibly poor were given property. Crucially, with this property came voting rights. Following the ideas of the philosopher John Locke, a system where property owners could participate in a representative government was developed. This form of government allowed disputes to be amicably settled and the rule of law enshrined. A culture of private property rights and constitutional government developed, which would form the basis of the US constitution.

In Spanish America, there was no such system. The Crown owned the land, which was governed by a tiny elite. Contrary to the British, the Spanish did not provide the colonial populations — mostly made up of natives rather than settlers — with land. This meant that although they were economically vital, they had no political power.

After independence, they had none of the democratic traditions so vital in the North. Dictatorship, civil war and petty squabbling followed.

The development of a model of representative government based on property rights was a killer application that drove Western civilization forward.

**The model of representative government based on property rights and the rule of law provides the strongest societies.**

### 6. The development of modern medicine spread Western civilization and helped raise global life expectancy. 

Before the development of modern medicine, life was pretty dreadful. Life expectancy across the globe was low and most people expected to die at a relatively young age. Diseases such as plague and smallpox were rife in appalling sanitary conditions; the population of Europe, for example, had been halved by the Black Death epidemic.

Yet in eighteenth-century Europe, life expectancy started to increase. Huge improvements in public health and science led to better sanitation and developments such as vaccinations. By the twentieth century, killer diseases such as typhoid, which had plagued Europe for centuries, were almost eliminated. These developments alone helped the standards of health in the West rocket above those in other civilizations.

However, just as health was improving in the mainland Europe, the Western nations were facing new dangers in their colonies, most notably in Africa. In order to continue their expansion in these areas, they had to overcome deadly foes such as malaria. The colonies effectively became huge laboratories where Western doctors could examine patients and look for cures to these diseases.

As new cures and remedies were discovered, Western empires could reach ever deeper into the tropics, enabling them to discover and exploit even more resources. Hence, the development of modern medicine was another killer application that helped the West.

Not only did these medical improvements benefit Western colonists, they also helped raise life expectancy for the native populations as well. Some, like the French empire, made it a policy to spread medical improvements across all their subjects. In fact life expectancy in Africa improved more under European occupation than with independence. Therefore, despite what some argue, many European empires also had a positive impact on their possessions.

**The development of modern medicine spread Western civilization and helped raise global life expectancy.**

### 7. The formation of a consumer society radically raised the standard of living in the West. 

The Industrial Revolution erupted from developments in the textile industry in eighteenth-century Britain. Due to high labor costs and the abundance of cheap coal, the early industrialists turned to technology to keep production profitable.

The results were astounding. Productivity in Britain shot up and the cost of cotton goods dived. But alongside this revolution in supply, a similar, lesser-known revolution occurred in the demand for textiles. Suddenly, people across the globe desired new, affordable clothing, and this demand fuelled the growth of the textile industry.

This development was not limited to textiles, as all manner of affordable goods were coveted. A market-driven _consumer society_ where people desired to buy and consume affordable goods was created. This was another killer app of Western civilization. Increased demand meant more factories, higher employment and better wages, which in turn enabled the workers to consume more goods.

Nothing better exemplifies the power of the consumer society than the failure of Communism in the Soviet Union. Although the Communists could keep up with the West in their military hardware and heavy industry, they utterly failed to provide their citizens with the consumer goods desired. What the citizens wanted was what people in the West had: jeans, Coca-Cola and pop music. In totalitarian societies, like Communism, what the people demand is often ignored in favor of the production of goods that uphold government power, such as armaments. The people might have money to spend but nothing to spend it on, meaning that living standards fail to rise and there is less incentive to work hard and be productive.

**The formation of a consumer society radically raised the standard of living in the West.**

### 8. Protestantism helped create a work ethic that encouraged people to work hard and save money. 

After the Christian Reformation, beginning in fifteenth-century Europe, the new Protestant nations that were formed grew at a faster rate than Catholic ones. Perhaps the main reason for this was the _work ethic_ developed from the values of Protestantism. This work ethic was one of the killer apps that enabled Western civilization to grow and prosper.

Protestantism is a religion where knowledge, literacy, thrift and hard work are highly valued. These are all principles crucial to economic prosperity. In addition, Protestant communities tend to be based on trust and honesty, allowing for networks of credit and business to thrive, again propelling economic development.

The development of this Protestant work ethic spurred several other developments that were crucial for Western prosperity. The Industrial Revolution, for example, was enabled by the fact that the workforce was more dynamic and hardworking than before. The work ethic, with its adherence to thrift, also encouraged people to save money, allowing them to accumulate capital and invest it onward, spurring yet more growth.

While the Protestant religion helped create a hardworking, success-driven population in the West, religions in other societies acted as a drag against progress. In China, for example, Confucianism helped create a _stability ethic_ where social equilibrium was more desirable than expansion. This was a major reason why Chinese society grew stagnant and then collapsed while the West expanded and progressed. In fact in recent years, the growth of the Chinese economy has coincided with the growth of Protestantism in the country.

**Protestantism helped create a work ethic that encouraged people to work hard and save money.**

### 9. Many other civilizations have tried to copy elements of Western success. 

Many in the rest of the world have looked back at the centuries of Western dominance and wondered how it has been possible. Eager for success of their own, several nations have aimed to _download_ the killer applications that enabled Western civilization to flourish.

The first nation to appreciate the need to westernize in order to grow was Japan. In the late nineteenth century, Japanese leaders attempted to completely transform their society, culture and institutions based on Western models. Out went the centuries of traditional clothing, for example. In its place came Western fashions, including a new Western dress code for civil servants and the military. This westernization also included the development of a Western style industry and consumer society, both of which greatly helped Japan catch up with the Western nations. 

Over the twentieth century, other societies have followed this approach: for example, the nations of Southeast Asia copied the Western industrial model with great success after World War II. Meanwhile, the Turkish rejected the traditional Islamic state after the collapse of the Ottoman Empire, embracing the scientific, rational nation state instead — even going as far as to westernize their alphabet.

Yet, not all such attempts lead to success. When the Japanese decided to westernize their society, they had no idea which elements could provide success (i.e., which apps to download). They therefore endeavored to copy as much as they could. This unfortunately included developing a Western style empire which left a bloody and disastrous trail until its fall in 1945.

Nevertheless, in the hopes of Western-like success, many nations have tried to reject their own traditions and download the killer applications.

**Many other civilizations have tried to copy elements of Western success.**

### 10. The future of Western dominance is uncertain, as Westerners have forgotten some of the vital killer applications. 

The _great divergence_,which saw the West's prosperity and power soar above the rest of the world, is slowly waning. Many fear that the dominance of Western civilization that has characterized the last five centuries is over.

The fears are largely justified. The Western economies have been hit hard by the recent financial crisis. Many are struggling with huge levels of debt and a declining confidence in their economies, while the economies in Asia, most notably that of China, continue to show staggering levels of growth.

Westerners seem to have forgotten some of their own killer applications. The work ethic that sustained so much economic success has largely disappeared in many areas. Europeans now work the shortest hours in the world, while the Western adherence to thrift and saving has disappeared with the availability of easy credit.

More worrying is a loss of Western self-confidence. Since the 1960s, teaching in the West has drifted towards favoring relativism; assuming every civilization is equal, without examining why one culture may be superior to another. This means young people in the West are taught little about the narrative of Western history and the successes of Western civilization. Even Western cultural institutions like museums, schools and the media show more interest in celebrating the history of other civilizations than their own.

While the future of the West could be bleak, we should remember that many elements of Western civilization are still thriving elsewhere: the rise of the rest of the world has been largely the result of them downloading the Western killer applications.

**The future of Western dominance is uncertain, as Westerners have forgotten some of the vital killer applications.**

### 11. Final summary 

The key message of this book is:

**Western civilization developed six killer applications, which transformed it from a position of relative poverty to total global domination. This brought massive benefits in prosperity, power and health compared to their counterparts elsewhere.**

**Yet this period of Western dominance could be coming to an end. Societies in the West seem to have lost confidence in themselves. At the same time, the rest of the world seems to be learning from the centuries of Western success and applying the same _apps_ to their own societies.**

The questions this book answered:

**How did Western civilization become the dominant global force?**

  * Western nations have grown exponentially from relative poverty to world dominance over the past five centuries.

  * Western civilization developed six killer applications, which enabled it to dominate the rest of the world.

**How did the killer applications cause Western civilization to become so powerful?**

  * A spirit of ruthless competition, developed in Europe, was vital for the development of capitalism and Western power.

  * The development of scientific principles gave the West a crucial advantage over other societies.

  * The model of representative government based on property rights and the rule of law provides the strongest societies.

  * The development of modern medicine spread Western civilization and helped raise global life expectancy.

  * The formation of a consumer society radically raised the standard of living in the West.

  * Protestantism helped create a work ethic that encouraged people to work hard and save money.

**How will future developments alter the balance of power which has lasted for the last five centuries?**

  * Many other civilizations have tried to copy elements of Western success.

  * The future of Western dominance is uncertain, as Westerners have forgotten some of the vital killer applications.
---

### Niall Ferguson

Niall Ferguson is one of the leading and most controversial historians of his generation. He is the author of many bestsellers on financial and political history, including _The Ascent of Money, Empire_ and _War of the World._

In addition to his academic work he has written and presented many highly successful television series based on his books, including _Civilization._

