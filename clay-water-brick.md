---
id: 56660f65cb2fee0007000019
slug: clay-water-brick-en
published_date: 2015-12-09T00:00:00.000+00:00
author: Jessica Jackley
title: Clay Water Brick
subtitle: Finding Inspiration from Entrepreneurs Who Do the Most with the Least
main_color: E55535
text_color: B24229
---

# Clay Water Brick

_Finding Inspiration from Entrepreneurs Who Do the Most with the Least_

**Jessica Jackley**

_Clay Water Brick_ (2015) explores the author's unusual business career in connection with stories of successful micro-entrepreneurs all over the globe. These blinks reveal the strategies of entrepreneurs who make something out of nothing while making a difference in struggling communities.

---
### 1. What’s in it for me? An entrepreneurial way out of poverty. 

Turn on your TV and it won't be long before you're confronted by a charity ad featuring a tearful celebrity broadcasting from a region mired in poverty. Chances are you'll feel sad and you may give a donation, but that's it. You'll soon forget about it, the celebrity will go home and the poverty will remain.

So what can we do instead? 

Well, you can trust the thinking of an expert in the field. Someone who has spent years battling poverty in the third world. These blinks, based on the author's real-life experiences, show how poverty can be ended, for good. They show how instead of relying on traditional charity, we should look to local entrepreneurs to create wealth and progress.

In these blinks, you'll discover

  * how one man used his hands and the clay under his feet to get himself out of poverty;

  * how the author's platform survived someone embezzling $100,000 on it; and

  * why microlending could be the solution for global poverty.

### 2. While conventional charities are floundering, entrepreneurs in developing nations are making a difference. 

Designer suits, expensive cars, cash to burn: that's how most of us picture the life of a successful entrepreneur. But not all entrepreneurs are enjoying life in the fast lane. Some of the most ingenious, resourceful and innovative entrepreneurs live and work in the poorest areas of the world. 

In recent years, entrepreneurs from developing nations have become beacons of hope. Far from widening the divide between rich and poor, the entrepreneurial spirit could help us close it. 

Entrepreneurship may even be a far better solution to poverty than any other current method. Why?

Well, consider how the average charity works. A person donates a given amount of money knowing that it's for a good cause. They don't know how that money will be spent, who it helps or the quality of the charity's assistance. 

Individuals that want to feel more involved in a charity's activity may volunteer. The author herself volunteered in many organizations. But no matter whether she was helping out at a hospital, at a sports club, in a soup kitchen or a halfway house for teenage mothers — she couldn't help feeling like she wasn't making a lasting impact. 

Moreover, seeing the same people return every day for assistance with little motivation to change their own lives is disheartening to many volunteers. Charities, of course, are an indispensable part of society. But there are more ways to make a difference. 

As the saying goes: "Give a man a fish and you feed him for a day; teach a man to fish and you feed him for a lifetime." The author understood the significance of this after gaining firsthand experience in East Africa with Californian nonprofit Village Enterprise. After interviewing local small business owners about their lives and the positive impact of Village Enterprise's financial sponsorship, one thing was clear: 

These people had the ideas, motivation and aspirations to fight their way out of poverty. All they needed was a little financial support. This gave birth to a revolutionary way of improving conditions in developing communities. The following blinks will tell you more about how it happened.

### 3. Microlending is a powerful tool in the fight against poverty. 

A few billion people around the world live in poverty today. The global situation seems to worsen every time we turn on the news. In a world where so many people are struggling, what can we do to help? We can donate to charities and raise awareness, but it's clear that a lot of money is the only thing that will improve the lives of the poor. Or is it?

It's impossible to eliminate all poverty at once. But what if we focused on lifting individuals, families and communities out of vicious cycles, one at a time? This approach is far more realistic, and a much smaller amount of money is needed to make it work. How? Through _microloans_. 

Microlending is a system that emerged a few decades ago thanks to the work of Dr. Muhammad Yunus and his Grameen Bank in Bangladesh. By giving budding businesses small loans with low interest rates, Yunus assisted thousands of village entrepreneurs in starting their businesses. 

Grameen Bank has since been awarded the Nobel Peace Prize and has over 7.5 million borrowers today. Dozens of new organizations around the world have adopted Yunus's approach to _microfinance_ in the fight against poverty. Kiva, the author's microlending platform, is one of them. 

Drawing on her firsthand experience of witnessing poverty in Africa, the author developed Kiva and revolutionized microlending. Kiva connects entrepreneurs in developing nations with lenders in developed nations. Whether they need funding to purchase new equipment, farm animals or other investments in their business, entrepreneurs receive financial support from voluntary sponsors. 

The progress of their business is shared with the lenders, whose loans are paid back in manageable chunks. Since 2005, Kiva has distributed more than $600 million in microloans to entrepreneurs around the world.

### 4. Great businesses don’t need great resources, just a resourceful leader. 

The founder of a tech start-up in Silicon Valley and a craftsman selling building materials in an East African village may not have much in common at first glance. But, while there is a stark contrast between the resources they have access to, both share the same entrepreneurial spirit. 

No matter where or how you start out, it's your resourcefulness that makes you a strong entrepreneur. Patrick from Uganda started out with nothing at all. Having lost his family when rebels attacked his village, he escaped with his surviving brother to live with distant relatives. 

Patrick was an orphan with no education and no money. All he had was his bare hands and the ground beneath his feet. And those are exactly what he used to make a living for himself. How? By digging for clay to create bricks, which he could then sell to builders. 

His bricks were rough and poorly made at first. But it didn't take long before Patrick improved his technique and began to make small profits. He used his profit to buy new tools. After years of hard work, Patrick had gotten a small but prosperous business off the ground. As well as providing much-needed building supplies to the community, he was able to create a livelihood for himself, his brother and several employees. 

Patrick's business emerged from the bleakest years of his life. In this way, his story illustrates that there's no time like the present when it comes to starting a project. When the author founded Kiva, the only real resource she could rely on was her practical experience. She didn't have contacts or a reputation, and had just received an initial rejection from Stanford Business School. 

With no MBA and little disposable income, the author was still determined to carry out her vision. Her husband joined her and together they began work on the ambitious project. Kiva was born one year later after a handful of loans were brought together to build a primitive website. 

The author could have waited until she was in a better position to start a business but is glad she didn't. Looking back, she knows that waiting for the right moment to start Kiva might have prevented her from building the business altogether.

### 5. A clear mission will keep your venture on track no matter what. 

Starting your own business venture is rather like sailing out to sea. You may have a route planned, but it's inevitable that strong currents will pull you in different directions. In order to get yourself back on track, you've got to keep your destination firmly in mind. 

In the early stages of Kiva, the author was approached by two entrepreneurs with a similar idea. They were ambitious, had friends in high places and wanted to join forces. Their goals and values were a little different to the author's, but she agreed to cooperate. 

However, the partnership didn't last long. The founders of Kiva were being treated like inferiors, while the two entrepreneurs were determined to tap into new markets. The project was straying from its original goal: connecting entrepreneurs with the sponsors they needed to make a change in their community. 

Kiva broke off the cooperation with the two entrepreneurs and returned to their original course with great results. The two entrepreneurs continued in their direction, and ended up creating a rival platform that paled in comparison to Kiva. It was the author's clearly defined mission that helped her know when to stop and change track.

> _"Your mission is your identity and your guide. It tells the world and serves to remind you, exactly who you are and who you are not."_

### 6. Honesty, even when things don’t go according to plan, is what wins loyal followers. 

Whether it's posting that glowing graduation picture on Instagram or proudly presenting stunning quarterly figures to your investors, most of us can't resist the opportunity to show off our successes. Of course, we're not all successful all the time. We just don't share that side of the story. 

It's easy to see why: we don't want to look like failures, especially to those who support or follow us. Yet, the reality is that people don't place their trust in a successful exterior. It's honesty and transparency that attracts loyal and committed customers. 

Direct contact between lenders and entrepreneurs is a cornerstone of Kiva. Sponsors receive regular updates on the progress of the budding businesses they support. 

This transparency is what sets Kiva apart from the anonymous approach of conventional charities. The inspiration it offers users keeps them coming back to Kiva as sponsors and telling other people about the platform. 

Transparency also helped Kiva regain its footing after facing their first PR crisis. Kiva was informed that a partner organization in Uganda had embezzled over $100,000 in loans by creating fake profiles of borrowers. 

The money had disappeared and Kiva was at a loss as to how to handle the catastrophe. Management decided that their only option was to be completely open. Lenders were informed and received an apology from Kiva, who admitted they had been too trusting. 

Users were understanding, appreciating the honest response and declaring their continuing support of Kiva. In a dilemma that could have cost Kiva its existence, the business stuck to the truth and came out even stronger because of it.

> _"No one expects perfection, but everyone does (and should) demand honesty and will be inspired by resilience."_

### 7. Teamwork can revitalize a business and see it reach new heights. 

Building a new business is one of the toughest challenges you can take on. So why go it alone? Two heads are always better than one, and surrounding yourself with supportive team members will allow you to overcome even the trickiest obstacles. So don't be afraid to collaborate!

Clay, founder of a candy store in Honolulu, found strength in teamwork during difficult financial times. A passionate entrepreneur, Clay treated everyone that walked into his store like _ohana_ (the Hawaiian word for "family"). He had a loyal customer base, but he still struggled to make ends meet. 

Enter Clay's nephew Bronson, a young and enthusiastic business student. He convinced Clay to join forces with him to give the store a makeover. With the help of crowdfunding platform ProFunder, the duo raised over $50,000 from family, friends and fans, rewarding sponsors with exclusive tasting sessions and personalized treats. 

The campaign was a success, and Clay's store received a new interior and a powerful marketing strategy. Today, Uncle Clay's House of Pure Aloha enjoys a fantastic reputation and financial success. 

The author herself also benefits from the power of teamwork. But it took a little convincing before she warmed up to the idea.

Following the overnight success of her platform _Kiva_, the author needed several skilled helpers to keep up with demand. Together with her husband, she gathered trusted friends to form the core team of Kiva. This team was nothing less than a family, many of them having quit their jobs to dedicate themselves to the project full time. 

This was quite the shift for the author. Having worked on the project independently, Kiva was truly her "baby." Her energetic team was full of new ideas and suggestions, and the author was quite reluctant to take these on. 

But, soon enough, she saw that it was the team that gave life to her project. Each team member brought something special to Kiva: whether it was programming, branding expertise or invaluable business contacts. Bit by bit, the author gave up responsibilities and let her team do the things they did best. Kiva flourished as a result, and the author's team continues to grow.

> _"[...] having a community of supporters dedicated to the venture's success can mean the difference between surviving and truly thriving."_

### 8. Years of trial and error is what leads to sustainable success. 

When the first iPhone came out, it was a revolutionary piece of technology. But compared to the most recent model, it looks incredibly underdeveloped. That comes as no surprise — not even Apple gets it right the first time. And Apple is constantly improving the iPhone's technology. 

The best projects aren't the result of a stroke of genius. Rather, they're borne out of innovation and refinement over many years. 

That's something Shona learned in the early days of her business. The young mother was determined to design custom appliances to help her severely disabled daughter Shelly perform basic physical tasks. In Shona's homeland of South Africa, the range of equipment to assist the disabled was limited. 

This made simple activities like sitting upright at a table impossible for Shelly. Shona was determined to change this. 

With some experience as a sculptor under her belt, Shona began to develop primitive designs that offered makeshift solutions for Shelly's difficulties. With Shona's tireless work, these improvised contraptions gave way to equipment that changed her daughter's life. 

Word began to spread about Shona's expertise and other caregivers with similar needs approached her. It wasn't long before Shona took her skills to a professional level, leading to the beginning of her enterprise _Shonaquip_. 

Today, Shona is the employer of dozens of technicians and therapists, and her intuitively designed inventions have improved the lives of 70,000 disabled children. Having started from scratch and worked through countless versions of designs, Shona has years of expertise that provide a solid foundation for her business as it grows and prospers.

> _"Many masterpieces are built over time by trial and error, by steps forward and backward, by constant iteration."_

### 9. Taking risks opens new doors for your business. 

While every entrepreneur's journey is full of unique challenges, there's one tough decision they've all faced: the decision to risk it all and start a business. By investing time, money and effort into starting a business, entrepreneurs voluntarily dive headfirst into the unknown. 

Take Katherine, a student from a Village Enterprise workshop in Tororo, Uganda. After learning basic business techniques, she felt pretty sure that selling fish was one of the most promising business strategies. Demand was high, and she decided to give it a try herself by buying fish from a local middleman. 

Her profits were limited because of the high price she paid for the fish. Katherine knew that if she wanted to really make it, she'd have to start buying her fish closer to the source. This meant taking the expensive and dangerous journey alone out to the lake by bus. A risky move for Katherine, but one that gave her a shot at success. 

With her $100 sponsorship from Village Enterprise, Katherine was able to safely take the bus out and return with a load of cheap and fresh fish. Her profit was maximized, and she had found a new strategy that would allow her business to grow and thrive. Without taking that first risk, Katherine wouldn't be running her business today. 

Taking risks and succeeding in those early stages of business also encourages entrepreneurs to keep taking calculated risks as their business evolves. Most entrepreneurs find that they exceed their own expectations to push their business ventures further than they had ever imagined.

> _"Dream — and then choose to believe in your own potential and help create the future you're dreaming of. The world needs you."_

### 10. Final summary 

The key message in this book:

**Microlending has allowed aspiring men and women in developing communities to not only change the way we see entrepreneurship, but make a difference in the global fight against poverty. With integrity, risk-taking, persistence, transparency and, above all, resourcefulness, budding businesses can achieve success against the odds.**

Actionable advice: 

**Admit your mistakes.**

Whether a project failed, a deadline was missed or big losses were incurred, all businesses mess up sometimes. If you're responsible for a mistake, there's no sense in hiding it. You'll only make the burden bigger for yourself! Instead, admit that you took a wrong turn, apologize and ask for help. This is the only way that your business will be able to get back on track. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jessica Jackley

Jessica Jackley is a social entrepreneur and founder of the unique and successful micro-lending platform Kiva. With a focus on financial inclusion, the sharing economy and social justice, Jackley has played instrumental roles in a number of other enterprises. She holds an MBA from the Stanford Business School.

