---
id: 5a097980b238e100065bcc98
slug: clean-en
published_date: 2017-11-16T00:00:00.000+00:00
author: Alejandro Junger and Amely Greeven
title: Clean
subtitle: The Revolutionary Program to Restore the Body's Natural Ability to Heal Itself
main_color: 8CC0F5
text_color: 51708F
---

# Clean

_The Revolutionary Program to Restore the Body's Natural Ability to Heal Itself_

**Alejandro Junger and Amely Greeven**

_Clean_ (2009) puts our bodies and environment under the microscope. It reveals just how many toxins we're constantly exposing ourselves to, which include dangerous toxins in our food and harmful emissions, coming from both indoors and out. Fortunately, we _can_ detoxify. So find out what steps you need to take to rid your body of the unhealthy residue that is currently increasing your chances for all sorts of cancers and diseases.

---
### 1. What’s in it for me? Cleanse your way to optimal living. 

There's a good chance that you're having a coffee to keep awake while reading this. Perhaps you're even on your third one. Now, what if you could reap the energizing benefits of coffee by removing toxins from — not adding them to — your body (because the sad story is that, yes, caffeine is a toxin). This is what these blinks will offer you: a way to achieve good health by cleansing your system.

You'll learn about the Clean Program, which teaches you how to change your lifestyle so that your body can detoxify and heal itself from the wear and tear of everyday life.

And you'll learn about the different toxins you expose yourself to daily, as well as how to go into a deep cleansing mode that expels these poisonous substances from your body. From the ideal diet to an array of exercises, you'll get the tools you need to start cleaning up your life.

You'll also learn

  * that most cleaners aren't actually all that clean;

  * how many times a day you'd be going to the toilet if your diet was cleaner; and

  * what radical reorientation the Clean Program requires.

### 2. Detoxing can help restore the functions of our body that are being damaged by toxins. 

If you're not feeling well, you might go to the drugstore to get some meds. Or if it's a really heavy cold, you might head to the doctor and pick up a prescription for something stronger.

But what if the problem is less straightforward than a cold or flu? What if the problem has to do with how your body is functioning?

Unfortunately, these days there's an abundance of unnatural environmental and dietary toxins that are damaging your body's ability to properly function and heal.

At virtually every turn, we're bombarded with pesticides on our food and dangerous chemical cocktails in our cleaning products.

While companies have been quickly manufacturing stronger chemicals to be used for an increasing range of purposes, our bodies haven't had the time to develop a resistance to these toxins. In just the last century alone, we've dramatically changed the way we eat and live, and it's taken a toll on our body's defense systems.

Toxicity comes with its fair share of symptoms, such as recurring tiredness and bloating, but these are commonly written off by doctors as signs of our body's normal wear and tear. Most doctors will hesitate to do anything unless symptoms are severe, and in those cases their advice will be to ingest powerful drugs that add to the toxic residue in your body.

But it doesn't have to be this way. There are viable alternatives.

Throughout the ages, healers have practiced regular deep-clean detoxes. In particular, Indian and Chinese cultures have long been proponents of detoxes for both physical and mental well-being. But in the West, today's mainstream medical community has turned their attention away from these traditional practices and toward molecular manipulation.

You don't need a doctor to start a detox program and reactivate your body's ability to heal and regenerate. But before we get into the process, let's take a closer look at the toxins you need to target.

### 3. Our exposure to global toxicity is made up of four layers. 

At his cardiology clinic, in New York, the author, Dr. Alejandro Junger, noticed a worrying trend: many patients were physically, mentally and emotionally off-balance. Symptoms of bloating, tiredness, sneezing and moodiness were common, and it made Dr. Junger wonder whether it was all due to environmental toxins.

Everyone on the planet today is infected with hundreds of toxic chemicals that have only come into existence during the twentieth century.

But to really understand how these toxins reach us, it helps to consider the four different environments your body comes into contact with every day. Dr. Junger refers to these as our _four layers of skin_.

The _first layer_ is our body's natural defense — our actual skin. The toxins that enter through this layer include chemicals in cosmetics and toiletries, such as the aluminum that's found in many deodorants. When we rub chemicals onto our skin, we force toxins into our bloodstream through the very pores that are designed to keep them out!

Our _second skin_ is our clothes. These often contain dyes, perfumes and residue from chemical cleaning products. Fabrics are more heavily treated with pesticides than our foods. In fact, 25 percent of the world's pesticides are used on cotton. Not only that, _perchloroethylene_ is a common ingredient in dry cleaning that has been found to cause liver, kidney and neurological damage.

Our _third skin_ is our indoor environment, including our workplace and home. Now, you might think your indoor space is cleaner than the outdoors, but the Environmental Protection Agency has actually suggested otherwise due to the toxic emissions from paint, insulation materials and your furniture.

Have you ever noticed the strong smell of a new shower curtain? That's likely from PVC plastic, which is one of the most toxic goods on the market.

The fourth and final skin is the outside world. While you might go outside for some "fresh air," research shows that even a few hours' exposure to heavy air pollution can raise the risk of heart attack. Power lines, mobile phones and computers all emit radiation that has been linked to the same symptoms caused by chemical toxins.

### 4. Processed foods are more harmful than nutritious, but there are ways to eat healthily. 

Who among us doesn't love to gather around a grill on a weekend afternoon to enjoy some barbecued meat or veg? History shows that food has always had the power to bring people together, but the unhealthiness of much of that food is a relatively recent development.

Modern processed foods, in particular, are downright poisonous, all in the name of "safety" and turning a profit through convenience and making the food look as good as possible.

Take spinach, for example: to kill bacteria, the healthy green vegetable is shot with X-rays that also destroy much of its nutrients. Food is also given unnatural, hydrogenated fats and oils so that it'll last longer on the shelf, but consuming these fats damages your body's cells by altering key proteins that control cellular growth.

Even the tap water is dangerous these days. The fluoride that's added to the water is meant to strengthen our teeth, but recent studies have linked fluoride to thyroid and kidney disease as well as neurological problems that can affect a person's IQ.

Packaging is a concern as well, as plastic chemical compounds called _phthalates_ can enter foods and build up in our bodies. Over time, phthalates can interfere with hormones and increase the risk for breast, thyroid and prostate cancer.

Thankfully, there are better ways to eat than indulging in processed foods, and you can start by looking toward your local food sources and eating what's in season.

If you look at the areas of the world with the highest life expectancies, you'll notice a trend in their eating habits.

In one mountain village in Sardinia, for example, an impressive percentage of people reach 100 years of age. People in this village and others like it eat a diet high in raw, seasonal fruits and vegetables, which are grown without the aid of chemicals. These people are in no hurry, either. They take their time to prepare their food and enjoy the ritual of cooking. For them, it's a pleasure, not a chore, and they even chew their food ten times longer than the average city dweller.

### 5. The buildup of environmental and dietary toxins in the body leads to puffiness and constipation. 

When toxic molecules enter a body, they don't just affect one area; they cause a chain reaction that spreads far and wide.

For instance, consider _amma_, the substance that, according to the Ayurvedic tradition of medicine in India, accumulates after prolonged exposure to toxins. A buildup of amma is a good warning sign that a detox is in order. Ayurvedic tradition believes in the importance of eating the right foods to keep your body in a healthy balance. When this balance is lost, that's when disease will arise.

So it's important to notice amma when it appears, which you can do by testing the elasticity of your face. When you pinch your face and tug on the skin, it should snap right back into place. But if you notice your face looking puffy, with pimples, visible pores, dark circles under the eyes and small depressions like the skin of an orange — this is a sign of amma and it should not be ignored.

You might think sagging skin is a normal sign of aging, but if you look at the 100-year-olds in those healthy communities, most of them have been able to maintain taut skin throughout their lives.

But toxic buildup doesn't just affect our appearance, either. Another symptom to beware of is chronic constipation.

Unsurprisingly, constipation is one of the biggest health complaints in Western medicine. For proof, you need only consider the size of the market for laxatives.

But the reality is worse than you think, since what is considered normal is itself unnatural. A healthy pattern is to use the toilet after every meal. But since our diet is so heavy on difficult-to-digest foods like wheat and dairy, our digestive system gets clogged up and the toxins remain in our body. All of this can cause painful constipation.

> _"Toxic thoughts and toxic relationships are also pollutants because they disturb the peace and normal body functioning we were born to have."_

### 6. Before starting the Clean Program and changing your diet, you’ll need to prepare your mind and environment. 

We can easily imagine what it's like for an external cut on our skin to heal. The skin cells around the cut divide and start covering up the area until it's completely healed over. It's good to keep this in mind and apply it to the healing that needs to take place inside your body.

This is what the Clean Program does — it cleans out the wounds in your body so that it can start the healing process.

Here's what happens during the _21-Day Clean Program_ :

The first rule is to change your diet and eat less difficult-to-digest food, which means less wheat and dairy, fewer solid foods and more juices. This will give your body more energy to heal and detoxify.

The next rule is to maintain a daily schedule that contains 12 straight hours when you consume nothing at all. This ensures that your body can _enter_ detoxification mode, which happens around eight hours after you've finished eating your last meal, and _complete_ detoxification, which takes an additional four hours.

Before you start these 21 days, you also need to remove all the toxins from your living environment. And if you really want the best possible results, you must prepare your mind, as well as a good schedule and your kitchen.

You should be aware that the first three days will be the toughest, so you'll need to be motivated. Keep in mind how the average person looks — not too good, right? Indeed, half of all Americans can now expect to get cancer in their lifetime. If you want to be in the half that doesn't, you need to take action.

For your schedule, plan to start your 21 days when there aren't any other big changes going on in your life. So don't start this if you're starting a new job, moving into a new place or going through a breakup.

Finally, make sure your kitchen is set up with these three essentials: a water filtration system, a high-speed blender and a powerful juicer.

> Be prepared: changing your eating and drinking habits may make you moody or irritable to begin with.

### 7. During the Clean Program, consume only approved recipes and supplements. 

Don't expect the Clean Program to be easy. To successfully get through these three weeks, you're going to need to rethink your relationship to food and hunger and recalibrate your eating habits.

During the Clean Program, each of the 21 days will consist of one solid meal and two liquid meals, such as a shake, juice or soup.

The liquid meals will cover breakfast and dinner; you'll have the solid meal for lunch. This way, you'll have more energy overnight during the consecutive 12 hours of fasting.

For all three meals, there are Clean Program recipes. For the liquid meals, there are choices like a mango coconut milkshake, which contains nutrients that are easy to absorb and will change your metabolism, so that you'll feel less hungry.

Chances are, at some time you'll feel the urge to snack between meals. But before you give in to temptation, ask yourself if you're _really_ hungry or are merely looking to food as a comfort or distraction from something else. If it's real hunger, snack on raw, fresh foods like vegetables, nuts or green smoothies, and stay away from simple carbohydrates like rice cakes.

To get the program's maximum benefits, add daily supplements like fiber, probiotics and antimicrobials.

Natural fiber supplements that contain flax seeds or psyllium husks will bind to toxins in your intestines and help remove them before they can get reabsorbed. And there are hundreds of different species of helpful intestinal bacteria that can be reintroduced with probiotic supplements. The best supplements are ones that contain a minimum of 15 billion organisms per dose.

Meanwhile, antimicrobials such as oregano oil or garlic cloves will help kill any bad bacteria.

In the last blink, we'll take a look at some more helpful ways you can make the most of your program.

> _"Olive oil taken at night will lubricate your intestines, kill germs and enhance fat burning."_

### 8. Enhance your body’s detoxification process with special activities and exercise. 

There are a number of complementary activities you can do to support yourself during the Clean Program, and while some are simple, none are unimportant. For example, make sure you get enough sleep, and don't forget to stretch, relax and be kind to yourself during the 21 days.

There are also important activities that can enhance the removal of toxins from your body.

One of the most effective is to undergo colonic hydrotherapy. This requires the assistance of a specially trained therapist who pumps low-pressure streams of water into the colon to aid the removal of waste.

You can also increase the amount of toxins released through your sweat by using saunas. Infrared saunas are especially effective as they involve powerful heat waves that activate the fat molecules deep under the skin's surface so that more toxins are released.

Breathing exercises can also assist your body in getting rid of carbon dioxide and reducing the acidity in your blood. A good exercise is to concentrate on taking full, deep breaths while picturing your mind and nervous system being swept clean with each breath.

One of the best ways to enhance all these methods is to stay physically active with regular exercise during the Clean Program. But remember: don't overdo it.

Any form of exercise can be great for promoting regular bowel movements, getting a healthy sweat going and inducing deep breaths. However, there are certain exercises that really take the detoxification process to the next level.

Jumping, skipping and trampoline exercises promote better lymph circulation, which is particularly useful since lymph fluids carry toxins through the lymph nodes, which filter out damaged cells and bacteria.

But pay attention to your energy levels, which can get depleted during the Clean Program, especially during the first week. Avoid intense training during these early days and whenever you're feeling weak. It's best to simply spend at least twenty minutes every day being mobile and active, whether it's stretching, dancing or walking.

So now you have the basic information necessary to achieve a healthier body and mind. The next step is for you to commit yourself to the program.

> _"Go to bed early during your cleanse, because sleep is when much of the important repair work is done."_

### 9. Final summary 

The key message in this book:

**Modern society in the Western world is overrun with obesity, depression, constipation and heart disease, all of which are made worse by the dietary and environmental toxins that can be found all around us. The Clean Program is designed to help you to detox so that your body and mind will have a fighting chance to heal and rejuvenate.**

Actionable advice:

**Stay away from chemical cosmetics.**

Don't use any cosmetics that you would be afraid to get in your mouth. By rubbing something on your skin, you're introducing it into your bloodstream in much the same way as you would by eating it. So if it contains nasty, unsafe chemicals, don't put it on your body!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Deep Nutrition_** **by Catherine Shanahan, M.D., Luke Shanahan**

_Deep Nutrition_ (2008) is about modern diets and how they're making people sick. These blinks explain the danger of industrially produced food, what it's doing to our bodies and how we can return to an earlier way of eating that will keep us healthier for years to come.
---

### Alejandro Junger and Amely Greeven

Alejandro Junger, MD, is a _New York Times_ best-selling author whose regular job is at a cardiologist clinic in New York. After finishing his medical training, Junger studied Eastern Medicine in India and became an expert in adrenal fatigue and the mental and physical effects of chronic stress.

Amely Greeven is a journalist, copywriter and health blogger whose articles have appeared in a variety of magazines, including _Vogue_, _Harper's Bazaar_ and _W_.

