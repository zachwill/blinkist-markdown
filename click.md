---
id: 533180dc3862370007310000
slug: click-en
published_date: 2014-03-25T15:15:00.000+00:00
author: Rom and Ori Brafman
title: Click
subtitle: None
main_color: E5302E
text_color: B22624
---

# Click

_None_

**Rom and Ori Brafman**

_Click_ (2010) delves into the miraculous topic of quick-set intimacy and explores what's at work when we're instantly drawn to some person or thing. It examines how and why clicking makes our life more meaningful and outlines the (very ordinary) factors that can make such "magical" connections more likely.

---
### 1. What’s in it for me? Learn how to instantly connect with others. 

We all know the feeling: we meet someone for the very first time at a party, or perhaps in line at the post office and instantly become fast friends.

We _click_.

Why does this happen? In _Click_, brothers Ori and Rom Brafman investigate the forces behind this phenomenon and suggest concrete ways that we can bring about instantaneous connections, or what the authors call _quick-set intimacy_.

The following blinks give you many key insights into the apparently miraculous dynamics of quick-set intimacy. For example, you'll find out why we experience "click moments" as meaningful and why such meaningful connections matter greatly to our wellbeing (hint: depression is very rare in close-knit communities).

You'll learn why meeting someone for the first time can make you high and how those very first moments can lend a magical quality to the rest of a relationship.

You'll find out why a hostage negotiator would tell a hostage taker about the day his mother died.

And you'll even discover why sharing a dorm could spur a consistently bad basketball team on to an incredible winning streak.

### 2. Clicking is when you suddenly feel a highly pleasurable, almost “magical" connection. 

Consider this familiar scene:

Two strangers happen to meet somewhere — perhaps it's on a train, during a long journey. One of them notices the other is reading a book by their favorite writer and can't resist mentioning it. Instantly, the pair are drawn to each other and as they start to chat about this and that, each of them begins to feel as if they've known the other person for their entire life.

This is _clicking_ and it's one of the most enjoyable things you can experience.

Why?

One main reason is that we experience clicking as a kind of euphoria.

In one experiment, neuroscientists scanned the brains of people who'd clicked in a particularly romantic way: they'd fallen in love. The scans showed that those areas of the brain usually associated with pleasurable experiences were exceptionally active. In fact, the level of activity that the scientists observed commonly indicates a state of euphoria — a level of pleasure typically triggered by drugs like cocaine.

Another reason is that we tend to think of clicking as meaningful. Indeed, we often even experience it as almost "magical."

When one psychologist asked a diverse group of people to recall an almost magical experience, out of all the events of their very different lives most people named precisely those moments when they'd experienced an instant connection with someone.

One woman, for instance, described how she and a stranger had locked eyes and were struck by an immediate sense of shared intimacy. Within no time, they'd become a couple.

Furthermore, nearly all the participants used the same attributes to describe those moments; they called them energizing, thrilling, special or euphoric. Even when the same question was posed to an entirely different group, they used the exact same adjectives.

So clicking can make us wildly happy and can even cause us to believe that, in those moments when our lives intersect with those of others, we are experiencing something out of the ordinary.

### 3. Clicking makes relationships special. 

What's the one crucial thing that leads to a happy marriage? Is it that both people like each other a great deal? Or is it that they share a lot of interests?

Or perhaps there's such a thing as love at first sight?

When psychologists investigated this very topic, they discovered a surprising correlation: those married couples who'd experienced an immediate and intense connection when they first met had a more passionate marriage.

In the study, investigators compared couples with different romantic histories. First, there were those couples who were friends before beginning a romantic relationship. Second, there were those who'd been through a traditional courtship period. Lastly, there were those couples who'd fallen madly in love the moment they met.

What they found was that, even after 25 years of marriage, a couple which had experienced love at first sight continued to differ from each other noticeably, whereas in the first two groups the spouses were much more similar to each other.

However, the couples whose relationship began with a passionate first encounter also continued to show the highest levels of passion, even after more than two decades.

Perhaps one reason for this is that, in the context of a "magical" first encounter, an entire relationship will appear as something special.

Consider Nadja and Paul, for example. These two first met at work and from the outset both sensed a special quality to their meeting and interaction. So much so, in fact, that after just three days Paul asked Nadja to marry him.

15 years later, even though their relationship was not without its challenges, the spark that had initially attracted them to each other still lends that magical quality to their relationship and keeps them striving to make the relationship work.

As these examples show, clicking can lend a special quality to the relationships that it initiates. Indeed, the very passion that made you high when you first met will continue to spark your mutual connection, whether it's a friendship or a romantic relationship.

### 4. We perform better in the company of people we click with. 

Surprisingly, the success of a string quartet depends overwhelmingly on one sole factor — that the musicians click with each other. But why would clicking play such an important part in the successful interaction of trained professionals?

One reason is that a group who clicks communicates more effectively, which, in turn, can result in increased proficiency.

Consider those people in a close relationship. They appear to understand one another without any effort. For instance, they need to use fewer words to communicate effectively and to make themselves fully understood — which means less misunderstandings.

For a string quartet, the main benefit of clicking in this way is that the players will be able to recognize musical cues without much effort at all.

Another benefit is that those who click are able to discuss difficult matters in a positive, constructive way. For example, a string quartet must often discuss how they are to interpret a piece of music. If the group clicks, any conflicts that arise in such a discussion aren't usually about asserting one's position in the group but, rather, the music.

In contrast, less well-attuned quartets don't resolve conflicts over musical interpretation. Instead, everyone sticks to his own version. The result? A much less cohesive performance.

A further benefit of a clicking team is that it's much more likely to persevere through difficult times and one reason for this is that its members are simply more supportive of each other. For example, the members of a well-attuned string quartet tend to root for their fellow performers and care about their well-being.

Finally, a team that clicks is also more creative and able to make unconventional, even daring decisions. In one experiment, MBA students in groups which clicked competed against groups that didn't. Each group was instructed to perform tasks that were unrelated to their field of study and unfamiliar to them. The result? The groups that clicked outperformed the other groups by a magnitude of 20 to 70 percent.

### 5. If you make yourself vulnerable to others, you're more likely to click with them. 

Why would a hostage negotiator tell a hostile captor about the day his own mother died? It sounds like a plot point from a movie, but it actually happened.

Moreover, it was exactly this revelation that marked a turning point in the negotiation.

Why?

If you deliberately make yourself vulnerable by revealing your emotions, weaknesses and fears, you signal to other people that you trust them. In turn, those people will be more likely to trust you and instinctively reciprocate by making themselves vulnerable too.

The openness that this promotes can lead to an intimate connection being established. When you open up to someone, they'll usually interpret this as an invitation to intensify the relationship. If they open up too, this means that they would also like to deepen the relationship.

In fact, this works so consistently that it's possible to make absolute strangers click with each other by gently prompting them to reveal intimate information to each other — like their deepest secrets or fears. In one study, students who'd never met before were instructed to interview each other, posing a series of probing and increasingly intimate questions.

The result?

Many of the participants actually clicked because of the experience and in fact developed friendships with each other that lasted throughout the year.

This mechanism in which one person's openness and vulnerability leads another person to open up is precisely the same strategy that the hostage negotiator uses.

To gain an influence on the tense, unstable hostage taker, the negotiator tries to connect with him. He waits for exactly the right moment to share a difficult situation from his own life with the captor. And once he's earned the captor's trust, they both click and it becomes possible to de-escalate the situation.

### 6. When it comes to clicking, spatial proximity also plays a part. 

For a long time, winning a basketball championship seemed out of reach for the Florida Gators. Their prospects seemed even worse when all the best players left.

But that was the exact moment when their luck began to change: four new teammates played together so well that the Gators won 17 consecutive games.

How? Those players who'd clicked so well on the court also happened to be roommates.

Indeed, the closer that you physically are to a person, the greater the chance that you'll become friends.

Consider this study: Cadets in a police academy were assigned seats in alphabetical order. Later, when they were asked to name those peers whom they'd formed close relationships with, nine out of ten cadets named the person sitting next to them.

As this shows, just being physically near to another person is a better predictor of social bonding than other criteria like age or shared interests.

But why is this? Quite simply, being near to someone facilitates spontaneous communication and that communication leads to bonding.

The nearer you are to someone, the more opportunities there are for you to strike up a conversation. And with every conversation you have, you learn something about each other. Furthermore, each time you talk, your perspectives align ever closer.

The effect of proximity on clicking can also be seen in the phenomenon in which many people perceive familiar people as more attractive.

In one experiment, four equally attractive women were instructed to attend a psychology course in a large lecture hall. In the meantime, they were asked to avoid all interactions with their fellow students.

One woman was to attend every session and the others were to attend ten, five or none of them.

When the other students were asked to rate the women's attractiveness, very few of them recalled even seeing the women, but most of those students preferred the women who'd attended the majority of the classes.

### 7. In order to captivate an audience, you have to be attuned to your spectators and fully immersed in your performance. 

In a casting session, up to a hundred actors read the same lines. Most of them are very eager, many are talented, yet soon enough all the auditions start to blend together.

Then one actor appears and the atmosphere changes. Suddenly, for some reason, the lines sound just right.

Whether it's in order to win an audition or shine as a musician, an impressive performance is possible only when a person is fully immersed in the task at hand. Or, in the words of psychologists investigating exceptional performance, in a "flow state": a state of total yet effortless concentration which can be reached only by experts.

Furthermore, captivating an audience means being fully engaged not only with your performance but also with the spectators themselves. If a performer isn't attuned to the mood of an audience, then no matter how technically brilliant the performance, clicking becomes impossible.

Just think of a great stand-up comedian whose act usually "kills." If on one night the spectators are upset, then he'd better read and respond to their cues or he won't make them laugh.

If a stage performer is both in a flow state _and_ fully engaged with the audience, then his verve will spread through the audience like a virus. This phenomenon is an effect of a "mirroring" process, which is mediated by mirror neurons in the brain.

For example, when you observe a person who's in great pain, this will activate certain cells in the region of your brain associated with pain. The result is that you'll most likely feel a mild pain, although you aren't actually physically hurt.

The same kind of neurological contagion occurs when a performer is in a flow state. Like that performer, the audience will soon find themselves deeply immersed in the performance.

### 8. We’re more willing to connect with those who are similar to us. 

We'd all like to believe that we're charitable people, willing to donate to any worthwhile cause.

But consider this experiment: Many women are approached by someone raising funds for a charity and respond by donating a certain amount. Then the fund raiser wears a name tag bearing the same name as the women she approaches. What happens? The donations double.

This is because we tend to prefer people similar to ourselves. Indeed, we often generally rate such people as more attractive and, in one experiment, students who were led to believe that a certain stranger shared their worldview found that person highly attractive.

This similarity bias also extends to other positive attributes. In the previous example, the students perceived the stranger as not only attractive but also intelligent, knowledgeable and moral.

Such similarities can lead us to invest in those relationships based on them. In one experiment, participants who were asked by a student to assess a paper she'd written were twice as likely to agree to do it if they believed that the student shared the same birthday.

So what's behind this phenomenon?

We tend to think of and treat those who share traits with us as members of our "in-group" — or, in other words, as _family_. That is, we usually associate the presence of similar traits with familial relations. Because family members are usually those who care about us the most and because we share many characteristics with our families — like curly hair, freckles, myopia — those people who have traits in common with us stand a much better chance of becoming our close friends.

So if you want to hit it off with someone, you should focus on your similarities or shared traits and characteristics. The same goes for nailing a job interview: try to subtly bring up an interest you have in common with the interviewer. And, of course, if you want people to give money, tell them you have the same name!

### 9. Belonging to a close community and overcoming adversity together can facilitate clicking. 

In their sweat lodge ceremonies, Native Americans huddle in a tiny hut and endure the overwhelming heat together.

It doesn't take long for them to connect; they begin by sharing stories of their lives and soon many of them become emotional and cry.

But why does this activity prompt such intense and intimate connection?

One reason is that people are brought together by shared adversity. Having to reckon with a common enemy can bring out a sense of camaraderie — as, for example, when colleagues join together in their dislike of a sadistic boss.

Furthermore, by enduring and surviving hardships together people can share an intense emotional experience in which they make themselves vulnerable by forgetting about being civil or maintaining the emotional barriers, like the public personas they use to protect themselves in everyday life.

Such closeness between people is the fabric of community and if a community is clearly defined then it's more likely to click. This is because the well-defined roles and parameters of those communities make such instant connections between people possible.

For instance, in a club or tribe, members are able to let down their defenses and make themselves vulnerable and therefore assume that any secrets they've shared are safe within the group. This goes some way toward explaining the outrage that occurs when a member reveals secret information to the general public — whether it's by outing a formerly closeted gay person or leaking to the tabloids a compromising photograph from an exclusive private party.

Also, in the meetings of a clearly defined community like a club or clan, members are constrained or encouraged to be physically close to each other on a regular basis. And, finally, being part of the same group generates a sense of similarity and belonging. Both of these factors are fundamental to the possibility of making instant connections — to clicking.

### 10. Some people have traits that make them more prone to click with others. 

Why are some people always in the center of a group? Why is it that when these people take a short weekend trip, they return home with a throng of new friends? You might reasonably wonder what makes them so attractive. Surely it's not just their looks. Right?

It's not. Such people are simply better attuned to whatever social situation they find themselves in, in two specific ways:

First, they're more inclined to notice social cues and act appropriately on them. Because such people are determined to be pleasant to others and to act appropriately in social situations, they've developed a heightened sensitivity toward the moods of other people and toward a situation's social context.

For example, if such a person is enjoying an intimate dinner in a romantic restaurant, she will easily correctly identify the atmosphere — the situation's social context — and adapt her voice level to fit with the environment.

Such people are also able to immediately notice if, in conversation, their companion becomes bored — and they'll change the topic without delay.

The second way that these people are more sensitive to social context is that they're more aware of their self-presentation and more adept at adjusting it to the needs of others. As such people are eager to please, they are inclined to monitor their own body language and emotional communication so that they can adjust it whenever necessary.

So whether their companion is in the mood to spend time with a gregarious person or a calm and quiet one, an aloof person or a caring and attentive friend, the click-prone person will modulate their behavior to complement the other's behavior and to give them what they want.

As social chameleons, such people can deal adeptly with a diverse array of characters, make everyone feel at ease and can even mediate between incompatible people.

### 11. Final summary 

The key message in this book:

**Though it feels magical when we instantly connect to someone or something, there are distinctive factors which are the basis of such "click" moments — including proximity, openness and shared traits. When we click with someone, this can lend a special quality to the relationship that will characterize it for the duration. Also, teams that click are much more productive than teams that don't.**

Actionable advice:

**Find some common ground.**

When you want to hit it off with someone, focus on the similarities you share. Typically, he or she'll find you much more attractive when they find out that you both share distinctive traits.

**Bring a team together to improve their performance.**

To improve a team's performance, you can put them in a position where they must face a big challenge together and let them compete against another team. This will enhance their sense of shared membership and belonging to the same, distinctive group — which makes it more likely that they'll click.
---

### Rom and Ori Brafman

Together, brothers Ori and Rom Brafman have authored two New York Times bestsellers — _Sway_ and _Click_. When not writing, Ori Brafman is an organizational business consultant and the co-founder of a network fostering peace and development projects. Psychologist Rom Brafman won awards for excellence in teaching and promoting positive human growth. He has a private practice in Palo Alto.

