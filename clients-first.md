---
id: 56cdb5cf74766a000700009a
slug: clients-first-en
published_date: 2016-02-26T00:00:00.000+00:00
author: Joseph Callaway and JoAnn Callaway
title: Clients First
subtitle: The Two Word Miracle
main_color: EBE850
text_color: 6B6A24
---

# Clients First

_The Two Word Miracle_

**Joseph Callaway and JoAnn Callaway**

_Clients First_ (2013) reveals how honesty, competence and compassion can become the keys to great success in your company by creating strong and trusting relationships with clients. The authors offer some excellent insights drawn from their own experience, having gone from flat broke to wildly successful real estate agents in just a few years.

---
### 1. What’s in it for me? Get in front by putting others before you. 

Just about everyone has heard the tired old phrase "the customer is always right." But despite being such a widespread motto, it isn't really true anymore. A far more appropriate philosophy for the modern world would be "the customer is always _first_."

But what does this mean in practice? 

These blinks, based on 122 simple instructions by a hugely successful real estate couple, show you why you need to focus — at all times — on the emotions, needs and desires of your clients. Instead of giving clients advice based on what you want, try basing your strategies and decisions on their wishes. It could very well give your business a new lease on life.

In these blinks, you'll discover

  * why the authors increased their sales by advising clients not to buy from them;

  * why customer loyalty can help carry you through a recession; and

  * why being a success in business depends on maintaining your desire to learn.

### 2. If you want your clients to trust you, you have to stay honest with them. 

Whether you're a high-flying consultant or a shopkeeper, you can be sure that good business comes from getting customers or clients to trust you. Of course, this is easier said than done. We all know _why_ we should create stronger bonds with customers, but do we know _how_?

In the end, it's not so complicated. By focusing on the three areas of _honesty, competence_ and _caring,_ you'll give your business the foundation it needs for a great customer-brand relationship. So, let's get started with the first of them: honesty. 

Honesty isn't just about telling your clients the truth, but telling the truth even when it might go _against_ your interests. But a little white lie never hurt anybody, did it? Well, let's dig a little deeper into why honesty is so necessary using the example of the real estate business. 

When it comes to real estate purchases, clients are making big financial _and_ emotional decisions. They're deciding where they and their family will live, and how much they're willing to pay for it. It's no wonder that clients often get overwhelmed.

You might think that the best real estate agents would be those that prey on these moments of weakness, but the truth is quite the opposite. Agents who can keep their clients calm and composed with honest statements are the agents who will benefit from lasting, trusting relationships.

In their real estate work, the authors met the Brown family. This family, though not very well off, were eager and determined to purchase the house being sold by the Smith family. The authors could have closed the deal with the Browns in a heartbeat; but knowing that the mortgage would cripple them financially, the authors told the families the truth and advised against the sale. 

The result? Both the Smiths and the Browns became loyal clients, seeing that the authors really did have the customers' interests at heart. And they made sure to recommend the authors to all their friends, too!

### 3. Only companies that demonstrate competence at every stage will win clients over. 

Honesty is the best policy, but it's not the only approach you should implement when dealing with customers. The second essential aspect of putting customers first is _competence_. Competence simply means showing the customer that you're an expert at what you do.

But competence isn't about working until you're the best and stopping. In order to demonstrate competence, you have to embrace a process of continuous learning and development. To become a great real estate agent, you have to be ready to learn non-stop. 

Since they were starting out as amateurs, the authors' first real estate jobs were complete disasters. This could easily have pushed them toward a completely different career path, but they were determined not to give up. 

The authors began attending conferences and seminars, often up to ten times a year. They subscribed to trade magazines and even used their commuting time to listen to educational CDs; it was this perseverance that gave them the competence to win over an army of loyal clients. Now it was time to ensure that their whole team demonstrated the same sort of competence, too. 

The authors put time and effort into assembling a top-notch customer service team, where members knew their roles inside out. This way, their clients would witness competence in action at every step of the process. But what does this look like?

Well, when a client comes in, they first talk to Martin. He arranges a time for Ed to visit their house and measure it. Next, Judy will stage their house and Aaron will photograph it. Donna oversees and open house and eventually when the house receives an offer, she contacts Joseph and JoAnn, the authors, to close the deal.

> Some say it takes 2,000 hours to become a hairdresser and only 90 hours to become a real estate agent.

### 4. Demonstrate care for your client by understanding where their interests lie. 

What do salespeople think about when trying to close a deal? Their commission, surely! Well, not if they're good salespeople. Putting the client first means, above all else, _caring_ for the client — and not just the commission!

Caring for clients doesn't refer to maternal feelings or offering a shoulder to cry on. Though we usually define caring in terms of sympathy or empathy, caring in this case is more about understanding where the client's interests lie. 

When clients see that companies really are looking out for their interests, they'll be more than ready to listen to the advice they receive. 

A caring company might even fight to change a client's mind if they think the client is heading down the wrong path. Think of the Brown family from the previous blink. In such cases, it's crucial that the client knows the company puts them first; only then will clients reward companies with their trust. 

In other cases, companies struggle to get to the bottom of what a client wants, because the client has irrational or embarrassing fears they wish to hide. But once they share a trusting relationship with the company, they'll feel comfortable enough to tell all.

And when things really go well, care goes both ways. Clients will care that the company performs well, and will take pride in spreading the news about them to their friends and family. Word-of-mouth advertising is incredibly powerful, and you never know when it could make a big difference for your company. 

During the market crash of 2007-2008, the author's competitors in real estate experienced a drop in commissions of up to 70 percent.

But the authors' commissions only fell around 20 percent. Why? Their loyal customer base showed that they cared about the company too, and continued to support and promote them despite tough times in the market.

### 5. Putting clients first guarantees smooth sailing for your company, even in turbulent financial situations. 

The economy operates in cycles — there are good times and bad times. But this doesn't make the bad times any easier for companies. Slumps in the market can threaten to put many once-thriving enterprises out of business. But there is one way to give your company the safety net it needs: building a strong customer base.

During the 2007-8 financial crisis, the real estate market in the United States collapsed. Buyers became reluctant to take out mortgages to buy homes, while sellers flooded the market and banks tried desperately to get repossessed homes off their books. In other words, the price of houses had plummeted, but nobody was buying. Real estate businesses across the United States were struggling to stay afloat.

The authors, however, managed to keep their real estate business relatively stable. The reason? They consistently put their clients first. Their honesty and care for the clients and their interests allowed them to maintain strong, trusting relationships despite a collapsing market. In addition, their competence helped the authors stay on top of changing legislation, ensuring their clients received the best advice possible.

It wasn't just a trusting relationship between client and company that helped keep the authors afloat during the financial crisis; a strong team relationship also carried them through. During the crisis, many businesses chose to lay off their staff and shrink their companies. 

But the authors weren't prepared to start firing employees, as it would cause their overall competence to drop. The team showed their appreciation for this by working harder, which kept customer service at a consistently high level during the crisis.

### 6. Learning to love putting clients first is the ticket to finding fulfillment in your job. 

Knowing how to put your clients first is one thing — enjoying putting them first is another! In order to truly build successful customer relationships, you need the right attitude. After all, there are few things more important than finding joy in your job. 

The truth is that with the right mindset, any job you do can make you feel fulfilled and, yes, happy! Rather than trying to do what you love, why not start trying to just love what you do, whatever it may be. This is itself a very useful skill, particularly when we end up doing certain jobs as a last resort. 

When their children were in elementary school, the authors urgently needed work, so Joseph found a job as a housekeeper at a local hospital. While other housekeepers didn't like their job, he took another approach and took the time to do the best possible work. 

His manager told him that in 32 years, she had never seen an employee do such a good job. Such praise made this supposedly undesirable line of work feel worthwhile. 

Ultimately, businesses revolve around people and their needs — and talking to and liking your clients is the only way to discover such needs. When he was new to the real estate game, Joseph used to go around his neighborhood knocking on doors, sometimes dropping by 30 to 40 neighbors each day. 

As he knew little about real estate, he avoided the topic and instead asked questions about the neighbors themselves. At first, it didn't lead to any business, but it taught him to like and care for the people around him. In the end, his caring approach paid off: within 12 years, Joseph had sold one-third of the homes he had visited.

> "Love what you do and you will never work another day in your life." - Confucius

### 7. Final summary 

The key message in this book:

**Because clients are people like anyone else, success when working with clients is fundamentally based on putting their interest first. By demonstrating honesty, competence and caring, you can set your company apart from the competition, while creating support networks that will keep you afloat even in times of crisis.**

Actionable advice: 

**Honesty is the best policy!**

Be honest and caring with people, and you will never have to worry about what you said or did in the past, or how it will affect you in the future. At the same time, honesty will create trust in your relationships with clients and make you a sought-after professional. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading: _The New Rules of Real Estate_ by Spencer Rascoff & Stan Humphries**

_Zillow Talk: The New Rules of Real Estate_ (2015) gives the reader all the tools they need to buy, sell or rent a home. From the conundrum of whether to rent or buy, to when to sell and how to boost the value of your property, these blinks shed light on the perennially important dilemmas of real estate — the biggest investment of your life.
---

### Joseph Callaway and JoAnn Callaway

Joseph and JoAnn Callaway are real estate agents who managed to reach $1 billion in real estate sales in their first ten years of business. They have sold over 5,000 homes and give talks to various associations and groups from coast to coast.

© Joseph Callaway and JoAnn Callaway: Clients First copyright 2013, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

