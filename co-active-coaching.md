---
id: 5637814f6234650007e70000
slug: co-active-coaching-en
published_date: 2015-11-05T00:00:00.000+00:00
author: Henry Kimsey-House, Karen Kimsey-House, Phillip Sandahl, Laura Whitworth
title: Co-Active Coaching
subtitle: Changing Business, Transforming Lives - The Book That Helped Define the Field of Professional Coaching
main_color: F1B130
text_color: 805D19
---

# Co-Active Coaching

_Changing Business, Transforming Lives - The Book That Helped Define the Field of Professional Coaching_

**Henry Kimsey-House, Karen Kimsey-House, Phillip Sandahl, Laura Whitworth**

_Co-Active Coaching_ (2011) is about designing an effective, empowering relationship between the client and the coach. The authors outline the cornerstones of collaborative coaching, providing applicable examples of how to achieve a successful and trusting coach-client relationship.

---
### 1. What’s in it for me? Discover what the co-active coaching model is and how it can improve coaching. 

Professional coaching is a great way to boost your career, find new purpose in life or gain new perspectives on difficulties you may be facing. Unfortunately, however, most coaches don't realize that coaching can be greatly beneficial to both the person being coached _and_ the person doing the coaching. And this is where the co-active coaching model comes in. Co-active coaching is more about discovery and development than about solving problems. It's about finding things out together. 

So how does it work?

As these blinks will show you, this model is based on a number of different skills, all of which combine to create the kind of conversation that will help the client grow. Foremost among these skills is listening. Of equal importance is curiosity. But there are more. Read on and learn what they are.

In these blinks, you'll also learn

  * why an environment of trust is essential in good coaching;

  * that the skill of listening is underrated; and

  * why dumb questions are sometimes the best ones.

### 2. The foundation of co-active coaching is open and collaborative conversation. 

Every effective coaching relationship is built on collaboration and trust. And in the upcoming blinks, you'll learn exactly how to build such a relationship with your client.

The authors use the term _co-active_ to describe a process in which both the client and the coach actively collaborate together: _both_ are involved in the process.

Ultimately, co-active coaching is _not_ centered on problem-solving, but on conversation. Of course, you'll be addressing issues and finding solutions, but the process is primarily about awareness, discover and choice. That process starts when you familiarize yourself with the four cornerstones of the co-active coaching model:

First, you have to begin by assuming that all people are inherently creative and resourceful. We are all capable of finding answers, making choices and learning from our mistakes. 

Secondly, remember that there's more to a coaching relationship than just problem-solving. Try to see the bigger picture: there are many different factors that impact a client's life — things like work, family, emotions and so on. 

Third: when you're talking to your client, make sure you pay attention to subtle details like tone, mood and body language — and not just on the content of the conversation. The co-active conversation can only work if you create a safe, trusting space that allows for vulnerability; such a space depends on your being tuned into your conversational partner. 

And finally, encourage transformation. Even if your client hired you to work with them on one specific area, having a broad vision will allow you to nudge them toward more holistic, and therefore valuable, change. 

So, all in all, the world of co-active coaching isn't about "fixing" clients; it's about helping them discover and develop their true selves.

### 3. Design an effective environment and define the terms of the relationship. 

In order to conduct a fruitful conversation with your client, you must first design an effective _coaching environment_. In co-active coaching, we like to encourage our clients to take risks. But remember: risks are only taken by people who feel safe.

The best way to foster a nurturing environment that encourages risk-taking is to ensure confidentiality, trust, honesty and spaciousness. 

Those are crucial qualities: fostering confidentiality will allow your client to speak freely — the only way to spark a process of discovery and change. 

Similarly, clients expect honesty: _being nice_ isn't what wins clients' trust; you have to be willing to speak out when the emperor isn't wearing any clothes.

One more thing to remember: in order to learn from their mistakes and move forward, clients need enough space to experiment and confront past failures. 

In addition to creating an effective coaching environment, it's also crucial to set the terms of your working relationship; that means the ground rules and your expectations should be clear from the very beginning. 

To that end, you should set aside a special _discovery session_ before you dive into the coaching process. Here, you can talk openly with your client about your working relationship and discuss the terms. 

You can get the conversation going by asking simple but powerful questions — things like, "Where would you like to make a difference in your life?" or, "How does failure affect you?" Such questions will prompt clients to reflect on where they are and where they're headed; it'll also give you a sense of their strengths and weaknesses. 

The discovery session is also a perfect time to talk about administrative matters like scheduling, cancellation and more. That way, you and your client can be on the same page and avoid future misunderstandings.

> _"Powerful coaching is not about being a powerful coach; it is about the power the client experiences."_

### 4. Learn to listen to your client on a deep level. 

Think about the last time someone was totally with you, truly _listening_ to what you were saying. This powerful experience is surprisingly rarely, isn't it?

Well, as a coach, it's your job to deliver that kind of experience to your client. Meaning, you have to learn to read between the lines, listening deeply for the meaning behind every story. To help you do this, the authors distinguish three levels of listening:

At level I, you're focusing on yourself. You listen to what the other person has to say, but you're mainly concerned with what the conversation means to you personally. 

For instance, let's say your client is having trouble balancing his personal and professional life. If you're at the first level of listening, you might end up offering advice that's based on your own life — how, for example, you successfully juggle the demands of work and family. 

At level II, you're wholly focused on the other person; and as a coach, your awareness is totally directed to your client. 

Continuing with the previous example: listening at level II, a coach will respond to the client's dilemma by working with the client to examine the available solutions in order to better understand why the client feels trapped. In other words, this level of listening is about empathy, clarification and collaboration. 

Finally, at level III, the coach draws on their intuition to gather information that's not directly observable. At this level, you deeply understand your client's dilemma and the internal dynamics behind it. You can sense the energy and emotions, both spoken and unspoken, driving the issue at hand. 

As you can see, listening empathetically is at the crux of coaching. That's the only way to understand whether your client's on-track with their goals and living according to their values. Co-active coaches learn to switch constantly between levels II and III; and once they understand what's really going on, they guide their client to articulate it themselves.

### 5. During coaching sessions, trust your intuition and be willing to speak your mind. 

As a coach, your intuition is one of the most valuable tools at your disposal. Intuition is a gut feeling that can't always be backed up by evidence or facts. And to act on it, you first need to interpret it. 

Start by simply noticing your intuition and drawing attention to it. That way, you create space to talk about any hunch you have, without letting it fly under the radar. 

The way you interpret an intuition will help you articulate it. However, don't get too attached to the meaning you assign your hunches. 

For instance, let's say you and your client are having a conversation. Your intuition informs you that something isn't right here and you interpret this as a sign that your client isn't telling you something. Test your hunch by asking, point blank, whether your client is holding something back. 

It doesn't matter whether your interpretation is correct or incorrect. The key is to use your intuition as a jumping-off point for deeper conversation and understanding. 

Sometimes, you might even want to interrupt the conversation, blurting out a hunch immediately and thereby creating a dramatic shortcut in the coaching conversation, thus leading to surprising new insights.

After all, you don't want the conversation to move on to an entirely new phase, while you're sitting on your hands worrying about whether or not your gut is telling you the right thing. 

Because the thing is, in coaching it's better to dive in headfirst. Don't worry about looking a little clumsy — it will only make you seem more human and authentic.

### 6. Tap into your curiosity to find powerful questions that spark meaningful conversation. 

Imagine a dinner party: you're seated next to a stranger who is infinitely curious about your life, your work, your instincts. For most of us, that level of curiosity is not only flattering — it's also encouraging. 

And expressing curiosity when you're with your client is exactly what will allow her to let down her guard and open up, creating a deeper connection between the two of you.

This is why co-active coaches cultivate a spirit of genuine curiosity and playfulness with their clients. There's a big difference between a conventional, interview-style format and a more personal, curiosity-driven approach. The former entails asking questions like, "What areas will your report focus on?" The curious version, in contrast, would be, "How important will finishing the report be to you?"

As you can see, tapping into your curiosity will help you find powerful questions that spark meaningful conversation.

Let's say your client is complaining — once again — about how helpless she feels in the face of her "impossible" work situation. You're familiar with this story. So you ask, "What are you getting out of this situation?" You can also say, "Is this the only way this can be?"

These are examples of the kinds of powerful questions that tend to shake people out of autopilot. So, don't be surprised if there's a sudden silence; the client will need a little time to think before they respond. 

It's also worth noting that, sometimes, the most powerful questions are so simple they almost sound stupid. For instance, imagine you and your client are discussing a tricky problem. The client knows all the intricacies of the issue and so has rehearsed answers to every question. 

In this situation, simple questions are surprisingly effective. Some examples: _What does the ideal resolution look like? What to do you do next? What can you learn from this?_ What's key is getting the client to see the situation anew.

### 7. Help your client discover their values and use those values to pursue fulfillment. 

Who _doesn't_ want fulfillment? Fulfillment is like a delicious dinner: satisfying, tasty and profoundly filling. And yet, unlike a great meal, fulfillment can be difficult to achieve and scary to pursue. 

As a co-active coach, your number-one goal should be to challenge clients to chase their fulfillment — to find a path that allows them to fully express their true selves. 

That means you have to work with clients to help them discover and articulate their values. The question is, what's essential for each individual?

If your client values risk-taking, chances are they want a life filled with adventure. If they value family, then they probably want to preserve a healthy balance between work and home. 

As you can see, values and fulfillment are deeply linked. In a nutshell, fulfillment is about living a life that's in tune with one's values. 

And thus, for a coach, understanding a client's values is a huge advantage. As clients make choices, their values can function as a litmus test. So when a client faces an important decision, ask him to evaluate to what extent each potential course reflects his values. After all, any decision made on the basis of the client's values will be inherently more fulfilling. 

Of course, this raises an important question: how to help the client discover his values in the first place? Well, the best way to clarify values is to find them in the client's own experiences. Make it a natural, exploratory process — don't just plop a checklist in front of the person!

And then, after you come up with the initial set of values, you can have your client rank them in order of importance. This way, your client will be able to reflect on what matters most in their lives.

### 8. Final summary 

The key message:

**Co-active coaching is about establishing a collaborative relationship with your clients to effectively empower them to find their own answers; your task is also to encourage and support your clients as they make choices to become more in-tune with their personal values.**

Actionable advice:

**Spend half an hour at a coffee shop practicing your curiosity.**

Watch people and try to imagine their values, their hobbies, their obstacles and their dreams. Then at the end of the half hour, find someone you can spend more time with; use this as an opportunity to indulge your curiosity by asking questions. 

Pay attention to how the person responds to your curiosity and then reflect on what you've learned. 

**Suggested** **further** **reading:** ** _Becoming an Exceptional Executive Coach_** **by Michael Frisch and others**

_Becoming an Exceptional Executive Coach_ (2012) aims to deepen your understanding of what it means to take on a coaching role. These blinks outline the core elements of coach-client relationships to present a flexible and rich approach to executive coaching.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Henry Kimsey-House, Karen Kimsey-House, Phillip Sandahl, Laura Whitworth

Henry Kimsey-House, Karen Kimsey-House and Laura Whitworth are the co-founders of The Coaches Training Institute, the foremost coach-training school in the world. Phillip Sandahl is the co-founder and principal of Team Coaching International and a former senior faculty member of CTI.

