---
id: 560864e38a14e60009000000
slug: co-opetition-en
published_date: 2015-09-28T00:00:00.000+00:00
author: Barry J. Nalebuff and Adam M. Brandenburger
title: Co-opetition
subtitle: A revolutionary mindset that combines competition and cooperation; The Game Theory strategy that's changing the game of business
main_color: B22830
text_color: B22830
---

# Co-opetition

_A revolutionary mindset that combines competition and cooperation; The Game Theory strategy that's changing the game of business_

**Barry J. Nalebuff and Adam M. Brandenburger**

_Co-opetition_ (1996) combines game theory with business strategy, presenting a roadmap for how to create a successful venture. The authors explain how running a business is just like playing a game: it involves mastering the rules, knowing the players and the value they bring, understanding tactical approaches and being able to see the big picture. With these elements in place, you can utilize them to improve your own position in the game of business.

---
### 1. What’s in it for me? Learn how to cooperate and compete simultaneously. 

Particularly in business settings, the concepts of _cooperation_ and _competition_ tend to be viewed as opposite, mutually exclusive ideas. But this isn't necessarily the case, and with a bit of creativity and a gentle merging of the two words, the concept of _co-opetition_ emerges. While it may seem a fairly strange word, it's actually a useful tool when setting up a business in today's complex marketplace.

If you're involved in business, whether as a customer, supplier, entrepreneur or competitor, you need to know how the game of business works and, perhaps more importantly, how to shift the rules to your own business's advantage. In _Co-opetition,_ Barry J. Nalebuff and Adam M. Brandenburger introduce the different parts of the game of business, and the multiple interdependent roles that each player has. 

In these blinks, you'll discover

  * how Super Mario ended up being more famous than Mickey Mouse;

  * how to use contractual terms such as "most favored customer" to your advantage; and

  * why people's perceptions of how your business is doing are an important factor.

### 2. In the business world, two players can cooperate and compete at the same time. 

What does it take to make it in the business world? Some people think it's purely a matter of getting one over on the competition. In this view, business is like a race, and you just have to keep your fingers crossed that your main competitors will trip and fall along the way.

But this metaphor doesn't necessarily apply to all business. In reality, two business competitors might actually benefit from supporting each other, instead of trying to outrun each other. 

To understand why, let's divide business players into four groups. The first three are obvious: the customers, the suppliers and the competitors. The fourth group? These are the _complementors_. 

In essence, complementors are products and services that supplement your product to make it more valuable. Hardware and software are perfect examples of a complementor duo: when people have better hardware, they need faster software, and vice versa. 

It's worth noting that there's not always a clear divide between complementors and competitors. Although we've outlined distinct business roles — suppliers, customers and so on — players can also take on multiple roles. 

Therefore, you might be cooperating with a player by adding value to your respective products and at the same time competing with them to determine how to divide the value between you. For example, in the cosmetics industry, manufacturers and retailers complement each other, as both play a vital role in supplying consumers with cosmetics. However, customers are only willing to pay a certain price for, say, lipstick, so manufacturers and retailers have to compete with each other when it comes to dividing up that sum.

And that's exactly where the term co-opetition comes from: it's when two players cooperate and compete with each other at the same time. 

These double roles may seem confusing, but being aware of them is essential if you want to develop an effective business strategy.

> _"Business is cooperation when it comes to creating a pie and competition when it comes to dividing it up."_

### 3. You can use game theory to navigate complex business relationships. 

Navigating the business world and understanding all the different players involved can be confusing. To make it a bit easier for yourself, use _game theory_, an approach for developing effective strategy when your actions depend on those of other players. 

First, you need to assess each player's relative position by unpacking the _PARTS_. The acronym PARTS represents the five elements of the business game: 

  * _P_ layers

  * _A_ dded value

  * _R_ ules

  * _T_ actics and

  * _S_ cope.

As explained in the previous blink, the four groups of players comprise customers, suppliers, competitors and complementors.

Each player adds value to the game, and some more than others. This added value gives players power to negotiate and can thus be used as leverage to change the **r** ules of the game. For instance, in the cosmetics industry, the retailers add value by controlling the distribution channel to consumers. The retailers could use this as leverage to change the rules of how much they pay the manufacturers for their products.

In addition, each player's distinct perception of the game determines their individual tactics, as well as their view of the game's overall scope. For the cosmetics manufacturers, the scope of the game may only be the cosmetics industry, but for retailers like department stores it may encompass many other industries as well.

Ultimately, the five elements of PARTS determine power relationships; each player's relative power determines his or her performance. 

And here's where it gets interesting: you can change the elements of the game to attain a better position. The PARTS aren't solid and inflexible: each and every element can be adjusted to your advantage. 

It's worth noting that any time an element of the game changes, the whole game changes with it. As such, it's essential to think through each of these elements in order to develop a sound business strategy. 

In the next blinks, you'll learn exactly how to change the different elements of the game to gain a better position.

> _"Changing the game is the essence of business strategy."_

### 4. Make sure that playing the game of business is worth your time. 

When a business player thinks about entering the market, they often forget one important point: whenever the number of players changes, so does the game itself. 

Your decision to enter the game might not necessarily be a bad thing for the existing player, and could even be as valuable to them as it is to you. After all, if you become a competitor to one player, another will profit. The customer, for example, will have more options if you decide to enter the market.

This is why you should definitely make sure you're getting compensated for your decision to enter the game. Ask your customers to contribute to your upfront costs or sign a guarantee that they'll work with you — think of it as insurance. 

This approach is all the more necessary if you consider the costs incurred when you enter the game in the first place. In order to enter the market, you have to spend time and money to craft an effective strategy and an attractive proposition. When venturing into a new game, you also risk losing your existing customers or provoking aggression and hostility from your new competitors. 

But let's say the first steps go well and you stay in the game. At this stage, you might benefit from bringing in new players; new customers and complementors will increase your existing value. Similarly, new suppliers could give you a better negotiating position. Even new competition can be valuable — at the very least, it will motivate you! 

So, remember to look for incentives to bring others into the game with you.

> _"The game after you've entered it isn't the same as the one you first saw."_

### 5. Added value is a power level in the game of business. 

Remember the A in PARTS? Right, it stands for Added value, which refers to whatever a player brings to the game upon entering. It's important to keep in mind that this added value isn't a constant, fixed factor — you can change it.

But first, why would you want to? Well, if you're already adding value to the game, you can gain power by diminishing the added value of other players. 

Imagine your business has a monopoly. Your added value is technically equal to the whole game because, well, you're the only game in town! But even then, some players — like suppliers or customers — have their own added value, and you probably want it to be as small as possible. 

As a monopolistic manufacturer of goods, you can achieve this by creating a supply shortage. This way, your customers have less added value, because there's always someone to take their place. 

That's exactly the tactic Nintendo used in the late 1980s. The video game developer made sure there were never enough games on the market, creating frenzied consumer demand. As a result, Mario, the hero of the company's best-selling game, became more famous than Mickey Mouse!

On the flipside, in a competitive marketplace, increasing your own added value (and not changing anyone else's) must be the priority. If you have loads of competition, it's possible that your decision to exit the game would have zero impact on the industry. If so, that would imply that you have no added value!

One way to make sure this doesn't happen is through trade-offs — lowering the quality of your product in exchange for a lower consumer price. 

Better yet, a _trade-on_ — increasing a product's value while simultaneously lowering production costs — will ensure added value. This works in the short term, but beware: the competition will catch up eventually. 

A more sustainable strategy is to build customer relationships, because even if your products are only mediocre, gaining loyal customers will guarantee added value.

### 6. Changing the rules of the game changes the balance of power between players. 

When we're children, other people's rules govern our behavior: we cross the street when the light is green, go to bed when we're told and walk through our elementary school hallways in single file. 

But in adulthood, we can make up some of the rules ourselves. And in the business world, this changes the game completely. Here, rules determine how you must treat your employees, the environment and your customers.

Of course, many rules like those relating to pricing are determined by laws, but the details are far more pliable than you might expect. Even though governments monitor business transactions and enforce antitrust regulations to keep the market fair, individual players have the autonomy to define the rules in contractual terms with their customers. 

For instance, in your contract with the customer, she might demand a most-favored-customer (MFC) clause, stating that you're going to give her a better price than any of your other customers.

You can also negotiate a meet-the-competition (MCC) clause, which gives you the right to match the price any other competitor offers. This way, the customer can't just leave you for a competitor; rather, you have the opportunity to keep them with a price cut. 

Introducing these kinds of rules will change the balance of power between all players. The MFC and MCC clauses represent agreements with your customers, but they also change the relationship between you and the other players. 

The MFC clause is ultimately about negotiating tough prices with everyone, since you can always tell your suppliers you have to reduce your overhead costs to make up for the losses incurred by your lower prices. 

And with the MCC clause, you get to decide whether or not you want to keep the customer. You also save the time it would take to craft a proposition, since the required price is immediately clear.

As you can see, these subtle shifts can have a big impact on the game as a whole.

> _"The battle to establish the rules is the battle before the battle."_

### 7. Changing how other players perceive the game will influence their behavior. 

The human mind is a powerful thing. In the business world, each player's perception of the game can actually change the reality of game itself! 

Why? Well, our ideas about how things work motivate our behavior. So, if you want to change how players in the game interact, start by changing their perceptions. 

Imagine someone believes they're doing poorly in a professional environment, such as in a negotiation situation. In order to improve their performance, they're likely to behave more aggressively to try to make up for their lack of confidence. Unfortunately, this rarely works, as people are unwilling to be coerced by someone aggressive.

On the other hand, someone who's confident will be more at ease and thus able to calmly impose his or her own rules on others. 

You can probably already see how disclosing, hiding or manipulating information can change other people's perceptions, thus giving you power over their actions.

Telegraphing strength is one way to selectively disclose information. For instance, you might invest in a nice suit and haircut for a job interview. This way, the hiring committee will get the message that you've been successful in the past, so you're likely to be successful at their organization in the future. 

Conversely, even if you're in between jobs and having financial difficulties, wearing an old, ratty suit will send the wrong message.

In some cases, it might even be better to keep certain information to yourself. Take the example of Frank Rice, president of Columbia Pictures, who sold the movie rights to _ET_ for just $100,000 — before the movie went on to earn $400 million! That is definitely not the kind of misjudgement you would want to bring up in an interview.

Finally, you can also change people's perceptions by presenting information in a confusing way. For instance, you can mask high prices through a complex calculation method.

> _"To anticipate other players' reactions to your actions, you have to put yourself in their shoes and imagine how they'll play the game."_

### 8. You can gain leverage by changing the scope of the game. 

If you want to, you can look at anything in life as a big game. But here's the thing: each game is part of another, bigger game. The simple fact that every game is linked to every other game means that your behavior as a player can have a ripple effect. 

After all, what you do today in the television industry might impact what happens tomorrow in the political sphere; just one action can link two completely separate games.

People tend not to see their reality in this way. For instance, they would rather think of this year's and next year's commodities markets as wholly distinct arenas, when in fact, these two games have a huge impact on each other!

Employing this knowledge to change the scope of the game is the last PARTS lever. You simply have to look for the links between different games and use these links to your advantage. 

That's exactly what the video game company Sega did when it entered the market in the 1990s. Instead of going after Nintendo's business, which was based around 8-bit systems, Sega developed 16-bit games. The company also set a higher price point for its product, well above what Nintendo was charging. As a result, they weren't technically competing in the same market with the established video game giant. 

In this way, Sega effectively expanded the scope of the game by dividing the market into 8-bit and 16-bit sectors; it thus avoided coming up against a powerful competitor and secured its own spot as an important player.

### 9. Final summary 

The key message:

**In the business world, and within the game of business, players can cooperate and compete at the same time. The key is to thoughtfully approach each important element of the game; players, added value, rules, tactics and scope are all essential concepts you can use to improve your own position.**

Actionable advice:

**Put your confidence forward.**

When trying to project confidence as an employee, you could try to negotiate a contract that combines a relatively low base salary with a high performance bonus. Similarly, as a business, you could offer free trials or launch ridiculously expensive ad campaigns. 

The point is, by taking on additional risks, you'll demonstrate that you're confident in your ability to deliver on your promises. In turn, the other party will be more likely to enter into a business relationship with you.

**Suggested** **further** **reading:** ** _Little Bets_** **by Peter Sims**

**The core principle of _Little Bets_ is taking actions to discover, develop and test new ideas. Little bets start out small, then develop and are refined over time into potentially life-changing actions. Particularly valuable in times of uncertainty, small actions are cheap, low-risk and quickly put into practice, and are therefore massively helpful when undertaking any project.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Barry J. Nalebuff and Adam M. Brandenburger

Barry J. Nalebuff is a Milton Steinbach Professor at the Yale School of Management. Adam M. Brandenburger is professor at the Stern School of Business at New York University, and was previously a professor at Harvard Business School. Both authors have conducted extensive research on game theory.

