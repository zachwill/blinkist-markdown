---
id: 56afa397377c160007000035
slug: coaching-salespeople-into-sales-champions-en
published_date: 2016-02-03T00:00:00.000+00:00
author: Keith Rosen
title: Coaching Salespeople into Sales Champions
subtitle: A Tactical Playbook for Managers and Executives
main_color: D72C2B
text_color: A32121
---

# Coaching Salespeople into Sales Champions

_A Tactical Playbook for Managers and Executives_

**Keith Rosen**

_Coaching Salespeople into Sales Champions_ (2008) is the sales manager's guide to coaching salespeople and learning how to build powerful connections among your sales force. You'll learn how to empower your team, let go of your fears and become a highly effective sales coach.

---
### 1. What’s in it for me? Be a great coach and a great sales manager. 

Great sales managers aren't great because they close the most deals. Like a sports coach, a great sales manager pushes her team to achieve more every day.

That's why a great sales manager is by default a great coach. The problem is that both hats require different skill sets. For example, a great coach is probably less concerned about quarterly sales goals than she is about an employee's personal growth.

Yet a great sales coach can make the difference between whether your sales team is great or just average. And as we all know, a great sales team can make the difference between a successful, growing company and one that bites the dust. 

In these blinks, you'll discover

  * why most sales managers are crippled by fear;

  * how to get salespeople to assume more responsibility; and

  * how to approach your first coaching conversation.

### 2. To build your sales team for success, you’ve got to discover exactly what they need. 

Imagine you're a sales manager who wants to grow her lead generation and sales efforts, but you're struggling without a defined approach or game plan. 

It's clear you need advice, so you decide to enlist a consultant, a trainer and a coach. While your decision is a good one, it's important for you to discover precisely which type of support your salespeople really need.

For instance, a _consultant_ will likely tackle your problem with market research for your target market. He'll then present his findings to you, as well as offer a strategy for employing them. 

The _trainer_ will build on the consultant's conclusions to identify areas in which you and your team need to improve. He'll then provide specific exercises to move you toward your goal. 

In fact, there's a lot a trainer could do to improve your sales approach. For instance, he might help you practice your sales pitch, creating role-playing exercises to do so. 

Clearly, hiring a consultant and a trainer can be helpful, but too many managers underestimate the third step, which is coaching. That's because after your training, a _coach_ plays an instrumental role in ensuring you and your team understand your training and effectively apply it. 

You need to understand the differences between a consultant, trainer and coach as you identify which tools you and your team need to improve sales.

If _you_ want to coach your salespeople, don't underestimate the workings of a coaching process. Be aware that your weekly sales meetings are not the same as a coaching program, despite what many managers believe. 

You'll have to do the coaching on a daily or weekly basis. You'll see, though, that these weekly coaching sessions will give you the chance to uncover and solve problems you might not have seen before. 

So remember, if you're going to undertake the challenge of both managing and coaching, it's essential you learn how to switch between routine managerial work and the work of coaching.

### 3. Don’t let the fear of missed goals paralyze your team and stress you out. Focus on the present. 

The average manager juggles sales goals, tight deadlines and an unending list of problems. As a result, most of them work in constant fear of failing to meet targets. 

The problem is that fear causes managers to worry about what _might_ happen, instead of viewing issues through a rosier lens. So if you want to be an effective coach, you'll first need to banish your fears. 

The author once worked with Michelle, an owner of a marketing and internet-strategy company with a sales force of ten people. Although Michelle was a superb manager, she was nervous — always worried about meeting her goals or concerned about underperforming employees.

As a result, Michelle was using up her energy focusing on everything that could go wrong. And, like most managers, she was mired in the past instead of concentrating on the present. In fact, focusing on the present is _exactly_ what it takes to conquer your fears. 

So as a sales manager and coach, how do you focus on the present? 

Being in the present moment doesn't mean being shortsighted and ignoring what's around the corner; it means that now gets priority. If you're obsessed with meeting future goals, for example, you'll struggle to remain in the present. 

Michelle would constantly ask her team, "Are you hitting your numbers?" or "How many cold calls did you make today?" The pressure of these goals and the managerial angst behind them only stressed out her sales force even more. 

In fact, this management style can even poison a positive relationship between a manager and a sales force, and this can spill out to prospective clients — killing potential sales before a customer even answers a call!

So temper your obsession with results. In the next blink, we'll learn how you can make this goal a reality.

### 4. Being a great sales coach means trusting process over outcomes; open your mind to possibilities! 

So you're ready to conquer your fears and excel as a sales coach. First you'll need to take a step back, learn to live in the present moment and open your mind. 

Reducing your focus on results means shifting your attention from the outcome to the process. After all, the result of an event is merely the product of an involved process. 

To ensure the results you want, you need to identify the process that will produce them — and then stick to it. 

Let's look at Michelle's example from the previous blink. She should be less fixated on the number of calls her team is making and more focused on how the sales process is or isn't working. By doing so, she transforms from a simple sales manager into an effective sales coach!

But sometimes you'll need a little extra help. Another strategy for focusing on process is to let go of expectations. To do so, you'll need to put your energy into creating possibilities, instead of fixating on what you think results should or will be.

A _possibility_ is something that _can_ happen; an _expectation_ is something you anticipate _will_ happen. For instance, you might expect your team to make a certain number of calls each day, but you could also give them the possibility to ask for assistance with difficult prospective clients.

### 5. Good coaches know exactly what each member of their team needs and wants. 

Perhaps you think you know how to improve someone's life, but the problem is that your idea of "better" might not exactly fit with the other person's idea of "better." 

Therefore, it's essential that you don't push individuals to do more than they want to do. 

Jack is a sales manager who, while wanting to become a stellar coach, got distracted by the idea that every time he sat down with a salesperson, he had to produce something of measurable value. 

For instance, if a salesperson came to talk with him about a target goal, Jack would push her to reach higher than the goal. Or if another salesperson wanted to make more cold calls because he was failing to secure enough meetings by referral, Jack would prepare call templates, an opener and a daily call target. 

So regardless of the issue at hand, Jack was a supportive and encouraging manager. But what he didn't get was that different people desire different things. 

For example, say an employee is happy earning $80,000 a year from sales commissions, yet another team member is earning twice as much. A manager like Jack might consider the employee as underperforming and in need of coaching, even though the employee might just prefer to spend time with his children, instead of working every waking hour!

Challenging salespeople to achieve more is a key aspect of coaching, as you want your team to be all it can be. However, coaching isn't about just what's attainable. In fact, it's more about the wants and needs of each individual, and how you can help each person meet the challenges of their personal and professional goals. 

It's therefore key to customize your strategy to fit the individual goals and needs of the person you are coaching!

> _"The amount of value received from coaching will be determined by the person you're coaching, not by you."_

### 6. Stop solving problems for your sales force – and start asking the right questions, so they learn. 

Plenty of managers find their days are spent attending unscheduled meetings, solving crises and fielding questions from irritated customers. In fact, most managers believe that their role is essentially to solve their employees' problems. 

However, what most managers don't see is that bailing out an employee or handing him a thought-out solution to an issue will only reinforce his belief that you can solve all his problems. That means the next time your team is in trouble, you'll be the first person they turn to. 

The result will only be greater dependency and, in the long run, your employees won't become accountable or learn how to troubleshoot their own problems. 

In other words, you'll be perpetuating a vicious cycle wherein you are constantly coming to the rescue and bearing more responsibilities than you should. 

Instead, you should entrust your salespeople with the task of solving their own challenges. After all, it's the only way they'll learn. In fact, it's actually easier than the alternative, since your salespeople will do their own work instead of you doing it for them.

Therefore, a great manager knows that when an employee asks for advice on a problem, the best approach is to let the salesperson solve it himself by asking the right questions. That's because this strategy encourages employees to think for themselves and, by generating their own solutions, employees will feel empowered. 

So what are the _right_ questions?

First of all it's essential to avoid _problem-focused questions_, or questions that center on what's wrong, as they only reinforce negative opinions. Instead, you should ask _solution-oriented questions,_ as the answers will help you and your employee move forward. 

Let's say an employee made a mistake. You might respond by saying, "Why can't you get this right?" — a classic problem-focused question. Instead, ask a solution-oriented question, "How can you do this better next time?"

### 7. Get salespeople invested in the coaching process by encouraging authentic conversations. 

Most of us tend to keep our distance from people we don't know well or have a hard time trusting. For this reason, in a coaching relationship, it's essential that you make your employee feel comfortable. 

But how exactly do you make someone feel comfortable?

It's easy to earn an employee's trust through _enrolling_, the art of deeply connecting with others by communicating in an authentic, honest and open way. It works like this.

First, _connect_ by sharing a story, maybe one that shows the person you've experienced something similar to what he might be going through. This will help build a trusting environment and can foster a more open conversation. 

Next, _show the opportunity_ by using words like "imagine," "think about" or even "consider," cues that encourage your employee to expand his vision of what is possible. 

After all, many people who need coaching need it precisely because they can't see potential right before their eyes. So to get your salesperson's mind moving, offer them a motivational sentence such as, "Imagine if you could achieve total happiness at work."

Third, ask for _authorization_ to move forward. For instance, you might say, "Would you like to discover how we can achieve this together?"

The fourth step is to _position yourself_ by specifying what you want for your employee. For example, "I want you to feel more valued at work."

The next step is to actually have a _conversation_. Deliver your ideas and plan for your employee, then listen to what he wants out of coaching and immerse yourself in his vision. 

Lastly, you need to _enroll_ him by agreeing on next steps and setting a schedule. This could involve deciding what you want to accomplish before the next meeting, how you will structure the coaching process, discussing any concerns or hopes — and before you know it, your coaching strategy will have naturally formed!

> _"True sales coaches leave not only a lasting impression, but also they also create one."_

### 8. Final summary 

The key message in this book:

**Being a manager and a coach are two distinct roles, which means guiding your employees to success requires being able to easily switch from manager to coach, and back again. Once you've mastered this skill, you can overcome your fears and foster authentic conversations with your salespeople to set the foundation for a trusting and thus effective coaching relationship.**

Actionable advice:  

**When offering praise, saying "Good work!" is not enough.**

Many managers know that praise is an effective tool for motivating team members. However, a simple "Good job!" can come off as too generalized. To give honest recognition, you have to pay close attention to what people _really_ do. Next time try something like, "You've done a great job handling this new project. You've been very patient throughout and maintained an optimistic attitude."

**Suggested further reading:** ** _The Sales Bible_** **by Jeffrey Gitomer**

Considered one of "Ten Books Every Salesperson Should Own and Read" by the Dale Carnegie Sales Advantage Program, _The_ _Sales Bible_ (1994, revised 2015) is a classic tome of sales strategy. The book takes an indepth look at the sales practices and techniques the author himself mastered to achieve lasting success in sales.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Keith Rosen

Keith Rosen is the CEO of Profit Builder, a company specializing in sales and coaching training. _Inc._ magazine and _Fast Company_ named Rosen one of the top five most influential executive coaches.

© Keith Rosen: Coaching Salespeople into Sales Champions copyright 2008, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

