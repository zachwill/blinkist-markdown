---
id: 5799e999e0bc1b000321bcb9
slug: cod-en
published_date: 2016-08-04T00:00:00.000+00:00
author: Mark Kurlansky
title: Cod
subtitle: A Biography of the Fish That Changed the World
main_color: 3B7447
text_color: 3B7447
---

# Cod

_A Biography of the Fish That Changed the World_

**Mark Kurlansky**

_Cod_ (1997) charts the rise and fall of the codfish. A major commodity in the European market during the discovery of the New World, cod went on to cause national conflicts and, due to overfishing, eventually became vulnerable to extinction. Find out how this fish changed the world only to end up on the verge of oblivion.

---
### 1. What’s in it for me? Get hooked on cod. 

Over the last few decades, cod has become a luxury fish, often served as fillets in fancy restaurants. There was a time, however, when cod was a humble and inexpensive fish. Native to the Atlantic Ocean, the cod can measure up to two meters in length and weigh as much as 100 kilograms. It's not difficult to catch, often staying in shallow waters close to the coast. So what makes the story of this large, vulnerable fish so fascinating?

Well, for one thing, without the cod, many a long sea journey would have been much more difficult. But most of all, it's such a gripping story because the cod, once in abundance, is now almost extinct. It's the story of conflict over food resources and how modernization can have negative effects on both ecology and society. It's a plunge into the deep waters of human food history.

In these blinks, you'll find out

  * how salt made cod a seafarers staple;

  * why the cod had a significant role for the Catholic Church; and

  * which tiny nation changed maritime and fishing laws to protect a valuable resource.

**This is a Blinkist staff pick**

_"Having fished a lot of cod as a kid, I was thrilled to learn what an important role this once-so-common fish has played in shaping world history."_

– Erik, Editorial Production Manager at Blinkist

### 2. The Basque people were the first to introduce North American cod to Europe. 

You probably like your fish prepared in a certain way, be it fried, steamed or grilled. But when it comes to cod, many countries, especially those in southern Europe, are traditionalists. They simply eat it salted.

This tradition dates back to the Basque people, the earliest fishers to catch cod. They would preserve the cod by salting it — a technique that turned out to be key in exploiting this invaluable resource.

The Basques are a relatively small population of people from modern-day northwest Spain; they've always been independent, maintaining their own language, sports and culture.

They originally set sail for North America in search of whale meat, which was in high demand in Europe. But, along the way, they discovered cod. They cured this fish with salt, thereby furnishing themselves with food on their long journeys.

In Europe, the only places to find cod were in Scandinavia and Iceland, and since the Basques were never seen there, nobody knew where they caught their cod. The European Vikings were already dry-curing their cod, but the salt that the Basques added to the process made the fish last longer and taste even better.

While salted codfish provided food for the Basques as they continued their North American journey along the coast of what is now the United States and Canada, it also made them wealthy back home.

Since the Catholic Church didn't allow people to eat meat on days of fasting, people chose fish instead, and the Basques made a lot of money selling cod to Catholics.

Also to their advantage, the Basques were the only ones who knew where to find cod along the coast of North America. They managed to keep this a secret for a while — no easy feat, considering how profitable cod could be.

But it wasn't long before the secret got out and conflict over the fish began to brew.

> _In Spanish, Italian and Portuguese the word for "fresh cod" doesn't exist. Instead, the word for "salt cod" –_ bacalao _,_ baccalà _and_ bacalhau _– is used._

### 3. The desire for cod led to many conflicts in both the old and the new world. 

At the beginning of the sixteenth century, other explorers discovered Newfoundland, now part of eastern Canada, and the Basques' cod secret was revealed.

Soon, everyone was rushing to get a piece of the fishy action.

The French, the Portuguese, the British and the Spanish all headed to North America to get their share of the cod. By the mid-sixteenth century, there was a massive demand, with cod making up a 60-percent share of the market in Europe.

To meet this demand, some nations teamed up. The British, short on salt, joined forces with the Portuguese, who possessed the necessary ingredient. But the relationship didn't last long. In 1581, the Portuguese split with Britain and teamed up with Spain. The British didn't take this well, and, in 1585, they attacked a Spanish fishing fleet and destroyed a Portuguese fleet.

As a result, Portugal never became a powerful force in the Newfoundland fishing scene.

The British were also worried about their colonists in Boston and other parts of New England who were approaching independence in trade.

New Englanders, trading with Europe and other European colonies such as Newfoundland, were becoming alarmingly rich and self-sufficient. This growing independence contributed to the American Revolution, resulting in the colonists breaking free from the British.

Bitter disagreements about fishing rights followed the revolution, but a settlement was eventually reached wherein the British were granted exclusive fishing rights to the Grand Banks in Canada.

Despite these conflicts, however, there was so much cod at the time that people thought it would last forever. But such beliefs changed as rapidly as the development of new fishing techniques.

> _"Many of the first American coins issued from 1776 to 1778 had codfish on them."_

### 4. The invention of longline fishing increased production but also endangered the cod. 

For a while, fishing remained a traditional practice with few innovations. All that changed, however, when the French invented longline fishing in the nineteenth century.

Basically, longlining is a technique where a small boat, called a dory, drops a long fishing line that sinks to the bottom. Along this line, at three-foot intervals, are lanyards with hooks. Floating barrels were used for buoys so that the fishermen could find these lines again.

The dory would then row back along these lines, remove the fish, re-bait the line and start the process all over again.

While longlining had been around for a while, it wasn't very practical unless you were in a place with a high quantity of fish. Therefore, the Canadian coast was perfect for this practice.

There were other benefits as well. In addition to the regular sale price, the French government subsidized fishermen by paying ten francs for every 65 fish they caught.

But longlining wasn't without controversy; several countries, including Iceland, feared that it could lead to overfishing. And these fears were justified as improved techniques disguised the fact that the fishing grounds were slowly being depleted.

As longlining became a common practice, especially along the Canadian coast, more fish were being caught every year. These statistics led some to believe that there was nothing to worry about. British scientific philosopher Thomas Henry Huxley, for instance, joined several fishing expeditions and said that fears about overfishing were unscientific and misguided.

Statements like these, along with the ability of improved fishing techniques to disguise the problem, further contributed to a general unawareness about overfishing.

### 5. The invention of the steam engine and frozen food forever changed industrial fishing. 

Most people know that the invention of the steam engine led to the Industrial Revolution; less known is how it also transformed the fishing industry.

In the nineteenth century, European waters were already quite empty and it was difficult to catch fish anyway, so the Europeans were the first to test the new steam technology in their boats.

With the steam engine making ships more powerful, people could continue to develop new and more effective fishing techniques, such as the _otter trawl_, which involves a ship dragging a huge net behind it.

Naturally, this provided a huge advantage to the fishing industry. People no longer had to wait for the cod to come to them; now fishermen could pursue the fish. The results told a clear story: otter trawl catches were six times as large as catches made with previous methods.

But this provided another problem for fisheries: it was becoming increasingly difficult to get all these fish to market in good condition.

Luckily, the answer was right around the corner: freezing!

Clarence Birdseye was an eccentric New Yorker who moved to Labrador, Canada, in 1910 to become a fur trapper. It was here that he made the discovery that frozen greens kept their flavor, allowing the vegetables to be eaten during the long Canadian winters.

Birdseye would freeze cabbage in a washbasin by pouring salt water over it and then exposing it to the freezing arctic winds of Labrador. Using this process, his family became the first in their town to eat "fresh" vegetables in the wintertime.

He continued to tinker with this idea, eventually reproducing the arctic winds with a fan and some ice. Before long, his invention caught the interest of the cod fishing industry and more and more people were soon able to dine on fresh fillets of cod.

As beneficial as these inventions were to the fishing industry, however, there was one problem that wouldn't go away.

### 6. Depleted fish populations inspired Iceland to make a bid to expand national fishing zones. 

Overfishing is a topic still debated today. Even at the turn of the twentieth century, people already knew it was a problem — and Iceland was prepared to fight it.

Around 1900, the British discovered that the number of cod in the North Sea had been depleted, so they turned their attention to Icelandic waters, leading to several conflicts.

As an isolated country, Iceland still used traditional fishing methods, guaranteeing a healthy supply of fish. But with the arrival of the British came modern fishing techniques, leading Iceland to upgrade its own fishing fleet and create an Icelandic entrepreneurial class.

However, it wasn't long before Icelanders realized that the cod's reproduction rate was limited and that their waters would quickly become just as depleted as the North Sea if the British stuck around.

For a while, during World War I and World War II, the British did stop their Icelandic fishing, but they returned to Iceland soon afterward. But when Iceland became independent from Denmark after World War II, they were determined to protect cod from overfishing.

Iceland knew how important their fishing industry was to the country, so they fought to expand the territorial limits of the fishing area around Iceland to keep other nations out.

Initially, the area was only three miles, but Iceland was able to expand it to four miles. Still, in 1958, Icelandic cod catches remained in decline, so they extended the limit to 12 miles.

For the British, this was too much. These were the very waters they were fishing, and this move by Iceland was the first of three _cod wars_ that would find Iceland and Britain in heated disputes whenever Iceland extended its territorial limit.

The second bout began on September 1, 1972, when Iceland extended the limit to 50 miles. British ships continued to fish within the limit and Icelanders fought back by cutting their nets and releasing their catch.

Eventually, Iceland extended the limit to 200 miles, and the same battle started again. But, in the end, the British backed off and accepted the territorial limit, which is still in effect today and has allowed Iceland to maintain a healthy supply of cod.

### 7. North American cod has become a scarce resource but measures have been taken to get the fish back. 

When it comes to fishing regulations, other countries haven't been as wise as Iceland.

While they took measures to protect their cod, other countries, especially in North America, have continued to overfish.

A perfect example is Canada: After getting rid of Spanish and Portuguese fishing fleets, and settling border issues with the United States., Canadians turned a disastrous fishing industry into a profitable one.

But this meant that the number of boats, processing plants and catches steadily increased. To compound the problem, fisherman were even using other fish, such as herring, mackerel and capelin, to catch the cod.

And when the cod started to disappear, the government assumed it was just a temporary problem. The population had dwindled before; they assumed it would bounce back.

But they were wrong. It wasn't just one or two fishing grounds that were declining due to changes in climate or migratory patterns. Cod were simply being overfished, and drastic action had to be taken.

So, in 1992, Canada's fisheries minister finally announced a moratorium that put a limit on cod fishing.

It was no easy decision, since it put 30,000 fishermen out of work. But the moratorium was eventually extended and remains in place today.

The decision effectively closed all cod fisheries in Canada except for one in south-western Nova Scotia. Furthermore, it placed a strict quota on all other ground fish.

While this decision might have helped save the Canadian cod from extinction, it also made the fish commercially unattractive. It essentially put a disastrous end to an enterprise that had lasted over 500 years.

There is still hope that the cod will come back, but much of this hope is paired with ignorance. Many people still refuse to believe that humans were what caused the fish to nearly disappear.

In this way, the story of cod is a cautionary tale: although there are promising signs, it is still unclear whether the cod will ever make a big comeback.

> _"Man wants to see nature and evolution as separate from human activities."_

### 8. Final summary 

The key message in this book:

**The cod, a valuable resource as well as a cause of national conflict, changed the course of history. Yet humans depleted this once-plentiful resource to the point of endangerment. It's important that we learn from our past mistakes and take real measures, like the ones that Iceland took, in order to raise awareness and protect our natural resources.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Your Inner Fish_** **by Neil Shubin**

Drawing on findings from paleontology, genetics and developmental biology, _Your_ _Inner_ _Fish_ describes the evolutionary history of the human body, tracing it back to the fish. The author shows how studying fossils, genes and embryonic development can help us understand our complex evolutionary past.
---

### Mark Kurlansky

Mark Kurlansky is an American journalist who has written a number of fiction and nonfiction books. His international bestseller _Cod_ has been translated into 15 different languages. His other works include _Salt_ and _World Without Fish_.

