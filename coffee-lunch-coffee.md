---
id: 53e1f9cf6339360007030000
slug: coffee-lunch-coffee-en
published_date: 2014-08-05T00:00:00.000+00:00
author: Alana Muller
title: Coffee Lunch Coffee
subtitle: A Practical Field Guide for Master Networking
main_color: F78F31
text_color: 91541D
---

# Coffee Lunch Coffee

_A Practical Field Guide for Master Networking_

**Alana Muller**

_Coffee_ _Lunch_ _Coffee_ (2012) is a practical guide to networking. Using her personal and professional experience, along with tips and exercises, author Alana Muller demonstrates how to develop networking skills and build lasting relationships that can help us in our personal and professional lives. A must-have for anyone who wants to succeed professionally.

---
### 1. What’s in it for me? Increase your professional and personal success by learning how to network effectively. 

Networking used to be seen as a necessity only when we were out of work. More than ever, however, networking has become a prerequisite to professional success, whether we are unemployed or not. When done well, networking provides us with a community and keeps us open to developments in the job market.

This book will show you how to succeed in and appreciate the networking process, how to build community and how you can connect with others professionally in an enjoyable way.

In the following blinks, you will find out:

  * how to connect with someone in 30-seconds,

  * how networking completely changed the author's life, and

  * how someone transformed a simple networking experience into $25 million.

### 2. Networking is a necessary skill to have in order to succeed and goes beyond collecting business cards. 

No matter what industry we work in, networking is an essential skill to possess. In fact, in our fast-paced, rapidly changing work environment, we don't have the luxury of avoiding it!

Far from merely an opportunity to collect business cards, networking is about establishing relationships and helping people.

For example, author and columnist Arianna Huffington once attended an event at a woman's home in New York City to talk and discuss the challenges women face in the workplace. After her talk, she gave her private email address to everyone in the room. Doing so enabled her to continue the conversation she had started with all the attendees far beyond the meeting.

To be effective networkers, we must be interested in the people we are talking to, not just in seeing what the other person can do for us. In the long term, helping others offers benefits to both our professional and personal lives.

Networking is not about the quantity of contacts, but the quality of our relations. It is a skill we need to succeed in life. It acts as a safety net in our evolving work environments and helps us move forward in our jobs.

We may not all be natural-born professional networkers, but there are tips and tricks that anyone can learn in order to hone these beneficial skills.

These tips include: making sure you have a 30-second elevator speech where you can quickly and easily introduce who you are and what you do; knowing what resources you can use to network (sites such as linkedin.com and job-hunt.org); how to tell your story to really connect with others and how to build your portfolio.

Getting a good grasp of these techniques can make your career satisfying and challenging.

> _"Networking is part art, part science."_

### 3. Networking is about building a community and can be life-changing. 

Networking means crafting relationships, connecting with people, and being part of a greater community. Our jobs may change, but the relationships we cultivate through networking endure over time.

Networking is about creating community.

For example, after the author left her job of 10 years at Sprint Nextel Corp., rather than wallowing in loneliness, she booked herself meetings every day for nine months in order to connect with new people and companies. By doing so, she created a safety net of contacts.

Muller's networking taught her how to adapt, following the changes in the business world and reacting to them in an appropriate way.

Her networking schedule also prevented her from isolation. Introducing herself to 200 people and going to 160 meetings in nine months meant that she had to constantly connect with others.

In turn, she was able to build a community where many people could communicate and work with each other.

To get the most out of networking, you should treat it like a job: get up early and start planning your meetings. Ideal times to meet up with others are morning coffee, midday lunch, and afternoon coffee.

Though some of us have a natural inclination to network, not all of us find it easy. However, by exercising your networking muscles — even if you think you don't need to — you'll always be prepared for the day when you could use some help from your contacts.

Beyond fostering community and serving as a safety net, networking can even be life-changing.

For example, after the author left her job, she succeeded in landing her current job as president of Kauffman's FastTrac — as a result of her nine months' of meetings.

> _"A network is a kind of ecosystem. It's organic. It's dynamic."_

### 4. To be successful at networking, you must know your goals and complete the necessary prep-work. 

Where do you see yourself in the next few months? What about in a couple of years? Addressing these questions can make networking more effective because they help you identify your goals. And identifying your goals helps you to step up your networking.

When you consider what you want, you should prioritize your long term goals so that the short term goals can get you there.

Short term goals can include meeting those who are involved in your field, discussing and receiving ideas from them and asking how _you_ might be able to help _them_.

Long term goals might include cultivating professional and personal relationships with colleagues, getting your dream job or making a positive difference to others within your current job.

Taking some time to make the following lists will help you achieve your networking goals:

  * People in your community that you already know

  * People you want to get to know in your community

  * Companies you want to get to know

  * Your "non-negotiables"

Your "non-negotiables" list outlines the things that you are not willing to compromise for your new job. This could be, for example, geographical restrictions or how you divide your work and home life.

As well as listing your goals, you should also prepare yourself for networking; specifically your mind, your body and your spirit.

Your mind's job is to focus on your end goal, which means doing homework on the person you're meeting with.

Your body's task is to take charge of your emotions in each meeting. Remember that positivity and cheerfulness are contagious and increase the chances of having a successful meeting!

Lastly, your spirit's responsibility is to focus on long-term goals. Focus on where you see yourself in the future, how you can help the person you are meeting, and the main points you want communicate.

> _"... With every fiber of my being, I am convinced that the way that work, that life, that everything gets done is through relationships and relationship building — achieved primarily through networking."_

### 5. Storytelling initiates networking and brings your work experience to life. 

So, how do you get the networking ball rolling? The answer is through _storytelling_, which means sharing anecdotes from your professional experience.

Your story should start with an introductory email requesting a meeting. How you introduce yourself here is very important.

If you didn't receive their contact information yourself, mention who gave you their details and how you contacted them.

In your email, briefly outline your work experience and close off by stating what you are interested in working on and asking whether the person is available to meet up.

In this initial email, avoid sending a résumé. Instead, send it in a follow-up email, when confirming the meeting.

When you're actually in the meeting, your story should be well-crafted, bring your work experience to life and contain the following features:

First, you should mention what _conflicts_ you faced in your job. For example, maybe you managed to retain a client who was considering leaving your services.

You should also have a _hero_ — you, of course — but mentioning particularly helpful colleagues along the way is also a good idea.

To keep your partner interested, there should also be _anticipation_. After the conflict, you can provide reasons why it was so important.

Next, your story should have a _peak_, that is, tell them how you solved the conflict!

After the peak, describe a _transformation_ by telling them how you changed as a result of the conflict.

Finally, provide an _explanation,_ where you emphasize how you grew from the experience.

Make sure your story is relevant to your listeners and encourage them to reciprocate with their own anecdotes.

It's a good idea to prepare your stories beforehand, write them down, and even practice them in front of the mirror.

### 6. When networking in person, you must be fully present and control your stress levels. 

It's the fateful day and you're eager to meet the person. So, what are some useful things to keep in mind?

The first thing to remember is that your mind and body should be present and calm when communicating with your partner.

First impressions are key, so approach your meetings with confidence by offering a firm handshake. Keep in mind the meeting should be an enjoyable event and something you've been looking forward to.

Make sure you have a process — agree on a time and place that suits you both and come prepared with key points and a main message you want to communicate.

Also make sure you research the person before walking into your meeting. Have some questions to ask them, such as: where is your company headed? What responsibilities do you have? How did you join the company? How did your previous work experience support you in this position?

Listening is critical! Pay attention to your partner's responses and how they connect to your professional interests.

Listening will help your body and mind stay present and keep the conversation moving smoothly.

So how can we control our stress levels? The following tips should help.

Use eye contact. Remember to look the person you are speaking with in the eye and avoid looking away when either of you are talking.

If you feel your nerves taking over and you find yourself sweating and short of breath, consciously slow down and deepen your breathing. This has a calming and cooling effect.

It is important to keep hand movements to a minimum. So if you have nervous hands, focus on eye contact and your hands should follow naturally. You can also keep them folded to minimize their twitchiness!

Having control over these reactions will make you appear more comfortable and help you connect with the other person.

> _"I like to think of networking not as a means to an end, but as a way of being."_

### 7. Networking effectively means building your portfolio. 

Your portfolio is the complete picture of how you present yourself to others. Paying attention to every aspect of your portfolio enables you stand out from the crowd. Simply put, a strong portfolio is a must-have networking tool.

Your portfolio should include an introduction, business cards, a résumé and your bio.

Your introduction should be a 30-second elevator pitch. It is a chance for you to advertise yourself. Use this 30-second time frame in an elevator or while walking between rooms with someone. During this time tell them a short snippet about who you are, your professional history and your goals.

Next, you should always have your business cards at the ready. You might even consider having a personal card that states your field of specialization in addition to your standard business card.

Your portfolio should also include a résumé. Make sure you keep this relevant and up to date. According to networking guru Eric Morgenstern, a résumé is like a negligee: it should be just revealing enough so as to entice the other party, but not give away everything! Make sure you also have an up-to-date, professional headshot of yourself to attach to your résumé.

You'll also want to be equipped with a professional bio to give to those you meet. Muller recommends having a brief version of about 200 words and a more detailed version of around 500 words.

Bear in mind your portfolio should continue to expand, even after the meeting. Be sure to write a follow-up email thanking the other party for their time and, if appropriate, send a thank-you email to the person who referred you.

Recap what you learned from the meeting, as recalling details from the first meeting is central to building solid professional relationships.

Assembling a portfolio in this way will help you stand out and connect with others both professionally and personally.

### 8. Networking involves taking risks and stepping outside your comfort zone. 

Aside from scheduling individual meetings, there are also other situations where we can network in a more spontaneous way. It might seem daunting, but it's a vital part of your networking training. It allows you to step out of your comfort zone and explore your community.

Risk-taking involves pushing yourself. By doing so, you will begin to see networking opportunities where you didn't see them before.

The reality is networking doesn't automatically happen when you schedule your meeting. You should be prepared to turn naturally-occurring situations into a chance to network.

Laurel Touby, founder of the networking and job search website Mediabistro, is great at doing that. After college, Laurel moved to New York City to start her career as a writer and after some experience working for magazines, she decided to move on to freelance work.

Finding freelancing rather isolating, Laurel started arranging weekly events with one of her freelancing friends, where they could each invite friends to connect with each other and discuss their work.

What started as a simple way to meet and connect with new people soon developed into a networking event.

Witnessing the success of this, Laurel decided to transform the concept into a job hunting and networking website, where employers could post job vacancies. If the employers were able to find someone, she would earn a commission.

Laurel's idea was so successful that she was able to sell her company for $25 million.

The moral of the story? Although not all of us will be able to turn a networking event into a multi-million dollar business, it's impossible to know the opportunities that may come about from informal meetups, unless we engage in them!

Laurel proved to be a highly successful networker precisely because she did not set out to run these meetings with a view to making money, but rather as an opportunity to get to know people.

### 9. Refusing to give up is key to mastering networking. 

As in all professions, becoming an expert requires effort. To become an excellent networker takes dedication, perseverance, and completing day-to-day practicalities such as meeting with others, being an attentive listener, following up and being ready to give back to others.

It's important to acknowledge that mastering networking is a lifelong task that requires patience and the willingness to put in the time needed.

Much like learning a language, you must put in the necessary hours if you want to become fluent. In other words, you must be open to learning at all times and gathering knowledge from your contacts.

Reaching out and asking questions is vital as this often results in learning something new.

Questions can be either trivial, such as asking what holiday destinations to go to or how to work an aperture on a camera, or more important, such as how you can help someone at a company that you would like to work at.

Mastering the networking language isn't just about talking business with only your own interests in mind. It's about listening to others and forming a personal connection with them.

You must also bear in mind that success and significant pay-offs don't always happen from networking.

Don't forget that in those nine months before her current job as president at Kauffman FastTrac, Muller attended a total of 160 meetings and met 200 new people. So perseverance is key!

Networking is about fostering relationships; like cultivating a garden, it would be absurd to expect instant results. The relationships need time and attention to grow.

As a lifelong student and networking practitioner, you should always seek out ways to connect with people and be prepared to never stop learning!

> _"Turns out, we're never fully cooked, folks! There is always more that we can do to learn, to expand our knowledge, to improve our prowess at whatever task is at hand."_

### 10. Final Summary 

The key message in this book:

**Networking** **is** **an** **essential** **skill** **in** **our** **ever-changing** **business** **world.** **Knowing** **how** **to** **connect** **with** **others** **and** **build** **lasting** **professional** **relationships** **will** **help** **you** **in** **your** **professional** **and** **personal** **endeavours.** **Whether** **you** **are** **looking** **for** **a** **job** **or** **simply** **want** **to** **explore** **more** **opportunities** **for** **growth** **in** **your** **current** **work,** **networking** **will** **help** **you** **achieve** **your** **goals.**

Actionable advice:

**Set** **up** **networking** **meetings!**

Networking isn't just for people who are looking to change jobs; it helps us to meet new people and expand our professional circle even if we are satisfied in our current job. Make it a point to network and meet with a certain amount of people per week or month. Aim to stay in touch with those people while meeting new people.

**Steal** **networking** **ideas** **from** **your** **social** **interactions.**

The next time you're socializing, take mental note of what makes you and the person you are socializing with feel comfortable, and how you can apply those same methods to your networking interactions. Think of ways you connect with others in non-networking situations that you can apply to your professional networking meetings to make them more effective, memorable, and enjoyable.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Alana Muller

Alana Muller is the author and creator of the blog Coffee Lunch Coffee (http://www.coffeelunchcoffee.com/) and is president of Kauffman FastTrac, a not-for-profit educational organization which helps and trains entrepreneurs. Alana holds a Master's degree in business administration from the University of Chicago, frequently runs business workshops and has contributed to Forbes.com and _The Huffington Post_.

