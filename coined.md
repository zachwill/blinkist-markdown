---
id: 559267e76561380007e70000
slug: coined-en
published_date: 2015-07-03T00:00:00.000+00:00
author: Kabir Sehgal
title: Coined
subtitle: The Rich Life of Money and How Its History Has Shaped Us
main_color: C9CC5E
text_color: 64662F
---

# Coined

_The Rich Life of Money and How Its History Has Shaped Us_

**Kabir Sehgal**

_Coined_ (2015) offers an in-depth explanation of money, a powerful and complex force that many of us take for granted. It examines money's historical roots and explains the relationship between it and our emotions, while offering theories on the future evolution of money.

---
### 1. What’s in it for me? An introduction to how money works and why we use it. 

As the famous song goes, "Money makes the world go 'round!"

Almost everything we do daily involves the give or take of cash in some fashion. We work to earn it; we shop to spend it; we save and protect our supply of it.

Yet why exactly do we spend our lives in the thrall of money? What is it that makes it so important?

These blinks will give you a unique look at why cash is king, from its early beginnings to what it's become in the twenty-first century.

In these blinks you'll discover

  * why cash is more valuable than the paper it's printed on;

  * why a German can't distinguish between debt and guilt; and

  * why money is to a man what pollen is to a bee and a flower.

### 2. Money arose as a medium of exchange when communities began to produce surpluses. 

Money is a crucial element of survival in the society we've constructed. We use cash as a medium of exchange for getting the things we want or need.

And if you really want to understand money, you first have to understand how it works in the context of the laws of nature.

The nature of exchange is a key concept for all living creatures. Organisms work together to survive, often entering into symbiotic relationships, or _symbiosis_. In such a relationship, two different organisms benefit each other so that they together can better survive and reproduce.

That's how bees and flowers work. Bees derive energy from the nectar in flowers and then make honey to store through the winter. In turn, they spread pollen from flower to flower, thus fertilizing the flowers and ensuring their survival.

Bees and flowers also exchange electrical energy. Flowers have a "negative" charge and bees a "positive" charge — like magnets, that's why they're attracted to each other. The bees are pulled toward the negatively charged pollen, which then sticks to them so they can easily transport it and pollinate other flowers. Both species benefit equally.

Ancient humans, in contrast, came to realize that they as a group had a better chance of survival if they helped each other.

So humans specialized in different skills and created divisions of labor. Some individuals hunted, while others raised children, for example.

Eventually, humans started producing more food than they could consume. For the first time, they had a surplus that they could trade with other groups for goods they needed. Thus groups began trading technology, such as hand axes or spears.

Over time, humans learned that trade was easier and more effective if there existed a universal tool for exchange, rather than exchanging goods or services themselves — and money was developed.

### 3. Our financial decisions aren't always logical but are affected by our emotions. 

Humans aren't always logical. Our emotions can push us into making irrational decisions, though economists tend to forget about that when doing calculations.

Economists often theorize that humans are always rational, but this is simply untrue.

Modern economics is founded on a certain model of human behavior, in which people weigh the costs and benefits of different options and choose the one that's most beneficial.

However, events like the 2008 global financial crisis illustrate that human behavior isn't always guided by logic. We're affected by _cognitive bias_, the tendency to have irrational thoughts that lead to errors or biases in judgment.

Cognitive bias is powerful. Did you know, for example, that the weather affects the amount of money you spend? It's been shown that customers tip more when it's sunnier. That's also part of the reason why markets perform better on sunny days.

_Loss aversion_ is another thing that affects what we do with our money. Loss aversion makes us perceive losses as more damaging than the possibility of a gain.

So how can we understand financial choices as a society if economists are basing their equations on faulty assumptions? And what's more, brain imaging has revealed that when a person makes a decision about money, certain areas of the brain linked to subconscious emotions are activated.

The _nucleus accumbens_, for instance, is associated with feeling pleasure or being motivated. It's activated when we anticipate gaining something, like winning the lottery.

The _anterior insular_, on the other hand, is associated with negative emotions like pain, and it's activated when we anticipate a loss. That's why we hate losing so much — it actually hurts!

So when you make a financial decision, you're actually affected by subconscious processes too, in addition to cognitive bias and loss aversion. Emotions influence our spending quite a bit!

### 4. Economists disagree on whether money has an intrinsic value, but there are general trends. 

"Money" might seem easy to define. It's simply the stuff we use to make a transaction, whether in the form of coins, bills or digital currency.

But there's actually a fair bit of disagreement among experts on what actually constitutes "money."

There are two opposing economic doctrines that seek to _define_ what money is.

The first doctrine, _metallism,_ posits that money derives its value from materials that have intrinsic worth, such as silver, gold or other commodities. So paper money should be "backed" by a valuable commodity to ensure its worth.

The metallist view considers money to be _hard_, meaning its value is determined by the market.

_Chartalism_, on the other hand, posits that money _doesn't_ have an intrinsic value. So a dollar bill is just a piece of paper that doesn't mean anything on its own.

The chartalist view considers money to be _soft_, meaning that a state can control the value of money by adding more of it to the marketplace. For chartalists, the value of money is a reflection of an economy's overall performance.

Although the doctrines differ, the history of money has shown a general pattern, in that we're moving gradually from the idea of hard money to soft money.

From currency's early days to the twentieth century, money was generally viewed as hard. Paper notes and coins were all tied to reserve metals, usually gold. In 1900, for example, the U.S. Congress established the _Gold Standard Act_, which tied the dollar to the price of gold.

In 1971, however, President Richard Nixon separated the dollar from the price of gold, and the rest of the world mostly followed suit. Countries gradually removed the connection of money to the price of material goods, so they could have more control over the actual value of their currency.

Today, money only derives its value from the amount of it that's in circulation; it does not have intrinsic worth.

### 5. Money has evolved from coins and bills to credit cards and mobile payments. What’s next? 

As the years pass, metal coins and paper notes become more and more outdated.

The rapid technological advancements of the last hundred years have altered much of society, money included.

The twentieth century saw several major changes in the global monetary system, such as the introduction of _credit cards_.

Credit cards are safer and more convenient than bills and coins, because a card can be used online or swiped quickly at a checkout counter.

Credit cards aren't necessarily widespread, however. While consumers in the United States have tons of cards, some 82 percent of global transactions are still conducted in cash.

Some countries, such as Germany, have few credit cards. Germany historically has been averse to debt. In fact, the German word for debt, _schuld_, translates to "guilt."

Yet credit cards have been found to boost spending, so it's likely that governments and businesses will continue to encourage their use, especially in rapidly developing markets such as China.

Consumer spending actually grows by 0.5 percent when credit card payments increase by 10 percent, research has shown.

Mobile phones have also changed how we pay for goods, and it's likely that they will have a far greater impact than do credit cards today. There are many more mobile phones than credit cards in the world, and such a network presents the potential for extensive payment systems.

Mobile payments are estimated to grow by 62 to 100 percent in coming years. We may see some major changes, like the rise of _mobile wallets,_ which allow a mobile phone to make direct payments.

Apple Pay is one example of technology that ties together mobile devices and payment systems.

Apple Pay allows a user to connect her credit or debit cards to an iPhone, and then pay for goods in stores that accept Apple Pay. A user just has to hold her phone up to a special reader in the store for the transaction to be completed.

### 6. The way a society prints, uses and understands money reveals a lot about its character. 

Money is practical, as it allows us to buy the things we need. Yet it has a symbolic purpose too, in that it can make a statement about the lives we lead and the society we've created.

Your opinion on and use of money says a lot about your values, which is why so many people use money to measure success, or failure. Plenty of people work tirelessly to earn as much as they can, only to spend that cash on status symbols such as expensive cars or clothes.

Yet how you see money depends on your background and your culture. Many religions, such as Hinduism and Christianity, preach that believers should seek money as little as possible. In Christianity, Jesus tells a wealthy man to get rid of all his possessions and follow him instead.

As a society, we've incorporated money in many of our value systems.

Money can even represent the values of an entire nation. Ancient coins have revealed a lot about the societies from which they came.

In ancient Vietnam, Dinh Bo Linh (968-979) unified Vietnam after a civil war and issued the new nation's first coins.

These coins tell an interesting tale. Heavier coins indicate a strong economy, whereas lighter coins suggest that metal was going to other uses, such as for weapons during wartime.

Images are also revealing. If coins are detailed and have complicated calligraphy, it indicates a higher educated society, with a ruler concerned with the literacy of his people. If the coins are simple and easy to understand, usually the opposite is true.

### 7. Final summary 

The key message in this book:

**Money is an extremely powerful force. We evolved to use it to collaborate more effectively and survive in the wild. The financial choices we make are influenced by our emotions; and our values often stem from how we treat and treasure money. Money has changed a great deal since it was developed, and it's clear that money will continue to evolve, especially as technology progresses.**

**Suggested further reading:** ** _The Ascent of Money_** **by Niall Ferguson**

_The Ascent of Money_ is an explanation of how different historical events led to the development of the current financial system.

It aims to show how, despite its proneness to crises and inequality, the financial system and money itself are drivers of human history and progress.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Kabir Sehgal

Kabir Sehgal is the former vice president of emerging market equities at JP Morgan. He's also a bestselling author, including titles such as _Walk in My Shoes_, _A Bucket of Blessings_ and _Jazzocracy_.

