---
id: 5908729fb238e10006e1db5a
slug: color-en
published_date: 2017-05-05T00:00:00.000+00:00
author: Victoria Finlay
title: Color
subtitle: A Natural History of the Palette
main_color: F2D036
text_color: 73631A
---

# Color

_A Natural History of the Palette_

**Victoria Finlay**

_Color: A Natural History of the Palette_ (2002) tells the intriguing and odd stories connected to the discovery of pigments and the use of paints. From the prehistoric use of ochre in cave drawings to the outlawing of white paint in the twentieth century, these blinks are full of surprising, colorful information.

---
### 1. What’s in it for me? Learn why the world around us features such vibrant color. 

Most of us don't spend too much time wondering what exactly gives rouge or cherry lollipops such a vibrant and intense red color. That may be just as well, because, as it turns out, one of the most common ingredients in red dyes is the blood of thousands of insects. Meanwhile, Napoleon's infamous demise could be linked to a poison found in green paint.

Who knew there was so much power, politics and death mixed up in the luscious hues we admire on each others' lips or the vivid shades we roll onto a canvas? These blinks will show you how the vibrant colors of everyday objects have complex and fascinating histories of their own.

In these blinks, you'll learn

  * why yellowish-brown was the first color of paint;

  * what goes into that special sparkle of a glass in paintings by the Dutch masters; and

  * how bullets came to alter yellow pigments during the Vietnam War.

### 2. Ochre was the earliest color of paint and it remains an important shade to this day. 

Have you ever seen pictures of prehistoric cave paintings, such as those at Lascaux or Altamira? They're truly wondrous designs, but you may have noticed that their color palette is quite restricted. There's a good reason for this.

The artists who created these cave paintings mostly used _ochre_, a naturally occurring pigment that ranges in color from earthy brown to yellow to red depending on the sort of iron oxide present.

Ochre was unearthed and used long before all other kinds of paint were discovered or circulated widely, and it has long been cherished as a result. According to the Roman encyclopedist Pliny the Elder, the rocks around Sinope on the Black Sea were the best source for ochre, but the pigment is found in many other regions as well.

For example, the native peoples of North America used the pigment on their skin, believing it protected them against evil spirits. It also repelled flying insects in the summer months and protected their skin against biting winter winds. In fact, painting their skin is probably why white colonists gave them the derogatory name "red Indians" or "redskins."

Ochre continues to play an important role in modern aboriginal art, and some of the best examples can be found in Australia.

The author headed to Alice Springs in Northern Australia to see these works of art in their natural setting. Many Aboriginal artists still work with ochre, especially those from the Australia's Central Desert Region.

These works are often given mystical titles, such as Two Snakes Dreaming, and their composition is similarly mystical. Ochre paintings often include patterns of dots, waves and circles, with each of these shapes carrying a significant meaning. Circles may represent water holes in the desert, ovals may represent shields and wavy lines may depict people sitting around a fire.

As you can see, Ochre is an ancient color and has been daubed on skin, stone and canvas — but it's just the start of our journey into colors.

### 3. White paint was popular, deadly and somewhat revolting. 

All kinds of objects have been used to make white paint, including chalk, fossilized sea creatures and rice, none of which were particularly effective. Historically, the most popular white paint was made with a far more dangerous ingredient: lead.

European artists in the sixteenth and seventeenth centuries loved lead white. Its color was so pure that it was used to highlight details, such as the reflection on a silver jar, or the glimmer in someone's eye. Lead white was especially favored by the Dutch School, which included artists such as Vermeer and Rembrandt.

It was already known in antiquity that lead white was poisonous. Pliny the Elder wrote that, if swallowed, it could kill. But it was only after many centuries, and after many lead-related deaths, that people realized it could also be absorbed through the skin.

The danger didn't end there. In the manufacturing process, artisans would inhale toxic lead particles.

Despite all this, the popularity of this paint could not be suppressed, and it was so highly regarded that it proved difficult to control its use. For example, it wasn't until 1978 that it was finally banned in the United States.

It's also worth noting that this paint wasn't exactly produced in pristine white labs. The manufacturing process itself was fairly unpleasant and, at its heart, had been carried out in the same way since Roman times.

To create the color, lead shavings were added to bowls filled with vinegar. The ensuing chemical reaction produces lead carbonate, a white substance that is the basis for lead white.

However, during the Renaissance, the Dutch discovered that if they surrounded the vinegar bowls with cow manure, they could optimize the process. The heat from the manure boosted the chemical reaction, which created more white lead much faster.

It's hard to believe, but this disgusting and deadly paint was widely considered to be irreplaceable.

### 4. Red carmine is powerful when first applied to a canvas, but gradually fades. 

When you're in an art gallery, it's easy to imagine that you're seeing the paintings as they were originally created by their artists — but this isn't entirely accurate.

Take, for example, the great English landscape painter Joseph Mallord William Turner, or more specifically, his 1835 piece _Waves Breaking Against the Wind_. While it depicts a rather unsettling gray sunset, it's really just the ravages of time at work. Turner originally used a bright red paint called carmine, but the pigment lost its luster over time.

Turner knew only too well that the pigment would dull, but this was of little importance to him. In fact, records survive of Turner's exchanges with the paint supplier William Winsor, proving Winsor tried to convince Turner to buy different and more durable pigments. But Turner wanted the immediate impact of the transitory carmine red.

But maybe Turner had a point. This impermanence of the color is perfectly aligned with Turner's subject matter, not just in this one piece, but more generally as well. After all, what is more fleeting and changeable than nature itself?

There is a simple fact that explains why red carmine fades: it's made of the blood of thousands of insects.

The cochineal bug lives in deserts, where it feeds on cactus plants and reproduces rapidly. Consequently, it can be regularly harvested and pressed for its blood.

It may surprise you to learn that this practice is still in use today — and not just in pigments, but also in food. Carmine is used in red lipsticks and eyeshadow, and is even found in Cherry Coke and sweets throughout the United States and Europe.

It's strange to think that we just can't let go of this unstable and somewhat sadistic pigment.

### 5. A widespread yellow pigment comes from Asia, but its purity hasn’t always been consistent. 

Think of the imports from the Far East and you'll probably recall the Silk Road, the route by which materials and spices were transported across continents.

Among these products was a pigment known in Asia as ivy yellow, one of the oldest shades of yellow. Confusingly, this pigment comes from the tree _Garcinia hanburyi_, and not actually from ivy. The garcinia tree is related to the mango, but instead of a delicious fruit, it produces a highly sought-after resin.

The yellow pigment, also known as _gamboge,_ is extracted from the resin of the tree _._ An incision is made in the tree's trunk, in much the same way as how latex is harvested from rubber trees. But unlike natural rubber, which flows within hours of an incision, it can take up to a year for gamboge resin to flow from the Garcinia tree once it's been tapped.

Another interesting fact about gamboge is that the exact shade of the pigment fluctuated during the twentieth century. This, odd though it may seem, was a consequence of the Vietnam War, as well as the violent rule of the Cambodian Khmer Rouge in the 1980s.

Over the course of bombing and fighting, many Garcinia trees were felled, and their resin mixed with the mud around them. The locals still harvested the product, but as it was more of a toffee color than a bright saffron yellow, it sold much less.

And it wasn't just mud — the exported pigment contained other impurities as well. The paint company Winsor ­& Newton analyzed the pigment packets they received. They found that there were traces of bullets in them, and there was no doubt that these bullets had become lodged in the trees during combat.

If poison and violence already seem to be mixed in with the history of paint, just wait until we move on to arsenic-based pigments!

### 6. Napoleon’s death may be attributable to the arsenic used in green paint. 

Arsenic's use as a poison is well known, featuring prominently in detective fiction. But even the most imaginative crime author wouldn't have connected its use in paint to a Napoleonic mystery.

This particular historical conundrum arose shortly after Napoleon's death in 1821, when one of his doctors took a few strands of his hair.

Napoleon had died in exile on the damp and windy island of Saint Helena in the South Atlantic. His doctors attributed his death to cancer, or possibly to exilic depression.

However, in 1960, the hair was sold at auction and subsequently analyzed. Much to everyone's surprise, traces of arsenic were found.

This opened up an intriguing question: instead of natural causes, had Napoleon died through some sort of poisoning?

Eventually, this arsenical mystery was linked to a contemporaneous green paint.

Prior to Napoleon's time, green paint had been extracted from minerals. However, in 1775, the Swedish chemist Carl Wilhelm Scheele made a discovery while manufacturing yellow paint. He accidentally added some arsenic to the chlorine and oxygen mix and, instead of yellow, a bright green color emerged.

Despite worries about the poisonous component in the paint, Scheele patented and marketed his discovery. It was a hit, and became very popular among the elite of the time.

In 1980, Dr. David Jones — a British chemistry professor — retrieved some the original wallpaper from Napoleon's room on the island of St. Helena. What color was it? You guessed it, green — and with definite traces of arsenic. Dr. Jones posited that damp conditions in St. Helena could have caused the walls to become moldy, releasing the arsenic through a chemical reaction.

We can't be absolutely certain that it was the arsenic-lined wallpaper that killed Napoleon, but we can certainly say it wasn't good for his health.

### 7. True ultramarine blue is highly coveted in many cultures and remains very valuable. 

The National Gallery in London is home to Michelangelo's painting, _The Entombment_ (1501). Beautiful though it is, there's an odd blank section in the bottom right-hand corner. Experts believe that he had saved the spot to paint a kneeling Virgin Mary. The problem was, Mary is traditionally depicted wearing blue clothing, and since blue paint was so expensive at the time, Michelangelo probably couldn't afford to paint her.

Beautiful, deep blue paint is extremely expensive because it has to be extracted from lapis lazuli, a rare semiprecious stone found in Asia, and especially in Afghanistan.

What's more, the pigment extraction process is especially arduous. For starters, lapis lazuli is made of different minerals, so impurities like calcium carbonate have to be removed.

After this, the stones have to be ground into a powder, which is then mixed with gum, resin, wax and oil. The resulting mixture is left to rest for three days, before a blue liquid is pressed out of this putty amalgam.

Finally, the liquid is left to evaporate, leaving a fine blue residue, which is the basis for ultramarine paint. It's quite a taxing process!

But ultramarine paint long predates Michelangelo. In fact, its earliest use could be found on two monumental Buddha statues carved in the sixth century AD into the side of a cliff in Bamiyan, a province in modern-day Afghanistan.

Many Buddhist monasteries were constructed there in that period, and the region was on the Silk Road, the famous trading route. Conditions were thus perfect to create a fabulous work of art.

The statues were crowned with frescoes painted with ultramarine paint extracted from lapis lazuli. Sadly, the Taliban dynamited these statues in 2001, despite the UNESCO World Heritage Site status.

The good news is, not all blues are extracted from stones. Nor do they involve such lengthy and complex production. Let's turn now to a more common shade of blue.

### 8. Indigo dye extracted from plants was used by the ancient Britons in battle. 

These days, getting a tattoo might seem trendy, but it's not a new thing.

Just consider indigo dye, which Britons used to paint themselves before battle. We know they did this because the Romans observed them doing it when Julius Caesar first conquered the region, and when they did it again in the first century AD.

Pliny the Elder noted that the Britons used a weed called glastum, more commonly known as woad, to give their skin a blue hue. Woad is a hardy and fast-growing plant with yellow flowers. Pliny thought the Britons daubed themselves blue along gender lines. He gave two reasons for his discovery: The women, he wrote, used the paint for naked religious rites and for invocations to the gods. The men, meanwhile, stained their skin to prepare for battle.

There were also practical reasons for using woad dye in battle, despite the immense effort it took to prepare it.

Because, it took a _lot_ of work. The Britons gathered the leaves of the woad plant, then crushed and fermented them. A liquid was squeezed out from the mulch, which went through another round of fermentation. The resulting yellow liquid was stored in pots which, once opened, came into contact with air and turned blue again. Only then could it be applied to skin.

So why do this? Woad probably acted as both an astringent and antiseptic. Therefore, if a Briton was wounded in battle, the woad would potentially reduce the possibility of infection. It would also cause the skin around the wound to contract and thus reduce blood loss.

For their part, the Romans used armor.

### 9. Purple is the most imperial of colors, and even played a role in the courtship of Caesar and Cleopatra. 

Though every color has a story to tell, no color carries the same glamor and historical clout as purple.

In fact, purple was even a factor in the romance between Julius Caesar and Cleopatra, ruler of Egypt. In 49 BC, Cleopatra organized a party for Caesar to celebrate his victories over his former ally Pompey. Like all the best parties, this one had a color scheme. Purple stones littered the palace, and the sails of Cleopatra's boat were colored purple. Even the palatial curtains and soft furnishings were dyed purple.

And this was no mean feat. Purple was a phenomenally expensive dye as it was made from rare sea creatures, and this luxury could not fail to impress. Caesar fell head over heels in love with Cleopatra.

From then on, purple took on a more significant role. Caesar was the first to don a toga dyed entirely in purple, and the Romans restricted the wearing of this garment to only the upper echelons of society.

This established purple as the color of the elite in Rome. But it wasn't only Roman law that limited the use of this so-called Tyrian Purple — it was also just incredibly expensive.

Even so, the purple toga was no short-lived trend. Under the emperor Nero (37-68 AD), no one except the emperor himself could wear purple robes, and those who did so anyway would be summarily executed. The emperors Severus (145-211) and Aurelian (214-275) allowed women to wear purple, but still restricted this privilege only to eminent men.

On the other hand, Diocletian (244-311) adopted an altogether different strategy: he actually encouraged the wearing of purple. This was a canny strategy, as this luxurious and sought-after dye was heavily taxed.

In this way, the history of purple is yet another example of how colors add to our understanding of human history!

### 10. Final summary 

The key message in this book:

**When we understand the history of paints and pigments, we can better appreciate the human experience and our engagement with the world around us. Vibrant dyes and hues didn't magically appear, they are the products of historical discoveries, as well as complex and considered processes. To this day, colors are rich with meaning.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Aesthetic Brain_** **by Anjan Chatterjee**

_The Aesthetic Brain_ (2014) explains how and why the human brain responds to beauty and art. These blinks break down the reasons why we instinctively prefer some faces to others, what art does to our brains and how we started making art in the first place.
---

### Victoria Finlay

Victoria Finlay was born in Britain and works as a journalist in Hong Kong. She was arts editor at the _South China Morning Post_ for four years before embarking on her own research into color. This is her first book.

