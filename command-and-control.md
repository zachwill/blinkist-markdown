---
id: 5984f315b238e1000629009c
slug: command-and-control-en
published_date: 2017-08-09T00:00:00.000+00:00
author: Eric Schlosser
title: Command and Control
subtitle: Nuclear Weapons, the Damascus Accident, and the Illusion of Safety
main_color: B63A32
text_color: B63A32
---

# Command and Control

_Nuclear Weapons, the Damascus Accident, and the Illusion of Safety_

**Eric Schlosser**

_Command and Control_ (2013) uncovers the disturbing truth behind the troubled and accident-prone US nuclear weapons program. Find out what's really been going on since World War II, when the first nuclear bomb was invented, and how lucky we are to still be here despite numerous accidents and close calls that could have kicked off Armageddon. If you think the stockpile of nuclear weapons in the United States has always been safely stored under lock and key — think again!

---
### 1. What’s in it for me? Dive into the deadly domain of nuclear warfare! 

In 1991, after the fall of the Soviet Union, the threat of nuclear annihilation that had hung over the world for 40 years seemed finally to have vanished. But the end of the Cold War did not abolish the possibility of nuclear destruction; thousands of nuclear warheads remained in their silos, ready to be launched at a moment's notice.

Unfortunately, not much has changed since then. If anything, the situation has worsened. The nuclear programs of rogue states such as North Korea mean that the possibility of nuclear escalation is now at its highest since 1991.

But how did we get here? And what can we learn from the mistakes of history? The following blinks will take us on a journey through the history of nuclear warfare — from the early days of the Manhattan Project up to the present day.

In these blinks, you'll discover

  * how an international team of scientists developed the first nuclear bomb during WWII;

  * what event in 1983 almost brought the world to nuclear annihilation; and

  * how a TV movie helped convince Ronald Reagan that nuclear war should never, ever happen.

### 2. A nuclear bomb contains a mix of high explosives which trigger a devastating chain reaction. 

On July 12, 1945, in a small New Mexico farmhouse, a plutonium core was placed inside the first nuclear bomb.

This event was the culmination of approximately three years of work conducted by the team behind the _Manhattan Project_ — a group of British, US and Canadian physicists who were put together as an allied response to the German physicists who were busy creating their own super bomb.

The bomb about to be tested in New Mexico housed a plutonium core surrounded by a series of high explosives, each of which pointed inward toward the core. When the explosives detonated, they would all fire at precisely the same instant, thus imploding the core and causing a chain-reaction explosion the likes of which had never before been seen.

After years of intense research, the scientists had eventually discovered that there were two materials that could cause this kind of explosion: uranium-235 and plutonium-239.

Nuclear bombs rely on _fission_ — when atoms split and release energy. Both uranium-235 and plutonium-239 have a high proton count, making it easier to start a fission reaction. The idea was to split the atoms in the bomb's core, releasing a massive amount of energy — and causing an unprecedented explosion.

When fission happens under strictly controlled circumstances, such as in a nuclear power plant, the produced energy can be used to fuel electrical generators.

However, on July 17, 1945, during the first bomb test, no one was exactly sure what was going to happen.

They knew the detonation had to trigger all the explosives at exactly the same time if the reaction was to be successful, so the scientist Donald Horning came up with the X-Unit. This was an electronic triggering device that would simultaneously set off all 32 explosives in the bomb. These explosives completely surrounded the core in the shape of 12 pentagons and 20 hexagons; it looked like a big soccer ball.

Horning's triggering device worked perfectly, and the first test produced a mushroom cloud that bloomed eight miles into the sky. The sight, along with the accompanying roar, caused at least one technician to think that this must be what doomsday would look like.

### 3. After Hiroshima and Nagasaki, the control of nuclear weapons fell to an uneasy mix of civilians and military. 

In June of 1945, World War II was still costing lives in the Pacific. So the question became, how to use the bomb in order to get Japan to surrender? Some were hoping for a public display of nuclear power in an unpopulated area. But there was a risk that the next bomb might fail to detonate.

So instead, the US military, along with the president, decided to deliver an unannounced bombing on Hiroshima.

This occurred on August 6, 1945. The bomb was called "Little Boy," and it was ten feet tall and weighed ten thousand pounds. Its core contained 0.7 grams of uranium-235, an amount that weighed less than a dollar bill but would unleash an explosion equivalent to 12 to 18 kilotons of TNT.

Yet despite the massive death and destruction caused by the bomb, the Japanese Emperor refused to surrender.

So a second bomb, this one with a plutonium core and the code name "Fat Man," was dropped on Nagasaki just three days later.

Remarkably enough, while the bomb was being loaded onto a plane, a technician noticed that it had been incorrectly wired. So a quick and dangerous rewiring was done on the spot, using a soldering iron right next to five thousand pounds of high explosives.

It is believed that only one-fifth of Fat Man's plutonium actually fissioned, but it was still a bigger explosion than Hiroshima, equivalent in yield to 21 kilotons of TNT.

This second bomb resulted in the unconditional surrender from Japan's emperor.

Afterward, international discussions raged. How was this revolutionary weapon going to be controlled?

Believe it or not, there were even some from the military who wanted to ban nuclear weapons. US Air Force General Henry H. Arnold called for the bomb to be immediately outlawed.

Eventually, the Atomic Energy Act of 1946 was issued, giving America's nuclear program civilian oversight through the Joint Committee on Atomic Energy, which was made up of politicians.

These politicians would have to work with a group of military officials who were supposed to act as advisors, yet would continue to push for more control in the decades ahead.

### 4. During the Cold War, an arms race began, and nuclear bombs grew increasingly powerful. 

If there was any hope of a world government — something similar to the United Nations — safely controlling the production and handling of nuclear weapons, it was effectively squashed by the tensions of the Cold War.

There were two events in particular that put the United States and the Soviet Union at loggerheads. In 1948, the Soviets staged a violent takeover of Czechoslovakia's democratic government; and in 1949, the Soviets successfully tested their first nuclear bomb, the RDS-1.

As a result, the US government spent the next 40 years, until the Cold War's end, fearing a Soviet invasion of Europe.

So the big question facing Congress in 1949 was how the United States should respond to such an invasion.

One side argued for conventional warfare; the other, for an "atomic blitz," which relied on nuclear weapons and minimal ground forces.

With the heavy losses of WWII still fresh in people's minds, the atomic blitz side won, and the Air Force's Strategic Air Command (SAC) was formed to prepare for a potential nuclear strike.

Meanwhile, as the Cold War continued, so did an arms race between the two nations.

Both sides started out with the implosion bombs — also known as Mark 3 bombs — that had been used in World War II. But in 1949, the United States began working on the Mark 4, which stored its nuclear core separately from the rest of the bomb. It also featured a safer detonation device that could detect a change in altitude and therefore remain unarmed until it was released from the plane.

But the Mark 4 was soon replaced by the Mark 6, which was ten times as powerful. And then the Mark 6 was supplanted by the Mark 7, which was smaller and lighter but no less powerful.

All the while, both the United States and the Soviets were racing to develop their own hydrogen bomb.

For the United States, the first full-scale experimental test of a liquid-fueled H-bomb was on November 1, 1952. The explosion was equivalent to roughly 10.4 megatons of TNT — five _hundred_ times more than the Nagasaki bomb. This was followed on March 1, 1954, by the famous Bikini Atoll test of a solid-fueled H-bomb that yielded 15 megatons.

And the Soviets weren't far behind; their own thermonuclear test of the RDS-6 occurred in August of 1953.

### 5. As the American stockpile grew, so did the risk of a deadly accident. 

The Strategic Air Command (SAC) took preparedness very seriously. There were countless checklists and endless drills, each designed to ensure that bombs could be loaded onto planes and planes put in the air at a moment's notice.

But this meant that nuclear weapons were constantly being loaded and unloaded from planes that were routinely flying over populated areas.

These routines came with a significant risk of human and mechanical error, and that risk would continue to increase as long as the nuclear stockpile grew.

And after the Soviets launched the Sputnik satellite in 1957, the stockpile increased considerably.

Not only did the satellite launch make the United States feel like they were losing the Cold War; the mystery surrounding Sputnik stoked fears that the Soviets were close to actually using their nuclear weapons.

So the US felt impelled to create even more impressive weapons.

The growing stockpile did raise concerns, however — concerns about both the proximity of storage facilities to densely populated areas and about how much the explosives were handled during training missions.

One primary concern was over "one-point safety," which is how vulnerable the bombs were to having just one of the explosive detonators accidentally go off. There are a number of theoretical scenarios where this might happen, such as a fire causing a short circuit in the wiring or a bullet puncturing the bomb's shell and striking or sparking the explosive.

If just one detonator around the bomb's core goes off, it wouldn't result in a 15-megaton explosion — but it _would_ likely release the core's poisonous radioactive material into the air. And even small traces of plutonium dust can be deadly.

Two of the most vocal proponents for trying to get more safety features built into the bombs were scientists Robert Peurifoy and Carl Carlson, both of whom worked for Sandia, the research and development laboratory that was under contract with the US military.

But many in the government and military were opposed to more safety features. They saw them as increasing the likelihood that the bombs might fail to go off when needed, and retrofitting old bombs was just too costly.

> _"The need for a nuclear weapon to be safe and the need for it to be reliable were often in conflict."_

### 6. Accidents were quite common, and there was little protection against disturbed military personnel. 

As it turned out, the people who were concerned about safety had every right to be, since reports show that accidents weren't at all uncommon.

One of the most dangerous accidents is known as "the Moroccan incident."

In January of 1958, a plane loaded with a Mark 36 hydrogen bomb, capable of a 10-megaton yield, blew a tire and caught fire at an American airfield in Morocco. US Air Force personnel tried to put out the flames that were feeding off a full tank of jet fuel — but couldn't. The base was evacuated. Luckily, the melted 8,000-pound bomb was recovered two hours later and the radioactive material was buried.

A month later, a Mark 6 atomic bomb was accidentally jettisoned from a plane flying over South Carolina. Thankfully, the fissile core was not inside the bomb, which detonated upon impact, leaving a 50-foot crater in the yard of a home, tearing apart the house and a car. Incredibly, the family suffered only minor injuries.

Later that year, the first detailed safety report on nuclear weapons was issued, and it revealed that an average of seven bombs were being accidentally dropped every year. On top of that, there were twelve crashes or accidents that involved a plane carrying a nuclear bomb.

The report suggested that these accidents would only increase as the stockpile grew, and things would get even worse as the bombs, missiles and rockets got more complex.

Another worrisome factor was the psychological element.

Throughout the 1950s and 1960s, there wasn't a thorough screening process for the military personnel tasked with handling these weapons. So there was a genuine threat that someone drunk, high or in the throes of a psychotic episode could accidentally or purposely detonate a nuclear device.

Indeed, the European stockpiles were especially vulnerable.

Amazingly, there were hardly any security measures in place to stop a pilot in Germany from taking off in a plane and launching a nuclear missile; or for a soldier in Italy to knock a guard unconscious, take the key from around his neck and launch a rocket.

Either one of these acts could have easily started a nuclear Armageddon.

> _"...in 1957, an atomic bomb or a hydrogen bomb was accidentally jettisoned once every 320 flights."_

### 7. Accidents continued throughout the 1960s and 1970s; B-52 planes were especially dangerous. 

You don't need to have military expertise to have heard of the B-52 bomber, but you may not know how problematic these planes were.

Despite their frequent malfunctions, however, they were the main transport vehicle for nuclear bombs, resulting in some especially dangerous incidents.

In 1961, a malfunction caused a B-52 to crash into a barley field in northern California after the crew safely evacuated. It was carrying two Mark 39 hydrogen bombs that luckily shattered without incident.

Just weeks later, a B-52 was flying over the Appalachian Mountains when turbulence caused its tail to break apart. The plane was carrying two Mark 53 H-bombs. It crashed into the side of a mountain, and only two of the five crew members survived. The bombs were recovered from the wreckage, undetonated and intact.

And in 1966, a mid-air collision occurred when a B-52 was attempting to refuel over the coast of southern Spain.

The fuel airship exploded, killing the entire crew. The B-52, carrying four Mark-28 H-bombs, broke apart, killing two crew members. It took over a month to locate all four bombs; all the while, the United States denied there was any threat. Yet one bomb had partially detonated, excavating a 20-foot crater near a cemetery and releasing a cloud of plutonium onto a nearby farm.

These are just some of the many accidents involving B-52s.

In the 1970s a different kind of threat emerged as military servicemen began to take part in the growing drug culture. Even the ones who handled nuclear bomb storage weren't immune to the cultural changes.

At one Air Force base in North Carolina, 151 of 225 security personnel were arrested on marijuana charges, and on Missouri base, over 230 officers were arrested for either using or dealing drugs on site.

During these years, it was common for hashish, heroin and LSD to be discovered in the barracks and workstations of military personnel.

### 8. Increased efforts to develop a reliable system of command and control didn’t necessarily make things safer. 

The desire to efficiently command and control US nuclear weapons led to some impressive developments. In the 1950s, NORAD became the first computer network, its purpose being to link the radar systems of Air Force bases across the United States to detect a Soviet first strike.

But there was another computer being used to program the US nuclear response. The Single Integrated Operational Plan (SIOP) dates back to the late 1950s when an early IBM computer processed a list of targets to come up with a plan for optimal destruction. Its precise goal was to ensure the complete annihilation of the enemy with a 75-percent probability.

On December 2, 1960, SIOP called for 3,423 nuclear weapons to be directed at 1,000 different ground zeroes. The plan would eliminate 3,729 targets and along with an estimated 220 million lives, with far more dying from the subsequent fallout and aftereffects.

Another disturbing thing about SIOP is that once it starts, there's no stopping it. When newly elected presidents throughout the years have been shown this plan, they're usually shaken and deeply disturbed by the massive level of overkill on display.

What's also disturbing is that, on more than one occasion, SIOP came close to going into effect.

In 1962, during the Cuban Missile Crisis, the SAC was ordered to be at DEFCON 2, the level of readiness just below imminent nuclear war. Sixty-five bombers were in the air, and multiple nuclear submarines were in striking position. Fortunately, the Soviet Union agreed to remove their weapons from Cuba, and the crisis was averted.

What's more troubling is the number of accidental occasions when faulty computers have nearly triggered war.

On multiple occasions, NORAD mistakenly identified incoming Soviet missiles. Once, this was due to the accidental running of a test program that was believed to be a real scenario. But thankfully, cool heads prevailed.

> GPS was originally invented in the 1980s as part of the nuclear defense program.

### 9. On September 18, 1980, a small mistake led to an emergency at a missile silo in Damascus, Arkansas. 

According to a report in 1968, there were an average of 130 significant accidents per year that involved nuclear weapons. That's 1,200 incidents between 1950 and 1968, with 13 of those incidents getting labeled as "broken arrows" — when a weapon gets accidentally fired or detonated and releases radioactive materials.

Sure enough, these incidents continued into the 1980s, which is when one of the biggest accidents took place.

It happened on September 18, 1980, at an underground missile silo in rural Arkansas, where an active Titan II rocket was being maintained.

This rocket carried a W-53 warhead, the most powerful one produced in the United States at the time. According to a list provided by Robert Peurifoy of the Sandia Laboratory, it is also one of the nuclear weapons with the highest risk of accidental detonation.

But the Titan II rocket comes with other inbuilt dangers. Its fuel tank is especially dangerous, containing highly toxic liquid propellants and an oxidizer that releases an even deadlier explosive vapor.

On the evening of September 18th, the maintenance crew was doing a routine fill-up of the oxidizer tank, when a metal socket was accidentally dropped; it fell 70 feet before striking the side of the rocket, and immediately, a nasty looking cloud of vapor began to fill the underground silo.

Alarms and warning lights went off, but the staff were confused and started to panic because, even though they had checklists to follow for every procedure, there wasn't one for this scenario.

Orders came to evacuate the silo, including the control room, even though it was protected by massive blast doors and sealed off from the vapor.

With everyone evacuated, hours went by as a toxic cloud of rocket fuel vapor rose up into the sky. Soon, people and news reporters began to gather around the access road to the silo.

Obviously, something needed to be done quickly, but the Air Force and SAC had a thorough bureaucratic process to follow. You'll discover how this turned out in the next blinks.

### 10. A number of poor decisions resulted in the explosion of the Titan II missile and the death of an Air Force officer. 

Jeff Kennedy was one of many people who received an emergency phone call that night. He was a highly respected Air Force missile mechanic who knew the Titan II inside out, and once he arrived on the scene, he knew what needed to be done.

But no one was ready to approve his plan, and nothing was going to happen until all proposals were passed up the bureaucratic chain of command. This frustrated Kennedy, who knew that waiting would only make the situation worse.

In total, it took nearly eight hours for a plan to be approved, and it started off with a bad choice. Technicians were ordered to enter the silo using the main access door instead of the escape hatch, which would've been a whole lot quicker, easier and safer.

But those in charge erroneously believed that people wearing protective suits and gear wouldn't be able to fit down the escape hatch. Kennedy knew that it was a mistake, but he put on his gear and followed the orders.

With the help of his friend and fellow technician, David Livingston, Kennedy slowly opened the series of blast doors. Once inside, they used a portable vapor reader to measure the air; the needle on the device maxed out at over 21,000 parts per million. The air was toxic enough to start melting their suits.

Rightfully, Kennedy and Livingston were ordered back outside. But then the final bad decision of the night was made. An order came to turn on the ventilation fan to clear the vapor from the silo.

Kennedy knew that it would only take a tiny spark to instantly turn the silo into a roaring fireball, but Livingston agreed to turn the switch and activate the fan. And sure enough, just after Kennedy stepped outside, the place exploded, sending a tower of fire into the sky, briefly turning night into day.

### 11. After the Damascus incident, a more dangerous threat emerged under the Reagan administration. 

Remarkably, David Livingston was the only person to die that night, although many others were also blown off their feet and suffered severe burns. The warhead, which was also blasted into the air, landed in a ditch right next to the access road.

The official report of the incident blamed the low-ranking officers involved and decided that the Titan II was still "basically safe." It was clear that very little would change during the 1980s.

Indeed, the most dangerous year in nuclear history, 1983, was still to come.

Mere months after the Damascus incident, Ronald Reagan was elected president, and his administration committed $1.5 trillion to a defense budget that included growing the nuclear arsenal.

One of these weapons was the Pershing II, a missile with a mere 1,000-mile range that was scheduled to arrive in West German bases in November of 1983. With a range that small, it was clear to the Soviets that these missiles had only one purpose — to annihilate Moscow in under ten minutes.

Making tensions worse, the Soviet Union invaded Afghanistan, and the United States began a campaign of psychological warfare by routinely sending SAC bombers into Soviet airspace and conducting Navy exercises close to Soviet bases.

Then, on September 1, 1983, the Soviet Union destroyed a Korean Airlines flight after it accidentally entered Soviet airspace, killing all 269 passengers.

A month later, the United States sent troops to Grenada to overthrow a Soviet-backed communist regime.

But the most dangerous incident occurred a week later when NATO conducted a drill called _Able Archer 83_, which was designed to simulate the authorization of nuclear warfare.

With the delivery of the Pershing II only weeks away, the timing for this simulation couldn't have been worse. When the KGB overheard communications from the drill, they thought they were hearing the real thing, bringing the Soviet Union perilously close to launching a preemptive attack.

Luckily, the drill ended soon, alleviating the worst fears of the Soviets.

### 12. More safety precautions were taken in the 1990s, but a very real threat is still present. 

On November 20, 1983, Ronald Reagan and millions of other Americans watched a made-for-TV film called _The Day After_. It was a realistic portrayal of how average people would cope with the outbreak of nuclear war, and Reagan was among those extremely shaken by what they had seen.

When the president appeared on TV shortly after seeing the film, his rhetoric had dramatically changed. He announced that a "nuclear war cannot be won and must not be fought."

Reagan's new attitude was matched by that of the new Soviet leader, Mikhail Gorbachev, and the two began working to reduce their nuclear programs and finally put an end to the Cold War.

Also, after decades of warnings, the military finally heard Robert Peurifoy and agreed to spend money on retrofitting some of its stockpile with improved safety devices.

It was clear how needed these improvements were when the House Foreign Affairs Committee graded the safety of every type of weapon in the US arsenal. On a scale from A to D, only three got an A, and 12 received a D.

More recent safety additions include a barcode system that was introduced to prevent the accidental transport of armed weapons on training missions. This is exactly what happened in 2009 when six nuclear missiles sat in an unguarded plane for two days.

There are still many improvements to be made, but the greatest risks may lie elsewhere.

While Russia possesses around 3,740 nuclear weapons, France around 300, China 240 and the United Kingdom 160, it's the nations of India and Pakistan that could pose the biggest threat.

These countries aren't exactly friendly neighbors, and Pakistan recently doubled its arsenal to around 100 weapons. But the added threat here comes from the number of Islamic radicals in the area who have already made multiple attempts to steal a nuclear weapon and might one day succeed if security isn't tight enough.

### 13. Final summary 

The key message in this book:

**Nuclear weapons are not something that can be safely controlled with strict bureaucratic checklists and procedures. As long as computers and humans are involved, something is bound to go wrong. Humans will make mistakes and computers will malfunction, and the more secretive nations are about those mistakes and the technology behind their weapons, the harder it will become to improve safety and design, making all of us more vulnerable to a catastrophic accident.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Atomic Accidents_** **by James Mahaffey**

_Atomic Accidents_ (2014) explores the evolution of one of the most fascinating and yet controversial technologies of our times, nuclear energy. These blinks explore the development of nuclear technology and reveal the details behind the tragic accidents that occurred along the way.
---

### Eric Schlosser

Eric Schlosser, an investigative journalist, is the best-selling author of _Fast Food Nation_. His work has also appeared in the _New Yorker_, _Vanity Fair_ and the _Atlantic_, among other publications.

