---
id: 58f140cb86c51e0004dd644d
slug: common-sense-pregnancy-en
published_date: 2017-04-18T00:00:00.000+00:00
author: Jeanne Faulkner
title: Common Sense Pregnancy
subtitle: Navigating a Healthy Pregnancy & Birth for Mother and Baby
main_color: 81CFDA
text_color: 446D73
---

# Common Sense Pregnancy

_Navigating a Healthy Pregnancy & Birth for Mother and Baby_

**Jeanne Faulkner**

_Common Sense Pregnancy_ (2015) reveals all you need to know about the surprises that await you during the magical experience of pregnancy and childbirth. There are many things to consider, including what to eat, who to call for help and what kind of childbirth you want to have. So empower yourself with knowledge and get a good idea of what to expect.

---
### 1. What’s in it for me? Prepare yourself for the challenges of pregnancy. 

Are you pregnant or the partner of someone who is? If so, you probably have a lot of questions. You might wonder when you're supposed to see a doctor, what you're supposed to eat or whether you should choose a midwife or a doula.

These are all valid and important questions. These blinks are here to help. Not only will they give you the answers to these questions but also guide you through many other dos and don'ts of pregnancy.

In these blinks, you'll learn

  * how much nausea is _too_ much nausea;

  * if and when labor induction might be relevant; and

  * which challenges you might encounter after giving birth.

### 2. You don’t need to see a doctor as soon as you become pregnant, unless certain problems arise. 

Finding out that you're pregnant can lead to a number of overwhelming thoughts. And your first reaction to learning you're pregnant might be to get in touch with your doctor. But don't be surprised or concerned if your doctor sets an appointment for six to eight weeks down the line.

While it's natural to be concerned, there's no need to rush yourself to a doctor the minute you become pregnant. The best thing you can do is be patient and make the most of these first few weeks.

Since it's too early for ultrasounds or screening exams, there isn't much for a doctor to do at this stage. But there are some simple things _you_ can do to help ensure a healthy pregnancy.

Prenatal vitamins are a good source of folic acid and iron, which are important at this stage. And you can also use this time to select a health-care specialist that you'd like to have by your side throughout your pregnancy. It's best to choose someone you're absolutely comfortable with.

There are also important signs you should not ignore — including bleeding or severe nausea — which do require an early doctor's appointment.

Some pregnant women can suffer from a case of severe nausea called _hyperemesis gravidarum_, which requires medication and rehydration.

Another symptom to be aware of is vaginal bleeding. This is known as _spotting_, and 25 percent of pregnant women experience it during the first trimester (that is, within the first 13 weeks).

If the spotting is light, be calm and see what happens. If the spotting persists or becomes stronger, or if you're not certain whether it's getting stronger, now is definitely the time to call your doctor, since it could be the sign of a miscarriage. A doctor will let you know what the next step should be.

In the next blink, we'll take a closer look at how to find a pregnancy specialist who can make the next nine months a little easier.

### 3. Midwives and doulas can offer valuable services, but make sure you choose wisely. 

If you're planning on giving birth in a hospital, you might be in for a surprise. For, when the time comes, you'll probably find yourself surrounded by strangers — the random doctors and nurses that happen to be on duty that day.

Just giving birth is stressful enough, so you might want to consider hiring your own team to help you feel as comfortable as possible.

Most pregnant women choose an obstetrician, but a midwife is often the better option.

According to the American College of Nurse-Midwives, nearly 92 percent of US women pick an obstetrician, even though obstetricians specialize in dealing with high-risk and complicated pregnancies that very few women have.

So if you're expecting a normal pregnancy, the better option is to find a midwife whom you're comfortable with.

One of the primary benefits of having a good midwife is that she'll have more time to provide you with personal care. Midwives can also competently handle all aspects of giving birth; they can help you avoid undergoing a C-section as well as attend to any trauma or injuries that may occur. And if you're concerned about any complications, don't worry — midwives are trained to recognize situations where an obstetrician is required.

In 2013, gynecologist Nancy S. Roberts conducted comparative studies between the midwife model — which is prevalent in Europe — and the popular obstetrician model of the United States. The results clearly showed that, by using a midwife, there is less risk of illnesses and fatalities for both the baby and the mother.

Another good option is to find a _doula_. Doulas are childbirth specialists, and they can provide invaluable assistance in getting through the labor process.

However, since there are no regulations regarding doula certification, their qualifications can vary greatly. So it's important to do your homework and check their background.

It's best to find a doula who is familiar with how hospitals handle patients in labor. That way, you'll avoid hiring someone who may get into a heated disagreement with hospital staff while you're suffering labor pains.

> _"Do you need a doula? No, you don't technically need one, but the right doula can be a real asset."_

### 4. Since diets were healthier in the past, pregnant women now need to be strict about what they eat. 

Believe it or not, 50 years ago there weren't any detailed lists enumerating the foods that pregnant women shouldn't eat. Pregnant women ate just about whatever they wanted.

This might sound worrisome, but the fact is that people had healthier diets back then, so there was far less risk of a woman gaining too much weight during pregnancy and thus endangering her child's future.

One of the best examples is high fructose corn syrup, which simply wasn't used in products in the past. Today, however, it's used in almost everything, and its prevalence has tripled the amount of calories in staple foods.

Past foods also had far less preservatives, chemicals and artificial flavors. Not to mention that the portions were a lot smaller back then.

During the 1950s and 1960s, pregnant women typically wouldn't gain more than 30 pounds. And if they did, their doctors weren't afraid to be condescending and scold them for it. This might sound unprofessional by today's standards — but a brief lecture is better than the 60 to 80 pounds that many pregnant women put on nowadays. Weight gain isn't only detrimental to their own health; it also puts the future health of their child at risk.

This is why we have those detailed lists to help guide pregnant women toward a healthy diet. 

Some of these diets seem complicated, but if you stick to it and follow the rules, they can really provide the optimal nutrients for an expecting mother.

Part of a successful diet is knowing what to avoid. Things like soft cheeses, unpasteurized beverages and raw or undercooked animal products can contain seriously harmful bacteria and toxins.

To maintain a good diet, it's also important to know which foods contain the vitamins and nutrients that expecting moms need for optimal health. These include fruits and vegetables, whole grains and a good amount of lean protein. Ideally, every meal should have these elements, as well as things rich in calcium, such as milk, leafy greens and tofu.

### 5. Checking on amniotic fluid levels is an important reason for pregnancy checkups. 

A fetus in a womb is not unlike a fish in a puddle. If the fish is going to survive, there needs to be enough water.

Part of the reason for having routine checkups with your doctor is to make sure there's enough amniotic fluid in your womb and to ascertain whether or not you have _polyhydramnios_, a rare condition where the womb contains too much fluid.

Only one percent of pregnant mothers will experience this condition, and, even in them, it's usually harmless and will go away on its own. However, it has been known to cause some serious complications, such as premature births, stillbirths or having the baby get stuck in an uncomfortable position. 

Unfortunately, the reasons behind this condition are mostly unknown.

Fluid levels are generally regulated by how the baby swallows, so it's believed that the condition might be due to infections or other health issues that might interfere with development and swallowing. Another theory might be related to the mother having diabetes, yet that only accounts for half of the cases. The rest remain a mystery.

Mothers can also experience _oligohydramnios_, the condition of having less amniotic fluid than there should be. This is also rare — four percent of pregnant women experience it — but it can have dire effects, including miscarriage and still or premature births in the early stages. In the later stages, especially after the water breaks, it can cause the umbilical cord to get pinched, which restricts the blood flow and oxygen supply to the baby.

There are two potential signs of oligohydramnios that you can look for. If you're leaking fluid or haven't been gaining weight, check with your doctor to make sure your amniotic fluid levels are okay.

### 6. Inducing labor is common, but it should only be done in emergencies. 

If you're at the beginning of your first pregnancy, you're probably dreading the idea of going into labor. Yet when those final weeks of pregnancy arrive, and the discomfort has reached its peak, most women can't wait for labor to start. 

The last few weeks of a pregnancy are an exhausting mix of cramps and aching and swollen body parts. It can feel intolerable, and, to make matters worse, the baby is regularly punching or kicking a vital organ every time it moves. No wonder that inducing labor is such a popular procedure.

According to the obstetricians Osterman and Martin, between 1990 and 2012, induced labor increased dramatically, from just 9.5 percent of pregnancies to 23.3 percent.

While it's understandable, the truth is that artificially induced labor is being over-prescribed and should be reserved for emergencies only.

A naturally occurring labor prepares the mother's body for giving birth, whereas artificially induced labor usually leads to a C-section operation, which is far more likely to cause injuries or even death. This is why inductions should be limited to emergencies and instances when the mother or the child's life is at risk.

An appropriate case for C-section is when the mother's blood pressure is reaching dangerously high levels.

According to Dutch gynecologist Corine Koopmans, high blood pressure is closely related to birth complications, since it can weaken the mother's organs, especially the kidneys. As a result, the baby can become weak, underweight and susceptible to injury. In cases like this, both the mother and the baby are better off with induced labor.

### 7. It’s important to know the difference between pre-labor and actual labor. 

No matter what you read or who you talk to, there's really nothing that can prepare you for the real experience of going through labor and giving birth.

But one thing you can prepare for is differentiating between labor and pre-labor.

Pre-labor pains can start as late as ten days prior to your expected due date, and the pain can leave you feeling so horrible and exhausted that you'll be 100-percent positive that your labor has begun.

But what's more likely is that you're only a little bit dilated, and that these initial contractions are only helping to get the baby into the right position for delivery. It's important that the child be as low as possible in the pelvis.

As painful as this can be, if you're still able to breathe and speak normally, and are able to move around to get things done, you likely have another day or two before the real labor starts.

Another way to tell that contractions are just pre-labor is to lie down in a hot tub or go for a walk — if the contractions are not regular, or if they settle down and become less painful, it is not real labor.

When the actual labor begins, the pain will be much worse.

At this point, the contractions will no longer feel like bad menstrual cramps. You'll feel excruciating pain shoot along your back and legs, and the contractions will come at faster intervals until they're happening every two or three minutes, with only a minute of respite in between.

When this happens, it's time to get yourself to the hospital.

### 8. Breathing techniques are essential for managing labor pain, and baths and compresses also help. 

If you've ever seen one of the countless movies or television shows featuring a character who goes into labor, then you probably know that being in labor is made easier by employing certain breathing techniques.

How you breathe is essential for managing labor pain. 

The most popular breathing technique that you'll see in movies is the _Lamaze method_. This method involves inhaling twice and then exhaling twice, while making the sounds "hee-hee" (when you breathe in) and "hoo-hoo" (when you breathe out). 

It's a tried-and-true method, but really any kind of deep, rhythmic breathing will help you get through labor.

The important thing is to focus your attention away from the pain by concentrating on your breathing. This can also relax the mind, increase the flow of oxygen and provide a helpful boost of energy.

But if you really want this technique to work when it matters most, you will need to practice.

Whether you're going with Lamaze or any other technique, you should practice it for the duration of your pregnancy. Remember, when it comes to the pain and excitement of childbirth, you'll have a lot of distractions to cope with. But you will find, that with practice, an effective breathing technique will help you dilate much quicker than you might expect.

Another method of easing the labor pains is to use a bath.

Lying in hot water is one of the simplest and most effective ways to relax and provide much needed pain relief. Ideally, you should be able to fill the water in your tub or Jacuzzi high enough to cover your entire belly, but if this isn't possible you can use a towel to cover the exposed area and keep it warm.

Nowadays, many hospitals provide the option of having a water birth, either in a tub, a Jacuzzi or an inflatable pool. So if this sounds like the procedure for you, make sure to do your research and find a hospital that offers it.

### 9. Don’t expect pains and complications to go away after childbirth. 

You might think that after going through the painful ordeal of childbirth you deserve a nice relaxing break, right? Unfortunately, life has other plans. 

There are a number of physical ordeals and discomforts awaiting you after birth.

Don't be surprised when, during the first six weeks following a birth, you experience cramps, bleeding and a soreness in your lower abdomen and vagina. Feeding your child is an ordeal. Breastfeeding often entails swollen, bruised and leaky breasts with dry and cracked skin.

And then there's the sleep deprivation. You should be thankful if you can snatch two hours of rest before being interrupted by your child's cries for milk and attention.

Fortunately, there are some simple steps you can take to help you through this trying time.

Warm baths are a great way to cope with the common swelling and soreness that follows a traditional, vaginal birth. Another useful tip is to buy extra-large hygiene pads, soak them in water and put them in the freezer. A couple of times a day, you can place an ice pad in your underwear for about 15 minutes for some soothing relief.

Of course, there is also pain-relief medication, such as ibuprofen, Vicodin and lidocaine.

These are especially useful when trying to deal with the strong pain following a C-section delivery. This pain can last for a few weeks, and you shouldn't hesitate to take medication if it's been prescribed to you. However, you should remain in contact with your health care provider during this time, as there is a risk of becoming addicted.

If all this makes it sound like childbirth is a lot of pain and hard work, well, that's because it is. But there are amazing rewards to be had as well, and once your child is in your arms, you'll know that it's all been worth it.

### 10. Final summary 

The key message in this book:

**Pregnancy and childbirth are experiences that are both beautiful and challenging. So it's best to be well prepared. Get organized so that you can make your birth as easy and comfortable as possible.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Art of Waiting_** **by Belle Boggs**

_The Art of Waiting_ (2016) details the social narratives surrounding birth, pregnancy and parenting. These blinks offer poignant personal anecdotes alongside historical examples to shift the spotlight onto the often unheard stories of adoption, _in vitro_ fertilization and forced sterilization.
---

### Jeanne Faulkner

Jeanne Faulkner is an expert in women's health with years of experience as a labor nurse. She is also the mother of four children. Her writing can be found at FitPregnancy.com.

