---
id: 55c0f04c3439630007750000
slug: common-sense-en
published_date: 2015-08-05T00:00:00.000+00:00
author: Thomas Paine
title: Common Sense
subtitle: None
main_color: A16338
text_color: 87532F
---

# Common Sense

_None_

**Thomas Paine**

When it was originally published, _Common Sense_ (1776) came in the form of a pamphlet, which George Washington read to his troops during the American Revolutionary War. These blinks are a window into the political mind of one of America's founding fathers, and will explain the logic that led to the American revolution, as well as to the establishment of an entirely new government.

---
### 1. What’s in it for me? Learn why representative government is common sense. 

Imagine you lived in British Colonial America. Back then, you could be taxed and wouldn't be able to do anything about it. The government, located an ocean away, could take your property and leave you helpless. Would you accept this state of affairs?

Of course not, and Thomas Paine didn't either. Instead, he wrote a pamphlet, _Common Sense_, which outlined what perfect government would look like.

So, how do you go about constructing a society the right way? These blinks will show you how.

In these blinks, you'll discover

  * why societies need rules to steer people's behavior;

  * why representation is needed as societies grow; and

  * how England crippled the American economy prior to the American Revolution.

### 2. Human existence depends on helping each other, which gives rise to societies and rules to guide them. 

How many people does it take to screw in a lightbulb? Truthfully speaking, probably just one. While simple tasks of household maintenance can be accomplished alone, many other jobs in life require us to enlist the help of others.

That's because to realize their potential, humans need to live and work with other people. Just consider how many things we're required to do that rely on others for their success. In the author's time, the late 18th century, that might have meant moving logs, building a house or plowing a field. Today, it's reflected in effective product design that demands a team effort, or a delicate surgery requiring a group of doctors.

But societies don't come about just to simplify tasks requiring many hands; they also exist because we rely on others for survival at difficult times. For example, when we get sick, we need other people to take care of us.

Imagine living in the author's time and coming down with a high fever. With nobody to bring you water you'd die. While in modern times this threat is much less common, we still rely on people to bring us food and pick up our prescriptions when we're sick.

It's for this reason that we're all invested in society prospering, and that means having guidelines. Why?

Because nobody except God is without vices and flaws. Moral shortcomings have been part and parcel of being human since we were expelled from the garden of eden.

This immoral impulse, an inherent part of our existence, causes us to harm others, since we disregard their well-being in favor of our own success. The danger of this threat necessitates rules in any human society; these are laws — statutes that govern how to behave and treat one another.

### 3. Representative government, not monarchy, is the best way to rule a society. 

We all know how difficult it can be to make simple decisions with colleagues and family members. Even small tasks like deciding what kind of takeout to order can turn into huge debates. But imagine trying to make the same decision with thousands of people. Impossible, right?

For this reason, as a society grows, representative governance becomes a necessity. Since making decisions in big groups is nearly impossible, larger societies require a different way to make decisions.

The best way to do so is to designate _representatives_ for different groups in society through elections, in which people vote for others who share their views.

But one election won't ensure that voters are represented. For this, elections must be held often, because they connect representatives with those who elect them. Therefore, having frequent elections keeps the people who govern a society attuned to the issues that are most important to their constituents.

So what does this say about a system of governance with no elections, such as a monarchy? It proves that monarchy is the worst form of government. 

All people are created equal, and to elevate one above the rest contradicts the will of God. Therefore, it's contrary to nature and God's will for a society to be governed by an omnipotent ruler, like a king.

But that's not the only problem with monarchy — another issue is its hereditary basis for transferring power.

Even if one monarch was benevolent, what's to say that his heirs will be the same? Instead of getting a good ruler, you might get a child or even a complete lunatic ruling over an entire society. Unlikely? Well, both actually happened within the English line of succession!

In fact, being born into power can itself be corrupting. An absolutely sane person might be driven mad by the knowledge of their unchallenged future rule.

Systems with elections are a more egalitarian form of governance, but to really see the evils of monarchy and the benefits of representative government in action, it helps to take a look at real-life example: the American Revolution, an event that the author himself helped instigate.

> _"In the early ages of the world there were no kings; and there were no wars; it is the pride of kings which throws mankind into confusion."_

### 4. America would be better off without England and should claim its independence. 

You know how every teenager reaches a stage where they need their independence? The same goes for certain countries. Sometimes, when a country comes of age, it demands its own freedom, and no example is clearer than the case of the United States and Britain.

It's obvious that no country should be able to claim ownership over another or its people. As such, the British claim to America was based on the assumption that the American population was British. Unfortunately for Britain, this turned out to be untrue; people identify as whatever they want to, and in this case, they wanted to be American.

For instance, plenty of people in England have French ancestors, but that doesn't make them French. And even if Britain was the "mother country," the way she treated her child was unacceptable.

By imposing harsh taxes, the English monarchy crippled America's economy, and people who stood up to the unfair taxation were punished in the cruelest of ways. Some of those who protested the taxation were even killed by the British at the Boston Massacre!

But what made the heavy taxes even more insulting was the fact that British America had no representation in the British parliament. Therefore, Americans had no say in the laws governing them, making British rule tyrannical.

The result?

A country in the service of another is bound to lose economic power, and America was no exception. Britain forbade many American ports from trading with countries like France and Spain when Britain was at war with them. This impinged on the freedom of Americans by preventing their ports from growing into the thriving trading posts they could have been, which would have enabled them to buy and sell goods from all over the world.

In fact, trade with other countries would have been easy, as the resources available in America were coveted the world over. Although they covered only one-eighth of their modern-day territory at the time, the Americas boasted stocks of iron, furs and various other goods that were in high demand.

### 5. Final summary 

The key message in this book:

**By 1776, America had become a land oppressed by its colonial rulers, Britain. Thomas Paine's simple, common-sense arguments regarding natural law and the will of God revealed that the only possible course of action was for America to claim independence and establish its own government. In order to fulfill its potential, this government would have to be a representative one with frequent elections.**

**Suggested** **further** **reading:** ** _The Black Jacobins_** **by C.L.R. James**

_The Black Jacobins_ traces the remarkable history of the revolution in the French colony of San Domingo (modern day Haiti). It describes the events that helped the revolution become the first successful slave rebellion in history.

In particular, _The Black Jacobins_ views the events through the prism of the revolution's greatest figure, Toussaint L'Ouverture. It shows how he, a former slave who was inspired by the ideals of the French Revolution, successfully defeated the European empires and helped to destroy the brutal practice of slavery in San Domingo.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Thomas Paine

Thomas Paine was born in England in 1737 and, sponsored by Benjamin Franklin, moved to the Americas two years before publishing _Common Sense_. He was a founding father of the United States and was deeply involved in the political life of the young country. His other titles include _The Rights of Man_ and _The Age of Reason._

