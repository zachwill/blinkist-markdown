---
id: 54fd8f85303766000a050000
slug: common-stocks-and-uncommon-profits-and-other-writings-en
published_date: 2015-03-09T00:00:00.000+00:00
author: Philip A. Fischer
title: Common Stocks and Uncommon Profits and Other Writings
subtitle: None
main_color: 84A36A
text_color: 465738
---

# Common Stocks and Uncommon Profits and Other Writings

_None_

**Philip A. Fischer**

_Common Stocks and Uncommon Profits_ gives you all the information you need to make smart investments, regardless of your investment style. Whether you're looking for huge profits or simply to maintain existing funds, this book shows you the path to success.

---
### 1. What’s in it for me? Learn the secrets to success of high-risk and conservative investing. 

While the stock market goes up and down from year to year, it's good to know that some of the fundamentals of investment theory have remained consistent for generations.

Although _Common Stocks and Uncommon Profits_ was published in 1956, the advice it offers on investing is just as relevant as it was when Eisenhower was U.S. president.

These blinks lay out the key characteristics of what you should look for when investing in a company, and helps you determine whether you're a high-risk investor or a conservative one.

Just like a detective, a successful investor does her homework and digs deep to get all the pertinent information before putting her money on the table!

After reading these blinks, you'll know

  * how to determine if a company stock is overvalued;

  * why stock price doesn't tell you whether a company is actually any good; and

  * how to conquer doubt and not hesitate when it's time to buy.

### 2. Smart investment strategies focus on companies with long-term growth potential. 

The common perception of investing is that it is fast-paced and brutal, with investors buying and selling seemingly on a whim, seeking quick profits above all.

Yet _smart investing_ involves much more thought and planning, and is ideally focused on the long term. Smart investors don't seek quick profits but instead looks for companies with growth potential that over time will multiply an initial investment.

It isn't easy to identify which companies offer this kind of growth potential. Indeed, many stocks are either over- or undervalued, which can make investing difficult.

Smart investors look for companies with potential that are nonetheless still undervalued, as such companies can, when the time is right, grow with such rapidity that an investor can double or even triple his initial investment.

Luckily, companies with growth potential can be recognized by their common characteristics.

Such companies offer products and services that ideally could sustain high sales volumes for at least a few years. Companies with good growth potential also invest in research and development, to continue growing even when a current product line no longer offers opportunities for growth.

The 1950s, for example, was a great time for television manufacturers. But by the middle of the decade, nearly everyone who wanted a black-and-white TV already had one. This meant that, until the color television was developed, TVs companies faced flat growth; so they had to adapt.

Motorola, now known for mobile phones, used to produce televisions and radios. But management had the foresight to harness the company's technical skills and experience to enter the two-way communication business, and continued to grow sales while other TV manufacturers' sales flatlined.

Companies with high growth potential also have a solid management team and good employee relations. Don't invest in a company whose employees are too busy squabbling to be productive, and whose executives can't inspire solidarity or a shared vision in the workforce.

> _"Common stocks are usually of greatest interest to people with imagination."_

### 3. Do your homework! Research a potential company from every angle you can before you invest. 

Successful investing is a lot like the work of a detective, as you need to research your topics thoroughly and analyze all available data to be effective.

When considering a company's investment potential, you'll want to collect detailed information about the company from every possible angle.

Of course, you could easily track down a trader and ask her which companies she would recommend investing in. However, there is no guarantee that she'll give you accurate information, because traders have to protect their own interests.

Instead, employ the _scuttlebutt_ method: dig for information from every possible source.

Contact vendors, customers, former employees and research scientists or executives in trade associations. You'll be amazed at what you find, as long as you ensure that you won't make the information public or out your informant.

Contacting a few of the company's competitors could also yield surprisingly accurate and detailed information.

Once you've developed a profile of the company, it's time to contact company management directly to ask them informed questions.

The scuttlebutt method is accurate, yet time-consuming. To avoid wasting any time, choose the companies you wish to research very carefully.

This will require you to pre-select companies that could offer the kind of growth potential you want for your investment.

Start by simply talking to friends or other investors, looking at printed materials and asking yourself which companies seem promising. If you read in the newspaper, for example, that a company's manager is resigning because of some questionable dealings, then you won't want to waste your time on that company!

> _"The successful investor is usually an individual who is inherently interested in business problems."_

### 4. Look for dips in a stock’s price to get in cheaply, to ride high when the company grows. 

Once you've identified a company with good long-term growth potential, is it time to buy?

As mentioned earlier, stocks are often over- or undervalued, so how can you ensure that you get the most bang for your buck?

Stock prices mirror the financial community's perception of a stock's value at the current moment, and these ratings create a vicious cycle. If the community values a stock too high, for example, then people rush to buy, based on its perceived value. The stock price then continues to climb, resulting in a price "bubble."

People don't buy and sell with an eye toward the future. For instance, if a successful company encounters an unexpected expense, even something as benign as a research project, this can cause the investment community to downgrade future projections for the company.

As a result, the company's stock price will fall, even if the research project would drive profitability down the road.

This simple realization about the stock market's irrationality can help you earn extraordinary profits!

Companies with potential to grow are often innovators, and they'll inevitably run into bumps along the road. Consequently, the financial community will undervalue the stocks of these companies.

To illustrate this, image you've found a widget manufacturing company with huge growth potential. At first, the hype surrounding the company causes the stock price to rise.

However, a problem soon appears: the casting mold for the company's widgets were improperly sized. Now everyone thinks the product is a flop; and the stock price falls.

This presents the perfect time for you to buy in cheaply, to profit when the company ultimately fixes its problems.

If you miss an opportunity like this, don't worry, as there may be another chance to get in during the next dip. For example, the company's stock rises after it fixes its casting mold problems, yet it announces that its sales staff has racked up unexpected costs in rolling out the product.

Exasperated, the financial community bails out, thinking the company will never make it. Yet for you, it's an opportunity to get the stock cheaply again!

### 5. Be confident in your choices. Don’t give in to doubt, or follow the investor herd mentality. 

Doubt is natural. Inevitably, once you've identified a stock that you want to buy, you'll feel the pangs of doubt that cause you to hesitate.

A successful investor however has no room for doubt. Don't hesitate once you've identified a company with profit potential.

Often this hesitation derives from the whims of the crowd. If no one else is jumping on a stock, couldn't that mean that it isn't really that valuable?

Yet you've already seen how the financial community is so often wrong about a stock's value. Surely, it's scary to buy when everyone else is selling! But if you truly believe a company has potential, then take a deep breath, and buy.

Besides, you really don't want to be buying what everyone else is buying. At that point, the stock will be too expensive to be a truly great investment.

And if you hesitate, you may never get the same opportunity again. Resist the urge to let a stock's price fall further to get a better deal. By doing so, you might miss a golden opportunity.

That's exactly what happened to one of the author's acquaintances who wanted to buy 100 shares of a stock listed at 35 1/2. In an effort to save himself 50 cents, he offered only $35 per share. The share price, however, continued to rise!

Had he made that extra 50-cent investment that day, he would have earned a total of $46,500 some 20 years later!

Finally, once you've found a good investment, you should hold on to it. From a business perspective, there are only three valid reasons to sell a stock.

One, you misjudged the company's growth potential. Two, your judgment was sound, but the company conditions changed. And three, you've invested in a middling stock in the near-term to keep you busy while you search for an amazing investment.

Any other reasons, such as making money quickly or following the crowd, can only harm you. A company with huge growth potential can never be overvalued!

> _"If the job has been correctly done when a common stock is purchased, the time to sell is — almost never."_

### 6. Conservative investors should seek out solid, organized companies with growth potential. 

So far we've examined how to win big with high-growth company investments. Yet for some investors, a more moderate profit strategy with gradual but consistent growth is more attractive. 

As a conservative investor, you want a strong company that still has the potential to grow. If you're trying to play it safe, you shouldn't invest in start-ups, no matter how promising they appear. Rather, keep your eye out for large, established companies that have a proven track record of profitability.

For this strategy to work, however, the company still needs to have growth potential to maintain its market position.

This doesn't need to be stratospheric potential, of course. Rather, the company must simply be able to grow and develop. Otherwise it will eventually be outpaced by more agile competitors.

A company that is both strong and able to grow demonstrates these four major characteristics.

First, the company's production methods are low cost, such that it can continue earning profits even when prices rise, through inflationary periods or market crashes.

Second, the company is well organized and effective in its market, meaning it can actually deliver its products and services.

Third, the company has an outstanding track record in research and technical development, allowing it to continue innovating and improve upon its products and services.

Finally, the company needs to demonstrate financial know-how. It must be capable of allocating resources only to initiatives with profit potential, and have a good eye for warning signs that indicate it's time to move on.

### 7. Valued employees form the core of any stable, growth-oriented company, and are good investments. 

If you are a conservative investor, it's also important to examine a company's employee base and learn more about how the company treats them. There are two main reasons for this strategy.

First, a company's growth potential is entirely dependent upon employees who put into motion the company's strategy. Indeed, all of a company's advantages are the direct result of work performed by its employees.

To foster innovative technical development, for example, a company needs devoted, ambitious research and development teams. To ensure production methods are low cost, a company needs a bright manager who is always on the lookout for gains in efficiency.

Second, how a company treats its staff can help you figure out just how productive and effective that company is overall.

People don't deliver the best results when they're mistreated; yet companies succeed when employees are valued. By looking at a company's human resource and management policies, you can get a better idea of how a company is run and better judge its long-term potential.

Look, for example, at the way the company handles promotions. If they rarely promote from within and instead prefer to hire from outside the company, then this is a strong indication that the company isn't grooming employees or developing potential through training.

This is a red flag that the company does not handle its human resources wisely, and is therefore not the kind of company in which you would want to invest.

The same applies to management's ability to work together as a team. If a company manager seems to think he's running a one-man show, how can he be expected to manage efficiently as the company grows?

Look instead for a company that forms well-organized teams, and for managers who can effectively delegate responsibility.

### 8. A conservative investor looks for companies that can ensure profitability over the long term. 

When evaluating a company, a conservative investor should look into the future. Not only must the company be strong today, but also it must have the means to protect its position over time.

When making this evaluation, an investor should view a company's profitability not as the sum of its returns, but rather in terms of the size of its profit margins. The bigger the better!

You want to find a company that will be profitable in the long term, one that will continue growing even during challenging economic times.

The reason that profitability is so important is because growth always requires monetary investment, be it for research and development, new inventory or marketing. Thus the company must ensure its future profitability to finance additional expenses to come.

Profitability also gives the company a buffer when it falls on hard times; when costs rise, the company must have cash on hand to stay in business.

But how can a business ensure long-term profits? In short, by being better than the competition!

A company's long-term profitability hinges upon its ability to do or create something that competitors can't, lest the competition steal its market share.

One way to ensure market dominance is through _scale_. In other words, a large producer can produce more than a small competitor can, and at a lower cost. 

For example, a big company that produces a million pencils per month will have lower production costs per pencil than a company that can only produce 100,000 pencils monthly.

Another way to ensure a company's market position is by creating technical developments that _can't legally_ be copied by other companies, due to patent or copyright.

You want to be sure that a company's strategy takes into account and is prepared for the long-term development of the business.

> _"Only by growing better can a company be sure of not growing worse."_

### 9. Calculate a company’s price-earnings ratio to determine whether you’re paying the “real price.” 

A company can be incorrectly valued based on the whims of the investment crowd. As you'll see, a stock's value is even _more_ subjective than that. In fact, its value changes from person to person.

Indeed, a company can be worth more for a risky investor than for a conservative one.

A risky investor might value a stock higher if a company is expected to grow rapidly, but this fact doesn't help the conservative investor. A conservative investor shouldn't pay in terms of expectation.

Instead, a conservative investor pays the _real price_ of a stock.

Essentially, the conservative investor is looking for stable, growing companies that are either currently undervalued or priced at their real value.

One way to determine this is by using the _price-earnings ratio._ To do so, simply take a company's stock price and divide it by the company's earnings per share.

For example, if a company is earning $1 per share, and sells its shares at $10, then its price-earnings ratio is 10/1, or ten. At its last annual report, however, if the company says it earned $1.82 per share, this brings the ratio down to 10/1.82, or five.

However, higher earnings represent positive projections, which could cause a huge spike in the stock price. Imagine that the company stock now sells for $40. The ratio would be 40/1.82, or 22, far higher than it was previously.

It's up to the conservative investor to decide whether the company's characteristics justify the market's belief that the company will continue growing beyond its current $40 share value.

While someone looking for huge short-term gains might not be bothered by a $40 share price, a conservative investor might find the price a bit too steep, considering what she knows about company management!

### 10. Final summary 

The key message in this book:

**To be a successful investor, you have to be willing to dig. A company's true value is based on so much more than its stock price alone! If you're willing to put in the detective work, you stand to reap great rewards no matter whether you're a conservative investor or a high-risk one.**

Actionable advice:

**Go to your bank as an information source.**

If you're having trouble finding information on a company, try contacting your bank. As long as you are open about your intentions, they can easily make an introduction for you. Be clear that you are looking for information for yourself and will treat the information with the utmost discretion.

**Suggested further reading:** ** _The Intelligent Investor_** **by Benjamin Graham with comments by Jason Zweig**

_The Intelligent Investor_ offers sounds advice on investing from a trustworthy source — Benjamin Graham, an investor who flourished after the financial crash of 1929. Having learned from his own mistakes, the author lays out exactly what it takes to become a successful investor in any environment.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts
---

### Philip A. Fischer

Philip A. Fisher is one of the original fathers of investment theory and the founder of the renowned money management company, Fisher & Company. His book, _Common Stocks and Uncommon Profits_, originally published in 1956, has remained in print ever since.

