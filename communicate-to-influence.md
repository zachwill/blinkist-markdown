---
id: 5a5b93b3b238e100078c4fc8
slug: communicate-to-influence-en
published_date: 2018-01-15T00:00:00.000+00:00
author: Ben Decker & Kelly Decker
title: Communicate to Influence
subtitle: How to Inspire Your Audience to Action
main_color: 2B9ED6
text_color: 1C668A
---

# Communicate to Influence

_How to Inspire Your Audience to Action_

**Ben Decker & Kelly Decker**

_Communicate to Influence_ (2015) is a detailed guide to being an effective communicator. This covers everything from choosing the right voice to knowing how to structure and deliver your message. Gain a better understanding of the pitfalls of public speaking so that you know what to avoid, and discover some fantastic tools that will help turn your information dump into an inspiring call to action.

---
### 1. What’s in it for me? Become a better communicator. 

Effective communication can be the deciding factor in acing that job interview or convincing investors to hop aboard your new company. Although communicating ideas effectively sounds like simple common sense, many of us continue to make basic mistakes that could cost us opportunities.

Great speakers capture the public's imagination with inspiring rhetoric. Take renowned rousing speaker John F. Kennedy, who led the US to achieve incredible feats such as the moon landing. But great communication skills don't necessarily have to lead to history-changing events: the techniques in these blinks can equally be used in your next meeting.

In these blinks, you'll learn

  * which five common communication mistakes you're probably making;

  * what role body language plays in effective communication; and

  * the SHARP method for fostering a connection with your audience.

### 2. Communication often fails due to five common mistakes. 

If you're giving a presentation, the last thing you want to see is the audience nodding off to sleep or playing Candy Crush on their phones.

To prevent this from happening, understand the five most common mistakes people make when trying to communicate with their audience. Avoid these, and you'll be on your way to a great presentation.

The first mistake is to place too much importance on the content and information of your presentation.

When you fall into this trap, you end up spending all your time overanalyzing every word and zero time making sure your material is presented in an interesting way.

Even if you're talking about the cure for cancer, standing still and speaking in a monotonous voice is bound to send people nodding off into dreamland.

The second mistake is to exhibit a lack of authenticity, which causes distrust and disconnect in the audience.

Keep in mind that in every presidential election between 2000 and 2016, the losing candidate appeared more contrived and manufactured, while the winning candidate appeared genuine and down to earth.

The third mistake is to be unprepared.

Presentations aren't the only thing for which you can prepare. Staff meetings, business lunches and office pop-ins can all be planned out in advance. In fact, you can prepare for any situation where you anticipate a chance to communicate so that your words can inspire and influence.

The fourth mistake is to lack self-awareness.

Don't forget that the way we see ourselves doesn't always match up with what others see. You can be all too familiar with the way you speak and believe it to be perfectly normal, while your audience will think you're speaking way too fast. So make sure you get feedback from someone before you deliver your big message.

The fifth mistake is adhering to the status quo.

Don't get too used to your comfort zone and forget to challenge yourself from time to time. If you hope to grow and become a better communicator, you'll need to take some chances.

### 3. Today, shortened attention spans and distrust of authority have put a strain on communication. 

If you want your message to be heard, you need to rise above the competition. And with mobile devices making it even tougher to get people's attention, it takes a strong voice to rise above the noise.

Online content has become part of what's known as the _attention economy_, with increasing focus on mobile devices, to the point where messages from other people are pushed aside.

According to a 2012 _Time_ magazine poll, 84 percent of people are attached to their phones on a daily basis, with 20 percent checking them every ten minutes.

Part of the attraction to mobile devices comes from an addiction to the rush of endorphins they deliver. Every time we receive notification of a like, share, retweet or comment, the pleasure center of our brain receives a satisfying rush of these neurochemicals. But all this has left our attention span at disparagingly low levels.

Meanwhile, our distrust for authority figures has risen.

A good portion of this distrust stems from the 2008 financial crisis, which experts and leaders failed to deal with properly. According to the Edelman Trust Barometer, less than 20 percent of the public trusts business or government leaders to be truthful about important matters. Since trust is vital to communication, this means traditional leadership has lost much of its influence.

So we now have an abundance of information with very little inspiration, and this imbalance has left today's generation feeling desperate to be part of something bigger. A 2013 study found that 85 percent of millennials want a job with a purpose and one that benefits the world, not just themselves.

As a result, there's a large demand for socially conscious products and business models. A good example is TOMS, a shoe company that donates one pair of shoes to needy children for every pair purchased.

So if you want to gain attention and be heard, you need to combine inspiring content with authenticity.

### 4. Use the Communicators Road Map to pinpoint your style and be as inspiring as possible. 

There's more than one way to communicate, and if you want to deliver your message in the best way possible, it pays to understand the different styles.

The Deckers have created a _Communicators Road Map_, consisting of four communication styles from which you can pinpoint your own. If you want to deliver your message in a way that moves your audience to action, you should aim to transition to the fourth _inspiring_ style.

The first style is _informing_. This is when someone simply dumps a load of information for the audience to do with as they please. There is often no added instruction provided, and the only intention is to inform.

The second communication type is _directing_. This is information that comes with instructions, and it's generally delivered by someone in a position of power. This is the style your boss uses to get you to follow orders.

The third style is _entertaining_. This content is meant to elicit an emotional reaction like laughter or tears. You can find this style practiced by talk show hosts like Jimmy Kimmel, and it usually comes without any instructions.

The fourth type is _inspiring_. Here, the communicator is engaging the audience's emotions, while delivering highly relevant content designed to inspire action. This is the kind of communication that can lead to memorable and moving speeches, exemplified by revered speakers such as Martin Luther King, Jr.

So how do you craft an inspiring speech that is effective and memorable? It's a matter of finding the right balance between creating the right emotions and tailoring a relevant message to your audience.

If you're an authoritative person who communicates with direction, you can improve your communication by finding an emotional connection with your audience. If you're an entertainer, you may easily establish an emotional connection, but can improve by centering your message around your audience. Whereas if you're an informer, you could work on improving both the emotional connection and your message.

Shifting your style in these ways will make you an inspiring, memorable, effective, and persuasive communicator.

Next, we'll look at some strategies you can use to increase connection.

### 5. Visual displays of warmth, confidence and trust will foster connection with your audience. 

Now that you have your style, it's time to prepare your delivery. You need to remember how important your appearance and mannerisms are. Believe it or not, non-verbal communication is _the_ _most_ important component of clear communication.

So consider how these four simple yet powerful tools can help you make the most of visual communication.

First of all, there is _eye contact_, which is one of the strongest ways to come across as caring, trustworthy and likable to your audience. For eye contact to work to its fullest, hold your gaze for five seconds with each person if you're speaking to a small group.

If you have a large audience, don't feel you need to make eye contact with everyone. Instead, pick at least one person in each section. Eye contact has a _halo effect_, and the surrounding people in those sections will all feel the positive effect that eye contact creates.

Now, it's important to consider your body language as well, and the "ready position" is a useful stance that avoids a lot of harmful mannerisms. To strike a ready position, stand straight as if your head were being pulled up toward the ceiling, draw your shoulders back and balance your weight on the balls of your feet.

Even when you're not doing anything else, this stance projects confident energy. But you still want to avoid _cold mannerisms_, such as leaning backward; standing with your legs crossed; shifting or pacing back and forth; or being frozen in one spot.

Good gestures add enthusiasm and passion to your message, and it's best to incorporate a variety of them. The best will align with your content and strengthen it. For example, when you say "there are _three_ contributing factors," you can raise your hand and hold up three fingers.

And don't forget to smile!

Smiling is the easiest and most effective way of providing warmth and appearing confident, calm and comfortable. Not only that, it's a great way of covering up any nervousness.

### 6. The way you use your voice and insert pauses is crucial to effective communication. 

If you were in the audience and the presenter was mumbling or whispering, it'd be understandable if you decided to walk out and spend your time elsewhere. This is why the sound of your vocal delivery is considered the second most important factor in effective communication.

The worst voice you could use is the slow monotone drone. It can make any message sound dreadfully dull.

So, to make sure your voice and delivery are as compelling as your message, find the right occasions to vary the pace, volume and pitch.

Whenever you're making an important point, you can engage the audience's sense of anticipation by raising the volume and speeding up your delivery. But make sure you don't overcompensate by speaking so fast you become incoherent.

Varying the pitch and tone of your voice is also a good way to keep your audience attentive and to emphasize the transitions and themes in your message. But be careful to avoid "up talk," which happens when your sentences start low, gradually increase, and then end on a high note. This can make every statement sound like a question, which will cause you to sound uncertain and incompetent.

Instead, you should aim to sound charming and confident, and a great way to strike this tone is to imagine that your audience is a group of close friends.

So imagine you're hosting a dinner party where you can be your natural, sociable self. This is the ideal version of yourself — the gifted storyteller who has their audience hanging on their every word. This will allow the right tone, personality, and vocal variety to come naturally.

Finally, try to remember how effective a good two- to three-second pause can be, especially after you've just presented a key point.

This is a powerful tool that adds dramatic emphasis to your message and gives the audience a moment to process the importance of what you've just said. It also adds to the sense of you being well-prepared, compassionate and credible.

### 7. Get your audience engaged by using stories and humor. 

If you don't want your audience to forget what you've said the moment they walk away, you need reliable ways to emotionally connect with them.

This is exactly what _SHARPs_ do. They are: _Stories_, _Humor_, _Analogies_, _References_, and _Pictures_.

Everyone likes a good story, and they're the perfect way to emphasize a point and stir a listener's emotions.

If your message is about friendship and loyalty, you could tell a story about watching six people in wheelchairs playing a game of basketball. And at the end of the game, five of the players stand up and accompany their wheelchair-using friend across the street to a pub, where they enjoy a cold beer.

This story comes from an award-winning ad that Guinness used to emphasize their message of celebrating friendship. But there are countless stories that could help you make a point — they can even make otherwise boring numbers and statistics seem interesting.

Heartwarming and tragic tales are incredibly effective at getting people to take action. At fundraisers for the Boys and Girls Club of America, teenagers tell their own stories of surviving gang violence, physical abuse, and drug-addicted parents. In one case, a teen told a harrowing tale of being locked in a trailer for two days as their mother binged on methamphetamines.

Stories like this will get more people to donate than simply listing the statistics and numbers of at-risk children in the community.

Humorous stories are another effective way to get your audience engaged and to come across as humble, good natured and likable.

The safest kind of humor is self-deprecation, and an excellent example of this comes from Katy Klein, the chief marketing officer at a US tech company. She started off a PowerPoint presentation with a slide of her driver's license. She then began with the line: "I have a confession, I lied on my driver's license. I'm only five foot one not five foot two."

At first, this seems wildly off-topic for a marketing presentation, but as Katy continues it later reveals itself to be directly related to the main point in her presentation — being honest.

> _"People crave emotional experiences. Logic makes us think, but a well-told story makes us feel — and emotions prompt us to action."_

### 8. Analogies, references and pictures make your content more engaging. 

Let's continue with the final three SHARPS: _Analogies_, _References_ (which also includes quotes), and _pictures_.

Analogies are a way to anchor an unfamiliar concept to something that is more easily understood, which makes it extremely useful if you're dealing with a complex subject like astrophysics or microtechnology.

And when they're used well, they can also surprise and captivate your audience.

In one presentation, an engineer was trying to convince his audience of the benefits of standardization. As an effective analogy, he explained how the Great Baltimore Fire of 1904 was made worse because the firefighters' hoses were incompatible with the city's fire hydrants. With its tragic consequences, this analogy made a powerful case in favor of standardization.

Using references and quotes is another simple yet effective way to get an audience engaged with what you're trying to say.

While a quote should always be short and sweet, a reference can help make your point by providing extra details that add credibility to your message. Both of these tools can provide opportunities to enliven and change up the tone of your delivery.

Pictures and visuals are a perfect tool for adding some pizzazz to your content.

Creative PowerPoint slides, striking videos and even clever props can be great additions to help bring your message to life. When choosing a visual element, keep _the three B's_ in mind: _Big_, _Bold_ and _Basic_. This is to make sure the audience will be able to see it and understand why it's there. Otherwise, it will just be distracting.

Marketing campaigns are filled with good examples of effective visuals.

The New York City Department of Health and Mental Hygiene had an effective subway campaign against sugary sodas. The visual showed a bottle of soda being poured into a glass and turning into human fat. The accompanying slogan was: "Are you pouring on the pounds?"

This is clear, concise and effective in making the overall point, which makes it a great piece of visual communication.

### 9. Get to know the wants and desires of your audience in order to get them to act. 

If you're trying to win over an audience, it makes sense to understand exactly what they want, right?

Indeed, once you're aware of what's relevant to your audience, you can start to design your message specifically for them.

So ask yourself three basic questions: Who is your audience? What do they expect? And, what do they already know?

The answers to these questions can then be summarized with three essential adjectives. And once you have these, you can use them to create an _audience profile_ that will help you frame your message.

For example, let's say your audience is the overworked staff of a small business. They expect to grow, but they're aware that their website isn't attracting any visitors and they're unsure how to improve. In this case, the three adjectives would be _busy_, _growth_ and _unfamiliar_. Now, you can develop a great pitch on how search engine optimization (SEO) can increase their web traffic with very little added work and everyone comes away happy.

If you're delivering a message that is intended to make your audience take action, you need to make sure it has a clear and concise _Point of View_ or POV. This is one brief statement that sums up the entire message and provides an obvious reason for the audience to do something.

For the business that could benefit from SEO service, the POV would be: "Your clients need to find you before they can hire you."

Once the POV is in place, you can provide an action, with clear and concrete steps to guide them along. This is when you can point them toward resources or services that you might be selling.

It also helps to give them a specific action, so that they'll act quickly. You might tell them that your services and books are currently at a discount, but only for the next 24 hours. So act now!

### 10. Use the Decker Grid to structure consistent and focused messages. 

Once you understand your audience and your content, it's time to put that information into a structure, which is where the _Decker Grid_ comes in handy.

The Decker Grid is essentially a template that helps you to organize and identify all your main points and themes.

The grid consists of 20 boxes arranged in four columns and five rows. Each box can be filled out to briefly describe the parts of your presentation, while the top and bottom rows are for your intro and ending.

Let's say you're preparing your pitch to get the small business to use your SEO services.

In the first box of the top row, you put in your opening SHARP. This can be a quote, a humorous anecdote or perhaps the tale of a previous client who benefitted amazingly well from your service.

The next box contains your POV, such as: "Potential clients have to be able to find you before they can hire you."

This leads to the third box, which contains your general ACTION, like further exploring SEO benefits.

In the last box, it's the BENEFIT, or the result of the action, such as increasing web traffic and getting new clients.

The next three rows are where you list three _Key Points_, followed by three _Sub Points_. The first could be "SEO is extremely valuable" followed by points that back this up, like "Google Analytics" and "megatrends," and so on.

To make sure you don't lose your audience along the way, at least two of these Sub Points should be presented in the form of a SHARP.

The bottom row is for your closing remarks, which is in a slightly different order from the top row.

First, restate your POV, then add a Specific ACTION Step, so instead of just "use my service" it's "use my service today, and I'll give you a discount." This is followed by another BENEFIT, so you can tell them just how much more web traffic they'll receive.

And then you end on a final SHARP, perhaps a simple quote from someone like Mark Zuckerberg, which sums up how important good SEO is.

> The decker grid template can be downloaded for free at decker.com.

### 11. Great communicators have an open mind-set that promotes humble confidence. 

Being a great communicator is more than just talking clearly and making a point. With the combined power of your presence, voice and the right words, you can change the way people think and behave.

This makes an expert communicator a powerful influencer, but to reach this level you need to avoid the _fixed mind-set_ in favor of a _growth mind-set_.

A person with a fixed mind-set places limits on their own growth. So rather than proving how much potential they have, they tend to show their limitations.

The growth mind-set, on the other hand, erases limitations and eliminates nagging fears about not being good enough or having a certain natural talent. This attitude is about recognizing the ability we have to grow and improve.

In Jim Collins's book, _Good to Great_, we see that all great companies share one important thing: leaders with a growth mind-set.

Not only that, but great leaders are also endowed with _humble confidence_ that inspires and motivates those around them.

Humble and confident leaders are there to serve others first and empower them rather than simply dominating. Perhaps most importantly, they're open to collaboration.

If you picture Nelson Mandela, you probably see him with a smile on his face, which is one of the most obvious signs of humble confidence. Mandela was never arrogant or self-aggrandizing; he knew the power of humility.

He was also a great communicator who was not afraid to shoot for the moon and propose solutions that required great leaps forward rather than small steps.

Mahatma Gandhi was a similar influencer who wasn't afraid to call for big change.

During the Salt March protest in 1930, Gandhi proved non-violent actions were an effective answer to the violent oppression of British colonialists. Thanks to his radical approach, people listened, and we saw how the right intentions could turn the unthinkable into reality.

These are the kind of influencers from whom you should take inspiration. Now it's your turn to put these tools to work, and, hopefully, leave a positive impression on the world.

### 12. Final summary 

The key message in this book:

**Building an emotional connection with your audience is critical to inspiring action. Conveying authenticity and warmth through your behaviors and your voice, as well as adding emotional triggers to your content, will foster this connection. Using the Decker Grid to develop a well-structured, unscripted message will facilitate the delivery of an effective communication.**

Actionable advice:

**Make audio recordings of your communications wherever possible.**

Record conference calls and board meetings. You will only need to listen to 10–30 seconds to identify what you need to improve. Using this feedback is a great way to foster self-awareness.

**Speak from your diaphragm, not your throat.**

This way, even the people in the back row will hear you. And don't aim your voice at your feet; project it outward, in the direction of your audience.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Talk Like TED_** **by Carmine Gallo**

In _Talk_ _like_ _TED_ (2014), you'll learn about presentation strategies used by the world's most influential public speakers. Author Carmine Gallo analyzed more than 500 TED talks to identify common features that make these talks so influential and appealing.
---

### Ben Decker & Kelly Decker

Ben and Kelly Decker are leading experts in business communication. They have vast coaching experience with ambitious startups as well as Fortune 500 companies. Together, they are the brains behind Decker Communications, a firm that has helped countless individuals improve their communication skills.

