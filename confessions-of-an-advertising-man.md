---
id: 51274f47e4b0714805cbbe98
slug: confessions-of-an-advertising-man-en
published_date: 2013-04-13T00:00:00.000+00:00
author: David Ogilvy
title: Confessions of an Advertising Man
subtitle: None
main_color: B2875C
text_color: 664D35
---

# Confessions of an Advertising Man

_None_

**David Ogilvy**

_Confessions of an Advertising Man_ (1963) is a collection of advice and techniques for building successful advertising campaigns and agencies. Written in the era of Mad Men, the book is still considered essential reading in the advertising industry, but also provides advice for aspiring managers in any business.

---
### 1. Choose your clients carefully and cultivate long-term relationships with them. 

In its infancy, an advertising agency must hunt for whatever business it can get. But in the long run, it should aim to cultivate a select number of large key accounts.

This means being very picky and gradual when adding new clients. A basic limiting factor on growth should be your ability to train new staff to handle an account — a process that takes about two years. A first-class agency can afford to be selective because the demand for excellence is always higher than the supply.

So how do you choose your clients? First of all, you must be comfortable working with them, for your relationship will be an intimate one. Find out why they are leaving their former agency. Could it be that no agency can abide them, because they actually need therapy more than they need good advertising?

Second, both you and your client must seek a long-term, mutually beneficial relationship. With an average profit margin of 0.5%, advertising agencies cannot afford clients who drive such a hard bargain that no profit is made on their account.

Some clients may have a reputation for changing their agencies frequently — avoid them, for you cannot remedy their infidelity. And never take on products that are either brand new or dying. The former are risky and very expensive to introduce to a market, whereas the latter cannot be helped by any advertising.

Some companies publicly announce which advertising agencies they are considering for their account. In such cases, you should withdraw voluntarily rather than risk failing publicly.

Finally, never add an account that is so big you cannot afford to lose it, or you will live in constant fear.

**Choose your clients carefully and cultivate long-term relationships with them.**

### 2. Build your business by being a lasting, valuable partner to your clients. 

Building an advertising business from scratch is very difficult, but it can be done with a bit of luck — and a lot of effort and self-promotion.

Landing new accounts is a thrilling experience, and your business depends on it. Nevertheless, consider client hunting as a sport: try to win but don't take it too seriously. Set your long-term goals high. On the second day of his agency's existence, Ogilvy named five major blue chip companies — including legends like Shell and the Campbell Soup Company — as targets. All five eventually became his clients.

Landing new clients may be exciting, but losing existing ones is pure hell: one defecting client can inspire others to leave too and kill an entire agency. Therefore, always have your best and brightest people working on existing accounts rather than chasing after new ones. If you give your existing clients your all and make yourself indispensable, you'll never be fired — no one kills the goose that lays the golden eggs.

To make yourself irreplaceable, get to know your clients intimately: use their products, buy their shares and understand their industry. At meetings, let them do most of the talking; you'll learn more and seem more thoughtful and wise.

On average, clients change advertising agencies every seven years and this is a harrowing experience. But companies change their internal advertising managers even more frequently, and new managers are often tempted to sweep aside anything old, including your agency. Hence you should woo these new faces as you would new customers, and try to establish connections at every level of your client's organization so one new face cannot topple your relationships.

**Build your business by being a lasting, valuable partner to your clients.**

### 3. Be honest in your advertisements and your business dealings – you’ll do better in the long run. 

The advertising industry is often seen as a realm of lies and illusions, but in fact dishonesty is short-sighted and unprofitable — a consumer duped into buying an inferior product once will not return for repeat business, and your agency will quickly lose its reputation.

Hence, only advertise products you can proudly stand behind. Unless you truly believe they are good, you have no business advertising them.

When you do advertise something, spit out the facts. Consumers are not morons, so don't insult their intelligence by assuming mere empty slogans will sell your product. The more information you provide, the more the product sells. This kind of informative advertising is not only more effective in increasing sales, but is also widely accepted as one of the socially valuable aspects of advertising.

Just as you would not lie to your spouse, do not lie to people in your advertisements. "Weaseling" can damage you and your clients in many ways, ranging from disappointed consumers to fines by government agencies. Only write advertisements that you would be comfortable with your family reading.

Extend this principle of integrity into your business dealings with clients; for example, honestly tell them if your agency is weak in a particular area. They will trust you more for your honesty. Similarly, never accept an account unless you honestly believe you can do a better job than the previous agency.

Finally, you will often find that once someone creates a great advertising campaign, other agencies quickly steal it for their own uses. Such copycatting is deplorable — do not stoop to it.

**Be honest in your advertisements and your business dealings — you'll do better in the long run.**

### 4. Build a great working atmosphere by hiring proven hard workers with brains and integrity. 

The number one responsibility of an advertising executive is cultivating a good atmosphere for the creative staff to work in. To do so, you must only hire people who will contribute to such an atmosphere.

Employ hardworking, well-mannered men and women who have the brains to produce great advertisements and the guts to stand up to their own bosses. Look for people who genuinely enjoy what they are doing, are excellent at their craft, and treat each other with respect and integrity.

Be constantly on the lookout for such people. For example, whenever you see a great advertisement, find out who wrote it and call them up to congratulate them. If your agency has a strong reputation among creative folk, the copywriter in question may well ask you for a job right there on the phone.

Whatever you do, ensure there is no incompetence amid your ranks, for nothing demotivates the competent faster than working alongside hacks or toadies. Never indulge in nepotism — hiring people due to family ties or politics — as it will undermine your commitment to high-caliber hires and also breed internal political maneuvering within your company. 

Be especially wary of hiring quarrelsome account executives. Madison Avenue crawls with these seemingly masochistic people who pick fights and inadvertently court rejection from their clients.

Finally, strive to hire young copywriters, for they will bring the fresh, new blood that will keep your advertisements contemporary.

**Build a great working atmosphere by hiring proven hard workers with brains and integrity.**

### 5. To inspire devotion and enthusiasm in your staff, demand excellence and lead by example. 

Long ago, Ogilvy worked as a chef in the elite kitchen of a Parisian hotel where the head chef, Monsieur Pitard, inspired an incredibly intense sense of duty, enthusiasm and devotion among his staff. It is from his example that Ogilvy learned the basics of leadership.

First, you must demand the highest excellence from your staff. Just as the head chef in Paris inspected every single dish before it left the kitchen, you too should inspect every single campaign before it leaves the agency, and send it back for more work if it is unsatisfactory.

Inspire excellence in your staff by using compliments sparingly instead of gushing constant approval. This way, employees will slave for just the slightest word of praise from you. One time Chef Pitard examined some frogs' legs Ogilvy was preparing, nonchalantly summoned the other staff to look at them, and remarked, "That's how to do it." This tiny bit of praise from Pitard practically made Ogilvy his devoted slave.

Also, lead by example: show your staff they are working for a master. Chef Pitard mostly designed menus and handled bills, but once a week he actually cooked something and the rest of the staff watched his mastery in awe. Similarly, if you wish to inspire your copywriters, descend from your ivory tower every now and then to write advertisements yourself. Also, show your staff that you too can burn the midnight oil when needed. Ogilvy actually worked longer hours than his staff so that they had fewer qualms about working overtime themselves.

**To inspire devotion and enthusiasm in your staff, demand excellence and lead by example.**

### 6. Your advertisements should sell products, not entertain people. 

Forget about designing so-called "creative" campaigns or writing "award-winning" copy. Good advertisements — regardless of media — should sell products without drawing attention to themselves. The most important metric to track for advertisements is the amount of sales they bring in, and it stands to reason that the advertisers who can most readily observe this metric know best what kind of advertisements work.

This is why it pays to monitor mail-order companies and large department stores. Both are in a position to count the sales generated by each individual advertisement — mail-order companies from the coupons sent in and department stores from the increase in sales the day after the advertisement. Hence, you can be sure they are well informed as to what works and what doesn't.

Some copywriters like to make their advertisements clever, for example by using complex puns or allusions in their headlines. But remember, most importantly you want people to buy the product, not admire your penmanship. In practice, your advertisements will sell the most if you promise the consumer benefits, like this headline does for Helena Rubinstein Hormone Cream: "How women over 35 can look younger."

Another way to increase sales is to insert certain powerful words into headlines. "New" and "free", for example, are very effective in grabbing consumers' attention. Similarly, emotional words like "love", "baby" and "darling" can evoke powerful responses. Consider, for example, one of Ogilvy's most provocative advertisements: A girl is sitting in a bathtub, speaking on the phone. "Darling, I'm having the most extraordinary experience … I'm head over heels in DOVE!"

**Your advertisements should sell products, not entertain people.**

### 7. To get people’s attention, use facts, intrigue and take advantage of research. 

Consumers are bombarded with billions of dollars worth of advertisements every day. To get their attention in this crowded marketplace, your voice must be unique and riveting. Be charming, memorable and straight-to-the-point, as if you were talking to someone at a dinner party.

Advertisements are most effective when they give people facts, even if those facts are not unique to your product. For example, when Ogilvy advertised KLM Royal Dutch Airlines, he emphasized KLM's comprehensive safety precautions. Every other airline also used these precautions, but since KLM was the only one talking about them, this approach proved immensely effective.

A great way to capture people's attention is to use images that ooze _intrigue_, making people stop to ask: "What is happening here?" For example, an empty chair with a cello leaning against it will beg the question, "Where is the cellist?" and bait the reader to read on.

Take advantage of research findings when designing your advertisements. For example, studies have shown conclusively that color photographs are more effective than black and white ones, and drawings are much less effective than photographs. Research also shows people tend to read headlines and captions far more frequently than body text, so ensure you mention your brand name in the headline, and _always_ use captions with your photographs.

Many people think consumers abhor long copy, but in fact, this is not the case. No fewer than 14,000 readers clipped a coupon from Ogilvy's 916-word advertisement promoting travel to Puerto Rico. Longer copy lets you use other stratagems too, such as giving readers helpful advice or including testimonials. Research has shown that they work better than plain product descriptions.

**To get people's attention, use facts, intrigue and take advantage of research.**

### 8. Create ambitious campaigns and test them thoroughly. 

In advertising, there is often pressure to come up with something new and discard the old. But when you create an advertising campaign, you must always aim to make it a long-term success rather than just a one-hit wonder. In fact, try to make it the most successful campaign in history — never shoot for mediocrity.

Think in the long-term. Great adverts can be run for a very long time, and never lose their pull; the Sherwin Cody School of English ran the same advertisements successfully for 42 years.

Be consistent: use every advertisement to build and enforce a desired brand image over the long term; aim to be as iconic as Campbell Soup and Guinness, both were built up by patient, distinctive advertisements.

Just as companies only find great products by weeding out the poor ones before they enter the market, you too should test images, headlines, and entire campaigns before running them. This not only makes the advertisements more effective, but also saves you the cost and embarrassment of having campaigns fail publicly.

The most important factor in advertising any product is the _promise_ of certain benefits you make to the consumer. There are usually several benefits you could truthfully promise, and dozens of different possible wordings for each, so you must rigorously test each variation until you find the most effective one.

In the case of a Helena Rubinstein face cream, for example, testing revealed that the promise "Cleans Deep into Pores" would generate far more sales than "Smoothes Out Wrinkles".

Similarly, if you're having trouble deciding between two layouts or pictures, test-run both of them in the same paper and see which one triggers more purchases.

**Create ambitious campaigns and test them thoroughly.**

### 9. As a client, find a great agency and do your best to help them create great ads. 

Let's turn the tables for a second. What if you are a client? How can you extract the best possible service from your agency?

First, don't haggle with them or try to cut corners by underspending on your advertising; it will just cost you in the long run. Unless your agency is making a profit on your account, they will not put their best brains on it and therefore, your advertising will suffer.

Also, avoid complex bureaucracy when approving proposed advertisements. Once, Seagram asked Ogilvy's agency to create a campaign for Christian Brothers wines and, despite getting approval from both Seagram and the monks who made the wine, the head of the Christian Brothers order in Rome rejected the advertisements. Having too many masters makes great advertising impossible.

Most advertising agencies live in constant fear of losing their clients to competitors, and many clients believe this threat works to their advantage. But fearful people cannot produce great advertising.

Rather than switching agencies constantly, try to select the right agency in the first place: spend time with prospective ones and talk to their existing clients to understand what they have to offer.

If you are not satisfied with your advertising agency, try to identify the root of the problem. Does it really lie with the agency? If so, be candid with them, relate your problems and clarify your needs. They may well put some more firepower onto your account.

Finally, set extremely high standards; do not settle for mediocrity. Make it clear you expect home runs and never let your agency rest on its laurels.

**As a client, find a great agency and do your best to help them create great ads.**

### 10. Career advice for the young: work hard, become a specialist and seize golden opportunities. 

When you start working at an advertising agency, you are usually assigned a rather unimportant job, perhaps as an assistant account executive. Nevertheless, from that moment on, your goal must be to become the most knowledgeable, best-informed person in the entire company on your specific assignment. Read textbooks, trade journals and marketing plans related to your client's industry, and spend your free time getting to know their consumers. Pretty soon, you will know more about your client's situation than your superior, meaning you'll be ready for his or her job.

Consider specializing in copywriting, research or a specific media instead of just making a career as an account executive. You will find it far harder to impress anyone as a run-of-the-mill account man than as a rock-solid specialist.

Most work in an advertising agency is routine labor, which won't impress anyone. However, every once in a while a golden opportunity will appear; the trick is to identify it and seize it. Take, for example, an opportunity that appeared several years ago when a large potential client requested policy papers on television advertising. While other agencies sent in a standard few-page response, one of Ogilvy's staff worked tirelessly for three weeks to put together a _175-page_ _analysis_. It was a classic career building moment; one year later he was elected to his agency's board of directors.

In general, working harder and longer will get you promoted faster. People who spend time with their families may be more likeable than workaholics, but by working longer hours you accumulate more experience in a shorter time span, and advance more quickly.

**Career advice for the young: work hard, become a specialist and seize golden opportunities.**

### 11. Final summary 

The key message in this book:

**Successful advertising agencies are built slowly through honesty, ambition, great people and patience. Always remember, the sole purpose of an advertisement is to sell products, so design them meticulously and test them rigorously.**

The questions this book answered:

**How do you build and run a successful advertising agency?**

  * Choose your clients carefully and cultivate long-term relationships with them.

  * Build your business by being a lasting, valuable partner to your clients.

  * Be honest in your advertisements and your business dealings — you'll do better in the long run.

  * Build a great working atmosphere by hiring proven hard workers with brains and integrity.

  * To inspire devotion and enthusiasm in your staff, demand excellence and lead by example.

**How can you create great advertising campaigns?**

  * Your advertisements should sell products, not entertain people.

  * To get people's attention, use facts, intrigue and take advantage of research.

  * Create ambitious campaigns and test them thoroughly.

  * As a client, find a great agency and do your best to help them create great ads.

**How can you advance rapidly as a young advertising professional?**

  * Career advice for the young: work hard, become a specialist and seize golden opportunities.
---

### David Ogilvy

Considered "the Father of Advertising", David Ogilvy is hailed as a genius by his clients and a marvelous boss by his staff. The Scotsman dropped out of Oxford and worked as a chef in Paris, a door-to-door salesman in Scotland and a researcher at Dr. Gallup's Audience Research Institute. He eventually founded and built up one of the world's most famous advertising agencies, known today as _Ogilvy & Mather,_ which has 450 offices around the world.

