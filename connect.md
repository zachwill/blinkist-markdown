---
id: 568afcd0de78c00007000021
slug: connect-en
published_date: 2016-01-06T00:00:00.000+00:00
author: Josh Turner
title: Connect
subtitle: The Secret LinkedIn Playbook to Generate Leads, Build Relationships, and Dramatically Increase Your Sales
main_color: 297FCF
text_color: 1F5F9C
---

# Connect

_The Secret LinkedIn Playbook to Generate Leads, Build Relationships, and Dramatically Increase Your Sales_

**Josh Turner**

_Connect_ (2015) lays out the most effective ways to use LinkedIn's potential to kick-start your marketing success. With the help of case studies, you'll learn how to use LinkedIn as a valuable sales tool, host great webinars and turn your enthusiastic audience into new clients.

---
### 1. What’s in it for me? Win more customers with the help of LinkedIn and webinars. 

Are you a big fan of Facebook or other social networks? If you are, you'll be thrilled to find out how your networking savviness can help your enterprise to thrive. Or maybe the hype never reached you and you don't know a lot about social networks yet. That's no problem — these blinks will coach you step-by-step to make and cultivate valuable business contacts with the help of LinkedIn, a social network designed expressly for professional purposes. And thankfully, you won't even have to share pictures of your meals to be liked. 

Once you're up-to-date on networking, you'll find out about another internet option that can hugely boost your business success: webinars, or internet-based live seminars. With a single webinar, you can reach thousands of people around the world with minimal organizational effort. You can establish yourself as an expert without ever leaving your apartment. These blinks provide valuable advice on how to make webinars work for you. 

You'll also discover

  * why everyone hates bagmen;

  * why repeating the same ridiculous statement again and again isn't as stupid as it sounds; and

  * how pretending to be interested in another person can help you to sell things (being actually interested works even better, though).

### 2. Cold callers are unfamiliar and annoying, which is why we don’t buy from them. 

Remember the last time someone tried to sell you a vacuum cleaner or a holiday over the phone? Did you get out your credit card? Probably not. Nevertheless, cold calling is still a commonly used sales tactic. Let's look at exactly why you didn't buy that vacuum cleaner.

According to evolutionary psychology, not trusting strangers is instinctive.

While studying tribal warfare, behavioral scientist Dr. Samuel Bowles found that people were more cooperative with those from their own tribe, and antagonistic toward strangers.

Of course, the cold caller is a stranger to you, thus you instinctively keep your guard up. They can sing the praises of that vacuum cleaner all they want but chances are, you won't trust them. 

We are far more attracted to familiar things. The more familiar something or someone looks, the more attractive and trustworthy we deem them to be. This is also known as _priming_. 

Priming is a mechanism that also works with images and statements. So, if you see a message more than once, you're automatically more likely to believe it. This is why commercials are more effective than cold calling. If you see a commercial for that vacuum cleaner a couple of times, it will be more desirable to you.

Not only is it ineffective, cold calling actually drives potential customers away. Why? Because it's annoying! It's based on a strategy called _interruption marketing_, which disrupts your current activity and attempts to draw your attention toward the product. Examples of this include intrusive Google ads, billboards, Facebook posts and TV commercials. It's not unlike someone stalking you with a megaphone and shouting product slogans every two minutes. You'd naturally hate that, and cold calling is no different.

Now, let's say you're a salesperson: you know cold calling is a dead end, so how _can_ you deal with your client? Easy: make them believe you're part of their tribe. Read on to find out how.

> _"Instead of working against human nature, learn to work with it."_

### 3. LinkedIn helps you connect with other professionals for maximum success. 

Meet Bob Sanders, one of the author's clients. Having gained many years' experience in the construction business, Bob is now setting up his own construction consultancy and is eager to attract clients. How can he make the business contacts he needs?

One way is through LinkedIn, a sort of business version of Facebook. If you're in business, using LinkedIn is almost mandatory.

Currently, LinkedIn has around 400,000,000 members, with 40 percent of them checking the site daily.

Let's go back to Bob. Bob created a profile page displaying his CV, skills, interests and his network. He created a page for his construction consultancy to show what it does and to share product information. Should he ever need to search for a new employee, he can post a vacancy on this page.

Recruiting via LinkedIn is becoming increasingly common because it's great for searching for specific candidates. Of course, job seekers also use it to research potential employers.

Being visible on LinkedIn is important, but the real power of the site is as a B2B (business-to-business) marketing tool. LinkedIn enables you to systematically connect with specific prospects and convert them into customers.

One effective way of connecting is to use LinkedIn groups. There are more than two million different groups on the site that are formed around common interests such as "Behavioral Economics," or around location, like "Berlin Start-ups."

You can join up to 50 groups and share content, instigate discussions or take part in existing conversations. Whenever you participate in a group like this, thousands of people will see it. Bob, for example, follows several groups focusing on housing and construction in Indianapolis.

> _"The most effective way to turn cold prospects into warm leads is to gradually entice them to join your clan."_

### 4. A few measures help to ensure that the right people join your LinkedIn group. 

Say you've set up a profile, made some connections with some group members and are active in a couple of groups. What now? Create your own group! In Bob's case he formed the group "Construction in Indianapolis." 

Of course a group is nothing without members, so you'll need to go about selecting some people. You don't want to send an open invitation to everyone you can find — you need people relevant to your business.

When considering potential group members, think about which companies are relevant to you, including their size, location and industry. Bob chose housing and construction in Indianapolis, and small- and medium-sized companies.

Next, determine which people you want to target. You can do this by making a list of all the job titles of your current clients. This list may include titles such as "building planner" or "property manager."

You can find people by job title easily on LinkedIn and afterward you can refine your search. 

For example, if you search for "property manager" and get 355,642 results from all over the world, you should narrow down your results by specifying a location, such as Indianapolis. This will make your results more manageable.

Next, it's time to send out connection requests — much like friend requests on Facebook — to the most relevant and important people on your list, and follow this up with a couple of casual messages.

Take care to address everybody by name and, if applicable, add a compliment, like "Congrats on your new job." Then you can follow this up with something that's useful for them, such as alerting them to news articles that you think they might be interested in.

Connecting in this way will put you a cut above the rest of the competition in the eyes of your prospects.

### 5. To do LinkedIn right, make your group large, exclusive and filled with good content. 

Have you ever tried to market yourself or your company by writing a book or speaking at conferences? Although these approaches may work for some, they can drain both time and money.

The good news is that you can make real headway by making good use of your LinkedIn group.

A LinkedIn group can be enough to position yourself ahead of the pack in your industry. But how do you get it right?

First, make your group exclusive. As you're the owner, you can determine who will be invited to join it. Remember to focus on key decision-makers and C level managers, meaning CEOs, CFOs, CTOs and so on. To ensure that invitees are seen as equal, make sure that C-level members of your own company are listed as group founders by name and position.

When sending invitations, aim for up to a couple of thousand. You can start off with the prospects you are already in contact with thanks to your messaging campaign. Additionally, you can invite members from similar groups and use your homepage or other social media channels outside LinkedIn to further build the group's membership.

Next, you need to think about content. It's crucial to find and share relevant content in order to gain respect as a leader.

But what is the right content and where can you get it? You need to look out for everything that's relevant for your prospects' business. This can be anything from industry trends and statistics, to government regulations and news about related industries.

Sounds like it might involve some intensive internet trawling, right? It's easier than you think: first find out the most relevant sites for your industry and then enlist the help of a _feed aggregator_ such as Feedly.com. A feed aggregator is a service which collects all noteworthy news from the sites you have selected. 

Once the people relevant to your business are listening to you, that's great. But you want to turn your contacts into clients! Read on to see how.

> _"LinkedIn groups are a highly efficient, cost-effective way to reach out to the key prospects in your target market."_

### 6. Webinars can further boost your business connections if they’re relevant to your prospects. 

Have you ever attended a webinar? Maybe you don't know what a webinar is, but take note — it'll soon become a valuable tool for you. 

Webinars are live seminars, lectures or tutorials, hosted on the web. During a webinar, a speaker will typically guide you through some PowerPoint slides which will be visible on your screen.

Because they combine online scalability with the power of public speaking, webinars can make for highly effective business development tools.

Another great advantage is that the location of the speaker and attendees is irrelevant. In Bob's case, he could be at home in Indianapolis while talking to potential customers in Ohio or Chicago.

Delivering a good public talk will also work wonders for your credibility and boost the visibility of your company.

Of course during your webinar, you'll want to share useful content and knowledge with your audience. Knowledge is power, and appearing knowledgeable through a webinar will reinforce your leader status. What makes webinars slightly different to run-of-the-mill group content, however, is that at the end of a webinar, it's common to include a product pitch or at least a small company presentation.

Bear in mind that your webinar should be worth your prospects' time. Your audience won't want to listen to you rant about useless information, so if they make the effort to attend your webinar, you should make it worthwhile for them and for you.

This means sharing valuable content, so make sure you research what your audience — your potential prospects — really care about. If your audience is bored stiff during your webinar, this could irritate them and erode your relationship.

Remember to not be afraid of shining a positive light on what you do by integrating successful case studies and examples from your own business.

### 7. Some small measures ensure that prospects attend your webinar. 

What is the worst thing that can happen to a party? No one shows up. The same goes for webinars. Imagine you've practiced over and over and learned your presentation by heart. But there are only five people coming and one of them is your intern.

Let's see how you can avoid this disheartening scenario and make sure your webinar is a roaring success.

First, choose the right time and day. Tuesday, Wednesday or Thursday tend to be the days most people can attend a webinar, but double-check if this is true for your particular audience. 

In the States, between 10 a.m. and 11 a.m. Pacific Time is ideal, as this allows people from all four US time zones to attend.

To draw in more participants, use a catchy title and create an enticing starting page. You can make your title appealing by demonstrating how useful the webinar will be to the potential attendee. For instance, "How to generate 235 leads in seven minutes a day using LinkedIn."

Next, invest some time and effort in your landing page. This is where people arrive after clicking on an advert in your LinkedIn group to get more information and to register. For this, you can use LeadPages — software that helps you create effective landing pages. You could also make a couple of different versions to see which one works best.

Now you'll need to promote, and LinkedIn can help here, too. By now, you probably have some promising LinkedIn contacts to tell about your webinar. The best way to get the highest response rate is to send out personally customized invitations. In groups, you can also send out an announcement to all members. This works best if you use an informal tone, as if you were talking to a friend about something interesting and cool they might like.

> _"Before you know it you'll be a webinar guru, raking in sales leads and creating new webinars with great success."_

### 8. As your webinar comes to a close, reap the benefits. 

Now you may still be a bit skeptical, even after you've put everything into your webinar. Will it actually lead to sales? The answer depends on how you end your webinar.

The stock webinar ending is a slide containing your contact information and a brief text like "I'd be happy to arrange a complimentary consultation." This is simple and clear but it still puts you in the passive position of waiting for others to call you. And of course that may never happen.

To ensure the webinar leads to profitable follow-up conversations, get active and make feedback phone calls. In the initial part of the conversation ask for feedback, but in the second part, ask questions like "Have you been able to integrate some of the concepts discussed at the webinar?" When people reply to questions like this, it often opens the door to a sales conversation.

The best way to turn webinar participants into business prospects is to tailor your approach to their willingness to make a deal with you. To do this, first determine their level of approachability:

On the first level, you have the hot prospects — those who are excited and eager to get in touch with you. Their interest in your service or product is already piqued and this group is the one you should target first. As you may have guessed, you shouldn't have problems converting them into actual customers.

On the second tier, there are the warm prospects — those who are only slightly interested. Then on the third tier, you have prospects who aren't potential buyers now, but might be at some point in the future. It might be more appropriate to contact these two groups via email — for example, with a recap of the webinar — as they won't have displayed enough enthusiasm for you to contact them straight away by phone.

### 9. There’s a recipe for a good sales call. 

Your webinar or your LinkedIn campaign was successful and you're inundated with new contacts. Great! But how about closing some deals and making some money? For this, you'll need to make a few phone calls, but you'd better make them good.

In any sales call it's crucial to take your time and form some kind of connection with the person you're speaking to.

Remember, people would much rather buy from their own tribe! Thus, everything will run more smoothly if they trust you and perceive you as familiar. 

To foster this sense of familiarity, engage in a few minutes of small talk before even mentioning your product or service. Simple icebreaking questions like "Mike, help me, what part of the country are you in?" can make all the difference.

One common and vital type of call you'll make is the _networking call_. Say you're talking with a prospect who doesn't want to buy anything from you, at least right now. They just want to get to know you and what your business is about.

Start by asking them about their own product or company. Sprinkle in some small talk, and find out some common ground between you, such as people you both know. 

You should do this for up to ten minutes and let the prospect do most of the talking, with you asking questions. As they're talking, monitor what they're saying for any signs of the company's needs and problems. Here, you can pose some questions to glean as much information as you can. A word of warning though — don't make them feel like they're being grilled.

When they have spoken for some time and you've found some common ground, you can say something like: "Mike, I was not sure where this call would go but after what you've mentioned in this conversation, I got the feeling you might be interested in what my company does. Because we helped similar companies like yours solving issues like X. Would you like to hear more about it?"

### 10. Final summary 

The key message in this book:

**LinkedIn is a highly effective tool for connecting with business professionals the world over. By knowing who to contact and how to contact them, and by enlisting the power of webinars, you can use your LinkedIn connections to generate sales leads and seal the deal with your target customers.**

Actionable advice: 

**Warm people up for your webinar.**

When your next webinar is approaching, try to build some rapport with those planning to attend. Send them a friendly email, a free product or, even better, try to exchange a few text messages with them. Doing these very basic things will make invitees more likely to perceive you as familiar and trustworthy and they'll be much more inclined to attend your webinar.

**Suggested further reading:** ** _Ask_** **by Ryan Levesque**

_Ask_ (2015) is based on a simple idea — that learning what your customer wants is as simple as asking. The trick is asking in the right way. Full of immediately actionable insights into customer behavior, _Ask_ gives you all the tools you need to uncover what your customers really want and give it to them.

**Got Feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Josh Turner

Josh Turner is a leading LinkedIn expert and the founder of LinkedSelling, a B2B marketing firm. His company also runs LinkedUniversity.com, which provides online training for marketing through LinkedIn.

