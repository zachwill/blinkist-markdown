---
id: 576182ee0beabd0003161b41
slug: connected-crm-en
published_date: 2016-06-23T00:00:00.000+00:00
author: David S. Williams
title: Connected CRM
subtitle: Implementing a Data-Driven, Customer-Centric Business Strategy
main_color: 259FB8
text_color: 1B7285
---

# Connected CRM

_Implementing a Data-Driven, Customer-Centric Business Strategy_

**David S. Williams**

_Connected CRM_ (2014) cuts through buzzwords like "the cloud," "personalization" and "big data" to show us how best to use information to serve customers in the brave new world of online business.

---
### 1. What’s in it for me? Learn how to make customer centricity the core of your business. 

CRM is one of those acronyms many have heard of but not understood. Short for __Customer Relationship_ Management,_ CRM is an approach to improving business relationships with customers by analyzing data about a consumer's history with a company in order to drive sales growth. The concept has been around for decades, but with new technology come new ways of interacting with consumers. Surely this impacts CRM too?

Of course it does. Connected CRM is a method for engaging customers in your business and putting them at the forefront of your business strategy. Let's explore these concepts and the ways in which they can become lodestones for companies in the future.

In these blinks, you'll find out

  * why different channels for customer contact should address different customer needs;

  * how the ads for iPhone 6s are a good example of customer engagement; and

  * why a focus group attended by all parts of a company will give better customer insight.

### 2. In the digital age, we need to engage customers instead of just showing them ads. 

In the twenty-first century, people the world over can buy countless varieties of products online instead of visiting real stores. But this isolates customers from marketers, forcing marketers to find new ways of reconnecting with their audience.

What exactly does this mean?

It means that today's ads need to go beyond promoting the latest products. They need to _engage_ the customer.

At the moment, many things come between the customer and the company. For example, in online shopping there's a lack of communication; the customer's reactions are hidden behind screens, so it's hard to ascertain how they feel.

Then there are also occasions when the problem is _too much_ communication.

We see so many ads throughout the day that they don't even affect us anymore. That's why ads need to go beyond just communicating and really draw us in. An example of an engaging ad is the iPhone 6 campaign, which displayed iPhone users' own photos. This triggered engagement because everyone who saw it felt they could take great photos with their own iPhones.

There are also trends that make it easier to reconnect with customers. One thing that helps customer engagement is the modern economy's shift toward _platform marketing_, as on Amazon or Ebay.

When someone buys something on your platform, like a cookbook, you could collect that data and use it to target them with food-related ads. However, when your competitors also have powerful platforms, you need to develop more and more sophisticated ways of interacting with your potential customers. So while platform marketing offers the chance to amass loyal customers, actually doing so presents a considerable challenge.

Another way of reconnecting with customers is to use _personalization_. But don't think that just calling a customer by their first name will suffice. Instead, you should group customers into segments, with each segment representing a certain kind of behavior. Then, by preparing messages for each segment at a frequency that is neither too pushy nor too shy, you can guarantee a high level of personalization for your customers.

### 3. Connected CRM is the fusion of business strategy and customer-centricity. 

Any enterprise can be analyzed from two main perspectives: the customer side, which includes the target audience, their age, demographic, and so on, and the business side, which includes the company's organization, its financial management, etcetera. Both sides need to be well nurtured in a successful business. So how can we directly address customers' values while building a great business?

By using _Connected CRM_ or _cCRM._

cCRM involves creating campaigns that balance return-on-investment with long-term behavioral changes in customers. Typical campaigns have two functions: building the brand by polishing its image and building the brand by generating direct sales.

The first aims to place the company above competing brands by emphasizing its quality, reliability and history. The second sets the product line in the foreground and achieves higher returns on investment.

cCRM balances these two approaches. This can only be done if customer values and the integration of the campaign into the business strategy are tackled simultaneously. An example would be a bank addressing customer interest in ecological values by inviting them to invest in a protected forest. This acknowledges the customer's values as well as the bank's need for more investments.

cCRM also involves considering the lifetime of a customer — how long they will use your product — so you don't only fulfill their needs, but also make a profit.

This means understanding that customers differ from one another. One client might regularly buy from you whereas another might buy only once. That's why it's crucial to consider the _Customer Lifetime Value_ or the estimated profit over the customer's expected lifetime. This scale allows you to avoid overinvesting in the wrong customers.

For example, if a member of a deluxe gym signs up for three years and pays $80 per month, the total Customer Lifetime Value for them is $2,880. Therefore, spending more than that amount on that specific customer would not make financial sense.

Since learning about your customer in order to fulfill their needs is so important, we'll devote the following blink to this very issue.

### 4. Use showcases to visualize your customers’ experience. 

There are a lot of concepts out there that try to classify customers: subcultures, trends, gossip and neologisms like "millennials." But do these concepts really tell you what your customers want?

Nope.

Luckily there is an extremely powerful tool for better understanding your customers: the _showcase_.

A showcase provides your company with a platform for authentic customer feedback, rather than simply imagining what the customer experience is like. It should be designed and evaluated by the creative talents and engineers who built the product and by managers, analysts and business leaders. A showcase could be, for example, a customer experience workshop or a visual display that shows the product's capabilities. All the people involved in the showcase will bring something different to the experience: the creative talents will aim for a unique or special design, the engineers will focus on efficiency, and the business leaders will hone in on the overall effect the product will have for the company.

After your showcase, you'll need to continue analyzing it. When you do so, remember to focus on the visualization of the _actual_ customer experience rather than an ideal experience. For example, this could come in the form of a "day in the life" sequence of photographs showing real-life situations or recordings of hands-on experiences.

Your showcase should also visualize negative customer experiences. For example, one health plan company had a nasty habit of flooding clients with paperwork. In the showcase analysis, this was visualized in the form of a waterfall spewing paper into a pit. This gave the company a clearer picture of what they needed to work on.

### 5. Decentralize customer strategies and shape your brand from the customer’s point of view. 

Think about a company you feel takes care of your needs. How exactly do you communicate with them? Chances are it depends on the medium you use, whether it's emails, phone or face-to-face. So why is that?

Because companies that communicate well use a _decentralized customer strategy_. This means engaging the customer according to the channel being used. While, for example, emails and phone calls tend to be formal, Facebook affords a more casual method of communication. These channels should address different aspects of the customer's needs in a complementary way.

For example, the FAQ on the website fulfills the need for quick information, while the call center generates trust in the company by offering immediate support, and the social media channels fulfill the need for updates with regular posts.

If the customer strategy were exactly the same for the website, the call center and the Facebook page, customer needs wouldn't be fulfilled. Instead, it's essential to define a single strategy with specific rules for each channel, while making sure they enhance each other.

While it's important to develop decentralized customer strategies, they should go hand in hand with a customer-centered approach. It's crucial to understand your customer's decision-making process. One way to gain more insight into this is to shape _brand attributes_, _brand benefits_, _emotions_ and _personal values_ from the customer's point of view.

_Brand attributes_ are subjective perceptions of the brand, like Beats by Dre being "cooler" because they're designed by Dr. Dre himself, while _brand benefits_ are exclusive brand features, like when Apple was the first to integrate music, internet and a phone in the first smartphone. _Emotions_ are what someone feels when they use your product, and _personal values_ are what your customer thinks is important — or not — in their life.

By linking these four together, it's possible to gain deep insight into your customers' decisions.

For instance, a customer might want a brand attribute of "good store layout" together with the brand benefit "pleasant atmosphere." This, in turn, could be linked to the positive emotion "solving my problems" and the personal value of "recognition."

Customers who embody all of these preferences would only stay in a store that was well organized, had a nice atmosphere and had staff that both gave them recognition and helped solve their problems. If the store didn't live up to their expectations — say the staff ignore them — they would leave the store instantly.

### 6. An insight platform shows who your customers are and includes a business analytics tool. 

As you can imagine, it's completely useless to collect tons of data without giving it meaning. That's why cCRM recommends setting up an _insight platform_ to make sense of the chaos.

How does that work exactly?

An insight platform defines distinct categories of customers according to their activities and connects strategy, technology, data and analytics. It provides information on what to maximize and what to minimize for the corporation as a whole, as well as showing the direction needed to realize both traditional direct-mailing campaigns and real-time digital campaigns.

Let's give an example of how it can be used. Say you want to launch a product through social channels. Your insight platform should describe your customers according to these features: Their use of specific keywords, their activities on the social channel, how many friends and followers they have, and so on.

From there, you can sort your customers into different categories, such as "the content consumer," who wants to read an article or see a video, or "the sharer," who will share anything fun or exciting to their network.

This allows you to attune your content to the different consumer categories. For instance, if 80 percent of your clients are "sharers," your best bet is to produce content that is easy to share, like short YouTube videos.

Your insights platform should also include a dynamic business analytics tool. This allows you to capitalize on your mountains of data by integrating it so that you can extract meaningful insights.

One of these tools is SAS, short for Statistical Analysis Software, which was originally developed by the North Carolina State University from 1966 to 1976. Such analytics tools are vital for handling the heaps of data you accumulate as your business grows.

### 7. Final summary 

The key message in this book:

**To become successful in today's business world you should use Connected CRM: a business strategy that puts the customers first and addresses their needs realistically, using precise communication through a variety of channels.**

Actionable advice:

**Pick your five favorite products and evaluate your own customer experience.**

By picking and evaluating your personal top five favorite products, you will cultivate your insight into what makes a customer experience really great. You can use this insight to help plan and optimize your next product. You can do this product-evaluation exercise alone, or in a group to get even more ideas.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _SPIN Selling_** **by Neil Rackham**

_SPIN Selling_ (1988) distills the author's 12 years of research and 35,000 sales calls into a coherent and applicable sales strategy that is guaranteed to bring success to any diligent salesperson. You'll learn why traditional sales methods are limited while exploring the benefits of the SPIN strategy when approaching small and large sales opportunities.
---

### David S. Williams

David Williams is the CEO of performance marketing agency Merkle. He is a frequent speaker at industry events and has written numerous articles and white papers on database marketing and analytics, customer relationship marketing and marketing technology.

© David S. Williams: Connected CRM copyright 2014, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

