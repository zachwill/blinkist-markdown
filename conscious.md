---
id: 5d1c79306cee070006fed959
slug: conscious-en
published_date: 2019-07-05T00:00:00.000+00:00
author: Annaka Harris
title: Conscious
subtitle: A Brief Guide to the Fundamental Mystery of the Mind
main_color: A56949
text_color: 734932
---

# Conscious

_A Brief Guide to the Fundamental Mystery of the Mind_

**Annaka Harris**

_Conscious_ (2019) offers a contemplative and probing look at one of life's central mysteries: consciousness. Author Annaka Harris explores two fundamental questions: How do we define consciousness? And how widespread is its existence in the universe?

---
### 1. What’s in it for me? Take a deep dive into the workings of the mind and explore what it means to be conscious. 

When you think about consciousness, you might think about things like self-awareness or the ability to engage in complex thought. Generally speaking, we tend to think of consciousness as it relates to other traits and characteristics that are often associated with human beings. But this may be taking a very limited view of the topic.

In these blinks, we'll explore two big questions: Is there really any external evidence of human consciousness? And is being conscious an important factor in human behavior? To answer these questions, we'll follow some logical arguments to reach some conclusions that might surprise you. 

We'll also look at just how widespread consciousness may or may not be in the universe. Conscious things other than human beings may not experience the same level of complex thought that we do, but does that mean they don't experience anything at all? 

In these blinks, you'll find out

  * what kind of communication takes place between trees;

  * what LSD experiences can tell us about consciousness; and

  * what happens when the human brain gets divided in two.

### 2. To understand consciousness, we must question our understanding of experience and intuition. 

When you think of what it means to be conscious, or to experience consciousness, what comes to mind? For something that is so central to our lives, consciousness continues to be an elusive and mysterious subject, and one of the primary reasons for this is that we tend to have a variety of ideas as to what it really means to have consciousness.

To gain a better understanding of what consciousness _is_, we can begin by ruling out what it _isn't._ But in order to begin this inquiry on a solid footing, it would be helpful for us to agree upon a rough idea of what it is we're talking about in the first place.

For this, we'll turn to the esteemed philosopher Thomas Nagel, who in 1974 suggested that "an organism is conscious if there is _something that it is like_ to be that organism." Essentially, this means that a conscious organism is one that has some sort of experience. 

So it would _be like something_ to be you in this moment, since you're currently experiencing things. But it may not be like anything to be the seat you're sitting on, if the chair isn't experiencing anything, right?

With that in mind, we can start looking at things that are often closely related to experience to see if they are indeed aspects of our consciousness or can be ruled out altogether.

However, it's important to note that when we do this, our intuition is playing a big role. Since no one knows for sure what consciousness is precisely, we often rely on our intuition to tell us what seems right or wrong when we consider questions like which things in the world experience consciousness. 

Ironically enough, intuition is also a mysterious thing that science has yet to fully grasp. Basically, intuition is that gut feeling you have when you know something is amiss and yet you can't pinpoint why. 

Maybe you see a stranger getting on the subway and sense that he poses a threat. Your intuition could be due to the man's face being flushed and his eyes being dilated. These are signs that he may become violent, and you may have noticed them, but only on an instinctual, subconscious level.

However, our intuition can, and often does, lead us astray as well. Early on, our instincts told us that the Earth was flat until someone noticed that the stars in the sky suggested otherwise. And today, many people feel instinctively more worried when they board a plane than when they get into a car, even though a car ride comes with a much higher chance of injury.

So, in the blinks ahead, listen to your intuition, but also try to keep an open mind about the possibilities of consciousness.

### 3. Traits we often associate with consciousness aren’t exclusive to human behavior. 

There are very few things we can say for certain about consciousness, but one of them is that we humans are indeed conscious. Of course, our form of consciousness is the only one we directly experience and are therefore familiar with. As a result, our intuition can lead us to believe that only things with human-like qualities and behaviors can have consciousness.

However, if we take a close look at our own behaviors, we can see that they're not so exclusive after all.

Consider the revelatory research that has gone into the largely unseen behavior of the plant world. In studies of Douglas fir and paper birch trees, researchers have found that there is a lot of activity going on underground, amid a vast system of fungi and roots called the _mycorrhizal network_.

Ecologist Suzanne Simard was amazed to find out that these two species of trees routinely help each other out in times of need, with nutrients being sent underground, from one tree to another. Not only that, the Douglas fir can identify a tree that is its offspring, send it nutrients and communicate with it, in order to help it survive environmental threats.

In general, plants are aware of and sensitive to their surroundings. They can send toxins into the mycorrhizal network to fight off other plants that pose a threat. And some plants, like ivy, will even explore their surroundings above ground, feeling their way around their environment to find ideal structures to support their growth.

It's safe to say that many plants also have some form of memory. The venus fly trap, for instance, won't snap shut and entrap its prey until two triggers are set off, which means it must remember that the first trigger was initiated.

The more we learn about plants, the more it becomes apparent they're not as far removed from us as we thought. After all, the genes that cause plants to react to darkness and light contain the same DNA found in humans!

This opens up two possibilities. The first is that plants have some form of experience — and therefore some form of consciousness. The second is that phenomena like having memories, sensing light, responding to danger and helping others aren't related to consciousness at all. 

In the next blink, we'll look at more human characteristics like these, and we'll see how they're quite separate from consciousness altogether.

### 4. Consciousness is separate from the decisions we make and the thoughts we have. 

For the most part, our behavior in the world is a matter of automatic cause and effect. Something happens and we react to it instinctively. And as it turns out, these instinctive reactions have little to do with our consciousness.

This is due, in part, to the fact that human senses reach the brain at different times, and what we consider conscious experience happens only after sight, sound, smell and touch have gone through a _binding process_ in the brain. 

So, in actuality, you could say that consciousness is "the last to know" about what's going on.

Many studies have been conducted to take a deeper look into the timing of our perceptions and reactions, and they've raised interesting questions about how much conscious thought goes into our actions versus how much of it is due to instinctual programming in our brain. 

In many ways, the system for deciding what you do in the present moment is actually like the system that controls a driverless car: it's taking in information about your surrounding environment, processing that information, and reacting to it. What your consciousness is doing is witnessing these decisions and fitting them into the ongoing experience that is your life. 

In other words, your brain is in the driver's seat, and your consciousness is just along for the ride.

Another aspect of human nature often associated with consciousness is complex thought. But here too, upon closer look, we can see that the two are separate. 

Let's say you just had a thought about an old friend from grade school that you haven't seen in ages. Did you consciously bring up this thought, or did it seemingly just happen? 

Generally speaking, we actually have little control over the thoughts that come and go. Just like everything else, they're a reaction to what's going on and the result of our brain's programming. The latter, in turn, is the result of genetics, instincts and what we've learned from past experiences.

Of course, we can attempt to consciously plan out our actions ahead of time and choose to do something in the future. But when it comes down to the moment-to-moment choices we make, we can see that these behaviors are less a matter of consciousness than they are a result of inherited, automatic brain functions.

### 5. Consciousness can also be seen as separate from our sense of self. 

The brain is capable of creating a number of illusions for itself. As we already mentioned, there's the illusion that we are making conscious decisions with every movement and action that we take. 

But there's also the illusion that we are receiving all of the information from all of our senses at precisely the same moment. It seems like everything is unified, but in reality, our sense of touch takes longer to reach our brain than our sense of hearing, for instance. 

Some people even experience this lack of synchronicity. For example, those suffering from the disorder known as _disjunctive agnosia_ will find themselves in the disorienting state of sight and sound no longer being synched up.

And then there's something that we've all experienced: our sense of self. This, too, is generally associated with consciousness; it is the perception that everything going on around us is happening to one centralized subject known as our _self_. The effect of this perception is that we tend to think of the self as being separate from all that it perceives, rather than being a part of it.

But in certain states, the apparent connection between self and consciousness also falls apart. For instance, when people take psychedelic drugs like LSD, they often experience a heightened awareness of their surroundings. As a result, their sense of self gets dissolved, and they feel more at peace with and connected to the world, rather than apart from it. 

Such a feeling of being more connected to the world and less caught up in the self can also come from meditation. Practitioners also experience a heightened awareness that can short-circuit the normal binding processes in the brain. This leads them to feel less attached to the self and more at one with the world.

These phenomena reveal that the self is a mental construct that is directly related to our perceptions of the world around us. The moment these perceptions change, so too does our sense of self. That sense of self can even disappear, while our consciousness remains. Therefore, our consciousness and our sense of self must be distinct from each other, since we can have one without the other.

In a way, the idea of the self is not unlike the concept of the flat Earth: it seems like reality until you step back and allow yourself to look at things in a different way.

### 6. The idea of all matter being conscious may sound crazy, but it is also scientifically sound. 

So now's a good time to ask the following question: If there are no human behaviors or characteristics tied to consciousness, is it possible that consciousness isn't limited to humans at all? And once we start opening our minds to this possibility, we can even take it to its extreme and ask an even more provocative question: What if all matter has some form of consciousness? 

At first, it may sound fantastical to suggest that all matter is conscious, but this theory, known as _panpsychism_, is far from being scientifically unsound. In fact, it agrees with everything we know about biology and physics.

The closer we're able to inspect the matter that human beings are made of, the more apparent it is that we're made of the same exact elements as everything else in the universe — from plants on Earth to the distant stars. 

Since this is the case, it stands to reason that this matter wouldn't suddenly gain consciousness in some instances and not in others. In scientific terms, if this happened, it would be considered a _radical emergence,_ and science generally finds radical emergence to be unwelcome, since it just raises more questions. 

Science likes clean, simple answers — and in many ways, panpsychism provides just that.

As a matter of fact, there are scientists and philosophers who've been supportive of panpsychism going as far back as the 1930s, including biologists J.B.S. Haldane and Bernhard Rensch. 

As philosopher Galen Strawson points out, all physical phenomena are considered a form of energy, according to the laws of physics. Panpsychism, in turn, is a valid hypothesis that posits experience as being an intrinsic part of that energy. In no way does it contradict physics, whereas trying to draw a line that says some things have consciousness and some things don't is bound to run into scientific problems.

For the most part, those who disagree with panpsychism tend to assume that the theory is suggesting that a rock has the equivalent of human consciousness. But this isn't what panpsychism is suggesting at all. Instead, it is open to the likelihood that there are countless forms of consciousness, some of which may be impossible for humans to comprehend.

But if you can imagine what it would be like to only experience the difference between light and darkness, or heat and cold, without thinking about these differences but simply recognizing them, then you may begin to understand what a limited form of consciousness might be like.

### 7. If we revise some of our limited ideas of consciousness, panpsychism could provide valuable answers to complex questions. 

The concept of panpsychism is far from having widespread acceptance. After all, it calls for a pretty big shift in our assumptions and intuitions about the world around us. But as we know, sometimes our intuition is wrong.

Even among scientists who are willing to consider the theory of all matter being conscious, some of them balk at the notion that a kidney or liver would have its own consciousness within a conscious human's body. 

And yet there actually is scientific evidence of multiple consciousnesses existing within the same human being. The evidence comes from _split-brain studies_.

Since the 1960s, researchers have been studying patients who've undergone a _corpus callosotomy_ procedure as a way of curing debilitating seizures. This procedure effectively separates the two hemispheres of the brain by severing the _corpus callosum_, which is responsible for all communication between the two halves.

Remarkably, the patients were found to be relatively unharmed by the procedure, but it did leave them with some interesting changes. 

Essentially, the patients began to have two different experiences, with information being received from one hemisphere no longer being communicated to the other. This gets a little complicated, since the right hemisphere of the brain controls the left-side limbs of the body, while the left hemisphere controls the right-side limbs. And here's one more fact to know: the left hemisphere also controls a person's speech. 

With that in mind, let's say a patient was holding a key in his left hand, but couldn't actually see it. If he was asked, "What are you holding?", he would respond that he wasn't holding anything. Why's that? Well, the right hemisphere of his brain would recognize the sensation of the key in his hand, but his left hemisphere — and therefore the part of his brain that formulates speech — would be unaware of it.

Thus, it is possible for two different conscious experiences to exist in one body. And, in fact, this may explain how complex thought and brain functions actually came to exist. 

Split-brain studies show that consciousness is malleable — that it can easily adapt when there are changes to the information it receives. This opens the door to the possibility that human consciousness, in all its complexity, is the result of the combining of matter that would otherwise have its own, less complex consciousness.

Unfortunately, we're still far from truly understanding the answers to important questions like these. But it is important to keep thinking creatively about consciousness. 

And just as the Higgs boson helped to solve one of the longstanding riddles of particle physics, there's a possibility that we may find the elementary particle that unlocks the mystery to consciousness in matter.

### 8. Final summary 

The key message in these blinks:

**Consciousness is a mysterious thing, but being conscious essentially means that you are having an experience. The one thing we know for sure about consciousness is that human beings have it, but that's about it. Upon closer inspection, however, we find that consciousness isn't tied to any specific human thoughts or behaviors. When we rule out these human aspects of consciousness, we can begin to speculate as to whether other things in the world also have conscious experiences. The theory of panpsychism goes even further, claiming that consciousness is an intrinsic element of all matter.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Moral Landscape_** **, by Sam Harris**

Annaka Harris asks some thought-provoking questions. If you'd like to keep exploring the gray area between philosophy and neuroscience that she has tread, then you're likely to enjoy what her husband, Sam Harris, has to say. He's the host of the popular podcast _Making Sense_, and he's also a science writer and a neuroscientist.

For some riveting insights into the issues of human morality currently being raised by today's most innovative and forward-thinking research, head over to the blinks to _The Moral Landscape,_ by Sam Harris.
---

### Annaka Harris

Annaka Harris is a writer whose work has been featured in the _New York Times_. She also works as a consultant for other science writers, and she was a collaborator in creating the Mindful Games Activity Cards. She is also the author of the children's book _I Wonder_ and has two children of her own with the podcast host and neuroscientist Sam Harris.

