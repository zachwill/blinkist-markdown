---
id: 55e45dd12852cc000900002b
slug: consider-the-fork-en
published_date: 2015-09-02T00:00:00.000+00:00
author: Bee Wilson
title: Consider the Fork
subtitle: A History of How We Cook and Eat
main_color: 9F8651
text_color: 52452A
---

# Consider the Fork

_A History of How We Cook and Eat_

**Bee Wilson**

Eating and cooking have always been crucial to our survival, but over time they have also become a subject of cultural and scientific interest. In _Consider the Fork_ (2012), author Bee Wilson blends history, anthropology and technology to tell the fascinating story of the evolution of cooking, while also taking a closer look at the creation of cooking tools and how they have shaped our culture and eating behavior.

---
### 1. What’s in it for me? Discover how pots, pans and eating utensils save our lives and change our anatomy. 

One thing all mankind has in common is eating, yet this most human of habits varies greatly across different cultures and over time. We take things like forks, knives, pots and pans for granted, and would probably feel a bit lost without a refrigerator. But is there a greater value to all these things than just our food culture?

In fact, the evolutionary impacts of cooking and the ways it has helped humanity for millions of years can be quite surprising. At its core, _Consider the Fork_ examines the notion that our cooking not only changes how and what we eat, but in some ways changes who we are and will become.

In these blinks, you'll discover

  * how humanity came to eat poisonous foods like manioc;

  * the role of the Lord's prayer in cooking eggs; and

  * why kitchen staff in medieval Europe walked around naked.

### 2. The invention of pots and pans for cooking was one of the greatest breakthroughs in human history. 

When we think of cooking, we mostly conjure up images of tasty dishes or recipes rather than considering the history of cooking, or even thinking about how our ability to boil food came to be. But if you think about it, it's quite intriguing that humans even came up with this technique in the first place.

Archaeologists estimate that the oldest pots ever found date back as far as 10,000 BC, and were created by people inspired by the shells of shellfish and turtles.

Gradually, pots and pans became less rudimentary and, in turn, made cooking more effective.

The majority of the earliest pots were made from clay, which had the disadvantages of tainting the food with a strange flavor, and of the pots themselves being fragile and easily breakable.

Then, around 3,000 years ago, the Mesopotamian people of Egypt and the people China began making pots out of metal. This opened the door to many new methods of cooking; at the same time, pots became easier to clean, didn't affect the taste of the contents and didn't crack in the fire.

Far more importantly, though, cooking with pots actually saved lives. Before pots, we had to chew all of our food, meaning that without teeth, one would simply starve to death. And because losing teeth from accidents or illnesses was a common occurrence for millennia, this was a very real threat.

When humans began using pots, we gained the ability to prepare food that didn't need chewing, like soups, mashed vegetables and porridge. Thus, the humble pot helped many escape death.

Pots and pans also allowed us to eat plants that would otherwise be toxic when raw. Take cassava, or manioc, as an example. These days, manioc is the third-most important source of carbohydrates in tropical regions, but it can only be eaten when cooked properly, since it contains toxic levels of cyanide in its natural state.

### 3. The use of fire has been the biggest constant in the history of cooking and eating. 

Long before pots and pans, we could at least heat our food. We can't say exactly how we discovered that we could use fire to prepare food, but it was no doubt a hit-and-miss affair, with the fire needing to be lit and maintained at all times.

Anthropologists believe the first time we were able to cook with fire — approximately 1.8 million years ago — was the most significant moment in our evolution from apes into humans. Why? Because cooked food is more easily digested, enabling us to draw more nutritional value from it than raw food. This additional energy aided the development of the human brain. Essentially, cooked food made our ancestors smarter!

As well as its integral role in cooking, the fireplace has always been the hub of every dwelling. People would have open hearths for cooking, heating and socializing, despite open fires being very dangerous.

It was not unusual for children to fall into the fire or for women's clothes to be set alight from standing too close to it. In medieval Europe, kitchen staff would even work naked in order to cope with the oppressive heat!

The smoke from open fires can also be fatal, causing bronchitis, heart disease and cancer, which is still a threat in developing countries, where people continue to cook over open fires.

Over time, modern kitchens have developed a far safer way of utilizing fire. By the 1830s, closed ovens and stoves were introduced in the United States and Europe, due to the increasing availability of materials like coal and iron during the Industrial Revolution.

Although people were initially skeptical, this new way of cooking became more common in the 19th century. Gas stoves then appeared during the 1880s, and from the 1920s onward, electric cooking became increasingly popular.

While we've managed to significantly reduce the danger implicit in early cooking methods, our fascination with fire is still very much alive: think of how much we love a campfire or barbecue!

> _"The average Third World open cooking fire — fuelled by coal, dung or wood — generates as much carbon dioxide as a car."_

### 4. Refrigerators have completely changed our behavior toward cooking and eating. 

Fire is not the only element we've harnessed for our survival; utilizing ice also revolutionized our eating habits, making them safer and more convenient.

It's hard to imagine life without it, but the refrigerator is actually a more recent invention than you might think.

Until refrigeration, we preserved foods like meat and fish by salting them, and saved fruits by drying them in the sun.

People began using the first modern refrigerators around the 19th century in the United States, when trains were equipped with refrigerated compartments in order to transport food over long distances. However, most people had their doubts about the quality of such food.

When the 1930s rolled around, the first refrigerators manufactured for household use became available in the United States. It took Europe slightly longer to catch on, though, and by 1959 only 13 percent of households in the United Kingdom had a refrigerator, compared to 90 percent of households in the United States.

It may not be obvious, but the refrigerator has changed our culinary habits in many ways.

When refrigerators first became ubiquitous, their main advantage, of course, was that they allowed us to store food for a longer time. This, in turn, had two resulting benefits: First, we could enjoy food that would have otherwise spoiled, like fresh fruits and vegetables, for far longer than before. Second, a daily trip to get groceries was no longer necessary, as we could live off of our supplies at home. The bulky economy packs that fill our supermarkets today would have been unimaginable without a fridge.

The possibility and ease of storing food has even changed what we eat. 

Take yoghurt: as a traditional Middle Eastern food, there was a time when it was almost unheard of in Europe and the United States. Only when refrigeration became common did it suddenly became an everyday food item in the Western world, and a million-dollar industry as well!

### 5. Our culture, our eating habits and even our anatomy are influenced by our cutlery. 

As we pace up and down our modern supermarket aisles, particularly through the ready-made section boasting fancy sandwiches and fruit salads to go, it seems we could easily survive without knives. 

But, of course, things were once very different. In fact, cutting, chopping and slicing are the oldest and most primitive ways of processing food. That said, the use of tools to cut is not limited to humans — apes and chimpanzees do it too.

The oldest cutting devices, which were made of stone, were found by archaeologists to date back to around 2.6 million years ago. That's more than 2.5 million years before the first pot.

Gradually, these cutting tools became more refined and practical, as we started fashioning them out of bronze, then iron, and finally from steel, as we do today.

But knives aren't just used for cutting food: they've had various different meanings and connotations throughout the world and over time.

Throughout the Middle Ages, it was standard practice for everyone — men, women _and_ children — to carry a knife on their person to use for eating, as well as for self-defense! 

It wasn't until around the 17th century that it became common to set forks and knives on the table. As a result, these knives were made to be more blunt, as they weren't required to inflict pain on another person, but merely to cut meat.

Clearly, having cutlery at the table greatly influenced our eating habits. What is more remarkable, though, is that it may even have altered our anatomy.

Nowadays, it's pretty normal to have a slight overbite. But archaeologists have evidence that until 250 years ago, it was normal for people in the Western world to actually have an edge-to-edge bite.

Why? In the Middle Ages, people used their front teeth to rip meat apart when eating. Then, after table manners were introduced along with the knife and fork, it was no longer necessary to do so. Gradually, a different kind of denture became the norm.

### 6. The way we eat and cook depends heavily on the culture we live in. 

So, cutlery and culture are more intertwined than one might have thought, and the way we use cutlery has indeed changed over time. But it's not just time that changes our habits; different cultures have formed their own rules at the dinner table, too.

Do you think there's only one correct way to eat with the cutlery laid out in front of you? You would be wrong! Conceptions of correct and appropriate etiquette vary greatly across cultures.

While it's normal in the United States to cut everything with your knife and fork, then lay the knife aside and eat holding the fork in your right hand, this would be considered strange in Europe, where the knife and fork are held at all times and you cut everything as you eat.

Then, of course, there are the cultures that don't use knives and forks at all. Chopsticks, for example, can make it tricky for foreigners to avoid a faux pas in front of their hosts.

In China, food is placed in large bowls at the center of the table, where everyone takes portions for themselves using their own chopsticks. When doing so, you should be careful to not take too much from one bowl, as etiquette says that no one should be able to tell which is your favorite food.

More social graces that can vary considerably relate to eating noises. Slurping and burping when eating are considered ill-mannered in Western countries, but are signs of satisfaction and praise for the cook in Japan, for example.

So, would it perhaps be better to discard forks, knives and chopsticks entirely? Well, in some cultures, not using any cutlery also carries with it certain rules.

Traditional meals in the Middle East are eaten with fingers only. But beforehand, you must be sure to wash your hands, and, most importantly, avoid using your left hand while eating; it's considered dirty and is reserved for use in the restroom.

> _"The foods we eat speak of the time and the place we inhabit."_

### 7. The art of measuring in cooking has been a topic of ongoing interest and change over the last few centuries. 

Cooking requires observation and measurement: Heat, time and quantity are of the utmost importance. So how exactly can we measure these?

Before the luxury of modern measuring devices, people had very creative approaches to measurement. Prior to the invention of the modern clock, for instance, people would measure time by reciting prayers.

Directions such as "stir the sauce while reciting the Lord's Prayer" were commonplace. Cultures who knew the prayer were familiar with its pace and timing from church services, making it an effective timing method.

But what about gauging heat before we had thermometers?

In order to tell when the oven was fully pre-heated and ready for the dough, bakers would place their hand directly inside the oven. They would know when the temperature was high enough simply by the amount of pain they felt!

And as for quantity?

During the Middle Ages, quantity was measured in cups, such as for flour, water and sugar. But this was a fairly inaccurate method, since cups measure volume and density rather than weight, and the sizes of the cups themselves also varied. Nevertheless, people still use this method today in the United States — the only place in the world where cup measurements continue to be printed in cookbooks.

In general, though, our measuring techniques have become increasingly sophisticated.

With the proliferation of devices that enable precise measuring, many cooks are fascinated with the exact science of cooking up a feast.

Some are discarding standard thermometers in favor of tools that can measure core temperatures, like the inside and outside of a steak. There's even a device to measure the pH value of food, supposedly helping chefs perfect their sorbets and sauces.

> _"The difference between a truly great dinner and an indifferent one might be 30 seconds and a quarter of a teaspoon of salt."_

### 8. We want our culinary world to stay the same, but our methods of cooking and eating will keep evolving. 

It's common to speculate about how humans will eat and cook in the future. Remember those who predicted that, by the new millennium, we would be surviving on pills or liquids designed to provide us with the perfect scientific balance of carbohydrates, proteins, fat and vitamins? While this is technically possible — astronauts use these kinds of processed foods, after all — we mostly still eat and cook "normal" food using traditional methods.

In fact, we tend to be rather rigid and resistant to change when it comes to cooking and eating, and this is the case for three main reasons.

Historically, experimenting with food was invariably a potentially dangerous task; for example, new ingredients could turn out to be toxic. So for evolutionary purposes, we cultivated a necessary caution regarding our food.

Second, as we have seen, it took a considerable period of time to craft our pots, pans and knives. Since their invention, though, these tools have proven very useful, so why try to fix something that isn't broken?

Third, cooking is intimately intertwined with emotion. Preparing potato soup in the same way as one's grandmother brings about a certain nostalgia and a connection to old family traditions.

Having said that, new methods of cooking do surface from time to time. But instead of replacing traditional methods, they tend to be passing trends. Just think of molecular gastronomy, a term coined by the French scientist Hervé This in 1990. The method is based on biological and chemical processes, and challenges traditional ways of cooking.

Chefs have also come up with surprising new combinations of tastes and textures. Examples include using carbon dioxide to produce foams, making sweets from oil, creating hot ice cream or inventing melon caviar.

Throughout our evolution, cooking has gone from a means of survival to an art, allowing us to enjoy a host of gadgetry and innovative cooking techniques today.

> _"The food we cook is not only an assemblage of ingredients. It is the product of technologies, past and present."_

### 9. Final summary 

The key message in this book:

**How we cook and eat has changed immensely throughout history, influencing our culture, perception, identity and health. Behind the kitchen tools and technology we take for granted today lies a fascinating story of survival, harnessing the elements, and the continual invention and refinement of how we eat.**

Actionable advice:

**Listen to your intuition while cooking.**

In modern kitchens, we have an overwhelming assortment of ingredients at our disposal; millions of recipes dictate what to do and how to do it. But cooking is more than just combining the right amount of ingredients — we are all unique and have individual tastes. So, instead of slavishly adhering to the recipe, trust your intuition in the kitchen once in a while. Cooking should be personal and is more about art and expression than how well you can follow instructions. 

**Suggested further reading:** ** _In Defense of Food_** **by Michael Pollan**

_In Defense of Food_ is a close examination of the rise of nutritionism in our culture, and a historical account of the industrialization of food. An expert in food ecology, author Michael Pollan takes a look at the way in which the food industry shifted our dietary focus from "food" to "nutrients," and thus narrowed the objective of eating to one of maintaining physical health — a goal it did not accomplish.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Bee Wilson

Bee Wilson, PhD, is a British historian and food writer. Thanks to her weekly food column "The Kitchen Thinker" in the _Sunday Telegraph,_ she was named food journalist of the year by the Guild of Food Writers in 2004, 2008 and 2009. Her other works include _The Hive: The Story of the Honeybee and Us_ and _Swindled: From Poison Sweets to Counterfeit Coffee._

