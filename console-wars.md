---
id: 5475165530386200091d0000
slug: console-wars-en
published_date: 2014-11-28T00:00:00.000+00:00
author: Blake J. Harris
title: Console Wars
subtitle: Sega, Nintendo and the Battle that Defined a Generation
main_color: 6CC9E5
text_color: 3C7080
---

# Console Wars

_Sega, Nintendo and the Battle that Defined a Generation_

**Blake J. Harris**

_Console Wars_ chronicles the epic, industry-shaping rivalry between video game developers _Sega_ and _Nintendo_ in the early 1990s. In five short years, Sega went from being a widely mocked underdog to U.S. market leader in console games, a dominance that essentially dictated the future path of gaming.

---
### 1. What’s in it for me? Learn how a corporate rivalry launched the modern video games industry. 

Gamers today have it easy. If you want to play a video game, your choices are many, and often overwhelming — first-person shooter or fantasy exploration, dedicated console or PC or handheld.

Just 20 years ago, however, things were very different.

In the 1990s, if you played games, you played a Nintendo game. In the United States, this company dominated the market and carefully controlled which games could be played on its consoles.

That is, until Sega and its rambunctious blue hedgehog wreaked havoc in the gaming industry.

These blinks tell the captivating story of how Sega and Nintendo battled for control of the video games market. With a flair for shock marketing, blood-splattered games and frisky gaming characters, Sega turned the industry on its head and set it on the modern path we know today.

In the following blinks, you'll also discover:

  * why gamers prefer their characters to bleed red and not gray;

  * how Sega blundered and helped create the _Sony Playstation_ ; and

  * how a blue hedgehog with an attitude tripped up a polite Italian with a mustache.

### 2. Nintendo's rapacious strategies earned it market dominance in the U.S., but also left it vulnerable. 

If you owned a game console in 1990, there's a good chance it was made by Nintendo.

Nintendo, a Japanese consumer electronics company, completely dominated the market for video games. In the United States at the time, the company claimed some 90 percent market share!

This dominance stemmed largely from Nintendo's strict control over who could create games for its console. If a company was interested in developing a game for Nintendo, it would have to purchase a game cartridge at a fixed cost of $10 — which meant games always sold for more than $10, regardless whether it was a hit or a flop.

This policy ensured that Nintendo would collect huge profits on highly successful games such as _Street Fighter II_, developed by _Capcom_ ; it also allowed Nintendo to closely control which companies produced games for its consoles.

Nintendo obviously was protective of its market clout. When a group of developers tried to create games for the console without Nintendo's approval, the company sued them. For many developers, the threat of losing favor with Nintendo (which could result in being ostracized from the market entirely) kept them loyal and obedient to the company's dictates.

However, being a market leader has its downsides. Nintendo had to continuously push new products and take large risks to maintain its position in the market.

Yet many of the products Nintendo pushed out were of poor quality, and in the rush to release new products, it made many mistakes. For example, the company's updated 16-bit console, released in the United States in 1991, was not compatible with games from its previous console. This angered many console owners, who were forced to buy their favorite games a second time.

These sorts of mistakes, combined with the ire of games manufacturers that Nintendo had effectively shut out of the market, created an opening for a new competitor. That company was Sega, and it arrived ready to take advantage of Nintendo's weaknesses.

### 3. Sega strove to position itself as the punk rebel to Nintendo’s boring parent, with great success. 

The epic battle of David and Goliath, while an often overused metaphor, is apt in the case of Sega versus Nintendo.

When Sega made its entrance into the video game market, no one expected that the company could take on the dominant Nintendo — and yet it did. But how did the company do it?

Sega identified the perfect market niche. By pinpointing all of Nintendo's flaws and missteps, the upstart created a market image for itself that was exactly the opposite.

One of Nintendo's weak spots was that the company was seen as over-controlling, even paternalistic. Many gamers also felt that Nintendo's titles were too focused on children's games.

So Sega decided to offer more freedom to game developers, and what's more, encouraged the development of more titles for adults. As a result, the range of Sega games was much more diverse as developers had a freer hand to create what they wanted.

And importantly, Sega wanted its company brand to contrast with Nintendo's staid, monolithic image. It wanted to be seen as fun, even rebellious ‒ the underdog with a mean bite!

Even _Sonic the Hedgehog_ — Sega's mascot — was designed to contrast with Nintendo's main game character, _Mario_. When Sega released its 16-bit console, the _Genesis_ (called the _Mega Drive_ in Europe and Asia), the company included the Sonic the Hedgehog game for free.

While Mario was a clean living, family-friendly cartoon man with a mustache, Sonic was a prickly, bright blue hedgehog that was unruly and moved incredibly fast — just like Sega.

The Sonic character was an instant hit and quickly became a pop icon, as many people, not just gamers, believed Sonic's attitude captured the speed and zaniness of the decade.

### 4. Sega pulled out the marketing stops to get people excited about Sonic, and ignore Nintendo. 

_Coke_ versus _Pepsi_. _McDonalds_ versus _Burger King_. _Apple_ versus _Microsoft_. Many of the top corporate players market themselves deliberately against their main competitors.

Sega employed comparison marketing to gain an edge over Nintendo, as we've seen with regards to the company's image. But Sega didn't stop there. Its sales strategy too sought to capitalize on the differences between the two companies. 

When Nintendo released its 16-bit _Super Nintendo (SNES)_ console, Sega took its own product, the _Genesis_ console, on a tour of shopping malls in the United States, calling the event the _Sega World Tour_.

The strategy was to poach customers who were essentially looking to buy Nintendo's new SNES. The tour showcased the antics and gaming excitement of Sonic the Hedgehog, in stark contrast to Nintendo's tired, old Mario.

It was a great success. The Genesis continued to sell well, despite the release of the SNES.

Another head-to-head challenge came about when both companies released a version of _Mortal Kombat_, a fantastical fighting game. Nintendo attempted to make its version less realistic, even choosing to make the fighters' blood the color gray instead of red.

Sega, in contrast, offered an uncensored version, complete with red-colored blood and the option to enter a special code to make the match even bloodier. The Sega version was much more popular.

And even though Nintendo had a much larger marketing budget than Sega, the upstart revolutionized game releases with an innovative approach and lots of flash.

Previously, to buy a game title you would go to a game retailer and see what was new. Sega transformed this process, making the release of new titles big media events.

When Sega released _Sonic 2_, the company declared it "Sonic 2 Day," and organized events around the world to coincide with the launch. It worked, and the game, the console and the company garnered tremendous publicity as a result.

So Sega quickly and successfully established itself as an unconventional, creative and radical gaming company. But behind the wacky facade, Sega still had to do business.

> _"Video games weren't just for kids; they were for anyone who wanted to feel like a kid."_

### 5. An uncanny sense of timing and a well-oiled corporate machine: Sega’s success had a broad foundation. 

Even the most creative, unconventional companies still need structure and talented employees to function. Sega's success in America began when _Sega in America (SOA)_ got a new head, Tom Kalinske.

Kalinske had previously worked at _Mattel_, where he'd helped create the powerful, muscular character _He-Man,_ as well as helped to resurrect the popularity of _Barbie_.

When Kalinske took the helm at Sega, he first had to smooth a lot of mistrust that had grown among team members at the company.

Much of the ire was directed at one employee, named Shinobu Toyoda. Many of Toyoda's American colleagues thought that he was a spy for Sega's parent company in Japan.

Kalinske put a stop to this. He openly declared his trust for Toyoda and encouraged others to do the same. Kalinske united the company, created a team spirit, and developed a creative and positive atmosphere.

Sega needed this. The company could only overcome one of its greatest obstacles — distribution — if employees worked together.

Many retailers were afraid that if they stocked Sega products, they'd lose Nintendo as a customer. The Sega team had to work hard to persuade retailers that this was not the case.

For example, Sega had to put a lot of effort into convincing _Wal-Mart_ to stock its products. Sega's top brass had to meet with Wal-Mart executives regularly, while the marketing department worked overtime to come up with effective advertising strategies.

The company's efforts eventually won _Wal-Mart_ over.

Timing was also a key skill of Sega executives. They were experts at knowing when to launch advertising campaigns or release new products, and the company continued to succeed.

Sega also had to anticipate Nintendo price cuts and react accordingly. On one occasion, executives found out that Nintendo was about to reduce prices for its consoles the night before the announcement. The Sega team worked the whole night and managed to cut its console price first. Thus customers thought Nintendo was copying Sega's price cut!

When Mortal Kombat was released, Sega eclipsed Nintendo as the market leader. Yet this was the peak for the company; and today, Sega consoles are only a memory.

So what happened? Read on to find out.

### 6. Going big on hardware instead of gaming software confused customers and cost Sega its crown. 

There are two main paths for gaming companies like Nintendo or Sega, in terms of growth. You could create new software by developing games, or create new hardware by developing consoles.

Making such a strategic decision is tricky. You have to consider what strategy will require more time and effort, and what choice is likely to bring in more sales.

Sega, however, knew what it wanted: it opted for the hardware route, which was a risky decision.

When the Genesis and SNES consoles both needed updating, Nintendo and Sega used vastly different strategies. Nintendo played it safe with its _Super FX chip_, which offered a rather simple upgrade to its existing console. Sega, on the other hand, went for something revolutionary: the _Sega CD_.

The Sega CD was technologically superior to Nintendo's upgrade, but cost much more to develop and in turn, was more expensive for retail customers. While the technology was innovative, the cost was prohibitive, and few people bought it.

Sega also faltered with its follow-up console to Genesis, the _Saturn_. The company failed to offer enough new, high-quality Saturn games to satisfy customers who had bought the console.

In short, Sega had too many devices. In addition to Genesis and Saturn, the company also had the _Game Gear_. Game developers couldn't churn out titles fast enough for so many different technologies.

Moreover, Sega couldn't match the demand for Saturn. It struggled to produce and distribute enough consoles to satisfy its fans, which turned many against the company.

Thus Sega's rapid growth came to a halt. The nail in the proverbial coffin came when Nintendo released its game _Donkey Kong Country_ in 1994. Since Sega's brief moment at the top of the gaming world, Nintendo had caught up again and surpassed Sega in sales.

### 7. The success of Sega in America began to dim when its Japanese parent company meddled too much. 

We tend to think of companies as one big entity. There's one _General Motors_ (GM), one _General Electric_ (GE), one Nintendo and one Sega. However, company structures are far more complex.

_Sega of Japan (SOJ)_ was the "original" Sega, based in Japan. The subsidiary, Sega of America (SOA), was based in the United States and managed by Tom Kalinske. Interestingly, SOA was the much more successful of the two companies.

In the United States, Sega was a market leader, but its parent company never reached the same status in Japan. SOA's market strategies were more lucrative, but also very different.

SOJ was never ready to take the risks that SOA did. Al Nilsen, SOA's global marketing director, realized this when he travelled to Japan and went to a restaurant with his colleagues.

His Japanese colleagues ordered him _fugu_, a fish that if not prepared correctly, can be poisonous. Nilsen tried it, then offered some of the fish to his colleagues. They politely declined.

Nilsen realized then that SOJ and its employees liked to play it safe, unlike the team at SOA.

SOJ had only allowed SOA so much independence in the beginning because Hayao Nakayama, SOJ's head, had trusted Kalinske. Gradually, however, SOJ began to chip away at SOA's freedom. The parent company listened to SOA's input less and less, or just ignored it altogether.

A flash point came when SOJ overruled SOA on the issue of the Genesis console's life cycle. SOA had calculated what it thought to be the ideal lifespan for the console, only to have SOJ come up with a completely different calculation.

SOJ chose to enforce its calculation, and pulled the device from shelves when it was still selling well. This decision ended up costing the company its leading position in the American console market.

### 8. Sega had its hands tied by its parent company’s refusal to partner; its eclipse was almost complete. 

In a fast-paced, creative market like the video games market, it's not unusual for different companies to work together, especially when they have compatible skills.

Unfortunately for Sega, this was not the philosophy of its parent company.

SOJ refused to cooperate with other firms because it didn't want to lose the tight control it had over the company as a whole. One of the joint ventures executives turned down was a potentially lucrative deal with Japanese consumer electronics company _Sony_.

Sony was keen to enter the video games market, but it was more interested in developing software than hardware. This was a great opportunity for Sega, especially because it had experience with CD-ROMs, through its Sega CD and Sega Saturn consoles.

However, SOJ flatly turned the offer down, even after negotiations between the two companies had reached an advanced stage. This mistake backfired in a big way.

Since Sony couldn't collaborate with Sega, it decided to develop its own console, the _PlayStation_. When the PlayStation was released, in terms of competitive advantage, it blew Sega's Saturn out of the water.

Sega's stubborn refusal to partner didn't end with Sony. The company also started talks with _Silicon Graphics (SGI)_ for a move to producing 3-D games, but then Sega got cold feet again and refused a deal.

Kalinske was so frustrated at being forced to refuse another deal that he gave SGI executives the phone number of Nintendo as an alternative!

Even internally, SOJ was reluctant to make deals with SOA. Initially, the American spin-offs of both Nintendo and Sega could only produce games with the approval of the Japanese parent company. Yet Nintendo eventually decided to give its U.S. subsidiary the freedom to develop games on its own.

With this new latitude, Nintendo's U.S. arm worked with a British company to develop _Donkey Kong Country_, the game that cost Sega its market leadership.

Kalinske came to realize that the real battle wasn't Sega versus Nintendo, but Sega America versus Sega Japan.

### 9. Sega revolutionized the gaming industry, setting new norms for tech, marketing and game play. 

When Tom Kalinske took the reins at Sega, it seemed like the company had little chance of survival. Not only did Nintendo dominate the games market, but the market itself was young and its future uncertain.

Over the next few years, however, Sega didn't just become successful — the company revolutionized the video games industry. Its aggressive marketing philosophy changed forever how games would be developed, released and consumed.

In 1990, the video game market was worth $3 billion globally, with some 90 percent of the market controlled by Nintendo in the United States alone. By 1994, the playing field had grown, with the market worth $15 billion worldwide and $6 billion in the United States, with Sega in the lead.

This rapid growth inspired social change, too. For the first time, lawmakers and politicians began to view the video games industry as something that needed regulation, like television or film. After Sega released Mortal Kombat, the U.S. Congress held hearings about violence and video games.

These hearings led to the creation of a regulatory group that would put age limits and ratings on video games that were considered too extreme or violent for minors.

Games themselves changed too. In the short period from 1990 to 1994, the technological improvements of games are stark. While early games were slow, with blocky graphics and limited in scope, games post-1994 were more colorful, more responsive and offered faster action.

So even though Sega's success was short-lived, its influence changed the industry forever. Video games as we know them today owe much of their character to the rivalry between Sega and Nintendo, and Sega's rapid rise to the top.

### 10. Final summary 

The key message in this book:

**While Sega's success was short-lived, the company's influence and innovative approach changed the industry significantly. In the early 1990s, Sega managed to topple the market leader Nintendo and established new standards in game production and marketing. Although the battle between Nintendo and Sega was brief, today's gaming environment is still very much influenced by this historic competitive rivalry.**

**Suggested further reading:** ** _Reality_** **_is_** **_Broken_** **by Jane** **McGonigal**

_Reality_ _is_ _Broken_ explains how games work, how they influence our everyday lives and what potential they have to improve our lived reality. Full of examples of different game styles and their effects on gamers' dispositions, it not only offers a broad perspective on what games are but also shows how game designers can use them to solve some of the world's most pressing problems.
---

### Blake J. Harris

Blake J. Harris is a writer and filmmaker who is also co-directing a documentary based on _Console Wars_. Harris is also the executive producer of a _Sony_ feature-film adaptation of the book.

