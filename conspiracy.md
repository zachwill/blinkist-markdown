---
id: 5ab6eb53b238e10005bf58dc
slug: conspiracy-en
published_date: 2018-03-27T00:00:00.000+00:00
author: Ryan Holiday
title: CONSPIRACY
subtitle: Peter Thiel, Hulk Hogan, Gawker, and the Anatomy of Intrigue
main_color: AF2B23
text_color: AF2B23
---

# CONSPIRACY

_Peter Thiel, Hulk Hogan, Gawker, and the Anatomy of Intrigue_

**Ryan Holiday**

_Conspiracy_ (2017) reveals the incredible true story behind the downfall of one of America's most controversial media outlets. The author explores the motivations and machinations of billionaire Peter Thiel, who conspired against Gawker Media, and details the dramatic courtroom trial that saw wrestler Hulk Hogan win millions in damages against the world's most notorious gossip website.

---
### 1. What’s in it for me? Get the inside scoop on the downfall of Gawker Media. 

If you followed the news in 2016, you probably read about ex-wrestler Hulk Hogan's lawsuit against the notorious gossip website Gawker. You might know that Hogan won and that Gawker declared bankruptcy, but what's the story behind the story? How did one of America's most respected and feared publications sleepwalk into its own financial ruin?

In these blinks, we'll take a look at how billionaire tech investor Peter Thiel shocked the world with his behind-the-scenes plot to destroy Gawker Media. We'll examine the origins of Thiel's hatred of Gawker, and learn how his covert actions ultimately shut down the controversial media outlet. We'll also hear arguments for and against Thiel's involvement in Hogan's case, and explore how Gawker found itself on the stand in the first place.

In these blinks, you'll learn

  * why Peter Thiel hated Gawker so much;

  * what a celebrity sex tape had to do with this conspiracy; and

  * how a few determined people plotted to destroy a powerful company.

### 2. In 2007, Peter Thiel believed Gawker Media had invaded his privacy. 

"The beginnings of all things are small," the Roman politician Cicero once said. So it was with the conspiracy at the heart of this story. That small beginning was a blog post published by a gossip website in 2007, which outed a tech investor named Peter Thiel as gay. Just 400 words long, this post is the genesis of a conspiracy that cost millions of dollars and lasted over nine years.

To understand why this post was so significant, we must first learn a little more about both its subject, Peter Thiel, and its publisher, Gawker Media.

Let's take a look at Peter Thiel first.

In 2007, Thiel was already a wildly successful entrepreneur. Having made his fortune as a founder of the online payments system PayPal, Thiel had also gained recognition as Facebook's first major investor. Despite coming out to his friends, family and colleagues, Thiel was still discreet about his sexuality in 2007, preferring to keep it a somewhat open secret in Silicon Valley. Indeed, when it came to any aspect of his personal life, Thiel was intensely private.

Now let's examine the website that published the blog post. This website was called Valleywag. Though it was billed as a tech-news site, Valleywag took its editorial direction from its parent company, the notorious gossip website, Gawker.

Both the Gawker and Valleywag websites were owned by an Englishman named Nick Denton.

Denton's background may have been in the tech industry, but his true interests were secrets and gossip. Specifically, exposing other people's secrets via his websites, in the name of entertainment and transparency.

Whose secrets? Particularly those of the rich, powerful or famous. Using an army of young, hungry writers with a gift for witty yet contemptuous writing, Denton encouraged his bloggers to expose and ridicule public people and institutions they felt were hypocritical or hiding something. And audiences loved it. By 2005, Gawker and Denton's other websites were making $120,000 in monthly advertising revenues. By 2012, Gawker's revenues were close to $40 million.

However, for all Nick Denton's media savvy, he gossiped about the wrong person on that day in 2007.

### 3. Gawker’s outing of Peter Thiel was just another example of their boundary-pushing journalism. 

On December 19, 2007, Peter Thiel's hatred of Gawker began. This was the day on which, under the headline, "Peter Thiel is totally gay, people," the Valleywag website outed him. But why did Thiel care so much about this blog post, and was it really the only reason for his hatred of Gawker?

To appreciate the depth of Thiel's loathing for this company, we must recall the social context of 2007, as well as examine some of the other stories Gawker published around that time.

Looking back now, it might seem anachronistic that a gay man would want to keep his sexuality a secret. However, in 2007, the world was a very different place. It would still be another five years before Obama endorsed gay marriage and it would take Hillary Clinton another six years to support it. In other words, the world was much less tolerant of gay people and their rights.

When Valleywag outed Thiel, he took it as a personal attack on his right to privacy. And Gawker Media's assaults on Thiel's private life didn't stop there. Later that year, Valleywag published more blogs about Thiel, including one naming and picturing his boyfriend. In fact, Gawker Media's articles about Thiel in 2007 and 2008 would garner a combined 500,000 views. Suddenly, the quiet and private Thiel was all over the internet, and so was his sex life.

Although Gawker's treatment of Thiel might seem harsh, this sort of behavior was completely unremarkable for it. In fact, Gawker Media was quickly building a reputation for its willingness to run stolen material and publish anonymous leaks. In 2005, it had published a stolen sex tape of Fred Durst, the Limp Bizkit frontman; Gawker writers prided themselves on saying and publishing things that more traditional media organizations wouldn't dare. What happened if one of Gawker's victims tried to fight back? Gawker would mock them even more, secure in the knowledge that it was protected by America's stringent free-speech laws.

After learning more about Gawker's shock tactics, Thiel began calling Gawker the MBTO, short for "Manhattan Based Terrorist Organization," and he quickly became convinced that something needed to be done about these fearsome, seemingly unstoppable publishers.

### 4. Attacking Gawker was difficult, but Thiel had a lot of resources to dedicate to the task. 

Between 2008 and 2011, Peter Thiel considered what to do about Gawker Media and its propensity for publicly embarrassing people. But how could he hurt them? Almost nobody had ever fought the American media and won. As an old saying goes, never battle anyone who buys their ink in barrels. In other words, don't pick a fight with journalists.

Sure, Peter Thiel was a billionaire and remarkably intelligent, but his friends were sure that Gawker Media couldn't be brought down.

So what made Gawker so untouchable?

First, America's First Amendment — that is, its free-speech laws — gave journalists almost bulletproof legal protection against anyone wishing to prosecute them for saying something unsavory.

Another problem was that Gawker Media couldn't be shamed. If Thiel had entered into a war of words with its founder, Nick Denton, and called his company out for bullying, Denton and his writers would merely have written even more inflammatory material about Thiel. In fact, they would have thrived on the drama and publicity of a feud with a billionaire.

Lastly, Peter Thiel couldn't simply buy Gawker Media and change its editorial direction. It was an independent company, answerable to no-one except Denton, and Denton was in no mood to sell his media empire.

However, Peter Thiel was not a man to give up easily. In 2011, he finally came up with a strategy to destroy his nemesis.

Along with a young business associate, known to the author only as Mr. A, Thiel hatched a plan to set up a new company, a company with only one aim. Presided over by Mr. A, this shell company would secretly recruit investigative journalists and lawyers. Their job would be to sift through thousands of Gawker articles, looking for evidence of illegal wrongdoing by Gawker.

Importantly, Mr. A and his team decided to look only for wrongdoing unrelated to freedom-of-speech laws. Why? Because cases like this were notoriously difficult to win in the American justice system.

However, if Gawker had put a foot wrong in any other legal area, Peter Thiel wanted to know about it. If their investigative team could find something that would stand up in court, he and Mr. A planned to sue Gawker into oblivion.

### 5. Hulk Hogan’s privacy case against Gawker was the opportunity Thiel had been waiting for. 

As Mr. A and his team reported back to Thiel on everything Gawker had ever published, Thiel became more convinced than ever that Gawker needed taking down. There seemed to be no lows this gossip website hadn't stooped to, and gotten away with; it'd even published leaked nude photographs of famous women. But what could Thiel and his lawyers make stick in a court of law?

As luck would have it, in 2012, the right opportunity came along.

In 2012, the professional wrestler and actor Terry Bollea, otherwise known as Hulk Hogan, found himself in Gawker's crosshairs. Some years earlier, unbeknownst to him, Hogan had been filmed having sex with his best friend's wife. This footage, which had been recorded without Hogan's knowledge or consent, had been anonymously given to Gawker in 2012, and the company had swiftly published it on its website. Shocked and mortified, Hogan quickly sent Gawker a cease-and-desist letter through his lawyers, threatening to sue if the sex tape wasn't removed, and explaining that it was filmed without his consent.

Unsurprisingly, Gawker refused, and Hogan and his lawyers prepared to sue them.

This was the opportunity Thiel and Mr. A and their lawyers had been waiting for. Hogan's claim against Gawker would not be a test of America's free-speech laws; instead, Hogan's case was that Gawker Media was guilty of not respecting the wrestler's right to privacy. Though this difference might seem subtle, privacy laws and the right to free speech were two very separate legal concepts. And, as it was easier to challenge on the basis of privacy than free speech, Hogan might just have a shot at winning.

Keen to pursue the first real opportunity to attack Gawker, Mr. A contacted Hulk Hogan and asked if he would be interested in suing Gawker with the financial backing of an unknown third party. Thiel wanted his interest in destroying Gawker to remain a secret. If Gawker discovered his devious intentions, they'd come after him with the full force of their vitriolic rumor mill. Who knew what lies they might make up about him, and what the damage to his reputation might be.

Luckily, Hogan did not care about knowing the true identity of his new benefactor. At that point, his only concern was to get the sex tape removed from Gawker's website.

Peter Thiel was suddenly much closer to getting his day in court with Gawker.

### 6. Gawker didn’t see Hogan as a real threat until it was too late. 

In 2016, Peter Thiel's conspiracy to attack Gawker came to fruition. A judge ruled Hulk Hogan could sue Gawker for a massive $100 million in damages; _Bollea v. Gawker_ would go before a jury.

At this point in the story, you might assume that Gawker Media and its CEO and founder, Nick Denton, were getting worried. Surprisingly, though, Gawker wasn't too concerned.

Right up until the court case with Hogan began, Gawker and Denton treated it as no big deal. One reason for their flippancy was that they didn't realize Hogan's legal fees were being paid by a billionaire. Instead, the media outlet assumed Hogan was footing his own bills. Heavily insured for the cost of its own legal fees, Gawker calculated that Hogan, who was only a single digit millionaire, would not be able to afford to fight a lengthy court battle.

Consequently, Denton and his lawyers believed Hogan would eventually settle the case out of court, for a far lower sum than $100 million.

Unfortunately for Gawker, they were wrong. When Gawker finally did offer to settle, just as the case was about to go to court, it offered Hogan $10 million — and no apology. With unlimited financial resources at his disposal, Hogan turned down the settlement in disgust.

Perhaps inevitably, Gawker would pay a heavy cost for its previously blasé attitude.

Once the trial of _Bollea v. Gawker_ finally went before a jury, Gawker quickly found itself on the ropes. It hadn't considered the fact that the case was being tried in Florida, which was right in Hogan's backyard, far away from the metropolitan citizens of New York City. This Florida jury was made up of local citizens who revered their local icons, among whom Hulk Hogan certainly numbered. When Gawker Media's lawyers tried to argue that all the snarky articles and leaked sex tapes were gestures of free speech, their arguments fell on deaf ears. All the jury saw was a popular local hero being dragged through the mud by sarcastic, cruel hipsters who might as well have come from another planet.

The verdict? In March 2016, the jury found in favor of Hogan, awarding him $140 million in damages. Thanks in part to Peter Thiel's scheming and perseverance, it was all over for Gawker.

### 7. Hogan’s case bankrupted Gawker and resulted in a media backlash against Peter Thiel. 

What do you do if a court orders you and your company to pay $140 million in damages? If you're an independent company like Gawker, you probably have no other choice than to do what Nick Denton did. You declare bankruptcy and shut down your business.

Interestingly, although you'd assume Peter Thiel would be thrilled at this outcome, there was a sting in the tail for him, too.

Shortly after the court verdict, Peter Thiel was unmasked as Hulk Hogan's benefactor. Although he had told almost no one about his involvement in the Gawker case while the trial was ongoing, the temptation to boast about his victory was too great, even for him, once the trial was won. He told a few friends, and, before long, the story was all over the international news.

For someone who had conspired so carefully over the past few years and planned for every eventuality, Thiel was now in for a big shock — America's media was very angry with him. Although many journalists had initially supported the jury's verdict against Gawker, when Thiel's involvement became known, they swiftly changed their minds. Suddenly, the media narrative changed to Peter Thiel as a petty, vindictive billionaire, whose war against a courageous media underdog was a threat to free speech everywhere.

Thiel was hurt and confused by the backlash against him. He believed his actions had been philanthropic. His victory had been a blow against a bullying tyrant on behalf of ordinary Americans everywhere, especially for those who couldn't afford to stand up for their rights against the media.

Was he right? Well, the author believes you'll have to make up your own mind on that one. However, you might want to consider the following before you do.

Thiel may have been secretive in his attack, but absolutely nothing he did was illegal. Considering his virtually unlimited resources, the fact that he chose to stay completely within the law should surely count in his favor.

Finally, there's one final thing the author would like you to consider. To orchestrate Gawker's downfall, Thiel demonstrated a huge amount of long-term focus and personal drive, as well as a refusal to accept the status quo. Arguably, these personal qualities were just as important to his victory as his wealth. In these troubled political times, in which the new status quo can often seem disturbing, perhaps we could all learn something from Peter Thiel's patient plotting. In other words, perhaps we need more rather than fewer conspiracies, if we want to change the world.

### 8. Final summary 

The key message in these blinks:

**Peter Thiel's conspiracy to destroy Gawker was nine years in the making, and would end up costing the billionaire somewhere around $10 million. What started with a short blog post outing Thiel morphed into a courtroom drama involving a professional wrestler, and ended in the bankruptcy of one of the world's most admired and feared online publishers. Whether Thiel's actions were right or wrong, the fact remains that his was one of the most audacious conspiracies of modern times.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Skin in the Game_** **by Nassim Nicholas Taleb**

_Skin in the Game_ (2018) explores the ways in which our interactions with others are secretly influenced by risk and symmetry. By drawing on ideas from the field of probability, and applying them to everyday scenarios, Taleb reveals unexpected and often dazzling insights about what really makes society tick.
---

### Ryan Holiday

Ryan Holiday is the best-selling author of _Trust Me, I'm Lying_. His work has appeared in publications such as _Fast Company_ and the _Columbia Journalism Review_.

