---
id: 53319aba3862370007b60000
slug: contagious-en
published_date: 2014-03-25T06:30:00.000+00:00
author: Jonah Berger
title: Contagious
subtitle: Why Things Catch on
main_color: F37931
text_color: 8C461C
---

# Contagious

_Why Things Catch on_

**Jonah Berger**

_Contagious_ (2013) examines what makes a product, idea or behavior more likely to be shared among many people. The book explores the question of whether contagious things are accidents or the results of good marketing, or whether contagiousness is an inherent feature of a product, idea or behavior. It argues that, far from being merely a matter of luck, the majority of very popular products and ideas are the result of a combination of savvy planning and execution.

---
### 1. What’s in it for me? Learn how to make your ideas or products spread like wildfire. 

You're probably well aware that certain ideas, behaviors and products can go viral. You've probably even played a small but essential role in the sharing that leads to those things becoming "contagious."

But do you know what makes some things wildly popular?

The author Jonah Berger explains that you can make your product contagious if you follow the STEPPS formula. STEPPS stands for social currency; triggers; emotions; public; practical value and stories. In the following blinks you will find out exactly what these are and how you can apply each of them to make your product go viral.

At a time when a contagious idea or product can mean the difference between success and failure, following the formula has never been more vital. 

Along the way, you'll learn

  * how an octogenarian farmer became an internet sensation;

  * how an anti-drug campaign actually increased drug use among teenagers; and

  * why we're more likely to share something if it triggers our emotions.

### 2. Contagious ideas or products can propel a company to success – and they’re often the result of smart planning. 

What is it that makes some products, behaviors and ideas shared all over the world and seen by many millions of people? Is the product just naturally shareable? Is it simply a matter of luck? Does it all come down to good marketing?

While certain products seem destined to capture the attention of the public, their popularity is often the result of intentional design and smart planning. 

Take, for example, the Barclay Prime steakhouse in Philadelphia, the popularity of which increased dramatically due to a single attention-grabbing item on the menu: a $100 cheesesteak made with Kobe beef, lobster tail and black truffles and served with champagne. 

The restaurant's owner, Howard Wein, had put a lot of time and money into creating a great restaurant atmosphere, but he created the $100 cheesesteak solely to generate buzz for Barclay Prime when it opened in 2004.

More than just an item on the menu, the $100 cheesesteak became a contagious product. The national media wrote about it, celebrities ate it and, in Philadelphia, where the cheesesteak is a popular fast-food choice, eating a $100 cheesesteak was an experience most people in the city simply couldn't pass up. And once they tried it, they had to tell people about it. 

So, even though Wein invested heavily in creating an excellent dining experience and atmosphere, it was this one _contagious_ idea that propelled Barclay Prime to success — despite the high probability of failure in the restaurant business and the competition of the numerous other steakhouses in the city.

Most of us can easily name a number of ideas and products that rapidly caught on among a large number of people. But how do companies actually create a contagious product?

### 3. Word of mouth is a bigger influence on what people share than advertising and marketing. 

It's no secret that a well-planned advertising campaign can help create a buzz about a product. But advertising alone won't guarantee that product will become contagious.

Rather, there's a tried-and-true way to make large numbers of people aware of your product: word of mouth.

In our society, word of mouth is a massively influential form of communication, and we're constantly sharing things with others — especially online. In fact, people share about 16,000 words per day online, where there are over 100 million conversations involving brands every hour.

While we might believe our online sharing is fairly insignificant, what we share actually influences the actions of others. Indeed, research reveals that word of mouth is the main reason behind up to one-quarter of all the purchases we make.

Although traditional advertising can reach millions of people, potentially attracting many of them to a product, consumers are influenced more by a friend's or coworker's recommendation via word of mouth simply because they value their opinions above the assertions of advertisements.

Another reason word of mouth is more effective than advertising is that it targets potential consumers more precisely. We recommend products and other goods to others, especially our friends, based on what we know about their tastes and preferences. You probably wouldn't tell your teetotal friend all about your new favorite beer, for instance.

Still, we shouldn't overestimate the power of social media. Although we certainly share a lot using social media, it amounts to only 7 percent of our total word-of-mouth communication.

So remember: while social media has proven a powerful medium for making certain products contagious, it's best not to rely on it as a strategy in and of itself. For instance, tweeting about your product or posting information about it on Facebook — regardless of how many people are reading it — isn't necessarily enough to make it contagious.

### 4. Sharing certain things makes us look good to others – which means we’re more likely to share them. 

Think about the last time you impressed someone. What did you talk about? Did you try to impress them with an interesting fact or by showing off some "insider" knowledge. 

If so, you wouldn't be alone. It appears that we gain a lot of pleasure from sharing, even if what we share are just our personal opinions. As one study showed, sharing our opinions activates the same area of the brain that's activated by things like food or money. 

But sharing can also cause us to feel good indirectly — by making us _look_ good.

This is because sharing is a form of _social currency_, with which we "buy" the favor and interest of others. For this reason, people are simply more likely to share something if they think it will impress their peers.

Many businesses exploit social currency to their advantage. Take, for example, New York's prestigious, exclusive bar Please Don't Tell. To enter the bar, customers have to use a secret entrance. This makes its patrons feel like insiders, which means they're more likely to share their experience to impress others. 

Other ways that companies make use of social currency include releasing unusual, or remarkable, content that will stimulate a discussion among consumers and using "game mechanics." 

For example, each bottle of Snapple iced tea contains an interesting fact underneath its bottle cap — like, "Honey is the only food that doesn't spoil." Consumers share these "remarkable" facts with others to appear more interesting themselves. Conveniently, when people do share them, they're also sharing the brand name.

People also like to share when they've won something and to display the badges and trophies they've collected. To that end, many companies offer rewards to customers for using their products or services. For example, many airlines have "frequent flyer" programs that make customers feel accomplished for flying a certain benchmark number of miles.

Whether it's through winning prizes or impressing others with a remarkable fact, we're more likely to share things that make us look good.

### 5. Some products and ideas consistently trigger other ideas, making them more influential and contagious over time. 

While the appeal of social currency is certainly a powerful influence on our sharing, it won't lead to us telling others about those things more than just a few times.

What makes us share consistently over time are _triggers_.

Triggers are a crucial factor in making a product contagious, as psychologist Daniel Kahneman has shown. He discovered that certain ideas trigger others by association, causing us to be reminded of something other than the thing we're presented with.

In fact, products or content make especially good triggers when they prompt someone to make an associative connection to the context or environment in which they are presented. For example, take the song "Friday," by Rebecca Black. "Friday" rapidly became an internet sensation because it was triggered by an experience that everyone has regularly: coming to the end of the week.

What this suggests is that for a product to become contagious, it can't be only immediately interesting or remarkable. Instead of triggering short-term, _immediate_ interest _,_ the most successful products trigger interest that's long term, or _ongoing_. And to achieve that, you have to create an effective trigger. 

For example, the renewed success of the _Kit_ _Kat_ bar was largely due to a marketing campaign which associated eating a Kit Kat with something many people do every day — taking a coffee break. Kit Kat commercials showed the candy bar next to a cup of coffee with the slogan, "a break's best friend." 

The Kit Kat and coffee campaign was successful because it established a link between the product and its existing environment; whenever you took a coffee break, the idea of eating a Kit Kat would be triggered.

So, to ensure people keep telling others about your product, you should employ a trigger, and remember the old saying, "Top of mind is tip of the tongue."

### 6. Products and ideas are more likely to be shared if they arouse strong emotions. 

As you know by now, we all like to share ideas and information, and some of them spread like wildfire. But which ideas, and which types of information, are we more likely to share? 

In short, anything that arouses our emotions. 

For example, a detailed study of the most widely shared articles on the _New_ _York_ _Times_ website showed the health and science articles coming out on top because those articles aroused awe and amazement in their readers, provoking an emotional response. 

But which emotions are more likely to prompt us to share? The same study revealed that although it appeared readers were more likely to share positive rather than negative articles, the most shared articles were, in fact, those which stimulated emotions that could spur their readers to take action. 

These _high-arousal_ articles provoked awe, excitement, amusement, anger or anxiety. By contrast, the other, _low-arousal_ articles tended to leave their readers either merely content or sad.

This reasoning not only explains why certain articles are widely shared, but also why certain videos go viral, too. On YouTube, for example, a video of a child who'd just received anesthesia from a dentist aroused humor. The fact that viewers found it funny prompted them to act — by sharing the video, millions of times.

The reason we're more likely to share _high-arousal_ articles and videos is that they excite us. In fact, as long as we're excited, almost any kind of arousal can prompt us to share. In one study, two groups of subjects were given the same article to read. The subjects of one group were instructed to run in place for a while before reading the article, while those of the other group were not. 

The result? Subjects from the first group, who were physically excited by exercising were more likely to share the article than those subjects who didn't exercise beforehand. 

No matter if it's an emotional or physical arousal, becoming excited makes us more likely to share.

### 7. We’re most influenced by products, ideas and behaviors if we observe them frequently. 

What was it about the famous "Just Say No" anti-drug campaign that actually _increased_ drug use? 

It turns out that if a behavior (or product or idea) is observable, its influence and, thus, contagiousness is increased. That's because we tend to imitate the behavior of others, so the products or activities we see used or performed regularly by other people become enticing and influential to us.

Take, for example, the men's health awareness campaign "Movember," which encourages men to grow mustaches during the month of November. As men witnessed others taking part in the campaign, their curiosity was piqued and they joined in. In this case, the effect of such contagious behavior was that it raised awareness of men's health issues: the quite sudden predominance of mustaches prompted others to ask the participants about their motivation for growing them. 

But why do we often imitate the behavior of people around us? One explanation can be found in the psychological concept of _social_ _proof_ : we do what others are doing because we assume that, since it appears to be so popular, there has to be a good reason for doing it. 

Clever marketers can take advantage of this tendency, by making their logo or brand particularly noticeable — like the bright yellow Livestrong bracelets many people can be seen wearing. 

Another smart way to take advantage of social proof is in a common practice of baristas: often, at the start of a shift, they put a few dollars in their tip jar. If customers think others have tipped, they'll be more likely to leave a tip as well. 

Likewise,the "Just Say No" campaign mentioned above showed a wide range of young people in a variety of situations being offered drugs and refusing them. But because the campaign made it seem that drugs are quite common, it happened to also represent taking them as normal. The fact that we tend to imitate the behaviors that are most visible meant that the campaign actually caused an increase in drug use.

### 8. If a product or idea is simple, practical and useful, it’s more likely to become contagious. 

As you know, the reasons for sharing are many and complex. But at least one of them is very simple: we're more likely to share those things we find _useful_.

That's because useful and practical things make our lives easier, so we consider them "shareworthy." 

For example, Ken Craig — an 86-year-old ex-farmer from Oklahoma — became an internet sensation when he produced a video that demonstrated that microwaving ears of corn made it possible to remove the husk without leaving behind any of the corn silk. This video attracted more than five million viewers.

Unsurprisingly, some of the most useful, practical and, therefore, contagious ideas are techniques for saving money. But did you know that how a particular discount is presented is more important than the actual value of the discount? 

In fact, businesses have established an easy-to-remember principle for offering discounts to customers called the _Rule_ _of_ _100_ : If a product is priced below $100, a percentage discount — such as "10% off" — will appear like a more attractive deal than "$10 off." Conversely, if a product costs over $100, a numerical discount will be more appealing than a percentage discount. 

Another useful principle for making products contagious is to always consider their potential audience. While you might think that targeting as broad an audience as possible will increase the chances of something being shared, it turns out that products that are narrowly focused on a smaller, more specific group of people are actually shared more widely.

A company that targets niche markets in this way, by promoting a certain product — like SCUBA equipment — to that niche, is more likely to see their promotion shared widely than a company that aims to reach a very broad interest group. That's because the small target audience feels special and also happy that their niche is represented, which makes them more likely to share information about a product with other people.

### 9. Narratives and stories are inherently shareable, and they carry powerful information within. 

All of us know the tale of the Trojan Horse; but have you even wondered why that is? In other words, what is it about certain stories that makes them endure? 

The reason is that we think in terms of narratives or stories, which can contain important information, rather than in terms of the bits of information or data themselves.

Humans have been thinking in this way for a while. For millennia before the written word, narratives were the primary carrier of ideas, as stories were passed from one generation to the next.

For example, the story of the Trojan Horse has been shared over time not just because it's an entertaining tale, but because it also teaches our society valuable lessons: never trust your enemies and don't celebrate victories prematurely.

But why are narratives so incredibly effective and influential?

Much like the Trojan Horse, entertaining but factual stories can smuggle ideas and information into our minds without us being aware. This means that when they're recounted by word of mouth (say, at the water cooler), people don't often stop to question the story's validity — especially when the narrative is about a specific event which a particular person experienced. Validity is simply less important to us than our enjoyment of the story itself.

Another reason we don't tend to disbelieve or question stories is because they capture our interest and we get caught up in the unfolding narrative. Take, for example, the story of Jared Fogle's "Subway diet." Fogle lost 245 pounds by eating only sandwiches from the American fast food company Subway. The sheer unlikeliness of losing so much weight by eating fast food meant that the story of his diet evolved into a contagious advertising campaign for the company.

Its message? Even if you eat only Subway sandwiches every day, they will constitute a healthy and satisfying diet.

Throughout human history, narratives have been crucial to sharing information among a massive number of people in an effective, memorable and engaging way.

### 10. Final Summary 

The key message in this book:

**A contagious idea, behavior or product is something a great number of people are impelled share with others. To create a contagious product, simply follow the STEPPS formula: make sharing information about your product a form of _social currency_ for people, use _triggers_ and arouse _emotions_ to get them to share consistently and frequently, make your product _publicly visible_ and _practically valuable_, and finally, build a compelling _story_ around it.**

Actionable advice:

**Try and insert a bit of competition into your marketing.**

People love to play games and, when they win, it gives them a reason to boast to others, conveniently sharing a brand name at the same time. So, try "gamifying" your product to encourage consumers to engage with your product and increase the probability of them telling others about it.

**Don't be afraid of making your audience sit up and take notice.**

The things that go viral tend to be those which arouse our emotions, thereby exciting us — e.g., anger, awe, humor. Try to stay away from so-called _low-arousal_ emotions, like sadness or mere contentment, as these don't seem to prompt people to tell others about them.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Hooked_** **by Nir Eyal**

_Hooked_ (2014) explains, through anecdotes and scientific studies, how and why we integrate certain products into our daily habits, and why creating such products is the Holy Grail for any consumer-related company. Hooked gives tangible advice on how companies can make their products habit-forming, while simultaneously exploring the moral issues related to such products.
---

### Jonah Berger

Jonah Berger's work specializes in social influence and virality — or why certain products and ideas catch on. He lives in Philadelphia, Pennsylvania, where he is Associate Professor of Marketing at the University of Pennsylvania's Wharton School.

