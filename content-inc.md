---
id: 57f8ad5cdbad80000386b30d
slug: content-inc-en
published_date: 2016-10-10T00:00:00.000+00:00
author: Joe Pulizzi
title: Content Inc.
subtitle: How Entrepreneurs Use Content to Build Massive Audiences and Create Radically Successful Businesses
main_color: F7A031
text_color: 915E1D
---

# Content Inc.

_How Entrepreneurs Use Content to Build Massive Audiences and Create Radically Successful Businesses_

**Joe Pulizzi**

_Content, Inc._ (2015) is a useful and practical guide on how to use content management to market your business, attract an audience and bring your ideas to the world. These blinks are packed with tips and tricks that will help you turn that audience into loyal subscribers and allow you to diversify your brand and move to the forefront of your industry.

---
### 1. What’s in it for me? Become an expert content creator. 

Every entrepreneur is familiar with the pressure to come up with the next big thing — you know, that great product that'll be an instant success. But what if, at the beginning, you didn't need a unique product? Wouldn't that be a relief?

Well, wipe the sweat from your brow, because, initially, there is indeed no need for uniqueness. You can create an audience and make them buy your product later.

Content creation doesn't require you to push a product forward. Instead, you must tune in to the deepest wants of consumers. This will help you identify the sweet spot where you can monetize your content.

In these blinks, you'll learn

  * how Andy Schneider turned his backyard poultry interest into a great entrepreneurial success;

  * how Matthew Patrick combined his acting skills and a passion for mathematics and gaming to create great content on YouTube; and

  * how to generate revenue from your content right from the very beginning.

### 2. Content marketing can attract an audience much more effectively than traditional advertising. 

If you have a product you want to share with the world, then you have to attract buyers. Now, the traditional approach to marketing mainly focuses on advertising — but, these days, people are all but buried under an avalanche of ads. This makes it hard for advertisers to stand out.

So, if advertising isn't as effective as it used to be, what's the best way to get your product noticed?

Today's customers are drawn to creative content, not advertising.

The Content Council is a global non-profit organization focused on content marketing. Their studies show that 61 percent of people are more likely to buy a company's products when that company provides content that is uniquely tailored to a specific audience.

These statistics are supported by the marketing hub, Content+, whose research indicates that 70 percent of consumers prefer becoming familiar with a company through articles and content rather than advertisements.

While this shift from advertising to creative content is a significant change from traditional marketing practices, it's actually great news for small business owners and start-ups.

Back in the day, if a company wanted to reach millions of people, they'd have to run a costly advertising campaign. But today, even the smallest companies have access to the technology and platforms needed to create and use the kind of content that can build a large and loyal customer base.

These methods also afford you the opportunity to get a competitive advantage by better understanding your customers' needs.

Unlike traditional advertising, content offers important and immediate insight into what your potential customers connect with. Pay attention to how potential customers engage and respond to your content by, for instance, reading the comments they leave and tracking how they share it.

You can even do this during the production phase of your product. By creating content and keeping an eye on what your potential customers are most excited about, you can focus on those aspects. This will help ensure its success when it goes to market.

> _"When all your focus is on an audience you know deeply, instead of a product, good things usually happen."_

### 3. Once you discover your sweet spot, you can find your target audience. 

Before you can put content to use, you need to identify what you want to write about.

So, the first step on the path to success is locating your _sweet spot_ : the combination of something you are especially skilled at, or knowledgeable about, and something you are passionate about.

Passion is important. If you're starting a business, you should be prepared to stick with it for the long haul — a task that will be easier if you're truly passionate about it.

To figure out your sweet spot, try making two lists. First, list all the things you are knowledgeable about, and then everything you're skilled at. Finally, consider what you are passionate about and what gets you motivated; it could be anything, from cooking tasty meals to helping others.

Entrepreneur Andy Schneider's passion is teaching and his unique skill is raising chickens in his backyard. Before long, Schneider realized he wasn't alone; a lot of other people were interested in raising their own chickens and regularly asked him for information.

Schneider saw an opportunity to put his passion for teaching into action by holding regular meetings about backyard poultry in Atlanta. Five years later, Schneider had a radio show, a magazine and a book. Today, he travels around the United States, leading workshops and helping others.

Schneider found his sweet spot by combining his knowledge of chickens and his passion for teaching. But he also found the right audience.

Finding your audience is a crucial part of getting a business off the ground. You need to know whom you're trying to connect with if you're going to create relevant content.

When you're starting out, it's best to try to focus on just one audience and worry about the rest later.

Andy Schneider could've tried connecting with students or amateur farmers, but he wisely focused on a perfect audience: homeowners with an interest in raising their own chickens. His success even earned him an apt nickname — "the chicken whisperer."

### 4. With unique and focused content, you can stand out and position yourself as an expert. 

Once you've discovered your own sweet spot, which is also your business niche, there's just one more step before you start producing content.

First, you have to create your own _content tilt_. This will help make your content different from all the other content that's already out there and, hopefully, make it unique, memorable and valuable.

There are several ways to go about this. Let's say your sweet spot has led you to the business of home-office decoration. To pinpoint your content tilt, you could peruse social media platforms to see what your target audience is talking about and then figure out what new and unique perspective you might be able to add.

You could also be more direct and come up with an online survey to find out what your potential customers are looking for or even ask them face-to-face.

Once you've found your content tilt and put your business or service out into the world, you should be prepared to put your expertise to work.

Your unique content should attract people who are interested in what you have to offer. And your knowledge and skill should make you a go-to person for information, thus growing your audience and helping you achieve expert status.

But, for this to happen, you need to do three things: focus on your niche, highlight the special knowledge and skills that you can offer and let your target audience know what they can expect.

The last item is something you can write down and share with your audience in the form of a mission statement.

For example, Darren Rowse, the founder of the Digital Photography School, has a clear mission statement on his website, where he also offers amateur photographers some simple tips to get the most out of their cameras.

Rowse's content attracted visitors and had them coming back for more. Eventually, he became an in-demand resource for his niche audience of digital-photography enthusiasts.

> _"If you can help people live better lives or get better jobs, you'll grab them emotionally and keep them as subscribers for life."_

### 5. Use the right platform to reach your audience, and hold on to them by using a content calendar. 

The right kind of content should attract customers like bees to pollen, but for your target audience to find that content, you need the right platform.

Your choice of platform should be a channel that is perfectly suited for your target audience, one that makes it easy for them to connect with your content.

So, what _is_ the right channel for your audience? A blog, a podcast? Or maybe a YouTube channel, a WordPress website or a printed magazine? These are the questions you need to ask yourself.

Matthew Patrick found the right platform for his audience in YouTube and has since become a famous internet personality with a series called _Game Theory_. Before that, he was a struggling out-of-work actor with a background in neuroscience.

Patrick had a sweet spot — science, acting and a passion for gaming. So, he decided to combine these things and make a video series on YouTube to prove to the entertainment industry that he could find and hold on an audience. And it worked: he quickly had a regular and loyal following of young gaming fanatics.

Once you have the right platforms in place, you'll need a _content calendar_ to keep your channel active.

It can be overwhelming to keep your audience hooked, so a content calendar will help you stay organized and on track to deliver creative content over a long-term period.

You can also keep track of your goals on the calendar by writing down the desired outcome for every piece of content and giving yourself a date to check back in and see if you've hit the mark.

For example, you might want to see if your content got the right amount of positive feedback, or if it increased sales over a certain period of time. By keeping track of such things, you'll be able to learn and adjust.

### 6. Email, search engine optimization and influencers are all important tools for growing your audience. 

Once you've found the best way to reach your target audience, the next goal should be to turn that audience into loyal subscribers.

There are many ways for you to contact your target audience, including through social-media sites like Facebook and Twitter, but the best method is via email.

Email subscribers are more important than social media followers; in fact, if you were to create a list of the most important people in your audience, they would be at the very top.

For this top-tier audience, you're not just another person in their feed or business competing for their attention. With email you get total control of the content they receive and the ability to reach out to them whenever you want.

For example, you could send them a daily newsletter that highlights your original content or a weekly newsletter that also includes information about exciting trends in your industry.

Another way to build a loyal audience is to make sure your content can be found by someone who's looking for it.

Among the different ways to do this is to practice good _search engine optimization_ (SEO).

Using good SEO techniques will increase the chances of your content getting picked up by search engines and thereby attracting more people. Without it, a search engine won't know to include your content about home-office decoration when someone does a search on the topic.

Essentially, good SEO means remembering to tag your content with helpful keywords, like "home-office decoration" and "useful tips," and formatting your content to highlight what it's about. You can also include relevant links to other websites to help raise your search engine profile.

A final way to increase your audience is to work alongside _influencers_.

Bloggers or media sites that already have an audience that naturally overlaps with the group you are trying to attract are perfect influencers. They can help you get more email subscribers by posting content that mentions your website or includes links to your content.

### 7. Improve your business through diversification, adding channels and buying content assets. 

Another key to making the most of your content is diversification.

The goal is to position yourself as an industry leader, who offers his audience more than just one blog or YouTube channel.

So, the first option for diversifying is to simply add different channels.

But remember, you should only expand to another channel once your first channel has secured a loyal audience.

Many successful entrepreneurs offer content digitally, in print and in person by running a blog, writing books and offering public speaking engagements. So, covering two or all three of these channels could serve as a good goal to reach.

If you want to keep things purely digital, another successful way to diversify is to simply add more channels to your current platform.

For example, creating a podcast is currently a very trendy way to diversify your content stream.

And, to return to the Digital Photography School, Rowse diversified his website by adding a page called _Snappin Deals,_ which highlights sales on photography equipment. Simple moves like this can really help you come across as an industry leader to your audience.

A third way to diversify is to buy a _content asset_.

These are content providers who already have a platform as well as an audience in the area you're looking to expand to. If you want to expand quickly, it makes sense to buy an existing asset and absorb their existing audience, rather than building a new one from the ground up.

This is what the photography supply store Adorama did. They purchased the magazine _JPG_ when it was about to go out of business; since the magazine had 300,000 subscribers, this diversification deal was a tremendous boost to their audience.

### 8. There are many opportunities for generating income with your content right from the start. 

At this point you might be thinking, this is all well and good, but how do I turn these helpful tips into money?

While any good business needs to patiently research and find out exactly what their audience wants and needs, there are ways to bring in money from the very start.

Once you're up and running you can experiment with different revenue streams to figure out what works.

_Game Theory_ 's Matthew Patrick, for example, was able to immediately generate an income by using ads on his YouTube channel.

Over at the Digital Photography School, Rowse used an affiliate-marketing approach to generate income. This method directs Rowse's audience to the websites of partners who then pay the Digital Photography School for the amount of traffic that is generated through their links.

As you get going, you'll find ever more ways to make money from your content.

While most people have one or maybe two sources of income, a successful entrepreneurs has a veritable nexus of income streams. And this is the model you should aim for as well.

So, always be on the lookout for additional ways to monetize your content, whether it's through selling content to advertisers or news outlets, or generating income through magazine or podcast subscription fees.

You could also look to crowdfunding sites like Kickstarter or cross-media offers like hosting a webinar or taking part in someone else's podcast.

The most important thing to remember is to keep your eyes open, as new and unexpected ways to make money are constantly popping up.

And don't forget to listen to your audience. Opportunities can come from customer feedback or from someone who heard you speak at an event.

As you can see, there are plenty of ways to experiment with earning revenue right from the start. And as your audience grows and you continue to diversify and expand, those opportunities will only become more plentiful and lucrative.

### 9. Final summary 

The key message in this book:

**The potential payoff from content management can be huge. It takes time to build a loyal audience, but with the right tools, a good strategy and the right kind of content, you can pull it off. And once you do, you'll have the asset that all businesses need: a loyal audience.**

Actionable advice

**Use the Google Trends tool to help you find your content niche.**

Google Trends allows you to see what people are searching for, where your product ranks and which related searches are popular, giving you insight into what to focus on.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Epic Content Marketing_** **by Joe Pulizzi**

_Epic Content Marketing_ (2014) offers you a step-by-step guide to mastering one of today's most innovative approaches to product marketing. Knowing your audience and assembling a top-notch content team are just some of the key elements to achieving a successful content marketing strategy and getting ahead in a highly competitive market.
---

### Joe Pulizzi

Joe Pulizzi is a leader in the content-marketing movement and the founder of the Content Marketing Institute. His other books include _Epic Content Marketing_, which _Fortune_ magazine named one the "Five Must Read Business Books of the Year." In 2014, Pulizzi received the John Caldwell Lifetime Achievement Award from the Content Council.

