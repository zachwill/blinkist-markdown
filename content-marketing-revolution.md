---
id: 56a5d7c4ce4ca9000700001f
slug: content-marketing-revolution-en
published_date: 2016-01-25T00:00:00.000+00:00
author: Dane Brookes
title: Content Marketing Revolution
subtitle: Seize Control of Your Market in Five Steps
main_color: B4242A
text_color: B4242A
---

# Content Marketing Revolution

_Seize Control of Your Market in Five Steps_

**Dane Brookes**

_Content Marketing Revolution_ (2015) is your introduction to pioneering new strategies in digital marketing today. These blinks guide you through the process of developing and implementing a content marketing strategy, helping you optimize your advertising efforts.

---
### 1. What’s in it for me? Revolutionize your marketing with top-notch content. 

Content marketing is much more than just a few randomly chosen words describing a product that you want to sell, or heavy-handed encouragement for your customers to buy it. Simply telling customers to make a purchase is a strategy that is sure to fail. 

What you need is a strategy — a content marketing strategy — to sell your product. In other words, you need well-crafted, juicy content that'll make your customers, both existing and potential ones, crave your products.

These blinks will show you how, giving you valuable insights into the universe of content marketing, including how to start, where to put it, where to focus and who to address. 

In these blinks, you'll learn

  * about the four stages that customers go through, and how to address each one;

  * what it means to have SMART marketing objectives; and

  * why spending money doesn't guarantee success.

### 2. Investigate your target group and your company to find out what content will boost your brand. 

What do Lego, Red Bull and the Four Seasons hotel chain all have in common? Well, they're all really good at one thing: _content marketing_. These three companies enjoy powerful brand loyalty and extensive customer engagement as a result of their pioneering content marketing strategies.

Content marketing is all about using interesting, relevant and remarkable _content_ to create a deeper relationship between a brand and its customers. Being a nuanced and innovative approach to marketing, content marketing requires a little bit of preparation; in other words, you've got to do your homework before putting your content out into the world!

The first thing you'll need to investigate is what your _target group_ of customers is talking about. You can start by researching trending topics among your brand's current and prospective customers, and learning about the experts, commentators and competitors in your industry. Hootsuite is one of many social media tools that makes this research a little easier, while Google Trends enables you to follow rapidly shifting trends in your target group with excellent precision. 

After your research, it's time to look inward and investigate the content already available within your company. You may for example find interesting reports or analyses that have only been published internally so far. Create an inventory to collect and keep track of the content that you find. You could start by creating an Excel spreadsheet to record content, making note of its format, whether it's published or not and the person responsible for it. 

Take this opportunity to get rid of poorly written, irrelevant or outdated content that isn't fit to represent your company. If you're unsure whether something deserves to be canned, ask yourself: is this piece of content how you want your company to be seen by a potential customer? If not, let it go.

### 3. Define the objectives and the timing of your published content. 

Say you're a wine manufacturer. You've got a nice collection of photos of your winery, as well as a few images for the labels and rough ideas for blog posts that you've been thinking about. Some trending topics you've identified are ecological production and the threats the grape wine louse poses to wineries.

You're off to a good start! But there are still a few things to figure out. Where do you want your marketing to take you? And when should you start publishing your content?

At the beginning of your content marketing journey, you should identify your _big aims_. These are the overarching goals that serve as your light at the end of the marketing tunnel. If, for example, you aspire to become a Europe-wide wine producer renowned for your expertise, this should be the focus of all your marketing activities.

Now that you've defined your big aim, you can start making it a real possibility by breaking it down into smaller goals. These could include increasing brand awareness or lifting your brand's reputation. These goals can in turn be divided into objectives.

Objectives should follow the _SMART_ principle: _specific_ (focused and actionable) _, measurable_ (ensuring that your content's success can be evaluated using metrics such as views, shares or likes) _agreed_ (everyone on your team is on board) _, realistic_ (achievable within your budget and timeframe) and _timed_ (published in accordance with your content calendar).

Publishing a newsletter each week is a _smart_ objective that helps you achieve your goal of raising brand awareness. But let's take another look at the _timed_ aspect of smart objectives — and what is a content calendar, exactly?

A content calendar outlines when something should be written, edited, released and then measured, allowing your company to organize the release of your content effectively. You could keep things simple and use an Excel spreadsheet for your calendar, or step it up and use an online tool like Gather Content.

### 4. Get inside the customer’s head with buyer personas. 

While content marketing may be a new tool, hundreds of companies are already making use of it. You can give your company an added edge by using _buyer personas_. 

Buyer personas are fictional characters that allow you to understand what makes your customers tick. The more you can flesh out your buyer personas, the more targeted and effective your content can be. 

Start by determining the basic demographic information of your buyer persona. Consider their gender, age range and location to create a profile that will help you examine their behavior and motivation.

But before you take your market research to the next level, you'll need to determine the direction of your research. 

You can do this by outlining the _key areas of insight_ for your buyer persona profile. One of these might be _success factors_, which are the benefits that your product offers to the customer. Another could be _decision criteria_, that is, which product features make or break a buying decision. 

Say a wine manufacturer already knows that her best-selling product is a sparkling wine in a pink bottle, which is priced at around ten dollars. Its success factor is that it makes for a great gift, while the decision criteria are both the charming presentation and affordable price. 

With your key areas of insight identified, it's time to get in touch with the buyers themselves, be it through interviews or in-store surveys. This information should allow you to finally bring your buyer personas to life, giving them a name, age and life story, as well as motivations and traits. 

Take Rosa. She is 45 years old and a mother of two. She's a housewife, looking for a present for her good friend Violet. Rosa loves gardening, exercise and adores the color pink — she is the buyer persona for your pink-bottled sparkling wine. 

Of course, not all your customers are Rosa clones. Your customer base is made up of several different types — groups of customers that share similar characteristics and behaviors, such as price-conscious occasional buyers or loyal, returning customers. With the help of Rosa and others like her, targeting your content marketing will be a lot easier.

> _"We will religiously ask our personas what they think about content ideas and publishing tactics and allow ourselves to be inspired by their needs and motivations."_

### 5. Tailor your content to the four stages of the buying process. 

As you might have guessed, the buying process involves much more than a customer simply wandering into a store and choosing a product at random. Rather, the buying process comprises four stages — so you have four opportunities to influence your customers with content marketing. 

The first stage of buying is _awareness and discovery_, which is the period when a potential customer realizes that he needs something. Content marketing during this stage is naturally centered on boosting brand awareness, so that your products are more likely to be discovered. 

A wine manufacturer might publish informative blog posts about matching wines to different meals. In turn, a customer might realize that he needs a different sort of wine for an upcoming dinner, so he investigates which products the brand offers. At this point, the customer enters the second stage of the buying process: _consideration_. 

During this stage, the potential customer has identified his need and different ways to satisfy it. Content marketing at this stage should be designed to assist your customer in narrowing his choices down — and showing why your brand is the best choice. 

For example, customer testimonials and reviews about your delivery performance, brand quality and customer service are all excellent options in this second stage. 

Next, the customer reaches the third stage of _purchasing_, and is now busy considering technical details like price, transportation, return policy, payment methods and so on. Content such as pricing guides and explanations of potential discounts are especially useful at this stage. 

The fourth and final stage of the buying process is _advocation and reconversion_. This involves turning your customer into a willing multiplier for your company and product. Word-of-mouth marketing is one of the oldest and most effective forms of marketing, so make room for it in your content marketing strategy by encouraging your customers to share their experiences on social media. In exchange, you can invite your customers to take part in a prize draw. 

By following this four-stage journey, you can ensure that your content marketing is as effective as possible; you'll simplify your customers' purchases while increasing sales for your company.

### 6. Maintain consistent style and quality to ensure your content stands out. 

There's no use putting time and money into excellent, targeted content marketing if you don't give it the presentation it deserves; even so, your content doesn't necessarily need to be overly elaborate. Above all, you want your content to appear credible and professional. 

Ensure your content is consistent and cohesive both online and offline by developing a _style guide_. This should cover basic rules such as logos, typefaces and color schemes, as well as specific guidelines on wording and tone. 

Also, consider how you want to address your audience. Whether you're going for a formal, polished voice or a casual and funny tone, make a clear decision and stick to it!

Alongside a consistent style, your content marketing also deserves a consistent level of quality. Nobody wants their brand to be known for its pixelated pictures and spelling mistakes. Invest time and money in thorough proofreading and reviewing to catch any mistakes before you publish. 

Though these tips may seem rather obvious, you'd be surprised at how many companies have spelling errors on their official websites. You can do better!

> _"Don't allow things like the pressures of a publishing schedule or a rapidly emerging opportunity to cloud your judgement. Quality concerns should trump all other commitments!"_

### 7. Find your niche, your sweet spots and the right budget to produce even better content. 

Armed with your style guide and standards for quality, you're almost ready to get your content marketing out into the world. You might be feeling a little worried — what if your content doesn't get many views? And what if you don't have enough money to produce top-notch content?

Don't fret! First of all, there's no need for you to try and reach a worldwide audience with your content. Instead, you should be targeting potential customers directly through your _niches_ and _sweet spots_. 

You can find your niches by asking one simple question: what is my competitor _not_ doing? These gaps left by rivals on the market are the perfect points of attack — your _niches_ — to which you can tailor your content. For instance, if no other wine producer offers tips about wine storage, you would be sure to address that in your next blog post. 

Of course, you should only address those niches that align with your content marketing strategy. Ideally, you should be producing content about your _sweet spots_ — that is, the points where your professional expertise and the needs of your customers overlap. It wouldn't make sense to write about whiskey or gin if your specialty is wine.

A targeted approach rather than universal appeal is what will make your content effective; likewise, generous funding won't necessarily guarantee success. If you are lucky enough to have a big budget, it's easy enough to employ a web and graphic designer as well as a copywriter on long-term contracts. 

But producing your content with a project-based approach by hiring freelancers is also quite common, effective and not so expensive. You could even look for hidden writing talent within your company, so check the employment background of colleagues for potential content masterminds!

No matter who produces your content, be sure to stay honest about it. If your content is produced by a professional film team, let your customers know. If it's amateur or homemade, don't be afraid to admit it.

> _"Your success in content marketing will not depend on the amount of money spent; in fact, it will hinge on the level of conviction invested."_

### 8. Final summary 

The key message in this book:

**You don't need a big budget or a global audience to produce quality content marketing. Tailor your content to the customers you want, maintain consistently high quality and seek out your niche to make your brand a trusted resource for customers — and to give your company a competitive edge.**

Actionable advice:

**Remember the importance of measuring your content's performance.**

If you want to know whether your content strategy is working or not, you have to measure it. Keep the measuring manageable and in line with the big goals you're pursuing. Online activities in particular provide huge amounts of data, so cherry-pick what you want to analyze and dig in. For example, if you want to know how much time a user spent on your website to read your blog post, check out Google Analytics. If you want to know how your brand is perceived in general, check out Radian6, Social Mention and Netvibes.

**Suggested further reading:** ** _Epic Content Marketing_** **by Joe Pulizzi**

_Epic Content Marketing_ (2014) offers you a step-by-step guide to mastering one of today's most innovative approaches to product marketing. Knowing your audience and assembling a top-notch content team are just some of the key elements to achieving a successful content marketing strategy and getting ahead in a highly competitive market.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dane Brookes

Dane Brookes is a content marketing and communication expert, with a background in business journalism and content strategy. Brookes was head of the editorial department of a major publishing house and has managed digital marketing for several other companies.

