---
id: 55b7ec3f3235390007c80000
slug: content-rules-en
published_date: 2015-07-31T00:00:00.000+00:00
author: Ann Handley & C.C. Chapman
title: Content Rules
subtitle: How to Create Killer Blogs, Podcasts, Videos, Ebooks, Webinars (and more) that Engage Customers and Ignite Your Business
main_color: 2E9BD4
text_color: 22729C
---

# Content Rules

_How to Create Killer Blogs, Podcasts, Videos, Ebooks, Webinars (and more) that Engage Customers and Ignite Your Business_

**Ann Handley & C.C. Chapman**

_Content Rules_ (2012) is a guide to content publishing that'll help you implement effective and sustainable strategies. Regardless of whether you're a social-media novice or a web-savvy pro, these blinks will guide you through the ins and outs of web-based content tools and social media sites, while offering plenty of helpful content tips along the way.

---
### 1. What’s in it for me? Discover why content is the new black. 

What's so great about content? All marketing in all times has had some form of content, but it seems like it's acquired a new meaning in the last couple years. So what is this mysterious new content and why does it get so much attention?

Following the revolution of social media, blogs, podcast and so on, content has become one of the most important ways for businesses to connect with their customers. Things like number of followers, the amount your content is shared, as well as re-tweets, have become just as important as sales figures, brand awareness and the bottom line.

So how do you do it? _Content Rules_ provides a concrete manual for how to work with online content — and, as you'll see, there are quite a few lessons to learn.

In these blinks, you'll discover

  * how golfer Charlie King used content to get through the recession in 2009;

  * how a Kodak executive used content for customer interaction; and

  * why having a calendar is the best way to get great results from content.

### 2. Produce online content that makes your company an information source and deepens your customer relations. 

Are Facebook, Twitter or Instagram a daily part of your life? Well, you're not alone. Since the past decade's expansion of social media, consumers have become increasingly enamored of it, even using the internet to research products and guide purchasing decisions. But consumers don't turn to social media sites alone; they also get product information from company websites, blogs and product reviews posted by other customers. So how can _your_ business attract the online attention it needs?

It all begins with content.

In a nutshell, publishing content means creating and uploading of any kind, be it a blog post, a video, an ebook or a webinar. But to be successful, your content needs to appeal to your target customers. Obnoxious marketing calls and mail advertising are yesterday's news; instead of going to your customers, the new strategy is to pull them to you with content they like.

How?

The most appealing and engaging content uses true stories about real people in normal-life situations. For instance, Procter and Gamble, the company behind the diaper brand Pampers, published a video series on their website called "Welcome to Parenthood." The videos showed real new parents describing their daily lives — the potty training, the changing of diapers, the preparations for nap time. Instead of just selling their diapers, the company made a point of sharing experiences that new parents are actually invested in.

So how do you show customers you care?

Keep in mind that content is an extension of your brand. It creates the image customers have of your company; it builds credibility, helps your customers to trust you. That's because content has the ability to foster one-on-one conversations between your company and your customers, even if your customer base is huge. Since your whole base sees your content, you're building one-on-one trust with all of them at once. For example, the Chief Marketing Officer of Kodak, a $7.6 billion company, once reached out to a single potential customer by using Twitter to offer her product information.

### 3. Find your brand’s voice, and then identify your audience and its needs. 

Before you go on a Facebook posting spree, you'll need to consider your brand's unique voice and how your business can help customers live better lives — because by creating an individual company identity your business can speak to customers as if they were a single, real person.

How?

First off, write in a friendly, conversational tone, using simple language to which your customers can easily relate. What you _shouldn't_ do is use generic corporate jargon, because words like "synergy," "revolutionary" and "proactive drive" will push customers away. Consider Charlie King, a PGA golfer and instructor, who started a blog called the New Rules of Golf, in 2008. On his blog, he shared free tips and explanations through both videos and written posts. His tone was friendly and welcoming, communicating that golf didn't need to be intimidating or unapproachable.

But that doesn't mean you should be afraid to make your posts fun or to give them personality. In fact, humor and entertainment appeal to everyone. Just take the networking equipment company Cisco Systems; the testimonial videos for their routers featured the Easter Bunny and Santa Claus singing songs of praise.

Once you've nailed down your company's identity, it's time to identify your specific audience so that you can create content they'll relate to. You can start by asking yourself questions about potential audience groups: Are they customers or prospects? How do we factor into their experience of the product? How old are they? What about their professions? Pretty soon you'll see a picture of your audience forming.

Now it's time to decide what they want:

Data-collection tools can be useful for tackling this task. For instance, services like QuantCast.com can provide you with anonymous data traffic and demographics for millions of websites. You can also use services like Google's AdWords and Wordtracker to identify common keywords related to your business that people use in search queries.

### 4. Connect to customers by gathering and sharing information through a variety of social media. 

So now you've got a brand identity. You've identified your customer and you know how to improve their lives. It's time to start publishing.

It's easiest to start small by publishing and sharing easily made content, while simultaneously eliciting responses from your audience. It's easy to start a blog with a program such as Wordpress, TypePad or Squarespace, and equally easy to create a Facebook, Twitter or Yelp account, and once you've established these social platforms, you can use them to do a whole lot.

For instance, you could ask your followers their opinion on a topic related to your business and use their responses to create a blog post. Or you could shoot a video presentation of a company report, publish it and promote it using your blog as well as your social media accounts. The golfer Charlie King is again a good example: he started small, but expanded over time, offering different ways to connect. Eventually he had a Facebook page, Twitter account, LinkedIn group and e-mail newsletter. He also produced further content that he tailored specifically to certain groups. As a result, his business excelled through the recession of 2009!

But constantly generating content can be hard, so to connect with more people you should also post other people's content. This technique is called _content curation_, and it's one way to extend your own content. For instance, you can use services like Eqentia, Lingospot and Loud3r to gather smart, real-time content from across the web simply by searching for keywords.

Another content strategy is to ask customers to tell true stories about how your product affects their lives. It's called _user-generated content_, or UGC, and it encourages customers to get involved. For example, the Ford Motor Company asks their customers to post their stories on the Ford Social website. As a result, one customer wrote an article explaining how moved she was by Ford's special Mustang edition, a car that raises money and awareness for breast cancer.

### 5. Share your content every chance you get and make it easy for others to share it, too. 

Ok, so you've produced some compelling content that engages your audience. Now it's time to get people to share it — because sharing content not only promotes your business but also keeps your audience engaged and interested. So how can you get shares?

The first step is to prominently feature your social-media share buttons on websites and blogs. Doing this makes it easy for anyone who enjoyed your post to share it on their own account. You can make sharing easy with sites like ShareThis.com, which lets you place a single share button that links to multiple social networks. But before you share your content, make sure to license it and set rules for how people can use it. Consider whether you mind others changing your content? What about using it for commercial purposes?

Once you're ready to share, start brainstorming shareable formats that are easily embedded in other sites. For instance, you can give your audience the option to download articles or presentations in PDF and PowerPoint formats.

But some platforms are better for sharing than others. For example, Twitter let's users share in real-time, encouraging others to regularly check your posts and share them right away. Twitter's micro posts (or tweets) allow you a maximum of 140 characters, making it easy to update your followers at any time.

So what makes a good tweet?

A compelling tweet is essentially a headline that grabs attention; it's both punchy and clever, not to mention funny. A great strategy for tweeting is to use superlatives, odd analogies and powerful statements — things like, "Is there an "Activia" for the mind? My brain is suffering from occasional irregularity."

Once you've got a Twitter account, users can subscribe to your feed to receive your latest tweets, and they can spread your messages to a wider audience by re-tweeting _._ Once you get the hang of writing tweets, you can enhance them by including video, photos and links to your other content!

### 6. Set a calendar to systematically produce, monitor and repackage your content. 

You know how personal calendars are essential to remembering meetings, birthdays and other important events, while also serving the crucial function of preventing overbooking? Well, calendars are equally crucial for content publishing. Because if you want to succeed, you need to keep track of all the media you publish.

To coordinate and manage your different media platforms, you'll need a systematized schedule called an _editorial calendar_. Try assigning a _Chief Content Officer,_ or CCO, to manage your editorial calendar and keep track of your content agenda and strategy. Your CCO should set daily, weekly, monthly and quarterly content schedules that detail what you want to publish and review.

For instance, your daily schedule might read like this: Update Twitter and Facebook with breaking news and respond to comments on the blog. Your weekly schedule could be: Make a short blog post or how-to article and update the website. Your monthly schedule could say: Write an in-depth article on a customer success story, build and send a newsletter, make a podcast and post to another publication. In the long term, your quarterly schedule might be something like: Publish a white paper as well as a collection of case studies and produce a video series.

Now that you have a calendar for your content, how do you stick to it and produce everything you want to?

One way to make content production easier is to repackage existing content in other formats, sizes and channels. For example, you could re-imagine an article on your blog as a short video or podcasted interview.

Or you could share the success story of a customer on your website and also post about it on Facebook. Repackaging works by allowing each piece of content to separately appear in search results, thereby increasing your visibility. For instance, Kinaxis, a supply chain management company, adopted a well-organized content publishing system including Twitter, blogs and videos.

The result?

Their web-based sales leads tripled in a year!

### 7. Metrics matter: set specific and measurable goals and be smart about keywords. 

As you might have realized from the editorial schedule, content publishing is a non-stop endeavor. It's therefore key to accurately evaluate your performance and use web-based tools to improve weak spots. Here's how to get started:

Begin by setting quantifiable goals and follow-ups in your editorial calendar. By tracking your content's performance you can constantly update and improve it. For instance, you might aim to produce a video that gets 1,000 views in a month. Or you could endeavor to get 10 bloggers to post positively about your company. As you set and attempt to achieve goals, keep track of your successes, but don't lose sight of your failures — because when you _don't_ succeed, it's necessary to know why.

But there are some tips to increase your overall online presence — namely, using keywords and posting original content. Doing this will make you easier to find with search engines like Google, which is what people usually use when looking for information online. But to be truly visible, you need content that earns a high position in search results, also called _ranking_.

How does ranking work?

Through _Search Engine Optimization,_ or SEO, a process which makes your site appear higher in search results depending on how unique your content is and on how many _inbound links_ (the number of times others have linked to your site) you have. Original content is key because it encourages sharing, thereby increasing visibility to search engine users who are invested in you and your product.

But keywords can also boost your search engine ranking, and choosing good ones is essential. A strong strategy is to use _long tail_ keywords — phrases composed of individual keywords. Long tail keywords are effective because they're more specific to your product and therefore more meaningful. One long tail keyword strategy is to write questions to problems. For instance, "how do I install a swimming pool?" is much more effective than simply "swimming pools."

### 8. Build unique content for business customers to suit their special needs. 

The core principles you've learned so far apply to creating content for all businesses, regardless of size and industry, but if your company specializes in business-to-business (B2B) commerce, there are a few specifics to keep in mind.

Why?

Because business customers tend to be guided by committees that conduct ample product research. As a result, the time it takes to seal a deal, also known as the _buying cycle_, can take months. Therefore, it's key to build a relationship that keeps your customers engaged during this long decision period.

But businesses have specific needs just like individuals and it's still essential to produce content that engages them, while anticipating their questions and offering helpful information. The only difference is that this process happens on a larger, more long-term scale.

So what's a good strategy?

One way is to build a spreadsheet that maps all possible questions and concerns individual buyers within a committee might have. To do this it's helpful to consult research from marketing-data services like TechTarget; this can give you insight into how large companies make purchasing decisions. Identifying the needs of individual buyers is key to keeping them engaged from start to finish.

But that's not the only way B2B content strategy is different; it should also set specific goals to guide customers through the buying process. To accomplish this, all the content you produce should be individually linked to both a short-term goal and your company's long-term business strategy. For example, a short-term goal of yours might be to encourage 100 website visitors to sign up for a _webinar_, or web-seminar, while a greater objective could be to instrumentalize webinars to boost sales from your website traffic.

Remember, the principle of re-imagining existing content through repackaging still applies. The only difference is to keep in mind how your repackaged content appeals to the specific concerns of your business customers. For instance, you could bundle information worksheets, customer success stories and webinars into a kit for prospective business customers to learn about your company.

### 9. Final summary 

The key message in this book:

**Producing attractive content requires bringing out the human side of your business. This means identifying your own voice and speaking to your audience about the things that matter to them. By providing helpful information and innovative ideas your customer's will learn to trust your content and, therefore, you.**

Actionable advice:

**Instead of remembering to "always be closing," try to "always be listening."**

Producing attention-grabbing content that matters to your customers is just as much about talking as it is about listening. So stay on top of the latest buzz by using tools like Google Reader, Google Alerts and Search.Twitter.com. You can't post it if you don't know it!

**Suggested** **further** **reading:** ** _Everybody Writes_** **by Ann Handley**

_Everybody Writes_ (2014) gives you invaluable advice on how to create great content, from using correct grammar to crafting engaging posts, tweets and emails. With just a handful of simple rules, these blinks will help you gain a better understanding of how to use the right words to keep customers coming back for more.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ann Handley & C.C. Chapman

Ann Handley is the Chief Content Officer for MarketingProfs, a company that offers marketing resources and training to 442,000 subscribers. She is the author of _Everybody Writes: Your Go-To Guide to Creating Ridiculously Good Content_, a _Wall Street Journal_ bestseller.

C.C. Chapman is a marketing consultant who, in addition to creating DigitalDads.com, co-founded the marketing company The Advance Guard _._ His previous positions include VP of New Marketing for the design-marketing firm, Crayon _._

  

[Ann Handley, C.C. Chapman: Content Rules] copyright [2012], John Wiley & Sons [Inc. or Ltd. as applicable] Used by permission of John Wiley & Sons [Inc. or Ltd. as applicable] and shall not be made available to any unauthorized third parties.

