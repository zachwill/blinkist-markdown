---
id: 55758e3e3935610007020000
slug: conversations-that-sell-en
published_date: 2015-06-08T00:00:00.000+00:00
author: Nancy Bleeke
title: Conversations That Sell
subtitle: Collaborate with Buyers and Make Every Conversation Count
main_color: F8C632
text_color: 856A1B
---

# Conversations That Sell

_Collaborate with Buyers and Make Every Conversation Count_

**Nancy Bleeke**

_Conversations That Sell_ (2013) reveals the changing nature of sales as its focus shifts away from waxing lyrical about a product to demonstrating a commitment to the needs of the buyer. From preparation to problem solving, these blinks guide you to a winning sales conversation and a strong sales career.

---
### 1. What’s in it for me? Master the conversations that will close sales. 

Our stereotypes about salespeople are way out of date. We think of a slick salesperson with a shiny suit pitching prospects and talking them around until they bite and buy whatever's for sale.

Nowadays it's different. As you'll see in these blinks, selling has transformed into something that is as much about _you_ as it is about the products you're selling. In short: Great salespeople today know that in order to sell their products, they have to work closely with prospects, and have two-way conversations.

That means less pitching and waiting to hear yes or no, and more working together to find solutions that work for both sides.

In these blinks, you'll learn

  * how to set up, moderate and close successful sales conversations;

  * why you shouldn't smoke before meeting a prospect; and

  * why you don't want to be like the Green Bay Packers.

### 2. The most important thing in selling is you, so show your strengths. 

Remember what salespeople were like when you were a kid? Pushy and slick?

Things are dramatically different today. For one, the stakes are higher.

Most products or services in today's businesses look fairly similar, and are priced similarly too. Because of this, the one-to-one pitching approach won't cut it anymore. Buyers aren't looking for someone to just talk about a product and its features. They're looking for someone that can make positive and lasting changes to their company, and above all someone who understands their concerns and ideas. Companies don't need salespeople. They need _differentiators._

So what makes a good differentiator? Three things: _preparation, transparency_ and _self-confidence._

Prepare yourself by ensuring you fully understand your client's situation. Collect as much information as possible not just beforehand, but throughout the process, too. The main thing is that you focus on _them_.

Don't see yourself as a salesperson trying to make a pitch, but a salesperson who guides the process, helps their client to overcome obstacles and outlines opportunities — so be open and trustworthy!

This in turn will allow you to build a valuable relationship with your client, which is vital for a successful sales process. To further strengthen this relationship, let your personality and its strengths shine through.

When she was new to selling, the author had a mentor who recommended focusing sales conversations on the product. But after a few early failures she realized that when she put her own personality and creativity on show, companies trusted her more, making her more likely to close the sale.

> _"You are an essential component of what you sell."_

### 3. Collaborate with your clients. 

Here's what a traditional selling strategy looks like: A client tells you about her problem, you prepare a solution for it, this solution is either accepted or declined. These days, things are different. It's all about developing a tailor-made solution together to achieve a perfect result.

This is what's called _collaborative selling_, and the first step is to understand a situation from all angles. Let's say a company's customer service department is unable to cope with all the requests it's getting, for example. To improve the situation, a salesperson could offer a new system to simply expand customer service. But why not ask _why_ they're getting so many requests in the first place? Perhaps they are not adept at solving problems the first time around, resulting in multiple requests from the same customer. With this in mind, another more valuable solution could be to improve customer service training.

Another way to improve collaboration is by _changing your language_. Most salespeople talk about _problems_, which they often approach using formulaic solutions. But by broadening your vocabulary to include discussion of _opportunities_ and what clients _want_ and _need_, you'll be better able to tailor your solutions to the company's context.

Perhaps the most important five words are: "What's in it for them?" Ask this of your client _and_ their customers, and consider how you can make a difference for them. Aim for a win–win–win situation. That's a profitable solution for you, the company and the customers.

Continuing with our customer service example, let's say that you notice that there's an underlying problem of poor training. If you build a new training method into your product, your client will see that you're focused on positive long-term results. You make a sale, your company's reputation is enhanced, and the client's customer service is finally improved.

We've now seen how collaborative sales work — but there are some other factors to consider when developing your collaborative sales strategy. Read on to find out in the blinks that follow!

> _"When you think beyond immediate wants and needs, you make a huge difference to your buyer."_

### 4. First, you need to prepare carefully. 

Now that you know the basic principles, let's get started. As always, preparation is key, so be ready to do some research!

Start by getting all available information together to create a fact sheet. Important surface details of the company include size, industry, business reports, and their philosophy. Next, dig a little deeper and ask what issues they're currently dealing with. Any bad quarterly results or management changes?

Then consider who it is you'll be talking to. Find out her working style and even personality type to adapt your presentation and questions.

Finally, outline the main facts about the problem they've hired you for, your solution and any possible questions that you anticipate will arise in the conversation.

The work doesn't end with your research. Now it's time to prepare yourself and your material.

Presenting yourself in the best way possible isn't difficult — just give yourself a quick check-up concerning your look, clothes and odors. Rule of thumb: don't smoke or use aggressive deodorants before seeing a client.

Additionally, check your materials, including your presentation, papers and even laptop. Small things count. The author was once sitting in an audience when someone else used her laptop. She suddenly saw all the dirty fingerprints on the back of it — she just wasn't aware of them before. To avoid making a bad impression, these days she always wipes it clean before a presentation.

After you've prepared your appearance, don't forget to prepare mentally. Take a few minutes to visualize your presentation and go through your strengths and those of your product again.

Of course, preparing this thoroughly will take a bit of time. Nevertheless, when you offer a solid presentation and are ready for any tough questions that follow, you'll know it was worth it.

### 5. The way you start a conversation is crucial. 

We all know how important first impressions are, and giving the wrong one will shape the whole conversation, especially when it comes to trust and transparency. Whether you're on the phone, face-to-face, or speaking at a conference, there are three things to tick off first. _Introduce yourself, state why you contacted someone_ and _the subject you'd like to speak about_.

Of course, you should greet your client and state your name and company. But there's no need to then immediately begin pitching your services. Instead mention previous contacts, a referral or any issues which are currently important for the client company.

Only then should you use one or two sentences to explain why your product or service could improve their situation. Remember: "What's in it for them?" Perhaps you could state that another client working in their industry reduced their production costs by 15 percent using your product.

Then get down to business as fast as possible using _connection questions_. Connection questions should engage the buyer personally and refer to a relevant matter, e.g., "I noticed you've been working with several vendors over the last ten years. What, in your opinion, were the best approaches for dealing with problems concerning X?"

When they sense the focus is on them, your prospect will begin to open up. This will help you to ask more questions in the ongoing conversation concerning the current issues.

Moreover, throughout the whole conversation, pay attention to their tone of voice and body language (if you're in the room with them) which can tell you something about their emotional state. If the buyer is looking at her watch all the time, you should shorten and conclude the conversation.

This all may sound daunting, but there's no need to panic. You can rehearse initiating a conversation. Even preparing some notes about possible connecting questions for different topics can make you feel more secure.

### 6. Ask the right questions to get the information you need. 

So you've done your research and laid the foundations for a promising sales relationship. Now you've got to sustain your success by asking the right questions. What are the dos and don'ts?

_Don't_ ever ask about something you already know or should know. This looks quite unprofessional and will make the client suspicious.

_Do_ replace closed questions with open-ended ones. These are questions referring to emotions, motivations and perceptions of problems. For example: "What could be improved in customer services?" "How would your company be impacted if delivery costs were reduced?"

_Do_ ensure your questions cover the following four areas: the current situation, the future situation, the risks, and the opportunities faced by the company. _Don't_ forget to prepare some of them beforehand.

Once you've figured out what questions to pose, consider _how_ to pose them. You'll want to develop a pleasant and productive conversation mode. One way to achieve this is by asking _follow-up questions_. This will keep your client engaged, as will your body language, eye contact and affirmative sounds. You can also paraphrase what they've said back to them in order to demonstrate that you're on the same page.

As the conversation draws to a close, it's time to address all the details: how the decision-making process will work, who will be involved, the budget and the time frame. This will make both you and your client aware of what needs to be prepared before the next meeting.

There's a lot to remember here, so to ensure you get your questions right, here's a tip: practice conversations with other clients or colleagues to see which questions work out best and use them in your next pitch.

### 7. When presenting your solution, focus on the buyer. 

We all know that horrible feeling when you look up in the middle of your presentation and see that your clients don't seem to be listening anymore. Sadly, this is usually our own fault. Fortunately, it's preventable.

As we learned in the first blink, the buyer isn't interested in your products' details, but in what it can do for her company. So help her see it, firstly in the way you structure your very sentences. For example, "Our repair service offers replacement within 24 hours, so _whenever you need it_, _you can be sure_ you won't suffer through any production delays."

You can also ensure your buyers connect with your product by supplying them with _stories_ about how the product benefited other clients, or make things hands-on by providing them with prototypes they can see and touch.

Nevertheless, objections can and do arise, so anticipate what they might be and strive to deal with them as soon as possible to keep a hold on the conversation.

Always ensure you're taking them seriously and reacting accordingly, to show that you're listening fully. Then stop for a moment and try to find a solution together with your client. This will demonstrate that you're an effective _problem resolver –_ someone who cares about more than just the issue at hand, with the ability to create flexible solutions to other problems for a lasting positive outcome.

For example, your client might raise concerns about how long the implementation of your service could take. To respond to this, ask them exactly how much time they'd be willing to allow, and what consequences any delay could lead to. You can then share stories of implementation processes in your other clients' firms, and what you learnt. Finally, explain options for accelerating the process.

### 8. Close the conversation in a productive way. 

Congratulations, you've made it through the most difficult part of the conversation, the questions. But don't lose concentration now, you're so close to closing the deal!

Any vagueness in an arrangement won't go unnoticed — it could even make your prospect doubt the deal and drop out. So keep things on track by ensuring everything you've discussed with your client is 100 percent clear. As the conversation concludes, summarize each problem and idea you've developed together, highlight the prosperous future ahead and ask after missing links or details.

It's always better to clarify something than to find out at the very end that the deal won't go through because of a misunderstanding. For example, if they're concerned about the high costs of implementation, provide concrete numbers up front.

Remember, your aim for every contact should be to get a decision or at least some commitment to something. So, after clarifying everything, go ahead and ask for a decision. You and your buyer should have developed a trustworthy relationship by now, so that any matters can be spoken about upfront.

If it's too early in the sales process to make a decision, try to get some commitments. You can do this by asking what steps should be taken next and what you and they can prepare for the next meeting.

As we know, transparency is crucial, nowhere more so than in this final stage of the process. So don't be shy about asking how many more meetings will be needed and what exactly they expect from you for the next one.

Finally, close the conversation as partners would. There's no need for subservient language here, so don't gush that you're honored or delighted. Simply reiterate your commitment to the process. This will further strengthen your relationship.

> _"Knowing what you want or need to close identifies your goal for the conversation."_

### 9. To be successful, you’ll need the skill and the will. 

By now, you've gained all the tools you need for a successful sales process. However, there's something else that differentiates a good seller from a great seller: _Will_.

Your will is your inner driving force. It gives you the courage to take action and work for your targets, and sometimes, it can be even more crucial than skill. Take the Green Bay Packers football team. They had the skill to reach the Super Bowl for two consecutive years, but they couldn't generate the strength required to win and lost each time. Why? They lacked the will.

Sure, without skills, you'll soon lose credibility as a seller. But without will, you'll never reach your goals and won't be able to compete with the ever-improving sales competition. So to ensure you've got sufficient will, consider its different components.

First, you have to _be confident_. This includes self-confidence as well as being aware and happy with your role as a seller and the value of your product or service.

Next, you need to _be transparent about your goals_ to yourself and others. Only if you know what you really want, can you take action to reach it. Stating your goals clearly to colleagues and clients also adds accountability. Once you know your goals you can take the most productive actions and _initiate_ your own success process.

Finally, what is often forgotten but equally important: Be aware of your emotions and deal with them. Being a seller can be hard, so you'll need strong _emotional intelligence_.

Don't worry if you don't have all of these qualities just yet — they all take practice! Just by focusing on a sense of will, you'll be better able to face whatever challenge your work throws at you.

> _"Purposeful, focused action stems from knowing what to do with your skill, and then tapping into your will to do it."_

### 10. Now take aim and get ready. 

You've got the skill and will to succeed in individual conversations, so it's time to think about your success in the long term. Of course, each of us has our own set of goals, but not all goals are created equally.

The most effective goals are both _measurable_ and _revisable_. As such, it's vital to define your goals carefully. What are you aiming for, and when? Can you sum it up in one practical, present tense sentence? For example: "By the 15th of June, I will have X contracts that are worth Y million dollars."

Now that you've defined your goal, consider the steps that lead to it and write them down too. Include time frames and who or what is involved. Need to improve your material? Ask for help from a colleague. Need practice speaking in front of groups? Take an evening class! Remember: you've always got options.

Whatever career development you take on, you'll need a flexible attitude that allows your goals to be renewed or revised when needed. Check from time to time if your long-term and short-term goals are still what you really want, and stay realistic.

For example, perhaps you wanted to learn Spanish to expand your skills, but job opportunities have just opened up in China. No worries — just renew your focus and start brushing up on your Chinese business etiquette.

It's also helpful to think about possible hurdles and how you could deal with them. This will keep you prepared in case they do occur. And finally, keep taking stock of the progress you make and the rewards that await you.

If your targets and action steps are clearly stated, it'll help you to actually start making your goals a reality. Now you have the skill, the will and the required tools to boost your selling career — go for it!

### 11. Final summary 

The key message in this book:

**No matter how great the product or service you're offering, always focus on how it creates value for your** ** _clients_** **. The most important thing in selling is to focus on them, understand their problems, opportunities, wants and needs and find a perfectly tailored solution!**

Actionable advice:

**Prepare for each meeting by writing a few things down.**

Before a meeting starts, always prepare a few main facts. Start with who you're talking to and the purpose of the conversation. Write down what you already know about their issues and requirements. Then think of a sentence to initiate the conversation and questions you need to ask, before moving on to finding the perfect solution with them. Lastly, note your objective for the meeting.

**Suggested** **further** **reading:** ** _Selling To Big Companies_** **by Jill Konrath**

_Selling to Big Companies_ (2006) closely examines the ins and outs of dealing with corporate decision-makers. From making an initial contact to developing your sales pitch, this book will give you all the tools you need to sell to big companies.

**How did you like these "niche" blinks?**

In an effort to deliver valuable insights to all our customers, we're considering expanding our selection to also cover specialized and somewhat less accessible business topics such as B2B sales. What do you think? Would you like to see more blinks on such specialized topics? Just drop an email to remember@blinkist.com and share your thoughts!
---

### Nancy Bleeke

Nancy Bleeke is the founder and president of the training and consulting company Sales Pro Insider. She is the developer of the Genuine training courses.

