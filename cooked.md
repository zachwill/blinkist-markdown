---
id: 587df559850310000464e8b4
slug: cooked-en
published_date: 2017-01-20T00:00:00.000+00:00
author: Michael Pollan
title: Cooked
subtitle: A Natural History of Transformation
main_color: EDD455
text_color: 6E6227
---

# Cooked

_A Natural History of Transformation_

**Michael Pollan**

_Cooked_ (2013) details the history of humanity's relationship with cooking, baking and fermentation. These blinks explain how cooking became an essential aspect of being human while exploring the varied techniques people have tried and perfected to turn nature's bounty into a delicious, nutritious meal.

---
### 1. What’s in it for me? Learn how cooking transformed what it means to be a human. 

What makes us human? Is it that we can speak and sing? Other animals can also do these things. Or is it that we are empathetic — probably not, as other animals show empathy, too.

The one thing that makes us human is a curious combination of fire and pot: humans cook.

This seemingly simple act has played an enormous role in shaping our history. By learning how to cook, humans as a species have thrived, achieving things we would never have been able to achieve had we stuck to a basic foraging diet.

These blinks walk you through humanity's kitchen, examining the pots and pans, and ovens and grills that together have helped create history.

In these blinks, you'll discover

  * why there's a connection between cooking less and eating poorly;

  * why consuming a cooked egg is healthier than eating a raw one; and

  * how yeast transformed society's relationship with grain.

### 2. Cooking food makes raw ingredients more digestible and more nutritious for humans. 

Some people think that raw food diets represent a return to nature — a healthier way to live. But such logic is off base. If we didn't cook food, we'd spend a ton of time just chewing it.

For humans to live well consuming just raw food, we would need a much larger gut and more powerful jaws. Our apelike ancestors did have these traits, but they came with a trade-off.

Primatologist Richard Wrangham hypothesized that before early humans began to cook, they spent over _half_ their day chewing their food.

We can witness this today with chimpanzees that like to eat meat but don't cook. When a chimpanzee eats raw meat, it has to chew for a long time, technically leaving it little time to hunt — not nearly enough time to properly support a carnivorous diet.

Regarding expended calories, eating hard-to-digest food is costly. For many species, the calories expended in digestion are nearly equal to the calories needed to move around.

Here's where cooking food makes a difference. Cooking alters the composition of food both physically and chemically, making it more nutritious and easier to digest.

When we cook a protein-rich food like meat, the heat works to unravel the structure of the meat's proteins, unlocking the energy within. These now weaker protein structures are easily digested by the enzymes in the human stomach.

When you boil an egg, for example, 90 percent of the cooked egg is digestible. A raw egg, in contrast, is only 65 percent digestible by the human gut. The same rule applies to many other foodstuffs: the more food is cooked, the easier it is for your gut to absorb the nutrients stored in the food.

Another benefit of cooking is that it makes food safer to eat. Some plants, like the root cassava, a staple of South American cuisine, is toxic when raw. Once cooked, it is safe to eat, nutritious and easily digested.

Cooking also works to preserve food. Thus raw meat that would spoil quickly remains edible for a longer period once it's cooked.

### 3. What do smoked bacon, mushrooms, kelp and anchovies have in common? They express umami. 

Lots of people would gladly add bacon to just about anything. But why has bacon become so popular?

The recent obsession with this cured pork product is inspired by the human affinity for a unique taste also present in mushrooms, meaty broths and dried anchovies.

It's called the fifth taste, or _umami_. Here's how umami was discovered.

For some time, scientists held that there were four tastes the human tongue could register: bitter, sweet, sour and salty. Since 1908, however, Japanese researchers have recognized a fifth taste — umami.

At the turn of the century, chemist Kikunae Ikeda was studying the snowy crystals that form on dried kelp, or _kombu_. The Japanese use kombu as an ingredient for soup stock, among other dishes.

To his surprise, Ikeda made a curious discovery. What he found was _glutamate_, the taste of which didn't quite fit into existing taste categories — so Ikeda coined the sensation as _umami_, roughly translated as "pleasant savory taste."

It wasn't until 2001 when US scientists studying taste receptors on the human tongue found one specifically for glutamate, thus cementing the idea in the West of umami as a fifth taste.

But while glutamate is the basis of many distinctly flavored foods, when consumed on its own, it's neither tasty or flavorful. Rather, umami happens when glutamate is combined with other flavors.

Umami is also associated with the texture of certain foods. For instance, liquids such as soup broth that contain flavorings consistent with umami tastes can feel thicker on the tongue.

So while glutamate is key to the expression of umami in food, two other molecules also influence umami flavors. Inosine is found in fish; guanosine in mushrooms. You've probably experienced umami when you add fish sauce to a steaming bowl of Vietnamese phở or porcini mushroom stock to a creamy risotto.

Traditional Japanese soup stock uses foods that contain all three of these molecules — a veritable umami explosion!

> _Breast milk contains a lot of glutamate, so it has a taste of umami. This may be one reason why infants love the taste so much!_

### 4. The move to convenience foods post-war led to a gradual but consistent decline in healthy eating. 

Just 20 percent of the money households spend on food is directed to the people who produce the food. So where does the rest of that money go?

America's "food-industrial complex" consists of more than just massive farms. This system is made up of food manufacturers, advertising executives and marketers, which, decades ago, worked to create a surplus of industrially-processed food. This quickly became the basis of the American diet.

After World War II, the food manufacturers that supplied rations to US troops abroad needed a market for their products. As a result, canned dinners, dehydrated potatoes and instant coffee became the staple for convenience foods in 1950s America.

Praised as innovative, convenience foods also created problems. As people gave up cooking fresh foods, diets became less healthy overall. Why exactly?

Processed ingredients, ready-made meals and "fast foods" are usually less healthy than fresh food. It's far cheaper for a company to create processed foods by piling sugar, fat and salt onto a base of corn or soybean material than it is for a farmer to grow whole, natural foods. Processed food is thus both big business and big money.

Aside from being unhealthy to eat, processed food poses another problem for society. Because it's so easy to obtain and consume, we eat more processed foods than whole foods because it's more convenient.

Popping a frozen pizza in the oven is far easier than making the dough and tomato sauce yourself. And if you had to mix the ingredients for every soda you guzzled, you'd probably drink far less!

In fact, the connection between the abandonment of cooking and unhealthy eating is so pronounced that time spent in the kitchen is inversely correlated to obesity levels.

In 2003, Harvard economists performed a study that looked at how people cook in different cultures. They found that the more time people spend preparing food in the kitchen, the less obese they were overall.

What's more, the economists discovered that the time people spent in a kitchen cooking was a better indicator of obesity level than was income or a country's percentage of women in the workforce.

### 5. The process of fermenting and baking dough unlocks nutrients, making bread a needed staple. 

Whether eating out or cooking at home, the sandwich remains the most common American meal. And what's more important for making a sandwich than the bread?

Bread has become a staple of the American diet for many reasons. When humans learned to make bread, they tapped into massive energy stores previously locked up in grasses and grains.

Our paleolithic ancestors used to eat the seeds of wild grasses — the only part of the plant that the stomach can immediately digest. Those same early societies began cultivating the seed plants for food, harvesting bigger, more nutritious seeds. With time, they found that by mashing, roasting or soaking the seeds, the food was more filling and sustaining. This seed mush was also cooked on a hot surface as a sort of unleavened bread.

Later, in Egypt around 4000 BC, a simple bowl of seed mush left in a warm place started to bubble, its contents expanding with air. Wild yeasts in the mush had begun the process of fermentation. A curious cook put the fermented dough in an oven, thus creating the first loaf of leavened bread.

This discovery was a culinary game-changer. Bread is far more nutritious than the sum of its parts; the processes of fermentation and baking releases nutrients inaccessible in raw ingredients. While a diet of raw wheat flour could sustain a person for a short time, a person can live on baked bread.

Nutrient density isn't the only reason bread became a staple. Bread represents a smart use of energy.

Grasses such as wheat, barley, oats and spelt cover some 65 percent of the earth's surface, absorbing the sun's rays and transforming them into energy through a process called photosynthesis. These grasses are food for the animals we in turn eat.

Yet animals at the end of this food chain miss out on a lot of that original plant-based energy. When one animal eats another animal, only 10 percent of what the prey has consumed is accessible to the predator as energy. The other 90 percent has essentially been used by the prey.

Think of it this way: it's as if you had ten pounds of wheat to eat, but threw nine pounds in the trash!

Consuming sources of energy lower down on the food chain, such as plants and seeds, is thus far more efficient. This is why there are more herbivores in nature than carnivores.

> Bread is now the world's most important food, accounting for one-fifth of the world's calorie consumption.

### 6. Industrial baking and the sweet taste of white bread has sapped loaves of any nutritional content. 

Americans get around 20 percent of their calories from wheat, which in itself isn't problematic. However, 95 percent of the wheat we consume comes in the form of white flour, which has little nutritional content.

In fact, eating white flour isn't much different than eating pure sugar.

The human craving for white bread is nothing new — but the industrial methods by which white bread is produced certainly are.

For ancient Greeks and Romans, white bread was a prized commodity. Back then, when spoiled food and disease were common, whiteness was an indicator of cleanliness and wholesomeness. White bread was also both sweeter and easier to chew than whole grain bread, an important consideration at a time when dental hygiene wasn't widely practiced and people often lost their teeth early in life.

Although white bread was prized in ancient societies, the transition to pure white bread came in the nineteenth century with the invention of roller mills. These machines completely separated the "undesirable" germ and bran from a wheat seed, leaving just the starch behind.

Removing the germ and bran, or the color and texture of a wheat seed, also removed the most nutritious parts of the seed where vitamins, minerals and antioxidants are found.

The results of our growing taste for and consumption of white flour were evident at the beginning of the twentieth century, as communities showed increased signs of malnourishment, diabetes and heart disease.

Because of increasing health problems, the government encouraged industrial bread producers to begin fortifying white bread with nutrients. In the 1940s, companies such as the Continental Baking Company and Wonder Bread began adding B vitamins to white bread.

Gradually companies started to add bran back to bread, creating "whole-grain" loaves. Yet the result still isn't nutritionally comparable to bread made with whole wheat. Even worse, many of these whole-grain loaves are loaded with additives and sugar to make them taste sweet like white bread.

### 7. Microbes are not only essential to fermentation, but crucial to keeping your gut healthy. 

If extraterrestrials came to study humans on earth, they might conclude that we are some type of superorganism, as hundreds of different species cohabitate each human body.

These countless organisms perform essential functions without our even being aware. We really are superorganisms composed primarily of microbes.

Some nine-tenths of the cells in your body are those of microbial species, and the vast majority live in your gut. As a result, 99 percent of the DNA in your body is microbial DNA. When you think about it that way, you're basically a vehicle for colonies of microscopic species.

With that in mind, perhaps you'd like to know what they're up to?

The main role of microbes is to serve as external digesters, breaking down food so your body can better absorb nutrients.

Microbes are also essential in the process of fermentation. The list of foods we love that wouldn't exist without microbes includes cheese, coffee, bread, beer and chocolate to name a few.

Nearly every culture on the planet uses the process of fermentation in one way or another to create edible foods.

Yet as we sterilize more and more of the food we eat, we're losing the potential health benefits that microbes can provide. Modern medicine has encouraged the overuse of antibiotics, which indiscriminately kill off microbes in the stomach, good and bad.

And those bright green pickles at the supermarket? They're sterile, which is a shame. Naturally fermented foods have been shown to improve digestion, counteract inflammation, strengthen the immune system and even fight cancer, all by increasing the microbial diversity of the gut.

For centuries, humans have known that fermented foods are healthy. For example, when Captain Cook embarked on a two-year sea voyage, he took along a supply of sauerkraut to feed his crew and combat scurvy. We now know why this worked, as sauerkraut is packed with vitamin C.

### 8. Humans and animals alike enjoy a stiff drink, but some fermented foods are an acquired taste. 

When the fruit of the durian tree drops to the ground and begins to rot, it's only a matter of time before visitors of all shapes and sizes gather to reap the spoils. Among them are tigers, wild pigs and rhinos, all seeking the spontaneously fermented alcohol that this process produces.

Alcohol is the most popular fermented food, not just across cultures but also the animal kingdom. While humans are the only animals that make alcohol — although there have been reports of monkeys in China fermenting fruit to drink the alcoholic result — many animals have it made _for_ them.

For instance, the bertram palm in Malaysia produces a daily supply of fermented flower buds for the pen-tailed tree shrew. The little rodent slurps from each flower as it flits from palm to palm, pollinating the trees.

In studies, chimps and rats have also been found to be liquor enthusiasts, although their drinking habits differ. If you put a chimp in an all-you-can-drink situation, that's precisely what it'll do.

Rats, on the other hand, act more like humans, enjoying an aperitif before dinner and another at day's end. They then proceed to get ludicrously drunk together about twice a week.

Other species also drink socially, and the reason for this behavior is simple: a single drunk animal is an easy mark for predators.

While we all seem to enjoy a drink, a taste for other fermented foods is often culturally determined.

Korean kimchi, Icelandic pickled shark and aged French cheese are all fermented foods with strong flavors for which people acquire a taste as children. Outsiders who encounter these foods later in life often have a strong positive or negative reaction to them.

One powerful example comes from World War II, when American troops were ordered to burn down a series of warehouses in Normandy, thinking they contained stinking corpses. In reality, their culturally unadapted noses had misidentified huge stores of typically pungent Camembert cheese.

### 9. Final summary 

The key message in this book:

**Cooking is more than a hobby, a chore or lively career path. It's a defining trait of the human race. However, the more we've placed cooking in the hands of corporations, the less wholesome and pleasurable our food has become.**

Actionable Advice

**Try your hand at fermentation.**

Have a go at baking a fresh loaf of bread, pickling a jar of vegetables or even making mead from honey. Even the simplest act of fermentation can elucidate the mystery of this natural process, all the while filling you with wonder at its infinite possibilities.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Omnivore's Dilemma_** **by Michael Pollan**

We face an overwhelming abundance of choices when it comes to what we eat. Should you opt for the local, grass-fed beef, or save time and money with cheap chicken nuggets? Organic asparagus shipped from Argentina, or kale picked from your neighbor's garden? The Omnivore's Dilemma examines how food in America is produced today and what alternatives to those production methods are available.
---

### Michael Pollan

Michael Pollan is a food journalist, bestselling author and a professor of journalism at the University of California at Berkeley. His other books include _The Omnivore's Dilemma_ and _In Defense of Food_.

