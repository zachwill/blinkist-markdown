---
id: 5694b4ca2f68270007000036
slug: cooking-up-a-business-en
published_date: 2016-01-15T00:00:00.000+00:00
author: Rachel Hofstetter
title: Cooking Up a Business
subtitle: Lessons from Food Lovers Who Turned Their Passion into a Career and How You Can, Too
main_color: CA8B5B
text_color: 7D5638
---

# Cooking Up a Business

_Lessons from Food Lovers Who Turned Their Passion into a Career and How You Can, Too_

**Rachel Hofstetter**

_Cooking Up a Business_ (2013) is essential motivational reading for any foodie-turned-entrepreneur. Whether you're testing the waters with a new product or looking to revitalize your company strategy, these blinks offer you guidance combined with real-life stories from leading entrepreneurs.

---
### 1. What’s in it for me? Live your dream as a foodie entrepreneur. 

Are you glued to the screen when Iron Chef is on? Do you live for taste, savoring each and every new flavor you come across? When you taste a food product, do you think: I could do better than this?

If you have dreams of becoming a food entrepreneur, these blinks will help you fulfil them! Based on the real-life stories of other foodies who went on to build successful businesses, these blinks show you just what it takes to get out of your kitchen and into the market. 

One thing is for certain: we all need to eat. Starting your own food business can make you rich only if you successfully meet the many challenges along the way. So heat up that pan and sharpen that knife — and take the advice of these seasoned professionals to heart. 

In these blinks, you'll discover

  * why fresh food products face tons of official regulations; 

  * how just a few freebies could bring you instant success; and

  * why when it comes to food products, it's all about need.

### 2. Lack of funding isn’t a limitation but an opportunity; work hard and get resourceful! 

You're constantly told, whether by inspirational books or close friends, to chase your dreams no matter what. But what if you want to start a food business and you're short on cash?

Lacking funding isn't a dead end. Rather, it's an opportunity to put your creativity and resourcefulness to the test.

Take Maddy D'Amato and Alex Hasulak, founders of Love Grown Foods. While still in college, they cooked up the idea of building a food business using Maddy's family's delicious family recipes for granola. Of course being students, they didn't have much in the way of financing.

By day, the two young people lived at their parents' homes and worked hard at their regular jobs. But by night, for hours they cooked and hand-packaged batches of granola in a catering kitchen, watching movies at the same time to keep them going.

This initial phase was tough, but fun! D'Amato and Hasulak proved that with hard work, they could start their business with very little start-up cash.

As well as working with limited resources, you should also feel comfortable with selling your product before you actually make it. This sounds risky, but will help you avoid unnecessary spending.

The first clients for Love Grown Foods were two neighborhood coffee shops. A great place to start, yet D'Amato and Hasulak had bigger dreams. Hasulak soon managed to close a huge deal with the Aspen City Market. They were worried as they had yet to produce such a large amount of granola — but with many long hours, they made their deadline.

Then D'Amato and Hasulak landed a deal with King Soopers, a supermarket in Denver that brought their product to 40 stores. Quickly the chain wanted to sell their product in 80 stores!

With this, D'Amato and Hasulak realized they could start working on Love Grown Foods full-time. In an impressive example of scaling, Love Grown Foods was stocked in 1,300 stores a mere 18 months after the duo's first supermarket deal!

### 3. While your product may change, the important thing is to stick to your brand story, no matter what. 

It goes without saying that to start a food business, you need to know what your product is. Right? 

Well, not necessarily. The best ideas often emerge from trial and error, especially in the food industry.

Zak Zaidman, for example, knew that he wanted to support organic farmers in Costa Rica. But it took him a while before he found an actual product to sell successfully. 

Zaidman had fallen in love with the Costa Rican landscape, and saw that the use of pesticides in banana farming was destroying this beautiful land. Working conditions on banana farms were also poor, and this inspired Zaidman to do something to improve the situation for workers. 

Simply importing bananas wasn't enough to support the farms and give them an edge in the larger market, so Zaidman considered selling banana vinegar, a local product. 

Additionally, Zaidman also imported chocolate and dried fruits. To his surprise, the vinegar didn't catch on but the chocolate was a hit! Today his company, Kopali Organics, stocks its products in every Whole Foods Market in the United States. 

Although Zaidman's first product didn't succeed, his backup idea combined with a genuine brand story — organic, fair-trade products — resonated with customers. 

Similar to Zaidman's experience, entrepreneurs Shannan Swanson and Liane Weintraub started their food business without a clear product in mind. As young mothers, the two women wanted to provide children with organic baby food. They knew they wanted their company to lead the market. But how?

After meeting with brand strategist Susan White, Swanson and Weintraub decided that their company would be based on three core attributes: genuine, all-organic and most importantly, fun. 

Though they eventually switched their product from baby food to kids' fruit snacks, they remained true to their original brand values.

### 4. Manage the challenge of scaling by staying in control of production and changing tack if needed. 

Many successful food entrepreneurs start their journey with a recipe cooked in their home kitchen. Yet scaling a small recipe from the kitchen to a multi-state business is no easy feat. 

Mary Waldner learned that staying personally involved in production is crucial when scaling. She was inspired to start a business for gluten-free products as she had a gluten allergy herself — a condition that in the 1990s was still mostly unheard of. 

Waldner made gluten-free crackers. Her friends loved their taste, and encouraged her to take the product further — and _Mary Gone Crackers_ was born. As Waldner's crackers became increasingly popular, her team decided to open a co-packing operation.

Though this was a considerable step forward, it presented other problems. Waldner had less control over her product, and her crackers weren't being produced to the standards she expected. 

So she decided to change tack. After securing venture capital, Waldner opened her own factory. Although she couldn't produce as much stock, Waldner did have more control over the production process. This raised the quality of her product, and Waldner was happy to let her brand and company grow at a slower but more steady pace. 

While sometimes businesses can scale too rapidly, some food entrepreneurs have a tough time scaling at all. Phil Anson wanted to sell fresh burritos as a wholesale product, but struggled with tough food regulations. 

As Anson moved production from his Colorado cabin to a commercial kitchen, he realized that fresh wholesale products containing meat were legally required to be made in a USDA-approved facility. 

After five years of struggling with cost, distribution nightmares and shelf-life issues, it was clear that his fresh food product was too hard to scale for the wholesale market. 

So he did some research and switched to frozen food, as frozen products are far more easily scalable than fresh. Anson's Evol brand of frozen burritos is now stocked in 7,000 stores — a huge improvement on the 400 that stocked his fresh product.

> _"In short, I realized that selling fresh food wholesale is about the most difficult path you can take in the food world."_

### 5. Consider exploring short-term credit or equity partners to compensate for a lack of capital. 

While funding is a challenge for any start-up, it's a particularly tricky problem for food businesses. With costly manufacturing processes and shelf-life issues, not to mention the constant search for suppliers and transport, it's often some time before a food business turns a profit. 

So how can you make sure your food business survives this tough initial stage?

One strategy involves selling your product to retailers before you purchase supplies. For example, Cameron Hughes Wine buys surplus wine in bulk from producers who don't want to bottle or sell it themselves. The wine is then sold under the Cameron Hughes Wine label. 

This sort of business strategy in the wine industry was established in ancient times, and such a wine reseller is called a _négociant_ in French _._

But how could Cameron Hughes Wine afford such large quantities of stock in the first place? The company makes use of a second strategy. Cameron Hughes sells its wines to bulk retailers. These retailers provide a short-term line of credit that Cameron Hughes uses to purchase the bulk wine. 

Once Cameron Hughes Wine delivers the stock, this credit is paid back in full. This sort of strategy is a great way to scale up your business without much initial capital. 

Another way to make up for a lack of capital is through _equity,_ or offering shares in your company. When entrepreneur Justin Gold of Justin's Nut Butters wanted to grow his business, he decided that he'd need the expertise of a senior businessman. 

Lance Gentry was his guy, chosen by Gold for his track record, as Gentry had turned another smaller company into a $25 million enterprise. While Gentry was talented, Gold could hardly afford to pay him the high salary he was used to.

Instead, Gold gave Gentry shares in his company as remuneration. Gold and Gentry then teamed up to find more investors and distributed the shares fairly between them, so that Gold always had the final word in the business. This network was just what Gold needed to boost his business.

### 6. A successful product can be either simple or sumptuous; yet must always fill a market need. 

In theory, a food entrepreneur can turn any kind of food into a product. But are there certain types of food that are best suited for a business? 

Well, there's really no formula, but more than anything else, success comes when you identify a need for your product to satisfy. Even the simplest products can sell well if they fulfill an important need. 

Food entrepreneur Kara Goldin wanted her family to consume less sugary foods. The problem was that most flavored drinks contained artificial sugars. But drinking water was just too boring, especially for her children. 

Through her own experience, Goldin had stumbled upon a need for a tasty and refreshing flavored drink made without sugar. 

By adding a few pieces of fruit to basic water, Goldin created a subtly flavored drink that she loved, which became the prototype for her product, Hint. 

Goldin's story is a great example of how basic needs can give birth to simple yet desirable products. 

Luxury products can also be great at fulfilling certain customer needs. Katrina Markoff had a successful career working at several world-class restaurants. She was fascinated by dynamic flavors, and traveled the world to taste as many flavor combinations as possible. 

Yet once home she realized things just didn't taste as good as they used to. Chocolate manufactured in the United States was too sweet and tasted artificial. She recognized the need for a chocolate that a refined palate would appreciate. 

Markoff experimented with recipes herself, inspired by the flavors she'd experienced in India. With some great recipes under her belt, she decided that Vosges Haut-Chocolat, her brand, also needed luxury treatment. The packaging, marketing and eventually the look and feel of her own stores all reflected the luxurious taste of her product.

### 7. Build buzz around your business by getting clever with free samples and personal promotions. 

Who can resist free samples at a store? This type of marketing is simple, and it works. Think about it: experiencing a new, great taste is a quick and easy way to convince a customer to buy. 

So why not offer a potential customer a sample before they break out their wallet? _Sample promotions_ are one of the most effective ways to get customers engaged with your food product and the story behind it. 

Each of the food entrepreneurs we've met in these blinks used sample promotions to get people talking about their product. The duo behind Love Grown Foods even spent their early days traveling from supermarket to supermarket across Colorado to offer granola samples!

Yet a campaign like this can be time-consuming. As an alternative, you could advertise your product with a _directed campaign_. This means that you present samples at locations where you know your target audience spends time or shops. 

Keith Belling did this to launch his product _Popchips_, a low-fat alternative to baked and fried chips. 

He began his campaign on the West Coast, knowing that his nutritious snack would fit well with the health-conscious lifestyle in Los Angeles. But once in New York, he knew he had to modify his approach to suit this tougher market. 

Belling handed out sample bags not just at grocery stores but also at high-profile events such as Fashion Week. His chips became popular as he targeted influential celebrities personally, by sending them sample packages with a personal handwritten note. 

In the course of a year Popchips was stocked in more than 2,000 stores in Manhattan alone! This goes to show that calculated marketing moves can give your business the buzz it deserves.

### 8. Final summary 

The key message in this book:

**If you want to turn your passion for locally grown food, healthy eating or fair trade into a business, then dive in! A food business presents plenty of challenges, however. Whether grappling with funding or scaling, if you're resourceful, creative and hold firm to your brand's values, you'll soon taste success as the world savors your innovative product.**

**Suggested further reading:** ** _Start Something That Matters_** **by Blake Mycoskie**

_Start Something That Matters_ gives concrete, colorful and often humorous advice on overcoming your fears and starting a company — even if you don't have business experience or many resources at your disposal.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Rachel Hofstetter

Rachel Hofstetter is an avid food writer and contributor to magazines like _O, The Oprah Magazine_ and _Reader's Digest_. She is also the founder of Guesterly, a service that creates who's who pamphlets for personal and corporate events.

