---
id: 574ffef37bea3d0003c71575
slug: cosmosapiens-en
published_date: 2016-06-08T00:00:00.000+00:00
author: John Hands
title: Cosmosapiens
subtitle: Human Evolution from the Origin of the Universe
main_color: EFC264
text_color: 705B2F
---

# Cosmosapiens

_Human Evolution from the Origin of the Universe_

**John Hands**

_Cosmosapiens_ (2015) is about the evolution of scientific theory — from the origin of matter and the universe to the emergence of life on Earth and the evolution of human consciousness. For centuries, we've been struggling to find out who we are and why we're here. Learn about the progress we've made toward answering these important questions — and about the barriers that still stand in our way.

---
### 1. What’s in it for me? Find out what we are and why we are here. 

Where did we come from? It's a question we ask almost as soon as we can talk. And the answers to questions about humankind, life and the universe depend entirely on the worldview of the person we've asked.

Instead of the usual subjective accounts, however, these blinks provide an overview of the evolution of scientific theory, exploring things such as the origin of matter, the universe, the emergence of life on Earth and the development of human consciousness.

From the big bang theory to how human thought evolved, you'll be taken on a journey through the scientific inquiries that have sought to find answers to our most profound questions.

You'll also learn

  * what the origin of the universe has to do with a turtle and an egg;

  * why we humans are unique; and

  * that belief systems developed in farming communities some 10,000 years ago.

### 2. Various cultures have different origin myths; science explains the emergence of the universe with the big bang. 

What crosses your mind when you think of the origins of human beings? For the majority of humankind, explanations of our genesis come from popular origin myths.

For example, 63 percent of Americans believe that what is written in the Bible is the word of God — that is, literally true — and the great majority of the world's 1.6 billion Muslims believe in the absolute truth of the Quran.

Throughout history, people have developed these myths to tell the story of how the universe and humankind came to be. Many of them describe a chaotic environment, often involving water, from which a god emerges and creates the world.

We see this in the ancient Egyptian myth of _Heliopolis_ : From the primordial watery abyss known as Nu, the god Atum arose, and from his seed the world was brought into existence.

Other myths found throughout Asia tell of a pre-existing animal, such as a turtle, that dives into the primordial waters to emerge with a piece of earth that later expands to form the world.

In other parts of Asia, India, Europe and the Pacific a symbolic egg is the source of all creation.

Very few of these myths tell an origin story of something being created out of nothing. Yet, this is the explanation that is currently favored by science.

This explanation is known as the big bang theory, and it holds that the universe, including all of space, time, energy and matter, exploded into existence from a single point of extreme density and temperature about 13.7 billion years ago. Eventually, this expanded and cooled into the universe we know today.

Scientists have found evidence of an expanding universe in what is known as the _redshift_. In 1929, astronomer Edwin Hubble discovered that when a source of light, such as a distant galaxy, moves away from an observer, it shifts further toward the red end of the color spectrum. And this is exactly what scientists are seeing when they are observing distant objects

However, there are gaps in the big bang theory. We'll take a look at these in the next blink.

### 3. The big bang theory fails to fully explain the emergence of the universe. 

Though the big-bang model has been around since the 1920s, it's still incomplete, failing to explain several observational and theoretical problems.

For instance, even the redshift evidence has holes in it.

Several renowned astronomers believe that, in addition to the expansion of the universe, there are many other explanations for the observed redshift in distant celestial bodies.

Another problem with the big bang theory is an underlying assumption: that the universe expanded within 1/10³⁵ of a second, which means that a point with a diameter of 1/10³³ of a centimeter supposedly expanded to more than 10 billion orders of magnitude greater than the size of the universe we observe today.

To achieve such an increase in size means that the universe expanded faster than the speed of light, contradicting Einstein's theory of relativity, which assumes that nothing can travel faster than light.

But perhaps the biggest theoretical problem facing the big bang theory is the question of where matter and energy came from.

There are two points to the issue: first, Einstein's theory of relativity also states that the amount of matter and energy in the universe should be equivalent; second, nineteenth century physicist James Joule's conservation of energy principle states that energy can neither be created nor destroyed, which means that the universe must contain the same amount of energy as when it was created.

But according to the big bang theory, the universe was created from nothing, so the total energy of the universe must be zero! This, of course, contradicts our observations.

So, as we can see, there are still unanswered questions about the origins of our universe. In the next blink, we'll look at the questions surrounding the origins of life.

> "_Neither science nor reasoning offers a convincing explanation of the origin and form of the universe... I think it most likely that it is beyond their ability to do so."_

### 4. There are six conditions necessary for life to emerge, making Earth a rare place in the universe. 

For ages, humans have wondered if life exists on other planets or in far away galaxies, a question that remains open to this day.

However, scientists have determined that there are six conditions necessary for the existence of life.

First: The essential elements that form complex molecules must be present.

In this case, carbon is the only element capable of forming complex, life-producing molecules. The presence of water in its liquid form is also considered essential.

Second: The size and mass of a planet is vitally important.

If a planet is too small, its gravitational force will not be strong enough to keep water on its surface or create an atmosphere that keeps in gasses. However, if a planet is too heavy, it would capture too much gas and become inhospitable.

Third: The temperature has to be just right.

If a planet is too hot, the temperature will break the bonds that form complex molecules. But if it is too cold, the metabolic reactions needed to create life will take too long to form.

Fourth: A planet must have a source of energy, such as a sun, to produce the appropriate temperature essential for both creating and maintaining life.

Fifth: The planet needs protection, such as an atmosphere, to keep out hazardous ultraviolet radiation.

Sixth: Finally, all of these conditions must remain stable for a period long enough to allow organisms to emerge from the complex molecules.

These six fundamental conditions are met on Earth, which makes it a unique place in the universe.

Since Galileo realized that the Earth was not the center of the universe and that many planets orbit the Sun, it was assumed that extraterrestrial life may exist on one of these planets.

However, scientists have come to discover that there are very few places in the universe that meet these necessary conditions for the creation of life.

### 5. Science struggles to explain what life is and how it emerged on Earth. 

To most of us, life seems self-evident: The cat that rubs against your leg is alive, the piece of toast on your plate is not. But defining what life _is_ — what distinguishes the living from the nonliving — is actually very difficult.

In fact, science is still struggling to define life.

Both scientists and philosophers find it difficult to agree on what the defining characteristics of life are. However, there are six characteristics that are invariably mentioned: _reproduction_, _evolution_, _sensitivity_, _metabolism_, _organization_ and _complexity_.

To further complicate matters, in 2004, the British science writer Philip Ball claimed that trying to define life is pointless. He suggests that there are no boundaries between what is alive and what is not.

To help make his case, Ball points to viruses. Even though they reproduce, evolve and are organized and complex, they are inactive outside a living cell. They only become active within a useful host cell, where they can take over the cell's metabolic machinery. This raises the question: is a virus a living thing or not?

Science also struggles to explain how life emerged on Earth.

Current estimates suggest life emerged on Earth around 3.5 billion years ago. This is the point to which all life (we think) can be traced — to a single-celled _last universal common ancestor_ (LUCA).

However, scientists still struggle to accurately explain how life first emerged from elements and molecules and answer the question: How did inanimate matter become life?

One popular theory is that life emerged from a _primordial soup_, a combination of elements that took energy from sunlight and formed organic compounds, eventually yielding a new kind of molecule that was able to replicate itself.

Still, scientists have no exact idea how even a single-celled organism could emerge from such a primordial soup on Earth.

Again, there is much we still don't know for certain, but in the next blink we'll move on to how life evolved on Earth.

> _"Then, what is life? I cried."_ \- Percy Bysshe Shelley

### 6. There is great evidence for biological evolution, and yet there are holes in Darwin’s theory of natural selection. 

So, how did we evolve from single-celled organisms into creatures capable of enjoying this blink? The best explanation that science currently has is _biological evolution_ — a series of changes within an organism that results in a new species.

Charles Darwin first proposed that biological evolution occurs through _natural selection_, a process where the strongest individuals and organisms in any given population have the best chance of survival and get the most mates, thereby passing on their characteristics to the next generation.

In fact, there is extremely compelling evidence for biological evolution.

Fossil records reveal skeletons that trace the evolution of an early horse-like animal to the modern horse.

Further evidence can be found by looking at living species. While the wings of penguins are useless for flying, their design suggests that they are the evolutionary remnants of wings their ancestors _did_ use to fly.

Biochemistry also provides an abundance of evidence: all plants, animals and bacteria consist of chemicals that are structured and react either the same or in very similar ways.

Then there is genetic analysis, which reveals that all existing life forms share around 100 of the same genes.

However, while support for biological evolution is strong, there are problems with Darwin's claim that the source of evolution lies solely in natural selection.

Modern science has revealed that, in addition to natural selection, there are other causes of biological evolution that can lead to the inheritance of acquired characteristics.

For example, it has become increasingly clear that environmental factors, such as diet and stress, can produce traits in all animals and humans that are transmitted to the offspring without any genetic change taking place.

At Tel Aviv University, theoretical geneticists Eva Jablonka and Gal Raz have compiled examples of characteristics acquired through non-genetic inheritance. Their work is further evidence that suggests the presence of factors beyond natural selection that must be considered when it comes to biological evolution.

> _"Darwin has become a secular saint and, like his religious counterparts, he has been mythologized."_

### 7. Humans are unique due to our reflective consciousness and learning behavior. 

It's difficult to define exactly what life is, and it's equally hard to pinpoint precisely what differentiates human beings from other living organisms. Scientists have pointed to particular traits — such as our unique way of walking on two feet and our artistic endeavors — but there are other things to consider.

Perhaps humankind's truly unique trait is the capacity for reflective consciousness.

Having consciousness means being aware of ourselves, our environment and other organisms, and then reacting to these thoughts. We share these abilities with other animals; however, these other animals don't have _reflective consciousness_ — that is, they aren't conscious of their own consciousness.

Humans can uniquely express this trait with questions such as: _What are we?_ and _Where did we come from?_ These questions have led to the development of systems that seek to provide answers, such as religion, philosophy and science.

But it doesn't stop there. Reflective consciousness has also led to uniquely human traits such reasoning, insight, imagination, creativity, abstraction and morality.

Another radical difference between humans and other animals is the way we learn. For instance, primates learn mainly by copying the actions of their parents. On the other hand, humans only rely on parents for the first five or so years before the education is taken over by schools, universities and books. And rather than only being taught survival skills like foraging, hunting and tool-making, humans learn a wide variety of other skills, such as art, philosophy and science.

Furthermore, humans also teach themselves by creating valuable resources and databases such as libraries and the internet. This kind of behavior certainly makes humans unique among other animals.

But how exactly did we gain this unique consciousness?

> _"An ability to recognize a reflection of oneself in a mirror is something quite different from a consciousness that reflects on itself and its place in the universe."_

### 8. The evolution of human thought is divided into three phases. 

Of course, humans didn't suddenly wake up one day with reflective consciousness; it developed gradually over the course of 2.5 million years.

And this process was initially at odds with the powerful survival instincts that we'd inherited from our prehuman ancestors.

The first phase in the evolution of human thought started around 10,000 years ago. During this phase, called _primeval thinking_, we began to self-reflect and to consider our relationship to the rest of the universe. Survival, however, was the main consideration, and all thought was guided by superstition.

With primeval thinking, nomadic hunter-gatherers settled into farming communities where they invented and developed writing. They also developed belief systems and early religions that came from a combination of imagination, a fear of the unknown and an inability to understand natural phenomena.

The second phase began around 3,000 years ago with the rise of _philosophical thinking_. This is when thought branched off from superstition, marking the advent of philosophy — the contemplation human behavior as well as the essence and causes of things.

Philosophical thinking is also defined by a desire to seek explanations that don't involve imagined spirits or humanoid gods.

The earliest known examples of philosophical thinking are contained in the _Indian Upanishads_, which is where we find the foundation for the philosophical concepts of Hinduism.

The third phase in the development of human thought brought us _scientific thinking_. It began around 500 years ago, when knowledge was obtained through scientific analysis as opposed to either philosophical speculation or beliefs based on supernatural revelation.

This is when we began to understand and explain natural phenomena by using systematic — preferably measurable — observations and experiments.

Scientific thinking resulted in a remarkable growth of empirical knowledge; this was aided by increasingly accessible education, which led to the training of even more scientists.

### 9. Scientific insight is limited. 

So far, we've come across many questions that science still can't explain. But science's failure to provide an answer today doesn't entail another failure tomorrow. However, scientific insight _does_ seem limited — and there may be questions that it simply can't explain.

First of all, the domain of science itself has limitations, especially when it comes to observation and measurement.

Take Einstein's theory of relativity, for example. If it is correct, and nothing can travel faster than the speed of light, then we'll never be able to observe anything past the distance traveled at light speed from the beginning of the universe. This limitation is known as the _particle horizon_.

Scientific data is also limited, much of it forever lost to time.

This is the case with the many fossil records that were destroyed by the movement of rocks over time, making it almost certain that we will never obtain evidence of the first life forms on Earth.

Currently, there are also many theories that are simply untestable.

Some scientific explanations of how the universe evolved involve the existence of other universes that are unreachable. And if we have no means of contact, we can't test these theories through the usual means of scientific observation and experiment. Therefore, we're faced with theories that lie outside the current limits of science.

These limitations even extend to natural sciences. While the laws of physics and chemistry may be able to explain and predict many natural phenomena, science cannot explain the essence of what we experience.

For example, science is unable to fully explain the laws of gravity. Sir Isaac Newton himself believed that gravity was the creation of God.

But science can't be blamed for failing to answer these questions. After all, it's possible there may simply be a limitation to the reach of the human mind.

> _"…there are things outside this limited domain of science that we need to know in order to understand what we are and where we came from."_

### 10. Final summary 

The key message in this book:

**Science struggles to explain the fundamental questions of life. How did the universe begin and how did human life evolve? Scientists have only been able to develop theories that try to explain these phenomena. We may one day be able to answer these fundamental questions, but, for now, it seems there may be limits to the scientific method.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _A Brief History of Time_** **by Stephen Hawking**

_A Brief History of Time_ takes a look at both the history of scientific theory and the ideas that form our understanding of the universe today. From big bangs and black holes to the smallest particles in the universe, Hawking offers a clear overview of both the history of the universe and the complex science behind it, all presented in a way that even readers who are being introduced to these ideas for the first time will understand.
---

### John Hands

John Hands, an author, is a Tutor at Open University who's been studying scientific theories on human evolution and the origin of the universe for over a decade. He graduated with a degree in chemistry from the University of London and has written three novels.

