---
id: 5447a23e3033610008960000
slug: cracked-en
published_date: 2014-10-24T00:00:00.000+00:00
author: James Davies
title: Cracked
subtitle: Why Psychiatry Is Doing More Harm than Good
main_color: E5352E
text_color: 99231F
---

# Cracked

_Why Psychiatry Is Doing More Harm than Good_

**James Davies**

_Cracked_ gives a clear and detailed overview of the current crisis in psychiatric science: malfunctioning scientific standards and the powerful influence of pharmaceutical companies have caused the overdiagnosis and overmedication of people all over the world.

---
### 1. What’s in it for me? Gain a glimpse into the corrupt world of psychiatric medicine. 

Despite its expensive research facilities and highly qualified doctors, psychiatric medicine is more like a religion than a science. With the help of huge pharmaceutical companies and their advertising dollars, psychiatry has become an unquestionable dogma.

In fact, psychiatry is rife with strange approaches and assumptions. Of these, few are based on the most current, objective scientific data, while many are directly harmful to our perception of mental health patients — not to mention the patients themselves.

In these blinks, you'll learn how mental health services got where they are today by examining the preferences of psychiatrists and mental health institutions — and by simply following the money.

Luckily, there are also things that we as a society can do to turn psychiatry back into something that benefits everyone, not just pharmaceutical companies.

After reading these blinks, you'll know

  * why blue pills will cause you to act differently than red pills;

  * why homosexuality was removed from the most famous psychiatry handbook; and

  * why you're more likely to be diagnosed with ADHD if you're the youngest kid in your class.

### 2. Discrepancies in diagnosis are a major problem for psychiatrists, even with a new manual. 

Let's start with a simple question: If you have a mental health issue, how can you be sure a psychiatrist will diagnose it correctly?

Basically, you can't. The process of diagnosing psychiatric conditions is more subjective than we might think, depending heavily on the whims and even location of the psychiatrist. One study conducted by Dr. David Rosenhan demonstrates the consequences of these highly individual diagnoses.

He sent a group of eight academics to different hospitals, each of them with the complaint that they heard a voice in their heads saying the word "thud," but were otherwise behaving normally. All of them were diagnosed with various mental disorders and prescribed strong antipsychotic pills. Most were held in hospitals for weeks, even after explaining that they were part of a study!

Furthermore, statistics show that psychiatrists across the world only share a consensus on patient diagnoses for the same patient 32 to 42 percent of the time. However, there are regional similarities in diagnoses. For example, when we compare trends in psychiatric diagnoses across countries, we find that psychiatrists from the US and Russia are twice as likely to diagnose their patients as schizophrenic than their colleagues in Europe.

These discrepancies arise despite the fact that psychologists have already standardized the diagnosis of mental disorders in the _Diagnostic and Statistical Manual of Mental Disorders_, or the DSM.

The DSM contains all psychiatric disorders known in modern medicine and has been continually edited and refined since its first publication under the title DSM-I in 1952.

However, the standards found in the DSM are based on the consensus of psychiatrists, not on scientific research. For example, homosexuality used to be categorized as a mental disease in DSM-II. This all changed during the gay rights movement in the 1970s, during which psychiatrists voted to remove homosexuality from the manual's third edition, DSM-III.

Unlike other medical sciences, where doctors can agree if a patient's leg is broken based on the available evidence, e.g., an X-ray, psychiatrists cannot diagnose patients in a way that enjoys a unanimous consensus.

### 3. The development of the DSM involves the violation of basic scientific rules. 

Knowing that psychiatrists use a standard manual for their diagnoses, you can't help but wonder how such a manual is created. When compiling the DSM, a small group of psychiatrists known as the _Taskforce_ is responsible for determining what makes it into the manual and what doesn't.

It's bad enough that the manual's content is voted upon — voting in and of itself is not part of objective science — but it makes it even worse that this democratic consensus includes so few people.

Scientific assumptions are supposed to be supported with evidence: there is no room for personal beliefs, opinions, hopes or prejudices, which can skew objective analysis. Unfortunately, this evidence-based approach is absent from the DSM Taskforce's decision-making process. Rather, decisions are based more on personal preferences and biased towards the loud or assertive personalities within the Taskforce.

Renee Garfinkel, a psychologist who participated in two DSM advisory committees, even described the process as resembling group of friends trying to decide where they want to go for dinner!

Not only is the decision-making process entirely subjective but the research methods that underpin the diagnosis of mental disorders are also full of methodological errors.

Consider that self-defeating personality disorder, SDPD, can be found in the DSM. The decision for its inclusion was based on merely two scientific articles, both of which contained many methodological flaws, and a questionnaire where psychiatrists had to answer whether they thought that SPDP should be included in the new DSM — to which only 11 percent answered "yes."

Despite the methodological weakness of the research and the underwhelming response from psychiatrists, DSM Taskforce leader Dr. Robert Spitzer forced SDPD's inclusion because he himself was convinced that it was the right thing to do. He later admitted in an interview with the author that the research was insufficient to justify inclusion of new disorders in the DSM-V.

Considering how flimsy the DSM appears to be, it's a wonder it's still _the_ standard for cataloguing and diagnosing psychiatric disorders.

### 4. Integration of new mental disorders and misuse of the DSM is leading to medicalization of people. 

One result of the subjective way the DSM is compiled has been a huge change in psychiatric diagnoses. The inclusion of new mental disorders into the DSM has led to a rapid increase in the number of diagnosed mental illnesses — a phenomenon called _medicalization_.

Just look at the statistics: about 26.2 percent of all Americans suffer from at least one disorder outlined in the DSM in a given year. Contrast this with the rates from the 1950s, which were around 1 percent and even as low as 0.1 percent in the early twentieth century.

The more disorders you include in the manual and the more lenient their definitions, the easier it is to diagnose healthy people as having a mental illness.

Moreover, psychiatrists don't look at the wider context when making a diagnosis, and instead merely reference the manual's _set of symptoms_.

Consider a study of Canadian schoolchildren published in March 2011, which showed that children born at the end of year were more likely to have ADHD (attention deficit hyperactivity disorder) than those born in January. Why?

Due to cut-off dates for enrolling children at school, children born at the end of the year are the youngest in their class. Their "immature" behavior in comparison to their older classmates is then misinterpreted as being symptomatic of ADHD.

Unsurprisingly, the explosion of "sick" people correlates with an explosion of treatment. And the most common treatment for mental health issues is to administer drugs.

Antidepressants, for example, are prescribed excessively — without any evidence that supports their efficacy. When professor of psychology Irving Kirsch examined various clinical studies dealing with success rates of antidepressants against one another, he found that antidepressants weren't more effective than placebos, i.e., sugar pills.

Yet the prescription and usage rates for antidepressants are growing faster every year. Americans consume three times more antidepressants than they did in 1986, reaching 235 million prescriptions in 2010.

The following blinks will take a look into the inner workings of the companies that produce the drugs to combat the diagnoses: Big Pharma.

> _"By lowering the diagnostic thresholds for warranting a diagnosis more people may be unnecessarily branded mentally unwell."_

### 5. Big Pharma uses the “chemical imbalance” theory to justify medicalization. 

In addition to the problems with the DSM and inconsistent psychiatric diagnoses, psychiatric medicine is also influenced by the pharmaceutical industry, sometimes called _Big Pharma_.

In the 1990s, pharmaceutical companies were forced to reinvent the image of mental health drugs. Previously, people saw them as mind-altering drugs, not legitimate medical products. To combat this, they promoted something called the _chemical imbalance_ theory in order to boost their drug sales.

This model attempts to explain all psychiatric problems as being a mismatch or imbalance of neurotransmitters in your brain, i.e., a chemical imbalance.

For example, _GlaxoSmithKline's_ anti-smoking drug _Zyban_ was marketed using the slogan "Zyban: Helping Smokers Quit Neurochemically," suggesting that the drug affects the part of the brain that is addicted to cigarettes.

Indeed, we're often told that antidepressants — more specifically, serotonin-specific reuptake inhibitors (SSRIs) — manipulate the activity of our neurotransmitters and in doing so should "correct" our chemical imbalance.

But if we want to understand today's psychiatric drugs, it's important to understand the construction of the brain's communication structures.

Brain communication works by way of _neurotransmitters_ that transmit information from one neuron to another. After they've transmitted the information, these transmitters go back into their neuron and wait for the next transmission.

SSRIs work by manipulating the neurotransmitters' return trip: they stay longer in the synapses and activate the neurons for longer periods of time, thereby correcting the imbalance suggested by the theory.

The chemical imbalance theory suggests that depressed people have lower levels of neurotransmitter serotonin. According to the theory, taking antidepressants such as _Prozac_ would raise the level of serotonin in the synapses and thus stabilize the imbalance (and the mental state of the patient).

But as you'll soon discover, this isn't necessarily true.

### 6. The chemical imbalance theory isn’t very conclusive. 

The chemical imbalance theory does everything it can to reduce psychiatric disorders to neatly identifiable causes and effects. That's what the pharma companies want you to think.

And despite all the heavy marketing in the pharma industry, the chemical imbalance theory has some serious flaws.

For starters, no studies can definitively prove that we can accurately measure chemical imbalances in the brains of depressed patients. Because the results of research in this area are so contradictory, we can't say with certainty that people living with depression have too much or too little of any particular neurotransmitter.

Dr. David Kaiser from _Psychiatric Times_ put it best: "Patients have been diagnosed with chemical imbalances, despite that no test exists to support such claim, and that there is no real conception of what a correct chemical balance would look like."

Moreover, biological causes alone are insufficient in explaining the emergence of mental disorders.

According to former president of the American Psychiatric Association Carol Bernstein, "We still don't know the relationship between biology and the mental disorders."

Take the long-lasting lawsuit between David Oaks and the _American Psychological Association_ (APA). The APA mistakenly diagnosed Oaks with schizophrenia in the 1970s, prescribed him strong antipsychotics and held him in psychiatric ward. While the APA initially alleged that the use of antipsychotics was necessary due to the biological nature of mental illness, they conceded in 2003 that there were no "discernible pathological lesions or genetic abnormalities" in mental disorders.

In its report entitled "Facts and Figures Conquering Depression," the World Health Organization also recognizes that the causes of psychiatric disorders are varied. Indeed, depression can manifest itself in different psychological, sociological and biological factors.

Although there are disorders, like Huntington's disease, caused by genetic factors, such diseases are exceptional. History of depression in a family likely has more to do with the family's environment than with biological causes.

But the pharmaceutical industry can't profit from explanations based on environmental factors, so they continue to pedal the concept of the chemical imbalance.

### 7. Psychiatry’s approach to drugs, especially antidepressants, is fatally flawed. 

Antidepressants don't work as smoothly as the chemical imbalance theory suggests. They don't reset your brain — they change it.

Consider a recent study undertaken at Oxford University, which showed that most SSRI antidepressants don't _just_ correct negative emotions: people taking SSRI antidepressants reported a _general_ reduction of emotions, especially positive emotions like happiness, love or excitement.

In addition, they felt emotionally separated from their surroundings and cared less about important things in their daily routine. They felt like their personality had changed; like they were behaving differently.

In this way, antidepressants appear analogous to alcohol consumption: while many people drink to drown their problems, it actually doesn't help them in the long run. It merely offers people a temporarily altered state of mind, allowing them to think less about their problems without actually solving them.

Moreover, a drug's efficacy depends heavily on our individual expectations and beliefs. The following study exemplified this: a group of students was instructed to take two different pills, one blue and one red. One was a tranquilizer and the other a stimulant, but the students didn't know which was which.

When they reported the effects of the drugs, the majority concluded that the red pill was the stimulant and the blue pill the tranquilizer. In actuality, both pills had the same contents: sugar.

So what was the difference? The participants perceived the color red as stimulating and the color blue as relaxing, and these perceptions influenced their experiences.

Interestingly, this has implications for drug advertisements. Regardless of whether their drugs work, drug companies want to produce as much of a placebo effect as possible. In fact, according to a major pharmaceutical company consultant, drug companies' advertisements are all about generating certain expectations in order to boost the placebo effect rather than informing consumers of a drug's intended effects.

### 8. The pharmaceutical industry’s coffers are as deep as its manipulative powers are wide. 

Here's a serious question: How can you explain the high rates of psychiatric drug prescriptions when there's no conclusive evidence that they actually work? The answer is pretty grim.

For starters, pharmaceutical companies suppress certain research results in order to let the positive ones shine. For example, drug producer _GlaxoSmithKline_ was fined $2.5 million in 2004 for defrauding customers through their dishonest research. How? They actively didn't publish studies that had mixed or negative results: some of the studies had shown that its antidepressant _Paxil/Seroxat_ was less effective than placebos.

Second, Big Pharma's massive funding efforts for universities and researchers gives their academic and social reputation a huge boost. We can easily see this influence in action by taking a look at statistics.

In the UK, nearly 90 percent of all psychiatric research is funded by pharma companies. These companies look to influence _key opinion leaders_, such as professors or doctors, who are influential within their industries.

On top of that, 56 percent of the Taskforce members who compiled the DSM-IV had financial ties to drug companies. Of those, 88 percent were responsible for diagnostic categories, such as mood disorders, where drugs were the primary method for intervention.

Finally, Big Pharma focuses on advertising rather than hard research to guarantee success. The numbers are astounding: in the mid-2000s, psychiatric drug companies spent £1.65 billion promoting their drugs in the UK alone.

And, according to a former editor of the _British Medical Journal_, the number of people working in pill promotion has increased by 59 percent between 1994 and 2004, while jobs doing actual drug research have seen a decline.

So how do we disentangle money from medicine? The Obama administration's _Physician Payment Sunshine Act_ took one step in the right direction in 2013, requiring all drug-related companies to publicize their university and research funding. However, this kind of transparency is rare: the government in the UK makes no effort at transparency in the pharmaceutical industry whatsoever.

> _"The things that get powerful institutions to change don't usually come from inside those institutions. They usually come from outside_. _"_

### 9. Psychiatry uses dangerous social myths to answer our health-related questions. 

Throughout human history, philosophers have pondered questions on the meaning of life and existence after death. In the process of posing and answering these questions, they contributed to _social myths_, or the prevailing ideas of their time.

We create our own social myths regarding our health when we ask: "Why do we have health issues?" and "How can we be cured?"

Although psychiatric disorders can be explained in many ways, today's psychiatry limits its focus to biological explanations.

This contrasts the _bio-psycho-social_ model of mental suffering found in psychology, which suggests that there are biological (i.e., from genetics), psychological (i.e., from trauma) and social (i.e., environmental) causes for psychological pain.

Scientists and therapists should therefore analyze and treat _each_ of these causes in order to give the patient optimal treatment. Unfortunately, however, a _bio-bio-bio_ model, which focuses only on the biological causes for mental disorders, is gaining in popularity.

We see this in the rising rate of pill prescriptions, the rarity of social interventions and the decline of long-term psychotherapy.

The same as in ancient times, when social myths become accepted, people no longer question the legitimacy of those explanations. This has profound effects on those who've received mental health diagnoses.

When a psychiatric diagnosis labels people as "sick," this changes the way society sees and treats them.

For example, a study at Auburn University revealed that people believed to have brain disorders were treated more harshly than those thought to have sociological or psychological disorders. Why? Probably because of the focus on "sickness" regarding biologically induced mental health disorders.

### 10. Western psychiatry is exporting its harmful mental health practices into new markets worldwide. 

As we can reasonably expect from any money-hungry cartel, Big Pharma is expanding its reach across the globe by intervening in the way other countries conduct psychiatric treatments.

Every country has an individual culture and a _symptom pool_ that generates certain culture-specific disorders. For example, some people in southwest Asia experience _koro_, the feeling that their genitals are retracting into their body, while people in Canada don't.

However, in the last few decades, pharma companies have started promoting and exporting their drugs to other countries, changing the symptom pool in those places in the process. As a result, "non-Western" countries have shown an epidemic increase in typically Western mental disorders.

For instance, in Hong Kong, _anorexia nervosa_ wasn't an issue until 1994, when a 14-year-old girl starved herself to death. Consequently, there was huge media coverage on this "new" disorder, which was formerly known to occur primarily in Western countries.

Afterwards, there was an increase in anorexia nervosa rates in Hong Kong reaching near epidemic proportions. Why? Most likely, merely being aware of this "new" disorder gave people a new way to express the suffering they'd already felt.

Cases like Hong Kong gave pharmaceutical companies a good reason to start collecting culture-specific notions of mental disorders so they could adapt their advertisements accordingly.

For example, the Argentine pharmaceutical company _Gador_ was in competition with multinational drug producer _Eli Lilly_ when the Argentine economy collapsed in the 1990s. Gador won the market using an advertisement that connected Argentinians' main complaint, globalization, to depression via images of downtrodden people from across the world. As a result, their brand of antidepressants became the most successful.

Today, drug companies are inviting researchers from all over the world to answer the question: "How can we get the people to think that their problems will be solved just by taking our pill?"

> _"If drug companies were non-profit-making industries … we might well see dubious research methods disappearing overnight."_

### 11. Psychiatry has to prove its scientific legitimacy by changing its paradigm. 

After reading these blinks, it might be difficult to imagine making an appointment with a psychiatrist ever again. But don't write off psychiatry just yet! Some psychiatrists are working towards a paradigm shift that would benefit us all.

One part of that shift includes more appropriate means of treatment based on a patient-centered attitude and in which psychiatrists put together individualized treatments.

This kind of treatment plan is not without precedent. As Dr. Sami Tamimi of the _National Health Service_ has shown, a _non-diagnostic_ approach in ADHD treatment has proven much more effective than other kinds.

In this case, "non-diagnostic" means that psychiatrists abandoned the formulaic "diagnosis XY = drug XY" approach in favor of considering the social context in which the disorder evolved. Although the patients treated with the non-diagnostic approach were also given psychiatric drugs, they had much better rehabilitation rates and were hospitalized less often than diagnostic-treated patients.

Dr. Tamimi's example shows that changing psychiatry for the better isn't about condemning psychiatric drugs, but recreating a balance in psychiatric treatment. Diagnostic manuals and drugs should be secondary aspects of a broader approach, whereas focusing on meaning and context and prioritizing relationships with people should be the main objective.

But there also are other ways in which psychiatrists can overturn the current paradigm:

  * First, psychiatry needs to become more "modest" as a science and admit that there are things it can't yet explain.

  * Next, we need better regulations regarding the relationships between researchers and pharma companies.

  * Finally, students of psychiatry should be taught to levy their own healthy critique towards their chosen discipline.

According to the author, these three propositions are very unlikely to happen. But there is a final proposition which might be more attainable.

We must enlighten the public to the dilemmas in psychiatry, as this could be a vital force in changing the existing diagnostic methods. When people recognize the unique character of mental disorders and the dishonesty in Big Pharma's marketing strategies, they will seek better alternatives to what is currently available to them.

> _"We … should start seeing the non-medical approach as the real work of psychiatry." — Dr. Pat Bracken_

### 12. Final summary 

The key message in this book:

**Psychiatric diagnoses are far more subjective and less scientific than most people would believe. Flaws in psychiatric standards, the interests of extremely wealthy industries, and an unwillingness to adopt a healthy skepticism towards psychiatry have all led to the overdiagnosis and overmedication of people today.**

Actionable advice:

**Think twice before taking pills to solve your mental problems.**

"Mental disorders" aren't as simple as pharmaceutical companies and mental health institutions would have you believe. In fact, psychiatric problems are far more complicated than simple chemical imbalances in need of minor adjustments. In most cases, it's not _you_ who will profit from a prescription, but Big Pharma.

**Suggested** **further** **reading:** ** _Psychobabble_** **by Stephen Briers**

_Psychobabble_ explores the most popular promises of the self-help industry and the tenets upon which they are founded. The blinks skillfully demonstrate that when it comes to the human mind, things aren't so easily reduced to catchy self-help mantras and lucid pop-psychology diagrams as the industry would have us believe.
---

### James Davies

James Davies is a qualified psychotherapist with a Ph.D. in medical and social anthropology. In addition to writing for numerous publications, such as _The Times_, _Daily Mail_ and _The Guardian_, he has also published three academic texts with _Stanford University Press_, _Karnac_ and _Routledge_.

