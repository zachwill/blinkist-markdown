---
id: 559261ca6561380007ad0000
slug: cradle-to-cradle-en
published_date: 2015-07-03T00:00:00.000+00:00
author: William McDonough and Michael Braungart
title: Cradle to Cradle
subtitle: Remaking the Way We Make Things
main_color: A4AF25
text_color: 5D6315
---

# Cradle to Cradle

_Remaking the Way We Make Things_

**William McDonough and Michael Braungart**

_Cradle to Cradle_ (2009) exposes the fundamental flaws of manufacturing and the damage it inflicts upon our environment, even as we attempt to be eco-friendly. These blinks also introduce you to ways in which you can make a positive impact on the planet, and guide you through the process of rethinking your business in order to become eco-efficient.

---
### 1. What’s in it for me? Find out why industry is so messed up today, and how we can fix it. 

When was the last time you tore a T-shirt, cracked your iPhone, or ripped the sole of your shoe and paid for it to be repaired? Chances are, you just bought a replacement instead.

Why? Because that's how poorly your T-shirt, iPhone or shoe was made. Essentially, most of the products we manufacture and use on a daily basis are designed to be destroyed. Whether it's clothes, houses, food or manufacturing, industry creates products that have one common destiny: the trash.

And it's not just the quality of the designs, it's also what they're made of. As these blinks show, too many products are made from materials that are extremely difficult to put safely back into the environment.

The good news is that there are a few ways we can reorient industry and manufacturing to be more efficient and sustainable.

In these blinks, you'll learn

  * why we make soap in the laziest way possible;

  * how you can balance profits with environmental awareness; and

  * a can't-lose business idea for sustainable TV rentals.

### 2. Industry is fundamentally damaging to the environment. 

During the Industrial Revolution, man strove to achieve production that was ever more efficient and profitable. Who considered the ecological consequences of this? Nobody. It was widely believed that humans had access to an endless supply of resources from Mother Earth, that the environment was a bottomless provider that simply wouldn't degrade over time.

Today, industry still functions as a _linear system_. It goes one way, from the producer to the consumer to the garbage, without anything going back to nature. Think about it: What do you own that _isn't_ designed to be thrown away when you're finished with it?

In this way, industrial production relies on the _cradle-to-grave model_, where resources are extracted, shaped into products, sold and eventually disposed of in a "grave" of some kind. Just consider how it's often more expensive to repair a damaged item, like the broken shoe, than to buy a new one.

Corporations also design products and processes for worldwide use with a _one-size-fits-all_ approach. For example, people in places in the USA with soft water, like the Northwest, only need small amounts of detergent to do their laundry, while in the Southwest, where there is hard water, more detergent is needed. However, major soap manufacturers choose to save money by producing a strong detergent that caters to the hardest water, despite the environmental damage this creates.

Further examples of the way humans take from nature on a massive scale include mining, burning fossil fuels and land-clearing for _monocultural_ agricultural properties, where natural diversity is destroyed to make way for a single crop that's cultivated for our consumption. At every level, our industry is built in a way that reflects the idea that nature is not something we work _with_, but rather something that exists for our use, and this fundamental principle wreaks incredible damage.

But we've made progress, haven't we? We're aware that our environment needs taking care of. However, our industries have yet to approach the problem of pollution with the effort that it really requires. Find out more in the next blink.

### 3. Reducing, reusing and recycling just aren’t enough to make an ecological difference. 

Industry leaders realized they had to take some steps to prevent serious ecological consequences. The result? A new concept called _eco-efficiency._

Eco-efficiency means getting the most output from the power, material and time that is put into production, while also generating less waste and pollution. That's what led to the _3R trend_ : _reduce_, _reuse_, _recycle_. We're all familiar with the 3R's, which seem to be a smart and simple solution. Or are they? Let's dig a little deeper.

The truth about _reduction_ is that it doesn't stop environmental damage. Instead, it only slows it down, allowing the damage to take place in smaller increments over a longer period of time. What's more, even tiny amounts of dangerous emissions can be disastrous for a biological system.

During incineration, dioxins and other toxins are released. And since materials like paper or plastic were never designed to be safely burned in the first place, these toxins end up in the air, water and soil.

_Reusing_ is also a less-than-ideal approach. By reusing our waste, we simply transfer the problem from one place to another. For example, in some developing countries, sewage sludge is often converted into animal food, even though it contains unhealthy chemicals.

And what about our old friend _recycling_? It's just as problematic. When we recycle our waste, the quality of the material is reduced over time. In effect, it's not recycling that we're doing. It's _downcycling_. Downcycling occurs because we can't separate the materials that were mixed in order to make the original product, which means that the materials cannot be returned to their initial status.

For example, when the high-quality steel used in cars is recycled, it's melted down together with other parts of the car, including copper, paint and plastic. Unsurprisingly, this reduces the quality of the steel. If the materials for new products were designed specifically for their safe and effective use in a second product, then recycling would be far more effective.

### 4. Eco-efficiency and current environmental regulations aren’t feasible long-term solutions. 

It's clear that our world needs a more thorough model than the 3 R's. Instead of aiming to make our production processes less harmful, we should be striving to create an industry that is 100 percent helpful.

Eco-efficiency provides only an illusion of improvement. It's true that energy efficient buildings have drastically reduced the use of oil for heating and cooling via better insulation and leak-proof windows. But meanwhile they're actually contributing to indoor air pollution from poorly designed building materials. People need more fresh air circulating through buildings, not less!

We find another example of dodgy eco-efficiency in so-called "efficient agriculture," which drains wetlands for monocultural crops. Historically, West Germany produced twice as much wheat as East Germany, thanks to the efficiency of their agriculture practices. 

But old-fashioned agriculture is better for the environment. East Germany didn't sacrifice so many natural habitats for wheat fields, and as a result, had healthier breeding lands for storks and other species.

Many eco-efficient factories are only distributing the pollution in less obvious ways. High smokestacks, for example, might not be contaminating local areas, but they still send pollution far from its origin, so that the resulting destruction is less visible.

But what about government regulations? Surely they'd be there to ensure eco-efficiency does the job. Unfortunately, their approach is hardly helpful. The state threatens industry with punishment instead of giving incentives, leaving businessmen to see environmental protection as just another hassle.

Instead of encouraging creative problem solving, industry gets _a license to harm_ — the right to dispose an acceptable amount of damaging material that's less than was previously permitted. Regulators use large-scale solutions for processes or systems instead of focusing on the designs which cause pollution in the first place. As such, in attempting to solve mere consequences, these regulations and eco-efficiency efforts fail to tackle the root of the problem.

> _"Efficiency has no independent value: it depends on the value of the larger system of which it is a part."_

### 5. Eco-effectiveness is a better approach than eco-efficiency. 

It's time we abandon our attempts to be efficient and start being _effective_ instead. By getting rid of the product-to-waste system, we'll make room for a new product-to-product approach: the _cradle-to-cradle_ model.

_Eco-effectiveness_ entails focusing on the right kinds of products, services and systems — ones that can truly help the world. Instead of trying to control nature, we must begin to engage with it instead. This may require a complete shift in perspective and radical change in the products we take for granted today.

Consider a roof: Traditional roofs contribute to floods by not allowing fluids to pass through them; they heat up cities in summer by absorbing and re-emitting solar energy; and deplete natural habitats simply by taking up space.

But it's possible to think about roofs in a totally new way. The authors developed roofs with a layer of soil that's covered in plants. This kind of roof maintains a stable temperature to keep a home cool in summer and provide insulation in winter. Plus, it creates oxygen, captures storm water and soot, and is more attractive than normal roofs! This demonstrates how eco-effectiveness can achieve everything eco-efficiency is supposed to, and more.

Moreover, instead of making industries and systems smaller, as eco-efficiency suggests, eco-effectiveness wants to make them bigger and better. By making industries that do the "right thing" in a bigger way, we can replenish, restore and nourish the rest of the world.

So what is "the right thing?" In this case, it's all about shifting focus from the outcome of production to the system as a whole. Imagine if the currently negative side effects of industry could be turned into something positive — if all the materials manufactured in industry were biodegradable.

Then of course we'd want industry to be as big as possible. With more industry, more biodegradable materials could return to the soil and nourish it, enriching the vitality of the ecosystem. So how can we start making "the right things," as eco-effectiveness suggests? We'll explore this in the next blink.

### 6. If we redesign the way we make things, we won’t produce waste. 

The first step toward eco-effectiveness is attacking waste. But is it possible to stop producing waste altogether?

We produce waste because we don't differentiate between biological and technical materials. _Biological materials_ are those that can be safely returned to the environment. _Technical materials_ are dangerous to the environment and shouldn't be thrown out.

We turn biological materials into technical materials, rarely returning them to nature in a usable form. For example, leather shoes were originally only made of biological material, but nowadays are tanned with chromium and have a rubber sole. Toxic waste is produced during tanning, while lead and plastic from your sole permeates the soil as you walk. After usage, these materials can't be separated and the shoe is thrown away.

What if we designed our products and materials as _biological nutrients_ that can return into an ecological cycle after we've used it? Biological nutrients are made up of biological material. It's possible to formulate soaps and other liquid cleaners so that when they are washed down the drain, pass through a wetland and end up in a river, they support the balance of the ecosystem.

Products and materials can also be designed as _technical nutrients_, which only contain technical materials, so they can go back into the technical cycle. Technical nutrients are made up of technical material. So a sturdy plastic computer case made only from technical materials could be recycled over and over without losing quality, instead of being downcycled and ending as flowerpot.

Or, instead of selling a television, a company could lend people a television for a certain period of time, after that the television would return to the company, where the material would be recycled into the newest television. This way, the material wouldn't get lost.

In order to truly prosper, we need to adopt nature's cradle-to-cradle system of nutrient flow and metabolism. In this way, we'll be able to revolutionize design, so that every product begins with the knowledge that waste is always avoidable.

### 7. We should respect and use the diversity of the world. 

The way you run your business is not the way you would run it if you were based in Tokyo, right? Not every place is the same, and it's critical that we respect local differences and address environmental issues accordingly.

Your sustainability will be maximized when you make the most of what your local area has to offer. One fantastic example is a 1992 project in Rio de Janeiro, Brazil, where a model waste treatment system was opened that used locally fabricated clay pipes to carry wastewater away from the village into a settling tank. From there, the wastewater was fed into a series of smaller ponds containing a great diversity of plants, microbes, snails, fish and shrimp.

This project's use of regional materials had several benefits: local people learned to work with their local materials, the local clay was better for local water than artificially manufactured clay and extra costs and environmental damage were avoided as no material had to be shipped in.

You can also make the most of local environments in your use of energy flows. Rather than installing more large-scale power plants, try to ensure that every south-facing house roof is equipped with solar collectors that directly supply the house with energy.

The same goes for wind energy. Enormous wind farms wouldn't be so necessary if utility firms rented land from farmers and built windmills there, maximizing the use of already existing power lines.

Finally, as every region of the world has its own needs and desires, products also have to differ from region to region. African villagers who are used to drinking out of clay cups don't have a recycling structure. They need packaging that can be thrown onto the ground, where it can decompose and nourish the environment. In India, where materials and energy are costly, people would welcome packaging that is safe to burn.

### 8. Make your company eco-efficient. 

Now that you've got an insight into how industry damages the environment and what we can do about it, it's time to apply the lessons in these blinks to your product.

First, you can explore ways of eliminating the damage you do to the environment, perhaps by excluding harmful substances such as PVC, cadmium, lead and mercury.

Once you've reduced your negative impact, it's time to create a positive one. That means relying on local materials, finding solutions to waste and so on.

But with so many other factors to consider in business, where does an environmentally friendly approach fit in?

Consider it in terms of a fractal triangle comprising _equity_, _economy_ and _ecology_. Economy concerns the reality of capitalism, i.e., the question: Will the product or service make a profit? Equity refers to fairness: Are your employees earning a fair living wage? Finally, there's ecology. Are you creating too much waste? Could you utilize more renewable energy? What impact do you have on the environment around you?

Your products and processes should strike a balance between these three aspects, and combinations of them. You could have an ecology/equity sector, where you ask questions that combine those two specific issues: Are your employees being exposed to a dangerous amount of toxins? An equity/economy sector, on the other hand, might ask if men and women were paid equally.

With this triangle in mind, you'll be able to start taking steps that transform your company into an eco-efficient one.

### 9. Final summary 

The key message in this book:

**The way industry operates today is damaging at its most fundamental ideological level. However, by rethinking recycling, respecting regional differences and incorporating ecology into your organizational structure, you and your company can lead the way for change.**

**Suggested** **further** **reading:** ** _Junkyard Planet_** **by Adam Minter**

Full of visceral details and fascinating personal narratives, _Junkyard Planet_ digs into the history and current state of the waste management industry. Through a riveting tour of the sites that take care of our trash, Minter argues that the recycling and reclamation industry, despite its well-publicized environmental hazards, represents the most logical and sustainable solution to offset the insatiable consumption of the developed world.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### William McDonough and Michael Braungart

Michael Braungart is a German chemist who holds a chair in industrial ecology and Cradle to Cradle management at Erasmus University in Rotterdam. He is a visiting professor at TU Delft, The Netherlands. He was one of the founders of Germany's Green Party.

William McDonough is an American architect and founding partner of William McDonough + Partners. In 1996, he received the Presidential Award for Sustainable Development. In 1999, _Time_ recognized him as a "Hero for the Planet."

