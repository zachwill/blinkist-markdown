---
id: 547344cc3635610009600000
slug: crazy-is-a-compliment-en
published_date: 2014-11-26T00:00:00.000+00:00
author: Linda Rottenberg
title: Crazy is a Compliment
subtitle: The Power of Zigging When Everyone Else is Zagging
main_color: 59428F
text_color: 59428F
---

# Crazy is a Compliment

_The Power of Zigging When Everyone Else is Zagging_

**Linda Rottenberg**

_Crazy is a Compliment_ explains why to pursue your dreams, you need to be entrepreneurial and maybe even a little bit crazy. This mind-set, combined with the expertise provided in these blinks, can help anyone to start the company of their dreams.

---
### 1. What’s in it for me? Learn how to minimize risk as an entrepreneur and achieve your dreams. 

We all dream. Your dream might revolve around one day starting your own company, to finally achieve true independence and importantly, the freedom to pursue something you're truly passionate about.

So what's stopping you?

In short: it's scary. The risk of leaving a cushy job to take on something completely new can seem daunting. In fact, you could say you need to be a little bit crazy to take the leap.

These blinks can help you smooth the road and minimize the risks in realizing your dream. You'll gain wisdom collected from hundreds of others like you who have made this leap. You can then put this advice to good use in your venture, whether you're still in the planning stages or well on your way to world domination!

In the following blinks, you'll also discover:

  * how _Starbucks_ raised its stock price tenfold by _closing_ all its stores;

  * how running an under-four-minute mile went from impossible to easy in a few months; and

  * which entrepreneurial type you are: a Steve Jobs, a Bill Gates or a Martha Stewart.

### 2. To do the impossible, you can’t listen to your friends or family. 

When facing important life decisions, most people eagerly consult their friends, family or partners for their opinions. But in fact, this could be a big mistake.

Why?

Because you need to get rid of so-called _psychological limiters_ : the beliefs that say you are incapable of reaching your goals. These limiters arise from a basic fear of everything that's unknown and uncertain. Your family and friends can actually _exacerbate_ these limiters.

For example, if you're a bit doubtful about quitting your job and starting your own company, and then talk to your parents about it, they may be horrified as they've been accustomed to holding down the same 9-to-5 job for 50 years. Their attitude will naturally discourage you.

So what can you do to overcome psychological limiters?

For one, you need to understand that the only reason you're facing negativity is because you're pushing the limits of what's thought to be possible. This is much more difficult than staying in the realm of what's already been done.

Before 1954, everyone thought that running a mile in under four minutes was physically impossible for the human body. But then, after Roger Bannister accomplished this "impossible" feat, 16 others accomplished it too in the following three years. 

A second option in avoiding psychological limiters is simply not sharing your ideas with family and friends. Your ideas are fragile when in their infancy, and you can be thus easily discouraged by negative feedback.

And somewhat counterintuitively, the people you trust the most can be the least trustworthy when evaluating your ideas. This is because their responses are by nature emotional: people may offer negative feedback because they secretly hate you, or because they're worried about the consequences of you pursuing your idea.

Or they might applaud you, just to give you an ego boost: "Oh wow, a hotel for pet iguanas is the best idea I've ever heard!"

Both of these approaches are harmful, because they give you a warped view of the quality of your idea.

> _"If you are not called crazy when you launch something new, it means you are not thinking big enough."_

### 3. Don’t crash and burn too soon; taking small, incremental risks helps protect your early business. 

Everyone knows that starting your own company is risky. Thankfully though, it's possible to minimize your risk level so you're not left completely vulnerable.

How?

First of all, ensure a single failure can't ruin you. Never take risks that could be catastrophic for you personally if the risk is realized: don't jeopardize your ability to provide for your family, for example.

Thankfully, most entrepreneurs seem to know this: of those people the author talked to, 80 percent had saved up enough to cover their living expenses for at least a year.

Second, don't rush in, but take _smart risks_ : incremental steps where you have the chance to get feedback and adjust accordingly.

This was the approach taken by _Zappos_ founder Nick Swinmurn. He started his venture by simply walking into local shoe stores and asking if he could take pictures of the products for his online shop. If he then made a sale, he would come back to the store and buy the shoes for his customer at full price. This was a very low-risk tactic, as he could test the feasibility of his idea without having to invest in inventory of his own.

Another great way to offset risk is to take advantage of _crowdfunding_, or raising small amounts of capital from a large number of people online. The most well-known sites for this are _IndieGoGo_ and _Kickstarter_.

Using crowdfunding comes with a number of benefits.

Most importantly, it requires you to pitch your idea to a large number of people, which generates valuable market input. If no one wants to invest in your product, this is a signal that you need to change something.

Another benefit is that the pitch simultaneously functions as marketing, bringing your product to the attention of funders and their friends.

### 4. Learn to deal with chaos! Chaos brings opportunities, yet stay true to your core principles. 

When you start your own business, you can be sure that things will get chaotic — this is unavoidable. But to be successful, you must learn to deal with chaos.

One key component is _getting creative_. When things get chaotic and you feel frustrated, try to see opportunities around you: chaos can often lead to great chances for improvement.

This is what J.K. Rowling, the world-famous author of the Harry Potter books, did. In the 1980s, she was working at _Amnesty_ _International_, but her heart wasn't in it: while she was supposed to be researching human rights violations, she secretly wrote stories. While she was fired for this transgression, it turned out to be a blessing in disguise, as it freed Rowling to do what she really wanted to do — keep writing.

Amid chaos, you also need to stand your ground. Most entrepreneurs panic and totally abandon their previously well-thought out plans and start doing whatever they can to survive when chaos strikes.

Don't make this mistake. Instead, look back to your original plan and core principles, and stay true to them. They will guide you forward.

The actions of chairman and former CEO of _Starbucks_, Howard Schultz, offer a great lesson in standing one's ground. In 2008, Starbucks was in trouble, its stock value having plummeted by 50 percent. Many companies would have panicked and grasped at whatever new trends were hot at the moment, but instead Schultz decided to go back to the company's core values: great coffee and a strong barista-customer relationship.

This is why he closed all 7,100 Starbucks outlets in the United States for an entire Tuesday afternoon to retrain baristas in the "art of espresso." This risky move upset stock analysts and investors greatly, fearing a further stock slump.

But instead, Starbucks share price skyrocketed, demonstrating the importance of staying true to your core principles even in turbulent times.

> _"[Entrepreneurs] don't succumb to the agitation around them; they stay calm, recognize the opportunities that the disruption around them creates, then seek to exploit them."_

### 5. Learn from great entrepreneurs who share your entrepreneurial personality type. 

Just as people have different personalities, so too do entrepreneurs. There are several different entrepreneurial personalities, each with their own strengths and weaknesses.

Let's examine the four basic ones.

First, there are _diamonds_ : visionaries who dream big ideas that can revolutionize people's lives. Though their brilliance is unquestionable, these diamonds can often be incredibly self-centered as well. Examples of this type would be _Apple's_ Steve Jobs, _Facebook's_ Mark Zuckerberg and _Google's_ Sergey Brin.

Second, we have _stars_ : charismatic leaders who can inspire deep loyalty within a large, diverse audience. They have the potential to quickly create a global success, but unfortunately, stars often operate alone, and such a "one-man show" is prone to the whims of the star. Examples of this type are Oprah Winfrey, Martha Stewart and Jay-Z.

Third, there are _transformers_ : change catalysts who typically work in an old, potentially stagnant industry in which they aspire to transform and revitalize their company. Examples include _Starbucks'_ Howard Schultz and the founder of _McDonald's_, Ray Kroc.

Finally, there are _rocketships_ : analytical thinkers who want to make strategic efficiency improvements, making everything faster, better and cheaper. Typically, such people are great at math and science, but as entrepreneurs, they will also need to master areas like customer service and management. _Microsoft's_ Bill Gates and _Amazon's_ Jeff Bezos are notable examples of this personality type.

Now that you know the basic entrepreneurial types, you need to figure out which one you represent. Knowing this will help you understand your entrepreneurial instincts and inclinations, and this will help you be more effective. You can study well-known entrepreneurs who share your type, and try to overcome their flaws.

For example, if you're a diamond, you might look to Steve Jobs. His weakness was that he often took his employees' ideas and claimed them as his own. But since you know there's a danger of this, you can ensure you always give credit where credit is due. Similarly, looking to fellow diamonds might show you some strengths you hadn't realized you had.

### 6. For your company to thrive, you need to continuously innovate and improve. 

So you've just launched your company and you run into an issue you don't know how to deal with. Now what?

Based on research of common issues entrepreneurs face, the author and her team compiled a list of advice that can help address the most-likely obstacles you'll encounter as a new entrepreneur.

First off, _close some doors_. Life is about making choices, and it's impossible to keep all options open forever. Once your business is up and running, go ahead and quit that day job so you can focus on what's important.

Then, _fire your mother-in-law._ When you're just getting started, you might need friends and family to work for you because you can't find or afford anyone else. As soon as possible though, end this and find people who truly are best for the job.

Don't be afraid to "_minnovate."_ Rather than spending months trying to come up with a massive innovation in how you do business, seek continuous but incremental improvements.

_Be open to change, but not too open._ Data shows that start-ups that pivoted — or changed their core business idea — once or twice managed to raise two and a half times as much funding as companies that either did not pivot at all or did so more than twice.

And don't forget to _dream big, but execute small_. Many entrepreneurs are so busy plotting their world domination that they neglect to lay the more mundane groundwork for success.

And even if you're hungry to conquer the world, remember to _eat the elephant one bite at a time_. Whenever you face a dauntingly large undertaking, chop it up into smaller, more manageable chunks.

One final piece of advice is that whatever issue you're trying to solve, examine it closely and in isolation from others. This will often yield solutions to even tricky problems.

You could even consider hosting a brainstorming session where you try to list every single issue your venture has. Then you can go down the list and solve each issue one by one.

For example, if your material costs are over budget, customer service is overloaded and the office has recently been broken into, examine the first problem and put everything else out of your mind. Track every penny of your material costs and you'll surely find out what the precise problem is.

### 7. Entrepreneurs must develop their leadership skills, yet they need good mentors to do so. 

Many entrepreneurs make the mistake of thinking that once a business is off the ground, leading the day-to-day operations will be a walk in the park.

In fact, nothing could be further from the truth. To run a business well, you need to be an effective leader, which is a skill most entrepreneurs initially lack.

So how do you get started on your path to being an outstanding leader? Try to abide by these simple management principles in your company.

_Keep your organization agile_ : divide workers into small teams with individual areas of responsibility, in which they can experiment and meet daily to review progress.

_Be accessible_ : today, every leader needs to be accessible, from their closest business partners to the lowest-ranking employees. Even U.S. President Barack Obama makes himself accessible through social media platforms, like _Twitter_ and _Reddit_. Being open to the thoughts of others is also a great way of soliciting feedback, which can help you improve how you lead.

_Be self-aware_ : don't be blind to how others perceive you and adjust your leadership style accordingly. Also, admit your own flaws once in a while, so you never forget the danger they pose.

_Be authentic_ : it's okay to show your softer, more human side as a leader once in a while. Exposing vulnerability will help people relate to you better and help you forge stronger bonds.

To accelerate the process of becoming an effective leader, try to find yourself a mentor, or a more experienced leader who can give you advice. Mentors are particularly valuable because they typically never sugarcoat issues. This is why you should pay special attention to their concerns, as they may be precisely the ones your employees are too scared to bring to your attention.

Because mentors are so critical, you'll probably need a number of mentors throughout your career to get the most benefit. If a mentor grows disinterested or your expertise surpasses theirs, it's time to move on and find someone else.

### 8. Work to instill an entrepreneurial instinct in your employees as well. 

As you've seen, leadership is a crucial element in the success of your company.

But it's not enough. For your organization to function well, you need to create an environment that also makes your staff feel a sense of _employeeship_ : an entrepreneurial instinct of their own that drives them to constantly improve and innovate.

So how can you instill this kind of attitude?

First of all, it has nothing to do with money. Author Dan Pink found that most employees actually don't consider their salary to be the most important part of their compensation. Rather, employees want rewards that are impossible to place a monetary value on, such as having a job title they can be proud of and the autonomy to make their own decisions.

Second, you should also be a flexible employer, as this can translate into a serious competitive advantage. Cleaning supply company _Clorox_ was able to attract top-caliber talent by offering working mothers flexible hours to better accommodate family concerns.

Third, make your company a "culture club," where the binding element is that every member fits in perfectly within the organization's culture. This will forge strong bonds among your staff.

Naturally, this means you need to let go of people who do not fit in. Zappos, for example, does this as quickly as possible. After a new employee's first week, the company offers an employee $4,000 to quit. The reasoning is that those who feel like they don't quite fit with the company culture will most likely take the money and leave.

Finally, you should also try to act like a regular employee yourself. Take vacations and put photos of your children on your desk, just like everyone else. This shows your staff that it's okay for them to have personal lives too.

### 9. In the choice between a career and family, choose both! 

These days, there's a lot of discussion around the concept of _work-life balance_. But "balance" itself is misleading, as it implies that to gain more of one, you need to sacrifice some of the other: go big with your venture _or_ go home to your family. In reality though, you can do both.

So how can you go big and build a large-scale enterprise?

Simply, you need help. You can't run a large business on your own, so you need to learn how to work effectively with others.

This has its benefits: other people can refine your raw ideas or take the ones that you've long since lost interest in pursuing to create something dynamic. Other people can also bring their own creativity into play, helping you solve problems more efficiently.

All this translates into a significant competitive advantage as long as you know how to work with and lead a large organization.

Yet with such responsibility, how can you ensure you remember to go home to your family?

Use technology to your advantage. You can set up a home office, conduct virtual conferences and try to keep your mornings and evenings clear of meetings so you can spend more time at home.

It's important you make this possible throughout your entire organization, as it shows your employees that they don't have to choose between career and family either; they can have both.

One person who embodies this approach is Tina Fey, who has a brilliant career as an actress, writer and producer, while still being a dedicated mother. You should be similarly undaunted by the prospect of pursuing your career ambitiously while making time for your family.

### 10. Final summary 

The key message in this book:

**To make your company successful, keep your cool: take only smart risks and when chaos strikes, find the golden opportunities it presents. Try to understand your own strengths and weaknesses and cultivate your leadership skills to work around them.**

Actionable advice:

**To generate creative ideas, organize a hackathon.**

Many companies develop stiff hierarchies as they grow. To stay fresh and innovative, consider organizing occasional _hackathons_, where you form cross-functional teams and let them spend a day or two working out a tricky problem or a project of their choosing — this process can generate innovative solutions in a surprisingly short time span. For example, in one hackathon, 100 Silicon Valley entrepreneurs came up with several innovative ways to improve education, all during one long flight from San Francisco to London!

**Suggested further reading:** ** _Smart_** **_People_** **_Should_** **_Build_** **_Things_** **by** **Andrew** **Yang**

_Smart_ _People_ _Should_ _Build_ _Things_ explores the dangerous consequences of top students' career choices in the United States, and offers practical solutions to reset the country's course toward prosperity by encouraging students to adopt an entrepreneurial attitude. Along the way, the author provides solid advice for budding entrepreneurs on their first adventure into business.
---

### Linda Rottenberg

Linda Rottenberg is the cofounder and CEO of the non-profit organization _Endeavor_. She has been named as one of the "100 Innovators for the Twenty-first Century" by _Time_ magazine and as one of "America's Top Leaders" by _US News_.

