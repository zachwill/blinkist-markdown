---
id: 53fcd12e3762620008000000
slug: creative-confidence-en
published_date: 2014-08-26T00:00:00.000+00:00
author: Tom and David Kelley
title: Creative Confidence
subtitle: Unleashing the Creative Potential Within Us All
main_color: 81B89D
text_color: 4B6B5C
---

# Creative Confidence

_Unleashing the Creative Potential Within Us All_

**Tom and David Kelley**

_Creative_ _Confidence_ shows us the amazing value and impact that creativity has in our everyday lives. In fact, being able to think creatively can increase your happiness and success in both your professional and personal spheres. Luckily, artists and musicians don't have a monopoly on creativity. With the right techniques and mind-set, anyone can think creatively.

---
### 1. What’s in it for me? Learn how to flex your creative muscle. 

Do you remember when teachers would chastise you for drawing pictures or singing songs in class, telling you to buckle down and do some "proper" work instead? Of course, math and history is important, but while these teachers might mean well, this insistence that learning facts is more important than flexing our creative muscle has a dampening effect on our ingenuity.

This lack of appreciation for creativity is outdated: today's companies actively seek out creative minds, and what's more, creativity is a key factor in designing your life to match your values and aspirations.

Luckily you can never lose your creativity entirely. It's always there; you just need to nurture it.

_Creative_ _Confidence_ helps you rediscover your creativity, showing you how to exercise your innovative muscle by drawing on the authors' experiences working at the design and innovation firm IDEO, as well as the lessons they learned and imparted to others at the d.school at Stanford University.

You'll learn how everyone — not just those in "creative" professions — can benefit from exercising their imagination.

In these blinks, you'll learn

  * which member of The Beatles almost lost his chance at fame and fortune by going into the manufacturing business;

  * why it's far better to fail than succeed on the first try; and

  * how the "do something" mind-set helped keep an elderly woman from standing in freezing rain.

### 2. Creativity is about all kinds of imaginative innovation, not just genius masterpieces. 

What is creativity? Is it daubing breathtaking paintings, sculpting Greek icons out of marble slabs or writing sonorous music?

Great creativity sometimes finds its expression in the fine arts, but it actually has a much broader application. Creativity means simply using your imagination to create something new.

This broader definition embraces many different aspects of creative work that otherwise go unseen. Not only does it include the work of artists, but also the work of more analytical types, like CEOs or computer programmers.

Indeed, these analytical types express their creativity whenever they make something new: for example, when the computer programer creates a novel web interface, or the CEO develops a new business strategy.

When we employ this broader definition of creativity, we discover that we are all born to be creative.

As young children, we finger-painted and danced around the room. We built tree houses with our own hands and found interesting solutions to the problems we faced.

Unfortunately, as we grow older, many of us stop acting out our creativity. However, we never actually lose the ability altogether.

In fact, you can think of your creativity as a muscle that you can train and use to find innovative solutions to problems.

Even if you haven't flexed this muscle for a while, it only takes a bit of training and hard work before you can make it strong again.

Just look at the magnetic-resonance imaging (MRI) technician Doug Mietz — very much the analytic type. He struggled to find ways to make children feel safe when they were being scanned in one of these intimidating machines, but with the help of the authors, he was able to train his creative muscle in order to find a solution.

His imaginative solution involved completely changing the look and designs of these clinical machines to something adventurous, like a pirate ship or a UFO. The kids then saw the experience of getting scanned as an exciting adventure rather than something dreadful.

> _"There's no word in the Tibetan language for 'creativity.' The closest translation is 'natural.'"_

### 3. After years of being considered irrelevant, creativity is becoming a highly desirable quality. 

"Creativity is only for the artsy types."

How many times have you heard this or even said it yourself? In our modern society, there is a myth surrounding creativity — that only those who do things like paint landscapes or sing in jazz clubs are paid for their creativity, not lawyers and CEOs.

Many people think that there is simply no room for creativity in traditional work environments. For example, those who work in areas like medicine or law often face others who tell them that there's no need to be creative to fulfill their duties satisfactorily, and that it's even _irresponsible_ to try and improvise.

For many decades artistic types, such as designers, musicians or painters, were essentially relegated to the kids' table, while the "grown-ups" — i.e., the more analytical professionals — held important business meetings in the hallway or board rooms.

We even see this social rejection in the classroom, where creativity is smothered in favor of rationality, and small children are told to obey the rules and only color inside the lines.

Just think, when Paul McCartney was at school he was told to give up music and pursue a "safe career" in Liverpool's manufacturing and shipping industries. Thankfully, he did not follow this ridiculous advice and instead became part of the most successful band in history.

But this anti-creativity mind-set is changing.

Today's businesses have begun to recognize that the best way to find novel solutions to complex problems is by investing more time and money in nurturing their employees' innovative drives.

For example, a very recent IBM survey of more than 1,500 CEOs reported that creativity is the _single_ _most_ _important_ leadership skill for enterprises engaged in the complex world of global commerce, where innovative solutions are necessary to pave the best possible path to success.

In order to reap the benefits of creativity, you'll first have to rediscover your own. The following blinks will show you how to do just that.

> _"Creative energy is one of our most precious resources."_

### 4. Develop the courage to explore your creativity – it’s a great first step toward discovering your new self. 

Have you ever been in a situation where you had to face your fears, perhaps by touching a boa constrictor in spite of a phobia? When you confront your fear of being creative, it's just the same.

Luckily, there are strategies you can use to help make the process easier.

Many people want to be creative, but they don't really know how to do it. The most basic thing you can do is to give up the notion that you cannot be creative. You must remember that _everyone_ — even you — has a creative muscle.

You must truly believe that you can grow, experience and create more than you ever previously thought was possible.

By adopting this _growth_ _mind-set_ you will automatically change your perception of yourself and the world around you, and be empowered to flex your creative muscles and come up with innovative solutions.

One way to develop this mind-set is to create a _roadmap_ to guide you through creative processes. This roadmap has all the different techniques you need to train your creative muscles, providing you with an easy, straightforward path to follow.

One technique that the authors often use with so-called "non-creatives" is _design_ _thinking_. Basically, design thinking helps us to identify human needs and create new solutions by thinking like design practitioners.

For example, in order to redesign a series of kitchen tools, the authors went out and talked directly with users who were experiencing problems, such as the elderly, in order to find out what bothered them about the existing designs.

Afterward, they used the knowledge they gained to experiment with different solutions, and eventually put the best ones into production.

> _"Belief in your creative abilities lies at the heart of innovation."_

### 5. Allow yourself to fail in order to succeed. 

Sometimes, when we're on our way to achieving our goals, our steps falter, causing us to lose confidence and give up. But this is exactly where the difference between success and failure lies.

Don't give up! Get up and try again!

Even the greatest creative geniuses fail. However, when they do, they use their failure to their advantage.

Do you think that Mozart wrote his first masterpiece without writing garbage first? Or that Thomas Edison simply flipped the switch on the first lightbulb without sweating through many different models and test runs?

Everyone fails. In fact, Professor Simonton of the University of California found that these iconic creative geniuses didn't fail less, but actually _more_ than others.

However, rather than quit, they learned to keep going. Creative geniuses learn from their mistakes and modify their next attempts so that they are closer to the mark.

If we want to be successful in our creativity, then we need to think like Edison: a failure is only a failure if nothing has been learned from it.

In fact, there's a paradox: success at your first attempt might actually _hurt_ your results in the long run.

This is because failures that happen early in the innovative process reveal the weaknesses in your product or line of thinking.

Consider, for example, the story of the Wright brothers, who launched the first successful airplane in Kitty Hawk, North Carolina in 1903. If they hadn't failed hundreds of times in unmanned trials and had the opportunity to improve their design, the Wright brothers might have been just another splat to clean up on the road to flying machines.

> _"If you want more success, you have to be prepared to shrug off more failure."_

### 6. New experiences will help you explore the creativity within you. 

How many times have you waited around for your muse to come to you? Often people sit around waiting for the proverbial apple to fall on their head, but it never works. If you want to gain inspiration, you'll need to have new experiences.

New experiences allow us to see the world in a different light, thus helping novel ideas come to us more easily.

For example, two of the authors' students were once faced with the task of finding a way to decrease child mortality. After doing some research on the internet, they decided that the only way they could really grasp the problem would be to go to the countries and the hospitals where this problem was worst.

The insights and experiences they gained from their travels helped them to develop a cheap sleeping pouch with a built-in heating pad that has kept countless children living in remote, harsh climates safe from hypothermia.

But let's say that you can't afford to fly around the world to gain inspiration. What should you do?

In order to gain these valuable insights, try to _think_ like a traveler, seeing everything — even the most mundane or familiar tasks and environments — with "new eyes."

Ask "why?" at every opportunity. You'll find that the answers will offer you insight into why people behave and use products the way they do, and how you can improve things.

For example, many people still use outdated technology, like landline phones. It's easy to just accept this fact and move on. However, if we asked "why," we'd probably discover that people use them purely out of habit, and _not_ because landlines are somehow more practical than the alternatives.

Armed with this information, you might look at ways to produce or market mobile devices more suited to a conservative mind-set about technology, rather than trying to win people over with gadgets that don't appeal to them.

### 7. Work with other people who support and share your passions. 

The typical image of creative geniuses, such as composers or great thinkers, is that of the intense loner, developing their strongest ideas in complete isolation.

However, this solitary approach is not the best way to exercise your creativity. In fact, quite the opposite is true: innovation is a product of teamwork. Indeed, the most creative environments are those where many people work closely together. Why?

First, working in a group lowers your own stress factors.

We've all had the experience at some point in our lives — whether writing an essay or preparing a presentation — where our mind goes blank and we lose our creative flow.

Often this results from being overstressed and trying too hard to keep coming up with the goods.

But if you are part of a team, and the innovative process is shared, you'll find yourself in a better, more relaxed frame of mind. If you don't have to bear the pressure alone, you're in an environment that is much more conducive to creativity.

Second, group work increases your creative output. Talk to others about your ideas and your mind gets the space it needs to roam. Whenever you're stuck, working in a team will help you to produce fresh insights.

A team increases the number of circulating ideas exponentially as people inspire one another and then reciprocate this inspiration in return.

Their opinions and ideas mean that you won't have to rely solely on your own _idea_ _bank_, especially if you discover that your own initial ideas aren't necessarily the best.

One example of these principles in action can be found at the consulting firm IDEO, where the authors work. They installed a wall-sized blackboard on which people could jot down their ideas, questions or quotes in order to maximize the potential for inspiration.

So don't be afraid to accept help. Even the smallest inspiration can be enough to destroy the dam that prevents your creativity from flowing.

> _"In a world with so much creative potential, it is dangerous to assume that all the good ideas are found at the top."_

### 8. The “do something” mind-set helps you see that you can and should change things for the better. 

How many times have you wanted to do something, like apply for a new job or approach an attractive person at a bar, but didn't because you were too scared?

Even when we know what we should do — like buy that person a drink — we simply don't. Instead, we just sit down and discreetly appreciate them from afar.

This _knowing–doing_ _gap_ isn't limited to dating. We see it in business, too. Just look at the Kodak camera innovation team in the 1990s: they _knew_ that the future of photography would be digital, but were too scared to change their strategy.

As a result, Kodak, once one of the largest and most powerful brands in the United States, continues to lose their market share to competitors — and the revenue that went with it.

Luckily, we aren't doomed to continue letting opportunities pass us by; it all starts with your mind-set.

Changing your mind-set from, "I know I should probably do this," to "I will do this," is the most powerful means you have of taking charge of your own life.

Even when the outcome is _not_ successful, you will have no regrets, because you know that at least you tried. The worst mistake you can make is to not try and never learn.

Consider, for example, John Keefe, an editor at a Manhattan radio station. One day he overheard a co-worker lamenting that her elderly mother never knew when the bus would arrive, and thus often ended up standing outside in the cold.

Keefe saw a possible solution, and acted immediately. He created a small hack into the Transit Authority website to monitor the buses, and set up a corresponding phone number that told listeners how many stops away the bus was.

The whole project took less than a day to complete, but this simple solution was only possible because he decided to act.

By now, we've seen how important creativity is in the business world. These final blinks will show you how creativity can change your personal life for the better.

> _"The best way to gain confidence in your creative abilities is through action — taken one step at a time."_

### 9. Balance money and passion to find a job that fulfills all your desires. 

What kind of job would you rather have: one that pays as much as possible, or one that allows you to pursue your passions?

Usually people want both, and this can cause a lot of stress. However, it's rare that both our passions and big money are equally represented in our job opportunities, so we often have to choose between the two.

One of the authors, Tom Kelley, faced this exact predicament when he left management consulting to follow his heart and work with his brother David at IDEO. After he left, he received a desperate phone call begging him to come back for a huge chunk of change.

Weighing his options, he saw that his work at IDEO allowed him to follow his passion, while the other job offered financial security. After days of agonizing thought, he finally decided to decline the offer, even though it would have paid a lot better than his work at the design and innovation firm.

He was able to make this decision by finding the balance between money and passion.

At first, it might seem as though a high-paying job will make you happy in the long run — but what if that same job required you to work through weekends, birthdays or holidays?

On the other hand, doing social work for free might feel like a personally rewarding way to spend your time. But what happens if you can't pay rent and end up needing the help of social services yourself? Can you still be happy if you abandon financial security in favor of committing to your passions?

Neither of these are appealing solutions for most. That's why finding the balance is so important. But you'll need to dare to be creative, finding ways of achieving that balance in order to satisfy all your needs.

Even if it takes time and effort to balance the scales so that you're happy, it will be time well spent.

> _"Money is always easier to measure, which is why it takes a little extra effort to value the heart."_

### 10. Channeling your creativity will improve your work and make you happier in your private life. 

Allowing yourself the freedom to think creatively and to innovate is one of the best ways to ensure your own happiness, both professionally and personally.

For starters, when there's innovation, engagement and creativity in your work, your employers will start paying attention. Nowadays, companies recognize the great value of people who can pitch new ideas and help their projects achieve new heights.

Not only that, but the recognition and success will make your job far more challenging and fun.

Nearly everyone who has worked with the authors to rediscover their creative potential has their own stories of how cultivating creativity did marvels for their career. It's allowed CEOs to involve themselves in innovative, grassroots work and helped lawyers approach tough cases in a novel and intelligent way, leading them to win more cases.

A creative mind can do wonders for your personal life as well.

We all become bored or unhappy with our life trajectories at some point. However, when you experience life with new eyes and see the value in even the smallest things, you put a fresh spring in your step.

Just look around while you're on your way to work and you will see that every day is an adventure, rather than a grim chore. For example, you might marvel at the way your kids and your partner play at breakfast, or how the leaves change with the seasons.

Not only will this new perspective help you to enjoy the mundane aspects of life, but the life you've built for yourself will further inspire and motivate you.

So be creative, be innovative and take control of your life. It's not time to be afraid; go out and _do_ _something_!

### 11. Final summary 

The key message in this book:

**Creativity** **isn't** **just** **for** **artists.** **It's** **in** **our** **blood.** **And** **although** **society** **does** **everything** **it** **can** **to** **smother** **it,** **you** **can** **still** **flex** **your** **creative** **muscles** **in** **order** **to** **win** **it** **back.** **The** **only** **way** **to** **do** **that,** **however,** **is** **to** **face** **your** **fear** **of** **failure** **and** **take** **immediate** **action.**

Actionable advice:

**Give** **your** **children** **the** **gift** **of** **creativity.**

If you want your kids to be able to think freely and creatively, then you'll need to combat the messages they get in school. Give them space to do and want things that are "unrealistic." Let them paint outside the lines and let them fail so that they can use that experience to develop their own novel solutions.

**Suggested** **further** **reading:** **_Creativity,_** **_Inc._** **by** **Ed** **Catmull**

_Creativity,_ _Inc._ explores the peaks and troughs in the history of Pixar and Disney Animation Studios along with Ed Catmull's personal journey towards becoming the successful manager he is today. In doing so, he explains the management beliefs he has acquired along the way, and offers actionable advice on how to turn your team members into creative superstars.
---

### Tom and David Kelley

David Kelley is the founder of IDEO, one of the world's leading innovation and design firms, as well as the founder of Stanford University's Institute of Design, d.school.

His brother, Tom Kelley, is also a partner in IDEO, and the author of the bestselling books _The_ _Art_ _of_ _Innovation_ and _The_ _Ten_ _Faces_ _of_ _Innovation_. In addition, he is an executive fellow at the University of California, Berkeley's Haas School of Business.

