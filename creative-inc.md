---
id: 561c1a8136356400075e0000
slug: creative-inc-en
published_date: 2015-10-14T00:00:00.000+00:00
author: Meg Mateo Ilasco and Joy Deangdeelert Cho
title: Creative, Inc.
subtitle: The Ultimate Guide to Running a Successful Freelance Business
main_color: F15830
text_color: 8C331C
---

# Creative, Inc.

_The Ultimate Guide to Running a Successful Freelance Business_

**Meg Mateo Ilasco and Joy Deangdeelert Cho**

_Creative, Inc._ (2010) provides a handy step-by-step guide to setting up and running a freelance business. Clearly structured and packed with tips on everything from advertising and agents to portfolios and pricing, this is an indispensable guide for those considering turning their artistic talents into a freelance career.

---
### 1. What’s in it for me? Turn your creative skills into a flourishing business. 

Are you a creative? Have you spent years finely honing your professional skills? Are you still struggling to turn these awesome assets into a substantial success despite all your hard work? 

Creatives might be at the top of their game but they still struggle to make the most of those talents in business. They just don't have the knowledge to master the tricky and competitive commercial world. 

But this is where these blinks come in. They explain, in just a few simple steps, how creatives of any stripe can turn their skillsets into viable and profitable businesses.

In these blinks, you'll discover

  * why every creative should bone up on tax law;

  * why it can help to give someone a portion of your profits; and

  * that nothing is more important than a strong portfolio.

### 2. Establish a unique brand that you feel comfortable with. 

So you've proved that you're highly adept at your creative skill. Whether you're an illustrator, graphic designer, writer or photographer, you've now decided to go it alone and work as a freelancer. Of course, there's a lot to think about. So where should you start?

Firstly, creating a _strong brand identity_ will set you apart and help you reach your target audience. Selecting the right company name is hugely important. After all, it's the first impression potential customers get of your business, and something that will endure as your market grows.

Using your own name is common for solo freelancers. This might cause you problems later down the line if you plan to collaborate, or you share a name with an established brand: Pat Starbucks, Freelance Web Designer, for instance, might face some legal issues if they went eponymous!

So you must check that the name is not already in use. Ideally, your business name will feel right to you and garner positive reactions from others. For example, when Matt, Jenny and Julia of freelance business Also arrived at their name, it caused them to scream with excitement. They loved it, as it didn't tie them to a particular niche.

The next step is to work on your branding: how will you present your business to the world? Your brand should give a strong impression of your business's identity. Is it about rustic design or a sleek, polished finish? 

Whatever you decide on, always ensure your logo, typeface and business card all carry the same feel. You might even consider hiring a specialist to help create your logo, as the better it is, the more professional you'll look. 

Communication, both visual and verbal, also make a huge difference. The way you advertise, conduct business and interact with clients all contribute to your brand identity. Photographer Thayer Allyson Gowdy recommends having a clear, personal style to inspire the potential clients you want to reach within your target market. A unique brand that reflects your personality will convey a strong message about your business.

### 3. Build an up-to-date and relevant professional portfolio. 

Once you've established your brand, it's time to show the world what you and your business can do. An impressive portfolio is the most powerful tool available to showcase your talents. 

One simple way to ensure your portfolio does its job is by keeping it up-to-date _._ An artist's style can change over time, so it's worth maintaining a portfolio that reflects your current output. This makes client interactions smooth, as prospects will know exactly what it is you specialize in, and they'll be less likely to ask you to produce work you no longer enjoy.

If you left college many years ago, avoid including work you did then, as this suggests you've done little of worth since. Professional work you have completed will in any case be more impressive, as it demonstrates that your skills have been utilized by real clients.

Your portfolio work should also be relevant. Consider your client's needs when presenting them with your portfolio. Use it to target what they are likely to want from your service. 

You may even opt to create different portfolios for different tastes or directions. A photography company, for example, often has a portfolio for professional portraits and another one for family photos. Then, when you receive an inquiry, you can supply the portfolio that's perfectly suited to it. 

Finally, it goes without saying that your portfolio should be professional. Not only should it include examples of professional work you have done, but the whole appearance of the portfolio should be high spec. 

For instance, the portfolio should be the right size: perfect for sitting on the table when you meet your client. Also, it should be clean and protect your work from stains and fingerprints. An up-to-date, relevant and professional portfolio will give you the best chance of gaining business.

> _"You have to be super proactive in this business. Grab your portfolio and get on a plane and go!" — Thayer Allyson Gowdy, photographer_

### 4. A strong business sense is as important as creative flair. 

Designing a brand and developing a portfolio may, for an artist, be the enjoyable parts of starting a business. However, you need your business head screwed on to guarantee profits.

A business plan is not to be overlooked — it's crucial to success. Though they may seem rather dull, business plans are great for setting your goals and determining your needs. These include financial projections and overheads such as office space.

Your plan should also feature a clear _mission statement._ This is an outline of the values you hold and the goals you wish to achieve. Following it will help you stay on the right course.

Be prepared to make financial sacrifices at first. Moonlighting is an option, especially for young freelancers, that can guarantee you income while your business is finding its feet. However, this can be exhausting. Your freelancing should always take priority.

Illustrator Andrew Bannecker juggled a job as an art director with freelancing for two years before being confident enough that his illustration work could take over full-time. He had to be strategic and put in long hours to pull this off, however.

Like Andrew, you'll need to be prepared to make personal sacrifices. Often this means being thrifty with your money. You could consider moving to a cheaper area, taking on a roommate, or asking your partner to help contribute more. You'll also save money if you only buy equipment when you need it, and try bartering when making purchases.

What you shouldn't scrounge on, however, are the legalities that a freelance business requires. You will be liable to pay taxes, insurance, and to abide by a host of rules and regulations. 

You may need to apply for a business license, register your company name and service mark, and set up a business bank account. Navigating the bureaucracy calmly a step at a time can save you a lot of time and money in the long run.

### 5. Get your brand the attention it deserves with some basic advertising. 

Now it's time to make sure you spread the word. There are many effective advertising strategies, but some of the most common are the simplest: business cards, websites and blogs.

Business cards should always be carried with you, just in case. Small in size, they are perfect for transferring from your wallet to your potential client's hand. Remember to keep it short and sweet: your name, contact details and an idea of what you do should suffice to grab a customer's interest.

A website is great for showcasing your talents 24/7 to clients all around the world. To create one, begin by securing a domain name that should also be short and simple.

Next, you'll need to develop a clear and easily navigable layout. Clients want to quickly view what you have to offer without having fancy animations crashing their computer. Agent Lilla Rogers uses artists' websites when selecting whom to represent. She only has time to stay for two to three clicks, so their sites must impress straight away. 

A blog can be an even more personalized way of marketing yourself — and more powerful at tapping into different networks, too. 

There is more space on a blog to include what you couldn't elsewhere, like sketches in progress or photographs that didn't make the final album. Photographer Matt Armendariz describes his blog _Mattbites_ as a way to share tidbits there was no room for elsewhere: anecdotes and outtakes from photoshoots, for instance.

This method of networking can help your company reach out further than was previously imaginable. Animator Ward Jenkins started a blog in 2004 that gathered a large following. One of those followers invited Ward to contribute to the _Drawn!_ blog, which has eight contributing bloggers from across the world. Because of a little blog, Ward's ideas are now reaching a massive global audience.

### 6. Ensure you can meet your clients’ expectations. 

So it's going well and you're getting some interest. With a reputation to build, however, think carefully before taking on jobs. As much as you might like to, you cannot say yes to everyone. 

When starting out, approach suitable clients. Your input is likely to make a bigger difference to a smaller company, so don't approach the big guns straight away. Additionally, when approaching the project, research your client. This will allow you to understand their needs better and see where they might want to head so that you can work with them more effectively.

Industrial designer Josh Owen recommends finding personal connections with clients when starting out, by using the network of people you know (or have found through your blog). No matter how you make connections, you should always ensure you and your client are a good match before agreeing to the job.

Make sure you understand what they want from you, both in terms of content and timescale. Don't be afraid to ask lots of questions. If it's not going to work, turn it down. The alternative is far worse: letting a customer down after committing to the job will reflect badly on you and could affect future business opportunities.

Andrew Almeter of Almeter Design only works with those whom he feels are the right fit for his business. He looks for opportunities that will help further his career and says no if he knows he won't enjoy the job.

Almeter points out that politely explaining to the would-be client why he wouldn't be a good fit could mean that client refers you to someone else, so you won't necessarily lose out in the long run. Being able to pick and choose is at the core of freelancing, so make the most of it!

### 7. Great client relationships begin with open communication. 

Whether your client has an office down the road from you, or is based on the other side of the planet, maintaining good communication throughout the job is crucial. So how you do this?

Ensure that expectations on both sides are clear from the beginning. When you have established the brief, the estimate and the timescale, ensure that both parties sign a contract. This ensures a professional approach and protects you in the case of potential disputes. If the client initiates the contract, make sure you read the small print!

Next, send your client a timeline with deadlines for various stages of completion. If this is tight, ensure they are aware of this and that extra fees will apply as a result. Then start discussing communication with your client.

The founders of Also and Thayer Allyson Gowdy both prefer face-to-face communication, at least once during a project. The latter will frequently fly to meet her clients. However, this may be impractical, or your customers may prefer video calling, emails or phone calls. 

Either way, do remain open with your client, and stay in touch between deadlines or major meetings with quick messages to reassure them they haven't left your thoughts.

_Feedback_ is a vital aspect of communication _._ It should be constructive, something that'll help you sculpt the final piece. Make your feedback process more effective by presenting clients with a few options at each step along the way. By keeping them involved, you'll be more likely to produce something that they love. 

If clients don't take to a particular option, unearth which elements they prefer, and try to combine them. For example, perhaps they like the typeface of one design and the layout of another. Above all, keep dialogue open. You should feel comfortable explaining your creative process and asking questions to ensure you are meeting clients' needs.

### 8. Know what your work is worth! 

While working hard on your design might be fun and rewarding, you can't make a success of it if you don't get out what you put in. So take the time to sit down and work out your finances.

Consider how much your talent is worth. To secure the ideal job, you may wish to undercut the competition. This option is probably more appropriate for young freelancers or those starting out: it will help build your portfolio and reputation. 

However, if you have more experience, your work should come with a higher price tag, and you shouldn't be afraid to aim for this. If your skill is particularly specialized or if you know you are far more efficient than others in the market, your work is naturally worth more.

Over time, as your experience grows, give yourself a pay rise. Experience is valuable, but demand for your work is likely to increase, as well as your overheads: for instance, you may need more office space if your business grows, or to take on an agent.

To ensure you turn this income into a profit, keep a clear list of your expenses. List your outgoings along with the amount they cost. This should include business expenses such as travel, utilities and marketing as well as your personal expenses such as rent, clothing and recreation.

Bear in mind the amount of savings you wish to make and the amount you wish to reinvest in the business. Tax can claim 25 to 48 percent of net business income, so make sure you've planned for this too. Using the above figures, you can calculate how much income you'll need to make, and thus your baseline hourly fee. 

Working out how much to charge can be a balancing act between how much you need to make, how to ensure you get job offers, and how much you think your talent is worth. Time spent considering this, however, will certainly prove valuable.

### 9. Agents, though not for everyone, provide great support for a growing business. 

As your business expands, a talent agent may help you spend less time negotiating contracts or sorting out administration and more time simply being creative. 

An agent can help secure work and deal with communications. Agencies can be extremely well-connected and know their regular buyers well. This may lead to you gaining opportunities you may have struggled to find on your own. 

Agents will do extensive marketing on your behalf. For instance, the Lilla Rogers Agency present their client with marketing options and attend trade shows such as Surtex to exhibit their work.

Not only could an agent approach a client for you, but they can also negotiate contracts and manage ongoing relationships. This can save you having to barter over prices and deal with dreary administration tasks.

However, using an agent won't be to everybody's taste. Without an agent, you get to keep 100 percent of your profits and don't give a chunk away to pay their fees. Agents' commision may range from anywhere between 15 to 50 percent of your payment for each job. Think carefully about whether you feel this sacrifice is worth it. 

By approaching potential clients yourself, and by negotiating your own fees, you maintain more control over the project and the way you handle your business. This could potentially foster better relationships with customers, and give you the opportunity to make interactions that align with your brand identity. 

Illustrator Nina Chakrabarti has never worked with an agent, despite being approached by some. While she used to hate negotiating her own contracts, once she got used to it she was fully able to enjoy the freedom of working without an agent.

### 10. Set boundaries to sustain your work/life balance. 

With a flourishing career to manage, it might be tempting to continue throwing everything you've got at continuing your success. However, this can be all-consuming, so it is important to take a step back and ensure a healthy work/life balance.

Be aware of signs you are becoming a workaholic and recognize when to step back. If you are seeing less of your family and friends, or having difficulties switching off at night, you might be letting your business take over too much of your life.

To minimize this, keep track of your goals. When buried in your work, you risk focusing too much on the everyday and forgetting the wider picture. Take a step back and re-evaluate your goals: both business and personal. 

Why not create a long-term timeline? For example: "after one year I will be submitting my portfolio to a top magazine;" followed by "after 3 years I will employ an assistant so I can take more time off." This will help you see the bigger picture and remind yourself of your priorities.

Creating boundaries between your home and work life will also sustain your creative energy. In most cases, this is as simple as maintaining a physical boundary, separating your office space from living space. If you don't have a separate workspace to operate from, no problem! Designate a particular room in your house to do your work in. 

Time boundaries are also important. Just as if you were working in a conventional job, set yourself working office hours where possible. Take breaks where you indulge in a completely non-work-related activity such as yoga, lunch with friends, or just plain old "me time" — whatever that might mean for you.

### 11. Keep reflecting on your progress in order to determine your business’s future. 

After the first few years of freelancing, you will have gained plenty of experience and have an idea about whether or not you made the right decision. It's time to ask yourself some key questions. One of them might even be: Is it time to give up?

If things aren't going well, you may want to consider returning to a full-time job as someone else's employee. If you aren't getting what you hoped for from freelancing, be it a healthy salary or more time with your family, it is perfectly reasonable to return to full-time work.

Remember: giving up doesn't make you a failure. When applying for jobs you will have a wealth of experience and skills to draw on, having run your own business. If, however, things are going well, you could be faced with the opposite problem. If you're constantly busy and demand is high, you may wish to consider expanding.

Perhaps it's time to invest in more office space where you can manage larger operations. You could then consider the mass-market, for instance, producing designs for a national brand.

In this case, you will likely also need to take on staff — perhaps an assistant or intern at the very least. If you are prepared to share your brand with someone else, partnering is also an option. Although you will have to sacrifice part of your creation, a partner can bring further connections and customers, as well as allowing you more vacation time.

However you define success, it is important to regularly consider whether you are achieving it. Maintaining a sense of perspective and keeping your future options open will ensure you remember what matters most, whether that is within freelancing or beyond it.

### 12. Final summary 

The key message in this book:

**By taking a considered approach to branding and business when you balance clients' needs with your own, you'll create the perfect conditions for a business that showcases your creative talent. Freelancing is a challenge, but with the right tools and mind-set, you'll be able to carve out a niche of your own that you love!**

Actionable advice: 

**Get your facts straight!**

Before taking the plunge, quitting your day job and starting to freelance, do your research. Ensure you know the industry you plan to work in, unearth some companies that you might be interested in working with and start to foster a network that you can tap into, perhaps through attending events. Preparation is everything! Such research can be done while you're still at work full-time, and will give you a head start once you dive into freelancing. 

**Suggested** **further** **reading:** ** _The Wealthy Freelancer_** **by Steve Slaunwhite, Pete Savage and Ed Gandia**

_The Wealthy Freelancer_ (2010) reveals the secrets behind a successful career in freelancing. These blinks outline how you can focus, structure, manage, organize, market and price your freelance business in the most effective and fulfilling way possible.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Meg Mateo Ilasco and Joy Deangdeelert Cho

Meg Mateo Ilasco set up her freelance business Mateo Ilasco in 2005, specializing in housewares, gifts and stationery. She has written several other books including _Craft, Inc._, and runs the Modern Economy multi-designer sample sales.

Joy Deangdeelert Cho launched her business, Oh Joy!, in 2005. Her designs for textiles, packaging and branding are snapped up by clients in the fashion and food world. She also markets her own line of stationery nationwide, as well as keeping an award-winning blog.

