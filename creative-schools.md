---
id: 57c1769b4a1a18000344dd05
slug: creative-schools-en
published_date: 2016-08-31T00:00:00.000+00:00
author: Ken Robinson and Lou Aronica
title: Creative Schools
subtitle: Revolutionizing Education from the Ground Up
main_color: F5D131
text_color: 756417
---

# Creative Schools

_Revolutionizing Education from the Ground Up_

**Ken Robinson and Lou Aronica**

_Creative Schools_ (2015) is a guide to transforming education. These blinks break down every aspect of education from its history to the essential needs of students. They also illustrate the ways in which all people can help kids get the schooling they need to succeed in a rapidly transforming world.

---
### 1. What’s in it for me? Join the creative school revolution. 

All children love to learn. It's simply in their nature. So why do so many children dread school? And does it really have to be this way?

Many of the things kids hate about school today trace back to the very beginning of formal education. As you'll discover in these blinks, conventional schools were never intended to be places of joyful, creative learning.

Fortunately, there's an alternative to this traditional approach: _creative schools_. This term doesn't describe schools with four instead of two hours of art class wedged between a math assessment and a grammar lesson. Rather it means approaching learning from a completely different perspective — by avoiding strict schedules, guidelines and frequent assessments and trying out new ways of creating the ideal learning environment for each individual pupil. Everyone learns, including the teachers, the parents and the schools themselves.

In these blinks, you'll also learn

  * how our schools resemble piggeries;

  * what a lone computer in an Indian slum can tell us about education; and

  * how an entire school is run by students.

### 2. Formal education is shaped by the needs of industry. 

Do you ever wonder how modern schools were first developed? Well, they certainly _didn't_ originate as a means to foster the unique personality, creativity and talents of individual students. Rather, conventional education was a result of the need to deliver highly standardized knowledge to young people so they could work in factories.

Modern schools arose over the course of the Industrial Revolution in the eighteenth and nineteenth centuries.

Before this period in history, only the privileged received any formal education. But this changed as new industries emerged, requiring workers to have some basic skills like the ability to read, do simple math and understand technical information.

So, Western governments began organizing mass education with one main purpose — to produce useful labor for factories. And, since industrial production relies on conformity, compliance and linear processes, education was based on these needs too. In fact, schools themselves were designed more or less like factories.

Jump forward to the present day and this tradition is alive and well with the _standards movement_, which endeavors to make the nation's workforce internationally competitive by holding education to firm guidelines and standards. At the same time, _STEM_ or _science, technology, engineering_ and _math_ subjects are given preference, regardless of a student's strengths and interests.

But where did the standards movement originate?

It had already begun in the 1980s, but gained prominence in the year 2000, when several Western countries like the United States, the United Kingdom and Germany performed poorly in the first _PISA_ or _Program for International Student Assessment_ test.

Shocked by their poor results, the countries searched for ways to enhance the performance of their students. But, instead of catering to the needs of individual students, they once again planned education like an efficient factory, setting out exactly what students of a particular grade should learn and how they should learn it — all the while assessing their progress through testing.

This meant that by ninth grade, for example, all students might need to know basic algebra and be made to prove their ability by taking a nationwide test.

### 3. An overly standardized education is highly problematic. 

If you gave a brand new, unknown digital device to several different friends, you'd find that each of them approached the object a little differently. Some of them would start by reading the manual, while others would search the internet for information and still others would simply turn it on and play with it. The point is, as much as our schools might think otherwise, humans can't be standardized — and education shouldn't be either.

After all, from this little thought experiment it's clear that your friends don't learn the same way, and neither do schoolchildren. Yet, schools treat them as if they do. For instance, they are all expected to learn by sitting in class and listening to teachers explain things, even though this may not fit their personal learning styles.

Not just that, but not all students learn at the same level in all subjects at the same age. Some first graders might be advanced in math, but still struggle with reading, while others are exactly the opposite. Nonetheless, all these students are grouped by their age, not their skill levels.

Given this reality, it's not surprising that the standards movement has failed to improve educational outcomes. After all, an education based almost entirely on exercises and tests will destroy a student's creativity and lead them to disengage. And disengaged students do _not_ learn well.

In 2012, 17 percent of US high school graduates couldn't read or write fluently, and 21 percent of everyone between 18 and 24 couldn't even point out the Pacific Ocean on a map!

But beyond that, students with skills outside of the prescribed academic areas, like those who are great with their hands or are superb singers, might also become discouraged by the incessant assessments demanded by the standards movement.

As a result, they may end up jobless, in prison or alienated from society. Worse still, students from underprivileged backgrounds are even more likely to fail in the modern education system. And even if they do succeed, these days a college degree is no guarantee of a job.

So, clearly, something has got to change.

> "_Civilization is a race between education and catastrophe."– H. G. Wells_

### 4. Organic farming is based on four principles that easily apply to education. 

It's easy to think about our education system as a factory and just as easy to see it as a factory pig farm — all about outputs. As long as the pigs grow fast enough, factory farmers don't care if the animals are sick or the farm is harming the environment.

And while today's students aren't being fattened up, mass education _is_ all about yields. It's overly focused on test results and the number of graduates produced.

We already saw how this system is failing but is there a better one?

Well, maybe we can get some inspiration from organic farming, which is based on four principles: _Health, Ecology, Fairness_ and _Care._

For instance, a system based on health, ecology, fairness and care is designed to improve the lives of everyone involved, including the pigs, workers and consumers. But it's also based on ecological systems and works in harmony with them. That means plants are grown using natural biological cycles.

And, since organic farming is founded on fairness and care, it strives to provide good living conditions for both present and future generations.

When applied to education, these principles work seamlessly. That's because, while conventional schools focus primarily on achievements, whether they be in academics or sports, organic schools care about the development of the whole student into a physically, emotionally and intellectually healthy person.

But that's not all — organic education also relies on the ecological system of the school community to foster every student's abilities. Grange Primary School in Nottingham is run like a town by its students. It has a council, a newspaper and even a food market. As students work at the school and interact with one another, they learn a wide range of abilities from social skills to arithmetic.

Furthermore, organic education is fair because it appreciates all students, not just those with academic gifts. And finally, teachers and mentors treat students compassionately to provide the best conditions for their development. In other words, they treat them with care.

But what if you're a teacher at a school that's not yet so creatively organic? What can every teacher do to ensure their students learn while remaining curious and growing their creativity? Read on to find out.

> _"At their best, schools are living communities of individuals who come together in a shared venture of learning and development."_

### 5. Children are natural learners and a teacher’s role is to guide them. 

If you walked into an average classroom, you'd see students who are bored silly by just about everything that's presented to them. While this sight might seem normal, it shouldn't be. After all, kids are natural born learners.

Babies are so eager to explore the world that they grab any new thing they can reach. They also soak up language, often becoming fluent by the time they're two or three.

And this type of hunger for learning goes well beyond childhood. This was illustrated by Sugata Mitra, professor of educational technology at Newcastle University in 1999, when he installed a computer in the wall of an Indian slum and observed children's reactions to it. The interface was only displayed in English, which none of them knew, but within just a few hours the kids figured out how to use the console to play games and record music.

So, kids are inherently curious and it's up to teachers to foster this curiosity, not kill it. Here it helps to think of the teacher as a gardener. He can't force the children to develop, but he can nurture their natural inclination toward growth.

Here's how.

First, he should get the students to engage by leveraging their natural curiosity, creativity and eagerness to master new skills. One way a teacher can do this is by addressing students' interests. For example, someone who's obsessed with baseball will appreciate physics if she can use it to calculate the best way to hit a curveball.

But that's not all that matters. A teacher's expectations and relationships with the students are also key. That's because a student will work much harder if her beloved teacher expects her to.

Beyond that, great teachers also understand that different students require different kinds of teaching methods. For example, a basketball coach might realize that one student needs her to demonstrate a shot rather than just describe it.

And finally, teachers need to empower their students to believe in themselves by showing them that they can deal with difficult and uncertain situations as long as they remain calm, confident and creative.

> _"If I want those children to work hard, then I better be the hardest worker they ever saw."_

### 6. Schools should give students eight core competencies, starting with curiosity, creativity and criticism. 

When approaching education, it's important for us to consider what exactly we want our kids to learn. Up until now, we've answered this question with a never-ending list of subjects from French to algebra. But to guide students in later life, we need to teach them competencies, not subjects.

That's because the future is uncertain and there's no way to know if the subjects we teach students today will help them in the real world tomorrow. So, a better strategy is to teach skills that will enable them to learn what they need while dealing with whatever social or economic situations they might encounter.

This is simple and just requires schools to teach students eight core competencies, also known as _the eight Cs_. The first is _curiosity_, which we already know kids have a lot of. Here the school's job is to develop the natural inquisitiveness of children by encouraging them to pay attention to the world and ask questions about what they find.

It's also necessary for schools to foster _creativity_, or the ability to form new ideas and put them into practice. After all, from the invention of written language to the rise of the internet, creativity has been central to all cultural progress. And, going forward, it's only going to become more important when the students of today face ever more complex problems that they'll only be able to solve creatively.

The third competency relates to the ubiquitous information overload we face today, which demands the ability to discern facts from opinions and relevant information from irrelevant noise. So, it's essential to teach students _criticism_, or the desire to question the data they observe and draw their own conclusions.

> _"The real driver of creativity is an appetite for discovery and a passion for the work itself."_

### 7. The final five competencies help students become better team members and citizens. 

We expect and deserve a great deal from our schools. So, how can we ensure we get it?

Well, education serves four main functions.

First, it's supposed to benefit students personally by helping them build on their individual talents. Second, it's meant to boost the economy by generating a stream of innovative, well-qualified new workers. Third, it should help young people understand their culture and appreciate those of others. And finally, schools are also tasked with producing politically engaged and compassionate citizens.

But our students won't be able to fulfill these functions without further competencies. So, here's where the ability to _communicate_ comes into play. After all, the ability to express oneself is key and it goes far beyond writing skills. It also includes the ability to speak clearly and confidently in public and convey information through things like art and music.

Beyond that, students also need the ability to _collaborate_, not simply compete. That's why good schools have students work on team projects where they learn to organize, compromise and resolve conflicts as a group.

Another essential competency to teach students is _compassion_, or the ability to feel empathy for the feelings of others. That's because an empathetic child won't bully others since he knows how terrible it is to be bullied and wouldn't want to feel that pain himself.

It's also important to teach children _composure_ through meditation and other mindfulness practices that help them connect with their feelings while developing inner balance.

And finally, while conventional schools might teach the theoretical aspects of politics, like how elections work, what's really essential is to teach _citizenship_. Doing so will help students oppose injustice and use politics to benefit their communities. That's exactly the idea at Grange Primary School, where the students run their own town council.

### 8. Everyone can contribute to improving our schools. 

Education isn't just about schools, teachers and students. Naturally, a school's principal is also essential to shaping the learning environment.

Creative principals don't just manage schools, they lead with a vision and search for new ways to improve their school. Just take Richard Gerver. When he became headmaster of Grange Primary School he truly had a vision and it began by transforming the school into _Grangeton_, a model town run by students. He wanted the students to learn through real world activities.

So, a principal's vision can create a shared purpose. Everyone in the school community will feel that their day-to-day actions contribute to a greater goal. Beyond that, great principals also work hard to invite everyone in the school to share their ideas, which also helps to build community and show people that they matter.

But principals aren't the only ones who can shape the vision of education; policymakers can also help improve our schools. In fact, schools can be improved even within the narrow limits of existing political structures.

However, policymakers will need to collaborate with schools and communities. And of course, they'll need to give each school the freedom and resources it needs to transform itself.

For example, students in South Carolina were lagging behind the national average for reading and math, with a quarter of students not graduating high school in the normal four years. Then, in 2012, a group of educators reached out to the state board of education for help.

The politicians brought in New Carolina, a non-profit consulting firm, and asked people within the community like teachers, parents, and city officials how they wanted their schools to change. A huge number of people contributed ideas and, together, agreed on a list of ways to improve education that are now being put into place across the state. This kind of broad collaboration is essential if we wish to transform our schools.

### 9. Final summary 

The key message in this book:

**Conventional education is all about maximum efficiency, and it's not working. After all, humans are individuals, and our teaching methods should be personalized too. We need an education system that fosters each pupil's natural curiosity and skills.**

Actionable Advice

**Let your students teach each other.**

Students learn best from their peers. This is because in most cases, these peer teachers have only just learned the skill they're teaching so they recall what was difficult about it. So, the next time you're trying to teach someone a challenging topic, try delegating the task to someone who also only recently mastered it.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Finding Your Element_** **by Ken Robinson**

_Finding Your Element_ offers engaging advice on ways you might discover your true passions and talents, and then reorient your life to incorporate more time for them. Written with a keen sense of wit, _Finding Your Element_ offers entertaining and inspiring wisdoms that will help you not only to be more productive but also to improve your overall happiness and quality of life.
---

### Ken Robinson and Lou Aronica

Ken Robinson is a writer, international speaker and education advisor. He's taught pedagogy at the University of Warwick and advised the UK government on arts in schools. In 2006, he delivered TED's most-watched presentation ever: "How Schools Kill Creativity."

Lou Aronica is an American editor and publisher who's written four novels and is the co-author of several works of nonfiction.

