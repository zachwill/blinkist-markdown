---
id: 564a0b3066343100079c0000
slug: creativity-en
published_date: 2015-11-20T00:00:00.000+00:00
author: Mihaly Csikszentmihalyi
title: Creativity
subtitle: The Psychology of Discovery and Invention
main_color: 8BD964
text_color: 497335
---

# Creativity

_The Psychology of Discovery and Invention_

**Mihaly Csikszentmihalyi**

_Creativity_ (1996) is an exploration of how creative people produce groundbreaking ideas. It unpacks the commonalities between creatives and their backgrounds, and explains exactly what it is that makes a creative person able to give birth to unique concepts.

---
### 1. What’s in it for me? Get insight into how to increase your own flow of creativity. 

Have you ever had an experience in which ideas and solutions seemed to flow effortlessly from your mind, where every action you took seemed natural and perfect? That's creative flow, a state that most creative people feel when doing their very best work. 

While you might know what it _feels_ like to be creative, it's a whole different challenge to actually _explain_ creativity and where it actually comes from, however. 

These blinks will show you just what sort of environment is needed to stimulate the creation of new ideas and concepts, through exploring the lives and work of contemporary creatives. You'll learn why humanity is dependent on creativity for its survival, and what you can do daily to improve your own creative flow. 

In these blinks, you'll also learn

  * why the Italian city of Florence was a hotbed of creativity in 1400;

  * why most adult creative talents weren't child prodigies; and

  * why your kitchen table is a great place to inspire creativity.

### 2. Creativity happens within a system, which is made up of a domain, a field and a person. 

We call the process by which a person comes up with a new or innovative idea _creativity_. Yet what exactly is the source of creativity? 

Some people believe that creativity springs somewhat magically from within each person, but it's certainly more complicated than this. We derive creativity largely from our surroundings. 

Think about it. If creativity appears simply out of the blue, why exactly was the Italian city of Florence such a hotbed of creativity around 1400?

It wasn't just a coincidence that between 1400 and 1425, Florence was the epicenter of the Italian Renaissance. The city was flourishing financially; patrons of the arts encouraged artisans to explore and create ever greater works of art. 

Some of Western civilization's great art works were created at this time, such as Lorenzo Ghiberti's bronze doors of the Florence Baptistry and the massive dome of the Florence Cathedral, designed by Filippo Brunelleschi. 

The Renaissance era illustrates the true nature of creativity, in that creativity essentially occurs within a system comprised of a _domain_, a _field_ and a _person_. 

The _domain_ is a broad category in which creativity occurs, such as in mathematics or music. 

Within the domain is the _field_, which includes individuals who are experts in the particular domain. These individuals serve as the domain's gatekeepers, determining which new ideas should or should not be included in the domain. 

In the domain of visual arts, for example, the field consists of art teachers, museum curators and government-run cultural agencies. 

The individual _person_ is the last component of the system. Thus creativity occurs when an individual uses a domain's methods (like a mathematical formula or a minor key) to produce something new (like a new hypothesis or piece of music), and the new product is accepted by the field's gatekeepers.

> _"Perhaps being creative is more like being involved in an automobile accident."_

### 3. Certain personality traits facilitate creativity; creative people tend to have complex personalities. 

What makes a person more likely to be creative? There's no simple answer to this question, yet some personality traits do facilitate creativity more than others do. 

Some people may be genetically predisposed or even physically gifted in a way that encourages interest in a certain domain early on in life; and early interest in a domain is crucial for creativity. A person particularly sensitive to color and light, for example, may be more inclined to paint. 

A person also needs access to a domain, a situation which often depends on luck. People born into wealth, with access to education and other creative personalities, can have an advantage. 

Yet access to a field is equally important. Even if a person produces something great, he may have a hard time getting acknowledgement for his work if he's lacking the right connections in his particular field. 

This is why, for example, the music of composer Johann Sebastian Bach was not widely known until composer Felix Mendelssohn rediscovered his work and promoted it — several decades after Bach's death. 

Creative people also tend to have complex personalities, which often leads to conflict among different personality traits. In general, inner conflict is common among creative individuals. 

Some creatives tend to be smart and naive at the same time, for instance. Composer Wolfgang Amadeus Mozart was a musical genius, but he also had a playful, almost childish personality. 

Creative people are also often simultaneously _both_ introverts and extroverts. They need time alone to practice their craft, but also feel the desire to share their ideas with the world.

### 4. Creativity has a few common threads that help explain how to get from creative problem to solution. 

Creativity is a complicated process. A scientist, for example, doesn't come up with a brilliant discovery in the same fashion as an artist who creates a memorable piece of art. 

While there isn't a single formula that applies to the creative process across all domains, some common threads do exist. In general, the creative process can be described in five steps. 

The first step is _preparation_, where the creative becomes immersed in a problem or idea. 

The second is _incubation_, when ideas develop without the creative being fully conscious of the process. The third is _insight_, or the "aha!" moment, when the new idea springs to the front of the creative's consciousness. 

Then the creative enters a period of _evaluation_, in which he has to decide if his insight is valuable and worth pursuing. The final step is _elaboration_, where the creative puts his insight into practice. 

It's still possible to produce something creative without taking these exact steps, but creative insight usually comes about in this fashion. 

The three main _sources_ of creative ideas are personal experience, the creative's domain and the creative's field. 

Visual artists and writers often derive their creativity from _personal experience_ : from emotions, like love or anxiety, or life events, like a birth or a death. Poets Anthony Hecht and Hilde Domin, for example, often recorded their inspirations by writing down their everyday experiences.

The domain is a large source of creative energy. Creative people often get ideas when they disagree with a prevailing idea in their domain's mind-set, and seek to rebel against it. 

People in a creative's field also inspire creativity. Teachers, fellow students, coworkers and mentors have a big influence on a creative's thinking. Scientists, for example, don't just learn from books. They also pull ideas and draw inspiration from seminars, meetings and workshops.

> _"One cannot be creative without learning what others know."_

### 5. Creative people experience flow while working, which makes goals more clear and time seem to stop. 

What motivates creative people to keep working? From interviews with creatives such as writers, musicians and scientists, the author found that most creative people do creative work because they enjoy it. 

The joy a creative person feels when he works is called _flow_. The author says there are nine elements within the flow experience; let's examine three of them in depth. 

First, flow has clear goals at every step. When you're in flow, you know what needs to be done. A musician knows which notes to play and a rock climber knows which steps to take, almost without thinking. 

Equally, a scientist who has made major contributions to her domain has clear goals on how to solve certain problems. She knows the gaps in her field of knowledge and she wants to close them. 

Second, a creative gets immediate feedback on his actions when he's in flow. A musician hears with all his senses if his notes are true, and a climber feels instinctively when his moves are correct.

Creative people also know how to give themselves effective feedback. Scientist Linus Pauling, who won the Nobel Prize in chemistry, has said that the difference between being creatively productive and nonproductive is the ability to evaluate your ideas and separate the good from the bad.

Third, flow distorts a creative person's sense of time. An hour of violin practice may feel like just five minutes to an artist in flow. 

Poet Mark Strand once said of the creative process, "The idea is to be so saturated with it that there's no future or past, it's just an extended present..."

### 6. A creative person’s surroundings can have a positive or negative impact on his creative output. 

Being in the right place at the right time makes a difference in the creative process. Choosing the right creative place is however challenging. 

A person's social, cultural and institutional context all play a role in the creativity he can generate. So it makes sense for creative people to move to the centers of information and action within their individual domain. 

New York, for example, is a hot spot for aspiring artists as it's a place where they can follow trends and events first hand. Scientists equally are attracted to institutions where they can work with other great thinkers in their field. 

An institution such as the Bell Laboratories provides a perfect location for theorists and experimentalists to work together, as offices are proximate to each other, allowing for easy sharing of ideas and inspirations. 

A person is also more creative when his environment is inspiring. Many cultures since ancient times have believed that your physical environment can have a big impact on your thoughts. Chinese sages wrote poetry on small island pavilions, while Hindu Brahmins wandered into the forest, seeking the divine; Christian monks chose beautiful spots in nature to build monasteries. 

When the composer Franz Liszt visited the Italian village of Bellagio on Lake Como, he wrote, "I feel that all the various features of Nature around me...provoked an emotional reaction in the depth of my soul, which I have tried to transcribe in music." New and beautiful environments can certainly inspire creative insight. 

Yet new environments aren't necessarily always beneficial to the creative process, however. The other creative steps, like preparation and evaluation, often play out more effectively in familiar, comfortable settings. 

Perhaps that's why composer Johann Sebastian Bach never traveled far from his native Thuringia in Germany, and France's Marcel Proust wrote his masterpiece in a dark study. Even Albert Einstein laid out the theory of relativity while writing at his kitchen table.

### 7. The myth of the child prodigy is just that; not every creative genius is made at birth. 

People tend to assume that creative individuals are born with their talent. The story that Italian Renaissance painter Giotto at an early age could draw a perfect freehand circle is just one example of how society expects a genius to be born, not made.

Granted, many creative people show talent early on in life, like composer Wolfgang Amadeus Mozart, who did display exceptional musical skills as a child. Yet not all adult creative geniuses were once child prodigies. 

Many stories about child geniuses are in fact just myths. Giotto's origin story is often told like a fairy tale, with an introduction such as, "Nothing is known about his youth — only legends."

Many creative adults certainly didn't show early signs of unusual talent. Albert Einstein and Charles Darwin weren't child prodigies. Winston Churchill's skill as a statesman blossomed in his middle age. Writers Leo Tolstoy, Franz Kafka and Marcel Proust failed to impress their mentors when they were young. 

In short, when examining the childhoods of creative people, it's difficult to find consistent patterns. 

Yet one of the few patterns that _does_ arise is that schooling seems to have little impact on a creative person's life. The author in his research was surprised to find how few creative people described special relationships with teachers in school, or cited school at all as a source of inspiration. 

Prominent biologist George Klein said that all but one of his teachers were mediocre. As a teenager, he learned more about philosophy and literature from his friends than his classes. 

Other prominent creatives, such as Albert Einstein, artist Pablo Picasso and writer T. S. Eliot, also said that their time in school contributed little to their later accomplishments.

> _"Children can show tremendous talent, but they cannot be creative…"_

### 8. Creative people don’t have traditional careers after college; often, they make their own career path. 

A person's experiences in early adulthood usually have a big impact on who he or she becomes later in life. The same holds true for creative people. 

This is why college or graduate school are commonly high points in a creative person's life. Creative people often move out of provincial areas where they might have felt out of place. For some, college might be the first time when they have mentors and peers who appreciate them. 

Creative people also often find their voice or calling in college. American poet Anthony Hecht, for example, loved math and music as a teenager, but discovered his love for literature in college and decided only then to become a poet.

College also allows a lot of people to be independent for the first time, which is vital for creativity to blossom. David Riesman, the famous American social scientist, finally had the freedom to study law instead of medicine during college, despite his father's wishes. 

And after college, creative people usually don't pursue "careers," at least in the traditional sense of the term. 

Most typical graduates find an entry-level job straight out of college and work for a time until they move on to higher corporate positions. Creative people, on the other hand, often need to invent their career path and even their own jobs. There weren't any psychoanalysts before Sigmund Freud, for example, or any electricians before Thomas Edison!

So creative people don't just develop new ways of thinking, often they're the first practitioners of the new domains they create. They pave the way for others to follow.

> _"Creative people don't have careers; they create them."_

### 9. Creativity doesn’t decline with advanced age; in fact, elderly creatives are often more energized. 

What's the relationship between age and creativity? Studies have suggested that a person's creativity peaks in his 30s, and that people over 60 rarely produce great creative works. 

More recent studies suggest otherwise, however. They indicate that a person's creative output isn't curtailed by advanced age. 

In fact, a lot of creative people claim that their mental abilities don't decrease with age. Elderly creative people might lament that their _energy_ declines, but that's different than weaker mental skills. 

Heinz Maier-Leibnitz, a prominent physicist in his 80s, said that his desire to work has _increased_ as he's gotten older, but his energy no longer keeps up with what he wants to do. 

A large number of creative individuals say that their mental abilities have stayed the same or even _improved_ as they've grown older. They accumulate more experience and understand the world better as time goes on, so they just keep getting better at what they do.

Isabella Karle, a crystallographer in her 90s, believes her life experience has deepened her knowledge and allowed her thinking to become more complex. Physicist Linus Pauling had his most prolific publishing period between the ages of 70 and 90. 

Many of the elderly creatives the author interviewed were still very enthusiastic about their work, and described current projects in great detail. Old age has allowed them to explore the world even more and broaden their interests by reaching out into other fields such as politics, human welfare or environmental studies, adding depth to their work. 

Not one creative feared death, either — they were content to keep doing what they loved.

### 10. Our survival as a species depends on creativity, and we need to take steps to foster it. 

The world's problems are many: overpopulation, dwindling energy sources and global warming, are just a few examples. Our future depends on the development of innovative new solutions.

More directly, this means our survival as a species depends on _creativity_. 

While our genetic makeup as humans has remained mostly the same, society has changed dramatically over the centuries. Cultural change has come through _memes_, or the ideas that we as individuals and groups learn and pass on to other individuals and groups. Memes determine what we know, what we believe in and what we do. 

Memes don't come from external influences, like natural predators or climate change. As humans, we create them ourselves and as such, memes have an enormous impact on our future. Crucially, we need to ensure we're being guided by the right memes and that these memes work to foster more creativity in the future.

There are ways we can encourage creativity in society. We need to help children get early access to as many domains as possible, so they have the chance to discover their passions early on.

Children also need better access to educational materials. Opening up the worlds of scientific articles, news gathering and the arts will encourage more new voices in many different domains. 

Crucially, we need domain materials that any layperson can understand, as to not discourage those people who may be interested but as yet unskilled. If a domain appears closed or inaccessible, fewer young people will put in the effort to enter it, which stymies creativity. Clear and transparent communication within all domains is important.

> _"Whether we like it or not, our species has become dependent on creativity."_

### 11. Harness your own creativity daily, by taking time to stop and observe the world around you. 

So what lessons can we learn from the lives of creative people? Let's identify some practical advice from the information we've covered so far.

The first step toward becoming more creative is to cultivate a passion for curiosity. Often we lose our sense of wonder as we get older, as life becomes routine. Creative people, on the other hand, stay curious late into their old age, even into their 90s!

So stay curious! Try to find something that surprises you each day. Take the time to pause, stop and try the new dish on the restaurant menu or actually _listen_ to your colleagues at the office. You can always find something new to learn; it doesn't have to be earth-shattering. Everything and anything can be potentially interesting.

Too often when something catches our interest, like a song or a curious flower, we think about it for a second then move on. But making a serious effort to learn is a crucial part of the creative process. So chase your interests down — don't let them get away! You never know where a new interest might lead you. 

Once you awaken your creativity, it's important to protect it from distractions. Creative people develop habits that help them focus their attention and avoid things that slow them down.

Make an effort to take control of your personal schedule. What you do during the day might not be supporting your creative goals. Figure out when you're most productive: early in the morning or late at night? Give yourself time to focus when you have the most energy to do so. 

Do everything you can to foster your own unique way of thinking, and creativity will become a way of life.

### 12. Final summary 

The key message in this book:

**Creativity may seem an elusive concept, but as a process it can be broken down into certain steps. Creativity usually happens in a system that contains a domain, a field and a person. Creative people pull inspiration from life; they work in a "flow" state and even stay creative far into old age. Importantly, the survival of the human race depends on creativity, so we as a society need to encourage its development. So work to cultivate creativity every day!**

Actionable advice:

**Enhance your creativity by personalizing your workspace.**

Create a space that caters to your specific creative needs. In which kind of space are you most productive? Think about it and experiment with different situations, then put in the effort to build the perfect creative space for yourself. 

**Suggested** **further** **reading:** ** _Flow_** **by Mihaly Csikszentmihalyi**

_Flow_ explores how we can experience enjoyment in our lives by controlling our attention and strengthening our resolve. This is achieved by being immersed in an activity or subject that makes us neither anxious if it's too hard, nor bored if it's too easy. In this "flow state" we lose our self-consciousness, selfishness and sense of time. Using goal-setting and immediate feedback, we can achieve a state of flow that improves our relationship with work, increases our self-worth and gives our lives meaning.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Mihaly Csikszentmihalyi

Mihaly Csikszentmihalyi is a professor of Psychology and Management at Claremont Graduate University's Drucker School of Management in Claremont, California. Author of _Beyond Boredom and Anxiety_, _The Evolving Self_ and _Flow_, Csikszentmihalyi has also written articles for _Psychology Today_, the _New York Times_ and the _Washington Post_.

