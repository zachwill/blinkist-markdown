---
id: 57764d9586883200034f47c0
slug: crippled-america-en
published_date: 2016-07-08T00:00:00.000+00:00
author: Donald J. Trump
title: Crippled America
subtitle: How to Make America Great Again
main_color: D4322E
text_color: BA2C28
---

# Crippled America

_How to Make America Great Again_

**Donald J. Trump**

In _Crippled America_ (2015), American businessman and billionaire Donald J. Trump diagnoses the problems America is facing today and explains how he will truly make America great again. With disastrous trade agreements hurting our middle class and the government failing to protect us against ISIS, it's no wonder America doesn't win anymore. Some major changes need to be made — and Trump is going to make them.

---
### 1. What’s in it for me? Learn how Trump wants to make America great again! 

America is often called the greatest nation in the world. But over the course of the last decade, political correctness and the disastrous Obama administration have left America crippled. This, at least, is how business leader, TV celebrity and presidential candidate Donald Trump sees it.

From his perspective, everything from defense, education, immigration and energy policies has gone south during Obama's presidency, and the only way to remedy these policies is to elect a president who isn't afraid to say what he thinks, even if it offends some people. And Donald Trump fits the bill perfectly.

In these blinks, you'll find out

  * how Trump would make the Mexican government pay for a wall on their border;

  * what Trump thinks about the federal government; and

  * how the myth of climate change has steered US policies the wrong way.

### 2. The media has treated Trump unfairly, but their coverage has also benefited his campaign. 

Donald Trump has received a lot of media coverage since he became a successful businessman in the 1980s. Once he announced his candidacy for the US presidency, however, the mainstream press became inaccurate and unfair in their treatment of Trump.

This attitude was on full display during the televised Republican debates.

Moderators such as Megyn Kelly not only asked unfair questions, they also misquoted Trump and got the other candidates to respond to these distorted statements without giving Trump a chance to set the record straight.

This media bias is also apparent in how news outlets treat personal attacks made on Trump by other candidates as big stories. Yet, attacks like these aren't news; the media should instead be focussing on issues that really matter, like the disastrous nuclear deal between Iran and the United States.

But Trump understands the real reason the media treats him this way: it's no secret that by attacking Trump journalists like Megyn Kelly get time in the spotlight, along with bigger ratings for their shows.

But all media coverage, however negative, only benefits Trump at the end of the day.

The truth is that these constant attacks by the media actually provide Trump with non-stop publicity, all free of charge. Trump even admits that he sometimes takes advantage of this situation to say crazy things to grab more attention.

This results in front-page or top-of-the-hour headlines that can last weeks, allowing him to get his message out to the public.

So, though Trump wishes the media's reporting would be more honest and accurate, he doesn't mind their mutually beneficial relationship.

### 3. To keep Americans and their jobs safe, we have to build a wall on the US-Mexican border. 

A major part of Trump's campaign platform is to crack down on illegal immigration. In order to do this, he proposes that the government build a wall along the US-Mexican border.

Trump believes that this would improve current immigration policies, which are putting American jobs and safety at risk.

The danger of US citizens losing their jobs to immigrant workers will only increase if we continue to permit the massive amount of illegal immigration we see today.

This point of view doesn't mean Trump has anything against immigrants; his mother was a Scottish immigrant, and he loved her dearly. However, illegal immigration is a whole other story. And if we continue to ignore the current levels of illegal immigration, we are actually doing a disservice to the immigrants who are trying to legally immigrate to the United States.

A more dangerous and problematic issue, however, is the security risk posed by the US-Mexican border.

The problem isn't solely illegal immigrants from Mexico. People crossing this border include Central and South Americans, as well as people from the Middle East. Trump is not suggesting that all of these people are criminals; he argues that we have no way of knowing who is and who isn't, because current US policy is severely lacking when it comes to providing security along this border.

Now, building a wall on the Mexican border might sound improbable, but, in Trump's eyes, this is a realistic effort that won't cost the United States a penny.

After all, if the Chinese could build a huge wall more than 2,000 years ago, why can't Americans do it today? Plus, while the Great Wall of China covers 13,000 miles, the southern border wall only needs to be about 1,000 miles long.

More importantly, Trump believes the United States won't have to pay a cent to build it. He proposes pressuring the Mexican government into completely funding the wall's construction.

Trump envisions two ways of getting this done: by threatening to cut off the payments sent home by Mexican workers in the United States, or by increasing the visa fees for Mexicans entering the United States.

Additionally, to encourage Mexico's cooperation, Trump would also threaten other countries with dire consequences if they failed to enforce the same policies.

### 4. Our allies don’t respect us and our enemies don’t fear us, and this has got to change. 

We live in one of the most dangerous times in human history, and Trump considers President Obama's foreign policy decisions completely disastrous for America, not to mention the rest of the world. The United States needs a president who not only recognizes this dire situation, but can do something about it.

Trump would fix things by first making sure the US military is the strongest in the world. Not only would this instill fear in enemies; it would also inspire trust in US allies.

As Trump sees it, the US military has deteriorated in recent history, and is currently in terrible shape. Personnel is limited and the quality of equipment is pitiful.

To turn this around, Trump would increase the military budget, which would also help the US economy. By allowing the military to place more orders for new equipment and airplanes, thousands of Americans would be put to work.

And once the United States regains its position of military power, Trump would make sure the whole world knows it. He would no longer follow the philosophy of "speaking softly while carrying a big stick" — he would speak loud and proud about military strength.

But Trump would only engage in warfare when American security is at stake, ensuring that enemies remain fearful.

In this regard, he sees past US military involvement in Iraq as a big mistake, since they posed no clear threat to American security.

On the other hand, Trump sees ISIS as the largest threat the world has faced in a long time. With their brutal regime operating out of Syria and Iraq, he believes they could launch a 9/11-style attack on the United States at any point.

Trump isn't opposed to putting military troops on the ground to destroy ISIS. He believes we shouldn't be satisfied with Obama's limited air response, and should strike ISIS repeatedly until they cease to exist.

### 5. The American education system has to be overturned; our children deserve better. 

Having attended the best business school in America, the Wharton School of Finance, Trump is familiar with the importance of education. He knows at first hand that a quality education is crucial to future success.

In fact, Trump believes that the federal government should no longer stand in the way of Americans getting a good education.

When Trump looks at current education initiatives — such as Obama's Common Core, which sets educational standards that all American students should strive to meet — he sees a downright disaster that needs to be reversed.

For Trump, such programs are nothing more than Democrats brainwashing children with progressive values, an approach that's resulted in the United States dropping to 26th place in global-education rankings.

And since Common Core and other federal education initiatives have failed so miserably, Trump suggests that the very existence of the Department of Education should be called into question. Indeed, if Trump had his way, he'd immediately get rid of the federal institution.

Trump believes that teachers and parents have a better idea of how to educate their children than bureaucrats in Washington do.

So, instead of getting rid of grades and trying to help failing children feel better about themselves, Trump would foster more competition in the education system.

To do this, Trump suggests giving parents the authority to choose where their kids go to school, which would cause schools to compete with each other. As with other businesses, competition will either lead to schools improving and becoming more efficient or, in the case of schools that don't perform well, to their closing down.

Plus, with less federal regulation in the education system, Trump believes charter schools will finally be able to flourish. This would be a good thing. Indeed, a Stanford University study which looked at 41 urban areas revealed that students from charter schools were outperforming public-school students in advanced math and reading.

### 6. We have to acknowledge that Obama’s focus on climate change has shifted US energy policy in the wrong direction. 

When Obama gave his State of the Union address in 2015, he named man-made climate change, rather than an existing threat like ISIS, as the most dangerous threat to humanity. In Trump's view, this kind of incompetence is getting us nowhere.

Unlike Obama, Trump would reexamine our stance on climate change and clean energy.

His first step would be to help people understand that climate change is a natural phenomenon — not a man-made one. And that it's not as bad as liberals would have you believe. For example, Trump points to tornado activity that, a hundred years ago, was far worse than it is now. 

But climate change does pose one problem for Trump: by brainwashing Americans into believing it is man-made, liberal politicians caused corporations to transition toward clean-energy technology. As a result, the country has wasted billions of dollars on unnecessary technological advancements.

Trump's second step would be to get the country to rely less on foreign oil and take advantage of America's own natural resources.

Government reports state that America has hundreds of years worth of its own natural gas. And a study done by Rice University in Houston, Texas, suggests that the United States has enough oil to last at least 285 years.

This leads Trump to ask two important questions. First, why should the United States allow foreign powers and OPEC to set the global oil prices that affect everyday Americans? And second, how much longer can the United States rely on foreign oil when much of it comes from the increasingly unstable Middle East?

Trump is ready to provide the leadership that is needed for the United States to take matters into its own hands.

This kind of leadership would allow for real progress in the energy industry. In particular, Trump wants to see projects like the Keystone XL extension move forward. This would bring oil from the Gulf all the way to Alberta, Canada — supplying the entire Midwest.

### 7. Trump would throw out Obamacare on day one and replace it with something that Americans can actually afford. 

What separates Trump from other candidates is that he is a businessman. This means he has experience providing health care to thousands of workers, and so, unlike other politicians, he knows what he's talking about when it comes to affordable options.

For Trump, the first step to creating a health-care system that works is to get rid of Obamacare, which has put doctors out of practice and forced everyday Americans to pay higher premiums.

Trump even suggests that Obama should be taken to court for fraud since his health care overhaul only became law after he dishonestly stated that Americans could keep the same doctor and coverage that they had before.

Once Obamacare is repealed, Trump would implement effective health-care solutions, and he stresses that he wouldn't touch Social Security or Medicare.

And while Trump admits that he used to advocate for the kind of single-payer system that exists in the United Kingdom, he believes that times have changed, and that such a system is no longer viable. He also believes that the poorest Americans still deserve affordable health care, and he wants to help them get it. Furthermore, he maintains that those who can't afford any coverage should be provided with free, basic treatment.

Unlike other politicians, though, Trump doesn't claim to have prepackaged answers to every problem. As a businessman, Trump would use his experience to bring people together to find the best solution.

In this case, he'd find the best health-care professionals, put them in a room together and tell them that they can't go home until they hammer out a solution to America's health-care woes.

### 8. Trump would provide the leadership America needs to fix its crumbling infrastructure. 

America has fallen behind in many areas, including keeping its infrastructure up to date. In fact, it's gotten so bad that it now ranks 12th on the World Economic Forum.

Trump finds it shameful how bad US infrastructure has become, and he pledges to tackle this problem before it's too late.

To begin with, Trump wants to start investing more money into fixing roads and airports. He believes that if China and Europe can spend nine percent of their budget on infrastructure, then the United States should be spending far more than the current 2.4 percent.

Trump points to how this lack of spending can even harm the country's productivity. He cites the yearly two-hundred-billion dollar loss due to infrastructure problems, such as Americans sitting in traffic jams.

But it's not only sapping US productivity and money; it's also putting our safety at risk. Reports show that one in nine bridges in the United States are in danger of collapsing. In the last 35 years, 600 bridges have collapsed.

Trump will be the president that can get America's infrastructure working again; his business experience has taught him how to actually get things done.

For example, at one point in time, New York City was trying to build a simple skating rink. The project was going to take seven years. But when Trump took over, it was finished in four months and he actually managed to save the city money in the process.

This is the kind of leadership Trump would bring to rebuilding the American infrastructure, and his spending would also result in jobs and economic growth.

He suggests that with $1 invested in repairing roads, bridges and airports, $1.44 would be brought back into the economy through improved trade.

Furthermore, these repairs would create numerous jobs. Indeed, a figure from the Senate Budget Committee estimates 13 million Americans would be put to work fixing America's infrastructure.

### 9. Final summary 

The key message in this book:

**Americans are sick and tired of a dysfunctional government that can't get basic things done. Donald Trump has a plan to turn this country around, put Americans back to work and protect the country from external threats. On top of that, he'll get all of this done efficiently and quickly, as only an experienced businessman could.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Outsider in the White House_** **by Bernie Sanders and Huck Gutman**

_Outsider in the White House_ (2015) tells the story of Bernie Sanders, the presidential candidate and US senator. From marching for civil rights in the 1960s to campaigning against big money in politics as a 2016 presidential candidate, Sanders has always been at the forefront of US left-wing politics. First published in 1997 as _Outsider in the House_, this updated version of Bernie Sanders's autobiography traces his lifelong fight for social justice and economic fairness.
---

### Donald J. Trump

An American businessman, Donald Trump is a candidate for the Republican nomination in the 2016 presidential election. He is also the well-known host of the reality shows _The Apprentice_ and _Celebrity Apprentice_. His other books include _Trump: How to Get Rich_ and _Trump: The Art of the Comeback_.

