---
id: 570a18f08b5d6e00030000fc
slug: crisis-caravan-en
published_date: 2016-04-14T00:00:00.000+00:00
author: Linda Polman
title: Crisis Caravan
subtitle: What's Wrong with Humanitarian Aid?
main_color: E6B74E
text_color: 735B27
---

# Crisis Caravan

_What's Wrong with Humanitarian Aid?_

**Linda Polman**

_The Crisis Caravan_ (2011) is about the complexities and pitfalls that come with delivering humanitarian aid to conflict zones. Though aid is usually provided with nothing but good intentions, there are political, social and economic obstacles that can cause it to do more harm than good. These blinks outline the reasons aid work often fails, and offer advice on how we can improve it.

---
### 1. What’s in it for me? Find out what’s wrong with humanitarian aid. 

We've all experienced it: we see something terrible on the news — starving children, for instance, or wounded people in a war-torn country — and we want, somehow, to help. And, thanks to humanitarian organizations, we don't have to rush to the region in crisis; all we have to do is donate to the organization and they'll provide the aid for us.

But does that donation actually make a difference?

As you keep following the news, you may get the impression that even millions of donations won't alleviate the suffering. In fact, sometimes, the more aid organizations there are, the more some conflicts seem to drag on and the stronger some warring factions seem to get. Indeed, you've probably heard that, for every dollar given, barely ten cents make it to the intended recipients.

So what is it that doesn't work about humanitarian aid, and how can we change that?

Read on and find out

  * what terrorists love about refugee camps;

  * why it sometimes pays for a poor country to hire PR specialists; and

  * why Caritas International gave money to the Tamil Tigers.

### 2. Modern aid organizations often fail to stick to humanitarian principles because they’re now commercial enterprises. 

What first comes to mind when you hear the term "humanitarian aid"? Probably, you think of the ICRC — the International Committee of the Red Cross.

Founded in 1863 by Henri Dunant, the Red Cross was the first Western humanitarian aid organization, and countless other aid organizations have sought to emulate its commitment to the principles of neutrality, impartiality and independence.

Unfortunately, however, today's organizations often fail to uphold these principles. This became clear in 1994, during the Rwandan genocide, when Hutus slaughtered hundreds of thousands of Tutsis.

When armed Tutsi forces tried to fight back, thousands of Hutus fled to the Democratic Republic of the Congo, where 250 aid organizations provided them with medical care, food and shelter in refugee camps in Goma. The organizations present included various UN groups, the International Federation of Red Cross and Red Crescent Societies, Oxfam and several branches of Caritas and CARE.

These refugee camps effectively served as strategic centers for the armed Hutu forces, who then carried out further killings of the Tutsi people. The extremist Hutu government simply relocated to Goma and continued the genocide.

The aid organizations knew about it, too, and failed to stick to the principle of neutrality by siding with the Hutus.

This happened largely because humanitarian organizations are now commercial enterprises.

Consider what happened when an Irish aid organization tried to cut down on supplies in protest of Hutu violence. When they stopped giving out extra soap and mattresses, another organization swooped in and started giving them out.

Aid organizations, instead of working together, are often in competition. They incur sky-high costs, so once they set up in a crisis zone, they try to earn back as much of their investment as possible, which they can only do by winning donor contracts. In the competition for donations, humanitarian principles are often set aside.

> _"For the aid organizations in Goma it was a matter of feed the killers or go under as an organization."_

### 3. Humanitarian aid is regularly abused by warring factions in conflict areas. 

Humanitarian aid is supposed to be about easing human suffering. Sometimes, however, it fails to make life better for the people it purports to serve. In some cases, it can even make life worse. How is this possible?

Humanitarian aid is often abused by warring factions. Providing aid in conflict zones is tough and often requires aid organizations to make problematic negotiations.

When Sri Lanka was hit by the 2004 tsunami, for example, aid organizations couldn't help with reconstruction without negotiating with the Tamil Tigers, a guerilla force seeking to establish Tamil as an independent state. The Tigers taxed every shipment of building materials that Caritas International imported. This increased Caritas's costs by 25 percent — and helped fund the Tigers.

UNHCR, the UN refugee agency, faced a similar problem in former Yugoslavia during the wars of the 1990s. Armed Serb forces set up roadblocks along key routes and UNHCR had to surrender 30 percent of the aid supplies they delivered.

Warring factions can also take advantage of refugee camps; indeed, it's now common for so-called _refugee warriors_ — fighters in conflict-ridden zones — to hide out in them.

Refugee warriors can regroup and reorganize themselves in the camps, where defenseless civilians also serve as human shields against attack. It's estimated that they constitute as much as 15 to 20 percent of the total population of refugee camps worldwide.

When Sudanese rebel refugee warriors launched a fight against their government from camps in Ethiopia, they referred to the civilian refugees there as "aid bait" — a means of obtaining more humanitarian aid. And of course, when refugee camps become strategic combat points, it only makes life more dangerous for the civilians in them.

### 4. Depending on their own cost-benefit analysis, aid organizations cherry pick which crises to help. 

In 2004, Kofi Annan, the former Secretary General of the United Nations, tried to persuade wealthy member states to provide more funding for projects in the world's poorer nations. It's their right, he said, to receive funding. But that's not really the case. Giving aid is a favor, not a duty.

In other words, aid organizations are free to ignore a crisis if they so choose. To return to the example of the Rwandan genocide: the Tutsis received almost no humanitarian aid; all the money and resources went to Hutus in camps in Goma and the neighboring Democratic Republic of the Congo.

Indeed, it's not unusual for organizations to ignore certain crises. In fact, that's the rule, not the exception. Jan Egeland, a Norwegian diplomat and aid worker, even compared receiving international aid to winning a lottery in which 25 equally desperate groups have to participate each week. Only one can win.

Humanitarian organizations choose which crises to focus on based on their own cost-benefit analysis. They're more likely to earn back their investment if they get a lot of donations, so they pick regions that attract more donors. Donors are attracted to crises that get more media attention, and so that's where the aid organizations go.

For a crisis to win the aid lottery, it has to get more attention, which means the victims must stand out from victims of other ongoing crises. Countries in crisis areas are now induced to fight PR battles to gain more support for their causes. In Palestine, for example, press officers are employed to try to garner international attention for the plight of the people.

> _"If you don't have starving babies you don't get the money."_ \- John Graham of _Save the Children UK_

### 5. Aid organizations manipulate the media to gain attention for their humanitarian missions. 

Few people from stable countries personally travel to conflict zones; instead, they rely on the media to keep them up to date on what's happening. But aid organizations, in an attempt to draw more donors, often try to gain more media attention. And they often resort to unscrupulous methods.

Aid organizations tend to exaggerate their reports from humanitarian missions. This happened when cholera broke out in a refugee camp in Goma during the Rwandan genocide. The outbreak was covered extensively by a number of journalists, and an international fund-raising campaign was launched almost immediately. A UN representative started holding daily press conferences in which each aid organization gave updates on what was happening.

The organizations began competing, each trying to present the highest death toll, and the official number of deaths per day quickly climbed from 600 to around 3,000.

The organizations also failed to report that some of the dead had really been murdered by extremist Hutus for their suspected disloyalty to the Hutu regiment that was based in the camp.

Why did these groups exaggerate their figures? Again: money. Higher death tolls mean more media attention, and more media attention means more donations. Exaggerating the cholera death toll was profitable.

Humanitarian organizations also try to garner attention by influencing journalists. Some organizations even embed journalists in their missions to make sure they witness the suffering there. They offer free flights and provide journalists with expensive chauffeurs and interpreters. In exchange, the journalists give them publicity.

Many journalists develop cozy relationships with aid organizations as a result, which makes it even harder for them stay neutral.

> _"Confronted with humanitarian disasters, journalists… suddenly become the disciples of aid workers."_

### 6. Victims of crises aren’t protected against unqualified humanitarian aid. 

You might expect humanitarian aid workers to be extremely adept at dealing with the intricacies of crisis zones. Unfortunately, this isn't always the case.

Humanitarian missions teem with unqualified amateurs. Since major aid organizations like the Red Cross and UNHCR have been known to make grave errors, more and more amateurs are coming to believe that _they_ can do a better job. Some now set up their own organizations, which are collectively referred to as "My Own NGO"s, or MONGOs.

MONGOs aim to deliver fast and cheap aid, and to sidestep the bureaucratic processes of traditional NGOs. However, they often only end up contributing to the crisis they're attempting to ameliorate.

In Afghanistan, for example, religious American MONGOs gave out Bibles along with the meals they distributed. This, naturally, created tensions with the Islamic Afghan government.

When MONGOs make mistakes like this, they often go unreported. MONGOs are typically funded by larger aid organizations that fear criticism of the MONGO will reflect badly on them.

All this means that crises victims aren't protected from the dangers of unqualified aid. This happened in Freetown, Sierra Leone, in a camp for amputees whose limbs had been cut off by rebels and soldiers during the decade-long civil war.

MONGOs and private individuals took several child amputees from their families and relocated them to the United States and Germany, supposedly so they could receive better medical treatment. However, the children had already been receiving appropriate medical treatment in the camp — there was no need for them to be taken away.

Victims of unqualified aid, like these children, have no protection against this kind of intervention. There aren't any laws regulating the quality of humanitarian aid.

> _"...humanitarian aid exists in a free market, where anyone who chooses to can set up a stall."_

### 7. Humanitarian aid work has failed in post-9/11 Afghanistan. 

Let's take a closer look at a place where humanitarian aid _hasn't_ worked: Afghanistan.

The United States' War on Terror began in Afghanistan one month after September 11th, and with it came one of the greatest humanitarian aid projects of our time. It failed. Why?

There are several reasons, but one of the main ones is that aid work in Afghanistan is often done through intermediaries, and money gets lost along the way. Aid organizations subcontract smaller organizations to carry out their projects, and those organizations usually subcontract the work again. A project typically goes through four to seven of these intermediaries, and each one absorbs part of the funds for their wages.

The United States Agency for International Development (USAID), for example, once set aside $15 million to build a road from Kabul to Kandahar. The money was then passed through three other organizations, each of which took six to twenty percent of the project's funding. In the end, there was only enough money left to build a low-quality stretch of tarmac that was barely an improvement on the road it replaced.

Aid organizations also usually fail to supervise their projects because they're trying to keep them secret from terrorists. Fearing the Taliban, organization headquarters are usually hidden and people rarely go out to check on the projects they're financing. That also means they're not there to intervene when funds are misspent.

Another problem is that, in Afghanistan, the line between Western military strategy and humanitarian aid has been blurred. The United States and Europe expect aid organizations to work as _force multipliers_ — that is, to support Western military strategy and supply aid when needed.

However, if humanitarian aid has a political slant, enemies of the United States consider aid organizations to be their enemy, too. In Afghanistan, aid organizations became targets for the Taliban, which forced these organizations to seek protection from military powers, tightening the bond even more. It's a vicious cycle.

> An estimated 35 to 40 percent of all international aid to Afghanistan is misspent.

### 8. We have to criticize humanitarian aid work if we want to improve it. 

So how can we make humanitarian aid actually work? There's no simple answer to this question, but if we want to improve humanitarian aid, we need to change our understanding of it.

First, we need to question the principle of giving aid at any cost. Blind adherence to this principle caused the humanitarian world's "mother of all controversies" during WWII, when the Red Cross cooperated with Nazi Germany.

The Red Cross knew about the Holocaust but nonetheless kept silent, fearing that lives would be lost if there was a ban on working within Nazi territory. They felt they had to uphold the principles of neutrality and impartiality, even in the face of Nazi terror. Today, the Red Cross considers this a "tragic mistake."

That tragic mistake has been repeated several times since, however. And, if we want to avoid such mistakes in the future, we need to reexamine the core values of humanitarian work. We have to consider how successful aid missions have been in the past, and whether the good they did outweighs the harm.

Humanitarian work also needs to become more transparent. All aid organizations promise to "learn" and "improve" with every new campaign, but how can we ensure that they actually do that?

In 1997, a number of aid organizations, in an attempt to address this issue, founded the Active Learning Network for Accountability and Performance in Humanitarian Action (ALNAP). However, after monitoring accountability efforts for eleven years, ALNAP concluded that humanitarian aid work still lacked a coherent means for assessing performance.

Aid organizations must be held more accountable, too. Journalists could be of great help, here — by staying neutral, for instance, and resisting the benefits that organizations offer them.

All in all, the first step toward improving humanitarian aid is to assess it more carefully. Humanitarian aid should never be above criticism.

> _"[The] crisis caravan moves on whenever and wherever it sees fit, scattering aid like confetti."_

### 9. Final summary 

The key message in this book:

**We need to think more critically about how aid work should be carried out. Aid organizations have now become commercial enterprises that strive to manipulate the media in order to garner more donations. They're manipulated by warring factions, as happened in the Rwandan genocide, and they're often poorly organized. When an aid organization makes big mistakes, there's little protection for the victims. The nature of humanitarian aid needs to change if we want to ensure that it really helps the people it aims to serve.**

Actionable advice:

**Before you donate, research.**

Before you give money to an organization, read as much as you can about them. Figure out where your money will go. When you make more informed decisions regarding your donations, your money does more good for the people you want to help.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Blue Sweater_** **by Jacqueline Novogratz**

_The_ _Blue_ _Sweater_ is an autobiographical look at the author's travels in Africa and how they helped her understand the failures of traditional charity. These blinks also outline why a new type of philanthropic investing, called "patient capital," developed by the author, may be part of the answer.
---

### Linda Polman

Linda Polman is a journalist with over fifteen years of experience reporting from war zones. She is the author of _We Did Nothing: Why The Truth Doesn't Always Come Out When The UN Goes In_, which was shortlisted for the Lettre Ulysses Award for the Art of Reportage.

