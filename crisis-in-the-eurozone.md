---
id: 543e75243065650008400000
slug: crisis-in-the-eurozone-en
published_date: 2014-10-17T00:00:00.000+00:00
author: Costas Lapavitsas and others
title: Crisis in the Eurozone
subtitle: None
main_color: EB4D4B
text_color: 9E3433
---

# Crisis in the Eurozone

_None_

**Costas Lapavitsas and others**

These blinks explain the root of the eurozone crisis in a comprehensive, methodical way. They shed light on the deep structural problems the eurozone is facing and outline scenarios that could help restore competitiveness among the southern peripheral states of the region.

---
### 1. What’s in it for me? Understand what caused the eurozone crisis. 

_Crisis in the Eurozone_ is a summary of the root causes of the eurozone crisis, which hit in 2009. That year, ten banks in the eurozone asked for bailouts and eurozone governments struggled to repay their debts.

These blinks will explain why the crisis hit the countries on the periphery of the eurozone worse than the others despite comparable GDP growth rates among eurozone member states. They'll also talk about possible structural reforms that would prevent a similar crisis in the future.

In addition, you'll learn

  * why Germany wasn't hit as severely by the crisis despite a higher unemployment rate;

  * why austerity measures didn't solve Greece's economic woes; and

  * what would happen if Greece left the eurozone.

### 2. Higher inflation rates explain why some countries suffered more than others in the eurozone crisis. 

The economies in the eurozone area are all connected, but when the euro crisis hit Europe in 2009, different countries were affected in different ways. Some countries, like Germany (in the core of the eurozone), fared better than others, like Greece (on the periphery).

What caused the differing impacts of the crisis?

We can't attribute the phenomenon to _GDP growth rates_, because these were similar across the eurozone.

The _GDP growth rate_ is a metric that helps determine a country's economic health: GDP is a barometer for how much a country produces over a period of time; its growth rates describe the increase in output from one period to the next.

In the eurozone, countries experienced similar GDP growth at the beginning of the 2000s. Greece's GDP growth rate was about 4 percent, whereas Germany's was 3.7 percent.

So if GDP growth wasn't a factor, can _unemployment rates_ explain why Germany fared better than Greece?

Unfortunately, they can't either. In 2006, 8.9 percent of people in Greece were unemployed whereas Germany's unemployment rate was at 11 percent.

Thus, just like GDP growth rates, unemployment rates can't explain the disproportionate impact of the eurozone crisis.

However, there is one thing that can explain the imbalance: Germany and Greece had vastly different _inflation rates_, which turned out to be harmful for Greece's economy.

_Inflation rates_ refers to how much prices increase over time. In 2006, Greece had an inflation rate of 3.2 percent while Germany's was 1.58 percent, or half of that.

The higher rate of inflation meant that Greek products were more and more expensive in comparison to German products, which made Greek companies less competitive.

So higher inflation rates were a big reason that Greece was hit so hard by the eurozone crisis. But what was causing them?

### 3. Differing labor costs explain why countries like Germany were more competitive than countries like Greece. 

We all know that some countries can offer their goods for better prices than others. How can we describe this relative competitiveness?

Experts describe competitiveness with a metric called a _nominal unit labor cost_ (NULC). This analyzes a country's competitiveness by dividing the cost of labor per hour by the productivity of labor.

For example, if a country's average hourly wage is €5 and, on average, one hour of labor produces €10 worth of GDP, then the country has an NULC of 0.5.

Since the measure decreases when employees are working more productively or getting paid less, the NULC is useful for comparing competitiveness between different countries.

If a country has a low NULC, domestic companies can sell their products more cheaply than companies based somewhere with higher NULC rates.

And when you look at the history of the NULC in the eurozone, you'll find that all numbers for this measure have gone up since 1995 — except in Germany.

In fact, the NULC in Germany was almost stagnant from 1995 to 2008: pretty much nothing changed.

In contrast, in countries like Spain, Greece and Portugal, the NULC rose considerably, meaning the wages of workers increased faster than their productivity.

In Greece, the NULC rose by over 60 percent from 1995 to 2008; in Portugal, it rose by 50 percent.

According to economists, Germany's success was the result of the _Agenda 2010 reforms_, which aimed to boost competitiveness by increasing taxes for employees and reducing taxes for employers, which created more room for employer investment.

This policy drove down German labor costs, allowing its companies to be more competitive and sell products at better prices than companies from the periphery.

So how did German companies' competitive pricing impact the eurozone crisis?

> _"The sovereign debt crisis has its roots as much in the performance of Germany as it does in the actions of peripheral countries."_

### 4. The balance of payment describes the difference between how much money flows into a country versus out of it. 

To get a better understanding of the eurozone crisis, let's take a closer look at _macroeconomic theory_.

In macroeconomic theory, all countries have something called the _balance of payments_ to record the country's economic transactions with the rest of the world. This balance is made up of two components: the _current account_ and the _capital account_.

The current account is a nation's net income. It includes

  * the trade balance (the value of exports minus the value of imports) and

  * returns on investment.

For instance, if you receive dividends on an investment you made abroad (because the venture was profitable), this is a positive return on investment, which will positively affect the current account.

There's also a third part of the current account called _net cash flow_. This money comes without consideration or obligations. In other words, loans wouldn't factor into this measure, though foreign aid would.

If the sum of the three parts of the current accounts is positive, a country has a _current account surplus_. On the other hand, if the net income is negative, that's a _current account deficit_, which must be compensated for by money transfers.

Money transfers are important because they're the second part of the balance of payments, called the _capital account._

The capital account is the balance arising from the different forms of money transfers, mostly investments, that flow into the country minus those going out of the country. These can be investments in

  * buildings or machinery construction ( _foreign direct investment_ ) or

  * bonds or shares of companies ( _portfolio investment_ ).

If, for instance, a country has a current account deficit from having more imports than exports, it needs to have an equally high capital account surplus. This can be achieved by selling bonds to finance the deficit.

In the end, this is all recorded in the balance of payments.

### 5. Before the eurozone crisis, countries from the periphery were running a deficit financed by banks from the core. 

Here's what happened in Europe pre-crisis in macroeconomic terms: countries from the periphery, like Greece and Portugal, had a _current account deficit_ from 1994 to 2008.

We already mentioned one reason for this, namely, the decreasing competitiveness of their domestic companies due to increasing wages. The higher cost of products from countries like Greece created less demand for them abroad. This eventually resulted in a decrease of exports.

Simultaneously, countries from the core with a low NULC, like Germany, could produce cheaply. This created a demand for importing their products into the periphery.

As previously noted, these conditions produced a _current account deficit_ in the periphery.

To complicate matters, the money that financed this deficit came from the core.

Germany, in particular, exported large amounts of capital to the periphery countries in various forms. German banks were not only lending money to banks and governments in the periphery but also making investments in machinery and buildings.

Why would banks lend money to countries in the periphery given their bad economic outlook?

There were two reasons banks from the core made these investments.

The first was the 2008 financial crisis and the collapse of the investment bank _Lehman Brothers_, which caused lots of uncertainty in financial markets, making it seem safer to lend money to banks in the periphery rather than to those outside the eurozone, i.e., in the US or UK.

Why? Because everyone believed that even if a bank in the periphery ran into trouble, European governments would act to save it.

Second, these investments were made because they seemed like they could be potentially lucrative. At the time, the return on investments was high and, before the euro crash, nobody thought a collapse was possible.

### 6. Although Spain had more overall debt, Greece had far more public debt. 

When the euro crisis hit, which country do you think had the highest debt? Hint: It wasn't Greece.

In fact, by the end of 2009, Spain had more outstanding overall debt — both absolute and relative — than Greece or Portugal.

Though that's hardly surprising given that Spain's economy was by far the largest of those three countries.

But, even in relative terms, when we assess the amount of debt in comparison to the GDP of each country, Spain _still_ had the highest debt.

How high? In 2009, Spain's debt was at €5,274 billion — five times its GDP that year — while Greece had comparatively little debt — €703 billion — "only" three times its GDP.

So why was Greece seen as more culpable for the crisis than Spain? Because of different kinds of debt.

One kind is _domestic debt_, which exists within a country when a bank lends money to a company in the same country.

Then there's _external debt_, which is a money transfer between countries. This kind of debt is also part of the _capital account_ we discussed previously.

And, for our purposes, there's another important differentiator of debt: _the debt sector_.

If a government is in debt, we call that _public debt_. On the other hand, the accumulated debt of private companies is called _private debt_.

Of all the countries in the periphery, Greece had the highest _public debt_ — far more than Portugal and Spain. In Greece, 53 percent of the debt was public; in Portugal, it was 27 percent; in Spain, 17 percent.

Since Greece had the same amount of domestic and external debt as Portugal, its high _public debt_ was the main thing that set it apart from other countries in the periphery. Meanwhile, Spain had more domestic debt.

But why was it so problematic that the Greek government had so much debt?

> _"The eurozone crisis has many aspects to it, but it is also undoubtedly a debt crisis."_

### 7. The eurozone’s rules of spending and borrowing put Greece in a financial bind. 

If you've ever worked on something as part of a team, you'll agree that it's important to have rules for a successful collaborative effort.

And since all countries in the eurozone were working together to manage one currency, they had to lay down some basic rules. The idea behind them was to avoid one country taking on too much debt and thereby affecting the international value and reputation of the euro.

These rules took the form of the 1997 _Stability and Growth Pact_, which established guidelines on national debt levels. These guidelines were necessary because the countries that made up the eurozone were sovereign and could decide their own _fiscal policy_ (government spending or taxation).

Thus, the national debt limit of each country was set to 60 percent of the GDP. Furthermore, countries weren't allowed to run a budget deficit above 3 percent of their GDP.

To clarify, the national debt is what's owed by a government and a budget deficit occurs when a government spends more than it earns. The latter pushes up the national debt.

So the _Stability and Growth Pact_ coupled with the decreasing competitiveness of its companies put Greece in a bad situation.

Since Greece had such high public debt, it wasn't allowed to engage in government spending to help its domestic economy because its budget deficit would have risen to unacceptable levels.

Although infrastructure projects, e.g., building roads between economic hot spots, might have benefitted industry, Greece couldn't invest in them.

Additionally, the decreasing competitiveness of Greek companies lowered government tax revenues, putting the government in a straightjacket.

Greece was trapped. Was there any way to improve the situation?

### 8. Greece resorted to austerity to solve its financial problems, but it didn’t work. 

Whether we borrow money from a credit card company or take out a home mortgage, most of us pick up a little debt in our lives. And what do you do when you're in debt? You save some money to pay it back.

When governments do this, it's called _austerity measures_. Austerity measures can help restore competitiveness and reduce the public deficit.

And that was Greece's approach. Since the country couldn't engage in government spending to invigorate the economy, it implemented austerity measures with two main goals:

  * First, to bring down the wages in Greece, which would allow companies to sell their products at better prices. Greece did this by cutting wages in the public sector, and the wage cuts eventually spread throughout the whole economy.

  * Second, to decrease public debt by increasing taxes and reducing spending on social services.

Another way the Greek government tried to reduce public debt was through privatization, which usually makes public companies more efficient and enables the government to spend less.

In 2010, the Greek government announced full privatization of casinos and sold 49 percent of the state railway operator. 

Although these austerity measures were all serious efforts to improve the economy and did succeed in lowering the nominal unit labor costs, it couldn't solve Greece's economic woes.

Why? Because all over the eurozone, other countries were taking similar measures which lowered their NULCs, so Greek companies didn't actually become more competitive.

In sum, although austerity did help Greece to a certain extent, the measures didn't solve problems fundamental to the country's economic woes.

> _"In short, the shift towards austerity measures was partly intended to cut fiscal deficits and partly to compress labor costs."_

### 9. Reforms might solve the structural problems of the eurozone, but not without risks. 

As we've seen, countries from the periphery were hit severely by the eurozone crisis. To recover, these countries tried austerity. But so far these measures haven't been fully effective.

Since the eurozone's problems are structural, the best way to fix them is by reforming its structures.

One such structural problem is the disjunction between monetary and fiscal policy.

_Monetary policy_ aims to influence the supply of money, i.e., the amount of it in circulation, and is controlled by one entity: the _European Central Bank_ together with central banks of member states.

However, there's no such single entity to regulate _fiscal policy,_ which relates to taxation or government spending. This ensures the sovereignty of member states, but it means there's no European entity organizing fiscal policy to serve the entire eurozone's interests.

There are, therefore, two possible ways to reform this disconnect and help economies in the periphery.

The first is to increase the _European budget_ (the money member states pay into the central organization). Increasing the European budget by around five percentage points would create a €500 billion safety net for states with high _public debt_.

However, for that reform to be effective, the eurozone would need a separate entity to decide where to spend the money. Thus, member states would lose some sovereignty. Politically, this wouldn't be popular.

A second possible reform is to abolish the Stability and Growth Pact, allowing states like Greece to engage in government spending to grow their economies.

However, the more debt Greece accumulates, the more likely it is to default. And that would lead to the drastic devaluation of the euro, impacting all the member states.

As it stands, although these potential eurozone reforms could improve the situation for countries in the periphery, they bear risks. But, luckily, there is one other option...

### 10. Calling it quits: What would happen if a periphery country left the eurozone? 

Here's some food for thought: What would happen if Greece left the eurozone and introduced its own national currency?

Analysts suspect that introducing a new currency would lower the burden of Greece's debt.

The country would likely reintroduce the drachma, it's old national currency. Let's say the new currency, the _new drachma_, would be introduced with an exchange rate of one. That means every euro in the economy would be replaced by a _new drachma_. If a bank has an external debt of €100 million, these debts would then amount to 100 million new drachma.

After this introduction, the currency would likely quickly be devalued; soon, each _new drachma_ might be worth only €0.5. The bank would still be 100 million new drachma in debt, but the actual value of the debt in euros would have decreased by 50 percent. This process would drastically cut Greece's public debt.

And Greece would have an additional benefit from this scheme: its companies would become more competitive.

Similarly to the process of debt reduction, the devaluation of currency would help companies in Greece sell their products more cheaply, domestically and abroad, because they would be cheaper in euros.

Still, this approach would also involve major risks.

First off, banks from the core of eurozone would suffer substantial losses as a sizable portion of their assets (e.g., Greek government bonds) would be worthless. This could eventually lead to a global banking crisis since core eurozone banks are heavily interconnected with US banks.

Another risk is that investments in the eurozone might decrease because prospective investors would have reason to fear another country exiting from the eurozone, causing further economic losses.

### 11. Final summary 

The key message in this book:

**Major structural problems led to the eurozone crisis, creating a disequilibrium in the economic competitiveness of different countries. Because these problems are so fundamental to the anatomy of the eurozone, the only possible reforms are structural — and each of the reforms comes with their own risks.**

**Suggested** **further** **reading:** ** _The Great Degeneration_** **by** **Niall Ferguson**

The Western world seems to be in crisis. It is faced with huge levels of public and private debt, and the economies of the rest of the world are fast catching up. After 500 years of total global dominance, the era of Western powers could be coming to an end. _The Great Degeneration_ aims to tackle why this is the case. It suggests that a decline in Western institutions is partly to blame. Only by arresting this decline through radical reform can the West recover.
---

### Costas Lapavitsas and others

Costas Lapavitsas is a professor at the University of London's School of Oriental and African Studies. He is also a member of the _Research on Money and Finance_ ( _RMF_ ) think tank.

