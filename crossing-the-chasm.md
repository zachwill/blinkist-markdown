---
id: 509d222ce4b0a13d41357e9e
slug: crossing-the-chasm-en
published_date: 2013-01-01T00:00:00.000+00:00
author: Geoffrey A. Moore
title: Crossing the Chasm
subtitle: Marketing and Selling Disruptive Products to Mainstream Customers
main_color: DD452E
text_color: AB3524
---

# Crossing the Chasm

_Marketing and Selling Disruptive Products to Mainstream Customers_

**Geoffrey A. Moore**

_Crossing the Chasm_ (1991) examines the market dynamics faced by innovative new products, particularly the daunting chasm that lies between early to mainstream markets.

The book provides tangible advice on how to make this difficult transition and offers real-world examples of companies that have struggled in the chasm.

---
### 1. Technological innovations are absorbed in stages by different groups. 

All innovative technological products take time to be absorbed by any community.

Due to differences in attitudes to new technology, this adoption process tends to happen in stages, one group of people at a time, according to the aptly named _Technology Adoption Life Cycle_.

The first to adopt a new technology are the _technology enthusiasts_, for whom technology is a central interest in life _._ They simply want the hottest new technology before everyone else, even if it is still bug-ridden and faulty in places.

Next in line are the _visionaries._ Rather than the technology itself, they are interested in the strategic competitive advantage it could provide. They seek breakthroughs, not minor improvements to the status quo.

The above two groups constitute the relatively small _early market_, which is followed by the vital and far larger _mainstream market_.

Once a technology has proven itself and a clear market leader has emerged, the _pragmatists,_ constituting roughly one third of the entire market, feel safe enough to jump on board. Unlike the visionaries, they are not looking for big changes, but rather incremental benefits gained from standardized, well-supported products. Pragmatists make for extremely loyal customers and thus winning their support is the key to long-term market dominance.

The fourth group, the _conservatives,_ is as numerous as the pragmatists, but suspicious of high-tech. They want simple, high-quality, low-cost products with no hassle involved.

Finally, the _skeptics_ are a small high-tech resistant group who are most often ignored as a customer segment, but who can provide valuable feedback on how your product is failing to meet their expectations.

### 2. Between visionaries and pragmatists lies a chasm where products languish and companies die. 

Ideally, as a product moves through the Technology Adoption Life Cycle, each group of customers will provide references for selling to the next, more skeptical group of adopters. Thus, like Tarzan swinging from vine to vine, the product sustains momentum.

The problem is that in the case of _discontinuous_ or _disruptive innovations,_ meaning products which demand substantial behavioral changes from their customers, this process does not unfold smoothly.

Such products face a wide and unforgiving _chasm_ that divides the early and mainstream markets, i.e. the visionaries and the pragmatists.

The reasons why visionaries and pragmatists buy high-tech products are radically different: visionaries want to provoke major changes, championing a technology even against resistance in their own company; pragmatists, on the other hand, want to minimize discontinuity, and seek incremental productivity improvements rather than major innovations.

Due to these dissimilarities, customer references from the visionaries will not impress the pragmatists. This poses a dilemma, since the pragmatists demand existing references and a comprehensive support infrastructure. They will only buy from established vendors, but without them a company cannot become an established vendor.

This catch-22 is the chasm, and products caught in it tend to languish and die.

While companies can still sell to their existing early-market customers, these sales demand a lot of effort in customization work and are hence low-volume. The high-volume mainstream markets remain just out of reach, and revenues come to a standstill. The company's value is diluted and the original management team may even be ousted by investors.

To avoid this gruesome fate, every company launching an innovative product must have a plan for successfully crossing the chasm.

### 3. To achieve mainstream market success, you must address your customers’ needs fully with a whole product. 

Unlike early-market customers, mainstream customers abhor products that require them to hunt for additional products and services.

Hence, they only want what are known as _whole products_, meaning products that completely satisfy their buying objectives. Pragmatists love Microsoft products, for example, because they know there is a wide infrastructure of supporting products and services available for them already.

In addition to the product that is shipped in a box (the _generic product_ ) they also demand things like installation, support and any additional soft- or hardware needed to fully meet their expectations.

Whole products are the arena where the battle for mainstream markets is won or lost. To reach the pragmatist group and cross the chasm successfully, you must provide a whole product for your chosen target niche. While a good generic product is a great asset, it is neither necessary nor sufficient to become a market leader.

Some of the components needed for the whole product will inevitably fall outside the core competence of your company, so you may need to find partners to address those needs. Such alliances should have the sole aim of developing and marketing a whole product for a specific customer segment.

Consider, for example, a company selling aggregated data on pharmaceutical research. Its customers will expect to have access to multiple sources of data, and to fully meet these demands, the company must partner with a number of data providers like public health agencies, managed care organizations and individual scientists.

### 4. Crossing the chasm is like launching an invasion – you must first secure a niche as a beachhead. 

Entering the mainstream market is an act of aggression: you are invading the existing players' territory, hence you should plan your attack as you would a military invasion.

You start by securing a beachhead. This means targeting a specific market niche within the pragmatist group and becoming its undisputed market leader. From this base you can then expand to other segments until eventually you can dominate the entire market. The niche is the kindling with which you light the fire.

But pursuing a niche requires focus: you must have the discipline to not sell outside the niche. Many companies in the chasm simply cannot resist additional revenue opportunities elsewhere. They wind up selling all over the market without establishing a credible position anywhere, and exhaust their resources on endless customizations to suit each market segment.

A small target niche helps you satisfy the pragmatists' preferences for buying products which are _well-supported_, _well-referenced_ and sold by _market leaders_ :

First of all, the smaller the niche, the easier it will be to win the majority of new orders within it and thus become the de facto market leader.

And a larger proportion of customers in the niche will be talking about your product and positive word-of-mouth references will circulate faster.

Lastly, operating within a niche allows you to develop a standard product specifically for that niche, including the additional components, services and support infrastructure that the pragmatists expect.

### 5. Choose your first niche by finding the most attractive customer segment. 

Choosing the right target niche to invade first usually means making a risky decision without having reliable information to base it on. Hence, it calls for informed intuition rather than analytical reasoning.

A tool to assist in this decision is a process called _target-customer characterization_. This means developing a variety of scenarios in which your product might be used by various customers. You consider the buyer, the end-user and how the product would actually improve the customer's current situation.

Thus one possible scenario for e-books would involve airline maintenance directors buying them for their repair crews. The crews could then access up-to-date repair manuals even on the tarmac, thus resulting in fewer costs from delays.

The goal of this exercise is to come up with a variety of potential customers so you can compare them and find the customer segment with the most compelling reason to buy your product. If your product doesn't solve a dire problem in a given segment, pragmatist customers will tend to postpone their buying decision, thus greatly complicating your entry.

Time is your enemy in the chasm. Within your intended niche, you must be able to find necessary partners and field a whole product that fully addresses customer needs within three months.

Finally, consider the existing competition in that niche. If a competitor has crossed the chasm before you, they will have the exact same advantages you were hoping to gain.

Once you have picked your beachhead, there is no looking back. While it is possible to succeed by invading the wrong niche, hesitation almost always results in failure.

### 6. To effectively position your product in customers’ minds, use a powerful claim to demonstrate market leadership. 

The _positioning_ of a product is the single largest influence on a customer's buying decision. Positioning means the attributes associated with that product in customers' minds, e.g. "Mercedes is a top-of-the-line car."

But different groups of customers value different things. For visionaries, the features of a product, like its speed or size, are key, but for pragmatists the main evidence of a product's value is its position in the market relative to competitors.

As a newcomer without competitors or a market position to boast of, this places you in a quandary.

The good news is that you can always define your competition yourself by giving customers two existing competitors as reference points.

For example, imagine you are running the company Silicon Graphics, which has just introduced the first digital film-editing tools.

First, you must identify your intended customers by stating a _market alternative_ : a competitor which your customers have been using for years. For Silicon Graphics, this would be old-fashioned method of cutting and splicing film.

You then differentiate yourself with a _product alternative_ : a competitor that has also harnessed a new, disruptive innovation, similar in some way to yours. For Silicon Graphics, this could be SUN workstations, which are leading edge, but not meant for editing film. 

Using these two competitors you can make a _claim_ to market leadership by showing that you operate in a new kind of niche. This claim must be powerful yet short, preferably no more than two sentences. Here's what those two sentences might sounds like:

"For film editors who are unhappy with traditional editing (market alternative), our workstation is a digital editor which enables you to modify images any way you choose. Unlike workstations from SUN (product alternative), we provide all the tools for film-editing."

### 7. Find a distribution channel pragmatists are comfortable with and motivate that channel to sell. 

Before launching your invasion on the target niche, you must choose the _distribution channels_ you will invade through and the _pricing strategy_ you will attack with. This basically means deciding who will sell your products then slapping a price tag on them.

Pragmatists are very picky about which companies they buy from. Hence, when crossing the chasm, the number-one priority is to secure a distribution channel that pragmatist customers are already comfortable with.

_Direct sales_ is usually the optimal channel for crossing the chasm, since it actively creates demand, works fast and facilitates cooperative relationships with customers. Typically, this channel means having a dedicated sales force working directly for you and interacting extensively with major corporate customers to make sales.

Once you have become the market leader in your target niche, you can then transition from direct sales into a channel more capable of fulfilling high-demand volumes. Depending on your product, this could mean selling through _retail stores_, _the_ _internet_ or even large _networks of value-added resellers_ who bundle your product with, say, software and sell it onward.

Selling an unknown new product is a demanding and risky task for distributors. Thus, in the chasm, the primary goal of your pricing strategy must be to encourage them to sell. This means initially giving them a disproportionate share of the price margin as a commission, and phasing it out gradually as you gain market leadership.

Your second goal in pricing is encouraging the customer to buy. Since the pragmatist group prefers to buy from market leaders, you should price your product as the market leader would.

### 8. The chasm is even harder to cross in consumer markets than in business markets. 

Most successful chasm crossings happen in business markets because it is extremely difficult to do so with a consumer product.

Businesses usually have the economic and technical resources to adopt even immature products, whereas consumers do not. Though technology enthusiasts exist in consumer markets, too, they tend to rapidly move on to "the next cool thing." At the same time, you are unlikely to find true visionaries to champion your product and underwrite your R&D.

Also, unlike businesses, consumers usually lack a truly compelling reason to buy your product; they don't have critical processes that only you can fix.

Consider, for example, Quicken, a financial-management application produced by Intuit. It faced the following problem: consumers were perfectly happy using pens, paper and checks for their financial management. Their processes were not so much broken as bent, so they lacked a truly compelling reason to buy.

Eventually, Quicken crossed the chasm by effectively positioning itself as a combination of a savvy financial-management tool and the old-fashioned pen-and-paper method.

### 9. To address post-chasm challenges, a company must secure profitability and realign its organization. 

After emerging from the chasm, companies often find they have inherited a number of commitments from the pre-chasm organization. These could, for example, involve the returns promised to investors.

To successfully manage these challenges and leave the chasm behind, difficult and profound financial and organizational choices must be made.

First and foremost, profitability becomes the focus of the post-chasm organization. In general, the sooner this happens, the better. Instilling a discipline of profitability early on helps companies avoid a "welfare mentality," and teaches them to be careful about which customers and projects they pursue.

Second, the post-chasm company goes through profound organizational changes. The few powerful _pioneers_ who fuelled growth in the early markets can become liabilities, as they tend to have little interest in administration and "business as usual."

What the post-chasm company really needs are _settlers,_ people who diffuse authority and build standardized, well-documented procedures.

The pioneers find this dull and alienating and are often unable to operate successfully in this new environment. Disputes arise almost inevitably, often regarding the fair division of compensation and sales bonuses from the vastly different early and mainstream markets.

As this transition happens, the company runs the risk of losing early-market customers and neglecting the product enhancements demanded by mainstream customers. Thus, two new jobs must be created, both of them temporary:

The _target market segment manager_ must transform every visionary customer relationship into a potential beachhead to enter the rest of that customer's industry. For example, if the customer is Intel, the industry is semiconductors.

The _whole product manager_ is in charge of managing the ever-growing list of product-enhancement requests and bugs to be fixed, ensuring retention of pragmatist customers.

### 10. Final summary 

The key message in this book is:

**To achieve mainstream market success, high tech products must cross the chasm between early and mainstream markets. They must focus on finding a single target niche where they can fully address customers' needs and become the market leader.**
---

### Geoffrey A. Moore

Geoffrey A. Moore is an author, consultant and venture partner. His other bestsellers include _Inside the Tornado_, _The Gorilla Game_ and _Living on the Fault Line_.

_Crossing the Chasm_ is derived from the author's work as a high technology consultant in Silicon Valley. Originally, both the author and publisher assumed the book would be of interest to a mere niche group of people and would probably only sell some 5,000 copies. In fact it became a runway hit with over 300,000 copies sold.

