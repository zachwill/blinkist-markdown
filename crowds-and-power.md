---
id: 5c570ea26cee070008d6c880
slug: crowds-and-power-en
published_date: 2019-02-06T00:00:00.000+00:00
author: Elias Canetti
title: Crowds and Power
subtitle: None
main_color: 2BB2D9
text_color: 186278
---

# Crowds and Power

_None_

**Elias Canetti**

_Crowds and Power_ (1960) is a troubling, prophetic and erudite analysis of human groups and their interaction with power. Written by Nobel laureate Elias Canetti, it asks why humans who prize individuality seek out membership in crowds and how rulers exploit that desire. This study is as wide-ranging in the sources it draws upon as it is thought-provoking in the conclusions it reaches.

---
### 1. What’s in it for me? A lucid look at the conundrum of crowds and power. 

Human experience is paradoxical. Often, we're pushed and pulled in different directions by forces beyond our control. And these forces have been analyzed by some of history's greatest geniuses. Just think of Shakespeare's _Romeo and Juliet_, which explores love and hate, or Freud's psychoanalytical investigation of attraction and repulsion.

Elias Canetti's _Crowds and Power_ could stake a solid claim to having achieved the same for the recurrent battle between individuality and the urge to lose ourselves in crowds. Probing a deeply ingrained human instinct, he takes readers from prehistoric packs of hunter-gatherers to the emergence of global religions like Islam and Christianity and the nation-state.

These blinks help explain the paradox of why individual humans — so proud of their own uniqueness — seek refuge in the group.

In these blinks, you'll learn

  * what distinguishes a crowd from a pack;

  * how command-giving evolved from a threat of death into a form of bribery; and

  * why nation-state crowds rely on distinctive symbols to bind their members together.

### 2. There are five different types of crowd that can be distinguished by their emotional content. 

If you've ever been running late for work on a crowded commuter train that suddenly grinds to a halt, you might have experienced something like this:

As the train stops, there's a shift in people's behavior. Just seconds ago, everyone was immersed in their own small world; now, as frustration mounts, there's suddenly a sense of togetherness. Everyone's anger is directed at the same target and everyone wants the same thing — to get the train running again.

That's a great example of how a _crowd_ forms.

A crowd isn't just a large number of people — it's a mass in which members identify with one another. When that happens, people enter into something that's greater than the sum of their individual parts: a crowd. In that moment, there's a sense of equality. Every member enjoys the same standing, regardless of previous differences.

Those are the general traits of _all_ crowds, but there are also specific _types_ of crowds. In fact, there are five different kinds of crowd according to their emotional content.

Let's start with the _baiting_ crowd. This crowd has a clear objective — to kill its chosen target. A classic example is the crowd that called for the crucifixion of Jesus Christ.

Then there are _flight_ crowds. These are formed when a group of people is faced with a common threat. Once the danger passes, however, the crowd dissolves.

Next are _prohibition_ crowds. Their purpose is refusal — think of striking workers manning a picket line.

_Reversal_ crowds are also rebellious, but their aim is to overturn existing power hierarchies. They form when slaves revolt against their masters or soldiers turn their weapons on their officers. 

Finally, there are _feast_ crowds. Their purpose? Common and equal indulgence, typically in the form of lavish food-based festivals.

Let's get back to what all these types of crowd have in common. There are four attributes that define all crowds.

The first is growth. Once a crowd exists, it tends to expand; it "wants" more people to join it.

Second is equality. As soon as a crowd has formed, all its members are equal.

Third, crowds are typically _dense_. Bodies are pressed up against each other, and nothing can stand in the way of this proximity or divide members from one another.

Finally, every crowd has a goal. Without a purpose, the crowd disperses and people become individuals concerned with their personal affairs once more. Once Jesus had been crucified, for example, the crowd that had been baying for his blood left the scene, and its members returned to their normal lives.

### 3. The crowd has its origins in the “pack,” of which there are four different kinds. 

So where do crowds come from? After all, humans haven't always lived in big cities teeming with thousands or even millions of people. Well, crowds are deeply rooted in an older form of human association: the _pack_.

Packs existed long before crowds. In fact, they can be traced all the way back to the era when our ancestors still lived a pastoral and nomadic existence. Unlike crowds, packs aren't defined by _growth_. A pack is an isolated group surrounded by wilderness. There simply aren't other people who can join.

That's one major difference between crowds and packs. Another is _density_ — crowds are much denser than packs. What they have common are the traits of _equality_ and _direction_ we looked at in the previous blink.

Let's take a closer look at the four different types of pack.

The first and most natural is the _hunting_ pack, which is defined by its aim of killing its prey, which is usually an animal too large or dangerous for an individual to attack alone.

Next comes the _war_ pack. This resembles the hunting pack, but with one significant difference: it doesn't aim to kill animals but other humans and packs.

Third is the _lamenting_ pack, which is formed when a group member dies and is thereby torn from the group. This pack's aim is to mourn the loss of life, ensure the continued unity of the group and, in many cases, administer last rites that protect the soul of the deceased.

Finally, there's the _increase_ pack. As the name suggests, an increase pack seeks growth; it wants to expand. It's here that we find the roots of the crowd. But the world had to become quite populous and connected before this urge could be fulfilled.

> _"It is certain that man, as soon as he was man, wanted to be more."_

### 4. The rituals of Christianity and Shia Islam reflect their origins in the lamenting pack. 

The rituals of the great world religions display dynamics that hint at their origin in much older pack and crowd behaviors.

Take Islam. Devout Muslims assemble for prayer five times a day, usually in small groups. Let's call those groups _prayer packs_. On Fridays, these groups merge into vast crowds. The crowd also plays an important role in _hajj_, the great pilgrimage to Mecca. Once there, the crowd of pilgrims is equal and united in its pursuit of a common goal — rounding the sacred Kaaba. 

But the strongest connection between packs and religion is their shared emphasis on lamentation. Within Islam, the Shia branch is a classic example of a religion of lament.

Shiites, as the adherents of Shia Islam are known, believe in the supreme authority of a spiritual and temporal leader — the Imam. As they understand it, the Imam is a direct descendant of Muhammad and carries the divine light of God within him. The first Imam was Ali, the husband of Muhammad's daughter Fatima. Ali's eldest son, Hasan, became the second Imam.

For Shiites, however, it's the suffering of the third Imam — Hussein, the younger brother of Hasan — that's especially important.

Hussein fought the Khalif's troops in Damascus in 680 CE to defend the Shia claim that the Imam was the true leader of all Muslims. He was beheaded during the battle.

Shiites mourn the death of Hussein to this day and place that grief at the center of their faith.

Shia Islam isn't the only religion of lament, though. Christianity is another example.

As we saw in the previous blink, lamenting packs typically form in the wake of the death of one of their members. Lamentation is especially important when the person who died is thought of as someone who sacrificed himself for those who now mourn.

In such cases, the pack laments the loss of a great hunter or someone who devoted himself to higher values. Often enough, the departed person comes to be seen as a savior.

Christians regard Jesus Christ as their savior, and many of their rites and rituals hark back to the lamenting packs that gathered beneath his cross and at his grave.

> _"The most spectacular example of such conscious crowd formation on part of the church is the Crusades."_

### 5. Nations are crowds that people relate to through the crowd’s symbols. 

Nationalism can inspire a fanatical belief in one's own superiority, uniqueness and greatness — ideas that, carried to their logical extreme, have underpinned some of the greatest atrocities of the modern era. So where does it come from?

Nations, Canetti argues, are rooted in the crowd, and people relate to them through crowd symbols.

To belong to a nation is to be part of a unit that transcends the individual — in other words, to be part of a crowd. Its members feel themselves to be equals and constantly seek to grow and expand.

Because a nation isn't a literal crowd of people, symbols become key to the way people interact with this perceived crowd.

Take the English. They identify their country with the sea. Famous for their individualism, the English see themselves as captains of small ships, plying the ocean of society in isolation from other ships and their captains.

The German crowd symbol that emerged at the end of the First World War, by contrast, was the army. That was related to the idea of a marching forest — a symbol that harked back to a longstanding German affinity for woodlands.

Germans take a mysterious delight in the parallel rigidity of upright trees. They contrast the vertical orientation of such forests with the uncontrolled growth in all directions of tropical jungles.

And every nation has its own crowd symbols. For the French, it's revolution; for the Dutch, dikes; and for the Spanish, the image of the matador.

Perhaps the most complex and interesting case, however, is that of the Jewish nation. The symbol that many Jewish people find compelling is that of the Exodus from Egypt. In this case, the nation is united by its wandering in exile and its search for the promised land.

### 6. Seizing, killing and eating are acts of power symbolized by hands, fingers and the mouth. 

These blinks are about crowds _and_ power. So far, we've taken a look at crowds and their precursor — the pack. Here, we'll move on to take a closer look at power.

So what is power? Well, at its most basic it's the _threat of force_. Think of it this way. Physical force can only act in the here and now. Power, on the other hand, exceeds the limits of such immediacy. It's about the extension of force through space and time.

The origins of power can be found in the acts of _seizing_, _killing_ and _eating_, actions performed — and thus also symbolized — by the hands, fingers and mouth.

The first act of power is to seize. The hand's tight grip on an object is characteristic of power. That's why the great paws of large felines like lions and tigers are so often used to symbolize power.

Next come the fingers, with their pointed tips and armor-like nails. Fingers, especially the index finger, are used to jab and point — actions that resemble stabbing. The powerful are always in a position to stab and kill.

Finally, there's the act of eating. When the prey is consumed, it is broken down and absorbed by the body of the powerful eater, who literally sucks the substance out of it.

To eat is to use one's mouth and teeth. The latter are natural instruments of power and have long been potent symbols of the powerful — just think back to the image of lions and tigers.

The prey that is killed and eaten is incorporated into one's body, an act of power again, where the consumer sucks the substance out of the prey.

But there's another element at play when it comes to eating. Whoever eats the most is also a champion; after all, food is often the product of killing other animals.

It's for this reason that some peoples make their champion eater their leader. In other cases, power is associated not with the act of eating a vast amount of food but simply possessing it.

> _"That which can kill is feared; that which does not directly serve killing is merely useful."_

### 7. Survivors are often powerful simply because survival is understood as a sign of power. 

Power isn't only about force. It's also about _survival_. To be powerful is to be able to survive, come hell or high water — and that survival is a symbol of power.

Imagine a battlefield shortly after the fighting has ceased. The survivor stands upright, surveying the fallen and slain at his feet. His survival is a triumph over the dead and gives him a sense of his unique status.

After all, _he_ survived — he's better than the dead for the simple reason that he's still alive. That's a potent ego trip. He now feels invincible.

But it's not only he who sees himself this way. Those around him are also likely to attribute power to him because of his survivor status.

You can see this in classic ideas of what it means to rule over others.

One of those ideas paints the ruler as a survivor — the paranoid king, for example, who sees threats lurking behind every corner and maintains his position by means of bloody tyranny and executions.

The link between power and survival is also made in the mythology of many premodern societies.

Take the tribes of Polynesia, for example, which believe in what they call _mana_. Mana is essentially a supernatural power that warriors carry with them. When they kill an opponent in battle, they "inherit" the opponent's mana and thus increase their own powers.

The Murngin people of Arnhem in Australia similarly believe that a man's spirit enters the body of the man who kills him, doubling the strength of the killer.

Then there's the remarkable number of tribes that trace their origin back to a disaster that only a few of their ancestors survived.

Think of the well-known Biblical myth of Noah and his wife, both of whom survived the Flood and stand at the root of humanity's family tree.

On the other side of the world, the North American Kutenai tribe has a similar myth.

According to their sacred history, there was once a great epidemic. It wiped out all but three people: a man, a woman and her daughter. In the myth, the man takes the daughter as his wife, thus founding the Kutenai tribe.

### 8. Symbols of power, as well as hiding one’s true thoughts, play a major role in maintaining power. 

Have you ever seen a cat catch a mouse? If you have, you'll know that it takes its time before killing its prey. Before that, it plays with the mouse — releasing it and letting it run away before slamming its paw down at the last second.

That's a great example of the difference between force and power we looked at earlier.

Here, the mouse has fallen victim, not to the former, but to the latter. Power, in other words, is about _more_ than just the ability to exercise force.

Nevertheless, force and power are often conflated. That's hardly surprising, really — after all, power requires force, even if force isn't all there is to power.

That's why symbols of force often come to stand in for power.

Animals are a classic choice. Lots of cultures use fleet-footed or swift-winged creatures like lions, leopards, eagles and falcons to symbolize power.

Lions and leopards, for example, were long used as symbols by African royal houses. Then there's Horus, the falcon-headed God of pharaonic Egypt. The Romans meanwhile depicted their emperors' souls ascending to heaven as eagles.

The Greeks took this a step further and symbolized the swiftness and power of their god Zeus with lightning bolts.

Power is also about manipulation. This manipulation can often be verbal or mental — things like a mastery of questioning and investigation or possessing the ability to disguise one's true thoughts and intentions.

But just because they're nonphysical doesn't mean they don't involve force.

Questions, for example, are an intrusion on one's freedom. To answer a question is an act of submission. Every reply to a question forces a person to reveal more and more of themselves. The greatest master of questioning was Socrates. With questions alone, he dominates his dialogue partner.

That's what makes an unanswered question so powerful. Silence is a form of armor, a barrier that repels the questioner's arrow-like queries. Power loves secrecy, and secrecy is vital to the exercise of power.

Just as the cat secretly lies in wait for its unsuspecting prey to emerge, the powerful ruler often disguises his true intentions from those around him.

> _"Personal freedom consists largely in having a defence against questions."_

### 9. Commands were originally a threat of death that only later became domesticated. 

Commands are, by their very nature, final and categorical. Put differently, they're clear expressions of power. And we're taught to obey them from the moment we begin to be socialized.

So what are they and what role do they play in social life?

The origin of commands can be traced back to a threat of death. The original command is the _flight command_. Think of a lion's roar. It threatens death and sends the command's recipient into flight.

The important point, here, is that a command never comes from inside us. It is always external. We experience commands as an imposition. We submit to them because we recognize that they've been issued by something stronger than us, something we can't hope to defeat in a fight. 

That means commands are intimately connected to power. And the power of command-giver grows each time that a command they issue is obeyed.

As human societies develop, however, the threat of death implicit in commands tends to retreat into the background. Command-giving, in other words, tends to become domesticated. Just think of the commands a mother gives to her children or of those a pet owner gives to his pet. The command looks pretty harmless compared to its high-stakes origins.

So what happened?

Well, _bribery_ took its place. Rather than threatening the disobedient with death, humans increasingly offer an inducement to those in their charge: food in return for obedience. Thus the master feeds his dog and the mother her children.

One area of life retains a closer connection to old-school chains of command, however — the army.

In fact, armies can only exist as long as commands from above are obeyed unquestionably by those below. That, incidentally, is why armies can never be true crowds: its members can never be equal.

### 10. Transformation is a key component of power, as southern-African bushmen have long known. 

In the old Georgian fairytale _The Master and His Apprentice_, a boy becomes an apprentice to the devil and begins to learn magic. After tiring of his tyrannical master, he tries to escape by turning himself into a mouse.

The devil transforms himself into a cat to pursue him. The boy then turns into a fish; the devil, into a net. On and on it goes, the boy's pheasant now pursued by the devil's falcon.

The story is a good illustration of the role transformations play in the struggle for power. That's something we see in nature. Take the tactic adopted by animals that can't escape a predator and so play dead in the hope of being left alone. With this piece of acting, the fleeing animal transforms its image in the hope to escape the power of its pursuer.

Humans also use transformation in their bid to gain the upper hand. Because they can't literally transform themselves into other creatures, they deploy cunning disguises to trick their enemies into believing them to be friends.

That's why the despot always wears a benevolent mask, and focuses his energies on identifying or "unmasking" his opponents — opponents who wear the mask of loyalty to disguise their rebellious motives.

The importance of transformation is something that the bushmen of southern Africa have long known about. Accounts of their societies are full of reports of their extraordinary sensitivity to the world around them. Many depict them as being able to literally _feel_ distant events and discern what will happen in the future. In other words, they can transform their own feelings and sense of self to reflect the wider world.

Some bushmen, for example, can supposedly sense a wound inflicted on someone close to them in their own bodies. Others can feel the weight of the child their wife is carrying, as if the infant were on their own shoulders.

That ability is also said to extend to animals, with bushmen being able to detect an insect bite on an ostrich's neck as though it were on his own!

### 11. Rank, posture and the regulation of time are also connected to power. 

Rank and power aren't only reflected in official titles but in the way humans conduct themselves. Someone's _posture_, in other words, tells us a great deal about how much authority they enjoy among their peers.

If someone sits up straight while those around him stand, or if he stands while other sits, we know that they are in a position of authority.

Kneeling in front of someone else, on the other hand, is a symbol of weakness — it's the position adopted by the petitioner. 

Human postures, then, are indicators of power and status. Let's take a closer look at three important positions: standing, sitting and lying down.

Take standing. To stand is to convey independence; it tells the world you don't need any support. Standing is thus associated with confidence and self-sufficiency. It also serves to distinguish humans from the animal kingdom, as few animals can stand unsupported on two legs.

Sitting can express both power and its absence — it really depends _where_ someone sits. There's a vast difference, after all, between sitting on the ground and sitting in a chair. The chair evolved from the throne. To be seated in a chair originally meant that one was distinguished. It was the act of a man of power in the company of his inferiors, who remained standing in his presence.

Finally, there's lying down.

To lie down is to be without armor and thus vulnerable, especially when sleeping. The difference between lying and standing is so great that a man who suddenly raises himself to his feet makes a powerful impression of vitality.

Then there's the question of time. Power is closely connected to the ability to regulate time.

Power stakes a claim on the eternal: it wants to endure through all time. One way it tries to make itself synonymous with time itself is to regulate time.

That's why Julius Caesar established the Julian calendar and then had the month of July named after himself, a feat emperor Augustus also achieved. It's also the reason Adolf Hitler placed so much emphasis on the idea of the "thousand-year Reich."

### 12. Final summary 

The key message in these blinks:

**Human history is defined by the crowds individuals enter into and the aims they pursue as members of those crowds. From prehistoric hunting packs to mass religions and the modern nation-state, our experience of the world has always been determined by our membership in large groups. The other part of that history is power. Whether it's a direct threat or a more subtle way of directing our behavior, power shapes us as both individuals and members of crowds.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The True Believer_** **by Eric Hoffer**

_The True Believer_ (1951), published in the aftermath of World War II, is an exploration of mass movements and the means by which they attract followers. These blinks will take you on a walk through history — showing how, under certain circumstances, be they right or wrong, anyone can become a _true believer_.
---

### Elias Canetti

Elias Canetti (1905-1994) was a German-language novelist, sociologist, memoirist and playwright. Born in Bulgaria, Canetti moved to Austria with his family before fleeing Nazi persecution and settling in England. His best known works include the novels _Auto-Da-Fè_ and _The Human Province_ ; his memoir, _The Torch in My Ear_ ; and his sociological study, _Crowds and Power_. Canetti received the Nobel Prize for Literature in 1981 for his unique achievements as a writer.

