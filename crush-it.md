---
id: 534ec2ec65316600074d0000
slug: crush-it-en
published_date: 2014-04-23T09:00:34.000+00:00
author: Gary Vaynerchuk
title: Crush It!
subtitle: Why Now is the Time to Cash in on Your Passion
main_color: 21A660
text_color: 177342
---

# Crush It!

_Why Now is the Time to Cash in on Your Passion_

**Gary Vaynerchuk**

_CRUSH IT!_ is a motivational text, a blueprint and guide for those who want to translate their passion into a business. Using the author's life as an example, this book details how everyone can "crush it," i.e., realize the possibility of living their passion, determining their livelihood and making a living off of what they love to do.

---
### 1. What’s in it for me? 

Today, many people work at unfulfilling jobs without realizing that they can make a living doing what they love to do. These blinks will show how you can transform your passion into a profession, and how you can make a living doing what you used to do in your "spare time."

By identifying your passions and developing the right kind of content, you'll be able to create a brand without having to compromise your personality.

You'll also learn the secrets of finding and captivating your audience simply by being part of a community that shares the same passion.

In this book in blinks, you'll learn why it's better to ruffle a few feathers than it is to try to play it safe. You'll also learn the surprising reason why cutting corners will ultimately drive your business into the ground.

But, most importantly: you'll develop the blueprint for turning your hobby into your life.

### 2. You can only create a successful business if you follow your passion and work hard. 

Have you ever dreamed of make a living doing what you love, but felt that, in reality, it's just impossible? If so, you aren't alone: many of us think that turning a hobby into a job is the stuff of fantasy. Fortunately, we _can_ all "_crush it_," i.e., profit from doing what we love. The trick is knowing how.

There are many steps involved in crushing it, and the first is to identify your _passion_, i.e., whatever makes you _you_. This could be anything in the world: from playing board games to making model trains, from gardening to cooking, anything goes.

Once you've figured out your passion, it's time to _live it_. Living your passion means turning it from a hobby to a central part of your life: your job. In other words, you don't want to just "make a living": you should be profiting from doing what you love.

Take internationally renowned talk show host Oprah Winfrey or world-famous chef Martha Stewart: both of these amazing women identified their passions (chatting and cooking, respectively), and then devoted their lives and careers to them.

You also need to combine living your passion with _loving your family_. By loving your family, you'll have a support system that will help you to turn your dreams into a reality.

Finally, you need to work _super hard_. This means doing more than just putting in an effort: it means directing _all_ your efforts towards your passion, and it's vital to your success.

If, for example, brewing beer is your passion, then you might be thinking about turning it into a job. But you'll only be able to accomplish this if you invest _every second_ of your time in it. This means completely immersing yourself in beer brewing by doing things like learning from other brewers, keeping up to date on new beers or watching videos on brewing techniques.

Once you've taken these three steps, it's time to start crushing it!

### 3. You can only monetize your passion by turning yourself into a brand. 

Once you've identified your passion and are trying hard to live it, what's your next step on the road to success?

Finding ways of letting the world know just how passionate you are about your field.

You can achieve this by creating a _brand_ : an image that shows people what you're passionate about and is a reflection of your personality.

The author, for example, translated his passion for wine into a brand by speaking about wine in plain English on his YouTube channel _Wine Library TV._ During his shows, he spoke about wine in a way that reflected his own personal tastes and loud, abrasive personality ‒ sometimes causing controversy ‒ instead of trying to find ways to alter his persona in order to conform to people's expectations about wine experts.

Although some find his personality off-putting, being genuine made him stand out from other wine experts. Indeed, Wine Library TV has succeeded because of the synergy of his hard work, his passion and the way the brand reflects his passion to the viewer.

But just having a brand is not enough: since your brand is the lens through which your audience views your passion, you need to make sure there are no "smudges." You can ensure that your brand stays well regarded and loved by displaying excellent customer service. Treat your fans and customers well by listening to them and maintaining an open dialog.

For example, when the author was building his brand, he would spend hours answering questions, comments and criticism from his fans and blog viewers. By investing time in them, they in turn trusted him and ended up returning to his blog and spreading the word about his brand.

### 4. Social media websites are perfect for attracting fans, customers and businesses to your brand. 

Once you've identified your passion and sculpted your brand, you have a new hurdle: reaching out to potential customers, fans and people with the same passion as you. The best way to do this is to use social media.

Social media has forever changed how people find fans and conduct business. Indeed, social media tools, such as Youtube, Facebook, Twitter and Flickr, allow you to access audiences interested in your passion with speed and ease that was once impossible.

Taking the author as an example, we can see how social media helped him find fans for his business, Wine Library TV.

For example, he used his Facebook fan page and Twitter account to connect with his followers and fans. In addition, he also made YouTube videos that viewers could comment on and ask questions about to discover more about his brand.

This allowed him to transform his passion into a business as more and more people found their way to his site via social media. As excited viewers spread the word, his fan base naturally grew, helping him garner the attention of traditional wine businesses, i.e., the _wine world_, who wanted a piece of the action by appearing and advertising on his show.

This increase in popularity allowed him to continue expanding his business opportunities with book deals and media appearances.

But social media doesn't just give you access to a large audience: it also means that you have complete control over your brand.

In the past, companies had to hire advertising agencies to market to potential customers. The problem? The agencies controlled the brand.

Today, you can _personally_ reach your customers by using social media with complete control: _you_ choose what to tell customers about yourself and provide them with direct feedback catered to their specific needs.

This is one of the secrets to the author's success: he used social media to create an image that his fans loved, _not_ an image that advertisers created for him.

### 5. Match your brand with the content of your product and the online platform you use. 

Once you have your brand and are sharing it with the world through social media, it's now time to decide how you want to convey your passion to your audience.

The best way to do this is using _stories_ that help your audience grasp and understand your position. Will you tell stories on a video blog on YouTube, a traditional blog on Tumblr or using pictures with captions on Flickr?

In short: What great content will you produce to tell your story?

We can draw some inspiration from one great storyteller, the radio personality and "shock jock" Howard Stern. He recounts all kinds of amusing and shocking stories about his life and passions that his listeners can relate to. And because he's so charismatic, he was able to use radio to turn his passion for stories into an incredibly lucrative business.

Now is the time to figure out exactly how you want to convey your passion to your audience. In order to do this, you need to decide what kind of medium is the truest to your personality.

For example, if you have a personality that catches people's attention, you should use a video or audio blog to better take advantage of this.

A perfect example of this principle in action is the radio show _Car Talk_, a weekly show that offers answers to listeners' questions about their car problems. However, the hosts are so personable, hilarious and knowledgeable about cars that they even manage to capture the attention of listeners who _couldn't care less_ about cars!

On the other hand, if you're shy in front of a camera or microphone, you should look at other media. For example, if you're a sports fanatic but hate being on camera, you can put your passion to paper by writing about sports via, e.g., a blog or Twitter. 

Whatever you decide, your brand will succeed if your content and platform match your passion.

### 6. Being authentic is the key to turning your brand into a success. 

Once you've transformed your passion into a brand and located the platform to match it, how do make sure that you stand out in the crowd? The answer is quite simple: be _authentic_.

Authenticity means being true to yourself no matter what. You can't be everyone's best friend, so even though your personality and mannerisms may turn some people away, you must persevere.

The author, for example, has played with the idea of changing the loud and obnoxious introductions to his wine review videos in order to prevent it from turning people away. However, he's resisted the urge and decided to keep them because they're a reflection of him and his loud personality.

Even though he may have lost some viewers due to the abrasive intros, he's undoubtedly gained more fans who appreciate that he's being true to his nature. If he were being inauthentic, his audience might think he were just putting on an act and that he's not truly passionate about wines.

But cultivating authenticity _does_ take work: it requires time, effort and energy. Truly authentic people are willing to work long hours and have the patience to build the momentum necessary to turn their passions into success stories.

If you think you can cut corners to speed things up, then you risk losing authenticity because you aren't willing to actually devote yourself to your brand. Only by devoting _all_ your time to your brand can you ensure that it truly reflects your personality.

And this, of course, takes patience: success doesn't come immediately, and without the patience to achieve later rewards, no brand can manage to get off the ground.

You should, therefore, be prepared to work long, hard hours on your brand. But don't worry — since your brand is your passion, it'll hardly seem like work at all!

### 7. To succeed, you and your brand must gain the support of a community. 

By now you have your authentic brand and you know how to tell people about it, but you still have a little way to go! It's time to figure out where to find the people who will be your fans and customers to ultimately help you on the path to "crushing it."

In order to find and interact with these future fans, you need to be part of a _community_, i.e., a group of people who share an interest in your passion and who come together to discuss, debate and share views on the subject matter.

If you want to grab the attention of your target community, find the places where they exchange ideas and participate in those communities yourself.

For example, when the author began his journey to live his passion for wine, he knew that the only way to be successful was to capture the interest of those who shared his passion. So, after setting up his own website and Facebook page, he read up on all the things that other wine lovers were talking about.

He would scour wine bogs, videos, journals, etc., for up to _eight hours a day_ in order to find more information about which wines people were talking about, buying and recommending.

He also participated directly with individual members of the community. Just like you might introduce yourself to your neighbors when you move to a new neighborhood, he would introduce himself to his wine community by posting comments and taking part in online discussions.

Because of this, members of the community knew who he was. Even more vital: he got an idea of what it is that they wanted.

Once the author had become part of the community, he maintained its support by treating people well: by responding to their questions and respecting opinions contrary to his own.

### 8. Once you have a popular brand, get yourself noticed by reaching out to investors and advertisers. 

Following your passion is all well and good, but it's not a sustainable endeavor if you can't put food on the table. Once your brand gains popularity, you'll undoubtedly be wondering how to turn that popularity into cash.

Quite often, you'll find that investors and companies looking for advertising space will begin to take notice of you once you've created a popular online brand with a solid following.

But, of course, it never hurts to help this process along.

One way to do this is to participate in conferences in your subject area. Indeed, conferences are great places to find investors and advertisers due to the high concentration of people who are already interested in precisely the thing you're passionate about.

Even better, you could make a strong impression by delivering a talk at a conference: What better way to advertise your passion and your brand than demonstrating that passion on stage?

Another way is to spend time actively researching which kinds of companies invest in or advertise to your community. After finding one such company, you should try to connect with them.

For example, if you run a sports blog, you could research sportswear companies that advertise in print media such as magazines or newspapers. When you reach out to these companies, explain to them how advertising on your blog could give them a greater return on investments since they could reach your large fan base and pay only a fraction of what they would in print media.

Given that online advertising is such a booming business, you might find this tip particularly useful if your brand focuses on its online presence.

Finally, your brand can even serve as a stepping stone to possible book deals and TV appearances if you gain a large enough following!

### 9. Respond to changes in your business environment quickly and effectively. 

So you're now ready to start making money from online advertising and investors. Great! But there's one more thing you have to think about: How do you ensure that your brand continues to thrive in the future?

The best way to ensure the future success of your brand is to adopt the practice of being a _reactionary business_, i.e., of reacting quickly and effectively to changeinstead of trying to base your plans on inherently inaccurate predictions about the future.

Reactionary businesses carefully observe the business environment in order to see when new developments emerge, and then quickly react to them. 

For example, the author demonstrated his reactionary business skills when he released Wine Library TV on Youtube in 2006.

At the time, YouTube was still young, and very few people realized that it had the potential to become such a useful business tool.

However, the author was one of the few who did recognize its enormous potential, and he reacted quickly by launching his YouTube channel very early on.

This reactionary business strategy not only allows you to seize new opportunities, but also helps you mitigate losses: if you fail to react to change, your business will suffer and you will lose your fan base and thus potential customers.

These changes don't necessarily have to relate directly to the business environment: you also need to respond to changes in the way different communities perceive your brand.

For example, in the late 1990s, Cristal champagne became extremely popular with the hip-hop community. But Cristal wouldn't embrace its new fan base, instead opting to distance itself from the community by declaring that it wasn't interested in them as customers, seeing them as too lowbrow.

And Cristal paid for its failure to react to this new community: hip-hop artists, most notably Jay-Z, publicly boycotted Cristal, thus causing the company to lose potential customers and tarnishing their reputation.

Ultimately, you can only produce a successful brand if you react quickly to the continual developments in our ever-changing world.

### 10. Final Summary 

The key message in this book:

**The secret to turning your hobby into your business is to devote your life to it. By creating your own authentic brand, engaging your audience on social media and intelligently reacting to changes in the business environment, you can attract the attention — and dollars — of investors and advertisers.**

Actionable advice:

**Reach out to your community.**

If you run a business and have wondered how to further expand and reach new customers, then try posting a video on Facebook talking about your passion and your brand. Tell people a story that they can relate to, take part in a conversation, and you will find them coming back for more and spreading the word to others.
---

### Gary Vaynerchuk

Gary Vaynerchuk is a best-selling author of titles such as _Jab, Jab, Jab, Right Hook_ and _The Thank You Economy_, as well as an entrepreneur and founder of Wine Library TV, an online video blogging site that offers information and advice on everything wine-related.

