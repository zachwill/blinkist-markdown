---
id: 5ab6ee2cb238e10005bf58f7
slug: crushing-it-en
published_date: 2018-03-27T00:00:00.000+00:00
author: Gary Vaynerchuk
title: Crushing it!
subtitle: How Great Entrepreneurs Build Their Business and Influence – and How You Can, too.
main_color: F3E65A
text_color: 736D2A
---

# Crushing it!

_How Great Entrepreneurs Build Their Business and Influence – and How You Can, too._

**Gary Vaynerchuk**

_Crushing It!_ (2018) explains and explores why having a strong personal brand is crucial in business. Following up on his 2009 best seller, _Crush It!_, the author draws on both his own experiences and those of readers to illustrate why having a strong presence across multiple digital-media channels is a blueprint for success.

---
### 1. What’s in it for me? Find your own route to success. 

The world has changed. Twenty years ago, if you wanted to get discovered as an actress, you'd maybe need to move to Hollywood. If you wanted to get ahead in business? You'd go to business school. Broadcast to millions? Well, it sure wasn't easy getting a primetime TV or radio slot.

The internet and social media, by essentially doing away with industry gatekeepers, have all but destroyed the barriers to success, whether you're an entrepreneur, a performer or any other kind of content producer. Top podcasts are streamed by millions every day. Actresses, artists and all kinds of creative professionals are discovered on YouTube and Instagram all the time. And entrepreneurs across the globe are realizing that social media gives them unparalleled opportunities to communicate directly with potential customers.

As you'll see in these blinks, building a personal brand and cultivating it through social media is a route to success accessible to all. And that's equally true if you're a frustrated accountant, a food enthusiast ready to share your love of homemade preserves with the world or a fashionista seeking to monetize your sense of style.

In these blinks, you'll learn

  * how a father and son became famous for cutting things in half;

  * why Snapchat is a great training ground for marketers; and

  * why altruism beats self-interest if you want to succeed as an entrepreneur.

### 2. A strong and personal online brand is now one of the clearest paths to entrepreneurial success. 

Ask a kid today what she wants to do when she grows up, and there's a good chance she'll say, "I want to be a YouTuber." Many adults might respond with a patronizing, "That's not a real job, sweetie." But maybe those kids are on to something.

The explosion of social media is a game changer that's made it possible for anyone to broadcast to a mass audience.

Let's look at a few stats.

Worldwide, people watch 1.25 billion hours of YouTube video every day. They post 66,000 photos or videos on Instagram every single minute. And, on average, 20 percent of the time people dedicate to mobile is spent on Facebook.

The scale and power of social-media platforms make it possible for any of us to earn money from a personal brand. With only 1,000 followers, someone on Instagram can earn $5,000 per year, from just two posts a week.

Not a bad side income, right? Now think if you were posting _more_ than twice a week.

For those that really want it, the potential to monetize a personal brand is unlimited.

Consider the author, Gary Vaynerchuk. He transformed his family wine business from a $4 million to a $60 million operation. His approach was simple: he built a strong personal brand by talking straight to customers. 

He ran an unusually honest video blog, doing tastings to the camera and giving frank advice. He spent hours every day replying to every single message he got on Twitter and Facebook. He helped people and, in turn, they came to like him, trust him and buy from him. He built a one-on-one relationship with his customers — the kind of relationship that, previously, you could only form with small, neighborhood stores.

And after that? He took what he'd learned about online marketing and advised others. Today, Vaynerchuk runs a huge digital agency with offices in New York, Los Angeles, Chattanooga and London.

And all because he embraced the power of his personal brand.

> _"If you're earning what you need to live the life you want and loving every day of it, you're crushing it."_

### 3. Building a personal brand offers multiple routes to personal and financial success. 

What could a wine retailer, a fashionista mom and a frustrated artist have in common? They all used a strong personal brand to lead a more successful life.

The author used his brand to sell a product — wine. But many people have no obvious product, and yet are successfully monetizing their personal brand.

Brittany Xavier's _Thrifts and Threads_ blog and Instagram were, in the beginning, just a fun way to showcase photos of her young family's life. But she noticed accounts like hers were tagging brands, and so she started doing the same. Six months and 10,000 followers later, she started to charge brands $100 for a post mentioning them. She discovered that she was aiming too low when a jewelry business proposed a single blog and Instagram post for $1,000. Today, Xavier focuses on her family lifestyle brand full time, earning plenty, traveling and having fun.

But what if, instead of revolutionizing your life, you just want to make it a little better? Will a personal brand still help? Absolutely.

Louie Blaka was a full-time art teacher and frustrated artist. He didn't want to give up his job, but he did want to have more success selling his art in his free time.

After learning about Vaynerchuk's experiences, he started to consider how he could market his work differently. He hosted free wine and paint classes, and posted pictures of the events on Instagram. What started as one class of ten people became many classes of up to a hundred — and a combination of Instagram and word of mouth continued bringing in more people.

The result? Well, he used to sell the occasional painting for $200, and this year he hopes to hit $30,000 in sales. That's a pretty hot side-hustle for a full-time teacher.

So, whether you want to attain riches or just make some cash on the side, a personal brand executed on social media can make it happen.

> _"There is literally_ — literally __ — _no reason why you can't become an entrepreneur in 2018."_

### 4. There are seven principles that really matter when you build your social-media content. 

A house built on weak foundations will fail, and the same is true of your social-media content.

So what really matters? It's pretty simple: there are seven principles you need to nail.

The first is _authenticity_. People can always smell a fake, so don't disrespect your audience. Be real.

You also need to be _passionate_. Whether for your product, your life or the process of being an entrepreneur — it's passion that will keep you going when things get tough.

_Patience_ is a virtue, goes the old saying. And it's true here — building something new takes time. Accept that.

What else is virtuous? _Hard work_. If you want to crush it, you don't have time to spend your lunch break watching cat videos on YouTube, or your evenings bingeing _Walking Dead_. Get on Twitter and make some connections! 

The world is moving fast, so you need to pay _attention,_ and move with _speed_. What are the latest trends? Is there a new platform you should be on? Look forward and don't waste time.

Finally, you need the right _intent_.

All great entrepreneurs have one thing in common: they're not only in it for the money.

It might seem counterintuitive, but it's true. All the best influencers and entrepreneurs are altruistic. They are driven by providing service and value, and they love teaching and helping others. Listen to Jenna Soard, founder of a brand-advisory service called You Can Brand. She says her "truest love is watching the 'ahas' go off in people's minds" as she helps them solve a problem.

When your intent is self-interested, customers won't tell others to come buy from you, and they won't come back.

So center your activities around altruistic values. Think about what you can give and how you can help. That's the only sustainable way to be a success in the long run. And hey — it feels good, too!

### 5. Don’t worry about creating content. Document instead. 

The idea of creating engaging content for social media might seem daunting. Sure, you love sports cars. But do you really know enough about them to run a weekly video blog on YouTube?

Well, don't let your misgivings stop you. Focus on _documenting_ rather than creating.

Rich Roll was an overweight, unhappy, 39-year-old lawyer when he decided to give up the junk food, embrace veganism and take up running. Years later, after becoming an endurance athlete, he thought it could be fun to film and share his training for the Ultraman World Championships.

His five-minute YouTube videos, showing training, or talking about diet, started to get thousands of views. That led to a CNN interview, which, in turn, got him a book deal. He launched a hugely popular podcast, blogged like crazy and kept YouTubing. Now with a second book deal and regular speaking engagements for the likes of Goldman Sachs, he is regarded as "an influencer's influencer."

Now, there's a lesson to be learned, here. Roll didn't wait till he had it all, and that's why he attracted so much interest. People enjoyed going on that journey with him, following as he documented his progress and watching him learn.

So, rather than stressing about how to create unique and creative content, embrace the possibility that _you_ are the unique content.

Vaynerchuk employs a videographer who films his every working moment — bathroom visits and confidential meetings aside. The content it generates is a great learning tool for other entrepreneurs.

Why not regularly share your world with Snapchat, or Instagram stories or Facebook Live? Document moving into your new apartment, or brewing your first batch of homebrew. Maybe it doesn't work out, but maybe you'll build an audience that earns you $50,000 a year through advertising, brand endorsement and affiliate marketing. You might even land a book deal.

Now that we've explored the potential of building your personal brand, let's look at the platforms that you can use.

### 6. Snapchat offers unpolished reality and a real test of your branding abilities. 

In 2015, the music producer DJ Khaled got lost, in the dark, on a jet ski. Attempting to get home in the dying light, he snapchatted his disorienting ordeal until finally reaching shore and safety. Already popular, his personal brand exploded.

Khaled was one of the first Snapchat stars, because he was authentic, and Snapchat is all about unvarnished authenticity.

Khaled didn't overthink things or edit too much or seek a perfect shot. He was just himself. Sure, the content was a little goofy, but it was _real_. In contrast to Instagram, which is often so curated that it seems fake, Snapchat offers an authentic peek into a person's life.

Now, this can make Snapchat a little tricky for marketers. But if you're already present on other platforms, Snapchat offers a great opportunity to show a more human side. It's where Sarah, a realtor, can admit that, despite her diet, she has a weak spot for fries. And where Shaun, a sports-store manager, snaps himself playing foosball with his kids.

There's another reason Snapchat is tough, and why it's such a great training ground for marketers.

With no hashtags and no sharing, Snapchat offers no discoverability. So to go big on Snapchat, you really have to be offering value. 

Sharing a picture of your cappuccino isn't enough. Lauren of _The Skinny Confidential_, a lifestyle blog, explains that, when she snaps a picture of her coffee, she also gives some value, saying something like, "I'm enjoying an iced coffee on this hot day. I'm using a silicone straw because it doesn't contain BPA , and I put cinnamon in there because it's good for your blood sugar levels." Snapchat forces you to give a little extra, to show your personality, to add some value — and that's good practice!

Ultimately, Snapchat is a space to be human. You might not make lots of quick sales there, but you can reveal a little bit of your personality. What's not to like about that?

### 7. Twitter is a great place to get noticed with a specific target audience. 

Back in the day, the watercooler was the place to catch up on the latest gossip about news or a hit TV show. Now, people chew the fat on Twitter, and that conversation is taking place 24/7, with input from every corner of the globe.

But Twitter offers more than just chatter. It's also a great platform for getting noticed.

Why?

First, on Twitter, you have more chances to spark word of mouth: no one posts 50 times a day on Instagram, but tweeting fifty times? Totally fine.

Second, the retweet function is an incredible way to generate awareness. Imagine you've made a video mash-up of rap artist Drake's work. He's probably not going to see it, even if you tag him. But share it on Twitter, and, if it's good, it might just get retweeted until Drake — and everyone else — notices.

Twitter is also an easy way to build your profile for a particular target audience. 

Maybe you've always dreamed of being a sports presenter. Here's how you could use Twitter to achieve that dream.

First, check out what sports topics are trending. Then start tweeting: write a quick comment, or post a short video. Use the right hashtags, so anyone searching for that topic will pick up your tweets. Chip in to conversations. Reply to some big influencers, and ask if they want to check out your blog.

On day one, nothing's going to happen. You'll tweet all day — and nada. But keep going. On day five, a journalist might retweet you. This could give you 200 new followers and, in a month, a sports blog might ask if you'd like to write a guest piece. After 12 months of doing this on, ideally, a daily basis, your local sports station might reach out to see if you'd like to come in for a chat and a possible job.

Being a hit on Twitter takes a heck of a lot of work. But if you put in those hours, you'll see what doors it can open.

### 8. YouTube is the best platform for creating wealth and opportunity, and you should get on it now. 

Did you know young adults watch more video on YouTube than on any TV network at prime time? And while you probably won't get on NBC anytime soon, you could be on YouTube _today_.

Many people hold back from YouTube because they think, "I'm no expert, and besides — I'm not that interesting!" You should banish such thoughts.

First, remember documenting is as interesting as creating. You don't need to be a whisky expert to launch a whisky vlog. Launch a "learning whisky with Bob" vlog and you and your audience — everyone put off by experts talking fancy language and drinking $200 malts — can learn together.

Second, however niche your interests, they'll find an audience on YouTube. Take garage sales. Not so exciting, right? But, on YouTube, there are garage-sale videos with 400,000 views!

The best way to get on YouTube is simply to do it — just start. If your videos aren't great, so what? Nobody is watching them to start with, apart from your mom. They'll get better with time. If they don't and you never attract viewers — hey, at least you've learned it's not for you, and you can try something else.

And on the flip side, really great content usually cuts through.

Take Dan Markham. When his son needed a school project idea, they decided to see what's inside a baseball. They filmed themselves cutting the ball open, and uploaded the footage to YouTube. A year later, Dan realized people had been watching it — enough people to generate ad revenue.

So he and his son made some more videos of themselves cutting things open. A football, a Rubik's Cube, even the rattle of a dead rattlesnake! To date, their "What's Inside?" videos have been watched over 550 million times, and they've worked with brands including Nike and the Bill and Melinda Gates Foundation. 

Pretty cool for a school project!

### 9. Facebook still combines clever distribution with innovation to make it the single most important channel. 

Maybe you've heard people criticizing Facebook recently, saying it's where baby boomers share photos of their grandkids, and that young people aren't there anymore. Well, in reality, Facebook is still the biggest show in town. Two billion monthly active users don't lie.

From a business perspective, Facebook's great advantage is its flexibility for both content and distribution.

Facebook offers a versatility that no other platform can. Want to pump out a twelve paragraph blog entry? That'll work. Ten-second video? Sure. Twenty-minute video? No problem. Some simple photos? Get 'em up there!

And it offers incredibly rich targeting. Want to target people between 18 and 25 years old who live in downtown California and are interested in skateboarding? No problem. Spend $12 and several thousand of them could see your post of a cool skate T-shirt that you designed. 

So that's all great, but can you do anything really innovative on Facebook? Sure — with Facebook Live.

Live lets you broadcast in real time to your followers, take questions and get instant reactions.

Now, it's not for everyone. There are only a few live TV shows for a reason: they're hard to do. But if you're experienced with video, it can be very powerful.

Brittney Castro of _Financially Wise Women_ is a great example of someone with a fresh personal brand in a stuffy old industry — personal finance. She first attracted attention for a finance rap video — the exact opposite of the marketing you'd expect from a financial advisor.

These days, she's a hit across all channels. But her favorite is Facebook Live. She loves the way she can answer women's financial questions live, bringing them real value and talking directly to her potential customers. And by doing Lives with her brand partners, such as Chase Bank, she is bringing her offer to a huge audience.

So for all the doubters — yes, Facebook is still innovating. And so should you!

> _"Here's the reality: if you're going to build a personal brand and try to monetize it, you have to have a Facebook page."_

### 10. Instagram is more important than ever, with a mix of content and great discoverability. 

People usually don't complain when there's too much beauty. But this was actually an issue for Instagram, until the site launched its Stories function, where people can post spontaneous photos and short videos, knowing that they'll only be up for a day.

Now Instagram doesn't suffer from being too polished — it has a perfect mix of curated and ephemeral content.

The day Stories launched, the site's CEO admitted he hadn't posted for six days because "none of the moments seemed special enough." Now, users can post stunning shots for posterity _and_ snap their morning coffee with a cheery caption and stick it on Stories, knowing it'll be gone in 24 hours. Stories is now hugely popular, and savvy users are employing it to show a little behind-the-scenes personality.

So the site's new balance is one reason its thriving. But, even more important for us, Instagram offers some super simple ways to do business development.

Here's a hypothetical success story that demonstrates the point:

Tom is an ambitious manager of a clothing store in Kansas City called EnAvant. He starts instagramming as much as possible. He posts photos of everything: clothes on hangers, shoes on shelves and tons of customers wearing their new outfits, all nicely filtered. Every post has the right hashtags — #EnAvantwear #Kansasfashion #falltrends #fallfashion. On his lunch break, he starts to reach out to stylish locals by searching "Kansas City" and seeing who pops up. He direct messages some of the more popular and fashion-oriented people, saying, "Hey I manage EnAvant, we love your style! Come by and we'll give you 25 percent off your purchase."

Of the 40 people, he reaches out to, six post on their social media about this nice guy who complimented them and helped them out. Tom then invites his influential locals in for a fashion night with a 30-percent discount, and makes the event so fun that he doesn't even have to take photos anymore: all the guests are uploading to Instagram themselves, #EnAvant.

Sales are up — and so is Tom's personal brand. What's next for him? His own fashion line? A successful blog? Thanks to his work on Instagram, the choice is his to make.

### 11. The hottest trend right now is spoken-word media: get on board before it’s too late. 

Ever tried watching video while cleaning your bathroom? It's not so easy. But listening to something? That works. By selling the ability to multitask, spoken-word media will always have a huge market.

Podcasts can be a great place to find and nail your niche.

John Lee Dumas was a podcast fan _and_ a wannabe entrepreneur. But when he searched for a podcast about entrepreneur's experiences, he found nothing. So he thought: Why not do it myself?

He had little experience, and he started out by talking to relatively unknown entrepreneurs. But by running the daily show Entrepreneurs On Fire, he learned fast and built a presence. Within months, he was hitting over 100,000 downloads. His guests got better. He started getting invited to conferences, building his credibility further. Today, he's become the entrepreneur he always wanted to be: earning millions from his podcast and his branded products and services.

Maybe you're thinking, "Good for John, but the podcast market is crowded now." The great news is a whole new platform is opening up, and fast. Content delivered by digital assistants such as Amazon's Alexa or Google Assistant in super-short, spoken _flash briefings_ is the next big thing.

The author already produces a daily flash briefing that you can subscribe to. Before you brush your teeth in the morning, tell Alexa to "give me my flash briefings," and you'll be able to hear his one-minute daily motivational message.

So there is a new platform with huge potential and few leading influencers right now. Let's say you're a landscape gardener. Wouldn't a one-minute "Ted's Daily Garden Tips" be a nice fresh way to boost your business's brand? Five years from now, quick audio updates won't be easy to launch unless you market the hell out of them. But now — do something interesting and it could take off.

So what are you waiting for? Get out there and crush it.

### 12. Final summary 

The key message in these blinks:

**If you are stuck in a job you don't like, or have a business but don't feel it's going anywhere fast, then have the courage to change something. Build a life and a business around something you truly love, whether that's helping people learn about fashion or custom-made tree houses or garage sales — whatever your passion may be. Document your journey on YouTube. Use Twitter to catch the eye of fellow tree-house enthusiasts. Distribute your fashion ideas on Facebook, and share across Instagram. And always keep one eye out for the next big platform.**

Actionable advice:

**Reach out to others but do it right.**

Reaching out to influencers — people with a large following on social-media platforms — through direct messages is a sure-fire way to build collaborations and get advice, so don't be shy. Be sure that you offer something of real value in return, though. If you can't offer exposure, then think of something else you can trade. If you're a graphic designer, offer to make some custom filters in exchange for advice. Make pizzas? Offer free slices in return for a collaboration. This approach can be hard work — you'll send a lot of messages before getting a single reply — but that's why most people won't persevere with it. If you do, you're already winning.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Crush It!_** **by Gary Vaynerchuk**

_CRUSH IT!_ is a motivational text, a blueprint and guide for those who want to translate their passion into a business. Using the author's life as an example, this book details how everyone can "crush it," i.e., realize the possibility of living their passion, determining their livelihood and making a living off of what they love to do.
---

### Gary Vaynerchuk

Gary Vaynerchuk is an entrepreneur and venture capitalist who has built a successful career around his personal brand and his mastery of social and digital media. First known for growing his family's wine business, he now runs VaynerMedia, a social and digital agency. He has written several books, including _Crush It!_ and _The Thank You Economy_.

