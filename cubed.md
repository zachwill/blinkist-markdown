---
id: 5799eebee0bc1b000321bce0
slug: cubed-en
published_date: 2016-08-05T00:00:00.000+00:00
author: Nikil Saval
title: Cubed
subtitle: A Secret History of the Workplace
main_color: F9DA31
text_color: 7A6B18
---

# Cubed

_A Secret History of the Workplace_

**Nikil Saval**

In _Cubed_ (2014), you'll discover that what you know as your office cubicle — that beige box so many workers worldwide toil in each day — is a fairly recent invention, despite its long history of development. These blinks will explain how the modern office came to be through a detailed account of the evolution of the workplace.

---
### 1. What’s in it for me? Venture on a historical journey through the workplace. 

Sitting at work, you use your brain to solve problems and think about all sorts of things. But how often do you ponder why it is that so many people spend upwards of eight hours in an office, each day? The thought probably never entered your head.

Don't fret if how your workplace came to be never crossed your mind. These blinks will enlighten you by exploring how the modern office space developed into its current, cubed form.

The cubicle-packed office is a recent concept that evolved over many historical shifts. From clerks crammed into small and dim spaces in the mid-nineteenth century to carefully designed skyscrapers complete with amenities like barber shops, these blinks will guide you through the history of the office.

In these blinks, you'll also find out

  * how a man named Taylor tamed office mayhem;

  * how a bomb caused the segregation between office and factory workers; and

  * how the invention of the cubicle arose from a new concept of movement.

### 2. Industrialization inspired the rise of the clerk and a distinct workplace, separate from manual workers. 

You know how receipts and personal documents often pile up on your desk? It takes a lot of work to keep track of all that paper, even when it belongs to just one person.

Now, imagine the amount of work it would take to organize documents for multiple employees, or even keep track of every business transaction within an organization!

Administrative jobs that deal with filing papers, paying bills and organizing accounts are common, so much so that they're considered a standard, entry-level position in any firm.

Historically, however, it wasn't until the advent of industrialization in the mid-nineteenth century that such positions became required posts in the workplace.

The people assigned to these jobs were called _clerks_. Initially, clerks worked alongside their bosses in so-called counting houses. These "offices" consisted of nothing more than a dark, tightly packed room. As a clerk, you were lucky if you even had a window!

One New York office, for example, housed ten people, six of which were clerks, in an office of 25 square feet — that's the size of a modest bathroom by today's standards.

By 1855, clerks had become New York's third-biggest group of workers. The sudden growth in the number of clerks meant people needed more space to work, a development which coincided with a spacial separation between manual and non-manual workers.

For instance, you'd often find non-manual workers in a separate office space downtown, apart from a factory where manual workers toiled. Or, if a business was under one roof, the physical separation of clerks and manual workers would be distinct, such as by having separate entrances for each.

Another key factor about the workplace in this era was that clerks and bosses usually worked together in close proximity. Consequently, they often developed good relationships with one another, with a clerk often becoming a trusted, "right-hand man" for a boss.

This workplace model wouldn't last long, though. We'll explore what happened as businesses grew in the next blink.

> By 1860, 25 percent of Philadelphians were occupied in non-manual work. In San Francisco, 37 percent; and in Boston, almost 40 percent.

### 3. As business advanced, the office space expanded, giving rise to the science of work efficiency. 

Imagine a clerk writing, copying and filing in his office in 1860. He suddenly travels through time to arrive in the year 1920. What changes would he see in the office?

Though our clerk may have taken a leap forward chronologically, we can't say the same for working conditions.

Granted, the workplace did change significantly over those 60 years, as gone are the calm days of working with a single boss and a few coworkers. Now non-manual workers seem to resemble factory workers, with hundreds of clerks clacking away on typewriters, and men with stopwatches recording how long it takes a clerk to perform a task.

How did it come to this? From 1860 to 1920, the advance of technology was correlated with the growth of business and office space.

In a sense, technology had made the world smaller. Railroads enabled markets to expand across the continent, and the telephone and telegraph enabled the exchange of information at faster speeds and further distances.

More and bigger businesses were now possible, and they created a demand for more workers, such as errand boys, typists and messengers.

All of these office workers, in turn, needed a place to work, and quickly. Without much thought put into how to scale the workplace for the growth of business, workers were simply stuffed into big office spaces with vast rows of desks.

This expansion gave rise to much confusion, however. No one really knew who did what, when and where, as management was still many years away from becoming an established field of knowledge.

Thankfully, this changed when the American engineer Frederick Taylor (1856-1915) developed a solution that would be eponymously named _Taylorism_.

Taylorism divided work into segments, with each worker carrying out specialized tasks. Everyone knew what to do, and they knew they needed to do it as fast as possible. Taylorism soon became synonymous with efficiency and mass production.

The development of management ideas translated into burgeoning administrative branches in business. So, what happened next?

> _"Workers who might have initially taken pride from their work were now reduced to, as the phrase went, 'cogs in a machine,' indistinguishable from each other..."_

### 4. The skyscraper was developed to stack offices on top of each other and offered amenities for workers. 

Think about it: When we grow from babies into adults, we grow outwards, but mostly upwards. And that's what happened to the modern office, too!

By the mid-twentieth century, buildings had become so tall they appeared to scrape the sky. Skyscrapers thus heralded a workplace design in which multiple offices were stacked on top of one another.

As new managerial ideas flourished in the United States, administrative branches grew subsequently, and eventually, this dynamism formulated itself in the image of the skyscraper — to many, a symbol of both aspiration and success.

Chicago and New York, in particular, saw the growth of tall office buildings skyrocket. From 1871 to 1923, approximately 74 million square feet of office space was constructed in New York alone.

The workers' offices inside these buildings were not as grandiose as the building's facades, however. In fact, offices were normally quite dull, but to make office workers feel special and distinct from the lowly factory worker, a wide range of amenities was offered in these new spaces.

The need to distinguish office workers from factory workers emerged as labor movements began to criticize big business and the capitalistic system in general. In Chicago, for instance, an active labor movement gained traction as businesses drove up rents in their acquisition of office space, pushing up the cost of living for working class people such as factory workers.

This hostility between labor and capital culminated in 1886 with a bomb explosion in a Chicago skyscraper that was connected to a labor union gathering. As a result, business leaders decided that the behavior of the working class was an obstacle to their goals for new offices and growing enterprises.

Thus, to make office workers feel prized and socially distinct from factory workers, many office buildings were designed with amenities such as libraries, dentists, barbershops and so on.

This served the purpose of having the workers feel well-off and working together as a new class of people for a grand enterprise.

### 5. In post-war years, a new office design was born, the organic office landscape. 

In European offices, American workplace designs had long been dominant. But in the 1940s, as war raged across the continent, cities and the buildings in them were destroyed.

As the war wound down, new ideas started to grow about how workers should be organized in office spaces.

In post-war Germany, where bombed cities offered a clean slate for urban design and a new thinking, one of the ideas to emerge was a human-based office design called the _Bürolandschaft_, or "office landscape."

German brothers Wolfgang and Eberhard Schnelle emerged as leaders of innovative office design, founding a space-planning firm called Quickborner.

Rather than relying on the hierarchical structures of Taylor's rows of tables and efficiency-based office design, the Schnelle brothers thought office space should be more organic in its layout.

They argued that office space should rely on the flow of human interaction and how different parts — such as communication and paper flow, varying needs of privacy and interaction — were linked.

The brothers would measure those factors within an office and design accordingly. This is how they came up with the Bürolandschaft concept. The idea caught on, and soon, major firms in Europe wanted to integrate this concept in their offices.

The architectural press began to report on this innovative German idea, and soon organic office designs started popping up in Sweden.

The concept even traveled across the Channel to England and across the Atlantic to the United States, where companies were eager to get rid of dingy offices and adopt more open, flexible office spaces.

> _"It was one thing to be an expert in Freud or Newton in a university; another to use the insights of Freud to sell a toothbrush..."_

### 6. The office landscape’s noisiness eventually gave rise to cubicle-filled open offices. 

Imagine walking into a big office with an open floor plan, with desks and workers everywhere typing, walking and chatting. How noisy do you think this space would be?

Probably pretty noisy, right? To remedy the issue of noise in the workplace, in the 1960s designers tried placing sound screens between desks. This wasn't entirely successful, however.

Why? Well, whether you were given a sound screen depended on your position in the office hierarchy. For instance, executives definitely had a screen; supervisors sometimes had one; but usually secretaries and other general workers didn't.

In terms of function, the screens didn't work that well. High-pitched sounds or ringing phones could still be clearly heard, for instance, so screens ended up serving merely as a status symbol.

Other attempts to improve office space proved similarly well-intentioned, but not totally effective.

In the late 1950s, for instance, the US firm Herman Miller Furniture Company hired art professor Robert Propst to help expand its office manufacturing business.

Propst held that office work was mental work, which correlated closely with the physical environment. As such, he believed that an office should facilitate health and movement, which resulted in his concept of the Action Office.

The Action Office centered on promoting both standing and sitting for workers, while also integrating elements such as movable display surfaces and shiftable, modular walls to provide privacy when necessary.

But the Action Office, like many other attempts to improve the workspace, was too expensive — which is how the cubicle was invented.

Despite innovative attempts at office design, the demand for cheap solutions was always more pressing. Ultimately, it turned out that companies didn't care much for progressive ideas such as workspace mobility or employee health.

Instead of spending money on carefully designed offices, companies wanted cheap solutions. And that's how the cheap, small, enclosed cubicle came to be.

### 7. Final summary 

The key message in this book:

**The standard office didn't always look as it does today. Rather, the cubicle is a product of many years of changes, such as technological advances and economic necessity, as well as different beliefs of how workers should be organized in a space.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Why We Work_** **by Barry Schwartz**

_Why We Work_ (2015) exposes the flawed assumptions that govern the modern working world. These blinks walk you through the reasons why current management strategies backfire, and show you some far more effective alternatives. In addition, case studies based on company success stories illustrate just how powerful engaged and fulfilled employees can be.
---

### Nikil Saval

Nikil Saval is an American writer based in Philadelphia. He is an editor at _n+1_, a New York-based literary magazine for culture, politics and literature.

