---
id: 5918bb84b238e100066a4e10
slug: cure-en
published_date: 2017-05-15T00:00:00.000+00:00
author: Jo Marchant
title: Cure
subtitle: A Journey Into the Science of Mind Over Body
main_color: D22A2E
text_color: B82528
---

# Cure

_A Journey Into the Science of Mind Over Body_

**Jo Marchant**

_Cure_ (2016) is your guide to the healing power of the mind. These blinks explain the true depth of the placebo effect, how hypnosis can cure illnesses and explain the fascinating, scientifically-supported alternatives to the painkillers and surgeries so prominent in Western medicine.

---
### 1. What’s in it for me? A new appreciation for the power of the mind in curing disease. 

Many of us tend to think of our body as a machine; if we fuel it with food and keep it hydrated it will keep running without any issues. Whenever one of our components stops functioning as it should, the most common reaction is to use a drug or surgical intervention to fix it up and keep the wheels turning.

But the machine metaphor omits the central switchboard of the human body — our mind. It is always our mind that makes a treatment effective, and it is our mind that can make us suffer from pain.

These blinks reveal the power of the mind in curing disease. You will learn about the power of suggestion and placebo, witness the healing effect of meditation, spirituality and hypnosis and see how modern, innovative technology can help us find cures otherwise thought impossible.

You'll also learn

  * why blue sleeping pills are ineffective for Italian men;

  * how a patient suffered from pain he didn't have; and

  * why wounded war veterans play computer games to alleviate their pain.

### 2. The success of a medical intervention depends on a patient’s belief in it. 

Have you ever read the packaging of a prescription drug, only to be blown away by the incredible number of unpronounceable substances it contains? Medicine that sounds this cryptic _must_ be effective, right?

Well, sometimes it is — and sometimes it's about as helpful as a spoonful of sugar.

Regardless of whether drugs are effective, if you want to get better, it's important to believe that they are. In fact, drugs sometimes have powerful effects simply because the person taking them believes they will. This is commonly known as the _placebo effect_, and it's widely proven to impact the efficacy of drugs, from antidepressants to sleeping pills.

Just consider _secretin_, a gut hormone. The company Ferring Pharmaceuticals manufactured a synthetic form of the hormone that rose to popularity in 1998 following anecdotal reports on the NBC show _Dateline_ that it could cure autism.

And this wasn't just make-believe. Studies actually did find that children benefited from the drug. The only thing is, children were also benefiting from a placebo treatment: both the placebo group and the drug group showed a 30 percent reduction in autism symptoms.

Sometimes, even outcomes categorized as "dramatic" or "excellent" can be achieved through placebos. For instance, a 2012 study conducted by Janet Hardy and her Australian research team found that placebo pills had a "dramatic" effect in alleviating cancer pain, comparable to the powerful sedative ketamine.

But that's not to say that the placebo effect is limited to pills. A 2014 study published in the _British Medical Journal_ found that fake surgeries are as effective as real ones in treating a variety of conditions, from angina to arthritis.

For instance, a common treatment for fractured spines is to inject cement into the fractured bones. Surgeons found that about 80 percent of patients benefited from this procedure — but in the end, they realized it was all due to the placebo effect. Performing a fake surgery was just as effective.

And the placebo effect is all around us. One common example is the way people tend to like a bottle of wine more if they think it's an expensive or sophisticated vintage.

So, having confidence in a drug or treatment can have incredible results. But what if you have negative perceptions about these interventions? You'll learn all about this in the next blink.

### 3. Your mind has a powerful influence over your body’s physical symptoms and performance. 

Most people don't bother to read about the side effects of the prescriptions they take and, in the end, that might not be such a bad thing.

The truth is that the negative expectations this information breeds can actually result in bodily symptoms. In this way, believing in side effects is a kind of self-fulfilling prophecy; this is known as the _nocebo_ _effect_, using the Latin word for "I will harm."

Just consider a 29-year-old man who participated in a clinical trial in 2007. Unbeknownst to him, he was assigned to a control group and given placebo pills rather than antidepressants.

During the study, and following an argument with his girlfriend, the man took all of his antidepressants in an attempted suicide. He was taken to a Mississippi hospital with a racing heart and plummeting blood pressure.

However, when the doctors told him that he had only been taking sugar pills, his symptoms immediately subsided. He had anticipated the effects of an overdose and was suffering from a powerful nocebo effect.

Beyond that, your mind sets your body's general physical limits. From an evolutionary perspective, it makes perfect sense to feel exhaustion as a warning to stop activity before running out of energy. But it's very rare for people to actually use up all their physical energy.

Tim Noakes, a South African physiologist, found that the cyclists he studies use fewer than 50 percent of their muscle fibers. Despite this, they feel a need to stop exercising due to exhaustion.

So, your mind can limit your body, but these mental limits can also be stretched through placebo drugs. Consider the case of elite cyclists in Wales who took ineffective substances that they were told would enhance their performance. That information alone was sufficient to boost their speed by two to three percent.

### 4. To fully exploit the placebo effect, make your drugs look right, use rituals and combine with real drugs. 

For most people, the placebo effect isn't anything new. But it might come as a surprise that certain physical features of a drug can also boost its efficacy.

In other words, pharmaceutical companies can benefit from making big pills with a large, recognizable stamp on them. Both size and this type of stamping have been proven to produce a placebo effect that enhances the effects of the medicine.

More specifically, a company that makes sleeping pills might want to make them blue, since this color has a profound placebo effect. Unless you're in Italy, that is. Interestingly enough, blue sleeping pills don't cause a placebo effect among Italian men. Instead, they get excited, because they associate the color blue with the Italian national soccer team.

But regardless of your cultural biases, to harness the full power of the placebo effect, you should combine real drugs with placebo pills and rituals.

First, combine real drugs with placebo because studies show that reduces the amount of the medicine you need to take. One study, for example, found that children with ADHD remained more attentive than normal after a placebo-controlled reduction of their usual ADHD-medicine dosage.

Second, combine real drugs with rituals and routines. The placebo effect is strongest when there's some uniqueness around the moment when you take the medicine. This is because the brain forms connections between where we take medicine and how effective it is.

For instance, leading researcher Manfred Schedlowski gives people strawberry milk with green food coloring and a dash of lavender oil, every time they take their medication. This makes the experience as memorable as possible, thereby amplifying the placebo effect. Other researchers suggest patients listen to a particular song to have the same effect.

### 5. A caring environment helps patients cope with pain. 

Do you ever wonder how much money pharmaceutical companies spend developing new painkillers? Well, the short answer is _a lot_.

Even so, there's already a well-developed treatment that has proven to be highly effective — and it doesn't cost a fortune. It's called human care.

A comforting and caring environment can do wonders in terms of helping people cope with pain. Just consider giving birth, which can be incredibly painful; no painkiller or other medication can reliably reduce the anguish, distress and risk of complications during this process, but human care has been shown to do all of the above.

For instance, in 2012, Ellen Hodnett et al. published a review of studies that covered 15,000 participants. They found that women who were supported by a single caregiver during the birth process were less likely to require C-sections or painkillers. Not only that, but their labor went more quickly and their babies were born in better health.

Or consider the radiologist Elvira Lang who has studied the medical results of _comfort talk_ by doctors, a technique that includes the use of empathetic language and hypnosis while patients undergo laparoscopic surgery. In a study conducted with 241 patients, Lang found that those who received conventional treatment reported maximum pain levels of 7.5 on a scale from 0 to 10, while those who had comfort talk reported maximum levels of 2.5.

The applications of such techniques are wide ranging, and caring psychological support can even aid terminal patients. Palliative care, for instance, is a medical approach that endeavors to raise the quality of life for patients with terminal diagnoses. Palliative specialists work hard to get to know the person they care for and provide the mental support they need, and it seems to work.

A study by renowned oncologist Jennifer Temel found that lung cancer patients who had four palliative care sessions were significantly less depressed, experienced fewer physical symptoms and had a higher overall quality of life than patients who didn't undergo any palliative care.

### 6. Friends rejuvenate your cells and build your immunity. 

The morning after a night of partying with your friends, you might think your body is less than happy with you. But, as it turns out, friends keep your cells healthy.

Chromosomes in the human body are capped by _telomeres_, which prevent these genetic structures from fraying. Each time a cell divides, the telomeres get a little shorter. In this way, much like the rings of a tree tell its age, telomeres reflect the age of cells.

What's more interesting is that the more social contact you have, the longer your telomeres and the more vibrant your health. For instance, a research team led by the demographer Luis Rosero-Bixby found that residents of the small Costa Rican city of Nicoya had oddly high life expectancies. A comprehensive study then found that the telomeres of these people looked younger than normal.

This could be due to the fact that Nicoyans are generally less likely to live alone than other Costa Ricans and more likely to have contact with children. Just that would be enough to make their telomeres grow. Also, once people from this city leave home, their telomeres shorten dramatically.

And that's not the only way social circumstances can influence your health. Friends are also crucial to helping your immune system respond appropriately to stress.

For example, when highly social people experience stress, their genes start producing antibodies to fight viruses and tumor cells. This makes evolutionary sense, since social people are more likely to be exposed to other people's germs, which are best fought through antibodies.

By contrast, socially isolated people's immune systems can trigger inflammatory responses that do long-term harm. This can also be explained by evolution, as such people were more likely to suffer physical threats and wounds, which are healed through inflammation.

The problem is that, nowadays, since physical threats like predators are rare, inflammation has lost its evolutionary benefit.

But that's not all to say that you should go out and make a million friends. According to the social neuroscientist John Cacioppo, the size of your social circle matters less than how lonely you _feel_.

> _"Social isolation was indeed a death sentence, as much a threat to our survival as hunger, thirst or pain."_

### 7. Stress can seriously harm you, but it has benefits too. 

It's never a bad idea to seek out help from your friends when you're stressed out. After all, stress can be catastrophically harmful and, in extreme situations, even deadly.

Consider a 1994 earthquake in Los Angeles. After the tremor, a cardiologist named Robert Kloner noticed that, on the day of the event, 125 people had died of stress-induced heart attacks, about 50 more than on an average day. Similar outcomes were found when researchers looked at the aftermath of earthquakes in Athens, Greece and Kobe, Japan, as well as missile attacks in Israel.

In this sense, acute stress might cause you to drop dead — but chronic stress is almost as bad, as it increases your risk of obesity, diabetes, inflammation and even Alzheimer's.

As proof, consider a 2014 University of California study that looked at mothers of children with and without chronic diseases, and those with chronic stress in particular. The study considered the length of the participants' telomeres and found that those of the mothers who had endured round-the-clock stress looked ten years older than those of the mothers who hadn't.

However, stress isn't _all_ bad. In the end, the impact of stress depends largely on your mindset and how you approach demanding situations. If you don't consider a problem to be solvable, you'll perceive it as threatening and jump into flight mode, making it more difficult to handle complex decisions. Even after the fact, people who hold this mindset experience elevated blood pressure and find it difficult to stop worrying. 

If, on the other hand, you consider a stressful situation to be solvable, you can enter into a _challenge mindset_, which triggers the evolutionary fight response, thereby boosting your performance. During this response, your heart pumps more efficiently and you derive innumerable benefits from the more oxygenated blood it pumps into your brain and body.

In a controlled study, the psychologist Wendy Mendes spoke to a group of participants who had prepared for an important test, telling some of them about the benefits of being stressed. The people who received the information were less anxious about the exam, felt more in charge and scored significantly higher.

### 8. Spirituality is good for your health – and that doesn’t necessarily mean believing in God. 

Religion is a central aspect of everyday life for billions of people around the world. But did you know that people who return from religious gatherings or pilgrimages often report witnessing or experiencing miracles, like supernatural acts of healing?

Well, whether or not such healing miracles truly occur, there is strong evidence to suggest that being religious is good for your health. Plenty of studies have linked a belief in God to favorable health outcomes, such as lower rates of heart disease, high blood pressure and cancer, as well as better outcomes in cases of HIV infection.

Beyond that, studies have found that people who regularly attend church are about 20 percent less likely to die within the following five years.

And perhaps most impressive of all is the finding from psychologist Gail Ironson that, of the 45 percent of HIV patients who became more religious following their diagnosis, all of them had lower viral loads in their blood. This could be due to reductions in stress, since members of religious groups tend to be supportive of one another, and which also makes it all the more important for these groups to avoid negative emotions like guilt.

But if religion isn't your thing, other forms of spirituality are just as helpful. Studies have found that both praying and meditation can lower your heart rate and pressure.

In fact, meditation reduces markers of stress, like the hormone cortisol, as well as those of inflammation. Just consider a study by Elissa Epel and Elizabeth Blackburn, which found that a three-month meditation retreat lengthened the telomeres of participants and slowed the aging of their cells.

To better reap the benefits of this practice, the Buddhist, biologist and yoga teacher Jon Kabat-Zinn brought together aspects of hatha yoga and meditation to create a practice called _mindfulness-based stress reduction_, or _MBSR_. MBSR encourages patients to focus on the present moment, rather than worrying about the future or past.

And it certainly seems to work. Depressed people who participated in such mindfulness practices reported having fewer symptoms, improved quality of life and fewer relapses than those on medication.

### 9. Hypnosis can help cure diseases. 

When most people hear the word "hypnosis," they think of a gimmicky magic show in which a host puts people into a trance-like state and makes them do ridiculous things. Despite this perception, however, hypnosis plays a serious role in scientific research.

For psychologists, hypnotic states are ones of increased focus that also result in reduced peripheral awareness and greater susceptibility. When hypnotized, people remain aware of their environment; as a result, when asked to walk around a room, they'll avoid objects, while those who have been asked to fake being hypnotized will often walk into chairs or furniture.

A good example of the effects of hypnosis comes from a study done by Edoardo Casiglia of the University of Padua, Italy who found that hypnotized people behave as if their imagination is real. To discover this, he told hypnotized volunteers that he would draw half a pint of blood from their arm, but didn't actually do so. The participants reacted as if they had actually lost blood, experiencing lowered blood pressure in the arm in question.

Or consider David Spiegel of Stanford University, who studied the brain areas responsible for color processing. He found that hypnotized subjects react to the instructions given by the experimenter, rather than what's physically in front of them. For instance, when the experimenter said they would see a grey-scale image, the subjects' brains responded accordingly — even though they were actually seeing a full-color image.

So, hypnosis is quite powerful and scientists study it regularly because it can help cure serious diseases. Hypnotherapy has even been helpful in alleviating irritable bowel syndrome, also known as IBS. IBS patients experience painful gut contractions, prolonged diarrhea and vomiting. In 1984, a gastroenterologist named Peter Whorwell used hypnotherapy on these patients, telling them to imagine their guts as calmly flowing rivers.

In the end, he managed to significantly reduce the symptoms of IBS among the hypnotherapy participants, even making a difference in 70 to 80 percent of patients who couldn't be helped through conventional treatments.

Beyond that, early clinical trials show potential for hypnotherapy to be effective in treating autoimmune conditions like Crohn's disease and gastrointestinal disorders like noncardiac chest pain.

### 10. Virtual reality can help people cope with pain. 

In the world of modern digital technology, keeping your attention focused is harder than ever before. That being said, technological innovation — and the distraction it offers — can also help people overcome painful or traumatic events.

Consider virtual reality, which can help take patients' minds off their pain. In an experiment at the Medical Center of the University of Washington, researchers had participants play a game called _Snow World_, in which they moved around freely in an arctic environment using virtual reality gear.

When the patients were given small electric shocks during their gameplay, their bodily responses were less painful than normal due to their distraction. In fact, the virtual reality was able to reduce their pain ratings by 35 percent, compared to just 5 percent for patients who were made to listen to music.

A similar experiment using the same virtual reality game was conducted by doctors at the Brooke Army Medical Center on soldiers who had been injured in combat. One of these soldiers was Lieutenant Sam Brown, whose Humvee had been hit by a bomb in Kandahar, Afghanistan in 2008.

Having suffered severe burns, he often reported maximum pain levels, despite taking heavy medication. While playing _Snow World_, Brown's pain score dropped from the maximum rating of ten out of ten down to a six out of ten.

What's truly remarkable is that, while playing _Snow World_, soldiers would only spend around 22 percent of their time thinking about their pain, compared to their normal habit of thinking about it 76 percent of the time.

Virtual reality devices can even be used to engage in hypnosis. For instance, rather than playing a game, patients can load prerecorded hypnotherapy sessions through their virtual reality goggles. In such sessions, a patient might see a forest scene and hear a soothing voice instructing them on how to relax and reduce their pain.

And these sessions can really help. Just consider the work of the psychologist David Patterson, who studied patients with broken bones and gunshot wounds. He found that virtual reality hypnosis can be even more effective than _Snow World_ in preventing what is often a typical rise in pain over the course of a day.

### 11. Modern technology is transforming health care. 

You might think that virtual reality is a cold, or inhuman application of technology. But the truth is, modern approaches to medicine would be practically impossible without technological advances.

Some modern technologies can even help people regain control over their bodies, and one such technology is called _biofeedback_. This term refers to any technological device that enables patients to monitor their bodily functions in real time.

For instance, patients can track their heart rate over time, thereby determining their _heart rate variability_, or _HRV_. This is potentially indispensable information, as neuropsychological studies have suggested that a high HRV leads to more flexible emotional responses to stress and stronger, more pleasant social relationships.

By changing their breathing patterns, patients can affect their heart rate and see the immediate results on a computer monitor. The physician and health consultant Patricia Saintey even claims that you can learn to raise your HRV through daily meditation and conscious breathing.

Beyond that, HRV biofeedback has been found to reduce the severity of stress-related conditions like asthma. A St. Petersburg, Florida hospital trains asthmatic children to increase their HRV using a video game that pushes them to reach a high level of HRV.

Finally, innovative technologies can directly diminish the symptoms of diseases. For example, a new and promising alternative to anti-inflammatory drugs is called _vagal nerve stimulation_, or _VNS_. In this treatment, patients run a small magnet over their chests, causing an implant to send a small electric shock to the vagal nerve in their neck.

VNS is being used to treat epileptic seizures and has caused patients to report better moods, regardless of the symptoms they experience. A good example comes from the University of Amsterdam, where researchers use VNS to help patients suffering from the autoimmune disease rheumatoid arthritis by electronically calming their overactive immune systems.

A 2015 report from the university stated that more than half of the 20 patients in the study demonstrated substantial improvements, which included regaining the ability to walk or even becoming entirely free of pain. This was even true of patients for whom no other therapies had been effective.

### 12. Final summary 

The key message in this book:

**The mind is a powerful thing, and it has the ability to heal any number of ailments, including serious physical illnesses. Through the use of techniques like hypnosis and technologies like biofeedback, modern technology and science is revolutionizing not only health care outcomes, but also our understanding of the relationship between the body and mind.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Body Keeps the Score_** **by Bessel van der Kolk**

_The Body Keeps the Score_ (2014) explains what trauma is and how it can change our lives for the worse. These blinks investigate the wide-ranging effects experienced not only by traumatized people, but also those around them. Nevertheless, while trauma presents a number of challenges, there are ways to heal.
---

### Jo Marchant

Jo Marchant, PhD, is a microbiologist, award-winning science journalist and the author of _Decoding the Heavens_ and _The Shadow King_. She has been the editor of various scientific journals, such as _New Scientist_ and _Nature_, in addition to writing for _The Guardian_ and _The_ _Economist_.

