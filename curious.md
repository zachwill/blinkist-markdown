---
id: 5485cfd96331620009940000
slug: curious-en
published_date: 2014-12-10T00:00:00.000+00:00
author: Ian Leslie
title: Curious
subtitle: The Desire to Know and Why Your Future Depends on It
main_color: 917547
text_color: 5E4C2E
---

# Curious

_The Desire to Know and Why Your Future Depends on It_

**Ian Leslie**

_Curious_ is all about one of the most fundamental forces for our success as well as our perception of the world around us: our curiosity. The book offers a unique look into how curiosity works, what you can do to nurture it and what sorts of behaviors stifle it.

****

---
### 1. What’s in it for me? Discover why you just can’t put down your favorite book. 

While there are many things that separate human beings from animals, one of the most remarkable is that we are the only creatures on earth capable of asking _why?_ Each and every human being is born with a strong desire to know about the world around her, both out of necessity and curiosity.

It should therefore come as no surprise that children between the ages of three and five ask around 300 questions per day!

Yet, as we grow older, we reach a point at which we are no longer interested in discovering new things. We run on autopilot, rely on our old assumptions and stop asking questions. And while this might be natural, it's certainly not desirable.

If you want to be successful in your professional and personal life, you have to be curious. Curiosity is absolutely necessary in your studies, career and personal life if you want to be successful in any!

In these blinks, you'll discover

  * why the internet is good for your brain if you're smart, but will just make you dumber if you're already dumb;

  * why Andy Warhol liked boring things; and

  * why most of us act as if we already know everything there is to know.

### 2. Curiosity is caused by information gaps that we want to close. 

A blinking ad on a website, a bizarre circumstance of a murder mystery or an apple falling from the tree: all these pique our curiosity. But why? What do these drastically different things have in common that captures our attention?

According to psychologist George Loewenstein, curiosity is a result of _information gaps_. Put simply, information gaps are missing pieces to a puzzle. When we realize that there is something we don't know, we then suddenly _really_ want to find out what it is.

For instance, if you're watching a thriller, there might be a murder, i.e., a piece of information. However, you're missing some crucial information to complete the picture: who _committed_ the murder? This is the information gap, and we continue watching with the hope that this gap will soon be closed.

Storytellers of all sorts use the principle of information gaps all the time; in fact, a good story depends on it! They create information gaps, and then close them, only to open yet another, and then another; it's how they keep us engaged in their stories, feverishly turning page after page.

But it's not the mere absence of information that sparks curiosity. Curiosity can't exist in a vacuum; we must first have _some_ knowledge about the subject. The gap exists only between something we already know and something we don't yet know, but would like to find out.

For example, if someone tells you all about the riveting performance he saw at a classical concert, you can't respond to this information if you know nothing about classical music. Thus, your curiosity remains uncaptured.

However, if he then tells you that this music also appeared in your favorite film and that the composer was born in the same city as you, then you might have enough relevant information to become curious.

### 3. There are two types of curiosity: diversive and epistemic. 

Curiosity is what brought humankind to the moon. Yet it's also the force that leaves us wasting half our lives scrolling through our newsfeeds on social media. This is because not all curiosities are created equal:

Curiosity is divided into two types. The first, _diversive curiosity_, is little more than a craving for new input, i.e., the desire for more novelty and excitement. On the one hand, it's what motivates our engagement in a topic in the first place. On the other hand, it can also be impulsive, superficial and difficult to resist.

Moreover, at it's very worst, diversive curiosity becomes aimless, little more than distraction.

We've all succumbed to this kind of curiosity late at night on the web. Clicking on one really interesting-looking link quickly leads to clicking another, and then another. Eventually, we realize that we've spent the past three hours watching cat videos on YouTube, far past our bedtime.

This contrasts with _epistemic curiosity_, which is all about the desire to know something new. This kind of curiosity goes far deeper and takes more work to sustain. It is a conscious choice, requiring self-discipline, effort and focus.

All good scientists and artists utilize their epistemic curiosity. Take Charles Darwin, for example, who found a strange barnacle during his journey to South America. This barnacle made him so curious that he dedicated the next eight years of his life to conducting research on this single species, dissecting samples under a microscope.

This isn't to say that you have to choose one kind of curiosity over the other. In fact, to achieve sustainable learning, you have to combine both types: diversive curiosity is a good way to familiarize yourself with basic information about a topic. But you'll have to supplement it with epistemic curiosity in order to help you dig deeper, gather specialized knowledge and avoid the temptation of distraction.

> _"I have no special talents. I am only passionately curious." - Albert Einstein_

### 4. We are all born with a desire to know, but this desire fades with age if we don’t nurture it. 

It's common knowledge that babies and small children are relentlessly curious about nearly everything. They point at things, babble and ask questions and they always want to know "why."

Indeed, we're all born with an innate desire to know. The instinctual knowledge that there are things we don't know, but that other people _do_ know, is incredibly powerful. This is the force that makes the inquiring minds of young children relentless: children between the ages of three and five ask around 300 questions per day!

So, if every child begins life with the same drive to learn, why do some people maintain inquiring minds while others don't? It all boils down to their surroundings, particularly during the formative first years of life.

Take, for example, the way that babies point at things they're interested in. When they point at something novel to them, they expect a reaction from those around them. If this curiosity is met with stony silence, then they're more likely to learn to abandon exploratory inquiry.

In fact, studies demonstrate that children whose parents react to their pointing continue the pointing behavior for longer and are better at acquiring language and knowledge later on.

Unfortunately, the older we become, the more our curiosity declines until we eventually reach a point at which we no longer feel we need to learn anything: a _saturation point_.

As adults, we have a considerable amount of knowledge that we simply don't question, yet still heavily informs our actions.

This accumulated knowledge is helpful in situations like driving, where we don't want to expend the energy interpreting the stimuli on the road, such as traffic signs and lights, and where it is simply easier to act automatically.

However, our faith in our accumulated knowledge makes us intellectually lazy, causing us to suffer from the _overconfidence effect_ : most of us think that we already know everything.

Now that you know a bit more about the nature of curiosity, our next blinks will examine how the internet affects our desire to know.

> _"If you allow yourself to become incurious, your life will be drained of color, interest and pleasure."_

### 5. The internet has produced a divide between the curious and the incurious. 

As far as learning is concerned, the internet has proven to be both a blessing and a curse. Depending on how you approach it, the internet enables you to easily learn everything about complex topics, like the theory of relativity, plate tectonics or French history, or you can spend all day looking at cat videos and pointlessly arguing with strangers about them.

In truth, the educational potential of the internet has yet to be fully realized. According to a study by The Kaiser Foundation on the media habits of Americans, children now spend an average of _ten hours_ per day on devices, which represents more than a 50-percent increase since 1999. However, the majority of this time is spent on entertainment, not on education.

Moreover, internet usage actually widens the gap between those who want to learn and those who don't. There is a growing _cognitive polarization_, a division in our society between the curious and the incurious. The internet makes curious minds more curious, while the incurious spend their time on the internet with entertainment, thus further diminishing their interest in learning.

There are two circles: a vicious and a virtuous one. Those who already thirst for knowledge realize that there's so much they still don't know, which makes them even more curious and eager to know more. The opposite applies to the incurious.

This disparity in curiosity will further widen social divisions via the educational system. In schools and universities, students with a higher intellectual curiosity will be more successful, as they have the willingness and capacity to learn more. Willing to explore and finding pleasure in accumulating knowledge, these students will get better grades, better jobs and better pay.

It's easy to blame the internet for how it encourages or hampers the development of our epistemic curiosity. However, the only person that can make you stupid is yourself: epistemic curiosity is a conscious decision, and thus so is ignorance.

> _"The internet is making smart people smarter and dumb people dumber."_

### 6. The internet stifles our curiosity and creativity by making it too easy to access information. 

It's an undeniable fact that the internet has revolutionized our access to information. Everyone who has an internet connection can learn about even the most obscure things. But it's also worth asking how this affects our learning, curiosity and creativity.

With regards to learning, the mere fact that information is easily and immediately accessible doesn't mean we are better at accumulating knowledge. This is because the internet spoils us: knowing that the knowledge is always just a click away, we tend to not make the effort to store it in our memory.

When we casually Google the information we need and accept the first answer that pops up, that knowledge isn't internalized and will soon vanish. In contrast, when we really make an effort to discover something new — for instance, by going all the way to the library to find information — then the knowledge we gain is added to our long-term memory.

What's more, Google answers our questions so precisely that it closes all information gaps. As you've already learned, curiosity results from information gaps and unanswered questions. But the internet has all the answers! It has all the information — there is no such thing as an information gap and thus no place for curiosity to take root.

Google's precision also makes it less likely that the user will stumble into unknown fields of knowledge and thus discover new interests. This has considerable effects on our creativity, which is formed by the combination of separate ideas that merge into something new. Creativity relies on the random and unexpected collision of knowledge in order to make novel connections between different areas of knowledge.

Creative people and innovators always have a wide knowledge base. Take Steve Jobs, for example, whose interest in many diverse areas such as Eastern philosophy, the Bauhaus art school, business and poetry all culminated into Apple's innovative projects and success.

You've learned how technology can make us intellectually lazy. Our final blinks will show you how you can stay curious.

> _"Anyone who stops learning facts for himself because he can Google them later is literally making himself stupid."_

### 7. Never stop asking questions. 

How many questions do you ask per day? You could probably stand to ask a few more, as questions are what makes your mind hungry and inquiring. Indeed, every answer is preceded by a question.

Asking questions is essential to uncovering the information you need. Unfortunately, it's not the kind of skill that can be easily passed on: you have to experience the power of good questions firsthand in order to improve.

However, we can help people ask more questions by leveraging the fact that question-asking seems to be contagious: several studies have shown that children who were asked more questions by their parents began to ask more questions in return.

Class, too, plays an important role in our development. Middle-class children, for instance, ask more curiosity-driven questions that start with "why" or "how," and ask more questions in general, than working-class children.

The difference is observable as early as age two, and these children have a much higher chance of success in their education.

As adults, however, the habit of asking questions can fall by the wayside. Sometimes, we fear that asking questions will make us appear stupid, as asking questions is an admission of ignorance. Other times, we're too busy to be inquisitive, or simply never developed the skills to ask relevant questions.

History has shown us, however, that going the convenient route and deliberately choosing ignorance over curiosity can lead to disasters. In fact, this is exactly what happened in the 2008 financial crisis:

While bankers traded highly intricate and volatile financial products, they could have easily paused at any time to question the inherent risk in their actions, but they chose not to. They were so busy reaping huge profits from their trades that they failed to learn anything more about them and thus failed to foresee the looming financial meltdown.

So, think of question-asking as a valuable skill that can be sharpened with practice, and try not to let it get dull.

### 8. Make an effort to gather knowledge – it will make you more creative and curious. 

Knowledge and creativity are often perceived as two opposing poles, where the mere gathering of facts is seen as detrimental to children's blooming creativity and curiosity. However, this couldn't be further from the truth! In fact, it's absolutely necessary to first accumulate a database of knowledge before any creative work can happen.

As you've already learned, creativity is the product of novel connections between seemingly unrelated thoughts and ideas. Consequently, the more you know, the more connections you can make.

Take William Shakespeare, for example. Historians know that he attended a classical school, where he learned all about Greek and Latin as well as the writers and thinkers of these classical cultures, such as Seneca and Cicero.

He used this broad and deep knowledge to create plays with a wide variety of themes, set in various locations and at various points in history. Romeo and Juliet, for example, took place in Verona, Italy, far away from Shakespeare's London home.

Curiosity, just like creativity, is fuelled by knowledge and facts: the more you know about a subject, the more you discover how much is still left to uncover, which then leaves you with information gaps that in turn further ignite your curiosity.

This is why going to school is often so difficult and frustrating for children when they are young. Many fields, such as history, require context and foundational knowledge in order to be understood. But children have neither the context nor the knowledge, so it's hard for them to sustain their curiosity.

However, it's not enough to haphazardly learn about everything that you come across. As you'll learn in the next blink, the ways in which you gather and store your knowledge are also important.

### 9. The modern world requires you to be both a specialist and a generalist. 

There are two different ways in which you accumulate knowledge: you either learn a great deal about a few areas, thus making you a _specialist_, or you learn a few things in many areas, which makes you a _generalist_.

This distinction roots back to the ancient Greek story of the hedgehog and the fox. The fox has various clever strategies to evade his predators, while the hedgehog has only one tried-and-true trick — he hunkers down and uses his spikes for protection. As the Greek poet Archilochus puts it: "The fox knows many things, but the hedgehog knows one big thing."

This distinction is especially relevant to knowledge-based careers, such as software engineering or research science. To achieve success in these fields, in which the line between different areas of knowledge becomes blurred, you have to be both a hedgehog and a fox.

On the one hand, you have to learn a massive amount about one or two big things in order to give yourself a knowledge-based advantage over your competitors. On the other hand, as your work becomes more complex, it's bound to cross over into other disciplines.

If you work in the music industry, for instance, then you also need to know about social media. Linguists, too, need a good grasp of data analysis, and football coaches need to know about psychology.

In fact, most of humankind's great thinkers represent both the fox and the hedgehog. Take Charles Darwin, for example:

He was a specialist in biology and knew everything there was to know about the life cycles of earthworms. However, he was also interested in other disciplines, and combined seemingly unrelated knowledge, such as the theories of the economist Thomas Malthus regarding the logic of population growth, in a way that eventually enabled him to think outside of the box and lay out his groundbreaking theory of evolution.

### 10. Anything can be interesting with the right perspective. 

Since 2010, something strange happens in London each year: young people come together and listen to presentations on topics often considered tremendously uninteresting, such as bus routes, electric hand dryers and the history of supermarket self-checkout machines. It's called the Boring Conference — and yet, the attendees all seem quite entertained!

The Boring Conference is all about the mundane and overlooked phenomena in everyday life. But these things are not intrinsically boring; the Boring Conference shows that our interest in a subject has everything to do with how we approach it. In this way, _anything_ can be interesting.

Andy Warhol surely inspired the Boring Conference's founders. He once famously quipped that he "like[s] boring things," and proved that boring things can actually be quite remarkable, turning one of the most ubiquitous and unremarkable objects of everyday life, a can of Campbell's soup, into a world-renowned work of art.

If you pay close enough attention, then anything can turn out to be amazingly interesting.

When we become bored with something, we tend to fault the thing that bores us. However, the _thing_ is not the problem. _We're_ the problem. We simply need to find the right perspective.

This is what the author Henry James did. On the outside, his life appears quite boring. However, he spent his whole life focused on discovering new interesting things in those ordinary experiences.

Many of his novels grew out of boring anecdotes recounted to him by his friends. He would mull over these stories for days, pondering the reasons why people behave the way they do, what they think and what their backgrounds might be. He then transformed these boring anecdotes into vivid fiction now considered to be among the best ever written.

In essence, curiosity is a choice. You choose your perspective, and thus you can choose never to be bored again.

### 11. Final summary 

The key message in this book:

**Curiosity can be thought of as a cognitive muscle that has to be constantly nurtured and fed with new knowledge in order for it to grow and flourish. If you can master this art, then you'll have a greater chance at being more fulfilled in your job, at school as well as in your personal life.**

**Suggested** **further** **reading:** ** _How We Learn_** **by Benedict Carey**

_How We Learn_ explains the fascinating mechanisms in our minds that form and hold memories, and shows how with this information, we can better absorb and retain information. You'll explore the many functions of the brain and gain practical advice on how to better study and learn.
---

### Ian Leslie

Ian Leslie works as an advertiser as well as a writer on culture and politics in _The Guardian_ and _The New Statesman_. In addition, he is the author of the critically acclaimed book _Born Liars: Why We Can't Live Without Deceit_.

