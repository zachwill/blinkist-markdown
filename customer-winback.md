---
id: 5534fb5466386300077a0000
slug: customer-winback-en
published_date: 2015-04-22T00:00:00.000+00:00
author: Jill Griffin and Michael W. Lowenstein
title: Customer WinBack
subtitle: How to Recapture Lost Customers – And Keep Them Loyal
main_color: F7D331
text_color: 786618
---

# Customer WinBack

_How to Recapture Lost Customers – And Keep Them Loyal_

**Jill Griffin and Michael W. Lowenstein**

Maintaining a happy customer base shouldn't feel like herding cats. _Customer WinBack_ (2001) reveals how companies can identify at-risk clients and win them back before they disappear from their databases. By using these savvy tricks, businesses can also refocus on their existing customers in order to cut costs and drive revenue over the long term.

---
### 1. What’s in it for me? Identify the customers who might leave you, and win back those who have. 

Of all the misconceptions in business, one of the most dangerous is the idea that once a customer is gone, they're gone for good. Sayonara, you say, happy trails!

But it's simply not true. When a customer cancels your service, or wants to cancel your service, there's a big opportunity for you not only to prevent them from leaving but also to improve your reputation, save money, and win more customers.

These blinks discuss every angle to winning back lost customers — including making sure they don't leave in the first place. From identifying at-risk customers to knowing when to let go, these blinks are essential reading for anyone who wants to ensure their company's market isn't slowly bleeding out.

In these blinks, you'll discover

  * how customer retention rates can trick you into a false sense of security;

  * why letting a customer go can help you win more customers later; and

  * how to use "CPR" to save your business.

### 2. Don’t ignore customer defection or be apathetic about winning them back. 

Many business owners believe that, since the market is full of potential customers, it's not worth it to spend time and money winning back the ones they've lost. "After all," they reason, "what's the loss of one customer compared to the many we've retained?"

The problem with this thinking is that _retention rates_, i.e., the number of customers you keep over a certain period, can be misleading.

To illustrate this, imagine a college that retains 80 percent of the students in each class from one year to the next. Seems healthy, right? But if the college starts with a freshmen class of 1,000 students, then the sophomore class will fall to 800, the junior class to 640 and the senior class will have only 512 students.

In addition, firms are largely unaware of both the substantial losses associated with customer defection and the substantial benefits of winning customers back.

The loss of just a single long-term customer means that companies have to expend resources — marketing materials, sales personnel costs, etc. — to acquire a new customer to make up for lost revenue.

Moreover, losing a customer offers firms a valuable experience. There is a _reason_ customers leave in the first place, and uncovering that reason can help to both win customers back, retain existing customers and even acquire new ones.

Unfortunately, however, many companies see lost customers as dead opportunities. Often, businesses have developed a caricature of the lost customer as merely a disgruntled individual with a chip on their shoulder, who would under no circumstances return to their company. It's no wonder that they don't even try to woo them back!

But this isn't exactly true. In fact, studies have found that the average firm is 60 to 70 percent likely to successfully sell again to "active" customers and 20 to 40 percent likely to successfully sell to lost customers. Now compare this to new prospects, where the odds of striking a deal are only five to 20 percent.

### 3. It’s time for your company to establish customer loss and win-back initiatives. 

Today's companies have far better technological tools available to them to win customers back than in the past. There's never been a better time to make winning back customers part of your business.

Nowadays, you don't have to write a letter to every single customer you want to reach or win back. Digital communication and inexpensive print communication solutions give you an opportunity to communicate with them in a creative, individual way.

The online direct-mail service ELetter Inc., for example, allows you to launch an entire direct-mail campaign in minutes. All you have to do is upload your address list and document file, choose from among various formatting options and in one to four business days your customers will receive your message.

What's more, in any market space there are a limited number of best customers, which makes it doubly urgent that you keep them close.

High-value customers deliver six to ten times as much profit as your low-profile segment. Furthermore, because there is such a small number of good customers out there, you'll need to do everything you can to keep your competitors from snatching them up.

For example, imagine that you run a yoga studio and have lost a customer. Because there are only a limited number of people who live in your neighborhood _and_ practice yoga, losing that one customer is a big deal. You simply can't know when the next potential customer will walk through the door.

Similarly, this one customer is just as big of a deal for the rival yoga studio down the street!

Finally, win-back programs can give you a real competitive edge. If you consider all the benefits that come with a solid win-back strategy — especially the cost savings and revenue retention — then your company will be well positioned to lead in your market.

Clearly, customer retention is extremely important for your business. The following blinks will show you how to prevent your customers from abandoning your business.

### 4. Customer retention strategies have two phases: the termination phase and the revitalization phase. 

It's a fact of life that each and every company will lose customers at some point or other. Some, however, are smart enough to have a special program or strategy to re-recruit these lost customers.

The first phase in winning them back is the _termination phase_ : When members opt out or cancel their contracts, ask them _why_, and offer them an alternative that counteracts their reasons.

During this phase, representatives will need access to all relevant customer data, and should also be well trained in offering alternatives.

Take DoubleDay Direct, for example, which works with and manages book clubs worldwide. When a member calls with the intention of canceling, they always ask why. If the customer says he's annoyed that he receives too much mail from DoubleDay Direct, the rep can simply offer to suspend those mailings.

The extra cost required to provide this kind of customer service is worth it, as these current customers boost profits more than new customers would after you account for the advertising it takes to win them in the first place.

And if the customer _still_ wants to cancel, take a good look at the customer's value to your company. If he provides only a minimal benefit, then just cancel his contract and thank him for his business.

The second phase is the _revitalization phase_, in which you contact lost customers about reactivating their membership.

One of the biggest benefits of expired members or a lost customer is that they're familiar with your products and services. When a contract expires, you have the opportunity to re-recruit them with minimal effort.

Doubleday discovered this to be true when they sent out two separate mailings with the same offer to externally acquired leads and expired members. As it turns out, expired members proved to be more profitable than the external list!

### 5. You can save at-risk customers by using CPR: comprehend, propose and respond. 

It's a lot easier to treat an illness when you catch it in its early stages. You can think about winning back customers the same way: Recognize the symptoms that might cause them to leave, and you'll have a better chance of keeping them as customers.

Use CPR: _comprehend_, _propose_ and _respond_.

During the _comprehension stage_ you identify at-risk customers, determine their lifetime value and try to solve whatever problem they're having.

Start by asking yourself: How much revenue has this customer generated compared to the cost of serving him? Then, listen to the customer's problem intently, and make him feel that his request is taken seriously. It's helpful here for customer service reps to paraphrase the customers' grievances to them to ensure that they are understood.

Before you make a _proposal_, you'll need to ask the customer what you need to do to keep him as a customer.

Sometimes, you'll find that they don't want anything at all; they just wanted to know that you care about their grievances. Other times, they'll be able to tell you exactly what you need to do to keep their business.

Say, for instance, that a high-value customer calls to cancel her internet service. She tells you that she's not able to use WiFi. So you offer her a replacement wireless modem. Problem solved! Both sides are satisfied and you kept your customer.

But it's not always that easy. Sometimes your customers will _respond_ to your offer negatively, in which case your customer service employees need to be trained to handle the ensuing conversation.

If the previous customer wanted something more substantial than a replacement wireless receiver, for example, the company will need to prolong the discussion with the customer. Depending on the customer's demands and value to your business, you may decide to sweeten the deal or simply let her go.

### 6. Surveys and frontline staff can identify at-risk customers. 

The CPR method is good for keeping customers once they've already voiced a complaint. But how can you identify at-risk customers _before_ it even gets to that point?

One way is to use customer research surveys, which offer valuable information about how customers feel about your product or service.

Take the Royal Bank of Scotland, who in 1998 received troubling feedback from a survey they conducted: 69 percent of their customers agreed with the statement "I don't have a relationship with RBS; all I do is pay money."

Recognizing that these were at-risk customers, the management reacted quickly: they called _every_ customer to offer them opportunities to get more value from the services they already used, and asked if there are other financial services they could assist them with. As a result, 86 percent of Royal Bank customers felt appreciated and agreed to participate in regular follow-up calls from a bank representative.

Another trick is to have your customer-facing staff collect information about your customers during chats or phone calls. These employees, who are frequently in contact with customers, represent a huge opportunity to identify clients who are at-risk.

For example, the San Antonio-based financial services giant USAA realized that its telephone sales team and service reps were in a unique position to gather information about their customers. So they created ECHO, an information collection system that gathers data related to feedback, complaints, competitive threats, market trends and new product opportunities.

If the system discovers an issue that puts a customer at risk of leaving, it's assigned to an internal action rep for quick resolution and customer recovery. And it works: using this program USAA achieves a 98 percent customer renewal rate.

### 7. When you want a lost customer back, separate the immediate from the longer-term win-back opportunities. 

If you've been in business for a while, then you know that winning back a customer isn't always as easy as making a single phone call. Sometimes it takes time and a more deliberate plan of action.

The quick win-back opportunity is only possible in the moments your customer wants to cancel, or immediately afterward.

During these periods, your company's credibility is at risk, so you'll want to take immediate steps to save the customer relationship. Listen to their issues carefully and acknowledge the customer's pain. Be generous in your resolution offer, but be equally gracious regardless of whether you win them back or not.

Your customer service reps need the power to act immediately. No waiting on managers for approval!

Sometimes, however, when a customer terminates her contract or discontinues your service, there is no opportunity for an immediate win-back. In those cases, you'll need to switch to a longer-term plan B.

This plan involves accepting your loss and letting the customer off the hook so that they will recommend you to others _despite_ the fact that they won't be using your service anymore.

Tell them that you're confident that they're making the right decision, that you wish them the best of luck, but you hope they'll reconsider your company in the future.

For example, if you're a golf club manager and one of your members has to cancel her contract because of arthritis, then you just have to accept it. There's no point trying to convince her to continue paying for a service she can't use!

Instead, treat her with respect and let her go without a fuss. This way, she's more likely to recommend you to a friend, and that's a long-term win!

### 8. Final summary 

The key message in this book:

**It's far more expensive to acquire new customers than it is to keep existing ones. For this reason, it's important that businesses do everything they can to recognize the signs of an at-risk customer in order to win back their loyalty and keep them around for the long haul.**

Actionable advice:

**Don't give up on the customers you think you've lost.**

The next time you send out a special offer to a list of prospects, add your former customers or expired members to the mailing list. Since they're familiar with your product or service, they're more likely to be receptive to your services and might even decide to return or buy again!

**Suggested further reading:** ** _Be Our Guest_** **by Disney Institute and Theodore Kinni**

_Be Our Guest_ (2011) reveals Disney's key tenets and principles of outstanding customer service and how following these has helped the company become the successful business empire it is today.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jill Griffin and Michael W. Lowenstein

Michael W. Lowenstein is managing director of Customer Retention Associates and author of a number of books, including _Customer Retention_ and _The Customer Loyalty Pyramid._

Jill Griffin is a customer loyalty specialist who has worked with huge names such as Dell, Wells Fargo and Microsoft. In addition, she is also the author of the bestseller _Customer Loyalty: How To Earn It, How To Keep It_.

© Michael W. Lowenstein, Jill Griffin: Customer WinBack copyright 2001, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

