---
id: 5885f6ac5806db0004d7fef7
slug: da-vincis-ghost-en
published_date: 2017-01-27T00:00:00.000+00:00
author: Toby Lester
title: Da Vinci's Ghost
subtitle: Genius, Obsession, and How Leonardo Created The World In His Own Image
main_color: C26434
text_color: 8F4A26
---

# Da Vinci's Ghost

_Genius, Obsession, and How Leonardo Created The World In His Own Image_

**Toby Lester**

_Da Vinci's Ghost_ (2012) takes you back in time to the Renaissance, when Leonardo da Vinci created his iconic drawing, the _Vitruvian Man_ — a naked figure framed in a square and circle. Today, this drawing can be found everywhere — it's even featured on coins in some countries. Find out what was going on at the time and what Leonardo might have been thinking when he embedded art, science and philosophy all in one image.

---
### 1. What’s in it for me? Follow the intricate path of a Renaissance genius. 

The _Vitruvian Man_ is one of the most famous drawings of all time. Crafted five centuries ago by Leonardo da Vinci, the sketch of a human male body, perfectly arranged inside a square and a circle, is a masterpiece of geometry and anatomy.

But have you ever stopped and wondered why Leonardo da Vinci created this image?

Well, learning why he did, and how he did it, can teach us a lot about the central ideas and beliefs that forged the Renaissance. These blinks will show you exactly what these beliefs and ideas are.

In these blinks, you'll learn

  * that you don't need a university degree to become a genius.

  * that questioning authority is as important as deferring to it; and

the secret meaning hidden behind the image of Leonardo's _Vitruvian Man_.

### 2. Many of the important cultural ideas of the Renaissance era can be traced back to ancient Rome. 

Have you ever been to Rome? Well, even if you haven't, you might be familiar with ancient Rome's architecture. Some of it still stands. There are multiples reasons why these structures have endured — a main one being the intentions and ideas behind them.

The architecture of ancient Rome was meant to impress and influence both people and gods. After Rome fell, it continued to inspire people, even during the Renaissance era of Leonardo da Vinci.

In the first century BC, Caesar Augustus became Rome's first emperor. He decided that the capital city needed a makeover to re-assert its power. At the time of his rise to power, Rome was in disarray. Years of war, political chaos and mismanagement had left the city in shambles.

Augustus was convinced that Rome's shabby state was the result of its citizens not showing enough love to the gods: Romans were being punished for letting their religious traditions lapse and their temples fall into disrepair.

Augustus spared no expense. Monumental new temples were erected, along with other imposing structures made of marble. The impressive and lavish buildings were sure to appease the mighty gods and improve Rome's status in the eyes of the mortals.

This architecture would continue to impress one generation after the next. But it wasn't Rome's only legacy. During the Renaissance, people were also impressed by the Roman dedication to collecting and compiling knowledge.

Augustus loved things to be in perfect order, and this led to the creation of style guides and rule books that sought to weave together all the various areas of knowledge and pieces of information. The Romans had a name for these catalogued and cohesive bodies of knowledge: a _corpus_.

Marcus Vitruvius Pollio worked under Augustus as an architect and military engineer, and when he noticed that there wasn't a corpus on architecture, he took it upon himself to fill this lacuna. Around 25 BC, he completed the _Ten Books on Architecture_ and dedicated it to Augustus.

### 3. Vitruvius sparked the ideas behind the importance of squares, circles and the divine human form. 

Today, there is an infinite number of manuals and tutorials you can consult if you're looking to add a new room to your house. But you're not going to find many how-to books that also explain why architecture is a divine calling.

Yet this is exactly what Vitruvius's _Ten Books_ did: it combined the nuts and bolts of _how_ to build something with profound and philosophical ideas about _why_ to build it.

One of Vitruvius's most important ideas was that all structures are essentially made up of circles and squares. This wasn't simple geometry. Vitruvius was making a metaphysical and philosophical statement. For him, a circle was a symbol of unity, wholeness, the godly and the cosmic.

Plato made a similar observation. In the _Timaeus_, he compared the cosmos to a sphere — the most perfect and orderly of shapes.

If the circle was cosmic and godly, then the square was seen as its earthly and secular counterpart. Its four sides perfectly represented the four cardinal directions: north, south, east and west. Vitruvius saw the square and the circle as the two ideal shapes, and he also saw a connection between these shapes and the other divine form: the human body.

After all, God created man in his own image. So the human body should also function as a source code for harmonious design.

This is what led to the idea of representing the human male body, with its arms and legs stretched out wide, as fitting perfectly within the classic shapes of a circle and a square. In turn, this led to the idea of the _microcosm_ : where the form of the human body is seen as symbolizing the greater universe.

Architecture and design began to embrace these profound ideas. In an attempt to better align them with the heavens, temples were now being built with the the human body in mind. Vitruvius wrote that a temple shouldn't be constructed "unless it conforms exactly to the principle relating the members of a well-shaped man."

> _The books is still studied in architecture programs today because it was the first to codify the famous architectural orders used by the Greeks: Ionic, Doric and Corinthian._

### 4. Leonardo lacked a formal education; he learned from nature before becoming an artist's apprentice. 

With this foundation set, let's turn to Leonardo da Vinci, who was born in 1452 to an elderly landowner living in the small Italian town of Vinci.

Like many other boys in the Tuscan countryside, Leonardo grew up working the fields and making wine and olive oil. But he also took the time to explore the natural world around him. Since he had little in the way of formal schooling, this first-hand experience guided much of his thinking, and it would continue to play a key role in much of his future work.

When he turned 14, it was time for Leonardo to leave his small hometown for the opportunities that Florence provided. It was there that he found a job working in the studio of artist Andrea del Verrocchio.

Even though he was working in one of the most highly respected workshops, Leonardo's life consisted mostly of tedious work: he ran errands, cleaned tables, swept floors. But it did offer him the chance to study painting, sculpture and drawing. And many believe that Verrocchio probably insisted that his apprentice study the ideal proportions of the human figure and learn them by heart.

Leonardo also found the time to read the works of Leon Battista Alberti, a vital thinker and prototypical Renaissance man. He was an architect, cartographer, linguist and poet who had written an influential treatise in 1435 titled _On Painting_.

Alberti used mathematical averages to calculate ideal human proportions, and many of his ideas can be traced back to Vitruvius.

Leonardo wasn't yet fluent in Latin, but the theories of Vitruvius were already influencing his development and playing an important part in his life.

> _Leonardo wrote from left to right and formed his letters backward, probably because he was left handed and writing normally would drag his hand across the wet ink._

### 5. Leonardo’s position as an outsider led him to develop his own unique methods. 

In 1481, following some dangerous accusations of homosexuality, which was considered a crime at the time, Leonardo fled Florence for Milan.

But no matter where he was living, Leonardo couldn't escape the feeling of being an outsider.

At this time, ancient Latin texts were given the highest respect, and the ideas contained therein influenced the court artists above all else. But Leonardo's Latin was meager at best; he couldn't master the texts and all their nuances, and his peers looked down on him for it.

However, these were the very conditions that led Leonardo to develop an experimental approach to his craft, which made him very unique.

Leonardo didn't have much respect for traditional education, and his independent thinking sparked important question that other artists weren't asking. Others were learning _how_ things worked, but Leonardo also wanted to know _why_ things worked.

To figure this out, Leonardo began recording his research and findings in his now famous notebooks.

It's in these notebooks that we see his interest in the microcosm, and how the human anatomy can be echoed in the cathedral architecture in Milan. On one page of drawings, he finds the symmetry between a human neck and skull and a structural column and its capital.

Leonardo's journals show him using classical sources as springboards to new ideas. And, when necessary, he questioned the ancient wisdom and used his own experience to guide his work.

Leonardo was especially skeptical of some of the rudimentary medical teachings of the time. So he went ahead and conducted his own dissections of animals and made extensive notes about decapitated frogs and various dismemberments and disembowelings in an effort to better understand life.

These journals show a man who is completely fascinated with human anatomy and all its connections to philosophy, engineering and religion.

### 6. Leonardo educated himself and visualized Vitruvius’s teachings in the iconic Vitruvian Man. 

Leonardo's journals clearly show a man who is on the path to creating something great. He could sense this as well, and he wasn't going to let his limited education keep him from becoming one of the great Renaissance men.

So, in the 1480s, Leonardo began an intensive self-education program. It involved a mountain of reading.

As we know from the previous blinks, Leonardo's poor Latin skills had been a barrier to many of Vitruvius's teachings. Up to this point, he'd picked up things second-hand. But now he was determined to learn the fundamentals of everything. And as he devoured books on anatomy, architecture, astrology, geography, geology, medicine and natural philosophy, it's likely that he also finally picked up Vitruvius's ideas directly.

Now the challenge Leonardo faced was how to create a visual representation of these fascinating ideas.

As a visual artist and thinker, Leonardo loved to work with visual analogies. He believed a good image could explain a concept better than words; that images have the power to deliver more information in a concise way.

However, during Leonardo's period of self-teaching, he was struggling with a particularly tough challenge: coming up with an image that did Vitruvius's ideas justice. And this is when he had his epiphany. He would make his own practical and comprehensive guide to the human body — something he could do in a single image.

Everything was leading up to this point. From ancient Rome to the Renaissance there had been an obsession with cosmic perfection and applying the idea of the microcosm to architecture and collecting knowledge into a corpus.

So, around 1490, Leonardo grabbed a sheet of paper, a piece slightly bigger than a page of printer paper, and drew the now famous _Vitruvian Man_.

This single image brought together Vitruvius's ideas and those of the microcosm, and it also captures Leonardo's own innovative genius, perfectly encapsulating all the aspirations of the Renaissance.

> _The text on the picture is in Leonardo's famous mirror-script, and begins with, "Vitruvius the architect, has it in his work on architecture that the measurements of man are arranged by nature…"_

### 7. Final summary 

The key message in this book:

**Leonardo's lifelong journey of discovery led him to the** ** _Vitruvian Man_** **. It symbolizes many things: the striving of man, as well as his accomplishments, and the connection between the divine and the earthly. It's not only an artistic achievement of the master Leonardo, but also of the ancient idea that the human body represents something greater than itself.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Tesla_** **by Margaret Cheney**

_Tesla_ (1981) offers an enlightening and intimate account of the life and accomplishments of one of history's most thought-provoking and innovative inventors. These blinks chart the career of Nikola Tesla from his early battles with Thomas Edison to the controversial debate over the invention of radio communication.
---

### Toby Lester

Toby Lester, a contributing editor at the _Atlantic_, is a former Peace Corps volunteer. His work has also been featured on NPR and _This American Life_. His other book, _The Fourth Part of the World_, was published in 2009.

