---
id: 56407ebe38663300076c0000
slug: daily-rituals-en
published_date: 2015-11-13T00:00:00.000+00:00
author: Mason Currey
title: Daily Rituals
subtitle: How Artists Work
main_color: DC5A5A
text_color: A84545
---

# Daily Rituals

_How Artists Work_

**Mason Currey**

_Daily Rituals_ (2013) is an entertaining and illuminating collection of the daily routines of great minds and artists. Including the work habits of people such as Jane Austen, Ludwig van Beethoven and Pablo Picasso, it offers insights into the best ways to maximize efficiency and prevent writer's block, as well as tips on how to get by in the creative world.

---
### 1. What’s in it for me? Access to the rituals that formed the world’s best artists. 

What do you do to get going? Do you go on a morning run? Eat the same breakfast every day? Maybe you always sit at the same table in the same cafe to do your work? Or maybe you ignore the day altogether, preferring to burn the midnight oil?

We all have our little habits that help us work more productively. And it was no different for history's greatest artists, musicians, writers and philosophers. They also had their little life hacks and eccentricities that helped make them geniuses.

These blinks unveil the weird and wonderful habits that drove creatives and thinkers such as Beethoven, Sartre and Marx.

In these blinks, you'll discover

  * how many beans Beethoven put in his morning cup of coffee;

  * which famous writer preferred to work facing a cow; and

  * why Benjamin Franklin liked to take a daily walk in his birthday suit.

**This is a Blinkist staff pick**

_"Just the morning routines alone would make these blinks worthwhile, but I especially love hearing about some of the eccentricities of my favorite artists — like Beethoven and his coffee beans."_

– Ben S, Audio Lead at Blinkist

### 2. Most artists do their creative work during a particular time. 

We're all familiar with the romantic image of the disheveled artist toiling through the night, typing or painting feverishly to finish their work. But this certainly isn't how all creatives function.

Poet W.H. Auden, for instance, doesn't fit the stereotype. He was particularly vigilant about his working hours, once declaring that "Only the Hitlers of the world work at night; no honest artist does." Though not all artists take such a radical stance, many certainly prefer daytime working hours.

A slew of creatives give compelling reasons for working at dawn or in the early-morning hours. Take author Toni Morrison, for example, who likes to rise at around 5 a.m. and witness the dawn of each new day. For her, it's important to wake before the light and observe the transition into day. She considers this a mystical moment that inspires her to write. 

Some people get such a boost from this morning energy that one morning simply isn't enough. Novelist Nicholson Baker, for example, reaps the benefit of two mornings in one day by waking up for a spell of writing at 4 a.m., then going back to sleep, and rising once again around 8:30 a.m. for his second morning.

But this penchant for early mornings is far from universal. There are many for whom nights are the optimal time for creative work. 

For instance, author Ann Beattie is an absolute night owl. A great condoner of the idea that we are all attuned to different clocks, her preferred working hours are from 12 a.m to 3 a.m. 

Gustave Flaubert is another example of someone who favored working at night. He had a routine that allowed him to fulfill his social and familial commitments during the day; this freed up the quiet hours of the night, when he found it easiest to write.

> _"For me, light is the signal in the transaction. It's not being in the light, it's being there before it arrives. It enables me, in some sense." - Toni Morrison_

### 3. Some creators have more time than others because of their life circumstances. 

Did you know that Richard Wright wrote the first draft of a 500-page novel in a mere five months? An impressive feat — but also one in large part made possible by the New York Writer's Program, which provided him with a full-time salary during this period. Most artists, both past and present, only ever dream of being so fortunate.

Great minds are often crippled by a lack of time and money. For example, Wolfgang Mozart had to put up with an arduous schedule in order to make ends meet; most of his time was dedicated either to teaching or to socializing with the many patrons who helped finance his music. He only had time to work on his compositions at night, and he would often keep at it until 1:00 a.m., a mere five hours before the beginning of his next hectic day.

Karl Marx is another example of someone who endured harsh circumstances. He spent most of his life in London as a political refugee, suffering recurrent bouts of poverty and disease before completing the first volume of _Das Kapital_, his masterwork.

Although no one consciously chooses difficult life circumstances, there are some artists who choose to have little free time or to keep their day jobs in order to pay the bills. Haruki Murakami, for example, ran a small jazz club in Tokyo for several years before his career gained momentum.

Indeed, sometimes the stability and routine of an office job is attractive to creators. For example, author Henry Green had plenty of family money, and could have devoted all of his time to writing, but he chose to spend his days working at the head office of the family manufacturing business.

### 4. Drugs and stimulants allow artists to work longer hours – or just to relax. 

Which words do you think pop up most frequently in the biographical writings of the world's greatest minds? Words like "inspiration," maybe, or "love"? Actually, one of the most recurrent words is "coffee." 

Thanks to its ability to kick the brain into action, coffee is widely and wildly popular, and creative minds in particular have such a penchant for java that they develop routines and ceremonies based on their daily consumption.

For example, Ludwig van Beethoven was very fussy about his morning brew; it was made with precisely 60 coffee beans, each of which the composer counted out by hand.

Another coffee enthusiast was Søren Kierkegaard, who had over fifty unique sets of cups and saucers for his evening coffee. He put his secretary in charge of selecting the appropriate cup and saucer for the day, a choice which he then had to justify, with good argument, to Kierkegaard.

For some artists and thinkers, though, coffee wasn't quite enough to maximize their creative output. 

W.H. Auden relied heavily on amphetamines to gear himself up in the morning for his strict regimen; at night, he used sedatives to ready himself for sleep. 

Jean-Paul Sartre also seemed to regard work as more important than health. Exhausted by a social life consisting of an abundance of food, alcohol, drugs and smoking, the philosopher used corydrane, a popular concoction of amphetamine and aspirin, in order to continue working. The recommended dose being between one and two tablets per day, Sartre usually downed about 20. 

But such historical creatives didn't only take drugs for work. The creative life often goes hand in hand with epicurean sensibilities. Possibly the most notorious example of hedonistic behavior is artist Francis Bacon, who was known for consuming inordinate amounts of alcohol and drugs, and usually being the last to leave a party.

### 5. Finding inspiring surroundings is important for creators. 

You probably think of artists as city dwellers working in the run-down studios of some bohemian neighborhood, taking breaks to meet in cliques at the local cafe. However, many artists and thinkers seek inspiration in seclusion and nature.

Take Carl Jung, for instance, who built himself a stone house near a remote village on the lake of Zurich. His amenities comprised a fireplace, some oil lamps and a nearby lake from which he fetched water. In this sanctuary, far from the frenzy of city life, he carved out the time to write and reflect.

Similarly, Haruki Murakami chose to move to a rural area to mitigate the detrimental effects the city was having on his health. The quiet surroundings enabled him to adhere to a rather idyllic daily routine: waking at 4 a.m., working for five or six hours, running or swimming in the afternoon, then listening to music and reading before falling asleep at 9 p.m.

For other creators, urban life, with the occasional indulgence, is necessary for their creativity.

For example, writer Patricia Highsmith preferred to work in her own bed, with cigarettes, coffee and a doughnut at arm's length. The goal was to avoid associations with discipline or conventional work by surrounding herself with a feeling of leisure. Later in life, she also kept a bottle of vodka near her pillow, taking a swig when she awoke and marking the bottle to track her consumption.

Another city lover, Erik Satie, would habitually stroll through the city every day in his velvet suit and bowler hat. He did this so consistently that one contemporary wondered whether the composer's musical ability of finding variation within repetition could be linked to his ritual of pacing the same city routes every day.

### 6. There is no wrong or right way; great minds develop their own creative process. 

Being creative can be a hard slog, especially if you get stuck and end up procrastinating or traipsing through the day without producing anything satisfying. But there is no cure-all remedy when it comes to creativity, and different working rhythms can produce equally effective results.

The composer Benjamin Britten, for instance, detested the idea of inspiration and believed in hard work alone. Some of his colleagues even complained that his life _was_ his work, as his creativity seemed to be so all encompassing.

In contrast, novelist Gertrude Stein would be satisfied with only 30 minutes of writing each day, preferably while looking at a cow. She once noted: "If you write a half hour a day, it makes a lot of writing year by year."

It's not only the amount of time dedicated to working but also the speed at which the work is completed that varies radically from artist to artist.

For example, Dmitri Shostakovich could conceptualize a complete composition in his mind before putting pen to paper, at which point he transcribed his conception with alarming speed.

The poet W.B. Yeats was the exact opposite, admitting to being a very slow writer when he said "I have never done more than five or six good lines in a day."

Creators often have their own tricks to maintain the creative flow once they're in it, however. When he had a clear idea of how a story was going to continue, Kingsley Amis would actually stop writing for the day. This made it easier to dive back in the next day.

The composer Morton Feldman also employed an interesting strategy that John Cage taught him. After writing a sequence of notes, he would pause and then re-write the same sequence. He found that duplicating his work in this way sparked inspiration for the following lines.

### 7. Connecting with the body is a popular way to free the mind. 

Sitting around while trying to create can drain you both mentally and physically. There are ways to pep yourself up, though, from traditional exercise and fresh air to some less orthodox practices, as you'll see below. 

Perhaps unsurprisingly, walking and other sports are popular among creative people. Kierkegaard, one such enthusiast, spent most of his afternoons walking through Copenhagen; in fact, it was while walking that he had his best ideas. When inspiration struck, he would hurry home and, standing at his desk, write down his idea, his hat still on his head.

Joan Miró also had a rigorous daily exercise regimen — boxing, jumping rope, gymnastics — that helped elevate his mood and keep his recurring depression at bay.

Connecting to the body through nudity has also proven beneficial for some great thinkers. Benjamin Franklin, for example, would get up every morning and spend thirty minutes or an hour naked in his room, taking an 'air bath' while he read or wrote. This is not unlike Woody Allen's preference for showering whenever he was feeling stuck with a project. As he put it, "It breaks up everything and relaxes me."

As well as nudity, sexuality seems to play an important part in the work of some creators. Author Thomas Wolfe, for instance, developed a writing ritual that involved touching his genitals. The aim was not to become aroused, but rather to connect to a 'good male feeling' — something he found inspiring.

Author John Cheever was also a proponent of the virtues of both the body and of sex, aiming for at least two or three orgasms a week. He believed it improved his concentration and eyesight!

### 8. Day-to-day relationships play an essential part in many artists’ lives. 

Some world-renowned thinkers and artists collaborated with other eminent creators; Jean-Paul Sartre and Simone de Beauvoir were one such duo. But as well as collaborating for work, many creators emphasize the importance of friends and partners, who often help with the more practical and mundane parts of life.

For example, Alice B. Toklas, Gertrude Stein's partner, managed all practical aspects of the household, who would wake up at 6 a.m. so that all would be prepared for Ms. Stein when she awoke four hours later. 

The composer Gustav Mahler's young wife Alma was similarly entrusted with the needs of her partner. After ensuring the silence of the children during the morning, she would accompany Mahler on his midday swim and walk. When he became inspired, she would avert her attention, so as not to distract him from his work. 

As well as having partners to help with everyday tasks, creators also often had valuable social interactions and friendships.

For example, Immanuel Kant would visit his best friend Joseph Green each day after his afternoon walk, and would stay for a few hours to talk with him. Kant was eager to socialize and enjoyed dining out as much as he loved hosting guests of his own. 

In a more extreme example, Karl Marx was not only the good friend and colleague of Friedrich Engels, but often relied on him for regular handouts, which Engels would send to keep the economist out of the gutter.

Some artists, however, were not so fond of company, preferring a more solitary life instead.

Patricia Highsmith was of this ilk. Once, she arrived at a cocktail party holding some lettuce and a bag of snails; these, she explained, were her companions for the evening.

### 9. Women and men often have different roles in a creative household. 

Whether true or not, it's long been thought that women are inherently adept multitaskers. It's certainly true that, historically, female artists have had to devise ways of navigating spousal and societal strictures if they hoped to meet with success.

Before women's liberation movements in the twentieth century, women who were able to dedicate their life to creative work were few and far between.

Jane Austen was one such rarity, however, going on to become one of the most successful writers of her time. Working in conditions markedly different from those of her male contemporaries, she usually wrote in the sitting room, which was open to the distractions and demands of the household. To add to this, her work had to be kept secret from servants and visitors, and she relied on a creaky door to warn her of unexpected company! Nevertheless, Austen was remarkably fortunate for her time, with her family supporting her and her sister taking charge of the majority of the household duties.

Alma Mahler wasn't quite so fortunate. A talented composer in her own right prior to her marriage to Gustav, she stopped her work after Gustav insisted there be no more than one composer in the family.

Thankfully, women's place in society has undergone some improvement over the years. But modern creative women nonetheless face their own set of challenges.

For example, writer Frances Trollope began writing when she was 53, to support her ailing husband and children. In order to find time for her writing while keeping the family afloat, she start work at 4 a.m. every morning before breakfast.

Author Toni Morrison, too, had to juggle work and home life. For most of her literary career, she worked a 9-to-5 job and raised her two sons single-handedly.

### 10. Final summary 

The key message in this book:

**By reviewing some of the routines of artists and thinkers both historical and contemporary, we learn that there are countless ways to find motivation and inspiration in the creative field.**

Actionable advice:

**Experiment with different rituals.**

Whether it is lounging around in bed or jogging before sunrise that gets your neurons firing and your creative juices flowing, find out what fits best for you. Remember that just because one habit worked for your favorite artist, it doesn't mean it will work for you!

**Suggested** **further** **reading:** ** _The Creative Habit_** **by Twyla Tharp**

In _The Creative Habit_ (2003) internationally acclaimed choreographer Twyla Tharp shares the approaches, habits and routines that keep her creativity honed and active. Drawing on her own experiences with creative projects and overcoming creative hurdles, she offers simple exercises and specific examples that can help you tap into your individual creativity and achieve your goals.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Mason Currey

Mason Currey is a magazine editor and freelance writer. His work has been published in _The New York Times_ and _Slate_.

