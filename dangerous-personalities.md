---
id: 59b6a7d5b238e100052dbc69
slug: dangerous-personalities-en
published_date: 2017-09-15T00:00:00.000+00:00
author: Joe Navarro
title: Dangerous Personalities
subtitle: An FBI Profiler Shows You How to Identify and Protect Yourself from Harmful Personalities
main_color: DA3740
text_color: A62A31
---

# Dangerous Personalities

_An FBI Profiler Shows You How to Identify and Protect Yourself from Harmful Personalities_

**Joe Navarro**

_Dangerous Personalities_ (2014) is a guide to the dark side of the human psyche. It offers a look inside the minds of some of the world's most dangerous people, exploring the kinds of personalities that have taken the most lives, as well as taken the biggest toll on society. Learn the traits of serial killers and those who wouldn't think twice about stealing your life savings. Who knows, you might be able to spot trouble before it has a chance to strike.

---
### 1. What’s in it for me? Learn the warning signs of a dangerous personality. 

Sometimes it's easy to recognize a potentially dangerous situation. When a wild-eyed man is stomping down the street, arguing loudly with himself, you know it's probably best to stay away and avoid eye contact.

Real danger is rarely so straightforward, but you don't need to be an FBI criminal profiler to pick up on the signs that you're in the company of a potentially harmful person. Every personality type has clear traits that go along with it, and many of these signifiers are easy to pick up on once you know what to look for.

So arm yourself with knowledge and reduce your chance of ending up in harm's way.

In these blinks, you'll learn

  * the personality traits of notorious criminals and serial killers;

  * why certain people are drawn to cults and hate groups; and

  * the important difference between "nice" and "good."

### 2. Narcissistic personalities care only for themselves, and they’ll do whatever it takes to feel superior. 

Do you remember the classic tale of Cinderella? Even if only foggily, you probably remember the evil stepsisters who forced poor Cinderella to do their bidding.

These stepsisters were memorable villains, but they're also a great representation of _narcissism_, our first dangerous personality disorder.

Narcissists generally believe they're above the law, exempt from society's rules, and they'll lie, cheat or even kill if it's in their best interest.

Outside of fairytales, we can see the real-world dangers of narcissism in the 2001 Enron scandal, still the biggest US corporate bankruptcy to date.

Behind this corporate crime was Jeff Skilling and Kenneth Lay, Enron's narcissistic CEOs, who were found guilty of conspiracy and fraud. Thinking only of themselves, these two continually lied to their 20,000 employees, encouraging them to invest any penny they had in Enron, resulting in thousands losing their life savings.

Records show this to be more than standard criminal behavior — these two believed they were _entitled_ to every ill-gotten cent. This is also a sign of _arrogance_, another narcissistic trait that often results in highly elaborate and grandiose schemes.

Society often benefits from big ideas, but when they come from a narcissistic mind, there's only one person who's benefiting. We can see this difference by contrasting Walt Disney with cult leader Jim Jones.

Disney envisioned Disneyland as a "magical" place for families to have an amazing time. Jim Jones took every cent from his followers to create Jonestown, an isolated camp in the wilds of South America where people had no choice but to obey his every command.

Disney wanted to spread the joy; Jones wanted it all for himself.

This also points to two other narcissistic traits: _superiority_ and _lack of empathy_, both exhibited by the notorious Bernie Madoff. This financier defrauded thousands by using money from new investors to pay off current clients who believed he was a sound businessman and a trustworthy friend.

But narcissists only see a friend as someone to be exploited for further gains.

### 3. Emotionally unstable personalities fear abandonment and thus seek attention and cause harm. 

When looking at dangerous personalities, we must examine those who fall under the _emotionally unstable_ category.

Emotionally unstable individuals are highly unpredictable — they're like an emotional rollercoaster: they might be riding an ecstatic high one minute and then spiraling down and feeling victimized the next.

They have a tremendous need to be loved but little ability to maintain a healthy relationship.

They tend to act like "wound collectors," storing up each and every occasion that they've felt mistreated. Whether a social slight was real or imagined matters not at all; they'll use each perceived affront as ammunition. The famous criminologist Leonard Territo sums it up well, describing unstable personalities as being "victims in search of an oppressor."

These personalities are commonly found in cults, since this environment usually offers unconditional acceptance, so their neediness can be indulged and given a structure that normal society can't provide.

The emotionally unstable person is known to use threats, including suicidal actions, as a way of manipulating others, especially when the person feels that they're at risk of being abandoned.

Just take the tragic case of famous _Saturday Night Live_ comedian Phil Hartman, who was married to an emotionally unstable woman. Fed up with his wife's drug use, Hartman threatened to leave her if she started using again. In response, she fatally shot Hartman before killing herself. Hartman's friends later said they had known for years that he was married to a woman with signs of emotional instability.

This personality type also seeks attention through impulsive and reckless behavior, which sometimes includes sexual exploits.

Many believe that Bonnie Parker, of the notorious bank-robbing duo Bonnie and Clyde, had an unstable personality. For starters, there's her impulsive decision to run away with Clyde Barrow, and then there's the couple's string of headline-grabbing bank robberies that resulted in the deaths of nine police officers.

Then there's the reality TV star and _Playboy_ centerfold, Anna Nicole Smith, who died after overdosing on prescription drugs in 2007. Sadly, she left behind a child, and her impulsive sexual history resulted in numerous men claiming to be the father.

> _All of Marilyn Monroe's biographers have written about her emotionally unstable personality._

### 4. Paranoid personalities can see anyone as a threat, which sometimes leads to hatred and violence. 

We all have a built-in warning system designed to alert us to potential threats. But a _paranoid personality_ is wired with a warning system that is constantly on overdrive, picking up threats from all sides.

There are a few ways to spot a paranoid personality. They often relentlessly monitor the words and actions of others, hoping to validate their irrational fears by finding signs of sinister or malicious intent.

This monitoring can extend to neighbors, coworkers, foreigners, different ethnic groups, the government, family members — virtually anyone.

One of the more famous paranoid personalities was President Richard Nixon. We can see it in the way he was constantly adding to his list of enemies and in his repeated claims that he was unable to confide in anyone.

When it comes to facts and history, paranoid personalities are very selective with what they decide to believe is true. They also have the ability to string together completely unconnected events and ideas to support their views and validate their actions.

Paranoia is what fueled Adolph Hitler's _Mein Kampf_, which exhaustively strung together 2,000 years' worth of grievances against Jews into a rambling diatribe.

Paul Jennings Hill had a similarly paranoid mind. Hill, a right-wing Christian extremist, believed killing a doctor who performed abortions was a justifiable way of protecting unborn children. So, in 1994, he walked into a Florida clinic and murdered Dr. John Britten along with Britten's bodyguard, James Barrett.

Paranoia can also convince people to join hate groups like the Ku Klux Klan and Aryan Nations.

This personality is naturally attracted to a group of people who don't need to be convinced that their fears and eccentric beliefs are sound. Plus, these groups feed their paranoia since they often involve getting prominently visible tattoos or wearing insignia and costumes that cause people to turn their heads and whisper to each other about their unusual appearance.

In the paranoid person's deluded mind, this is precisely the sort of behavior an enemy conspirator would engage in.

### 5. The predatory personality poses the biggest threat since it can kill without hesitation or remorse. 

Of all the dangerous psyches, the _predatory personality_ is the most troubling since they have no conscience when it comes to their actions.

They're cold, remorseless and completely indifferent to the harm they cause. If they want a car, they won't think twice about stealing one; if they want sex, it doesn't matter if they have to use violence to get it.

This is the kind of mind Josef Fritzl has. The Austrian predator kept his daughter imprisoned in a cellar for 24 years, raping her over three thousand times and siring seven of her children.

When asked about his atrocious behavior, Fritzl seemed to be fully aware of what he'd done and that he could have stopped at any time during those eight thousand nightmarish days. He told the psychiatrist, "I simply have an evil streak."

Naturally, this is the kind of mind that makes for serial killers, like Henry Lee Lucas, who summed up the unique mindset of the predatory personality by saying, "Killing someone is just like walking outdoors. If I wanted a victim, I'd just go out and get one."

Psychologists have noted that, even when recalling their horrific crimes, predatory personalities have a "flat affect." They are cold and detached in their speech and mannerisms. This emotionless state is how Dennis Rader responded when telling the police how he bound, tortured and killed ten people, a process that earned him the name, the BTK Killer.

Normally, people use words to communicate, but for predators, words are a tool to manipulate and coerce people into doing their bidding.

A famous example of this is Jack Henry Abbott, who wrote a celebrated book called _In the Belly of the Beast_ (1981) while doing time in jail for forgery and stabbing another inmate to death. One of the book's admirers was the famed author Norman Mailer, who thought it was a remarkably eloquent recounting of Abbott's time in jail.

Like Mailer, many thought Abbott's writing was a sign that he couldn't possibly be a coldhearted murderer, and Abbott took advantage of this to convince Mailer to plead on his behalf at a parole board hearing. It worked, and Abbott was released early.

But six weeks later, Abbott stabbed a man to death because he didn't like the way he was staring at him in a cafe.

### 6. People can have multiple dangerous personalities, and this can heighten the risk of their doing harm. 

One of the dangers of flying a plane is _target fixation_ — when the pilot is so focused on a task that she overlooks the fact that there's a huge mountain right in front of her. A similar danger exists in the process of assigning someone a dangerous personality. If you're too quick to isolate one category, you run the risk of missing additional clues.

There are many instances throughout history of people who display multiple dangerous personality types.

Joseph Stalin is a singular example of just how epic the influence of someone's dangerous personalities can be. Stalin had a totalitarian grip not just on the Soviet Union's citizens but also on its intelligence services and military.

A sign of Stalin's narcissistic desire for validation can be seen in the multiple honorary titles he gave himself, such as Father of Nations, Brilliant Genius of Humanity and The Coryphaeus of Science — which roughly translates to "the conductor in the field of science."

But Stalin also had his paranoid side, having nearly a quarter of his military staff executed following World War I, due to his suspicions about their loyalties. Ultimately, Stalin's dangerous combination of personalities resulted in approximately 30 million people being killed.

What gives multiple personality types the potential to be more dangerous is that one trait often has the tendency to heighten the others. For example, if someone is a paranoid narcissistic predator, their suspicious and self-obsessed nature can make them even more of a threat to those around them.

This is what was found when Warren Jeffs, a polygamist cult leader in Utah, was arrested in 2006. He'd turned his cult into a personal breeding ground, where he could have sex with underage girls at will, often with the help of the girls' complicit mothers.

Jeff's narcissism was exhibited in his beliefs that he was not subject to "earthly laws" and that God told him to commit the crimes. His predator side was evident in his lack of remorse, and his paranoia can be seen in his desire to keep all outsiders, or potential competitors, at bay.

### 7. Protect yourself from danger by being aware, strategic and understanding of people’s intent. 

All too often, people fail to realize how dangerous someone is until it's too late. However, there are actions you can take to stay safe.

For instance, you can reduce your chances of danger by keeping a sharp eye on the time and paying attention to your location.

The highest rates of violence occur between 8:00 p.m. and 2:00 a.m., so stopping to get gas at 11:00 a.m. is simply safer than stopping at 11:00 p.m. You can always be more cautious by planning ahead.

Predators seek to control your mind, body, money and feelings, but they need to be close to you in order to do so. So in situations like taking money out at the ATM, be aware of your surroundings and make sure no one is trying to get close to you.

Another thing to be aware of is that niceness and goodness aren't the same thing.

Dangerous personalities can act in ways that seem nice, but they can never be good, since goodness requires honorable intentions.

One of the more sinister examples is the serial killer and sexual predator John Wayne Gacy, who would dress up and perform as a clown for neighborhood kids. While investigators were exhuming twenty-six corpses from Gacy's Illinois house, his neighbor was busy explaining what a nice guy he was.

So make sure you determine someone's intentions before you let down your guard.

If you determine that a dangerous personality is posing a threat, you may find these strategies helpful:

Set strong boundaries. Once you've said "No," don't back down; they'll try to convince you otherwise, but remain firm and final. Make your boundaries firm. If you give them an inch, they'll take far more than just a mile.

Form supportive alliances. Don't be afraid or ashamed to tell a friend or local services if you're being tormented or mistreated by a dangerous personality. It's always helpful to have people who can check in with you and verify your experiences.

The French chemist Louis Pasteur said, "Chance favors the prepared mind." You can consider yourself more prepared and informed than you were ten minutes ago, so use this information to stay safe.

### 8. Final summary 

The key message in this book:

**There's no help center for those who suspect a dangerous personality. So just as you should look both ways before crossing the street, you should scrutinize people's personalities closely before getting close to them. Be aware of your surroundings and the situations you put yourself in. Knowing the signs that dangerous personalities give off can help you monitor your surroundings for any potential threats. If there is a risk, take the necessary steps to inform others and keep these potentially dangerous individuals from getting close.**

Actionable advice:

**Determine the threat.**

Here are some questions to ask yourself if you're not sure about someone's intentions. If you're answering a lot of these questions in the affirmative, there's a good chance you're dealing with a dangerous personality.

  * Do they negatively affect my emotions?

  * Do they behave erratically, unethically, antisocially and disregard the law?

  * Are they manipulative or exploitive?

  * Are they frequently behaving in a dangerous manner?

  * Are they often acting impulsively, or out of control, with a need for instant gratification?

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _What Every BODY is Saying_** **by Joe Navarro**

This book is all about the hidden meanings we can find in our body language. Our brains control our body movements without us being conscious of it, and sometimes those movements can be very revealing. This book goes into detail about the ways you can train yourself to become an expert in observing other people's nonverbal cues and uncovering their meaning.
---

### Joe Navarro

Joe Navarro has over 25 years experience as a criminal profiler for the Federal Bureau of Investigation. He is also the author of the best-selling book _What Every BODY is Saying: An Ex-FBI Agent's Guide to Speed-Reading People_.

