---
id: 58b559852626b200041744dc
slug: dangerously-sleepy-en
published_date: 2017-03-02T00:00:00.000+00:00
author: Alan Derickson
title: Dangerously Sleepy
subtitle: Overworked Americans and the Cult of Manly Wakefulness
main_color: 5491BB
text_color: 3D6987
---

# Dangerously Sleepy

_Overworked Americans and the Cult of Manly Wakefulness_

**Alan Derickson**

_Dangerously Sleepy_ (2014) shines a light on one of the oft-ignored yet highly relevant legacies of the industrial revolution: lack of sleep. The nineteenth century brought us many innovations, such as electricity, railroads and modern machinery, but it also led to exploited workers and the idea that sleep is for the weak — a luxury that Americans can't afford.

---
### 1. What’s in it for me? Debunk negative myths about sleep. 

"But first, coffee" — such slogans are common not only as cafe advertisements but as general mantras among working professionals and students. Many people fuel themselves with caffeine and other drugs to be hyper-productive while sacrificing sleep, which is seen as a waste of valuable time. Perhaps you remember your own college days, when students would compete about how many all-nighters they'd pulled while preparing for exams!

Indeed, sleep is often associated with weakness and decay. Why is that? Where did this idea come from? And what are the negative side effects of refusing to rest?

In these blinks, you'll learn

  * how Edison's invention of electricity is connected to his championing of less sleep;

  * why little sleep came to be associated with masculinity and achievement; and

  * about the dangerous risks of sleep deprivation.

### 2. Benjamin Franklin was one of the first Americans to theorize about sleep, but he left a poor legacy. 

Perhaps you've heard the saying, "I'll sleep when I'm dead." Such sayings are emblematic of a common American attitude — that sleep is for chumps, for the lazy masses who have nothing better to do. But when did this unhealthy attitude start?

To find that out, we'll have to rewind back to the eighteenth century, when one of the nation's founding fathers, Benjamin Franklin, gave the subject some serious consideration.

Before he was a politician, Benjamin Franklin was a prototypical polymath whose long list of professions included inventor, newspaper printer and author. One of his publications was the yearly _Poor Richard's Almanack_, which included poems, proverbs, astrology and useful advice for the early settlers of the 1730s.

One of his most famous sayings from the almanac, a saying still bandied about today, is "early to bed, early to rise, makes a man healthy, wealthy and wise." In 1735, when Franklin published this advice, he warned people that getting up late would only leave them scrambling and trying in vain to catch up on lost time.

Franklin's own sleep habits were relatively normal, and perhaps surprisingly economical. He would end every day at 10:00 p.m. and wake up every morning at 5:00 a.m.

But as time went on, Franklin's attitude began to change, and sleep became ever more undesirable.

During the 1740s, in other writings under his "Poor Richard" pen name, Franklin took on a more judgmental view toward sleep. His maxims now implored people to get up, to stop wasting their lives; he even started telling people that there would be time enough for sleep once they were six feet under.

Franklin's anti-sleep attitude may well have been spurred to life by John Calvin. Franklin deeply admired the Protestant reformer, a man known for his aversion to sleep. When Calvin died at the relatively young age of 55, Franklin noted in his eulogy that he'd actually lived much longer than most, since he'd wasted so little time on sleep and sloth.

> _"Sleep is an absurdity, a bad habit."_ \- Thomas Edison

### 3. Thomas Edison was a tireless and early promoter of America’s culture of sleeplessness. 

When you think of Thomas Edison's contributions to society, you probably think of his inventions, like the light bulb or the phonograph. You perhaps even admire his impressive productivity.

But what many people don't think about are the long-lasting repercussions of the tireless industriousness that enabled Edison to be so prolific.

Thomas Edison was certainly a major role model for early Americans, and his example, perhaps more than anything else, advanced a culture of sleeplessness in the young nation.

First of all, by introducing the light bulb, Edison made the idea of working through the night a truly viable option. And as the founder of General Electric, Edison had a million reasons for wanting his workers to believe in the competitive advantages of foregoing sleep.

Edison was so committed to promoting the merits of going without sleep that he conspired with journalists and spoke to the public at exhibitions. The message was clear: Sleep is a waste of time!

Sleeplessness also influenced Edison's hiring practices. He only chose employees who showed a willingness to stay up and work for long stretches at a time.

Newspapers, fascinated with Edison's success story, were more than willing to spread his message. In a 1878 edition of the _Chicago Tribune_, an admiring profile detailed Edison's tireless work ethic; he stayed awake, day and night, only resting for two or three hours to rejuvenate.

It wasn't hard for people to see how Edison's successful inventions were linked to his policy of sleeplessness. In 1879, Edison improved upon his light bulb by creating a more durable and longer-lasting filament, an invention that surely helped Edison and his team work through many sleepless nights to develop his plan of distributing electricity throughout the United States.

His reputation was further cemented in 1885 when the poet and activist Sarah Bolton published _How Success is Won_. In it, Bolton recounted a particularly impressive stretch when Edison worked for 60 hours straight, and she estimated that his average workday around that time lasted 18 hours.

### 4. With his sleepless flight, Charles Lindbergh became the embodiment of virility in the 1920s. 

In the twentieth century, a new breed of sleepless masculinity emerged — one that both grabbed the public's attention and embodied their adventurous spirit.

In 1927, Charles Lindbergh became an American idol and a paragon of manliness when he announced that he would break records by piloting a plane over the Atlantic Ocean.

If successful, Lindbergh would fly his plane 3,600 miles, from New York to Paris, and shatter records for the longest flight ever attempted.

But in order for his plan to succeed, Lindbergh had to make his fifty-foot plane as light as possible. This meant flying alone and without any communication equipment on board.

Naturally, Lindbergh's unprecedented feat captured the public imagination. He was dubbed "The Lone Eagle" and newspapers were filled with eagerly awaited reports of his every move.

One of the most fascinating aspects of Lindbergh's flight was his need to remain awake. It would last a total of thirty-three and a half hours, and Lindbergh would be risking death if he nodded off for even a moment.

Lindbergh's plane began its historic journey on May 20, 1927, and the press immediately escalated the suspense. In the _New York Time_ s, other long-distance pilots were insisting that the most crucial factor wasn't the weather or the plane's gadgets but Lindbergh's ability to stay awake.

The press further stoked the excitement by reporting that Lindbergh had only managed to sleep for an hour or two on the night before his take off.

But Lindbergh landed quite safely in Paris on the evening of May 21st. And when the press ran to get his statements, he seemed to want to maintain his public image of rugged virility. So, even though he appeared rather groggy after landing his plane, he told the _Chicago Tribune_ that it was no problem at all to stay awake during the flight.

Lindbergh is still celebrated for his sleeplessness. Few future shunners of sleep would enjoy such accolades.

### 5. Charismatic and successful businessmen furthered the fight against sleep in the 1980s. 

In the 1980s, there was a new global economy that required people to work longer hours than ever. Since money never sleeps, more people were adopting the same attitude, despite the fact that there was also a booming fitness craze.

Remarkably, psychologists and scientists were also claiming that there were health benefits to sleeping less.

Journalists were seemingly happy to perpetuate the myth of sleepless success by seeking out scientific data that supported this narrative.

There were entire books devoted to the subject. Everett Mattlin's 1979 book, _Sleep Less, Live More_, was filled with research that argued against the need for eight hours of sleep per night.

In 1981, psychologist Ernest Hartmann released his own findings, which suggested that those who sleep six hours or less are more energetic, capable, successful and happy. He even encouraged people to train themselves to sleep less in order to reap these benefits, just as dieters need to change their lifestyle if they hope to lose weight.

The 1980s were also filled with charismatic business leaders that further promoted the sleepless American lifestyle.

One of the best examples is Walmart CEO, Samuel Walton. He was famous for his weekly 7:00 a.m. Saturday staff meetings, which he would get up at 2:00 a.m. to prepare for by meticulously reviewing the sales figures of all his stores.

Stories about Walton's methods took on mythical proportions in Tom Peters's book, _In Search of Excellence_, which highlights Walton's eccentric, late-night motivational tactics, like showing up at Walmart's distribution centers at 2:30 a.m. to surprise employees with a bag of donuts.

A normal business day for Sam Walton began at 6:00 a.m. and didn't end until the clock struck midnight. And given the obvious success of Walmart, business executives across the United States were eager to follow his lead and put in 90-hour workweeks.

But one 1980s businessman embodied this ethos better, perhaps, than all the rest...

### 6. Donald Trump and American sports culture continued to link sleeplessness with success. 

Thanks to the tumultuous presidential elections of 2016, Donald Trump has come to represent a lot of different things to a lot of different people. But there's one thing that has remained consistent since his days as a real-estate mogul in the 1980s: he doesn't care for sleep.

Over the past 20 years, Trump has been an icon for hypermasculine business executives, and he's made his distaste for sleep a part of that image.

Since his first high-profile real estate deals in the 1980s, Trump has been very active in crafting and promoting his own public persona.

In his first best-selling book, _Trump: The Art of the Deal_, he laid out the central ingredients to his recipe for success, and one of them is to work extremely hard and sleep as little as possible _._ He emphasizes the importance of getting a leg up on competitors by waking up earlier than them and taking advantage of a business environment that never sleeps.

In his 2004 book, _Trump: Think Like a Billionaire_, he told readers that his own sleeping schedule had shrunk to four hours, between 1:00 a.m. and 5:00 a.m., and encouraged them to sleep as little as possible.

This kind of mentality can also be seen in the world of sports.

In the National Football League, players, coaches and managers can all be held up as examples of great, masculine leaders.

Some of the most successful managers were famous for their sleepless management style. For 60 years, George Halas worked 16-hour days while managing the Chicago Bears until his death in 1983.

Halas's former assistant, George Allen, adopted this strenuous work ethic when he became the manager of the Washington Redskins. Allen only slept a few hours every night and he used his office as a bedroom so that he wouldn't have to waste time commuting.

At this point, you might be wondering: Was there anyone supporting regulated working hours? Let's find out in the next blink.

### 7. Despite multiple Supreme Court cases, there still aren’t many regulations for safe work schedules. 

Even if your workday is a typical nine-to-five event, it can still lead to feelings of stress, burnout and being overworked. But look on the bright side! At least you aren't working in the early 1900s.

Despite horrific working conditions throughout the nineteenth century, there were still no regulations regarding working hours when the United States entered the twentieth century.

Those hoping for improvements were paying close attention to the Supreme Court case of _Lochner v. New York._

The case focused on the poor working conditions of bakers, the majority of whom worked in cold, cramped cellars with little to no daylight. Their health was suffering as a consequence. At the center of the lawsuit was a bill that New York had passed that limited the working schedule of bakers to a maximum of ten hours per day and 60 hours per week.

But after a routine inspection, bakery employer John Lochner was indicted by the state for violating these new rules and he appealed his case all the way to the US Supreme Court.

Unfortunately, those hoping for federal intervention were let down when the Supreme Court ruled in favor of Lochner, calling it a private matter between the baker and his employees.

Regulations eventually did start to appear, when a similar case, this time involving women in the state of Oregon, appeared before the Supreme Court in 1908.

Oregon had passed legislation to limit the working hours of female employees in commercial laundry houses.

Future Supreme Court justice, Louis Brandeis, represented the state when it appeared before the court, and he delivered a passionate speech; he argued that by working over ten hours per day, women were risking their ability to bear children.

It might seem like an unusual argument today, but it worked and successfully convinced the Supreme Court to support the state's regulations.

However, the lack of overall federal regulation would go on to promote dangerous conditions for generations to come.

### 8. Only healthcare workers have received regulations to prevent overworking. 

When you think about scandalous working conditions, you might think they're limited to coal miners and factory workers, but this problem continues to be a very immediate and widespread threat.

Hospital accidents are a prime example of how exhausted workers suffering from sleep deprivation continue to be a problem.

It took an unfortunate and deadly case for regulations to finally be established in the healthcare field: In March of 1984, Libby Zion was admitted to the Manhattan Hospital in New York. The 18-year-old was diagnosed with flu-like symptoms, but otherwise she was a healthy young woman.

So it came as a shock when, just a few hours later, Libby died.

Reports were unclear, but it seems likely that she was mistakenly prescribed tranquilizers that reacted fatally with the anti-depressants she was on.

What was clear was that the hospital's staff, being overworked to be the point of incompetence, played a significant role in her death. The intern placed in charge of her care had been working 100-hour weeks, including three night-shifts per week.

While Libby's health was rapidly deteriorating during her brief stay, this intern was busy having to deal with other clients and didn't check in on her. She only managed to ask nurses to restrain her and increase the dosage of the tranquilizer that likely killed her.

If there's a silver lining here, it's that Libby Zion's father sued the hospital, and a grand jury decided action needed to be taken. They didn't rule against the doctors for negligence; instead, they recommended that the working hours for physicians be regulated.

And a year later, that's just what happened. Physician Bertrand Bell headed a committee that decided medical professionals would work no more than 80 hours a week, and no longer than 24 hours at one time. And every week, they must have an uninterrupted 24 hour break in order to recuperate.

This is a good sign, but, sadly, such a commitment to safety is still the exception rather than the rule.

### 9. Final summary 

The key message in this book:

**A lot of businessmen, scientists and other hard-working professionals believe that not sleeping is a manly achievement to be celebrated, and one of the keys to success in the business world. But, in fact, it's an unfortunate legacy of the industrial revolution, and it's resulted in Americans being overworked in an unhealthy and potentially deadly fashion.**

Actionable advice:

**Make yourself a sleeping plan.**

Sleeping less than six hours a night will have adverse effects on your work. So carve out enough time for a good night's rest during your busy schedule. You'll likely find that your work efficiency actually improves when you get a solid amount of rest.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Sleep Revolution_** **by Arianna Huffington**

These blinks are about the importance of a basic human necessity that we often brush aside: sleep. Getting enough sleep isn't just about feeling better in the morning — it improves your work performance, health and even your personal relationships. Similarly, sleep deprivation isn't a by-product of hard work; rather, it _prevents_ you from reaching your full potential. _The Sleep Revolution_ (2016) explains why sleep is so critical, and what you can do to get more of it.
---

### Alan Derickson

Alan Derickson, a historian and professor at Pennsylvania State University, specializes in the history of labor and the working class. His other books include _Black Lung_, _Health Security for All, Worker's Health_ and _Worker's Democracy_.

