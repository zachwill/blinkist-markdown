---
id: 56b8e166d119f9000700007b
slug: daring-to-trust-en
published_date: 2016-02-10T00:00:00.000+00:00
author: David Richo
title: Daring to Trust
subtitle: Opening Ourselves to Real Love and Intimacy
main_color: ABA567
text_color: 5E5B39
---

# Daring to Trust

_Opening Ourselves to Real Love and Intimacy_

**David Richo**

_Daring to Trust_ (2010) offers a clear path to finding the confidence to trust yourself and the world around you. You'll learn to overcome past trauma and accept life for what it has to offer, discovering how to live in the moment and work toward a better future.

---
### 1. What’s in it for me? Rediscover your ability to trust yourself and the world around you. 

Have you ever been told that you're too trusting? Do you fear that your character allows people to potentially take advantage of you? Or are you the opposite, trusting no one, not even yourself?

For many people, trust is a tough feeling to master. A lack of trust, however, can fill your life with nothing but fear, undermine relationships and keep you from realizing your true potential. 

If you need a confidence boost and are seeking ways to become more trusting, these blinks will show you where to start. You'll learn proven methods to open up to other people and as a result, feel much safer in the world at large. 

In these blinks, you'll also discover

  * why overreactions are often not wrong, just misdirected; 

  * how a distanced partner may be desiring more intimacy, not less; and 

  * why listening to your breathing can actually boost your self-confidence.

### 2. Early childhood relationships shape your ability to trust yourself and others. 

How difficult is it for you to speak about your feelings with a stranger? Do you feel the world is generally a safe place? Your answers will depend mostly on your childhood experiences. 

Children who develop strong bonds with parents end up feeling safer in the world and learn to trust people more easily. When a mother comforts a frightened child, the child learns to trust the mother, grows more attached to her and feels more comfortable in the environment at large.

Children who never experience this sort of familial bond typically grow up struggling with adult relationships. They have a hard time opening up to others, and fear they'll be emotionally hurt if they try.

Our bodies remember the peaceful, safe moments we spend in a parent's arms all our lives. These memories comfort us during difficult times, and remind us that the world is a safe place. 

When the author was young, his mother, grandmother and great-aunt gave him a bath every Saturday night. Such warm memories still help him cope with difficult situations, even today. 

Children need to be cared for, but sometimes _too much_ care can also cause problems. If a parent is over-possessive, a child can't develop needed independence. 

That's why it's important that parents gradually relinquish control as children grow older. Young people need space to explore the world, testing boundaries, while parents are still around to offer support if things go wrong. 

Such freedom also allows children to develop trust in themselves — a key part of forming and maintaining healthy relationships as adults.

> _"We move from trusting in the trustworthiness of someone to trusting ourselves, and then to trusting others."_

### 3. Healthy relationships are built on trust; remember your fundamental needs to cement that trust. 

Movies often depict an idealized version of romance, with new lovers euphorically rushing into a relationship. Yet "real" relationships often play out quite differently. 

Wise adults spend the first part of a relationship assessing a partner's trustworthiness. Trust is the foundation for a stable, healthy relationship, as it allows both partners to feel safe and truly be intimate with each other.

A trustworthy person is committed to a partner's well-being, and puts in an effort to keep the relationship alive. Your happiness shouldn't depend entirely on your partner's trustworthiness, however. The first teaching of Buddhism tells us to accept that the world is constantly in flux, and nothing is completely reliable. Accepting this is a key part of being happy — so accept that your partner can, and will, change.

A distrustful partner can't fulfill your needs; and a partner who doesn't fulfill your needs can't be trusted. Remember what your _fundamental needs_ are (the Five A's): a partner who pays _attention_ to you, _appreciates_ you, _accepts_ you, shows _affection_ and _allows_ you the freedom to be yourself. 

A partner who can't trust himself or others may be so consumed by own inner conflicts that he does the opposite of fulfilling his partner's needs. He might try to control the partner's every move if he expects cheating, for instance. This would be a violation of _allowing_ freedom in a relationship. 

A partner who doesn't trust _himself_ might also think that he's unable to be happy alone and rely on a partner too much. He'll expect more _affection_ than it's possible to give, and might keep a partner at a distance out of spite, telling himself things like, "I hate clingy people anyway!" when he doesn't get the attention he thinks he needs.

### 4. Fear and past trauma can negatively affect present relationships, even if you don’t realize it. 

People often overreact when they observe certain behaviors in a partner. Minor events, such as a partner in a bad mood, might trigger a disproportionate negative reaction on behalf of the other partner. Yet what might cause someone to overreact like this?

It's difficult to trust people if you've been hurt in the past. Most people are sensitive about being rejected or deceived by a person they care about. They'll be deeply hurt when their trust is violated, and have a hard time trusting others in the future. 

So if you find yourself struggling to trust a certain person — say, a new girlfriend or a new colleague — that mistrust doesn't necessarily come from them. It could come from you. Say you're overcome with fear every time your new girlfriend gets angry. That might stem from your own childhood traumas, like the fear of a parent who became violent when angry. 

Aside from trust issues, there's another kind of pain to which people are highly susceptible, and that's _do-it-yourself pain_. 

Do-it-yourself pain occurs when you're unrealistically negative about yourself. Imagine a person after a breakup who tells herself that no one will ever love her again, for example. This is a classic example of do-it-yourself pain. Sure, it's hard to be dumped, but don't make the situation worse by telling yourself such falsehoods.

Fear and past trauma can also induce you to lie to a partner or remain in an unhealthy relationship. We often lie out of fear. A person might be afraid to admit her feelings if those feelings could be then hurtful to others.

So if a former drug addict relapses, for example, the addict might lie to a partner about the relapse as he's afraid of the partner's disappointment or possible rejection of him for his behavior. 

Contrary to reason, a person also might choose to stay in an unhealthy or abusive relationship. If that person was raised in an abusive environment, for example, pain or abuse may seem normal — and she might actually feel uncomfortable _without_ such negative feelings in her life.

> _"When we lie to others, we are cheating ourselves...That causes a dangerous erosion of our self-esteem."_

### 5. Accepting yourself for who you truly are is an important step on the road to tranquility. 

Would you trust someone who deceives you about who they really are? Of course not. It's important to be honest about yourself, which also means you have to be honest _with_ yourself.

Start out by acknowledging and accepting your own feelings — you can't know who you are until you know how you _feel_. 

Suppressing or denying emotions might make things easier in the short-run, but it only causes pain in the end. When you suppress your feelings, you impede your own ability to know yourself or what you want in any given situation. 

You'll only feel secure when you stop lying to yourself and accept your feelings for what they really are. And once you acknowledge what you truly want out of life, you can go after it!

Accepting emotions instead of fighting them also gives you a sense of tranquility. Your negative emotions manifest in other ways when you suppress them — ignoring them doesn't set you free at all. Suppressed sadness could manifest as a nagging sense of tension, for example. When you allow your sadness to flow freely from your subconscious, the tension will resolve itself.

Knowing yourself isn't just about understanding your emotions — it means knowing your body, too. Tuning into your body can help you handle difficult or stressful situations.

Your body offers a physical reflection of your anxiety when you're stressed. Your palms might sweat, your breathing might become more shallow, your heart rate might increase; you might even start trembling from head to toe. 

If you focus on what your body is saying, you can learn to systematically calm your mind and body during stressful times. Remember to focus on your breathing, as breathing exercises help you center yourself and focus on what really matters at the moment.

> _"The more we trust our awareness, the more do we trust our inner voice...as a source of safety and security."_

### 6. Trusting in reality helps you to grow spiritually and discover your higher Self. 

Fear of emotional pain is a natural part of being human, but you can lessen this fear by reminding yourself that truly, you can cope with anything that happens to you. 

At the end of the day, every experience — good or bad — is an opportunity for personal growth. 

This belief is called _core trust,_ because it comes from within you. Core trust enables you to see that every experience is a chance to practice mindfulness. Strive to love and accept life rather than fight against it — doing so is a key part of your mental well-being. 

Core trust won't completely shield you from pain, but it's a major step toward true enlightenment. So let go of control and learn to accept whatever comes your way!

Sometimes you might find an inner source of love, comfort or wisdom during frightening situations. It might feel as if a "higher" power is taking care of you or watching over you, but this power really comes from within you. In a sense, the concept of "God" is something that exists in each person. You too can learn how to get in touch with this inner power. 

In such moments when you find yourself in touch with the "God" power, you experience _loving-kindness_ — an unconditional form of love that spreads from yourself to everyone around you. It's a source of wisdom that starts from within you, but then transcends you as an individual.

The more you practice forgiveness and loving-kindness, the more you get in touch with your higher _Self._ What does this particular term mean, exactly?

The renowned psychoanalyst Carl Jung used the capitalized word _Self_ to refer to the powerful, immortal inner force that connects everything in the universe. Many people understand it as "God." This concept differs from the _self_ (with a lowercase "s"), which is based on the individual, mortal ego. 

Only the ego is susceptible to fear. You'll become stronger and more at peace with the universe the more you get in touch with your _Self_.

> _"The opposite of being in control is resting secure and being alert to what comes next."_

### 7. Practicing mindfulness and focusing on positivity helps you overcome any hardship. 

Connecting with the Self empowers you and helps protect you from your own inner demons. That's why it's so important to practice mindfulness during highly stressful situations.

Practicing mindfulness allows you to focus on the present moment and maintain self-control. When you lose self-control, you're essentially losing touch with your own emotions. 

Learn to reconnect with yourself by stopping what you're doing. Spend a few minutes focusing only on your breathing.

When you are sad, for instance, you're really struggling with a loss you can't make peace with. You can overcome this by using your inner strength to build more trust: pay attention to your thoughts and emotions as they come and go. Don't judge them or grow too attached to any of them — such thoughts are fleeting, after all. 

It's also a good idea to keep reinforcing positive thoughts and ideas. Positive affirmations lessen your pain and increase your self-confidence!

The perspective you take on any given situation has a big impact on your emotional reaction to it, so a positive outlook is key. When you're confronted with something difficult, remind yourself that you're probably making it out to be worse than it actually is. Try to focus on positive thoughts, instead of dwelling on problems and negativity. 

Let's say you're afraid of checking your email because you've been getting a lot of bad news, for instance. In such situations, it's tempting to tell yourself, "The world is against me. I can't handle this and I don't want to know what comes next."

But you'll be much happier if you instead tell yourself, "Everyone goes through difficult times, but I've overcome hardships in the past, so I can overcome this, too." 

You have the strength to pull through! Don't ever forget this.

> _"We can rebuild trust in each other by our three-step practice: breathe, pause, start over calmly."_

### 8. Final summary 

The key message in this book:

**Happiness, confidence and positivity might seem like elusive concepts but they really come from within you! So accept yourself for who you** ** _really_** **are, positive and negative attributes included. Accepting yourself is a fundamental part of learning to live life as it should be lived, building healthy relationships and moving toward a future that's better for you and everyone you love.**

Actionable advice:

**Let it go.**

Don't dwell on negative emotions — they'll overtake your life if you do. Accept your pain instead. Accepting pain for what it is allows you to eventually overcome it and grow as a person. 

**Suggested** **further** **reading:** ** _The Trust Edge_** **by David Horsager**

_The Trust Edge_ explains why trust is not only essential for a happy personal life, but the fundamental factor in successful businesses and business relationships. It outlines and discusses eight so-called pillars of trust and argues that these principles should be at the basis of your daily actions, in order for you to gain what the author terms the "trust edge." The book also discusses how to rebuild trust once it's been damaged.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### David Richo

David Richo is a psychotherapist who combines Jungian and Buddhist perspectives in his work. He's written over 15 books on psychology and spiritual growth.

