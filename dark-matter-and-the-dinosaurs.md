---
id: 576906d1ae95d80003cf773b
slug: dark-matter-and-the-dinosaurs-en
published_date: 2016-06-22T00:00:00.000+00:00
author: Lisa Randall
title: Dark Matter and the Dinosaurs
subtitle: The Astounding Interconnectedness of the Universe
main_color: 2BD99E
text_color: 177354
---

# Dark Matter and the Dinosaurs

_The Astounding Interconnectedness of the Universe_

**Lisa Randall**

_Dark Matter and the Dinosaurs_ (2015) takes a close look at the remarkable interconnections between Earth and the universe around it. These blinks explain how dark matter, the invisible stuff that makes up most of the universe, relates to the mass extinctions of the past and to the comets that might one day bring about another.

---
### 1. What’s in it for me? Learn what dark matter is and how it affects our solar system. 

Forever seen as the magnificent giants that once roamed Earth, dinosaurs seem to keep our imagination and fascination in a firm grip. Over the past few decades, evidence has piled up to reveal that a giant meteoroid caused the great dinosaur extinction — but more details are now emerging that might shed light on why events of mass extinction such as this can be found throughout our planet's history.

One force that may have contributed to these fateful meteoroids is _dark matter._ This strange substance is believed to make up the majority of the matter in the universe. But only now are we beginning to uncover its role in shaping the universe, as well as our own solar system and planet.

Take a closer look at what dark matter actually is, before exploring how the universe was created, what parts meteors have played and what, if anything, dark matter had to do with it.

In these blinks, you'll discover

  * the forces created by dark matter;

  * how meteors could have played a key role in creating life on Earth; and

  * what predictions we can make about huge meteors in the future.

### 2. We might not be able to see dark matter, but that doesn’t mean we can’t understand it. 

Has anyone ever told you, "I'll believe it when I see it?"

It's fine to want tangible evidence of something. But this leaves us on shaky ground when it comes to _dark matter_ ; even though it's all around you, you can't see it.

In fact, at this very moment, billions of particles of dark matter are moving through you. You can't perceive them visually because dark matter doesn't interact with light. Humans also can't touch or sense dark matter in other ways because it doesn't have electromagnetic interactions — at least not that science has been able to discover so far.

So, while it's uncertain what type of particles compose dark matter, it's clear that they're not the typical atoms or elemental particles that humans are familiar with and which we can see.

In this way, dark matter is a bit like the microscopic world of bacteria that live all around us. We can't see bacteria, but they're essential to our healthy functioning. What's more astounding is that this invisible dark matter makes up 85 percent of all the matter in the universe!

How do we know?

Well, dark matter can be detected through its interactions with gravity.

Everything in the universe is in constant motion, and the rate at which objects like planets and stars move depends in large part on the gravitational pull of massive objects like the sun.

In the 1930s, scientist Fritz Zwicky was following the velocity of stars and galaxies when he noticed that their visible mass wasn't enough to account for the gravitational pull being exerted. This led him to the conclusion that there must be matter that we can't see — he called it _dunkle Materie_, or "dark matter."

### 3. Dark matter played a significant role in shaping our universe. 

So, Zwicky was the first to theorize about dark matter; but since then, a lot more research has been invested in figuring out what exactly it is. Some of the strongest data has been drawn from the _cosmic microwave background_, ancient radiation that originated during the formation of the universe.

By analyzing this information, scientists can figure out the amount of radiation, matter and energy that were present a mere 380,000 years after the big bang, when the universe was in its infancy. In fact, this data lines up with the research conducted by Zwicky and other scientists, in that it confirms a significant quantity of dark matter, containing five times the energy of all visible matter.

Furthermore, the data proved that dark matter is essential to creating the gravitational force necessary to structure a galaxy. Since dark matter doesn't interact with radiation or gases, it could clump together and form a gravitational force while everything else in the universe was expanding in the wake of the big bang.

In other words, dark matter laid the foundation for the structure that our galaxy and universe hold to this day. For instance, our solar system was formed 4.56 billion years ago when an area of dense gas collapsed, thereby forming the Sun. Then, with the aid of dark matter, material that had been spread all over the galaxy began to form a disc around the Sun, making planets.

But they weren't all created equally. In fact, the planets closest to the Sun, like Mercury, Venus, Earth and Mars, are composed of materials that will not readily burn up, like iron and aluminum. Meanwhile, planets further away, like Jupiter and Saturn, are larger because they could collect all the other material — much of which was gaseous — that would have otherwise gone up in flames.

As a result, if you leave the Sun out of the picture, Jupiter, Saturn, Uranus and Neptune make up 99 percent of the mass in our solar system.

> _The Sun circles around the center of the Milky Way at 220 km/sec, taking 240 million years to orbit the center of the galaxy._

### 4. Meteoroids have been hitting Earth for billions of years and might have played an important role in the formation of life. 

You now know how dark matter was essential to the formation of the Sun and other planets. But the solar system contains so much more. In fact, craters on Earth's neighbors, the Moon and Mercury, show that when the solar system was still settling into place, Earth was regularly being struck by _meteoroids_.

What are those?

Essentially, a meteoroid is any extraterrestrial material that enters our atmosphere, whether it's an asteroid, meteor or smaller object. So, while the solar system was coming together, billions of asteroids formed and settled between the gravitational pull of Mars and Jupiter, as well as the far reaches of the solar system.

The majority of this meteoritic activity on Earth falls under two categories: the _Early Bombardment period_, which happened about 3.8 billion years ago and the _Late Heavy Bombardment period_, which occurred 500 million years ago.

But how did these periods of meteoritic activity affect Earth?

Well, many of the valuable minerals that we mine from Earth originated in these meteoroids. As Earth was first forming, heavy elements like iron and nickel were pushed to the planet's core. So, the materials we mine today were deposited by meteorites composed of these materials as well as carbon, frozen gases, water and even amino acids, the building blocks of protein and DNA that enable our planet's amazing complexity of life.

Scientists have even theorized that the amino acids from meteoroids might have played a major role in developing life on Earth. Life began to emerge on our planet following the Early Bombardment period, 3.8 billion years ago.

In fact, as scientists have examined fossilized records around the world, they have discovered evidence of a surge in complex life right after the impact of meteoroids. For instance, in China's Yangtze Gorge, trilobite fossils were discovered right above the chemical deposits that form during a meteoroid impact. This suggests that such meteoroids played a central role in the development of the now fossilized organisms.

### 5. Unlike the meteoroids that enter Earth’s atmosphere every day, comets are unique. 

Throughout its long history, Earth has experienced intense periods of meteoroid bombardment. But unlike these past events, most space debris that enters the planet's atmosphere today doesn't make much of an impact. In fact, millions of tiny meteoroids, adding up to 50 tons, burn up in Earth's atmosphere on a daily basis.

So, on a clear night, away from the light pollution of a big city, it's not hard to spot a streak racing across the sky, evidence of a meteoroid burning up. But these astrological events shouldn't be confused with comets.

Comets have long, brightly burning tails. They originate far beyond Jupiter, in the icy depths of the universe, and the closer they get to the Sun, the more their frozen contents, like helium and ethanol, begin to burn, forming a brilliant light display in their trail.

But even when it comes to comets, there are different categories, which, depending on their origin, are called either _short-period_ or _long_ - _period_ comets. The former come from the _Kuiper Belt_, which starts just after Neptune and is home to tiny "dwarf planets", like Pluto, as well as thousands of other unidentified objects that scientists are slowly discovering.

And even further into the universe is the _Oort cloud_, named after the Dutch astronomer Jan Oort and the place where long-period comets originate. The Oort cloud is on the very edge of the solar system, in a place where the Sun's gravitational force is weakest. As a result, it just takes a small push for something to launch out of the Oort cloud as a comet.

There might be trillions of objects in the Oort cloud, including some big enough to be considered minor planets, or the type of major comet that could cause a mass extinction on Earth. However, the precise details are few and far between as humans continue trying to explore this distant region.

### 6. Devastating meteoroids don’t strike Earth often – but recent ones have revealed their destructive potential. 

So, the role that meteoroids played in the development of life on Earth is based largely on speculation. But the _destructive_ power of a meteoroid striking our planet is much better understood. For a perfect example of this, you need look no further than a single event in Tunguska, Russia, in 1908.

Back then, a 50-meter-wide meteoroid exploded in Earth's atmosphere over Russia, letting loose the equivalent power of a ten to 15 megaton bomb. For reference, that's 1,000 times stronger than the atomic bomb dropped on Hiroshima.

The sound of the blast could be heard as far away as France and the shock wave it produced registered around a 5.0 on the Richter scale. It traveled across the planet three times, started fires, depleted about half the atmospheric ozone and destroyed 2,000 square kilometers of forest. And remember, this meteoroid didn't even strike land!

So, meteoroids are clearly a force to be reckoned with and scientists closely monitor the sky for ones with destructive potential. In fact, over the years, a catalog of _Near-Earth Objects,_ or _NEOs,_ and _Near-Earth Asteroids,_ or _NEAs_ has been created. Since records have been kept, thousands of such objects have been observed — thankfully, none of them pose a current threat.

For instance, the only NEO with any chance of hitting Earth has a 0.3 percent chance of coming close to the planet, and not until the year 2880. But things do change, and since these objects orbit the Sun, another planet's gravitational pull could change their trajectory.

That's why there are US-led projects like the _Asteroid Impact and Deflections Assessment Mission_ and the _Asteroid Redirect Mission_, which are hard at work coming up with ways to avoid a potentially cataclysmic impact by increasing or decreasing the speed of a meteoroid, making it safely avert Earth.

### 7. The meteorite impact that caused the dinosaur extinction was one of five major extinction events. 

There have been five mass extinctions in Earth's history, transitions with significant consequences for life on the planet.

The first two extinction events delineated the transition between the _Ordovician_ and _Silurian_ periods some 440 million years ago, and the end of the _Devonian_ period, 380 million years ago.

Then, 250 million years ago, the third and most destructive event occurred. It is known as the _Permian-Triassic_ extinction and it wiped 90 percent of Earth's species off the face of the planet, including most of the insects.

After that, with the end of the _Triassic_ period, 200 million years ago, the planet witnessed the extinction of 75 percent of its species.

And finally came the _Cretaceous-Paleogene_ or _K-Pg_ event that wiped out the dinosaurs. It occurred about 66 million years ago and again caused the extinction of 75 percent of Earth's species, including dinosaurs, plants and marine life.

But this event is unique in that, by examining exposed rock formations, we can see where the K-Pg boundary is in time and find evidence of a meteoroid impact.

When a meteoroid strikes Earth, it leaves signature signs like diamonds and _shocked quartz_, a deformation that only occurs in the mineral under two circumstances: a nuclear bomb detonation or the impact of a meteoroid.

So, the K-Pg boundary gives scientists evidence that the dinosaurs were wiped out by a meteoroid. But it also contains _iridium_, an extremely rare mineral on Earth. Here's why that matters:

In the 1970s, the geologist Walter Alvarez discovered that the iridium levels on K-Pg samples were 90 times higher than normal. Therefore, he knew that a meteoroid must have struck Earth, bringing with it about 500,000 tons of iridium. With this information, he could then predict that the meteoroid was a gigantic 10 to 15 kilometers in diameter!

So, while we don't know much about mass extinctions on Earth, one thing is clear: the K-Pg extinction was caused by a meteoroid — and a massive one at that.

### 8. Despite the massive damage ancient meteorites caused, finding evidence of their impacts on Earth isn’t easy. 

As you can imagine, a 10 to 15-km-wide meteoroid could do some serious damage if it were it to hit Earth. Assuming the object that hit our planet 66 million years ago was traveling at a velocity of at least 20 km per second, it would have the power of 100 trillion tons of TNT — that's more than one billion times the explosive force of the Hiroshima bomb.

Such an event would cause devastating winds, tsunamis, tidal waves and an earthquake so powerful it would be felt all over the world. But it would also result in trillions of tons of material being launched into the atmosphere. That, in turn, would result in the heat caused by the impact to become trapped in the planet, causing Earth's surface to cook.

There's evidence of just such a sequence in the K-Pg layer: a huge amount of charcoal soot suggests that over 50 percent of Earth's organic matter incinerated in the wake of the impact.

But the air and water would also be poisoned with nitrous oxide and sulfur, which would produce acid rain that would last for years.

So, if all that happened when the dinosaurs went extinct, there must still be physical signs of the impact, right?

Well, most craters caused by such a large meteoroid actually date back beyond the K-Pg impact to the Early or Heavy Bombardment periods. Over such a long period, natural effects like erosion would have covered up most of the impact craters. And if a meteoroid struck the ocean, there'd be little chance of finding the exact site of impact.

However, the evidence uncovered from the K-Pg layer by Walter Alvarez and others suggests that the meteorite struck a continental shelf, which, as you'll see, quickly pointed scientists to the impact crater's location.

### 9. Some very specific clues led to the discovery of the K-Pg crater. 

Figuring out where the K-Pg meteoroid landed took some serious investigation. But one clue was especially helpful: given the predicted size of the meteor, the crater it caused should have been about 200 km wide. This knowledge, along with two major leads, led scientists to the impact crater.

First, geologists who were working for the Mexican oil company Pemex found odd magnetic disturbances off the Yucatan peninsula in the Gulf of Mexico. In the 1970s, a magnetic survey was carried out, showing that the disturbances were caused by a circular region about 180 km in diameter.

In 1981, an American consultant named Glen Penfield, along with the geologist Antonio Camargo, presented their findings on the matter at a geophysics convention in Los Angeles. However, while their evidence pointed to the existence of an impact crater, nobody made the connection to the 66-million-year-old K-Pg meteoroid.

Later, in 1990, a University of Arizona team led by the graduate student Alan Hildebrand examined a K-Pg layer full of shocked quartz and iridium in Haiti, determining that the impact crater was within 1,000 km.

It was at this point, with all the information available and just needing to be put together, that the University of Arizona team presented their findings at a scientific conference attended by Carlos Byars, a reporter with the _Houston Chronicle_.

Byars remembered Penfield's discovery and pointed it out to Hildebrand who, with his team, studied the core samples the oil company had drilled out of the crater.

And guess what?

They found what they were looking for: shocked quartz and a layer of iridium that dates back 66 million years. In 1991, their full results were published in the journal _Geology_ and the crater was named Chicxulub after Chicxulub Puerto, a small fishing harbor not far from the crater.

### 10. Evidence suggests that there may be a regularity to mass extinctions caused by comets. 

While the Chicxulub crater was a major discovery, it's by no means the only such crater that scientists have found on Earth. In fact, scientists have been gathering all the data they can on Earth's impact craters, and the results suggest that there might be some regularity to comet strikes.

For instance, research on fossil records points to a regular extinction every 30 to 35 million years.

In 1977, Princeton geologists Michael Arthur and Alfred Fischer hypothesized that fossil records showed life on Earth rising and falling every 32 million years. Then, in 1984, researchers studying the record of extinctions at the University of Chicago stated that such information pointed to an extinction every 27 to 35 million years.

And, most recently, a study done in collaboration between the University of Kansas and the Smithsonian Institute found that extinctions happen at a regular rate of every 27 million years, give or take 3 million years.

There's also very similar data emerging from scientists who gather data from impact craters to date them and search for any regularity. A 1984 study done at the University of California, Berkeley, examined 11 craters and hypothesized a regularity of 31 million years.

That same year, New York University put out a study of 41 craters that pointed to a 31-million-year regularity. And in 2004, Kyoto University looked at 91 craters, going back 400 million years, to suggest a regularity of 37.5 million years.

So, the regularity of these impacts seems plausible — but studies have also suggested that the larger impacts were caused by faster moving, long-period comets that came from the Oort cloud.

### 11. The Milky Way’s galactic tide and our solar system’s orbital path offer possible explanations for why comets come from the Oort cloud. 

So, why do these potentially cataclysmic comets leave the Oort cloud?

Well, one of the best explanations has to do with how our solar system moves through the galaxy. As the solar system orbits the center of the Milky Way on a 240-million-year journey, it can experience what's known as a _galactic tide_ caused by the gravitational force of the Milky Way.

Similarly to how the Moon causes the tides of Earth's oceans to ebb and flow, the Milky Way's gravitational force affects our solar system. This galactic tide can elongate the normally spherical shape of the solar system's outermost edge — the Oort cloud.

A potential result of this elongation is to push objects further inward, possibly making them more susceptible to the gravitational pull of the Sun and primed for some force to come along and send them flying. But the galactic tide alone doesn't explain why there would be periodic comets emerging from the Oort cloud every 32 million years or so.

Another contributing factor in comets leaving the Oort cloud might be the solar system's orbital path. It's important to note that our solar system moves up and down from the galactic plane as it orbits the Milky Way. These movements are known as _oscillations_ and the Sun usually makes three to four of them on its path around the Milky Way.

The solar system passes through the center of the galactic plane just about every 32 million years, and the author suggests that our solar system might encounter a change in density in the middle of the plane. But for that to happen, some type of matter would need to affect gravity.

Can you guess what it might be?

### 12. Only time will tell if dark matter is the reason for the massive meteoroids that hit Earth every 32 million years. 

Okay, so you're probably wondering just how dark matter relates to comets hitting Earth, and the facts are all about to come together. As the author suggests, in addition to galactic tides, dark matter might explain massive meteorites striking the planet.

In fact, it's possible that more than one type of dark matter exists. After all, there are plenty of types of other known particles like quarks and neutrinos, so it's not unreasonable that there could be multiple forms of dark matter. One such type might be _self-interacting dark matter_, dark matter that's attracted to itself and which therefore clumps into a higher density.

So why is this important?

Well, if there was a type of dark matter that interacted with itself in this way, it might explain why some galaxies have experienced irregularly distributed density with greater concentrations closer to their center.

This might even be exactly the case with the Milky Way. Self-interacting dark matter might be forming a smaller disk inside the center of the galactic plane, thereby creating an area of higher density.

While the solar system orbits the galaxy, as it oscillates, it would have to pass through this dark matter disk approximately every 32 million years. When it does so, it would hit that pocket of higher density, providing the kick a comet primed by the galactic tide would need to get shot out of the Oort cloud and toward Earth.

But it's hard to say if this is true and only time will tell. In fact, scientists should soon be able to validate or disprove the author's idea; in 2018, they will get data back from the GAIA satellite, which takes precise measurements of the galaxy. The results should show whether or not there's an area of high density in the center of the galactic plane that changes the velocity of stars.

### 13. Final summary 

The key message in this book:

**Dark matter is all around us. We can't see it or feel it, but it's vitally important; in fact, it has played a central role in forming the universe itself. There's also a good chance that it's responsible for sending meteoroids to Earth that contained the essential ingredients for life as we know it.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Cosmosapiens_** **by John Hands**

_Cosmosapiens_ (2015) is about the evolution of scientific theory — from the origin of matter and the universe to the emergence of life on Earth and the evolution of human consciousness. For centuries, we've been struggling to find out who we are and why we're here. Learn about the progress we've made toward answering these important questions — and about the barriers that still stand in our way.
---

### Lisa Randall

Lisa Randall is a science professor at Harvard University specializing in cosmology and theoretical particle physics. In 2007, she was named one of _Time_ Magazine's "100 Most Influential People" and is the author of other books including _Warped Passages_.

