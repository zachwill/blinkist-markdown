---
id: 59354e2fb238e100051fda82
slug: dark-money-en
published_date: 2017-06-09T00:00:00.000+00:00
author: Jane Mayer
title: Dark Money
subtitle: The Hidden History of the Billionaires Behind the Rise of the Radical Right
main_color: 507025
text_color: 3E571D
---

# Dark Money

_The Hidden History of the Billionaires Behind the Rise of the Radical Right_

**Jane Mayer**

_Dark Money_ (2016) is a chilling look behind the scenes of American politics, outlining how a small handful of the country's richest people have been influencing the country's political landscape since the 1970s. Far from a conspiracy theory, these are the cold hard facts of the powerful and immensely wealthy individuals behind the rise of today's radical right-wing conservative movement.

---
### 1. What’s in it for me? Follow the money to the highest levels of US politics. 

Have you ever heard the phrase "follow the money?" Popularized by the 1976 movie _All the President's Men_, it was used by the FBI informant known as Deep Throat to describe a money trail that would lead to corrupt politicians. But is the corrupting influence of money in politics still something we need to fear?

You bet.

These blinks reveal how two brothers, Charles and David Koch, and their conservative friends have used their enormous financial resources to influence and alter politics in the United States over the last decades. We'll look into where this involvement stems from, as well as the methods the Koch brothers use and the impact they've had on the country's political landscape.

So let's follow the dark trail of money and see how it has fostered the emergence of radically conservative politics in the United States.

In these blinks, you'll learn

  * why the Koch brothers decided to get involved and influence politics;

  * how think tanks and other NGOs hide the money trail; and

  * how the Tea Party movement was stirred up and utilized by the Koch Brothers.

### 2. The Koch brothers head a network funded by billionaires who are determined to control US politics. 

When it comes to US billionaires, you probably think of Mark Zuckerberg or Bill Gates. But there are others who wield even more power, yet remain in the shadows.

The billionaire brothers Charles and David Koch run Koch Industries, the nation's second-largest private company, which was founded by their father, Fred Koch, in 1940.

Fred Koch started amassing his fortune by building oil refineries for the Soviet Union and Josef Stalin, which led to Koch's company being commissioned to construct a massive oil refinery for Nazi Germany in 1934.

Fred was indeed a Nazi sympathizer at the time. He even hired a nanny who was a Nazi sympathizer to look after his children. In fact, the nanny was such a passionate Nazi supporter that she moved back to Germany after Hitler's invasion of France in 1940, when Charles was just five years old and David was a newborn baby.

Eventually, Charles and David would grow up to become business partners, and since the 1970s they've been building an elaborate network of political donors and fundraising institutions. This network has so many arms, and reaches so deep into the heart of American politics, that it's known as the _Kochtopus_.

Ultimately, the goal has been to spread their message of libertarian values. These values focus on supporting a free market and small government devoid of regulations, taxes or anything else that might get in the way of profits.

In 1980, this crusade led David Koch to make a brief run for office as the Libertarian Party's vice presidential candidate. That year, the Libertarian Party only managed to capture around one percent of the national vote.

But the experience informed the brothers' future political strategy. They realized that a politician is merely a public voice — real power is creating and shaping the messages that those voices project.

So, the Koch brothers decided to remain behind the scenes from that point onward, and began focusing their energy on an ambitious long-term strategy.

### 3. The Koch network uses nonprofits to keep political donations anonymous and tax-free. 

For the Koch brothers to successfully change public opinion, they knew they had to find a subtle way for their ideas to gain traction. They eventually landed upon a great way to do this: disguising their political agenda as philanthropy.

They soon began spreading their libertarian ideology through nonprofit organizations such as advocacy groups, think tanks and private foundations. It proved to be a great strategy, since nonprofits are both cost-effective and require very little public disclosure.

This meant that all donations could be made anonymously and the amount could be kept hidden from public view. Donors could write as big a check as they wanted, and everything could be kept hush-hush.

This secrecy is where the term "dark money" comes from. Nonprofits make it impossible to know how many millions of dollars are being funneled in, where the money is coming from and what it's being used for.

But there's another important reason the Koch brothers use these organizations: nonprofit donations are also tax deductible. So, by having their family inheritance donated to a philanthropic foundation, the brothers can avoid paying inheritance tax while helping fund the cause of their choice.

Over time, their network of institutions would come to envelop many big political names, such as Senator Ted Cruz and New Jersey Governor Chris Christie. Big donors to their organization include financial titans like Charles Schwab and the cofounder of Amway, Richard DeVos, both of whom had contributed at least a million dollars between 2010 and 2011.

All this money adds up to provide the Koch brothers with tremendous financial resources.

By 2013, they controlled over 100,000 of these private foundations, with combined assets of over $800 billion. One of the oldest and most influential arms of the Kochtopus is the Heritage Foundation, which has assets of $174 million.

That's a lot of money to use influencing public policy. A lot of money escaping the scrutiny of voters, avoiding political transparency laws and taking advantage of tax loopholes.

### 4. The Koch network includes educational programs and think tanks focused on long-term influence. 

To effectively influence public opinion and decrease the chance of future resistance, educational organizations and programs became a vital part of the Koch brothers' conservative fundraising network.

The idea was to donate to respected universities that would return the favor by providing a space to nurture a new generation of conservative thinkers. This would increase their influence on society by churning out future political leaders who'd been taught the Koch ideology — which is why law schools quickly became a primary target.

One of the first right-wing educational organizations was the Olin Foundation, named after John M. Olin, who is the head of the arms manufacturing company Olin Industries.

Olin was the mastermind behind the _Mansfield's Program on Constitutional Government_ at Harvard, which received a $3.3-million donation from the Olin Foundation. This program provided a conservative interpretation of American government and its students went on to teach at Georgetown, Harvard and Yale.

Another method for supporting the conservative agenda came in the form of conservative think tanks, which became an effective way to raise doubts about scientific data.

The George C. Marshall Institute, for example, is one of the premier conservative think tanks and the recipient of donations from the Koch network.

This institute is the home of physicists Fred Seitz and Fred Singer, who were at the forefront of challenging the science behind climate change. Despite Seitz and Singer's lack of expertise in the field, their message of doubt effectively grabbed the public's attention.

Think tanks like to claim that they provide a necessary balance to scientific debate, but what they're really offering is misleading information with little academic merit. They're simply delivering the results of dubious research funded by people who wanted favorable results that could be utilized to support their cause.

These think tanks were particularly influential during the Reagan administration: a copy of the Heritage Foundation's _Mandate for Leadership_ was handed out as a playbook to every member of Congress.

### 5. The Tea Party movement became a way to manufacture grassroots credibility. 

By 2009, the groundwork for a political movement had been put in place by the Koch brothers. It was now time for those patiently nurtured nonprofits and educational programs to pay off in the form of the Tea Party movement, which finally brought libertarianism to the mainstream.

The seeds of this uprising can be traced back to the Koch brothers' right-hand man, Richard Fink, and his _Structure of Social Change_ manifesto from the 1980s.

This plan gave precise details on how to start a legitimate political movement. First, the flow of information should move from the brains at the think tanks to the politicians at the podiums. Then, the time would come to send out the foot soldiers and take the message to the streets.

This last step is exactly what happened throughout the 90s and early 2000s, as the Koch network supplied the necessary money to stage authentic-looking uprisings. And even though these events were anything but spontaneous and genuine, the general public was fooled into believing they were.

For it to look like a legitimate political movement, it was important for the libertarian message to be seen as "grassroots" and originating from everyday people. But since it was all manufactured by the Koch brothers, it would become known as an "astroturf" movement, taking its name from the brand of synthetic grass.

Their cause was also helped significantly by the amount of right-wing media publicity it received.

While CNBC reporter Rick Santelli often gets credit for helping launch the Tea Party movement, he was actually taking the bait that had been left by Wilbur Ross, Jr.

Ross, a close friend of David Koch, was on CNBC to attack President Barack Obama's plan for helping people escape the mortgage crisis. Santelli took the bait and railed against Obama's plan as an example of "promoting bad behavior" and asking why people's tax dollars should help pay for the second bathroom their neighbors couldn't afford.

Another Koch-funded think tank called _Freedomworks_ paid over a million dollars for conservative media personality Glenn Beck to read the think tank's "embedded content" on Fox News.

> _At one Tea Party rally, protesters had a banner depicting corpses from Dachau concentration camp, implying Obamacare was comparable to the Nazis' state-ordered murders._

### 6. A Supreme Court decision in favor of Citizens United gave the Koch brothers even more power. 

Things took a dramatic turn in 2010, when the case of _Citizens United v. Federal Elections Commission_ went before the Supreme Court.

The conservative think tank Citizens United ended up winning their case, and the decision said that corporations have the right to freedom of speech. It was a game-changing ruling, and it opened the floodgates for even more cash to flow into the Koch network.

What really changed was that both nonprofit and for-profit corporations could now spend as much money as they wanted to either support or oppose a political candidate. Remarkably, the limit on how much an individual could give to these corporations would also soon disappear.

This effectively reversed important century-old laws that had been in place to ensure that an even political playing field wouldn't be thrown off-balance by wealthy supporters.

This decision also achieved another long-term goal of the conservative think tanks: it destabilized unity among liberal groups, as many liberals joined the fight to advocate for broader First Amendment rights.

One thing that remained unchanged was that donors could still keep their anonymity, and this secrecy only helped increase the amount of dark money in politics.

Citizens United received a flood of donations after the decision — so much that they were able to spend up to 49 percent of their overall revenue on political activity.

They could also transfer entire multimillion-dollar donations to another group in the network for a completely different purpose, creating a never-ending maze of untraceable money, all changing hands out of the public eye.

With these new rules, the Kochtopus was able to flex new, even stronger muscles in the days leading up to the 2010 midterm elections, making their success even more of a foregone conclusion.

> _"There was a huge change after Citizens United . . . anyone could spend money without revealing who they were, [and] the floodgates opened."_ — _Democratic Congressman Rick Boucher_

### 7. The Koch’s REDMAP plan gave Republicans control of key states and the House of Representatives. 

After the Democrats won the presidential election in 2008 and increased their majority in both the House of Representatives and the Senate, Republican strategist Ed Gillespie came up with a plan known as _REDMAP_.

The goal was simple: get as many Republicans as possible elected to Congress in the 2010 midterms so they would have the legislative control necessary to then redraw the congressional districts in 2011.

The act of redefining the borders of a state's congressional districts in order to benefit a given party is called _gerrymandering_. For REDMAP, it worked by shifting the borders of these districts such that the vast majority of citizens who tended to vote for the Democratic Party were lumped together in a single district.

This would leave the Democrats with a significant majority in that one district, but they would then be underrepresented everywhere else, giving them only a minority of districts in each state.

This plan was actually in place before the Citizens United decision, but it benefitted greatly from it. Millions in dark money were used to promote Republican candidates and attack Democrats in vulnerable states through vicious attack ads.

North Carolina is a perfect example of where the REDMAP plan worked like gangbusters. The longtime Koch ally and donor, Art Pope, offered extraordinary financial support in the state, and the Republicans won 18 of the 22 local legislative races. As a result, Republicans represented North Carolina in both Congress and Senate for the first time in 140 years.

The plan also succeeded at the national level, with the Republicans taking control of the House of Representatives in 2010. With their majority in the House in place, the party was now free to redraw the borders of congressional districts and skew electoral outcome for years to come.

North Carolina again offers a perfect example of the disastrous effects that the REDMAP gerrymandering had. As part of the Koch brothers' small-government agenda, unemployment benefits in the state were cut massively, resulting in the state with the country's fifth-highest unemployment rate receiving one of the smallest amounts of federal aid.

As we can see, you don't have to be in the White House to gain control of public policy and enact your vision, which is exactly what the Koch brothers were able to do.

### 8. With a Republican majority, the Kochs went after the Obama administration and climate change. 

The Kochs' intense hatred of Barack Obama was regularly discussed at their semiannual donor summits.

In June of 2011, Charles Koch borrowed a phrase from Saddam Hussein to describe the war they were about to enter and their hopes of being able to block Obama at every turn. He called it, "The mother of all wars." Thanks to the success of REDMAP, and the radical Republican representatives that were elected as a result, they went on to engineer the government shutdown of 2013.

Mark Meadows is the man who is often credited as the mastermind of the shutdown. He was one of the North Carolina congressmen who benefitted from gerrymandering and was elected during the 2012 midterm elections.

Meadows wrote an open letter to Republican leaders in the House of Representatives, inciting them to withhold the funds that were required to implement the Affordable Care Act, better known as Obamacare. Everyone knew that if the Democrats didn't back down, the government would grind to a halt.

This new wave of Republican extremists in the Senate also put an end to any hope of there being a reasonable debate on subjects like climate change.

Believe it or not, global warming used to be a bipartisan issue that united Democrats and Republicans, as both sides recognized the serious and imminent danger of environmental degradation. Over time, however, many Republicans backtracked and denounced climate change as a non issue or even a hoax, as the influence and power of the Kochtopus continued to grow.

Climate change was always a thorn in the side of the Kochs, as more than one multimillion-dollar lawsuit had threatened their companies due to the health and environmental dangers they presented.

As a result, they had spent years publicly denouncing well-respected environmental scientists and pitching an alternative story, namely that the government's climate change regulations were an attack on people's freedom.

Meanwhile, the stranglehold that the Kochtopus had on the Republican party was about to reach an alarming level, as we'll see in the final blink.

### 9. America is dangerously close to becoming an oligarchy. 

At the heart of the American Dream is the belief that anyone can make their dreams come true, regardless of their background. Yet the work of the Koch brothers has widened the country's economic divide. The rich are getting richer and it's becoming increasingly difficult for anyone to challenge their authority.

In fact, their network is so big that it's practically taken over the Republican party.

Any politician that steps out of line has to face the wrath of the Koch brothers, including former Koch favorite and Republican Speaker of the House, John Boehner, who stood up against the Koch brothers and refused to agree to their demands.

The result? He was essentially forced to resign by the ultraright-wing Representatives that had been groomed and elected with the network's help.

This is a clear example of how a democratically elected official can come to be controlled by a few of the nation's wealthiest people.

Indeed, it would imply that the country's political system is far closer to an oligarchy than a democracy, since an oligarchy is exactly that: a government controlled by the will of its richest people, regardless of the will of the citizens.

Yet there is one billionaire who can afford to ignore the mighty Kochtopus: Donald Trump.

Trump can cast himself as being on the side of the working class by arguing against tax loopholes that billionaires like the Kochs profit from.

However, Trump's choice for vice president was Mike Pence, one of Charles Koch's favorite politicians. Leading up to the 2016 presidential election, experts estimated that the Koch network would spend $889 million to influence the outcome, a clear sign of the disproportionate advantage they have over their opponents.

But the Kochs aren't alone. At least eleven of the 200 people present at their June 2010 retreat were on the _Forbes_ 400 Wealthiest Americans list, with a combined fortune of $129.1 billion.

We all know that money equals power, but that doesn't mean we should let a small group of wealthy men take over the "Land of the Free." The United States is also the "Home of the Brave," and it's time to take it back.

### 10. Final summary 

The key message in this book:

**American politics is dominated by a handful of billionaires led by the Koch brothers. Their main priority is to shape policies according to their self-interest, regardless of whether those policies harm the general population — or even the planet. Working in their favor is a shady maze of largely untraceable dark money, which allows the Koch brothers to spend billions to get loyal politicians elected and spread their agenda.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Crippled America_** **by Donald J. Trump**

In _Crippled America_ (2015), American businessman and billionaire Donald J. Trump diagnoses the problems America is facing today and explains how he will truly make America great again. With disastrous trade agreements hurting our middle class and the government failing to protect us against ISIS, it's no wonder America doesn't win anymore. Some major changes need to be made — and Trump is going to make them.
---

### Jane Mayer

Jane Mayer is an investigative journalist and staff writer for the _New Yorker_ and recipient of the John Chancellor Award for Excellence in Journalism in 2008. She is also the author of _The Dark Side: the Inside Story of How the War on Terror Turned into a War on American Ideals_.

