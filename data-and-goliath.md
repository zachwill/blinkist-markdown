---
id: 55acf8a165363800079a0000
slug: data-and-goliath-en
published_date: 2015-07-22T00:00:00.000+00:00
author: Bruce Schneier
title: Data and Goliath
subtitle: The Hidden Battles to Collect Your Data and Control Your World
main_color: None
text_color: None
---

# Data and Goliath

_The Hidden Battles to Collect Your Data and Control Your World_

**Bruce Schneier**

_Data and Goliath_ (2015) takes an in-depth look at one of the most pressing issues of our time: mass surveillance. It traces the origins of surveillance, explains its various negative impacts and offers advice for organizations and individuals looking to evade it.

---
### 1. What’s in it for me? Learn why people are after your online data, and what you can do about it. 

In May 2013, when Edward Snowden spilled the deepest, darkest secrets of US intelligence agencies, the issue of mass surveillance was immediately brought to the fore. As it turned out, the NSA had been spying on us all, collecting and sifting through our online data for years.

Many were shocked, but many others remained unconcerned, and a common refrain quickly emerged: "I haven't committed a crime, so why would I mind the government snooping on my data?" And yet, whether law-abiding citizens or not, we should all be worried about mass surveillance.

These blinks show you how mass surveillance is undermining many of the things we hold dear, from the freedom of the internet, to our privacy and economic prosperity. But even so, as you will find out, there is still a lot we can do about it.

In these blinks, you'll discover

  * why the internet knows more about those closest to you than you do;

  * why one man's plan to party extra hard led to him being kicked out of the United States; and

  * how searching for weird things on Google can help you avoid surveillance.

### 2. Surveillance is more widespread today than at any other point in history. 

In the 1700s, British philosopher Jeremy Bentham developed a novel and important concept: the _panopticon,_ a hypothetical prison where every inmate could be watched at all times. Bentham was interested in the effect that constant surveillance would have on the prisoners' behavior.

Until recently, Bentham's idea was just that — an idea. Even in a prison, it simply wasn't possible to keep watch over everyone, all the time. But today, digital technology has changed this for good.

Facial recognition software, for example, allows authorities to use video cameras to digitally identify people. The FBI already has a database of 52 million faces, and they can use it to track persons of interest.

Meanwhile, authorities in Dubai are using facial recognition software from Google Glass to identify people on the street. And video cameras aren't the only tools of surveillance, either: authorities can also track our actions online.

A total of 76 exabytes (76 billion billion bytes) of information pass through the internet every year, and the development of cloud technology has made it easier and cheaper to store all this data.

Every bit of data you generate online can be collected or tracked by someone else. A law student named Max Schrems realized the gravity of this situation in 2011, when he demanded that Facebook disclose all the data they had on him.

Facebook sent Schrems a 1,200-page document — and it wasn't only information from his profile and newsfeed. Facebook also had a record of every page and photo he'd ever clicked on, and every advertisement he'd ever seen.

Jeremy Bentham's panopticon is already all around us. We're monitored at all times and all of our actions are recorded and stored indefinitely. Welcome to the global surveillance state.

### 3. The internet isn't free: you pay for it with your privacy. 

How much do you pay to access the sites you view on your internet browser? Well, you don't pay anything, because online content is "free," right?

Wrong. Online companies don't give you their products or services for free, they've just found ways to get your money other than by charging you directly when you use their site.

When the internet was first developed, it was open and non-commercial. So when companies first started moving their business practices online, they faced a completely unprecedented challenge: they couldn't charge people for content. Customers simply ignored any content that cost money, since there was so much available for free.

Companies realized that they couldn't charge their customers directly, so they started making money off the data they generated instead.

The practice of collecting and storing data of all kinds is called _big data_. The process of going through it to identify key information is called _data mining_.

In the business world, collecting and mining data is all about finding more effective ways to advertise. After all, it's much easier to sell products to your customers when you know exactly what they want.

This strategy has been quite successful, too. In 2012, for example, a man complained to _Target_ when they began sending his teenage daughter advertisements and coupons for baby products, which he found to be inappropriate.

But Target had a good reason for its seemingly odd marketing strategy: By analyzing her online data, the company had deduced, correctly, that the man's daughter was pregnant. Thanks to big data, Target knew more about the teenager than her own father did.

As this story illustrates, the internet isn't free. Rather, we pay for it with something other than cash: privacy.

### 4. Mass surveillance is harmful to our liberty, intellectual freedom and even our health. 

You might think that mass surveillance is mostly harmless, even if it's a bit uncomfortable. It could even be useful, especially if companies use it to sell you things you need. As long as your data isn't misused, you'll be fine, right?

Wrong again. Mass surveillance and the loss of our privacy damages both our individual lives and society as a whole.

The greatest cost of mass surveillance is our loss of liberty. Without liberty, we lose the right to be different.

An Irish man named Leigh Van Bryan learned this the hard way in 2012, when, before traveling t o the US, he tweeted "Free this week, for quick gossip/prep before I go and destroy America." His use of the word "destroy" was a reference to his rather ambitious party plans.

Unfortunately for him, the American government surveys the entirety of the Twittersphere and didn't take his joke lightly. Just after exiting his plane, Van Bryan was accosted by agents who questioned him for five hours before sending him back to Ireland.

Van Bryan's story might seem trivial, but it's quite the opposite. If people can't use unconventional language for fear of getting in trouble, their ability to communicate will become increasingly restricted.

And it won't stop there: as we're subjected to more and more surveillance, people will simply start trying to blend in to stay clear of the authorities. In turn, society will grow boring and stale. As Frank Zappa once said, "without deviation from the norm, progress is not possible."

Our loss of privacy affects our health, too. We perceive the loss of privacy as a loss of control over our own lives, which can make us physically and emotionally ill. In fact, studies have shown that people under surveillance are more likely to develop depression, anxiety and low self-esteem.

### 5. Mass surveillance damages the economy – but it also creates a market for companies offering ways to avoid it. 

Mass surveillance isn't only harmful to our bodies and the society we live in — it hurts our economic well-being too.

It likely doesn't come as a surprise that companies don't respond well to mandatory surveillance policies. If the American government starts passing laws that threaten their business success, companies will simply choose to move their data away from American jurisdiction.

The revelations brought forward by Edward Snowden illustrated the tenacity of this issue. In 2013, Snowden released information containing proof of the NSA's extensive data collection programs, which pushed many American companies seek out cloud storage services overseas. They didn't want their data to be jeopardized by the American government by keeping it at home.

This has resulted in significant financial losses within the American technology sector. In 2013, Forrester Research, an internet analysis firm, predicted that American cloud providers would lose $180 billion over the following three years.

It is possible that surveillance could be economically beneficial for some people, however. A new market is opening up as more and more consumers are willing to pay to keep their data private. In fact, a study from 2000 estimated that internet spending could increase by $6 billion if customers could be assured that their data was secure.

Some businesses are already responding to this demand. _DuckDuckGo_, for instance, is a search engine that ensures its users aren't tracked. _Wickr_ allows its users to send encrypted emails that the government can't spy on, while _Ello_ offers a social network that doesn't store and mine its users' clicks.

### 6. There are four ways a company can respond to the threat of government surveillance. 

Imagine you're a private business owner. One day, men in suits come to your door, flash their badges and demand you hand over your data. What do you do?

Businesses can respond to this scenario in one of four ways.

First, a business can be open. Several computer companies do this by regularly publishing _transparency reports_ that give a general overview of the data requests sent by the government, and the ones that they complied with.

Google, for example, reported in 2013 that they had handed over the internet data of between one and 2000 customers, and shared the communication content of between 18,000 and 20,000.

Companies can also be more transparent by using _warrant canaries_, subtle cues that alert users if the government secretly demands their data.

Apple did this in 2013 when they released a transparency report containing the sentence, "Apple has never received an order under Section 215 of the USA Patriot Act." This way, if and when it does receive an order, it can remove the sentence as a signal.

If a business doesn't want to be open, it can also try to defend itself. Many companies encrypt their customer data to keep it safer from the government. Yahoo did this after learning that the NSA was monitoring its customers' usage.

A business can also respond with litigation. Companies that face similar challenges can come together and file briefs to protect themselves and each other. This happened in 2013, when the FBI demanded that Lavabit hand over its user data. Google, Microsoft, Yahoo and several other email service providers filed briefs to stop this from happening, as the case could have been used as a precedent to go after them next.

Finally, if they're powerful enough, a company can lobby. Some large, international technology companies have serious political influence and can use their power to lobby for legislative restrictions on surveillance.

### 7. Individuals can try to subvert surveillance by avoiding, blocking and distorting it. 

We've examined some of the ways that organizations can respond to government surveillance, but there are important steps that individuals can take as well.

The easiest way to evade surveillance is to change your behavior. Start paying in cash rather than using a traceable credit card, and change your daily commutes to avoid any cameras.

In China, where automated surveillance is widespread, many people have started writing notes on paper and sending photos of them, which makes their messages harder to track.

If you aren't being directly targeted, you can make it hard for the government to track you by using _blocking_ strategies.

Privacy-enhancing technologies, like browser plug-ins that block sites trying to spy on you, are the first line of your _blockade._

Privacy Badger, for example, is a free software program that blocks advertisements and tracks cookies. Meanwhile, Ghostery allows its users to detect and control _web bugs_, hidden objects that are embedded in web pages and collect information on users' browsing habits.

Encryption is the second line of your blockade. The best encryption programs for your hard drive are Microsoft's BitLocker and Apple's FileVault.

Finally, you can take measures to _distort_ surveillance. Two people can use each other's information online, throwing off tracking algorithms by making them record incoherent and irrelevant information.

The author, for example, configured his browser to delete his cookies every time he closes it, making it much harder for companies to track his habits; as a result, targeted ads don't follow him nearly as much.

Likewise, when the author shops at Safeway, he uses his friend's shopper number, which distorts any potential surveillance of either of them. You can also pollute your personal data by entering irrelevant information into web forms and searching for random things on Google.

### 8. Final summary 

The key message in this book:

**The internet era is upon us. We generate more and more data as technology floods further into our lives, and although this development is beneficial to some, it's ultimately detrimental to our freedom, health, society and economy. If we want our society and liberty to prosper, individuals and organizations alike must start taking steps to protect themselves from surveillance.**

Actionable advice:

**Get informed and do what you can to protect yourself.**

Use the Rubicon Project, AdSonar, Quantcast, Pulse 260, Undertone and Traffic Marketplace to learn more about out which companies are tracking you. Install the DoNotTrackMe browser plug-in and protect yourself from more sophisticated flash cookies with _FlashBlock_. Encrypt your email with PGP (but be aware that it doesn't protect your data while your computer is in use).

**Suggested** **further** **reading:** ** _Big Data_** **by Viktor Mayer-Schönberger and Kenneth Cukier**

_Big Data_ provides an insightful look at why a change to "big data" is a major shift in how we collect, use and think about the data around us. It provides great explanations and examples of how individuals and companies already ahead of the curve are using the tools of big data to create value and profit. Casting an eye forward, the book also outlines the future implications for a big-data society in terms of the risks, opportunities and legal implications.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Bruce Schneier

Bruce Schneier is a world-renowned security expert and the best-selling author of over 13 books. He's also a fellow at the Berkman Center for Internet and Society at Harvard Law School and the Chief Technology Officer of Resilient Systems, Inc.

