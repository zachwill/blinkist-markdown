---
id: 57c6c34dd4d3c400037d1864
slug: dazzled-and-deceived-en
published_date: 2016-09-01T00:00:00.000+00:00
author: Peter Forbes
title: Dazzled and Deceived
subtitle: Mimicry and Camouflage
main_color: 95AD37
text_color: 53611F
---

# Dazzled and Deceived

_Mimicry and Camouflage_

**Peter Forbes**

_Dazzled and Deceived_ (2009) explores the fascinating phenomenon of camouflage and mimicry in nature, where animals act like leaves and harmless prey look like ferocious predators. These blinks examine how the science of camouflage has influenced not only artistic expression but also how humans fight and win wars.

---
### 1. What’s in it for me? Be dazzled by nature’s deceptions. 

Octopuses are famous for their uncanny ability to hide in plain sight. If you've ever gone snorkeling, chances are there was one right under your nose, with you none the wiser. Over millions of years, these creatures have evolved into masters of deception.

And it's not just octopuses that can "disappear" at will. Nature is full of animals with stunning hiding skills. It certainly makes sense for survival: if predators can't see you, they can't eat you!

Ever since naturalists in the nineteenth century began to study this hidden world of camouflage, what they revealed has greatly influenced scientists, inventors and even artists. Modern warfare, for example, would not be the same without it. Welcome to the dazzling world of camouflage!

In these blinks, you'll learn

  * why so many butterflies look alike;

  * how the praying mantis catches insects; and

  * why texture is so important.

### 2. Distinguished scientists like Darwin, Wallace and Bates studied mimicry in nature for over a century. 

Have you ever looked at a tree trunk and been startled to see a butterfly emerge, seemingly out of nowhere? If so, you've experienced the wonder of mimicry, a phenomenon that has captivated laypeople and researchers alike.

Let's go back in history to see how the study of mimicry originated.

In the nineteenth century, English naturalist Henry Walter Bates and friend Alfred R. Wallace set out to research butterflies in the Amazonian rainforest. 

They were delighted by the unbelievable variety of different species they found. What excited their scientific imaginations even further were the fascinating, mysterious behaviors of these many species.

For instance, these pioneers found that the benign _Leptalis_ genus of butterfly mimicked the look of poisonous butterflies. Leptalis butterflies did this so effectively, in fact, that they shared almost the same wing pattern with poisonous butterflies. Only when Bates caught, dissected and compared the two types of butterflies was he able to recognize them as two distinct species.

This behavior, in which harmless creatures mimic poisonous or harmful ones, is now aptly called _Batesian mimicry_. This finding, among others, marked some of the first steps of mimicry study.

If the names of these scientists don't ring a bell, you'll be interested to know that Charles Darwin was also a frequent collaborator with Wallace. In fact, Darwin had initially felt threatened because Wallace's theories on natural selection were similar to his, and thus created the potential for competition.

Luckily, the two scientists decided to channel their competitive energies into collaboration. Darwin was particularly interested in how mimicry might influence his theories of evolution and the survival of the fittest. Together, Wallace and Darwin explored the intersection of mimicry and mating, with questions such as whether potential sexual partners would be more attracted to colorful mimics.

Though most of their theories didn't hold up, their mimicry studies were highly influential on their research and publications on evolution theory — and on science as a whole.

### 3. Mimicry in nature is used both for protective and predatory behavior. 

So we know that animals use mimicry in enchanting ways, but what motivates them to mimic in the first place?

A key reason animals hide themselves in nature or recreate the looks of more dangerous species is for protection from predators.

Many insects, for instance, are easy prey for animals like birds. To evade bigger animals, some butterflies and moths have adopted the imitation approach, blending in with surrounding leaves, stones or minerals.

On top of a pile of dead leaves, for instance, you might find a _Kallima_ butterfly, which looks exactly like a rotting leaf when it's sitting still!

Other animals choose to stand out rather than dissolve into their surroundings. Some insects have evolved to boast an array of fluffy antennae or brightly colored wings with green, blue, yellow or red spots to appear poisonous. Mimicking insects display bright, eye-catching colors and shapes to signal to potential predators that they are venomous and likely to cause sickness. This keeps the hunters safely away, even though in reality, the mimickers are completely innocuous.

It's not just weak animals that use mimicry, though — predators also use camouflage to help them hunt.

Take the _Celaenia excavata_ spider species. These spiders wrap and hide themselves in a silky web to create the illusion of a mound of bird poop. Why? Because tiny flies and other insects are attracted to animal feces and as a result, they get caught in the sticky net and become a tasty snack for the hiding spider!

The shape and color of praying mantises help these insects blend in with their surroundings, too. Because they are often mistaken for flowers, praying mantises can simply wait until a bee comes buzzing around for pollen, then attack and devour it!

### 4. We still have a lot to learn about the biological development of mimicry and camouflage. 

The basic principles of natural selection in modern science are well-established today. But back in Charles Darwin's time, many biologists still struggled to understand how changes in a species were related to evolution and survival.

In Britain during the late eighteenth century, for instance, the boom in factories was causing forests around industrialized cities to become heavily polluted with coal dust.

As a result, light brown trees began turning black — and in turn, so did moths that lived in the forests. By the early nineteenth century, a variety of black-winged moths appeared, outnumbering those with light-colored wings.

Biologists had some theories for why this was happening. Could the moths somehow darken their wing color? Or was it because light-colored moths were more easily hunted by predators, and had consequently dwindled in number?

Only after Darwin's death was it proven that the cause was indeed that light-colored moths had become easy prey. Darwinian theories of natural selection were thus supported.

The advent of fields such as genetics and epigenetics have brought about further progress in mimicry research. _Epigenetics_ is the study of changes in organisms caused not by actual alterations in genetic code but rather through modifications of gene expression.

For example, recent studies have found that a species of mouse has fur that changes color when a particular single gene is activated. When these mice live in a forested area, the color-changing gene is activated, meaning the mice have brown fur. Yet the same species of mouse living in rocky or volcanic areas has black fur. Both forest mouse and volcano mouse share the same genetic code!

Biologists are still baffled, however, by the camouflage abilities of many other animal species. It's still a mystery how certain octopuses can change their skin from being smooth and flat — when moving over smooth, sandy parts of an ocean floor — to rough and bumpy while moving over rocky ground. 

Though there is still much to be learned, humans remain fascinated by mimicry. In fact, our culture would be vastly different if we had never discovered nature's talent for camouflage. We'll explore this idea next.

### 5. Mimicry and camouflage have had a large influence on art and artists. 

Just as scientists have been captivated by the mimicry talents of animals, many artists have also drawn inspiration from the natural world.

Some have gone even further and studied such phenomena in a scientific manner.

American artist Abbott Handerson Thayer started studying camouflage with a goal to employ similar techniques on canvas. He became intrigued with how the coats of many animals are two-toned — dark on top and lighter toward the bottom.

His research led him to the discovery that this pattern allows an animal's coat to reflect light in such a way that it becomes less visible in bright sunlight.

Bright light usually makes prey more visible to predators, but this two-toned effect, called _countershading_, allows an animal to camouflage itself more effectively.

Thayer would spend his later years putting his findings to another use, by helping military camouflage efforts in applying countershading techniques to the hulls of ships. 

Major innovations in the art world have also been catalyzed by a creative fascination with natural phenomena.

Painters like André Mare started exploring different ways to represent an object by taking its natural appearance and fragmenting its form into basic geometric shapes. A face rendered in this style would still be recognizable as a face, yet be represented on canvas by triangles, cubes and lines in unnatural colors.

In another of Mare's paintings, a handgun is barely detectable as it's been expertly camouflaged within the painting itself. Such a breakdown of normal vision and sight through artistic camouflage was the beginning of a highly influential painting style called Cubism.

Many Cubist techniques echo the inspiration of animal camouflage, such as cats with different patterns on their fur, helping to distort their body's outline. Such patterns enable animals to blend better into a background, whether forest or dry savannah.

A leopard's coat is a great example of this phenomenon. Its palette of sandy colors, combined with spots that range from black to dark brown, enables a leopard to hide in plain sight as it crouches and waits for prey — then pounce!

### 6. The first attempts at using military camouflage in World War I were inspired by the natural world. 

Wartime inspires people to find innovations by drawing upon diverse sources. In World War I, military experts sought the assistance of zoologists and artists.

What exactly was the reasoning behind bringing in such an eclectic mix of people?

The nations involved in the war, such as the United States, France, and Britain, wanted to optimize their chances of victory on the battlefield. To do so, these countries decided to invest in technologies and scientific innovations to protect airplanes and ships through camouflage.

Merchant ships were key targets for German submarines, so it was a top concern to hide them more effectively. Some of the early attempts to make these ships less detectable were to use distorted color schemes of white over blue and black. These tactics, however, weren't successful.

The military then decided to pursue a new tactic: _dazzling_ the ship. Dazzling consisted of painting the ships in black and white, zig-zag patterns. Such a pattern made it seem to enemy ships that the ship was headed in a completely different direction from its actual course.

Such a strategy worked similarly to how a two-dimensional optical illusion distorts our sense of space and reality — that is, by manipulating a viewer's perception of light, shadow and movement.

The technique was so effective that it fooled even highly trained Navy generals, and probably helped to reduce the loss of ships. 

Next, military officials sought to apply camouflage techniques to ground troops, weapons and machinery. The military dressed snipers in mud colors, covering them with nets and plants, so as to better blend in with their surroundings.

The military even used camouflage to hide entire sets of weaponry, by placing them under dirt-colored nets so that overhead planes wouldn't be able to detect them.

### 7. World War II brought the understanding that for camouflage, texture is more important than color. 

If you were walking through London in the early 1940s, you would've seen the city's red public buses partly painted in "camouflage" colors of khaki, brown and green.

Of course, such coloring made no sense for blending into a metropolitan city, where surrounding colors are more often gray, black and white — not the natural colors of a forest.

But in any case, a huge bus couldn't hide effectively with camouflage coloring alone. Something else was needed, but what exactly?

British zoologist Hugh Cott suggested that the military abandon its futile efforts of painting large objects and instead use texture to help these objects hide in plain sight.

A deeper understanding of air combat and reconnaissance missions showed that as planes fly at high altitudes, even objects painted in camouflage were detectable, as painted surfaces still reflected light.

Working with the military, Cott sought to shift their camouflage efforts from emphasizing color to using texture. He convinced the military to use camouflage tactics such as throwing leaf-covered nets over trucks, guns and missile installations in a forest.

Though the military initially resisted Cott's suggestions, his ideas proved effective. When military experts compared photos of color-camouflaged guns and texture-camouflaged ones, the results were unbelievable. While painted guns stood out, textured ones were nearly invisible.

You can guess where Cott got his inspiration — from nature, of course. In addition to countershading principles, he learned a lot about the importance of texture from animals who employ similar tactics for self-preservation in the wild. Some crabs species, for example, drape seaweed and leaves over themselves to look like plant residue on an ocean floor, to hide from hungry predators.

### 8. The science of military camouflage is more popular than ever. 

The military's trial-and-error approach yielded a variety of camouflage methods, some more successful than others. For a while after World War II, though, interest in camouflage waned.

The defeat of Nazi Germany signaled a period of healing for the traumas and wounds inflicted during the prolonged war. Knowing that camouflage research itself was a mixed bag, the US and British military placed much of their military camo research on the back burner.

In 1967, however, the Vietnam War triggered the United States to get back in the camouflage game. There was a need for effective camouflage, as American troops were fighting in jungles and forests against local militia groups far more familiar with the territory.

The US military at this time developed the so-called _tigerstripe_ pattern, bringing together narrow green, brown and black stripes that mimicked a tiger's fur in forest hues.

In the years since research and development in camouflage have become more extensive. Today there are entire facilities and university departments dedicated to finding cutting-edge ways to camouflage people or objects.

Interestingly, the rise of technology has created an ever-increasing need for _digital camouflage_. This means disguising important information and devices as something else, such as radars, heat monitors or and other technological applications.

The fast-paced nature of technology means that once forward-thinking concepts of camo uniforms or dazzled boats are antiquated at best. Such techniques are only marginally useful in today's world of night-vision glasses or scanners that can "see" the body heat of a camouflaged soldier.

Camouflage will continue to play a large role in military and scientific research, but it must evolve rapidly to keep up with the changing environment.

### 9. Final summary 

The key message in this book:

**In the fascinating world of mimicry and camouflage, nothing is as it seems. Butterflies, moths and other insects mimic each other to survive and hunt, and humans have also taken those tactics and applied them to art, science and war. There's still much that's unknown about the genetics behind species imitation — but it's clear that when done well, camouflage and mimicry can save the lives of entire species.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Greatest Show on Earth_** **by Richard Dawkins**

_The Greatest Show on Earth_ tackles the arguments made by the creationists who deny that evolution is a scientific fact, and shows their criticisms to be false.

Richard Dawkins provides the evidence that explains how the wide variety of life on Earth has developed through the process of evolution by natural selection, a process first discovered by Charles Darwin in 1859.
---

### Peter Forbes

Peter Forbes received the Warwick Prize for Writing for _Dazzled and Deceived_. He is also the author of _The Gecko's Foot_ and is a Royal Literary Fund fellow at St. George's University.

