---
id: 55409ef66233390007a10000
slug: dealing-with-china-en
published_date: 2015-05-01T00:00:00.000+00:00
author: Henry M. Paulson
title: Dealing with China
subtitle: An Insider Unmasks the New Economic Superpower
main_color: CD3329
text_color: 99261F
---

# Dealing with China

_An Insider Unmasks the New Economic Superpower_

**Henry M. Paulson**

_Dealing With China_ reveals China's journey to becoming the economic superpower it is today. These blinks explain the advantages and disadvantages of this rapid growth, and offer insights into how the US and China should work together to face today's global challenges.

---
### 1. What’s in it for me? Find out how China became an economic powerhouse, and what this means for the US. 

If you need a reminder of how quickly China has grown, consider this: When it started its economic rise, the internet was in its dial-up days. Yet in just over 30 years, China has modernized its economic sector and has become the second-largest economy in the world.

How did this happen? What does it mean for the rest of the world?

These blinks explain the steps China took to transform a mostly centralized, communist economy; how the US changed its communication strategy toward China; why we can't turn a blind eye to China's internal problems; and where China's meteoric rise encountered some speedbumps.

In these blinks, you'll learn

  * why Chinese universities couldn't produce good business managers;

  * how California's coastline relies on China; and

  * what China did to make their banks and oil companies more competitive in global markets.

### 2. China’s unrivalled economic growth is the result of sweeping reforms. 

If you'd have speculated in the late 1970s that China's economy would grow to be one of the largest in the world, very few would have believed you. Yet today, it's the reality. So what's the secret behind China's unexpected and unparalleled growth?

It all started with the introduction of western economic ideas. After Chairman Mao Zedong's death in 1976, Deng Xiaoping came to power and over the next two years, he developed several new economic initiatives. The purpose of these initiatives? To open China up to the global marketplace.

The results of these initiatives were extraordinary. Within a few years, hundreds of millions of Chinese had already been lifted out of poverty. By the early 1980s, China's GDP was increasing by 10% per year, on average.

At the core of this explosion in economic activity was one particular policy: providing _state-owned enterprises_, or SOEs, with more authority. Although they were still required to meet planned quotas set by the central government, SOEs were now allowed to sell their goods and services on the open market with flexible pricing.

Another crucial aspect of Xiaoping's economic plan was the creation of _special economic zones_ (SEZs). These served to kindle the dormant Chinese entrepreneurial spirit by granting foreign and Chinese companies lower tax rates, loosening import and export restrictions and providing easier access to foreign investment. Lenovo and the beverage company Hangzhou Wahaha Group were both founded in this period.

SEZs worked as economic laboratories where China could experiment with economic practices already common in the west, such as competition for construction contracts or incentive pay for workers.

Prior to these initiatives, bright and business-minded people could rarely put their abilities to good use in the jobs they were handed. But the reforms led to the creation of more companies, as it had become possible to start your own. Soon young entrepreneurs were popping up like mushrooms!

### 3. Restructuring the telecom industry was a critical step in the modernization of China’s economy. 

While these initiatives encouraged business in China, influential economic events around the world inspired Chinese business people too. One such event was the massive deregulation and privatization spearheaded by Margaret Thatcher in the United Kingdom between 1985 and 1990.

Privatization in China proved to be a godsend for slacking state-owned companies run by people with little knowledge of modern business practices and laden with debt by the mid-1990s. By selling shares of these companies to the public and international companies, capital was raised _and_ SOEs were improved, because they had to adopt global accounting standards.

The first sector to undergo privatization was the telecom sector, led by the SOE _China Telecom_.

Between 1992 and 1996, China spent more than $35 billion on telecom infrastructure, which saw the number of people with a fixed telephone line skyrocket from 11.5 to 55 million. But the system was by no means economically efficient. By the latter half of the 1990s, infrastructure spending was depleting more capital than the national telecom company could generate on its own.

Using the 1996 privatization of Deutsche Telekom, which raised over $14 billion, as the blueprint, China Telecom's privatization goal was to raise $2 billion. But the Chinese telecom industry was far more complicated than Germany's — it took over 350 full-time accountants to get an accurate grasp of the situation!

Nevertheless, when the company finally offered stocks to the public via an IPO in October 1997, it doubled expectations, raising over $4.2 billion. The successful privatization of China Telecom opened the door to competition in the Chinese telecom sector. By 2008, China had three large, competitive national carriers. In this way, the entire industry was privatized. Where a single telecom company had stood, there were now many that could compete against each other.

> China Telecom's listing code number on the Hong Kong Stock Exchange was 941. These numbers, as pronounced in Chinese, mean "survival in the midst of dangers."

### 4. The restructuring of China’s oil sector shows that the country’s economic ascent wasn’t entirely smooth. 

The privatization of China Telecom was wildly successful, so why stop there? The next step was oil, a sector also in desperate need of reform.

As the 1990s drew to a close, the Chinese oil sector was highly inefficient and begging to be restructured. Despite some previous attempts to improve matters, the China National Petroleum Company (CNPC) still lagged behind its Western competitors.

One of the CNPC's greatest challenges was the high cost of its employees. Before Xiaoping's reforms, Chinese workers were assigned to a workplace for life. Companies provided housing and healthcare, workers stayed with their company and the company never fired anyone. This was the case with most SOEs in China, and especially in the CNPC. By 1999 it had 1.5 million active workers, a staggering number compared to the 80,000 employed by oil giant BP at the time!

It comes as no surprise, then, that the restructuring process was highly complex and costly. While privatizing the telecom sector had its difficulties, turning CNPC into a competitive company on the global market was a whole different ball game.

For one thing, the macroeconomic situation was incredibly difficult to navigate; the recent Asian financial crisis had stifled oil demand, while oil prices around the world had sunk to their lowest levels since 1973.

When CNPC was finally introduced to the global market as PetroChina, about two-thirds of its workers were laid off in a push to make the company profitable and attractive to investors in the global market. The majority of the laid-off employees could not find new jobs and their severance was paltry. This led to massive protests in China and even in the US.

Unfortunately, these layoffs reflect a wider trend in China's economic modernization: In 2004 the International Monetary Fund estimated that the state-owned sector in China had shed over 40 million jobs between 1990 and 2001.

### 5. Reforming China’s banks and educational system made it more compatible with the global marketplace. 

Amid all of the sweeping changes in China, there was one glaring problem that couldn't be ignored. In terms of education and banking, China was totally incompatible with the global marketplace. So how was this addressed?

In the 1990s, Chinese universities had no problem producing fantastic engineers. But when it came to educating capable managers, they lagged far behind Western universities. Chinese Premier Zhu Rongji recognized this, and decided that business education at Tsinghua University needed to be reformed. He asked the author to evaluate the school, known as the "MIT of China," and propose a new executive program.

When the author attended Harvard Business School, there were never any right or wrong answers. You had to think for yourself, a necessary skill for any would-be manager. Bringing this attitude into teaching was the goal for the new executive program at Tsinghua. How? By shifting toward case study-based education and focusing less on theory.

In 2001, the first program, Managing in the Internet Age, was launched. Since then, over 50,000 have gone through Tsinghua's executive training programs.

The banking sector also required radical restructuring in order to adapt to a global marketplace. But how? At first, four state-owned banks were created to compete with one another and make the system more efficient.

However, things didn't quite go to plan. These banks started financing SOEs through unsound loans that soon spiraled out of control, leading to inflation and threatening the entire economy. Bad loans at the four banks just kept piling up and a solution was desperately needed.

The government was determined to clean up this mess. Through a series of restructurings and layoffs, China's biggest bank, ICBC, managed to dispose of $135 billion worth of bad loans in just six years, finally making it more competitive.

### 6. China needs broad political reform in order to build a sustainable economy. 

We saw in the first blink that massive economic change in China had an equally massive impact on the nation. Today, as a leader in the global marketplace, China has the power to influence the rest of the world too — and to quite a frightening degree!

One alarming development that could potentially have massive reverberations worldwide is China's debt. If rising debt is not reigned in, China might face an economic meltdown that would rock the world economy. The amount of debt in the Chinese economy rose from 130 percent of GDP in 2008 to 206 percent in 2014. What's more, the debt is actually growing much faster than the GDP; in other words, it's the perfect recipe for a crisis! In April 2014, the IMF made their concerns clear, urging China to take control of its ballooning credit levels.

How could China avoid such a meltdown? Well, the first step would be to delegate more authority to SOEs. Currently, the hiring and firing of executives in the SOEs is still controlled by the Communist Party — a political body. In order to handle debt more effectively, companies should really be operated commercially, thus allowing them to compete in the market.

Another area that calls for change is the environment. Chinese economic growth has had an enormous impact on the environment. Pollution in Beijing has reached levels far beyond what the U.S. Environmental Protection Agency considers hazardous. Groundwater reserves in the northern parts of the country are now almost depleted, while rivers and lakes are polluted to the point where the water is undrinkable. 

To combat environmental degradation, China must first invest in energy-efficient technology. This is no easy feat, so it is necessary for the US and other countries to assist China in its transition to a more sustainable economy.

The Paulson Institute, founded by the author, is dedicated to achieving this very goal. For instance, the Institute has established courses for Chinese mayors on how to achieve urban sustainability, and has created a plan to map the biodiversity of Chinese wetland areas to promote better environmental protection.

### 7. More effective communication strengthened the US-China relationship. 

In any good relationship, open communication is essential to solving complex problems. It's even more important between global superpowers.

And yet, in the early 2000s, dialogue between the US and China was quite unstable. So how did they improve things?

In 2006, President George W. Bush and Chinese President Hu Jintao agreed to launch the _Strategic Economic Dialogue_, or SED, a series of meetings designed to improve dialogue between China and the US on economic issues.

Prior to the SED, high-level Chinese officials met with different US cabinet members to discuss different issues. Since these cabinet members all had varying ideas as to what the overall aims of the US were, the Chinese government would receive conflicting and confusing messages. To ensure that members of the US Cabinet spoke with one voice, a position was created to coordinate cabinet members' communication with China. This top-down process suited the Chinese, who were used to strict hierarchies. Bush nominated the author to be the first person to fill the role.

The first Strategic Economic Dialogue was held in Beijing on December 14-15, 2006, and made important strides for the US-China relationship.

For example, the Chinese agreed to let the Nasdaq Stock Market and the New York Stock Exchange open business offices in China, and agreed to resume stalled negotiations on expanding US carrier flights to and from China. Another change facilitated financing to support US exports to China. Since trade between the two countries had long been a touchy topic, this marked a crucial fresh start.

### 8. The US and China must work together to confront global issues. 

Most people know that the US and China have had a strategic relationship for many years. Their strategic relationship goes back to the 1970s. The basis of the relationship is that the the US benefits from low-cost imports and China has profits from the American obsession with cheap consumer goods.

Now, as China has grown to be one of the world's largest economies, many Americans wonder: Why help a competitor?

Simple: because Chinese issues are global issues that affect the US too. A case in point is a 2014 National Academy of Sciences study, which concluded that up to a quarter of the sulfate pollution on the US West Coast is caused by Chinese manufacturers and carried by winds across the Pacific Ocean. It's clear that supporting China in its goal to become more sustainable is an investment in the future of the US as well.

Furthermore, a relationship of mutual investment between the two countries is mutually beneficial. As US investment grows in China, Chinese investment in the US is increasing too. Chinese investment in the US doubled from 2012-2013, reaching $14 billion across several sectors, from agribusiness to real estate.

Although some Americans dislike the idea that American companies should have foreign owners, they should consider the massive boosts in growth and employment that it creates. Take the Wanxiang Group, China's biggest maker of auto parts, as an example. With revenues of $23.5 billion, it employs about 6,000 Americans in 14 states. During the financial crisis, Wanxiang invested heavily in buying up struggling auto parts manufacturers, saving over 3,500 jobs.

So, looking into the future, it's clear that there's much to be gained by working with China, and not against it. Developing a solid relationship with China and promoting a better understanding of Chinese objectives is vital if we want to navigate the global challenges of the future.

### 9. Final summary 

The key message in this book:

**Through the introduction of western economic principles and sweeping reforms, and by opening up to the global marketplace, China has risen to become a new global superpower. In order to sustain this growth, China must ensure that it addresses rising debt and environmental issues. It's in the best interests of the US to work closely with China so that both nations can confront these global challenges together.**

****Suggested** **further** **reading: _Partners and Rivals_ by Wendy Dobson****

In _Partners_ _and_ _Rivals,_ Dobson lays bare the relationship between the two biggest powers in global politics: the United States and China. She describes the consequences of China's meteoric rise to power, and the inevitable tensions it has created. But she also offers advice that both the United States and China would do well to follow — for the good of the whole world. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Henry M. Paulson

Henry M. Paulson, Jr. is a former United States Secretary of the Treasury. He brings many years of experience in dealing with China, first as CEO of investment bank Goldman Sachs and later as Secretary of the Treasury during the presidency of George W. Bush. He is Chairman of the Paulson Institute, an independent think tank that promotes sustainable growth in the United States and China.

