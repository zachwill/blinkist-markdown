---
id: 5d2eed646cee070007a71812
slug: dear-ijeawele-or-a-feminist-manifesto-in-fifteen-suggestions-en
published_date: 2019-07-18T00:00:00.000+00:00
author: Chimamanda Ngozi Adichie
title: Dear Ijeawele, or A Feminist Manifesto in Fifteen Suggestions
subtitle: None
main_color: 78CBC4
text_color: 3C6663
---

# Dear Ijeawele, or A Feminist Manifesto in Fifteen Suggestions

_None_

**Chimamanda Ngozi Adichie**

_Dear Ijeawele_ (2017) is a series of suggestions for raising young girls to be strong, independent women. A few years ago, a childhood friend of author Chimamanda Ngozi Adichie asked her advice on a very important topic — how to raise her daughter to be a feminist. Her friend was called Ijeawele, and this book is the author's response.

---
### 1. What’s in it for me? Empower yourself, empower your daughter. 

_Dear_ _Ijeawele_ started as a letter of advice from the author to her childhood friend. Both women are Igbo — an ethnic group in southern Nigeria — and the author considers how to raise a daughter in the often misogynistic Igbo society, as well as the world in general. Even when discussing this particular group, the message is relevant to almost every culture in any part of the world.

Adichie makes 15 suggestions for raising a healthy, happy feminist daughter. As well as learning what you should be teaching young girls, you'll see how your own behavior makes a difference, and how to identify and modify your habits to foster a positive, empowering environment.

In these blinks, you'll learn

  * the downside of complete tolerance;

  * how a nickname can represent more than affection; and

  * that parenting as a father isn't about "helping" the mother.

### 2. You should be a full person, defined by more than just motherhood. 

Like it or not, our children will follow our example. How you see yourself will determine how your daughter sees herself. That's why it's important to remember you're a full human being — being a mother doesn't define you.

The most obvious way to do this is to reject the idea that you have to choose between motherhood and work. In many societies, people will tell you women shouldn't work because it's not how things are traditionally done. But the only thing that matters is that _you_ are happy with your decision. It's also worth noting that the "tradition" or history of women not working isn't that long. In some areas of Nigeria, trading was done exclusively by women, meaning Igbo families usually had two incomes. That only changed with British colonialism.

One great example of a woman who balanced work and family is American journalist Marlene Sanders. Not only was she the first woman to report on the Vietnam war from inside Vietnam — she did it while raising her son. She believed women should never have to apologize for working, and once advised a younger journalist that loving what you do is a gift to your child.

But what if you don't love your job? There are still plenty of other advantages to work, such as confidence and the sense of achievement that comes with earning an income. Most importantly, you should make an effort to remain a full person by making sure your needs are met and by pursuing your passions.

Of course, it's hard to take care of yourself when you're looking after a child — especially in the early weeks. You can't do everything. But what you can do is be kind to yourself and ask for help when you need it. One way to be kind is to allow yourself to fail. There's no one way to be a parent, and there is no shame in making mistakes. Nor is there shame in turning to friends, family or even the internet for advice.

Finally, you should never see yourself as a woman "doing it all." The idea of doing it all — work and parenting — is based on the sexist view that childcare and domestic work is inherently female. That said, this doesn't mean you can't be proud of what you do.

### 3. Committed parents raising a daughter together must reject gendered social norms. 

How many times have you heard people use biology as an excuse for the inequality between men and women? Well, this is nonsense. There's no biological reason for excusing men for certain behaviors.

Sadly, women often absorb harmful gender norms from a young age. The author gives an example of a woman she knows from the Yoruba tribe in Nigeria. She married an Igbo man. Her husband would only consider Igbo names for their children. The wife blindly accepted this, as she believed it was an unalterable fact that a child belongs to its father.

However, this makes no sense. If biology really informed social conventions, then children would be the property of their mothers. When a child is born, we can only be sure of the mother's identity. We have to take her word as to who the father is.

Not only is it illogical to make assumptions about parentage, but it's also dangerous. The author knows other Igbo women who've escaped bad marriages only to be denied access to their children because of the "fact" of paternal ownership. As two parents create a child together, the mother and father should share the parenting work equally.

There's only one area in which parents are biologically unequal. Fathers are unable to breastfeed. Despite this unique exception, mothers have been conditioned by society to take on the majority of the childrearing. Forced to become experts in looking after their child, mothers can become perfectionists. They may not mean to, but their high standards can diminish the agency of the father. For example, a mother might get very demanding about exactly how a diaper should be changed and criticize her partner when he doesn't follow her example.

Though you may love doing everything for your daughter, it's important to let her father care for her, too. He also loves her. This may mean refraining from correcting his efforts. Just remember — you're equal partners in bringing up your child.

Much of the language around childcare reinforces the idea that parenting is a woman's job. One of these is the language of "help," which you should avoid using. When a father looks after his son or daughter, we often say he's helping. But he's not helping, he's not being Mr. Mom, and he's certainly not babysitting. He's simply doing his job as a father and, as such, doesn't deserve special praise.

### 4. Gender roles are ridiculous and restrictive and should be rejected. 

Run like a girl. Throw like a girl. Cry like a girl. These expressions are common and make us feel that our gender dictates how we perform or react in certain situations. Or we get the idea that it limits what we are allowed to do. But gender should never limit us.

There's an idea that some behavior and activities are inherently female. We must teach our daughters to question and reject this. Your ability to cook, clean or raise children is not dependent on having a vagina, because domestic skills aren't intrinsically female. These are learned skills that anyone — male or female — can acquire.

By not placing limits on children based on their gender, they're free to achieve their full potential. Assigning gender roles begins very early, with the color-coding of baby clothes. The very idea that color has a gender — blue for boys, pink for girls — is ludicrous. Rather than organizing clothes by gender, it would make sense to arrange them by age and remove color as a designating quality.

There is a risk when we restrict young children with gender roles that we limit their potential. The author saw this first hand when she went to a mall in the US with a Nigerian woman and her seven-year-old daughter. The girl became fascinated with a remote-control helicopter toy, but when she asked her mother to buy it, she refused. The reason? The girl already had dolls to play with. This prompted the girl to ask if she was only allowed to play with dolls. The author still wonders today what impact that incident had on the little girl. If she'd been given the helicopter, would it have inspired her to explore how things work? Would she have become an engineer?

The word "girl" has many associations attached to it. So instead of seeing your daughter as a girl, you must see her as an individual first and foremost. Young girls are subject to rules that boys are not, such as being told to be pleasant and being rewarded for obedience. Rather than restraining her, encourage her to be active, to fix things herself and to be self-sufficient.

### 5. Feminism Lite is just a subtler expression of misogyny. 

Chances are, if you're committed to raising your daughter to be a strong, independent individual, you'll have done some research. You may have come across the idea of _Feminism Lite_. The author calls this "hollow" and "worthless" because it presents female equality as conditional. Slogans such as "The husband is the driver, but the wife is in the front seat" are typical mottos of women's pseudo-empowerment.

Feminism Lite has a habit of excusing or justifying sexism. The author sees the trend of Feminism Lite even amongst her closest friends. For example, take the author's friend Ikenga. He won't listen to arguments about misogyny. He explains it away and talks about "women's privilege." When he talks about his family, Ikenga says that, although his father is technically head of the household, behind the scenes it's his mother who's really in charge.

The author takes issue with the view that a woman is allowed to be powerful, but only if she disguises it 'behind the scenes'. It's an issue powerful women have long faced, and not just from proponents of Feminism Lite. Powerful women are disliked. We've been conditioned to believe that a powerful woman goes against the norm. She's therefore judged for everything she does in a way that a powerful man wouldn't be. She's also still held to the same sexist standards — judged by how agreeable she is, and how well she performs domestic tasks.

This goes to show that we're not uncomfortable with the idea of power itself — we're only uncomfortable with power held by women.

Another feature of Feminism Lite is that it perpetuates misogyny through the subtle use of language. One of the most visible forms of this is the _language of allowing_. One supposedly progressive British newspaper demonstrated this idea when it reported that Prime Minister Theresa May's husband was "taking a back seat" to allow his wife to shine. Imagine if the roles were reversed. Would a newspaper ever describe Theresa May as taking a back seat to allow her husband to shine? When two partners are equal in a marriage, there is no room for the word "allow."

### 6. You must question language, and teach your daughter to do the same. 

All our assumptions and beliefs about the world come out in the language we use. Teach your daughter to question what she hears. Children learn by example, so you must first think about your own use of language. It can be as simple as the pet name you choose for her. The author mentions a friend who refuses to call her daughter "princess," preferring the less-gendered "star" or "angel." The problem with the word "princess" is that it's strongly associated with clichés and assumptions about how girls should behave. Princesses are seen as delicate and passive.

Old sayings should be challenged, too. One example is an Igbo saying that people use to scold girls when they're acting immature: "Don't you know that you are old enough to find a husband?" The author doesn't advocate avoiding the phrase completely but suggests changing the word "husband" to "job," so that it doesn't reinforce the idea that marriage is something women should aspire to.

When you're talking to a child about feminist ideas, you should try to avoid jargon. Though words like "patriarchy" and "misogyny" make sense to adults, they're complex ideas to communicate to a child. Using everyday examples is the best way to make sexism clear. For example, point out that when a woman is told off for doing something that a man would get away with, it's a clear case of double standards — a sign of sexism.

You can also point out the difference between the way few people complain when men are angry, ambitious, loud, stubborn, cold and ruthless, whereas people frequently complain about women who have those traits. This way, your daughter will learn to notice the subtle misogyny in everyday language and recognize that it's just as damaging as overt misogyny.

You should also teach her to be aware of the language that indicates a woman is only important in relation to men. How many times have you heard men explain that we should care about victims of rape because they could be their own mother, daughter or sister? How many male politicians have you heard speak about championing or revering women? This is also patronizing language. Women are equal to men, not a special or superior species.

### 7. We must teach our daughters not to overvalue love and marriage. 

Love, marriage and babies. These are the things society says girls are supposed to want from life. Almost from birth, these expectations are imposed on them. However, this creates an imbalance. While girls are conditioned to aspire to love and marriage, boys are not brought up to see romance and matrimony as goals. The problem occurs when boys and girls grow up and want to marry — if the marriage matters much more to one partner, it's unequal from the very start.

Even the most powerful women are not exempt from being defined by their marriage. When Hillary Clinton ran for president in the US, the first word she used to describe herself in her Twitter profile was "wife." What was the first word her husband used in his Twitter profile? "Founder" — not "husband."

And that isn't the only disparity between society's treatment of married women and married men. Let's go back to Hillary Clinton. When Hillary Rodham married Bill Clinton, she chose to keep her last name. However, over time she began to use the name Clinton, ultimately adopting it completely. The rationale was to appease voters who didn't look kindly on a woman who hadn't taken her husband's name.

This imbalance starts even before the wedding day. Many cultures around the world believe a man should be the only one to propose. According to advocates of Feminism Lite, this is proof that women really hold the power, as the marriage can't happen without their consent. However, the author dismisses this as nonsense. When women have to wait for the man to initiate a change that will have a major impact on both their lives, it's obvious who is really in charge.

Finally, girls must see love itself as equal. Teach your daughter that she shouldn't only give love, but take it, too. One side effect of conditioning girls to overvalue love and marriage is the implication that girls must sacrifice part of themselves to attain them. Men then become central to women's lives, whereas we would never expect a man to make a woman the center of his.

### 8. Your daughter will need encouragement to develop a sense of self and her own identity. 

"Sugar and spice and all things nice" — that's what girls are made of, according to an old nursery rhyme. Apparently, the author's friend Chioma also thought that a girl's primary concern should be to please others. Chioma would pressure the author to modify her behavior to seem more likable.

When it comes to raising your daughter, though, teach her not to be too concerned with being likable. This is yet another example of the double standard we have for boys and girls. Boys are encouraged to be honest and express themselves, not to be mild-mannered and obedient. Not only is it unfair to be more lenient with boys' behavior — it's downright dangerous. When young girls are abused, they tend not to complain for fear of being seen as "not nice." Sexual predators often take advantage of this tendency.

You can teach your daughter about consent from an early age. For example, if another child takes one of her toys, make sure she knows she doesn't have to oblige them. Of course, kindness is important, but kindness can exist alongside honesty and courage. Teach your daughter to express her true thoughts and feelings.

As well as being female, there are other parts of your daughter's identity that you can help her incorporate into her character. Let her know that it's fine to embrace elements of her culture and let other parts go. In the Igbo culture, there's beauty and wisdom in the language, the proverbs, and the strong sense of community. Adichie advises her friend to teach her daughter to appreciate these without requiring her to accept the materialism and unfair gender expectations of the Igbo culture. 

As they grow up, girls are bombarded with representations of white beauty and accomplishment. As Adichie advises, it's important to counter these with images of African and black beauty, achievement and history. It's a sad probability that, as a black African woman, school will most likely not instill in her a sense of pride in her heritage.

Lastly, Adichie suggests that her friend can give her daughter an Igbo nickname, or a name from any other culture she belongs to. Not only will it kick start her imagination, but it will also give her a sense of pride to remember where she comes from. The author's aunt nicknamed her Ada Obodo Dike as a child, which means "Daughter of the Land of Warriors."

> "_Teach her that she is not merely an object to be liked or disliked, she is also a subject who can like or dislike_."

### 9. Engagement with a girl’s appearance should be deliberate and careful. 

Some girls are 'tomboys' and some girls like makeup. Whatever fashion choices your daughter makes, that's her decision, not yours. She may not be interested in how she looks at all. That's her choice, too.

There's a misconception that feminism and femininity are incompatible, but this is a misogynistic view. When you're raising a feminist daughter, there's no need to reject femininity. This includes so-called female interests, such as fashion and make-up. Women who like these things have an internalized sense of shame, whereas men are free to express their passion for 'male' pursuits, like sports, openly.

This double-standard also extends to appearance. You should never attach undue meaning to your daughter's appearance — you wouldn't do this to a son. Women often worry about an outfit or lipstick choice, fearing that their intellect or professionalism will be questioned because of it. No well-dressed man has to take that into consideration.

Most importantly, you should never link the way your daughter chooses to look and her morality. Of course, it's natural that you'll have an opinion about what she wears. By all means, tell her if you find an outfit unflattering. What you should never do is suggest that her short skirt is immoral.

You can't control the values your daughter is exposed to, either. It's up to you to make sure that she has positive examples in her own life. In the mainstream, certain kinds of beauty are seen as more desirable than others. Usually, white skin and straight, swishing hair are framed as beautiful. Children are smart — they will notice that such kinds of beauty are valued over others. You can counteract this message by showing her that other types of beauty are appreciated and introducing her to alternatives.

Think of the people you know who can set a great example. Surround her with 'aunties' and 'uncles' — not necessarily relatives, but people you admire who are examples of strong women and caring men.

Having examples of adults who challenge traditional gender roles will give her a powerful tool to counter gender expectations as she grows up. Say you introduce a man into her life who loves to cook. This sends a message that cooking isn't just a woman's job.

### 10. From early on, girls need tools for understanding and talking about sex and love. 

You can only really influence what your daughter encounters at home. Just as with beauty standards, when she's out in the world your daughter will come across some ludicrous ideas about love and sex. Just make sure you provide her with reliable information first.

The first step is to help your daughter understand all that sex involves, and that she is the only one who can make choices about her own sex life. There's a tendency to talk about sex only in terms of reproduction. Of course, it's important to talk about the physical consequences for women, such as infections and pregnancy, but she should also be aware of how beautiful and emotionally rewarding sex can be.

Try to think back to when you were young and were likely told that sex is only acceptable within marriage. The author remembers a particularly unusual school seminar on sexuality. Rather than an education, it was a series of unpleasant warnings. The girls were told that talking to boys was shameful and would lead to pregnancy. She saw through the hypocrisy, just as your daughter will see through the myth that sex should only be between married people. Above all, avoid any mention of virginity in relation to morality. The whole idea of virginity leads straight back to the misconception that sex is shameful. Tell her your own opinion — that waiting until she's an adult to have sex will be best for her — but accept that your daughter's body is hers alone.

As well as good practical knowledge, your daughter needs the right language to talk about sex with you, no matter what she chooses to do. As her mother, it's vital that she feels able to talk to you about anything. Experts in childhood development recommend that, even from a young age, you refer to female and male genitals as the vagina and the penis; their proper names. It's your decision to make, but whatever words you use should be free of shame.

This is the most important point of all. Menstruation, also, must never be associated with shame. You see it in every culture in the world — people speak about periods under their breath, feel ashamed about menstrual blood staining clothes, and act as if periods don't happen at all. But there's nothing more normal. Nobody would exist if periods didn't exist.

### 11. Your daughter will need to see the normality of difference in the world, without idealizing the oppressed. 

As your daughter grows up, she will find the world is full of difference. Rather than teach her to tolerate this difference, teach her to accept it for what it is. Difference tends to be something that is either celebrated or viewed with contempt. Your daughter should learn that difference is neither good nor bad — the world is simply diverse. If she expects to encounter difference, she will be able to cope with any situation. 

The key to acceptance is to respect the differences between herself and others, as long as those differences don't harm anybody. The earlier she recognizes that there will be much in life she doesn't know, the easier it will be to accept people as they come. It's also absolutely essential that she knows that difference is the norm. This knowledge will keep her humble, knowing that her experiences and those of others mustn't be universalized. But don't confuse this idea with being non-judgmental or lacking an opinion. She can still have her opinions — hopefully, well-informed, broad-minded opinions rooted in the reality that difference is normal.

When it comes to teaching her about oppression, try to do so without illusions. It can be tempting to paint all oppressed people as saints, but they aren't. You don't have to be a saint to be entitled to dignity and equality — even unkind and dishonest people have a right to these. The same goes for women. You needn't tell your daughter that all women are superior to men just because misogyny exists. Women are just as capable as men of good or bad behavior. After all, we're all human.

Your daughter may even meet women who don't like other women. Just as not all men are misogynists, not all women are feminists. Their behavior doesn't discredit feminism. It just goes to show how insidious patriarchy is. If this seems like a lot to take in, don't worry. You're not expected to dismantle the patriarchy on your own by raising a feminist daughter. What's important is that you try your best to do so.

### 12. Final summary 

The key message in these blinks:

**Trying to raise a feminist daughter isn't easy. You'll have to push back against all the misogynist assumptions so deeply entrenched in society. But by making a concerted effort, which begins with embodying feminist values yourself, your daughter will grow up with the tools she needs to be a healthy, empowered person.**

Actionable advice:

**Separate money and gender.**

Your daughter will hear strange ideas about money and gender from other people. She may be told that a woman's money belongs to her husband, or that it's the man's role to provide. Teach your daughter that, in an equal relationship, whichever partner _can_ provide should do so, regardless of gender.

**What to read next:** ** _We Should All Be Feminists_** **, by Chimamanda Ngozi Adichie**

_Dear Ijeawele_ isn't Adichie's first book on the subject of feminism. Now that you've learned the benefits of raising a feminist daughter, the best-selling prequel _We Should All Be Feminists_ (2014) explains what can be gained when everybody embraces feminism. Drawing on philosophy and personal anecdotes, the author expands on her much-praised TEDx talk. She covers the misconceptions people often have about feminism, how far we are from equality and how we might achieve it, so to find out more, check out our blinks to _We Should All Be Feminists_.
---

### Chimamanda Ngozi Adichie

Chimamanda Ngozi Adichie is a Nigerian author. Her previous books include _Purple Hibiscus, Half of a Yellow Sun_ and _Americanah._ In 2012, she gave the TEDx talk, "We Should all be Feminists." In 2014, this was published as an essay in a standalone volume. Adichie was awarded a MacArthur Genius Grant in 2008.

