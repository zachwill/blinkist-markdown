---
id: 548a1be03363370009850000
slug: death-of-the-liberal-class-en
published_date: 2014-12-26T00:00:00.000+00:00
author: Chris Hedges
title: Death Of The Liberal Class
subtitle: None
main_color: FCC756
text_color: 634F22
---

# Death Of The Liberal Class

_None_

**Chris Hedges**

_Death of the Liberal Class_ is a serious indictment of modern liberalism and today's liberal leaders. It offers a scathing critique of the failures of contemporary liberal institutions while still providing a glimmer of hope for the future of American democracy.

---
### 1. What’s in it for me? Discover how the American liberal class has destroyed itself. 

If you listen to conservative talk radio or watch television shows hosted by politically conservative personalities, you might be led to believe that the United States is plagued by a liberal menace. Every great U.S. institution, from universities to the government, is under the thrall of liberalism.

In reality, however, the complete opposite is true.

Far from being a growing force, liberalism is in decline in the United States. Once a large swathe of the general public adhered to liberal ideas, such as believing in liberty and equal opportunity for all, no matter how powerful or wealthy. This political belief is disappearing, and fast.

These blinks show why liberalism is dying a slow death, and importantly, highlights the awful consequences to come in the United States of a one-sided political and cultural system.

In the following blinks, you'll also discover:

  * why the American media isn't really free anymore;

  * why in society, no one is interested in "standing up for the little guy"; and

  * why the United States went to war against a noun.

### 2. The liberal class has failed to protect American workers from exploitation. 

The American political and social system is divided into two broad groups. 

There are _conservatives_, who stand up for business interests and encourage smaller government, and there are _liberals_, who stand up for the rights and welfare of everyday people in society.

In recent years, this divide has begun to look more and more lopsided with the decline in power and influence of the _liberal class_. This decline is most easily recognized by the group's recent failure to live up to their goals, namely: protect the rights of normal people.

Liberal politicians and policy makers have, for example, done next to nothing to protect workers against plummeting wages and the ever-growing practice of outsourcing jobs to developing countries, like China or India, resulting in an increase in poverty and unemployment.

To illustrate this, let's look at the life of Ernest Logan Bell as a typical case of the condition of the working classes in America.

The 25-year-old Marine Corps veteran has been unemployed for several years. He has little money and little hope of finding a good job, and thus feels increasingly alienated as well as abandoned and betrayed by government and the country he served.

Moreover, regulators and liberal politicians failed to curb financial speculation that put many working families' savings at risk. After the most recent financial crisis, the U.S. Congress had a strong mandate to create laws that would end risky trading and financial exploitation, in order to avoid another crash.

But the needed laws never came into existence, and penalties for banks were virtually toothless. As a result, working families are no less at risk of losing their homes or their savings than they were a few years ago.

Clearly, the liberal class has failed to stick to its own stated goals, protecting the people from exploitation and neglect. As you'll see, however, the problems of the liberal class extend well beyond this superficial analysis.

### 3. The liberal class has purged itself of radicals and unconventional thinkers to its detriment. 

The problems of the liberal class aren't just that it has failed to deliver on its promises. This political and social group also has internal problems, the most important being a complete lack of unconventional and critical thinkers within its ranks.

Dissidents and radicals are people who can identify social problems and find solutions. These individuals are vital if any political class wants to have and maintain influence in society.

Yet such dynamic people are virtually non-existent within today's liberal circles in America, a problem for which the liberal class itself is entirely to blame.

The gradual exclusion of radical personalities came in waves, during which controversial minds were shunned and expelled from liberal ranks.

In 1956, for example, unconventional liberal thinkers were blacklisted for voicing unpopular opinions, such as those that criticized the government. Such people were declared communists, expelled from television shows hosted by liberal personalities, and lost both jobs and funding from traditional liberal organizations, like universities and the media.

One such dissident, Sheldon Wolin, a political philosopher who attacked the rise of the corporate state, was declared a "communist sympathizer," a mark that would prevent him from finding a publisher willing to print his writings.

These expulsions were, on the whole, done quietly. Among the most common punishments for voicing unwelcome ideas was simply pulling support. This meant no more publishing or broadcast opportunities and no more invitations to speak at big events.

For some, such ostracization essentially meant an end to any significant livelihood.

The author himself is a good example of how the liberal class has punished dissidents among its ranks. After criticizing the participation of the United States in wars and general foreign policy objectives (for example, in a controversial speech held before the U.S. president and 1,000 guests), he was no longer allowed to write for _The_ _New York Times_ newspaper.

By excluding critical and unconventional thinkers, the liberal class has destroyed their chances to reform themselves and correct mistakes. Looking at today's political climate, the failure of the liberal class and the disappearance of liberal thought can be felt in many places.

### 4. The arts have abandoned liberal values; money has taken the place of political expression. 

In the past, institutions such as universities, media outlets and the U.S. Democratic Party were responsible for developing and propagating liberal ideas and projects. Indeed, there was a time when these institutions promoted important liberal values, such as freedom and equal rights.

The arts in particular is one area where the debate over liberal values was quite strong. Artists used their freedom of expression to create meaningful works, offering social commentary and promoting or condemning political ideas.

Artists were invited to speak at universities, and their highly critical works were put on display in art galleries and museums.

In 1968, for example, the artist Alan Magee, whose images and sculptures depicted the violence of war and physical abuse, could work freely in New York while also being part of the Philadelphia College Art Department. There, he and his students worked on critical and controversial works that reflected contemporary ideas and issues.

Today, however, political expression is almost entirely absent from the arts. Most artists don't focus on expressing personal feelings or creating something meaningful. Instead, they focus on making as much money as possible.

Students at larger art schools are taught only the norms and techniques of how to turn art into cash, and such schools no longer teach and encourage controversial or political theories.

In part, this change is because wealthy patrons who in the past have financed the work of artists aren't interested in critical or subversive politics. Instead, they are more interested in the aesthetic value of art, and since they pay the bills, they dictate what the market gets.

As a consequence, many artists have turned to mass producing their works to sell them in quantity. Individual paintings no longer are precious, but are produced simply to be sold.

### 5. The media is no longer free and independent, as corporations have taken over a majority of papers. 

The triumph of financial gain over liberal values hasn't been confined just to the art world, but has affected many other institutions as well.

The media is one such institution. Newspapers today, for instance, are no longer independently owned but are instead controlled mostly by huge corporations.

Consider that, in 1910, some 58 percent of all U.S. cities supported an independent newspaper, meaning that the paper was owned and controlled by local interests and not by a larger corporation that had no local presence.

Contrast this with the situation some 20 years later when 80 percent of U.S. cities had newspapers exclusively owned by large corporations.

It should come as no surprise that the media, now owned and controlled by corporations, represents the interests of its corporate ownership. Consequently, the media often targets — or ignores — groups whose agendas conflict with those of big business or with conservative values.

For instance, the media often targets people who hold pacifist ideals. Politicians who do not support U.S. involvement in warring conflicts, for example, are often branded as radicals, or accused of treasonous activity. Some accusations go even further, claiming such individuals are influenced by foreign governments. All because a politician advocates nonviolence!

Moreover, there is little to no media reporting on projects that espouse liberal ideas. Theaters that host controversial plays or musicals can expect to be covered only by local newspapers, and certainly not by nationally syndicated papers.

An example of such exclusion was witnessed as early as 1937, after the debut of _The Cradle Will Rock_, a musical that criticized a corrupt political system. Audiences loved the musical, but the press practically ignored it.

Corporate control doesn't stop with the media, however. As you'll see in the next blink, its tendrils reach much further.

### 6. We live in a world of total corporate control, where wars are endless because profits are high. 

We've seen how commercial interests exert influence on the arts and media, pushing their own agenda at the expense of others. But this is just one small piece of the larger puzzle.

In fact, our _entire world_ is under total corporate control, maintained through a state of permanent war.

The United States has fallen into a pattern of war after war, be it against real nations, such as Vietnam or Iraq, or merely against abstract threats, like the so-called War on Terror.

These wars can provide justification for insidious acts that wouldn't normally be permitted by society.

Consider, for example, that American citizens have been assassinated in targeted drone strikes — an act which obviously goes against an American citizen's right to a fair trial.

Yet because such individuals were allegedly "terrorists," that is, enemies in the War on Terror, these rights were deemed forfeit and the individual's assassination permitted.

What's more, a large and steady transfer of responsibility from official governmental institutions to private companies has given corporations greater control over how foreign policy is executed.

The coordination and planning of many military operations lies in the hands of private security firms and weapon manufacturers, such as Military Professional Resources Inc., rather than being the sole responsibility of government.

These companies unsurprisingly often have interests that diverge from government goals. One simple example of this is while governments ideally want to end wars, corporations often don't.

Governments lack a long-term interest in military operations, and thus want to end a war as quickly as possible. Weapons manufacturers, however, profit from conflict and therefore have a strong incentive to maintain activities as long as possible.

Consequently, they may act more aggressively and recklessly, or even engage in a misinformation campaign in order to raise the perceived need for new military gear and general spending.

So how is it that liberal institutions have been unable to reverse these frightening trends?

> _"The corporations that profit from permanent war need us to be afraid."_

### 7. Many liberals have blindly hoped that technology and a free market would bring about utopia. 

From roughly 1800 to 1930, the liberal class in the United States was strong. So what changed?

It would seem that the liberal class simply had too much faith in their ideals, and it was a misplaced faith, to boot.

The promise of _utopia_, an abstract perfect world, was a strong motivation for many liberal thinkers. They dreamed that, in this utopia, everyone would be rich, since globalization would spread wealth to all the people on earth.

They also believed that developments in science and technology, together with the transformative power of a free market, would lead to lasting improvements in living conditions and good health for everyone.

Liberals clung to these ideas even after it became clear that global wealth would not be spread equally, and that an unfettered free market could actually be destructive.

Compare this to the school of classical liberal thinkers preceding the twentieth century, individuals who were always skeptical and carefully scrutinized new ideas. Classical liberals were open to criticism and internal debates, and aware of our imperfect nature and the existence of evil.

Modern liberals have lost this awareness, believing instead that certain institutions could and would inevitably improve our quality of life. It's because of this blind faith that liberals failed to recognize the problems inherent to these institutions.

For example, while classical liberals were always hesitant to authorize or defend violence, modern liberals, such as President Woodrow Wilson, embraced war as a possible solution to achieve liberal goals as he brought the United States into World War I.

While having faith in humanity can be a good thing, optimism and faith in globalization and the free market has made the liberal class unable to recognize the real dangers of capitalism.

But that's only half of the story.

> _"We are just as uncritical of science as people many years ago were uncritical of God."_

### 8. Many of today’s liberals have been bought off with wages or promises of a luxurious life. 

Some reasons for liberalism's failure seem noble. While many liberals placed too much trust in the market, they really only wanted to raise the quality of life for everyone in society.

Another reason, however, is far more profane. Many liberals were simply bought off.

Indeed, many liberal thinkers have become extremely wealthy, and have no desire to change the system that gave them their wealth.

Institutions that were always a home to liberal thinkers, such as universities and churches, now offer salaries large enough to encourage liberals not bite the hand that feeds them by criticizing the system.

Professors at elite schools, such as Harvard University or Princeton University, can earn up to $180,000 per year. With an income like that, it's tempting to abandon advocating for reforms that could end the gravy train. They're also less likely to pass on liberal values to their students.

Of course, non-liberal institutions, such as corporations, are just as eager to buy off critics by offering huge rewards to those who remain on the sidelines and stay out of a corporation's way.

Labor union leaders, for instance, can earn huge salaries or even become junior partners at large companies, but only if they promise to keep their criticism of corporate interests to a minimum.

The perks of a cushy job might make it easy to stay quiet, even if you know that your fellow workers are being exploited.

Whether a consequence of blind faith, big wages or corporate promises, however, the result is the same: the liberal class has failed to protect us from corporate domination.

But what does this mean for the future?

### 9. The death of the liberal class can destabilize the entire democratic system. 

A future without a liberal class will prove to be a problem not only for the people it is supposed to protect but also for the integrity of the American democratic system as a whole.

Indeed, the government needs to have a liberal functioning class, as it acts as a safeguard against policies that are too harsh. A liberal bulwark too is often the last hope for those whom the government has wronged.

It was the liberal class, for example, that pushed reforms such as workers' rights, saving people from complete exploitation under an unfettered free market or despotic government.

A functioning liberal class also acts as an attack dog, battling radical movements that might wish to topple a government by instituting the moderate reforms that discredit more radical action.

The liberal class can claim, rightly or wrongly, that its policies can improve a social situation without the insecurity and chaos that can come with radical change.

Furthermore, Americans have become disappointed and restless without a functioning liberal class.

The failed liberal class in the United States provides no new meaningful reforms and is thus no longer a safeguard against governmental controls. Consequently, today's working class feels disappointed and angry.

We can also find examples throughout history of how the disappearance of a functioning liberal class has caused the collapse of entire governments.

At the end of the Weimar Republic in Germany, for example, the liberal class failed to satisfy the needs of its citizens, such as providing job security and a stable economy. With their needs unmet, the public turned to extremists on both sides of the political spectrum for support, which eventually allowed the Nazi party to rise significantly in power.

The failure of the liberal class weakens an entire political system, the consequences of which are much harsher and broader than you might initially think.

### 10. Society as we know it will collapse unless we institute the necessary liberal reforms. 

We've seen where the liberal class has failed. Now what will the death of the liberal class mean for society as a whole?

The future looks quite grim.

_Blind capitalism_, or the wholesale deregulation of markets made possible by a failed liberal class, is an undiscriminating destructive force.

Without limitations imposed on mass production or industrial growth, the ecosystem that supports our very existence has been brought to the brink of collapse. Climate change, spurred by unregulated production and consumption, will drastically alter the living conditions on earth.

Moreover, there is a revolution in our future, and one that will be born at the far-right end of the American political spectrum.

Revolt is necessary, as it is our only means of toppling corrupt government. However, this revolt won't come from the middle classes or be inspired by moderate politicians.

Looking back to Germany in the late 1920s, citizens then knew revolt was coming. Theirs too came from from the far right, as Adolf Hitler and the Nazi party used government weakness along with people's frustration to gain political power.

The only solution is a complete restructuring of human society into small communities, in which small groups of people can rebuild their lives.

These communities will have to be modest and self-sufficient, growing and building everything they need themselves. To live within their means, these groups can't be much larger than a few families, otherwise they risk becoming reliant on larger industrial producers.

Hopefully, these small communities will give birth to a new kind of political system, one that is not so easily corrupted by the power of money or wishful thinking.

### 11. Final summary 

The key message in this book:

**Modern society's liberal thinkers have failed in promoting and developing the ideals they supposedly hold dear: protecting the meek and regulating government. The consequences for this failure are far greater than dysfunctional government; indeed, they may be catastrophic.**

**Suggested further reading:** ** _Manufacturing Consent_** **by Edward S. Herman and Noam Chomsky**

_Manufacturing Consent_ takes a critical view of the mass media to ask why only a narrow range of opinions are favored whilst others are suppressed or ignored.

It formulates a propaganda model which shows how alternative and independent information is filtered out by various financial and political factors allowing the news agenda to be dominated by those working on behalf of the wealthy and powerful. Far from being a free press, the media in fact maintain our unequal and unfair society.
---

### Chris Hedges

Chris Hedges worked as a journalist for _The_ _New York Times_ as a foreign correspondent in Central America, the Middle East, Africa and the Balkans. He has also written two bestselling books, _Empire of Illusion_ and _War is a Force That Gives Us Meaning_.

