---
id: 578f504a11dc890003abcc1d
slug: deaths-summer-coat-en
published_date: 2016-07-22T00:00:00.000+00:00
author: Brandy Schillace
title: Death's Summer Coat
subtitle: What the History of Death and Dying Teaches Us About Life and Living
main_color: 6CA7AB
text_color: 4C7578
---

# Death's Summer Coat

_What the History of Death and Dying Teaches Us About Life and Living_

**Brandy Schillace**

_Death's Summer Coat_ (2016) is a peculiar and sometimes gruesome look at the history of a subject we don't like to think about: death. Learn about how death rituals and the medical profession affect our relationship with the deceased — and that defining death isn't as easy as one might think.

---
### 1. What’s in it for me? Take a not-so-deadly-serious look at death. 

All humans have two things in common: we all are born and we all will die. But these two events, the bookends of our lives, aren't treated equally. Birth is publicly celebrated; death, privately mourned.

However, it hasn't always been this way.

Throughout history, views on the dead and the dying have varied widely. Even today, different cultures have different takes on how to treat their dead. In these blinks, we take a look at the world's different views and customs and get a clearer view of how we cope with death and the dead.

In these blinks, you'll find out

  * what it means to have a "good death";

  * why modern medicine led to body-snatching; and

  * that we can learn about mourning from the Wari' tribe.

### 2. The human mind instinctively creates categories; death causes anxiety since it cannot be categorized. 

If you've experienced discrimination, you might have imagined how wonderfully different life would be if people didn't put everything they see into categories like race, age and gender.

But categorizing is something every human does from birth onward — even if born blind!

In 2009, a team of Harvard psychologists led by Alfonso Caramazza discovered that people born blind have the exact same neural pathways for building categories as those born with sight.

For those who can see, the brain's categorization works like this: upon seeing an object — like a dog or a hammer — different neural connections are formed between the eyes and a specific part of the brain that creates categories. Thus categorized, these objects are now recognizable to us as a friendly animal or a useful tool.

Amazingly, this function is such a basic part of the brain that people who are born blind have the exact same neural connections, despite having never used their eyes. This means our brain's capacity for category-building is already in place by the time we are born.

And since our brain is so primed for finding categories, things like death, which aren't easily categorized, can cause us anxiety.

Death is difficult to process; it is both a singular, final event as well as an ongoing process. So, even if we try not to think about death, we still have trouble escaping the knowledge that, as we get older, we are slowly dying.

Reminders abound. For instance, the dust that collects upon surfaces and swirls in the sunlight is primarily made up of dead skin that, but a week ago, was a living part of our body.

This reminds us that death, although an ending, is also part of being alive — a contradiction that is tough to comprehend, and one of the reasons we prefer not thinking about death while we are healthy.

> _"At what point does dying become death? The knowledge "I will die" becomes "I am dying" largely based on timing."_

### 3. The concept of a “good death” has been around for ages. But dying well requires time and preparation. 

Death is a difficult subject. If forced to think about death, people tend to envision an idealized deathbed scenario: they will die painlessly, in their sleep, surrounded by the people they love. Such ideal visions, however, differ from the age-old concept of a "good death."

People started striving for a "good death" in the eighth century BC, when hunter-gatherer societies gave way to agriculture and more communal ways of living.

Around this time, violent and sudden deaths became less common. People's lives were longer, which meant they could become prosperous and gain status in their community. People now had a reason to put their affairs in order before dying, and time to do it. This created a new standard for dying a good death.

A good death now meant taking care of moral responsibilities, such as deciding who would take over the estate or family business. It was also important, in these medieval times, to be spiritually prepared for dying, and the ailing or elderly usually spent their last years in a state of reflection and prayer.

In modern times, a good death still involves putting one's affairs in order.

For example, in 2014, CBS News reported on how psychotherapist John Hawkins dealt with his death.

In his last years, he wrote his autobiography and had a photographer document the final stages of his life. Eventually, Hawkins died at home, surrounded by his closest friends.

This certainly seems like a good death. Unfortunately, not everyone can afford such a peaceful and reflective end.

While 70 percent of Americans would like to experience this kind of good death, only 25 percent have the funds for such a luxury.

> _"The idea of a "good death," of dying well, is not something that happens to you, it is something you do."_

### 4. Death stopped being sacred in the nineteenth century when corpses were desecrated for profit and education. 

You might think that a coffin burial provides a decent amount of security. After all, one is sealed in a box and safely buried in the ground. There was a time, however, when this was far from a final resting place.

In the nineteenth century, more and more people started studying the medical sciences, and teachers of anatomy suddenly needed more cadavers for their students to dissect.

Some people were horrified that dead bodies would be removed from their resting place for such an appalling purpose. But science soon began to take precedence over religious morals, and, in 1832, the United Kingdom issued the Anatomy Act. This law allowed anatomy schools to use the unclaimed bodies of the deceased poor for teaching purposes.

However, there simply weren't enough cadavers to go around; grave robbing, or body-snatching, soon became a common practice among medical students and those looking to make some quick cash.

In the United States, the stealing of corpses had particularly racist overtones: black people were exhumed far more than white people. In the United Kingdom, however, bodies were primarily taken from the graves of poor white people. English body-snatchers did once manage to dig up a corpse from an affluent family, after which public outrage and protest reached a fever pitch.

Nonetheless, bodies kept getting snatched right up till the end of the nineteenth century.

Body snatching was a lucrative business. The profession even earned itself a moniker: _resurrectionist_.

Resurrectionists often operated as a team of three men who would dig up a coffin, break open its lid and pull out the body with the help of a rope coiled around the corpse's neck. The body would then be stripped naked, to make it as anonymous as possible, and taken away on a cart.

The gravesite would then be tidied up in an effort to cover up the nefarious business of the resurrectionists.

> _"What we see is a contest of sorts between the medical profession and the public over the sanctity of death."_

### 5. Modern medicine has blurred the lines between life and death, creating new ethical dilemmas. 

What defines death? Is it when your heart stops? Or is it when brain function stops? These days, science is constantly redefining the terms of death.

In particular, modern medicine has made it commonplace to artificially prolong one's life.

In 1947, heart specialist Claude Beck discovered _cardiopulmonary resuscitation_ (CPR), making it possible for patients to survive a cardiac arrest. This changed the prevailing definition of death: it was no longer the same thing as a stopped heart.

And then there are the ventilators that can keep a patient "alive" for many years while in an artificial coma — although, under these conditions, bodies eventually end up contaminated by infection.

Then, since heartbeat no longer differentiates the dead from the living, neurologist Robert Schwab proposed that patients could be considered dead when brain activity is no longer detectable.

However, since it's possible for patients to recover from brain failure, even this definition of death is being put into question. Doctors and relatives of comatose patients now face a difficult decision: choose death, or cling to the slight possibility of recovery, and prolong life.

These new uncertainties surrounding death have provoked ethical dilemmas and sparked some big conflicts.

In 2013, for instance, a 13-year-old Californian girl named Jahi McMath suffered a brain failure due to a series of complications following a tonsillectomy. Though Jahi was pronounced dead by the doctors, Jahi's parents disagreed and they went to court in order to keep her in an artificial coma. To this day, Jahi is being kept alive while the family and doctors continue to fight over the definition of death.

Some families, like those of Lia Lee, can even bring their comatose relatives back home.

The Lee's are Laotian refugees living in California, and they took care of their comatose daughter, Lia, for 22 years, pre-chewing her food in order to feed her. Only after Lia contracted pneumonia, which caused her heart to stop, did her parents consider her dead.

### 6. Some cultures ate their dead as a way of grieving, suggesting we could strengthen our bond with the deceased. 

Today, even if there is an open-casket funeral, most people will pay their respects by keeping a polite distance from the corpse. The funerary ceremonies of early societies, in contrast, were much more hands-on.

Some early cultures even practiced _necrophagy_, the ritualized eating of the deceased.

It might sound gruesome, but necrophagy was an important part of any funeral for the Wari', a tribe that lives in the Brazilian rainforest. The eating of the dead was a sign of both love and respect for the deceased, as well as a way for the living to deal with their own grief.

The mourners would circle the corpse and wail in order to fend off evil spirits. Afterward, the men ritually dismembered the corpse and cooked all the edible parts over a fire. But it was not the immediate family who would then eat the body but the distant relatives and extended family members.

To avoid offending the dead person's spirit, the members of the tribe made sure to entirely consume every piece of meat. Inedible parts of the body, such as bones and hair, were thrown into the fire so that, by the end of the ceremony, nothing remained. Among the Wari', this practice didn't end until the 1960s.

These early customs provide insight into how modern culture might benefit from a less detached relationship with the deceased.

For the Wari' tribe, necrophagy was an attempt to literally keep the deceased within the family. Even the ashes of the deceased are spread directly under the floor of the house where the family sleeps.

While no one is suggesting we start eating our dead, there are certainly ways we could have a stronger, more personal relationship with the dead — a relationship that might help people today find better ways to grieve.

> _"Ideally, all death rituals should serve equally to quell the rage and disperse the grief of our loved ones."_

### 7. Memento mori photography in Victorian England shows the value of grief and the ambiguity of death. 

_Memento mori_ is an old Latin phrase that means, "Remember you will die." It continues to remind people today that they are not immortal, but in nineteenth-century Victorian England it led to a whole new industry: memento mori photographs.

These are photos that portray a recently deceased loved one in a life-like posture and everyday setting, often propped up next to living family members.

These photographs became such a hot trend that Victorian people were prepared to pay a considerable amount of money for them. Grief was so important to them that it wasn't uncommon for these to be the only photos a family possessed.

At the time, cameras were heavy machines that used copper plates, mercury vapor and chemical crystals, all of which were very expensive. On top of that, producing a photograph took a considerable amount of time. All of this meant that few people could afford to have more than one photograph taken, so they chose to save up for a meaningful memento mori photo.

This trend also illustrates a growing ambiguity that the Victorians had in their relationship to death.

For instance, these old photos, showing a dead person in a very life-like pose, might seem eerie today; bodies appear to slumber peacefully, or to sit in an armchair or to stand, propped up by a wooden pole. The memento mori photos go out of their way to hide the presence of death.

In one photo, a small child appears to be sleeping peacefully on a bed. But upon a closer inspection, you can see that the photo is taken through a pane of glass, and on that glass is a barely visible sign that reads "smallpox quarantine."

What many of these photos really show is the human capacity for denial of death and efforts to somehow make our loved ones live on.

### 8. Emerging modern memorial traditions include life reenactment funerals and facebook memorials. 

Have you ever been to someone's funeral and thought, "Wow, I wish they could be here to see this amazing turn out." Well, you're not alone. Indeed, today's growing trends are making the departed the veritable life of the party.

Or at least that's the idea behind some eccentric new ceremonies, such as _life reenactment funerals_.

One New Orleans family made headlines in 2014 when they organized a funeral for their recently deceased mother, Miriam Burbank, and made sure she was in attendance. Miriam was dressed up, wearing sunglasses and sitting in a life-like pose at her own party. And her family also made sure she was surrounded by the things she loved: beer, cigarettes, music and a spinning disco ball.

Then there is Christopher Rivera's mortuary pose, which is perhaps even more striking.

For his funeral, the former boxer's corpse was placed standing just outside a boxing ring, as if he were about to begin another match.

Some might find these displays distasteful, but one could argue that they are more appropriate than the prevailing trend of friends and family displaying a general disregard for the bodies of the deceased.

As well, the internet has created new possibilities for how we might forever remember the dead.

In particular, Facebook memorials have become a huge trend over the past decade. Currently, there are 30 million profiles on Facebook that continue to run even though the owner's status has switched over to "Deceased."

In a twist that wouldn't be out of place in a sci-fi story, the dead can even post messages to their loved ones from beyond the grave, as long as they plan ahead.

Facebook now has an app called _If I Die_, which allows users to pen messages intended for loved ones to receive on special dates in the event of the user's death. While it might sound like a neat idea on the surface, keeping Facebook profiles "alive" can make it harder for those in mourning to accept the finality of their loved one's death.

### 9. Final summary 

The key message in this book:

**Human beings haven't always been so squeamish about death and corpses. From necrophagy to grave-digging body-snatchers, societies in the past were less afraid to look death in the face. Today's grievers could benefit from a healthier relationship with death and being less dismissive about the bodies of our deceased. By being in denial and leaving the entire process to the undertakers, it only makes death more painful.**

Actionable advice:

**Plan your funeral and that of your loved ones.**

Talk openly about death before it's too late. What kind of funeral would you like? What rituals would do you want your loved ones to perform in order to help ease the transition and allow them to express their grief? Ask your parents the same questions. It may save you a lot of uncertainty when the time comes.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Being Mortal_** **by Atul Gawande**

_Being Mortal_ (2014) helps the reader navigate and understand one of life's most sobering inevitabilities: death. In this book, you will learn about the successes and failures of modern society's approach to death and dying. You'll also learn how to confront death and, by doing so, how to make the most out of life.
---

### Brandy Schillace

Brandy Schillace is an interdisciplinary scholar specializing in medicine, history and literature. She works for the Dittrick Museum of Medical History and edits a medical anthropology journal. Her other publications include _Unnatural Reproductions and Monstrosity_ and _Hauntings: An Anthology_.

