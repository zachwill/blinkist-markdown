---
id: 54f444d5333035000a240000
slug: debt-en
published_date: 2015-03-02T00:00:00.000+00:00
author: David Graeber
title: Debt
subtitle: The First 5,000 Years
main_color: E0352D
text_color: AD2923
---

# Debt

_The First 5,000 Years_

**David Graeber**

Many of us assume that money — or capitalism in general — is a fundamental part of human society. In _Debt_, author David Graeber presents an anthropological examination of money that challenges common assumptions, asserting that money and the concept of debt are actually products of specific historical circumstances.

---
### 1. What’s in it for me? Learn about the origins of money and how its use has changed through history. 

The actual text of the famous Rosetta Stone is often overlooked, given the stone's importance in helping archaeologists crack Egyptian hieroglyphics. Yet this ancient rock is in fact a legal document, pronouncing amnesty for debtors and prisoners.

The concept of debt goes not only as far back as ancient Egypt, but even further, to the first agrarian societies. Our ancestors didn't live in simplistic bartering societies, as so many school books might have you believe; debt as a concept has been around as long as humans have had needs and goods.

Author David Graeber dials back the clock and takes a look at the origins of money, and how its use and utility has changed through history. Through peace and war, village life and city life, how we count coins and how we deal with debt has evolved tremendously.

These informative blinks, chock-full of fascinating facts and anecdotes, are a must-read for anyone curious about how we as a society have "done business" throughout the ages.

After reading these blinks, you'll know

  * how the concept of credit actually precedes printed coins or money;

  * why during wartime, soldiers paid in cash and family friends on credit; and

  * how religion played a part in keeping people out of debt.

### 2. Systems of debt and credit have existed since the beginning of recorded human history. 

Economic history textbooks tell us that the earliest human exchanges were based on a bartering system. Money was invented only to facilitate bartering; later, the idea of credit was introduced.

However, there's no actual evidence that bartering preceded money. In most modern, pre-industrial societies, bartering is only done among strangers.

The _Pukhtun_ of northern Pakistan, for example, are famous for their hospitality, but use a form of barter called _adal-badal_ for those to whom they have no social obligations.

This is somewhat intuitive. When a family member or close friend asks you for something, do you start scheming about how you can benefit from the transaction? Certainly not; but classical economics would have it otherwise.

Economic theory actually has it backwards. Anthropology teaches us that people were using virtual money — credit — far _before_ the inventions of coinage or bartering.

Virtual money was already in use in ancient Mesopotamia, around 3500 BCE. The silver _shekel_ functioned as the single, universal system of accounting, but most of the silver that underpinned this early monetary system was closely guarded in temple and palace treasuries.

Society also maintained an exchange rate between shekels and barley, the staple crop. Debts and taxes were usually charged in shekels, but paid in barley.

Despite anthropological evidence, the barter myth persists as it is central to the discipline of economics. It goes hand in hand with the assumption that all individuals are interested in maximizing profit, and that an individual is able to assign an abstract value to anything.

In reality, this tendency to see the world in terms of exchange value is not natural, but a product of a specific historical process.

### 3. Market economies rely on violence and the decontextualization of goods and people to function. 

Another common misconception about money has to do with its use. Money is actually more important _socially_ than it is economically.

Unlike the market economies we see in industrialized societies today, many pre-industrial societies functioned as _human economies_.

In human economies, each person is considered unique. This means a person can't be reduced to a mathematical value. Every individual is tangled in a complex web of social connections that can't be replicated, so each person has a particular value that can't be quantified.

In such societies, a suitor gives his bride's family money in the local currency, but that doesn't mean he's "buying" his wife. The value of the money given is in no way equal to the particular value of his bride. Instead, the money functions as a placeholder, acknowledging his social debt to the family.

It's only possible to treat people as if they're identical — and therefore exchangeable — if you rip a person violently out of their social web and essentially make them a commodity.

Consider the institution of slavery. Slaves were often prisoners of war who owed their lives to their captors, as they would've been killed otherwise. This absolute debt meant that all previous obligations — to friends, family or former societies — were negated.

Slaves, by definition, can be bought and sold. If a person is removed from his social context, he can be exchanged with other individuals who are thought to have the same "value."

Slavery is an extreme example, but the same process is at play when we exchange objects or money. We trade currency or goods based on abstract exchange values the coin or good has been assigned when they've been decontextualized.

The corn you buy in the market, for example, has no context, unlike the corn given to you by your grandmother. This decontextualization, interestingly, often comes about as a result of violence.

> _"If we have become a debt society, it is because the legacy of war, conquest and slavery has never completely gone away."_

### 4. Nickels, dimes and markets didn’t just happen; they’re tied directly to the interests of the state. 

As you might've guessed, there's more than just one way that violence and money are linked.

The theory of _chartalism_ argues that the emergence of the state is responsible for the development of money. It outlines another major reason in which violence has inspired the growth of our modern economic system: war.

Chartalists argue that states invented money to make conducting wars easier. Rulers need crucial materials such as metal and food to maintain their armies, but it's impractical to collect such items directly. Going door to door for war supplies would require yet another army to do so, and such a system could never be sustained.

Instead, states invented taxation. Markets then appeared alongside. Rulers distributed coins to their soldiers, who spent them either where they were stationed or where they were fighting. Rulers then demanded that every family within the state pay some fixed amount of coin back to the state.

Such a system turns a society into an efficient machine to continually build up a state army. When these new, valuable coins are introduced, people want to trade items for them. State theorists argue that this is how the concept of the market was born.

This sequence of events matches the historical record, as well. Ancient markets often developed right after organized armies appeared; or, markets emerged near palaces and military outposts.

The evidence suggests that there is an intimate relationship between markets and the state — and thus also markets and war.

And contrary to economic theory, markets certainly weren't inspired by inherent human tendencies.

### 5. A society wracked by war prefers its payments in cash; while credit relationships crave peace. 

Another interesting connection between money and war is one in which the prevalence of gold or silver as currency — that is, bullion — is determined by just how chaotic a society is.

Coinage developed independently yet almost simultaneously in three different places around 600 BCE to 500 BCE: on the Great Plain of northern China, in the Ganges river valley in India and in the lands surrounding the Aegean Sea.

Rulers in these regions encouraged their populations to use coins to facilitate everyday transactions either at home or when involved in trade abroad. This continued for some time when around 600 CE, a return to credit-based transactions proliferated.

What inspired the switch? Well, it seems that war craves bullion, while peace prefers credit.

During alternating periods in history, it appears that society either preferred offering people credit in business or wanted payment in gold or silver up front. The reason for this is straightforward.

Credit systems are complicated, and often built on trust. During periods of war, when many of your customers are itinerant soldiers, the complexities of a debt relationship would be something a savvy trader would want to avoid.

Yet cash was something a looting soldier had a lot of, and regardless of where the cash came from, it had a set value in society. Thus, merchants would demand payment for goods or service in bullion.

So in an era plagued by war or social instability, it was simply more practical for business to be done with bullion.

### 6. In the First Agrarian Empires (3500-800 BCE), most business was done using virtual money. 

Now that we've examined the origin of money as a concept, let's see how the relationship between coinage and virtual money (including debt) played out through history.

In ancient Mesopotamia as well as in India and in China, money was an abstract concept and the state bureaucracy controlled its value.

In Mesopotamia, for example, temples and palaces kept stores of silver ingot that provided the underpinnings of the state's monetary system. Since silver is a precious metal, however, it was abstracted into units instead of physically changing hands. The value of silver was used for calculating taxes and debts, but not for actually paying them. The people's physical currency was _barley_.

Thus if you owed a certain amount of silver in a debt, you would pay the debt with bushels of barley equal to the value of the silver.

Yet in local markets, most buying and selling seems to have been done on the basis of credit, another form of abstract value.

Inn and tavern workers, for example, kept running tabs for regular customers, and vendors built up lists of trustworthy customers. These debts would be collected at harvest time.

For major debts, debtors would give a creditor a clay tablet that detailed and promised a future payment. The tablet was physically broken when the amount was paid, the debt fulfilled.

Yet these tablets could also be used as a form of payment for _other_ debts. So if Person A had a tablet from Person B worth five shekels, they could use the tablet to pay a debt to Person C. Person B would then owe the five shekels to Person C.

People then developed lending money at interest, as a way to secure their payments if they were dealing with someone outside of the local community.

The practice was safer, say, than asking for a percentage share in profits, as there was no guarantee a traveling merchant would necessarily be honest about the money he made while on the road. In sum, levying interest was common when there was little ground for trust in a business relationship.

So interestingly, our "modern" systems of credit were already in place and utilized in the ancient world. With the onset of war, however, things radically changed.

### 7. The Axial Age (800 BCE-600 CE) saw the rise of coinage, which led to markets and modern religion. 

The Axial Age, coined as such by philosopher Karl Jaspers, was indeed an axial, or pivotal, time in history.

The great thinkers Pythagoras, Buddha and Confucius all lived in this era; yet it too was filled with violence, with war among states in China and the fall of the Roman empire.

The rise of organized armies and the chaos they inspired led to the development of coinage. Interestingly, coinage wasn't developed to facilitate trade, a common misconception. In fact, the Phoenicians, who based their empire on trade, were one of the last civilizations to adopt coinage.

The first coins appeared around 600 BCE in the kingdom of Lydia, in modern-day Anatolia, created specifically to pay mercenary soldiers.

The precious metals, such as gold and silver, used to mint the first coins seem to have been plundered from enemy palaces and temples, places typically where state treasures are stored. As armies returned home, their arms full of plunder, it seemed logical for rulers to pay the soldiers with the same loot.

What's more, paying soldiers in coins minted from gold or silver made more sense (and was more practical) than paying them in livestock, or issuing unreliable promissory notes.

While the development of coinage led to the creation of markets, it also potentially inspired the rise of some of the world's major religions, as Buddhism, Judaism and Christianity all emerged around this same time.

In the Axial Age, commercial interactions gradually became _materialistic_. Whereas goods were previously exchanged on the basis of abstract values such as debt and credit, exchange was now based on the physical exchange of precious metals, in the shape of a coin.

Many religions that took hold in this era supported beliefs that were in essence a reaction to this development. Believers imagined a spiritual world that would transcend the mundane interactions of material objects. Christianity, which divides the world into the carnal and the divine, offers one example of a religion addressing the changes in society.

### 8. In the Middle Ages (600 -1500 CE), virtual money made a comeback as markets and societies changed. 

In the Middle Ages, the tenets of religion and the practices of the market merged.

In the Middle East in particular, Islamic empires consolidated their lands and led a return to virtual money, in the form of debt or credit.

Interestingly, Islamic values began to influence how mercantile exchanges were carried out. Lenders stopped charging interest on loans, as the practice was banned in the Koran, for example.

United by a common religion, people found it easier to trust each other and thus merchants could extend credit to more people over a larger area.

The relationship between commerce and religion actually gave the state _less_ control over the market. In fact, the Middle East — and especially Persia — saw the first "free markets" that functioned independently of state control.

Europe too shifted from bullion to virtual money during the Middle Ages.

This was partly for practical reasons: after the collapse of the Roman Empire, precious metals were in short supply, thus people used instead currencies such as checks, tallies or paper money. Religious groups, such as the Knights Templar, increasingly regulated these alternative currencies.

In China, the state controlled the market until pressure from religious groups sparked a change. Buddhist monasteries demanded donations from surrounding communities as a show of devotion.

Eventually, donations began to actually deplete the bullion supply, which in turn inspired economic chaos. The state then cracked down on monasteries, to restore order in the markets.

Meanwhile, merchants in China began abandoning bullion in favor of storing capital in local banks and writing checks instead. The state initially tried to stop this practice, yet eventually decided to instead take control of the system by transforming checks into standardized paper money.

> Fact: **** Evidence suggests Adam Smith's concept of the "invisible hand" was taken from the writings of free market theorists in medieval Persia.

### 9. The Age of European Empires (1500-1971 CE) saw cash as king again, and the birth of capitalism. 

You might assume that a return to bullion was inspired by the discovery of the New World and the gold found there that was brought back to Europe.

The process actually was a bit more complex, and considerably more global.

The bullion run began again in China, where popular movements in the fifteenth and sixteenth centuries forced the government to abandon paper money and return to silver coinage.

Taxes were collected in silver, so the state sought to bring in as much silver as possible in an attempt to stifle the threat of mass uprisings.

Yet China's silver policy had major repercussions worldwide. The mining of metals in the New World wouldn't have reached the levels it did if it weren't for Chinese demand.

In fact, most of the galleons laden with New World metals either went straight to China or only passed through Europe on their way to China. In exchange for metals, China traded silk, porcelain and other precious goods. Bullion thus dominated global trade.

This new obsession with metals separated currency and religion, and money once again became integrated into the systems of the state.

The state worked to normalize monetary exchange by establishing legal codes. And crucially, the holding of debt came to be seen as negative, and defaulting on debt criminal. Debtors' prisons soon emerged.

Thus the more society looked down on debt, the weaker the bonds in society that made debt possible became. More and more, social interactions and thus market interactions were believed to be motivated by self-interest alone. Thomas Hobbes's _Leviathan_ is a poignant example of this theory.

And in this environment, the tenets of capitalism were born. Currency transformed previously context-based human relationships into impersonal, mathematical transactions motivated only by self-interest.

### 10. The Current Era (1971-today) has seen a return to virtual money, but with fewer controls. 

Our current monetary era can be said to have started when U.S. President Richard Nixon announced that the U.S. dollar would no longer be convertible to gold at a fixed rate.

The end of the "gold standard" ushered in a new era of virtual money. Money — seemingly no more than numbers on a screen — could be traded, invested, gained and lost.

Unlike previous eras of virtual money, however, our current era has very few controls for protecting people from falling into debt.

There are no _jubilees_ — state-sponsored debt forgiveness, like those that were held in the Levant in ancient times — and our economy isn't kept in check by religious moralities that look down on excessive interest, or help to prevent the burden of tremendous debt.

The most recent financial crisis stemmed from people borrowing too much, blinded by the illusion of value. The crash and its aftermath have highlighted the destructive potential of a virtual monetary system that lacks sufficient checks, whether by the state or society.

So what can we do? The first step is perhaps to examine the concept of debt outside the twin frameworks of the state and the market.

We need to understand that our negative views on and harsh enforcement of debt are purely historical phenomena, the product of the last few centuries.

We also need to recognize the absolutely crucial role that violence plays in how we understand money, markets and society itself.

Let's take a step back and ask: what would debt — and indeed, humans relations as a whole — look like if we freed ourselves from the legacy of violence that has sculpted the way we understand society?

What kind of world could we create?

> _"What is a debt, anyway? A debt is just the perversion of a promise."_

### 11. Final summary 

The key message in this book:

**Our current notions of debt as a calculable burden that we are morally obligated to repay developed from a historical legacy that was intimately connected to violence. When we examine debt through an anthropological lens, we see that debt does not have to be the prison it has become. As a society, we can escape this poisonous legacy to create a world in which the value put on living is greater than the value put on money.**

**Suggested further reading:** ** _The Ascent of Money_** **by** **Niall Ferguson**

_The Ascent of Money_ is an explanation of how different historical events led to the development of the current financial system.

It aims to show how, despite its proneness to crises and inequality, the financial system and money itself are drivers of human history and progress.
---

### David Graeber

David Graeber is an anthropologist, activist and professor at the London School of Economics. A prominent organizer in the social movement Occupy Wall Street, Graeber's work and writings on anthropology, anarchism and social justice have earned him many accolades.

