---
id: 530c937434623100087b0000
slug: decisive-en
published_date: 2014-02-25T08:00:00.000+00:00
author: Chip and Dan Heath
title: Decisive
subtitle: How to Make Better Choices in Life and Work
main_color: 91C640
text_color: 5A7A28
---

# Decisive

_How to Make Better Choices in Life and Work_

**Chip and Dan Heath**

The book identifies the main issues that typically stand in the way of decision making: a narrow view on our problems, short-term emotions, and overconfidence when it comes to predicting the future. It gives knowledgeable insight into how our decisions are formed and how to avoid making bad ones.

---
### 1. What is in it for me: making decisions is hard, let’s learn how to do it right. 

Making decisions is part of everything we do, and the better our choices, the better our lives, whether deciding between cappuccino or latte macchiato, or between two potential romantic partners.

In _Decisive_, brothers Chip and Dan Heath explain how in many situations our decision-making isn't ideal: we think too narrowly; we're biased by previous choices, personal values and short-term emotions; and we're overconfident about our decisions.

The book also illustrates how making poor decisions — or not making decisions at all — can have dramatic effects, as can be seen in the example of Kodak, the once-leading manufacturer of photographic film.

Kodak was aware that digital photography would likely usurp analog photography, as early as 1981. However, they remained indecisive about taking action. Eventually, by 2002, once sales of digital cameras surpassed those of analog ones, Kodak had been left behind.

The book answers the questions:

  * What alternative choices do I have and how can I find them?

  * How can I avoid making a decision simply based on what I like?

  * How can I stop my temporary emotions from standing in the way of a wise decision?

  * How can I be ready for the good and bad results stemming from my choices?

### 2. When you get stuck making a decision, don’t artificially limit your choices. 

We often think of our decisions as involving a choice between two options. Indeed, we get stuck much of the time trying to settle on whether or not to do a particular thing. But by thinking about decisions as a simple, binary choice, we usually fail to consider other alternatives. 

When you find yourself getting stuck like this, consider the whole array of alternative options available to you.

Consider teenagers. They often get stuck making decisions: Should I smoke cigarettes or not? Should I go to the party or not?

It's clear that these aren't decisions that consider alternatives among multiple options. They're just votes for or against a _single_ option. The decision to go to that party or not, for instance, could be made a lot easier if the teen would consider that they could also go to the movies, or watch a football game.

Another way to help you find your way out of a sticky decision is to consider the "opportunity cost" of your decision. In other words, what will you be giving up by making this choice?

Imagine you're stuck between buying a fancy stereo for $1000 or a basic, functional one for $700.

To consider the opportunity cost, think about what you'd do with the $300 you'll save when buying the cheaper one. For example: Would you rather have the one with a nice design and great sound? Or the merely functional one, plus 300$ worth of records?

Strangely, we tend to forget about opportunity costs. One study demonstrated that that when people were given a choice between buying a video that they liked for $14.99 or not buying it at all, only 25% didn't buy it. But when the wording of the negative choice also stated "_Keep_ _the_ _$14.99_ _for_ _other_ _purchases_," 45% didn't buy the video.

Since the choice itself remained the same in both cases, this example shows that just the faintest suggestion of another alternative is enough to improve our decision-making.

### 3. When problem-solving, don’t pursue just one idea; multitrack many options to find the best solution. 

We often try to solve a problem by implementing the one option that seems best to us. However, trying out just a couple more can yield much better results.

This is _multitracking_ — or, actively trying out several options simultaneously — and it can improve the decision-making process dramatically.

In one study, for example, two groups of graphic designers were tasked with making a banner advertisement for an online magazine. The first group created just one ad at a time, receiving feedback after each round. The second group, however, began their process with three ads, which received direct feedback, then they narrowed the choice down to two options. On the basis of the next feedback round, they arrived at a definitive result.

The ads of the second group were rated higher by magazine editors, independent ad execs and in real-world tests.

Why?

By working on several ideas simultaneously, the designers were able to directly compare the feedback for each round of work, and to combine those elements that were judged positively into a single ad design.

Not only can it result in higher-quality work, but, as studies show, exploring several alternatives at the same time actually speeds up the decision-making process.

One reason for this is that by having more alternatives, you're less invested in any single one, and therefore allow yourself to be more flexible in your opinion.

Another is that, when weighing multiple options, you always have a Plan B at hand. If Plan A fails, you have a fallback candidate.

However, you should beware of _choice_ _overload_, as too many options may paralyze your decision-making. For example, studies have shown that, when offering 6 kinds of jam for sampling on one day and 24 different kinds of jam on another, customers confronted with the choice of 6 jams were ten times more likely to eventually purchase a jar.

So add one or two options — not 24!

### 4. Look at the solutions someone else has found for the same problem. 

Sometimes it feels like we're the first to face a particular problem.

The fact is, however, that what you see as _your_ problem may have already been solved by someone else. You just don't notice it because their problem is slightly different.

When you find yourself wondering how to proceed with a certain problem, you should explore what other people are doing. For instance, if yours is a business problem, look at how your competitors handled it in order to find the best way forward.

Take, for example, Sam Walton, the founder of Walmart. In 1954, he took a 12-hour bus ride to see a new kind of checkout line at Ben Franklin variety stores — which led customers through one central line, rather than to separate counters for kitchen supplies, toiletries, groceries, etc.

Impressed, Walton immediately implemented it in his own stores. Indeed, throughout his career Walton kept an eye on what his competitors were doing, even admitting that almost everything he'd ever done had been copied from someone else.

Another technique is to search for analogies, as they are effective in helping us to think about specific problems in a more general way, leading us to solve them.

For example, consider the creation of Speedo's Fastskin Swimsuit.

Its designer, Fiona Fairhurst, created this suit by examining "anything that moves fast" — sharks, torpedoes, space shuttles, etc. She investigated the texture of sharkskin and the shape of torpedoes, finally arriving at a fabric that emulates key features of both: a roughness to reduce drag, and greater coverage of swimmers' skin, compressing the body into a torpedo-like shape.

Rather than thinking specifically about making a fast swimsuit, she thought generally about speed, which ultimately led to a swimsuit that offered such a competitive advantage that the fabric was banned in 2010.

As you can see, it pays to look into different areas to make yourself aware of solutions you probably wouldn't figure out on our own.

### 5. To shake off any bias in your decisions, play devil’s advocate and try and build a case against your decision. 

It seems logical that, when making decisions, we choose those options that we like the most. Unfortunately, however, these aren't always the best decisions.

But though our preferences might bias our decision-making process, there are actually a couple of ways to shake off the influence that our likes and dislikes have on us.

First, make it easy for someone to disagree with you. Consider what would have to be true to make your least favorite option, or just the other options, the best choice.

By doing this, you're not arguing for or against personal preferences, but instead analyzing the logical constraints of the options and allowing for disagreement without generating antagonism.

If you can't do this yourself, allow someone else to play devil's advocate, and make a convincing case against what you (and maybe everyone else) believe to be the best option.

Second, ask disconfirming questions to surface opposing information.

Say, for instance, that you're offered a job at a big law firm. The pay is good, their team seems to lead balanced lives and it's a secure position in an established company.

When meeting lawyers from the firm, instead of asking whether they are happy with their jobs or whether they have a life outside work, ask how often they had dinner at home, at which time, whether they worked afterwards. How many lawyers were hired in the past five years and how many are still at the firm? How many secretaries has the CEO had in the last few years?

Now that we've found some good ways to question our own decisions, another option is to look at how a decision of ours has worked out for _other_ people.

### 6. Think about how your situation looks from the outside. 

Although we often believe our situation is unique, it benefits us to look at how others in a similar situation have fared. We're usually more alike than we tend to believe.

You can do this by finding out how a situation like yours usually unfolds. Consider Jack, for example, a brilliant Thai Cook who is considering starting a restaurant. He's found what he sees as the perfect location on 4th Street, with huge foot traffic and other Thai restaurants close by. This is his _inside_ _view_.

The _outside_ _view_ is concerned with data revealing how other people in similar situations got along. In this case, the outside view can be found in the _base_ _rates_, showing 60 percent of restaurants failing within three years of opening.

This doesn't mean Jack shouldn't open a restaurant, but it's an important aspect to consider.

Second, while it seems obvious to ask experts for predictions about your situation, you should look to them for base rates only. This is because they may also slip into the inside view when presented with an individual situation; they, too, might end up neglecting base rates.

So ask them indicative rather than predictive questions. For example, ask a lawyer: "How many cases similar to mine were settled before going to trial?" rather than "_Will_ this case get settled before going to trial?"

But make sure not to let yourself take even informed opinions at face value. Instead, always be certain to take a closer look.

For example, imagine you are a Thai food lover searching online for a truly excellent restaurant, but the best you can find is one with a 3.5 rating (out of 5).

Looking closely at the reviews from which the rating derives, you see that many people found it expensive but thought the food was outstanding.

Since you have no issue with the price, the base rate of the restaurant experiences (the rating) lacked the specific information relevant to your situation.

### 7. Rather than make a plan, run a small experiment to see if your idea works. 

In many situations it's wise to dip your toe in the water, rather than dive in headfirst. This process of testing ideas on a small scale is called _ooching_.

Deciding upon something based on our _belief_ that it will or won't work is a bad strategy compared to trying it on a smaller scale.

For example, taking an internship is a good idea because it's the fastest way to discover whether or not we want to pursue a certain profession.

Imagine your daughter told you she wanted to enroll in law school, because she believes that being a lawyer will involve reasonable hours and pay an attractive salary. Naturally, you're not convinced: she can't base her decision on such weak evidence. But if she'd already completed an internship at a law firm, you might be convinced.

Furthermore, we're bad at predicting the future. So, rather than guess, we should test.

For example, many firms hire their staff based on interviews, because they are victim to the _interview illusion_ : the belief that they've taken good measure of the interviewee. The best option is to ooch, by giving potential staff a trial run.

But we don't always have the option to ooch. Some situations will require our full commitment from the outset. For instance, we can't temporarily quit our football team just to see how it feels. Doing so would violate our obligation to our teammates. And no one should go to university just to see if it suits them. Rather, we should try to find out beforehand whether we like the subject we plan on studying and only _then_ commit to it.

Instead of mulling endlessly over whether you should commit to a certain option, it is a good idea to first give it a try on a smaller scale.

### 8. To get some perspective on your decision, shift your focus to the future 

Many of our choices are hijacked by what we believe is important in the moment that we're faced with them. However, since these are often bad in the long run, here are some techniques for encouraging our brains to consider long-term consequences.

The first is to find emotional distance by imagining the outcomes from a future perspective.

This is because present emotions are often very clear and precise, while future emotions are not yet well-defined. Salesmen often exploit this — they try to excite us so that we'll make a purchase based on that short-term emotion.

One simple, effective method to equalize their influence is called 10/10/10: Actively consider future emotions by asking yourself how you would feel about your decision 10 minutes, 10 months, and 10 years from now.

The second technique is to take the observer's perspective. Imagine you're considering calling a guy you like. You know him from one of your classes but haven't really talked that much to him, so you're afraid he won't remember you.

So, you must decide between either:

(a) not calling him until you know him better, or

(b) calling him.

Most people choose (a) for themselves but suggest (b) to a friend, because short-term emotions — for example, fear of rejection — are not as present when it comes to the decisions others have to make.

By looking at your decision from an observer's perspective — from a distance — the most important aspects will seem obvious to you. Often, these aren't even the ones you are preoccupied with in the thick of the moment.

In the end, one of the best questions to ask ourselves when facing a tough dilemma may be: What would I recommend that my best friend should do?

### 9. When your decisions are based on conflicting values, identify your core priorities. 

Sometimes our decisions aren't distracted by short-term emotions, but by an unclear order of priorities in our lives. For instance, even after your initial excitement over a job offer has vanished, you may find yourself stuck in the decision of whether to take it or to stay at your old firm.

The solution here is to find your core priorities. Ask yourself: "Which long-term emotional values, goals or aspirations are most important to me?"

Should you stick with your old job, which gives you enough money and time to go on holiday with you family and see your friends? Or should you take the new job, which will give you more responsibility and better pay, but also longer working hours?

Maybe your children are young and you want to prioritize your family. Or perhaps your children are fully grown and independent, and you're looking for a new challenge. Either way, it should be clear that stating your core priorities in this way will help you to make a decision.

Once you've found your core priorities, you must commit yourself to acting on them. We only have a limited time in our lives, so spending more time on core priorities cannot be achieved without limiting the time we spend on other things.

Of course, we'd like to think we can make time for everything by multitasking, but the truth is that when, for example, someone decides to prioritize exercise over reading, because they prefer fitness over knowledge, they'll need to commit to a regular training schedule.

You need to ask yourself, what are you prepared to give up so you'll have more time to spend on your core priorities?

For example, think of your calendar as the ultimate scoreboard for your priorities. If time spent on an activity indicates your preference of it, what would your core priorities be? Browsing the internet for job opportunities (career) or spending time with your children (family)?

### 10. To prepare for the consequences of your decisions, think of the future as a range rather than a point. 

Often we imagine one possible consequence of our actions and base our decisions on that one specific idea of how the future will unfold — even though we have no way of actually predicting the future!

To counter this foolish tendency, you should, firstly, consider both the worst and best possible outcomes, as this will allow you to estimate where you are at a given point, and react when reality moves closer to the worst outcome. To do this, you can use prospective hindsight — the notion that we can cognitively evaluate facts better than possibilities — to your advantage. Ask yourself, "It is one year from now, and we've failed. Why?", rather than "What could happen that would lead to failure?"

But you should also make sure that you're able to handle any possible success. For example, if you plan to launch a new product, do you have the capabilities of meeting the demand should it become suddenly successful and popular?

Secondly, you should implement a safety factor to prepare for unforeseeable circumstances.

Consider the elevator: surprisingly, its cables are made eleven times stronger than needed for it to function. Engineers calculate how much weight it will transport, what strain will be put on the cables and which cables could carry that weight — which they then multiplied by eleven.

We tend to be overconfident in our knowledge and expertise, so it's a good idea to adjust our predictions based on that fact.

So, when you schedule your next project, consider on the one hand the importance of meeting the deadline and, on the other, the time that you expect the project to take, and add a safety factor for your own benefit. For example, if you think you can design a webpage in three days and it needs to be done by then, add half a day just to be sure.

### 11. Set a tripwire to shift from autopilot to manual control and enforce a decision. 

When we do a certain thing every day we often don't notice the gradual changes that, over time, can amount to a drastic situation. Hence, we need a figurative "tripwire": a signal that makes us aware of our behavior, and, if necessary, prompts us to correct it.

One way to do that is to establish clear signals to interrupt any "autopilot behavior."

For example, the American shoe seller Zappos offers their employees $4000 to quit. Once they feel they don't like working there anymore, they're encouraged to take the money and leave.

This is a tripwire to help unmotivated staff see their situation clearly. It interrupts indecisive behavior that's based on habit, and prompts conscious decision-making. (Bonus: It also gets rid of underperforming staff.)

Another method is to set deadlines and partitions to keep yourself from falling into bad habits. Deadlines help us enforce a decision that we'd otherwise procrastinate on. For example, one study offered college students $5 to fill out a survey. When given a five-day deadline to complete the survey, 66% collected the money, but without a deadline only 25% collected it.

Partitions work the same way. For instance, instead of handing out one huge sum, large investments are distributed by dispensing smaller sums over time, prompting conscious attention. Each round of partial investment serves as a tripwire to make sure that everything is going the right way.

The final method is to use labels to recognize disturbing (or encouraging) patterns.

For example, pilots, in their training, are introduced to the concept of _leemers_, which describes "the vague feeling that something isn't right, even if it's not clear why." Having a name for this feeling means the pilots are less likely to ignore their feelings.

As you can see, when what's at stake are the lives of many people, even a nagging feeling can act as a tripwire prompting you to pay conscious attention to the situation.

### 12. Final summary 

The key message in this book:

**When you make a decision, follow the WRAP-process: Widen your Options, Reality-test your assumptions, Attain distance before deciding, and Prepare to be wrong.**

Actionable ideas from the book:

**Set a concrete time**

Next time you want to exercise, clear a specific time slot in your organizer for it. This will make you aware of your priorities (exercise) and ensure that it doesn't fall prey to something more urgent (e.g., doing the dishes).

**Find the alternatives**

If you are wondering whether you should go to a party or not, come up with at least two alternatives, e.g., not go to the party and watch a movie, or go to bed early and go running the next morning. This way you can estimate more truthfully what other options you have.
---

### Chip and Dan Heath

Brothers Dan Heath (Senior Fellow at Duke University, supporting social entrepreneurs) and Chip Heath (Professor in the Graduate School of Business at Stanford University) are the authors of international bestsellers _Switch_ and _Made to Stick_.

