---
id: 596caa5fb238e1000517dcd8
slug: deep-nutrition-en
published_date: 2017-07-21T00:00:00.000+00:00
author: Catherine Shanahan, M.D., Luke Shanahan
title: Deep Nutrition
subtitle: Why Your Genes Need Traditional Food
main_color: 2547B0
text_color: 2547B0
---

# Deep Nutrition

_Why Your Genes Need Traditional Food_

**Catherine Shanahan, M.D., Luke Shanahan**

_Deep Nutrition_ (2008) is about modern diets and how they're making people sick. These blinks explain the danger of industrially produced food, what it's doing to our bodies and how we can return to an earlier way of eating that will keep us healthier for years to come.

---
### 1. What’s in it for me? Boost your health through simple and traditional food. 

Have you ever had a diet phase in which you were obsessed with those low-calorie, low-fat, pre-packaged "health" foods, like breakfast bars? Did the diet succeed? Probably not.

Now try to think — were such foods around when your grandparents were growing up? Of course not, but health and nutrition are not improving today, despite these innovations. Industrially made, processed foods like those breakfast bars contain a huge amount of sugar and are far removed from the natural, whole foods eaten by people back in the day.

These blinks explain how we can overcome modern eating habits and return society to a healthier state.

In these blinks, you'll learn:

  * why although the average lifespan is lengthening, this might be misleading;

  * how vegetable oil is not as healthy as vegetables; and

  * the mechanism that makes fresh foods more nourishing than processed.

### 2. Despite incredible developments in medicine, our health is declining. 

Our grandparents' generation is living longer than just about every generation before them, but, unfortunately, that doesn't mean _we_ can count on such longevity.

While the octogenarians of today benefited from breakthroughs in modern medicine such as the development of antibiotics, the health of younger generations is not maintaining this upward trend. Rather, people today are experiencing age-related medical issues from a younger age than their parents and grandparents.

For instance, through her practice, Catherine, one of the authors, encounters people as young as 40 who are already mired in cardiovascular issues and joint problems. The parents of these patients didn't have to deal with such ailments until they were _much_ older.

So, why is this happening?

In large part, it's because prior generations ate more healthily. Their diets consisted of more natural foods and they had fewer of the processed options available to us today. Fundamentally, because of the industrialization of food production, and a misplaced fear of saturated fat and cholesterol, we've slowly replaced natural foods, like eggs, cream and liver, with processed, nutrient-poor options that are high in sugar and other detrimental components.

A good example is the way we've replaced butter, which is loaded with beneficial fats, with margarine made out of artificial _trans fats_. These trans fats have been linked to a variety of health issues like atherosclerosis.

Furthermore, nowadays we put more faith in artificially produced vitamins and supplements for our health, than in getting these necessary substances from beneficial food. This is not surprising, given that medical schools don't teach their students a single useful thing about nutrition.

Rather than learning how to identify the cause of problems, prospective physicians are simply taught to treat issues as they arise. Because of this, lots of doctors don't pay attention to the power of nutrition and are quick to recommend artificial vitamin supplements instead of nutritive food.

But the good news is that, by eating foods that people ate in the past and skipping the industrial food products of modern times, you can treat the very root of your wellbeing problems. In the next blink, you'll learn how.

> _In 1830 the percentage of Americans who were 100 or older was 0.02 percent, in 1990 it was 0.015% percent. The expectations for today are 0.001 percent._

### 3. Traditional eating habits were developed for health and for a long life. 

Today, many countries are absolutely overrun by illnesses from cancer to heart disease. But while these medical issues represent an absolute epidemic among certain groups, there are others who hardly suffer from them at all.

Traditional cultures don't have to deal with diseases like cancer since they know and apply the power of food. Just take an example from the beginning of the twentieth century, when explorers discovered that among the Hunza, a partially nomadic tribe of Afghanistan, cancer was non-existent, nobody needed glasses and it was not uncommon for tribal members to live beyond the age of 100.

Learning of these incredible facts, a dentist from Cleveland, Ohio named Weston A. Price, set out to uncover the secrets of these super healthy humans and found that they simply applied traditional methods to grow extraordinarily nutritious foods.

His research found that the diets of these tribal members often contained ten times more vitamins and 1.5 to 50 times more minerals than those of your typical Americans.

So, nutritious food is key, but traditional cultures rely on another piece of health knowledge; they know that wellbeing begins with pregnancy. As a result, such cultures feed expectant parents a highly nutritive diet.

This makes perfect sense since pregnancy is extremely taxing on the mother and, if she isn't properly nourished, both her wellbeing and that of her unborn child could suffer.

Just take the Maasai, a hunter tribe of Eastern Africa. They still have a tradition in which, for several months before they marry and conceive a child, couples drink incredibly nutritious milk produced during the rainy season, when the grass the cows eat is lusher.

Conversely, eating a typical modern diet that's loaded with sugar and fried foods will have a profoundly negative effect on the health of a child. That's why mothers today need prenatal vitamins that partially correct their deficient diets.

### 4. Your brain has a natural antioxidant system, but vegetable oils disrupt it. 

Everybody knows that vegetables are good for your health. But vegetable _oil_ is a different story, and the unhealthy nature of this common food product affects your brain. Here's how.

Your brain is threatened by _oxidation_ especially in the form of _free radicals._ Free radicals are molecules that are missing an electron and are therefore incredibly reactive as they constantly attempt to pull electrons from surrounding molecules. As these molecules seek positively charged particles, they transform otherwise functional molecules into toxic ones that can damage cells, causing inflammation and disease.

Free radicals are especially dangerous to the brain because the brain is composed of 30 percent _polyunsaturated fats_ or _PUFAs_, which are heat-sensitive, unstable lipids that produce trans fats when heated up. These structures are extremely vulnerable to oxidation.

Luckily, however, the brain has a natural defense mechanism against these threats. It uses two basic types of antioxidants to neutralize cell-damaging free radicals. There are _enzymatic antioxidants_, which are produced by your body, and _diet-derived antioxidants_, which you take in through food.

So, what does this all have to do with vegetable oil?

Well, regular old vegetable oils like canola are made of the same PUFAs as your brain, and this means that they contain huge amounts of trans fats.

The trans fats in vegetable oil wreak havoc with your brain's antioxidant delivery system.

Processed vegetable oils are a relatively new addition to the human diet, and we simply haven't adapted to them. As a result, the brain can't reject them. This means these unstable PUFAs and trans fats are free to use up the antioxidants of your brain's defense system before the antioxidants even reach the brain itself. The brain then sees them as natural fats and accepts them, along with their free radicals, which proceed to damage brain cells.

### 5. Sugar is addictive, damages your brain and is in just about everything. 

What do you think is the most dangerous drug in the world? Heroin? Cocaine? Well, sugar should be on the list as well.

This common food additive is more addictive than cocaine and causes cellular brain damage. Now you're probably thinking, _how is that possible_?

Well, your brain has receptors for different substances, one of which is responsible for sweetness. The problem is that these receptors were formed at a time in human history when people had practically zero access to sugar.

So, modern overexposure to sugar can easily overload the brain, leading to addiction. A French study from 2007 tested cocaine and sugar on rats to determine which was more addictive. Shockingly enough, sugar was the drug of choice for the brains of these rodents.

But sugar's habit-forming qualities aren't the only problem. It also damages the cells of your brain.

A robust brain cell looks like a tree with tons of branches. Hormones regulate the growth of these branches, which are really neural connections. The loss of these connections can result in dementia, and sugar leads to just that situation by disrupting your hormones.

Sugar is bad news, and our food is loaded with it.

In part, this is due to the craze that led to the proliferation of low-fat products, which rely on added sugar to make them tasty. Just take _Pediasure_, a drink for children that's recommended by pediatricians as a milk replacement. A liter of the stuff contains a whopping 108 grams of sugar, which is many times the amount in a liter of whole milk.

And to make matters worse, food companies give sugar lots of confusing names to hide ever larger quantities of this cheap, addictive ingredient in their products, including _malt_, _maltodextrin_, _sucanat_, _corn syrup_ and _fructose_.

> _Glucose, a type of sugar, is the most common organic molecule on earth._

### 6. Eat meat cooked on the bone and organs for a beneficial diet. 

Now that you have an idea of what to avoid, it's time to learn what you _should_ eat. This brings us to the author's picture of optimal nutrition: _The Human Diet_. This concept relies on four pillars that are essential to all traditional eating regimens.

The first is to eat meat cooked on the bone. This is not only tastier meat, it's also more nourishing. When meat is cooked on the bone and with skin or ligaments attached, _glycosaminoglycans_ are released.

These molecules are the very same stuff you'll find in joint-nourishing supplements, and they're the reason that Thanksgiving turkey tastes so good. Best of all, when cooked, these molecules break down to a size small enough to fit the receptors in your taste buds.

Another factor in the nutritional superiority of meat on the bone is the minerals it releases. Bones are full of minerals, and by cooking them, you can derive both the flavor and benefit of these essential nutrients.

However, if you want the most bang for your buck, organ meats are a great option. They hold more nutrients than most fruits and vegetables and are the second pillar of the Human Diet.

By comparison with broccoli, liver contains three times the amount of vitamin B1, practically five times more vitamin B6, twice as much folate and 40 times more vitamin A! Interestingly, the organs that we eat from other animals are nourishing for the same organs in our own bodies.

A good example is brain tissue, which is full of brain-healthy fats like omega 3s. And, while eating eyes might sound gross, the layer around the eyeball is full of _lutein_, which, in addition to its role in preventing the severe eye disease _macular degeneration_, is great for prostate health.

Realistically though, most people aren't going to start eating eyeballs tomorrow. That's no problem; there are plenty of other foods that are good for you and that you're likely already eating. Next up we'll explore one group in particular that meets these criteria: fermented foods.

### 7. Sprouting or fermenting your ingredients makes them more nutritious. 

Lots of people today steer clear of wheat since it makes them feel poorly, but a simple switch to sprouted bread might clear up those issues. But what the heck is that?

Well, sprouting is an ancient technique that allowed humans to boost the nutritional content of their food. It's a simple process wherein seeds or legumes are moistened over a few days to make them germinate.

This little change makes all the difference since the valuable nutrients that normally stay enclosed within the seed are freed to nourish the young sprout. Not just that, but sprouting causes enzymes to convert the starch within a seed into nutrients, thereby resulting in a more nutrient-dense food.

As an added bonus, sprouting seeds and legumes can also eliminate plant toxins like _phytates_, which bond with nutrients and prevent them from being taken up by the body.

As well as sprouting, another healthy approach is to eat fermented foods. In the _fermentation_ process, microbes break down sugars, transforming them into advantageous, easily digested nutrients.

A great example of a fermented food that most people have eaten is sourdough bread. But when it's not fermented, bread can cause serious trouble.

In Turkey in the 1960s the number of children born suffering from dwarfism rose. While Turkish scientists assumed the issue was genetic, they eventually discovered that unleavened bread was to blame.

Families had been purchasing this non-fermented option because it was cheaper, but it also contained all the phytates from the wheat used to make it. These chemicals depleted mothers and their children of calcium and zinc, both of which are crucial to growth.

That's why fermented bread is healthier, but if that's not your thing, other fermented options like cheese, beer, wine, yogurt, tofu and miso might be. Just make sure you eat some sprouted or fermented foods since they make up the third pillar of the Human Diet.

### 8. The final pillar of the Human Diet: antioxidant-rich fresh plant foods. 

Earlier you learned that there are two types of antioxidants: those that your body makes and those that you get from food. Now it's time to learn about the foods that are loaded with the latter.

The good news is that lots of foods make this list, and you don't need to buy fancy supplements or exotic berries to get all that antioxidant goodness. Many plant foods are already packed with antioxidants and are tasty to boot.

As a result, you can get a great mix of antioxidants from _flavonoids_ to _phenolics_ and _coumarins_ just from a diet rich in fresh greens, herbs and spices. Good vegetable choices here are peppers, broccoli, garlic and celery.

If you want to get more of these foods into your diet, one simple way to up your antioxidant intake is to plant a little herb garden on your balcony.

But these natural sources of antioxidants aren't just easy to consume, they're also perfectly balanced and able to function harmoniously, which is a lot more than can be said of most supplements.

If you want to get a bigger dose, get more antioxidants out of the foods you're already eating by consuming them at the peak of freshness and eating most of them raw. As you learned earlier, antioxidants neutralize free radicals, but many antioxidants are lost before they can perform this function. Oxidation occurs during the storage of food and also during cooking or processing.

That's why most foods have their highest antioxidant content when eaten fresh and raw. There are, however, exceptions to this rule. Carrots are loaded with cellulose that keeps antioxidants locked in. Therefore, they need a bit of heat or fermentation to release these antioxidants and make them available to your body.

### 9. Final summary 

The key message in this book:

**Modern industrially produced food is making us sick. But the good news is that we can heal our bodies and live healthier, longer lives by returning to an older way of eating. That means avoiding detrimental foods like sugar and loading up on natural ones like organ meat, fresh vegetables and fermented products.**

Actionable Advice

**Detox your kitchen.**

Making a big diet shift is no easy task, so start by getting rid of the bad stuff. To do so, just go through your kitchen cupboards, tossing anything that contains vegetable oil as one of the first six ingredients. If you feel bad about throwing these products in the trash, just donate them to the local food bank.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _It Starts With Food_** **by Dallas Hartwig and Melissa Hartwig**

_It Starts With Food_ (2012) gives you the inside scoop on the profound effects food can have on your body and well-being. Importantly, these blinks explain how you can alter your diet to both lose weight and feel better, body and soul.
---

### Catherine Shanahan, M.D., Luke Shanahan

Catherine Shanahan, M.D is a certified family physician who has practiced medicine in Hawaii for over a decade after receiving her education at Cornell University and the Robert Wood Johnson Medical School.

Luke Shanahan, Catherine's co-author and husband, is a writer, lecturer and graduate of the University of Iowa's prestigious Writers' Workshop. His passion lies in literature, art and cooking.

