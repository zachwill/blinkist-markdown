---
id: 575a6dfe5b96b60003675a18
slug: deep-work-en
published_date: 2016-06-17T00:00:00.000+00:00
author: Cal Newport
title: Deep Work
subtitle: Rules for Focused Success in a Distracted World
main_color: F5EB31
text_color: 757117
---

# Deep Work

_Rules for Focused Success in a Distracted World_

**Cal Newport**

_Deep Work_ (2016) is all about how the rise of technology has wrecked our ability to concentrate deeply on tasks — and how to overcome this blockade. These blinks illustrate different strategies that can help you improve the output of your work and get the most out of your free time.

---
### 1. What’s in it for me? Realize your full potential through focused, deep work. 

Be honest, over the course of these blinks, how many notifications, emails and texts do you think you'll get? Chances are there might be quite a few. The question is: How will that impact your understanding of these blinks?

Most likely you'll be less focused and probably miss some of the details. In an age where technology is evolving at a pace we could have once only dreamed of, we must acquire the skills and ability to focus on one task at a time in our daily work without interruption. We must learn to practice _deep work._

What does that mean and how can it be achieved? For starters, you'd better turn off your notifications and then you'll find out.

In these blinks, you'll discover

  * how multitasking makes you less productive;

  * the difference between deep work and being in the zone; and

  * how taking a shower can be a good time for focusing on a specific issue.

### 2. Multitasking and distraction are the enemies of productivity. 

A lot of people think that doing tons of things at once is the most productive use of their time, but this logic is dead wrong. That's because multitasking does not equal productivity. Sophie Leroy, a business professor at the University of Minnesota who conducted research on this phenomenon in 2009, shows why.

She demonstrates that when switching from task A to task B, our attention stays attached to the first activity, which means we can only half-focus on the second, which hurts our performance. Her experiments utilized two groups: group A worked on word puzzles until she interrupted them to go on to reading resumes and making hypothetical hiring decisions; Group B got to finish their puzzles before moving on to the resumes.

In between the two tasks, Leroy would give a quick test to see how many keywords from the puzzles were still stuck in the participants' minds.

The result?

Group A was much more focused on the puzzle and therefore less focused on the important task of hiring the right person.

The long and short of it? Multitasking is no good for productivity. Neither is being electronically connected all the time. In fact, while it might seem harmless to keep social media and email tabs open in your web browser, the mere fact of seeing things pop up on your screen is enough to derail your focus, even if you're not immediately addressing notifications.

For instance, a 2012 study by the consulting firm McKinsey found that the average worker spends over 60 percent of the workweek using online communication tools and surfing the internet with just 30 percent devoted to reading and answering emails.

Despite this data, workers feel like they're working more than ever. That's because completing small tasks and moving information around makes us feel busy and accomplished — but it's actually just preventing us from truly focusing.

> _"Instead of scheduling the occasional break from distraction so you can focus, you should instead schedule the occasional break from focus to give in to distraction."_

### 3. There are different strategies for achieving deep work – all of which require intention. 

So now you know some of the roadblocks that get in the way of deep work, but how can you overcome them? While there's no universal strategy, here are a few you might find helpful:

The first is the _monastic_ approach. This strategy works by eliminating all sources of distraction and secluding yourself like a monk.

The second is called the _bimodal_ approach, which involves setting a clearly defined, long period of seclusion for work and leaving the rest of your time free for everything else.

The third is the _rhythmic_ approach. The idea here is to form a habit of doing deep work for blocks of, say, 90 minutes and using a calendar to track your accomplishments.

And finally, the _journalistic_ strategy is to take any unexpected free time in your daily routine to do deep work. But regardless of which technique you employ, it's key to remember that they're methodical, not random.

In fact, that's exactly the difference between being in the zone and deep work. After all, you get in the zone by chance and often only after hours of procrastination. On the other hand, deep work is intentional and desired, which makes it essential to have rituals that prepare your mind for it.

One ritual might be to _define your space_. It can be as simple as placing a "do not disturb" sign on your office door, or going to a library or coffee shop. The latter is especially helpful if you work in an open office.

Just take J.K. Rowling, who, while finishing her last _Harry Potter_ book, stayed at a five-star hotel just to escape her hectic home environment and cope with the pressure so she could get into deep work.

Another ritual is to _define boundaries_, for example, by disconnecting the internet or turning off your phone.

And finally, _make your deep work sust_ _ainable_. Because, whether it's light exercise, food, or a caffeine pick-me-up, it's essential to give your body what it needs if you want to focus. If you don't, you'll never have the mental energy you need to stay in deep work.

### 4. Focus your brain and be selective about your use of technology. 

In the modern world, our brains have grown accustomed to craving distraction. After all, everywhere we look, people are glued to their screens, playing games, messaging or refreshing their Facebook pages on repeat.

The problem is that our brains are wired to be easily distracted. That's because, evolutionarily speaking, these distractions could pose risks or opportunities. As a result, it's hard for us to deeply focus on one task.

But don't worry, _productive meditation_ can rewire your brain and help you focus. Here's how it works:

Use moments that would otherwise be unproductive — like walking your dog, taking a shower or commuting to work — to consider a problem you need to take care of without letting your mind change subjects.

To get started, ask yourself questions that identify different issues in solving a given problem. Then, once you've landed a specific target, ask yourself _action questions_ like, "What do I need to accomplish my goal?"

Think of it as a hardcore workout routine for your brain that will help build your focus!

It's also key to be mindful of your intentions when using social media and the internet. For instance, if you use Facebook to keep in touch with friends, then use it to communicate with them, but also make an effort, when possible, to spend more time with them in person.

And, if you can't manage to do that, try going cold turkey: quit social media for 30 days and afterward, ask yourself:

Would the past month have been that much better with social media in my life? Did anyone care that I stopped using it?

If you answer no to both, give it up for good. But if you answer yes, then it's probably for the best to return to it.

> _"Maybe social media tools are at the core of your existence. You won't know either way until you sample life without them."_

### 5. Scheduling both work and free time is essential to restoring energy. 

When you get home from work or running errands all day, often all you want to do is, well, nothing. And for lots of us, that means having no fixed time slots where we have to complete tasks.

But ironically enough, we end up stuck in the same routine every night: we watch TV, scroll through our phones or stare at our computers. Then, when it's finally time to go to bed, we feel more tired than when we got home, leaving us depleted of energy for the next day.

How can you avoid that situation?

By scheduling everything you do, you'll free up time for being mindful of how you spend it. At the start of every workday, create a schedule that's divided into blocks of at least 30 minutes. In this schedule you should set both work and personal tasks like time to relax, eat or catch up on email.

It's inevitable that your schedule will change during the day, but if this happens, just rearrange your blocks. The idea isn't to strictly abide by your itinerary, but to cultivate awareness about how you spend your time.

That means it's also key to plan your evenings and weekends ahead so you can take actions toward specific goals. So, try to leave your work at the office, for instance, by imposing limitations and not checking your email after a certain time. By doing so, you'll give your mind the space it needs to shut down.

Finally, planning your evenings and weekends around activities other than those involving the internet can help you revitalize your mind and body. Maybe it's reading, exercise or just some quality time with loved ones.

> _"If you want to eliminate the addictive pull of entertainment sites on your time and attention, give your brain a quality alternative."_

### 6. Final summary 

The key message in this book:

**Distractions are everywhere in the modern world where multitasking has become our default state and is killing our productivity. The good news is we can take back control of our time by eliminating distractions and letting our brains focus on one task at a time.**

Actionable advice:

**The next time you feel completely exhausted at the end of an unproductive day, consider taking an "internet sabbath."**

Just take a notepad to work in which you write specific time slots when you're allowed to use the internet and avoid using it outside these time blocks. This will increase your productivity because you'll unconsciously want to take more advantage of the time you _have_ allowed yourself to use the internet. You'll be amazed at how your focus skyrockets, merely by being present when browsing the web instead of in a half-conscious state where time slips by.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _So Good They Can't Ignore You_** **by Cal Newport**

Now that you've learned how to achieve greater focus and do higher quality work in a world of constant distractions, let's zoom out and look at your work from a broader perspective. It's time to ask that daunting question: Do I have a career that I'm happy with?

People grappling with this question are routinely told to "follow their passion." But is passion really the best guiding principle when it comes to finding the right career?

Cal Newport thinks not. In fact, following your passion can even be a trap, increasing the likelihood that you'll end up with a job hate. So what should you do instead? To find that out, along with other surprising facts, like what Steve Jobs' _real_ passion was, we recommend the blinks to _So Good They Can't Ignore You_.
---

### Cal Newport

Cal Newport is an assistant professor of computer science at Georgetown University specializing in the theory of distributed algorithms. Several publications, including _Inc. Magazine_, _The Globe and Mail_ and _800-CEO-Read_, have featured his most recent book, _So Good They Can't Ignore You_, on their lists for the best business book of the year.

