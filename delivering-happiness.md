---
id: 50f4086ae4b0627d64b97abf
slug: delivering-happiness-en
published_date: 2013-01-01T00:00:00.000+00:00
author: Tony Hsieh
title: Delivering Happiness
subtitle: A Path to Profits, Passion, and Purpose
main_color: 5F82BE
text_color: 46608C
---

# Delivering Happiness

_A Path to Profits, Passion, and Purpose_

**Tony Hsieh**

The central theme of the book is the business of literally delivering happiness while living a life of passion and purpose. _Delivering Happiness_ (2013) tells the story of Tony Hsieh and his company Zappos, demonstrating how thinking long-term and following your passions can not only lead to profits but also a happy life for your employees, your customers, and yourself. The book describes an alternative approach to corporate culture that focuses on the simple concept of making people around you happy, and by doing so increasing your own happiness.

---
### 1. It is important to discover the one thing you are passionate about, and conversely, what you don’t want to do. 

In life and in business, you will win some and lose some, but what is truly important is finding out the one thing you are passionate about.

Realizing your passion is important, because it makes all the decisions that follow easy. Because you always know what you are aiming for, your ultimate goal.

For instance, if you know you are passionate about running your own business, and you love the satisfaction and fulfillment it gives you, the decision to turn down an offer to sell it for a large amount of money becomes easier to make.

Creating, doing, and making things that you are passionate about is more important than making a lot of money doing things you can't stand. Before you go any further, you need to decide if you are going to be true to yourself and, "stop chasing the money and start chasing the passion."

One way to find your passion is by trying many things to see what works. Along the way, you might find that one thing through the process of elimination, by realizing what it is you _don't_ want to do.

The more things you eliminate from your list, the easier it becomes to find your real passion. And the closer you get to finding your passion, the less likely it is you will end up chasing the wrong dreams.

### 2. Take things slow. Growing fast can be counterproductive if the wrong people are hired. 

Rapid growth for your company can be exciting, gaining investment dollars and hiring new employees every week. But beware of hypergrowth and take it as a warning sign. Growing too quickly and hiring the wrong people can diminish your company's culture.

No matter how many positions you think have to be filled, always take the time to carefully consider each new hire. You have to ensure every person on your team shares your vision and wants to be a part of the company culture you've created.

Conversely, making quick hires can be seriously damaging to company culture if they are out solely for personal gain and career advancement. Make these hiring mistakes, and you are bound to wake up one morning realizing that you no longer enjoy your workplace and the people in it.

This is exactly what happened to Tony Hsieh and his first company, LinkExchange, when they began growing so quickly that he realized he didn't even recognize the people in his own office. Caught up in the rush and excitement of the company's success, the company culture suffered, because the new hires weren't passionate about LinkExchange's mission. They were just looking to make a lot of money and retire.

In the end, Tony learned the valuable lesson that it's best to grow a company slowly to carefully monitor the hiring process and ensure that everyone on board is contributing to the culture you wish to create. Short-term sacrifices to protect company culture and stick to core values are a long-term benefit.

### 3. The success of your company is closely tied to your culture, so focus on creating one that you believe in. 

The culture of a company is one of its most important characteristics. Your culture is your brand, your employees are your brand ambassadors.

So make it a point that each new hire joins the team for more than the skills and experience listed on their CV. Only hire people you would happily go out for drinks with.

Connectedness, and feeling like part of a tribe makes people happy and creates a sense of fulfillment. Both are strong motivators. When a group of people feels connected, like a family, there is a strong sense of obligation to the whole team, to work harder and treat each other better.

However, simply feeling connected is not enough. Your team should also have a shared purpose and shared passions. Hire only people who emulate, live, and breathe your core values.

If you have a strong company culture, formed by people who have a common goal, your company's core values will form naturally.

Company culture is even more important than customer service, because if the culture is right, great customer service will naturally develop from it.

For Tony Hsieh, the process of finding a great culture involved much trial and error, but mainly boiled down to spending time together as a team outside the office. When the entire Zappos team moved to Las Vegas together, it forced them to rely on each other and grow closer.

A team doesn't need to move to a new city to do that, but it helped Tony and the Zappos team hunker down and think about what was collectively important to them, and out of this came their core values.

### 4. Strive to be always learning, and make it a priority for your employees as well. 

To build a great company, you have to pursue growth and learning. Continual growth should be a goal for your overall business and for all the people that are part of it.

Create a culture that fosters both personal and professional development. Build an office library, offer classes for developing new skills. Employees should feel like their work is part of a greater purpose, and that learning and growth are not only allowed, but appreciated.

It benefits your business to constantly present all employees with new challenges and opportunities to grow. They won't feel stagnant, but can develop and take on more tasks and responsibilities as they grow with you. 

The Zappos team has a goal of helping employees unlock their potential by constantly taking on new challenges. The overall business is constantly facing new challenges, so it benefits both them and the company if employees are taking on new responsibilities and developing new skills to tackle whatever issues Zappos faces. They learned that the best expertise someone could bring to the team is an ability to learn, grow, and adapt, and they aim to give employees the freedom to pursue new challenges as they feel ready to take them on.

One Zappos employee, inspired by her freedom within the company to propose new ideas and go after more challenging projects when she felt ready, became so confident that she started speaking about Zappos at conferences. And the growth carried over to her personal life as well, inspiring her to pursue more reading and a healthier lifestyle. Essentially, a happier, more fulfilled life!

### 5. Choose one thing you want your organization to be the best at and focus on that thing. 

Having one thing your company aims to be the best at is helpful because it allows you to focus and specialize, and thus become really great at something rather than being merely adequate or good at several things.

Tony and the Zappos team decided that customer satisfaction and creating a WOW experience for their customers was the one thing they wanted to become great at. They wanted to focus on extraordinary customer service, and on delivering happiness to people through great service.

Since a company's culture and its customer service are closely tied together, delivering great customer service and making your customers happy is only possible if you start by treating your employees well and making them happy. Happy employees will be passionate about their jobs and make customers happy through great service.

Tony Hsieh's most important piece of advice is to never outsource the one thing you want your company to be best at. From his experience with his former company, eLogistics, he learned that if there's something you want your business to be a leader in, it must extend throughout the company. It can't be up to just one department.

A radical way that Zappos stuck to their commitment to extraordinary customer service was by moving headquarters to Las Vegas, where they wanted to build their customer support call center. It was their way of making customer support their utmost priority, as an entire team.

### 6. Don’t focus on building buzz around your brand, but rather build engagement and trust by treating people well. 

Attempting to create buzz around your brand is counterproductive when you could just be putting your effort into doing what you do best. Instead of stressing over how to get media attention, focus on delivering a great customer experience and service. The rest will follow, and your buzz will self generate.

The goal of your customer service should be to create positive emotional associations with your brand. Every interaction with your customers is an opportunity to further your brand. Great customer service builds your brand and drives word of mouth advertising. Let loyal customers do your marketing for you.

When you're doing something that naturally creates interesting stories, people will want to get in on the action and talk about it. Word of mouth is a powerful thing. Press will follow.

Zappos has developed a following thanks to great customer service interactions over the phone, in particular. Creating a great impression over the phone will not only result in word of mouth advertising from the customer, but will also increase the lifetime value of the customer.

Zappos uses the money they would have spent on traditional advertising to pour back into their customer experience instead. That means customer perks like free shipping on every order, a 365-day return policy, unexpected next day shipping for customers, and authentic interactions on the phone when customers call for assistance. Because Zappos goes above and beyond to wow their customers in these ways, buzz has naturally generated around their brand, and they have received a great deal of media attention for it.

### 7. Have a vision of a higher purpose so that your ultimate outcome will be happiness. 

Having a vision of a higher purpose means being about something bigger than whatever you are selling. It means having a goal that goes beyond making profits and growing a company.

Even though Zappos is a shoe business by technicality, that's not the real thing they're selling. There's a higher purpose–namely, making people happy. By delivering great customer service. WOWing people by going above and beyond their expectations. Whether this means surprising a customer by upgrading their shipping or offering free return shipping, Zappos always has the longer term goal in mind which is about more than bringing in revenue.

Having such long term goals, in life, and in business, means asking yourself why you are doing something. And knowing you do what you do for a higher purpose will make you happier.

This is where it all comes together. For Tony Hsieh the whole point of building a company based on customer satisfaction, of creating a culture he and his employees believe in–is happiness, both for employees and customers.

### 8. Final summary 

Company culture and customer service are the keys to a successful business. Ultimately, happiness is what everyone is looking for in life, and a company can be successful by finding ways to make its employees, customers, and partners happy. This is achieved through extraordinary customer service and a company culture that is unique, one that employees believe in.

Got feedback?

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with _Delivering Happiness_ as the subject line and share your thoughts!
---

### Tony Hsieh

Tony Hsieh is an American entrepreneur and venture capitalist best known as the CEO of Zappos.com. He sold his previous company, LinkExchange, to Microsoft for $265 million in 1999.

In 2010, _Delivering Happiness_ debuted at #1 on the _New York Times_ Best Seller list and ever since, Hsieh has been considered an expert in customer service and corporate culture. He is also recognized as an influential Twitter user, having gained a huge following over the years.

