---
id: 53b967553062390007000000
slug: design-for-the-real-world-en
published_date: 2014-07-08T00:00:00.000+00:00
author: Victor Papanek
title: Design for the Real World
subtitle: Human Ecology and Social Change
main_color: 59AE5F
text_color: 3E7A43
---

# Design for the Real World

_Human Ecology and Social Change_

**Victor Papanek**

_Design_ _for_ _the_ _Real_ _Word_ takes an uncompromising look at the social and ecological repercussions of industrial design in the United States. In order to combat this destructive trend, author Victor Papanek offers fundamental insights into socially, morally and environmentally responsible design, as well as ideas for achieving those goals.

---
### 1. What’s in it for me? Learn how to change the world by shifting the focus of product design. 

Have you ever bought an expensive piece of equipment — like a laptop, camera, or mobile phone — only to have it suddenly break long before you'd expected? Did it make you angry? Did you even get the feeling that that was some sort of _conspiracy_ out there to make sure products are designed in such a way that suckers like you keep going out year after year to buy new, expensive products?

If any of those questions resonate with you, then _Design_ _for_ _the_ _Real_ _World_ can offer you some of the answers you seek.

In this book, written in 1971 and yet more relevant than ever, the author gives us a relentlessly honest account of irresponsible design practices in the United States and their contribution to the surge in production of the disposable and low-quality consumer goods that line our shelves today.

But in his scathing critique, the author is careful to provide many practical ways in which designers can refocus their efforts so that they become positive forces for change, rather than contributors to the destruction of our environment.

In these blinks, you'll learn:

  * why sometimes it's better to produce a simple radio rather than one decked out with all the latest gadgets,

  * how it could be worthwhile to put a positive spin on waste,

  * why watching Saturday morning cartoons can be seriously dangerous for children's health, and

  * why the super affluent sometimes buy chairs that are super uncomfortable.

### 2. More than beauty: design produces functional tools and objects for human society. 

What do you imagine when you hear the word "design?" High fashion and fancy chairs?

Certainly, today's understanding of "design" is mostly associated with the design of products — e.g., the sleek casing of the iPod — and is often considered to be the art of making things look beautiful.

In fact, there are many examples where product design focuses only on appealing to our senses, i.e., look, touch, smell, sound and taste. One particularly exaggerated example of this is the disc player that produces an aromatic scent whenever the disc plays. Consumers then have the option of choosing between "romantic" or "natural" aromas.

Although this design feature is entirely cosmetic and an unimportant improvement to standard players' function, it would not be unusual for products like these to be referred to as "good design."

However, good design encompasses far more than just aesthetics. Good design is about creating functional tools, products and systems.

In addition to aesthetics, _functional_ _design_ considers the methods employed by the designer, the intended use of the product, the human needs the product aims to meet, the associations the design creates with friends and family, and its relationship to nature and society as a whole.

In order for a design to be considered "fully functional," it must fulfill _all_ these elements.

Consider, for example, Leonardo da Vinci's mural, _The_ _Last_ _Supper_. The painting is clearly beautiful, and thus satisfies an aesthetic function. But it does much more than this! For example: its immediate use is to cover an otherwise bland or ugly wall; contemplating the painting serves our need for spiritual enlightenment; and it creates clear associations with the Bible, thus helping people to identify with the design.

A successful designer, such as da Vinci, needs to balance these varying demands when designing her product. This is especially important as design can have far-reaching, unintended effects, as we'll see in the next blink.

### 3. Designers carry a serious responsibility for the consequences their designs have on society. 

In the face of ever-increasing complexity in our modern society, there are more and more things which need to be planned and designed.

Consequently, the impact of a designer's decisions goes far beyond a single customer or targeted market group. In fact, every design decision today has far-reaching consequences that reach deeply into different areas of society, such as the economy, politics and culture.

A perfect example of this is found in the design of fast-food products. Many fast-food hamburgers are wrapped in non-degradable plastic foil, and the food itself contains enormous amounts of sugar and salt. These design decisions contribute both to the production of environmentally unfriendly packaging material _and_ to health problems such as obesity and malnutrition.

Clearly, even seemingly simple design decisions, such as recipes and packaging choices, have large impacts on the world, and can serve either as a force for good or destruction.

Designers therefore shoulder an enormous amount of social and environmental responsibility, going far beyond mere consumer satisfaction and market reaction.

As an example, imagine that a designer is commissioned by a business to design a radio that it can sell. Not only must it function properly, but it must also produce a profit.

The designer thus has to consider things like the cost of production, packaging, and price point, _in_ _addition_ to the actual function of the product. And if he wants to be a force for social good, then he'll have other things to take into account: the labor involved, the factory conditions, the environmental impact of the materials, and so on.

But what if he's tasked with designing a low-quality radio? This might require cheaper labor and environmentally harmful parts. However, a cheap enough design might also provide the world's poorest with access to information, which would provide a great social benefit.

As this example illustrates, every designer has to make tough choices with each project, all of which can have major consequences for society.

And in light of these consequences there also comes considerable responsibility. In the following blinks, you'll learn just how designers are failing this responsibility.

> _"Each hamburger, fish sandwich, egg burger, or what-have-you comes in its styrofoam sarcophagus, is further wrapped in plastic foil and accompanied by numerous condiments . . . each in its own plastic or foil pouch."_

### 4. Unfortunately, design is often completely detached from real-world needs. 

In the last blink, we talked about the great social responsibility that designers carry. In recent decades, however, design has become increasingly detached from this responsibility.

Rather than designing for society's actual needs, designers instead tend to focus on the highly individualistic preferences of only a narrow portion of society.

Consider, for example, the design trends from the 1920s and 1930s, where designers translated the "de Stijl" painting movement, consisting of squares, rectangles, and straight lines, into furniture. The results were extremely uncomfortable chairs whose only purpose seemed to be to represent a specific kind of artistic expression.

In spite of their limited functionality, these chairs remain highly priced because they are considered stylish and trendy "must-have" objects, even to this day. As a result, only very affluent people — the well-positioned economic elite — can afford to show them off in their homes.

In designing for a rich minority, designers have forgotten all about real needs of everyone else.

The consequences for this are far-reaching, as only very few of us can afford the luxury items designed by today's top designers. Moreover, it affects those who would benefit most from good, well-thought-out designs, such as the blind, mentally challenged or otherwise handicapped people all over the world.

In fact, many of the needs of handicapped people are left unfulfilled. For example, those suffering from arthritis often find it extremely difficult to open pill boxes. Compare this to the 500 kids who die from misusing pills every year simply because they have _no_ trouble opening them.

And yet, in spite of the terrible need for a solution, no famous designer or company ever bothered to find one. It took a team of committed design students that were trained in socially and environmentally responsible design to conquer this task.

By taking the needs of handicapped people seriously, they were able to devise an easy-to-open and child-proof pill box. Truly, when there's a will, there's a way. Designers must simply have a keen eye for where the need is.

### 5. Current design produces meaningless, tawdry and short-lived products. 

During spring cleaning, do you ever find these relics — the junk collecting dust in your closet, unused? Did you find yourself asking: "Why the hell did I even buy this in the first place?" Well, there's a good reason for this.

Currently, rather than expending their energy designing products that would add value to our societies, designers instead busy themselves with the production of useless, tacky products, such as gadgets and toys. Unfortunately, the number of examples which illustrate this trend seems endless.

For example, did you know that there is a whole slew of robots that you can purchase today that, for whatever reason, are in high fashion? Most of these toys have no practical value whatsoever. There is even one robot that can speak, but cannot do _anything_ else apart from that.

But the absurdity of useless designs doesn't end there: consider the mink-covered toilet seats, shoe horns decorated with rhinestone, or electric dish towels, all of which you can buy and all of which will quickly be forgotten.

What's more, the production of meaningless products goes hand in hand with a culture of disposability and manufactured obsolescence.

Think about it: what's better than designing a product that people want to buy? A product that people want to buy _and_ that must be replaced often. Designers are therefore responsible for producing short-lived products and have become complicit in the harmful effects of a profit-driven economy.

You don't have to look far to find examples of this. Consider your last car. Its function aside, how long did it take before it _looked_ old? Indeed, superficial changes are made to car designs each and every year just to change their look and entice you to buy.

And then every three years there are major changes in style that cause consumers to run out and replace their cars, like clockwork. But is that really necessary?

### 6. Misdesign has dramatic negative consequences for both society and the planet. 

So far, we've learned about the grave social and environmental responsibilities that come with product design. But what happens when designers don't live up to these responsibilities?

First and foremost, since many products are intentionally designed to quickly become obsolete, they are also sloppily designed. This lack of diligence in the design process can have damaging effects in terms of health and safety.

For example, even simple products like safety goggles might become a serious health threat if not properly designed. Surprisingly, many safety goggles are themselves very unsafe, as they tend to break or shatter when exposed to even minimal force, and can even break the bridge of your nose if the goggles get hit.

While not everyone owns or uses safety goggles, most people have TVs. Some older color television sets emitted harmful rays that could damage the reproductive organs of children seated too closely to the TV during their morning cartoons.

Even worse, this _misdesign_, i.e., socially and environmentally irresponsible design, can have disastrous consequences of epic proportions. Just consider how many of today's products are "disposable." What does that mean for our environment? Not only do these products contribute to pollution, but they must also be continually replaced.

But disposable products aren't conjured from nothing: an economy based on the production of disposable consumer goods requires raw materials to produce new products, which then puts a strain on the environment. Meanwhile, the disposed products end up in the ever-growing landfills, where dangerous chemical agents often leak into the ground or water.

And it's not only consumer products that can be misdesigned. In larger-scale projects, irresponsible design can also contribute to the immediate, far-reaching destruction of natural habitats.

One poorly designed large damming project — the felling of trees and diverting of rivers in Florida — resulted in huge loss of wildlife, the withering of the Everglades and desertification.

### 7. Certain myths act as roadblocks on the path to more meaningful design. 

In order to stop the destructive consequences of bad design, we must fundamentally rethink the purpose of design. But to do this, we must first overcome certain barriers.

Some kinds of misdesign are upheld by powerful social myths based on ignorance and misinformation.

One such myth, for example, is that sales and market research departments have control over what gets produced, and so designers lack the power to act as a force for good.

And yet designers conceive, invent and plan gadgets such as radios shaped like hamburgers and electrically heated pants! How can it be that they can't put that same effort into designing something that will actually help people?

Designers also wrongly believe that they serve the masses. Consider, for example, that if there are 22 million chairs produced in the United States, the designer of a single chair effectively caters to the needs of only around 500 people, due to the large variety of models manufacturers rush into the market.

However, if a designer created chairs with a specific, positive purpose in mind — for example, simple, ergonomic, and inexpensive chairs — instead of designing fancy chairs for American high society, they could produce billions of functional chairs for use in schools, hospitals and homes in poorer areas.

Adding to these myths is a range of emotional, intellectual and cultural hurdles that block the path to innovation. Indeed, a large portion of society increasingly celebrates conformist values, making it hard for designers to be creative problem solvers.

Consider, for example, the way that we think about waste: essentially, it's gross and we want to just get rid of it. But if we think about it in a more positive sense, then we can start finding creative ways to use waste products in order to fulfill important functions in our society. For example: How can we use chicken droppings to power automobiles? Or can we use manure to make electricity for our homes?

So you now have a good idea of what stands between our current situation and a morally and environmentally responsible design process. The following blinks will explore what that process would look like.

### 8. Time for a radical rethink: Designers must begin with the real needs of people. 

If they're going to meet the real needs of real people, designers must explore fundamentally new paths in order to to overcome the barriers and myths that perpetuate a socially and environmentally destructive system.

The way to accomplish this is through _integrated_ _design_, a comprehensive design strategy that incorporates various factors and perspectives into the decision-making process. Integrated design is also proactive, in that it seeks to create positive future outcomes.

While there are, of course, many practical elements that help create a comprehensive design, an essential piece of the puzzle is the multi-disciplinary design team. This team should be a representative cross section of the many areas of our society, such as culture, anthropology, business and others. It's the lead designer's job, then, to bridge the gap between varying approaches in order to facilitate an integrated design.

The strategy of producing an integrated design also places its project within a social context, and reflects the needs of real people. This means that the end users, as well as the workers who manufacture the product or service, should also be part of the design team.

Good examples of this can be found in product designs made for developing countries. For example, designers have drafted the design for a small radio device that produces electricity with the help of a kind of candle placed inside a can. Its design is very simple and can be adjusted to local needs, and the candle inside can even be powered by things such as wax, wood, paper or dried cow dung.

Local users can even participate in the product design. For example, users in Indonesia decorated the tin-can radio with materials found in their surroundings such as glass and shells.

By incorporating the elements of integrated design and soliciting the input of the end user, designers will be able to develop all kinds of unique and socially beneficial devices.

> _"We must see man, his tools, environment, and ways of thinking and planning, as a nonlinear, simultaneous, integrated, comprehensive whole."_

### 9. The natural environment offers important insights for designers. 

So where do designers get their inspiration to overcome barriers and revolutionize design today? One good place to look is Mother Nature, with her billions of years of design experience.

In fact, there are many ways in which designers can learn from nature and biology simply by studying natural processes and applying them to their designs for novel products and solutions. Examining, for instance, the natural growth processes of plants, the skeletons of animals and the structure of crystals can help us to find the inspiration for innovation.

This, of course, is nothing new. Humans have always learned from nature: the hammer, for example, is a more durable iteration of the human fist, the rake much like animals' claws, and so on.

We could also turn our attention to the sea. Lichen, for example, is a mixture between algae and fungi that somewhat resembles moss, and exists naturally in more than _one_ _hundred_ colors — from bright orange and blazing red, all the way to pale gray. But what might we do with it?

It could, for example, function perfectly as a low-maintenance and long-lasting wall paint. Or it could serve as a replacement for the grass planted in the median strips of highways, thus radically reducing public mowing costs.

However, as modern society's problems become increasingly complex, designers not only need to study the shape and form of individual organisms, but must also understand how entire natural systems come into being.

For instance, when scientists study the way dragonflies take flight, they don't just look at their wings and their bodies, but instead study how all the various elements work in concert as they lift off, fly and land.

For example, scientists examine dragonfly behavior inside wind tunnels in order to glean insights into aerodynamics. They might also discover some other seemingly unrelated things. For example, insights into how dragonflies fly are used not only in designing flexible and fuel-saving aircrafts, but can also be used to predict weather and ocean currents.

### 10. In order to be effective forces for good, designers need experimental design education. 

Would you trust a stylist to cut your hair if he had only studied, but had never actually _used_ the scissors in his hand? Probably not, and we should take the same approach to design.

Today, most design graduates leave school without the necessary competency to actually solve the problems our society faces. That's because their studies teach them only the technical aspects of design, such as work processes, tools and methods. Aspiring designers, however, must become generalists that learn from a variety of sources.

This means that designers must expand their horizons in a multi-disciplinary environment, where they are exposed to other disciplines, such as anthropology, psychology and behavioral science. These various disciplines will help them to approach design problems from differing perspectives, and better understand other crucial aspects, such as the needs of people, the role of social dynamics, and so on.

These diverse disciplines also "cross-pollinate" one another, further broadening the students' perspective and helping them come up with unexpected design solutions.

In addition, designers need real, practical design experience. In conventional education, young designers do a large portion of their learning in the classroom. However, as they will be working on designs with far-reaching implications for society, it's critical that they work on real-world, real-time problems in the course of their studies.

A good design education would encourage students to travel far and wide to witness firsthand various environments and cultures, and then work on practical design problems, such as those found in offices, industry, factories and farms.

There they can design new tools, machinery, devices and structures with the direct aid of the end-user. This is clearly a win–win situation: designers get hands-on experience, and the community would benefit from the new design solutions that they helped to create.

> _"Education can once again become relevant to a society of generalists, in other words, designer-planners."_

### 11. Placing our focus on research, personal ethics and design education will help us to make positive design contributions to the world. 

So far we've talked about the "big issues" facing designers and design education. This final blink will discuss the three simple steps that will get designers started on the right path.

First, we should focus our energy on large-scale research efforts. If designers are to be a force for good, then it is essential that they have an informed perspective, and that means having an understanding of the underlying mechanisms of the world we live in.

This research effort would consist of multi-disciplinary design teams gathering knowledge about different cultures, institutions, psychology and sociology in order to understand what the driving forces are in today's society.

And they should be asking the "big questions": What are optimal ways to live? Or organize a society? What roles do religion and morality play within our society?

Second, we must stop participating in immoral design activities. This means that designers stop contributing to any project that may be harmful to society or the environment. This, of course, is a huge change, so it's OK to start small. For example, designers could decide to start allocating ten percent of their time to solutions for social and environmental problems, and then scale up from there.

Finally, we need to fundamentally rethink design education. Most critically, young designers need to work on real-life issues in a way that is both multidisciplinary and socially and environmentally responsible.

One way would be to create a combined learning and working environment with no more than 30 aspiring designers, who would also be part of a larger international network of 1,000 international learning centers around the globe. Keeping a small class size would encourage individual development and practical learning while also facilitating a spirit of international cooperation.

These are just a few ideas; there are many ways in which designers can start creating a world we all want to live in. But these three steps will at least point us in the right direction.

> _"Design, if it is to be ecologically responsible and socially responsive, must be revolutionary and radical in the truest sense."_

### 12. Final summary 

The key message in this book:

**Design** **that** **does** **not** **take** **the** **real** **needs** **of** **real** **people** **into** **account** **and** **disregards** **its** **own** **environmental** **consequences** **is** **no** **longer** **tolerable.** **Instead,** **design** **must** **be** **socially** **and** **morally** **responsible** **in** **order** **to** **become** **a** **positive,** **innovative** **force** **in** **society.**
---

### Victor Papanek

Victor Papanek was an Austrian-American designer and educator renowned for his highly critical views on the social and environmental responsibility of designers. He taught at various universities throughout North America and authored and co-authored a number of books, including _How_ _Things_ _Don't_ _Work_, _Design_ _for_ _Human_ _Scale_, and _The_ _Green_ _Imperative_.

