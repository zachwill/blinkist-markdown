---
id: 5550be2f6332630007be0000
slug: design-to-grow-en
published_date: 2015-05-14T00:00:00.000+00:00
author: Davis Butler & Linda Tischler
title: Design To Grow
subtitle: How Coca-Cola Learned to Combine Scale and Agility and How You Can Too
main_color: E5352E
text_color: B22924
---

# Design To Grow

_How Coca-Cola Learned to Combine Scale and Agility and How You Can Too_

**Davis Butler & Linda Tischler**

_Design to Grow_ (2015) reveals how iconic brand Coca-Cola went from a small local soda company to one of the biggest and most innovative companies in the world. With these blinks, you'll be able to ensure your company is both agile and scalable, no matter how big your endeavor is.

---
### 1. What’s in it for me? Discover Coca-Cola’s secret recipe to success. 

In the 1985 blockbuster _Back to the Future,_ Marty Mcfly is transported back in time to 1955. Naturally, he finds things very different from his own times, but one product is almost exactly the same, a bottle of Coca-Cola. The funny things is, in the 30 years since the film came out, a bottle of Coke has still changed very little.

You might think that a product that hasn't really changed in 60 years would be seriously outdated, but you'd be wrong. Coca-Cola's brand is partly a success because it is designed to stay the same; it was designed for universality. And yet at the same time, while the basic product is the same, the company is incredibly innovative in other areas.

Coca-Cola is both agile and stable, which sounds impossible, but these blinks show you how it's done, so that you and your company can do the same.

In these blinks you'll discover

  * how one jingle of just five notes has been used over and over again;

  * why nobody carries a blackberry anymore; and

  * why Coca-Cola asks for your help to market their products.

### 2. Good design is the key to company growth. 

In our minds, the word "design" isn't that far from the word "art." Even though we're used to seeing design as a form of artistic pursuit, this is often far from the case. While beauty in art is subjective, we can all agree on the differences between good design and bad design.

When a painter creates a picture, we compare it to the kind of art we're partial to, or declare it particularly creative, and judge it good or bad accordingly. Design is totally different. Take a look around you: Every object you see, from your coffee cup to the phone you're reading this on, has been designed _for a purpose_.

The ubiquity of design has sharpened our keen and universal sense of what _good_ design is. For example, a website that allows you to buy something in a few clicks is well designed, right? By the same token, sites that force you to fill out form after form are poorly designed. In short: good design fulfills the function we need it to, and fulfills it effectively.

This applies to your company too. While there are many ways to approach design in a business context, it helps to first consider it in terms of the visible and the invisible. _Visible stuff_ refers to the elements of design your customers see, such as your products or customer service. _Invisible stuff_ includes your processes and partnerships — things the customer cannot see.

Good company design combines the visible and the invisible, and no company reflects this better than Coca-Cola.

When the iconic brand needed to redesign their Minaqua bottled water in Japan, they combined invisible and visible features. First they did their research, and found that Japanese people tend to be passionate about recycling and live in relatively small houses. The company processes and the product itself were then designed to match these findings.

Coca-Cola developed a small, lightweight plastic bottle that could easily be twisted to maximize storage space and make it easy to recycle. Visually stylish with great invisible design, this rebranding significantly increased product sales. Coca-Cola shows us that we should never underestimate the importance of design.

### 3. Simplicity and standardized quality are what makes a company scalable. 

For a start-up to be successful, it must first be _flexible_. Fledgling companies must never stop experimenting and should constantly keep their eyes peeled for their ideal market. But once your start-up finds its market niche, flexibility must be swapped for a new _stability._

Any company that wants to scale must be able to standardize its products and processes, while ensuring that the highest quality is constantly upheld. In this way, a recognizable brand will be created for fast and efficient scaling.

If this all sounds a bit abstract to you, let's have a look at how Coca-Cola took on the task of scaling through designs whose simplicity and quality boosted their growth.

Take the _contour bottle_, developed in 1915. At the time, Coca-Cola needed to grow, so they raised awareness about their product with a competition to invent a unique bottle design. The winning entry was based on a cocoa root, and became what is today one of the most recognizable objects in the world.

Next, think of the _logo_. By 1923, Coca-Cola had already standardized their curly script, and it has stayed the same ever since across all of their products and marketing campaigns.

Finally, consider the Coca-Cola _Franchise Business Model._ In 1899, the company wanted to expand their product. So, Coca-Cola struck a deal with two businessmen who were allowed to buy the rights to bottle Coca-Cola for just a dollar. The franchise system was born.

As bottlers bought up the rights to bottle the drink and sell it in their areas, Coca-Cola expanded across the world at an incredible speed. And because of their already-established bottle shape and logo, the brand was recognizable, famous and, above all, popular.

So the next time you're considering implementing yet another complex management system, a sub-logo, or another range of products, think it over. Why not go for simplicity and quality? That's the secret of scaling.

### 4. Businesses face complex problems, which require targeted solutions. 

It's true that standardization and simplicity are vital for successful scaling. However, this does not in any way mean your company should rest on its laurels! In fact, there has never been a more dangerous time to be an established company than today. Why?

Well, in our post-internet world, any geeky teenager can design an app in his bedroom that totally undercuts your hard-won success, and you never know when this might happen. Business today is made up of many intricate relationships that determine how the system acts, but in a way that makes it nearly impossible to predict what might happen.

This means that businesses have more and more _wicked problems_. These are issues that simply can't be solved with a formula. Each problem requires a specific and considered solution.

While Coca-Cola faces many of these wicked problems, one of the most pressing is the issue of _water._ Since clean water is one of the most vital commodities for Coca-Cola, its factories and its workers, the various factors that disrupt its supply are a major threat. These factors include climate change, poor infrastructure and rapid population growth, among others.

With so many factors at play, the problem of clean water is a complex one in need of a targeted solution. So what did Coca-Cola do?

Coca-Cola has declared that it aims to be water-neutral within ten years. By returning the same amount of water as it uses to the supply, Coca-Cola is able to ensure that there is enough water for its employees and their communities.

This kind of solution requires _agility_ in order to be implemented before it's too late. But what exactly does agility look like? Read on in the next blink to find out.

### 5. Stay agile by experimenting fearlessly. 

Back in the first decade of the new millennium, if you had a swanky cell phone, chances were that it was a BlackBerry. Between 2004 and 2010, BlackBerry held 50 percent of the market share. What happened?

In the last five years, the company's popularity has collapsed, overtaken by the smartphones of Apple and Sony. How could a former market leader let this happen? BlackBerry must have noticed their competitors' ever-increasing popularity. So why didn't they respond?

The truth is that BlackBerry is a company based on _scale_, and not on _agility._ They failed to adapt to their changing environment and paid the price for it. So how can you ensure you never stop adapting? By experimenting, even if you think you might fail at first.

Take Norm Larsen: After 40 attempts to invent a substance that prevented rusting in nuclear missiles, he still hadn't achieved his goal.

What he _had_ invented was a substance that stopped doors from squeaking, cleaned guitar strings, and even removed tomato stains from clothing. His failures had led him to what we know today as WD40.

If you want to make room for experiments like Larsen's, consider adopting _modular systems._ A modular system is rather like a set of lego blocks — fixed elements that you can combine in different ways to produce different results.

Coca-Cola shows us the brilliance of modular systems with their jingle. Designed in 2006, the tune had just five simple notes. This was enough for it to become a success, something that brought Coca-Cola to mind whenever you heard it.

And because it was modular, the notes could be adapted to any style. For example, during the 2010 FIFA World Cup in South Africa, a series of African-influenced versions of the jingle were created. Coca-Cola shows us that just one simple system is enough for experimentation, which allows a company to learn and become smarter.

However, learning by doing is just the first step toward agility in your company design. The speed at which you implement your smarter ideas is also crucial. Read on in the final blink to find out how this can be achieved!

### 6. Keep your company lean by crowdsourcing innovation. 

No matter what line of business you're in, no matter how long you've had success, your customers will always try to tell you how you can do your job better. It's inevitable! So why not make the most of it?

With an _open innovation system,_ you can use your customers' ideas to keep your company _lean_, or, in other words, as efficient as possible. An open innovation system refers to a modular system with fixed elements that also allow for flexibility.

For a great example of open innovation, just think of Wikipedia. Its fixed elements include the font, the format, and the coding — no one can change these aspects of the site. However, the _content_ is generated by users.

With so many individuals working towards the same goal of getting information out there, there's a great opportunity to generate ideas and drive improvement, and all with minimum input from the organization itself!

By making the most of crowd knowledge, large companies can also reduce their research and development expenses. Coca-Cola took advantage of an open innovation system with their Design Machine, a web-based platform that encourages users to generate their very own Coca-Cola ads and product designs.

With a handful of fixed elements in place, such as standardized templates for packaging, advertising slogans, the Spencerian script, and the Coca-Cola red, the rest is up to the customers and their preferences.

Coca-Cola can then track their customers' preferences in order to learn what's loved and what isn't. Customer-generated content like this has reduced Coca-Cola's research and marketing expenses by a whopping $100 million.

### 7. Final summary 

The key message in this book:

**For a big company to stay ahead of the market, it must be both scalable and agile. By continuously encouraging growth and adaptation through considered design and innovative systems, you'll be well on your way to long-term success.**

**Suggested** **further** **reading:** ** _Good to Great_** **by Jim Collins**

_Good to Great_ presents the findings of a five-year study by the author and his research team. The team identified public companies that had achieved enduring success after years of mediocre performance and isolated the factors which differentiated those companies from their lackluster competitors.

These factors have been distilled into key concepts regarding leadership, culture and strategic management.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Davis Butler & Linda Tischler

David Butler works at Coca-Cola where he is Vice President of Innovation and Entrepreneurship. He was named in Forbes magazine's "Executive Dream Team" in 2014.

Linda Tischler was a founding editor of Fast Company's design website, where she still works. She has also written for _Metropolitan Home_ and _The Huffington Post._

