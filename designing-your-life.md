---
id: 5a3a32b8b238e1000765bb45
slug: designing-your-life-en
published_date: 2017-12-28T00:00:00.000+00:00
author: Bill Burnett and Dave Evans
title: Designing Your Life
subtitle: How to Build a Well-Lived, Joyful Life
main_color: AB4D97
text_color: 914180
---

# Designing Your Life

_How to Build a Well-Lived, Joyful Life_

**Bill Burnett and Dave Evans**

_Designing Your Life_ (2016) is about taking control of your life by creating a plan that'll do away with an uninspired nine-to-five routine and usher in a career that you really love. You'll find advice and exercises that will point you toward your true calling, along with progressive ideas that challenge the limitations of traditional career counseling.

_"I love this practical, no-nonsense approach to the often nebulous topic of identifying the life you want to lead."_ — Ben H, Head of Content at Blinkist

---
### 1. What’s in it for me? Get the tools and tips to design a great life. 

Have you ever sat in a beautifully designed chair or an exquisitely well-made sports car? Or maybe you've stopped and marveled at the design of your smartphone? If so, then you've experienced the bliss of quality design. Now, let's imagine that you could tinker with and fine-tune your life so that it, too, was perfectly designed.

Just as designers solve everyday problems by creating new things, so can you too address the problems in your life by focusing on its design. Designers have tools and training at their disposal to help them in their work, and these blinks aim to give you the tools and training that will allow you to build a life that brings continuous joy.

You'll also find out

  * what the four most important parts of life are and how you balance them;

  * how coherency between your beliefs and what you do will give you a compass for life; and

  * why you want to design several different types of life.

### 2. Too many people are stuck in a rut, but you can design your way to a better life. 

If you feel completely content with your personal and professional life, then consider yourself lucky. Most people are unhappy with at least some aspects of their life.

For instance, many people worry that they've chosen the wrong profession.

In a poll of US workers, two-thirds of the respondents expressed dissatisfaction with their job, some of them saying they feel trapped because their diploma is in a field they'd rather not be in. They have a degree in civil engineering, for instance, and feel doomed to pursue a career in this field that they never really enjoyed in the first place.

Indeed, around 31 million Americans between the ages of 44 and 70 would choose a different _encore career_ if they could, one that gives them a sense of personal meaning and societal impact, as well as a steady paycheck, but they see no way of getting there.

With all these unsatisfied workers, what we need is a new way of designing our lives to meet our needs and desires.

A degree shouldn't limit your options. After all, three-fourths of US college graduates end up in a field that has nothing to do with the subject they majored in. You may have chosen to get a certain degree, but it needn't determine your future.

Whatever your age, background or profession, thinking like a designer and creating a _well-designed life_ can bring you more satisfaction and joy.

A well-designed life is one that allows you to be creative, happy and fulfilled. It helps you to avoid feeling stuck and unproductive while providing you with a sense of freedom — freedom to change and grow, in both your personal and professional life — no matter how old you are.

Everything around you is designed: your house or apartment, the chair you sit on and the computers and devices you use. So why not design your life, too?

### 3. Take stock of the four key elements of life and then identify where you have problems. 

If you're trying to get directions from Google Maps, you need to know more than just the destination; you also need to know your current location. The same is true when you start designing your life.

The first step to figuring out your current situation is to assess four critical areas in your life: _health_, w _ork_, _play_ and _love._

_Health_ includes your emotional, mental and physical health.

_Work_ includes both paid and volunteer jobs.

_Play_ is any activity done simply for the fun of it.

And _love_ applies to partners, children, friends and pets.

The goal is to find a healthy balance among all four of these areas, and the precise balance is totally up to you. Someone young will likely give more importance to play, while an elderly person might focus more on health.

After you've taken stock of these areas, it should become apparent to you if one is being neglected. For example, maybe you're too focused on work, and it has caused both your health and your love life to suffer? Or maybe you've been too immersed in play and have neglected your career aspirations?

Once you've made this assessment, it's time to assume a _beginner's mind_ and start asking the questions a complete novice would. A beginner's mind is a great perspective to have whenever you're facing a life-changing decision, such as which career to choose.

Let's say you're interested in studying marine biology because you love seals. Before you make the decision to start applying to colleges, approach the situation like a beginner and ask yourself basic questions like "What is the life of a marine biologist really like?" and "How much of it really involves seals?"

This will help you find the right direction. Maybe you should do some volunteer work on a research vessel to get a better idea of what the day to day is really like? If you approach situations with a beginner's mind and ask the right questions, you'll reduce the risk of regretting your choices later down the line.

### 4. Let your workview and lifeview guide you to a career that you’ll find meaningful and satisfying. 

A great many discoveries were made by explorers who had little more than a compass to guide them. When entering the great unknown of your future, you can use a compass as well.

In this case, the two guiding poles of your life are your _workview_ and your _lifeview_.

Your workview is your own philosophy on work and what it means to you. Everyone has their own ideas about what "good work" entails and how important money and the societal impact are to their job satisfaction. Your idea of "good work" might be whatever earns enough to pay the bills, so long as you don't have to work on weekends.

Likewise, your lifeview is your philosophy on life, meaning your personal values and perspectives on how it should be lived. This view covers things like how important religion is in your life, how you think society should function and what you consider to be moral and just. Your lifeview might, for instance, place a lot of importance on the environment and lead you to live a self-sustainable life on a farm somewhere.

When you start designing your life, reflect on these two areas and write down at least 250 words to accurately describe both. Try not to spend more than half an hour on each description.

Now, the goal is to find a coherent balance between the beliefs in your lifeview and the kind of job your workview suggests; if you can manage this balance, your compass will be correctly aligned.

Without the right balance, you're bound to feel discontent and unhappy sooner or later, because you've compromised your beliefs, and therefore your integrity. If your lifeview says the planet must be treated with care, then you don't want to take a job with a corporation that dumps chemicals into the ocean every day, right? And if your workview says that money and prestige are important to you, you need to choose a career that can realistically deliver those.

By taking both views into consideration when designing your life, you should be able to stay on course and know when to adjust your route.

Now that our compass is set let's put it to use.

### 5. Log the times and activities that keep you engaged, energetic or in a state of flow. 

With your compass in proper working order, let's take a closer look at _engagement_.

To live a joyful life, it's important to know what engages and disengages you. And for this, a _Good Time Journal_ can be an invaluable tool. This is a place to write down your experiences, in both good times and bad, and doing so will help you understand what brings joy to your life.

Throughout each day, wherever you are, write down how engaged and focused you feel, also noting if you're unhappy or bored. Also, jot down what you were doing at the time.

One thing to make special note of is when you experience _flow_, where you become completely immersed in an activity and time seems to fly by. Maybe it happens when you're researching a legal document or when you're chopping carrots. Maybe you feel it when you play sports. It depends on you.

If you ever emerge from being in the zone and are surprised by how much time has passed, be sure to make note of what you were doing. These moments are a great indicator of your engagement, and you should keep them in mind when designing both your career and your life.

Another clue to help you find your way in life are your _energy levels_.

Every day involves both physical and mental work, but some activities take a bigger toll than others. And some activities can even _raise_ your energy levels.

Use your Good Time Journal to keep track of which activities drain your energy and which ones help to sustain it. If you feel completely drained after sitting in meetings or doing data entry, but feel energized after pitching products to a potential customer, make a note of it.

Understanding how to stay energized and make the most of your day will be useful information for designing your life.

In the next blink, we'll look at what to do when your life has you feeling neither engaged nor energized, just stuck.

> _"Since there's no one destination in life, you can't put your goal into your GPS and get the turn-by-turn directions for how to get there."_

### 6. Get unstuck by generating new ideas with a mind map. 

At one time or another, most of us have felt stuck.

Just consider the story of Grant, who was working at a car-rental company. Even though he had recently been offered a promotion, he still loathed having to deal with angry customers and boring contracts. He felt like a mere cog in the corporate machine with nothing to keep him engaged or energized.

Like a lot of people who don't like their jobs, Grant looked to the weekend for engagement. He enjoyed hiking and playing basketball, but these energizing activities couldn't pay the bills. In college, he'd studied literature, but he'd felt the need to jump at the first decent paying job he found, which is how he got stuck in the car-rental business.

Eventually, Grant got unstuck, after he did some _mind mapping_.

Because he knew he enjoyed doing things outdoors, Grant picked "outdoor activities" as his starting point and wrote this in the center of a sheet of paper. He began to free associate and jot down what popped into his head, like "hiking," "surfing" and "travel." For each of these, he made secondary associations. For hiking, it was "mountains" and "exploring"; for "travel" he wrote down "Hawaii" and "tropical beach."

Now Grant could start to put these ideas together into a plan. He thought about accepting the promotion the company had offered, but only if he could relocate to another office where he could surf and enjoy the beaches, such as California or Hawaii.

This is how mind mapping works. It allows you to open your mind to new ideas that you otherwise might not have thought of. With just a pen and paper you can begin to find your new path.

### 7. Give yourself options and alternatives by designing multiple lives and parallel paths. 

Most of us have regrets from time to time. If only I'd played that one situation slightly differently, we think, everything would be better today! But the truth is, there's no single, specific choice that will lead to a perfect life.

In fact, there isn't a single best or perfect way to live a life, period. Throughout your life, you'll have many options to choose from, even if they aren't always apparent.

This is why it's important to think ahead and plan, or design, many possible lives for yourself — not just one.

Consider the story of Chung: when he graduated from UC Berkeley, he was offered four different internships, three of which were ones he'd hoped to receive. But he hadn't expected to be in the position to choose among three great opportunities.

At first, he felt immense pressure to choose the right path. After all, this would likely influence his choice of graduate degree, his career and the rest of his life. But then it hit him: there's nothing stopping him from doing all three one after the other — which is exactly what he chose to do.

But then came another twist: while doing his first internship, Chung spent a lot of time talking to his friends via Skype. He was so pleased with his decision to do all three internships that he felt motivated to help them with their career choices, too.

In the end, he realized he enjoyed this so much that he skipped the other internships and began studying career counseling in grad school.

You can do a similar thing by charting three different paths, which the author calls _odyssey plans_.

Life is an odyssey, with a variety of possible routes and circumstances, so it's wise to give yourself at least three options. But don't rank them from worst to best. Instead, have an open mind and look at them as three equal, and equally possible, plans. An example of three equal plans could be, for instance, becoming an ecological farmer, journalist or kibbutz volunteer.

At the Stanford Graduate School of Education, professor Dan Schwartz and his colleagues have observed that people who have multiple ideas from the get-go as to how to solve a problem are more likely to come up with innovative solutions than people who start out with just one idea. So don't settle for a single path. Keep an open mind!

This way, if your first choice doesn't work out, you won't panic about not having a perfectly viable plan B to turn to.

### 8. Final summary 

The key message in this book:

**You can be the designer of your own life. First, focus on your current situation, and the balance of work, health, play and love in your life. Then understand your workview and lifeview. Next, do some mind mapping to figure out three solid options of where you want to go.**

Actionable advice:

**Stuff keywords into your CV**.

If you discover that you're in the wrong line of work and decide to look for a new job, you should be aware that many companies often just automatically scan incoming CVs for certain keywords like a "Phd," "creative" or "motivated". Therefore, to ensure that your CV gets to the top of the pile that actual humans look at, you should stuff as many of those keywords into your CV as possible. Look at the job description for guidance as to which ones to include.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Geography of Bliss_** **by Eric Weiner**

_The Geography of Bliss_ (2008) asks which nations are the happiest on Earth, and what it is about these countries that makes their citizens so joyful. The answers to these questions reveal some fundamental truths about our many cultural differences, as well as the many similarities and contradictions we share.
---

### Bill Burnett and Dave Evans

Bill Burnett has a master's degree in product design from Stanford University, where he now teaches and heads the Design Program. With over thirty years of experience, he is an expert who still draws, designs and builds things every day.

Dave Evans also teaches at Stanford as part of the Product Design Program. After a varied career path that took him from marine biology to mechanical engineering, he worked at Apple and eventually cofounded Electronic Arts, where he still offers his services as a consultant.

