---
id: 5a53443ab238e100063370d7
slug: developing-the-leader-within-you-en
published_date: 2018-01-11T00:00:00.000+00:00
author: John C. Maxwell
title: Developing the Leader Within You
subtitle: None
main_color: F03050
text_color: A32136
---

# Developing the Leader Within You

_None_

**John C. Maxwell**

_Developing the Leader Within You_ (1993) is a guide to becoming a leader in whatever context you choose. These blinks lay out the levels of leadership, the qualities necessary to achieve them and the concrete steps you can take to move up the leadership ladder.

---
### 1. What’s in it for me? Grow your organization by growing yourself. 

On the battlefields and in the boardroom, in classrooms and in courtrooms, leaders play a crucial role: they bring people and organizations together, guiding them along a particular path and giving them courage to carry on. But what exactly makes a leader? And how can you become one?

Well, leadership comes in many different forms and leaders operate with varying degrees of sophistication. This hierarchy can be thought of as a leadership ladder, and advancing up it is a long process that requires learning self-discipline, how to prioritize tasks and how to infuse every action with integrity and vision. In short, a leader knows that her personal growth is central to the growth of her organization.

If you can commit to this process, you'll become a better leader in no time, embodying the word in everything you do.

In these blinks, you'll learn

  * why a willingness to change is crucial to success;

  * how Henry Ford almost failed; and

  * why actions say more than words.

### 2. Leadership is always about influencing others, but it can take several distinct forms. 

Everybody — from football coaches to business executives to full-time mothers — knows that _leadership_ is an essential quality of powerful people. But what exactly is it?

In a simple sense, leadership is just _influence_ ; it's the ability to inspire others to follow you. In other words, all moral considerations aside, anybody who can attract a group of followers qualifies as an effective leader. That includes the likes of John F. Kennedy, Winston Churchill and even Adolf Hitler. All these people were leaders simply because people followed them.

That being said, it should also be noted that there's a hierarchy of leadership. To be exact, there are five different levels: _position, permission, production, people development_ and _personhood._

The first, position, is the lowest level of leadership. It's about having the _right_ to be a leader. To put it differently, leaders of position are solely leaders because of their titles.

The second level, permission, is about relationships. If you've attained this level of leadership, people follow you because you're tuned into their needs and wishes; you listen to and value the opinions of others. As a result, leadership of this type won't last long if you neglect the needs of others. They'll soon lose interest and stop following you.

The third level of leadership, production, is attained when relationships such as those from the second level start to produce results. At this level, your leadership is the result of your and your team's success. People admire those results and are therefore happy to follow you.

If you manage to attain the fourth level, people development, people will follow you because they value your mentorship. At this level, a leader takes care to grow the skills of others, who respond with absolute loyalty. In cases like this, people follow you because they're thankful.

The final tier of leadership is personhood. While it's certainly attainable, it's only reached by leaders who spend their entire lives influencing others. At this level, you're a person who commands respect, which is what inspires others to follow you.

Which level describes your current leadership status? Regardless of where you're at, in the blinks that follow, you'll learn how to climb the ladder and excel as a leader!

### 3. Leadership is about setting priorities at multiple levels. 

So you want to develop your leadership abilities. Well, then you better know about _priorities_. Having solid priorities will enable you to determine what's most important and who you should focus on. It sounds simple enough, but good leaders prioritize on more than one level.

For one, you have to be able to prioritize different projects and tasks. Some of these will be important, others urgent and some will be both. This last category is the one to prioritize.

For instance, writing a report on sales figures may be important, but not that urgent. Such tasks should be a part of the everyday work of your team, but they should be worked toward deliberately, not rushed.

Then there are projects and tasks that aren't important, but that _are_ urgent. Checking your email is a prime example here. It should be done quickly so you can move on to the work that matters.

And, finally, there's work that's neither important nor urgent, like filing documents, which can be done in brief chunks of time, maybe just once a week.

Being able to break things down like this is key, because if you fail to set the right priorities, you're bound to lead your organization in the wrong direction. But tasks and projects are just one level; you also need to be able to prioritize the energy, money, time and staff devoted to any given project or task. To do so, apply the _20/80 principle._

That is, spend 80 percent of your resources — like money and energy — on the top 20 percent of your organizational priorities. Similarly, you should plan to dedicate 80 percent of your time to the top 20 percent of your most productive staff, who account for 80 percent of your organization's success. Every priority below this, the remaining 80 percent, should be delegated and outsourced. And, naturally, new personnel should be trained by the top 20 percent of your people.

To put it simply, prioritize the best people, tasks and projects. Investing in them will result in the most success.

### 4. A leader acts on her convictions and backs up her words with actions. 

Now that you know how to set priorities, it's time to talk about another core leadership concept: _integrity_. To have integrity is to adhere to a set of values without fail; these values are your compass, guiding everything you do. To put it differently, people with integrity act based on who they are.

But that's not all it means. Integrity is also about being consistent and doing what you say you'll do. Being able to follow through like this will inspire people to trust and follow you.

For example, research from Stanford University that indicates that the vast majority of learning — 89 percent of it! — is visual. In other words, if the people you lead hear you say that your job is to focus on the customer and then see you paying special attention to customers on a regular basis, they'll eventually start doing the same.

Being able to maintain such consistency will reinforce your credibility as a leader because people will know who you are and what they can expect from you. This in turn will produce trust, which is crucial to your success.

After all, would you follow the advice of someone you didn't trust?

Or imagine being in the army. Would you follow your commanding officer into a dangerous situation if you thought he was a phony or worried that he might reverse his decisions at the last minute?

Clearly, trust is essential to leadership, and integrity can help you build it. Integrity will also be the foundation of your long-lasting reputation. As an example, think about what it's like to meet for the first time someone you idolize.

Many people are disappointed during such meetings because the idolized individual doesn't turn out to be the person he seemed to be from afar. Maybe he appeared to be making clear decisions, but in reality is just blowing in the wind.

On the other hand, people with integrity secure a reputation that endures. That's because, if you have integrity, there'll be no difference between the person you seem to be from afar and the person you are in meetings and day-to-day interactions.

### 5. Leaders must embrace change and support their followers. 

You might think you're a pretty good leader, but how can you be sure?

One way is to assess how beneficial your work has been. In fact, this criterion is so important that _creating positive change_ is the third principle of leadership. Leaders need to be ever ready to change; if they're not, their inflexibility can negatively affect the entire organization. A leader must be able to adapt.

For instance, when Henry Ford created the Model T, he became so infatuated with the car that he refused to consider changing a single detail; he believed it was perfect as it was. Because of this, he even fired his top production official, William Knudsen, who suggested a new design.

Ford may have been a manufacturing genius, but such a rigid resistance to change was a major misstep. Remember, the world moves at a rapid pace and competition is fierce. To succeed in this environment, your business has to embrace the possibility that major shifts will occur. If _you_ can't change, your organization won't be able to, either.

In Ford's case, changing the Model T was simply necessary. Despite what Ford thought about the car, it _was_ old-fashioned and General Motors had already started developing more modern vehicles that the Model T would have to compete with. While Ford remained reluctant for years, he finally did update the Model T, releasing the Model A and maintaining his strong position in the automobile industry.

But, as a leader, you must do more than simply embrace change. You also need to recognize the psychological challenges that change can impose on your employees.

Change requires employees to step out of their comfort zones and face dramatically new contexts, and this is bound to lead to stress and insecurity. So to help employees ease into this process, you should do everything you can to ensure that they know in advance why the change is happening as well as the objectives it seeks to realize. By keeping your employees informed, you give them the chance to take ownership of the change, which will reduce its negative impacts.

### 6. Self-discipline is crucial to leading others, but it can’t be rushed. 

Do you take pride in your self-discipline? Can you accomplish everything you set out to do without anybody cheering you on from the sidelines?

Well, if not, you better learn how to, because self-discipline is a key trait of all leaders. If you can't follow your _own_ lead, how can you expect others to?

To put it differently, as a leader, your growth is essential to the growth of your organization. That being said, growth requires hard work. And this is where many people become their own worst enemy; they lack self-discipline and, as a result, fall short as leaders.

So get started today with a small but committed plan that involves a daily routine. Starting slow is important as it takes time to build up self-discipline and nobody can do everything at once.

Consider the basketball team of Indiana University. In 1976, they won the NCAA National Championship. After the victory, their coach did an interview on _60 Minutes_, explaining that the primary reason for the team's success was its self-discipline and willingness to continually prepare and improve.

Here's what you can do to set yourself up for the same kind of success:

Begin by writing down and ranking five areas where you want to become more disciplined. Then find someone who is already disciplined in the area you put as your top priority and ask him or her to help you check your progress.

From there, spend 15 minutes every morning focusing on this area of improvement and take five minutes twice every day — once at noon and once in the evening — to reflect on your progress.

If you repeat this daily routine for 60 days, you should have built up the self-discipline you need for success in this area, and you can cross it off your list and move on to the next priority.

And don't forget that it's important to celebrate when you master a new area, preferably with the person who helped you through the process!

### 7. Good leaders have a strong vision that they find deep within themselves. 

Do you consider yourself a visionary person?

Well, to be a leader, you have to have a vision of where you're headed and how you'll persuade others to follow you there. In this sense, a vision is simply a clear idea of what the organization in general, and your team in particular, should do. Your vision is what fuels your day-to-day work.

For instance, did you know that Beethoven was deaf for the final 20 years of his life, including during the time he wrote some of his most famous compositions? Or did you know that Homer was blind and that Rockefeller, when he first started working, earned just six dollars per week?

Nonetheless, all these great figures accomplished incredible things because they believed deeply in what they wanted to achieve. In other words, they had a vision and lived by it.

Conversely, if you don't have a vision, it'll be difficult to lead others toward a goal. In fact, doing so is simply not possible without the drive, perseverance and team spirit that a vision brings.

Okay, so if you feel that you lack a vision, what can you do?

Well, try thinking about what you feel deep down inside yourself. What do you dream of? What keeps you going day after day?

Ask these questions in regards to every aspect of your life. Remember: Only when you see where you're headed will you be able to get there.

But keep in mind that a vision alone doesn't make a leader; you also need the experience to walk the path you set out on.

Such experience is what sets a vision apart from naive idealism. For example, if you lack experience, you might expect people to follow your vision blindly, never asking questions. But if you have led others before and have a wealth of leadership experiences under your belt, you'll know that people won't just trustingly follow a vision laid out by some random person. People follow leaders. Once your leadership attracts followers, it'll be easy to get them to share your vision as well.

### 8. Final summary 

The key message in this book:

**Leadership isn't about being bossy and ordering others around. Rather, good leaders set strong examples and work diligently to improve themselves personally. They live by their values, are open to change and are guided by a vision that they summon from deep within themselves.**

Actionable advice:

**A good leader listens to his team.**

The next time one of your team members comes to you with a question or concern, be sure to listen carefully to whatever is voiced. It's important to focus on more than the literal words that are being spoken. Pay attention to tone of voice and body language, and if the team member says something you disagree with, don't get annoyed or interrupt. By listening in this way, you might gain valuable information that you'd otherwise have missed entirely.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Thou Shall Prosper_** **by Daniel Lapin**

_Thou Shall Prosper_ (2009) offers a revealing look at what Jewish principles can teach us about building wealth and finding success. By adhering to these principles, one can survive and thrive in today's volatile economy.
---

### John C. Maxwell

John C. Maxwell has over 20 years of experience studying and teaching leadership all over the United States. In addition to being an expert on leadership development and a frequent speaker on the topic, he's a pastor and the author of several books, including _The 21 Irrefutable Laws of Leadership_ and _The 21 Indispensable Qualities of a Leader._

