---
id: 55237ed73965360007000000
slug: difference-en
published_date: 2015-04-07T00:00:00.000+00:00
author: Bernadette Jiwa
title: Difference
subtitle: The One-Page Method for Reimagining Your Business and Reinventing Your Market
main_color: EA2F34
text_color: B82528
---

# Difference

_The One-Page Method for Reimagining Your Business and Reinventing Your Market_

**Bernadette Jiwa**

In _Difference_ (2014), author Bernadette Jiwa explains how you can stand out and stay competitive in today's riotous business world. To cut through the noise of advertising everywhere, you have to connect with people emotionally and offer them something _more_. This book shows you how!

---
### 1. What’s in it for me? Learn how to create an emotional bond between you and your customers. 

Be honest — do you feel an instinctive loyalty to a product you purchase often? Probably not. Most of the time, we buy products based on price or simple convenience. If something else came along that was cheaper or easier to find, you'd make the switch without a second glance.

But there are a few brands about which people seem to feel deeply. Services like Airbnb, companies like Apple and social media sites like Facebook have a devoted fan base made up of passionate people who are indeed loyal to the company and all it offers and does.

Yet what is it about these companies that inspires such love? And how do you create the same sort of buzz for your own brand?

As you will find out in these blinks, these brands take the traditional "Ps" of marketing and add one more important element: _people._ They place us, the customer, our problems and our lives, at the center of everything they do. And in return, we love them for it.

In the following blinks, you will discover

  * why innovation doesn't start with a product, but with a truth;

  * how Airbnb changed the way we travel completely; and

  * why charitable giving can help a company sell more shoes.

### 2. If you want to be successful, you can't just be different – you've got to create difference. 

In the past, the path to creating, developing and marketing a product was straightforward.

First, you had an idea: a new teapot. You'd develop your teapot, deciding on the curve of its handle, the color of its glaze. Then you'd try to start selling your teapot, targeting your advertising to areas or demographics where you might find some potential customers.

But this kind of thinking doesn't work anymore. You can't just rely on your fantastic teapot eclipsing all your competitors. There are so many products available today that it's nearly impossible to truly make your teapot stand out!

If you want to be successful, you've got to think in a new way. You can't just try to be different; you've got to _create_ difference.

Creating difference isn't about beating the competition but about doing something they've never done before. It's about _reinventing your industry_.

You have to think about what people really need if you want to create difference. You have to solve a problem for them and truly make their lives better. A new shiny teapot alone won't do that.

This means your creative process has to be different, too. Your product doesn't come first; what you need first is a _truth_. A truth about someone's problem. Then you think about solving that problem. Creating and launching the actual product comes at the very last, after this period of empathetic thinking. This is how you make your company stand out in an overcrowded market.

Warby Parker, an eyewear retailer, found success by finding a truth about eyeglass wearers and solving it.

The company founders realized that people who might not have hundreds of dollars to buy name-brand, stylish glasses still had the real desire to wear them. Yet there were no affordable options on the market for these customers.

Warby Parker recognized this and became successful by solving this problem — creating stylish and affordable eyewear — for this market segment.

### 3. Stop yelling at your customers with impersonal ads. Tell a story that they can relate to and care about. 

How many advertisements do you think you've come across today? Probably hundreds.

While we're surrounded by ads, it's never been clearer that traditional advertising just doesn't work anymore, as there's too much competition. Why should we pay attention to any given ad?

Moreover, traditional ads usually don't give a potential customer reason to really _care_ about whatever a company is advertising. Messages are generally designed to appeal to the largest number of people possible, without targeting specific groups or personalities.

Such ads don't try to solve our problems or fulfill our desires, they just shout at us. "Buy this! We're slightly different!"

If everyone is shouting, it's easy to ignore the noise, and most people can tune it out. What's more, you're an individual, and you want to be treated as one. Thus companies need to appeal to people in a personal way, if they want potential customers to tune in and listen.

Effective, modern marketing isn't about yelling, but instead about telling stories potential customers can connect to or identify with. If your story shares something meaningful about someone's life, for example, your potential customer may be much more likely to listen.

Companies that tell a story with which their customers can empathize are more often than not successful!

The online vacation rental company Airbnb, for example, tells a great story. The company knows that hotels are expensive and impersonal. Thus Airbnb doesn't just offer cheaper hotel rooms, they offer a whole new experience, encouraging customers to explore the world in a new way.

So how do you create a story your customers can connect to? You need to follow the _difference model_, which focuses on the six P's: _principles, purpose, people, personal, perceptions_ and _product_.

We'll look at each of these elements in more detail in the next blinks.

### 4. To focus on your principles, find the fundamental truths of your business, market and customers. 

The first step in following the _difference model_ is to focus on your _principles_.

Your principles are the fundamental truths behind your business, market and customers.

First, you have to identify these truths. What are your strengths and weaknesses? How can you take these into account and use them to reach your goals?

Let's look at Airbnb again. The company recognized the truth that it had very little capital to build a business. Yet it did have a wealth of creativity and design skills. The company founders also had a real desire to change people's lives.

So Airbnb's founders built their company on the skills they had, playing to their natural strengths.

You also have to recognize the truths behind your industry. What determines exactly how your industry works? How can you use these truths to your advantage, or how can you change them?

Apple's Steve Jobs did this when considering a design for a new phone. He recognized that an important principle of the industry was that mobile phones had to be functional. He exploited this by developing a phone that could function not only as a mobile phone but also much more.

Finally, you need to analyze your customers' principles. Think about the people whose lives you want to change. What truths lie behind their behavior? What truths limit them or make them unhappy? What do they desire?

After you are able to answer these questions, you can work on making a difference in their lives.

Entrepreneur Sara Blakey did just this when she designed her line of undergarments, called _Spanx_. She recognized the truth that many women were unhappy with the visible lines caused by poorly fitting underwear. So she developed a range of body-shaping clothes that eliminated these lines, and quickly became enormously successful.

### 5. Why does your business do what it does? Define your purpose to give your company direction. 

The most important thing you have to identify in business is your _purpose_.

In short, your purpose is the very reason your business exists.

So the second step in the _difference model_ is to carefully analyze what you want to achieve. Think about the impact you want your business to have on the world.

_By the Way Bakery_ — a small, old-fashioned bakery in New York — built its business in this fashion. The bakery only serves gluten and dairy-free desserts, creating top-quality products even though the ingredients they can use are obviously limited.

Importantly, the bakery founders wanted to give customers a great experience and feed them delicious food, regardless of their dietary restrictions.

Having a strong purpose like this gives you and your company direction and keeps you on track.

When you think about the impact you want your business to have, remember to focus on the people. Don't think of your customers as some abstract demographic or a bunch of numbers in a customer data chart!

Treat both customers and potential customers as actual _humans_ with emotions, desires and problems. You can't do anything to help them unless you understand them as the complicated, multifaceted beings they are.

Online bank _Simple.com_ built its success on understanding its customers and their needs. The company founders understood that traditional banks often make people feel powerless, or worse, stupid. They also understood that people often dislike or distrust institutional banks, and thought that perhaps customers might feel more comfortable banking online.

Thus Simple.com was born as an online banking service to address exactly this problem, and this purpose is what made the company stand out.

### 6. Create something to which people can personally connect and perceive in the right way. 

It's vital to understand your customers' thoughts and wishes, but this alone won't make your business successful. You also have to ensure that people _care_ about what you're doing.

Thus the next steps in the difference model is to make a _personal_ connection with customers to ensure that they _perceive_ the difference in the quality of services or products that you offer.

Whatever your product or service, it has to resonate with the lives that your potential customers are living. That is, people should feel good when they're using your product or service.

The shoe company TOMS offers a great example. TOMS promises that for every pair of shoes you buy from the company, they donate another pair to a child in the developing world. The company calls this their "one for one" model.

This model definitely makes people feel good about buying TOMS shoes, as every time they put their shoes on, they remember how they've helped a child in need.

So think about people's beliefs and ideas, and make your business match those ideas. Offer people a product or service that they can feel good about!

Taxi service Uber became successful in this way. The company realized that people had grown frustrated with taxi services, as they are often unreliable and expensive. In short, people distrusted the taxi industry.

So Uber set out to offer a transportation service that people could actually enjoy, and more importantly, come to trust. The customer's _perception_ of the honesty and integrity of the company made a difference.

The company didn't just set out to solve entrenched problems with the taxi industry but essentially changed the way we use transportation in general.

Uber has in turn changed people's ideas about the industry at large. People now expect cheaper and more reliable taxi services, because Uber set a new standard for the industry.

### 7. Once you understand your market, your customers and yourself, you can then create your product. 

So after you've spent time researching and thinking about your customers, your business and your industry, you can finally move to the last step in the difference model: developing your _product_.

Remember that your product has to fill a particular void in people's lives, giving them something that they really need.

This means you shouldn't create a product just because you think it could somehow plug a gap in the market or compete with similar products already out there. Your product has to make a real difference for people. It has to change something about your customer's life!

Airbnb again offers a great example. The company could have operated just like a hotel, but instead of hotel rooms, offer rooms in private homes. You could simply book online, stay and then leave.

Instead, the company went further. Airbnb made it so travelers could interact and connect with the owners of the house or flat, and both sides could talk to each other, offering feedback. The company offered a great customer service experience with the chance to comfortably explore the world.

Airbnb didn't just make it easier to find a place to sleep, the company changed the way we travel!

There's never been a better time to figure out what people need and make a difference in their lives.

So what are you waiting for?

Social media is an incredible tool to help you get your message across, allowing you to make meaningful and empathetic connections with strangers in all sorts of unprecedented ways.

Doing your research on social media is a great place to start. Get a feel for how people live their lives, see what kinds of problems they're talking about and then focus on how to help them. Solving real problems is the key to your success!

### 8. Final summary 

The key message in this book:

**Traditional marketing is dead. You can't get attention anymore through having flashier ads than your competitors! You've got to connect with people in a more meaningful way. When you figure out how to solve a problem for people, you won't just make them happier — you'll be more successful too.**

Actionable advice:

**Don't try to appeal to the masses.**

People don't respond when you treat them as if they're all the same. You have to appeal to your potential customers as individuals, with unique feelings and thoughts. Tailor your marketing to the people you're really trying to help. If you try to appeal to everyone, you'll end up appealing to no one

**Suggested further reading: _The Brand Gap_ by Marty Neumeier**

In _The Brand Gap,_ you'll get the inside scoop on how a strong brand can give your company a competitive edge. When you learn how to implement the five branding disciplines outlined in this book, you'll understand that in closing the gap between strategy and creativity, you'll be able to build an irresistible brand that will make customers take notice...
---

### Bernadette Jiwa

Bernadette Jiwa is an author and speaker who specializes in teaching businesses and entrepreneurs how to adapt marketing strategies for the modern age. Her book, _The Fortune Cookie Principle,_ was a business bestseller on Amazon.

