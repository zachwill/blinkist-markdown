---
id: 53db94df6433330007020000
slug: diffusion-of-innovations-en
published_date: 2014-08-05T00:00:00.000+00:00
author: Everett M. Rogers
title: Diffusion of Innovations
subtitle: None
main_color: EAD338
text_color: 6B611A
---

# Diffusion of Innovations

_None_

**Everett M. Rogers**

_Diffusion_ _of_ _Innovation_ s is an examination of the many ways that new ideas and technologies come to be adopted by users. In particular, this book describes in detail the stages an innovation passes through on its route to adoption, the different types of people that adopt innovations and how the process of diffusion can be influenced.

---
### 1. What’s in it for me? Find out how to increase the chances of your innovation being adopted. 

It seems that at any time and in any part of the world, you'll find an innovation beginning to take hold. Whether it's a new technology, like the now-ubiquitous smartphone, or an idea and practice, such as safe sex, innovations spread throughout a society in a systematic way.

This process is what author Everett M. Rogers calls "the diffusion of innovations."

In this groundbreaking book, the author describes the process in which an innovation comes to be adopted, whether an idea, a practice or an object, and identifies the five stages in the process: Knowledge, Persuasion, Decision, Implementation and Confirmation.

The potential user's path through these five stages essentially determines whether an innovation will succeed or fail. You'll learn what happens in each of these stages and find valuable advice about how to increase the chances that your innovation will be adopted.

In the following blinks, you'll also discover:

  * why some people in the German Democratic Republic filled their washing machines with fruit,

  * why people in countries such as China or Japan are more likely to adopt a certain innovation just because others are already using it,

  * why there are fewer and fewer smokers these days, and

  * why the elderly are always the last social group to purchase a smartphone.

### 2. In the Knowledge stage, an individual learns of the innovation’s existence. 

Think back to the last time you decided whether you were going to use a new product or service. Do you know what motivated your decision?

We may not be aware of it, but our decision making always follows a certain pattern, which comprises five stages.

The first is the _Knowledge_ stage, the moment that we first learn of a new technology or innovation. But what leads us to enter this stage? Is it that we have a specific, prior need for a particular technology? Or, is it simply that we have become aware of the innovation?

Some people actively seek out a particular innovation, such as a specific technology, because they have a certain need for it in their lives.

In this context, a need is a state of frustration or dissatisfaction which arises when an individual's desires exceed the currently available resources. This need will drive that person — who has a specific problem and a desire to fix it — to ask around and seek a solution.

For example, imagine that you're a farmer and that your crops are being destroyed by a particular pest. In such a case, you'll likely actively seek out a pesticide to deal with that bug.

Other people, however, enter the Knowledge stage not because they were actively searching for a particular innovation but because they were made aware of the innovation's existence.

Sometimes, the release of a new product into the market is communicated via channels that don't target _you_ specifically. However, you might learn of a product by word of mouth: perhaps a friend informs you about a new device they think you'd be interested in.

For instance, when the latest version of Apple's iPhone is announced at a developer conference, many technology magazines will provide details of its release and new features. As a result, one of your friends — perhaps a tech expert, always on the lookout for new gadgets — will read one of those magazines and, knowing you'd be interested, tell you all about the new iPhone.

> _"Individuals tend to expose themselves to ideas that are in accordance with their interests, needs and existing attitudes."_

### 3. In the Persuasion stage, an individual forms an attitude toward the innovation. 

However your knowledge of a product's existence comes about, once you _have_ learned of it, what happens then? Do you simply decide, without bias, to use that product?

In a word, no.

After the Knowledge stage, you form a favorable or unfavorable attitude toward the innovation. This is the _Persuasion_ stage.

First, you either like the product or you don't. If you do like it, you then have to research the product and find credible information about it. But the problem is, in the Persuasion stage, we're always selective about the information we seek, to the extent that we might ignore information that challenges our initial feelings about the product.

For example, if you find a product you believe makes you look cool, you might choose to ignore a review that discusses how unreliable the product is.

Following this, we then reconcile the attributes of the innovation with our own attitude toward it, and try to anticipate its future evolution. In other words, we balance our feelings about the product against aspects such as price, complexity and operation.

If we are still uncertain about the product, we try to anticipate its evolution. For example, if you purchase a new model of camera, you may feel uncertain as it's impossible early on to know whether the camera will be used by other people, or even whether you are using it correctly.

To handle this uncertainty, we anticipate our future use of the camera and we consider how much it costs, how hard it is to use and how our friends will perceive it.

Then, after doing your research and dealing with your uncertainties, if your attitude toward the product remains positive, you'll most likely start using it.

However, some innovations don't require that an adopter have a positive attitude toward the innovation. For instance, most people, including smokers themselves, have a negative attitude toward cigarettes. Yet most smokers will not give up their cigarette habit.

> __

### 4. In the Decision stage, an individual chooses to adopt or reject an innovation. 

Having sought out information about the new product and formed an attitude toward it, you're now going to decide whether to use it. In other words, you'll either _adopt_ the innovation (choose to make full use of it) or _reject_ it (decide not to use it).

This is the _Decision_ stage, and there are a few factors here that are pertinent to an innovation being adopted or rejected.

First, innovations that can be tried out before purchase are more likely to be adopted. And if they are found to have a relative advantage over other similar products — for instance, better functionality — they'll be adopted more quickly.

For example, in 1930s Iowa, corn seed salesmen handed out bags of a new seed to farmers, enough to plant approximately one acre of corn. Because this new seed was proven to be superior to that which they currently used, the farmers were convinced to purchase it.

Second, rejection can happen actively or passively.

In an _active_ _rejection_, an individual has considered adopting the innovation and has possibly tried it out, yet decided to not adopt it.

For example, a software developer considers switching to a new development kit to build his tools. However, after some testing, he finds the kit to be unsuitable for his needs and far too complex to use, so he actively decides to reject it.

A _passive_ _rejection_, on the other hand, occurs when an individual hasn't actually considered adopting the innovation or has simply forgotten about its existence.

Third, the decision to adopt a product is influenced mainly by cultural norms. People living in collectivist cultures — societies that value community and collective action over individual needs, such as China, Korea and Japan — are more likely to adopt an innovation if some members of their group have already adopted it.

For example, if a critical mass of Korean women decide to get an intrauterine device (IUD) fitted, it's likely that all women in their social group will follow suit.

### 5. In the Implementation stage, an individual begins using an innovation. 

So far, the decision process has mostly involved thinking about the innovation and deciding whether to adopt it. But once the decision has been made to adopt, things change. Now, in the _Implementation_ stage, an individual has to physically put an innovation to use.

But what does implementation actually involve?

Fundamentally, it involves a change in the user's behavior. Because the new product has to be put to use physically, the individual must actively change their behavior, in that they must learn and apply the methods that correctly using the product necessitates.

To implement an innovation, you have to obtain it, learn how to use it and find information about any potential difficulties or side effects. For example, a patient who begins taking a new drug to regulate high blood sugar must change her behavior — she cannot drink alcohol while taking the new drug, she must take it for several weeks before the drug will have any effect, and so on.

Interestingly enough, during the Implementation stage, many adopters "reinvent" the product or service by using it in ways not intended by its creators.

For example, in the former GDR — the German Democratic Republic — some people used the WM66 washing machine to preserve fruit, because the machine could be heated without having to start a wash cycle.

Moreover, a higher degree of reinvention can extend the lifespan of a product and lead to it being used more actively than expected.

For example, take Lego Mindstorms. In its original incarnation, its software wasn't open source, but after it was hacked by a few programmers, Mindstorms' active and dedicated fanbase were able to change it constantly, thereby extending its life significantly. Ultimately, Lego would embrace the opportunity by providing software development kits to Mindstorms fans.

### 6. In the Confirmation stage, an individual may reverse his decision to adopt. 

Say you've started putting an innovation to use. Does this mean you're going to use it forever? Perhaps some innovations are so great that they will last forever, but others you will stop using at some point.

Why is that?

After an individual has made a decision to adopt a certain innovation, he begins to seek assurance that he's made the right decision. In other words, he's entered the _Confirmation_ stage.

This stage is marked by _internal_ _dissonance_ in the individual — an uncomfortable state of mind that he wishes to diminish, and one which arises both before and after adopting a product.

Consider, for example, the early incarnations of Microsoft's operating system, Windows. When Apple built its own operating system, which was easier to use, those people who switched from Windows to Mac probably experienced internal dissonance prior to switching, as they were dissatisfied with Windows.

To sustain positive feelings about their decision, adopters of a new product often will try to avoid experiencing this dissonance by filtering out any information which disconfirms their decision. (This is called _selective_ _exposure_.) Instead, adopters seek positive affirmation of their decision: for example, sales data that suggests the increased popularity of Macs, positive product reviews and so on.

Sometimes, in the Confirmation stage, adopters reverse their decision and stop using the innovation.

Because there are always drawbacks to adoption — such as the possibility of disconfirming information reaching the adopter, or difficulties in using the innovation outweighing its anticipated benefits — an adopter might discontinue using a new product or service even after having implemented it.

Another reason might be that the innovation is replaced by another idea that is perceived as better. Yet another reason is that the innovation loses its luster because adopters are dissatisfied with its performance.

Take, for example, smoking, which was once a popular lifestyle choice. As more and more studies began to confirm the serious risks of smoking, the number of smokers decreased.

This was mainly because governments and non-profit organizations began to inform the population about these risks and proceeded to establish barriers to smoking — such as smoking restrictions on airplanes, no-smoking areas in bars and special taxes on cigarettes. As a result, the number of smokers has fallen drastically.

### 7. Those with the right knowledge and resources lead the way in adopting new innovations – that’s why they’re called innovators. 

When it comes to people adopting and making use of an innovation, there are always some individuals ahead of everyone else.

Why is that?

One reason is that certain individuals are simply more innovative than others.

For instance, innovators — the first to adopt a new product, idea or practice — make up just 2.5 percent of all adopters. They are people always on the lookout for new ideas, and part of a network of likeminded individuals from whom they receive news.

Take, for example, a person who loves watches — let's call him John. John and his friends are so interested in watches that they participate in online forums and devour magazines dedicated to their obsession. John's an _innovator_ : he's aware of the latest trends in watchmaking and knows which are the best new watches on the market.

But, like all innovators, John is in the minority. The remainder of adopters comprise _Early_ _Adopters_ (13.5 percent), the _Early_ _Majority_ (34 percent) and the _Late_ _Majority_ (34 percent). Those that adopt an innovation much later than the majority are called _Laggards_ (16 percent). For example, when it comes to smartphones, elderly people are usually Laggards: those last to purchase and use such a device.

Another reason that innovators are ahead of the pack is that they have a wealth of resources and knowledge about a given idea.

To be an innovator, an individual must have the financial resources to compensate for any losses incurred from purchasing previous innovations. Equally, they need the ability to understand and apply complex technical knowledge.

Also, this individual has to cope with a high degree of uncertainty about the future of the innovation. For example, people who purchased the very first mobile phones in the early 1980s had to spend a lot of money, understand when and how to use the phones and deal with the uncertainty of not knowing whether everyone else was going to be using the phones too.

### 8. An innovation’s relative advantage and compatibility drastically accelerate its rate of adoption. 

We have seen how our own attitudes and behavior influence whether we adopt or reject innovations. But the innovation itself plays a role, too: its attributes clearly impact whether people will adopt it.

First, innovations must have a _relative_ _advantage_ over other, competing innovations. In other words, _relative_ _advantage_ represents the perceived superiority of an innovation over the original idea that it supersedes.

It doesn't matter whether the field of the relative advantage is economic, social or otherwise. What _does_ matter is that a sizeable gap exists between the original product's attributes and those of the superseding innovation.

For example, the Iowa corn farmers mentioned in an earlier blink saw a clear economic advantage to using the new seeds they were given: their yields grew, as did their profits.

In contrast, for people interested in buying fashionable clothes, it's not economic factors that matter but rather the social prestige they acquire by wearing those clothes.

Second, to improve the odds of it being adopted, an innovation should be _compatible_ with the existing values and needs of users. This is because innovations that are compatible with users' values, and effectively fulfill their specific needs, are more likely to be adopted.

Take, for example, a new technology — like the wildly successful social networking site, Facebook. This innovation has a high compatibility with users' values, beliefs, ideas or needs: it meets the needs of people wanting to stay in touch with old pals and friends abroad.

Incompatible innovations, on the other hand, will probably fail to be adopted. For example, for two years a public health worker in a Peruvian village tried to convince inhabitants there to boil their drinking water. However, the campaign was ultimately unsuccessful.

Why?

The local tradition of the area connected the consumption of hot foods with being ill. The hotter the food or water, the more suitable it was for the sick to consume. The very idea that hot water could be associated with good health was incompatible with the beliefs and values of this village's people.

> _"Getting a new idea adopted, even when it has obvious advantages, is difficult."_

### 9. The complexity of an innovation can be a major obstacle to its adoption. 

Why were MacBooks, iPods and other Apple products so readily adopted by consumers?

Certainly Apple's marketing efforts, and the brand name itself, played an important role. But the primary cause of Apple's rapid success was the simplicity of its products. Indeed, the more complex an innovation is — the more difficult to understand and use — the slower people will be to adopt it. Therefore, products that are easy to grasp and use will be adopted more quickly than those that are complex.

Of course, this depends on the _kind_ of adopters we're talking about.

For example, the very first people to adopt home computers were hobbyists who were competent in their use of technology, such as engineers or scientists. The adopters who followed them, however, found computers difficult to understand and use as they lacked the required knowledge. These people had to ask friends, experiment themselves or join a computer club. Ultimately, in time, computers became more user-friendly and thus far more popular.

Moreover, easy-to-use innovations often have an edge when they also have a relative advantage — for example, if they are cheaper than existing products, or they confer social prestige.

When, for instance, the first cellular phones were introduced, they operated in the same way as regular phones. Eventually, however, SMS (short messaging service) became a standard feature, one that was simple to understand and use. Thus it was no surprise that SMS was so quickly adopted by young people. Texting was a cheap, quick and easy way of interacting with friends.

> _"Some innovations are clear in their meaning to potential adopters, while others are not."_

### 10. Being able to test a product before buying it increases the likelihood that you’ll purchase it. 

What was the last product you bought? Did you test it before you decided on the purchase?

It's a fact that innovations that can be tried before purchase spread much faster. This is called _trialability_ **–** the degree to which an innovation can be tested prior to adopting it.

By trying and testing a new product, potential adopters can dispel uncertainty about an innovation.

However, not all innovations can be devised for trial. For example, when buying a car, you can go to your car dealer and test-drive different brands and models. But when you're deciding which airline to use, you can't "test-fly"!

In addition, trialability is more important to _earlier_ _adopters_ **–** those that purchase and use an innovation before the masses adopt it — than later adopters. This is because earlier adopters have no precedent to follow, whereas later adopters can always refer to the trials of earlier adopters; they don't always need to try it out for themselves. Furthermore, later adopters — such as the Laggards — are quicker to commit to adopting an innovation.

Take, for example, those who bought a smartphone when nobody else had one. These adopters probably visited trade fairs or shops to see for themselves how the technology works.

But, now that almost everyone has a smartphone, new adopters don't need to test the technology (at least as intensively). Instead, they can simply trust product reviews or recommendations from friends.

As this shows, for innovators who begin to use an innovation when it initially comes out, trying it out first is very important. If, in contrast, you've purchased the product once everybody else has already used it, a trial session is less important.

Why is that? Partially it's because you have observed its functions through others — and you'll find out how much this matters in the next blink.

### 11. Innovations that can be observed in use, or communicated to others, are adopted more readily. 

While trialability allows you to test a product, thereby increasing the likelihood of its adoption, what also helps is allowing people to observe the product in action and communicating to them how a product works.

This is because explaining and showing a new product to others enables them to better understand its functions.

Some ideas are easy to observe and communicate to peers, which can help to dissipate any doubts they might have and enables them to assess the idea's complexity or its relative advantage. For example, the way in which people use Facebook is easy to observe and communicate, because one can see the activity on a user's profile.

However, there are innovations that are unobservable or difficult to communicate, such as the notion of religious salvation. Moreover, certain technologies have both observable and unobservable aspects; for example, the hardware and software of computers.

The _hardware_ _aspect_ is the material or physical form of the tool; the object itself. The _software_ _aspect_ is the information base for this tool. A computer's hardware, for example, is the electronic equipment, whereas its software is the operating system.

Because the software aspect tends to be less observable than the hardware, those innovations that have a dominant software aspect usually have a slower rate of adoption.

Take the idea of _safe_ _sex_, for example. This is a rather vague idea because the focus is on software elements: abstinence, sexual monogamy and other safe sex recommendations are hard to observe, and are not hardware-dominant. This is why safe sex campaigns have had only middling success.

As we can see, it is sometimes difficult to explain and communicate an idea. When you do, try to make the idea tangible by highlighting its hardware aspect.

### 12. Change agents can develop a need for change and thus speed the adoption of an innovation. 

As we've seen, people adopt innovations in their own way and at their own speed. But one other factor makes a big difference to whether an innovation is adopted: the _change_ _agent_.

What is a change agent, and how does this individual influence the adoption process?

Change agents — such as teachers, consultants, public health workers and so on — make potential adopters aware of a particular need for change, and they build rapport.

These agents have strong ties to, and communication channels within, the social system that will adopt an innovation. Also, they can identify new solutions to existing problems and, in doing so, change behaviors. They can communicate the importance of a problem and can convince individuals or whole groups that they are capable of tackling their challenges.

For example, Jen — who has lived in Kenya for five years — is a public health worker with close ties to schools and government officials. She has made a great effort to present the impact of cervical cancer to teachers and officials, and to discuss with parents the need to get girls vaccinated. Due to this effort, and her status within the community, an increasing number of girls are vaccinated.

A change agent should be an empathetic person and act always in the interests of the client. To be effective, she should focus on her clients' needs instead of the change agency's demands. (A _change_ _agency_ is the agent's employer, which can be a non-profit organization, government or consultancy.)

Such empathetic change agents are more feedback-minded, build closer rapport with individuals and enjoy higher credibility.

For example, a salesman who tries to convince a potential client to purchase his company's new fertilizer has a greater chance of selling if he points out how the product can solve an existing problem that the client has.

If, on the other hand, the salesman is not focused on the individual's needs and thus cannot provide a solution to his problems, his efforts will fail.

### 13. People in a position of influence have a great impact on whether an individual adopts an innovation. 

Imagine you've created a new, innovative website. Once the site is up and running, you want your target audience to find it and start using it.

But how do you do that?

One way is to seek out an _opinion_ _leader_ — a person who has a great influence over other individuals' attitudes and behaviors.

Opinion leaders have an extensive interpersonal network of followers, and are more active in participating within their social groups. They also have more extensive access to mass media than their followers.

Opinion leaders also often have a higher socioeconomic status and are recognized as competent and trustworthy experts when it comes to innovations.

For example, an opinion leader in the field of internet media could be a prominent blogger and expert who covers new websites and evaluates their functionality. Because of his position, this opinion leader can reach out to his followers and encourage them to visit the site.

Another reason why it's worth seeking out an opinion leader is that the success rate of change agents (discussed in the previous blink) is even higher when an agent works in collaboration with opinion leaders.

This is because a change agent working alone usually can't address all potential adopters individually. Yet, by working with an opinion leader, the agent can reach many more individuals.

For example, in certain parts of Latin America, there are agricultural extension workers responsible for introducing ideas and products to 10,000 farmers. Reaching this many farmers was effectively impossible for a single extension worker, but by getting support from a few opinion leaders, the extension workers were able to achieve their goals and to get farmers to implement the ideas they proposed.

So, when it comes to your website, you should try to convince opinion leaders of its value — in this case, the leaders are high-status bloggers who have a strong relationship with your target audience. If you are able to convince them, others will follow!

> _"An individual is able to influence other individuals' attitudes or overt behavior in a desired way with a … high frequency."_

### 14. Final Summary 

The key message in this book:

**There** **are** **five** **stages** **of** **the** **innovation-decision** **process** **that** **every** **potential** **adopter** **of** **a** **new** **idea** **goes** **through:** **Knowledge,** **Persuasion,** **Decision,** **Implementation** **and** **Confirmation.** **Every** **innovation** **has** **certain** **attributes** **that** **influence** **its** **rate** **of** **adoption,** **such** **as** **relative** **advantage,** **complexity** **and** **trialability.** **Change** **agents** **should** **be** **client-focused** **and** **make** **use** **of** **opinion** **leaders** **to** **increase** **the** **rate** **of** **adoption** **of** **an** **innovation.**

Actionable advice:

**To** **get** **more** **people** **to** **adopt** **your** **product,** **make** **sure** **they** **can** **observe** **it** **in** **action.**

If nobody recognizes your brand or your product, how will they know to buy it? Making your product observable can mean something as simple as allowing others to see it in use — for example by giving product demonstrations, or allowing potential customers to have a trial period with the product. And if you use social media, such as Facebook and Twitter, you'll be able to show the product in use to a much larger audience.
---

### Everett M. Rogers

Dr. Everett M. Rogers is a pioneer of research on innovation diffusion, and coined the now-famous phrase, "early adopter." Before his death in 2004, Rogers was a Distinguished Professor Emeritus in the Department of Communication and Journalism at the University of New Mexico.

