---
id: 56f13471f77893000700005e
slug: digital-gold-en
published_date: 2016-03-25T00:00:00.000+00:00
author: Nathaniel Popper
title: Digital Gold
subtitle: The Untold Story of Bitcoin
main_color: B9A05D
text_color: 6E5F37
---

# Digital Gold

_The Untold Story of Bitcoin_

**Nathaniel Popper**

_Digital Gold_ (2015) tells the story of the many different individuals — including cypherpunks, nerds, investors, gamblers and visionaries — that contributed to the rise of the world's most successful cryptocurrency, Bitcoin. The book gives an overview of the way Bitcoin developed, showing how its value rose from nothing to over $1 billion, and telling the story of its troubled early stages.

---
### 1. What’s in it for me? Learn how Bitcoin can make us rethink money. 

For thousands of years, humans have used the physical entity of money as a means of exchange. To this day, we continue to accept these small objects of value as a form of payment for virtually anything we desire. But have you stopped to wonder if this system is actually severely outdated in today's highly technological and digital age?

Well, some people certainly think it is — and that's exactly why the cryptocurrency of Bitcoin emerged. If you have yet to hear of Bitcoin and cryptocurrencies, here's your chance to get informed.

With these blinks to _Digital Gold_, you'll be walked through the world of Bitcoin, learning the essential ideas of what exactly it is and why it became so famous.

In these blinks, you'll learn

  * how Bitcoin became connected with the Occupy Wall Street Movement;

  * how (and why) Bitcoin is managed communally; and

  * why it is so incredibly hard to hack it.

### 2. Bitcoin is a new kind of money that is created, held and transferred by its users. 

On January 9, 2009, a mysterious internet user hidden behind the pseudonym Satoshi Nakamoto brought forward an idea that would change the financial world forever: _Bitcoin._

Bitcoin is a digital currency radically different from any traditional cash system. First of all, anyone can get involved by simply downloading its _open-source code._

Anyone can look at the code, examine what is happening under the hood, so to speak, and even help make improvements. This contrasts with classic software programs like Photoshop or Microsoft Office, where a closed group of developers build a program whose code only they can see and change. 

Another way Bitcoin differs from a typical currency is the total absence of a centralized authority.

Unlike a bank, where each user's holdings and transfers are stored privately, Bitcoin uses a communal database called the _blockchain_ that everyone can see. The blockchain is like a giant public ledger that stores the record of all existing bitcoins and every transaction ever made. But instead of being located in one place, a brilliantly designed mechanism distributes this exact same record to every computer in the Bitcoin network.

As a result, Bitcoin is not as private as the international banking system, where only you and your bank can access your finances. However, there's also no limit to the amount of Bitcoin accounts, called _wallets_, that a user can own. And unlike a bank, all you need to create a new wallet is an e-mail address, so anyone can open an account and Bitcoin has the potential to be used totally anonymously.

Bitcoin also updates traditional currencies by integrating powerful encryption techniques into its core. While a lot of cash withdrawals still rely on primitive PIN numbers, Bitcoin uses robust _public-key_ cryptography that not even the most powerful supercomputers can crack.

Public-key cryptography has two components: a private and a public key. When a Bitcoin transaction takes place, the public key is recorded in the blockchain for everyone to see. But only the users who made the transaction have the private keys necessary to decrypt the transaction and access the funds. By using cryptography in this way, Bitcoin is able to bypass centralized security systems like banks.

> _"Privacy is necessary for an open society in the electronic age."_ \- from the Cypherpunk Manifesto, passed along mailing lists in 1993

### 3. Bitcoin embodies a new way of communally handling money. 

After the financial crisis in 2008, people all over the world began to think about alternative ways of handling money. One major idea that became central to Bitcoin was _communal management_.

In fact, two key Bitcoin structures — the maintenance of the blockchain and system updates — are both managed communally. 

The blockchain is continually updated with new transactions in the form of _blocks._ Blocks are formed every ten minutes in a way that ensures everyone ends up with the same blockchain, no matter where they are in the world. 

This is a crucial step because the blockchain records which transactions took place and who owns which bitcoins. By creating a communal record of the system, Bitcoin protects itself from hackers who would try to inject false information and create money out of thin air.

The same mechanism of communal management is also used to update the whole system.

To change the Bitcoin protocol, a majority of its users have to approve the proposed modification, no matter how slight. This means that no individual can manipulate the protocol to their advantage, and changes happen only after lengthy discussions on Bitcoin forums.

This doesn't mean Bitcoin is invincible: if a hacker managed to control 51 percent of the entire network — which is highly unlikely, especially as more and more people use the system — they could change the protocol as they wished and take control of the blockchain.

But even then, they wouldn't be able to spend anyone else's bitcoins. The encryption is so strong that the hacker could only reverse his own transactions and block other people from conducting any themselves.

> _"_ _There is no central bank to debase the currency with unlimited creation of new money."_

### 4. Bitcoin’s first surge of popularity came from the drug trade on the underground website Silk Road. 

We've all heard of the hidden black markets of the world, where shady dealers sell all kinds of illegal goods. But did you know there was an equivalent black market on the internet?

Hidden deep in the _Darknet_, a section of the internet only accessible with anonymizing software like TOR, the Silk Road marketplace once allowed buyers to purchase drugs and get them delivered to the address of their choice.

Bitcoin was the perfect complement to Silk Road's anonymity; sellers were happy that Bitcoin transfers could not be reversed, and buyers felt more secure buying drugs without having to conduct a face-to-face transaction. In fact, Bitcoin proved so useful for Silk Road's profitable black market that Silk Road became the principal source of all Bitcoin transfers. As Silk Road grew, so did the value and popularity of bitcoins.

In February 2011, when Silk Road was born, one bitcoin was worth around $1. By mid-May of the same year it was already worth around $10. And by June 2011, after a senator called for Bitcoin to be eliminated, describing it as a form of money laundering online, its value rose to $30 within two days.

Thanks to Bitcoin, Silk Road grew into a libertarian's dream: a highly organized marketplace that was self-regulated, anonymous, decentralized and beyond government control.

By March 2012, Silk Road boasted 10,000 registered users buying $35,000 worth of product a day from vendors based in at least eleven countries. On the forums, security moderators explained the ins and outs of Bitcoin safety, and doctors advised how to best consume each drug.

Silk Road came to an end in September 2013 when its owner, Ross Ulbricht, shared essential information with an undercover FBI agent. But Bitcoin had never been stronger — by then, one bitcoin was worth $140.

### 5. Wikileaks and Occupy Wall Street highlighted Bitcoin’s capacity to take power from financial institutions. 

What do cypherpunks, anarchists and supporters of the Tea Party have in common? 

They're all strongly opposed to government control in society. 

At times, this mistrust of government can be well founded, such as when governments use their influence to cut cash flow to dissident political groups.

Take the case of Wikileaks. In September 2011, WikiLeaks released the largest set of confidential documents ever made public: 250,000 diplomatic cables sent to the US Department of State from all around the world, containing top secret assessments of other countries and their diplomats. WikiLeaks's goal was to reveal the true face of American power — hidden behind the handshakes and big business smiles. 

But the US government fought back. They circumvented US laws and forced Visa, PayPal and Western Union to stop accepting donations to WikiLeaks, thereby eliminating 95 percent of its revenue.

Some people suggested that WikiLeaks accept Bitcoin as a form of donation. Bitcoin developers debated the issue at length in their forums and eventually concluded that it would be too risky. The government scrutiny that would follow might prevent their still-young code from maturing to its full potential. 

But people around the world had seen the malicious power of a government in action — and saw the value of a decentralized currency.

WikiLeaks wasn't the US government's only challenge in September 2011. The Occupy Wall Street Movement, which was grassroots anti-capitalist, took control of Zuccotti Park in Manhattan, New York. And where there was a fight for freedom, so too was there Bitcoin.

The Occupy movement was inspired by anti-austerity protests in Spain, contesting the government's bailouts of big banks and not of the people. They were interested in Bitcoin as a way of receiving donations that couldn't be frozen by any central authority, and as a place to store private funds that wouldn't be undermined by the whims of the stock market. 

The movement learned much about Bitcoin from the concurrent Bitcoin Meetup in New York, where Bitcoiners from around the world expounded the possibility of creating an alternative, freer way of organizing the economy.

> _"No one can stop the Bitcoin system, short of turning off the entire system."_ \- _PC World_ in an article after the Wikileaks blockade.

### 6. Argentina’s financial crises have shown how Bitcoin can transfer power from governments to the poor. 

The citizens of Argentina are all too familiar with financial experiments. The country's history is riddled with financial crises and severe inflation, and many failed attempts to mitigate them. So, at the second official Bitcoin meetup in Argentina, many people were excited about the possibilities of the new currency. Soon, Bitcoin would break down many of the government restrictions imposed to control the value of the Argentine peso.

For example, exchange rates for the US dollar had been placed artificially high, all PayPal transactions were banned and economists who spoke out against the policies were fined. And if you wanted to buy something overseas with a credit card, you had to deal with extended delays.

The Bitcoin meeting organizers experienced this themselves. If they wanted to sell a $100 ticket through credit, they would receive 595 pesos and the transaction would take 14 days; but if they used Bitcoin, they could get 920 pesos in only two days.

Bitcoin also became a way of protecting funds from government interference. People in Argentina typically kept their money in cash because, as in many developing countries, opening a bank account was difficult — and getting a credit card was even harder.

But Argentinians had come to deeply distrust the government's currency management, as they had seen their money become worthless from rapid inflation on multiple occasions in the past. So when Bitcoin arrived, they saw it as a safe, stable place to keep their money, even though they couldn't spend it directly in Argentina. 

In fact, since early 2013, the Argentine peso fell by an additional 25 percent against the US dollar, while Bitcoin has risen by an astonishing 860 percent.

> _"The root problem with conventional currency is all the trust that is required to make it work."_ \- Satoshi Nakamoto, in early writings about bitcoin

### 7. The Bitcoin protocol has many advantages over typical currencies – far beyond storing and transferring money. 

In the same way that the internet liberated information and communication from postal services and major media corporations, some believe Bitcoin will free money from the strictures imposed by banks.

And they might be right.

One major advantage Bitcoin has over the banks is its capacity to transfer money instantaneously.

We live in a world where it's normal to transfer gigabytes over the internet in minutes or call a friend on the other side of the world in HD, but a bank transfer still takes multiple days.

This anachronistic arrangement came into sharp focus during the financial crisis. The major bank JP Morgan Chase was on the point of collapse and needed funds urgently from a bank in Japan. However, the situation arose on a weekend and the next Monday was a bank holiday. So to save the bank, they were forced to write a $9 billion paper check and send it by mail! 

With Bitcoin, it would have taken milliseconds.

But Bitcoin beats the banks in another way: its digital nature makes it perfectly suited to the new economy of the internet.

Bitcoin's universality allows money to cross borders without foreign transaction fees. And as digital currency can easily be divided into very small amounts, online services can charge tiny fees, like 0.01 cents to read one page of a book or 0.02 cents to skip an ad.

But Bitcoin is not only transforming the world of finance; its unique structure has many other applications too.

Consider the core of Bitcoin, the blockchain protocol, as an example. Its powerful verification system allows people to build trust in a wide variety of circumstances. Since everyone can see the blockchain and no one can modify it, this concept can replace traditional legal systems of trust, like when someone needs a witness for the signing of a contract or will. In the past, an official like a notary or solicitor had to be present, but with the public blockchain, everyone can see that the contract really took place.

Bitcoin has the power to fundamentally disrupt the way we use money. But the most interesting uses of Bitcoin have yet to be discovered.

> _"We see the intrinsic value of Bitcoin as the conduit in a new global crowd-funded open source payment network."_ \- Research analyst from a Wall Street trading firm

### 8. Final summary 

The key message in this book:

**Bitcoin is a virtual currency created and controlled by its users. Through its uncrackable cryptography and decentralized organization, Bitcoin fundamentally limits the power of governments and banks, and empowers individuals to transfer money around the world anonymously, safely and quickly.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Age of Cryptocurrency_** **by Paul Vigna and Michael J. Casey**

_The Age of Cryptocurrency_ gives an overview of the history and nature of Bitcoin. It explores the definition of "money" and explains the dramatic impacts that cryptocurrencies like Bitcoin will have on our economy and the world at large.
---

### Nathaniel Popper

Nathaniel Popper is a Harvard University graduate who has written for _The Los Angeles Times_ and _Forward_. He currently works as a business reporter for _The New York Times_.

