---
id: 561436433632330007170000
slug: disney-u-en
published_date: 2015-10-07T00:00:00.000+00:00
author: Doug Lipp
title: Disney U
subtitle: How Disney University Develops the World's Most Engaged, Loyal, and Customer-Centric Employees
main_color: DA2C2D
text_color: A62122
---

# Disney U

_How Disney University Develops the World's Most Engaged, Loyal, and Customer-Centric Employees_

**Doug Lipp**

_Disney U_ (2013) lays out the strategy behind the massive success of the Disney theme parks. By caring for their staff and providing them with the highest training, Disney ensures that their employees have the customer service skills necessary to create a truly magical environment. These blinks teach you how to implement Disney's philosophy in your own business.

---
### 1. What’s in it for me? Learn the magical qualities behind Disney. 

Today, most children dream of going to Disneyland or Disneyworld, where they hope to meet one of their heroes — someone like Peter Pan or Snow White or Cinderella. And consider this: that was also the dream of children a generation ago — to meet a favorite Disney character at a theme park.

How has one company managed to do this? How has Disney managed to stay at the heart of Western culture for well over half a century? The answer lies, partly, in the amazing way they train their staff at Disney University. In this special place, Disney employees are taught all they will ever need to know about treating customers like VIPs. In these blinks, you'll learn how to do that, too.

In these blinks, you'll also discover

  * why Disney managers held crucial meetings in Cinderella's Castle;

  * why great customer service is a mixture of art and science; and

  * why you can't buy alcohol at Disneyland Tokyo, but can at Disneyland Paris.

### 2. A company's values should help the staff stay motivated, hardworking and happy. 

The employees are the most important part of any business, especially in a customer-focused industry. You can't achieve success without a well-trained, motivated and happy staff.

So keep your workforce happy by providing them with _values_, not just physical goods. Most companies fail here because they assume they can motivate their employees solely with pay or bonuses or little perks. They don't realize they need to share the company's values with them, too.

If your employees don't care about your company's values, they won't be interested in its progress or stakeholders. They'll only be concerned with their own material needs.

Disney strives to avoid this. They carefully promote the company's philosophy at Disney University, where staff (or _cast members_, in Disney terms) is trained. The cast members come to share the company's values, so they feel a much deeper connection to it.

You also need to keep your employees happy. The best way to do this is to ensure that the management stays in close contact with them. The management should know immediately if the employees encounter any problems.

Van France, the founder of Disney University, used to take walks through the institution to see how the cast members were doing. During one of his walks, he noticed a problem with the orientation program for newly hired members: the company and its parks had expanded so quickly that trainees were struggling under the old orientation program, which had become too small. It was out of date.

So France developed a new orientation program that was more suited to the needs of the company and its trainees. The management should always be aware of the organization's inner workings, and seek out ways to make work more efficient and enjoyable.

### 3. Business is a mixture of science and art. 

Is building a successful business a science or an art? Well, it's really a combination of both.

Your organization's "scientific side" is about its infrastructure and physical products. For Disney, that means building and maintaining the parks and attractions. Disney's success largely depends on the layout of their parks, and on their quality, cleanliness and exciting rides.

But great infrastructure and well-engineered roller coasters aren't enough on their own. That's where Disney's artistic side comes in! A business's artistic side is what gives it that special touch that sets it apart.

The staff holds the key to the business's "art." After all, it's up to the staff to make the customer's experience special, and that's what makes all the difference at Disneyland.

Disney approaches this challenge and maintains their artistry in several ways.

First, they teach the staff the interpersonal skills they need to have positive interactions with customers. Children tend to be very excited about visiting Disneyland because they get to meet their heroes, like Snow White or Donald Duck; clearly, the cast members who play those characters have a lot to live up to. They need extensive training.

Cast members who play Disney characters also have to adapt to being surrounded by an audience at all times, and they need to know how to interact with guests of all ages.

Disney performers also need a lot of technical skill. The guests should feel like they're walking through a fairy tale when they're in a park, so Disney wants their actors to seem like living film characters. The actor playing Snow White has to know how her every movement ought to look. She literally has to transform into the character.

### 4. Strong companies look to the future and the past at the same time. 

You can probably name a few companies that once dominated their market but eventually fell into bankruptcy. Nokia, Kodak, Blockbuster — each suffered this fate. Why?

Companies often collapse when they fail to keep up with changes. They cling to the business practices that once made them successful, even when those practices no longer work.

A company can also fail if it changes too quickly, however. If it abandons strategies that work and adopts new ones that don't, the company risks destroying itself.

So how do you find the balance between changing and staying the same? Here's Disney's key: keep one foot in the past and one in the future.

Naturally, Disney keeps up with the latest in animation, and its parks are highly advanced. But no company values its history more than Disney.

When Walt Disney died, for example, the company knew they had to keep his spirit alive. So they organized a new program to uphold the Disney tradition, where newly hired cast members could learn about the company. They learned how it got its start and what made it so great in the first place.

Preserving the past and moving into the future are equally important, but they're both useless without another critical tool: feedback! You'll never be successful if you don't receive ongoing feedback from the people working with you.

When a Disney executive visited one of these tradition programs - called Disney Traditions - he noticed a flaw. The first questions the newly hired members asked were about work schedules, salary and what their costumes looked like. The tradition program, however, taught them first about the company.

So the executive changed the order of the program. When new employees got their personal information first, they were much more engaged in learning about the company, because they saw themselves as a part of it.

### 5. Speak the language of success and adapt yourself to different cultures. 

Language is what sets humans apart from other living creatures, but businesses often fail to cash in on its potential. They fall back on boring jargon, like "dynamism," "energizing" or "going the extra mile."

Those words aren't engaging! If you want to use really great business language, you need to step it up!

Use a trick called the _language of success_ to make your customers and employees feel more valued. That means using unique language that sets your company apart from all the others. Small changes make a difference: Disney uses its own special names for various parts of the company. Employees are _cast members_ and customers are _guests_. Disney parks don't have "crowds" — they have an _audience_.

These terms might sound odd, but they really do change the way the employees and customers feel; Disney's unique language immerses them in the magic of the Disney environment.

Disney adapted these terms to suit the branches of their company that speak different languages as well. If you want your company to succeed globally, you have to take the time and care to make sure that it resonates with people of different cultures.

For example Disney parks in different countries have different rules. When Disney opened Tokyo Disneyland, they maintained the drinking rules that are enforced in the US: no alcohol anywhere in the park. Japanese people responded well to this and many wrote letters to Disney thanking them for it. One housewife wrote that her husband wouldn't have spent time with her and their children if the park had sold sake.

The alcohol rule didn't go over well in France, however. The French press criticized Disney for their insensitivity to French culture, so wine is now allowed at the park in Paris.

### 6. A good business never gives up and always strives to keep its employees happy. 

Running a company is a bit like being married. There are always ups and downs, and times when you just want to give up.

It's not possible for a business to avoid these difficult times. So what do you do when they hit?

Even a company as strong and well structured as Disney faces problems. In 1971, for example, Roy Disney, the company's inspirational leader, died a mere two months after Walt Disney World was opened. His death was very hard on the cast members, as many of them had been close to him. Understandably, the cast was generally demoralized and struggled to focus on their work or perform with smiling faces.

When you reach a tough period like this, the best thing to do is have everyone come together and work it out as a group. Don't give up! Gather your team and have everyone share their ideas on how to make the situation better. Try to get something positive out of it.

When Roy Disney died, the managers held emergency meetings in the tower of Cinderella's Castle. They discussed the issue and collected feedback from everyone on the team, and eventually came up with some ideas for reinvigorating the company and its employees, like improving the staff training and HR department. In the end, the company only became stronger.

And there's another reason that running a business is like being married: one of the keys to a successful marriage is to not think of yourself all the time. Instead, think about what you have to give.

In business, this means you shouldn't focus only on money and profit. You also have to help your staff have fun!

Walt Disney understood this well. His motto was "Work hard, play hard," and he was always ready to plan fun events for his staff and let them enjoy themselves.

### 7. Final summary 

The key message in this book:

**A company's character and strength depends on its staff. Disney understands this well. They give their employees more than just a good salary; they include them in the Disney magic by sharing their values and making them understand the legacy they're a part of. Disney holds onto traditions and looks for new ideas, and they work together through difficult times. Even if your organization isn't as big as Disney, you can still incorporate many of their successful ideas.**

Actionable advice:

**Have fun!**

Van France, the man responsible for Disney's unique training process, once said, "The business we're in, if we can't have fun, how could we expect the public to have fun?" So help your staff enjoy themselves! They won't just feel better; they'll be more productive workers, too. 

**Suggested further reading:** ** _Be Our Guest_** **by** **Disney Institute and Theodore Kinni**

_Be Our Guest_ reveals Disney's key tenets and principles of outstanding customer service and how following these has helped the company become the successful business empire it is today.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Doug Lipp

Doug Lipp helped create the first international version of Disney University in Tokyo Disneyland _._ He was also a mentor in Walt Disney World's corporate headquarters and worked under the founder of Disney University, Van France.

