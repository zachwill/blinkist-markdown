---
id: 58130c78f70d8c00030942b6
slug: disrupt-yourself-en
published_date: 2016-11-02T00:00:00.000+00:00
author: Whitney Johnson
title: Disrupt Yourself
subtitle: Putting the Power of Disruptive Innovation to Work
main_color: FAC732
text_color: 7A6118
---

# Disrupt Yourself

_Putting the Power of Disruptive Innovation to Work_

**Whitney Johnson**

_Disrupt Yourself_ (2015) is about embarking on the journey of constant discovery that is your career. By following your interests, discovering your unique talents, taking the right risks and being prepared to learn, you will find yourself constantly stimulated and satisfied by your work.

---
### 1. What’s in it for me? Get ready to disrupt yourself! 

The tech industry is one of the most innovative industries in the world — things are always churning and changing. One minute, a company can be happily leading the market; the next, a small competitor takes them by surprise with a new product and charges ahead. Just consider Nokia, which once ruled the cell-phone market. Where are they today?

The engine driving all the innovation is disruption. Companies big and small are always looking for new ways to do things, for new technology that will crush the competition.

But disruption doesn't have to be limited to companies. You can disrupt _yourself_ and gain the success you're after.

In these blinks, you'll also find out

  * how one lawyer was promoted to partner after a smart disruption;

  * what unexpected shortage 72 percent of successful new businesses have in common; and

  * how taking a step back can lead to the most successful disruption.

### 2. There are many types of risk; some are more important than others for success. 

Have you ever considered skydiving? Maybe you watched a video on YouTube and thought it looked like fun. But before you make that jump, it's important to do your research and ensure you know what kind of risks you're taking. This same philosophy applies to jumping into the world of business as well.

If you're preparing to take a risky business move, it's important to distinguish between _competitive risk_ and _market risk_.

So what makes something a _competitive risk_? Well, let's say you have an idea for a great product and it has tested well in a variety of studies and appears to have the potential for huge success. But, at the same time, you're pretty sure that similar products are being developed by other companies that are aware of the demand.

By going ahead in this scenario you're taking what's known as a competitive risk, since you'll be competing against others.

A _market risk_, on the other hand, is when you have a unique idea for a new product or service but are uncertain about its chances of success.

In this scenario you have no idea whether your company will generate revenue, but you _are_ certain that your unusual idea will give you a head start on the competition.

While a competitive risk is seen as the safer route, since there's a known demand for the product, the market risk is usually the best option if you're looking to disrupt yourself.

In fact, studies show that start-up companies fare better when they take a market risk.

In 1995, Harvard Business School professor Clayton Christensen conducted a landmark study on the computer industry. He determined that two kinds of disc drive companies had emerged between 1976 and 1993, and they'd either taken a market risk or a competitive risk.

Of these companies, it turned out that only six percent of competitive risk companies had reached $100 million in revenue, while 37 percent of the market risk companies had soared past the $100 million mark.

> _"Be a Columbus to whole new worlds within you, opening new channels, not of trade, but of thoughts."_ \- Henry David Thoreau

### 3. To be successful, identify your distinctive strengths and pair them with unmet needs in society. 

Everyone has a special strength that can make them stand out. Even the gentle and seemingly lazy koala has the unique ability to digest poisonous eucalyptus leaves, which no other animal can stomach.

To be successful in a highly competitive marketplace, you need to identify and develop your own distinctive strength.

To see how you can use your own unique abilities as a business advantage, let's look at an example from the 2014 film _The Hundred Foot Journey_.

In the movie, an Indian family is forced to flee during political uprisings in Mumbai, India. They seek refuge in Europe and arrive in a small French village where the family acquires a rundown restaurant.

The family's talented son, Hassan, is an expert in Indian cuisine, but the local villagers are suspicious of the new restaurant and not keen to try out the exotic fare. But eventually a French neighbor takes Hassan under his wing and teaches him the art of French cuisine.

Now Hassan has the unique power to fuse traditional French flavors with Indian spices, and once he puts this distinctive strength to work, the restaurant ends up becoming a great success. 

Hassan's food also opened an untapped market; likewise, you need to aim your strengths toward some unmet need in society.

For example, when Jayne Juvan started at the law firm Roetzel & Andreas in Cleveland, Ohio, she was a young female lawyer in a conservative male world. In order to prove herself, she needed to discover an unmet need in this society.

She looked around and saw that very few people were using social media, so Juvan began advertising the firm's services on Twitter and Facebook and, before long, she was landing major clients.

Due to her ability to see the world around her and what was missing from it, her bosses quickly took notice of her strengths and Juvan was made partner of the law firm at the young age of 32.

> _"Each bird must sing with its own throat."_ \- Henrik Ibsen

### 4. You can turn limited money and experience into great sources of motivation. 

If you've ever stared at the thousands of options on Netflix and been unable to pick something to watch, you're not alone. When it comes to decisions — in life and in business — sometimes limitations can be a good thing.

In fact, having a limited amount of money can force companies to get creative. 

Real estate manager Nick Jekogian is well aware of this. Early on, when the budget was tight, his business flourished. During this time, employees knew that the company couldn't afford mistakes, so they put in quality effort and worked hard.

Later on, in 2007, the company was a success and the budget was no longer a concern. And this is when the focus was lost and the company ended up in a downturn.

Nor is Jekogian's experience an anomaly. In a 2007 study by _Entrepreneur_ magazine, 72 percent of the most successful new businesses don't have access to money from private investors or bank loans.

And though you may not think it, limited experience in your given field can also lead to success. 

Athelia Woolley LeSueur was experienced in the field of international relations, but she had to give up this career due to health issues. That's when she decided to venture into the unknown world of fashion.

LeSueur launched an online clothes shop called _Shabby Apple_, and her lack of experience actually worked out in her favor. Unfamiliar with the customary practices of hiring representatives and pricey wholesalers, she skipped all these steps and saved herself a lot of money and trouble.

These business partners are often unhelpful and not worth the difficult negotiations you have to go through to secure their services.

LeSueur forged ahead on her own and today her company is worth $47.5 million.

### 5. Feelings of cultural and intellectual entitlement are killers of innovation and leadership. 

If you've spent any time in the business world, you may have met a boss that is surrounded by yes-men and people who don't dare challenge the head honcho's ideas. This kind of behavior can be deadly if you're looking to disrupt yourself.

To disrupt, you need to discover new ideas and new people, and this won't happen if you fall victim to _cultural entitlement_. 

It's natural for human beings to group together with like-minded people; if those around you share your cultural values and ideas, your own ideas will be understood and appreciated. But this can also lead to a feeling of superiority over other cultures and a sense of entitlement.

Research indicates that staying within your own social circle can make you less innovative.

The Kellogg School of Management looked at all the scientific papers that were published between the years 1990 and 2000. The papers were deemed either successful or unsuccessful, the criterion for success being how frequently the paper was quoted in other academic papers.

They found that the most successful papers were those that used a majority of well-established academic sources as well as a small amount — 10 to 15 percent — of unusual or alternative sources.

This means that the most successful authors had searched beyond their recognized circle of academic friends and sought out some unique voices for inspiration.

Another pitfall is _intellectual entitlement_, which can prevent a good leader from paying attention to voices of dissent.

This happens when leaders get so convinced of their own intellectual superiority that they don't feel the need to listen to others.

Brooksley Born, who served as chair of the US Commodity Futures Trading Commission prior to the 2008 economic crash, knows what it feels like to be ignored.

She repeatedly pointed out the need for regulations in the derivatives market. But the chairman of the Federal Reserve, Alan Greenspan, and the Treasury Secretary, Larry Summers, were too experienced and entitled to listen to her concerns.

### 6. Knowing when to make a career move is crucial, but this sort of decision should not be taken lightly. 

If you've watched Olympic divers, you know how much skill it takes to keep from hitting the water with a tragic belly flop. Well, it takes just as much poise and control to successfully execute one of the dicier moves in business: stepping down.

It's crucial to know when to take a step down in order to achieve success. 

Carine Clark figured this out as a senior manager for online products at the software company Novell. After launching a wide-reaching $80-million marketing campaign, she felt she had accomplished everything she'd set out to do, so she decided to step down and start anew.

Clark joined Altiris, a small start-up that offered a platform for IT management assets, and her timing couldn't have been better: A few years later, in 2007, Altiris was acquired by the $6-billion software company Symantec, and Clark was made CEO of the new company.

Even if you're forced to step down against your will, it can still turn out to be a great thing. 

In 2009, Clark had to step down after she was diagnosed with breast cancer. This caused her to spend several years out of the business loop, and when she recovered she decided it was time for another fresh start.

In 2012, Clark built her own small software company from scratch, which was acquired by MaritzCX in 2015. Again, she was selected to be head of the new company.

Carine Clark's success story shows how important it can be to keep challenging yourself and recognizing when it's time to start over and try something new.

When you look at your situation clearly, you can make the right business decisions, take the right risks and, like Clark, become a respected business leader.

### 7. Intelligent people are often particularly afraid of failure, and yet failure is crucial to success. 

It can be frustrating to see other kids breeze through school while you have to struggle just to get a passing grade. But all that hard work can actually give you an advantage. 

After all, everyone is going to eventually hit a setback, and it will take hard work to recover.

We can see this in the studies done by child specialists Carol Dweck and Claudia Mueller, who explored how different praise can affect our level of resilience.

First, they gave a group of fifth graders some easy problems to solve. After they successfully finished, one group was told how smart they were, while the other was praised for its hard work. 

Then came the second round. They were all given very difficult problems that no one got right. Finally, in the third round, the children were once again given problems that were easy to solve.

In the end, the "smart" children did 25 percent worse in the third round than in the first round, while children who were praised for working hard showed a 25-percent improvement in the third round.

This shows us that the wrong kind of encouragement can actually set people up for failure when they hit a roadblock. Children should be taught that, as long as they work hard, it's okay to fail.

In fact, a lot can be learned from failure, which can then be put toward future success.

Entrepreneur Nate Quigley wanted to create a Facebook spin-off called FolkStory, which would act as a blog for the whole family. But it never took off.

So his team came up with the idea for JustFamily, which would let families collect and share their photos on a cloud service. This also failed.

Still, they kept at it, reaching out to target users to find out what would work and finally coming up with Chatbooks, a service that compiles the social media photos and stories of various family members so that it can all be neatly printed out into a book. Unlike the previous ventures, Chatbooks was a huge success.

### 8. The most successful careers are driven by a spirit of discovery and the most successful companies develop flexibly. 

Every so often you come across someone who knew at a very early age what they wanted to be when they grew up. Such stories offer a certain narrative satisfaction — but don't be frustrated if you're still searching for your vocation.

Most successful people are actually driven by a restless spirit of curiosity.

Linda Descano is one such person. She started out as a geology student at Texas University, which got her work as an environmental consultant. This led to legal counseling work and her becoming an expert witness.

She then joined Citibank's legal sector, where she looked at potential environmental risks and provided estimates. Then she became an asset manager before transitioning into the position of being Citigroup's lifestyle blogger.

It should be noted that Descano didn't plan any of these career moves. Rather, she remained curious and willing to discover new fields of work, and this can-do attitude eventually convinced her employers that she was fully capable of learning any new task.

Descano's story also shows the importance of being flexible and adaptable.

In fact, research shows that 70 percent of all successful new companies end up with a different product or strategy than they started out with.

Take Millenium Pharmaceuticals, for example. They started out as a biochemical company treating genetic diseases before recognizing that treatments for other diseases, such as cancer, were more important to their target population.

Gradually, they branched out into researching and evaluating new drugs and, in May of 2003, Millennium produced a new drug called Velcade, a successful treatment for multiple myeloma, a cancer of the blood.

This was a milestone achievement, and the company was convinced that they should focus on anti-cancer drugs as their main product from now on.

So remember, life is full of surprises. Don't be discouraged if you don't end up where you thought you would. You might end up doing things you never dreamed of!

### 9. Final summary 

The key message in this book:

**You've got to be disruptive, both in your professional and in your personal life. Don't be afraid to challenge received ideas about best practices and savvy strategies. And don't be afraid to wipe the slate clean and start from scratch! If you're unsure of what you want to do, figure out what your unique talents are and how they could be used to contribute to society. And remember: don't forget to learn from your failures and continue to challenge yourself.**

Actionable advice:

**Listen to your inner voice.**

You probably have an inner hunch as to what would interest you or what your talent might be. But perhaps you think there's no way you can earn money with it. Don't think that. Stay with your hunch and think of Susan Cain, a woman who wanted to focus on the value of introverts in society. Is that a good business model? You might not think so, but Susan Cain ended up writing a best-selling book and becoming the CEO of the social media network The Quiet Revolution.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Disrupted_** **by Dan Lyons**

_Disrupted_ (2016) demystifies the culture and practices of tech start-ups by taking a revealing, behind-the-scenes look at Boston's HubSpot software company. After 25 years as a technology journalist, Dan Lyons was fired from _Newsweek_ and accepted a new job at a start-up. These blinks follow Lyons's bumpy and humorous journey as he tries to navigate a weird new world filled with candy walls and other bizarre instances of HubSpottiness.
---

### Whitney Johnson

Whitney Johnson is the influential co-founder of Rose Park Advisors and a strong advocate for using disruption as a way to move forward, both in life and in work. She is also a contributor to the _Harvard Business Review_ and the author of multiple best-selling books, including _Dare, Dream, Do_.

