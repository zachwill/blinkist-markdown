---
id: 57f2911b90bf6100031a3f9c
slug: disrupted-en
published_date: 2016-10-07T00:00:00.000+00:00
author: Dan Lyons
title: Disrupted
subtitle: My Misadventure in the Start-Up Bubble
main_color: FA7D32
text_color: AD5723
---

# Disrupted

_My Misadventure in the Start-Up Bubble_

**Dan Lyons**

_Disrupted_ (2016) demystifies the culture and practices of tech start-ups by taking a revealing, behind-the-scenes look at Boston's HubSpot software company. After 25 years as a technology journalist, Dan Lyons was fired from _Newsweek_ and accepted a new job at a start-up. These blinks follow Lyons's bumpy and humorous journey as he tries to navigate a weird new world filled with candy walls and other bizarre instances of HubSpottiness.

---
### 1. What’s in it for me? Peel away the gloss of the glittering tech start-up industry. 

Silicon Valley is the sacred site of the high-tech start-up scene, where internet giants like Google, Apple and Facebook forge the future and new ideas and innovations pop up like mushrooms. But this fantastic public facade masks some sobering private practices.

In these blinks, you'll follow the story of author Dan Lyons who, jobless and in his fifties, entered the start-up world at HubSpot, a company specializing in inbound marketing and sales. While working there, the author witnessed weird business practices and poor working conditions — and discovered the truth behind the hype.

You'll also learn

  * what it takes to be truly "HubSpotty";

  * that some people think talking to a stuffed animal is innovative; and

  * what a "bozo explosion" is.

### 2. Traditional journalists like the author have been forced to reinvent themselves in the modern media industry. 

In 2012, journalist Dan Lyons, who'd previously been employed as the technology editor at _Newsweek_ magazine, was in his 50s and desperately looking for work. He'd just been fired and was now facing a radically different job market.

He wasn't alone. The internet-technology boom of the 2000s was causing many older professionals to reevaluate their positions in the media industry.

During the early 2000s, new internet-based tech companies, including Google, Facebook, Zynga and Groupon, were on the rise. Meanwhile, traditional media industries, such as newspapers and magazines, were struggling to survive and adapt.

These newly ascendant companies were offering products and services that were changing central human activities — how we shop and socialize, how we get our news and information.

Instead of relying on newspapers and magazines, readers could now get all the information they needed with a simple click. Print magazines like _Newsweek_ were in free-fall as advertisers began moving to online platforms and people began canceling their subscriptions.

All of this led to Lyons being let go, just after _Newsweek_ — the very company he was working for — published an article entitled "The Beached White Male." Ironically, it was about a generation of experienced, older professionals who were suddenly finding themselves bereft of work due to company cutbacks.

To find a new job, Lyons needed to reinvent himself — and his attempt at transformation led him to the new world of start-ups.

Lyons was married, had two young kids and was the sole breadwinner, all of which made a steady job and good health insurance imperative.

His first new job took him to San Francisco, where he worked for a tech-news website called ReadWrite.

It was okay, but not ideal, since his family was rooted on the other side of the country, in Boston. However, while in San Francisco, he also got a first-hand look at the booming start-up businesses of Silicon Valley.

This got him thinking: Maybe he could reinvent himself as a writer in the marketing department of a start-up?

### 3. Entering the world of a start-up as a 50-year-old was a strange new experience for Lyons, but he was willing to adapt. 

As a tech writer for _Newsweek_, Lyons was familiar with companies like Twitter and Facebook. He had interviewed many of these businesses' CEOs and knew that the people who were on the ground floor when these companies took off reaped huge financial rewards.

But, despite his familiarity with their products, Lyons was surprised by the strange business practices of start-ups.

Lyons landed an interview for a position at HubSpot, a software start-up based in Cambridge, Massachusetts, after he responded to a post on LinkedIn. The interview went well enough, but Lyons was puzzled when he was offered the vague position of "marketing fellow," which hardly seemed like a job title at all.

This hazy way of doing things continued when he spoke to Shah and Halligan, the two founders of HubSpot, about what they'd like him to do.

Even though they had a lengthy conversation, it was never made clear what exactly Lyons's role at HubSpot would be.

Lyons understood that they liked the idea of hiring a journalist who could assist in making HubSpot a "thought leader" in the world of marketing. But Lyons was never given any clear indication of how they imagined him accomplishing such a task.

They came closest to outlining a specific task when Halligan talked about providing "missions" for Lyons to go on — but this just sounded like they wanted him to improve their blog to raise brand awareness.

While his initial meetings at HubSpot were confusing, Lyons was willing to adapt and tried to remain open to new ways of doing things.

He was actually quite excited about the possibilities of this new start-up and felt ready to learn about marketing, a department he'd never worked in.

And although the job came with a rather small salary, he was given stock options for HubSpot and he knew that these would pay well if the company became a success.

### 4. The culture at HubSpot was filled with strange lingo and odd, cult-like practices. 

Lyons became an official HubSpot employee in April, 2013, and it was then that he was introduced to the company's odd, cult-like practices, and a new world of missions, culture codes and spiritual leaders.

He quickly learned that HubSpot wasn't just out to make money; it was on a "mission" to change the world through unique marketing software. He also learned that HubSpot's co-founder, Dharmesh Shah, was being referred to as a "spiritual leader" by some employees and clients.

During his first days, Lyons was emailed a manifesto that contained 128 PowerPoint slides and was called _The Hubspot Culture Code: Creating a Company We Love_.

The presentation pitched HubSpot as a utopian society where the team is more important than the individual and people don't care about a work-life balance, since work is life.

While much of this seemed strange, it wasn't utterly alien. Many Silicon Valley employees at companies like Google and Apple are famous for "drinking the Kool-Aid" and transitioning from a regular employee into a devoted believer of shared values and "world-changing" missions.

At HubSpot, employees were encouraged to adopt a strange lingo and dress code as a way to inspire uniform happiness.

This meant that if an employee was truly "HubSpotty" they would apply the principles of _HEART_ to "make magic." At HubSpot, HEART stands for _humble_, _effective_, _adaptable_, _remarkable_ and _transparent_.

The most HubSpotty people also regularly wear orange and religiously obey _fearless Fridays_, a monthly ritual where employees do something they're afraid of, unrelated to work.

The language at HubSpot is so confusing that the company created a Wiki page to help newcomers decipher the jargon.

People in meetings might talk about SFTC, getting an SLA or ask about the KPI, rather than talk about solving for the customer, service-level agreements or key performance indicator. And employees who are in GSD mode "get shit done." Employees not in GSD mode might be heading for "graduation," which happens when employees leave HubSpot, regardless of whether they're fired or they quit.

> _"I want to tell Zack that they all need to STFU because WTF does any of this have to do with how ordinary human beings actually speak..."_

### 5. The working environment at HubSpot was quite a culture shock for an older employee like Lyons. 

While they might be odd, the practices at HubSpot were meant to inspire teamwork and unity. It wasn't long, however, before Lyons began feeling like a misfit.

As an older man, Lyons wasn't used to open-plan offices that afforded employees zero privacy.

The long tables packed with employees reminded Lyons of the working conditions in Bangladeshi sweatshops — only, instead of hunching over sewing machines, the people at HubSpot were hunched over laptops.

But more than that, Lyons had never worked at a company where so much emphasis was placed on forcing "fun" upon employees.

This was definitely the case at HubSpot, which featured multiple areas of the workplace that resembled playgrounds.

They had a "nap room" that contained a hammock; an area with musical instruments intended for spontaneous jam sessions, though the instruments were never used; and the conference room doubled as a game room, with ping-pong, foosball, pool tables, as well as video games.

Lyons was particularly taken aback by how proud HubSpot was of its so-called "candy wall" — an entire wall in the cafeteria composed of glass cases containing a variety of candy bars and junk food.

But this perhaps wasn't as odd as the time when HubSpot asked its employees to talk to a teddy bear.

Lyons was especially bewildered when his boss claimed to have come up with an innovative management breakthrough. Alas, the idea was nothing more than bringing a teddy bear named Molly into meetings to represent the customer they were always trying their best to serve.

This was a bit disheartening for Lyons, whose previous boss was the Pulitzer Prize-winning author Jon Meacham. Now he was working for a man who thought that talking to a stuffed animal was an innovation.

> _"_ _We're not just selling a product here... HubSpot is leading a_ revolution _._ _A_ movement _._ _HubSpot i_ s changing the world _."_

### 6. Lyons started running into conflict after he proposed changes for improvement. 

Three months into his new job, Lyons was still trying to figure out what he was supposed to do in his role as marketing fellow.

And when he tried to bring some new ideas to the table, he was disappointed to find that the company wasn't really interested.

Lyons was under the impression that he'd been hired to improve HubSpot's company blog. So, with this in mind, he wrote blog posts that would appeal to venture capitalists, CEOs and people that might be interested in investing in HubSpot.

Unfortunately, Lyons soon learned that the people his blog posts were supposed to appeal to were fictional characters called "Mary the Marketer," "Enterprise Erin" and "Ollie the Owner."

These were small business owners who were looking for blog posts with helpful marketing tips like "15 Free Stock Photos You Can Use" and "How to Create a Facebook Brand Page." HubSpot wanted these people to click on links at the end of the posts that would take them to an online form that would give HubSpot their personal information.

Lyons was especially disappointed when they asked him to actually dumb-down the blog.

Frustrated at the idea of having to write a purposefully dumb blog, Lyons pitched the idea of starting a separate blog with high-end content called _Inbound_.

Conflict began when Lyons took the idea to the company founders after it was initially rejected by middle management.

The founders loved the idea — but, at HubSpot, a CEO's approval doesn't mean it will really happen. Middle management ensured his idea remained squashed.

But Lyons put up a fight and it was eventually decided that he could run a small "sub-blog" that would allow him to write articles that were a bit more sophisticated.

However, he would have to work in the noisiest room in the company; the dreaded telemarketing room, which employees referred to as "the spider-monkey room."

### 7. Start-ups can reinforce mediocrity and poor working conditions in the race to expand quickly. 

It eventually became clear to Lyons that good management was nowhere to be found at HubSpot. After being kicked down to the telemarketing room, he saw how bad the working conditions could really get.

Lyons realized that management issues at start-ups are often the result of a phenomenon called "the bozo explosion."

Steve Jobs coined this phrase to explain how the initial employees at a start-up might not be the sharpest tools in the toolbox, but they'll still end up rising through the ranks due to their seniority.

These bozos are then in a position to hire other people and they'll tend to hire even more mediocre bozos, people whom they can feel superior to. This is how a start-up like HubSpot can end up with such exceedingly poor management.

Further, when mediocrity is rewarded, you can end up in some bleak situations. For instance, Lyons was once asked to do an all-night "hackathon" in order to create a surplus of purposely mediocre blog posts.

Mediocre management is one thing, but when Lyons moved into his new working environment in the telemarketing room, his eyes were opened to a whole new situation.

Here, Lyons discovered many recent college graduates, nicknamed "spider monkeys," tightly crammed into a large room, making old-fashioned cold calls to potential customers. They were hard at work because they would be fired if they didn't meet the required number of successful sales.

To make these stressful conditions more tolerable, the spider monkeys were given an unlimited supply of free beer.

These telemarketers were actually very important to HubSpot at the time since the company was about to launch its _initial public offering_ (IPO), at which point it would issue its first shares of stock.

So, HubSpot was desperate to grow as much as it could, as quickly as possible, because initial buyers don't care about how much profit a company has made; they only care about how quickly it's growing.

### 8. HubSpot employees ignored the lack of benefits and job security, because the company made them feel special. 

The people toiling away in the spider-monkey room, desperately trying to meet a monthly quota or else lose their jobs, weren't the only people that Lyons felt the company was mistreating. And Lyons was surprised by how many people put up with these conditions.

But ever since Google rewrote the book on how tech companies treat their employees, places like HubSpot have followed their lead by removing any sense of job security and treating employees as temporary workers. This means many start-ups offer no long-term contracts, pension plans or employee union, not to mention showing little loyalty to employees. 

This attitude also applied to the employee benefits HubSpot offered and the low wages it paid.

While HubSpot offered employees the perk of "unlimited vacation" time, this was just a way for them to spin the fact that they didn't have any vacation plan at all. Then, if an employee was fired, HubSpot didn't need to justify the firing and didn't owe the employee any money for accrued time off.

Lyons understood that these policies were made in an effort to cut costs ahead of the IPO.

Since growth, rather than turning a profit, is the most important thing leading up to an IPO, it was more important to raise sales numbers by using a poorly paid telemarketing team than to worry about pensions.

Lyons also understood that employees put up with it because HubSpot created an atmosphere that made them feel special.

He was surprised that employees didn't seem to be concerned about the lack of job security or the low wages and stressful quotas, and that they were quick to tell him they were on a mission for HubSpot. Further, the company kept them distracted with free beer, candy and games, and made them feel like part of a team, even though a member could get fired at any moment without explanation.

> _"Grow fast, lose money, go public."_

### 9. Even with a poor product and a bad forecast, tech companies like HubSpot can be successful by creating buzz. 

You might be wondering how a company like HubSpot can become successful. It's all about the buzz, which can be so powerful that both employees and investors may fall under its spell.

Even companies that make a poor product and fail to turn a profit can succeed; if they have positive buzz on their side, they can have a successful IPO.

And HubSpot did indeed have a poor product.

There's a definite irony in the fact that HubSpot was trying to sell marketing software that could help a small business. HubSpot certainly didn't use it; they relied on old fashioned telemarketing and cold-calling techniques.

But none of this mattered — as long as they created enough buzz to attract investors at the public offering.

In the business, creating buzz is compared to "making a movie."

This means they create a mythological narrative for their business. In HubSpot's case, it was the revolutionary story of changing people's lives through its software. They cast one of their young co-founders in the role of attractive leading man and made his story into a hero's journey, giving the impression that he was overcoming impressive obstacles.

When the IPO rolled around, investors were lined up like it was opening night for a blockbuster.

Even a weak prospectus won't get in the way of good buzz. It was clear in HubSpot's IPO prospectus that they had a history of losses and there was a good chance that the company wouldn't become profitable anytime soon.

But HubSpot had built such a strong buzz, and crafted such a compelling story, that the IPO was a roaring success and led to the co-founders becoming multi-millionaires.

> _"The old tech industry was run by engineers and MBAs; the new tech industry is populated by young, amoral hustlers."_

### 10. Lyons’s success at creating buzz helped him to cope with HubSpot's ageist culture and get a new job. 

Lyons actually played a significant role in helping HubSpot create its buzz leading up to the company's IPO.

As the former tech editor for _Newsweek_, many tech blogs took note and helped generate buzz by reporting on his move to work for HubSpot.

And during his time at HubSpot, Lyons also began working as a writer on HBO's critically acclaimed television series, _Silicon Valley_, which only added to the company's interesting story.

This was a good thing for Lyons, since this added buzz helped him keep his job in the face of HubSpot's prevailing ageist attitude.

The fifty-something-year-old Lyons never really fit in with the HubSpot crew, the majority of whom were in their twenties and clinging to a frat-culture frame of mind. But he did need the health insurance.

So, they put up with each other despite HubSpot's ageist attitude, which was well stated when one of HubSpot's co-founders was interviewed in the _New York Times_ : He mentioned that experience and gray hair was really overrated in the tech world and that the company wanted to build a culture that was specifically designed to attract "Gen Y'ers."

Lyons posted these remarks from the interview on his private Facebook page and in response, received a flood of support from his followers.

As it turned out, many people had their own stories to tell. They'd experienced ageism in the tech world, too, and were baffled that such remarks, which would get a CEO into hot water in most other industries, were tolerated.

HubSpot and Lyons eventually parted ways when HBO's _Silicon Valley_ was doing so well that he was offered a job to write for Gawker Media's _Valleywag_.

Strangely enough, HubSpot worked hard to put a negative spin on Lyons's exit, wording the memo that was circulated to give the impression that he was fired.

Stranger still, when word of Lyons's book got out, one top manager at HubSpot resigned and another was fired after an attempt at illicitly obtaining the manuscript ended up requiring the involvement of the FBI Cyber Division.

### 11. Final summary 

The key message in this book:

**The business practices at a tech start-up are not as transparent as you might think. Companies routinely push their company to reach an IPO without regard for making profit or a good product. On top of this, only a few investors and founders really hit the jackpot when the companies actually make it. Meanwhile, the average employee gets the short end of the stick, with little or no job security and a poor life-work balance.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Originals_** **by Adam Grant**

In _Originals_ (2016), Adam Grant taps into the field of original thinking and explores where great ideas come from. By following unconventional rules, Grant gives us helpful guidelines for how we can foster originality in every facet of our lives. He also shows that anyone can enhance his or her creativity, and gives foolproof methods for identifying our truly original ideas — and following through with them.
---

### Dan Lyons

Dan Lyons is a journalist, novelist and screenwriter. He was a technology editor at _Newsweek_ for many years and is currently on staff at the HBO series _Silicon Valley_. His blog, _The Secret Diary of Steve Jobs_, was a tremendous success.

