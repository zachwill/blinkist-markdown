---
id: 554881e93737610007000000
slug: diversity-and-complexity-en
published_date: 2015-05-05T00:00:00.000+00:00
author: Scott E. Page
title: Diversity and Complexity
subtitle: None
main_color: 74A0EC
text_color: 4F6DA1
---

# Diversity and Complexity

_None_

**Scott E. Page**

_Diversity and Complexity_ (2011) offers an in-depth examination of the nature of the world's complex systems, from natural ecosystems to economic markets. It outlines the reasons that diversity within such systems is so crucial, as well as showing how diversity helps fuel innovation and novelty.

---
### 1. What’s in it for me? Discover the intricate relationship between diversity and complexity. 

Did you know that there are over 35,000 types of spiders, 10,000 species of birds and some 440 varieties of shark? Such facts might make you wonder: how does such a small planet maintain such a wide diversity of life?

Diversity stems from the gradual process of evolution through natural selection — a process so intricate, with so many variables (temperature, food sources, geography and so on) that it is inherently complex. In short, the complexity of our world breeds its extraordinary diversity.

These blinks provide insight into the subtle dance between complexity and diversity, a dance which has led to the huge variety we enjoy in our world every day.

In these blinks, you'll discover

  * how a Mini Cooper and a Jeep are both the same and completely different;

  * why when it comes to diversity, smaller is actually better; and

  * why there are more varieties of automobile than of pencils.

### 2. There are three forms of diversity: within types, between types and of composition. 

What do you have in your refrigerator? You may have the same _types_ of foods, like various kinds of cheeses. You'll also have foods of different types, like eggs, milk and meat.

Why is this important? The differences between these items illustrate an important theme: there are three _types of differences_ between things. In other words, there are three types of _diversity_.

First, there's diversity _within a type_. This kind of diversity is determined by the variations in the type's particular characteristics.

Think about a specific species of animal, like a cardinal. Each cardinal is unique — some are redder or have slightly smaller beaks — but they're all still cardinals. They're all the same _species_ of bird.

At some point, variations become so great that we can't call them _differences within a type_ anymore. Instead, we're really looking at _different types_.

Compare cardinals and flamingoes. They're similar enough to be the same _type_ : they are birds. Yet they're distinct enough to be considered a separate species. Cardinals and flamingoes are thus _different types_ of birds.

The third kind of diversity is about the way separate parts are put together, called _composition_. Individual pieces can produce something quite different when they're put together in a certain way.

Think about a washing machine or an airplane. They're both comprised of several smaller parts, like bolts, screws and a spinning engine. However, these parts combine to create something completely new and distinct in each example.

So when considering diversity, we have three types. Diversity _within a type_ can be called _variation_, diversity _between types_ can be called _variety_, and then there's the factor of composition.

But how are these sorts of diversity created? Read on for more.

### 3. Diversity within types, or variation, occurs when new individuals are produced in different ways. 

Let's start by examining the ways in which diversity arises _within_ different types.

This usually begins when new individuals of a certain type are created. When a new individual is produced, errors or _drifts_ can occur.

_Mutation_ is one such drift. It's caused by minor changes in the production process.

One of the clearest examples of mutation is in the development of life. Mutations in DNA result in offspring with slightly different characteristics. The tiniest DNA mutation can make a certain bird's beak longer than usual, for instance.

_Inversion_ is also another type of drift. Inversion happens when the information that's used to produce an individual gets _inverted_, or flipped, thus creating a new variation in that type. So if a code, like 10100100, gets inverted during the copying process, it could end up being 01001010. If an inversion happens within DNA coding, for example, it could result in a significantly different type.

Think of an inversion like an inside-out spring roll: while it has the same ingredients, they're assembled the wrong way around.

Diversity within a type can also occur when elements from different variations are combined or transferred to each other.

_Recombination_ happens, for example, when two people combine their genes to produce a child. If the mother can be represented as 00000000 and father as 11110000, their offspring might be a random combination of the two, such as 00110000.

Recombination happens in a cultural context as well. When musician Bruce Springsteen says Woody Guthrie and Bob Dylan are his influences, he's really describing recombination in a metaphorical sense.

When elements transfer only in one direction instead of mixing together, it's called _transference_. Transference is common in the products we use and consume. A cup holder, for example, can be transferred to boats, lawn chairs or cars.

### 4. Complex and adaptive systems, like the world we live in, encourage greater diversity. 

You're probably familiar with Darwin's idea that organisms best adapted to their environment are able to survive and pass on their genes. But shouldn't this theory result in _less_ diversity, as the few stronger individuals triumph over weaker individuals?

It doesn't, however. Evolution through natural selection has produced a tremendous diversity of life.

But how can this be? The answer lies in the complexity of the world's environments.

Environments are shaped by a number of different factors, such as temperature, altitude, water presence or land type. These factors are constantly changing, as they're influenced by things like climate change or plate tectonics. These changing factors encourage diversity.

When environments are constantly changing, the characteristics that allow organisms to survive within them are also constantly changing. A slight change in temperature could cause different plants to proliferate. Certain birds would then have to adapt to a change in food source. Over a longer period of time, these changes could result in the evolution of a new species.

Another key feature in complex and diverse environments is the close relationships that appear between the different types within it. This, in turn, leads to even greater diversity.

A successful species will go into decline if it uses up too much of its food source. Imagine a species of bird, for example, running out of worms. The birds will also decline if there's an increase in the number of predators, like cats.

So diverse environments produce birds that rely on different resources and have different predators. The diversity of the environment is what allows a species to exist, as without such diversity, the species might go extinct.

Ultimately, the world thrives because it's a complex space that's constantly in flux. The diversity across types, in addition to the variation in environment, keeps the world healthy.

### 5. Diversity in composition arises through the various ways individual parts are put together. 

Imagine a simple chair. Which elements are needed to assemble it? A seat, legs, perhaps a backrest?

There are an infinite number of chairs that can be produced depending on the way each of these individual elements are designed, and in the way the parts are put together.

The multiple compositions of these individual parts is what creates diversity.

Looking at it another way, we can compare two mammals, such as rats and humans. The cells these two mammals are comprised of are virtually identical. It's the way the cells are assembled that makes the two organisms so vastly different.

Cars work in a similar fashion, as most cars are built with the same parts: engines, doors, air bags and so on. Yet how a car is designed and manufactured can mean the difference between a sturdy Jeep and a sporty Mini Cooper.

So how do various types arise? A species needs to find its _niche_, or an environment in which it can survive and thrive. These niches set the stage for different types to proliferate.

Successful restaurants, for instance, often rely on finding a niche in the market. A restaurant owner needs to create a certain kind of space in which his staff can tailor the menu and experience to a customer's individual taste.

The many variables involved can create a number of different niches for a restaurant to exploit. Even just two basic variables, such as spice or plate size, result in many niches: mildly spiced dishes served on family-sized platters; chili-rich dishes served as small, tapas-style plates; and so on.

These niches can exist side by side, too. So in sum, even a small number of variables can result in quite a bit of diversity.

### 6. There are many constraints that can limit diversity. 

How many ways can you build a house, compared with the ways you can build a bench?

There are limits to diversity, and these limits are set by a large number of factors. For example, the number of ways a type's parts can be put together may be limited. There can also be a limit to a part's dimensions.

We can understand this better by thinking about the formula _X D_ _,_ where _D_ is the number of possible dimensions. The value of _D_ has an enormous effect on diversity, in that the more dimensions, the more diversity.

There are more models of cars than pencils, for instance. A car has many dimensions; that is, it can have several kinds of seats, doors, windows or colors. A pencil has fewer dimensions, so it by default has less diversity.

A type's size compared to the size of its niche also acts as a constraint on diversity. Think of it this way: larger animals need more food and a greater space in which to live. For this reason, the earth maintains a greater number of smaller organisms, like insects and bacteria, and fewer large animals, like elephants and blue whales. Larger animals just require more resources to sustain themselves.

The largest animals are found in vast landscapes for the same reason. Whales live in open oceans, while elephants live in African savannas.

If there's too much interdependence between types, this also reduces diversity. When types rely on each other, they're much more limited in their scope.

This is why there are so many more books than computer operating systems. Books aren't dependent on each other; and they're less dependent on other factors. Cookbooks don't rely on history books, for example, and self-help books don't rely on novels.

Computer operating systems, on the other hand, need to integrate with many other types, such as other software programs, types of computers or servers.

### 7. More parts in a system does not signal complexity. Complexity is found between order and randomness. 

Which is inherently more complex: the seating on an airplane or the movements of people on a dance floor?

Actually, the answer is neither. The first is organized in a specific order, whereas the second tends to be quite random. Complexity, however, is found somewhere between the two, represented by the acronym _BOAR_ : _between order and randomness_.

A system can have many parts and still not be complex. When you're building something with instructions, like an IKEA bookshelf, there might be many individual pieces but the process of assembly is straightforward.

Randomness, on the other hand, is different. By definition, randomness is _not_ complex.

Consider the act of flipping a coin. The outcome will always be random. Adding more coins won't make it any less random or more complex.

Complex systems have a mixture of parts, some which are orderly and some which are random.

Think of "cakewalk," a game similar to musical chairs. Participants play by walking around a clock on the floor, and they stop when the music stops. Then a number is drawn, and the person standing on that number on the clock is the winner.

The game does have an order, as participants play according to a set of rules, one of which is that everyone walks in the same direction around the clock. There are also random elements: participants walk at different speeds, and no one knows when the music will stop.

So in observing the patterns of movement in the game, it actually is quite complex.

### 8. Complex systems cannot easily be predicted and tend to be unstable. 

Complexity, by definition, isn't simple. The difficulty of explaining complexity is part of what makes it complex!

You can describe something as _complicated_, but that doesn't necessarily mean it's _complex_.

What's the difference? A technical device like a blender might be _complicated_, but it's not complex. It's easy to describe and predict what it can do. It's very unlikely that your blender will start making French toast, for example.

So what is complexity?

In a complex system, entities don't necessarily act according to rules, so the system is much more difficult to describe and predict.

Let's go back to the game of "cakewalk." Imagine if the participants stopped walking in the same direction and started moving in different directions. Some walk backwards, and some move sideways. The game would evolve, and it would be more difficult to describe and predict the game's potential results.

A complex system also has a number of _tipping points_. If any are reached, large-scale chaos can ensue.

Part of the reason it's difficult to predict what a complex system will do is that huge changes or crises can occur quickly and with little initial input.

For example, imagine a population of 100 people. At least 20 people need to start rioting for the population to lose its stability and fall apart. This might make society seem mostly stable and easy to predict, but that's not necessarily true.

What if you only need ten people to start a riot that quickly grows to 20 people? If the society has other similar traits, it isn't as simple or predictable as you might think. One small change, then, can suddenly cascade through the entire system, affecting it tremendously.

### 9. Diversity increases robustness in complex systems. 

What's our most important skill in a tough situation? It's really our skill in combining the _other_ skills we have. This is called _robustness_.

Robustness is the ability to function in periods of disturbance or uncertain conditions. On average, systems with greater diversity are _more_ _robust_ than systems with less diversity.

To understand this, think of a field of crops. The higher the number of crops the field has, the more it yields. In other words, more crops make it more robust.

Imagine a field that only has two kinds of crops. Each crop has a mean yield of 100 pounds with a standard deviation of 10. So there's a 95 percent chance the whole field will yield between 80 to 120 pounds.

Now imagine a field with 16 kinds of crops with the same mean and standard deviation. This field's yield would have a 95 percent chance of being between 95 and 105 pounds. A more diverse system is _more_ robust, so it has less variation in performance.

Systems with limited diversity tend to be susceptible to the same influences, which also makes them less robust.

Think about a stock portfolio that's made up of companies that all rely on oil, like car manufacturers or airlines. A change in oil prices would directly affect the performance of these companies. Thus a stock portfolio like this would be _less_ robust than a more diverse portfolio.

Ecosystems work the same way. If all the species in the same ecosystem can be easily influenced by the same factors, the ecosystem is less robust.

The Irish potato famine was a tragic example of this sort of weak ecosystem. Before the famine, almost all the potato plants in Ireland were of the same variety. There was little variation, so there was little diversity.

When potato blight came to Ireland, it devastated potato crops, as potato plants (being nearly all of the same variety) were highly susceptible to the disease.

### 10. Too much of the same offers diminishing returns in complex systems. 

What things do you need in a bedroom? A dresser, an overhead light, a bed? Would the room function better if it had several beds? Not really, as you probably only need one.

The concept of _diminishing returns_ states that when you add a certain type to a system, every time you add another individual of the same type, that addition is less valuable than the first added. New individuals still add value, granted, but less and less so with each addition.

Let's say we have to assemble a product development team. If our product is to be an app, adding a computer engineer would increase the team's performance significantly. Adding a second engineer, however, might be helpful but not as helpful as our initial guy. Adding a third would probably be overkill; and so on.

Our team instead would really benefit by adding other types of specialists rather than more computer engineers. A _diversity of skills_ would increase our team's performance overall.

So instead of adding more of the same type to a system, it's better to add different types. This makes a system perform better overall.

Let's look at an example that contains a formula for determining how well a system is performing.

A shopping mall has three types of stores: type _A_, type _B_ and type _C_. The first store of each type might increase the mall's overall performance by 30 percent; the second store of each type by 20 percent; and the third store of each type by 10 percent.

When we combine these performance percentages, we can get a sense of how well the system as a whole is doing.

So if our shopping mall has one of each type of store — ABC — our mall has a system performance of 90 percent. Yet if our mall has two of one type of store and one of another — AAB — our mall has a system performance of just 80 percent.

And if we've a mall with three of the same type of store — AAA — our performance is just 60 percent. In sum, a diverse mall will deliver better results.

### 11. A highly complex environment has a greater need for diversity. 

Think about a typical day, and all the things you do. Without thinking consciously about it, we participate every day in a number of very complex systems, from saying hello to a high school friend on social media to riding the train into work.

What's more, our world is progressively becoming more complex. What does this mean for the future?

The pace of change has accelerated, and our lives are increasingly more interdependent and interconnected. This also increases the complexity of our lives' "systems."

We've already seen how diversity makes complex systems perform better. This means that diversity today is more important than ever. Yet this isn't necessarily happening.

For example, the plight of bees has been the topic of much speculation, as a lack of diversity in their environment has led to colony problems. Bees require a variety of flower species to sustain themselves, so modern agrarian monocultures are potentially harmful.

We could face a large-scale environmental crisis if bees were to go extinct, as many plant species rely on bees to spread pollen and assist in plant reproduction.

Our complex world systems are also more likely to produce large-scale events, especially if we fail to diversify sufficiently.

The 2008 financial crisis, for example, was caused largely by a lack of diversity in financial products (subprime mortgages) and strategies. Greater diversity would've made the system more robust. Even if the crisis had occurred in a robust system, robustness would've made it recover faster.

Stock portfolios work in a similar fashion. A balanced portfolio, with a combination of stocks from various industries with varying levels of risk, might not always earn the highest profits but will be much more resistant to market swings. If one sector is hit, the portfolio won't suffer much. A diverse portfolio is therefore more robust.

### 12. Final summary 

The key message in this book:

**Complex systems have an intimate relationship with diversity. Greater diversity means greater robustness, which makes a system perform better. Diversity also encourages innovation, as it pushes types within a system to search for new niches in which to compete and thrive. These characteristics are true of all complex systems in the world, from ecosystems to financial markets.**

**Suggested** **further** **reading:** ** _Simply Complexity_** **by Neil F. Johnson**

In _Simply_ _Complexity_, Neil F. Johnson presents an introduction to complexity theory, explaining what complex systems are, where we can spot them in everyday life and how we can benefit from understanding complexity. Although a young field, complexity science already offers us ways to help explain and potentially avoid complex phenomena, such as traffic jams, financial market crashes and modern warfare.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Scott E. Page

Scott E. Page is an author, social scientist and professor at the University of Michigan. He's also the director of the Center for the Study of Complex Systems at the Santa Fe Institute. He's written several books, including _The Difference: How the Power of Diversity Creates Better Groups, Firms, Schools and Societies_.

