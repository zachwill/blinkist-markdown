---
id: 5a3d2e2db238e10007128b73
slug: do-i-make-myself-clear-en
published_date: 2017-12-26T00:00:00.000+00:00
author: Harold Evans
title: Do I Make Myself Clear?
subtitle: Why Writing Well Matters
main_color: CEA966
text_color: 826B40
---

# Do I Make Myself Clear?

_Why Writing Well Matters_

**Harold Evans**

_Do I Make Myself Clear?_ (2017) offers a much-needed look at why clear and concise messages are, now more than ever, so important. There is an overwhelming abundance of content these days, and yet finding the truth has never been more difficult. Politicians and marketing executives use deliberately misleading words that obscure the truth and leave us confused and distrustful. Other times, bad writing simply leaves us scratching our heads. If we hope to better understand the facts, we need more people who can deliver clear and meaningful writing.

---
### 1. What’s in it for me? Get your message across with clear and concise communication. 

Is there anything worse than dragging yourself through a contract, trying to understand the dense legal language regarding matters that may be of the utmost importance to you? Or desperately trying to stay awake while reading an academic paper on a potentially fascinating subject, but being unable to enjoy it because of the jargon and obscure vocabulary?

These days, in a digital era without word limits, we have to endure more inscrutable prose than ever. Gone are the days when a writer or editor had to make their message crystal clear in the space of a paragraph or a page.

In these blinks, you'll learn the rules of clear communication. In the process, you'll not only improve your prose; you'll rediscover the beauty and importance of clarity.

You'll also learn

  * why you should avoid using the passive voice;

  * how to remove any words that cloud your message; and

  * how to slay "zombies" and "flesh-eaters."

### 2. There is an abundance of bad writing on the internet, but good, clear writing can be learned. 

If you're a long-time fan of good journalism, there was probably a time when you looked forward to reading the latest issue of the _New Republic_ magazine. When it existed in print form, each issue was a guaranteed good read, filled with sharp and engaging writing.

But in 2012, the publication was purchased by Facebook co-founder Chris Hughes, who hired new writers and moved it strictly online. Many people worried that the quality would suffer, and these worries were pretty much confirmed when the new venture was described in a cryptic press release as a "cross-functional collaboration" to "align themselves from a metabolism perspective" as a "vertically integrated digital media company."

Sadly, this kind of mumbo jumbo is found all over the internet.

Take another example. The _Financial Times_ asserts that their online content is "improving the efficacy of measurable learning outcomes." Say what now?

In the days when print newspapers were the leading source of information, there was a limited amount of space that a writer could use to get his message across to the reader. This meant that the writer had to be clear, and get straight to the point.

Online content, on the other hand, generally fills up space using a lot of words to say very little, especially now that it includes clickbait and fake news.

But it isn't just websites and Facebook that are to blame; TV news reporting and academia are also home to some of the worst writing around.

However, It doesn't have to be this way. Contrary to what some might say, writing isn't some talent with which one is imbued at birth. Like any other skill, writing can be improved if you're determined and put in the effort.

Even the Bard himself, William Shakespeare, worked hard to improve his writing.

From his unspectacular early work, to later works like _King Lear_ and _The Tempest_, Shakespeare's career is representative of how a dedicated writer can improve his craft.

And this is something you can do as well. In the blinks ahead we'll look at a variety of tips and tools you can put to work today.

### 3. It’s helpful to be aware of traditional writing structures and readability indexes, but don’t be over-reliant on them. 

There's nothing wrong with the proper sentence structures you were taught in grade school, but keep in mind: If each sentence is structured the same way, the writing will be pretty dull.

It's wise to remember that, first and foremost, the job of a sentence is to express a complete thought, and there's more than one way to do this. Classic sentence structures are guidelines. They're there to ensure your subject, verb and predicate are in the right place, and thus to make your complete thought easily understandable.

But you can strike the subject, verb and predicate and still have a useful sentence. For example, a paragraph can end with a one word sentence. Rejoice! (You see?)

Another piece of advice writers are often given is to keep things simple. This is true as well, and it's always a good idea to keep an eye out for needless adverbs and adjectives that you can shed to make your sentences clearer. But you don't want to write one boring short sentence after another.

Remember, the goal is clear communication. While simple sentences like, "The cat sat on the mat," are perfectly understandable, you're not communicating well if you're boring the reader to sleep. And a paragraph of such sentences _is_ tedious.

Helpful advice can also come from a readability index, but this is another guide you shouldn't place too much confidence in.

Since the late 1800s, researchers have been examining the properties of clear writing and it's given us formulas like the _Flesch Reading Ease Index_, which provides a score to suggest how difficult it is to read a piece of writing. There's also the _Flesch-Kincaid grade level_, which tells you how much schooling the reader needs to understand the writing.

Other guides include the _Gunning fog index_ and the _Dale-Chall Formula_, the latter of which takes into account how many "difficult" words appear in your writing.

These analytics can be insightful — for example, Flesch's formula tells us that the ideal sentence has an average of 18 words — but they can't tell you everything. In fact, you could write pure nonsense and, as long as you used basic vocabulary and relatively short sentences, it would score well on the indexes.

> You can enter your text online at readabilityformulas.com or readability-score.com to see how your text scores on variety of different formulas.

### 4. Avoid front-loading your sentences and overusing the passive voice. 

Harold Evans has edited some major authors, among them Norman Mailer, E.L. Doctorow and Gore Vidal. And though each has his own unique style, they all follow the same principles of clear and effective writing.

Most writers agree that the passive voice should be avoided.

This is because the passive voice adds needless words to your sentence and strips away the urgency and command that the active voice provides.

For example, you might write a memo in the passive voice that reads, "It was decided that the next employee meeting should be held on Monday." But in the active voice, the memo can clearly state, "The next employee meeting is on Monday."

Of course, every rule has its exceptions and the passive voice can be useful for showing tact or delicacy around a sensitive subject. In other cases, you might want to use the passive voice to write a sentence that highlights the receiver rather than the doer, which is something the passive voice can do by putting the receiver at the front of a sentence.

So if your story is focussed on a baby, and not, say, a president, it might be best to write, "The baby was kissed by the president," rather than, "The president kissed the baby."

Another golden rule is to avoid front-loading your sentences with dozens of words before stating your point.

Here's a typical example: "Given the problems of unfriendly climate, poor infrastructure, various militant groups vying for bribes and a lack of refrigerated trucks, it was difficult for the government to transport food to the village."

This sentence forces the reader to keep track of twenty words and a list of problems before they know what the issue is. A better and clearer sentence would state the problem first and _then_ list the reasons.

> _"All great writing focuses on the significant details of human life and in simple, concrete terms."_

### 5. Interrogate your sentences and remove unnecessary words like adverbs and parasitical prepositions. 

If you've worked in an academic, bureaucratic, political or technological setting, then there's a good chance you're familiar with writing that's been puffed up with unnecessary words. Sometimes these superfluous words are used in an effort to sound intellectual or cutting edge; in other cases, they've been deliberately employed to confuse the reader.

Believe it or not, it's common for voting proposals to be written in a deliberately confusing manner so that bewildered citizens will vote for a law that they would otherwise _never_ vote for.

Confusing language is full of unnecessary words such as adverbs, adjectives, parasitical prepositions and abstract nouns. As one of the world's most successful authors, Stephen King, once said: "The road to hell is paved with adverbs."

So the simple rule of thumb is to _ration adjectives and raze adverbs_.

Adverbs are words like "exactly," "precisely" and "really," and, in most cases, you'll find that you can remove these words while losing nothing of value. Instead of writing, "the price was exactly five dollars" just say, "the price was five dollars."

Generally speaking, adjectives are adverbs without the "-ly" at the end, such as "precise" and "exact," and should be used sparingly.

Remember: a writer's job is to express a clear thought. When she does this well, she will naturally avoid superlatives. A writer shouldn't say an event was "shocking," but explain _why_ it shocked people.

_Parasitical prepositions_ are another type of useless word.

These appear after perfectly functional words and add nothing of value. The words "up" and "out" are common ones. So the next time you write, "Let's meet _up_ at the cafe and test _out_ the new app," why not save a few words and write, "Let's meet at the cafe and test the new app!"

And then there are _abstract nouns_. These words have no concrete definition and therefore fail to provide any clarity to your sentences. They include: "regard," "indication," "facilities" and "issue."

Be specific! Don't say you "take issue" with something when you can tell us exactly how you feel.

> _"You cannot make yourself clear with a vocabulary steeped in vagueness."_

### 6. Clear writing avoids overusing not’s and engages the reader by resisting the trap of dry, mechanical language. 

Every writer knows that a double negative is a big no-no. You _don't_ want to _not_ keep that in mind when trying to be clear. Get the picture?

When appropriate, you can even go one step further in your avoidance of nots by looking for chances to turn your negative sentences into positive ones. After all, a writer should tell the reader what _is_ happening rather than what isn't.

Most of the time, a quality sentence will be clearer and use fewer words than a poor one. So, instead of writing about how "it is unlikely that the fees will not be raised next year" you can simply write that "the fees will likely increase next year."

Assertive writing like this will always be more engaging, meaningful and understandable to the reader.

Another writing pitfall to be aware of is dry and mechanical language.

Good writing has a certain rhythm to it. And just as in music, variation is a virtue.

There are three ways to keep your writing lively and your reader engaged; you can vary the _form_, the _function_ and the _style_.

Sentences come in two _forms_ : simple and complex. Good writing mixes these two together. So you can first use a few simple sentences, like, "She got in the car and drove away," to draw the reader in and then use a longer, more complex and satisfying sentence — or vice versa.

You can vary your _function_ by mixing up statements, commands, questions and exclamations. Attention, reader! Why not break things up by throwing in a question every once in awhile?

Then there's _style_. There are three at your disposal: _loose sentences_, which are conversational and reflect the way everyday people talk; _periodic sentences_, which are tight, punchy and great at emphasizing a point; and _balanced sentences_, which have more symmetry and order.

While a periodic sentence will make its point abruptly, a balanced sentence (like this one) does so calmly.

### 7. Beware of zombie nouns, verbose flesh-eaters and stale expressions. 

We've looked at parasitical prepositions, but the damage done by those pesky little words is nothing compared to the havoc wreaked by _zombie nouns_ and _flesh-eaters_.

_Zombie noun_ is a term coined by Professor Helen Sword, of the University of Auckland, and it refers to words that began life as a verb but have since been devoured and turned into deadly, sentence-ruining nouns.

Common zombie nouns include "implementation," "documentation" and "authorization," each of which were once perfectly good verbs — "implement," "document," "authorize." This transformation — or zombification, if you will — is also referred to, ironically enough, as a _nominalization_.

Zombies are also known to attack adjectives and emerge as redundant new words like "applicability" and "forgetfulness."

Every once in a while a zombie may be unavoidable, or at least justifiable — when, for instance, writing about an "invention" or someone's "imagination" — but it's best to search your work and double-check any word ending in "-ation," "-ance," "-mant," "-ment," "-ence" and "-sion." Such words could probably be removed or replaced with the original word to make your point clearer.

_Flesh-eaters_ are useless words and phrases that can murder a sentence.

Any unnecessarily wordy way of saying something simple is a flesh-eater. A common example is the phrase "in the possession of" rather than "has"; or "concerning the matter of" instead of "about."

As you probably know, flesh-eaters are frequently found in legal documents, which is why contracts are such a chore to read.

Flesh-eaters are similar to the stale, overused phrases that should be avoided whenever possible.

It's foolish to think we can avoid clichés altogether, but it's worthwhile to try and keep them out of your writing if you want it to sound fresh and original.

Part of the challenge of good writing is coming up with new expressions and ways of rephrasing clichés like "blazing inferno," "hammered out a deal," "last-ditch effort," "pool of blood" and so on.

### 8. As long as we care about meaning, good writing is a powerful antidote to a post-truth society. 

In his classic book _1984_, George Orwell invented "Newspeak," a form of language that both diminishes the meaning of certain words and eliminates free thought.

In Orwell's book, a totalitarian government has implemented a plan to have every citizen communicate solely in Newspeak by the year 2050. Well, recent events have shown that we might be running ahead of schedule since the true meaning of words are already being dangerously manipulated by politicians.

As Roger Cohen, a columnist for the _New York Times_, put it: "Emptying words of their meaning is an essential step on the road to autocratic rule." Part of what Cohen was commenting on was Donald Trump's boast that he won a "landslide" victory and that members of the media are "among the most dishonest human beings on earth."

Scottie Nell Hughes, a representative of the right-wing Tea Party, was on the _Diane Rehm Show_ when she said, "People say facts are facts — but they're not really facts."

Frequently, we hear Donald Trump and other notable politicians using words like "challengeable" and "questionable" to cast doubt on what are, in fact, facts.

Both the humanitarian author Hannah Arendt and the satirical writer Jonathan Swift wrote about the dangers of the "political lie." They pointed out the big difference between saying something dishonest as a way to avoid trouble and telling a lie as a way to "disempower facts" and create a "fictional narrative."

This is why it's of great importance that we fight back against these attacks on truth with good writing.

It's up to us to keep the truth alive, and to do that, we need to use the right words and show that we care about their real meaning. This includes knowing when to use "effect" or "affect," "continual" or "continuous," "loan" or "lend" and "reign" or "rein."

### 9. Politicians and banks use bad writing to make money and push forward harmful policies. 

You might be thinking that correct usage of a few words can't make much difference in the grand scheme of things, but as recent events show, bad writing can cost billions.

The recent recession that began in 2007 is a perfect example of how much damage bad writing can cause.

According to the journalist and _Financial Times_ editor Gillian Tett, banks write the details of their subprime loan packages in complicated and impenetrable language so that customers and regulators won't understand it. Just try reading the fine print of a Collateralized Debt Obligation (CDO) or a Structured Investment Vehicle (SIV).

If customers don't know exactly what they're buying, they won't have any ground to stand on if things go wrong. And if the people in charge of creating and enforcing regulations don't understand what's going on, it makes it pretty difficult for them to get in the way of the bank's money-making schemes.

In other words, it can be argued that bad writing brought the global financial system to its knees, causing thousands of people to lose their homes and life savings.

Former political speechwriter Barton Swaim has admitted that politicians also seek out meaningless language. In this case, it's often to distance themselves or buy time before taking any real stance on an issue.

But there are worse things going on behind the bad writing in politics. In the controversy surrounding climate change, political organizations intentionally employ misleading language to undermine the efforts of those trying to help the environment.

According to the Texas Republicans, "'climate change' is a political agenda which attempts to control every aspect of our lives."

Since 2009, Republicans have also been using misleading language to block the Affordable Care Act and turn people against it. They once famously claimed it called for "death panels" to kill senior citizens.

But the English language can be used for good as well. Today, it's more important than ever that we use it to fight for the truth.

### 10. Final summary 

The key message in this book:

**The importance of clear writing can't be overemphasized. Language shapes our thoughts and perceptions, and it can be used in both good and bad ways. Politicians and banks sometimes intentionally employ abstruse language to veil their agendas. So fight for clarity! By following a few simple guidelines, you can make your writing cogent and concise — and that will be good for all of us.**

Actionable advice:

**Beware of pleonasms.**

Pleonasms are common phrases that are completely redundant. To avoid embarrassment, you should avoid them. Examples include: "anonymous stranger," "circular shape," "collaborate together," "descend down," "new beginning," "merge together," "sink down," "still persists" and "uncommonly strange."

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Sense of Style_** **by Steven Pinker**

_The Sense of Style_ (2014) offers a refreshing and relevant guide to writing potent, readable texts of all kinds. Instead of extolling the same confusing and sometimes counter-intuitive rules found in traditional style guides, _The Sense of Style_ offers simple tricks and heuristics guaranteed to improve your writing.
---

### Harold Evans

Harold Evans is one of the most respected editors working today. Over the course of his illustrious career, he spent 14 years working for the _Sunday Times_ and seven years as president and publisher at Random House, US. In 2004, he was awarded a knighthood in recognition of his outstanding work as a newspaper editor.

