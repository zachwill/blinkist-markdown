---
id: 57225a695ed4340003a94ae2
slug: do-more-faster-en
published_date: 2016-05-05T00:00:00.000+00:00
author: David Cohen and Brad Feld
title: Do More Faster
subtitle: Techstars Lessons to Accelerate Your Startup
main_color: 66B7E8
text_color: 197D40
---

# Do More Faster

_Techstars Lessons to Accelerate Your Startup_

**David Cohen and Brad Feld**

_Do More Faster_ (2011) presents a step-by-step guide to launching your start-up. It's crucial to stay on track as you move toward success, from honing your ideas to finding the right team and getting investors. The author also stresses the importance of the work-life balance while showing you how to _do more faster_.

---
### 1. What’s in it for me? Discover the pillars every entrepreneurial success is built upon. 

You have a great business idea? You're very passionate about something in your life? Well then you might be the right candidate for growing a business and becoming a successful entrepreneur.

Of course, having an idea and being passionate aren't guarantees of success: you'll need more than just that.

These blinks will tell you about everything else you need to succeed in business, including how to turn your passion into your vocation, refine your product and reach out to investors. You'll also find out about who to hire and who to fire, how to address the basic legal questions you'll face and, finally, how to keep your personal life afloat while getting your business off the ground.

You'll also learn

  * how one man turned his passion for comics into a successful business;

  * why emails should be no more than three sentences long; and

  * why meetings on bicycles can be extremely productive.

### 2. A good business idea is important, but it’s not as important as passion. 

Ever felt that all you needed to get your new business off the ground was one great idea? Well, the reality is, a fantastic idea isn't the main prerequisite for making it in the business world.

There are probably other people out there who have the same or similar ideas, and there's always someone who could pull it together before you. A good idea doesn't guarantee an investment, either. Investors don't necessarily look for good ideas. What they look for is people with _passion_.

Ideas are powerful, but remember: they can always be improved. Don't settle on doing things a certain way. That's why you should be getting feedback about your project's strengths and weaknesses from other entrepreneurs, potential customers and investors, and using it to make your idea even better. If you don't listen to feedback, you might create a product no one really wants or needs.

Even the best ideas are still secondary to the most important ingredient in business: passion. The story of Kevin Mann, the founder and CTO of _Graphic.ly_, is a good example.

Graphic.ly is a digital distributor of comic books. Mann had the idea for the company after he once drove 100 miles to a comic book store just to buy a new release, only to find it had already sold out. He realized how passionate he was about comics and decided to help make them more accessible to fellow fans.

In other words: aim to create a business around something you truly care about, like Mann did. Passion — not just detailed business plans — is what keeps people going when they're starting out.

> "_One can steal ideas, but no one can steal execution or passion_."

### 3. Don’t go it alone: hire team members who can help and challenge you. 

Humans are social creatures. Don't forget that when you build your company!

Never try to start a business by yourself. There are a few reasons why it's not possible, even if you have all the necessary skills.

First off, you'll go through a lot of rough times in the beginning. Important investors could turn you down or the market could undergo unexpected changes. Positive team members help you stay motivated during such struggles. If you're on your own, you'll probably be more tempted to give up.

There's a lot of work to be done when you start your business, too. You have to refine your product, attract customers and investors, set up your work space — and that's just the beginning! You'll move a lot faster if you have a team to help you with these tasks.

When you have a lot of people on board, you also minimize your risk of running out of money before your project is ready to be launched, because you can execute important tasks faster and make your product ready for the market.

But you have to hire the right people. There's a simple rule that can help you here: always hire people who are better than you.

Find team members that can challenge you. Business partners should work together like athletes: athletes constantly work to stay in shape because their performance suffers if they don't. It's easier for them to finish a race or win a game when they maintain their fitness routine and work together.

So take time to look for the right people. Don't rush your recruiting decisions. You'll work with your team for long time, so it's important to get people who are talented and driven.

> "_The three most important things in entrepreneurship are people, people, people_."

### 4. Start-up success requires three key things: a flexible team, a high-quality product and well-written emails. 

What do you prefer: making decisions in small or large groups? If you've experienced both, you probably know it's usually easier in small groups because they're more flexible.

You might assume that large corporations have an edge on you, but that's not necessarily true. Large companies have a major drawback in efficiency: bureaucracy. Their bureaucratic procedures make them slower to adapt to any changes in the market. After all, they have multiple departments and management layers, so it harder to keep everyone on the same page. Start-ups are completely different.

Smaller start-ups function better for the same reason: a small group of people can make and execute decisions more efficiently.

Flexibility isn't everything, however. You also need a good product. So when it comes to designing your project, put quality over quantity.

Keep it simple. You'll make your product more difficult to use if you overload it with features. A product with just a few, high-quality features is much easier to market than a product with a lot of complicated features.

Think of it this way. Which would you prefer: an iron that flattens out wrinkles perfectly or a so-so iron that's also a kettle and an answering machine? Customers want something that can get the job done, not something with as many uses as possible.

Once you have a flexible team and good product, it's time to look for an investor. A key part of finding the right one is perfecting your emails.

So remember the _three sentence rule_ : keep the bodies of your emails down to three sentences or less. Long emails discourage busy people from reading them. If they're interested, they'll reply and you can send a more detailed email once you've gotten their attention.

> "_Sucking at e-mail is a surefire way to get your mentors, potential investors, and customers to lose interest in you_."

### 5. Raising money is about convincing investors, but there are other financial sources, too. 

You have to present yourself the right way if you want to attract investors. So make sure you're what they're looking for!

Investors invest when they're convinced a start-up team can successfully execute its idea. There's no hard-and-fast rule here, but investors usually look for people with passion, intelligence, empathy and natural leadership skills. When you meet with an investor, tell anecdotes that demonstrate these qualities in you and your team.

An investor won't base their decisions on the team alone: they'll also want to see a compelling product.

So figure out the most effective way to present yours. Would a video or a live demonstration suit your product best? Create a presentation that's engaging and informative.

It's also important that you know a lot about the market you're aiming for. Are there a lot of potential customers? What are they like? If you don't have in-depth knowledge of your market, investors will lose interest in you.

Investors aren't the only way to raise funds, either. Let's look at some of your other options.

_Angel investors_ are wealthy people who like your idea but aren't looking to make a huge profit. They may help your company get off the ground just out of interest or kindness.

Your customers might also help. It could be advantageous to sell your product to a small number of customers in the beginning, so you can raise some funds while remaining independent from investors for a while.

Bigger companies might also provide you with funding if you're partnered with them. If you have such a partnership, reach out to them about it directly because they might not come up with the idea themselves.

Finally, keep an eye out for R&D grants for technology start-ups.

> "_Investors look for a smart-ass team with a kick-ass product in a big-ass market._ "

### 6. Make sure you get the legal basics right. 

Are you familiar with business law? If not, don't worry. You only need to know a few things for now. In fact, there are only two really important legal issues you have to address in the beginning.

First off, what's your company's _type?_ In the United States, there are three general types of corporate forms: S-Corp (small business corporation), C-Corp (regular or "classic" corporations) and LLC ("limited liability corporation"). They're all taxed differently.

Each type has its own advantages and disadvantages, but for businesses trying to raise money from venture capitalists, it's best to be a C-Corp.

Next, you have to figure out how shares are divided in your company. It's important to have clear rules in place so your shares are distributed evenly.

_Vesting_ is a good strategy. In the practice of vesting, people aren't fully vested in their shares from the start. Instead, they earn them over time — usually over the course of around four years.

Vesting helps you avoid the problems that arise if someone leaves the company early. Imagine a company with three founders who each own a third of the shares. If one of them leaves after the first year, they'll still retain their shares, which can be disruptive to the company. Vesting incentivizes people to stay committed for longer, and protects the company if they don't.

It's also important to find the right lawyer, so look for one with expertise in your field. It's critical to stay on top of legal issues. If you're a tech company looking to patent something, for instance, you have to make sure your patent is absolutely correct. A competitor might sue you if it's not, which could cost you a lot of money or even force you to give up.

### 7. Keep a healthy work-life balance and pursue other activities or regularly disconnect from work. 

Work can be a lot of fun if it revolves around your passion. But don't forget that life isn't just about work!

Maintaining a healthy work-life balance is important for two main reasons. The first one is: if you work too much and don't give yourself time to recharge, you'll eventually burn out, which might put you back at square one.

The second reason is: you run the risk of neglecting people in your personal life if you spend all your time on work. You'll strain your relationships with friends and family if you don't spend enough time with them. That just creates more stress for you, and it can follow you back to work.

Sometimes you can balance work and life in creative ways, such as by combining work with your favorite leisure activities. Seth Levine, the managing director of _Foundry Group_, loves cycling, so he decided to try to incorporate it into his work.

Foundry Group is a venture-capital firm that invests in start-ups. Levine started inviting start-up members to discuss their ideas on bike rides. He says that holding these cycling meetings helps him come up with more creative ideas and maintain his physical fitness at the same time.

You still have to fully disconnect from work sometimes, however. Treat yourself to a week-long vacation once in a while. Vacations don't just help you decompress; they regenerate your creativity so you'll be even more productive and motivated when you get back.

So even when your business is in its most hectic periods, don't neglect the importance of your work-life balance. If you do, you could end up suffering both at work and at home.

### 8. Final summary 

The key message in this book:

**Starting a business shouldn't be about filling out forms and doing calculations. It should be about passion. So focus on a field or topic that's meaningful to you and assemble a passionate team that can challenge you. Present yourself the right way to investors, stay on track legally and don't neglect your personal life. Long-term success is about hard work, happiness and balancing the two.**

Actionable advice:

**End your relationships with bigger companies if they aren't productive.**

Be careful when you collaborate with bigger organizations because some start-ups lose a lot of time here. Remember: big companies are slower than you, so don't keep waiting for them if you can move forward more easily without them.

**Suggested further reading:** ** _The Hard Thing About Hard Things_** **by Ben Horowitz**

_These blinks explain why the job of a CEO is among the toughest and loneliest in the world, and how you can survive all the stress and heartache involved._
---

### David Cohen and Brad Feld

David Cohen is the founder of Techstars and has successfully started several other companies. Now he invests primarily in internet start-ups. Brad Feld works for the venture capital company Foundry Group and has been active as an investor and entrepreneur for over twenty-five years.

©David Cohen and Brad Feld: Do more faster copyright 2010, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

