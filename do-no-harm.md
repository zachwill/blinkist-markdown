---
id: 55ed2b7b11ad1a000900005a
slug: do-no-harm-en
published_date: 2015-09-09T00:00:00.000+00:00
author: Henry Marsh
title: Do No Harm
subtitle: Stories of Life, Death, and Brain Surgery
main_color: 4450A0
text_color: 4450A0
---

# Do No Harm

_Stories of Life, Death, and Brain Surgery_

**Henry Marsh**

_Do No Harm_ (2014) is the memoir of leading London neurosurgeon Henry Marsh, whose anecdotes and recollections provide an intimate look into the operating room. Marsh has learned that much in his vocation falls within a moral grey area — and that much in life does, too.

---
### 1. What’s in it for me? Discover what the life of a leading neurosurgeon is like. 

When the television show _ER_ became a smash hit in the US, the number of applicants to medical schools around the country increased significantly. Something about doctors captures our hearts; their job seems so full of purpose compared to others. Making tough decisions and saving lives — who wouldn't want to do that?

But, as these blinks explain, there are no Dr. Houses in the real world. Even the best doctors and surgeons make mistakes. They're not gods. And when they do fail, they, and their patients, have to live with the consequences. 

Leading London neurosurgeon Henry Marsh has a long and successful career behind him and has saved and improved the lives of many. But he's also made serious mistakes that have injured and changed patients' lives forever. These blinks tell you his story.

In these blinks, you'll learn

  * how to cope with having damaged a patient's brain forever;

  * why having a bad memory is a good thing if you're a surgeon; and

  * why death is sometimes better than life.

### 2. A career in surgery requires a balance between detachment and compassion, hope and realism. 

Henry Marsh has been a Consultant Neurosurgeon at London's Atkinson Morley's and St. George's Hospital since 1987. It is his hope that his stories will help people understand the difficulties doctors face — difficulties that often have more to do with human nature than technical setbacks.

One such difficulty is our ability to empathize. The author recalls that, when he was a medical student, it was easy to feel sympathy for patients, as he wasn't yet responsible for the outcome of their treatments. However, as he moved up the ladder and gained new responsibilities, feeling this sympathy became harder.

Responsibility entails a fear of failure, making patients a source of anxiety and stress. Marsh, like many other doctors, became hardened over time, regarding patients as a species entirely different from the invulnerable doctors like himself.

This doesn't mean that there is no place for hope or empathy. But striking the balance between hope and realism is difficult when developing a medical prognosis; if doctors venture too far on either side of the spectrum, they can either condemn their patients to live in hopeless despair for the remainder of their lives, or end up being accused of dishonesty or incompetence when things like tumors turn out to be fatal.

According to the author, one of the most anxiety-inducing situations in surgery is when surgeons operate on other surgeons. For instance, when he needed retinal surgery, he knew that his friend (who was also a doctor) saw this request for treatment as both a compliment and a curse. In these situations, the usual rules of detachment break down — the operating surgeon feels exposed because his patient knows he is fallible.

However, this learned detachment fades over time. Now that the author is older, he has become less frightened, and more accepting, of failure and mistakes. He's realized that he is made of the same flesh and blood as his patients, and is equally vulnerable and fallible.

### 3. Doctors are human, just like the rest of us. 

Despite their vast medical knowledge, doctors are no different than anyone else; they're human, and they'll make mistakes. But this realization only comes with maturity, humility and experience.

In fact, there's just one way to become a good doctor: practice, take on challenges, make mistakes (some of which will seriously injure patients) and learn from them.

To illustrate this point, the author recalls a surgery during which he worked too hard for too long. The strain led to his damaging a man's brain by removing too much of a brain tumor. Comatose, this man spent the rest of his days in a nursing home.

The author was haunted by memories of this man, but in the end his hubris taught him a valuable lesson: operate in stages, ask colleagues for help and know when to stop a procedure.

Learning to let go of pride and embrace humility has not only made the author a better doctor, but also a better person.

Indeed, this lesson spilled over into other areas of the author's life. For example, he recounts a story in which he was standing in a long line at the grocery store waiting for checkout. He became annoyed that an important neurosurgeon like himself had to wait in line with the rest of the rabble after a triumphant day's work.

Then he remembered that the value of his work is measured by the value of other people's lives — like those of the people in front of him in line. It was a humbling experience.

Furthermore, accepting that doctors will make mistakes goes hand in hand with accepting that some things — even things on the operating table — come down to luck.

Success and failure are often out of the doctor's control. Sometimes, no matter how much you get right, things go wrong anyway. Neurosurgeons aren't gods, after all. They're beholden to the same dumb luck that rules the rest of us.

### 4. There is rarely a clear “right” decision when it comes to neurosurgery. 

Life is full of difficult decisions. But in neurosurgery the stakes are so high that making the right decision can feel like a nightmare. 

As you've already learned, even surgeons can't escape chance. This makes the decision of whether to operate quite tricky. 

For example, the author remembers a patient who was very athletic and passionate about biking and running. However, he had a serious tumor, and there was a chance that operating might leave him in a vegetative state, unable to take care of himself ever again. So, considering the patient's passions and the devastating risks of this particular surgery, is it right to operate?

Ultimately, there are no black-and-white solutions to such surgical quandaries. That's because the questions extend beyond surgical expertise into deeply ethical, life-and-death considerations.

The difficulty of these decisions can be seen in one case in which the author and his team debated whether to operate on a very elderly woman, whose chances of continuing an independent life post-surgery were close to zero. But the patient would have rather died than live in a nursing home. While some doctors argued that they couldn't just let her die, the author countered: "Why not? It's what she wants."

Would it perhaps have been better to simply end her pain with euthanasia? And is euthanasia an ethical practice?

The author touches on this question when asking himself what he would do if he were diagnosed with a malignant brain tumor. His solution would be suicide, though he admits that he'll never know for certain until he is actually faced with that decision.

Nevertheless, sometimes a quick death can be better than a slow one, and the author understands that death isn't necessarily the worst possible outcome. Imagine living out your days in a vegetative state due to a failed operation — is that better than a quick death?

### 5. Being a neurosurgeon exposes you to some of the most difficult questions of the human condition. 

Part of finding the balance between detachment and passion, optimism and realism, lies in confronting the reality that the human condition is full of complexities. Confronting these complexities is only more difficult in areas where the stakes are high, like neurosurgery.

The author remembers being a student working as a nursing assistant at a psycho-geriatric ward. Though he found the work to be miserable and unrewarding, it nonetheless taught him about the limitations of human kindness, in particular his own.

The author found that many of the patients in the hospital had been there for years. Some of them had had _lobotomies_ — a procedure that, by severing the frontal lobes of the brain, was once thought to turn schizophrenics into happier, calmer people. In reality, however, they became catatonic, lifeless and zombie-like. 

He was astounded to find that many of the patients had no medical notes, and that no checkups had been performed on patients who had been in the hospital for decades. To the author, this reflected the apathy and cold-heartedness of some of the doctors and nurses in the hospital.

While we all want to believe in the power of human love and kindness, the reality is that very difficult and discouraging working conditions can wear people down, bringing out the cruelty, apathy and laziness that exists alongside our virtue.

In fact, most neurosurgeons' lives are punctuated by periods of deep despair. Psychological research has consistently shown that the most reliable route to personal happiness is making others happy. Although the author has made many patients immensely happy with successful operations, these highs are accompanied by the emotional nadirs following his many terrible failures and mistakes.

But there is a silver lining. The author remembers an old boss telling him, "Great surgeons tend to have bad memories." Mastering despair and the haunting memories of mistakes is essential to being able to carry on, overcoming mistakes and, ultimately, improving.

> _"The surgeon...has known heaven, having been so close to hell."_

### 6. Final summary 

The key message in this book:

**Neurosurgeons aren't all-knowing gods. They're human beings, just like you. Their expertise and vast medical knowledge don't prevent them from making very human mistakes and wrestling with real ethical dilemmas. But it's precisely this fallibility that makes them so good at what they do.**

**Suggested** **further** **reading:** ** _Being Mortal_** **by Atul Gawande**

_Being Mortal_ (2014) helps the reader navigate and understand one of life's most sobering inevitabilities: death. In this book, you will learn about the successes and failures of modern society's approach to death and dying. You'll also learn how to confront death and, by doing so, how to make the most out of life.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Henry Marsh

Henry Marsh is counted among Britain's foremost neurosurgeons and has been the subject of two documentary films. As a senior consultant at St. George's Hospital in London, he helped develop a revolutionary surgical procedure that keeps patients awake through local anesthesia in order to reduce damage to the patient's brain during surgery.

