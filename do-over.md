---
id: 5524612230666500070b0000
slug: do-over-en
published_date: 2015-04-08T00:00:00.000+00:00
author: Jon Acuff
title: Do Over
subtitle: Rescue Monday, Reinvent Your Work, and Never Get Stuck
main_color: 30C4F2
text_color: 1C728C
---

# Do Over

_Rescue Monday, Reinvent Your Work, and Never Get Stuck_

**Jon Acuff**

_Do Over_ (2015) explains why drastic change in your career is hardly something to be afraid of — that is, as long as you've got the tools you need. These blinks guide you through building up your own Career Savings Account filled with tips and tricks to turn your dead-end job into an opportunity to do something totally new.

---
### 1. What’s in it for me? Learn how you can launch yourself into your dream job. 

Do you like your job? If the answer is yes, then you are in the minority; a 2013 Gallup poll showed that 63 percent of Americans don't feel "engaged" in their job, and a further 24 percent outright hate them.

And yet despite all this disengagement with our work, we don't seem to do anything about it. How many of us resolve to quit our lame job and launch a more fulfilling career? So few of us do this because, even though we hate our jobs, we fear change even more.

But it doesn't have to be this way. These blinks show you how to transform your life and become someone who, unafraid of change, is always on the lookout for a better, more fulfilling career.

In these blinks you'll discover

  * why your most important skills may be invisible;

  * how changing your career depends on your Career Savings Account; and

  * that starting at the bottom can get you to the top.

### 2. A career do-over is always possible – there’s no need to be afraid! 

Do you love Monday mornings? Do you spring from your bed excited to begin yet another amazing week of great work at your fantastic office with your awesome colleagues? It's not unlikely that your and most other people's answer to that is a resounding _NO._

In fact, up to 70 percent of Americans said they hated their jobs or felt disengaged, according to a Gallup poll in 2013. If you're one of those people who lives for the weekend, then perhaps it's time for a _Do Over_, that is, a complete reorientation of your career.

Though change can be frightening, it can also greatly improve our lives. For example, after he began working freelance, the author suddenly had the freedom to live according to his own schedule and to engage in more creative work. It took courage to make that first change — but, clearly, it was worth it.

Perhaps you too can imagine the positive changes a Do Over would bring about, and yet, you don't quite feel brave enough to take the plunge. One way to make things easier on yourself is to build up a _Career Savings Account?_

So how do you get your Career Savings Account, or CSA, started? Well, your CSA is your abilities — how well you build strong relationships, learn new skills, improve your character and up the hustle you put into your work. It's a toolkit that will help you during those tough periods of transition in your career.

Of course, these transition periods don't all look the same for everyone. There are, however, four main types of transition, times during which a Do Over is possible. In order to give you a better understanding what skills and tools to put in your CSA, these blinks will explore all four species of transition, starting with that seeming dead end, the _career bump._

### 3. Surround yourself with a supportive network to overcome any career bump. 

Sometimes our career comes to what seems like a dead standstill. Perhaps you get laid off. Or perhaps, after graduating college, you spend months fruitlessly searching for work. Such _career bumps_ are difficult to overcome, especially if you're unsure whom to turn to.

So it's crucial to develop supportive and reliable relationships. That way, you'll be prepared to cope with a career bump _before it happens_.

Begin by taking stock of those people with whom you already have a relationship. Don't only count your current coworkers; include people from previous workplaces, too, as well as your friends and acquaintances. What kinds of relationships are these?

It's important to classify these relationships so that you can tell your _friends_ from your _foes_.

Most of us have foes in our life — those people who keep us from achieving our goals. And often they do this in ways we don't at first recognize. For example, your buddy that keeps you up late in a bar will hamper your productivity.

So if some of friends are, in fact, foes, how do you tell the difference? Try asking yourself, "Who do I know that has experience and wisdom to offer about the world of work?" Whoever fits the bill is someone you'll want to build a strong relationship with. These are your friends — and also, perhaps, your _advocates_.

Our advocates provide insight and help untangle tough decisions by proffering considered advice. Friends, on the other hand, support your decisions, as well as motivate and inspire you with their own career choices. Friends and advocates can help you deal with tough situations now, or they may be of help at some point farther down the track.

But remember: friendship is a two-way street! Give back to these people when they need a hand or face a tough period of transition. Keep in touch. And if you do part ways, stay on good terms. By being there for them, you can ensure that they'll be there for you when you face a career bump of your own.

> _"Relationships get you the first gig."_

### 4. Break through your career ceiling with new skills. 

Say you've got a stable job. Everything's going fine, and you've got nothing to complain about. Well, except that you're _dissatisfied,_ and you feel that there's no way out. When we lose the ability to imagine a better job for ourselves, we've hit a _career ceiling._ There are, however, ways to break through this barrier.

How?

It's all about your skills. Ask yourself, "What am I good at?" If you draw a blank after listing just a handful of your abilities, it's time to think a little harder.

You may have a lot of _invisible skills_ — abilities that you may not recognize as strengths, but which are nevertheless incredibly useful. Say you're good at organizing dinner parties. This quality might not seem like anything special at first, but think about it: it also reflects your general organizational talent. That's a strength you shouldn't neglect.

Re-evaluating your own skills will give you the attitude boost you need in order to truly envision yourself creating change in your career. But now that you can visualize change, perhaps you've spotted a few things missing from the picture. Are there new skills that would benefit you?

If so, then don't wait to go out there and learn them. Not only will they bolster your resume; they might also lead you on new adventures.

Consider Brazilian immigrant, Alex Atala, who came to Europe with very little to his name or on his CV.

He began working in a restaurant, one of the few places he could get work. But by starting at the bottom, he found himself developing new strengths that ultimately helped him make the decision to open his own restaurant. Atala's restaurant was voted the 7th best in the S. Pellegrino list of the world's 50 best restaurants. Atala shows us that it really does pay off to invest in new skills!

> _"Relationships get you the first gig but skills get you the second."_

### 5. Hit the ground running in your career jump by investing in your character. 

Perhaps after improving your skills, you decide that you don't want simply to move up in your industry. Instead, you'd like to create an even bigger change by starting your very own enterprise. You're looking at a _career jump_, and in order to make the leap, you'll need to work on your character.

A strong character will give you the ability to build stronger relationships, and to always leave a lasting impression on those you meet. But beware: negative impressions are far worse than no impression at all. Coworkers won't ever forget them.

Just think about that former colleague of yours — remember, the one who always expected the worst outcome? Did you completely forget that she made great cake, too? Negative traits will nullify positive ones every time if you let them.

But by working vigilantly to keep our narcissism, dishonesty and pessimism at bay, our positive traits will begin to shine through. So what characteristics _should_ we be building up? The top three are _generosity_, _empathy_ and _presence_.

Generosity is vital in creating loyalty and trust. Say you're a boss that's just received free tickets to a football game. You'd love to go, but you also know your employee would, too. Why not simply pass them on to him, and you can catch the game on TV? It's a small gesture, but you'll have guaranteed his loyalty, as well as his support in future ventures. 

Empathy, your ability to understand the needs of others and behave accordingly, and presence, your ability to pay attention, will prove especially useful as you begin to handle the new relationships, opportunities and challenges of a career jump. So where should you start?

It's simple. Just put away your phone, look your coworker in the eye and for the first time, truly _listen_ to them when they talk about their family.

### 6. Hustle your way to a career opportunity. 

It might be a bit of a stretch, but imagine this for a moment: Your boss unexpectedly announces that he's moving to Hawaii. This means that you'll get his position — your dream job. Right now, you're in the middle of a fantastic _career opportunity._ So how can you make sure you grab it?

You've got to have _grit_ and _hustle_. 

Whatever images the word "grit" brings to mind, within this context it means making the conscious decision to overcome your fear of the unknown, and to work hard to achieve your goals. This means taking questions like, "What if I don't have what it takes?" or "What if I miss another opportunity?" and throw them out the window.

Grit will allow you to ignore self-doubt and become a full-time swing dance instructor (if that's your heart's desire), even if you worry that swing dancing is only a fad.

With grit in your arsenal, let's turn to your hustle. In your career, hustle is the desire to reach your goal. It _does_ require some flexibility, though. Remember: you don't need to be perfect at your job from the outset. But you _do_ need to actively try your best and engage with learning.

So simply rigidly holding onto your initial expectations won't work. Doing that prevents growth. If you want to embrace new opportunities, expect the unexpected, and welcome it when it arrives. Consider this: A decade ago the only way to publish was through publishing houses. Then along came the internet, which opened up completely new avenues for authors to achieve success.

Additionally, the level of hustle you'll need changes in different seasons. Some periods don't require too much hustle; others will require your hustling harder, such as when you're preparing for that job interview or cramming for your exams. So keep those seasons in mind, and your hustle won't let you down!

### 7. Final summary 

The key message in this book:

**To deal with any career bumps, jumps, ceilings and opportunities you encounter, just remember the acronym SHINS: find a _Supportive_ network, _Hustle_ for new opportunities, _Invest_ in your character and acquire _New Skills_.**

**Suggested** **further** **reading:** ** _Quitter_** **by Jon Acuff**

Finding out where you really want to be in life requires patience, hard work and planning, but anyone can do it with the right dedication. Filled with many personal anecdotes from a 12-year span of job-hopping, _Quitter_ shows you the smartest way to quit your day job for your dream job.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jon Acuff

Jon Acuff changed his life for the better when he joined the Dave Ramsey team to become a full-time author, in 2010. Since then, he has contributed to CNN.com, spoken nationally and written multiple books, including _Gazelles_, _Baby Steps And 37 Other Things Dave Ramsey Taught Me About Debt_ and _Stuff Christians Like_.

