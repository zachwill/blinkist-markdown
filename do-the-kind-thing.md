---
id: 55a38f5b37386100078a0000
slug: do-the-kind-thing-en
published_date: 2015-07-15T00:00:00.000+00:00
author: Daniel Lubetzky
title: Do the KIND Thing
subtitle: Think Boundlessly, Work Purposefully, Live Passionately
main_color: C1C54A
text_color: 5C5E23
---

# Do the KIND Thing

_Think Boundlessly, Work Purposefully, Live Passionately_

**Daniel Lubetzky**

_Do the KIND Thing_ (2015) passes along the valuable lessons the author learned while building his successful and ethical brand. These blinks guide you through the ten fundamental tenets of creating a business that benefits the individual and the world, and is prosperous too.

---
### 1. What’s in it for me? Discover how to sell great products while doing the kind thing. 

Most people link a company's success solely to profit and growth. But Daniel Lubetzky, the creator of the popular KIND Healthy Snacks, encourages companies not only to focus on numbers, but also to _do the kind thing_.

These blinks take a closer look at the ten central tenets of doing the kind thing, teaching you how to run a business that is both economically sustainable and socially impactful. Learn how practices such as insisting on the word "and," being true to your brand and staying grounded in nature will help you sell great products, all while making the world a better place.

In these blinks, you'll discover

  * what sun-dried tomato spread has to do with forging peace;

  * why see-through wrapping can earn people's trust; and

  * how giving your seat to an elderly subway passenger might earn you some snack bars.

### 2. Being kind to yourself and the world is the first tenet of the KIND philosophy. 

Author Daniel Lubetzky built his company, KIND, around the idea of a business that was both economically sustainable _and_ socially impactful. His motto, of course, was "Do the KIND thing." Let's take a closer look at what kindness means to the author.

On one hand, kindness implies taking care of your own body by, for instance, eating healthy foods or using gentle cosmetics. KIND got started by releasing its line of KIND Healthy Snacks, which, instead of containing synthetic emulsions and pastes, are made from the finest whole nuts and fruits.

Doing the KIND thing is also about being kind to the world, a principle that shaped the author's 1994 cooperative venture that brought Arab and Israeli innovators together to create a line of KIND products. KIND proved that a company can come up with production processes that help forge social change by building bridges between people, even those on opposing sides of a political conflict.

The philosophy behind KIND is to think in terms of "AND" instead of "OR," so that opportunities arise that benefit not only oneself, but also the world.

Many people seem to think that a business will be either economically sustainable _or_ socially impactful, as if these two models were mutually exclusive. But this isn't the case. People should shift their perspective and take the stance that a business must be economically sustainable _and_ socially impactful.

The philosophy of KIND can be applied by all sorts of entrepreneurs who want to challenge their assumptions about what is truly attainable, and build businesses that are both kind to themselves and to the world at large.

### 3. KIND’s second and third tenets encourage making a passion your purpose, and achieving it through determination. 

Many entrepreneurs start their businesses with dreams of power and financial gain. But these goals rarely help them when they encounter bumps in the road. Since the journey toward your business goals will always be turbulent, you'll need a purpose that will fuel you and keep you on track.

Consider the author's story. With a Jewish background and family members who had suffered in concentration camps during the Second World War, he is passionate about creating peace.

This is exactly the passion that led him to use one of his early businesses to build a peaceful, cooperative relationship between team members from Palestine, Israel, Egypt and Turkey through the production of a sun-dried tomato spread.

The author's passion was aligned with his business purpose, giving this endeavor the staying power it needed, even when the business had trouble getting off the ground. Indeed, doing the right thing often takes a lot more effort than taking the easy way out, so determination is vital.

The author encountered many challenges when starting up his own KIND business, selling healthy snack bars. Because of their high quality, his bars were more expensive to produce than his competitors' products, which contained cheaper emulsions and pastes.

Selling a healthy snack bar was another challenge altogether, as it was new to a market in which healthy products were practically unheard of and had yet to gain a foothold. Nevertheless, the author was determined to make his business grow, and even went door-to-door around the country to sell the bars.

If you want a kind business like the author's, you must be determined to stick to your values, no matter what obstacles you face. By sticking to your values, you'll also let customers know that your brand is one they can trust. Find out why this is so important in the next blink!

### 4. KIND’s fourth tenet entails being true to your brand to earn your customers’ trust. 

Any business seeking long-term success must be willing to create a brand that people trust — but this is only possible if you stay true to your word. Start by asking yourself what it is about your brand that consumers can rely on.

The author failed to ask himself this question when launching a new sweet-and-spicy Asian Teriyaki pepper spread; the esoteric Asian flavor had nothing to do with the KIND brand, which was based on Mediterranean flavors.

Consumers were disappointed, their trust in the brand was damaged and sales declined considerably. To avoid a mistake like this, always keep your brand in mind when innovating, so that new products are consistent with the brand identity that your customers know and love.

Staying true to your brand also entails focusing on the stores where your product performs best. Remember: selling a new product isn't about entering as many stores as possible — in fact, this is more likely to be a waste of resources than anything else!

The author and his team began selling their snack bars with a focus on leading health food stores and specialty chains. They figured these stores might be the best fit for their product, and they were right. At Whole Foods locations across the US, hundreds of KIND bars were sometimes sold in a single day. In contrast, only one or two bars would be sold in generic convenience stores.

By investing the most in those stores that really make your product sell, you'll be ensuring that the customers who truly care about your product are reaping its benefits. You'll also be more likely to earn their trust, developing a strong, steady stream of income and getting a return on your investment.

### 5. Keeping products simple and grounded in nature is KIND’s fifth tenet; staying on brand when innovating is the sixth. 

Today's marketplace is flooded with cheaply manufactured products that respond to fluctuating trends. These trends are short lived, and consumer demand shifts accordingly. But if your products are honest and down to earth, you'll ensure your business has staying power.

This idea makes up the fifth KIND tenet, and is what led the author's snack bar to sustainable success. How? Well, if he had created a low-carb version of the snack bar in order to jump on the low-carb bandwagon, he might have experienced some temporary success. But it wouldn't have lasted, since this trend came and went in the same way all consumer trends do.

In the '70s, consumers were keen to buy processed foods — but by the '90s, they were on the lookout for foods that were grounded in nature. By keeping your product simple, you'll make sure your business isn't affected by consumer whims.

Another way to make sure your business keeps prospering is by staying on brand. This will protect you from any competitors that enter the market.

If you confuse consumers by changing your brand identity while innovating, you'll again see a loss of trust among your customers. Though it's tempting, there is no need for you to compromise your brand in order to keep up with competitors.

As the KIND healthy snack bars gained traction, several companies launched competing products. A new entrant claimed that their snack bar contained less sugar than the KIND bars. Instead of compromising the KIND brand by using emulsions to lower the bars' sugar content, KIND launched the KIND Nuts and Spices line. These bars had less sugar due to the removal of fruits, and they stayed consistent with the company's nature-grounded brand, making them a hit with consumers.

### 6. Transparent and authentic marketing is the seventh KIND tenet. 

Everywhere we look, we're bombarded with advertising for all manner of consumer products. Though these ads promise happier lives and fulfilled dreams, the products often only disappoint us. Why? Because they can't live up to their own marketing.

Extravagant promises are just another way of damaging your customers' trust. If your product is authentic and your campaigns are transparent, you'll make your brand more trustworthy than others on the market. Use your products' names, designs and marketing to ensure that what customers see is exactly what they get.

For instance, one KIND product is called Dark Chocolate Cherry Cashew, and not, for instance, Black Forest Cake Supreme. Consumers know precisely what to expect when they purchase the product because of the no-frills name.

KIND also uses transparent wrappers, so consumers can literally see through the packaging, making it nearly impossible for them to expect more than what they're getting. Moreover, KIND markets their products with pictures that aren't stylized or romanticized, but that simply tell the truth.

If your brand is authentic, consumers will choose your products, even if the products themselves aren't perfect. After all, authenticity has nothing to do with being perfect — it's about sticking to who you are and what you believe in.

Remember the Greek yogurt craze? Several companies attempted to profit from this trend by introducing sweets and snack bars with a coating reminiscent of Greek yogurt. But this coating was far removed from the authentic Greek recipe. Though many were fooled, most people were able to distinguish between authentic Greek yogurt and those making use of artificial flavors. Consumers lost their trust in the artificial products, and stuck with the authentic ones.

### 7. KIND’s eighth tenet is showing empathy, and its ninth is trusting your team to lead. 

There's no denying that it feels great when someone does you a favor or gives you a compliment. In fact, you'll probably want to spend more time with that person because of it. Why not make your brand do the same thing by celebrating empathy?

As part of a promotional campaign, KIND developed the #kindawesome cards, which were to be handed out to people in public performing _kind_ acts. If, for instance, someone on the subway offered their seat to an elderly or pregnant woman, and a person from KIND spotted them, the thoughtful passenger would be handed a #kindawesome card with a code that could be entered on the KIND website. This code would then unlock a packet of snacks that would be sent to the person's address.

By linking the concept of empathy to your business, people will develop powerful positive associations with your brand. And, of course, if people have positive associations with your brand, they're more likely to purchase your products.

The ninth tenet of running a kind business is learning to take a step back and let your team lead. Having insightful and dedicated team members is an asset to your business, and if you let them take the spotlight, you'll be pleasantly surprised at their skillful contributions.

At the 2013 National Product Expo West trade show, one of the world's largest, the author and his KIND team were presenting new products to their largest and most loyal retail accounts. The author thought that he could speak best about the products' features, so he started off the presentation on his own.

However, as the presentation wore on, he observed how well his team presented the products and realized that they were performing just as well as he was, and sometimes even better! By letting his team lead, KIND made a memorable presentation and every retailer bought its entire line of new products.

### 8. KIND’s tenth tenet encompasses a focus on resourcefulness and the empowerment of your team members. 

Running a kind business is demanding, and it's vital that you keep your team going, even in the face of setbacks. But how? Enter the tenth and final tenet of the kind philosophy: a focus on resourcefulness and empowering your team.

So what does it mean to be resourceful? Well, for one thing, it's not the same as being frugal. If the main priority in your business is frugality, your team members' concern with budgets will come at the expense of other important aspects.

In KIND's early years, the author chased down every single overdue payment from his distributors and retailers. But he soon realized that the hours spent chasing a retailer who owed him $80 could have been better spent closing new deals. Resourcefulness became his new focus, and it served him well later on.

When KIND lost Starbucks as a retailer of the KIND bars, the team focused on their resourcefulness again. Instead of being blinded by budget concerns, they focused on how to get into new stores and coffee chains. As a result, they successfully entered several well-regarded chains like The Coffee Bean & Tea Leaf and Peet's Coffee & Tea.

Finally, if you empower your team members to think and act like owners, you'll foster a sense of responsibility and commitment. KIND gives all its team members stock options, treats every employee equally and encourages them to speak up and share their ideas, leaving team members feeling appreciated and committed to the business.

So, whenever the business is doing well, everyone can take credit for the success. Likewise, if things are _not_ going well, everyone will feel responsible for getting the business back on track; in this way, the responsibility for your business is shared. This is incredibly valuable for both your business and your employees, creating a KIND experience for everyone involved!

### 9. Final summary 

The key message in this book:

**Doing the KIND thing is about being kind to both yourself and to the world. By creating an authentic brand that is driven by your passions and trusted by consumers, you can ensure that your business makes a positive social impact while achieving long-term success.**

Actionable advice:

**The "Push" is not all you have to consider.**

Next time you _push_ a store to stock your product, stop for one moment and consider this: Did you think about the _pull_ factor? Will the store's customers _pull_ the product off the shelves? It doesn't do your business much good if you succeed in getting your product on the shelves, but customers don't buy it. This might also result in a poor relationship with the buyer. So the _push_ factor is not the whole story — you have to consider the _pull_ factor as well.

**Suggested** **further** **reading:** ** _Delivering Happiness_** **by Tony Hsieh**

The central theme of the book is the business of literally delivering happiness while living a life of passion and purpose. _Delivering Happiness_ tells the story of Tony Hsieh and his company Zappos, demonstrating how thinking long-term and following your passions can not only lead to profits but also a happy life for your employees, your customers, and yourself. The book describes an alternative approach to corporate culture that focuses on the simple concept of making people around you happy, and by doing so increasing your own happiness.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Daniel Lubetzky

Daniel Lubetzky is a social entrepreneur. He is the founder of PeaceWorks and KIND Healthy Snacks, and his work in business has been recognized by the World Economic Forum, _BusinessWeek_ and _TIME_ magazine.

