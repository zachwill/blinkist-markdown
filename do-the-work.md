---
id: 561c18c73635640007540000
slug: do-the-work-en
published_date: 2015-10-13T00:00:00.000+00:00
author: Steven Pressfield
title: Do the Work
subtitle: Overcome Resistance and Get Out of Your Own Way
main_color: C5B470
text_color: 736941
---

# Do the Work

_Overcome Resistance and Get Out of Your Own Way_

**Steven Pressfield**

_Do the Work_ (2011) outlines ways to help you conquer your fears, stop procrastinating and accomplish the things you've long desired. Learn about the many ways you can fight _resistance_, the negative internal force that tries to stop all of us from reaching our goals.

---
### 1. What’s in it for me? Identify your inner resistance, and learn about how it can make you your own worst enemy. 

Have you ever gotten stuck in whatever task you're pursuing, unable to transform the ideas in your head into a concrete finished product?

Most of us find ourselves in that position far more often than we would like. When it comes to bringing our creativity to life, it can be almost impossible to get out of these ruts. So, what exactly is this inner resistance that stops us from achieving our goals, and where does it come from?

These blinks offer the help you need to identify the causes of resistance and how it stops you from taking action. Explore various techniques to overcome resistance and get back on track, so you can finally make your dreams, ambitions and goals a reality.

In these blinks, you'll learn

  * how aviator Charles Lindbergh was helped by his ignorance and arrogance;

  * why your enemy in the struggle to get things done might be you; and

  * why author Michael Crichton slept in hotels to save his marriage.

### 2. Identify your enemies and allies so you can more effectively get things done. 

Certain things help you move toward your goals, while others get in the way. Good music might motivate you to work out, for example, while your favorite television show might make you want to stay home. These are the _allies_ and _enemies_ in your life. 

Ambition and self-awareness are the real keys to achieving any goal, but they're always under threat by a common enemy: _resistance_. 

Resistance manifests itself in a number of ways. When you're working on something meaningful that you have real passion for, but find yourself bogged down by self-criticism and doubt, that's resistance setting in. 

Any activity that requires a lot of energy and time can draw resistance. Actually, the more important an activity is to you, the more you'll have to fight the resistance that acts against it. 

Acting was so important to Henry Fonda, for instance, that he would throw up every time before he went on stage — even when he was 75 and already one of the most famous actors in the world!

Passion and dedication are strong allies, but they aren't the only ones. Traits that we tend to perceive as negative — like ignorance and stubbornness — can be allies too, especially for artists and entrepreneurs.

Such "negative" characteristics helped famous American aviator Charles Lindbergh accomplish extraordinary feats. He had no idea how difficult it would be to fly for 33 hours straight, but his ignorance and stubbornness, along with his arrogance, made him believe he could do it. 

And he succeeded: Lindbergh was the first person in history to be in New York on one day and Paris the next. 

Stubbornness also helps you stay dedicated to your work, especially in difficult times. Steve Jobs was notorious for being stubborn, and this was instrumental while guiding his company, Apple, from its modest beginnings to becoming one of the most successful technology companies in the world.

### 3. Excessive preparation, overthinking and working without a clear structure are all barriers to completing your goals. 

Have you ever had a great idea, but failed to put it into practice because you spent so much time preparing for it? If so, you aren't alone. 

Let's look at some strategies for avoiding this counterproductive habit. 

Excessive preparation and overthinking prevent you from getting things done, so learn to resist them. Working is like meditation — you have to get into a mental state where you can assess your own thoughts and feelings objectively. If you start to feel self-doubt, learn to ignore it — that doubt is a form of resistance. 

It's better to act first and reflect on it later, especially when you're starting a new project. Start focusing on _doing_ work, not thinking about it. 

So if you're a writer, for example, _action_ means putting words on the page, while _reflection_ means evaluating what you've written the next day. Don't try to do these two things at the same time; instead, create productive schedules and structures for yourself.

One useful structure to apply is the _three-act structure_. In the three-act structure, you divide your idea into its beginning, middle and end.

This basic structure can even be applied to the creation of the social media site Facebook. In the beginning, Facebook was a digital meeting space where anyone could create their own page. Its middle act was that each person could decide who is allowed to access the page, creating an interconnected web of contacts. Now, in its end phase, people can communicate and share anything they want through a worldwide community of friends. 

So, when you're ready to take on a big project, stop and ask yourself some basic questions. What is this project about? What is its theme? What's missing?

> _"Whatever you can do or dream you can, begin it. Boldness has genius, power and magic in it." — Johann Wolfgang von Goethe_

### 4. Overcome creative blocks by confronting the enemies within you. 

It's normal to hit a block now and then when you're working on something creative. Most of us lose confidence when this happens, but there are ways to overcome creative blocks. 

When you hit a wall, stay calm and remember that it's always possible to get past it. Recognize that there's an enemy force that's actively working against you and your dreams. 

This enemy isn't hidden within your boss, your children or your spouse — it's inside _you_. It's in your head and only _you_ can defeat it, so don't place blame on the outside world. 

But remember that just because this enemy is inside you, it doesn't mean that it _is_ you. You haven't done anything wrong yourself; it's just resistance trying to block your true creative self. 

You are a knight and your resistance is the dragon. To overcome this fierce and persistent creature, find your love for your work, and for what you have already created.

When resistance appears, it forces you to ask two questions of yourself; only one answer to each of these will allow you to go on with your personal pursuit. 

The first question is, _how badly do you want this?_ If your answer is that you're just in it for the money, fame or power, you simply won't succeed. You must be completely committed to pushing yourself toward your goal, which is why the only answer to this question that will allow you to push on is "_totally committed_." 

The second question is, _why do you want this?_ It's alright if you want something just for the fun or beauty of it, but your answer should include something along the lines of, "_because I have no choice."_

Only this kind of profound dedication and determination will push you to do what you _really_ want.

### 5. Finishing your work and overcoming fear and crashes make you stronger. 

At some point in your career, you're almost certainly going to face the _Big Crash_, a major obstacle you have to overcome. Perhaps your computer suddenly crashes and takes your unfinished novel with it, or maybe a major problem in your personal life pushes you off track. 

You can't predict when a Big Crash will hit, but when it does, remember: it's just another problem, and you can get through it. 

Crashes can be extremely difficult and demanding, but they can be of benefit to you in the long run. A crash forces you to figure out what is and isn't working in your project. 

The author experienced this after he finished his book, _The Profession_, which had taken him two years to write. He gave it to his friends to read and, to his surprise, they hated it. 

After several long conversations with colleagues, he was able to improve the book — but only after another year of hard work. 

You may also experience a crash when you advance to a higher level in your work or creative pursuits, and realize that you're about to cross a threshold. Much like a child taking his first steps away from his mother, each new attempt to do so will bring the courage to go further. 

Like new advances, the fear of success is also a sort of crash. In fact, it lies at the very core of resistance itself — the fear of success is an enemy you will have to face. 

But resistance is often strongest when you're about to finish a project and have to push through it. Take bestselling author Michael Crichton as an example: whenever he was about to finish a novel, he would wake up earlier and earlier each morning to keep his creative momentum going, working around the clock until he was finished. 

Sometimes he would even stay in a hotel because his increasingly erratic routine would drive his wife mad!

The good news about overcoming struggles like this is that the more problems you overcome, the easier they'll be to overcome in the future. Crashes ultimately help us become wiser and stronger.

> _"Our deepest fear is not that we are inadequate. Our deepest fear is that we are powerful beyond measure." — Marianne Williamson_

### 6. Final summary 

The key message in this book:

**Often we stop dead in our tracks just when we're about to start achieving real success. Instead of just going for it, we can overthink our actions. Take the time you need to identify your enemies and allies along the path to your goal, and stay focused. Remember that challenges are just an opportunity to grow stronger; you can defeat resistance if you remain completely dedicated.**

Actionable advice:

**Don't let fear overcome you.**

Fear kills passion. When you feel the urge to abandon your work, take a step back and try to quiet the chattering in your mind. Focus on the joy and satisfaction your work brings you, rather than on the fear of failure. Conquering this fear is a major step toward always working with passion. 

**Suggested further reading:** ** _The_** **_War_** **_of_** **_Art_** **by Steven Pressfield**

In _The_ _War_ _of_ _Art_, author Steven Pressfield helps you identify your inner creative battles against fear and self-doubt and offers advice on how to win those battles. An inspirational book for anyone who's had trouble realizing their passion, it offers an examination of those negative forces that keep you from realizing your dreams, and shows how you can defeat your fears to achieve your creative goals.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Steven Pressfield

Steve Pressfield is a writer of nonfiction, historical fiction and screenplays. His first book, _The Legend of Bagger Vance_, was made into a Hollywood film in 2000. He's also the author of the bestselling book, _The War of Art_.

