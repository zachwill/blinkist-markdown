---
id: 5c75682e6cee070007e641a8
slug: do-what-you-are-en
published_date: 2019-02-28T00:00:00.000+00:00
author: Paul D. Tieger, Barbara Barron & Kelly Tieger
title: Do What You Are
subtitle: Discover the Perfect Career for You Through the Secrets of Personality Types
main_color: F36C3D
text_color: A64A2A
---

# Do What You Are

_Discover the Perfect Career for You Through the Secrets of Personality Types_

**Paul D. Tieger, Barbara Barron & Kelly Tieger**

_Do What You Are_ (1992) is a classic career guide, updated in 2014 to even better suit today's constantly changing job market. By becoming more aware of their unique personality types, millions of people have found more fulfilling and satisfying careers doing work that better coincides with their natural strengths, interests and temperaments.

---
### 1. What’s in it for me? Find out why you may be better suited for a different career. 

If you've ever struggled with a job only to realize that your coworker is doing the same thing with complete ease and a smile on her face, then you'll realize how your personality can affect your work. The truth is, no two people — not even identical twins — will share a personality. Likewise, what one person finds enjoyable and to be rewarding work the next person might find tedious.

It's also safe to say that it's much easier to know what you don't like to do than what you _do_ like to do. Fortunately, knowledge of your personality type can help steer you to work you'll likely enjoy and away from work you'll hate.

Thanks to research into the likes, dislikes and behavioral traits of people around the world, we have a nuanced idea of different personality types. And while no two people are alike, we share certain commonalities based on preferences for how we interact with the world, take in new information, make decisions and structure our lives. To find out which personality type you have and what kind of work would suit you best, read on.

In these blinks, you'll also learn

  * how the wrong job is like writing with the wrong hand;

  * what separates a _Traditionalist_ from an _Idealist_ ; and

  * how you can benefit from having an "encore career."

### 2. The notion of personality types is enduring, and it could be the reason you’re unsatisfied at work. 

Do you dread having to drag yourself to work every morning? Does getting through a typical day feel like a monumental struggle that requires every ounce of your effort and leaves you feeling like an empty husk of a human being by 5 p.m.? Then there's a good chance that your personality just isn't suited to the job you're doing.

The notion of different personality types isn't some newfangled gimmick. It's a tried and true concept that goes back to ancient Greece and was perhaps most influentially refined, in 1921, by the esteemed psychoanalyst Carl Jung.

Not long afterward, an industrious American woman, Katherine Briggs, along with her daughter Isabel Briggs Myers, developed this concept even further. Following years of research and testing, in the 1940s they developed the Myers-Briggs Type Indicator (MBTI®), which relies on four preference scales and includes 16 different personality types, which we will go into more detail in later blinks.

But how does this history lesson relate to your career?

To begin with, personality types highlight the fact that everyone has their own set of preferences for interacting with the world around her. Therefore, your most satisfying career is going to be a job that suits your preferences and specific personality.

Working at a job that doesn't suit your personality type is like trying to write a sentence with the hand you don't naturally use for writing. Sure, you'll eventually finish the task, but each letter will likely be an uncomfortable and poorly executed struggle.

The truth is, work doesn't have to be an unpleasant chore. When your job suits your personality, you'll look forward to doing it and even feel energized by it!

Let's look at two people, Arthur and Julie, who were working as placement counselors at a job recruitment firm, a competitive job that required making dozens of calls a day and getting rejected by most of their targets. Arthur was a talkative, high-energy, thick-skinned person, while Julie was slow-paced, disliked conflict and preferred to focus on the minute details.

Since Arthur liked jumping from one conversation to the next and didn't take rejection personally, he thrived at this job, while it wasn't long before Julie quit. This just goes to show how critical your personality type is for professional success and fulfilment.

### 3. To find your personality type, start by determining how you interact with the world and take in information. 

So, how do you find out which of the 16 personality types is closest to you? As mentioned in the previous blink, the MBTI® involves four preference scales, essentially asking you to put yourself on a spectrum between two choices.

The first scale involves your preference for how you interact with the world and asks you to place yourself on the spectrum between _Extraversion_ (E) and _Introversion_ (I). Extraversion can be described as looking at the world and asking yourself, "How do I affect this?" While Introversion asks the question, "How does this affect me?"

A common trait for those who lean toward Extraversion is thinking by talking. For example, _extravert_ students will often raise their hand in class before their answer is fully formed in their mind. If called upon by the teacher, they'll think aloud and talk their way to a conclusive answer. _Introverts_, on the other hand, tend to mull their answer over in their head until they reach a firm conclusion. Introverts also tend to be energized by getting to spend some time alone, while extraverts are energized by being with other people.

In all of these scales, it should be understood that no one lacks both characteristics entirely, though everyone will lean more in one direction than the other, however significantly or minutely. If you're unsure which side of the scale you fall on, ask yourself: if I had to spend the rest of my life one way or the other, which would I prefer?

The next preference scale is between _Sensing_ (S) and _Intuition_ (N), and this is about how you take in new information.

_A sensor_ would be anyone who relies on the five senses of touch, taste, smell, feeling, sight and hearing, and concerns themselves with what can be measured and what is happening in the here and now. While sensors trust personal experience and are good at noticing and remembering, _intuitives_ trust hunches and are good at gaining insight and recognizing the future implications of a present situation.

So, while sensors see a situation and are concerned with "what is," an intuitive will focus on "what could be." Intuitives place a premium on imagination, and instead of concentrating on the face value of something, they'll think about how to interpret that information to uncover its underlying meaning.

One quick difference between sensors and intuitives is how they put something together. Sensors will look at the pile of parts for their new grill and be drawn toward the instructions, while an intuitive will often start putting it together, well, _intuitively_.

### 4. The last two pieces of your personality type concern decision making and whether you’re structured or spontaneous. 

At this point, you have a good idea of whether you're an extravert or introvert, and more of a sensor or an intuitive. Now, let's put together the last two aspects of your personality type.

The next preference scale is between two styles of decision making: _Thinking_ (T) and _Feeling_ (F).

_Thinkers_ are drawn to logic, objectivity and analytical conclusions that don't factor in personal emotions. _Feelers_ will factor in their personal values and do what they _believe_ is right. So, while a thinker may be criticized for being too impersonal in their choices, an empathetic and compassionate feeler could be deemed over-emotional.

When considering on which side you fall, keep in mind that society tends to have a gender bias in conditioning men and women into being thinkers and feelers respectively. So, when deciding which one you truly are, don't think about what people may _expect_ you to be.

A good example is the story of a freshman college student who was caught smoking marijuana in his dorm room. The school rules said that anyone caught smoking would receive a one-semester suspension. However, the freshman did have an outstanding academic record with no prior incidents, and he'd been given a dorm room with two older students who _did_ have a history of troublemaking. Also, the freshman seemed genuinely apologetic and deeply worried about how his parents would react to a suspension.

The school's dean was a thinker and believed that rules were rules, so a suspension was in order. But the assistant in charge of deciding the punishment was a feeler and believed probation was sufficient. In fact, he believed the school had made a mistake by placing the freshman with two troublemakers and felt the suspension could adversely harm an academic career that was just getting started.

So, if you feel like the dean was right in suggesting the by-the-book punishment, you're probably a thinker. But if you agree with taking all external factors into consideration too, you're more likely a feeler.

Now, the final preference scale is between _Judging_ (J) and _Perceiving_ (P), and this reflects how you'd like your life to be structured: strictly or in a loose and spontaneous way.

If you like consistent order and hate when things are left unresolved, then you're probably a _judger_. But if you like flexibility and prefer to leave your options open for as long as possible, then you'll fall into the _perceiving_ camp.

Think of it this way: Would you rather your business released a newsletter every month, regardless of the circumstances, or would you rather have the flexibility to delay the newsletter whenever you're unsatisfied with the content? A judger would prefer the unambiguous schedule, and a perceiver would rather stay flexible.

### 5. Piecing together your personality type can bring helpful insights, but remember that no type is inherently superior. 

Now it's time to bring all your choices together to find out your personality type. Next to each choice was a letter, so your type should look something like this: INFP for introvert-intuition-feeling-perceiving or ESTJ for extravert-sensor-thinking-judging. Altogether, there are 16 possible types that can help you become more aware of your potential strengths and pitfalls when it comes to finding a satisfying career.

For example, ENFJ, or an extraverted-intuitive-feeling-judging person, is usually what you'd call a people-person, as one of their biggest concerns is the well-being of others and forming strong relationships. This makes them well-suited to being leaders, as they also tend to be charismatic and good at negotiating. However, they can also become too involved in drama and get immersed in problems that aren't worth their time and effort.

An ISFP, or an introverted-sensing-feeling-perceiving person, tends to be a patient, caring, gentle and flexible person with little need for power or control. This makes them effective and loyal team players, but it also means they're susceptible to taking conflict and disputes badly as their indirect way of expressing themselves can often be misunderstood.

An INTJ, or an introverted-intuitive-thinking-judging person, can be an ingenious perfectionist with a desire for autonomy and full confidence in his plans and ideas, which can be visionary and insightful. As such, the INTJ tend to be unbothered by criticism and capable of superb focus, energy and determination. However, they can also set expectations too high and be so confident that they miss flaws in their ideas and neglect to get a second opinion.

As you can see, each personality type comes with strengths as well as weaknesses. But unfortunately, some cultures tend to promote the outgoing characteristics of the extravert as being somehow better than the inward focus of the introvert. So people assume that extraverts have a greater chance of success, but this is nonsense.

The truth is, no one type is going to have an advantage over someone else in life. For example, an ESTP, or extraverted-sensing-thinking-perceiving person, is usually action-oriented and great at negotiating and problem-solving, but they can also be too focused on the here and now and fail to properly plan ahead.

An ENTP, on the other hand, tends to excel at planning ahead and thinking strategically, but they can also be too flexible and resistant to commitments. On top of that, they can be so fast-acting that they'll fail to take in the advice of those around them or fully consider the feelings of others.

So don't succumb to the mistaken belief that one type will give you an advantage over another.

### 6. Personality types fall into four basic temperaments, the first two being traditionalists and experiencers. 

Of course, no two people are exactly alike, but in a general sense, those with the same personality type, or even just two or three of the same preferences, can have a lot in common — especially when it comes to work. So, while there are 16 personality types, those who share specific preferences will fall into one of four _Temperaments_.

The first Temperament encompasses _traditionalists_, who are people with S and J in their personality type. In other words, they are _sensing judgers_. Some will be extraverts and some introverts, so there will be significant differences among them, but you can think of Temperaments like instrument sections in an orchestra — you'll find a lot of differences among the instruments in the string section, but there is a fundamental commonality to them as well.

A good motto for the traditionalists is, "Early to bed, early to rise," because they feed off order, stability and conformity. Traditionalists are reliably consistent and will follow through on whatever task they're given. But they have an inability to change or adapt to a new way of doing things, as well as a tendency to avoid long-term thinking.

Given their fondness for order, it's no surprise that around 50 percent of police officers are traditionalists. In general, this Temperament is well suited to working in strong organizational structures with clear operating procedures, expectations and chains of command. The less confusion, the better.

Next up are the _experiencers_ or _sensing perceivers_, as they're made up of types that share S and P. The motto for this Temperament could be, "Eat, drink and be merry," as they're an adventurous, outgoing and impulsive lot.

Experiencers are out to make a difference in the world, and their strengths include courage, resourcefulness, improvisation and a willingness to change, adapt and take chances. They also tend to be good with tools and instruments.

As for pitfalls, experiencers can be so impulsive that they cross over into irresponsible or childish behavior. They can also be less than brilliant at recognizing patterns and making connections.

It's no coincidence that experiencers also show up a lot in law enforcement, but for them, it's the thrilling unpredictability that makes them want to become police officers, firefighters or other emergency responders. They excel at these jobs and others that offer variety, autonomy and unpredictability.

### 7. The other two temperaments are idealists and conceptualizers. Various temperaments can make a well-balanced team. 

Next on the list of Temperaments are the _idealists_, otherwise known as the _intuitive feelers_ since they share N and F in their personality types.

The motto for this group is, "To thine own self be true," since idealists are all about personal growth, gaining knowledge and finding satisfaction in authenticity and integrity. Idealists are drawn to meaningful work that explores new possibilities and often deals with the philosophical or spiritual aspects of life.

The strengths of idealists include being receptive to and accepting of all kinds of people, an ability to bring out the best in others and a knack for coming up with creative solutions. On the downside, they can be moody, over-emotional, poor at handling criticism and being disciplinarians, and of course, too idealistic and insufficiently practical.

If you're an idealist looking for a job, it's best to stay away from competitive environments and instead find ones that are democratic and harmonious. Idealists tend to find fulfillment in artistic jobs and endeavors that help people, such as teaching, consulting, counseling, human resources and community outreach.

Last but not least is the Temperament of _conceptualizers_ or _intuitive thinkers_ who share the N and T preferences. The motto here is "Be excellent in all things," as conceptualizers are into seeing great possibilities everywhere and being the agents of change to bring about those improvements.

The strengths of conceptualizers include planning and designing innovative solutions, being confident and clever and recognizing complex patterns and trends. Among the pitfalls to look out for are arrogance, disregard for authority, being too complex for others to understand and a lack of interest in small details as well as the feelings of others.

The ideal job for a conceptualizer will be challenging and intellectually engaging with a good amount of autonomy. They tend to thrive in positions of upper management and in fields like science, technology, medicine, law, or at a university where they're surrounded by capable colleagues.

It's helpful to recognize that despite the differences between the four Temperaments, they often come together to strike a harmonious balance.

Think of what it takes to keep a big organization like a hospital running. The finance department needs a highly organized person like a traditionalist. The marketing and planning department needs a future-minded person like a conceptualizer. Human resources needs an idealist to promote personal development. And the director of operations will likely be an experiencer, someone who's focused on the here and now and is always ready to put out the next fire.

### 8. Knowing your Dominant Function can help you find satisfying work. 

Knowing your personality type is great, but it's just the start! With every personality type, you'll find a _Dominant Function_, or the strongest aspect of your personality, and an _Auxiliary Function_, as well as a third and fourth function that are less influential in your day-to-day life.

It pays to be aware of your Dominant Function because when you're using this function at work, you'll find it easy and pleasurable. In other words, it's like being aware of your natural talent.

If your personality type is ISTJ, ISFJ, ESTP or ESFP, you're a _dominant sensor_, which means you probably prefer cold, hard facts — the more specific, the better. You're also likely to have a memory that is the envy of your friends. Dominant sensors often shine when they're in a position to collect, analyze and put those facts to work. Maybe a research post would be your dream job?

If your type is INFJ, INTJ, ENFP or ENTP, you're a _dominant intuitive_, so rather than facts, you focus on implications — all the meaning and subtext that is going on behind the facade that other people can't see past. You're also likely to have a strong creative streak and will be in your element if you can exercise your imagination or originality. Have you thought about a job in advertising?

If you're an ISFP, INFP, ESFJ or ENFJ, this means you're a _dominant feeler_, which suggests you'd do well in a job that reflects your personal values. The common characteristics here are compassion, loyalty and empathy, so satisfaction tends to come from work that is personal and focuses on the human experience — it could be artistic, or it could be as an advocate for customers or people in need.

And finally, ISTP, INTP, ESTJ and ENTJ are all _dominant thinkers_, which means you're probably great at cutting through the touchy-feely stuff to make strictly logical decisions. You'll be rewarded in any situation that requires someone to make tough choices, which is why dominant thinkers make great lawyers.

### 9. A good career path should anticipate changes in your interests as you age. 

Unfortunately, there's more than one way to end up with an unsatisfying career. Due to expectations placed on you from an early age, you may have been pushed in a direction in which you had no interest. But many people end up going down the wrong path because most traditional educations put us in the position of choosing a career when we're barely out of our teens.

Our skills and abilities are always developing and changing, and to form a well-rounded personality and stay satisfied, we should plan on a career that changes along with us.

Let's look at how that development happens, starting from the beginning.

For the first six years of your life, your personality type starts to develop, and between ages six and 12 your Dominant Function comes into focus. If you're a dominant feeler, you'll start to show a strong sense of empathy, or if you're a dominant thinker, an early sign might be to logically talk yourself out of a punishment.

From age 12 to 25, your _Auxiliary Function_, or secondary skill, will become clearer. If you're an ISTJ or ESTP, this means you'd be a dominant sensor and an auxiliary thinker, so you'd start to notice that you're not only great at gathering and analyzing info, but you're also pretty good at making decisions. During this time, your third and fourth functions will emerge as well, though they may not be as readily apparent yet.

Between ages 25 to 50, your third function, or third strongest skill or interest, will truly develop. This often takes place after your fortieth birthday and can coincide with the standard midlife crisis — when you feel a strong desire to change careers. After all, if you've spent 20 years focusing on the same skill or interest, it's natural to feel the desire to shift priorities. You want to devote some time to that other interest, whether it's your auxiliary, third or even fourth function, which often develops after age fifty.

For example, Marty was an ISFP, which made him a dominant feeler and an auxiliary sensor, with a third function of intuition and a fourth function of thinking. Sensors tend to take things at face value, but after his thirty-eighth birthday, Marty's third function started to become more appealing, and he began to question the underlying meaning of things more often.

At whatever age this shift in interests happens, it doesn't always lead to a career change — it can often lead to forming a new hobby — but it's wise to be aware of these changes and understand why they're happening.

### 10. Match your personality to your ideal career by taking everything into account when conducting a job search. 

The perfect career path is one that brings together two things: what you're good at and what you're interested in. And the only way to combine your strengths with your values and interests is to have the kind of self-awareness that comes with knowing your personality type and everything that goes with it.

Of course, there's little chance someone's just going to suddenly email you to offer a dream job. Instead, you'll need to do what can feel like a job in itself: a job hunt. Here, an effective search brings together everything you can gain from knowing your personality type.

The first step is, of course, to know your specific four-letter personality type, which comes with your potential strengths and weaknesses, as well as your four main functions and a list of jobs with the potential to be a perfect fit.

The next step is to take this information and consider what's most important to you. If you're an ENFP, that's extroverted, intuitive, feeling and perceiving, you'll likely want challenging, varied work that allows you to meet new people, learn new skills and get rewarded for your imagination.

Now, when you can bring that together with your interests, you might look to the recommended creative jobs and see that journalist, art director and costume designer are just a few of the suggestions. Or maybe your interests guide you more toward marketing jobs like public relations specialist or market research analyst. If education or counseling is more your thing, you'll find suggestions like rehabilitation counselor, special education teacher, social worker or anthropologist.

It's your job to rank the ones that sound best to you and keep a strong focus on your top five. You might ask yourself: If money wasn't an issue, which jobs would I be happy to do for free?

When you have your list, the next step is to write down all the ways in which your skills and strengths would be put to use in these jobs. This way, you'll be prepared to explain why you're a good candidate. You can also list past examples of effectively utilizing those strengths and skills, along with instances where pitfalls came into play, to show your potential employer that you're also knowledgeable of your weaker points.

Next, do some research on your potential job. Find someone who currently does it and see if he can tell you what the job is really like. Sometimes you'll find that what you thought was your dream job may not be so appealing after all.

> _"The goal is to find the right match — the career that will let you do what you do best and enjoy most, one that corresponds with your personal interests..."_

### 11. It’s never too late to shift careers and start experiencing truly fulfilling and satisfying work. 

As health care costs and life expectancy rates rise, older people are increasingly hanging on to their careers for longer periods of time. In fact, 40 percent of Americans are planning to "work until they drop."

Whether you want to keep working past retirement age or are just looking for a fresh start, it's never too late to find new work that better suits your personality.

When you start a new career late in life, you can call it an _encore career_, and many Baby Boomers born between 1946 and 1964 are expected to have one. If this sounds like you, gaining an awareness of your personality type and Dominant Function are sure to help make your encore career your best one yet!

No matter your age, a change in career can bring a breath of fresh air to your life when you start doing what you're meant to be doing.

For example, Jay was a Traditionalist who studied government in college and then spent time as a journalist and PR consultant early on in his career. He got his MBA while working as a financial analyst but then ended up joining the family business, which involved the manufacturing of industrial sewing machine parts. Jay became the company president but wasn't too happy about it.

While working for the family business, Jay started coaching sports on the side, and he enjoyed it so much that, at 46, he decided to start a teaching career. Jay now teaches social studies and history to high-school students, and though the change came with some challenges, he wishes he'd done it sooner.

Being an ISTJ means Jay is a dominant sensor, so it's no wonder he likes teaching history since it's filled with facts and details that he can share with his students. Traditionalists like Jay also want to believe in the mission of what they're doing and see clear results of their effort. So after years adrift in the business world, Jay is now in his element, where he can see the benefit of his efforts in his students every day.

So whether you're headed to college or facing retirement age, you can gain from knowing more about your personality.

### 12. Final summary 

The key message in these blinks:

**Knowing the secrets of your personality type can greatly improve your working life. When you understand your personality type, you'll become better acquainted with your strengths and weaknesses. This will allow you to make sure you're using your primary or Dominant Function at work. When you're doing this, your job will become easier and more enjoyable.**

Actionable advice:

**Get to know your educational and training options.**

The internet brings access to job training and continued education right to your fingertips. Discovering the secrets of your personality type can also reveal that you need some training to better qualify for one of your ideal jobs. So check out the websites of the schools and training programs around you to see what they offer. Furthermore, many online schools now offer accredited programs in a wide variety of subjects.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Personality Brokers_** **, by Merve Emre**

A lot of research has gone into the study of personality types, and perhaps none was more groundbreaking than the work of Katherine Briggs and Isabel Briggs Myers. But did you know that the story of these two researchers is pretty fascinating in itself?

Much of today's scientific insight into personalities was made possible by the development of the Myers-Briggs Type Indicator. So now is the perfect time to dive into the blinks to _The Personality Brokers_ and find out how one pioneering woman and her daughter started it all.
---

### Paul D. Tieger, Barbara Barron & Kelly Tieger

Paul D. Tieger is a widely recognized expert in personality types, and he uses his knowledge to help individuals, teams and businesses thrive

Barbara Barron uses her insight into personality types in her career as an advancement consultant, where she works with nonprofits and private schools in areas such as program development and fundraising.

Kelly Tieger has gone from an editorial assistant for her parents, Paul D. Tieger and Barbara Barron, to being an integral part of researching and developing material for the newest editions of _Do What You Are_.

