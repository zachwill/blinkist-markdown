---
id: 55bf56846364610007010000
slug: do-you-believe-in-magic-en
published_date: 2015-08-03T00:00:00.000+00:00
author: Paul A. Offit, MD
title: Do You Believe in Magic?
subtitle: Vitamins, Supplements, and All Things Natural: A Look Behind the Curtain
main_color: 86A142
text_color: 465422
---

# Do You Believe in Magic?

_Vitamins, Supplements, and All Things Natural: A Look Behind the Curtain_

**Paul A. Offit, MD**

_Do You Believe in Magic?_ (2013) uncovers the hidden truth behind the alternative medicine. With insightful research findings and revealing case studies, these blinks will make us rethink our beliefs about supposedly all-natural treatments, and their less than healing side effects.

---
### 1. What’s in it for me? Find out about the irrational health choices made by rational people. 

Did you ever go to the doctor's, hurting all over, hoping to get help — just to be told: "You're as healthy as a horse. All you need is a little exercise." Alternative healers would never dismiss you like that. They'd use a pendulum, ask many questions or look deeply into your eyes to find out what's really ailing you.

Is it any surprise that many Americans are unhappy with modern medicine: They perceive it as bureaucratic, impersonal and cold. They prefer a naturopath's personal touch to scary high tech machines, and they've heard scary stories about "Big Pharma."

But alternative medicine isn't always an alternative.

For one, natural remedies can be dangerous: Between 1983 and 2004, poison-control centers in the United States received 1.3 million reports of adverse reactions to vitamins, minerals and dietary supplements. Secondly, unlike conventional medicine, alternative medicine is supported only by very scarce evidence, like a few enthusiastic testimonials.

In these blinks, you'll learn

  * about the miraculous powers of the great Dynamizer;

  * the nature of the tragic connection between a former Playmate of the Year and dying babies; and

  * what nurses in World War Two used on patients when they ran out of morphine.

### 2. Dietary supplements and natural remedies are not as healthy as you think. 

Do you feel extremely tired sometimes? Don't look as youthful as you did ten years ago? Then you're definitely lacking vitamins! But here's the good news: You just need to pop some pills and everything will be OK. Right?

If only it were that simple. The reality is that vitamin supplements and their alleged health benefits are vastly overrated. If we listen to the claims of supplement providers and get sucked in by those promising words in their campaigns — _natural_, _bio_, _organic –_ then we're easily convinced that vitamins are a modern miracle.

Even exceptional chemist and two-time Nobel laureate Linus Pauling claimed that a daily dosage of 3,000 milligrams of vitamin C could cure not only a common cold, but also cancer. Unfortunately, this just isn't the case. Studies have shown that vitamin pills don't improve our health. In fact, they can do just the opposite.

In 2004, researchers from the University of Copenhagen reviewed several studies of a total of 170,000 people to determine whether taking vitamins A, C, E and beta-carotene prevented intestinal cancer. The results were surprising, and disturbing: death rates are six percent _higher_ in people taking vitamins.

### 3. Celebrities have been instrumental in promoting dubious miracle cures. 

Desperate to gain fame and money, some celebrities are willing to promote all manner of products, even if they endanger people's health.

Take actress Suzanne Somers, for example. She played a large part in promoting a deeply problematic form of hormone therapy. After her recovery from breast cancer, Somers subjected herself to a regimen of "bioidentical" hormones and dietary supplements. What she didn't know was that these supposedly natural hormones were in fact conventional hormones engineered in a German factory.

Somers soon began to promote the same regimen to other menopausal women, claiming to have discovered the elixir of youth (in the meantime, she regularly injected her face with Botox). The hormone shots she endorsed came with all the common side effects of hormone replacement therapies: heart disease, blood clots and cancer.

Similarly, _Pla_ _yboy_ model Jenny McCarthy promoted a controversial autism treatment. When her son was diagnosed with autism, McCarthy desperately sought alternative care. This is how she came to know Jerry Kartzinel, a Defeat Autism Now (DAN!) doctor. DAN!, a controversial program of the Autism Research Institute _,_ blamed vaccines for causing autism.

McCarthy aligned herself with DAN! and joined the group's crusade. It's due to their campaigns that many parents became wary of vaccination. As a result, fewer people are properly immunized today. In recent years alone, Americans have witnessed an increase of deaths among newborns from diseases like whooping cough.

Of course, many still admire celebrities and are inclined to trust their testimonials, but beware: A star on the Walk of Fame doesn't make you a health expert!

> _"We hate Big Pharma...We leap into the arms of Big Placebo." - Michael Specter_

### 4. Outrageous claims and bizarre therapies have characterized alternative medicine for the past 150 years. 

Plenty of supposedly natural remedies combine false claims and unexpected side effects. This trickery isn't new, either. Way back in 1905, journalist Hopkins Adams published an article entitled "The Great American Fraud" after having a chemist analyze common herbal medications.

It turned out that several contained large quantities of alcohol, like "Hostetter's Stomach Bitters" at a hefty 44 percent — equivalent to a strong whisky. Some medications even contained opium and cocaine!

Some of the wackiest claims for treatments were made up by rogue MDs. In 1910, neurologist Albert Abrams claimed that he could detect cancer and other ailments with a simple electrical device. Abrams came up with the theory that humans emit vibrations that are rather like radio waves. He presumed that cancer would change a person's vibrations.

To detect these vibrations, Abrams invented the _Dynamizer._ Besides detecting tumors, the device was supposed to reveal someone's place of birth, ethnic background, year of death, religion and golf handicap.

It involved coils, batteries, rheostats and wires that were attached to the patient's forehead. A drop of the patients' blood was placed inside a box, and a diagnosis revealed. Economically, the Dynamizer was a huge success: Abrams raked in more than two million dollars. Medically, it was an absolute sham.

In the 1930s, the physician Max Gerson came up with a bizarre cancer treatment. Gerson advised his patients to consume massive amounts of juice, vitamin supplements, thyroid medication and "Lugol's solution" — a disinfectant. Even worse, patients were subjected to daily coffee enemas to cleanse their intestines!

It didn't help: Not one of Gerson's 18 patients was cured and 17 died from their cancer. Despite this, his method became quite popular.

But all this is in the past, right? Nope, the modern world still has its fair share of quacks and charlatans. Take Rashid Buttar, MD, who isn't content with providing just one miracle cure.

He offers an anti-autism cream, and "Trans-D Tropin," an anti-aging drug that remains unapproved by the FDA. Buttar has also treated cancer patients with intravenous hydrogen peroxide, an aggressive substance normally used for bleaching. None of these treatments improve people's lives in any way, or cure them.

> _"The offer of control in a health-care system where patients feel little or no control is irresistible."_

### 5. Some alternative treatments work, just not in the way you expect. 

"It's only the placebo effect" — that's a phrase we all know. But the placebo effect really is one of the ways these therapies _actually_ work.

Take acupuncture, a treatment that millions of people swear by. For thousands of years, acupuncture has been known as an effective treatment. Reliable studies have also confirmed that at least some people experience positive effects from acupuncture, such as patients suffering chronic pain. 

But what's less known is that acupuncture works even if needles are inserted in the wrong place or if they aren't inserted at all. How? Well, treatments that involve rituals are comforting in very convincing ways. 

Just think of an acupuncture session where you have to lie still while the acupuncturist sticks needles through your skin in a solemn manner. If a treatment involves a ritual, we're more inclined to believe that it works, and many alternative treatments are based on rituals.

People also experience health rituals as very positive therapeutic interactions. During an acupuncture session, you can relax in a comforting atmosphere while another person is tending to your body and trying to heal you.

These psychological effects trigger the body's own ability to fight pain. During rituals like that of acupuncture, the brain starts to release substances that act just like morphine. They are called endorphins, and are able to diminish our stress and pain. 

Just believing that you're receiving an effective treatment can be enough to make your brain release endorphins and make you feel better. During World War Two, military hospitals ran out of morphine, so nurses injected the wounded soldiers with salt water, pretending it was morphine. The placebo effect worked, and the soldiers' pain subsided. 

Indeed, as famous physician Albert Schweitzer claimed, we all carry our own doctor inside. And notwithstanding the legions of charlatans, a few real healers help by activating the body's own self-healing powers.

### 6. Final summary 

The key message in this book:

**Rather than being miracle cures, vitamins, supplements, and natural medicines may be a real danger to your health. By understanding the lies that surround alternative medicine, you'll be better able to make the right choices for your health and safety.**

Actionable advice:

**Get your vitamins from your diet, not the pharmacy!**

Vitamins taken in concentrated pill form may not have the same effect on your body as vitamins from fruits and vegetables. The smartest way to be healthy is by paying a little more attention to your diet. So instead of trying to figure out which vitamins to buy, start learning how you can create a balanced diet that gives you everything you need.

**Suggested** **further** **reading:** ** _Why People Believe Weird Things_** **by Michael Shermer**

_Why People Believe Weird Things_ provides an overview of the most common pseudoscientific and supernatural theories. It'll teach you why so many people believe in them, why they're wrong, and what methods proponents of pseudosciences use to assert their incorrect theories. It also offers both rational arguments _for_ science, and rational arguments _against_ pseudoscience.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Paul A. Offit, MD

Paul A. Offit, MD, is chief of the Division of Infectious Diseases and director of the Vaccine Education Center at the Children's Hospital of Philadelphia. He's the author of _Autism's False Prophets_, _Vaccinated_ and _Deadly Choices_.

