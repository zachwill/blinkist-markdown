---
id: 57923e18b3b9b300032e80d2
slug: dogfight-en
published_date: 2016-07-26T00:00:00.000+00:00
author: Fred Vogelstein
title: Dogfight
subtitle: How Apple and Google Went to War and Started a Revolution
main_color: F7DD40
text_color: 786B1F
---

# Dogfight

_How Apple and Google Went to War and Started a Revolution_

**Fred Vogelstein**

_Dogfight_ (2013) recounts the tale of how a once amicable business partnership between tech giants Apple and Google turned into a bitter rivalry as the companies fought for control of the mobile internet device market.

---
### 1. What’s in it for me? Step behind the scenes of a technology dogfight between Apple and Google. 

In one corner of the ring stands Apple, creator of the iPhone and its proprietary operating system, iOS, not to mention defender of innovative features further protected by legions of lawyers and patents.

In the other corner stands Google, search engine giant and software visionary, armed with the strength of an open-source operating system called Android and leagues of phone manufacturing partners.

Once friends, these two tech behemoths turned enemies, fighting for an ever-larger piece of the lucrative smartphone market. These blinks document how an early partnership turned into a legal dogfight, complete with backstabbing, closely-guarded technological secrets and piles of nondisclosure agreements tossed in the trash.

In these blinks, you'll discover

  * how Steve Jobs was misled by his "friends" at Google;

  * what gorillas, glass and iPhones have in common; and

  * how many lawyers it takes to cripple a smartphone manufacturer.

### 2. Steve Jobs was reluctant to develop a phone, choosing to focus instead on the iPod and iTunes. 

In 2001, the technology market was wowed with the introduction of Apple's iPod. Yet the device had only one function: playing music.

Meanwhile, Apple executives had been dreaming for some time of creating an Apple phone, but Steve Jobs didn't share their dreams.

He didn't want to waste money and effort developing a phone that would only end up being meddled with by the large phone carriers, such as T-Mobile or AT&T. At the time, companies that manufactured phones depended on carriers not just for marketing but also for subsidizing a phone's purchase price; thus carriers had a big say in the development of any phone technology.

True to character, Jobs didn't accept this situation, as he wanted Apple to have full control over any phone it developed.

Apple instead focused its efforts on the iTunes Store, a service that offered music and videos for download. It launched in April 2003 for Mac and iPod users, as only Apple hardware had the software required to run iTunes.

Yet competition in the market was growing. Other phone manufacturers were equipping phones with music apps that allowed users to download music from Amazon or Yahoo, for example. People no longer needed a phone and a separate MP3 player — they could have both on a single device.

Instead of developing a phone that was compatible with iTunes, however, Apple partnered with technology company Motorola to launch a music phone called Rokr.

It seemed the perfect fit: Motorola would develop the hardware, negotiate with phone carrier AT&T and then pay Apple a licensing fee to run iTunes on the device.

Rokr was a flop, however. The device was chunky and could only store 100 songs. It could run iTunes but didn't allow a user to download music from the internet directly.

Yet this setback wasn't of huge concern to Apple. In 2004, iPod sales began to soar, and at the same time, Jobs started to think again about an Apple phone.

### 3. Apple wanted to make a phone with innovative features that people had never seen before. 

By the end of 2004, Steve Jobs was warming to the idea of an Apple phone. He knew that mobile connections were now fast enough to run a web browser, for example, or even stream video.

Cingular, a phone carrier that was soon to be absorbed by AT&T, promised it would cede full control of product development if Apple were to ever develop a mobile phone.

So Apple started development, with the lofty goal of creating smartphone features that had never been seen before. For instance, the company wanted to pioneer a _multi-touch_ screen, which would allow a person to use two fingers to navigate.

Today, using pinching or stretching gestures on a smartphone screen is commonplace, yet in 2005 phone chips weren't powerful enough to run the graphics software required to interpret two-fingered motions.

Due to these ambitious goals, Apple's first prototypes were very large. Over the next two and a half years, the development team worked to shrink the technology sufficiently to make it fit inside a regular-sized smartphone.

The company also found a way to manufacture a scratch-resistant screen. Early on engineers were frustrated with their two options: plexiglass screens that could survive a fall but scratched easily, and ordinary glass screens that didn't scratch but shattered in a one-meter drop test.

Jobs had an epiphany when he met with glassmaker Corning and learned about gorilla glass, which is the hardest glass in the world. Originally developed for fighter-jet cockpits, gorilla glass was the solution Jobs was looking for, and he convinced Corning to start producing it for Apple.

With a screen made of gorilla glass, the iPhone passed all its durability tests. The phone screen didn't scratch when shaken in a bag filled with coins; it even survived a fall onto a concrete sidewalk.

While we take innovations such as gorilla-glass screens and multi-touch navigation for granted today, at the time such ideas were groundbreaking.

### 4. Steve Jobs went to great lengths to keep the ongoing development of the iPhone top secret. 

Imagine you arrive at work one day to find that half the rooms and even hallways in your office have been sealed off and you can no longer access them. Although this seems ridiculous, it's exactly what happened at Apple during the development of the iPhone.

Jobs didn't want any information about the iPhone to be leaked. So as development proceeded, he began to cordon off areas, limiting employee access. Over one weekend, certain common areas became off-limits to any employees not on an iPhone team. Some buildings were virtually divided in half.

Importantly, employees on the iPhone team had to stay silent about their work. Jobs had team members sign a nondisclosure agreement (NDA), but even this step wasn't enough. He also had employees sign another contract, confirming they had indeed signed the NDA!

The bottom line was that if anyone was caught discussing the project with outsiders, they would be fired.

Even the teams working on the iPhone couldn't share information about their work with other iPhone teams. Engineers working on the phone's electronics worked apart from people who worked on software, for instance.

In fact, when the software team needed to test-run programs, they weren't even given the real phone that the engineers were working on but had to make do with a simulator!

Other companies that provided iPhone components were kept in the dark, too. Marvell Electronics provided the phone's Wi-Fi chip, but the company didn't even know the chip would become part of a smartphone, thinking it was for a new model of the iPod.

To keep Marvell in the dark, Jobs even showed their executives fake schematic graphs of a fictional iPod project!

> _"Steve loved this stuff. He loved to set up division. But it was a big 'fuck you' to the people who couldn't get in."_ — Andy Grignon, former senior Apple manager

### 5. Two of Apple’s star executives, Tony Fadell and Scott Forstall, competed to control the iPhone project. 

Scott Forstall was a brilliant software engineer and had worked for Apple since 1992. Yet despite his successes, he still remained outside of Steve Jobs's "inner" circle of favored developers.

So imagine how Forstall felt when Tony Fadell, a relatively new engineer, quickly became one of Jobs's favorites and was appointed head engineer for the iPhone project. Fadell had led the team that developed the iPod, and by late 2006, the iPod was generating 40 percent of Apple's revenue.

Apple's initial plan for the iPhone was to first create a new, better iPod, then add a phone function. The device was to run the iPod operating system.

Forstall had a different vision for the iPhone, however. He approached Jobs and suggested that the team instead develop a compressed version of OS X, the operating system for Apple's Mac computers.

Jobs liked the idea and allowed Forstall to take on the project, even though he wasn't sure it could be done. At the time, mobile chips weren't nearly powerful enough to run OS X. The operating system would have to be scaled down to a few hundred megabytes, a tenth of its size.

Yet Forstall managed to do it. As a result, the rivalry between Forstall and Fadell heated up, and they fought about nearly everything over the next two years. When Forstall's team was doing well, for example, Jobs allowed him to poach the best engineers from Fadell's team.

Forstall also kept his project a secret, and Fadell's iPhone team didn't even know about it!

Insiders say their rivalry was partly fueled by Jobs's behavior. He would seem to favor one executive's team for a while, then switch to favoring the other, stoking their competitive animosity.

### 6. While partnering with Apple on the iPhone, Google started its own secret smartphone project. 

In 2007, few people would have considered technology giants Apple and Google direct competitors. Google made its money from search ads, while Apple sold electronic devices.

Rather, the two companies were on friendly terms. Jobs was friends with executives at Google, and several of Apple's board members also served as Google's advisors.

What's more, Google CEO Eric Schmidt was a member of Apple's board of directors.

Google was even a partner in the development of the iPhone. Jobs was keen to include Google software such as YouTube and Google Maps in the device, and so Google organized a special iPhone team to work on these projects.

Yet at the same time Google was developing its own smartphone project. In July 2005, the company bought Android Inc. for $50 million and put Android cofounder Andrew Rubin in charge of its project.

Rubin worked on the Android smartphone operating system in secret, and only a few top Google executives knew that the company had partnered with phone manufacturer HTC to develop a new kind of smartphone.

The first phone Google developed was similar to the BlackBerry device and was called the Sooner _._ Yet the device was not successful, primarily because it was released on the heels of the far more impressive iPhone in 2007.

Jobs had succeeded in creating a smartphone with a multi-touch screen and a cutting-edge design — and it overshadowed the Sooner completely.

The Android team dropped the Sooner shortly afterward and set to work on the Dream. This new project was to offer features that even the iPhone didn't have, such as a 3G connection, a copy-and-paste function and Google Street View.

> _"Up until then, Android had been like Google's mistress — lavished with attention and gifts but still hidden away."_

### 7. Pre-launch problems had iPhone engineers in a cold sweat, but the company managed to meet its deadline. 

Jobs presented the Apple iPhone at MacWorld on January 9, 2007. True to form, his presentation was flawless.

The engineers sweated out the whole event, however. They knew the device was far from being ready for the market. For instance, when the phone processed too many tasks at once, it ran out of memory and crashed.

Jobs secretly used several iPhones during his presentation, switching to a new phone whenever one device froze.

While Jobs could hide this glitch during his presentation, the next step could have been the company's undoing, as the iPhone was scheduled to launch on June 29.

Jobs's team had a number of problems to overcome. The phone's virtual keyboard still didn't work. Multiple letters would pop up when a user touched one key, and only after an irritating lag time.

What's more, engineers still hadn't figured out how to properly embed multi-touch sensors into the gorilla-glass screen.

The phone's tiny antenna was presenting problems, too. The manufacturing company couldn't mass produce them properly, and transmission performance was unreliable.

Despite these challenges, Apple managed to meet its deadline. The international media covered the June 29 launch as if it was a historic event, and the iPhone remained in newspaper headlines for weeks. Stories captured young Apple enthusiasts rushing to touch and purchase the iPhone as if it were a sacred relic!

In the first two days, Apple sold 270,000 iPhones, and went on to sell another 3.4 million over the next six months.

### 8. Jobs felt betrayed when Google unveiled the extent of its Android project and its competitive stance. 

Friends had warned Jobs about Google's secret Android project, but he didn't take their warnings seriously.

On November 5, 2007, Andrew Rubin held a press conference to announce the development of the Android operating system and a newly founded Open Handset Alliance, or OHA. This was a consortium of 34 phone producers, software developers and carriers, including T-Mobile, Ebay, Intel and Sony.

Jobs didn't like this at all. The Android mobile operating system would be open source, so that any phone manufacturer could use it. Once this platform launched, it would be in direct competition with Apple's iOS.

The goal of the Open Handset Alliance was to develop open, shared standards for mobile devices instead of having companies develop proprietary software, like Apple had done with its iOS.

Google aimed to promote Android devices on the basis of their open-source operating system, which the company itself would benefit from in turn.

The following week, Google released a video in which cofounder Sergey Brin and Android engineering director Steve Horowitz were shown playing with different phones. One Android phone offered features that the iPhone didn't have, such as Google Street View.

Jobs felt that the Android phone was basically a knock-off of the iPhone and confronted Google executives about it. They initially reassured Jobs that the phones that were featured in the video were just prototypes for testing the Android OS, and not actually new smartphone models.

Jobs mistakenly believed them, as he considered these executives his friends.

However, in mid-2008 he learned that Google was planning to release a new phone with iPhone-like features, such as swipe navigation. He threatened to sue Google if the company released a multi-touch smartphone, a feature that Apple had patented.

The two sides met for a long meeting. Jobs demanded that certain features be withheld from the new Google phone. Google executives complied, but not for long.

### 9. When new Android phones began to offer multi-touch features, Apple launched an epic legal battle. 

While the first smartphone running the Android operating system, the HTC Dream, turned out to be a flop, better ones were on the way. Google was in the process of developing other Android phones with multi-touch features, despite Jobs's legal threats.

In late 2008, Google partnered with Motorola and Verizon to develop a second Android phone, the Droid. It was released in October 2009, and Google marketed it by highlighting features that the iPhone didn't have, such as the ability to run several tasks at once. In its first three months on the market, the Droid sold better than the iPhone!

Then in January 2010, Google released a software update that enabled multi-touch navigation on the Droid. That same month, the company partnered with HTC again to release a third Android phone, the Nexus One, which also offered multi-touch features.

As a result, Jobs quickly began legal proceedings over patent infringement, and this opening salvo kicked off a long battle between Apple and Google.

While Jobs would have preferred to sue Google directly, he knew he was better off taking the phone manufacturers to court for copying patented iPhone features.

After all, Google didn't actually manufacture its phones, and it gave away its Android software for free.

First Jobs sued HTC, the Taiwanese maker of the Nexus. And as Android began to be used in other smartphones, Apple filed patent lawsuits in nearly every industrialized country.

By 2012, Apple had about 50 pending lawsuits in at least ten countries just against Samsung, the top distributor of Android-based smartphones. Apple hired 300 lawyers from 50 law firms to process the Samsung suits alone!

In the summer of that same year, Apple won a $1 billion judgment in its high-stakes fight against Samsung. Sure, Apple wanted to protect the iPhone's special features, but it also wanted to weaken its now biggest competitor in the smartphone market.

> _"I will spend my last dying breath if I need to, and I will spend every penny of Apple's $40 billion in the bank to right this wrong."_ –Steve Jobs

### 10. Legal battles with Google made Apple look bad, but Apple didn’t give up on its competitive drive. 

Steve Jobs criticized Google publically at every turn, from 2008 until his death on October 5, 2011. And all the while, Google fought back in the market, releasing new products to chip away at the iPhone.

In 2008, Google launched a new application called Google Voice that extended its mobile reach even further.

This app allowed a person to make free voice calls online, combining phone numbers and email addresses into a single identification number. If someone calls you over Google Voice, your phone rings and a transcript of the caller's voicemail is sent to your email.

Considering how Google Voice functions, using the application meant that a user's full contact database would end up on Google's servers, even if that user was calling over an iPhone.

This app could crush Apple, and the company knew it. As a result, when Google offered Google Voice to Apple for inclusion on the iPhone, Jobs rejected the offer.

But this move made Apple look bad, as the company was withholding a popular and useful app from its user base.

Amid the back and forth, media outlets began to portray Jobs as a control freak, and Apple users were turned off by the endless drama. The Federal Communications Commission even looked into the matter, suspecting that Apple was potentially violating FCC regulations by refusing to allow Google's app on the iPhone.

Jobs eventually caved in and included Google Voice in the Apple iTunes app store.

Apple responded, however, by attacking Google's dominance in mobile advertising and creating a service to replace Google Maps.

In 2010, Apple launched its advertising platform, _iAd_. Just three years later, the company had already earned $200 million from this service. And in 2012, Apple revealed its alternative to Google Maps and incorporated the software in the iPhone 5.

Yet Apple Maps failed terribly on release, as its maps misled drivers and even showed an incorrect location for the famous Washington monument in Washington, D.C.

These developments were far from the end of the battle between Apple and Google, which seems set to continue for years to come.

### 11. Final summary 

The key message in this book:

**Google and Apple started out as friendly companies with shared goals. As the market for mobile internet gained traction, however, the two firms began competing directly with each other in an effort to dominate the young industry, eventually ending up in the courts. Along the way, users have enjoyed an ever-improving stream of iPhones and Android-based smartphones.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Steve Jobs_** **by Walter Isaacson**

This book chronicles the audacious, adventurous life of Steve Jobs, the innovative entrepreneur and eccentric founder of Apple. Drawing from Jobs's earliest experiences with spirituality and LSD to his pinnacle as worldwide tech icon, _Steve Jobs_ describes the man's successful ventures as well as the battles he fought along the way.
---

### Fred Vogelstein

Fred Vogelstein is a journalist and former staff writer for _Fortune_ and _The Wall Street Journal_. He is also a contributing editor at _Wired_ magazine. _Dogfight_ is his first book.

