---
id: 57924c5cb3b9b300032e80f2
slug: doing-good-better-en
published_date: 2016-07-27T00:00:00.000+00:00
author: William MacAskill
title: Doing Good Better
subtitle: A Radical New Way to Make a Difference
main_color: E95531
text_color: 9C3921
---

# Doing Good Better

_A Radical New Way to Make a Difference_

**William MacAskill**

_Doing Good Better_ (2015) is a guide to making the largest positive impact possible through charitable donations. In examining many of the popular misconceptions about effective giving, this book gives you all the tools you need to truly make a difference.

---
### 1. What’s in it for me? Maximize the impact of your charitable giving. 

Do you donate to charity? An awful lot of us do, and that is why charities from the ASPCA to OXFAM find themselves better funded than ever before.

But are your donations effective? You probably have no idea. The actual act of charitable giving is what we care about, but as soon as we've handed over our cash, hardly any of us follow what happens. And this has led to a rather inefficient charity sector, with money wasted in over-funded areas, or on people who need no help.

These blinks explain how we can send our donations in better directions: to sources that will help those who really need it the most. By following a few simple principles, you stand the best chance of making the world a better place.

In these blinks, you'll also discover

  * which mathematical law applies to charitable giving;

  * why Fairtrade may do more harm than good; and

  * why we shouldn't care too much when charity CEOs earn big salaries.

### 2. When giving to charity you should give where you expect your impact to be greatest. 

With so many ways to give to charity and so many problems that require attention, how do you decide where to give?

The answer lies in plugging in this simple formula: How many people will benefit from your charitable donation, and by how much?

No one has unlimited resources, and so if you donate to one person's cause, someone else always loses out. Knowing this, you need to make a choice that maximizes your donation's effect.

It was this thinking that made Dr. James Orbinski's time with the Red Cross during the Rwandan genocide manageable. Orbinski had too many patients to manage, and had to prioritize between them.

So, he developed a system: he wrote the numbers "1," "2" or "3" on his patients' foreheads. "1" meant "treat immediately," "2" meant "treat within 24 hours" and "3" meant "irretrievable." Using this system, Orbinski was able to save more people by making the best use of his limited resources, even though it meant he had to leave some patients to die.

Sometimes a charitable deed will be the best choice because it has a chance of making a _huge_ impact, even if this chance is slim. To determine whether this is the right course of action, you need to first compare choices' _expected value_.

You calculate expected value by multiplying an outcome's value by its probability. For example, if your donation has a 50 percent chance of saving 3,000 lives, its expected value is 1,500 lives saved.

If the accident management planners at the Fukushima Power Plant had used the concept of expected value, they could have avoided the tragic disaster of 2011.

The plant had a very low probability of a _huge_ catastrophe — so low that the planners neglected the danger entirely. However, the _expected damage_ was huge. In the aftermath of the 2011 accident, around 1,600 people died.

> "When it comes to helping others, being unreflective often means being ineffective."

### 3. Given the law of diminishing returns, don’t give to causes that already receive a lot. 

Charitable organizations like disaster relief funds seem to need all the help they can get. It's obvious, right? More money surely equals more help? However, this isn't necessarily the case.

Charitable giving, like most other economic endeavors, is subject to the _law of diminishing returns_, which states that the more of something you add, the less of a difference each new addition makes.

To illustrate this, imagine that you've become homeless, you have no sweaters and winter is quickly approaching. One sweater could make a huge difference in your life: it might well save you from hypothermia!

If you already have a couple of old sweaters, one new one might keep you a little warmer. It would no doubt make a difference to your life, but probably wouldn't be life-saving.

And if you already have numerous sweaters, then one additional sweater is just something extra you have to lug around with you. It will make very little difference to your quality of life at all.

The same thing applies to charitable donations, where each additional dollar makes less of a difference than the last.

Whereas your hundred-dollar donation is just a drop in the bucket for an overfunded cause, it will make a huge difference for an underfunded, or _neglected_ cause.

Disaster relief is a type of charitable giving that tends to be widely publicized and attracts many donations. For example, for every person who died in Japan's 2011 earthquake, aid organizations received $330,000 in donations.

In other words, disaster relief tends to be overfunded. Given the law of diminishing returns, donating to disaster relief organizations won't make much of a difference.

Ongoing poverty-related causes, on the other hand, are typically _neglected_. For every poverty-related death, aid organizations receive only $15,000. If you were to take what you intended to give to disaster relief organizations and instead give it to poverty-related causes, like fighting malaria, your money would go much farther.

### 4. Making the biggest impact means figuring out where you can make the biggest difference. 

Many young people travel to the developing world to build hospitals and schools, and most of us think that this is a pretty good cause. After all, if we want to do good, isn't it best to offer our time, rather than just our money? To work directly for an organization? Not necessarily.

When assessing how much of a contribution you're actually making, you have to think about "_what would have happened otherwise_ " by using a scientific practice called "_assessing the counterfactual_."

To illustrate this, imagine seeing a man choking to death. There's no one else around, so you run up to perform the Heimlich maneuver on him. While you manage to clear his throat and save his life, your inexperience in first aid has left him with permanently damaged vocal chords.

But what would have happened otherwise? He probably would have died, so your deed was ultimately good, despite the unintended negative consequences.

Imagine instead that there was a trained paramedic at the scene, but you wanted to feel like a hero, and intervened, again saving the man's life but permanently damaging his voice in the process.

What would have happened otherwise? The paramedic would have saved his life without causing him any permanent damage. In this case, your deed was not so good.

If you want to make the biggest impact, you need to develop the skills to do so — rushing out to Africa to build schools is useless if you don't know the first thing about building. However, there might be a local builder who could construct the school with donor money, thus helping the economy in general. It's worth bearing this in mind when thinking about all career choices. Ask yourself: Could the work I'm doing be done better by someone else?

If the answer is "yes," then a better option might be to pursue a lucrative career that is suited to your skills — perhaps even as a banker or a stockbroker — in order to substitute direct action with the possibility of making large monetary contributions. In this way, you can "earn to give," a principle which we will discuss in the next blink.

Now that you have the theoretical tools needed to make the best charitable decisions, the following blinks will show how these tools can be used in real-life situations.

### 5. Working for an NGO or “following your passion” may not be the best career choices. 

How do you choose a career that will have the biggest positive impact on the world? Here are a few easy tips to make it happen.

One way is by "_earning to give_." For example, suppose you're a doctor and have to choose between working for an NGO or specializing in oncology. Working for an NGO would allow you to directly impact people's lives, but what would happen if you chose the other path?

In all probability, someone else would have taken the same job at the NGO, and maybe would have even done a better job. A specialization in oncology, however, with its high salary, would enable you to donate a portion of your sizeable income to effective charities.

But what if you took the job at the NGO? Someone else would have taken the oncology job, but may have been less inclined to donate to charity.

A high-paying job gives you an opportunity to make a difference by donating large sums while still earning a comfortable salary.

Second, think "personal fit" rather than "following your passion" when it comes to careers. The goal is to ensure a long period of steady income, part of which you'll donate to charity.

Careers motivated by passion tend to be the most difficult to get into. In music and sports, for example, only the most talented (or lucky) few make a steady living. In the United States, for instance, fewer than one in 1,000 college athletes break into professional sports.

Moreover, interests change over time. Think about it: do you have the same interests today that you had ten years ago?

Finding a good "personal fit" means considering how much the work offers you independence, variety and a sense of completion. Carpentry, for example, offers a high sense of completion, as you contribute to a finished, tangible product.

> "Your choice of career is a choice about how to spend over 80,000 hours over the course of your life, which means it makes sense to invest a considerable amount of in the decision."

### 6. What a charity actually does is much more relevant than how much it spends on administrative costs and salaries. 

When people decide where to make a charitable donation they often examine the charity's overhead costs (especially executive pay) to see whether the money is going to the needy or into the pockets of rich executives.

Charity Navigator, the oldest and most popular charity assessor (with 6.2 million visits in 2012), ranks charities by how much of their total donations go directly to their main programs.

But this approach is misleading, as overhead costs and similar expenses don't tell us much at all about a charity.

Imagine that you've set up a charity that provides caviar to hungry bankers. Only 0.1 percent of the donations are spent on overhead costs, and the rest goes toward the procurement and delivery of caviar. Being the generous CEO you are, you also earn no salary.

According to organizations like Charity Navigator, your charity would have a top-notch ranking.

It's not how the money is allocated that matters. It's what the charity actually does — its _impact_ — that determines whether it deserves our money.

Take the charity Development Media International (DMI), for example. They spend 44 percent of their donations on overhead, so if that's all you care about then they probably won't get your donation.

However, that overhead is used to run a $1.5 million media campaign that promotes health education in a particular country.

Diarrhoea, for instance, kills 760,000 children every year in the developing world, and could be easily combatted if people were educated to practice better hygiene.

Charities like DMI are hugely beneficial and worth investing in, despite the high overhead costs.

### 7. Often, well-intentioned acts of charity can have the opposite effect. 

Popular campaigns have led us to believe that we shouldn't buy "sweatshop goods," and that we _should_ buy Fairtrade coffee. This is a mistake, and here's why.

Sweatshops are actually a boon for the poorest of the poor. Remember, we have to ask: What would happen otherwise, if the sweatshops didn't exist?

In developing countries, the grueling and tedious nature and low pay of factory work is preferable to the lower-paid, backbreaking farming jobs out under the scorching sun.

For example, many Bolivians emigrate to Brazil illegally, risking deportation, in order to seek out higher pay in the sweatshops. A sweatshop worker in Brazil averages about $2,000 per year, which is considerably more than the average $600 per year that Bolivians earn, typically in agriculture or mining.

Despite this, many people would prefer to pay more for Fairtrade goods and avoid products produced by sweatshop labor altogether.

Indeed, Fairtrade seems to have noble intentions. For example, it guarantees producers a price of $1.40 per pound of coffee, which in theory guarantees better wages for all.

But we need to consider the actual _impact_ of buying Fairtrade goods. Typically, the poorest countries — those which according to the law of diminishing returns would most benefit from Fairtrade money — can't actually meet the difficult standards Fairtrade sets for participation. As a result, they get nothing.

The majority of Fairtrade coffee production comes from countries like Mexico and Costa Rica, which are ten times richer than countries that would benefit most from the extra money, like Ethiopia.

Moreover, only a small proportion of the additional price of Fairtrade products actually reaches producers. Dr. Peter Griffiths, economic consultant to the World Bank, estimates that number to be only one percent of the additional price.

In summary, you should try to always be aware that the difference you _think_ you'll be making with charity might not end up being reflected in reality.

### 8. Final Summary 

The key message in this book:

**Sometimes our charitable actions, though well-intended, have far worse consequences than we imagined. To make the most of our charitable donations, we have to think rationally and critically — not only about whom we give to and how we give, but also how that money will be used.**

Actionable advice:

**Make a habit of giving**

One way to easily make a difference in the world is to make charitable giving a regular part of your life. Once you've found a few charities where you know your contribution will make a real, material difference, set up a monthly payment plan and donate ten percent of your income to those charities.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Crisis Caravan_** **by Linda Polman**

_The Crisis Caravan_ (2011) is about the complexities and pitfalls that come with delivering humanitarian aid to conflict zones. Though aid is usually provided with nothing but good intentions, there are political, social and economic obstacles that can cause it to do more harm than good. These blinks outline the reasons aid work often fails, and offer advice on how we can improve it.
---

### William MacAskill

William MacAskill is an associate professor in Philosophy at the University of Oxford as well as the co-founder of the charities Giving What We Can and 80,000. His organizations have been featured in the _New York Times_, _Wall Street Journal_, on NPR, TED and numerous other media outlets.

