---
id: 587df18e850310000464e85e
slug: doing-the-right-things-right-en
published_date: 2017-01-19T00:00:00.000+00:00
author: Laura Stack
title: Doing the Right Things Right
subtitle: How the Effective Executive Spends Time
main_color: E7B62E
text_color: 82671A
---

# Doing the Right Things Right

_How the Effective Executive Spends Time_

**Laura Stack**

_Doing the Right Things Right_ (2015) cuts to the core of successful leadership. It teaches you how to manage a team and how to work with others to achieve profitable and productive results. Get ready to feel confident and lead your team to success.

---
### 1. What’s in it for me? Become the leader you’d want to follow. 

Are you a leader? Perhaps you're in charge of a team or a department, or maybe you're convinced that you _could_ be a leader, but aren't sure which next steps to take?

Well, in any of those cases, these blinks are for you. They explain what it takes to become an effective leader — that is, someone who achieves his or her goals and who excels at solving difficult problems, not by any means necessary, but by doing the right thing.

They also explain how to become more efficient in doing the great work you already do so you can achieve the same results in less time and with less effort.

There are twelve concrete measures that will help you achieve superb results; these blinks introduce you to nine of them.

You'll also discover

  * about a top company that didn't invest in a young start-up called "Google";

  * why sleep makes you a better leader; and

  * what the medical term "triage" has to do with excellent time management.

### 2. Leaders must set the right kinds of goals and communicate them effectively to the team. 

Being the leader of a company is like being the captain of a ship: It's your job to get your company and its crew to the desired destination while steering clear of any iceberg-like disasters. And just as there are guidelines to help captains effectively navigate ships, there are simple practices that can help leaders stay on the path to success.

The first practice is to set clear goals for your team that align with the overall mission of the company.

With the right kinds of goals, you can do a number of important things. Goals give focus, and by focusing your team members you provide them with a sense of purpose. And purpose leads to productivity.

A tangible goal is the engine driving your team. It gives meaning to their work. It allows them to see the big picture, which will motivate them to continue working even if they're stuck with a difficult or boring task. In short, meaning will imbue your team with determination.

But remember: make sure the goal is perfectly aligned with the company's strategy. This alignment will prevent people from wasting time on projects that don't advance the company in the right direction.

This brings us to the next practice: communication.

If you're not clear and specific about what your team should be doing, there's a good chance you'll waste time and money on ineffective workers who are unsure about how they can be useful.

One company was handling a job for a client who needed a specific spelling for a certain word. But due to a communication breakdown, the team was never told how important this request was and hundreds of copies of material had to be revised and reprinted.

Obviously, you don't want to end up in a situation like that. So, to practice good communication, remember these three things:

Remind your team of what's expected in a simple and straightforward way.

Make sure everyone knows exactly how they're contributing to the goal.

And provide both written and verbal instructions to ensure there's no misunderstanding.

### 3. A leader must be willing to adapt along with the changing marketplace and act on their ideas. 

Change: some people love it and some people hate it. There are those who can't wait to get their hands on the newest gadget and those who prefer to do things the old-fashioned way.

But whatever your personal preference might be, as a leader, you need to be willing to adapt and change with the modern world if you hope to keep your company profitable.

It's a simple fact: As the world changes, the way people do business in your industry will change, too. If you look at today's most successful companies, they've all adapted to globalization and the technological innovations associated with mobile computers and the internet.

Inevitably, the companies that refuse change, and insist on playing by the same old rules, are doomed to fail.

Imagine if a shipping company in the 1990s insisted that the internet was just a passing fad, and the company's leader refused to start a website or use email. Do you think they'd still be around today?

What people who fail to embrace change and innovation don't realize is that they're missing out on great opportunities.

When Google first emerged, the shortsighted executives at Yahoo actually rejected an offer to invest in the young start-up, and you can bet that they're still kicking themselves.

By embracing new ideas, you can also attract new clients and keep your company looking fresh, modern and relevant.

And when it comes time to implement a change or execute a decision, a leader must do this with confidence.

It can be easy to get stuck procrastinating or hung up on details, so keep this in mind: Results are what matter most in business, and there are no results without action. And action, of course, requires a plan.

So, once you have a good and thoughtful plan — and a policy for what to do if it doesn't work out — act on it! Even if it turns out to be a dud, it'll probably prove better to have tried and failed than to have never tried at all.

> _"An ounce of action is worth a ton of theory."_ \- Ralph Waldo Emerson

### 4. To do excellent work, a team must have the tools they need and be properly motivated. 

When you hear the word culture, you probably think about art and literature — or perhaps a biological sample. Yet there is such a thing as business culture; it refers to the social environment of a workplace, which is something every leader must tend with care.

So another practice for successful leadership is to create an open work culture that allows your team to be as efficient as they can be.

Some leaders are satisfied with having a team that puts in just enough effort to produce average work. The most successful leaders, however, will push and encourage their team to be the best they can be.

If you push your team to work hard, you'll get amazing results from those promising employees who get bored when they're not being challenged. This is crucial, because a lack of inspiration anywhere in the team will spread like wildfire. All it takes is one bored person to damage the morale of the entire team.

If you're uncertain about how to properly motivate someone on the team, don't be afraid to ask what kind of task he or she might find particularly challenging. Another tip that can increase efficiency is to reward team members who come up with ways to save time.

Simplifying how your team can get things done and removing any obstacles that might be in the way is another important part of any leader's job.

A successful team is one that is able to perform at its best. It shouldn't have to wrestle with outdated software, a slow internet connection or faulty computers and printers.

So if your team needs new equipment or training in order to do a better job, it's up to you, the leader, to spot this problem and fix it. Remember: The team is trying to reach the goal you've set. Any distractions or obstacles need to be removed.

Also, keep an eye out for unnecessary paperwork or redundant procedures that might be slowing things down. Sometimes, the way things are being done can create problems.

> _"As a leader, you're responsible for making things easier and faster for your team."_

### 5. There are many ways to motivate your team, and it’s also important to establish mutual respect. 

In the previous blinks, we touched briefly on motivation, but the importance of being a motivational leader can't be understated.

Motivation is what gets your team to be passionate about their work. It's what will make them dedicated workers who can tackle the most difficult tasks.

When your team is motivated, you'll find there's nothing they can't do, whether it's handling endless requests from the fussiest of clients or being creative enough to launch an app that makes learning calculus fun.

So it's the leader's job to motivate their team and get them to care about the work they do.

There are many ways to do this, but it's especially important that each and every team member understands why their work is important so that they're not just working for a paycheck.

It's also important to avoid micromanaging. Motivated employees need a sense of ownership, and they won't feel that they own their work if you're constantly telling them to do things your way. Micromanaging can also be an obstacle to action. You don't want all your team members feeling like they need to get your permission before they do anything.

Plus, don't forget to celebrate the accomplishments your team makes. Instead of jumping from one goal to the next, throw a party when you meet a milestone or complete a big project!

Another crucial part of leading a motivated and loyal team is to earn their respect.

These days, it's common for an employee to jump from one job to the next, hunting for the best-paid position at the most prestigious company. But this doesn't mean that you can't win the loyalty of a good employee.

At the heart of a successful company is a feeling of mutual respect between the team and its leader.

To establish this, you must first show them _your_ respect, which you can do by regularly asking for their opinion and never dismissing their concerns.

And to solidify their respect, always keep your promises and show solidarity by putting in just as much hard work as you ask of them.

### 6. To be efficient, prioritize your work, don’t overload on data and remember to take care of yourself. 

One of the most challenging aspects of being a leader is finding the time to manage your team without neglecting all the other work you have on your plate. This challenge is compounded by the fact that multitasking increases your chances of spending too much time on something that is ultimately unproductive.

This is where prioritizing comes in. To sort out your work and make sure you're doing what really needs to be done, adopt a _triage mentality_ and rank your tasks by how important and productive they really are.

Triage is more often used in a medical setting to make sure the sickest patients get treated first, but it applies perfectly to business as well. Just like sick patients, there are some tasks that need more urgent care than others, and there are certain projects that need to be dropped altogether.

There are also disruptive activities that can seriously hurt your productivity, such as checking your email every few minutes. Every time your focus shifts to a new activity, you disrupt your workflow and waste time, because you then need to re-focus your attention back to the more important work. So be more efficient by de-prioritizing your email and setting specific times to visit your inbox.

Another key practice is knowing when to cut off the amount of data you let in.

You can spend a lifetime trying to educate yourself about a certain problem. This may feel like the only way to make an informed decision. But, in reality, our brain can only handle so much information before we become confused and distracted. So limit the time you spend gathering data and you'll be more productive _and_ make better decisions.

Finally, don't forget to take care of yourself.

With so much going on, it can be easy to forget to prioritize your own health.

Making decisions and solving problems can be just as tiring as physical labor. And mental overexertion can have some unpleasant side effects. People who are overworked and sleep deprived often feel burnt out, moody, sick and have trouble focusing.

So remember to take breaks, have fun and get enough sleep. It's not only good for your health; it'll lead to being more productive and better leadership, too.

### 7. Final summary 

The key message in this book:

**As an executive you are responsible for the environment in your company, and for the performance of your team. You are their leader, but you'll find productivity only when you act like you're a part of the team. That means establishing goals, being respectful and, perhaps most importantly, looking out for the team's (and your own) well-being.**

Actionable advice:

**Don't overthink it.**

The next time you need to make an important decision and you're torn between several options, don't think too much. Simply compare the possible benefits and repercussions of every action, ask yourself what's best for your team and business, listen to your gut — and act.

**What to read next:** ** _The Effective Executive_** **, by Peter F. Drucker**

As we've just discovered, becoming a productive and effective leader isn't easy. The steps outlined in these blinks will help you up your management game, but if you want to take your leadership skills even higher, you need to consult Peter F. Drucker, the ultimate authority in business leadership.

In _The Effective Executive_ — first published in 1967 — Drucker offers a step-by-step guide to becoming the best leader you can be. Millions of aspiring leaders, including Jeff Bezos, have benefitted from Drucker's classic work, and you can too. Just head over to our blinks to _The Effective Executive_ and discover the vital skills every great leader needs.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Laura Stack

For decades, Laura Stack has helped organizations get remarkable results. She is the president & CEO of Productivity Pro, Inc., a member of the CPAE Speaker Hall of Fame and a best-selling author. Her other books include _Execution is the Strategy_ and _The Exhaustion Cure_.

