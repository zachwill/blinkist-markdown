---
id: 58fc94efa6d7cf0004aeb8fe
slug: dollars-and-sex-en
published_date: 2017-04-25T00:00:00.000+00:00
author: Marina Adshade
title: Dollars and Sex
subtitle: How Economics Influences Sex and Love
main_color: ED3239
text_color: A12227
---

# Dollars and Sex

_How Economics Influences Sex and Love_

**Marina Adshade**

_Dollars and Sex_ (2013) demystifies the complex issues of sex and love by looking beyond the raging hormones and cultural mores and exploring the driving force behind most of the world's sexual trends: economics. By applying simple economic theory, we can better understand why we regard romance the way we do as well as why contemporary relationships face so many challenges.

---
### 1. What’s in it for me? Discover the economy of love and sex. 

Imagine if, one morning, your partner turned to you and said, "I'm with you because I'm not attractive enough to be with the people I'd rather be with; you're simply the best partner I can get right now." If such a declaration didn't leave you speechless, you'd probably get a little angry. This analysis seems awfully cold and unromantic — an utterance more appropriate to lab workers than to lovers.

Most of us want to believe that the laws of love transcend the harsh logic of supply and demand. But, as you'll learn from these blinks, many of our "romantic" choices aren't all that different from our buying decisions. If you can briefly ignore the protestations of the heart, you'll be amazed at how economic factors influence our decisions about love and life partnership.

In these blinks, you'll learn

  * what's wrong with many online dating sites;

  * how income inequality can lead to divorces; and

  * why there are fewer virgins in colleges with fewer male students.

### 2. In many societies, liberal views on female sexuality have led to more freedom. 

If you were to compare your views on sexuality to those of your grandparents, there's a good chance that your attitude would prove to be the less conservative one.

Over time, most societies have grown more promiscuous, and that's no longer a sign of declining morality. Today's educated and independent women are more promiscuous than those of previous generations, and there's an interesting economic explanation. The women of today can _afford_ to be promiscuous.

Most fundamentally, they have the means to buy their own contraceptives, which protect them from STDs and inopportune or unwanted pregnancies. What's more, today's women are sufficiently empowered to insist that the contraceptives are used correctly.

If we go back just one or two generations, women weren't half as free. Unmarried women who maintained sexual relationships with more than one man were essentially pariahs. They were publicly shamed and their reputations were forever tarnished.

Today, however, premarital and extramarital relationships are far more socially acceptable. So are pregnancies that result from such relationships. Since the average educated woman earns much more now than she would have a generation ago, she is in a position to raise a child on her own if she so desires. And, if she chooses to terminate the pregnancy, that is also a socially acceptable, not to mention affordable, decision.

Thanks to these developments, women today are better able to cope with the difficulties that often accompany an unexpected pregnancy.

### 3. With lowered financial and social risks, the number of premarital pregnancies has increased. 

You might think that the introduction of safe and reliable contraceptives such as birth control pills would reduce pregnancies. However, premarital birth rates have actually increased over the years.

This might sound contradictory, but remember that as contraceptives improved, the potential financial and social risks surrounding premarital sex dramatically decreased, leading to more active sex lives.

If we go back to 1925, an unmarried woman actually had an 85-percent chance of getting pregnant if she had unprotected sex with a man. Back then, that was an even bigger deal than it is today. A premarital pregnancy would result in a damaged reputation and make it much harder to find a job or a husband.

But it could also have major financial consequences.

Let's apply some basic economics to this scenario. Say her lifetime income in 1925 is $45,000. We can take that 85-percent chance of pregnancy and multiply it by $45,000 to get the potential cost of premarital sex in 1925: 0.85 X $45,000 = $38,250.

But let's take this scenario a step further. If condoms had been available in 1925, it would have reduced the probability of pregnancy to 45 percent. While this wouldn't change how much income was at risk, the potential cost of premarital sex would drop to $20,250.

As risks decrease, it's only natural that more people decided it's worth it to have premarital sex. And when we add more lenient cultural attitudes toward promiscuity to the equation, people become even more willing to have intercourse before marriage.

Such attitude changes happen gradually, however. People in liberal areas begin having premarital sex without getting stigmatized, which gets noticed by those living in more conservative areas. Eventually, those in the conservative areas shed their fear of other people's opinions. In the long run, the more liberal attitude prevails.

But as great as contraceptives are, they're never 100-percent effective. In fact, 45 percent of all sexually active women still end up getting pregnant at some point, which is how we end up with a lot more pregnancies than before the introduction of contraceptives.

### 4. The law of supply and demand has led to women becoming more sexually active in college. 

If you didn't get lucky in college, don't feel too bad. After all, the marketplace may not have been in your favor.

That's right, when we look at sexual activity on college campuses, we can clearly see the laws of supply and demand in action. Unsurprisingly enough, heterosexual male students stand a better chance of having sex before graduation if they attend a college with a higher female population.

And, happily for these heterosexual male undergrads, the number of female college students is on the rise. Indeed, at many colleges, the number of females exceeds the number of males. In fact, in Canada, only 42 percent of college students are male.

In economic terms, colleges are a "buyer's market" for heterosexual male students; their power in the overall sex marketplace increases as the number of female students increases. It's straightforward supply and demand: if there's an abundance of a certain product or service, all the competition ends up driving the price down.

But in today's particular marketplace, the women are the ones with the overabundance of (so to speak) goods and services. In this "sex market," the women will put a lower "price" on their availability and might be more willing to push their limits and be more likely to offer sexual intimacy to interested young men. All because of these "market" forces.

According to studies conducted by the authors Mark Regnerus and Jeremy Uecker, in their book _Premarital Sex in America_, if a college campus is predominantly male, a female has a 45-percent chance of keeping her virginity. But the number drops to just 30 percent when the campus is predominantly female.

All this suggests that, when men are scarce, women feel the need to change their attitudes about when they should or shouldn't become sexually active.

> _"...when women are abundant, there is far less traditional dating and far more 'hooking up.'"_

### 5. The criteria of internet dating sites create a thin marketplace. 

If you've had any experience with online dating, you know that these sites can have thousands of active members, and that it's impossible to find the time to inspect every single profile. So, instead of scrolling for all eternity, you rely on the questions the site asks. You check a few boxes about your likes and dislikes, and you hope for the best.

However, the extremely broad questions these dating sites ask often make the market ridiculously thin — drastically reducing the likelihood of your finding your ideal partner.

Criteria like preferred age and height can shrink the marketplace of the online dating pool dramatically. Such a shrunken marketplace is, in economic terms, called "thin." A thin market is one where it's hard to find anyone to "trade" with because there are so few buyers or sellers interested in the same thing. In this kind of environment, you get people who are missing out on other opportunities and settling for bad deals. Such markets tend to leave no one satisfied.

And the online dating marketplace usually isn't very satisfying. Two people who might be the perfect match are forced to put in restrictive criteria that only serve to limit the chances of their meeting or even getting the chance to chat.

So, to create a healthy marketplace that'll be better at helping people find their soul mate, online dating services should de-emphasize quantitative data like height, and incorporate more experiential criteria.

Experiential criteria are more reliable because they're based on how we actually experience and interact with people. Thus, they're less limiting. After all, a person's sense of humor is more important than the size of their bank account or their TV-watching habits.

Think of someone you really love. What are the chances that they would've made it through the filters imposed by strict dating-site criteria? It's likely your true love could be a few inches taller or earn a little less money than your ideal preferences would indicate!

### 6. Marriage makes good economic sense for the household and the bedroom. 

Have you ever wondered why people get married? It's a fair question. And, in truth, the answer likely has more to do with economics than romance.

A household isn't unlike a business with demands that need to be met. Like business partners, partners in marriage are, ideally, two people with complementary skills — skills that allow them to satisfy household demands better than either partner would alone.

So marriage makes good business sense, since two people can maintain a household more efficiently than one — whether the household requires outside goods and services or the maintenance of in-house functions.

And a well-functioning household presents a lot of jobs, or "services": cleaning, laundry, keeping track of bills, the purchase of groceries. With two people, there's a better chance that all of these jobs will be done well. Perhaps one person is better at carpentry while the other has a knack for bookkeeping; by dividing the jobs to suit the strengths of each partner, the household can be run successfully and efficiently.

This applies to the bedroom as well. In economic terms, sex could be considered both a "good" that individuals need and a "service" that marriage provides in a more efficient manner.

A lot of time and effort can be spent trying to acquire sex in the marketplace, so it makes good economic sense to have it be a part of your household. It also removes the risk of disease, violence and humiliation that comes with dating, and the risk of arrest that comes with an illegal sex market. It's hard to argue that sex isn't more efficiently provided within a marriage.

According to a 2004 study by economics professors David Blanchflower and Andrew Oswald, 76 percent of people who are married have sex two to three times per month, while only 57 percent of unmarried people report the same frequency.

> _"...the most productive matches are those in which individuals are different enough from each other so as to exploit the gains from trade."_

### 7. Better economics pushed polygamy aside in favor of well-educated wives with marital power. 

Have you ever wondered why some societies accept polygamous marriages? Though the idea of a man taking multiple wives might seem strange to you, there are, in some countries, clear reasons to condone the practice — specifically, economic reasons.

From an economic standpoint, it makes good sense that a woman would prefer becoming the second or third wife of a wealthy man than the first and only wife of a poor man. While being an additional wife might come at a "cost," that cost is nonetheless outweighed by the benefits.

The advent of a new wife might impose a psychological cost on the rich man's first wife, too, but, since each wife is economically better off than before, it's worth it. And if the rich husband is happy, everyone wins, right?

Well, yes and no. When you reduce the number of poor men and uneducated women, polygamous partnerships start to make less sense.

After the Industrial Revolution and the move to the cities, it was better for men to have a few smart children rather than the many children who were necessary to maintain a vast farmland. Children needed to be smart in order to land a decent job, and the best way to ensure intelligence was to make sure their mother was an educated woman.

These new economic necessities actually proved to be a boon for women, since the increased demand for skilled wives also gave women more freedom to choose a husband.

Naturally, an educated woman with a range of skills is more likely to achieve financial independence. She will have the wherewithal to provide for herself and therefore needn't resort to becoming someone's second or third wife. If she so desired, she could even marry a penniless man.

So, polygamy tends to become less common as societies become more educated. And, as we'll learn next, education is also what keeps a monogamous relationship together.

### 8. Divorce rates are closely related to financial security and economic equality. 

Most of us probably think that love is the glue that keeps most marriages together. But money and education play big roles as well.

According to a recent study conducted by economics professors Philip Oreopoulos and Kjell Salvanes, those with a postgraduate degree only have a 3-percent chance of getting divorced, while those with a high school diploma have a 16-percent chance.

It's not that people with an advanced degree are better able to find a perfect match; it's that people with a better education usually make more money.

Financial instability tends to lead to stress, which is a well-known marriage-killer.

Picture a small neighborhood where each family's house looks the same. Each person wears similar clothes and drives similar cars. Now imagine one family begins to earn much more money. They build a fancy new house and start buying expensive new clothes and cars.

The rest of the community would likely start to feel as though _they_ deserved these things, too; no family wants to look like the poorest on the block. Maybe one or two families would start saving up, but, in reality, most would start taking out loans and racking up debt, making their situation worse and entering a downward spiral.

When a family hits economic hardship like this, it creates a tough and stressful situation. As the threat of bankruptcy looms, they might take on more work, further increasing the stress, and, before long, the marriage will start to come apart at the seams.

Unfortunately, this kind of economic inequality — and the marriage-killing status anxiety that goes along with it — is extremely common.

The research of economists Adam Levine, Robert Frank and Oege Dijk shows that countries with high levels of inequality also have high rates of divorce. In fact, they're so tightly linked that a 1-percent increase in a country's financial inequality will increase its divorce rate by 1.2 percent.

In other words, as a select portion of society reaps the benefits of financial gain, the matrimonies of the less financially fortunate suffer.

### 9. Final summary 

The key message in this book:

**The world of love and sex isn't merely a matter of oceanic desires and emotions. Our choices and their consequences actually make a whole lot of sense when we look at things from an economic standpoint. In fact, the logic of economics explains the otherwise mystifying world of dating, marriage and divorce.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Freakonomics_** **by Steven D. Levitt and Stephen J. Dubner**

_Freakonomics_ applies rational economic analysis to everyday situations, from online dating to buying a house. The book reveals why the way we make decisions is often irrational, why conventional wisdom is frequently wrong, and how and why we are incentivized to do what we do.
---

### Marina Adshade

Dr. Marina Adshade, PhD, teaches at the University of British Columbia's School of Economics. She regularly writes for the _Globe and Mail_ and is a frequent contributor to publications such as _Time_ and the _Wall Street Journal_.

