---
id: 5d210d886cee07000704d9b7
slug: donothing-en
published_date: 2019-07-08T00:00:00.000+00:00
author: Rob Dubé
title: donothing
subtitle: The most rewarding leadership challenge you'll ever take
main_color: 6EA5D7
text_color: 466A8A
---

# donothing

_The most rewarding leadership challenge you'll ever take_

**Rob Dubé**

_donothing_ (2018) offers executives and business leaders a first-hand account of how a regular meditation practice can bring priceless rewards to their business and personal lives. In addition to drawing from his experiences, as well as those of other successful leaders, author Rob Dubé provides scientific evidence of how lives and businesses can be enriched by meditation. Newcomers will find tips on how to start their own practice and what to expect at meditation retreats.

---
### 1. What’s in it for me? Learn how meditation can make you a better boss and improve your business. 

There's a good chance you've heard someone gush about how much meditation has helped them with their stress and anxieties. But less is said about the many ways in which meditation can also support being a boss and even improve a small or large business.

The idea of meditation being good for business may raise some skeptical eyebrows, but author Rob Dubé has put together some pretty convincing evidence that will likely win over the doubters. For starters, he charts his experiences of adding meditation to his daily routine and how it helped him turn his business around and earn record profits.

Dubé also offers a lot of scientific evidence to explain why regular meditation will not only result in benefits like better attention and listening skills, but how they can help you become a better boss. As the saying goes, numbers don't lie, and in this case, they all suggest that meditation is something that every manager and executive should consider adding to their toolbox.

In these blinks, you'll learn

  * how listening can empower your employees;

  * how a skiing accident led to one company saving millions in health-care costs; and

  * how meditation can make you a better runner.

### 2. Meditation has improved the author’s life and his business. 

In 1991, Dubé and his friend, Joel Pearlman, founded imageOne, a company that recycled toner cartridges for laser printers. At the time, sitting down to meditate was the last thing on Dubé's mind. 

Fast forward to 2004. The company now had annual earnings of $6 million and was bought by a billion-dollar corporation. The deal was supposed to allow Dubé and Pearlman to run their company as a subsidiary, with their autonomy intact, but it soon became clear that they were no longer serving their own vision.

As they lost control of their company, they lost their passion for business. For Dubé, once he no longer knew what he was working for, he began to grow increasingly anxious and worn out. Fortunately, this is when he discovered the peace of mind that comes from meditating.

The first time Dubé tried meditation, it was just a five-minute session that involved nothing more than sitting in his chair. But these five minutes eventually led to a daily practice that has brought him innumerable benefits.

To begin with, it allows Dubé to focus on his work with a clear mind, unencumbered by judgemental or otherwise distracting mental noise. As a result, his practice has made him more productive, more capable of seeing the day's trivial problems with a fresh perspective, and given him greater access to his innovative ideas.

He's also experienced the wonderful meditative benefit of no longer being controlled by a fear of failure, while also experiencing a newfound openness to experiencing novel situations. Meanwhile, he was finally able to relax in his spare time.

While these are all priceless benefits, Dubé also has meditation to thank for getting his company to where it needed to be.

In June 2006, Dubé and Pearlman jumped at the chance to buy back their company. At the time, Dubé was nine months into his daily practice, and this was instrumental in helping him manage the unavoidable stress and anxiety that took place during the re-buying process. Meditation was the tool that allowed him to slow down, take a step back and re-examine his priorities.

With his added clarity, he and his partner were able to identify what mattered most going forward. This included being the kind of leaders who cared about their employees, put a high value on company culture and made a positive impact on their community.

> _"Between stimulus and response lies a space. In that space lie our freedom and power to choose a response. In our response lie our growth and our happiness."_ — Viktor Frankl, Austrian neurologist.

### 3. Meditation can improve your ability to listen to others, which comes with its own benefits. 

Thanks to the revitalization effort Dubé and Pearlman put into their company, a decade after they reacquired it, imageOne was featured on a 2017 _Forbes_ list called _Small Giants: America's Best Small Companies_.

This honor was the result of years of hard work and overcoming one obstacle after another. And for Dubé, it was an honor made possible by meditation and the clear and calm determination it provided.

One of the more memorable obstacles occurred around the summer of 2014 when it was becoming obvious that the company was on a bad trajectory. Unless changes were made, it was clear that they were not only going to miss their sales and revenue targets, they'd also be operating at a loss.

What followed was a series of "panic meetings" initiated by the executives. Although the situation was dire, Dubé didn't feel the least bit panicky. His regular meditation sessions were making him feel calm, clear and confident — so much so that he took on the bold solution of switching to the process of open-book finances. Such financial transparency would mean that the company's balance sheets and income statements would be shared with everyone on the team.

This wasn't an easy decision to make, but with meditation helping to calm his nerves, he was able to follow through and, sure enough, the transition worked wonders: with all those extra hands on deck, it not only led to quick solutions but it also led to 2016 and 2017 becoming record-breaking years for profit.

Another valuable benefit of meditation has to do with attention. Thanks to his practice, Dubé was now feeling more present in meetings and better at listening and truly understanding what people were saying. 

While this is naturally useful in personal relationships, it's also highly valuable in a company. Every effective leader needs to be fully present and hear the input of their team members and what they need.

After all, people have a basic need to know that they're being heard. So giving someone your undivided attention shouldn't be seen as doing them a favor. Instead, it's a basic courtesy that everyone from children to adults deserve.

As Dubé discovered, listening is also an act of empowerment. By being fully present with his employees, he's shown that he values their service and opinions. This, in turn, gives them the confidence needed to grow and become even better at their jobs. Plus, when you truly listen, you'll be in a better position to understand someone's strengths and weaknesses so that you may provide more thoughtful improvement strategies.

Therefore, when you listen, everyone wins.

### 4. Science has proven the benefits of meditation, and successful leaders are catching on. 

If you're still skeptical about the benefits of meditation, you don't have to take the author's word for it. You can also look to scientific facts.

In 2015, the _Journal of Management_ published a study from Pepperdine University School of Business. It revealed that a person's attention and focus could be significantly boosted by just a few hours of mindfulness practice. Mindfulness is a form of meditation intended to bring more awareness to what's going on around us and in our minds in the present moment.

Since our minds are estimated to be aimlessly wandering for approximately 50 percent of our waking hours, mindfulness, which can reduce that amount, can go a long way in making us more productive.

But increased attention isn't the only scientifically proven benefit. A 2011 Harvard University study used MRI scans to show that two months of mindfulness meditation increased the amount of the gray-matter density in the hippocampus, which is the area most closely associated with memory and learning.

It perhaps comes as no surprise that meditation has now become a common practice in the business world.

Mark Bertolini is the CEO of the insurance giant Aetna, and according to _Fortune_ magazine, he's also among the top 50 World's Greatest Leaders. In the early 2000s, Bertolini had to face some serious hardships after helping his teenage son recover from a cancer diagnosis and overcoming his own life-threatening injuries from a skiing accident that left him with a partially paralyzed left arm.

Bertolini began his meditation practice during his recovery, and he credits it for helping him to heal faster. So, after he was made Aetna's CEO in 2010, he set about sharing these benefits with his employees. This included a health and wellness program that promoted better sleep and nutrition habits as well as the practice of yoga and meditation.

The program proved popular, with over a quarter of Aetna's 50,000 employees signing up. Soon, the participants reported a 28 percent decrease in their stress levels, while their productivity increased by an additional 62 minutes per week. That added up to around $3,000 more in annual revenue per person. 

But it wasn't just the employees who benefitted. Aetna's health-care costs also dropped by 7 percent.

These numbers tell the truth: everyone can benefit from meditation.

### 5. Meditation has benefits outside the workplace too. 

While meditation has a proven track record for helping business leaders and their companies, there's really no limit or boundaries to how calm focus and increased awareness can improve one's life.

For example, many soldiers have found meditation to be extremely useful for combating the stress and trauma that comes from being on active duty.

The events that a soldier experiences on the battlefield often lead to Post Traumatic Stress Disorder (PTSD), a debilitating condition characterized by anxiety, amnesia and repeated flashbacks.

Holly Richardson and Matt Jarman teach cadets meditation at the Virginia Military Institute, and in doing so, they're giving the cadets a valuable tool to prevent and cope with the symptoms of PTSD.

Many studies support what Richardson and Jarman have experienced: When active-duty soldiers regularly practice meditative awareness, they're better prepared for going into battle and coping with the post-combat stress that inevitably follows. As a result, the soldiers are more capable of recognizing when they're experiencing a flashback and bringing themselves back into the safe environment of the present moment. By doing so, soldiers have been able to reduce significantly the post-combat stress that leads to PTSD.

As for Dubé himself, he's found meditation to be a great asset in helping him stay calm and clear during the 14 marathons in which he's participated. 

This calm clarity is especially beneficial during the "taper period," which is the days leading up to the big event when runners hold back and run fewer miles than usual to save their energy. During this time, it's common for runners to become anxious and wonder if they should be running more, so meditation has been extremely useful in reducing this anxiety and finding a calm and focused presence ahead of a big race.

And then there's the recovery period after the race when meditation can ward off the feeling of emptiness that often takes over once the high of the race wears off. 

Perhaps most useful of all was the help meditation provided when tragedy struck the Boston Marathon in April 2013. Dubé's family was at the marathon to cheer him on, and he credits meditation with enabling him to stay calm and care for his family after the bombings and the chaos that ensued.

Even though he was shaken by the attack, meditation also helped him move on and run the New York Marathon seven months later, in defiance of the terrorists.

### 6. Meditation involves finding a comfortable environment and posture and then simply noticing your breathing. 

Now that you're well aware of the benefits, let's get down to the business of how you can start reaping the rewards of your meditation practice.

The first step to any successful session is to find a good place where you can practice undisturbed. This should ideally be a quiet spot at home or in the office, where you can enjoy some privacy or otherwise close the door and let others know that you're not to be interrupted.

While peace and quiet may be important early on, after practicing for a while, you'll find that the level of noise is less important when considering a place to meditate.

Once you have your location, it's time to find a comfortable position.

Many people, including the author, enjoy sitting cross-legged on a meditation pillow so that they can feel a strong, grounded connection to the earth. This isn't a requirement, however, so don't worry if this position feels uncomfortable for you.

But keep in mind that the main priority is comfort and not relaxation. So, if you prefer a meditation bench or a chair, avoid leaning back in the chair. But if you can only get comfortable by leaning back, or even lying down on a bed, that's OK. And if you fall asleep during the session, don't worry, you probably needed the rest!

That said, there is an ideal posture for meditation. Just like a sprinter's starting position, this position is intended to maximize the performance by letting you be comfortably still for long periods of time.

The best posture is sitting upright without too much rigidity or slouching. Your chin should be pointed toward your chest so that your eyes are aimed slightly downward. You can keep your eyes closed or open, but you should try to focus your gaze softly inward as if you're looking out through the back of your head.

Once you're settled, let yourself feel grounded within your body by noticing how parts of your body, such as your feet, legs and bottom, are connecting you to the ground.

Now, you can begin by simply noticing your breathing. 

Notice each inhale, with the breath coming through your nose and down into your lungs, and then coming back up and out through the exhale. As much as you can, continue to notice your body and your breathing. Some thoughts are sure to intrude, but that's OK. Whenever a thought arises, just gently move your attention away from the thought and back to the in-breath and out-breath.

That's all there is to it. It may sound simple, but as we'll see in the next blink, it takes consistent, regular practice for you to experience sustained benefits.

### 7. Developing a regular practice is difficult, but it is possible – and crucial. 

Just like any other exercise, the rewards of meditation come from regular practice. So, in a way, you can think of sitting and meditating as a way of strengthening your "presence muscles."

But if you've ever let a gym membership expire, then you know that being consistent at exercising is easier said than done. So here are some helpful tips to help you stick with it.

First off, you should set a goal for yourself. If you're a manager or business leader, then you know the motivational power of clear goals — they can act like a guiding light to keep progress and development moving forward. Therefore, a good goal to start with would be to sit down for 20 minutes of meditation, every day, for three weeks. As science tells us, three weeks, or 21 days, is the average time it takes to start a habit.

The second tip for getting you through those crucial first weeks is to give your practice a steady and familiar context, by sticking to a regular time and place — such as in the bedroom, first thing every morning.

The third tip is to start small and keep in mind that the continued practice is key. If 20 minutes proves too difficult, make it 15. If one day all you can do is take one mindful minute, that's all right. It will still keep the ball rolling and help get you toward a regular practice.

The last tip is to look at the various meditation apps that are available, such as _Meditation Studio_ and _10% Happier_. These can also help keep you on track.

If you still feel like you don't have enough time or patience for meditation, maybe Tim Ferriss can convince you otherwise.

Ferriss is the author of multiple best-selling lifestyle books, including _The 4 Hour Workweek_ and _Tools of Titans_. He's also a very busy man. If he isn't writing or researching a book, he's likely prepping or recording his popular podcast _The Tim Ferriss Show_.

Nevertheless, amid all these responsibilities, Ferriss never fails to find 20 minutes every day for meditation. Like many others, he sees his daily practice as _giving_ him time, not taking it away. Ferriss believes meditation has made him at least 50 percent more productive than he used to be. He's called it a much-needed "warm-up in recovering from distraction." 

It's also worth noting that when Ferriss interviewed one hundred of the best entrepreneurs and innovators for his book, _Tools of Titans_, he found that more than 80 of them had a daily meditation practice.

### 8. Silent retreats can help you deepen your meditation practice. 

If you want to become a better leader, a regular meditation practice will help by making you more self-aware and attentive to your surroundings and other people. But if you want to take it a step further, you could try a silent retreat.

Silent retreats offer you a way to go even deeper into your practice and have become popular for a variety of successful leaders, including Jeffrey Walker, a leading UN administrator, and Andrew Cherng, CEO of Panda Express.

While people can go on silent retreats that last a day or several years, the author recommends a first-time retreat of around four to seven days. During a typical retreat, the first couple of days involve settling in and receiving instructions about how all the participants and instructors will remain completely silent.

When you begin to meditate at a silent retreat, you're likely to start out just skimming the surface of your thoughts. But then the complete silence begins to set in, and you'll find yourself going deeper and letting go of mundane worries, like that recent argument you had with your partner or that stressful project at work. 

When you're immersed in a silent retreat, you may find some deeply buried thoughts and emotions rising to the surface, but at the same time, you may find groundbreaking ideas and connections also emerging. Once, when the author was sitting at a retreat, he noticed the hair bun of the person in front of him and became overwhelmed by tears of happiness since it connected him to cherished memories of his daughter.

In the final days of your retreat, you'll be eased out of the silence and prepared to re-enter the world. This is like being pulled from the depths back to the surface of your thoughts. But it's done in a gentle way so that you'll continue to feel grateful for the increased awareness, clarity of mind and fresh perspectives on yourself and the world around you.

If this sounds good, you should research the many options around you.

Naturally, the author recommends his four-day _donothing_ Leadership Retreat, located in the Rocky Mountains near Fort Collins, Colorado. Along with sitting meditation, this retreat also offers mindful approaches to eating and resting, all while being in the company of other open-minded leaders eager to grow and learn.

If you're on the East Coast of the United States, you may prefer Menla, a Tibetan-inspired retreat and spa located in the Catskill Mountains, around two hours north of New York City.

Whether or not you choose to take a retreat, one thing's for sure: you won't regret introducing a new and rewarding meditation practice into your life.

### 9. Final summary 

The key message in these blinks:

**Regular meditation practice offers countless benefits, many of which are scientifically proven. It can provide you with a clear and calm mind to cope better with the daily stress of life, as well as provide you with more capabilities for being present and listening to employees. This will make you a better leader and more able to support your growth as well as the continued development of your employees. Embarking on a regular meditation practice is like any other habit; it takes consistent practice. And while this can be challenging, the rewards are well worth the effort.**

Actionable advice:

**Divide your practice into chunks.**

If you set a goal for practicing 20 minutes a day, but find it difficult to sit still for 20 minutes, consider dividing that time into smaller chunks. For example, you could do ten minutes in the morning, before breakfast, and ten minutes before going to sleep at night. If this makes it more comfortable and more likely that you'll continue a daily practice then make this your routine with the goal of eventually working your way back to a 20-minute sitting.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Search Inside Yourself_** **, by Chade-Meng Tan.**

If you want to get a better idea of just how effective meditation in the workplace can be, then you'll want to check out _Search Inside Yourself_. These blinks will take you behind the scenes at Google, where author Chade-Meng Tan established a groundbreaking mindfulness program for the company's employees.

Through his years of research with the program, you'll learn about the benefits that come from mindfulness and increased emotional intelligence. In addition to the improvements people experience on a personal level, you'll also discover the effects meditation can have on a company-wide level in terms of creativity and productivity.
---

### Rob Dubé

Rob Dubé is the co-founder and president of imageOne, a printing services company that has been ranked by _Forbes_ magazine as one of the Best Small Companies in America. He also created the _donothing Leadership Retreat_, located in the Rocky Mountains. His writing has been featured in _Forbes_, _Thrive Global_ and _EO Octane_.

