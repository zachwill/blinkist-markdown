---
id: 55ac2f0f65363800073c0000
slug: dont-buy-it-en
published_date: 2015-07-21T00:00:00.000+00:00
author: Anat Shenker-Osorio
title: Don't Buy It
subtitle: The Trouble with Talking Nonsense About the Economy
main_color: 4961A8
text_color: 334475
---

# Don't Buy It

_The Trouble with Talking Nonsense About the Economy_

**Anat Shenker-Osorio**

_Don't Buy It_ (2012) explores the ways language influences our understanding of complex issues. Anat Shenker-Osorio brings her research and expertise to bear on a question that plagues progressives: why do conservatives always win economic debates in the United States, despite the deep inequality and structural injustice epitomized by the financial crash and the Great Recession? These blinks answer this question by analyzing the language employed on either side of the political spectrum.

---
### 1. What’s in it for me? Discover how politics is framed by the language we use. 

Do you remember the great financial crisis of 2007-2008 and its aftermath? No event in recent decades has shaken the world to such an extent. Banks fell, governments went bankrupt and millions of people around the world lost their jobs and livelihoods.

And yet, what has actually changed in the years since? You'd think that governments would have acted swiftly to implement policies to stop such a disaster from happening again — but they never did. Instead, the neoliberal policies that caused the disaster have persisted. Why?

It all comes down to the type of language we use. When describing the economy and the financial collapse, we do so in terms that benefit conservatives. These blinks show you how this happens and, crucially, highlight some better ways to describe the economy — ways that inspire change and progress.

In these blinks, you'll discover

  * why you should describe the economy like a car rather than a person;

  * why it's unwise to call the financial crisis a financial crisis; and

  * how we can knock down the barriers to equality.

### 2. The financial crisis exacerbated the deep inequality that has long plagued the American economy. 

Open up any newspaper and you'll encounter politicians, analysts and journalists attributing all of society's economic problems to the 2007 financial crisis. But in reality, there were deep economic problems long before that, especially with respect to wealth and income inequality.

In the decades leading up the crisis, real wages (adjusted for inflation) were stagnant, even while corporate profits were skyrocketing and overall economic productivity was on the rise.

As a result of these factors, wealth moved toward the top of society: today, 84 percent of wealth in the United States is owned by just 20 percent of Americans.

And that's a problem for two reasons, the first being psychological: studies show that societies with greater levels of inequality have lower levels of contentment overall, with grave psychological consequences for those at the bottom.

The second reason is economic: the United States is dependent on consumer expenditure for economic growth. Since declining wages — and thus, declining disposable income — reduce consumer demand, economic output also declines as a result.

So, as we've established, inequality was a problem long before the 2007 financial crisis. But that doesn't mean that the economic collapse didn't add insult to injury.

According to the Economic Policy Institute, 25 percent of American households experienced zero or negative net growth in 2009, up from 18.7 percent of households in 2007. And even more shockingly, that number rises to 40 percent when only taking into account African-American households.

Worst of all, individuals facing foreclosure, poverty and unemployment received no federal assistance after the economic crisis. Meanwhile, the financial sector was bailed out by the government.

Taken together, the crisis revealed the intense financial pressure facing households in the United States' middle- and low-income brackets. In the following blinks, we'll find out how Americans responded, and whether the downturn led to any meaningful policy changes.

### 3. Although they had an unprecedented opportunity, progressives failed to shift the economic debate after the crisis. 

We've just learned that the 2007 financial crisis exacerbated existing economic problems in the United States. However, the crisis did raise public awareness of income inequality and injustice.

Following the financial collapse, there has been unprecedented public interest in political and economic issues, as well as widespread anger about the unfair system currently in place.

Consider the fact that, in October 2008, Pre Research found that a full 70 percent of the notoriously apathetic American voting public was following economic news (the previous peak was 49 percent during the 1993 recession).

Just think of Barack Obama's successful 2008 presidential campaign, carried largely by a narrative of changing America to make it a fairer place, a stance that epitomized the public mood.

It's also worth noting that in this time of upheaval, progressive academics and policy experts appeared in the media more than ever before. Prior to the collapse, airtime went mainly to those who agreed with the conservative neoliberal agenda. But in the aftermath, the media finally started listening to left-wing economists and those with progressive views.

Despite the public anger and the influx of progressive voices, however, nothing really changed. Ultimately, the conservatives won the post-2007 argument.

Their message was that government interference in the economy had caused the crisis, and they argued that to solve the country's economic woes, the government had to back off.

If you look at the post-crisis elections, this message clearly got through to voters: Republicans won back Congress in the 2010 midterm elections. At the same time, the United States witnessed the rise of the anti-government, pro-free market Tea Party movement.

In fact, according to a 2011 Bloomberg poll, 57 percent of those surveyed agreed that "spending/tax cuts will give businesses confidence to hire."

How did conservatives manage to pull off such an unlikely victory? Keep reading to find out.

### 4. By helping us make sense of complex concepts, metaphors ultimately shape our thinking. 

Why do we say that someone who likes to garden has "a green thumb?" Is just a predilection for poetry? Not at all.

We naturally use _metaphors_ (non-literal speech) to make sense of complex and abstract ideas and concepts. Example? The economy!

This notion has been proven by the esteemed linguists George Lakoff and Mark Johnson, whose research shows that metaphors appear in our speech constantly — every ten to fifteen words, on average. To explain our love of the metaphor, Lakoff and Johnson argue that our brains are naturally wired to compare complex concepts to more tangible entities.

However, the metaphors we use to frame concepts aren't neutral. In fact, they profoundly influence our understanding of reality.

There is compelling evidence to suggest that we judge concepts very differently depending on the metaphors we use to describe them: two Stanford psychologists found that people who read passages describing crime as a "beast" are much more likely to say they support crime-fighting measures through harsher law-and-order techniques.

Conversely, those encouraged to think of crime as a "virus" were far more likely to support crime prevention measures.

Ultimately, this study suggests that one group of participants subconsciously drew upon their assumptions about how to deal with a "beast" when they thought about how to deal with urban crime — you fight it! Meanwhile, thinking about the same problem, the other group relied on associations related to coping with a "virus," such as immunization or washing one's hands.

All in all, using metaphors to simplify abstract concepts shapes our thinking, which, in turn, can have real-world consequences. Accordingly, the metaphors we use when discussing the US economy have had a major impact on the resulting economic debates.

### 5. Conservative politicians use powerful metaphors to advance their economic agenda. 

In the previous blink, we learned that metaphors have a massive impact on our thinking. Unfortunately for progressives, conservatives employ this insight extremely effectively.

This becomes clear when evaluating the metaphors conservatives use to influence economic debates. For instance, Republicans consistently use language that frames the economy as a living entity, which greatly affects public opinion about desirable economic policies.

Following the 2007 financial collapse, conservatives almost always referred to the economy as a patient, deploying words like "recovery," "ailing" and "fragile."

This linguistic framing directly aligns with two core neoliberal principles: first, that financial crises occur naturally, just like the flu, meaning that no one is responsible for what has happened; and second, that just like a patient sick with the flu, the economy will recover on its own, without any undue interference.

Similarly, conservatives often describe the economy as a medium for enforcing desirable moral behavior. According to this narrative, the economy rewards virtuous, hard-working people with wealth. Meanwhile, those with a lesser work ethic will be punished with poverty. This story incentivizes those on the lower economic rungs to emulate the purportedly honorable virtues of the affluent.

Surely you recognize this narrative. You've heard it every time a conservative pundit or Republican politician has railed against the possibility of raising entitlement benefits.

Here's just one example: reacting to a presidential proposal to reduce the principal owed by those at risk of defaulting and having their homes foreclosed, Republican Senator Bob Corker (Tennessee) retorted, "People who acted responsibly in Tennessee will be paying for the bad behavior of [other] lenders and borrowers."

Once again, this kind of language encourages people to see economic fortune as an indicator of moral worth — a powerful approach to political persuasion. But don't progressives also employ narratives?

> _"Examples of likening the economy to something natural, most often a body, abound."_

### 6. Progressives can reframe the debate by using the metaphor of a moving car to describe the economy. 

Conservatives are masterful at persuading the public to share their perspective on the economy, but progressives struggle in this respect. What can they do to reframe the debate and advance their own agenda?

One option could be to describe the economy as a moving car — maybe it's "stuck in a rut," "out of control" or "needs a jumpstart." This framing would allow progressives to promote government intervention in the economy.

After all, just imagine a moving car without a driver. Most people will associate the image with something dangerous and out of control. Thus, the metaphor would implicitly promote government involvement to "steer" the economy. Or, it might highlight the importance of imposing "rules of the road" on the financial industry. 

In general, it would present the economy as a man-made entity, in accordance with the progressive notion that it can be altered to serve society's best interests (and not vice versa).

Another major advantage of using the car metaphor? It defines the economy as a vehicle that facilitates an individual's financial journey through life.

This narrative implies that helping all citizens develop their potential and realize their dreams is the true role of our economic system. Contrast this story with the conservative account, which holds that whatever the outcomes offered up by the economy, they are natural and right.

To help promote this perspective, progressives can start using certain phrases: some people have "different starting points than others," or are "carrying more baggage." Both of those phrases succeed in conveying the progressive notion that certain individuals face unfair barriers to their progress through no fault of their own.

And crucially, it also communicates the point that these unequal starting points can be changed if we remove barriers to basic necessities like employment or education.

### 7. To promote the progressive perspective on the Great Recession, pundits and advocates should stop using words like crisis. 

Now that we know what progressives _should_ be doing, let's look at what they're doing wrong. Because even when they're passionately and sincerely arguing their own case, progressives rely on words that implicitly align with the conservative agenda.

To start, even the simple word "crisis" carries ambiguous connotations, since it's often used in a way that contradicts the progressive account.

Remember, progressives must convey that the economic collapse of 2007 was a long time in the making; the result of deliberate actions, misdeeds and oversights; and that sound government intervention could prevent a similar calamity from happening in the future.

Well, the word "crisis" doesn't carry any of these connotations. Consider the dictionary definition of the word: "An emotionally significant event or radical change in a person's life, e.g., a midlife crisis; an identity crisis."

In this context, the word suggests an unexpected event that will sort itself out naturally without necessarily requiring outside intervention. This happens to be the exact message the progressives _don't_ want to communicate.

Instead, they could employ other terms, like "economic blunder" or "damage," which would frame the events in a more favorable light.

Similarly, the term "financial reform" might be counterproductive if progressives wish to advocate for radical change to the financial system. After all, "reform" implies that the entity being reformed should remain in existence — that it should be, as per the dictionary, "reshaped from existing materials."

Thus, the term implicitly precludes a more radical rethink of the financial system — and thus, the neoliberal principles on which it rests.

Consider, by way of contrast, what might be implied by phrases like "financial overhaul" or "consumer protections." It's easy to imagine how these terms would trigger associations related to more radical changes.

### 8. Using the passive voice to describe economic events prevents us from holding people accountable for bad decisions. 

Think about all those negligent, and sometimes even fraudulent, CEOs and traders who contributed to the 2007 financial crash. They evaded financial regulations and deliberately obscured the riskiness of derivative financial products.

And yet, almost none of them have been held accountable!

How is that even possible? Well, perhaps it's not entirely surprising when we consider the ubiquity of passive language during economic discussions.

Examples abound: We talk about how "the unemployment rate rose" or "the dollar fell." You might hear a TV pundit say something like, "most people struggled with costs that _were_ growing and paychecks that _weren't_."

As mentioned earlier, the passive voice implies that events happen of their own accord, thus obscuring the mistakes and misdeeds that directly cause negative events. This ultimately prevents us from holding people accountable for their poor decisions.

Because in reality, the dollar didn't just fall on its own. Rather, economists and politicians made decisions that led to a currency depreciation. Just compare those two framings, and notice how the passive voice makes it easier for people to avoid taking responsibility for their actions.

Besides, the passive voice makes it far easier for financial institutions to profit from the poor and get away with it. Look at one prevalent post-crash phrase: "people are losing their homes." This formulation totally misses the point: people weren't losing their homes at random, banks were taking them away!

Moreover, by obscuring the real actors in the story, the passive voice carries the extra danger of lessening public empathy for the most disadvantaged people.

Our brains are wired to fill in gaps and create a causal link between action and consequence. So, when we refuse to clearly name the forces and actors who made conscious decisions to the detriment of others, it implicitly puts the burden on those who are suffering from the consequences. In other words, we end up blaming the victim.

> _"When we take care to select words that affirm what we believe, we increase the odds our audiences will understand and trust us."_

### 9. Today’s dominant political metaphors undermine the progressive cause. 

As we've discussed, inequality is on the rise in the United States; at the same time, social mobility is much lower than in many European nations. Even so, Americans continue to cling to the idea of the American Dream. Why?

The answer is language, of course! Although phrases like "the top/bottom 20 percent" have been picked up by the political left in recent years, metaphors related to vertical height are problematic because they imply notions of hierarchy and deservedness, concepts that advance the conservative agenda.

Just think of the preexisting associations we have with the concepts "up" and "down," particularly the link between being "at the top" versus "feeling low." We've also inherited historical notions about the upper classes being one's "betters," whereas belonging to the "lower" social classes is bound up with inferiority.

Clearly, these vertical metaphors don't send the right message. Instead, to energize public debate on the subject of socioeconomic inequality, progressives should talk about "imbalances" and/or "barriers."

One big advantage of both framings is that they don't imply any kind of hierarchy, but they are still effective. After all, brief jaunts on a rollercoaster aside, people loathe the feeling of being off balance.

Describing inequality as a "barrier" is similarly effective, especially since it connects to supplementary metaphors, like "being held back," "roadblocks," "being trapped in poverty" and "denied access to education."

There are three reasons this language helps underpin the progressive account: it underscores the structural causes of inequality; it stresses the fact that inequality is neither natural nor inevitable, but rather an alterable man-made phenomenon; and it takes the blame off the poor and puts the burden on those at the top, suggesting that those who have more have — perhaps deliberately — erected "barriers" to keep others out.

It's worth noting that Martin Luther King, Jr., actually used this barrier metaphor to great effect in the 1960s, when he successfully and movingly advocated for an end to racial segregation.

### 10. Final summary 

The key message:

**Unlike conservatives, who cannily use language to advance their political agendas, those on the progressive end of the political spectrum have long failed to carve out a distinct and persuasive narrative. To effectively counter the other side's position and advocate for their own principles, progressives should rethink the metaphors they use to talk about the economy and social order.**

Actionable advice:

**Be wary of the metaphors you encounter.**

Be mindful of the fact that metaphors shape our thinking; especially during campaign season, a healthy dose of critical awareness will help you cut through the false narratives and develop genuine political views.

**Suggested further reading:** ** _Manufacturing Consent_** **by Edward S. Herman and Noam Chomsky**

_Manufacturing Consent_ takes a critical view of the mass media to ask why only a narrow range of opinions are favored whilst others are suppressed or ignored.

It formulates a propaganda model which shows how alternative and independent information is filtered out by various financial and political factors allowing the news agenda to be dominated by those working on behalf of the wealthy and powerful. Far from being a free press, the media in fact maintain our unequal and unfair society.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Anat Shenker-Osorio

Anat Shenker-Osorio is a strategic communications expert, researcher and pundit specializing in public affairs and social issues in the United States. Her past clients have included the Ford Foundation, the Roosevelt Institute, the Congressional Progressive Caucus and the Ms. Foundation.

