---
id: 54299a1462666100081c0000
slug: dont-go-back-to-school-en
published_date: 2014-10-02T00:00:00.000+00:00
author: Kio Stark
title: Don't Go Back to School
subtitle: A Handbook for Learning Anything
main_color: 4190A0
text_color: 2D636E
---

# Don't Go Back to School

_A Handbook for Learning Anything_

**Kio Stark**

_Don't Go Back to School_ shows how not having a degree doesn't doom you to an unsuccessful life. In fact, quite the contrary: chalked full of real-life examples, this book presents a strong case for independent learning as well as principles you can immediately enact to make independent learning a part of your life.

---
### 1. What’s in it for me? Discover how to forge your own path without a college education. 

Graduate high school, get a degree and get a good job so that you can retire with security. This is the path that many aim to follow, and more often than not each of these steps is considered integral for a successful life. In fact, the aspiration of getting a formal education has become a defining characteristic of the generations following the mid-twentieth century.

Think about it: how many times have you heard someone say that you had better get a degree "unless you want to flip burgers for a living"?

However, more and more people are discovering that there are other ways to equip themselves with the skills and connections necessary to succeed in today's job market. In these blinks, you'll learn how to forget about fighting for a lofty degree and saddling yourself with debt by taking a different path: independent learning.

In these blinks you'll discover

  * how one entrepreneur became a high school teacher without having a high school diploma;

  * how having a little chutzpah can mean big rewards in the future; and

  * how taking a professor out to lunch can save you from the debt of a college education.

### 2. The value of college education is contestable. 

Many people today have shelled out a lot of money and racked up mountains of debt in order to get a college education, hoping that a job opportunity down the line will help them recoup all of the money they've invested. However, many never find such an opportunity.

College degrees are losing their value, and it's not always true that people with a degree will have higher lifetime earnings. While college degrees have proven their value since World War II, the trend has shifted.

When the GI Bill was passed shortly after World War II, war veterans returning home had an opportunity to start a new life by having their college tuition paid for by the government. Consequently, the percentage of US citizens with college degrees spiked hugely. 

Meanwhile, a college degree became prerequisite for an increasing number of jobs, such as journalism or public relations.

In addition, more women began receiving a college education to secure better jobs. Already at a disadvantage in the employment market, women in the US were able to forge new job opportunities by demonstrating aptitude with a degree.

During that time, college degrees guaranteed higher lifetime earnings due to moderate loan debt, easy employability and an annual increase in the value of a college degree. But those times are over.

Getting a degree is no longer a safe path. As more people with similar credentials flood the job market and the cost of education rises, higher competition and generally low wages mean that repaying college debt isn't easy.

What's more, degrees aren't the only kind of credential and are even becoming less necessary, while other credentials, such as portfolios and recommendations, are becoming increasingly important.

Work portfolios, for example, are as good at convincing prospective employers to hire you as is any degree. More and more often, jobs are found through network connections and within communities. Just knowing who's hiring can help you land a job as much as a degree can.

### 3. Independent learning is far more satisfactory than traditional schooling. 

When you go to school, teachers issue demands and then reward you with high marks for grades, tests and "correct" answers, and you eventually get a degree. This method can hardly be considered personally rewarding.

In fact, for many, these rewards are _de_ motivating, and cause people to lose their passion and curiosity at school. The rigid structures and expectations, combined with the competition for artificially scarce rewards (grades), stifles and confines our curiosity.

The rewards offered at school are forms of _external motivation_. Your grades, praise for getting "correct" answers, and a degree are all rewards that come from the outside.

However, we are far more motivated by _intrinsic motivation_, when we do things because the reward is the task itself. If you've ever just felt good admiring your achievement or mastery over something, then you've experienced intrinsic motivation.

While the traditional school systems barely offer opportunity for intrinsic motivation, self-organized _independent learning_ initiatives do, and the benefits of this learning style are amazing. In fact, learning what you want to learn leads to better and faster achievements as well as a longer period of engagement.

However, independent learning doesn't mean doing everything on your own. Instead, you should join groups and share in the experience of learning with others, yet remain self-motivated and self-organized.

Some modern forms of learning have attempted to use technology to try to open up education, such as _Massive Open Online Classes (MOOC_ ). However, they haven't been successful, because they replicate schools:

For example, professors vary widely in their teaching skills and engagement. In addition, grading systems are built around tests or quizzes without proper feedback, meaning it's unlikely that you'll retain the material for long.

MOOCs put _teaching_, not _learning_, online, but this is totally backward: it's not teaching that has to be reformed, but learning itself.

> _"There is something really special about when you first realize you can figure out really cool things completely on your own."_

### 4. Despite the need for a radical shift towards independent learning, we shouldn’t do away with schools just yet. 

Even though independent learning has a lot to offer, too many people depend on schools to abolish them entirely. Moreover, independent learning still relies on the materials produced by and for schools and universities.

Independent learners have to learn how to obtain their own material and information in order to shift away from a reliance on standardized schools. Interlibrary loans, for example, are a great way to acquire the texts you need to learn something new. Independent learners can find copies of federal documents as well at Government Documents Centers, often housed in university libraries.

Scientific articles, on the other hand, are a bit harder to find. However, while they are most often found in expensive journals, the trend is toward open access. Open access gives independent learners a chance to access information which is normally only available either with money or at universities. It also signals that universities are open to finding ways to remain important sources of information.

Many scientists themselves want to share their ideas and results with a much broader group. At the time this book was written, 13,000 researchers had gone on strike and withheld their work from journals, wanting instead for their work, as well as the discussions and criticisms of their work, to be freely available.

Not only that, but they're often not even paid for their articles or reviews, despite the prohibitive costs of the journals!

Some intellectuals are even actively taking their articles and ideas to other platforms, such as public lectures, more diverse publications or even tutorials.

If this trend continues and more intellectuals and academics follow suit, the wealth of freely available knowledge will make independent learning far easier in the future.

Now that you've learned about the substance and value of independent learning, the rest of our blinks will give you practical tips to reap its rewards.

### 5. Don’t learn alone; be independent but not an autodidact. 

Many people imagine that all independent learners are autodidacts, poring over books at home in complete isolation. However, self-teaching without community is not the best way to learn! Successful independent learners connect, interact and share.

Quality learning requires the support of not only learning materials, but also other learners. By learning with others, you have an opportunity to share your knowledge and experience with the group, and have others do the same for you. This exchange of information also allows you to share feedback on how you and others might improve.

In fact, Quinn Norton, a writer and photographer interviewed for this book, emphasizes that _you_ learn best when _others_ tell you what they know. In return, you offer some of your own knowledge that the other learner is interested in. Whether this leads to a short connection or a long relationship, you're actually _using_ your knowledge to interact.

Contrast this collaboration with schools, which are defined by a certain passivity — you are merely a sponge absorbing knowledge.

Passive learning through things like attending lectures is a great starting point for many, because it offers them a chance to encounter new material and find out exactly what it is that interests them within a specific field.

However, teaching is equally important. In fact, presenting the knowledge you've gained to others is fundamental to successful learning for two reasons: not only do you get to test and enrich your own knowledge, but you will also feel like you've achieved something.

> _"The first thing you have to do is take the auto out of autodidact."_

### 6. Learn your way, and for the right reasons. 

When you were in school, did you learn about what you were interested in, or what someone else decided was important for you? Probably the latter. But what would you learn if you were free to decide?

As an independent learner, you are free to choose your own educational path. However, you should make sure that you are learning things that you actually want to learn — your learning should be driven by curiosity and a thirst for a certain type of knowledge.

As we've seen earlier, learning inspired by intrinsic motivations is far more satisfying and sustaining than learning inspired by external ones. Creative writer Dan Sinker describes intrinsic motivation as a pressure within himself, something that he urgently wants to know, understand or be able to do.

In order to harness that motivation, you'll first have to identify it, as well as the processes and methods that help you learn the best. While this will come at the cost of some frustration, in the end you will discover the learning strategy that works best for you.

Let's say you want to learn a new language. You might want to learn systematically, meticulously following each chapter in a grammar book. Or you might prefer a more chaotic approach, learning from various sources, interacting with people who speak that language, and in no particular order.

However you go about it, follow your interests in your own way. But whichever method you use, be sure to involve real-life context, as this drastically improves learning.

Learning in a real-life context gives the impression that your learning has real consequences, both positive and negative. You will find yourself failing, but with that failure comes greater motivation and personal rewards as well as better retention of what you've learned.

Indie-culture tastemaker Jim Munroe, for example, always wants to learn by doing, which is only possible in a real-world context. He believes that by putting his work in front of an audience he can more thoroughly internalize criticisms, knowledge and skills.

### 7. To be successful finding a job without credentials, you’ll need chutzpah. 

Most people think that, without the right credentials, some jobs are impossibly out of their reach. Lacking a degree, some might not even try to get the job they want. But these assumptions are totally misguided.

If you lack proper credentials, then you'll need to show _chutzpah_, or an insistent, confident attitude, in order to land the job, and you'll have to be prepared to learn.

Even if you don't have the particular knowledge and skills that your employer wants, it's okay to be sure of yourself and your potential, if you believe you can successfully master the knowledge and skills needed.

In order to get across to your potential employer that you can excel at the job, it may be necessary to stretch the truth a little. Author and journalist Quinn Norton shows us how being able to "fake it till you make it" can lead to better opportunities. For example, knowing that she had the skills for the job, she was able to become a high school computer teacher without a high school diploma herself.

While chutzpah is important, evidence that you know what you're doing, such as recommendations, can open doors.

An important resource in finding a job is the _economy of generosity_ — helpfulness is always a two-way street. This will come in handy, because in order to get access to jobs and knowledge about jobs, you'll need help. For instance, you'll need someone to introduce you to whoever's hiring or pass along your portfolio. You might also be able to help others land a job, and hopefully they'll connect you to other opportunities in the future.

ComBots Cup organizer Simone Davalos explains that she was only able to start her robotics business because of her connections with people who did similar work, who could help her find space and funding for her projects. Now she tries to do the same, opening doors for others just as her network helped her to gain a foothold in a new industry.

> _"I tend to do first, then ask questions and learn from my mistakes."_

### 8. Build your own solid professional network. 

People who graduate with MBAs love to tell you that the network you'll build is worth the money you pay for the education. However, you can build a professional network as an independent learner, too.

If you embark on independent learning, then you should make it a goal to get in touch with others who are interested in the same or related topics, as connecting with like-minded people will help you make progress toward your own objectives.

Not only will being able to share your passion keep you motivated, but also communicating with others about your work means that they have a basis for giving you recommendations and hooking you up with their connections later on.

Consider the serial entrepreneur Caterina Rindi, for example. In order to learn more about running a business, she set up an alternative business network based on mutual improvement. In the group, members shared experiences and funding options to support each other and each other's businesses. Everything she knows about business and administration came from her community and friends!

But even experts can be brought into your network without you having formal education. Because most experts are deeply interested in their specialties and enjoy discussing them, you can benefit from their knowledge and information if you can just get them one-on-one.

Getting in touch with professors or other experts isn't difficult, but should be planned. Write them a simple email with questions that they'll find interesting, and not merely the kinds whose answers you can find with a quick internet search. Be sure to show your own engagement and interest in the subject matter, as well.

Remember, too, that professors and experts are probably busy. Your email should contain clearly worded questions, using bullet points, to make their lives as easy as possible.

Software engineer Zack Booth Simpson used this approach, and learned by asking professors to lunch and reading textbooks. As it turns out, the cost of a few lunches is far less than the cost of college tuition!

### 9. Jobs are necessary and invaluable places to learn. 

A job isn't just a place to earn a paycheck — it's also the best place to learn. Not only will you learn from your tasks and from the wisdom of others, but you also gain valuable networking opportunities.

But this is only possible if you remain objective about your work. In other words, don't pretend to be an expert if you're still learning, and know where you can improve. Being objective allows you to give and receive valuable feedback at work, meaning that both you and your co-workers have a lot to gain. In fact, you'll find lots of people are also looking for feedback on their own work, and not just phoney politeness.

And, as exemplified by author and journalist Dan Sinker, even grunt work can prove to be a necessary skill in another field. He couldn't have known that the hours he put into video editing would later translate into a crucial skill when editing his online magazine!

What's more, you'll learn how to find the best sources of information in order to develop your skills in a given field.

If you are starting a job in unfamiliar territory, you'll have to read up on the knowledge and skills necessary for your work. But often at a new job your co-workers are also trying to develop their skills, meaning that you can collaborate on finding new valuable sources of information.

Indeed, the best job is the the one in which you'll learn the most, often the case at smaller companies. Because you aren't just a cog in a giant machine, small companies give you the leeway to try new things, assert some influence and see the results. Learning on the job is best achieved by adopting the attitude of an apprentice: be helpful, ask quality questions frequently and take risks.

> _"Maybe it was the path that I took that made me extraordinary and not the other way around."_

### 10. Final summary 

The key message in this book:

**Going to college or university no longer guarantees future success like it did in the past. Luckily, today's independent learners can get a better education for less money, open up more career opportunities and have more fun in the process if they use the tools at their disposal.**

**Suggested** **further** **reading:** ** _The End of College_** **by** **Kevin Carey**

The End of College (2015) is about the American higher education system. These blinks give a historical overview of how the author sees the development of the American university and its evolution from European models. He evaluates its current status and advocates for the _University of Everywhere_ — a remotely accessible university of the future.
---

### Kio Stark

Kio Stark is a writer and adjunct professor at New York University's graduate Interactive Telecommunications Program. In addition to conducting active research on learning and teaching she has also published a novel entitled _Follow Me Down_.

