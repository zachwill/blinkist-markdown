---
id: 58e20ad3fe685600046c9386
slug: dont-sleep-there-are-snakes-en
published_date: 2017-04-07T00:00:00.000+00:00
author: Daniel Everett
title: Don't Sleep, There Are Snakes
subtitle: Life and Language in the Amazonian Jungle
main_color: 5D9024
text_color: 3B5C17
---

# Don't Sleep, There Are Snakes

_Life and Language in the Amazonian Jungle_

**Daniel Everett**

_Don't Sleep There Are Snakes_ (2008) tell us about the unique culture and language of the Pirahã, an indigenous people of the Amazonian jungle who don't use numbers, have names for colors or bother with small talk. They also laugh and smile more than most other cultures. These blinks explain what languages can tell us about the human experience and, moreover, why we shouldn't forget how many other cultures and languages besides our own exist around the world.

---
### 1. What’s in it for me? Learn how to see and hear the world from an entirely new perspective. 

Can you imagine what it would be like if you had no words to classify the colors you see? What if you didn't have numbers? It would certainly alter the way you interpreted your surroundings.

If you saw six snow-white doves nestling in a dovecot, would you think of them as "six," or even as "snow-white"? This might sound like a frivolous thought experiment, but for the Pirahã, a people in the Amazonian jungle, this is simply how things are.

The author, Daniel Everett, spent many years living with the Pirahã, isolated in the depths of the Amazonian jungle. This story isn't a patronizing look at how these people are different from our Western norms; rather, it's a fascinating account of their language, and how it is vastly different from anything we might be accustomed to. We will also see how their language shapes, and is shaped by, their perspectives on the world around them.

In these blinks, you'll learn

  * why the phrase "don't sleep, there are snakes" is indicative of the Pirahãn mentality;

  * how the Pirahã use word endings to indicate evidence behind their statements; and

  * why endangered languages like the Pirahã language should be preserved and studied.

### 2. The Pirahã live in the Amazonian jungle and speak a language unrelated to any other extant language. 

We use language to conceptualize the world as we see it. Imagine not having a word for "worry" because no such notion exists. This is true of the Pirahã (pronounced pee-da-HAN). It's not that their lives aren't without hardships, they just conceive of the world differently.

The Pirahã are hunter-gatherers who live along the Maici River in the Amazonian jungle of northwestern Brazil. The Pirahã language is the last remaining dialect of the Mura language group.

The Pirahã are monolingual and probably moved to the area after their language developed. We're confident of this because they use Portuguese loanwords, probably picked up from other tribes, to describe some of the native species of monkeys.

It's not an easy life in the jungle — the Pirahã are a tough lot. But they also happen to be among the happiest people you're likely to meet.

Anthropologists who have spent time with them were asked to assess them in terms of how much time they spent laughing and smiling compared to other cultures they had encountered. The carefree Pirahã reliably came out on top.

The worldview of the Pirahã is perhaps best summarized by how they wish each other "good night."

They have several ways to say this and one of them is "don't sleep, there are snakes." While this is quite a peculiar way of wishing someone sweet dreams, it's also quite matter-of-fact.

First of all, poisonous snakes do slither about in the Amazonian jungle, and the Pirahã's bedtime advice is their way of saying that if you don't keep your wits about you, you won't last long. Second, it shows us how they treat sleep; they may not sleep the whole night through, but they certainly don't lie awake paralyzed by fear, either.

Late-night conversations can be heard in the open huts, and the night air is regularly punctuated by laughter — a sign of the Pirahãn way of life.

### 3. The Pirahã don’t use numbers and have no form of counting as we know it. 

While living with the Pirahã, the author, Daniel Everett, received his field supplies every eight weeks, but the Pirahã would constantly ask him when the supply plane would arrive. While this initially confused him, he eventually understood: they were astounded by his ability to know the exact day the plane would arrive.

This fascination was due to the fact that the Pirahã have no system for counting. Instead of numbers, they use comparative terms such as "bigger" or "smaller." To them, two fish are "bigger" than one fish, just as one large fish is "bigger" than a minnow.

What's more, they don't represent numbers using physical signs either. At one point, Everett held out two fingers to indicate the plane would arrive in two days — but this meant nothing to them.

However, the Pirahã wanted to be able to handle money and ensure that they wouldn't be fleeced by traders on the Macai River, so Everett set about teaching them numeracy. But they just couldn't grasp the concept, even after months of evening classes. Not one of them learned to add 1 + 3 or even 1 + 1.

Everett conducted experiments with psychologists and was subsequently able to demonstrate that this was simply not how the Pirahã envisaged the world.

And it wasn't only with numbers that he found the Pirahã were different; they also lack names for colors. This isn't to say that they are colorblind, they just don't categorize the color spectrum into more subtle or specific colors such as teal, tan or taupe.

We might think it natural to be able to distinguish and name the tones on our paint swatches — but it isn't necessarily. Just as with numbers, the Pirahã conceive of colors in relative terms. That is, something can be "darker" or "like blood."

### 4. The Pirahã Language is as distinctive as those who speak it. 

The Pirahã haven't had much in the way of contact with other cultures. As a result, they value first-hand experience and knowledge above all else. This may be why missionary Christians have had such a tough time trying to bring the gospel to them!

Their matter-of-fact engagement with the world is echoed in the fundamentals of the Pirahã language. Let's consider their use of suffixes, the particles added to the end of a word to modify its meaning. Take the English suffix "-ful." This creates an adjective from a noun and gives the quality of "fullness" to it, as with "beautiful" or "colorful."

The Pirahã use suffixes differently. Suffixes denote how much evidence the speaker has to support what she is saying. Linguists calls these sorts of suffixes _evidentials_, and they are a highly productive way of speaking. A Pirahãn evidential, of which there are three, can convey information for which an entire English sentence might be needed.

Let's consider how the Pirahã express "Your boat has a hole in it."

The first type of suffix denotes hearsay: "I know it because I was told it." The second indicates observation: "I can see fish swimming in it." And the third, deduction: "I can see your boat sinking, so it must have a hole in it."

Furthermore, the Pirahã language also lacks _phatic communication_, which is linguistic jargon for small talk.

"Hello," "how are you?" and "my pleasure" are examples of phatic communication. They don't give us any new information but they do pragmatically reinforce social mores.

The Pirahã don't go for this way of speaking. Instead of expressing thanks, they tend to reciprocate the kindness at a later date.

The Pirahã also speak much more directly. Questions are direct, such as, "Where is the firewood?" Declarations are equally categorical, for example, "It's down by the river." And commands are just that: "Go get the wood and bring it here."

### 5. The environment in which we grow up affects the way we see the world. 

During one particular journey down the Amazon River, Everett steered his motorboat out of the way of a floating log, which can be dangerous for small boats. What he hadn't expected, and simply hadn't conceived of, was that the log could actually be a giant anaconda. It emerged from the water and nearly landed right in his boat.

Similarly, the Pirahã have trouble comprehending previously unencountered phenomena. To them, for example, two-dimensional images are meaningless.

At one point, MIT researchers showed photographs to the Pirahã. Although they could recognize the images, they struggled as soon as quality of the photographs degraded. This even happened when the degraded photographs were placed beside the originals.

Over time, the Pirahã became more successful in recognizing images as they saw more of them, but it was nonetheless clear that they struggled to perceive in two dimensions.

On another occasion, during a trip to a town, Everett had difficulty crossing a road with the three villagers accompanying him. They couldn't discern the oncoming vehicles' speed — they simply weren't used to encountering cars.

Conversely, Everett struggled to perceive risk in the jungle. Another time, he almost bumbled into a scaly caiman waiting in the night. Luckily his Pirahãn companion stopped him in time. He had seen its two red eyes, unblinking and glimmering in the distance, well before the encounter.

The fact of the matter is that survival is not a given in the jungle; you have to work for it. Consequently, the Pirahã treat their children as adults. They certainly don't engage in any baby talk, or what linguists call _motherese_. Rather, the Pirahã address children just as they would adults. To Everett, the conclusion was obvious: the Pirahã see everyone, adult or child, as equals.

It's thus clear that humans use language to reflect an image of ourselves and articulate our conception of environments with which we're familiar.

### 6. Half of the world’s languages may be extinct by the end of the twenty-first century. 

Have you ever wondered just how many languages there are in the world? Well, while there are a jaw-dropping 6,500 in total, about half of them are under threat and could potentially disappear within the next 50 to 100 years.

There are two ways language extinction occurs. First, languages can die out if the people who speak them are themselves at risk. This can happen through war or famine, or if foreigners start to threaten local ways of life.

This is exactly what happened to the Pirahã. Neighboring cultures boxed them in, and there are now only about 400 native Pirahã language speakers remaining.

Language extinction is also caused by market forces, specifically when individuals are incentivized to speak a second, more culturally dominant language for economic reasons. In Brazil, for example, a native speaker of an indigenous language could learn Portuguese, which would allow him to trade or buy things like a motorboat or gasoline.

Native speakers of less widely spoken languages do this because they must. If you hope to maneuver in another, more dominant, culture, you have to learn the language — even if you might lose your own in doing so.

Thankfully, this second factor isn't such a danger for the Pirahã. They are quite happy remaining isolated and, although they occasionally trade, they're generally indifferent to outside material goods.

When thinking about languages, we also have to consider that they are sources of unique cultural knowledge. And once they're gone, they're gone.

Languages are much more than their composite grammatical structures or sounds; they can articulate novel ways of conceiving of one's environment, or express cultural knowledge that existed for millennia separately from more dominant cultures.

As the world's population grows ever bigger, the livelihoods and languages of many remote peoples are under increasing threat. Wherever possible, we must protect these communities from forced assimilation — otherwise, their cultures and languages will cease to exist.

### 7. Final summary 

The key message in this book:

**The environments in which we live determine how we see the world, interpret our surroundings and interact with one another. Languages articulate these interactions, but are also shaped by them, making every culture and language unique. When a language or dialect dies out, we don't just lose data for linguists to sift through, we also lose valuable human knowledge and unique cultural perspectives.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Through the Language Glass_** **by Guy Deutscher**

_Through the Language Glass_ (2010) explores the many ways in which language both reflects and influences our culture. By exploring the different ways that languages deal with space, gender and color, the book demonstrates just how fundamentally the language you speak alters your perception of the world.
---

### Daniel Everett

Daniel Everett is an American linguist and author who spent four decades living and working among the Pirahã. He is the Dean of Arts and Sciences at Bentley University in Massachusetts.

