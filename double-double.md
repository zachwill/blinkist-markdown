---
id: 5559ff8c3834360007010000
slug: double-double-en
published_date: 2015-05-18T00:00:00.000+00:00
author: Cameron Herold
title: Double Double
subtitle: How to Double Your Revenue and Profit in 3 Years or Less
main_color: D02A31
text_color: 9E2025
---

# Double Double

_How to Double Your Revenue and Profit in 3 Years or Less_

**Cameron Herold**

_Double Double_ (2011) gives you the keys to unlock powerful growth in your business. These blinks will coach you, from effectively defining your company's vision to boosting employee performance to ensuring your resilience as a leader.

---
### 1. What’s in it for me? Grow a company faster than you thought was possible. 

Nowadays, it feels like products come and go faster than the produce at the supermarket. But that is definitely a good thing, because it means that, though you're just a nice person with some good ideas today, you might become a serial entrepreneur tomorrow.

Cameron Herold knows a thing or two about that feeling. As COO of 1800-GOT-JUNK, he managed to transform a junk-removal company into a business juggernaut in just a couple years.

These blinks explain how you can do something similar. It starts when you have a vision and know how to carry out that vision. Then you need to take care of yourself, know how to communicate with your employees and how to use today's technology to save a bunch of money.

In these blinks, you'll learn

  * why you shouldn't try to visualize numbers;

  * how to build a company culture like Google's; and

  * where on the roller coaster your company is right now.

### 2. In order to set your company goals, you’ve got to visualize them first. 

Could you run 5,280 feet? No? But can you see yourself running four times around a quarter-mile track? If you haven't done the math, that'd be the exact same distance. So why does the latter sound so much easier? Because it's easier to achieve _visualized goals_ than those expressed in numbers.

Just like running track, running a business is easier when you swap abstract goals for concrete, exciting visions. "Conceive, believe, and achieve" is a mantra that guided the author as he turned his $2 million annual revenue into $106 million over the course of a mere six years.

This technique of first conceiving or visualizing can apply to any kind of goal. Say you're a basketball player: instead of telling yourself to score 20 baskets, visualize the act of shooting and scoring instead.

Creating visualizable goals makes us more likely to set realistic expectations for ourselves or for our company. This is vital, because setting abstract goals for our company's future can do more harm than good.

Let's return to our runner example to see why.

Imagine setting yourself the goal of running 10km in 30 minutes, even though, normally, you'd need an hour. By aiming straight for that high goal, you'd increase the risk of injury. The same goes for businesses that set very high goals at the outset. After achieving it, they often realize they've harmed the business in the process. Companies that expand too quickly often end up making an inferior product.

But let's say you considered your running goal by first _visualizing_ how you'd work up to 10km in 30 minutes. You'd realize that you have to gradually build your speed from 4.5 minutes/km to 3 minutes/km. That's a reasonable and achievable goal!

So before setting growth goals for your company, ask yourself first: Can I visualize these goals? If you can, then it's time to figure out how you'll achieve them.

### 3. Make your vision achievable by reverse engineering it. 

What do you do when you've got too much on your plate? Most of us make a to-do list or work out how to break our errands down into smaller tasks.

This simple process of taking a larger goal and working backward to break it into smaller goals is called _reverse engineering._ And it's vital for making your company's vision achievable _._

Think of reverse engineering like home renovation. After deciding how you want your new home to look, everyone, from tilers to painters to carpenters, should know exactly what they need to do. Be the architect of your company's vision, and figure out what tasks are required to build it.

If you started out with goals like "Increase sales!" or "Increase profit!" or "Our company must grow!", you'll know now that these goals should be broken down. So let's reverse engineer!

Take this goal: "Increase profit!" We can't achieve this goal directly, so let's look at the underlying goals that comprise it. One goal could be increasing the number of new customers. But another, perhaps more important target, could be determining whether the demand you need is feasible, and to what extent your product is needed.

It doesn't take long to break a large goal down into smaller and more effective targets! So now that you know what your vision looks like after reverse engineering, what's next?

It's time to investigate how to take action. Read on to find out more!

### 4. Cut through conflict in your company with open communication. 

We all know that most conflicts come from simple misunderstandings. Yet, whether it's with our spouse or our coworkers, we still struggle to communicate clearly. If you really want to eliminate conflict, then it's time to build better communication practices.

Good communication should flow top-down, bottom-up, between peers and across departments. One way to achieve this multi-directional flow is through an open-door policy for employees and leaders, according to which everyone is welcome to pop into anyone's office to discuss issues.

Open communication will lead to improved performance because individuals will be able to seek assistance as soon as problems arise, and leaders will have a better grasp on what employees find difficult or frustrating. What's more, open communication can also boost the circulation of good ideas.

Take the author's practice of a "daily huddle" at his business. This allowed employees to share good news, stay updated on key data and hear about goings-on within the company. Often, employees would offer helpful solutions to problems faced by coworkers in different departments — something that simply didn't happen before open communication was introduced!

Open-door policies allow everyone to talk about everything, and that's great. But such a policy won't be half as effective if nobody is listening! The truth is that we're often so desperate to be heard that we simply don't listen. Try to start listening twice as much as you talk, and you'll realize how much you've been missing out on!

Take the phrase "I didn't say you were beautiful" — depending on which words are emphasized in the phrase, the meaning conveyed varies dramatically. Now consider the multiplicity of meaning that a whole speech might hold! All communication has its nuances, and these make a world of difference. If you notice that your business is struggling, communication should be the first thing you address.

> _"God gave you two ears and one mouth; use them in that ratio."_

### 5. Make the most of new technology to boost your company performance. 

Good communication is one of the best ways to boost your business. Taking advantage of innovative technology is another! Smart use of digital tools can leverage the productivity of your employees and even allow your business to lower its operation costs.

One way to up your technology-game is by switching from desktop to laptop computers. Not only do laptops cost the same or less, your employees can use them to work from anywhere at any time. For example, instead of wasting time retyping handwritten notes from meetings, your employees can bring their laptops to meetings themselves and have those notes ready straight away. The author even found that once his employees had laptops, they were more likely to take them home to finish up extra work each week.

Of course, new technology always entails training time and extra costs. If you want to make the most of your innovative options with minimum stress, follow this rule: Any software your company uses should be as simple as possible to meet your needs. For example, there's no need to buy Photoshop if all you have to do is crop and shrink pictures — a job that Microsoft Paint could do equally well!

But what if there's a task that requires a more advanced solution? If you don't have the expertise to do it well, or the time to learn how, why not outsource your project?

Outsourcing Things Done is a company that hires, trains and manages executive assistants whom you can assign tasks to and communicate with. Crowdspring and 99designs are other great outsourcing companies that offer high-quality marketing work, done cheaply and quickly.

After posting a project description and budget, people from around the world submit designs, hoping to be chosen. Both of these are great services for getting items like brochures or flyers designed, and they're often a lot cheaper, too!

### 6. Strong business growth requires a strong business culture. 

Considering Google's success, their corporate motto must be amazing, right? Got a guess what it might be? They came up with it 14 years ago: "Don't be evil." This mantra is the basis of a strong company culture that makes Google one of the most attractive employers worldwide. So how can you start implementing your own positive company culture?

Think about your office. Is it a place people are excited to visit each day? If not, it's time to make some changes. Walls between staff are barriers to teamwork: they downgrade communication, give rise to office politics and diminish team spirit. Super-traditional boardrooms and bland photos on the walls kill creativity. So, do away with traditional private offices and strive for a work environment that's both open and unconventional.

Rethink, one of the top ad agencies in North America, has a Ping-Pong table for their boardroom, and Google is known for their unconventional meeting settings, too. Their Zurich office has a ski gondola, while the Dublin office features a pub-like meeting room.

In comparison to Google's company culture, Microsoft's is often considered boring — a bunch of hunched employees cranking out code in dark private offices. This difference in reputation speaks to the importance of space in company culture. Think about it: If you were a hotshot developer, you'd much prefer working at Google than Microsoft!

In addition to attracting employees, a strong business culture creates a buffer during high-stress periods. So what kind of culture is the strongest under pressure?

In an entrepreneurial culture, the company treats employees as co-founders. Allow your employees to get to know your company inside-out by sharing financial statements and information about decision-making. This boosts employee responsibility and accountability, encouraging them to go about their tasks with pride and remain on board in times of trouble.

But in times of trouble, your company won't just need loyal employees. As a leader, you'll also need to demonstrate your commitment to the endeavor, and that isn't always easy!

> _"Once a company decides to actively design and build its own signature culture, the process required to grow it is sort of like chasing the horizon."_

### 7. Entrepreneurship is a rollercoaster – so learn how to handle it! 

Running a business is much like riding a roller coaster: you can hold on for dear life and scream, or you can throw your hands in the air and have fun.

The ability to enjoy the ride comes when you get used to the roller coaster's different stages. So what are they?

The first stage is that of uninformed optimism. You've just strapped yourself in and you're on the way up, with your adrenaline pumping as you near the top.

At this point, you might have unrealistic expectations for your business and quite possibly some manic energy.

With no idea about what's in store for you, you're vulnerable to anything you think might help the business grow, from knick-knacks like company logo pens to expensive consultants. By controlling your excitement, you'll save money and valuable time.

The next stage on the roller coaster is when it all gets real. You're flying down the track and it's a lot scarier than you thought it would be!

As a leader of a business at this stage, you may experience fear, nervousness and frustration. Sure, you may be feeling a lot more critical and pessimistic, but, now that you know what running a business is really like, don't let yourself get too discouraged about your endeavor.

Instead, take advantage of your newfound knowledge and critical abilities by applying them to some conscientious planning, budgeting and decision-making. You're less excitable now, and will be able to make better choices, but don't forget to enjoy the ride!

We all go through times when the rush of the ups and downs seems to have been replaced by stagnation. Everyone is susceptible to procrastination, even entrepreneurs. Learn how to beat it in the blink that follows.

> _"Stop complaining and stop making excuses. As an entrepreneur, you should realize that the unknown will always be a part of your life."_

### 8. A leader must have focus if he wants to be productive. 

Do you struggle to stay on task? Here's the good news: You're not alone. The bad news is that if you don't improve, you'll put your business at risk. Time for some good news again: There are things you can do to increase productivity!

First, it's time to put all delusions about your abilities to multitask behind you. Doing a lot of things at once is really just another way of not doing much work on anything at all. Multitasking is procrastination in disguise, so don't be fooled! Instead, seek out a focused approach to lift your productivity and quality of work.

In fact, the ideal leader may even be a "monomaniac with a mission," as Tom Peters argues in his book, _In Search of Excellence_. As a monomaniac, the leader only focuses on his most important project, and isn't afraid to say no to others. In this way, all his effort is directed where it's needed most, and the project itself can be completed to a higher standard.

Although you might think you're being incredibly effective when taking conference calls while going through your emails, you'll most likely miss important points on the call, or make mistakes in your emails, which means more work for you later on! Try to block out time to handle your emails in one go, or limit conference calls to 30 minutes so you can concentrate on them completely.

With this new approach, you'll spend less time doing the same amount of work, or even complete more work in less time. However, this doesn't mean that you should work non-stop! There's no greater obstacle to productivity than an unbalanced lifestyle, as we'll learn in the final blink.

### 9. Become a better leader by putting your wellbeing first. 

Ever felt so overwhelmed with work and home issues that you couldn't muster up the strength to better either situation? Too often, in striving to achieve their personal goals, entrepreneurs forget their personal priorities. But they're doing themselves no favors, because overwork erodes the quality of their own leadership and can even drive them to a burnout.

Studies have shown that many successful entrepreneurs struggle with bipolar disorder. In fact, it's manic depression has even been nicknamed "the CEO disease." But when was the last time you heard a CEO go public — mysterious complaints on Twitter don't count — with their struggle for mental health?

Instead of ignoring their internal life, a good entrepreneur takes care of his company by taking care of himself. So how can you ensure that you're dividing your time between your work and your personal life effectively? You've got to work hard, and play hard.

Why not split your day into "work" and "fun"? You can use the "fun" part of your day to lift your mood after work, or even look forward to it throughout the day to boost your motivation. If you use time outside of work to recharge, you'll perform far better when you're back in the office.

### 10. Final summary 

The key message in this book:

**If you want your company to grow faster, you'll first need to define your vision and outline how you can realistically achieve it step-by-step. Then, boost your employee performance by opening up communication and taking advantage of new technology. Finally, keep your company on track by learning to handle the ups and downs that you'll experience as a leader.**

Actionable advice:

**Good conflict management: Stay impersonal and keep things cool.**

Next time an employee brings up an issue, remember: you're confronting the conflict, not the person. By keeping things impersonal, you'll be better able to take an empathic approach, and consider the things you could have done better. In this way, you'll be able to put the problem behind you much faster, and work together with your employees to uncover genuinely effective solutions.

**Suggested** **further** **reading:** ** _Simple Numbers, Straight Talk, Big Profits!_** **by Greg Crabtree and Beverly Blair Harzog**

_Simple Numbers, Straight Talk, Big Profits_ (2011) outlines the essential, interconnected elements you need to know that affect your company's longevity and growth. Through a series of simple steps, you can create a more productive workplace to ultimately boost performance and build greater wealth.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Cameron Herold

Cameron Herold is an entrepreneur and business coach. In addition to having built three multimillion-dollar companies, Herold is an active advocate of entrepreneurship education.

