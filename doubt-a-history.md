---
id: 55d1dcc7688f670009000119
slug: doubt-a-history-en
published_date: 2015-08-21T00:00:00.000+00:00
author: Jennifer Michael Hecht
title: Doubt: A History
subtitle: The Great Doubters and Their Legacy of Innovation from Socrates and Jesus to Thomas Jefferson and Emily Dickinson
main_color: DA2E30
text_color: A62325
---

# Doubt: A History

_The Great Doubters and Their Legacy of Innovation from Socrates and Jesus to Thomas Jefferson and Emily Dickinson_

**Jennifer Michael Hecht**

_Doubt: A History_ (2004) is a journey through one of time's best kept secrets: the people who have stood up to accepted truths, even when it cost them their lives. These blinks share the stories of doubters with conviction from Ancient Greece and India to the modern era, and how they have shaped the way we live and think today.

---
### 1. What’s in it for me? Discover how doubt is an unknown, potent force in history. 

What connects ancient philosophers like Socrates and Confucius with modern scientists like Einstein? They're great thinkers, of course, but there is something more, something not mentioned as often as their original ideas and concepts. That something is doubt.

Throughout history, doubt and doubters have played a crucial role in the development of what we now know as the modern world. This doubt has been the spark for scientific innovation, a challenge to entrenched authorities, and the foundation for new religions. It's been the source of despair and reassuring thought alike.

So how come we don't hear about it? These blinks investigate what most history books ignore, and show us how doubt is about more than modern scepticism.

In these blinks, you'll discover

  * how fellow doubters helped Moses Mendelssohn in the eighteenth century;

  * why Giordano Bruno's doubts led to his death; and

  * how Muslim thinker Yaqub ibn Ishaq al-Kindi preserved Greek doubt for the modern world.

### 2. Doubt, an influential force throughout time, has often been omitted from the history books. 

Do you remember sitting in history class and learning about all the incredible people, full of creative ideas, who stuck to their convictions and changed the world? Well, while they're certainly captivating, the stories history tells us tend to omit one thing: _doubt._

Why?

Because the arguments and even the existence of doubters have often been wiped from the slates of history by states and religions, probably because doubters have tended to be fringe critics of just those institutions. Naturally, a state or religion wouldn't want to give much credence to these naysayers and would avoid acknowledging them whenever possible. 

Take Judaism. In 200 BCE many Jewish communities replaced their traditions with customs from Greek culture, including its language, communal exercise, and even aspects of Greek religion. 

The result?

Some Jewish authorities viewed these communities as a threat and destroyed them. Now they're barely referred to in Judaism's official religious texts.

But even when history does mention doubters it tends to essentialize them as individuals with specific ideas. This results in a further eclipsing of the greater histories of doubt and the connections between them.

In fact, it's even possible for a trained historian like the author to miss the signs of doubt. The history of doubt was always in her peripheral vision as she worked on other projects, and it took time for her to focus on it. It wasn't until she saw the sheer number of doubters through history and how their ideas influenced one another that she got the idea to write a book on the subject.

And it's lucky for you that she did, because the history of doubt encompasses both highly abstract and unknown thinkers as well as some of the most brilliant minds ever to exist. But how far back does this history go?

### 3. Doubt is an ancient and universal practice. 

So, it's clear that doubt has a greater place in history than we may have been led to believe, but how old is it? Is doubt specific to one region or is it a global phenomenon?

Actually, since the dawn of time there have been doubters. In fact, the history of doubt can be traced from Ancient Greece through the Roman empire to the Middle Ages, the philosophers of the nineteenth and twentieth centuries and right up to our own modern era. 

Who does the running list of doubters include?

Such brilliant minds as the Greek philosophers Socrates and Aristotle, Cicero and Emperor Marcus Aurelius of Rome, the French philosopher Descartes, the Jewish Enlightenment thinker Moses Mendelssohn and scientists like Charles Darwin and Albert Einstein. 

But it also includes another name you might not expect: Benjamin Franklin. That's right, one of America's founding fathers was doubtful of many things, and especially religion, from an early age.

But doubters aren't confined to Europe and America. You can find them all over the world. In addition to existing in the ancient Western empires of Greece and Rome, there have been doubters in the East throughout history. 

Take Confucius, who lived in China around 500 BCE. He doubted a variety of things, among them the custom of making sacrifices to dead ancestors. His perspective lives on as his teachings influence the culture and philosophy of not just Asia, but the world to this day. 

Just as doubt has no geographical borders, it's not specific to men either. In fact, there were many influential female doubters in history. For a long time female doubters had a much rougher time than their male counterparts. Luckily for them, and us, many of them prevailed to be known today. 

For instance, Marie Curie, a famous scientist and doubter, studied atoms, a concept first introduced by Greek philosophers. The research she conducted was instrumental to science's current understanding of atomic theory and won her a Nobel Prize in 1903.

### 4. Doubters have often been persecuted and oppressed. 

But just because doubters have existed across the world and throughout history doesn't mean they've had it easy. Their different perspectives often caused trouble. 

That's because doubt tends to challenge the authorities and ruling classes. Throughout history there are examples of these groups responding with violence and oppression to doubters who threatened their hold on power.

This goes for both religious leaders who will settle for nothing less than total belief in their doctrines, as well as politicians who demand approval of their decisions and the legitimacy of their rule. As a result, such leaders employed oppressive tendencies that were bound to be challenged by doubtful minds. 

The Inquisition is a particularly upsetting example. The Inquisition were the ideological police of the Catholic Church in the fifteenth and sixteenth centuries. They relentlessly attacked anyone who questioned the church or its teachings. 

And whom did they attack?

The victims of the Inquisition included astronomers who doubted that the Earth was at the center of the solar system. One Italian priest and astronomer named Giordano Bruno was found guilty of atheism for just this reason in 1600, and burned alive. 

But it wasn't just doubters themselves that burned for challenging authority. It was commonplace for political and religious leaders to ban and even destroy texts written by doubters — a measure intended to curb the spread of doubtful doctrine and therefore challenges to their authority. 

As a result, it's exceedingly difficult to reconstruct the thoughts of historical doubters today — a major reason why they've been forgotten by history. In seventh century BCE India, the Carvaka philosophical movement threw official doctrines, including belief in the afterlife, into doubt. 

The fate of Carvaka today?

Their only surviving texts exist as citations in official responses or disapprovals by their enemies. All the rest has been destroyed.

### 5. Many of the attacks on religion came from great doubters. 

What's the opposite of doubt? It's belief, of course. But what happens when these contradictory forces meet?

Religion and the doubters who have thrown it into question throughout history are a prime example. That's because all religions believe in something, whether it's the existence of a higher power or an afterlife, verifiable or not. For this reason, religions make a natural target for doubters. 

But just like doubt, belief is an age-old concept. In Ancient Greece, religion and belief in a greater being with influence over human lives were cornerstones of cultural and social life. Yet even before Ancient Greece there are plenty of examples of _earlier_ belief systems.

Since both doubt and belief have roots deep in history, there are plenty of accounts of doubters standing up to the authority of religion and the church. 

How?

Usually by uncovering logical fallacies in the arguments of priests or proposing alternative explanations for the occurrence of natural phenomena like thunder. Some Greek thinkers like Xenophanes called into question the stories that explained how the gods came to be. They suggested, for example, that the gods were simply great heroes of times past, normal humans who'd become godlike in the eyes of society when supernatural powers were attributed to them. 

But that doesn't mean all doubters were anti-religious. Some had faith but questioned certain aspects of it. In fact, some religious doubters only resisted _because_ of their deep involvement in religion. 

Martin Luther threw into question the teachings of the Catholic Church — most famously in 1517 when he nailed a list of his 95 theses to the door of a church. However, Luther's criticism was not an attempt to bring down the church but rather to help it overcome what he saw as its flaws. As a result, many of his intentions came to fruition with the founding of protestantism.

### 6. Religions have sometimes incorporated doubt in order to address it. 

So religions and doubters have often been at odds with each other. But how did religious institutions handle this fierce opposition? The classic adage applies, if you can't beat them, join them. And if that doesn't work, make them join you. Religions accomplished this by incorporating doubt, a universally held emotion, into their teachings. 

Because everyone, even the most devout believer, is doubtful sometimes and religion couldn't very well ignore this fact. 

When Jesus Christ was resurrected after his crucifixion, the Bible says that an apostle named Thomas questioned whether or not Jesus had truly risen. He said he would only believe it when he could stand before Jesus and feel his wounds. 

But the Bible has even more stories of doubt. In fact, one of the best examples of doubt in religion comes from the Book of Job in the Old Testament. 

What's that?

The story of Job, which appears in all Judeo-Christian faiths, goes like this: Job is a happy man with a big family, an admirable house and many other worldly possessions. In addition to being well off, Job is also a pious man who participates in religious services and regularly gives to charity. 

But one day God tests Job by taking away all the things that bring Job happiness. It's not until Job loses his family and everything he owns that he stands up to God and asks whether he is truly just and merciful. So God appears and answers Job's doubtful question. 

The fact that such a great doubter has a book of the Bible devoted to him is a testament to the important function that doubt, and the struggle with it, serves in religious doctrines. Job's story stands in for all the people who might question an omnipotent God who allows evil and injustice in the world.

### 7. In rare cases, doubt has even been the basis of entirely new religions. 

So, you know how doubt and religion fought and how they sometimes collaborated, but in certain cases doubt over old doctrines actually formed _new_ religions. 

That's because doubt doesn't have to be destructive. Some doubters have pointed out the flaws of a religion and used it as a springboard to form a new one they preferred. Take Siddhartha Gautama, better known as the Buddha, who challenged Hinduism and in the process formed a completely new faith. 

Buddha doubted that starvation helped people to reach enlightenment, although it was a technique common among Hindu monks. Instead, he proposed finding a comfortable middle ground between gluttony and starvation. 

But even if doubt doesn't form a new religion, it still serves the crucial function of renewing the church's relevance. Because while it's important to hold on to certain traditions, all churches need to be malleable to avoid obsolescence. That's why doubters like Martin Luther, who point out things in need of change, can be helpful to a church. 

In fact, doubt can actually serve as a foundation for religious belief. That's because doubt and religion are not entirely at odds, and some doubters can examine religions with a critical eye while still maintaining their faith. 

Many Greek philosophical texts, like those of the doubter Aristotle, only survived because of Muslim thinkers like Yaqub ibn Ishaq al-Kindi, who used them in religious arguments in 800 AD. A fact that required him to have them translated and copied. 

So doubt and religion have had a long and interesting history, but doubt has also had a long partnership with another major institution.

### 8. Doubt and science have always gone hand in hand. 

While the history of doubt and religion is mostly one of difference, there is another field that seems to naturally pair with a doubting mind: science. In fact, doubt is, in some respects, the origin of science. 

How?

Early on, doubters revolted against blind belief and worked to develop different methods for thought and debate. They began critically interrogating everything they could and would only accept information derived from empirical data and rational arguments. 

For instance, the pre-Socratic Greek philosopher Thales, known as the first Western philosopher, began by doubting traditional explanations and went on to develop his own scientific methods. As a result, he successfully predicted a solar eclipse as early as 585 BCE.

But Thales was just the beginning. Many great doubters were scientists. Take Charles Darwin, a stellar example of the natural affinity between doubt and science. Darwin was extremely doubtful and readily discarded any theory he failed to find convincing. As a result of his open mind he began to see that the observations he recorded during his travels contradicted traditional theories of inheritance. From there he used precise scientific observation to develop his theory of evolution. 

Although his theory was initially questioned, it started to gain traction and today it forms the leading scientific understanding of the development and interrelation of different species. 

So, doubt has led to many positive discoveries for humankind, but it's not without its down sides.

### 9. Doubt can make people desperate. 

It wouldn't be fair to sing the praises of doubt and all the positive effects it has had without mentioning its negative consequences: doubt can lead to desperation.

How?

By leaving a doubter unsure and depressed. Because if you doubt everything you can start to feel like nothing is real and that it's impossible to know anything for certain. As a result, depression can set in. 

You've seen how doubters challenged early explanations for natural phenomena, like debunking the idea that thunder and lightning are caused by angry gods. While this strategy is effective at producing more accurate knowledge in the long term, its short-term effect is to throw everything into question, meaning people don't know what to believe or how to make sense of things. 

This is why many Greek philosophers supported religious rituals and feasts while doubting the existence and nature of the gods. 

Why?

Because they knew these rituals served the essential function of comforting people and giving them a common identity. 

Another example of how absolute doubt can increase knowledge while leaving people confused is the philosophical work of René Descartes. His _Meditation on First Philosophy_ begins by doubting everything from the reality of physical objects, to the reliability of his own sense perception, to the existence of God. He concludes that everything he thought he knew could just as easily be a trick concocted by an evil spirit who constantly deceives him. 

As a result, Descartes himself confessed that it was impossible for him to let go of his doubts, leaving him feeling like he was lost in deep water. 

But doubt wouldn't have such a long history if it only had negative consequences for human consciousness. So what has it accomplished for humanity?

### 10. Doubt has plenty of potential to positively influence your life. 

Being doubtful isn't necessarily negative. There are a number of positive things and feelings that doubt can bring us. Let's take another look at Descartes' philosophical musings.

His apparently depressing and futile exercise actually turned out to have positive results. 

How?

Descartes realized that his doubt at least confirmed one thing: he existed. Because if he didn't, he wouldn't have been able to doubt all the things he did. 

But there are other upsides to doubt. Doubters have a positive influence on each other. Many of them have claimed that the teachings of other like-minded individuals helped them lead happy, productive lives. Take the work of Greek philosophers like Socrates, which profoundly influenced the thinking of Descartes. What's more, by examining their theories and discussing them with others, Descartes in turn influenced other great doubters like the Dutch philosopher Baruch de Spinoza.

Another great example of doubters helping each other is the life of Jewish philosopher Moses Mendelssohn, who lived in Germany around 1750. Despite the rampant anti-semitism of that time and place, Mendelssohn managed to survive and enjoy relative freedom thanks to the many doubters and philosophers who aided him. One philosopher friend of Mendelssohn's, named the Marquis d'Argent, convinced King Frederick to sign a letter protecting Mendelssohn.

But even when doubt throws everything into confusion, sometimes we can just accept the fact that there's nothing we can do about certain things. For example, there have been many doubters who thought about the implications and importance of death. 

Their conclusions?

Many decided that death was not something to worry about because it's impossible to know what comes after it. 

Socrates gave a famous speech about death's consequences. In it he argued that whether or not there was an afterlife we would never know for certain and therefore it doesn't affect us. By accepting uncertainty in this way, Socrates met his own death calmly.

> _"Plato offers the amazing idea that contemplation of the way things really are is, in itself, a purifying process that can bring human beings into the only divinity there is."_

### 11. Final summary 

The key message in this book:

**The ancient and universal emotion of doubt has been by and large cut from history's pages. But this hidden history, formed by chains of doubters throughout time, has many positive effects for humanity.**

Actionable advice:

**Don't take every fact at face value — instead maintain an impulse for doubt.**

When you were a kid did your parents ever tell you not to believe everything you see on TV? Well the same goes for all information, regardless of the authority that disseminates it. In order to form your own opinions it's essential to allow doubt and skepticism a place at the table. Just remember not to fall into despair when nothing seems to add up, because you're not alone.

**Suggested** **further** **reading:** ** _Breakfast with Socrates_** **by Robert Rowland Smith**

_Breakfast with Socrates_ whips you through a normal day with commentary from history's most venerated thinkers, explaining exactly how their major contributions to philosophy, psychology, sociology and theology impact your daily routine: wake up with Descartes, brace yourself for a world of Freudian conflict, and when you go to work, either submit to Marx's wage slavery or embrace Weber's work ethic. Argue with French feminists and then slip into a warm bath, bubbling in Buddha's heightened consciousness. Finally, end the day by drifting away into Jung's collective unconscious.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jennifer Michael Hecht

Jennifer Michael Hecht is an American teacher, writer and philosopher. Her other well-known books include _The Happiness Myth: Why What We Think Is Right Is Wrong_ and _Stay: A History of Suicide and the Philosophies Against It._ In addition to non-fiction, she is a prolific writer and publisher of poetry.

