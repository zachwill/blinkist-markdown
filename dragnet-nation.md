---
id: 54280b9b3338660008020000
slug: dragnet-nation-en
published_date: 2014-09-29T00:15:00.000+00:00
author: Julia Angwin
title: Dragnet Nation
subtitle: A Quest for Privacy, Security and Freedom in a World of Relentless Surveillance
main_color: 97B94C
text_color: 596E2D
---

# Dragnet Nation

_A Quest for Privacy, Security and Freedom in a World of Relentless Surveillance_

**Julia Angwin**

_Dragnet Nation_ details the ways in which governments and corporations can gather vast amounts of your personal data and how this affects you. The book explains how easily such sensitive data can be abused, and that ultimately, such practices lead to a loss of freedom for all of us.

---
### 1. What’s in it for me? Learn how and why governments and advertisers are collecting your personal information. 

Cars mounted with panoramic cameras photograph our homes and schools and post the images online. Surveillance cameras perched on street corners follow our daily movements. Advertising companies load software on our devices to track which sites we visit online.

This is not the stuff of science fiction, but today's reality. Companies and governments now go to great lengths to develop sophisticated systems to capture your most personal information.

A _dragnet_ — a series of actions to sniff out a criminal — is a rare occurrence and usually requires an extraordinary effort by law enforcement officials. Yet in our digital era, there is a new kind of dragnet. This modern version uses advanced technology to scoop up vast amounts of personal information on anyone and everyone — and it's not just the police who are doing it.

Today, everyone is watching, and our privacy — not to mention our freedom — is at stake.

Confronted with so many prying eyes, many people give up on privacy, thinking that it's impossible to fight in our digital age. But you shouldn't give up. These blinks will show you how to take small but effective steps to protect your information and regain your privacy.

In these blinks, you'll also learn

  * how personal data is now considered as valuable as oil;

  * why some cookies are not at all delicious but downright dangerous; and

  * what you can do to avoid being tracked by technology behemoths online.

### 2. You’re not paranoid, you’re right: We live in a world where someone is always watching us. 

The threat of espionage was a concern once limited to heads of state — but now, each one of us should always wonder if someone, somewhere, is spying on us.

New technology has changed the rules of the game. The ubiquity of computers has made it easier to gather information at little cost. Before this, tracking was simply too expensive — just imagine how much it would cost to have a private investigator tail a subject for months, for a few bits of trivial information.

Thus governments kept just a few data points, such as birth or marriage records, property deeds or death certificates. Companies only had access to your information once you purchased an item or filled out a contract.

Within the past ten years, however, the price to store data has fallen over 300 percent. Now it is cheaper than ever for governments and companies to keep our personal records.

Thanks to Edward Snowden — the whistleblower who leaked information in 2013 on the activities of the U.S. National Security Agency — we know institutions can and do gather a lot of data. Though the NSA is mandated only to intercept foreign communications, Snowden's documents revealed that the agency also collects phone records and online information of U.S. citizens.

Yet the NSA isn't the only agency playing this game; American local and state governments also are involved in surveillance. Some agencies use automated car license readers, for example, to help track and analyze citizens' movements.

Companies are also involved. Cell phone companies such as AT&T and Verizon sell information on your physical location to data brokers; advertisers follow you online. Mall owners can also track how you browse from shop to shop, and some retailers now use facial recognition to remember who you are and what you've purchased.

Should you be concerned about surveillance? The next blink will answer this very question.

### 3. Your personal data can be easily accessed and used by those who may want to harm you. 

You might be wondering: "OK, my data is being collected and stored. But aren't there laws that protect me from potential abuse?"

Yes, and no. There are laws, but there are also legal loopholes that are continually exploited.

Where your information is stored is a problem. Consider that there may be several databases that hold information like your name, your address or even the location of your cell phone at any time. It's close to impossible to check and then delete that data at every single location — as you might not know where they all are.

So if someone wants your information — an employer or even a stalker — they will probably find a way to access it.

For example, in 1999 Liam Youens, who was mentally ill, wanted information on a woman over whom he had been obsessing. All he had to do was pay online data broker Docusearch to locate information about her, including where she worked and where she lived.

He then drove to her work and killed her, before killing himself.

Spying on someone is now so easy that you don't even have to be a tech wizard to do so. In 2009, technicians at a high school installed spying software on students' MacBooks, technology that was provided by the school. The technicians could then activate the laptops' webcams and take photos.

Some students were photographed thousand of times. One student, Blake Robbins, was photographed eating candy that unfortunately was shaped like a pill. Thinking that Robbins was consuming drugs, the assistant principal accused him of "improper behavior in his home."

Many students were shocked by the school's actions. One former student reported being so embarrassed and emotionally distraught that he decided to no longer attend the school.

### 4. Where there is a society constantly under surveillance, there is also a culture of fear. 

How would you feel if surveillance cameras were installed in your home, with strangers watching your every move? Or if you realized someone was following you constantly on the street?

You might feel fear, or even anger. We now know that being spied on affects how we think and how we act. Living in a society under complete surveillance has a severe effect on our lives.

Consider the _Stasi_, the former East German secret police force, the largest secret police organization (per capita) in history. One in fifty East Germans were somehow involved with the Stasi, which kept files on over 4 million citizens, or a quarter of the total population!

Thus every social contact was a potential spy, and unsurprisingly, a culture of fear emerged.

In a survey after the fall of East Germany, 43 percent of former East German citizens agreed with the statement, that "one felt spied upon. You couldn't trust anyone."

Another study on the psychological effects of Stasi surveillance found that once people felt threatened, they either became became model communists or absolutely paranoid. In one way or another, they adapted to a state of repression.

However, a repressive regime isn't required to have people censor themselves or relinquish personal freedoms. Even if you might trust the person watching you, simply the feeling of being watched is enough to make you change your behavior.

Finnish researchers confirmed this in an experiment in 2011. They installed surveillance equipment in participants' homes and monitored their behavior. Participants knew that only the researchers were allowed to observe them, and that they could turn off the cameras and mics whenever they wanted to.

Nevertheless, participants changed their routines. They would undress in more private places, moved sensitive conversations into the bedroom (where there were no microphones) and invited friends over less frequently. What's more, they reported that being monitoring annoyed them and made them feel anxious.

Now you know just how easy it is to collect data and store it. But who exactly is doing the collecting, and what do they do with the data?

> _"We are constantly being asked to give up privacy in the name of security."_

### 5. Since 9/11, the NSA has collected internet and phone call data in mind-boggling amounts. 

Edward Snowden's revelations showed us that the biggest player in the surveillance "business" is the U.S. government, and the National Security Agency (NSA) in particular. We also know that legally, the agency's surveillance methods are murky at best.

So how did we get here? Our "dragnet nation" was born with the 9/11 terrorist attacks.

After the attacks, it became clear that traditional methods of intelligence gathering had failed. As the NSA began to explore new methods of surveillance, however, things quickly got out of hand.

Snowden's documents showed how on September 14, 2001, NSA director Michael Hayden approved the warrantless interception of any phone call within the United States to or from numbers in Afghanistan that were specifically connected to known terrorists.

Soon after, Hayden wanted even _more_ data. So the NSA began to scoop up domestic information as well with the approval of President George W. Bush, originally only for a 30-day period. This order, however, was constantly renewed and expanded.

What was first an emergency program essentially turned into a giant domestic spying operation.

Moreover, it's not clear that the NSA dragnets are legal, both in terms of U.S. and international law. The agency has been operating within a legal gray area, exploiting loopholes in established law.

One such loophole is the abuse of the term "_metadata_," or data about data. Think of metadata as the envelope containing a letter, where the letter itself is the data. For example, phone call metadata identifies callers and call recipients, but not the conversation itself.

The NSA argues that, because much of U.S. phone and internet traffic data is essentially metadata, the agency is not actually violating citizens' right to privacy. Critics argue, however, that metadata tracking is indeed a serious breach of privacy.

> _"These [legal] loopholes have become large enough to allow for the type of suspicionless searches that outraged the Founding Fathers."_

### 6. Internet companies are watching how you surf and then selling that information to others. 

As the market for software began to slow in the early 1990s, companies in Silicon Valley sought a new revenue model. They first jumped on the idea of online advertising. Yet after the dot-com bubble burst in 2000, the market for online advertising too was dead in the water.

Or so it initially seemed. A new technology, called _cookies_, would rekindle the promise of online advertising.

A cookie is a small data file that is installed on your computer when you visit a website. A company or advertiser that controls the cookie can then track or follow your activity online, even when you've already left the original website.

For example, say you're browsing a particular site for a Swiss watchmaker. Data is exchanged and a cookie is created. Once your device is recognized, your cookie will then be auctioned to advertisers looking to reach consumers like yourself who are interested in Swiss-made watches.

The advertiser that wins the auction has the right to essentially "follow" you online; you may notice ads for Swiss watches appearing on other sites you frequent, such as news sites.

Early on people questioned whether this practice was in violation of laws that limit wiretapping, hacking and electronic surveillance. However, a New York court ruled that the practice was indeed legal, as a web surfer must first consent to the installation of cookies on his computer.

The wealth of information made easily accessible with technology inspired a whole new industry for the collection and sale of personal data. With cookies, companies watch how you surf, construct a profile and then sell that data to the highest bidder.

In fact, these methods are so popular that the ads created from user behavior profiles have been called "the new oil of the internet."

> _"Personal data is the new currency of the digital world."_

### 7. Protecting your privacy in the modern world isn’t easy; you will have to make tradeoffs and sacrifices. 

Knowing how easy it is for others to obtain your private data, how can you protect yourself? Do you go "off the grid" and hide in a cave, or can you protect your privacy and still live in modern society?

We'll start with the bad news. It is nearly impossible to leave no data trail at all. There are hundreds of data brokers that store personal data on just about everyone, yet only a handful allow people to see, correct or delete their records.

Some people do try to erase their digital traces. Dubbed _surveillance vegans_, these people make it as hard as possible for anyone to track their actions or movements. They pay in cash, set up multiple identities online, and use multiple phones and email addresses. Even their closest friends don't know where they live.

Obviously, this kind of life isn't easy to maintain. Not only does it require a huge effort, but also makes holding down a job or keeping friends around difficult.

Instead, you could try being a _surveillance flexitarian_. Like dietary flexitarians who eat less meat but aren't quite vegetarians, you can take small, manageable steps to protect your privacy.

Unfortunately, there's no silver bullet for privacy protection. While absolute privacy is possible, it's not easy to achieve in contemporary society — you'll have to make some sacrifices.

The social networking site Facebook offers an interesting case. Facebook collects your personal data and sells it to advertisers; for this reason, some people have decided to delete their accounts. But this comes at a social cost, as without Facebook, it may be more difficult to network or keep in touch with old friends.

Your cell phone, too, transmits tons of data about you, your location and your habits. However, some people, such as parents of young children, feel they need to be reachable at all times, and that means keeping a cell phone turned on despite knowing they'll be tracked.

### 8. Don’t like the government sticking its nose in your emails? Find a safer way to send messages. 

Google is certainly not the worst culprit when it comes to data collection or data abuse. In fact, it tries to be transparent about surveillance requests, being one of the first companies to make public the number of information requests it receives from law enforcement.

Nevertheless, Google has been at the center of many reported privacy abuses and violations.

Despite the company motto, "Don't be evil," Google has indeed crossed the privacy line by gathering personal information from Wi-Fi networks with its Google Street View camera cars.

There have also been a number of reported abuses by Gmail, Google's free email service. For example, in 2010 a Google engineer was fired for looking through personal Gmail chats. Google also scans the content of emails to "improve" the personalized ads that appear next to email messages.

If such examples concern you, there are of course alternatives to Google. The company DuckDuckGo, for example, is committed to offering a comparable search engine to Google's while also maintaining a policy of zero-data retention **.**

If you are looking for a more secure email client, you could consider the privacy-protecting email service RiseUp, run by a collective in Seattle that aims to fight intrusive government surveillance.

Unfortunately, several similar email encryption services have closed up shop in recent years.

Lavabit, used by whistleblower Edward Snowden, recently ended their email services as they said they didn't want to "become complicit in crimes against the American people."

However, this hasn't stopped other services like RiseUp from continuing to fight government intrusion.

### 9. Apps and other software can help keep cookies at bay and thwart pesky advertisers. 

Among the most popular methods for preventing data tracking is the use of encryption and ad-blocking software. Nowadays, using encryption is easy: all you have to do is download and install an app on your phone or device.

The company Silent Circle provides secure, multiplatform communication services for mobile devices, enabling encrypted text messaging as well as encrypted phone and video calls.

Two of its apps, Silent Text and Silent Phone, work on both Android and Apple devices and cost $9.95 per month.

In addition to encryption, Silent Text can _burn_ messages that you've sent or received; that is, the messages are irretrievably deleted from your device. The only drawback is that the person you are messaging needs to use the app as well for the service to work.

There is also software to help you thwart internet companies and advertisers from following you around online with cookies. If you use the Firefox Web browser, take a look at its two most common anti-tracking add-ons **,** AdBlock Plus and NoScript.

AdBlock Plus prevents online advertisements from displaying and stops advertisers from installing tracking cookies on your computer in the first place.

In contrast, NoScript prevents JavaScript, Flash and similar software — which are often used to install all kinds of tracking software, including cookies — from loading without permission from you.

As you can see, it's not impossible to take control of your information. But there is always a cost, whether migrating email contacts, leaving social media platforms or mulling whether you want JavaScript to run on a site you're visiting.

In our digital age, you'll have to choose between privacy and convenience.

### 10. Final summary 

The key message in this book:

**We live in a world of indiscriminate dragnets and ubiquitous surveillance. Critically, the consequences of this near-universal data mining are still widely underestimated. Knowing that surveillance alters our minds and behaviors, it's important to not give up on protecting our privacy.**

**Suggested** **further** **reading:** ** _No_** **_Place_** **_to_** **_Hide_** **by Glenn Greenwald**

In _No_ _Place_ _to_ _Hide,_ author Glenn Greenwald details the surveillance activities of secret agencies as according to information leaked by American whistleblower Edward Snowden. Rather than serving as a means to avoid terrorist attacks, as the US National Security Agency (NSA) claims, Greenwald explains that these dubious activities instead seem to be a guise for both economic espionage and spying on the general public. _No_ _Place_ _to_ _Hide_ also brings to light the media's lack of freedom in detailing certain government and intelligence agency activities, and addresses the consequences whistleblowers face for revealing secret information.
---

### Julia Angwin

Julia Angwin is a Pulitzer Prize-winning author and investigative journalist for independent news organization ProPublica. She is the author of the book _Stealing MySpace,_ charting the creation and rise of social media site MySpace, and was formerly a staff reporter for _The Wall Street Journal_.

