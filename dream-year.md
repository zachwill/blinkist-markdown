---
id: 546a0e653034360009730000
slug: dream-year-en
published_date: 2014-11-18T00:00:00.000+00:00
author: Ben Arment
title: Dream Year
subtitle: Make the Leap from a Job You Hate to a Life You Love
main_color: E5594F
text_color: B2453E
---

# Dream Year

_Make the Leap from a Job You Hate to a Life You Love_

**Ben Arment**

Many people today are frustrated with their day-to-day jobs, because deep down they know that they're not really living the life they desire. _Dream Year_ helps you identify your dreams, and offers practical advice on how you can go about starting your own business to pursue them.

---
### 1. What’s in it for me? Identify your dream and plot a clear, actionable path toward it. 

These days, the majority of working Americans _hate_ their jobs. Every day they sit in cubicles or stand behind counters feeling increasingly frustrated, but don't know what to do about it. This sad state of affairs arises from the simple fact that they're not following their own dream, but instead working for someone else's.

If you find yourself in this situation, these blinks will show you how to overcome your fears, so you can break free of your job and pursue your own dreams. You'll find out how to take this massive-sounding endeavor and break it into small, manageable steps that will lead you to where you really want to be. Little by little, you can make this year the one where you finally begin to achieve your dreams.

You'll also discover

  * why it's actually a good thing that you're frustrated at your current job;

  * how anyone can get to personally meet dozens of world leaders; and

  * what being shot at and starting your own business have in common.

### 2. Use your frustration with your current job as fuel to pursue your dreams. 

Do you dislike your job? If so, you're not alone: studies have shown that a whopping 66 percent of working Americans can't stand what they do for a living.

But why is this?

Most of the frustration you, as an employee, experience stems from the fact that you're working to make someone else's dream come true, not your own. Your job is not meant to be fulfilling or satisfying; your employer has probably designed it so it's just bearable enough that you won't quit.

Luckily, this sense of frustration can have a positive outcome.

For one, it can help you understand and articulate the problems you face so you can better address them: when you complain to a friend about your job, you may find that you're actually listing all the issues very clearly.

What's more, frustration can also push you to improve your situation. It's an energy source that can fuel your efforts to make a change.

Finally, the frustration you feel now will also help you stick to the changes you make later, as it will help quell any doubts you have.

Let's say you decide to quit your IT job and go back to school to study medicine. Undoubtedly, at some stage you'll begin to have second thoughts: "Was this the right move?" "Should I quit?" This is inevitable, and when it happens, you can think back to the frustration you felt at your old job. This will strengthen your resolve and help you stay the course.

So now the question is, what kind of change is the right one for you?

Quite simply, you need to stop following other people's dreams and start pursuing your own. Take your own dream and your own ideas, and start a business around them.

> _"...frustration is the fuel that propels you through the challenges when your idealism runs out."_

### 3. Don’t let fear stop you from following your dream – harness it for motivation. 

If so many Americans hate their jobs, you might be wondering why more of them don't quit and start their own business.

For many, there is only one thing stopping them: _fear of failure_.

If you're afraid of failure, then the idea of following your dream and starting your own business will no doubt terrify you. In this light, a regular job where you simply work for someone else's dream might seem relatively attractive.

But what are you actually afraid of here? What's the worst that can happen? If your business fails, your standard of living will take a hit: you may have to skimp on things like cable TV and eating out for a while. But you can definitely bounce back from this temporary setback, so this fear is unfounded.

Instead, what you should really fear and avoid at all costs is _insignificance_. If you play it safe by ignoring your own dream, you'll end up leading a life devoid of significance — a truly horrifying thought.

You must embrace this fear of insignificance, and let it be the force that drives you to start your own enterprise.

Another simple antidote for the fear of failure is _experience_. When you first take the leap to pursue your dream, you'll no doubt fret constantly about the risk you're taking. But very soon you'll start acclimatizing to it.

One Los Angeles-based security company manager trains his bodyguards by shooting at them. The normal human reaction to such a stressful event is to freak out completely, but the bodyguards need to remain calm even when under fire. At first, the trainees panic just like everyone else, but after about ten shots, they have acclimatized to the high-risk scenario and remain collected.

Similarly, you too will acclimatize to the high risk of following your dreams.

### 4. Discover your dream at the intersection of your passion and your gifts. 

Let's say you're ready to quit your job and start following your dream. But what if you don't know what your dream is?

Many people struggle with this dilemma, but you can unravel it by turning back the clock.

One starting place could be your childhood, because in many ways you were freer then than you are now.

As adults, our desires and choices are largely dictated by the expectations of society. For instance, these days it has become a near-universal goal to become rich, famous and successful. But what if these goals have nothing to do with your dream?

In contrast, as a child you probably didn't care about what society expected of you. You simply did whatever _you_ wanted.

If you wanted to become an astronaut, you probably didn't worry about the cost of the endeavor or whether your insurance policy would cover it. Instead, you simply did everything you could to get into NASA: learning about the universe, building model rockets and attending space camp.

By looking back on your childhood dreams in this way, you can find inspiration for your dream as an adult.

Once you've found a dream you're passionate about, tweak it so that it matches your _gifts_. Maybe your space career came to a halt because you didn't shine at science in high school, but that didn't mean you had to abandon it altogether.

Naturally, you should be passionate about your dream — so much so that you'd gladly pursue it for free, without any compensation. Let's say you're passionate about music. This means you probably gladly spend hours listening to albums, going to concerts and reading interviews with your favorite artists. What's more, you'll pay for the pleasure of doing this!

But just because you're passionate about music doesn't mean you should necessarily become a musician yourself, because your gifts may not be suited for this. If you have no singing talent, you probably shouldn't try to become a singer, but perhaps a producer, concert organizer or music teacher. These are the dreams where your passion and gifts match each other.

> _"Your dream is the outcome of a life lived up until this point, not a new beginning."_

### 5. To pursue your dream, you need a business idea that solves a problem and is unconventional. 

Once you've identified your dream, you need to find a way to turn it into a business.

The first step is to come up with a business idea that allows you to pursue your dream while simultaneously solving a problem that people face. This is the only way anyone will care about _your_ dream.

A case in point is Chuck Templeton's passion project: the restaurant reservation service _OpenTable_. When he originally offered this service to restaurants, he found they weren't interested because they were already full every night.

Instead of giving up on his dream, Templeton tweaked OpenTable by adding a function that let restaurants access detailed customer information. Now he was solving a real problem for the restaurants: they didn't know their customers. Demand for this new version of the service skyrocketed, and OpenTable is now used by over 30,000 restaurants.

Another key criterion for your business is that it has to be unconventional: it must break the rules of your industry.

This is because a conventional idea will put you in direct competition with established firms in the industry, who have advantages like more experience and an established customer base.

So work out what everybody else is doing, and then subvert it.

_Netflix_ showed the power of this approach in the 1990s when they disrupted the video rental industry. Instead of setting up retail stores like everyone else, they rented videos through their website, delivering and retrieving the DVDs via mail. They also charged a monthly subscription instead of a fee for each rented video. This approach was highly successful, but once video streaming technology had advanced sufficiently, Netflix broke the rules again by releasing movies directly over the internet. Today, the company is a major player in the on-demand internet streaming industry.

### 6. Your dream has to be financially viable. 

Once you've come up with a business idea that will let you pursue your dream, you'll probably be raring to get started. But at this stage, it's actually vital that you keep a cool head and take a sober look at your finances.

For starters, your idea has to be financially viable — this is what differentiates it from a hobby.

It's not enough that you and your customers love your idea, it must also generate enough profit for you to live on.

For instance, you've probably seen restaurants that are unbelievably popular because they serve great food at really affordable prices. Sadly, these businesses usually go bust sooner or later, as they're simply not profitable — a steady flow of customers is not enough.

So profits are important, and one way to maximize them is to create multiple revenue streams: offer services or products that are related to your main idea. If you own a hair salon, don't limit yourself to just offering haircuts, but also sell hair care products and provide spa treatments. This will not only increase your profits, but also make them more robust. If one revenue stream dips, the others are still on tap.

Another crucial part of running your business is keeping your outlays low. An easy way to increase your profits is to cut all expenses which don't lower the quality of the service you're delivering. So don't go overboard by, for example, investing in a big office or fancy antique furniture just because you're excited about your new venture.

Go further: if you've started a web design agency, you definitely shouldn't bother with an office. You can start out working from your own home. Cost-saving tactics will give your idea the best possible chance of turning a profit.

### 7. Make a plan and get started on your dream – no excuses! 

In any undertaking, the hardest part is getting started. And pursuing your dream is no different. It's very tempting to just keep dreaming and planning your business rather than actually taking action to make it happen. It's easier and more appealing than actual work.

Another common excuse for not pursuing your dream is saying that it's not the right time to take action. But this is false reasoning: circumstances will never be perfect.

Consider the creators of the popular game, _Angry Birds_. When they started their company in 2003, they didn't have _any_ great ideas for games. But they launched anyway, and developed 51 unsuccessful games, almost going bankrupt in the process. But the 52nd game turned out to be Angry Birds, the most popular mobile game in history.

Similarly, you too need to understand that taking action is more important than waiting for all the stars to align.

But first take a step back and create a realistic schedule for yourself to follow. This will keep you on track and mindful of the big picture.

In this schedule, you should define what you want to achieve each day. These tasks should be as small as possible so they don't feel overwhelming. For example, don't put down "do this year's taxes" but instead split bigger items up into tasks like "print out the tax declaration form" and "collect receipts for March." This will help keep up your spirits.

This kind of schedule will allow you to adopt a very hands-on mind-set in your own business, much as you would at a regular job. After all, at a regular job you can't just procrastinate and daydream all day, you have tasks to do and milestones to hit. The same should apply when you're pursuing your dream.

Just remember not to make your schedule too ambitious, or you'll inevitably fall behind and lose heart.

> _"From thirty thousand feet, creativity looks like art. From ground level, it's a to-do list."_

### 8. Entrepreneurs can’t be afraid of asking for what they want. 

If you were to write to the president of the United States requesting a meeting, do you think he would find some room in his calendar for you? Probably not, right?

But in fact, you'll never know unless you try.

Take the author Bob Goff: After the 9/11 terrorist attacks, he wanted to help his children come to terms with what had happened, and so he asked them to imagine what they would say if they met the leaders of the world.

One of his daughters insisted on actually writing to them to ask for a meeting, and so Goff found some addresses for her. Astonishingly, she received 29 affirmative replies to her request, even from as far away as Israel and Switzerland.

This just goes to show that you never know what you can get unless you ask for it.

This lesson is especially important for entrepreneurs, as they need to be bold in _making asks_ — their livelihood depends on what they can get from others. They might need to ask a supplier for a discount, a realtor for a prime retail location or a customer for an advance payment.

For most people, asking for things feels uncomfortable. Thankfully, you can make it less so.

A simple rule is not to formulate your requests as "can you do this for me," or "I need you to do this for me," because these will be rejected, and you'll feel embarrassed.

Instead, you need to make it clear that you're not really asking people for something, but rather giving them the opportunity to participate in your dream in a win–win scenario.

This is what filmmaker Kristin Harle did when she wanted the movie rights for her favorite childhood book. She was able to start a dialog with the author, but only because she emphasized that her film would help sell more books, benefiting the author, too.

### 9. Create a unique brand experience for your product and build a loyal fanbase to spread the word. 

There are countless smartphone and tablet companies out there, but none evokes quite the same passion as Apple. Their products may be pricey and lack the features of other leading-edge competitors, but people still swoon over them.

Why is this?

Because Apple has succeeded in creating a _brand experience_ that sets their products apart from the rest.

Then there's _Whole Foods_, the American supermarket chain. People tend to see shopping for groceries as a necessary chore, so Whole Foods decided to make the shopping experience more personal. The company encouraged employees to chat with customers in a friendly way and started posting signs at their deli counter announcing who had sliced the meat each day. The result? Customers loved this personal approach.

If you want to succeed, you too need to think about what kind of brand experience your company can offer, and how it will help you stand out.

A compelling brand experience leads you to another key element for success: building a _platform_ — a loyal fanbase who you can count on to buy your products and promote them to their friends.

Without a platform, you'll have very little chance of success, even if your idea is brilliant. According to Kevin Kelly, co-founder of _Wired,_ you need at least 1,000 "true fans" to start earning a living from your company.

So how can you reach out to people to build a platform?

One popular method is to start a blog or use other forms of social media, but you can also consider more innovative approaches.

Consider author Tim Schraeder's approach when writing _Outspoken,_ a book on communications: He invited 60 experts to contribute to the book, knowing that each expert would engage their own fanbase, thereby ensuring a large platform for the book. It worked too: the book sold 1,200 copies in just five days.

### 10. Final summary 

The key message in this book:

**You can escape career frustration by becoming an entrepreneur who is pursuing his or her own dreams. Be upfront about what you want from people, and keep both your revenue streams and your customer base broad. It will be a rocky road, but with level-headed, realistic planning, you'll get there.**

Actionable advice:

**Break down your dreams into actionable steps.**

Once you've identified your dream, take the time to plot out each step you need to take. Then break those steps down into smaller and smaller tasks until you reach a level where each task can be accomplished within a single day. This will stop you from being overwhelmed. For example, don't put a check box by "generate first million dollars of revenue." Instead, work through "identify potential customers," "draft pitch email to potential customers" and "send off pitches."

**Suggested further reading:** ** _The 4-Hour Workweek_** **by Tim Ferriss**

_The 4-Hour Workweek_ advocates the idea of the New Rich. These are the people who abandon their jobs as modern desk slaves and instead live a life that is all about enjoying the moment while still achieving big goals.
---

### Ben Arment

Ben Arment is a serial entrepreneur who manages _Dream Year_, a coaching organization which helps people to realize their dreams.

