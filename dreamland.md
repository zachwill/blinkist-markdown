---
id: 5360dc836233360007400100
slug: dreamland-en
published_date: 2014-04-29T11:31:36.000+00:00
author: David K. Randall
title: Dreamland
subtitle: Adventures in the Strange Science of Sleep
main_color: None
text_color: None
---

# Dreamland

_Adventures in the Strange Science of Sleep_

**David K. Randall**

_Dreamland_ is a journey into the world of sleep. By examining scientific evidence it explores why we and all other animals on the planet need sleep to survive, as well as how modern-day life can fly in the face of our natural sleeping patterns.

---
### 1. What’s in it for me: Discover how to sleep better and be sharper the next day. 

If you stop to think about it, sleep is a bizarre phenomenon: we basically lie more or less unconscious for a third of our lives. What's more, in these periods we often have vivid and even bizarre dreams that we could spend hours trying to interpret.

Most of us don't pay much attention to sleep until something goes wrong — say we begin to suffer from insomnia or, as happened to the author, injure ourselves while sleepwalking.

Yet when we begin to examine the phenomenon of sleep, we find that it is truly fascinating, and in many ways still mysterious, from its evolutionary background to the mystifying dreams it produces.

In these blinks you'll discover, among other things,

  * Why teenagers aren't being lazy when they sleep late in the morning.

  * Why you're missing out on the very relaxing midnight experience that pre-industrial people enjoyed.

  * Why you should seriously consider taking naps at work.

### 2. Sleeping is a universal, yet poorly understood phenomenon that comprises five progressively deeper stages. 

Some people may find it frustrating that we humans spend around a third of our lives asleep, but they can comfort themselves with the fact that sleep is a universal phenomenon: every animal on the planet sleeps.

It's interesting though that there seems to be little relationship between an animal's size and position in the food chain, and the amount of sleep they need: tigers and squirrels typically get about 15 hours of shuteye each day, while giraffes, on the other hand, make do with just an hour and a half. Dolphins and some birds have even mastered the art of only half-sleeping: one hemisphere of their brain remains awake to watch out for predators, while the other sleeps.

Despite its universal nature, the true biological function of sleep remains something of a mystery.

In fact, up until the 1950s, scientists showed little interest in sleep, as it was commonly believed that the brain was not really doing anything interesting as it slept.

It was only in 1952 that a breakthrough came: scientists observing sleeping subjects discovered to their surprise that in the middle of the night their subjects' eyes began to move around rapidly. This phenomenon was aptly named _rapid_ _eye_ _movement_ or _REM._

After further studies, it was found that in fact sleep consists of five stages that we cycle through repeatedly, each cycle taking roughly 1.5 hours.

In the first four stages, the brain goes into progressively deeper sleep, which is shown by the fact that someone woken up in the first stage may not even realize that they were sleeping, whereas someone woken in stage four will feel severely disoriented.

The fifth stage of sleep is the REM stage, where the brain actually emerges from deep sleep and has been shown to be as active as if you were awake. Perhaps this explains why REM sleep is also the time when most _dreams_ occur.

### 3. Sleep deprivation has severe negative physiological and cognitive effects, and can even be lethal. 

Have you ever pulled an all-nighter — for example, when studying for exams at university? If so, you probably know how unpleasant the next day can be.

In fact, scientists have studied the effects of _sleep_ _deprivation_ in order to better understand the function of sleep. As you might have guessed, the results indicate that you simply must sleep to function, and even to survive.

Consider Randy Gardner, a high school student who set the world record for sleep deprivation by staying awake for 264 consecutive hours in 1965. After the first day, his condition deteriorated rapidly: he lost the ability to do even simple arithmetic and became increasingly paranoid as the days wore on.

Subsequent experiments on laboratory subjects found that after the first 24 hours of sleep deprivation, subjects' blood pressure increases, body temperature drops and immune systems weaken. They also begin to crave carbohydrates uncontrollably as their metabolism goes haywire.

It was also found that sleep deprivation impaired the subjects' _prefrontal_ _cortex,_ which is essentially the executive decision-maker of the brain, thus impairing decision making and adaptation to new situations.

What's more, the subjects' ability to learn and retain new information was also greatly curtailed.

One experiment was particularly enlightening: In a fictional business setting, it was found that unlike their well-rested counterparts, sleep-deprived subjects couldn't adapt to changes in their business environment, and eventually went bankrupt.

On a more extreme note, it seems that prolonged sleep deprivation can even be fatal.

Experiments have found that laboratory rats that are not allowed to sleep eventually just drop dead from exhaustion.

In humans the effects of prolonged sleep deprivation can be observed in sufferers of a rare genetic disorder known as _fatal_ _familial_ _insomnia_ or _FFI_. FFI makes it progressively harder for patients to fall asleep, and a year after its onset, they typically die after months of agony due to exhaustion and chronic migraines.

Thus it seems that sleep really is a survival necessity, like food or water.

### 4. Sleeping helps us learn new things and find creative solutions to problems. 

By now, you may already have guessed some of the benefits of sleep.

First of all, deep sleep is one of the few chances that the prefrontal cortex has to rest, since it is typically active every waking moment, so by extension sleep is crucial for good decision making.

Secondly, as mentioned earlier, sleep also seems to be crucial for learning new things and remembering them.

Studies have shown that this applies to simple chores like memorizing lists of words as well as to more complicated tasks involving procedural skills, like playing Tetris or typing out text.

Scientists speculate that the memorization benefit is due to dreaming in particular. The theory is that dreams are the brain's way of sorting all the new information obtained during the day, and selecting the most important parts for storage in long-term memory.

Finally, sleep also seems to help us solve problems and be creative.

Consider the example of the chemist August Kekule, who discovered the structure of the benzene molecule after dreaming about a snake eating its own tail. Or Paul McCartney, who woke up one morning to find the future smash-hit song "Yesterday" fully formed in his mind.

So how does the brain deliver such insights?

In short, by trawling through existing memories to connect to the day's events.

This was illustrated by a study where subjects played an alpine skiing game, and the following night were repeatedly woken up during REM sleep and questioned about their dreams. It turned out that they initially dreamed of skiing, but as the night wore on, they began dreaming of things increasingly unrelated to skiing — for example, being on a conveyor belt in the forest.

It's as if the brain is looking for memories, emotions and knowledge to connect to whatever happened during the day, and as the night wears on, looks in ever-more unconnected areas.

From this broadening search for connections, a new and unexpected insight or solution to a problem may emerge.

### 5. Sigmund Freud was the first to ascribe meaning to dreams, but today scientists doubt there is any. 

Throughout history people have ascribed meaning to the content of dreams. Their often bizarre narratives and emotional overtones — such as in nightmares — have inspired many cultures and religions to see dreams as prophecies and omens of things to come.

In the scientific community, however, up until the twentieth century dreams were not considered meaningful at all. This changed when the hugely influential psychologist Sigmund Freud published his landmark work, _The_ _Interpretation_ _of_ _Dreams_.

Freud believed that dreams represent the unconscious fears and hopes that we have suppressed, which manifest themselves as symbols. He thought most of our unconscious desires were sexual, and hence attributed many dream symbols to sex and sexual organs. Consider that in Freudian literature, no fewer than 102 symbols for the penis and 95 symbols for the vagina have been found.

But in the 1950s, scientists began to seriously doubt the idea that dreams represented unconscious desires or fears.

Psychologist Calvin Hall spent 30 years cataloging and studying dreams, and came to the conclusion that dreams are just an accidental by-product of how our brain processes waking activities, and as such have no meaning or purpose whatsoever.

Hall did note, however, that most dreams tend to be negative, a fact he ascribed to us spending a lot of time worrying while we're awake.

One cognitive psychologist, Antti Revonsuo, speculated that this could in fact signify an evolutionary purpose: when we dream of bad things happening, we get to practice our reaction to them — a bit like a dress rehearsal. However, this view is still contested by other scientists.

### 6. Our modern once-a-day sleeping pattern is not the natural one. 

These days most people take it for granted that the "natural" way of sleeping is in one long block at night.

But in fact, before artificial light became so commonplace, people slept very differently: History books often mention people having "two sleeps." The first sleep would last from roughly sundown to midnight, after which people would awaken for about an hour before falling asleep for the second sleep. This one-hour of waking time has been described as immensely pleasant, and an excellent time to devote to things like reading, praying and sex.

One researcher performed a study where he simulated pre-industrial lighting conditions for test subjects, and found that within weeks they also began to exhibit the "two sleeps" pattern, waking up for an hour or so around midnight. What's more, they too described this interim period as incredibly relaxing and pleasant.

Regarding our natural sleeping patterns, scientists have also discovered that they are directed by our _circadian_ _rhythm_, an internal clock that determines when we feel sleepy and when we wake up.

For most people, the circadian rhythm gives us a boost of energy at around nine o'clock in the morning, and then causes us to start feeling sleepy in the afternoon at around two. We then get another boost at six in the evening, until starting to shut down for the night at around ten.

This strange pattern might arise from an evolutionary advantage it provided — encouraging early humans to rest up in the afternoon for the long trek back home after foraging for food.

Our natural sleeping patterns also seem to change throughout our lives. Teenagers, for example, tend to lag a few hours behind adults, falling asleep and waking up later, whereas the elderly usually only sleep from nine in the evening till three or four in the morning. Scientists speculate that this shift also has an evolutionary purpose, as it helped ensure that whatever the time, someone in the family would be alert to keep watch out for threats.

### 7. Typical school and work schedules clash with the circadian rhythm, and this is slowly being recognized. 

Let's now examine how modern life interferes with our typical circadian rhythm, and how this can be remedied.

One of the most obvious conflicts arises from teenagers' need to sleep late, and the demands of early school days.

Most high schools in America start classes at 8 a.m. or even earlier, when teenagers are barely alert, which obviously hinders learning. Indeed, one study found that most students got better grades in classes that started later in the day, simply because they were more likely to stay awake for them.

In response to discoveries about the circadian rhythm, several schools have begun experimenting with later starting times, and getting very promising results.

One school in Edina, Minnesota postponed the start of the school day from 7:25 a.m. to 8:30 a.m., and found a number of benefits: students' SAT scores shot up, the dropout rate slowed, and fewer students reported feeling depressed. Similar benefits were found in subsequent experiments.

Another clear conflict stems from the demands of working life and our body's afternoon sleepiness. After all, this would be a prime time to take a nap, but most workplaces frown upon employees sleeping during the day.

Thankfully, today many companies are catching on to the idea that allowing their employees to nap could actually provide a competitive advantage in a better-rested, more creative workforce. Google, Nike and Cisco Systems, for example, have designated napping areas in their offices for this purpose.

In fact, an entire _fatigue_ _management_ _industry_ has sprung up to consult for companies on the importance of sleep and fighting fatigue. One fatigue management company, Circadian, has consulted for over half of the companies on the Fortune 500 list — as well as one Super Bowl-winning football team — on how to develop working environments that allow employees to function at high levels despite the modern-day requirements that clash with natural sleep needs.

### 8. Professional athletes and the US military are also recognizing the importance of sleep. 

Considering all the downsides of sleep deprivation, you might assume that in a military setting, where life and death can hang in the balance, sleeping well would be highly encouraged.

But in fact, the opposite is true: even in peacetime soldiers are typically only allowed six hours of sleep, and even less during actual battle.

American soldiers compensate for this by consuming large amounts of caffeine, but as you can imagine, it is no substitute for actual sleep.

This constant fatigue leads to mistakes: In 1996, for example, 32 accidents that destroyed American military aircraft were attributed to crew fatigue. And in the Gulf War, a quarter of American combat deaths were caused by friendly fire. Looking into these results, the military concluded that they were due to the fact that soldiers were simply sleeping too little.

By this time, the US military had already spent millions looking for ways to get by with less sleep, even trying to instill a dolphin-like "half-sleep" in soldiers, but to no avail.

With the depressing consequences of fatigue so obvious, soldiers are now gradually being allowed to get more sleep, and the results are promising, with great boosts in morale.

Another setting in which sleep-related performance issues can be significant is that of professional sports.

This was shown by a study of how the circadian rhythm affects performance in football games, more specifically games where an East Coast team travels to the West Coast of the United States to play. In games played at, for example, 8 p.m., the East Coast team's bodies are effectively shutting down for the night, while the West Coast team's players have just received their natural evening energy boost.

As you might suspect, the West Coast teams have a significant advantage. In fact, the researchers found that a punter could have made a lot of money by consistently betting on the West Coast team, since even Las Vegas bookmakers underestimated the importance of the circadian rhythm.

### 9. Whether sleeping all together or each in a separate room, there are pros and cons to different familial sleeping arrangements. 

Now that we've discussed the importance of sleep in general, let's get down to specifics: what are your family's sleeping arrangements like?

For example, do you share a bed with your spouse?

Though sleeping in one bed was once an unquestioned practice, couples are now increasingly opting for separate beds and even bedrooms. In fact, it has been reported that over half of all new custom-built homes in the United States have separate master bedrooms.

The reason for this is simple: people just sleep better when they have their own bed. One study found that people sharing a bed were 50 percent more likely to be disturbed during the night than people sleeping alone.

Regardless, most couples continue to share a bed for various reasons, such as feeling safer, closer emotionally and having easier sexual access.

In many families, sleeping arrangements truly become an issue with the birth of a child, as nighttime feedings tend to initially lack any rhythm.

Up until the early twentieth century in the West, it was customary for infants to sleep in between their parents in the same bed, and this _co-sleeping_ arrangement is still common in many parts of the world. The practice began to wane due to fears that children might be crushed by their sleeping parents rolling over, and now most infants are kept in cribs in separate rooms.

Lately, however, some experts have once again advocated co-sleeping, saying it allows parents to be more responsive to their infants' needs and that it can foster a healthy attachment. As yet, there is no consensus on the matter.

Whatever sleeping arrangement is chosen, however, experts agree that parents should stick to this routine consistently to help the child get to sleep.

What's more, parents should force their children to nap in the daytime. It was found that this translates into many benefits, not least of which are better interactions with parents.

### 10. Sleep apnea and sleep walking can be dangerous sleeping disorders. 

Let us next examine some of the common sleep-related problems people experience.

One such problem is a condition called sleep apnea, in which a person's throat closes up repeatedly and randomly as they sleep. This prevents the person from breathing until they jolt awake, gasping for air. These episodes can happen up to 20 times an hour, but the sleeper may not remember anything the next morning.

Nevertheless, they _will_ feel badly rested due to never actually sleeping more than a few minutes at a time, and many sufferers complain of constant exhaustion. Some studies even indicate that severe sleep apnea can and does trigger heart attacks and strokes.

One treatment for the disorder now exists: a type of mask that actively forces air into the sleeper's lungs. People using it rave about the benefits, many claiming that it has allowed them to feel fully rested for the first time in years.

Another sleep disorder, _sleepwalking_, is fairly common. In fact, one in five people will sleepwalk at least once in their lives.

Normally, when someone sleeps, the parts of the brain responsible for muscle movement are shut down, but for sleepwalkers this does not happen. This means sleepwalkers can perform the same activities as a waking person, from eating to driving to having sex.

As you can imagine, without the conscious control of the waking mind, many of these activities can be harmful to the sleepwalker as well as others. There have even been cases of sleepwalkers injuring themselves leaping out of windows or shooting themselves in their sleep.

Some sleepwalkers even commit crimes — in the worst case, murder. A Canadian man named Ken Parks went to bed one night, only to get up and drive 14 miles to kill his mother-in-law, all while sleepwalking. Despite initial doubts, all evidence supported his claim that he was asleep, and a jury found him not guilty of murder.

### 11. Insomnia has been historically treated with drugs, but nowadays therapy is the recommended treatment. 

Do you have trouble sleeping? Studies indicate that on any given night, some 40 percent of Americans do.

But only some ten percent suffer from _insomnia_ at some stage in their lives — a disorder defined as "difficulty getting or staying asleep, or having non-refreshing sleep for at least one month."

Historically, sleeplessness was often treated with drugs. Unfortunately, many of these drugs were dangerous or came with nasty side effects.

For example, the first sleeping pill, Veronal, introduced in 1903, was a barbiturate that quickly built up a tolerance in users, while being lethal in even fairly small amounts. This led to many accidental overdoses.

Later drugs like Halcion had unpleasant side effects like memory loss. People taking them before plane trips to negate jet lag would occasionally wake up at their destination with no idea who or where they were. The drug was eventually banned in many countries.

The current popular sleeping pills, Ambien and Lunesta, seem to have fewer side effects, but studies indicate they do not actually increase the quantity or quality of users' sleep. Instead, they merely prevent users from remembering how much they tossed and turned before falling asleep, which makes them assume they probably slept more.

Given these shortcomings, these days many organizations like the National Institute of Health recommend _cognitive_ _behavioral_ _therapy_ as treatment for insomnia rather than drugs. In this therapy, patients learn to, for example, identify and challenge the thoughts that worry them and keep them up at night.

Though therapy takes longer to yield benefits than sleeping pills, therapy has been found to be almost equally or even more effective in terms of increasing sleep quality and quantity. What's more, the benefits last longer than in the case of drugs, where they end with the last pill taken.

### 12. To sleep well, exercise during the day and avoid bright lights, alcohol and caffeine before going to bed. 

Happily, there are also steps that you can take to improve both the quantity and quality of your sleep.

However, before we get to them, you should know that there's no evidence to back up the claim of mattress salespeople that the firmness of your mattress affects the quality of sleep you get. Rather, studies show that people tend to be most comfortable with whatever kind of mattress they are currently sleeping on.

So now that the mattress myth has been debunked, let's turn our attention to factors that do actually matter.

First of all, before going to bed, try to avoid sending your body any signals that might conflict with your circadian rhythm. For example, for about half an hour before bedtime, avoid the bright lights of a TV or computer screen, and try to dim the lights in your apartment. Also, consider lowering the temperature in your bedroom at night, as this is also in line with how your body naturally lowers its body temperature when it prepares you for sleep.

Second of all, avoid caffeine and alcohol before going to bed. The former is a stimulant, and will keep you up for hours, whereas the latter prevents you from getting a full, deep night of sleep.

Third, try to exercise regularly during the day. Studies indicate that even small increases in the amount of exercise a person gets can bring significant improvements in how quickly they fall asleep and how long they sleep.

Finally, there's the mental part: to fall asleep, your body and your mind must be at ease. Your body must be comfortable enough for your brain to forget it's even there, and your mind must cease consciously thinking about everyday activities and concerns. Studies have found breathing exercises — like simply thinking of the word "in" on every inhalation, and "out" on every exhalation — are good for encouraging this state.

In conclusion: good night, sleep tight, and don't let unnatural disturbances of your circadian rhythm bite!

### 13. Final Summary 

The key message in this book:

**Sufficient sleep is critical for many areas of cognitive function, from problem-solving and making decisions to learning and remembering new things. Many demands of modern life clash with the body's natural sleeping patterns, and these days many organizations like high schools, companies, sports teams and even the US military are beginning to change their ways to better reap the benefits of sleep.**

Actionable advice:

**If you're having trouble falling asleep, try a breathing exercise.**

The next time you find yourself tossing and turning in bed, feeling like you just can't fall asleep, try this breathing exercise: Lie on your back with your eyes closed, and every time you exhale, think of the word "out" and every time you inhale, think of the word "in." This method has been shown to relax both body and mind.

**Let your teenager sleep.**

Many parents feel irritated by the fact that their teenage children seem to stay up for hours at night and then sleep in all morning when there's a beautiful day to be enjoyed. But in fact, teenagers' circadian rhythms are naturally this way: they only get sleepy around 11 p.m., and naturally also then need to sleep longer. Rather than fight them on this, try to indulge them, and you'll see a sharper, more motivated teenager emerge. You could even consider suggesting to the local high school that they try an experiment where classes would start later, allowing all the sleepy teens to be fresh and rested in class.

### 1. What’s in it for me? Get to grips with a uniquely American crisis. 

These days, few can honestly doubt the seriousness of the opiate crisis in the United States of America. It has come to the attention of those at the top rungs of the country's government, including President Donald Trump — but the problem remains, as threatening as ever.

To call the opiate crisis an epidemic is by no means hyperbole. Indeed, in 2015, the US Drug Enforcement Administration put it in precisely those terms: overdose deaths, especially those from prescription drugs and heroin, had reached epidemic levels.

What is fascinating about this health crisis is that it is a uniquely American one. American attitudes toward health care, addiction, poverty, the media, crime, law enforcement, large corporations and scientific research have all contributed to this nationwide crisis.

Join Sam Quinones as he details how this epidemic came to be, offering fascinating insights into addiction and health, as well as the very functioning of the United States as a whole. Despite the bleak situation, hope is not lost and the crisis is resolvable. The country can get back on its feet.

In these blinks, you'll learn

  * what brand of opiates parents used to give to their unruly kids;

  * how one letter to a newspaper got mistaken for something it wasn't; and

  * how to run a successful drug cartel.

### 2. A tiny pill has played a major role in the current opiate crisis. 

The opiate crisis is being felt all over the United States — but it didn't come out of nowhere. It's a been a slow process, the foundations of which were laid back in the early 1980s.

In 1984, the drug company Purdue released a new product called _MS Contin._ It was a time-release morphine pill designed for treating pain in patients who were dying or who had just been released from surgery.

It was highly successful, so much so that the company released a similar drug in 1996. This new drug had _oxycodone_ as its active ingredient, rather than MS Contin's morphine. Oxycodone is an opium derivative not dissimilar to heroin, and once more Purdue used its time-release coating formula. The name of this pill was OxyContin.

The FDA approved OxyContin in 1995, as they believed Purdue's big claim about the pill: supposedly, their time-release coating would limit addiction by preventing the sorts of rapid highs and lows usually associated with opiates.

The consequences of this decision were serious: now Purdue could market OxyContin with a unique safety label that stated the drug had less potential for abuse than other painkilling drugs on the market.

It was this claim that became the cornerstone of OxyContin's marketing. This wonder drug would purportedly be the only painkiller a doctor would ever need to prescribe. It was to be an almost risk-free solution to chronic pain; unlike other opiates doctors had been prescribing for pain, this new drug had been shown to be addictive in less than 1 percent of patients.

But there was a very specific reason these rates of addiction were so low: MS Contin was being used in carefully controlled hospital conditions. Conversely, OxyContin was marketed to doctors _outside_ hospitals. And these doctors were used to dealing with chronic pain in their patients by prescribing opiates.

Now, these old opiates were quite weak, their dosages were small, and they contained agents such as acetaminophen or Tylenol designed to prevent abuse.

Oxycontin was different. It contained large doses of oxycodone and was only deemed safe because of the special time-release coating designed to supply the active ingredient over a long period.

Purdue soon began pushing its new product. Its representatives visited doctors several times a year and took them out to lavish lunches, attempting to drive home the merits of the time-release coating and how it ensured addiction was not an issue.

It didn't stop there: mountains of OxyContin merchandise and medical seminars at luxurious resorts were quite the norm.

And it was a success: Purdue's sales tripled. In fact, bonuses for sales rose from $1 million in 1996 to a whopping $40 million in 2001. By 2003, OxyContin was predominantly being prescribed by primary care doctors with very little pain management training. Purdue had successfully transformed the entire industry: prescribing for pain had become the norm, not the exception.

> OxyContin prescriptions for chronic pain rose from 670,000 in 1997 to 6.2 million in 2002.

### 3. The history of morphine goes back a long way. 

When telling the history of the opiate crisis and OxyContin, it's easy to forget that we're not just talking about the last 40 years — humans have loved opiates for thousands of years. The ancient Sumerians called opium the "joy plant," while it was also used in ancient Egypt and classical Greece as well as in India and the Arab empire.

The active molecule in opium is morphine. It's present in a number of other plants too, but not nearly in the same potency. Morphine was first successfully isolated in the early 1800s, and its use spread in the nineteenth century thanks to its properties as a painkiller. It was also put to use in many wars.

The inventor of the hypodermic needle, Alexander Wood, believed his nineteenth-century invention would reduce drug abuse, since users would be able to accurately measure their intake, something impossible through oral ingestion. Sadly, the story goes that Wood's wife became the first recorded victim of an intravenous opiate overdose.

At about the same time in the United States, marketing certain kinds of medicine was big business. Typically, they were sold as miracle cures and, more often than not, they contained opiates.

Boisterous children? Be sure to dose them up with Mrs. Winslow's Soothing Syrup!

By the beginning of the twentieth century, sales of such _patent medicines_ had reached $75 million per year. It was in this climate that Dr. Alder Wright synthesized the opioid heroin in 1874, as part of an effort to find a nonaddictive alternative to morphine.

Heroin — which is actually highly addictive — went on to be prescribed for everything from coughs to menstrual cramps. Consequently, rates of addiction exploded, and it happened because a doctor swore it was safe.

There's some solid chemistry to explain why morphine is so addictive. The morphine molecule fits perfectly into the "mu-opioid receptors" present in the brains of all mammals. These normally produce pleasure when they receive endorphins produced by the body, but when morphine is present, there's no need to wait for the endorphins.

When morphine enters the receptors, they are overwhelmed. The result is intense euphoria as well as a numbing of pain and feelings more generally.

However, the counterbalance to the euphoria is the brutal withdrawal symptoms that addicts face. These include insomnia and diarrhea, on top of weeks of intense pain.

In fact, withdrawal can be so overwhelming for some addicts that it pushes them to lie, steal, betray and even to put their bodies at risk just to get their hands on a dose of the molecule.

The chemistry behind such problematic withdrawals is plain enough: while most drugs can be easily broken down into glucose in the body and then expelled, morphine is different. The morphine molecule stays intact and lingers.

> "_Like no other particle on earth, the morphine molecule seemed to possess heaven and hell._ "

### 4. Starting in the 1980s, the concept of treating pain underwent a revolution in America. 

It might seem hard to believe, but a single-paragraph letter published in 1980 changed the course of medical history.

A doctor at Boston University's School of Medicine, Hershel Jick was asked in the early 1960s to put together a database of the records of patients who'd been hospitalized there.

In 1979, Jick became curious about what he could learn from this amassed data. He wanted to find out how many patients treated with narcotics while at the hospital had become addicted. He discovered that of about 12,000 patients, only four became addicted.

Jick wrote a short paragraph together with Jane Porter, a graduate student who'd been assisting him, in which they described their findings. They sent it to the New England Journal of Medicine, where it was published in the letters section under their names.

Elsewhere in the medical profession at about the same time, doctors were becoming increasingly interested in the idea of _pain_. For years, they had had patients suffering from chronic pain coming to them and begging for help. The implication seemed clear enough: pain was a serious matter and it was being undertreated.

Consequently, in 1996, the American Pain Society went so far as to declare that pain was the "fifth vital sign."

It was in this context that advocates pushing for more treatment with opiates, such as California's Board of Pharmacy, started putting out statements that made bold assertions, such as "studies show [opiates have] an extremely low potential for abuse" when correctly used.

Simultaneously, doctors became fearful. They were told that _patient rights_ meant that they would be sued if they didn't treat patients' pain — and that meant doling out opiates.

Suddenly, and seemingly out of nowhere, people starting citing "Porter and Jick," that little paragraph that had been written years earlier about patients in Boston. But it was being cited by people that hadn't actually read its few lines, and who were treating it as gospel science.

Indeed, the science magazine _Scientific American_ labelled it an "extensive study" in 1990, while a 2001 _Time_ story casually referred to it as a "landmark study."

Just like that, long-held wisdom about opiates was being thrown out the window. If a patient asked for higher doses of other drugs, it was taken as an indicator that the treatment wasn't working. And with opiates, that meant that the dosage prescribed was simply too low.

A new term even cropped up: "pseudoaddiction." It described the appearance of addiction that manifested in some patients. But in reality, all they wanted was a sufficient dose to end the pain. It was determined that these patients ought to have their dosage increased "aggressively."

### 5. In the 1990s, pill mills spread rapidly across the American Rust Belt. 

Portsmouth, Ohio was once a bustling town, built by and for American industry. But globalization hit it hard. Factories closed and jobs disappeared. By the 1990s, this town, like so many other cities in the American Rust Belt, was a shadow of its former self.

It was in places like Portsmouth that a sinister industry emerged. The jobs were hard to come by, but Medicare cards were much easier to find. This new economy was based on _pill mills_, clinics where doctors did little else other than write up prescriptions.

The number of applicants for the disability program called Supplemental Security Income nearly doubled between 1998 and 2008 in Scioto County, Ohio, which includes Portsmouth.

Many of these new applicants made regular visits to pill mills where they could stock up on OxyContin and trade these pills on the black market for whatever else they might need.

Some even got entrepreneurial about it and sought out groups of addicts, whom they would drive to the clinic. In exchange for the ride and for paying the clinic fee, the entrepreneur received half of the pills that were prescribed to the addict.

Before too long, an "Oxy" — a common street name for the OxyContin pill — was worth $1 per mg. From just selling pills, it was possible to buy anything from TVs to laundry detergent.

Subeconomies also developed around the illegal market. Since patients sometimes need to pass urine tests to get their prescriptions, clean urine was also worth a pretty penny. Child's urine went at a particular premium, up to $40 for a bottle.

Many pill mills weren't actually opened by doctors, so their success depended on the less reputable sort of medical practitioners. This is when so-called "locum tenens" lists were used. These were lists of doctors who were desperate and on the lookout for temporary employment. This was often because of license problems, substance abuse or simply because they were unable to obtain malpractice insurance.

A perfect ecosystem of abuse had been created, and it wasn't long before established drug dealers got in on the act.

### 6. The tiny Mexican village of Xalisco has had a huge impact on millions of American lives. 

While Oxy use was on the rise in the Rust Belt in the 1980s, migrants from Xalisco, a small Mexican village, started selling black tar heroin in the San Fernando Valley in the early 1980s. They went by the name of the _Xalisco Boys,_ and they quickly met with success.

Their business strategy was different from the other large heroin dealers. Instead of selling their product wholesale, they sold it retail.

Selling wholesale involves cutting the heroin each time it is sold on, thereby reducing its potency. Before the arrival of the Xalisco Boys, the powdered heroin sold on the streets was very weak, often containing as little as 1 percent heroin.

Black tar heroin was different. It began life as poppies growing in the hills near Xalisco. They were harvested by one member of the family, then refined into heroin by another. A third ran the cells that sold the heroin. The result was that the heroin arrived on the street at full potency.

The Xalisco Boys were also innovative in another way: their system operated similarly to franchises. The manufactured heroin was sent north to a wholesaler, who then supplied small, self-organized cells.

Each cell comprised an owner, usually based in Xalisco, as well as a manager, a phone operator and some drivers.

The operator took calls from addicts, and told them where to meet a driver, who they also kept in the loop.

The drivers drove around all day, their mouths stuffed with tiny balloons. Each had a single dose of heroin contained within. When the drivers met an addict, they spat out the necessary number of balloons and were gone in minutes.

And, if stopped by the police, the drivers would simply swallow the balloons.

The clever part was that the drivers were paid a steady salary, independent of the amount of product shifted, so they had no incentive to rip the addicts off or start cutting the heroin.

It was a good business model. A cell that started out earning $5,000 a day could make an incredible $15,000 a day within just one year.

> _"The Boys remained decentralized, resilient and adaptable. They embodied America's opiate epidemic: they were quiet, nonviolent."_

### 7. The Xalisco Boys soon expanded into new markets across the United States. 

The next part of the story has its roots back in 1947, the year methadone was invented. It was special because, although an opiate, it did not result in addicts constantly having to increase their dosages over time. It was therefore seen as a way that addicts could be treated: they could be supplied with a regular amount of methadone on a periodic basis to stop their addiction becoming worse.

Based on this principle, methadone clinics were founded. They provided an effective way of treating addicts too. However, over time, the idea took hold that methadone should be used to wean addicts off heroin completely. Consequently, for-profit clinics began reducing the amount of methadone they supplied to each addict.

The results were inevitable. Because these clinics weren't giving their patients enough opiates, populations of addicts began to search en masse for other means by which they could satisfy their addiction.

This is where the Xalisco boys came in. It was thanks to dissatisfied addicts coming out of these methadone clinics that they were able to expand out across the San Fernando Valley in the 1990s.

Drivers showed up at clinics and handed out free samples of heroin to addicts, along with a phone number they could call to get more. The clinics gave the Xalisco boys access to a new market they hadn't yet tapped.

It was also training for new drivers. They were taught to loiter near clinics looking for addicts. They came in close when they spotted one and asked for directions. At that moment they would spit out a few balloons of heroin and thrust the phone number into the addicts' hands. Spitball by spitball, this was how the Xalisco boys built up their client base.

By the late 1990s, the Xalisco Boys were the undisputed drug kings in many cities.

Take the effect they had on Central City Concern, a nonprofit organization running detox centers in Portland, Oregon. In the mid-1990s, 5 to 10 percent of the patients on their books were there to detox from opiates.

When the Xalisco boys showed up, everything changed. By 1997, heroin addicts made up 50 percent of the clinics' patients, and almost all of them had become initially hooked on Xalisco dope.

It took until 1999 for Portland officials to finally notice that they had an epidemic on their hands.

In 1991, heroin overdoses had resulted in ten deaths. By 1999, that number was 111. In just a few years, it had become the second-largest cause of death among men aged 20 to 54.

### 8. The Xalisco Boys’ approach to marketing was key to their success. 

If you want to run a good business, you've got to keep your customers happy and you've got to ensure that it's convenient to buy your product. The Xalisco Boys knew this well, and it was this approach that set them apart from other dealers. It also meant they were adept at marketing their product to an important demographic: middle-class white kids.

The Xalisco Boys didn't just hang around on street corners waiting for their customers to show up, like other heroin dealers. They frequently approached potential new customers with free samples and price breaks.

The Xalisco Boys even made follow-up calls to good clients: were they satisfied with the service and product they were receiving? The gang also monitored regular customers for signs they might quit, and even went so far as to give going-away presents of free heroin for customers who said they were checking into rehab.

Recently released prisoners were sometimes even given packs of dope when they got out of prison to get them hooked and to come back to the Xalisco Boys for more.

Their customer service was so good that one undercover cop working narcotics in the police department of Charlotte, North Carolina, even thought their service model was probably better than many legitimate retailers.

The Xalisco Boys' dealers were different in another way, too. Their drivers didn't carry guns, were almost never violent and didn't have much heroin on their persons at any one time. Because of this, if they were arrested and convicted, the punishments weren't very harsh: maybe a bit of jail time or, at worst, deportation. It was a good deal, and it meant that there was always a steady supply of Xalisco youths eager to leave home to earn lots of cash at relatively low risk.

On top of all that, because each cell effectively operated as a separate franchise, competition between cells was how each cell could differentiate itself. For a cell to succeed, their prices and customer service had to be top notch.

Competition also pushed the Xalisco Boys' cells into more territories. Working in less saturated markets could also drive success.

They even elicited addicts to help them push new business. For bringing in new customers or providing information about new markets, addicts were given product for free.

The Xalisco network had become monstrous: the authorities realized that something had to be done. On June 15, 2000, the FBI and the Drug Enforcement Agency launched _Operation Tar Pit._ In terms of geographic spread, it was the largest drug investigation in the country's history: there were busts in 27 cities across 22 states.

But despite the numbers, the operation caused more problems than it solved.

The arrests didn't get rid of the addicts; consequently, there was still plenty of demand for dealers. In the vacuum left behind by the Xalisco busts, competition between new suppliers lowered prices and actually increased the supply of black tar heroin across the United States.

### 9. The pain revolution left opiates triumphant – prescription was the norm. 

By the 2000s, the trajectory of the pain revolution was plain for all to see: opiates had emerged as the victors. Now, most of the United States' 100 million chronic pain sufferers were receiving them. The country was consuming 86 percent of the world's oxycodone, as well as 99 percent of its hydrocodone, another popular opiate.

In fact, hydrocodone has since become the United States' most prescribed individual drug. There are currently 136 million prescriptions for hydrocodone-related drugs each year, while opiate painkillers are the most prescribed class of drugs.

There were knock-on effects to these prescriptions too. As more opiates were prescribed, more people began to use them recreationally. Between 2002 and 2011, 25 million Americans took prescription pills in a manner for which they weren't intended.

On top of that, users were getting younger. In 2004, a study conducted by the Substance Abuse and Mental Health Services Administration showed that 2.4 million people 12 or older had tried prescription pills nonmedically for the first time within the previous year. That's more than the number trying marijuana for the first time, who are on average 22 years old!

At the same time, opiate overdose deaths rose from ten a day in 1999 to 48 in 2012. Additionally, by 2011, prescription painkillers had resulted in a tripling of emergency room visits when compared to seven years earlier.

But the really shocking statistic is this: a decade after the release of OxyContin, 6.1 million people in the United States had abused it — that's around 2.4 percent of the country's entire population.

Now, addiction to opiate painkillers isn't a new thing. Drugs like Lortab and Vicodin had been used by addicts for years, but their dosages were generally weak, and the pills contained substances like acetaminophen which, as mentioned earlier, limited the potential for abuse. Consequently, abusers rarely died from taking these pills.

OxyContin is different. It contains much higher doses and lacks those addiction-suppressing chemicals. And as for that special time-release coating, well, addicts soon found that they could remove it by sucking on the pills. This meant that pure oxycodone could be easily snorted or injected.

Additionally, patients who were requesting higher doses of Lortab or Vicodin were now being prescribed Oxy instead. This meant the potential for abuse was much higher, as Oxy contains much higher doses. In fact, many users went from orally taking prescribed pills to intravenous use as a way to up their highs. And, of course, once users got used to needles, it was all too easy for them to turn to the far cheaper substance of heroin and become serious addicts.

The number of heroin users in the United States rose from 373,000 in 2007 to 620,000 four years later. Of these, 80 percent had begun by using prescription painkillers.

### 10. Eventually, a few people noticed the increasing opiate abuse, and a backlash against opiate prescriptions followed soon after. 

Opiate abuse in the United States was now getting too big to ignore, and there was increasing pushback against unnecessary opiate prescriptions.

The first lawsuit against the producer of OxyContin, Purdue, was filed by a lawyer named Joe Hale. Hale was a public defender who often worked in an area called the Bottoms, a run-down neighborhood in Lucasville, Ohio, near Portsmouth.

In the late 1990s, he realized everyone in town was talking about pills they were calling OCs. And, apparently, many people were injecting them too.

Then, in 1999, Hale received a visit from the "unofficial godfather" of the Bottoms. The godfather's daughter had recently died of an OxyContin overdose, and he wanted justice.

And so it was that, in 2001, Hale filed the first wrongful death lawsuit with reference to OxyContin.

However, Hale's quest for justice didn't last long. After a few court dates in which he found himself facing down eight sharply dressed Purdue attorneys, he saw no alternative but to drop the suit.

Hale wasn't alone. In 2005, Ed Socie was working in Ohio's department of health when he noticed deaths from accidental poisonings were on the increase. He had a look at the data and came to the conclusion that almost all these deaths involved opiates.

But Socie had trouble getting his colleagues interested in his findings. However, in 2007, Christy Beeghly took over as his department supervisor — and she did take notice. In fact, she was utterly astonished. They realized at that moment that overdose deaths were about to overtake auto accidents as the number one cause of accidental death in Ohio.

That's no mean feat. Ever since its invention, the car had been _the_ leading cause of accidental deaths — until 2008, the year accidental overdoses took the crown.

What's more, the statistics painted a clear picture. The number of painkillers prescribed directly correlated with overdose deaths. Both had risen by over 300 percent between 1999 and 2008. It was hardly tendentious either: the correlation was a staggeringly high 97.9 percent.

### 11. The first major criminal case against the opiate manufacturers was brought forward in Virginia. 

It was becoming increasingly clear that the only way to stop the opiate crisis was to get to the source of the matter. There was no point rearranging the deckchairs — something big had to be done.

John Brownlee was among the first to attempt to face down the opiate epidemic. Brownlee had become a US attorney in 2001 at a time when people across Ohio, Kentucky and Virginia were dying of overdoses.

Brownlee began by doing what many other prosecutors were doing. They had been prosecuting the pill mill doctors, and Brownlee at first followed their lead. But when it was clearly going nowhere, Brownlee reassessed the situation and realized there was no way around it: he'd have to set his sights on Purdue. He subpoenaed all records related to the marketing of OxyContin.

The records showed that Purdue had been actively misleading in their marketing. They were claiming that OxyContin was nonaddictive, and they were doing it in all 50 states.

By fall 2006, Brownlee was ready and filed a case accusing Purdue of criminal misbranding.

He was able to show that despite Purdue's claims in their ads, they had in fact supplied no evidence to the FDA showing that OxyContin was less addictive than other painkillers.

It was also demonstrated that the company had been teaching sales reps to claim OxyContin was harder to abuse than other drugs, even though Purdue's own research didn't show that to be true at all.

On top of that, the reps had been told to say that patients who were using up to 60mg of OxyContin had the ability to stop at any point without suffering withdrawal symptoms. In fact, a 2001 company study showed this not to be the case at all.

It was a strong case and Purdue ultimately pled guilty to the charge of criminal misbranding.

Brownlee set October 25 as the deadline for Purdue to accept a plea agreement. Purdue agreed to pay a $634.5 million fine to avoid prison sentences for its executives, three of whom had pled guilty. Each was sentenced to three years of probation and 400 hours of community service.

And for Brownlee, it was just the start, as successful case followed successful case. The jewel in the crown was Pfizer, against whom he managed to levy $3 billion in fines for false advertising relating to several drugs.

### 12. It took time, but victims of the opiate epidemic finally began speaking out. 

One reason that it's critical to look at the effect opiates have had on the United States is that it took so long for the issue to be acknowledged. It had rumbled on and on, and there were now 16,000 fatal overdoses each year, but only a handful of parents' groups had formed with the mission of raising public awareness of the issue.

Jo Anna Krohn was one such parent. Her son had been high on Oxy and shot himself in the head while messing around with a gun.

It was while she was sitting by her comatose son's bedside for his last few hours of life that Krohn decided she had to speak out. If she were honest and truthful, and really pushed the issue, then maybe another family wouldn't have to go through this pain.

Krohn formed SOLACE as a group for parents mourning the loss of their children to opiates. She also started giving talks at schools in which she detailed her son's death. Soon, SOLACE had chapters in 16 counties.

Krohn's method isn't the only way to fight the epidemic. Brad Belcher found a particularly creative method. Belcher had been sober for five years and was tired of the fact that everyone was ignoring the heroin problem in his hometown of Marion, Ohio.

So one evening he ordered 800 large signs, and printed on each was "HEROIN IS MARION'S ECONOMY".

Late one night, Belcher started hanging them up all around Marion, but he was caught by the city authorities when he got downtown. By morning, only eight of the signs remained: the rest had been taken down.

However, a local drug dealer had seen one of the signs, took a photo of it and posted it online. The image went viral, and, consequently, the town realized it had to act.

The death of one celebrity in particular focused the minds of many. On Super Bowl Sunday, 2014, beloved actor Philip Seymour Hoffman died unexpectedly with a syringe in his arm.

In the days of grief that followed, media outlets around the country seemed to all, suddenly and simultaneously, grasp that the country had an opiate epidemic, some 15 years after it first began.

### 13. The pain revolution has been shown to be misguided – now the recovery has begun. 

Two decades after the start of the pain revolution, the tide finally began to turn, and a new consensus has formed as a result. Opiates should no longer be seen as necessary in combating pain. In fact, they can be considered risky at best: up to 24 percent of patients were reporting "aberrant" usage according to one 2007 study.

Purdue eventually acted too, but it was too little too late. In 2010, the company added a new ingredient to OxyContin: an abuse deterrent that made it harder to inject and abuse. If they had done this in 1996, then this story might have been very different indeed.

The FDA began requiring drug companies to educate doctors and patients about the risks of addiction for time-release painkillers. But, once again, this action was taken far later than it should have been.

All the while, OxyContin sales continued to rise. Purdue was netting an incredible $3 billion from sales of the drug each year.

But while they benefited, others suffered. According to the Center for Disease Control, fatal heroin overdoses tripled between 2010 and 2013.

Meanwhile, back in Scioto County, in the last year of their operation before they were officially banned in Ohio, pill mills there legally prescribed 9.7 million pills. The county only has 80,000 people in it.

Unfortunately, even after the pill mills had been closed for two years, doctors were still prescribing 7 million pills in the county each year.

But it's not all bad news. As the pain revolution is coming to its close, there are signs that from the wreckage of addiction, recovery is possible.

Portsmouth is one town that's showing the way. As former addicts ditched their drugs, they went on the lookout for a better quality of life. Gyms began to open up there, and former addicts even started setting up counselling centers to help others escape the grip of opiate addiction. People started to see former addicts in a new light: they faced fewer prejudices than before and found it easier to find work.

Now, people from all over the Rust Belt travel to Portsmouth to break their addiction cycles and get clean. In a complete turnaround, a real sense of community is growing in what was once a forgotten town. This part of the American Rust Belt is quietly rejuvenating itself.

### 14. Final summary 

The key message in these blinks:

**The opiate crisis currently plaguing the United States has its beginnings in the 1980s, when OxyContin, a new opiate-based drug used to treat pain, was introduced. It was designated as safe and treated as such, but addiction to this new drug was soon on the rise. At the same time, increasing amounts of heroin were entering the country through sophisticated drug networks. The failure of regulatory bodies and law enforcement to address the worsening situation has contributed to making drug overdose the number one cause of accidental deaths in the United States.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Chasing the Scream_** **,** **by Johann Hari**

_Chasing the Scream_ (2015) gives a riveting account of the first hundred years of the disastrously ineffective War on Drugs. Weaving together fascinating anecdotes, surprising statistics and passionate argumentation, Hari examines the history of the War on Drugs and explains why it's time to rethink addiction, rehabilitation and drug enforcement.
---

### David K. Randall

David K. Randall is a British journalist who became interested in the phenomenon of sleep when he injured himself by sleepwalking into a wall one night.

