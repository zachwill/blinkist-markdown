---
id: 541989116439610008510000
slug: drunk-tank-pink-en
published_date: 2014-09-18T08:00:00.000+00:00
author: Adam Alter
title: Drunk Tank Pink
subtitle: And Other Unexpected Forces that Shape How We Think, Feel, and Behave
main_color: F07496
text_color: A34F66
---

# Drunk Tank Pink

_And Other Unexpected Forces that Shape How We Think, Feel, and Behave_

**Adam Alter**

_Drunk Tank Pink_ probes the hidden psychological and social influences that shape the way we see, think, feel, and act in the world.

---
### 1. What’s in it for me? Learn what factors affect your behavior. 

The old wives' tale says that you should never show a red rag to a bull as it will provoke the animal to attack you. But did you know that the same advice applies to you? Although you probably won't charge at someone in a red shirt, your behavior toward them may very well be affected by their wardrobe choices.

In fact, you are constantly being subliminally affected by forces within you and around you — far more so than you can ever begin to imagine. Certain names trigger negative feelings, labels can create false memories, being watched makes you more honest, and being in a crowded place can even make you less helpful.

In these blinks, you will learn how your thoughts and actions are shaped by factors such as the colors around you, the letters in your name, the climate in your country, and the people in your surroundings.

You will also discover

  * why bright pink is a naturally occurring sedative;

  * why female lap dancers earn more money when they're ovulating; and

  * why hitchhikers should wear red.

**This is a Blinkist staff pick**

_"I am always interested in the subtle things that shape people's behavior. From pink prison cells to your initials, these blinks present astounding facts on what influences the human mind."_

– Laura, German Editorial Lead at Blinkist

### 2. Your name influences the life you lead by triggering strong mental associations in others. 

Guess how many baby "Adolfs" there have been since the Second World War? Hardly any? You're right.

Parents tend to steer away from names that have strong associations with negative concepts. And the world will forever associate any dear little "Adolf" with right-wing dictatorship.

Besides concepts, we also associate names with demographic information. This means that you can guess a person's approximate age, gender, ethnicity and even social status from his or her name.

For instance, most people would presume that Dorothy is a white female, Fernanda is Hispanic, and Aaliyah is black.

Studies have even revealed a strong relationship between a mother's education and the names she chooses for her children. For example, white boys named Sander are much more likely to have mothers who finished college than white boys named Bobby.

What's more, names even have the power to influence important life outcomes.

For instance, a study showed that job applicants with white names (Emily, Anne, Brad) receive callbacks 50 percent more often than applicants with black names (Aisha, Kenya, Jamal), though their applications were equally strong. This indicates, disturbingly, that names can lead to racial discrimination and shape the life outcomes of the name-bearer.

So we know that our names affect how other people see us, but do they also influence our own behavior?

Most definitely!

Psychologist Jozef Nuttin demonstrated that people like the letters in their own name so much, that they tend to donate more often and more handsomely to causes that share their initials. This meant that after Hurricane Katrina left New Orleans in ruins in 2005, charitable donations from people whose names began with K increased by 150 percent.

### 3. The labels we use shape what we see, bias our judgments and create false memories. 

"Black," "white," "rich," "poor." We are constantly giving labels to the things and people that fill our everyday lives. But how do these labels affect us?

Most importantly, labels shape the way we view the world.

Even the everyday labels we use in our native tongue can affect our perception of things. For example, when it comes to colors, the words we have at our disposal dictate how well we perceive different shades.

In one experiment, Russian and English subjects were given a color perception test: they were shown two blue squares with slightly different hues, and were asked which one matched a third blue square on a computer screen.

The results showed that the Russians were much quicker at this task because the Russian language has two distinct linguistic labels to describe different shades of blue: _goluboy_ for light blue and _siniy_ for dark blue. The Russians' more precise color labels made them more adept at perceiving hues of blue, especially at the border between these two shades.

What's more, labels can also bias our judgments.

In another study, subjects were asked to decide which of three faces with written labels was the darkest. Though the faces were identical in tone, people perceived the face labeled "black" as darker than the face labeled "white," suggesting that racial labels makes us incapable of judging skin tone accurately.

Finally, we now know that language can even create false memories.

In one study, two groups of subjects were shown a video of two cars colliding. Afterward, the group that was told by researchers that the cars had _smashed_ into each other, mistakenly remembered the presence of shattered glass when they recalled details of the video. However, the subjects that were told that the cars _hit_ one another remembered the details much more accurately. Clearly, labels can even distort our memory.

> _"Labels are immensely powerful, shaping not only what we see but also events that haven't actually taken place."_

### 4. Symbols are so powerful that they inspire reactions without us even being aware of them. 

Try to close your eyes and visualize the swastika. For most people in the West, this simple composition of straight lines triggers purely negative feelings.

This illustrates the power of symbols in influencing people and generating strong reactions.

Take, for instance, the diverse range of responses that banknotes — a symbol for money and wealth — trigger.

In one study, subjects' brains were scanned as they watched a video of hands destroying banknotes. The brain's _temporoparietal network_, which processes the way things should be used, became overly active, and the subjects reported feeling agitated and uncomfortable. This suggests that the misuse of money causes strong displeasure in people.

In another experiment, the mere suggestion of money made people better at solving tasks independently. Subjects were given intellectual tasks to figure out, and one group worked with a pile of Monopoly money beside them on their table. Everyone who took part was told that they could ask the researcher for help if they encountered difficulties.

The result?

Those who had the subtle reminder of money sitting on their table were much less inclined to ask for help, because the money, according to the researcher, reminded them of their independence.

Another symbol that triggers strong responses is the lightbulb. A psychology experiment even revealed that when researchers turned on a lightbulb — rather than a lampshade or a fluorescent tube — students were better able to solve tricky insight-based problems. This indicates that the lightbulb is so strongly associated with the concept of insight that it makes people have insights themselves.

So why are symbols this powerful?

Apparently because we perceive them so effortlessly, even unconsciously.

Thus, the students in the lightbulb experiment weren't paying conscious attention to the source of light in the room where they were sitting, since every darkened room has to be illuminated. But still, below the surface of conscious awareness, the lightbulb managed to shape their thoughts.

### 5. The mere presence of other people changes our thoughts and behavior. 

In 1970, a 13-year-old girl called Genie was rescued from parents who had forced her to spend her whole life immobilized and in complete isolation.

Her condition?

She could neither speak nor engage in basic social interactions, and, despite help, never attained normal developmental standards, demonstrating how vital interaction with other people is for the development of our own behavior.

In fact, our behavior is affected even when other people's presence is only hinted at.

In one experiment, researchers in a psychology department aimed to stop staff members taking coffee and tea without contributing a small fee. They found that just placing a picture of a pair of eyes in the kitchen made people more likely to pay a fee, indicating that the mere suggestion of someone watching is enough to make people more honest.

This also indicates that just thinking about other people's standards makes us scrutinize our own behavior.

The company Opower made use of this by releasing an app that allowed users to compete with their neighbors for the the title of "most energy efficient." It turned out that people consumed less electricity if they saw how much energy they were using relative to others.

So is the mere presence of other people always a good thing?

Definitely not.

Having people around us can also affect us in a negative direction, by _weakening_ our sense of personal responsibility.

This tendency to think that we don't have to intervene in a situation because someone else might come along to help instead is called the _bystander effect_. Real-life incidents have illustrated its gruesome consequences.

Take, for instance, the infamous murder of a woman called Kitty Genovese in Queens, New York in 1964. The attack by a knifeman lasted half an hour and at least a dozen apartment residents were witnesses — but none of them called the police during the attack.

Why?

They thought the responsibility to act lay with everyone else because there were so many people around.

### 6. Our basic drives for safety, love and reproduction influence our thoughts and behavior. 

We all eat, drink, breathe and (strive to) reproduce. And when this has been fulfilled, we also want to be assured that we have safety and love.

According to the psychologist Abraham Maslow, these are our most basic needs, and they guide our actions far more than we think.

Let's take a look at some of these basic needs, starting with _reproduction_.

To find out how this shapes our behavior, psychologists went to a gentlemen's club and investigated whether the tips the lap dancers received depended on where they were in their ovulation cycles.

Amazingly, the results showed that the dancers were tipped significantly more during their fertile phase, presumable because the men were picking up on physical cues from the fertile women. This shows that the men were motivated by sexual reproduction, and not just the dancers' feminine beauty.

What about _safety_? After we've gotten all our physiological needs covered, our desire for safety can be seen in our preference for the familiar.

In a psychology study, students were presented with a series of pictures of a group of strangers — some of whom cropped up more times than others — and asked whom they liked the most. It turns out that they had a strong preference for those they had seen more frequently, suggesting that we're inclined to familiarity, which signals safety.

So, once our safety has been secured, what do we crave?

Love, of course — a need which affects our attitudes toward others profoundly.

This is clear when we look at how the "love hormone,"_oxytocin,_ affects people's behavior.

Oxytocin is the natural chemical that drives mothers to care for their newborn babies. And surprisingly, studies show that when it is sprayed into the noses of research subjects, it makes them trust other people more. Subjects who got a small dose of oxytocin were much less suspicious when they were tasked to play gambling games with complete strangers. Thus our need for love — even if it's sprayed onto us — is a powerful force that brings us closer, even to strangers.

### 7. Our culture greatly influences the way we perceive the world around us. 

From chess clubs to global regions, the cultures we are part of influence the way we perceive the world.

This was demonstrated in an experiment where Chinese and American students were shown different photos with a central object against a background. Later, they were shown another series of photographs and asked whether they had seen the central objects before. The Chinese students had a harder time remembering whether they had seen the central objects before, when the objects were presented against new backgrounds, while the Americans had no difficulty with this task.

How could this be?

If we go back to ancient Western philosophy, we'll see that Westerners have always had a tendency to analyze objects in isolation from their contexts. On the contrary, Chinese philosophers have always been far more focused on the relationship between objects and their contexts.

This cultural difference also explains why the backgrounds in East Asian portraits tend to take up so much more space than those in Western portraits.

In fact, after examining 500 famous portraits from each culture, researchers found that in East Asian portraits the subject's face covers an average of only four percent of the canvas, whereas in Western portraits it's 15 percent!

A similar tendency applies to the way in which the two cultures perceive people.

In one experiment, American and Japanese students were asked to interpret the feelings of a cartoon person who stood in front of a group of four other cartoon people. Sometimes the group had the same facial expressions as the person in the foreground and sometimes they displayed different emotions.

Most of the Japanese students couldn't ignore the expressions of the group in the background, and therefore interpreted the man in front as less happy if the group was sad, and vice versa.

Again, this shows that East Asian culture takes the background and context into account. The Americans, on the other hand, didn't mind the background "mood." Instead, they saw the person in the foreground as distinct from everyone else.

### 8. Colors play a powerful role by affecting us physically and evoking associations. 

Do some colors affect you in certain ways? Does blue make you think of clear skies and put you more at ease?

The colors in our surroundings have been shown to actually affect us physically.

For instance, a series of experiments conducted in 1979 by Professor Alexander Schauss revealed that the color bright pink appeared to leave people temporarily depleted.

After staring at a bright pink cardboard, Schauss' subjects were much weaker physically — even barely able to resist as the researcher pushed their arms down.

This discovery inspired county jails to toss aggressive drunks into pink holding cells, and the color _Drunk Tank Pink_ was born. In fact, for many years, holding cell walls were painted pink to try and tame violent behavior.

The color red has also been shown to affect us physically, but in a quite different way to pink.

Studies show that people who are exposed to red light tend to shake more and feel more agitated, as the color elevates their blood flow and nervous system responses.

Not only are we physically affected by colors, we also link them to different concepts in our world.

One of the most common and universal associations is between the color red and romantic or sexual thoughts.

For instance, an experiment has shown that almost twice as many heterosexual male motorists stop when female hitchhikers wear red shirts than when they wear any other color.

However, the red shirt didn't make any difference to heterosexual female motorists, indicating that red enhances purely romantic and sexual appeal.

Researchers have found that red intensifies females' erotic appeal because women naturally experience reddening of their skin when they're sexually aroused.

So it's really no wonder that there's such a strong association between red and passion.

> _"Colors are powerful, not just because we respond to them physically but also because they remind us of the objects that embody them..."_

### 9. Our physical location and surroundings also play a distinct role in shaping our thoughts, feelings and behaviors. 

Whether you live in a city or on a farm, your physical location and surroundings can affect you powerfully. Depending on whether you're in a place that's quiet, noisy, empty or packed, your thoughts, feelings, and behavior will change.

One of the dimensions that guides our actions is the crowdedness of our surroundings.

This was shown in an experiment conducted among college students living in low-, medium-, and high-density places. The psychologists found that the students in the low-density locations were more likely to help out their fellow residents than the students living in the medium- and high-density places. For instance, they were much better at posting "lost" letters in the right mailboxes.

Another feature which strongly affects us is the "naturalness" of our surroundings. And as you might have already guessed, natural environments are good for us.

One study even showed that people recover four times more quickly from gallbladder surgery if they are occupying a room with a view of some trees, rather than a brick wall.

Similarly, researchers have found that children living in more natural environments are much more buffered against stresses than children who live in more man-made environments.

So why are "green" surroundings so good for us?

Psychologists think that natural environments promote well-being because they give us the chance to think as much or as little as we'd like, and thereby expose us to lower levels of stress.

Other, subtler cues in our environment also play a big role in our behavior.

Take, for instance, litter.

A study showed that if a person walks up to their parked car and finds a flyer placed on the windshield, they are more likely to just drop it on the ground if the parking garage is already full of litter.

Thus, dirty locations make people more prone to littering, showing once again that our surroundings shape us — for better and for worse.

### 10. The weather has a powerful effect on our mood and behavior. 

Be it sunshine, rain, sleet or snow, every time we leave our home, we expose ourselves to the weather. So the weather shapes every moment we spend outdoors, and this has some serious consequences for our behavior.

One of these consequences is aggression.

By studying baseball games and traffic on particularly hot days, researchers have found that when the temperature rises, aggression escalates, probably because the heat makes people uncomfortable, so they're more easily agitated.

Furthermore, scientists have found that crime rates rise and that the nature of crimes becomes more violent during hotter months. Consequently, the southern United States are more prone to violent crimes than other parts of the country, simply because they have to endure hotter summers.

On the other side of the weather spectrum, the cold of winter has a strong impact on our behavior: it brings us closer to each other, both figuratively and literally.

For instance, researchers have found that heterosexual men responded more positively to pictures of women in the winter than in the summer.

And also, by looking at birthrates and counting back nine months, we know that conception rates are higher in the winter.

Some researchers believe that this can be explained by the fact that physical coldness activates a sense of loneliness, which in turn leads people to seek social comfort.

But unfortunately, winter doesn't just lead to physical attraction and more sex. Sad to say, it can also lead to melancholy.

This effect can be seen most clearly in people suffering from seasonal affective disorder (SAD), where sufferers experience depression during wintertime, mainly because of a lack of daylight.

And while we're able to control many of the cues in our surroundings, we probably won't be able to tame the weather any time soon. Thus it can be considered maybe the strongest of all the forces acting on us.

> _"Bad weather brings us together, but protracted rain, snow, and darkness are also responsible for great unhappiness."_

### 11. Final summary 

**The key message in this book:**

Our thoughts and behavior are in many ways determined by our surroundings and culture. From the first letters in our names to the colors in our environment, the unlikely cues that direct our actions can be found everywhere. Understanding these ubiquitous influences can lead us to a better comprehension of broader behaviors, such as love, racism, aggression and helpfulness.

Actionable advice:

**Calm people down with pink.**

The next time you need to calm a person down or resolve a conflict, try to get them into a room with as much pink as possible. Bright pink, also known as Drunk Tank Pink, has been shown to take people's aggression levels down a notch or two.

**Don't be sucked into the bystander effect — ACT!**

If someone is calling for help, hurry up and come to their rescue, instead of assuming that they're someone else's responsibility. This is what everyone thinks when they're in a location with many other people, and this tendency can lead to really horrific incidents, where people who could've been saved don't get any support at all.

**Suggested** **further** **reading:** ** _Predictably Irrational_** **by Dan Ariely**

_Predictably Irrational_ explains the fundamentally irrational ways we behave every day. Why do we decide to diet and then give it up as soon as we see a tasty dessert? Why would your mother be offended if you tried to pay her for a Sunday meal she lovingly prepared? Why is pain medication more effective when the patient thinks it is more expensive? The reasons and remedies for these and other irrationalities are explored and explained with studies and anecdotes.
---

### Adam Alter

Adam Alter is a New York University psychology and marketing professor whose research deals with behavioral economics and the psychology of judgment and decision making. He has contributed to magazines such as the _New Yorker,_ the _New York Times, Psychology Today_ and _Popular Science_.

