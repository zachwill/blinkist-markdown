---
id: 54cf8a58323335000a3e0000
slug: duct-tape-marketing-revised-and-updated-en
published_date: 2015-02-03T00:00:00.000+00:00
author: John Jantsch
title: Duct Tape Marketing Revised and Updated
subtitle: The World's Most Practical Small Business Marketing Guide
main_color: 4089B2
text_color: 254F66
---

# Duct Tape Marketing Revised and Updated

_The World's Most Practical Small Business Marketing Guide_

**John Jantsch**

_Duct Tape Marketing_ outlines the most essential facts about effective marketing for your small business. It explains what techniques really work and how you can build a marketing campaign that will not only bring in customers but keep them and have them spread the word to their friends, too.

---
### 1. What’s in it for me? Master the easiest, cheapest and most efficient marketing techniques for your small business. 

If you studied marketing in college or business school, here's some bad news: You're way behind the times.

Today's marketing strategies are a far cry from those textbook examples. In fact, for small businesses, what marketing means has changed completely. Modern strategies are now much more practical and easy to apply in our technological age.

The strategy goes like this: Find people who have a problem you can solve, and make sure they know, like and trust you. But the challenge remains: How do you find those people?

As you'll learn in these blinks, it starts with a plan. Whether you're in desperate need of a marketing tune-up or ready to throw more cash at your successful early strategy, you need to ensure your messaging makes customers stick — like duct tape — and stay.

After reading these blinks, you'll know

  * how to talk to your ideal customer and get her to recommend others;

  * how to craft a great marketing purpose statement; and

  * why traditional advertising is far from dead or irrelevant.

### 2. A clear objective, a unique mission and a concrete goal make up a solid marketing strategy. 

Lots of people are full of ideas for colorful print ads or flyers, and think that's what it takes to know how to market their business best.

But before you dive into the details, take a step back. The first thing you need is a _general_ _marketing strategy_. A marketing strategy outlines _how_ you want to reach your ultimate goal.

First, you need to define your _objectives_, _missions_ and _goals_.

An _objective_ could be to become market leader; a _mission_ might be serving your customers honorably; and a _goal_ could be to double your number of customers within a certain period.

After you define these terms, you can think about _how_ you want to reach them. This is your marketing strategy.

There are three steps to take when developing a strong marketing strategy.

The first step is to figure out _who_ you want to serve. Find your niche and define your ideal customers. This will make it much easier to appeal to them with your specialized services.

Next, think about how you want to attract those customers. How can you stand out against your competitors? You might focus on offering a unique service, or higher-quality products. If you don't, your potential customers will just compare prices; and if you aren't the cheapest, you won't be the first choice.

When you've got the first two steps figured out, you can move to the third step, which is combining them. How will you bring your service to the right people?

Start by analyzing your business environment, like your competitors, and your customers' habits and needs.

### 3. Define your ideal customer and seek to solve their problems; they’ll stay loyal and refer friends. 

Marketing used to be based on trying to reach as many customers as possible. Nowadays, that's ineffective. Instead, you need to appeal to people who will really use your products.

This means you need to find your _ideal customers_. Who are they? What problems are you solving for them?

You want to leave your customers happy and satisfied, so they'll keep buying your product; and when that is true, they'll tell their friends about your products, too.

So how do you find these ideal customers?

Start by looking at your own business history. Make a list of all your clients and see which are satisfied and which groups bring in the most profit. The clients who are happy with your services as well as profitable are your ideal client group. They'll be loyal, and they'll refer you to friends.

Next, look for other clients in the same demographic. Find out what things attract them, and why they need your services. Remember, your product needs to _solve a problem_ for them.

In the end, you'll have a biographical sketch of your ideal customer. Make sure you can describe them in detail, so your staff knows how to spot them!

You can even create some specific characters and give them names, so you have something more tangible to think about when you imagine the ideal client. Before making any marketing decisions, ask yourself, "Would this appeal to Bob?" This will keep you on track.

You can also visit the places where your ideal clients go. If you're targeting people in the tech world, for example, you can attend some technology-related conferences and get to know your ideal customers on a personal level.

### 4. Create a core message that serves as the foundation for all your marketing activities. 

After you've found your ideal customers, you need to talk to them!

Every marketing campaign needs to have a powerful message that will convince your target customer group that your product is the best in your market segment.

So first, develop a _marketing purpose statement_. This isn't something you broadcast to your customers, but the foundation for all your marketing efforts.

Start out by saying out loud what you want your organization to be. You don't need to word it perfectly!

A window cleaning business developed a marketing purpose statement that set them apart from their competitors. They realized their competitors acted unprofessionally with clients; the company wanted to show that they'd treat customers with more respect.

Thus, their marketing purpose statement was, "We want people to know that we treat window cleaning as a profession, and that our people are true professionals who treat the homes they enter as they would their own." The company could then make sure all their future decisions aligned with this statement.

This sort of clarity and original thinking is what you should aim for!

Next, create a _talking logo_. A talking logo is a short statement that sums up the greatest benefit your clients get when they choose your company. It should make people ask, "Really? How do you do that?"

So choose a strong action verb, like "I teach...", name your target market, and say how you're going to help them. The window cleaning company's talking logo was, "We help homeowners see a better world."

Use this statement whenever potential customers ask you what your business is for.

Finally, create a _core marketing message_ for the public.

Look at your marketing purpose statement and talking logo, and come up with a short, succinct statement that can serve as a slogan for your business.

FedEx's uses, "On Time Every Time." The window cleaning company: "Your Pane is Our Passion."

> _"Find something that separates you from your competition; become it and speak it to everyone you meet."_

### 5. Tailor your marketing message to appeal to the different groups within your target audience. 

Just as you don't want to send out one message to as many people as possible, you also don't want to use the same marketing techniques for everyone.

Instead, tailor your marketing activities to certain groups.

Great marketing appeals to different customers in different ways, depending on their needs. So once you've determined who your ideal customers are, you can further subdivide them into separate groups.

_Suspects_ are people who fit the description of your ideal customer, but haven't been in contact with your business yet. _Prospects_ are those who've responded to you and want more information. They may have signed up for an email list, for example.

_Clients_ are people who've tried your services or products; _repeat clients_ keep coming back for new products or upgrades. Finally, _champions_ are those who tell others about your business and indirectly advertise for you.

Initially, you'll have a big group of suspects. In the end, you'll hopefully have a base of champions!

Real growth doesn't come from getting new customers to buy your product just once. It comes from regular customers who continuously purchase from you and refer others to you.

So you need to develop different ways of appealing to these groups.

For suspects, focus on getting their attention. The best way to do this is to offer test versions of your product, or free information about it. Free reports, workshops, books or checklists like "12 Ways to End Back Pain Now" are especially useful here.

Since prospects have already shown interest in you, offer them something so they'll become real customers. Trial services or discount offers can help pull them in.

Champions are your most valuable clients, so be very good to them. Memberships and affiliate programs can serve as great rewards for the work they do for you.

### 6. Optimize your web presence by building an attractive website and participating in social media. 

Does your business have a web presence? If not, you don't exist. Online marketing is crucial!

Interestingly, many assumptions people have about online marketing are _completely wrong_.

First, don't make people search for your telephone number or email address. Display your most important contact details on every page of your site.

Also, many business owners assume customers know the correct names of their products or services, but they often don't. So don't assume they'll know exactly what to search for! Be sure to research what keywords people are searching for online and figure out how to entice these people into finding and exploring your website.

Properly optimizing your website for search engines may well bring in customers, but don't rely on this strategy exclusively. Establish a presence on social media sites, advertise elsewhere online, and be sure to name your website in offline campaigns as well.

Your customers will expect your website to look good. How can you ensure this?

It's a good idea to hire a professional web designer. Have them keep your website simple, so content is clear and easy to access. Don't use too many large images, because they'll take longer to load.

Make sure your website is easy to navigate. Create some good, text-based navigation links. Your site should be so easy to navigate that a child could do it!

Also be sure to include video and audio content. This is good for SEO rankings, and it keeps visitors on your site for longer periods of time. It also makes your site more interesting.

Finally, give people a chance to give you feedback. Consumers are used to accessing ratings and reviews, so they'll be expecting them on your website too.

### 7. Offline ads are still effective; they should grab a viewer, making them stop and pay attention. 

Traditional ads might seem boring compared to online options, but they are still quite effective.

There are many forms of free advertising, but paid advertising is also important. The bottom line is that advertising gives you more credibility. When you can afford ads, people think your business must be doing well, so they'll pay more attention to your message.

And advertisements don't just get attention for your message, but they also promote your whole company as well. Journalists will be more aware of you; people will search for you on social media; and your employees can proudly show off their company to their family and friends.

When you create ads, there are certain things to which you need to pay special attention.

First, the headline. Your headline is your advertisement's heart — it should make people stop when they're flipping through the pages of a magazine. Spend more time on writing the headline than on any other part.

A strong headline mentions what you're offering or reaches out to the specific customers you're targeting.

Also, your ad needs to include proof of whatever you're promising to your customers. This could be a positive editorial, written by a happy person whose problem you've solved.

Your ad also needs to call your customers to some action. If you're advertising a limited offer on something, for instance, the ad needs to clearly explain how people can get it.

Once you've developed some ads, you need to test them to see which ones are the most effective. You won't just learn if the ad itself is good, but you'll be able to see if your chosen medium is the right one for your message.

When you've got a great ad that really works, try to beat it with an even better one! Your best working ad is called your _control ad_ — use it for as long as it remains effective.

### 8. Cultivate good relationships with bloggers or journalists to encourage positive press coverage. 

Everyone wants to have good public relations, and some people pay very handsomely for it.

What inspires them to do this?

Public relations have a huge impact on how your company is viewed. When people read good news about your products or services, they'll trust the message more if it was written by an objective third party. Newspaper articles are much more credible than ads, for example.

PR firms can also tell a personal story about your brand. When people learn how you started the company, how it grew and what its culture is like, they'll feel more connected to it.

Positive media attention also increases your employees' satisfaction. When people read good things about the company they work for, they'll feel proud to be a part of it.

The good news is that you can influence how the press writes about you. This is very important!

Get to know the most influential journalists in your industry; you'll need to learn about them before they learn about you.

Make a list of ten prominent and relevant bloggers or journalists, and read their work and social media profiles. What are they interested in?

Then get in touch with them. These days, most journalists have Twitter accounts or blogs where you can reach out and make them aware of who you are.

Strive to build good relationships with these writers. You can serve as a valuable resource for them, especially if you provide good content for their next article.

Finally, post information about important events or changes in your company on your own website. Create an "In the Press" section on your home page, where you can collect any mention of your company in the media.

And don't forget to sum up all this important news in press releases, which you can send to your clients and others in your network.

> _"Don't sell; build relationships."_

### 9. Strive to get good referrals from customers and other companies; your business will grow! 

Who would you trust more when you're deciding to purchase a new television? An ad, or your best friend?

Naturally, a friend will probably offer a better referral.

Getting good referrals is the most important aim of your marketing campaigns. When your business has a good reputation, these referrals will bring in more and more customers.

Both you and your clients benefit from referrals. Generally, people love giving this kind of advice to friends and family. They enjoy helping people they care about, and they demonstrate how clever they are by finding good products or trustworthy businesses.

Referrals are great because people trust them. When someone wants to go to a doctor, for instance, they almost never look one up on the internet. They ask their friends first.

New customers who come to you because they've heard good referrals are also usually willing to pay more for your services, because they already expect them to be good.

So how do you get good referrals?

First, make your customers happy. They'll only refer you if they are! So make sure you're doing all you can to satisfy their needs.

Be sure to provide your customers with opportunities to refer you to others. When they make a purchase, you might give them the option to post about it directly on social media, for example.

Also, remember that referrals aren't only important for your customers. When you're selling your services to other companies, you can also get referrals from them. So send them a supply of business cards, or pens with your logo. Their partner companies will see them too.

Finally, be sure to reward your referral sources. Offer a lower price to customers who refer you to their friends, or send gifts to other businesses who refer you to new customers. Don't forget to refer people back to them as well!

> _"Referral marketing can provide the greatest return on your investment of time and money."_

### 10. Final summary 

The key message in this book:

**If you want to succeed, strong marketing is absolutely essential. So outline your marketing strategy, clearly define your ideal clients and reach out to them with specific and meaningful ads. If you make your customers happy, they won't just keep buying your products but they'll also refer their friends, which is better than any advertising you can buy!**

Actionable advice:

**Tailor your marketing to your clients' specific needs.**

Remember that you aren't just targeting your ideal customers with your ads, but you need to go further. Craft ads that speak to the subdivisions _within_ your ideal client group, including suspects, prospects, customers and champions. The more your ads speak to each group specifically, the more they'll respond, bringing in more profits for you!

**Suggested further reading:** ** _Growth Hacker Marketing_** **by Ryan Holiday**

_Growth Hacker Marketing_ charts a major departure from traditional marketing practices, relying heavily on the use of user data and smart product design. This book illustrates how today's top technology companies, such as Dropbox and Instagram, have used this strategy to gain millions of users.
---

### John Jantsch

John Jantsch is a marketing consultant who specializes in small businesses. He's written several successful books, including _The Referral Engine_ and _The Commitment Engine_.

