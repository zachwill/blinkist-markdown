---
id: 57500afb7bea3d0003c715a5
slug: dude-youre-gonna-be-a-dad-en
published_date: 2016-06-09T00:00:00.000+00:00
author: John Pfeiffer
title: Dude, You're Gonna Be a Dad!
subtitle: How to Get (Both of You) Through the Next 9 Months
main_color: F2A659
text_color: 8C6034
---

# Dude, You're Gonna Be a Dad!

_How to Get (Both of You) Through the Next 9 Months_

**John Pfeiffer**

_Dude, You're Gonna Be a Dad!_ (2011) is the perfect guide for any anxious dad-to-be. Written by a man for men, these blinks tell you exactly what to think about, discuss and expect when you're going to become a dad, from conceiving to recovering after birth.

---
### 1. What’s in it for me? Become a kick-ass dad! 

For some people it comes as a shock, for others it's less of a surprise: but no man is ever truly prepared for those world-altering words from the lips of his partner: _I'm pregnant._

Having a baby is arguably one of the biggest events in a man's life, and it's only natural to feel overwhelmed, even frightened. However, being well prepared will make everything much easier. But how _do_ you prepare?

These blinks are for all the men out there who are about to have or are planning to have a baby. They will teach you some of the basic biology of pregnancy and birth, show you how to give your partner the best possible support (because, let's be honest, she's the real hero here) and what to think about once the baby has arrived. Because, dude, you're gonna be a dad!

In these blinks, you'll learn

  * how to avoid "parental freakout";

  * why you should always have lemons in the house while your partner is pregnant; and

  * how to handle renegade grandparents.

### 2. Prepare for fatherhood by getting informed about the next nine months! 

Are you going to be a father or are you planning on becoming one soon? If you want to make it through the next nine months, then it's time to get informed. Are you an expert on pregnancy already ? If not, don't fret: these blinks will provide you with everything you need to know. And the right information makes just about everything easier.

Even getting your partner pregnant is easier if you know certain things — like the details of her cycle. You've got a small window of opportunity to conceive every month, and you can't make the most of that window if you don't know when it is! If conceiving proves harder than expected, you and your partner may need medical assistance. Likewise, it helps to do your research here and learn how you can support each other through the process.

Once you've conceived, you should also know that there's always a risk of miscarriage. That's why you should choose your announcement date wisely. Usually waiting until the third month is the best option, so make a plan to break the news together. This is one of many great ways to bond during your partner's pregnancy.

Other opportunities to get closer include doctor's visits and ultrasound appointments. You might not be able to get to every appointment, but do make sure you're at the first, third and fourth. These are the times when you'll have the pregnancy confirmed, hear the first heartbeats and see the child via ultrasound for the very first time.

As your partner confronts all the physical challenges of pregnancy, you'll need to learn the different ways you can be there for her. To help manage the morning sickness that can linger into the afternoon (or the night), keep some lemons and ginger on hand to make tea, which is excellent for calming an upset stomach.

Exercise also helps, so encourage your partner to exercise regularly. And no, this probably won't work unless you get moving along with her! This is another great way to bond and shave off a few pounds while you're at it. Chances are that extra chores will keep you on your feet, too, especially if your partner continues working during pregnancy. Be ready to get cracking on those dishes without being asked!

Finally, your partner needs your support as her body undergoes dramatic changes. This can be quite a confusing and stressful experience for women. So, do what you can to show your love for her transforming body, and help her grow to love it, too. Be affectionate, intimate and reassuring (but avoid massages, as they might trigger contractions!).

> _"So love her body, whatever the shape or size. Encourage her. Touch her with familiarity, whether it's with a gentle neck rub or holding her hand at the mall."_

### 3. Use the second trimester to prepare for challenging stages ahead. 

If you've reached the fourth month, congratulations! Your partner is now far less likely to miscarry, and you two can start preparing for the baby's arrival. But what should you expect as you enter the second trimester?

First off, get ready for some heartburn, snoring, forgetfulness and mood swings as your partner's hormone levels fluctuate. Also, be aware that the second trimester means decision time — you'll soon be able to find out the sex of your child. Choose together whether or not you want to know what it is . The second trimester also marks the beginning of false labor pains. Work together with your partner to help distinguish between those pains and the real ones. False labor pains are triggered by movement, don't follow regular patterns and aren't debilitatingly painful.

The second trimester is also the best time to prepare for the final trimester. Before things get serious in that last stage, get as much of the baby stuff ready as you can. Why not hold a baby shower? Register things that you and your partner will need that cost over $50 — this will save you both a lot of time and money. Your partner will also be keen to start nesting and preparing the home, so be ready to do your part.

If you ever think of something that needs to be assembled, set up or purchased, write it down and get it done ASAP. For instance, a car seat isn't something you want to have to rush to buy and install when your baby's already there. Why? Because you're likely to install it incorrectly, which puts your child at risk. A 2008 sampling of 619 car seats in Pennsylvania revealed that almost 80 percent were installed incorrectly. Take the time to read the manual and practice setting it up. You'll be glad you did.

Finally, the second trimester is a great time to start picking a name, which is not always as fun as it sounds. Choose a silly name and your child may be the butt of every joke in elementary school; choose a common name and your kid will have four classmates with the same one. Take your time and find a name that you, your partner and other friends can agree on. Remember, a name is for life.

> _"Apple doesn't sound cool as a person's name, even if your mother is Gwyneth Paltrow."_

### 4. Get to know the birthing process itself as the due date draws near. 

As the third trimester begins, your baby's arrival is getting closer by the day! Your home is ready, and you've got a name picked. What's next?

It's time to come to terms with the birthing process. Birthing classes are a good option. Yes, those birthing videos can be confounding, but you'll need them to at least gain a basic understanding of what's on the horizon. Spend some time getting to know the hospital, too. How do you get to the reception area? Where's the nursery? After all, you'll be spending a pretty intense time here soon enough.

Refresh your knowledge of first aid and consider all the different possible delivery scenarios, too. Discuss with your partner whether she wants to have a vaginal or a cesarean birth. Do you want to save the umbilical cord blood or the placenta? If so, make arrangements to do so in advance. It's always advisable to make important decisions like these early.

Aside from all the preparations, make sure you enjoy your last months before parenthood begins. Having kids truly is life-changing — your priorities will never be the same again. So take a relaxing final vacation with your partner before the birth. It's also worth talking to your boss to discuss taking time off after the birth or when you have to be home if your child or partner is sick.

Your finances also deserve some thought at this point. Plan ahead and understand the costs — disposable diapers, for example, cost about $100 per month. Start thinking about saving early for your kid's education. You might have fewer luxuries in the years ahead, so enjoy your final vacation together on your old budget.

> "The latest numbers compiled by the Families and Work Institute show that more than 75 percent of married couples have both parents in the workforce."

### 5. It’s time for babywatch – so keep supporting your partner in the final weeks. 

Once you hit week 38, you'll be on constant _babywatch_. Ninety-eight percent of women will give birth anywhere between week 38 and 42, while only 4 percent give birth on their predicted due date.

During these weeks, be at your partner's side whenever possible. Have a hospital bag ready in case you need to dash to the delivery room — bring something to read, a camera, your contact information and your birthing plan. It's around this time that you might start experiencing "parental freak-out." If you feel anxious at the thought of impending parenthood, open up about it with your partner and work out how to cope if the other has a panic attack. As long as you're there for each other, you'll make it through.

This phase also comes with its health risks for your partner. Preterm labor — or labor before the end of week 37 — necessitates immediate treatment followed by bed rest. It might sound fun to lie in bed and watch Netflix all day, but your partner is more likely to feel trapped and frustrated. Be supportive, give her attention and keep her busy.

As the birth draws closer, your partner will have to think about the possibility of taking painkillers during labor, having the labor induced or getting a cesarean section.

Talk to your doctor and consider the pros and cons. Above all, let your partner make the right decision for herself and her body.

Finally, if you feel helpless as you stand by your partner, whether before or during the birth, remember how much she values and cherishes you for the supportive role you play. She needs you, and let her know how much you need her, too! Let your nerves show and don't hide your feelings. This is an incredible time for you both, so live it to the fullest!

> _"Let the baby go to the nursery as much as possible. Erase the guilt by reminding yourself that you will need every second of sleep"._

### 6. Communication, planning and a supportive relationship will help you transition into your new lives as parents. 

The birth is over! Your kid is here! And now you're going home together. Your new life has just begun.

Brace yourself for yet more decision-making. In fact, learn to welcome it. First up: breastfeeding. Will your partner go the natural way, which is healthier for her and the baby, or use formula, which can be more practical? As with everything, discuss it beforehand so you're both on the same page.

Take time out to care for yourself, too. Giving birth sure isn't easy, but being a new dad is no walk in the park, either. While the spotlight will be on mother and baby for the next months, you've got a considerable burden to carry as well. Studies show that not only women suffer from postpartum depression; 15 percent of new fathers do, too.

And what about your parents? Grandparents can be a great help and are awesome to include from the beginning of your kid's life. But remember to set boundaries: they'll be brimming with advice you might not particularly need (or want). If they think it's all right to give your kid sugar or that physical punishment builds character, stop them. You decide how your child is brought up, so make that clear to them.

Once you and your partner are back home, everyone will want to come and meet your newest family member. But remember to keep a visiting policy in place: you'll all need a lot of rest and your baby's immune system is fragile, so keep sniffling friends out!

Be cautious when it comes to sex. Your partner has just been through a very demanding physical process. Doctors typically recommend not having sex until at least six weeks after the birth.

Finally, sleep deprivation is going to be one of your biggest challenges. Studies have revealed that, during the first two years of a child's life, parents miss the equivalent of six months of sleep. How will you handle it? The same way you handled everything over the past nine months: by communicating, planning ahead and supporting each other!

### 7. Final summary 

The key message in this book:

**Pregnancy isn't just about mom: dads-to-be have an important supporting role to play. Spend the next nine months planning ahead for future challenges, working together, communicating and helping your partner out whenever you can. You'll be more than ready to be a great father. Good luck!**

Actionable advice:

**Consider finding a doula.**

Do you know what a doula is? These women are experts in supporting your partner as an informed and experienced teammate during labor while the doctor takes care of the medical requirements. From reducing stress with massages to telling supportive stories and giving pep talks, doulas can be a great help for you and your partner, so discuss with her whether you'd like to find one.

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Minimalist Parenting_** **by Christine Koh and Asha Dornfest**

_Minimalist Parenting_ (2013) presents a new conception of parenting: fewer rules and more listening to your gut. Rather than following the herd's latest parenting trends, Christine Koh and Asha Dornfest present an approach to raising a family that puts your personal and family values at the heart of your life as a parent.
---

### John Pfeiffer

John Pfeiffer is a financial advisor and author. As a father of three daughters, he holds the topic of parenthood close to his heart.

