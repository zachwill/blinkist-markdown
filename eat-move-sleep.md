---
id: 5694b3802f6827000700002a
slug: eat-move-sleep-en
published_date: 2016-01-15T00:00:00.000+00:00
author: Tom Rath
title: Eat, Move, Sleep
subtitle: How Small Choices Lead to Big Changes
main_color: E42E37
text_color: B0232A
---

# Eat, Move, Sleep

_How Small Choices Lead to Big Changes_

**Tom Rath**

_Eat, Move, Sleep_ (2013) offers simple tips for improving your health and well-being in some very important ways. You don't have to revolutionize your lifestyle to get in shape and increase your energy levels — little changes can make a big difference, and these blinks will show you how.

---
### 1. What’s in it for me? Kick your unhealthy habits out the door. 

How many diets have you tried in your life? Most of us have probably attempted to follow at least one strict eating plan at some point. And how many of us have benefitted from this in the long term? Likely very few. Chances are, many will have given up halfway through, while others would simply return to their unhealthy habits right after completing the chosen program.

So what should we be doing instead? These blinks, based on the views of Tom Rath, who has battled a long-term serious illness to lead a tremendously healthy and prosperous life, show that anyone can become healthy in the long run. All you need to do is focus on eating right, moving right and, crucially, sleeping right. 

In these blinks, you'll discover

  * why, when it comes to eating right, quality is better than quantity;

  * why going to the gym is pointless if you spend all day in a chair; and

  * why sugar is probably killing you right now.

### 2. Small lifestyle changes can have a big impact and increase your chance of living a longer life. 

A lot of people don't start taking care of themselves until their doctor utters some fateful words, likely along the lines of "You're out of shape," or "You'd better start shaping up if you want to make it to your retirement."

If you want to avoid this awkward conversation, heed this simple advice _before_ a doctor gives it to you. 

Living a healthy lifestyle isn't easy. It can be hard to say no to a can of soda or a larger portion of fries, but resisting these temptations will bring you great benefits. The sooner you start treating your body right, the better your chances of living a long and healthy life. 

In fact, researchers from the University of Gothenburg found that 90 percent of the population could live to be 90 years of age or older simply by making some important lifestyle choices. You can increase your energy levels, maximize your potential at work and sleep better just by eating the right foods, starting with a healthy breakfast. 

It's true that health is partly determined by your genes, but it's important to develop healthy habits regardless of your genetic makeup.

You can't change your DNA but you _can_ change your lifestyle. And your lifestyle itself has an impact on your genes!

Tom Rath, the author, is an extreme case of this. When he was only 16, he was diagnosed with a rare genetic disorder called _Von Hippel-Lindau_ (VHL), which shuts off the gene in charge of suppressing tumors, leading to the growth of cancers throughout the body.

Instead of giving up, the author focused on what he _could_ control: his diet, exercise and sleep. He learned how to decrease the odds of the cancers spreading and has managed to live a long and healthy life. 

Even if you have a genetic tendency to be obese, for example, exercise can reduce that predisposition by up to 40 percent.

### 3. Pay attention to every bite you take: eat more proteins and less empty carbs. 

Maintaining the right diet is trickier than it seems. You can always check food labels for nutritional information, but that isn't enough on its own. You have to think a lot more deeply about your food if you _really_ want to be healthy. 

There's one question you should ask yourself before you take a bite of anything: Is this a _net gain_ or a _net loss_ in terms of nutritional value?

People go on diets assuming they'll reach a specific end point. For example, they might hope to lose ten pounds by avoiding carbohydrates for two months. Diets are thus short-term; they're not enough for you to maintain a healthy lifestyle.

Long-term healthy eating means determining for yourself whether your food has a positive or negative effect on your body, and this means considering _all_ the ingredients that it contains. A healthy salad, for instance, can become a net loss if you add fried chicken and bacon to it.

Rath realized his favorite dish, salmon with barbecue sauce, was a net loss because of the sauce's sugar content. So he turned it into a net gain by learning to enjoy the taste of salmon on its own, without the sauce. 

Also, the _quality_ of what you eat is more important than the quantity. Don't just rely on counting calories — there's a lot more to your food than that. 

Overall, try to eat foods with a ratio of at least one gram of protein to each gram of carbohydrate. But that doesn't mean you can eat bacon and sausages all day! Processed meat should be _last_ on your list of proteins. 

Meanwhile, some foods should always be avoided, like potato chips, which have over 20 grams of carbohydrate for each gram of protein!

### 4. Limiting the time when you’re inactive is even more important than exercise. 

When it comes to staying active, going to the gym every two days isn't enough; you have to limit your _inactivity_, too.

Chronic inactivity is a big risk in the modern world, as a great deal of people have jobs that require them to sit at a desk all day. This only adds to our other inactive hours that we have while sleeping, driving somewhere or watching TV.

Reducing your chronic inactivity is even more important than doing short exercise sessions. In fact, a 240,000-person study by the _National Institutes of Health_ found that adults who spend the most time seated have a 50 percent higher mortality rate; even exercising for seven hours a week wasn't enough to reduce this.

If you sit for more than six hours a day, your risk of death increases at a rate similar to the risk of smoking or being overexposed to sunlight. Your good cholesterol drops by 20 percent after just two hours of sitting — and you'll start burning calories at a rate of only one per minute!

So try to be active in your daily routine by adjusting your habits. It's more effective than irregular exercise and it's easier too!

It's okay to start small. The idea of "getting in shape" might sound tough, but taking the stairs instead of the elevator is easy. Every task is an opportunity to fit some more activity into your day.

And stay active at home too! Home is the most convenient place to develop a more active lifestyle. Get a treadmill, elliptical machine or start doing aerobics with online videos. In addition, try to cut down on your TV time. People who watch over four hours of television per day are more than twice as likely to suffer a major cardiac event.

### 5. Getting enough sleep is a crucial part of staying healthy and productive. 

Few people get a proper amount of sleep these days. Sure, we're all busy — but sleep is one thing you should never neglect.

There's a common misconception that people who sleep less are harder workers; they stay up all night because they're endlessly productive.

But while staying up until 5:00 a.m. to prepare for the next morning's presentation might sound like dedication, it's actually quite the opposite. You're less alert on a day when you haven't slept enough the night before, so your presentation won't be nearly as good as it would be if you had gotten a good sleep. In fact, studies have shown that losing just 90 minutes of sleep decreases your alertness by one-third. 

Think of it this way: who would you rather have fly your plane — a well-rested pilot or a pilot who stayed up all night studying landing techniques?

There's an unfortunate reason most of us don't get enough sleep. In our fast-paced society, sleep is viewed as a sign of weakness. The author learned this from his parents as a child, so he stayed up late as a teenager even when he wasn't doing anything productive. Over time, this took its toll on his academic performance. He felt like going to sleep was wrong or lazy.

The highest-performing people actually tend to have healthy sleeping habits. You may have heard of the famous study that found that it takes 10,000 hours of practice to become brilliant at something. But did you know that same study also found that top performers get an average of eight hours and 36 minutes of sleep each night?

By comparison, the average American sleeps only six hours and 51 minutes on weeknights. Sleep is a vital part of reaching your goals, so don't write it off as a sign of laziness.

### 6. Sugar is a serious health hazard and should be treated like a controlled substance. 

Would you give your child a cigarette? What about a sugary snack? You might've answered "no" to the first question and "yes" to the second, but cigarettes and sugar are more similar than you think in terms of the harm they can cause the human body.

Sugar is a toxin and it's very unhealthy. Together with its derivatives, it fuels diabetes, obesity, heart disease and cancer. 

The average person consumes their own weight or more in sugar every year. Added sugar in processed foods is always unnecessary — healthy foods like fruits and vegetables already contain enough natural sugar as they are. 

Added sugar is only there to give your food extra flavor; it doesn't have any nutritional value and it's dangerous to our health. One Harvard University study found that sugary drinks contribute to 180,000 deaths every year!

Some nutritionists even call sugar "candy for cancer cells," because it accelerates aging and inflammation, fueling tumor growth in turn. 

Sugar is also addictive, which means it should be treated like any other drug. It's important to keep it under control so that it doesn't control us. 

Your brain gets excited and releases dopamine when you eat sugar, in the same way that it does if you smoke a cigarette. And like cigarettes, the more sugar you eat, the more of it you crave. You also build a tolerance to it, so you have to eat more and more of it to experience that same pleasurable sensation. 

The best way to avoid this cycle of abuse is to never start in the first place. Look out for other sugary products and ingredients too, like agave nectar, aspartame, dextrose, fructose, corn syrup and even honey. Some of those foods may be healthier than sugar, but they'll still increase your craving for sweet and sugary foods.

### 7. Quality is just as important as quantity when it comes to getting a good night’s sleep. 

Have you ever felt tired, even after sleeping for nine or ten hours? Sleep is an instinctive act and we all do it, but it's still something you can improve.

Since it's such a fundamental part of your health, it's important to sleep efficiently. _Efficient_ sleep is the time when you're really _sleeping_, and not just tossing and turning in bed. Some people might lie in bed for nine hours but only get five hours of real sleep. 

The _Rapid Eye Movement_ (REM) phase is the most important part of efficient sleep. This is when your brain processes your thoughts and memories, putting them into perspective for you. REM sleep also plays a key role in overcoming difficult or traumatic events. 

There are a few strategies to use for getting adequate REM sleep. 

First off, try to limit your use of the snooze button. Lots of us give ourselves an extra hour or half hour of sleep each morning, but that extra sleep isn't useful if it's broken up every five minutes. 

So set your alarm to go off at the latest possible time; let yourself get as much sleep as you can. 

You should also limit your exposure to artificial light before you go to bed. Artificial light has a negative impact on your melatonin, which plays a key role in regulating your sleep cycle. So if you read before bed, use a small reading lamp, and try not to watch TV or go on the computer just before you sleep. 

A good night's sleep impacts your health in a number of ways. In fact, research has shown that people with low sleep efficiency are 5.5 times more likely than efficient sleepers to develop a cold.

### 8. Final summary 

The key message in this book:

**Staying healthy isn't just one decision; it's dozens of little decisions every day. You don't have to reinvent yourself to get in shape and increase your chances of living a long and healthy life. Start by eating a lot of protein, avoiding excess sugar or carbohydrates, limiting stretches of inactivity and taking the stairs instead of the elevator whenever you can. Finally, get enough sleep and make sure it's the** ** _right_** **kind of sleep. A lot of little changes taken together can make a big difference.**

Actionable advice:

**Eat fruits and vegetables with dark and vibrant colors.**

There's no shortcut for knowing the healthiest foods, but going for dark, vibrant fruits and vegetables is a good rule of thumb. Eat anything green, like broccoli, spinach, kale or cucumbers. Red and blue fruits like apples, strawberries and raspberries are also highly nutritious. 

**Suggested** **further** **reading:** ** _Are You Fully Charged?_** **by Tom Rath**

_Are You Fully Charged_ (2015) is your guide to eliminating your off days, one positive interaction at a time. From socializing more to sitting down less, these blinks reveal easy-to-implement tips and tricks for generating the mental and physical energy you need, all by finding greater meaning in your life.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Tom Rath

Tom Rath learned the value of living a healthy lifestyle when he was diagnosed with a serious illness as a teenager. Though his illness could have killed him, he learned to take control of his body and live a healthy and fulfilling life by making positive changes in the ways he eats, moves and sleeps.

