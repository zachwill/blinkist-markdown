---
id: 534d583066336100078f0000
slug: eat-to-live-en
published_date: 2014-04-15T06:21:13.000+00:00
author: Joel Fuhrman
title: Eat to Live
subtitle: The Amazing Nutrient-Rich Program for Fast and Sustained Weight Loss
main_color: 22AB32
text_color: 187823
---

# Eat to Live

_The Amazing Nutrient-Rich Program for Fast and Sustained Weight Loss_

**Joel Fuhrman**

_Eat_ _to_ _Live_ gives readers a comprehensive overview of human nutrition, a re-evaluation of conventional nutritional wisdom, personal case studies and a practical dietary program with lots of recommendations. The reader can expect to learn about a number of different nutritional studies as well as the health benefits and repercussions of basic foods such as meat, milk, fish, vegetables and fruits.

---
### 1. What’s in it for me? Discover a simple strategy for a healthy diet, backed by cutting-edge research. 

Atkins, South Beach, paleo — do these names sound familiar? If so, you're probably one of the many people who's tried losing weight with the help of some kind of diet.

More than ever before, we struggle to maintain an optimal body shape in a world of readily and widely available food. It should thus come as no surprise that we're constantly bombarded with countless weight-management programs promising rapid weight loss and instantly improved health.

While _Eat_ _to_ _Live_ might appear to be just another dieting book in a never-ending series of dieting books, family physician and nutritional researcher Joel Fuhrman has actually produced a guide that aims to completely revamp the way we nourish ourselves. The book introduces readers to the benefits of a plant-based, high-nutrient diet intended as a lifelong dietary regimen that will result in excellent health and a prolonged life — hence the book's title, _Eat_ _to_ _Live_.

In the following blinks, you'll learn all about the problems associated with the typical modern diet, as illustrated by the American example.

You'll also discover what a diet that provides optimal nutrition is composed of, based on what nutritional science and many current case studies suggest.

Finally, you'll be introduced to a practical dietary plan that includes specific recommendations and recipes.

### 2. The typical American diet consists of high-calorie but low-nutrient foods. 

Even though Americans have access to a wide variety of healthy, nutritious food, they instead make unhealthy dietary choices, choosing to eat junk food like pizza, burgers and french fries.

Indeed, the typical American diet mainly comprises processed and high-caloric foods, such as refined carbohydrates (like pasta, bread and bagels), fat (oils) and animal protein (meat and dairy).

In fact, as research shows, the average American gets 62 percent of his or her calories from processed carbohydrates and extracted oils, 25.5 percent from fiberless dairy and animal produce, and just 5 percent from fruits and vegetables, excluding potatoes.

Consider, for example, that the human stomach can hold about one liter of food. Since food like french fries, cheese and meat are dense in calories, a stomach full of any of them contains approximately 3,000 calories. By contrast, a full stomach of much healthier foods, like greens, beans or fruit, would contain just 200 to 500 calories.

But aren't calories useful? Don't they provide the energy we need to survive?

While it's certainly true that these high-calorie foods deliver energy to the body, they hardly contain any of the nutrients that are vital to optimal health.

All foods contain calories and nutrients: calories come from carbohydrates, proteins and fat; nutrients come from vitamins, minerals and water. Although nutrients contain almost no calories, they're essential to the proper growth and development of the human body.

But just because a food contains many calories — as is typical in the modern American diet — this doesn't mean that it necessarily supplies a lot of nutrients to the body. For example, one tablespoon of olive oil contains about 120 calories. While this is more than 5 percent of the typical daily calorie consumption, it actually provides almost no vitamins or minerals.

### 3. This diet is based on incorrect nutritional wisdom. 

Given that the majority of Americans are obese, it's surprising that most of the population still prefers a diet rich in calories and poor in nutrients.

What's behind this behavior? Simply put: widespread societal myths that lead many people to believe they're actually making healthy food choices.

Indeed, America has derived very biased and narrow conclusions from its knowledge of other countries' diets.

For example, many Americans have grown up believing the myth of the healthy Mediterranean diet.

According to this myth, the famously good health of the people of Crete was due to their consumption of large amounts of olive oil, pasta and white bread.

But this is just one side of the story: the Cretan diet also used to include large quantities of vegetables, fruits, beans and fish. Furthermore, Cretans used to perform grueling physical activities every day. In other words, they were healthy _in_ _spite_ _of_ a calorie-dense diet of pasta, bread and olive oil, not _because_ _of_ _it_.

Moreover, since the 1950s, the picture has changed dramatically. Nowadays, Cretans eat a lot more meat, fish and cheese, and they're far less physically active than they used to be.

The result? Cretans are now as overweight as Americans.

Myths like this one are rife in American society. For example, the majority of Americans continue to believe that animal protein, as found in meat and dairy produce, is beneficial to their health because it helps people grow taller much faster.

However, many studies of animals have revealed that slower, not faster, growth goes hand in hand with a longer life. What's more, current research unequivocally shows that a quicker development and earlier puberty increase the likelihood of developing many different kinds of cancer in adult life.

### 4. Key actors, such as the food industry, the media, scientists and governmental agencies, uphold false knowledge about food. 

While prevalent myths about food have a huge influence on today's American diet, they're not the only determining factor. There are also certain deep-seated interests whose aim it is to preserve America's current food culture.

Take the United States Department of Agriculture: its original role was to promote the meat and dairy industries. Today it still clings to its institutional roots, promoting animal products to the extent that it allocates over $20 billion in price supports to the beef, veal and dairy industries. However, it offers no such financial support for the production of fruits and vegetables — at least, not the ones cultivated for human consumption.

Then there's the incredible amount of misinformation in the marketplace. For instance, in one case, newspapers printed sensational reports on new research with the headline: "High-fiber diet does not protect against colon cancer."

Given that more than 2,000 research papers demonstrate exactly the opposite, how could the news media make such a claim?

Basically, one study focused very narrowly on investigating the links between fiber _supplements_ and cancer, which in no way indicates that a true high-fiber diet — one based on vegetables, fruits and whole grains — doesn't prevent colon cancer.

The effect of misinformation can be seen also in the popularity of the Atkins Diet. This is a ketogenic diet that prescribes an increased intake of animal products and a decreased consumption of carbohydrates. The problem is that Atkins is extremely dangerous: it can cause a shift in electrolytes that can lead to a (potentially fatal) irregular heartbeat.

In fact, one sixteen-year-old girl died suddenly while on the Atkins Diet, and many other sudden deaths have been linked to similar forms of ketogenic diets.

The prevalence of food myths and the influence of dominant actors might explain the very gradual awareness of the fact that the contemporary American diet is a crucial factor in the population's current state of poor health.

### 5. The typical American diet causes adverse health effects and serious diseases. 

According to the latest research, the state of the population's health reveals that there is something wrong with the well-being of American people.

Even though today's Americans are lucky to have an unprecedented level of material wealth, they suffer from obesity and a number of other chronic diseases.

In fact, almost 75 percent of all Americans are considered obese. Furthermore, the current trend indicates that, by 2048, _all_ American adults will be obese. This is a very disturbing development, as obesity has been linked to premature death due to heart attacks and a variety of potentially fatal diseases, such as cancer.

What's behind this unwelcome development? As a huge number of reputable scientific studies show, the development of serious diseases is the direct effect of a diet that comprises a high consumption of animal products, refined foodstuffs and sugar.

Consider, for example, one large-scale nutritional study that investigated the link between diet and disease — the China-Cornell-Oxford Project (the results of which were published in the book _The_ _China_ _Study_ ). This study examined a number of regions of China with different, well-established dietary habits, and its remarkable results suggested a strong link between disease and the consumption of animal protein. In fact, instances of cancer and heart attacks were found only in the regions where animal products were part of the established diet, while the regions where very few or no animal products were consumed were nearly cancer free.

Furthermore, these findings are backed by the Physicians' Health Study, which linked diets that include an excessive amount of dairy consumption to a variety of diseases. For instance, the study demonstrated that daily consumption of 2.5 servings of dairy increased the risk of prostate cancer by 30 percent.

### 6. A plant-based diet fulfills all nutrient and energy requirements of the body. 

Given the dangers associated with a diet rich in animal products, it makes sense that people should seek an alternative diet based on the best nutritional wisdom available.

Ideally, it's a plant-based diet because most of the nutrients essential to good health — all the important vitamins, mineral and phytochemicals — can only be found in high concentration in plant-based foods.

For example, consider that 100 calories of broccoli provides 2.2 mg of iron, 46 mg of magnesium and 118 mg of calcium. In contrast, 100 calories of steak provides 0.8 mg, 6 mg and 2 mg of these minerals, respectively.

Much like E=mc² is the key formula in physics, H=N/C, or Health=Nutrients/Calories, is the key formula in nutrition. This proportion is known as _nutrient_ _density_, and the higher the ratio in any given food, the better the nutritional value.

For example, dark leafy vegetables — like spinach, Brussels sprouts and broccoli — and other greens, beans, legumes and fruits are the most nutrient-rich foods. In terms of the nutrient density scale, such foods score between 50 and 100 points, while whole grains score 20 and refined sweets 0.

Although many people seem to think they wouldn't get enough calories from a diet based solely on plants, a plant-based diet actually delivers approximately 1,000 to 2,000 calories per day. Also, some people believe that they require more calories because they exercise a lot, but they should remember that their bodies will tell them if they need to eat more.

Moreover, plant-based foods also contain a more-than-sufficient amount of carbohydrates, fat and protein for a healthy diet.

Spinach is of 51 percent protein, while a cheeseburger or meatloaf are only 20 percent protein. This means that you'd need to eat a lot more calories of these less nutritious foods to get the same amount of protein that spinach provides.

Furthermore, seeds and nuts contain healthy fats in sufficient quantities, in particular the essential fat omega-3.

### 7. A plant-based diet leads to sustained weight loss, protects against illness and can even reverse chronic diseases. 

Although science has only begun to investigate the benefits of a plant-based diet, recent examples and personal experiences emphasize just how beneficial whole, natural foods are to human health.

One clear benefit is that such a diet can lead to weight loss as it prevents overeating. That's because there's a complex system of receptors in the digestive tract that tells the brain when the stomach is full. This occurs when the stomach is filled with sufficient nutrients — which happens quickly when fruits and vegetables are eaten — and when it's filled with a lot of fiber, which is readily found in natural foods.

Take Scott, for example, who had difficulties controlling his weight since he was very young. Before he started to consume foods rich in nutrients, he weighed over 500 lbs, and left his house on only a handful of occasions per year.

Now on a plant-based diet, Scott has lost 333 lbs and reduced his body fat from 62 percent to 10 percent.

Another benefit of this diet is that plants contain many substances that curtail the development of diseases.

Indeed, as the chairman of Harvard's influential Department of Nutrition stated: "The most compelling evidence of the last decade has indicated the importance of protective factors, largely unidentified, in fruits and vegetables." A tomato, for instance, contains over ten thousand of these "protective factors," known as phytochemicals.

Consider the effect of a plant-based diet on the development of cancers. Cancer develops as a result of damage to our DNA. Studies have demonstrated that the nutrients in plants are able to diminish and, in some cases, even reverse this damage. Furthermore, other studies have seen equally positive effects of plant-based nutrients for people who suffer from migraines, diabetes, autoimmune disorders and other illnesses.

### 8. Raw and steamed vegetables, beans and legumes, and fruits may be eaten in unlimited quantities. 

Since the _Eat_ _to_ _Live_ plan is not a typical short-term diet, there's no portion control. Rather, the plan is based on consuming as many greens, beans and fruits as the dieters like. In fact, they're encouraged to eat as much of these foods as they can.

In particular, raw and steamed greens can be consumed in abundance. They are considered superfoods as they have the highest nutrient density. The greens with the highest nutrient density scores are vegetables such as kale, collard greens, Swiss chard, romaine lettuce, broccoli and cabbage.

One advantage to eating these foods is that the body uses more energy to digest them than they provide. In other words, they have a negative caloric effect.

The _Eat_ _to_ _Live_ plan advises consuming more than one pound of raw and one pound of steamed vegetables per day.

Secondly, beans and legumes have a number of positive health benefits. Beans contain high amounts of resistant starch, a substance connected with weight loss, digestive health, lower blood levels and diminished risk of heart disease and cancers.

Nutritional science has also linked legumes, regardless of whether they're brown beans, peas or soy, to long life in various countries. And so, it's recommended that dieters eat at least one can of beans and legumes a day.

Finally, a minimum of four fruits should be consumed daily, as they're essential for the success of the _Eat_ _to_ _Live_ diet. Fruits are especially good for those people with a sweet tooth because fruits generally are sweet and thus sustain the motivation to continue with the diet.

### 9. Other foods should only be consumed in limited amounts or avoided altogether. 

If greens and fruits are considered the most beneficial foods, nuts and seeds are a close second. Lowest in priority are all other foods.

That's because seeds, nuts, starchy vegetables and whole grains provide healthy fats but also have many calories.

For instance, a recent study found that seeds and nuts are linked to the prevention of heart attacks and cancer. Nevertheless, they are extremely rich in calories, so should be used only in moderation, and complemented by physical activity.

It's also true that starchy vegetables, like potatoes and squash, can be valuable additions to a healthy diet, but they are also very calorie dense, which means that some people find it hard to lose weight when including them in their diet.

Apart from the above foods, all other foods are optional and should be avoided, or at least they should constitute only 10 percent or less of all consumed calories.

Optional foods are those that don't contain any healthy substances required by the body. They contain none of the important vitamins, mineral or phytochemicals that are readily provided by a plant-based diet. The only exception is vitamin B12, which is found only in animal products. Dieters should take a supplement to obtain this vitamin.

Thus, foods like sweeteners, oils and salt should be avoided altogether, or their consumption should be limited to very small amounts, as they are obviously detrimental to health. In addition, consuming more salt than foods naturally contain has been linked to stomach cancer and hypertension.

Finally, while it's common for people to receive negative signals from their bodies when they start the _Eat_ _to_ _Live_ diet, these are merely signs of detoxification that indicate that their body is beginning to repair and get better. If dieters are disciplined and stick to the plan, these initial effects will pass after some time.

### 10. This nutritional program is easy to implement. 

If you base your nutritional choices on the _Eat_ _to_ _Live_ plan, you'll maximize your health. Also, you won't have to worry about counting calories or preparing sophisticated meals.

The only thing you'll have to ensure is that you follow one basic rule of thumb: your diet should comprise mostly greens, beans and fruits.

More specifically, it's recommended that you stick to the 90 percent rule: you should consume at least 90 percent of your calories from unrefined plant-based foods in order to reap all possible health benefits. This means that you're allowed to eat just one small cookie or around half a bagel daily.

Furthermore, meals should include massive portions of greens. Dieters should keep in mind that their success is determined solely by eating more of the right foods. In contrast to other diets, the _Eat_ _to_ _Live_ plan requires no sophisticated formulas.

Another way in which this diet is easy to implement is that meals can be easily prepared on a daily basis. Again, there's no need for sophistication: recipes can be simple and easy, yet still extremely effective. At first, you might find your meals lacking in taste, but after a while your taste buds will reset and your appreciation of the taste of fruits and vegetables will increase.

For example, the author eats a large bowl of fruits for breakfast, and a salad made of beans, peas or broccoli for lunch.

Moreover, dieters are encouraged to think of the salad as the "main dish." Eating a lot of salad will fill you up, and it ensures that your intake of nutrients is maximized while consumption of calories is minimized. Again, you can be creative with this salad, as there's room for variety in the diet: for instance, iceberg lettuce in the morning, mixed baby greens for lunch, and romaine for dinner.

With just a few simple recommendations, the _Eat_ _to_ _Live_ plan promises to provide a solid foundation for sustained weight loss and optimal health.

### 11. Final Summary 

The key message in this book:

**Optimal health can be achieved by following a plant-based diet based mainly on raw and steamed greens, beans and legumes, and fruits. The diet doesn't rely on portion control or excessive exercise, both of which often only yield temporary positive effects. Instead, by following this diet as a lifelong plan, dieters can expect to _sustain_** **weight loss and superior health.**

Actionable advice:

**Optimal health is earned, not inherited.**

Remember, optimal health has nothing to do with your genes. It depends on the food choices you make for yourself every day. Make sure that you treat this knowledge as a piece of personal empowerment.

**Eat more of the right food.**

Think twice the next time you're thinking about eating a candy bar. The pleasure you take from it will be over in a second, while the calories go directly to your waist. Think about eating as much vegetables and fruits as you like instead: they contain the healthy ingredients your body needs to function at its best.

**Keep it simple.**

Focus on the essentials — greens, beans and fruits — and make sure you base your diet on them. Use a can of beans with your salad, make a simple vegetable soup or a basic fruit salad. With time and experience, the meals you prepare will become more sophisticated.
---

### Joel Fuhrman

Joel Fuhrman, M.D., is an American family physician and nutritional researcher. Fuhrman specializes in using the wisdom of nutrition and nature to prevent serious diseases. He has collected substantial scientific material and many case studies proving the effectiveness of his recommendations. Other books he has published include _Eat_ _for_ _Health_ and _Disease-Proof_ _Your_ _Child_.

