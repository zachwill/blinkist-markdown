---
id: 531714013764650008910000
slug: eating-animals-en
published_date: 2014-03-05T12:18:11.000+00:00
author: Jonathan Safran Foer
title: Eating Animals
subtitle: None
main_color: 82C051
text_color: 4E7330
---

# Eating Animals

_None_

**Jonathan Safran Foer**

_Eating Animals_ (2009) offers a comprehensive view of the modern meat industry and demonstrates how the entire production process has been so completely perverted that it is unrecognizable as farming anymore.

The book explains the moral and environmental costs incurred to achieve today's incredibly low meat prices.

---
### 1. Factory farms are more factory than farm. 

When most people think of a farm, they think of barns, pastures, red wooden houses and barnyard animals grazing peacefully.

This is history. 99% of all land-animals farmed in the US today come from so-called _factory farms_ : industrialized, streamlined production facilities which bear zero resemblance to the "farms" in most consumers' imaginations. A factory farm is more like an assembly line, where each animal is another unit to be processed as quickly and cheaply as possible.

The logic behind factory farms can be summed up with one word: _efficiency_.

Over the past century, farm animals have been bred to be so fast-growing that they are slaughtered as soon as they reach adolescence. This unnaturally rapid growth induces such severe hereditary health-problems that they are often unable to survive outside the factory farm.

Sick and injured animals are left to die where they fall. Any form of care, even mere rest and water, are considered inefficient and thus not provided.

Artificial lighting and ventilation ensure the animals' internal clocks continually push them to grow. At the same time their feed is supplemented with vitamins and antibiotics to keep perpetually unhealthy creatures alive until slaughter.

Labor is minimized through automated herding, feeding and slaughter, but the few workers used are usually poorly paid and under constant stress, leading to mistakes and even deliberate sadism.

If you believe that the animals your chicken nuggets or pork chops are made from have ever seen the light of day or felt grass under their feet, you are living a fantasy of bygone times.

Today, animals are a nameless, faceless mass being processed.

### 2. Factory farmed poultry is both ethically and hygienically revolting. 

As per the factory farmer's efficiency ethos, chickens are divided into _broilers_ (fast-growing chickens bred for meat) and _layers_ (fast-laying chickens bred for laying eggs).

Due to optimized breeding used since the advent of factory farming, layers now lay eggs at twice the rate they used to, and broilers' daily growth rate has increased by 400 percent.

This extreme growth makes the birds totally unviable outside the farm, making chickens effectively an entire species on artificial life-support.

On a factory farm, layers live in coops stacked 9 stories high with less than a square foot of living space per bird. Broilers are kept on the floors of massive rooms in flocks of tens of thousands.

In such cramped conditions the birds often go insane, pecking at each other constantly. To avoid this, their beaks are sliced off with a searing hot blade. This is the equivalent of cutting the fingers off a human, leaving the intelligent and inherently curious creatures without their primary exploration device.

At slaughter, adolescent birds endure pain and terror as the stunning and slaughter machines often botch the job, leaving them writhing in pain till the end.

The meat is then pumped full of various broths to make it look, smell and taste more like chicken. The master stroke is immersing it in so-called "fecal soup", a cooling liquid oozing with pathogens and feces from dead birds, to soak up some 20% more weight. The process virtually ensures cross-contamination of the meat by any diseases individual birds may have carried.

Thus the poultry industry reaps a 20% windfall profit from selling consumers feces and bacteria as chicken meat.

### 3. Hog farming is the height of animal cruelty. 

Pigs reared on factory farms suffer on a multitude of levels. Perhaps the most stress-inducing is the way that pigs' so-called _species-specific behaviors_ are suppressed. Pigs naturally want to act like pigs, rooting around in mud, playing, building nests and sleeping communally in hay. When they are confined to cramped, steel-and-concrete-lined multi-tiered factory farms, they can do none of those things and suffer greatly.

Sows endure the worst of this. They are hormonally manipulated to be pregnant virtually constantly and are forced into tiny gestation crates where they cannot move, much less nest and prepare for their piglets as is their natural instinct.

The piglets themselves know suffering from the start. Their tails and _needle teeth_ are removed within 48 hours of birth, since at a factory farm they would otherwise constantly bite each other out of frustration.

The piglets' testicles are also removed (without anesthesia), simply because consumers today prefer the taste of castrated meat.

The piglets are initially kept in stacked wire-cages, where feces and urine drip from animal to animal. Eventually they are forced into pens so cramped they cannot move, thus saving calories and fattening up quickly. According to an industry magazine: "Overcrowding pigs pays."

As piglets grow, the smaller ones are "thumped" to death, i.e. held by their hind legs and smacked headfirst into concrete, because they are not growing fast enough to be profitable.

Sometimes even multiple thumping fails to kill them, so they end up running around in agony with grotesque injuries, like an eyeball hanging out of its socket.

### 4. Fishing and fish farming constitute a war of extinction against all aquatic life. 

Modern industrial fishing methods and fish farming share the same efficiency ethos as factory farms.

We easily ignore the suffering of fish, seeing them more as a faceless commodity than as individual animals. Thus they are often treated even more cruelly than many other animals, and are in fact being systematically annihilated. Scientists predict a complete collapse of all fished species within the next 50 years.

_Underwater farming_ forces salmon into such crowded conditions and foul water that they bleed from their eyes, cannibalize each other and attract sea lice in such quantities that many fish faces are eaten to the bone (a phenomenon known as a "death crown").

Farms with a 10-30 percent death rate are considered to be doing well. Before slaughter, the fish are starved for seven to ten days, then have their gills sliced and are left to bleed to death, writhing in pain.

Wild fish may have a better life than their farmed cousins, but their death is equally agonizing and results in tremendous collateral damage. The keyword is _bycatch_, which is aquatic life other than the target species caught and killed through the process of fishing.

The worst offender by far is the method of _trawling_, where a funnel-shaped net is dragged on the ocean-floor for hours, mostly in an effort to catch shrimp. The result, however, is an average 80-90% of bycatch, which is thrown back into the sea, dead.

Though not as ruthless as trawling, _long-lines_, another dominant industrial fishing method, also kill some 4.5 million sea-animals each year as bycatch.

Both fishing methods inflict prolonged agony as the helpless fish either dangle on hooks for hours or are scraped along the bottom of the ocean.

### 5. Employees at factory farms and slaughterhouses become brutal and sadistic. 

There are no real farmers at factory farms.

Labor has been automated so thoroughly that only desk jobs and menial tasks, such as slaughter, remain. Low-paying work in highly stressful and dehumanizing working conditions can brutalize the employees, making them sadistic towards the already suffering animals.

On chicken farms workers have been documented tearing the heads off birds, breaking their bones, spitting tobacco juice in their eyes and stomping on them to watch them "pop".

Pig farms induce sadism as well. Workers have been found to beat pigs with wrenches, shove iron poles and cattle prods into their vaginas and rectums, slice off their snouts and drown them in manure. One video even showed workers skinning a pig while the animal was fully conscious.

Other animals also get their share of abuse. Baby turkeys have been used to play baseball with and slaughterhouse employees have knowingly dismembered fully conscious cattle.

This is a common phenomenon. Deliberate acts of cruelty occurred regularly at 32% of slaughterhouses observed in announced audits. Just imagine what unannounced audits would have uncovered!

Managers seem to condone their employees' actions, and any sanctions, much less prosecutions, are extremely rare.

### 6. Eating meat is environmentally unsustainable. 

Whether to eat meat or not is one of the most significant environmental choices you make. According to the UN, livestock produces around 18 % of global greenhouse gas emissions, a full 40% more than the transportation sector.

An omnivore contributes seven times the greenhouse gas a vegan does. Meat-heavy diets are becoming more common globally in countries with rapidly expanding populations like China. As this trend continues, greenhouse gas emissions are set to grow radically.

For developing countries facing food or water shortages, the increase in meat consumption is also a worrying trend. By 2050, the amount of food diverted to feeding livestock would be enough to feed 4 billion people, and even today 50% of China's total water consumption goes into animal farming.

But the environmental problems do not only exist in far off places like developing countries. In the US alone, animal farming produces some 87,000 pounds of animal feces per second, most of it from factory farms.

While animal feces can be a valuable fertilizer, such extreme quantities on a small area are simply too much for the environment to sustain.

Worst of all, the waste is incredibly toxic: 160 times more polluting than raw municipal sewage. Chicken, cow and pig shit has already polluted some 35,000 miles of rivers in the US alone. Regulations are blatantly ignored even in the few places where they do exist, and in just three years 13 million wild fish have been poisoned by shit.

Liquefied feces collect in vile lagoons the size of Las Vegas casinos, seeping into waterways and into the air. Families living near hog farms frequently complain about nosebleeds, headaches, diarrhea and burning lungs, while the value of their land plummets.

### 7. The meat industry often bends regulatory authorities and the law to its will. 

Food companies exert tremendous influence over public institutions. Much like tobacco companies, they lobby congress to eliminate unfavorable regulations, press authorities not to enforce ones that exist and challenge any unfavorable court decisions.

Consider the _United States Department of Agriculture_ (USDA): It is charged with promoting the health of the nation through its nutritional guidelines, but paradoxically also with promoting the agricultural industry.

This conflict of interest means that the USDA cannot proclaim, "eating less meat is healthy," because it would come under fire from meat industry interest groups.

Or look at animal rights: 96% of Americans say animals deserve legal protection and 62% believe strict laws should be passed to govern the treatment of farm animals. And yet it is legal to put 30,000 chickens in a cramped, fully enclosed room with a tiny, closed door and label them "free-range".

In fact, animal treatment on factory farms is so cruel that even very weak animal welfare legislation would ban it. To get around this, _Common Farming Exemptions_ (CFE) are granted to any practices common in the industry.

Incredibly, even the most unimaginable animal cruelty becomes instantly legal if it is widely adopted by the industry. And as we know, the industry will do anything to increase efficiency.

Another example of industry power is the use of antibiotics on factory farms. Several prestigious institutions like the _Centre for Disease Control_ and _World Health Organization_ (WHO) have called for a ban on the overzealous use of antibiotics on livestock, because it decreases their effectiveness and can even create resistant strains of pathogens.

Thus far the industry has managed to oppose such a ban in the US.

### 8. The price of meat is low because it does not reflect the true production cost. 

From an economic perspective, factory farming has made meat very cheap — impossibly cheap, in fact.

In the past 50 years, the prices of houses and cars have increased around 1500 percent, but the price of eggs and chicken meat haven't even doubled. Why?

One reason is that many of the costs of the meat industry are borne by the rest of society. Factory farms do not pay for the treatment of their waste, the development of new antibiotics to replace ones they are making obsolete through unnecessary use, nor for the lives taken by the viruses that their farming methods produce.

But the primary driver behind today's impossibly cheap meat is the complete abandonment of any kind of effort to treat animals well.

If animals had been reared on small family farms with traditional methods, allowing them to go to pasture, feed on grass, roll in the mud or walk in the sun, we could never afford to consume the amount of meat per capita as we do today.

Instead, to maintain low prices, we have crammed increasingly sick animals into even smaller spaces, given them increasingly large quantities of chemicals, and caused them greater suffering.

But what is the cost of cruelty? The actual price of meat may not have kept up with inflation, but as a result its production is now so grotesque that most people don't even want to know how it happens. What should be the price of tortured flesh?

### 9. Factory farming makes us sick today and will inevitably cause the next global pandemic. 

Factory farming is making us sick. According to consumer reports, 83% of chicken meat sold is infected with either salmonella or campylobacter. Some 76 million cases of food-borne illness occur in the US alone every year.

Since factory farms use almost 25 million pounds of antibiotics annually for non-therapeutic use (i.e. not to actually treat sickness), we can expect more antibiotic resistant strains of pathogens emerging from factory farms.

The way in which our food is made is sickening, and it is making us sick.

In addition to sickness today, factory farming could well bring about a doomsday event. The WHO is convinced the world is overdue for another _influenza pandemic_, which will affect every country on the planet, and for which we are grossly underprepared.

The most notorious pandemic occurred in 1918, when the Spanish flu wiped out some 50-100 million people. The virus was recently identified as avian influenza — a disease transmitted from birds to humans.

Birds, pigs and humans are somewhat vulnerable to each others' flu viruses, which is why they offer dangerous breeding grounds for viruses to mutate. A pig infected with influenza viruses from two different species could allow these separate diseases to combine into something new and deadly.

Where might one find pigs or chickens packed into cramped spaces in unsanitary conditions, bleeding from open sores and not being cared for when they become sick?

Consider for instance that some 30 to 70 percent of factory farmed pigs will have some sort of respiratory infection by slaughter.

It is inevitable that the next global super flu will originate in a factory farm.

### 10. There is no rational justification for treating dogs differently from pigs, chickens and fish. 

Most people think of dogs as intelligent, emotional creatures, more companions than mere animals. Suffering inflicted on dogs would make most of us angry, because we know that dogs are evolved enough to experience pain and fear, just like humans. We empathize and would certainly think twice about eating a dog (at least in a Western culinary context).

But step back for a moment and consider why we afford dogs this special status in our hearts? Is it because of their intelligence?

Pigs are even more intelligent than dogs and display learning capabilities exceeding those of even chimpanzees. They work in teams, communicate in their own language and come to the aid of other pigs in distress.

Fish and chickens are also far more intelligent than once thought. Fish form relationships, use tools and interact socially, while chickens have been found to be at least as intelligent as mammals, possibly even primates.

Pigs, fish and chickens all feel pain and fear just like our beloved dogs, so why do we ignore their suffering? From a sentimental perspective one might argue that the proximity of dogs to our daily lives grants them a special status, but that is all it is: a sentimental argument.

From a strictly rational standpoint, we should care about the suffering of pigs, chickens and fish, just as much as dogs.

****

### 11. It is almost impossible to eat ethically without being vegetarian. 

For anyone even remotely interested in preserving the environment, protecting the welfare of other living things or just avoiding the next global flu pandemic, becoming a vegetarian is really the only ethical and rational choice.

Choosing what to eat (and what not to) is one of the most powerful ways we can demonstrate our values, and stop meat industry giants from wielding the power they do today.

While non-factory farmed, "ethical" meat does exist, you can safely assume any meat you buy without painstaking research is factory-farmed.

Even ethical meat has its issues: most slaughterhouses are owned by meat industry giants, which means profits still end up in the pockets of the worst offenders.

Some may interpret this book as encouragement to start buying ethical meat while continuing to also eat factory farmed products. Nothing could be further from the truth.

The very least we can do is to stop giving money to factory farms altogether. An honorable form of _omnivory_ may be possible through specialized farms and wiser animal agriculture, but at the moment vegetarianism is the simplest ethical choice to make.

Some might call vegetarianism sentimental. But consider which is more sentimental: blindly indulging in what you feel like eating at any given moment, or rationally considering that there are more important factors than passing desires and sentiments?

****

### 12. Final summary 

The key message in this book is:

**Almost all our meat is produced in factory farms, resulting in immense suffering for animals, major environmental damage as well as all manner of current and future health problems for humans.**

The questions this book answered:

**How is meat produced today?**

  * Factory farms are more factory than farm.

  * Factory farmed poultry is both ethically and hygienically revolting.

  * Hog farming is the height of animal cruelty.

  * Fishing and fish farming constitute a war of extinction against all aquatic life.

  * Employees at factory farms and slaughterhouses become brutal and sadistic.

**How does the meat industry affect us and the environment?**

  * Eating meat is environmentally unsustainable.

  * The meat industry often bends regulatory authorities and the law to its will.

  * The price of meat is low because it does not reflect the true production cost.

  * Factory farming makes us sick today and will inevitably cause the next global pandemic.

**Why is eating meat unethical and irrational?**

  * There is no rational justification for treating dogs differently from pigs, chickens and fish.

  * It is almost impossible to eat ethically without being vegetarian.
---

### Jonathan Safran Foer

Jonathan Safran Foer is a promising young novelist from New York who has written the international bestsellers _Everything is Illuminated_ and _Extremely Loud & Incredibly Close_.

Contemplating the responsibilities of fatherhood lead the author to question what kind of diet he wished to provide his firstborn son. Eating Animals documents his findings and reflections on the topic.

