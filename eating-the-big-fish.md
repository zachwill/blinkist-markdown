---
id: 5950e03fb238e10005bb49bb
slug: eating-the-big-fish-en
published_date: 2017-06-30T00:00:00.000+00:00
author: Adam Morgan
title: Eating The Big Fish
subtitle: How Challenger Brands Can Compete Against Brand Leaders
main_color: 51AFCE
text_color: 316A7D
---

# Eating The Big Fish

_How Challenger Brands Can Compete Against Brand Leaders_

**Adam Morgan**

_Eating the Big Fish_ (2009) gives a strategic overview of how second- and third-tier brands can challenge industry leaders and climb to the upper echelons of the business world. These blinks are full of concrete advice to help emerging brands make a name for themselves in competitive markets.

---
### 1. What’s in it for me? Make people sit up and take notice of your product and brand. 

These days, walking down major streets in city centers across the Western world can get a bit monotonous. Whether you're in Barcelona, London, Hamburg or San Francisco, you always see the same signs belonging to huge franchises such as Starbucks, McDonald's and H&M.

While this can be tiring or even annoying as a consumer, it's far worse for new companies that want to market their products; they're constantly fighting an uphill battle to be seen and heard alongside such enormous multinational companies.

However, these _challenger_ _brands_, as they can be called, are crucial to keeping the market open, creative and healthy, and help provide interesting alternatives for consumers. In the end, it is indeed possible to succeed as a challenger brand, just as long as you respect certain key principles and keep some useful strategies in mind.

In these blinks, you will learn

  * how to create a strong identity for your brand;

  * what advantages you have over established brands; and

  * how even cleaning products can be turned into something trendy.

### 2. Market leaders are a force to be reckoned with, but emerging brands can still pose a challenge. 

When you were in school, did you ever compete with your peers over grades or in gym class? If you did, this competitive spirit probably led to you coming face-to-face with someone who always seemed to have the edge over you — and the world of business poses a similar situation.

In business, these powerful rivals are called _brand leaders_. They are established firms with clear competitive advantages over the brands below them, which are known as _challenger brands_. As a result, leaders naturally enjoy a higher rate of profit than challengers.

For instance, data collected in 2007 by the Profit Impact of Market Strategy, or PIMS, found that the return of an investment placed by a brand leader in Europe is 40 percent, while the rate for the second-ranked brand is just 26 percent.

In the United States, firms put up comparable numbers, with the leader getting a 32-percent return on their investment and the number two brand bringing in an 18-percent return.

Simply put, brand leaders get more bang for their buck. This higher level of profit also means that they invest more and, therefore, make _even more_ profit. They can even invest in long-term projects like researching ways to increase their competitive edge down the line.

But that doesn't mean that brands in the second position or lower are entirely outgunned — these companies simply need to think differently.

While it's great to be the best brand on the market, it's still pretty good to be number two, three or four. Just take the American car rental service, Avis. The company knew it wasn't the leader in its industry; it was clear that their fierce competitor, Hertz, held that title.

In fact, Avis was closer to the rear of the pack. But with an incredible advertising strategy, they managed to climb into second position and close Hertz's considerable competitive advantage.

And that's just one example of how challenger brands can strut their stuff. While they might need to work harder and do a bit more to keep up, they can make great leaps through creative approaches.

But before you learn more about the mind-set challenger brands should adopt, let's take a closer look at the hurdles they face in today's market.

### 3. Challenger brands have to contend with increasingly skeptical, distracted and stressed-out consumers. 

When people are stressed, they tend to stick with what's comfortable: their favorite restaurant, their most worn-out T-shirt and the album that's been stuck on repeat in their headphones. But for the small store down the street that just opened, this human tendency can pose serious problems.

After all, since people's attention has become harder to capture, marketing has become a tough nut for challenger brands to crack. For instance, we text on our way to work, use our cell phones at work and take them out of our pockets to listen to the radio, browse the internet or read blogs.

In short, we're busy — and amid this constant distraction, our attention spans are waning. Just take a 2006 study conducted in the United Kingdom, which found that 36 percent of texting occurs in front of the TV. Competing with this incredible flow of information is the task facing challenger brands, and it's not one to be underestimated.

But what makes this project even more difficult is that people feel increasingly strapped for time, which stresses them out and makes it even harder to grab their attention. Surveys have revealed that people need time each day to recuperate and that, during this time, they prefer silence. As such, they're sure to avoid pop-ups and marketing ads that might distract them.

People are exhausted by the modern-day barrage of information and want a few minutes to themselves here and there. Therefore, it's a struggle for marketers to reach their consumer targets — at least without irritating them in the process.

Finally, people are also increasingly skeptical of marketing. A US study even found that consumer trust for brands dropped from over 50 percent to around 25 percent between 1997 and 2006. In this climate, there's no reason to assume that a consumer will gladly absorb and accept your marketing strategy. Brands simply tried too hard to push sales in the past, misled consumers a bit too much and left their target demographics mistrustful of the entire practice of marketing itself.

So, there are some serious difficulties facing challenger brands, but this isn't the half of it. Next up, you'll learn which other barriers these brands need to break through.

### 4. The boundaries between different products are blurring, thereby increasing competition. 

Whether it's black or white, big or small, humans have developed ways of categorizing the world around them. This practice is certainly helpful at times, but when it comes to products, marketers need to be wary of making categorizations.

Product categories are blurring like never before and marketers can't rely on the same classifications they used to. Marketers still pay close attention to these categories of products or services, but their target audiences often perceive them entirely differently.

For instance, imagine you are the founder of Flickr, the online image and video hosting platform. What category do you think your product falls under? Perhaps social networking and entertainment?

Well, consider the fact that one consumer used Flickr to find a hotel in Hawaii. Does this make Flickr a research tool, a travel agency or a photo-sharing platform?

The point is, the boundaries are bleeding into one another.

And technology is blurring these lines even further. Just think of an iPhone — is it a phone, a camera, a browsing device, all three or much more?

Because of these changes, categories are now whatever consumers want them to be, as they will always find different ways to use the products they buy. This makes paying excessive attention to categories a total waste of time for marketers.

But this shift has major implications for business at large. Competition is no longer confined to your "category" of business but is instead much broader. Suddenly, there are plenty more products for you to compete with.

Imagine you want to sell iPhones, but since the categories are blurring, you're not just competing with other smartphone retailers; you're also going head-to-head with camera companies like Nikon and Canon.

Now that you've got a good sense of the difficulties challenger brands face, let's take a look at the characteristics that make it possible for them to pose a threat to brand leaders.

### 5. The lack of experience common to many challenger brands allows them to ask essential questions. 

There are all manner of challenger brands out there, but regardless of how varied these companies are, they all have eight common characteristics. These are the _eight credos_ and they describe how these brands tend to behave.

Let's take a closer look at some of these credos:

The first is called _intelligent naivety_. This one is especially important, since being able to intelligently use what little experience you have can enable you to ask the right questions and develop a powerful challenger brand.

In fact, experience is much less important than most people think — and it can even become a stumbling block.

After all, when you've been in the same industry for decades, it can be a challenge to look outside the norms of your field. As a result, experienced brands tend to be blinded by the conventions and procedures of their companies, as well as the fields they operate in.

However, when you have little or no knowledge of a specific field, you're much more likely to ask yourself the questions you need to transform your area of focus. Just take Method, a home cleaning product created by Eric Ryan in 1999. Ryan knew absolutely nothing about cleaning products, but had a great design background.

He asked the question: Why can't a cleaning product come in a designer bottle, rather than one that's purely functional?

From there, by observing the various home makeover TV shows, he realized that cleaning products could be a powerful symbol for creating an elegant home ambience. So, with a friend of his, he created a well-designed and ecologically friendly cleaning product that became a core aspect of household design.

Today, Method is the seventh-fastest growing product among all packaged goods in the United States. This rapid success was only possible because of Ryan's essentially nonexistent experience with the field he endeavored to enter. He simply applied his knowledge of another field to shift the perception of his product.

### 6. Strong values and emotional relationships with customers are vital for challenger brands. 

Nowadays, lots of companies base their businesses on identifying problems — like feeling tired because of a heavy workload — and offering solutions, like a soft drink that's tasty and highly caffeinated.

But this classic approach is far from what a challenger brand will do. Another credo of successful challenger brands is having a _lighthouse identity_, which pushes consumers to embrace the brand's values.

Lighthouse identities make their primary marketing activity the intense communication of a particular opinion or belief and precisely why they hold it. In effect, this creates ownership of a category or product and communicates how the company thinks the world should be.

Consider the Spanish shoe brand, Camper. They quickly built a powerful brand identity by continuously stressing their extreme dislike for the rapid pace of the world. They perfectly captured this perspective in their motto, "Walk, don't run."

Slow living then became a value that many of their consumers sympathized with and adopted, leading them to buy products that matched this belief. What was even more perfect in this example was that, for Camper, the slow living image they cultivated was easily reinforced by the slow pace of Mallorca, the island on which the shoe brand was launched.

But that's not all strong lighthouse identities can do — they can also build emotional relationships with customers. This is an essential step, since challenger brands will never succeed simply by offering convenience or appearing trustworthy; it's also important for such brands to cultivate strong relationships with their customers.

Just consider Apple, which has been wildly successful at getting customers to identify with their products. As a result, if you have an Apple computer, it immediately says something about your creativity and originality. Or, to put it differently, consumers emotionally identify with Apple products because they want to align themselves with what Apple stands for.

### 7. Habits tend to dictate consumption, but powerful symbols can change that. 

After finding that a particular cheese satisfies your taste buds, do you keep looking for new, interesting flavors, or do you more or less stick with the variety you know you like?

Well, for most folks it's the latter and, in general, people consume based on regular habits, which makes perfect sense for practical reasons.

After all, over the course of an average day, most people have a lot to take care of; there's work, raising kids, sudden emergencies, like a sick pet or vehicle breakdown. This all means that, when people are faced with making purchasing decisions, whether it has to do with groceries, T-shirts, or cleaning products, they basically go on autopilot; that is to say, they don't actually make a decision, they just go with what they've bought in the past.

This is where the _symbols of reevaluation_ come in. This is yet another credo of challenger brands and it's essential to breaking classic consumer habits.

With this tool, challenger brands employ powerful symbols and actions to awaken "sleep shopping" consumers, cause them to rethink a particular category, brand or product and reconsider why they were or were not buying it.

This is precisely what the discount retailer Target did. As a means of brilliantly relaunching their brand, the company partnered with prominent American architect Michael Graves, who created the brand's first designer product line.

Not only that, but the products were showcased somewhere you'd probably never associate with Target: the Whitney Museum of American Art in New York City. By working together with Graves and landing, of all things, a museum show, Target forged a powerful symbolic message that made consumers reconsider their opinions of a brand that they had, until then, likely considered a cheap and unfashionable option.

So jump right in and challenge the consumer habits. Just remember: when you're staring down a fierce competitor, you've got to be even fiercer yourself!

### 8. Final summary 

The key message in this book:

**Brands don't have to give in just because they aren't number one in the market. While there are certainly obstacles facing small and ambitious companies, they also have advantages that other, more experienced brands don't enjoy.**

Actionable advice:

**Limit yourself to two marketing actions.**

As a challenger brand, your marketing budget is naturally going to be limited, which is why it's important for you to spend those funds in the most effective way possible. Generally speaking, just one or two of all the marketing actions you take will make up 80 percent of your brand's marketing success, so it's a good plan to focus on them.

To identify these marketing approaches, begin by listing your brand's marketing goals over the next year. From there, cut out the least important goal, then the next-to-least important and so on. Eventually, you'll be left only with your two most important goals.

From there, do the same thing with all the marketing activities you're planning to implement to achieve these goals. After cutting away all the rest, you'll be left with the two actions that you should invest in for maximum marketing return.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How Brands Grow_** **by Byron Sharp**

In _How Brands Grow_, Byron Sharp tackles conventional marketing wisdom, disproving many of the conventional marketing myths with scientific facts and establishing some scientifically proven principles marketers should use.
---

### Adam Morgan

Adam Morgan is a best-selling author and the founder of the renowned marketing consultancy _eatbigfish_, which helps clients build powerful marketing strategies.

© Adam Morgan: Eating The Big Fish copyright 2014, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

