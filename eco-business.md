---
id: 556c57be3236630007110000
slug: eco-business-en
published_date: 2015-06-01T00:00:00.000+00:00
author: Peter Dauvergne and Jane Lister
title: Eco-Business
subtitle: A Big-Brand Takeover of Sustainability
main_color: AF473E
text_color: 963D35
---

# Eco-Business

_A Big-Brand Takeover of Sustainability_

**Peter Dauvergne and Jane Lister**

_Eco-Business_ (2013) unpacks corporate sustainability initiatives to reveal a business model that has far more to do with profit and market share than earnest environmentalism. The tools and tactics described in these blinks allow businesses to cut costs and maximize profits, all in the name of the environment. Yet through firms' collaboration with governments and NGOs, some corporate eco-initiatives can actually have a positive effect on the environment.

---
### 1. What’s in it for me? Discover the real reason why companies are exploring sustainable practices. 

In your local supermarket, take a look at the section for cleaning products. It's a fair bet that you'll notice row upon row of "eco" products, with green labels that tout the company's "sustainable" credentials.

From fast food to furniture, companies in every industry are trying to show the market that their practices are eco-friendly and if you buy their products, you're doing your part to save the world.

But this is far from the truth. As these blinks show, corporate sustainability practices today are mostly designed to increase profits and market share. Once you've discovered the secrets of corporate environmentalism, you may never look at another "green" product the same again.

In these blinks, you'll discover

  * why Greenpeace and Coca-Cola share a common cause;

  * how a company can track a single banana from Peru to your breakfast plate; and

  * why a soft-drink company is more powerful than the United Nations.

### 2. Eco-businesses are primarily motivated by profit and not by concerns for the health of the planet. 

What comes to mind when you hear the term _eco-business_?

You might think about a company that restructures its business practices out of concern for the environment. But in fact, the pursuit of profit is the main driver of the eco-business trend.

How can this be? Well, increasing the sustainability of a company is, above all else, a great way to cut manufacturing costs and increase efficiency. In other words, becoming an eco-business allows a company to make and sell more goods.

The Fairmont Royal York Hotel in Toronto invested $25,000 CAD to upgrade its water systems as part of an energy conservation program. The result? A more efficient and less wasteful system that generated over $200,000 CAD in annual savings.

So from a business standpoint, being sustainable is smart. But isn't it also good for society and the environment? Actually, not really. In the long term, eco-businesses have a negative impact on social justice and environmental conservation.

That's because, as we've mentioned, supply chain efficiency facilitates accelerated production at lower per-unit costs. In other words, companies make more stuff for less.

As a result, the total environmental impact of an eco-business _increases_ even as the per-unit consumption of water, energy, material and waste _decreases_.

Consider Coca-Cola. A decreased per-unit cost of production means lower costs, which allows the beverage company to meet increasing demand. But this accelerated production has had a taxing effect on its sugar cane suppliers. In fact, soil fertility in Papua New Guinea has dropped by 40 percent within the last two decades, due to increased production of sugar cane.

Coca-Cola's story also hints at another influencing factor. As eco-business practices make corporations ever-more successful, their power over the economic system grows. And that's why, according to the World Wildlife Fund, Coca-Cola has more power over commodity producers than does the United Nations.

### 3. Several economic and social factors have motivated companies to embrace eco-business ideas. 

More and more, companies are jumping on the "eco" bandwagon. But what are the economic and social factors motivating this shift?

The first big mover is globalization, which has forced companies to break down production processes to save costs. Adopting sustainable practices is a way for firms to maintain control over their outsourced manufacturing.

That's what American toy manufacturing company Mattel experienced after transferring its production to China in 2002. Since toy parts were being produced in so many different facilities, the company couldn't fully control the process, leading to over 25 product recalls in a decade.

To address quality issues, Mattel devised a strategy centered around efficiency and sustainability, implementing this strategy in over 5,000 Chinese factories. In the end, making the production process more "sustainable" led to a massive improvement in the quality of the company's products.

Commodity price fluctuations, a major threat to profitability, are another key factor in the rise of eco-businesses. For instance, the price of oil fell from $200 a barrel in 2013 to just $50 in 2014. Sharp fluctuations like these put enormous pressure on commodity-dependant companies.

So under the banner of sustainability, companies have become involved in extraction processes, allowing them to better control price levels and mitigate price swings. Coca-Cola, for instance, has invested heavily in water production to stabilize the price of its supply.

The final factor feeding the eco-business boom has to do with the large and growing middle class in developing countries. As an untapped source of revenue, this segment presents a huge opportunity for corporations. That's why Hindustan Unilever added 500,000 new stores to its existing 6.1 million retail outlets in India in 2009. The expansion resulted in a 14 percent jump in sales volume!

Of course, to meet increasing demand, companies have to produce a lot more products, which requires more sustainable (and thus, efficient) processes.

> A market study by A.T.Kearney showed that companies with a commitment to sustainability outperformed their peers by 15 percent during the financial crisis.

### 4. By adopting “eco” practices, businesses can increase profits and tap new markets. 

So we've learned that businesses are adopting eco-practices to survive in today's competitive market. But investing in sustainability isn't just about staying afloat, it can also lead to tremendous profits.

In fact, studies show that eco-business opportunities will be _worth $6 trillion_ over the next 40 years.

How can this be? Basically, efficiency-targeted practices tied to eco-businesses decrease operating costs and boost firms' bottom-line earnings.

After all, when Walmart reduced its in-store lighting from 32 to 25 watts, each of its stores saved $20,000 a year. With over 10,000 stores, Walmart's cost savings totalled some $200 million annually.

"Green" labeling is another major advantage of eco-businesses, as it fosters customer trust, thus leading to increased sales.

For instance, homes labeled "Energy Star" are said to reduce operational costs in heating, cooling and water delivery by 20 or 30 percent, with regular annual energy savings. And according to research, homes with an "Energy Star" label add an average of nine percent to their selling value, compared to comparable homes with no green label.

In addition to increased profits, there's another opportunity associated with eco-business practices. This growing global obsession with optimization has essentially created new eco-related markets worth trillions of dollars.

The information and communication technology (ICT) sector has become increasingly interested in cutting energy consumption, a major source of costs. So in an effort to monitor, track and reduce both energy consumption and costs, companies have started installing smart meters and grids.

This technology has been shown to cut energy consumption by 40 percent, so it's no surprise that so many companies are flocking to buy them.

And so a massive new market has emerged — according to the Electronic Research Institute, the smart grid market will be worth $2 trillion over the next 20 years.

### 5. Eco-businesses take control of the supply chain by tracing every step of the manufacturing process. 

As we've learned, companies have serious incentives to go "green." And for many businesses, this means _tracing_ — that is, taking control of the supply chain, right up to the extraction of raw materials.

Through a process of documenting and controlling every stage of manufacturing, eco-businesses locate and eliminate waste, while also mitigating the threat of criticism or legal action.

That's exactly what Home Depot did, when the Rainforest Action Network (RAN) accused the home improvement company of selling endangered or old-growth wood. Home Depot responded to the allegations by examining its timber supply chain for each and every product sold and ensuring it knew from where each product was sourced. After seeing the results, RAN called off its boycott.

Another advantage of supply-chain testing is that it allows companies to implement _a life cycle assessment_ (LCA) to ensure efficiency at every stage of the manufacturing process. The idea here is to get a sense of where the most damage is being done; in this way, a company knows exactly where to make improvements.

For instance, Swedish furniture company IKEA's LCA showed that its board material supplier was using too much energy. So IKEA focused on improving efficiencies in this area. In the end, it improved the supplier's energy efficiency by 37 percent, which saved the company €350,000 per year.

But how do eco-businesses ensure that suppliers actually adopt green-friendly approaches? Companies can follow Hewlett Packard's lead by establishing a sustainability code of conduct for suppliers.

In 2000, HP became the first in its industry to establish this kind of program, and it's efforts have paid off. HP now produces 8 percent less greenhouse gases than it did in 2010. Also, its new server system consumes 89 percent less energy and space, and costs 77 percent less than do traditional servers. Not to mention that HP now produces 37 percent less waste than it did in 2012.

### 6. Companies establish eco-business credibility through eco-labeling and sustainability reporting. 

"Going green" is merely the first step in becoming an eco-business. The next step? Become recognized for your efforts. After all, credibility and trust are fundamental to brand reputation and consumer loyalty.

To establish a "green" image and capture growing demand for eco-friendly products, many companies turn to eco-labeling. This can be lucrative, as General Electric was reportedly earning $17 billion in annual revenue from its green products and technologies just _four years_ after launching its Ecomagination project.

Of course, since more and more products are now stamped with "green" labels, consumers are growing cynical, which has led to diminishing returns.

That's why so many companies pursue eco-certification from government regulatory bodies, a surefire way of establishing a friendly, green image. If you can get a certified label (such as from the European Union's standards body or from the U.S. Energy Star program) on your product, customers will be far more likely to trust it.

However, to convince all these parties — customers, regulators and NGOs — that you really are as green as you say you are, you need to be transparent about your environmental credentials.

This is important, because many companies are now being accused of _greenwashing_, or pretending to follow green principles for the sake of publicity. To avoid such criticism, more and more companies publish comprehensive information about their sustainability programs.

The numbers are striking: In 2003, less than 20 percent of Fortune 500 companies were reporting carbon emissions. By 2010, that number was up to 75 percent.

In another full-disclosure strategy, Disney allows customers to track online a Mickey Mouse T-shirt through each stage of its production, using the product's unique tag number.

Similarly, Dole allows customers to use Google Earth to track bananas all the way back to the farm in Peru where the fruit was first harvested.

### 7. Governments and NGOs can harness the power of eco-business, yet they should be wary. 

As we've mentioned, big brand sustainability pushes aren't primarily concerned with solving environmental problems. And in the long run, such initiatives can create more problems than solutions.

But that doesn't mean eco-businesses have zero positive impact. Though corporate sustainability programs aren't ideal, they do improve waste, energy, water and material management systems globally, often in new and dynamic ways.

This is a crucial point, as environmental regulations and standards are still weak globally. But collaborating with eco-businesses is one way governments and NGOs can make dramatic progress.

That's what Greenpeace did in 2009 when it joined forces with Coca-Cola. The beverage company agreed to invest $50 million to develop hydrofluorocarbon-free vending machines and coolers, and to use these all throughout its supply chain.

This was a huge pact. Fully eliminating hydrofluorocarbons in the commercial refrigeration industry would be equivalent to eliminating the annual greenhouse gas emissions of Germany.

But it's important to tread carefully. Collaboration can bring good things, but it can also create problems. When you give corporations too much power, they sometimes take advantage of it.

For instance, corporations could work together to fix prices on eco-products. In fact, the European Commission had to fine Unilever and Procter & Gamble more than $450 million for doing just that, colluding and setting prices when introducing an eco-friendly laundry detergent.

The close, collaborative relationship between corporations and NGOs poses another problem, as it reduces the NGO's ability to criticize its partner.

To that end, it's possible to imagine a scenario in which Greenpeace turns a blind eye to some of Coca-Cola's bad practices, since it's relying on the company to curb carbon emissions — and doesn't want to damage the relationship.

### 8. Final summary 

The key message in this book:

**Eco-business is ultimately about expansion and profit, not environmental protection or social justice. That's because the same eco practices which companies use to address water, energy, material and waste issues in the short term also lead to increased consumption in the long term. Thus in general many "green" initiatives just don't benefit the environment.**

Actionable advice:

**If you really care about environmental issues, ignore the eco hype.**

Instead of buying into the green credentials of different brands, shoppers can have the biggest impact on the environment by decreasing consumption altogether.

**Suggested further reading:** ** _Everything Connects_** **by Faisal Hoque and Drake Baer**

CEOs practicing meditation, the challenges of corporate politics, social differences between team members and customer satisfaction: it's all connected. Using a holistic approach, _Everything Connects_ (2014) will help you transform your working environment into one that understands such connections and supports as well as increases creativity and innovation.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Peter Dauvergne and Jane Lister

Paul Dauvergne is a political science professor and the director of the Liu Institute for Global Issues at the University of British Columbia. He's authored and co-authored several books, including _Timber_ and _The Shadows of Consumption: Consequences for the Global Environment_. 

Jane Lister is a senior research fellow at the Liu Institute for Global Issues at the University of British Columbia. Her previous books include _Timber_ and _Corporate Social Responsibility and the State: International Approaches to Forest Co-Regulation_.

