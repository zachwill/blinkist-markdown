---
id: 5447426930336100081f0000
slug: economics-the-users-guide-en
published_date: 2014-10-22T00:00:00.000+00:00
author: Ha-Joon Chang
title: Economics: The User's Guide
subtitle: None
main_color: 47C8DB
text_color: 266B75
---

# Economics: The User's Guide

_None_

**Ha-Joon Chang**

_Economics: The User's Guide_ lays out the foundational concepts of economics in an easily relatable and compelling way. Examining the history of economics as well as some critical changes to global economic institutions, this book will teach you everything you need to know about how economics works today.

---
### 1. What’s in it for me? Learn the foundations of economic theory and how it affects your daily life. 

Did you snooze through your economics class in college? You're probably not alone. Yet economics is more than just a bunch of boring numbers — it plays a critical, intimate role in our day-to-day lives.

From the clothes on your back to the screen on which you're reading this text — literally _everything_ we interact with — is the result of a series of economic transactions that end with a product in your hands.

In these blinks, you'll learn how economics molds our lives and how it has also changed over the last few centuries. With an in-depth look at the history of economics, you'll better understand how our society has been sculpted by its laws and practices.

Moreover, you'll get a crash course on the specific mechanisms that shape economic behavior, and gain the knowledge necessary to examine a country's economic health.

In the following blinks, you'll also discover:

  * why we shouldn't really worry about why and how sumo wrestlers cheat;

  * why a plane made of solid gold might not be worth that much; and

  * how spending less at the grocery store could mean a pink slip for the cashier.

### 2. Economic theory can be applied in many places, but it’s best used in the study of the economy. 

If you've ever read the book _Freakonomics_ (as many millions of people have) then you've seen the variety of ways that economics can be applied to situations not directly related to "the economy."

For example, the book's authors attempt to explain why sumo wrestlers might cheat by using the economic theory of rational choice, which assumes that each person's decisions are calculated to produce the best possible outcome.

But why would a sumo wrestler cheat? Imagine that two sumo wrestlers are facing each other in a match. They also happen to be best friends. However, since one has already qualified for the next tournament, it's rational for him to lose _on purpose_ to increase the odds that his friend advances.

This is merely one example. In fact, economic theories have been used to describe a whole host of phenomena.

And while interesting, we shouldn't neglect the most important application of economics: studying the economy. Indeed, among the most important elements of economics is the way in which _money_ makes economic systems function.

In essence, money is the measurement of what society owes you, usually as a result of your labor.

However, sometimes money is given away freely, through a process called _money transfer_. In welfare systems, for example, money is usually transferred from those who have it to those who don't, to provide for basic needs, such as shelter or food.

Money can be used toward the consumption of goods and services produced by organizations that have a specific combination of _labor_ (workers) and _capital_ (machines and tools necessary for production).

Your mobile device, for example, came to be through this combination: its processors were invented by workers (labor) and then produced with the help of machines (capital).

It is these types of relationships, far more than the dilemma of a sumo wrestler, which are the heart of economic theory.

### 3. From medieval village blacksmiths to multinational companies: the capitalist economy has changed radically. 

The pros and cons of capitalism is a hot topic. Indeed, in recent years an ideological war has raged, with statements such as "Capitalism is in crisis" or "Capitalism creates jobs" parried back and forth.

But what exactly is capitalism?

One of the most influential early definitions of capitalism comes from Scottish economist Adam Smith in his seminal work, _An Inquiry into the Nature and Causes of the Wealth of Nations_, published in the eighteenth century _._

According to Smith, capitalism is a system of natural liberty, in which the central aim of political and economic systems is the accumulation of profit. One of Smith's key insights into capitalism was his connection between increased productivity and the _division of labor_.

To illustrate this, he examined pin manufacturing. He noticed that when a worker was given a limited yet well-defined role within the production process, such as either forging metal or creating molds, the worker's unique specialization allowed him to work more productively than if he tried to produce a finished pin, from start to finish, himself.

It's insights such as these that categorize Smith's book as a groundbreaking description of a capitalist economy, and its influence is felt even today. Yet a lot has changed since the eighteenth century!

Economic actors and institutions are vastly different from those in Smith's world. Back then, businesses were owned by individuals who were actually involved in the production process — such as the village blacksmith or butcher.

Today many companies are owned by a multitude of shareholders who are almost completely divorced from the day-to-day management of their enterprises.

Markets as well have changed. In Smith's time, most markets were regional, or national at their very largest. Consequently, most companies were small. Today, globalization has made markets international and has enabled the growth of huge multinational corporations.

> _"Few thought that there was anything wrong with hiring children."_

### 4. Trade policy and boom-and-bust cycles give us insight to how our economic system was shaped. 

We've seen just how significantly the global economy has changed in the last few centuries. But which events helped bring about this change?

Importantly, the politics of powerful Western nations at the time of the Industrial Revolution led to the rapid development of the economies of these countries.

During the Industrial Revolution, from the late 1700s to the early 1800s, the governments of the United States and the United Kingdom had a policy of _protectionism_, enforced through industry tariffs.

If companies from foreign countries wanted to sell their products to US or UK consumers, they would have to pay additional tariffs or fees, which made their products more expensive. This, in turn, helped make domestic products both cheaper and more popular.

Meanwhile, these same governments forced countries in Latin America and Asia into free-trade contracts — trade without tariffs — opening these markets to highly competitive Western products.

It is precisely this imbalance in historic trade policy that brought about the economic conditions we know today, with Western economies being the richest and most developed.

And then came the Great Depression, which shocked governments into taking more control over the economy.

On October 24, 1929, the US stock market experienced a sudden crash which left the economy reeling. Confidence plummeted, and few people were willing to invest. This led to a long-lasting economic depression, with high unemployment and poverty for millions across the globe.

Faced with desperate circumstances, governments sought to better manage the economy, attempting to influence it in a way that would ensure that citizens would have enough to survive, or better, prosper.

The US Social Security Act in 1935 was one such intervention, introducing old age pension and unemployment insurance. Even if people couldn't work, they wouldn't be left destitute.

Gradually, with such protections, workers in Western countries became increasingly economically comfortable, and with a few exceptions, remain so today.

### 5. With its stress on limited economic intervention, the Neoclassical School dominates today’s economic thought. 

When you watch the news, often you'll witness two economists arguing over which theory should guide government economic policy. But why are there so many differing viewpoints?

The science of economics has been divided into many different schools of thought, simply because the workings of any economy is inherently complex and often difficult to understand.

Let's look at some of the more influential schools of economic thought.

The _Neoclassical School_, first elucidated by economists in the 1870s, is the preferred theory for most modern economists. It focuses its attention on individual actors, and advocates intervention only when markets malfunction.

As the name implies, the Neoclassical School draws from two central ideas of the _Classical School_ that emerged around the time of Adam Smith.

The first idea is that economic actors — whether businesses, producers or consumers — are driven by self-interest.

As actors pursue things they want, the competition that emerges between actors will generate the best possible outcome for everyone. For example, if every car manufacturer competes for the most sales of the same model of car, then the price of that model will fall, meaning increased sales for merchants and more affordable prices for consumers.

The second idea is that markets are _self-equilibrating_ ; that is, markets will reorganize to an optimum level after an external shock, such as an oil crisis or war. This implies that markets — in most cases — shouldn't be tampered with, as they can take care of themselves.

The Neoclassical School differs from the Classical School in that it views the value of a product as dependent both on its production cost as well as its valuation by consumers.

Say an airplane made of solid gold is for sale. According to the Classical School, the plane's high production cost would give it a very high value. However, no airline would purchase such a heavy, impractical airplane, as it would be unable to fly! Thus according to the Neoclassical School, the plane's actual value would be much lower.

### 6. Keynesian economists stress the importance of government spending in difficult economic times. 

But if the market works so perfectly — as Neoclassical economists suggest — why are so many people out of work? This is where the _Keynesian School_ steps in **.**

According to the Keynesian School, long-term unemployment as well as products that cannot be sold, such as our solid gold airplane, contradict the Neoclassical insistence on markets' self-equilibrating power.

If a product goes unsold for a prolonged period, Keynesian economists say, then the money typically used for consuming such a product must have been spent somewhere else. Usually, the money instead will have gone into _savings_. And when people save money instead of spending it, they essentially remove money from the economy and thus help create unemployment.

To illustrate this, imagine two people working at a supermarket where three customers are shopping. If the three customers decide to not spend all of their salary but instead save a third of it, this would have the same effect as if the supermarket had only two customers.

However, a supermarket with only two customers requires only one working employee, thus resulting in the other employee becoming unemployed.

To prevent unemployment, Keynesian economists suggest that the government should intervene as to maintain investment in the economy.

When investors and consumers save money, overall spending falls. This in turn reduces income, as consumer spending is essentially the income of other employees.

To compensate for this, the government should invest in the economy when overall investment is low, for instance, during financial crises when people are too scared to spend money. This government intervention ideally can help prevent shrinking incomes and even possible recession.

To do this, the government could invest in infrastructure projects, such as building airports or improving highways. Such projects would provide people with both labor and wages, thus allowing them to spend money on goods and services and therefore provide others with jobs.

Now that we've explained the major schools of economic thought, these next blinks will examine methods to measure the health of an economy.

### 7. A country’s economic health can be determined by looking at key measurements, GDP and GDI. 

As individuals compete on a sports field, countries compete against each other in global markets. Some countries are seen as economic champions, others as struggling. Yet how do we determine exactly how well a country performs economically?

The prevalent measure of a nation's economic health is its _Gross Domestic Product_, or GDP.

GDP represents the monetary value of everything a country has produced within a given period of time. Specifically, it only accounts for the _added value_, that is, the difference between the value of the final product and the value of _intermediate inputs._

For example, a bakery has $150,000 in revenue. Over the course of a year, the baker had to buy $100,000 worth of baking ingredients, his intermediate inputs. The added value, then, is $50,000.

Another important measure of economic health is _Gross Domestic Income_, or GDI, the sum of citizens' income in one country.

Unlike GDP, we can't use GDI to compare countries, as each country's cost of living is different. While an iPod might cost $200 in the United States, it might cost only $150 in India. Thus, if India and the United States had equal GDIs, that wouldn't necessarily equate the same level of wealth.

To use GDI as a means to compare countries, economists developed the concept of _purchasing power parity_ (PPP), which measures a currency's purchasing power with regards to the price of specific goods. It then asks: How much of these goods can I purchase with my income in my country?

In India, the PPP conversion factor from 2009 to 2013 was 0.3, meaning that if you exchanged US dollars for Indian rupees, to buy milk you would need only 30 percent of the money in India than you would in the United States.

> _**"** In several of the poorest countries, the average person did not even earn $1 of income per day."_

### 8. GDP growth isn’t the whole story; a country needs to invest to have a healthy economy. 

Now that we know that GDP is one of the main indicators of economic health, what exactly can we say about a country's economy with reference to its GDP growth? Not much.

Sometimes GDP growth isn't a good indicator of a country's economic development.

Just look at Equatorial Guinea, a country where GDP grew 18.6 percent per year from 1995 to 2010. That's more than _twice_ the GDP growth of China, 9.1 percent, in the same period.

However, nobody talks about Equatorial Guinea as being a growth superstar, while many people have made excited references to the "Chinese economic miracle." Why is that?

In part, this is due to size, as Equatorial Guinea has a population of only 700,000 people. More important, however, is that the small country's GDP growth didn't result from increased productivity. Rather, the discovery of a large oil reserve in 1996 led to an influx of foreign investment.

A better indicator for economic health is _economic development_, which describes a country's economic growth resulting from an increase in its productive capabilities, that is, the ability to organize and transform production activities. Purchasing new machines or an improved communication infrastructure, for example, are a few indications of economic development.

Another good measurement of economic development is the _share of investment_ in GDP. When firms in a country invest more profits into _fixed capital_, or the machines and infrastructure that make production possible, we can view this as a good sign of a country's development potential.

A great example of such an investment is computer numerical control (CNC) machines. These machines can be programmed to translate models into an actual product, and are far superior in terms of speed to machines that require manual operators.

Essentially, if a company is investing in machines that allow for faster production, then that company can produce and sell more goods in a shorter period of time.

> _"No economy has achieved "miracle" rates of growth over...time without investing at least 25 percent of GDP."_

### 9. When a country has more “have-nots” than “haves,” then its economy isn’t healthy. 

The pursuit of equality is intrinsic to human nature, and people instinctively try to avoid situations in which a few lay claim to all the resources we collectively need to live.

Indeed, our shared history is full of examples where people have fought for equality. The French Revolution is just one poignant example, with its motto: "Liberty, equality, fraternity or death."

It's good that equality is a concept we naturally seek, as inequality is bad for an economy.

Inequality in a country often leads to political and social instability, which can frighten off potential investors. Compare, for example, the investments made in Austria and in Somalia in 2011. Despite its larger population, the highly unstable state of Somalia received $100 million while Austria, a stable electoral democracy, received $10 billion.

High inequality also implies limited _social mobility_, or the possibility of individuals to climb the social ladder. When only some people can afford an expensive, high-quality education, they'll be the ones who land the best jobs. Those who can't afford a quality education, even if they might be better suited for a job, will never have the opportunity to compete, which in turn hurts the economy.

One way to measure inequality is to use what's called the _Gini coefficient_, which measures the degree to which the actual distribution of income differs from an optimal (or equal) distribution of income.

The smaller the Gini coefficient, the more equally income is distributed. A value of "0" indicates perfect distribution. As values approach "1," fewer and fewer people have almost everything and the rest has near to nothing.

In most developed countries, Gini coefficients range from 0.3 (in Italy) to 0.5 (the United States), and are therefore situated between these two extremes.

The next blinks will examine how governments can use information like inequality measurements to determine how to better support an economy.

### 10. Through fiscal policy and monetary policy, a country’s government can influence the markets. 

Despite a majority opinion among economists that the economy works best when it's left alone, there are some instances that make government action mandatory.

In a completely free market, for example, there would be no taxation. However, if a government needs to build a road, it needs money to do so. So who will pay for the road?

To ensure that there is enough money to build a road and that everyone pays his fair share, a government can levy taxes from the population.

Or what about when a company has no competition in its market segment? These _natural monopolies_ most often develop in sectors where the cost of entry is simply too steep for the competition to get a foothold, such as in energy, water or public transportation. In this case, a government needs to step in to ensure fair practices, such as by imposing price controls or otherwise interfering in the market.

There are two major ways in which a government can affect a market.

The first is through _fiscal policy._ Governments can decide to spend more or less, or raise or lower taxes. Fostering highway expansion or other public works projects using public funds are examples of fiscal policy in action.

The second is through _monetary policy_. A government can either directly influence the amount of physical currency in circulation or adjust interest rates on the loans banks receive from the country's central bank, or a combination of both.

If a government lowers interest rates for borrowing money from the central bank, then the cost of borrowing across the country becomes cheaper as a consequence. This in turn encourages companies to take out loans at this new, lower interest rate, allowing them to more affordably invest in new fixed capital _,_ such as machines, or hire new employees.

Armed with the knowledge of how a government can intervene in the market, the final blinks will more closely scrutinize our modern global economy.

### 11. Financial products first deemed clever turned out to be toxic and helped cause the financial crisis. 

You may think of a bank as simply the place you go to deposit a paycheck or open a savings account. Today's banks however are far more varied.

In fact, there are two different kinds of banks.

The first is a commercial or deposit bank, which interacts directly with individuals. Germany's _Sparkasse_ or America's _Bank of America_ are examples of deposit banks, which offer a place for individuals to shelter their money.

The second is an investment bank, the original purpose of which was to help companies (not individuals) raise money from investors. _Goldman Sachs_ or the now-defunct _Lehman Brothers_, the bank that played a critical role in the 2008 financial crisis, are both examples of investment banks.

Since the 1980s, investment banks have increasingly focused on creating and trading new kinds of financial products — as we've seen, with disastrous results.

One such financial innovation which also played a major role in the financial crisis is called an _asset-backed security_ (ABS).

An ABS pools a large number of loans, such as mortgages or student loans, in order to turn them into a bigger bond, thus making them more attractive to investors.

Pooling these liabilities makes the bonds appear very secure. If you pool thousands of mortgages together, you can be sure that, on average, most people will be able to make their payments. Some might default, but these defaults will be covered by other secured payments.

At least, that was the idea.

Many ABSs were in fact highly overvalued and carried increasingly more risk, as more people began to default on their loans. This led to a devaluation of ABSs as well as other similar financial products. Once the reality of the situation became clearer, many banks that traded in ABSs were unable to sell them, and were thus hit with huge losses, the effects of which cascaded across the global economy.

### 12. Company downsizing, technological advancements and economic crises all cause unemployment. 

Although we now spend most of our time at work, we actually spend far less time in the office than workers did less than two centuries ago.

Up until the nineteenth century, people typically worked between 70 and 80 hours per week. Today those numbers are much smaller: in Greece, the average person works nearly 40 hours per week, compared to only 35.6 hours in Germany.

Nevertheless, our time spent at work is still a very large proportion of our waking hours. This has serious implications, as work has a noticeable impact on our psychological and physical well-being.

Some jobs, like construction work, are physically demanding and can be hazardous. Working long hours can make people tired and ultimately unhealthy. Furthermore, our psychological well-being is influenced at least in part by how much we like our work — those who are satisfied tend to be happier than those who aren't.

Far more stressful than an unsatisfying job, however, is having no job at all. Economists divide unemployment into three main types.

_Frictional unemployment_ occurs "naturally" when companies shrink or when employees quit in search of a different or better opportunity. Frictional unemployment then describes the time a potential worker spends between jobs.

_Technological unemployment_ occurs when machines replace workers, which was the case during the Industrial Revolution when manual textile weavers were replaced by steam-driven machines. This sort of unemployment continues today, as technology improves and manual work is made redundant.

Then there is _cyclical unemployment_, caused by a lack of demand for labor due to external circumstances, such as during the Great Depression. People may want work, but they can't find it.

While these distinctions do little to raise the spirits of the unemployed, they are useful in determining how exactly to combat unemployment.

### 13. A company seldom acts alone when it comes to decision making, as many actors hold voting power. 

When you decide how to spend your money — for example, by buying a new home, a car or even an ice cream sandwich — you essentially make that decision yourself.

How companies decide to spend money, however, is generally far less straightforward.

Unlike individuals, corporate decision making is influenced by shareholders and management. Many corporations are owned by many shareholders.

However, only very few companies have an individual shareholder with enough shares as to determine a company's future alone, as is the case with Sweden's Wallenberg family, which owns 40 percent of _Saab_, a car and aircraft manufacturer.

In contrast, the managers of companies have effective control, being more directly involved in day-to-day decision making. This can of course cause problems when the interests of shareholders and managers differ.

A manager is looking for prestige, which is positively linked to the size of the company she manages. In contrast, shareholders care more about profit than prestige, which isn't necessarily related to a company's size. To shareholders, higher profits mean higher dividends and thus a higher rate of survivability, since a company with high profits is less likely to experience a liquidity crisis.

It is conflicts of interest such as these that can lead to conflicts between shareholders and managers.

Sometimes, too, governments or even workers can affect corporate decision making. Workers can organize in trade or labor unions, which help fight for higher wages or to improve working conditions.

Some companies too are partially owned by governments. The German government holds a 25-percent stake in the country's second-largest bank, _Commerzbank_, which gives the federal government a direct say in the bank's corporate decision making.

### 14. International trade is increasingly important, for both established and developing countries. 

Have you noticed that wherever you may be in the world, you can still find a can of _Coca-Cola_?

The reason for this is _international trade_, or the exchange of capital, goods and services across international borders. In fact, international trade has increased dramatically in the past 50 years.

In the early 1960s, trade between countries accounted for 12 percent of global GDP, but by 2010, had risen to as high as 29 percent.

The effects of international trade on markets and services can be best illustrated through the following examples.

First, many technology companies are outsourcing services, such as customer service call centers and software development, to countries where wages are lower.

Second, manufacturing has become a global business, with manufacturing's share in the world merchandise trade (the global trade of products) in 2010 as high as 69 percent.

And lastly, developing countries are playing a larger role in the world economy thanks to international trade. China, for example, in 1980 accounted for only 0.8 percent of world manufacturing compared to 16.8 percent in 2012.

Participation in the global economy of course requires money; this can come from many sources.

Some money comes from the _trade surplus_ that accrues when countries export more than they import. Sometimes, however, the opposite occurs, and a country will experience a _trade deficit_.

To pay for trade deficits, countries can draw from _investment income_, or money that comes from the country's financial investments abroad, such as dividends from shares of foreign companies held by the country's residents.

Another option is to procure money in the form of _foreign aid_, or grants given by other governments. If those funds aren't enough to cover _trade deficits_, countries may have to either borrow money or to sell assets, for example, in the form of bonds.

As we look to the future, globalization will only make international trade that much more important.

### 15. Final summary 

The key message in this book:

**The economy has changed significantly over the last 300 years. Not only have the economic actors changed, but also the structure of corporations and global trade make us more connected than ever. To understand the structure of our societies and the complex relationships between countries, it's critical to have a foundational understanding of economics.**

**Suggested** **further** **reading:** ** _23_** **_Things_** **_They_** **_Don't_** **_Tell_** **_You_** **_About_** **_Capitalism_** **by** **Ha-Joon** **Chang**

In _23_ _Things_ _They_ _Don't_ _Tell_ _You_ _About_ _Capitalism_ Ha-Joon Chang destroys the biggest myths of our current economic approach. He explains how, despite what most economists believe, there are many things wrong with free market capitalism. As well as explaining the problems, Chang also offers possible solutions which could help us build a better, fairer world.
---

### Ha-Joon Chang

Ha-Joon Chang is the author of the bestseller _23 Things They Don't Tell You About Capitalism_ (also available in blinks) __ and writes a regular column for _The Guardian_. He has advised a number of national and international banks, and teaches economics at Cambridge University.

