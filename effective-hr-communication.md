---
id: 59538407b238e10005d20efe
slug: effective-hr-communication-en
published_date: 2017-06-30T00:00:00.000+00:00
author: Debra Corey
title: Effective HR Communication
subtitle: A Framework for Communicating HR Programmes with Impact
main_color: FAC345
text_color: 7A5F22
---

# Effective HR Communication

_A Framework for Communicating HR Programmes with Impact_

**Debra Corey**

In today's competitive environment for talent, effective communication is crucial for showcasing human resource programs in a way that will help attract, retain and engage key talent. Whether introducing new HR programs or re-launching existing ones, the IMPACT communication model presented in these blinks will help deliver your messages so that employees understand, appreciate and take action on them.

---
### 1. What’s in it for me? Learn techniques to achieve the effective HR communication you need and your employees deserve. 

Most companies are very mindful of how they communicate with customers, but when it comes to their own employees, communication with them is not usually as ambitious or carefully planned.

This is a mistake because just as communication with customers keeps them engaged and loyal, the same is true for communication with a company's employees.

Human Resources, or HR, is traditionally focused on contracts, pension schemes, training and recruiting, and less on how to communicate all this work in a way that encourages employees to interact and get the most out of their relationship with their company. So, let's look at how an effective HR communication campaign works and how to go about creating one.

In these blinks, you'll learn

  * What IMPACT has to do with communicating effectively;

  * why the question "why?" is so important; and

  * why you should learn to KISS.

### 2. HR communication is key to reinforcing company culture and values, building employee trust and improving employee engagement. 

We humans are social beings, and communication plays a significant role in our day-to-day lives.

This is precisely why HR communication constitutes such an important part of a business's success; it is central to how employers interact with their employees.

When communicating with employees, you can think of them as the audience at a play: if they're going to embrace and engage with the play, you'll need to grab their attention and hold their interest — otherwise, they'll just fall asleep in their seats.

So, if one of your company values is "we have fun at work," you need to back this up with actions that truly communicate your message. Create fun branding, use slogans and get creative with different ways of establishing a fun-loving communications campaign. This will help employees see that your message and company values are more than just words on paper.

Communication is also vital to conveying the serious and complex matters of employment, such as benefit plans, reorganizations, and new HR policies.

Since matters like these are often highly technical and confusing, employees usually do nothing more than glance at the paperwork and file it away.

But it doesn't have to be this way. Why not present this information in a way that's both informative and engaging? How about displaying the information in a magazine format that communicates the details in a clear and accessible way?

Effective communication should also engage employees and build trust. After all, when employees trust the people they work for, they're far more likely to push themselves to get the best possible results.

In a London Business School study from 2014, companies that reported high levels of trust regularly outperformed their competition by two to three percent.

That same year, a study by the Institute of Leadership and Management found that honest and open communication was key to building trust in the workplace.

So, nurture a feeling of trust by providing ways for employees to give feedback and taking their suggestions seriously.

> _"Communications which are open and honest will help build trust, as will leaders and management being visible and accessible to employees." –_ Helen Wright, Head of Communications at Great Place to Work

### 3. The objective of effective communication is to create a shared meaning and a call-to-action for your employees. 

You might think that effective communication with employees is easier said than done, so let's take a closer look at the ways you can get your employees engaged.

What effective HR communication does is take the company's messages and share its meaning with the employees, such that they truly understand and embrace the messages that the company wants and needs to promote.

Now, if you simply tell your employees, "We want people to have fun at work," this will mean different things to different people.

This is why the company Merlin Entertainments took impressive steps to promote their message of a fun workplace and make it clear to their employees that they meant it.

In 2013, their HR team launched a campaign called "For the Love of Fun" and sent boxes to all of the company's team leaders.

Inside the boxes was a film that detailed the concept of maximizing fun at work. But the box also contained "Fun Goggles," which took this concept a step further by giving employees a way of literally seeing the world differently and reminding them to always be on the lookout for new ways to have fun.

Employees were also given a "fun-o-meter," which not only allowed them to measure their fun on a scale that went from a chuckle to a belly laugh, but which also, and more importantly, kept them in the right frame of mind.

Another key part of any successful HR campaign is to have a clear _call-to-action_ : something you want the employees to do. This could be, for instance, to register online for a new company employee benefit scheme, or, in the case of Merlin Entertainments, to have fun at work.

But how do you get your employees to actually take action? The first step is to make the company's message clear; employees need to understand, accept and commit to it before they'll act on it.

At Merlin Entertainments, to gain commitment from their employees, company leaders promoted the idea of a fun workplace through videos, meetings and by being fun themselves.

However, to really connect with your employees and provide them with the right kind of message, you need to understand who they are, which is what we'll look at in the next blink.

### 4. Collect the facts prior to a campaign by asking the right people the right questions. 

When describing how he would use one hour to solve a problem, Albert Einstein said that he would first spend 55 minutes precisely clarifying the question. Once the problem was crystal clear, he would only need five minutes to come up with an answer.

Businesses should use a similar approach: first collecting valuable information about their employees to help them communicate more effectively.

So, when you're wondering how you can develop the best HR communication campaign, collecting facts can point you in the right direction.

At a previous job, the author was preparing a campaign to introduce a new benefits program. She sent out a questionnaire that asked the company's employees about how they preferred to receive news about new benefit opportunities.

The author suspected that people would want to reduce the number of e-mails they received, but surprisingly, the results were overwhelmingly in favor of e-mail! Clearly, asking beats conjecturing.

Collecting useful information is also about knowing how to ask the right questions.

One useful technique for getting a lot of information is to ask open-ended questions — the kind that can't be answered with a simple "yes" or "no." An open-ended question can produce a broader range of insights that may never even have occurred to you.

Another useful question to ask is "why?" If you're speaking to someone directly, you might even ask this question up to five times until you get to the real reason behind an employee's opinion or motivation.

Along with the right questions, you also need to ask the right people.

When trying to get a complete and accurate picture of your workforce, make sure to include people from all age groups, genders and positions.

Once, the author was asked to conduct a survey and told to only include people from a single branch. But she knew that if the company in question wanted an accurate account, she would need to include people from the company's regional offices as well.

She insisted, and ultimately got the data from other offices. This added information changed the picture considerably, enabling a much better communication campaign.

> _"You should never theorize before you have the facts."_ — Sherlock Holmes

### 5. Before you start to communicate, ask “why?” to set a clear goal and direction. 

When you're preparing a new HR communication campaign, constantly ask yourself "why?" when you're making decisions — this way you can set a clear goal and make sure you stay on target.

Every campaign should have a clear goal, so the first "why?" should be: "Why are we launching this new campaign?"

The insurance company Liverpool Victoria wanted to start an interactive online campaign that would encourage innovative collaboration among the company's employees. So the answer to the first "why?" was to get employees to use the platform in order to generate new ideas.

With the goal clearly defined, success soon followed: 11 percent of the company's employees used the online platform every day, with an additional 17 percent logging on every week. As a result, innovative ideas were quickly being developed.

When you know exactly what you want to achieve, it's good to keep asking "why?" as you move forward. This will keep you focused and prevent you from getting sidetracked when challenges or obstacles appear.

For example, let's say your company has a manufacturing site and wants to introduce a new system to help promote employee recognition and to encourage coworkers to talk to each other about recognition. With this in mind, you decide to set up a local intranet site for employees to use.

However, it soon becomes clear that your employees aren't using the intranet because they don't have access to it during the workday and aren't clear on how to use the site. So new suggestions are made to develop a form that people can fill out and use to submit recognition to the HR department.

But if here you were to pause and ask "why?", you'd see that this method of one-way communication wouldn't help reach your goal of getting coworkers to communicate with each other about recognition. Instead, you could set up kiosks where employees could use the intranet during the day, do hands-on training classes on how to use the site and finally remind employees that they have access to the site 24/7 from their home computer. All of these work together to deliver a system and process that achieves your objectives.

### 6. Use a medium that best suits your message and your audience. 

As Walt Disney once said, "You can design, create and build the most wonderful place in the world. But it takes people to make the dream a reality." The same is true for your HR communication, because if you don't select the right communication vehicles, or media, you'll never be able to entice your employees to focus on your message, let alone do anything with it.

The good news is that nowadays there are many media to choose from. Gone are the days when the most exciting thing you could do was design a fancy poster or send out an email. Now you have options like the company intranet, social media, webinars and text messages. The bad news, and the flip side, is that with all this choice, how do you decide which one is right?

Let's say the results of a questionnaire show that employees are unhappy with how complex the company's new retirement program is, so aren't engaging with it. Clearly, the booklet you worked long and hard on isn't working, so you need to find a new medium to take its place. A solution one company used was to add a visual storytelling portion to the company's website, which explained the employees' retirement options. It used a combination of images and graphic design to simplify complex details and make it easier for employees to find the program that best suited their needs.

Thanks to these changes, employees were led through a series of choices that featured intuitive images and phrases that they could click on to get additional information. This was a huge improvement over the indecipherable terminology that employees had to read through before.

Visual storytelling is just one possible choice of medium, of course. The HR team at the company Reward Gateway decided to use a medium similar to a social media platform in an effort to get their young employees interested in their voluntary benefits program.

The feature was added to the company's website, and it allowed employees to post comments about their experiences with the range of insurance products and investment opportunities.

And it worked. The platform got more people talking, and since it was fellow employees who were engaging in conversations, it helped people better understand what the different benefit packages had to offer.

It was a great way to both spread word-of-mouth interest in the programs and clear up any confusion.

In the next blink, let's look at how you can plan and coordinate your next campaign.

### 7. Good project planning will help deliver your communications on time and in the right way. 

Even if you think your communication is so great that it couldn't possibly fail, it very well might if you don't put a proper plan together. A good plan will make sure you don't miss those small but important details.

During one communication campaign, the author and her team spent days working on a video explaining the different benefit plans that the company was offering. But after they sent them out to all the different locations, they discovered that the company computers didn't have the right software to play them. So when the employees tried to watch the videos, they could only see the images without any audio.

This is the kind of embarrassing problem a good project plan will help you avoid.

So, to help you create a strong, coordinated and collaborative approach, use the _RACI model_. This will help you determine who is **R** esponsible for a task, who is **A** ccountable, who should be **C** onsulted and who is **I** nformed.

Let's say you want to develop a video campaign to promote new benefit plans. First of all, you'll need a script written by someone who knows all the details of these packages, so the benefits specialist should be _responsible_ for this task.

When you assign the benefits specialist to the task, you might also tell her to _consult_ the marketing team with any questions she has about how to best frame the message. She should also _inform_ the rest of the HR team when the campaign is ready. Meanwhile, as the project leader, you're _accountable_ for the project's completion.

Everyone working on the project should know their role and be held accountable for it. This ensures the team works together efficiently, meeting both individual and overall team objectives.

### 8. Having allies for communication projects is important, but you need to find the right ones. 

One of the most celebrated teams in sports history is the 1980 United States Olympic hockey team, a team of amateur players that managed to defeat the four-time gold medal-winning Russian team.

The secret of the US team's success was that they had a diverse lineup of players who added their own different and unique skills. And this is exactly what you should look for when you put together a team of allies for your next communications project.

Think of your allies as the people you need to surround yourself with to make you win when it comes to achieving your communication objectives. Going back to the hockey team, it's not just the players that make them a success, it's the coach, the personal trainers, the marketing team, and so on. They each have an important role to play, and without them, the team wouldn't work.

Speaking of roles, not all allies have the same role. They're needed for different reasons and can help you in different ways. Some are what you'd call endorsers: people out there supporting you and the campaign, giving it credibility. Some are partners, who work closely with you to develop and deliver the communication. And some are contributors, whom you go to at key points throughout the campaign to get input and steering.

It's important to find the right people to fill these roles, or it could hurt your campaign. The author had an experience where the wrong person came on as an endorser: instead of talking about the benefits of the new HR program, this person was actually talking it down, encouraging employees at their site not to participate.

Don't make this mistake. Ensure that your allies are truly on your side, helping you create and deliver a successful communication campaign.

### 9. For effective content, make it valuable and relevant for your employees, keeping messages simple and sticky. 

To reach your employees and truly connect with them, you should invest a lot of time and effort into the content you create for your HR projects, ensuring that it's valuable, relevant and "sticky."

A good mindset is to think of employees as the customers you're trying to win over with your HR "products" and to see yourself as the marketing expert who's pitching the product.

For example, Liverpool Victoria Insurance launched a campaign to empower their employees and get them to generate innovative new ideas for the company. They chose an online platform to help get the employees talking; to facilitate this process of empowerment, it was designed with a superhero theme.

This theme extended to the launch party as well, where people were encouraged to dress up as superheroes, and all the brochures and materials handed out also looked like comic books.

This wasn't just fun for the employees, it also made the content accessible, engaging and "sticky."

Another crucial guideline for your content is to always keep your message simple and straightforward. In fact, many communication experts emphasize this, including Dr. John Kotter from Harvard Business School.

So, when you're thinking about how to deliver your message, remember the acronym _KISS_ : **K** eep **I** t **S** hort and **S** imple.

One easy way to keep things short and simple is to avoid repetition — so be sure to go through your project copy and delete any redundancies.

And while you're going over your sentences, take the time to question each one and ask yourself, "Is this absolutely necessary for getting my message across?" If it can be cut without the overall document losing any value or emphasis, get rid of it. Each sentence should serve a purpose by supporting the primary point in a succinct way.

You can also make your message more powerful by using an active voice and favoring strong descriptive words like "important," "amazing" and "brilliant."

### 10. Prioritize your messages, making sure the most critical ones come first. 

People today are constantly bombarded with information from a variety of sources, so don't assume your audience will take the time to read more than the first few sentences of the message you're hoping to deliver.

With this in mind, it's always important to prioritize and highlight the essential parts of your message. This can be tricky since the subjects that HR deals with are often full of complex details, but it's your job to be highly strategic and identify captivating details that will best highlight the value of what you're promoting.

In the author's experience with HR communication campaigns, people generally won't read very long messages to end. This means it's crucial to prioritize what you want to convey.

The author proposes using a pyramid structure, where your message starts with a short sentence containing the most important piece of information you wish to convey; the one that every single employee should know. Next, you give the second most important piece of information and then the third in similar succinct sentences. Only after you've briefly presented all three points should you go into their details with longer explanations.

This ensures that even if employees only read the first three sentences, they will still have seen all of the essential points regarding the program or benefits package you're trying to get them interested in.

Another way to maximize impact is to personalize your communication.

Personalized communication is a trend in both marketing and HR communication campaigns, as it makes customers and employees feel like they are being personally catered to. And it requires more than just adding an employee's name at the start of an e-mail.

Let's say you're promoting a new retirement program; you could send out an e-mail with custom graphics showing precisely how much the recipient in question would benefit by joining the program sooner rather than later, accounting for the recipient's age and the number of years left till retirement.

### 11. Test the effectiveness of your communication campaign frequently to ensure you stay on track. 

When it comes to HR communication campaigns, there's no autopilot that would allow you to sit back after launch. Instead, you have to remain on top of things to adjust as needed.

A good method for this is to conduct regular tests along the way that are designed to show how you're meeting your goals.

Let's say your current campaign was launched to ensure more employees would understand, and take part in, a particular recognition program. There are simple methods to test and determine whether your efforts are making this happen.

One way is to send out employee feedback forms that ask specific questions about whether or not employees know about the program and how to use the recognition tools. If the results show that employees are still unclear on what is available to them, then you know you need to make changes.

To determine what kind of changes you need, you might use employee questionnaires. One example of a good question might be, "How would you prefer to be informed about new programs? E-mail or brochures?"

But even if the results come back in favor of one particular format, you still need to test the result to make sure your methods are working. If employees respond with a preference for e-mails, but the campaign is still struggling, the e-mails might still need to be personalized or be made more interactive.

When it comes to what tools to use, traditional online and paper surveys work well for obtaining quantitative data. But to help you make creative decisions with qualitative data, focus groups are a tried and tested method that can steer you toward the best choices of content, themes and media. All you need to do is find a small group of employees and provide the right questions for the moderator to ask them.

These questions should be designed to get the group talking, so they can express their likes and dislikes. Then, you can listen carefully, take notes and start devising a way to better appeal to their preferences.

Also, another increasingly popular method is to use social media platforms to test ideas and the potential popularity of campaigns.

### 12. Use the IMPACT model to create an effective communications campaign. 

As we wrap up this book-in-blinks, let's review all the tools we've covered that will allow you to communicate more effectively with employees.

To help you remember each tool, just think of the acronym IMPACT: **I** nvestigation, **M** edium, **P** lanning, **A** llies, **C** ontent and **T** esting.

The first stage of the IMPACT model is conducting an _investigation_ into what the best ways to communicate with your employees are and what the objectives of your overall communications campaign should be.

Next, choose the _medium or media_ that best suits both your message and your audience. You can then start _planning_ your campaign carefully with a clear timeline and building a strong team of _allies_ with diverse skills and different roles.

Now, you can develop your _content_ in a way that's clearly relevant and valuable to employees.

Finally, don't forget to _test_ the results and make changes, if and when needed.

Using this model and the other methods described in these blinks, you'll have everything it takes to deliver a winning HR campaign.

To conclude these blinks, let's look at a case study of how British Telecommunications (BT) informed their employees about SaveShare, a new investment opportunity to buy BT company shares at a low price.

When the HR team investigated the program, they saw that it was complex and needed to be simplified for the employees to understand more easily.

As the medium, they chose an interactive online model, along with personalized e-mails and instruction pages. They also created a unique and attractive orange design as the theme of their campaign.

When employees went to the website, the interactive features showed them exactly how different investment packages would benefit their savings. And to help provide answers to their questions, BT also created Question Time, an online talk show featuring experts who answered any questions the employees wanted to submit.

The campaign was a huge success, with over 90 percent of employees joining the program over the course of its five-year enrollment period. Remarkably, when a consecutive plan began accepting enrollments in 2014, an additional nine percent signed on.

This campaign is a great example of what happens when effective HR communication clearly outlines the benefits of a program: everyone wins.

### 13. Final summary 

The key message in this book:

**HR communication is crucial to keeping employees engaged and active within a company. It ensures that employees understand and make the most of the company's HR programs, which, in turn, can help create a more stimulating and productive workplace.**

Actionable advice:

**Add a bit of magic**

Sometimes the best way to make your communication effective is to add a bit of _magic_, or what the author calls your "secret ingredient" or "secret sauce." This is something that delivers a memorable experience that your employees will engage with and never forget. Maybe a magic mirror where messages appear when employees walk by? Or LED lights embedded in a boring poster to make people stop and take notice? Find the magic, and employees will love it.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Essential HR Handbook_** **by Sharon Armstrong & Barbara Mitchell**

_The Essential HR Handbook_ (2008) is a guide to human resources management. These blinks are full of useful tools and important insights on how to manage your organization's most important resource: human capital.
---

### Debra Corey

Debra Corey is an award-winning global HR director and communication specialist with 30 years of experience. She has launched outstanding communications for companies such as Gap, Honeywell, Merlin Entertainments and Reward Gateway, and is also a sought-after instructor and speaker on the topic of communication and human resources.

