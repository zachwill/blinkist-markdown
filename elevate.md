---
id: 5d8d39956cee0700080bbf89
slug: elevate-en
published_date: 2019-09-30T00:00:00.000+00:00
author: Robert Glazer
title: Elevate
subtitle: Push Beyond Your Limits and Unlock Success in Yourself and Others
main_color: F0E730
text_color: 706C16
---

# Elevate

_Push Beyond Your Limits and Unlock Success in Yourself and Others_

**Robert Glazer**

_Elevate_ (2019) is a manifesto for aspiring high-flyers looking to achieve more. Divided into four core competencies, these blinks provide a wealth of actionable advice on boosting your spiritual, intellectual, physical and emotional performance. Focusing on these areas, Robert Glazer argues, is a guaranteed method for taking your personal and professional life to the next level.

---
### 1. What’s in it for me? A four-step plan to unlocking your hidden potential. 

What do you see when you look at the top achievers in your field or industry? Are they lucky or uniquely gifted in ways you aren't? Well, probably not. What they most likely _do_ have is a proven recipe for success. The trick is simply choosing the right cookbook. 

Luckily, you've already done that. In these blinks, we'll break your performance down into four key areas or "capacities" — spiritual, intellectual, physical and emotional. Think of them as tools that, when used together, will see you through any project. 

This is valuable because life is full of demanding long-term projects. Whether it's your personal life and its many relationships or your career with its numerous challenges and hurdles, success is the fruit of patience, diligence and flexibility.

In these blinks, you'll also learn 

  * how to gain clarity on your mission in life; 

  * why mornings are the best time to start building new routines; and 

  * why your brain suffers when you don't take care of your physical health.

### 2. Building capacity is the key to self-improvement and high achievement. 

Do you want to improve yourself and achieve more? Great — you're in the right place! Let's kick things off with the two most important words in these blinks: _building capacity_. This is about continually learning and honing new skills that allow you to elevate yourself and grow. What it _doesn't_ mean is simply doing more things — you also have to do the _right_ things. 

There are four principal areas when it comes to building capacity. The first is _spiritual capacity_. This is about focusing on getting to know yourself and clarifying your values and desires. Next up is _intellectual capacity_. This includes how you think, plan and execute with maximum efficiency and discipline. Then there's _physical capacity_ — your physical health and well-being. Finally, there's _emotional capacity_. That's your ability to deal with challenging situations and get the most out of your relationships. 

One way to visualize these areas is as a ball divided into four chambers, each of which can be filled with gas. The 2D version of this ball would look like a pie cut into four quarters. The more air you pump into each of these chambers, the more momentum the ball will have as it moves. But here's the catch: if you're not filling each chamber with the same amount of gas, the ball will start to wobble and go off track.

It's the same with your capacities. All four need to be _balanced_. This means developing them simultaneously with equal care and attention. 

Now, building capacity isn't something that happens overnight. It's like working out. Sure, you have to be motivated to book your first gym session, but that's just the start. If you want to build muscle and get that washboard stomach, you need to be hitting the gym week after week and improving bit by bit. 

Building capacity is just like that — after all, achieving big goals takes time and dedication. Most importantly, it means taking those small steps that will carry you over the finish line every day. Sometimes, the process takes months; other times, it's a matter of years. Stick at it, though, and you're sure to realize your dreams. 

So, now that you know how to build capacity in theory, let's take a look at what you need to do in practice.

### 3. Spiritual capacity involves figuring out what you want and aligning your actions accordingly. 

Building capacity isn't something you can do willy-nilly. Before you start expending your time and energy, you need to be clear about what it is you're trying to achieve and why. That's exactly what "spiritual" means in this context — it's about figuring out who you are and what you want. Think of it as a compass that will keep you on course and ensure you're using your resources to get to where you want to go. 

So here's the million-dollar question: how do you do that? 

Well, if you want to define your core values, you need to ask the right questions. This means taking time to reflect on what's most important to you. Ask yourself what makes you feel happy and energizes you, and also what gets you down and saps your energy. In other words, when do you succeed and when do you struggle? 

The next step is to reach out to friends, family members and colleagues and ask them how they see things. Here you'll want to compare your answers with theirs to make sure you're seeing things clearly. Then, you'll want to take all those answers and look for common words. Maybe "compassion" crops up repeatedly, or maybe "independence" is a running theme. Whittle down that list to just four or five key concepts. These are your _core values_ — the principles by which you want to live. 

That brings us to your _core purpose_. This is your mission in life. It combines your values and gives you a clear sense of where you're going. The author, for example, defines his purpose as "finding a better way and sharing it," and it's this that motivated him to write his book. 

Take a moment to examine your values and then see if you can form a sentence that represents your long-term direction in life. If you're struggling to do that, try writing your own obituary. It might sound a little macabre, but this is a great way of getting out of your head and asking yourself how you'd like others to see you. That will likely contain vital clues about your core purpose. 

The last step is to begin aligning your actions with your values and purpose. This is all about putting your energy in the right place. True success is the fruit of this alignment. Fail to do that and your success won't be defined by you but by others.

### 4. Build your intellectual capacity by adopting a growth mindset, and then find mentors and routines to help you. 

If you've updated your computer software recently, you'll know how much more efficiently and smoothly applications run when you give them a bit of TLC. The same thing happens when you work on your intellectual capacity. The more time you put into it, the more tasks you'll accomplish while expending the same amount of energy. 

The key to building that capacity is a _growth mindset_. People who adopt this attitude reject the _fixed mindset_ — the idea that our intellectual capacity is fixed at a predetermined point. This is all about embracing the fact that it's never too late to learn new skills and that mistakes and failures are simply part of the process. 

The growth mindset is vital to your efforts to expand your intellectual capacity, but it's not something you can cultivate on your own. That brings us to the second step: finding mentors who will help you keep learning and growing.

The world's high-flyers and top performers all have one thing in common: they surround themselves with coaches and mentors who challenge them to keep improving. These people are so important because they don't tell you what you _want_ to hear — they tell you what you _need_ to hear. They pull you up on avoidable mistakes and point out how you can achieve more. 

That's vital because we're all short-sighted when it comes to understanding ourselves. Sometimes, you need an objective outsider to tell it like it is. 

Finally, you'll need to use routines to set yourself up for success. Routines are important because they help you make a habit of being productive. That's crucial when it comes to achieving long-term goals like learning a language or writing a book. 

The best time for this is the morning. It's as simple as waking up 15 minutes earlier than usual and using this extra time for quiet, focused work. Once this habit has been consolidated, you can start adding extra minutes to your routine. What, for example, would happen if you got up a full hour before your family and used the time to meditate, work out and jot down ideas for your book? 

These are small tweaks to your daily schedule but, cumulatively, they can add up to life-altering changes.

### 5. Build your physical capacity by eating well, managing your stress, getting enough sleep and embracing competition. 

Physical capacity is about more than just being able to run a marathon. When your body is in poor shape, your brain also suffers: you're more easily distracted, less resilient and more likely to be knocked back by stress and setbacks. That means it's time to start looking after your health. 

Let's start with what you eat. The key point here is to make sure you're getting a balanced and nutritious diet. Highly processed junk food is everywhere today and, in many countries, people are more likely to die as a result of obesity than they are from starvation. But with so many different kinds of diets out there, it's often hard to choose what's right for you. Is paleo better than keto, for example? Or is low-carb preferable to slow-carb? 

Well, let's make things easier with a tip from author and journalist Michael Pollan. His rule of thumb is simple: don't eat anything your great-grandmother wouldn't have recognized as food. 

Next up: managing stress. Stress isn't necessarily a bad thing — in fact, it was stress that ensured our ancient ancestors were alert and attentive enough to spot dangers. Unfortunately, our brains respond to our frenetic, digitalized world in much the same way as they would to snakes and tigers. This means that many of us are regularly experiencing more stress than we can cope with. 

According to Dr. Heidi Hanna, the most effective way to reduce stress is to take regular short breaks throughout the day. These allow you to take a deep breath, relax or even meditate. Getting enough sleep is also essential to your performance levels and well-being, so make sure you're spending a good 6-8 hours in the Land of Nod each night. 

And here's one last tip: embrace competition. This has become something of a dirty word, but it's important to remember that competition shouldn't be about crushing your opponents and winning at all costs. What it really means is going the extra mile and challenging yourself to become better. Whether it's intellectual or physical competition, it will push you to build your capacity.

### 6. Build your emotional capacity by stepping outside your comfort zone and seeking elevating relationships. 

Finally, we come to emotional capacity — the tool you'll use to navigate your relationships both with yourself and others. This has two sides: dealing with people around you and dealing with unintended consequences. Think of driving a car: to do this well, you have to keep an eye on other road users while also anticipating their sudden swerves and stops. 

Building emotional capacity begins by leaving your comfort zone. It's all too easy to get stuck in a particular mindset, hemmed in by limiting beliefs or the low expectations of others. Overcoming these boundaries is only possible when you set yourself new challenges or allow someone else to push you. If you've ever had a colleague or friend tell you something you didn't want to hear, you'll know all about this. Sure, it's uncomfortable at first, but it ultimately motivates you to make improvements. 

Stepping out of your comfort zone also builds your resilience. This is a vital skill. Every day, we face new challenges and situations which can throw us off course. The more resilient you are, the more likely you are to clear those hurdles. 

So how do you do it? Well, it's as simple as trying new things. You can cook an unfamiliar dish or travel somewhere new, for example. You can also try doing something you usually shy away from, like cold-calling a potential client or giving a colleague some honest negative feedback. 

This isn't something you can do on your own: to maximize your emotional capacity, you need to surround yourself with the right people. As the author and entrepreneur Jim Rohn once put it, you're the average of the five people you spend the most time with. That's why it's so important to seek out people who share your values, energize you and inspire you to be your best self. 

The flipside of that is avoiding people who undermine you. These are _energy vampires_ — so-called "friends" or family members who sap your strength and undermine your effort to improve yourself. Disengage from these individuals and you won't just feel better — you'll also have more time for relationships that truly elevate you.

### 7. Final summary 

The key message in these blinks:

**The key to boosting your performance and achieving more is working on four core areas of your life. These are your spiritual, intellectual, physical and emotional "capacities." How do you do that? Well, the best place to start is clarifying what you actually want in life. Once you're clear about your goal, you can start honing the skills and abilities you'll need to reach it**

Actionable advice:

**Protect your mornings by keeping them tech-free.**

The time between waking up and beginning your routine is crucial — it's literally what sets you up for the day ahead. Unfortunately, our digital devices and gadgets mean we often fail to make the most of this time. Rather than working on our own projects, we waste these precious minutes answering emails or browsing Facebook. But there's a simple fix: commit to keeping the first hour of each day tech-free. Whether you put your phone in a drawer or use an app to restrict usage is up to you. Either way, you'll be amazed at how much you can get done when you're not distracted!

**One last thing:** The author helped us create this book-in-blinks. What's more, he's offering the Blinkist community a discount on the book itself. Just head to geni.us/ELEVATE and enter the discount code _10BLINKIST_ to get yours.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Daring Greatly_** **, by Brené Brown**

In an act of real generosity and vulnerability, Robert Glazer has shared his recipe for integrating spiritual, intellectual, physical, and emotional growth to build your capacity. Acts of vulnerability are at the heart of many great things. 

Take it from author Brené Brown. She's researched just how crucial embracing vulnerability is for living a fulfilled and successful life. If you'd like to learn more about her work and how you can embrace vulnerability in your life, check out our blinks to _Daring Greatly_.

### 1. What’s in it for me? A pro’s guide to personal growth. 

The modern world is bursting with data. All this information can be overwhelming. What does it all mean and, more to the point, what's the best way of achieving success and satisfaction in _your_ life? 

Luckily, you don't have to try to figure it out on your own. In these blinks, we'll turn to entrepreneur, author, and lifelong wisdom-seeker Joseph Deitch for help. Born in 1950, Deitch's chameleon-like career has seen him excel in industries as different as financial planning and entertainment. 

But whatever he turned his hand to, there's always been a guiding thread: a commitment to personal growth and a burning desire to achieve excellence. And that's just what we'll be exploring as we unpack Deitch's toolkit for upping your personal and professional game. 

Along the way, you'll learn 

  * why listening is the key to learning; 

  * how rewards can help you reach your goals; and 

  * why love is all you need.

### 2. You can solve many problems and get what you want by simply asking the right questions. 

How do you solve problems in your life? As you think of various techniques, you might be forgetting something obvious: taking a moment to think things through and asking yourself what the solution might be. It's a deceptively simple approach that delivers incredible results. 

Take it from the author, who spends his winters in Boston, Massachusetts. It's a cold, grey, and dreary place and, like lots of Bostonians, he suffers from a mild form of winter depression. For a long time, he dreaded the arrival of November. 

There was one highlight, however — Thanksgiving. That got the author thinking. What, he asked himself, was it that made the annual holiday so enjoyable? More to the point, what could he do to make the rest of November as enjoyable as Thanksgiving? 

The answer? Getting together with friends and family. And that was the solution to the author's winter blues. Today, he plans parties, excursions, meetups, and game-evenings with the folks he loves. It's had a miraculous effect. Rather than despairing about the coming winter, he actually looks forward to it! 

Here's another easy way to tackle problems: don't just ask yourself what you want, but _how_ you're going to get it. Think back to when you were a kid and how much you wanted sweets or that new toy your parents refused to buy you. Children typically react to these frustrations by whining or even crying. This is a self-pity trap that you need to learn to avoid. 

The trick is to ask the right question: What do I need to do in order to get what I want? The author taught his son, Matt, to take this approach, and it's worked wonders. When Matt wants something — a video game, say — he realizes that he has to do something to earn it, like cleaning the dishes after dinner or completing other household chores. 

As you can imagine, that's a great attitude to take to life's many challenges and frustrations. Rather than passively grumbling about your lot, it encourages you to put yourself in the driving seat and proactively solve problems for yourself.

### 3. Listening to others is not always easy, but it provides valuable information and helps you improve. 

If you want to learn something new, you have to listen to others — after all, when you're the one doing the talking, you inevitably end up saying what you already know. Whether you're with your friends and family or in a business meeting, listening is the best way of getting information and finding out what you can improve on. 

Taking feedback on board can be tricky, though, especially when it's critical. The author witnessed just how hard this can be a couple of years back, when he attended a seminar in which participants received feedback from their colleagues. 

Now, because every attendee was receiving comments from multiple sources, they knew the comments weren't arbitrary. One participant, however, had a meltdown and started shouting about how the feedback he had been given wasn't fair. Ironically, this temper tantrum confirmed what his colleagues had said — that he always assumed the folks he worked with were out to get him. 

That clearly isn't a healthy approach. So how can you cultivate your listening skills? Let's take a look at some easy tools to help you become a better listener. 

The first thing you'll need to do is commit to listening. If you're in the habit of interrupting or ignoring others, you need to recognize this and make a conscious choice to change your behavior. 

The second step is realizing that listening is an _active process_. The idea isn't to simply sit there and let the words wash over you as you idly contemplate what you're going to eat for dinner that evening. True listening means engaging with what's being said, noticing when you become distracted, and making an effort to refocus your attention. 

Finally, you'll want to go into conversations with an open mind. You'll only ever hear what someone is saying if you drop your assumptions and take their argument on its own merits. Remember, you can always decide whether you agree or disagree after they've said their bit!

> _"Listening is a never-ending journey upon an ever-improving road."_

### 4. The way we think and talk affects our lives and shapes the way we feel. 

Every night before bed, the author told his son Matt that he was an excellent sleeper who could always drop off whatever the situation. Amazingly, this worked and Matt has never had a problem getting a good night's sleep. What's going on here — how does this work? 

The answer might surprise you. How we think and talk about things actually affects our lives. Think of the human brain as a computer. Like a PC, it can be programmed. Rather than using code, however, it's programmed using regular, everyday language. 

When you think or talk about yourself in negative terms like, "I can't sleep at night" or "No matter what I do, I just can't lose weight," you're encoding these messages in your brain. And the more you repeat them, the truer they become. Why is that? Well, you're basically hardwiring yourself to run negative programs.

Luckily, this problem can be fixed. The way to do that is to reverse the code and consciously think and talk about yourself in positive terms. Repeating ideas like, "I can sleep easily every night" or, "I can lose weight with little effort" makes it easier to get a full night's shut-eye and shed those extra pounds!

Feelings can also be manipulated in this way. Put differently, you can _choose_ whether you feel bad or good.

Not convinced? Try this exercise. Dim the lights in your room, slump down in your chair and focus your attention on a negative experience in your life for ten minutes. How do you feel? Pretty terrible, right? 

Now try the opposite. Brighten the lights, strike a confident, upright pose and focus your mind on a great memory that makes you smile or laugh. Raise your arms in the air and shout "Yes!" a couple of times. Chances are, you'll feel a whole lot better!

It just goes to show how much we can influence our emotions. Next time you're feeling low, think back to this little experiment and remind yourself that _you're_ responsible for how you feel.

### 5. Positive, immediate, and certain motivation is the best way to achieve your goals. 

Have you had a boss who inspired you to go the extra mile and produce the best work of your career? If so, you've probably wondered what it was that made them such a great motivator. Surprisingly, it's a lot less complicated than you might think — in fact, it all comes down to a couple of straightforward rules. 

Here's the most important point: if you want to get the best out of others, you need to give them _positive, immediate and certain motivation_. Let's break that down.

Lots of folks cling to the old-fashioned idea that the best way to get results out of workers is to scare them. The latest research shows that this is a damaging fallacy. In reality, negative reinforcement undermines performance. That's because scare tactics encourage people to do the bare minimum necessary to avoid punishment. 

That's where the _positive_ part comes into the equation. If you reward good work positively, employees are more likely to give it their all. This is especially true when those rewards are _immediate_. Thanks to the way our brains are wired, most of us will, given the choice, prefer $1,000 today to receiving $2,000 next year. As a result, we all work harder when we know we'll see the fruits of our labor sooner rather than later. 

Rewards also need to be _certain_. Think of it this way. If your company is flaky when it comes to bonuses and sometimes goes back on old promises, you're unlikely to feel motivated. If rewards are fixed and non-negotiable, on the other hand, you're much more likely to push on and hit your targets. 

These principles don't just apply to companies, though — you can also use them to motivate yourself. Imagine that you want to lose weight. As anyone who has been on a diet will tell you, this can be a grueling experience, because eating tasteless, low-fat food feels like a punishment.

Add in the fact that the rewards are neither immediate nor certain, as well as our monkey-like craving for immediate gratification, and you can see why it's so hard to stick to your new dietary plan.

So here's an idea: make dieting a positive experience by preparing small yet delicious meals and creating a rewards system that gives you little delights each time you hit a milestone. You could, for example, reward yourself with a trip to the spa or a night out with friends for every 2 lbs you lose.

### 6. Structures help you to avoid mistakes, while conserving your energy prevents burn-out. 

Life has a way of throwing us curveballs. When that happens, we're quick to adapt. If a close friend or family member gets seriously ill, for example, we often discover previously unknown resources to structure our time and get a huge number of things done.

But what if there was a way of doing that every day, not just in times of crisis? Here's the good news — there is. Let's see how.

Whether it's to-do lists, morning or evening routines or a weekly cleaning plan, structures all have one thing in common: they help you use your time more efficiently and avoid mistakes. 

Look at the aviation industry. Today, airline pilots work their way through an extensive security checklist before take-off. This is strictly enforced in countries like the US, and the results speak for themselves. Since the introduction of such tests, the number of accidents involving aircraft has decreased dramatically. 

Doctors and nurses have a similar approach and use checklists to create structure in their diagnostic procedures. Studies have shown that these lists have reduced the number of slip-ups and mistakes in medical environments like hospitals. 

And another tip: don't waste resources like time and energy. Here it's worth taking a tip from nature. In the natural world, resources are scarce and precious, which is why every organism uses the minimum amount of energy necessary to sustain itself. 

This also applies in everyday settings. Imagine you're about to lift a heavy suitcase. If you're expending too little energy, you won't be able to get it off the ground; expend too much, however, and you'll end up swinging the suitcase around and knocking stuff over. 

Now think of the activities needed to complete a project. The same principle is at work. Too little effort and you won't get anything done; too much and you'll eat into your energy reserves and soon find yourself running on an empty tank. Put differently, you'll burn out.

### 7. Power naps are a great way to make up for lost sleep, but they can’t replace it entirely. 

Professional success and personal fulfillment are both built on the same foundation stones. One of the most important and often overlooked parts of this structure? Sleep. If you're struggling to get a proper night's rest, don't worry — in this blink, we'll explore hacks for quality rest. 

Let's start with power naps. When you're tired, there's often little point trying to press ahead with your work. You're guaranteed to be working inefficiently and wind up with something that falls below your usual standards. So here's the alternative: take a short nap and refuel before returning to the task at hand. 

The reason power naps give you such a boost is easy to explain. Twenty to thirty-minute naps are almost entirely made up of _rapid eye movement_ or _REM_ sleep. This is particularly good for your cognitive and biological functions. More importantly, your body doesn't fall into _deep sleep_. This means you don't wake up feeling groggy and confused.

If you're still not convinced, take it from Thomas Edison, Leonardo da Vinci, and Eleonore Roosevelt — three of history's most successful power nappers!

There is one caveat to bear in mind, however. As effective as power naps can be at giving you a much-needed cognitive boost, they can't replace regularly getting a full night's sleep. Unfortunately, sleeping disturbances are on the rise and lots of people are struggling to get that. 

If you're one of them, it's worth noting that medication should always be treated as a last resort. Before you go to the doctor, try building plenty of exercise into your daily routine. Combine that with natural remedies from traditional Chinese medicine or Ayurvedic cures and you might find that, in time, your sleeping problems clear up.

### 8. It’s worth investing a lot of energy into a new venture, and keeping track of your time. 

It takes a huge amount of energy to get an airplane off the ground, but once it reaches altitude it can cruise while burning minimal amounts of fuel. The momentum achieved by that initial spurt, in other words, is enough to keep it in the air. 

What does this have to do with your personal and professional life? Quite a lot, actually. Like an airplane, you need to expend a lot of energy when you launch new projects. Start half-heartedly, by contrast, and you'll soon find yourself out of runway. That's when you hit a wall and give up. 

The author, for example, spent many years trying to learn how to ski. Every now and then, he would make it up a mountain. Because he didn't go skiing regularly, however, he never got good enough to really enjoy the sport. In the end, he called it quits. 

Whether it's skiing, picking up a new language, or mastering an unfamiliar skill, you have to fully commit at the beginning of the process. That's just what the author did when he decided to take up yoga. He threw himself into his new venture by embarking on a 30-day yoga challenge. This was a great way of getting over that awkward early stage and reaping the full benefits of the practice. Today, yoga is a part of his everyday routine! 

Okay, you know how important commitment is, but where do you find the time for your project? Well, here's the thing: you have more of it than you might think. 

To find out just how much time you actually have, you'll need to analyze how you're spending it. Grab a notebook and record your activities over the next two weeks, jotting down what you're doing every six minutes. This will give you a highly detailed overview of what you're doing with your days. 

Chances are, you'll be appalled at how much time you waste watching TV, using social media, or simply worrying about things. And awareness is half the battle — once you can see how fruitless this use of time is, you can start focusing on things that enrich your life like learning to play an instrument, spending more time with your family, or exercising.

### 9. Bringing more love into your life and remembering the importance of touch will make you happier. 

As the famous Beatles song has it, "All you need is love." On some level, we all know that love makes our lives happier and fuller. Unfortunately, we often overlook this basic insight and neglect this vital matter. That has to change. 

Love is contagious. Spend time around a loving person and you'll find yourself feeling much more connected and mellow. That's why it's so important to surround yourself with loving influences. This can be as simple as spending more time with the folks who love you and make you feel good. 

Then there are children. Kids are masters of the art of unconditional love. Make an effort to hang out with these little bundles of loving enthusiasm and you're sure to learn a thing or two about affection. Spirituality is another great way of getting in touch with your loving self and nourishing your heart. 

And here's a final tip: if you're struggling to make loving connections, just picture how puppies behave when they meet one another. This is the energy you should be channeling when you're getting to know new people — fun, easy-going, and playful. 

Love isn't an abstract idea, either — it's also about touch. Whether it's in a platonic or erotic context, physical contact is one of the most effective ways of expressing love. No wonder, then, that touching and being touched has been shown to improve our physiological and mental health!

A good way of integrating this into everyday encounters is to hug people. That's obviously trickier in some places than others — restrained cultures like that of the northeastern US states, for example, don't exactly encourage this behavior. But if you make an effort to overcome your natural reticence, you'll be amazed how much closer you feel to those around you. 

And when it comes to your partner, don't underestimate the importance of nurturing slow sex and tenderness to your mental and physical well-being. Here you can take a tip from the Indian _Kama Sutra_, an ancient text written around BCE 200 to help lovers transform sex into an emotional and spiritual experience, as well as a physical one. 

And there you have it — everything you need to start elevating your life from learning to ask the right questions, to cultivating love in your day-to-day life!

### 10. Final summary 

The key message in these blinks:

**Life isn't simply about going through the motions — it's about elevating yourself to ever higher levels of proficiency and, ultimately, achieving satisfaction and success. But here's the catch: oftentimes, we just don't know where to start this process. Fortunately, it's not as hard as you might think. The place to start? Ask yourself what you want to change in your life, and refuse to give up until you have a firm answer. Once you've clarified that question, it's all about saving time, structuring your days for maximum efficiency and taking care of your physical and mental health.**

Actionable advice:

**Find out what the people around you want.**

Do you ever feel frustrated because the folks around you just don't seem to get what you're driving at? Or maybe you've noticed that your partner seems grumpy but you can't figure out what's wrong? Well, whatever it is that's vexing you, it's time to put an end to the suspense. Here's how: stop pretending you're psychic, and start communicating! If you want to solve an issue, you need to ask what other people want. Once you know that, it's much easier to give it to them or, if that's not possible, rationally explain why you can't. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Live It!_** **by Jairek Robbins**

Right, by now you should have a good idea how to start elevating yourself and getting the most out of life. But knowing something in theory and actually doing it in reality are two different things. Still scared to let go and start following your dreams? Don't worry, we've got you covered! 

Enter Jairek Robbins, a decorated coach and lifestyle entrepreneur who has helped readers around the world to realize their ambitions with his proven, step-by-step process guide to filling the gap between where you are today and where you want to be tomorrow. So, if you're in the market for authentic happiness, check out our blinks to _Live It!_
---

### Robert Glazer

Robert Glazer is an entrepreneur and best-selling author. He's the founder and CEO of Acceleration Partners, a global performance marketing agency, and author of the international best seller _Performance Partnerships_ (2017). Glazer is a regular contributor to _Forbes_, _Inc., Thrive Global_ and _Entrepreneur_, and the host of the _Elevate Podcast_.

