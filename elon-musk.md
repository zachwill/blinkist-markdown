---
id: 556c53313236630007010000
slug: elon-musk-en
published_date: 2015-06-05T00:00:00.000+00:00
author: Ashlee Vance
title: Elon Musk
subtitle: Tesla, SpaceX, and the Quest for a Fantastic Future
main_color: EBC252
text_color: 856D2E
---

# Elon Musk

_Tesla, SpaceX, and the Quest for a Fantastic Future_

**Ashlee Vance**

_Elon Musk_ (2015) gives us an insight into the brilliant and difficult character of today's most innovative entrepreneur. Interwoven with details of his turbulent private life, these blinks reveal why Elon Musk is so determined to save the human race, how he's worked towards this goal so far, as well as what's on the horizon for potentially the richest and most powerful man of our future.

---
### 1. What’s in it for me? Learn what drove Elon Musk to the pinnacle of success. 

There are only a few businesspeople in the world as well known and admired as Elon Musk. His successes in a range of areas — from electric cars to space travel — have established him as a household name.

But what is it that makes Elon Musk so special? These blinks lay bare his interests and passions — the forces that drove him to become the man he is today. They outline Musk's unique passion: the desire to save mankind from destruction. It's this goal that fueled his work on solar power, electric cars and space exploration, and, ultimately, led to his prodigious success.

In these blinks, you'll learn

  * why Musk married, divorced, remarried and then divorced his second wife;

  * how fornicating mice kick-started Elon Musk's mission to Mars; and

  * how Elon Musk made electric cars sexy.

### 2. Saving the human race is what drives Elon Musk. 

Achieving success in the sustainable-technology industry isn't easy. Many try and most fail. And yet Elon Musk, founder of Tesla Motors and SolarCity, has managed to succeed not once, but twice. How did he manage it?

It comes down to the way he sees the world. Musk is not your average money-obsessed, Silicon-Valley entrepreneur. Uniquely, he possesses a kind of universal empathy. Harboring a deep care for the whole of humanity, Musk is driven by a single goal: to save us, the human race, by relocating humanity to Mars. In Musk's view, the Earth, vulnerable to asteroids and with dwindling resources, is no longer a tenable home.

This concern never leaves his mind, and has instilled in him an unshakeable determination to get things done. Of course, this doesn't always manifest itself well. Musk is notorious for setting unrealistic goals, assigning incredible workloads and verbally abusing his employees.

Musk even berated an employee who, instead of attending a company event, chose to be present for the birth of his child. Musk demanded that he consider where his true priorities lay. The way Musk sees it, you can either commit 100 percent to changing the history of the world, or not at all.

Love him or hate him, Musk is nevertheless respected by employees for his sense of mission. They know it brings success. And he's no hypocrite, either. Musk's sense of purpose is plainly evident in his grueling weekly schedule. Monday begins at SpaceX, in Los Angeles, where he works until Tuesday night. He then jets to Silicon Valley, where he spends Wednesdays and Thursdays at Tesla. Then it's a flight back to L.A. There's no way someone could commit to such a lifestyle without believing in what they were doing.

### 3. Musk’s unhappy childhood shaped his innovative and ambitious character. 

Thanks to a unique and powerful sense of purpose, Elon Musk is one of the most successful entrepreneurs in modern society. But how did he come to see himself and the world the way he does? Well, it all started with a difficult upbringing in South Africa.

The young, near-friendless Elon Musk had a difficult relationship with his father, Errol. And yet when Musk's parents separated, Musk chose to live with his dad to give him some much-needed company. But life with his father was difficult. In addition to the turmoil at home, Musk was often bullied by classmates, once receiving a beating so severe that he couldn't attend school for a week.

To escape this, Musk retreated into reading and study. Possessed of an astonishing photographic memory, Musk was able to read two encyclopedias — and remember everything. _The_ _Hitchhiker's Guide to the Galaxy_ was one book that deeply influenced Musk; it led him to realize that answering a question is easy, but that asking the right question is much harder.

At this early stage, Musk was already pondering questions that might expand and improve human civilization. By the time he started high school, he had strong opinions about solar power, conquering other planets, paperless banking and space rockets. He was also becoming entrepreneurially aware, selling his video game creation, Blastar, for $500 when he was just 12.

### 4. Musk’s confidence and determination flourished even more during his college years. 

In 1988, Elon Musk decided that he didn't want to do military service in South Africa. So he left the country. Though he dreamed of moving to the US, Musk first went to Canada. His first year was tough, as he went from relative to relative and drifted between odd jobs. Then he enrolled at Queen's University, where, as his self-confidence began to soar, his character took definite shape.

Musk was more ambitious in college than in high school. He entered public speaking contests, studied business, and even successfully wooed the woman of his dreams, Justine Wilson, who later became his first wife and the mother of his 6 sons.

Their courtship was romantic and competitive. To begin with, Wilson had no interest in Elon Musk. But he wouldn't take no for an answer. When he got stood up for their first date at an ice cream parlor, he found out where she was studying and asked a friend of hers what her favorite ice cream was. He then showed up carrying two chocolate-chip ice creams. This tactic of success-through-determination came to be a distinct approach in all areas of Musk's life.

After two years at Queen's, he transferred to the University of Pennsylvania and continued to flourish. As he grew more comfortable among his fellow physics students, Musk made friends who proved valuable — and not just personally, but monetarily as well. Musk and Adeo Ressi, a good friend, hosted house parties in a 14-bedroom house they'd rented; admission was $5, and Musk, straight-laced and sober all the while, raked in considerable profits. One night even brought in enough money to pay a month's rent!

### 5. Musk’s first start-up turned him into a dotcom, Silicon-Valley millionaire. 

Fresh out of college and eager to jump on the dotcom bandwagon, Musk created his first company. In 1995, he and his brother founded Global Link Information Network, which was later renamed Zip2. Their aim was to help businesses clueless about the internet to get online for the first time. 

Few small businesses understood the consequences of the internet; they had little idea how to get on, and saw little value in listing their business online or in having their own website. Things were tough at first, Musk and his brother worked very hard and still didn't sell. They received a lot of rejections, the most amiable ones declaring that the internet was "the dumbest thing they'd ever heard of." 

Things began to change when Mohr Davidow Ventures, the venture-capital firm, invested in the start-up after being impressed with Musk's energy and drive. They moved Musk down and hired Rich Sorkin as CEO. And, as the money started coming in, they hired better engineers, who changed and shortened much of the bulky coding. This got on Elon's nerves. He was, after all, a self-taught coder. 

However, Mohr Davidow also brought a more refined structure and outlined more realistic goals. Jim Ambras, the vice president of engineering at Zip2, knew that when Musk said a task should be completed in an hour, that actually it would take a day or two. When Musk said something would take a day to complete, it would actually take a week or two. 

Finally, in February 1999, PC-maker Compaq Computer offered to pay $307 million in cash for Zip2. But Musk never considered sticking around at Compaq and was already thinking of new projects. He wanted to become a successful CEO.

### 6. Losing the war over Paypal left Musk with millions. 

Musk, with his newfound money, joined the big-boys' club. He used his earnings from Compaq to buy a McLaren sports car, a condo and a small prop plane. But the rest of his money went straight into his next business: X.com.

In those days, people were reluctant to buy books online, let alone share bank account details. But by partnering with Barclays, Musk succeeded in establishing X.com as one of the world's first online banks, backed up with FDIC insurance and three mutual funds for investors to choose from.

Things were going well, but, soon enough, some major competition arrived. Max Levchin and Peter Thiel had been working on their own payment system at Confinity, before creating the first version of PayPal.

After a brief battle, the companies decided, in March of 2000, to join forces: Confinity possessed the sexier product (PayPal), and X.com had the money and superior banking products, so the merge made sense.

But Musk was soon to be pushed aside in his own company once again. Two months after the merge, Thiel resigned, Levchin threatened to do the same, and Musk found himself in charge of a divided company. Though most of his coworkers favored PayPal, Musk persisted in promoting the X.com brand. Meanwhile, computer systems failed regularly and the website crashed weekly.

Then followed one of the meanest coups in Silicon Valley's history: as Musk and his wife Justine boarded a plane for their overdue honeymoon, the executives went to the company board and asked Thiel to come back as CEO and to demote Musk. The coup succeeded, and Musk was left as an advisor.

The company changed its name from X.com to PayPal and was finally sold to eBay, in July, 2002, for $1.5 billion. Musk netted $250 million, enough to make his wildest dreams possible.

### 7. Musk’s determination led him to another frontier: the space industry. 

After his 30th birthday in 2001, Musk decided to escape the rat race. He relocated his family to Los Angeles, right around the corner from the hub of space industry. 

Musk had always been keen to get involved in space. At the time, the Mars Society was developing a plan to test the feasibility of putting life on Mars by sending fornicating mice into orbit. Musk thought the plan was good; the only improvement, he thought, would be sending the mice straight to Mars.

In the end, the plan was dropped, but it didn't stop Musk from entering the space industry. He decided to make his debut by exploring how one might construct cheaper rockets. In June of 2002, Space Exploration Technologies (SpaceX) was born, with the mission to emerge as the South-West Airlines of Space. At a time when sending 500-pound payload started at $30 million, Falcon 1 would carry a 1,400-pound payload for $6.9 million. 

Perhaps unsurprisingly, Musk's demands were unrealistic. His original timeline forecasted a completed first engine by May, 2003; a second engine in June; the body of the rocket by July; and everything assembled in September. The first launch was planned for November — a mere 15 months after the company started! 

Unsurprisingly, it took about 4 years for SpaceX to successfully launch a rocket. Although Musk dislikes the lack of a clear plan of attack, he understands that things don't always work out first time, failure is just a part of the process. The reality is that most launches fail. He knew that 9 out of 20 Atlas launches had succeeded, so failure was the norm. But, come hell or high water, he was determined to use his skills to ensure that SpaceX succeed in the end.

His passion and drive, however, proved inspirational. SpaceX's became the first commercial company to carry the Dragon capsule to space and retrieve it safely after an ocean landing. The company continues to develop in incredible ways, as we'll find out in a later blink.

### 8. Under Musk, Tesla Motors gave the electric car a future. 

Electric cars used to have a less-than-cool reputation; they were certainly no match for high-power brands like Jaguar and Ferrari. But if you've seen any of the new Formula E cars, you'll know things are changing. One man pushed harder than the rest to make electric cars cool and desirable, and that was Elon Musk. 

Musk helped the world recognize electric-car technology for what it was: exciting, and always progressing. 

It all started when J. B. Straubel and, unbeknownst to him, Martin Eberhard and Marc Tarpenning, were working on electric cars powered by lithium-ion batteries. On July 1st, 2003, Eberhard and Tarpenning founded Tesla Motors; Straubel joined later. 

The idea was to license the technology that AC Propulsion developed to power the tzero (a full electric car that accelerates faster than a Ferrari!), and to use the Lotus Elise chassis for the body of the car. But venture capitalists didn't invest and didn't see beyond the shoddy plastic finish of the tzero. 

Musk, however, chose to invest $6.5 million, becoming the sole shareholder and chairman. He thought the project could revolutionize electric cars, making them popular and efficient, and the world a less polluted place.

Despite a slow and unceremonious beginning, Tesla emerged as a great success. In mid-2012, Tesla's Model S sedan changed transportation forever. With continuous internet access and a sensor that allowed the driver to start the engine without touching a single button, it's been referred to as a "computer with wheels."

In November of 2012, _Motor Trend_ named it car of the year, and later, _Consumer Reports_ gave the car the highest rating in its history (99/100), declaring it probably the best car ever built! America hadn't seen such a successful car company since the emergence of Chrysler, in 1925. 

This achievement is rather astonishing, as Silicon Valley had been little involved in the automotive industry, and Musk hadn't even manufactured cars before. But when we consider Musk's determination, this success is perhaps not such a big surprise after all.

> _"Elon Musk had built the automotive equivalent of the iPhone."_

### 9. From SolarCity to Tesla, Musk’s companies are unified. 

Musk's star power stems from three ventures simultaneously: SpaceX, Tesla, and SolarCity. This is because they all ultimately help him pursue his real, and unifying, goal: the survival of the human species.

Musk had long wanted to go into solar, but prior to creating SpaceX, he hadn't thought there was any money in it. So when his cousins, the Rive brothers, were brainstorming about a new venture, Musk suggested solar.

The brothers spent two years studying the solar-power industry before hitting upon their idea. Though solar panels were slowly becoming more affordable, the cost and effort of installation was enough to drive many consumers away. So the Rive brothers decided to give customers what they really wanted: someone to take care of the whole process, from selection to purchasing to installation. 

Musk helped his cousins come up with the structure and became the chairman and largest shareholder. Six years later, and SolarCity has become the largest installer of solar panels in the US, living up to its goal of making solar panel installation painless. It's expanded from individual customers to businesses like Walmart and Intel and, in 2014, it was valued at close to $7 billion.

Musk's businesses, while successful individually, also strategically complement each other. Tesla makes battery packs that SolarCity can sell to end customers, and SolarCity supplies Tesla's charging stations with solar panels.

This is because, despite being passionate about cars and solar panels and batteries, they are all just side projects for Musk. His main goal remains to ensure that humans start living sustainably now so that humanity has a future. In this way, all his endeavors are united by one ambitious goal.

### 10. Musk aims to further transform the aerospace, automotive and solar industries with the Hyperloop and other projects. 

Musk has always had grand plans — so grand that people often find them a little far-fetched. In August, 2013, he unveiled more of these plans: the Hyperloop, as well as other developments at Tesla and SpaceX.

The Hyperloop is a new mode of transportation for fairly short distances. It's a large-scale pneumatic tube, just like the ones used to send mail around offices, but, in this case, it's for transporting people and cars in pods. 

Similar ideas have been proposed before, but Musk's is different. His design runs under low pressure, while the pods float on a bed of air. Each pod is thrust forward by an electromagnetic pulse, and motors throughout the tube give the pods an added power-boost when needed.

These solar-powered mechanisms could keep pods going at 800 miles per hour; at that speed, you could get from Los Angeles to San Francisco in 30 minutes.

For Tesla and SpaceX, Musk has other plans. Tesla's primary focus in 2015 will be bringing the SUV Model X to the market. Then, planned for 2017, is the highly anticipated Model 3. This car will cost just $35,000, instead of over $100,000 — the price of a typical Model S.

In 2014, Musk also announced plans to build a Gigafactory, the world's largest lithium-ion manufacturing facility. That will increase the batteries available in the marketplace, a crucial part of the strategy to make Tesla cars driveable over long distances, even without access to a recharging station.

In the near future, SpaceX will begin testing its ability to take people into space. SpaceX aims to perform a manned test flight by 2016, and to fly astronauts to the International Space Station for NASA in the following year. 

SpaceX is also likely to move into building and selling satellites, one of the most lucrative areas in the industry. Musk is reportedly daydreaming about perhaps becoming the first man to set foot on Mars!

> _"The more you know about Musk, the harder it becomes to place him among peers."_

### 11. Musk’s success comes with a turbulent personal life. 

Throughout these blinks, we've seen that Musk isn't always an easy person to get along with. Even his marriages are a testament to this. 

Married three times, twice to the same woman, Musk is romantic and hot-headed. His first marriage was passionate. However, he wasn't always a compassionate husband — Justine recalls how she once reminded him, in exasperation, that she was his wife, not his employee. Musk responded by telling her that if she was his employee, he'd have sacked her. 

According to Justine, Musk gave her an ultimatum in June, 2008: they would either fix their marriage that day or he would file for divorce the next morning. Justine asked to wait another week, so Musk bulldozed ahead and filed for divorce the following day. 

Nevertheless, Musk suffered emotionally following the divorce, and his friend Bill Lee tried to lift his spirits with a holiday to London. There, Musk met the then 22-year-old actress Talulah Riley who became his second wife.

He divorced Riley in 2012, stating that he "would always love her but wasn't in love with her anymore." They remarried directly after the divorce was finalized, when he found it impossible to date while maintaining his unbelievably busy schedule. (By his calculations, a woman requires a minimum of 10 hours per week.). In late 2014, they divorced again.

Many think Musk can be tough to the point of being mean and capricious. Some even say he lacks empathy completely citing the dismissal of his most loyal assistant, Mary Beth Brown, as an example. Practically doing everything for him, she had long been the only link between Musk and all his interests. Yet, when she asked to be compensated on par with other SpaceX execs, he told her to take two weeks off and that he'd take on her work to gauge whether the request had merit. When she came back, he told her that he didn't need her anymore.

Yet, despite these personal failings, those closest to Musk say that he is a loving and caring person at heart. And Riley claims that, despite his incredibly busy schedule, he always tried to get home to have dinner with his family and play computer games with his children.

### 12. Final summary 

The key message in this book:

**Elon Musk is an exceptional man. Ambitious, passionate and driven, he never takes no for an answer. His deep concern for humanity's survival is coupled with a large ego and difficult personality. No matter what others think of him, Elon Musk has propelled sustainable technology to astonishing new heights, and he will continue to do so, as one of the leaders, if not the leader, of the aerospace, automotive and solar industries.**

Actionable advice:

**Think big.**

Next time you have an idea, don't worry about how crazy it sounds. Take a page out of Elon Musk's book. Fornicating mice on Mars, all electric cars faster than Ferraris, marrying the same woman twice? Nothing is too absurd. Think the impossible and then make it happen.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _T_** ** _he Innovators,_** **by Walter Isaacson**

Elon Musk may be one of the greatest tech entrepreneurs of today, but he's standing on the shoulders of giants. His achievements are just the latest in a long line of innovations and inventions stretching back for centuries. In _The Innovators_, Walter Isaacson, the author of several best-selling biographies, including _Benjamin_ _Franklin_, _Einstein_ and _Steve Jobs_ (all available on Blinkist), examines the lives of some of the key figures in the history of computers and the internet.

As it turns out, although bright individuals play an important role, the greatest leaps in innovation in these fields were the achievements of many.

If you want to learn more about the history of innovation and how great minds collaborate — including what role the English poet Lord Byron's daughter played in the early days of computers — we recommend the blinks to _The Innovators_.
---

### Ashlee Vance

Ashlee Vance is one of the most prominent technology writers today. An American business columnist, he has written for the _New York Times_ and _Bloomberg Businessweek_.

© Cover image "Elon Musk" by David Shankbone

