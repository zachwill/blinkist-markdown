---
id: 59f5e58eb238e10006f6076f
slug: emotional-agility-en
published_date: 2017-11-03T00:00:00.000+00:00
author: Susan David
title: Emotional Agility
subtitle: Get Unstuck, Embrace Change, and Thrive in Work and Life
main_color: 56BAF2
text_color: 326C8C
---

# Emotional Agility

_Get Unstuck, Embrace Change, and Thrive in Work and Life_

**Susan David**

_Emotional Agility_ (2016) provides the theories and tools that will emancipate you from the fickle rule of your emotions. If you can distance yourself from the knots of anger and fear in your stomach, you can gradually learn to unwind and heal.

---
### 1. What’s in it for me? Learn to deal with your emotions as they arise. 

"I'm not good enough," "If only I were a better partner," "I'll never be able to handle that presentation tomorrow"– do any of these thoughts sound familiar? They probably do.

Unfortunately, our inner voice often sounds like a drill sergeant whose sole job is to make harsh judgments and create a swirl of negative emotions.

In these blinks, you'll learn how to see these self-deprecating words for the distortions that they truly are. You'll be presented with ways to unhook yourself from the unhelpful patterns and negative emotions that are created by the distorted stories your mind creates. And you'll learn that being emotionally agile means dealing with your emotions as they arise, an ability that is sure to benefit your relationships and your work, not to mention your relationship with yourself.

You'll also learn

  * why you don't have to force a smile if you don't feel like smiling;

  * how downplaying your weaknesses can actually make you less self-confident; and

  * that people who claim to like playing golf are often just kidding themselves.

### 2. The human brain can create distorted stories based on lived experience. 

It's long been known that great movies need a great "hook," a simple device that motivates the characters and gets the story moving. But hooks exist outside of film, too. In fact, we often get hooked into the story of our own life in the same way.

From one moment to the next, our brains are constantly trying to make sense of our experiences and turn them into a coherent story about our lives. They create narratives from billions of pieces of sensory input.

For instance, the author's basic "story" goes like this: "I am waking up and getting out of bed. The small person jumping at me is my daughter. I grew up in Johannesburg, but I currently live in New York. I need to get up today because I'm a social worker."

As long as things are simple and positive, it's not tricky. But the story construction often goes awry.

The issue is that the stories our minds manufacture are rarely accurate. Instead, they distort reality, often in a negative way. And that's not good because misrepresentation produces negative emotions.

For instance, let's say your parents separated soon after your birth. You might blame yourself for their divorce, even though you're completely innocent. Or you might think you'll always be unloved because you were a shy, introverted child in a family of extroverts.

These kinds of distortions happen every day, and the results can be harmful. Imagine you're at loggerheads with your boss, but, instead of addressing the issue directly, the event negatively clouds your thinking and so you go home and snap at your spouse because she forgot to run the dishwasher. Thanks to distortion, you've not only failed to resolve the conflict with your boss; you've riled up your partner, too.

Put simply, we rarely see our lives as they really are. Rather, we weave distorted stories that make us emotionally unhappy. Emotional agility is the ability to step back from the emotions and figure out what needs to change.

So how should we set about unraveling this mess?

> _"These dubious, inaccurate stories leave us conflicted or waste our time."_

### 3. Pretending we’re happy gets us nowhere – and negative emotions can have an upside. 

Are you ever annoyed by the relentlessly cheerful people who see the world through rose-tinted glasses? Surely it can't be healthy for them to keep smiling through breakups and bereavements, can it?

The simple truth is that it's not. Forced optimism avails nothing, and avoiding negative emotions by forcing a smile and thinking positive — though a common strategy — tends to do more harm than good.

Researchers from the University of California at Berkeley confirmed this a few years ago when they inspected the class photographs of Mills College, a private women's foundation. Some of the women smiled genuinely, while others forced it.

It's possible to detect the difference. Genuine smiles activate the orbicularis oculi eye muscles and the zygomaticus major mouth muscles. But fake smiles only involve the mouth.

When the researchers tracked down the classmates thirty years later, they found that the genuine smilers were blessed with happier lives in terms of marriages, well-being and careers.

It was clear: Grinning and bearing it is not a constructive way to deal with negative emotions.

It might seem counterintuitive but another reason not to suppress negative emotions is that they can actually be very beneficial.

Here's an example: One of the author's clients thought he had an anger problem. But, with the author's help, he realized he was dealing with unreasonable spousal demands and expectations. His response seemed more appropriate given the circumstances.

So he took pains to observe and recognize his emotions, which helped him created boundaries and engage in clearer communication with his wife. This hard work gradually improved the marriage.

Clearly, facing emotions is better than repressing them. But what's the best way to achieve that?

### 4. Start Showing Up for your emotions by using self-compassion. 

Many of us imagine our emotions, especially painful ones, as some sort of many-headed monster out to get us. When faced with such a beast, you'd instinctively want to get away as fast as possible. But, in fact, it's much better to stand your ground and face the monster. This is the first stage in dealing with your emotions: _Showing Up_. But how do you do it?

Well, experience shows that self-compassion is the best way to deal with painful emotions. The path to self-compassion isn't always easy to follow, but it's worth walking — and it begins with recognizing and listening to your emotions.

Here's an exercise that'll help you get started. Imagine yourself as the child you once were. Visualize the life circumstances and difficulties you had. Can you feel compassion for that child? In your mind's eye, embrace and comfort that imagined child. The key step is realizing that the adult you are also needs to be comforted like the child you were.

In fact, showing yourself compassion is essential when dealing with painful emotions.

In 2012, psychologist David Sbarra made a study of divorcees. He found that participants who expressed self-compassion recovered from divorce faster than those who resorted to self-criticism or blame.

There's a strategy to self-compassion. It involves taking a broad view of yourself, accepting who you are and seeing everything, including all your flaws, with compassion.

The psychologist Dr. Kristin Neff conducted a serious examination of self-compassion in 2007 by asking the subjects to present themselves in mock job interviews.

The results were obvious. Self-compassionate interviewees talked about their weaknesses openly without diminishing their sense of self-confidence. On the other hand, less self-compassionate interviewees attempted to downplay their weaknesses, but only succeeded in becoming less self-confident as they did so.

Self-compassion, then, assists in dealing with negative emotions. But it isn't always easy to feel it, especially when we're overwhelmed by emotions. So let's look next at how to distance ourselves from the whirl of emotion.

### 5. You can Step Out of destructive emotions by practicing mindfulness. 

Is there something that's niggling at you? You know, something that's really getting your goat? Classically, people tend to find professional or relationship problems the most troublesome. So, to deal with such situations, choose an object, such as a chair or a pillow, and let it act as a stand-in for your problem. Now yell at it — just scream out everything you feel.

It's a fun exercise. But it's also helpful, since it allows you to _Step Out_ of your problem by externalizing your feelings, which, in turn, will enable you to see it from a fresh, external perspective. Once you've done this, you'll have an easier time thinking calmly about the destructive emotions that may have given rise to the problem in the first place.

On one occasion, the author was fuming at a call-center support agent; the telephone company kept sending incorrect phone bills! After a moment, however, she was able to collect herself, and she saw her emotional reaction for what it was: blind anger directed at the wrong person. After apologizing, she was able to work together with the agent to resolve the billing issue.

The creation of space between emotions and yourself will make you a better person. There's a strategy for doing this. It's called _mindfulness,_ and it allows you to calmly observe your emotions and your environment.

Mindfulness is the practice of paying purposeful attention to something, be it a sensation, an emotion or your breathing, without judgment.

The method is based on science. In 2011, psychologists at Harvard took brain scans of people before and after they had gone through mindfulness training, and the comparison showed that the parts of the brain responsible for stress, memory, empathy and identity had been positively altered by the training.

Mindfulness can be seen, therefore, as an antidote to distraction. When you're mindful, you'll recognize negative emotions for what they are as they arise, and learn to handle them immediately.

The next step, once you're attuned to your emotions, is to look at your life and decide what you want from it.

### 6. It may be difficult making decisions that are truly your own, but it's important to take the time to do so. 

In 2000, Tom Shadyac, the director of Hollywood blockbusters like _Ace Ventura: Pet Detective_, seemed to be on top of the world. He was successful; he was rich; he was still relatively young.

But he wasn't happy. He'd spent years working toward an ideal life, and yet, when he got there, he longed for something different, something simpler.

It just goes to show that it's hard to make decisions that are truly our own, especially when success is judged by standards that have nothing to do with the individual involved.

If we don't think about it, we tend to blindly follow the examples of those around us or live according to an abstract blueprint. The name for this phenomenon is _social contagion_. You might think you want to play golf, for example, but in fact, you only play because your peers do. The same holds true of money, property, cars and family.

But unless you actually happen to enjoy the same activities as your friends, you may feel empty. This is why Tom Shadyac eventually sold most of his possessions, donated significant amounts of money to charity and opened a homeless shelter. He found it more fulfilling to do what he wanted to do, rather than what society expected of him.

So fight the urge to follow the herd, and think carefully about what you really want for yourself and from life. A sense of clarity will change your life for the better.

One psychological exercise you might like to try is writing a letter to your future self. Can you imagine who you'll be then, and what you would like to say to that person now? Write about who you are now, and what's important to you. Writing will help you articulate what matters most to you.

It's no parlor game, either. The results speak for themselves. A 2013 study by psychologist Karen Gelder showed that participants became less likely to take part in illegal activities after engaging in exercises like this one.

### 7. You can Move On in your emotional life by improving your availability for emotional bonding. 

Consider this normal relationship quarrel. Cynthia's been scrimping and saving for years. And now David, Cynthia's partner, wants to splurge the money on a family trip to the Grand Canyon.

Well, this is actually a real-life example from a 2004 study. Cynthia and David were actually being filmed as they argued, as psychologists Driver and Gottmann attempted to work out what made couples tick by looking at various emotional factors.

For this purpose, the two psychologists built an apartment in their lab. They then invited various couples in and asked them to live their lives as normally as possible while the cameras were rolling. It might sound like an odd experiment, but it did reveal some deeper truths about happiness in relationships.

Most importantly, the researchers found that the manner in which couples responded to requests for emotional bonding was critical for building mutual happiness and for _Moving On_ positively in their emotional lives.

They saw that partners engaged in all sorts of activities, all geared to getting emotional responses from each other, such as pointing out a beautiful object.

The partner being petitioned for engagement then tended to react in one of three ways. They either turned toward their partner and offered some sort of response, turned away and didn't respond at all or they reacted strongly against their partner's suggestion, saying, for instance, that they wanted to be left alone.

These small behaviors might have seemed inconsequential at the time. But when the researchers revisited the couples six years later, those who had shown high levels of positive emotional response to requests for attention were all still married. Those who had turned away or ignored their partners during the experiment were mostly separated or divorced six years later.

So, in order to Move On with your emotional agility, practice making yourself more emotionally available to your partner or friends. Now, let's look at why leading a balanced personal life is also important.

### 8. Stay at the top of your game by making sure you’re challenged and stimulated – but within reason! 

Do you remember what it took to learn to ride a bike? The excitement, the crashes, the elation when you finally managed to keep it going straight?

Does cycling still bring you the same feelings? Of course not. It's now just run-of-the-mill.

Well, it turns out that in order to thrive you need to be challenged and develop emotional agility. Once you get too proficient at something, it's easy to switch on autopilot. This, in turn, leads to rigidity, disengagement and boredom.

It's fine to go through the motions for mundane tasks like brushing your teeth. But life quickly becomes dull and unfulfilling if you aren't consciously challenged by it. A routine task or job just gets tedious.

It's important, then, to spice things up a bit. Search for something a little daring. Maybe you could launch a new initiative at your workplace?

That said, though it's good to be stimulated, you shouldn't overdo it.

The trick is to balance a little positive stress from new situations with secure feelings of assurance and calm. This is known as living at the edge of your potential. That edge is a line that can be gradually pushed forward, but you shouldn't suddenly overstep it.

A good way to start challenging yourself, perhaps, is to learn a language or how to play a musical instrument.

Or you could find small challenges in your daily tasks. You could, for instance, walk to work in a mindful manner, paying attention to your surroundings and movement, instead of daydreaming or fretting about your to-do list.

So we've looked at what it takes to practice emotional agility. Let's consider an example from the workplace to round things off.

### 9. When we’re not emotionally agile, we get stuck. At that point, we should take steps to free ourselves. 

It's easy to get the wrong impression of someone. The person might seem picture perfect, with a good job and a close family, but then, after you've exchanged a few words, you might get the sense that a nervous breakdown is brewing, and that he or she is in fact stuck in emotional distress.

When we're not emotionally agile, we get stuck.

Erin, a friend of the author, got trapped in just this way. She was a mother of three, working a job four days a week, but she really struggled to keep her two lives separate. However, she did not show her distress to anyone, or take the time to make changes.

On one occasion, her boss scheduled a phone meeting on her day at home. Erin felt she couldn't say no, but she was also acutely aware that it would be embarrassing if her children could be heard mid-kerfuffle in the background. She ended up taking the call in her closet, crouching beneath her clothes.

It was right there, ensconced in her closet, that Erin realized she had to move beyond her threshold of discomfort, talk with her boss and improve her situation.

This is the great benefit of emotional agility. It helps us make the changes we need to, in order to get unstuck in our lives.

So she mustered all her courage. She needed to know her feelings and explain to her boss just what was wrong. Here's what she came up with. She resented the troubles she had balancing her work and family life. She was struggling with perfectionism. She took the time to explain that while she loved her work, her day off was sacrosanct. She needed it for her family.

The clarification helped all parties and Erin could finally stop being so anxious.

The lessons of emotional agility are clear and they can be applied to work relationships and life generally. Remember to distance yourself from the negative patterns in your life, move out of your comfort zone and find creative solutions. The benefits are yours for the taking.

### 10. Final summary 

The key message in this book:

**If you're trying to find more fulfillment in your life, you need to develop your emotional agility — the ability to put distance between yourself and your negative emotional patterns. This gives you the space to examine them and the room to find constructive solutions for your problems.**

Actionable advice

**Skip the small talk and aim for meaningful conversations.**

The next time you're spending time with friends or family, ask yourself if you're really engaging with them, or whether small talk is being used to deflect or avoid real issues. Don't be afraid. Go deep and go meaningful. You'll all feel better for it in the long run.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Positivity_** **by Barbara L. Fredrickson**

_Positivity_ presents the latest research into the positive emotions that are the foundation of our happiness. By presenting different strategies to increase the amount of positive emotions you experience, this book will help you adopt a positive general attitude toward life.
---

### Susan David

Susan David is a medical psychologist. She runs the Institute of Coaching at the McLean Hospital, in Massachusetts, and has worked for major companies as a consultant. Her writings have been published in the _Harvard Business Review_ and the _Wall Street Journal._

