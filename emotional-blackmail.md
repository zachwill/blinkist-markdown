---
id: 572c63f1d77ba6000351702f
slug: emotional-blackmail-en
published_date: 2016-05-10T00:00:00.000+00:00
author: Susan Forward, PhD, with Donna Frazier
title: Emotional Blackmail
subtitle: When the People in Your Life Use Fear, Obligation, and Guilt to Manipulate You
main_color: EB2F42
text_color: 9E202D
---

# Emotional Blackmail

_When the People in Your Life Use Fear, Obligation, and Guilt to Manipulate You_

**Susan Forward, PhD, with Donna Frazier**

_Emotional Blackmail_ (1997) helps us understand, identify, confront and remedy manipulation in our closest relationships. These blinks are filled with insightful explanations about the true nature of toxic relationships and provide you with the tools you need to break out of this vicious cycle.

---
### 1. What’s in it for me? Understand, detect and resist the extortionists of the heart. 

Have you ever felt guilty after saying you couldn't help a friend or family member? Did it come from you or was it something the other person said that made you feel it? If it's the latter, chances are you've been subject to emotional blackmail. But isn't that a bit too strong a word for our social interactions?

Not really. Most of us have been the victim of emotional blackmail at some point in our lives. Sometimes it's obvious, other times it's harder to see. One thing's for sure: it leaves us feeling pretty lousy. In these blinks, we'll meet couples from the author's therapy practice who show us how emotional blackmail works — and what we can do to not partake in it.

In these blinks, you'll discover

  * how emotional blackmail works best in the FOG;

  * why certain character traits might make you more susceptible to blackmail; and

  * how your points of pride might become weapons for your blackmailer.

### 2. Emotional blackmail is a subtle form of manipulation that may shape some of our closest relationships. 

Leaked government secrets or organized crime might be the first things that come to mind when you think about blackmail. But blackmail is something that happens in our private lives, too. _Emotional blackmail_ may be lurking behind some of your closest relationships.

Like other types of blackmail, emotional blackmail is rooted in a fundamental _threat_. Usually, it's something along the lines of "If you don't do this for me, you will suffer the consequences."

Classic emotional blackmail occurs when someone threatens to end a relationship when she knows the other person cares too much about her to let that happen, _and_ that the person would do anything to prevent it if forced. Essentially, whenever people threaten to make you suffer if you don't give them what they want, you're being emotionally blackmailed. Chances are, you've been emotionally blackmailed more than a handful of times over the course of your life.

So why do we let it happen to us? The problem is that it's all too easy to walk straight into a blackmailer's trap _without realizing_ it. Emotional blackmail happens in close relationships, as the blackmailer knows you and your weaknesses extremely well. For instance, a blackmailer might get the best of someone who prides herself on her generosity, compassion and loyalty by calling her greedy, insensitive and untrustworthy. Unless, of course, she does what the blackmailer wants her to do.

Despite the deep psychological suffering this behavior causes, emotional blackmail remains hard to recognize for another reason: we don't want to see it. Let's face it, the fact that your loved one is manipulating you is a bitter truth to swallow. Many of us would rather turn a blind eye. But if you've decided it's time to weed out toxic relationships in your life, stay tuned for the next blink.

### 3. There are six key symptoms of emotional blackmail. 

Although emotional blackmail is hard to spot, we can diagnose it by seeking out the six key symptoms that distinguish it: _desire, resistance, pressure, threats, compliance_ and, finally, _repetition_.

Emotional blackmail begins with an individual's _desire_ being blocked by resistance. Anna and Artie were a couple seeing the author together for therapy. Artie wanted to move in with Anna — that was his _desire_. But Anna, though she deeply cared about Artie, wasn't ready to live with him yet. She explained that having her own space was important to her — that was her _resistance_ to Artie's desire.

The next stage of emotional blackmail is marked by the symptoms of _pressure_ and _threats_. In this case, Artie didn't respect Anna's need for her own space and started to _pressure_ her in the hopes that she would change her mind. He would talk about how he wanted to take their relationship to the next level because he was committed to her. He would even ask Anna if she really loved him or cared about the relationship like he did.

The problem here was that, although Artie demanded empathy, understanding and agreement to his desires from Anna, he did not offer her these things at all. Instead, his pressure escalated to become veiled emotional threats as he declared he might leave her if they couldn't live together.

But it didn't end there. The final two symptoms of emotional blackmail are what make it especially dangerous: _compliance_ and _repetition_. Once Artie was successful and Anna gave in to his will, he won her _compliance_ through her fear of hurting or losing him. What's worse, this compliance is the beginning of a slippery slope: once Artie learned he could guilt Anna into meeting his demands, he became likely to _repeat_ this strategy, creating _repetition_ and a vicious cycle of emotional blackmail.

### 4. FOG – or fear, obligation and guilt – blinds us from emotional blackmail. 

In the first blink, we learned how easy it was to turn a blind eye to emotional blackmail. Now let's take a closer look at how the FOG, that is, _fear_, _obligation_ and _guilt_, of emotional blackmail prevents us from seeing the actual state of our relationships.

_Fear_ is the fuel of emotional blackmail. The blackmailer acts out of fear, using it as a weapon against their partner. People prone to manipulative behavior are often driven by a powerful fear of abandonment leftover from negative childhood experiences. This deep fear of abandonment can cause people to manipulate others they care about as soon as they fear they aren't truly loved.

Take Margaret and Mark's story, for example. As a married couple, Margaret was the breadwinner who provided for both of them. Mark secretly feared that his wife would leave him for someone who was better off financially and professionally. So what did he do? By reminding her how depressed she felt when she was single, and warning her that no other man would take her if they split, Mark blackmailed Margaret into staying by his side. She resented this, but was too afraid to speak out against it for fear of Mark leaving her.

_Obligation_ and _guilt_ are blackmail weapons that go hand in hand with each other. When they arise naturally, these feelings are perfectly normal. But when used in blackmail, obligation and guilt can make us feel lost and helpless.

A mother prone to manipulating her grown-up children might use obligation and guilt to get them to take her on a vacation. She might refer to how she put so much into raising them, how it's the least they could do to show their appreciation, or that it's simply what caring children would do. Of course, if her children are negligent and hardly spend a second thinking about her, perhaps these claims might be more appropriate. But if her kids are continually generous and dedicated to her, that's when she's crossing the line.

Every relationship is different, but when fear, obligation and guilt are used to cross normal boundaries, it's a case of emotional blackmail. In the next blink, we'll take a closer look at how you can assess your own relationships, keeping their unique characteristics in mind.

### 5. A drive to find a fair solution means separating other conflicts from the emotional blackmail itself. 

Not all conflict in relationships is a case of emotional blackmail. How can you tell? The key difference is whether your partner really wants to find a solution or whether he simply wants to win.

People who actively attempt to resolve conflicts will speak openly about what they want while taking into consideration their partner's feelings and desires. They try to clearly and cooperatively find the source of the tension, and understand and accept ownership for their contribution to it.

We see this in the story of Jack and Jill. They'd been married for a long time, and one day Jack confessed to Jill that he'd been unfaithful.

Naturally, Jill was upset and hurt. After taking some time for herself to consider her feelings, she asked him to recommit to an exclusive relationship and to go to couples therapy so that their marriage could continue. Jill promised that she wouldn't use the fact of Jack's infidelity against him to get what she wanted later down the road. Despite the complexity and heart-wrenching nature of such conflicts, they are also an opportunity to make a relationship stronger than ever before if both people demonstrate respect, are open with one another and set boundaries, as Jill did.

While Ron and Rori faced a similar issue in their marriage after Ron admitted his infidelity, the outcome was rather different. Unlike Jill, Rori used Ron's mistakes as ammunition when she wanted something from him, whether it was his undivided attention or expensive presents. Meanwhile, the reasons for Ron's infidelity and the path to their reconciliation received no attention at all. By guilt-tripping Ron whenever she could, Rori was perpetrating emotional blackmail.

### 6. Victims of emotional blackmail should by no means become enablers. 

We're all susceptible to emotional blackmail — though some of us more than others. It all comes down to a handful of traits that make certain people more vulnerable. These include the need for validation from others, difficulty with confrontation, low self-esteem and a desire to save others. People with these traits are likely to prioritize the blackmailer's needs above their own.

This was the case with Elliot and Eve, an artist couple. Elliot was quite successful, while Eve was just getting started. Eve moved in with him early on in their relationship to save money. Later on, she expressed interest in taking graphic design classes, but Elliott was entirely against the idea.

He felt that she was abandoning him by doing something new on her own — even competing with him. On the morning of her first class, he verbally attacked her, demanding to know why she was so insistent on hurting him in this way. A part of Eve knew that going to class was the right thing for her to do. And yet, her inability to handle intense confrontations made her doubt herself. She stayed home and dropped the course.

As we know, blackmail usually doesn't occur just once but becomes a cycle. When we give in to blackmail, we feel ashamed as our integrity and self-worth have been compromised. Because of this, our resolve to resist blackmail is even weaker the next time around.

This was the case with Eve. After dropping out of her class, she was overcome with self-loathing and was confused about and frightened of her responsibility for Elliot's well-being. Elliot, meanwhile, knew exactly how to coerce Eve into compliance who gave in more readily each time. The lesson here? Though victims of blackmailing aren't to blame for it happening, they are responsible for resisting the fact of its existence. For those doubting their abilities to resist blackmail, the next blink provides the necessary steps to ensure you keep your integrity intact.

### 7. Nip blackmail in the bud by responding at your own pace and objectively observing the situation. 

How can we avoid falling prey to emotional blackmail? Here are a few simple steps you can take.

The first is to _resist reaction_ to the blackmailer's provocations. Blackmailers may attempt to create an atmosphere of tension that makes victims feel like they have to comply immediately or continue to suffer. To avoid this, be firm and buy yourself some time.

Lines like "I don't have an answer for you right now, so give me some time to think" and "I'm not ready to make a decision at this point. Let's talk about it later" are great to keep on hand. Even if blackmailers don't react amicably to this, don't let yourself get worked up: simply repeat your statement and stay calm.

Remember, their timeline does not define yours. You have your own priorities which are just as important as theirs. Take the time you need to consider your response to a conflict, and you'll be on your way to breaking down manipulative patterns in your relationship.

The next thing to do is _detach_ yourself from the situation. Detachment helps to get some perspective on the conflict. If you're finding it hard to see things from the outside, try asking yourself the following questions: "What does my partner want? How did he ask me? How did he react when I didn't agree with him?"

Next, think about your emotional reactions to see if they exhibit blind spots or willful delusions. Sentiments like "I'm responsible for his happiness; he needs me; I'm the only one he has," "If I give in this time, he won't pressure me anymore" or "I'm so selfish to resent his demands" are red flags. If you're feeling trapped, frightened, frustrated, overwhelmed, angry or guilty, think about what triggers these feelings. Does your blackmailer give you the cold shoulder, roll his eyes, cry or shout?

Write these thoughts down for the final step, making connections between his actions and your reactions. With this mapped out, you're ready to end the cycle of emotional blackmail once and for all.

### 8. Set boundaries and speak up to end the cycle of emotional blackmail. 

If you've realized over the course of these blinks that you're a victim of emotional blackmail and want to put an end to it, what should you do? Here are some final tips to get you on your way.

First off, get to know your boundaries. If it's clear to you that, after mapping out your partner's actions and your reactions, her manipulative behavior is weighing on you, then don't push this aside. Instead, dig deeper and work out when your boundaries have been crossed. This way you'll recognize when she's going too far in the future. You'll also be better able to communicate your boundaries, so your partner knows exactly what you will and won't tolerate.

The way you communicate with your partner is also crucial at this stage. Don't attack her or be offensive: be understanding, frank and calm. Express your regret that she's upset, tell her you can understand why she feels that way, let her know you respect her opinion but that you're going to have to agree to disagree.

You can also be upfront and tell her that you won't be guilt-tripped into anything, and that it's not the end of the world if you want different things. These statements will prevent your fight from escalating while ensuring you stay firm with your priorities.

Finally, remember that it will take time to change your blackmailer's habits and your relationship. Improved communication, new boundaries and careful reflection will help you ensure that you're no longer the victim of emotional blackmail. It may also be time to walk away from the relationship if necessary. It all depends on how much work you and your partner are willing to put into making a change.

### 9. Final summary 

The key message in this book:

**There's no need to feel trapped and overwhelmed by your relationship. By learning to recognize the signs of emotional blackmail and its impact on your life, you can begin taking steps toward standing up for yourself, setting boundaries and creating change in your relationship.**

Actionable advice:

**Listen to your inner voice**

If you feel like your partner is being unreasonably demanding, don't just push that thought aside. Listen to your feelings and watch for reactions of guilt and fear. In order to stand up for yourself, you've got to know when and why you feel powerless. Once you realize what's going on, you'll understand that you don't deserve to feel that way anymore.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Narcissist You Know_** **by Joseph Burgo**

"Narcissism" has become a buzzword and a snap diagnosis, but how much do we really understand about this condition? _The Narcissist You Know_ (2015) unpacks the myths and the truths. Narcissism isn't just a serious psychiatric disorder, it's part of life — we all share some tendency toward it. By analyzing a wide range of narcissists — many of them celebrities — Joseph Burgo reveals the hidden shame that lies behind all the pain.
---

### Susan Forward, PhD, with Donna Frazier

Susan Forward, PhD, is a therapist and an internationally renowned expert in relationship and familial conflicts. She is the author of the number one _The New York Times_ bestsellers _Men Who Hate Women and the Women Who Love Them_ and _Toxic Parents_.

