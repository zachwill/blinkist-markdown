---
id: 55ae019b3366610007000000
slug: emotional-intelligence-2-dot-0-en
published_date: 2015-07-24T00:00:00.000+00:00
author: Travis Bradberry and Jean Greaves
title: Emotional Intelligence 2.0
subtitle: None
main_color: F68131
text_color: 8C4A1C
---

# Emotional Intelligence 2.0

_None_

**Travis Bradberry and Jean Greaves**

_Emotional Intelligence 2.0_ (2009) gives you expert insight into which skills you need to read others and build better relationships. It breaks down the four aspects of emotional intelligence, or EQ, and gives advice on what you can do to improve your own skills.

---
### 1. What’s in it for me? Improve your emotional intelligence and succeed in work and life. 

To maximize your career potential, you need to strengthen your people skills. Even if you're a strategy expert or tech genius, if you want to climb the corporate ladder, you need to know how to interact with people, whether employees, investors or customers.

And people skills are just the start. To survive the stress of modern working life, you too need to become an expert self-manager, able to control your emotions — even under fire.

Thus success today depends largely on having strong levels of emotional intelligence, or EQ. Solid EQ is what allows you to develop strong relationships not only with others but also with yourself. These blinks provide tested ways for you to learn what it takes to become more emotionally intelligent.

In these blinks you'll discover

  * why you should get to know your coworker's nickname;

  * how you can embrace public shaming to get stuff done; and

  * why you can tell a lot about someone from just looking at their shoulders.

### 2. This is emotional intelligence: know yourself; behave yourself; know others; build bonds. 

Some people are uncommonly good at reading body language, able to judge at a glance what someone is thinking — and respond accordingly. They can calm a coworker who's angry, or reassure a friend who's anxious.

Why are certain people so good at this? The answer has to do with _emotional intelligence,_ or _EQ_.

Emotional intelligence is the ability to recognize and understand your own feelings and the feelings of others, and influence those feelings to your advantage. A person who's good at "reading" people has high emotional intelligence.

EQ combines four different elements.

The first is _self-awareness,_ or the ability to understand your own feelings and behaviors.

The second is _self-management._ Self-management is about keeping yourself in situations in which you know you'll be able to behave correctly.

The third is _social awareness_. Once you know how to manage your behavior and understand your feelings, you'll know how to read the emotions of others. You'll understand what makes people feel angry, sad or excited, and you'll be able to better read body language.

The fourth and final element is _relationship management_. Understanding your own behavior and the behavior of others around you enables you to build stronger relationships with the important people in your life.

For example, if you know that one of your coworkers gets upset when he's criticized, you'll know how to give feedback to which he'll be able to respond more effectively.

Let's look closer at the four elements of emotional intelligence. Read on!

> _"The link between EQ and earnings is so direct that every point increase in EQ adds $1,300 to an annual salary."_

### 3. Improve your self-awareness by getting to know yourself – the good, the bad and the ugly. 

How well do you know yourself?

You might know what sort of things you like or dislike, but _self-awareness_ is more than that. It's about deeply understanding your emotions, so you don't become overwhelmed by them.

It's important to understand why you feel the way you do, including when you're angry or annoyed. Being in a bad mood doesn't necessarily mean you have to have a bad day.

Maybe you left your briefcase at home, spilled coffee all over your desk or had trouble getting your coworkers to stay on task. On days like this, you might start to feel negative about everything and even lash out at small things — which only makes things worse.

When you find yourself in a bad mood, just remember that such feelings do pass. Whatever has put you in a bad mood can't possibly be the end of the world, so why overreact?

Don't lose sight of your self-awareness on good days, either. We tend to rush into things head first when we're in a good mood.

Imagine your favorite store is having a sale with markdowns of 75 percent, for example. You might want to run in and buy everything you see!

In that moment, your excitement overpowers your other emotions. You might forget to pause and ask yourself if you really need the things you're buying — and your good mood would probably turn sour quickly when those credit card bills come due!

So check yourself and think twice when you get excited — don't make rash decisions just because you're caught up in the moment.

Remember to always consider what the consequences of your actions might be.

### 4. To practice better self-management, you need to balance your emotional and logical sides. 

Many of us find it challenging to manage ourselves. When we try to assert control, we often get sidetracked if how we feel inside pushes us to act differently. We also tend to give up if things start to get too rough.

Good _self-management_ is a crucial part of having high emotional intelligence.

One useful tool when you're faced with a tough decision is to make an _emotion versus reason_ list.

Draw a table with two columns. The first column is where you write down what your emotions are telling you to do; the second column is for what your logical reasoning tells you to do. This exercise helps you balance each side and keeps one from dominating the other.

See which list has the stronger points. Are your emotions clouding your judgment? Are there any holes in your logic?

Let's say you're faced with deciding whether to fire an employee. His work isn't up to par, yet you like him a lot as a person. This is a great opportunity to make a _emotion versus reason_ list.

Telling your friends and family about your goals is another good self-management tool. Your loved ones can do a lot to motivate you and help you stay on track.

If you tell your friends what you want to accomplish, they'll hold you accountable, which can be a great source of motivation. You don't want to let your friends down, after all!

One professor constantly struggled to meet deadlines, and decided to make a change. His self-management strategy: he told his colleagues he'd pay them $100 whenever he missed a deadline! Sure enough, he changed his behavior for the better.

### 5. Eyes, mouth and shoulders communicate a lot. Learn how to read the signs to know what best to say. 

Some waiters always seem to know what a customer wants. They instinctively know who wants to be left alone, or who wants company or special attention.

Such a person has a high level of social intelligence. To become more socially aware, there are two things that you should consider.

First, watch a person's body language to get a sense of what he's feeling, and plan your responses accordingly.

Start with a head-to-toe body language assessment. Check the eyes first, as they give away a lot of emotional signals. Too much blinking might indicate deception, for example. Then move to the mouth. Is the smile sincere or fake? Only authentic smiles come across as truly sympathetic.

Next, check the person's shoulders. Are they slouched, or tight? Shoulders can express self-confidence or even shyness.

Once you've read the message a person's body is communicating, adapt yourself to it. If someone is angry, it's not a good time to pester him with questions — wait until he's in a better mood.

Second, be sure to greet a person by his name. Doing so helps you come across as warm and trustworthy. People with high social awareness usually don't address others as "Sir" or "Mr. So-and-so." Instead, they make an effort to learn people's first names.

Addressing a person by his first name will strengthen your relationship with him. You can learn a lot about someone from their name (like family history or identity, if they have a nickname).

Use this knowledge to further your bond and make the person feel appreciated.

### 6. Don’t send mixed signals. Make sure your body language is in line with your words. 

Have you ever promised to keep in touch with a friend who has moved to another town? A lot of the time, such promises are easily broken. What happens in this situation?

Well, relationships take a lot of time and effort to maintain — and most of us just can't commit.

Yet the ability to build _strong relationships_ is a crucial part of emotional intelligence. So how can you build and maintain them effectively?

First, check your own body language to make sure you aren't sending mixed signals. Your body, voice and behavior convey a lot of information, so to avoid confusing others, make sure what your body is saying is clear. It's frustrating and disorienting to receive mixed signals!

Let's say you need to congratulate your employees for great work, but you're upset because you fought with your wife that morning. If you mumble through your speech while frowning, your compliments won't seem sincere. Your body language and voice don't match your message.

Another important key to building future relationships is to get feedback from people already in your life. Feedback is vital if you want to improve your relationships, yet it's often hard to respond to feedback well.

We often take feedback badly or simply ignore it — who likes being criticized, really? Yet learn to take constructive criticism by reminding yourself that the person offering it is really just looking out for your best interests. Try to use the feedback to improve yourself.

And remember to thank anyone who gives you helpful feedback. Always show your loved ones that you appreciate them!

### 7. Final summary 

The key message in this book:

**You can increase your level of emotional intelligence, or EQ. Focus on understanding and controlling your own emotions and behaviors first, then build your relationships by paying attention to body language and addressing people in a personal way. Balance your emotions with rational thinking, and never stop trying to improve yourself through feedback. When you have high emotional intelligence, you don't just relate better to others — you'll manage yourself better, too.**

Actionable advice:

**Listen actively.**

Listening to other people's thoughts and reading their emotions is an integral part of having a high EQ. So pay serious attention when you listen — it'll give you more insight on a person's mind and show that you care about her thoughts.

**Suggested further reading:** ** _Emotional Intelligence_** **by Daniel Goleman**

_Emotional Intelligence_ is a #1 bestseller with more than 5 million copies sold. It outlines the nature of emotional intelligence and shows its vast impact on many aspects of life. It depicts the ways emotional intelligence evolves and how it can be boosted.

It poses an answer to the overly cognition-centered approaches to the human mind that formerly prevailed in the psychological establishment.

It will present the reader with new insights into the relationship between success and cognitive capabilities, and a positive outlook on his possibilities to improve his life.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Travis Bradberry and Jean Greaves

Travis Bradberry and Jean Greaves have written several award-winning books, such as _The Emotional Intelligence Quick Book_ and _Leadership 2.0._ They're also the founders of TalentSmart, an international consultancy for emotional intelligence training in business.

