---
id: 57d65c6b08c9270003b191be
slug: emotional-intelligence-for-project-managers-en
published_date: 2016-09-15T00:00:00.000+00:00
author: Anthony Mersino
title: Emotional Intelligence for Project Managers
subtitle: The People Skills You Need to Achieve Outstanding Results
main_color: 2CB3DE
text_color: 1D7591
---

# Emotional Intelligence for Project Managers

_The People Skills You Need to Achieve Outstanding Results_

**Anthony Mersino**

_Emotional Intelligence for Project Managers_ (2007) builds on the writings of Daniel Goleman, who developed principles of _emotional intelligence_. Emotional intelligence is crucial in many aspects of life, and in these blinks Anthony Mersino tailors Goleman's principles specifically to the needs of project managers.

---
### 1. What’s in it for me? Become a superb project manager! 

Consider this combination: project management and emotional intelligence. Perhaps that seems like an odd matchup, but, in fact, these things are intricately connected — at least, they are if you want to be a successful project manager.

But do you know how to combine these things?

Well, this is exactly what these blinks seek to teach you. Read on, and enhance your abilities as a project manager by learning all about concepts related to emotional intelligence, including self- and social awareness as well as self- and relationship management.

In these blinks, you'll discover

  * why emotional intelligence is especially important for project managers;

  * the wisdom of setting emotional boundaries; and

  * how to build strong relationships with your stakeholders.

### 2. Emotional intelligence can advance your career as a project manager. 

Have you ever felt like your workplace was stifling your emotions? If so, you certainly aren't alone. A lot of project managers deliberately try to create an emotion-free work environment. That, however, is a big mistake. Positive emotions (and even some negative ones!) can actually _help_ in the workplace.

A bit of _emotional intelligence_ can help lead a project to success. Emotional intelligence, a term popularized by Daniel Goleman's 1995 best seller, _Emotional Intelligence: Why It Can Matter More Than IQ_, refers to our ability not only to identify emotions in ourselves and others, but to use those emotions to guide our behavior.

For project managers, emotional intelligence should be geared toward improving work performance. Emotional intelligence is very powerful. In fact, one study of the relationship between IQ, emotional intelligence and project managers found that a manager's emotional intelligence often correlated with a project's success, whereas there was no correlation between success and IQ.

And emotional intelligence isn't only critical for managing your project — it gives you a competitive advantage as a manager, too!

Emotional intelligence allows project managers to develop positive relationships with their team members and stakeholders, and it can give them the edge on competition, too. If you're good at working with people, you'll set yourself apart from other managers, and therefore get better projects and more chances to use and develop your emotional skills

And the competition is rising! Every year, more and more people try to get their PMP (Project Management Professional) certificate. In 1995, only 4,410 people took the test; in 2011, 500,000 took it! In this increasingly competitive work environment, you need to make yourself stand out in any way you can. Emotional intelligence can do a lot in this regard.

> The term _emotional intelligence_ was first used by psychologists Peter Salovey and John D. Mayer in 1990

### 3. The basis of emotional intelligence is self-awareness. 

_Self-awareness_ is the base of emotional intelligence. It refers to your ability to recognize your own emotions and the effect they have on those around you. And it comprises three components: _emotional self-awareness_, _self-assessment_ and _self-confidence_.

Emotional self-awareness is about understanding your own feelings. This is often a challenge. Luckily, you can use the _SASHET_ framework to identify those elusive or frightening emotions.

Emotions are always in flux, making them distinctly difficult to pin down and analyze. Moreover, many of us were raised in emotionally unhealthy environments and thus struggle to come to terms with our negative emotions in adulthood. Early on, the author learned to suppress his emotions, which, if expressed, would often set off his alcoholic father. And this early habit carried over into adulthood.

That's why the SASHET framework is useful. SASHET stands for the six families of emotion: _Sad, Angry, Scared, Happy, Excited_ and _Tender_. Those six families are further subdivided into many other emotions, but it's a good start if you can identify which family you're in at any given moment.

The next step in achieving self-awareness is to master _self-assessment_, the ability to pinpoint and explore your own strengths and weaknesses. Self-assessment is also about listening to feedback about yourself and your behavior, and adjusting yourself accordingly.

Finally, work on your _self-confidence_. Self-confidence, which builds off self-assessment, is about being able to maintain your composure, no matter the situation.

A person with high self-confidence is centered at all times. When you're truly self-confident, you essentially understand yourself so well that you're no longer affected when others doubt you.

> _"Emotions provide us with information about our environment."_

### 4. Control your own emotions through self-management. 

Naturally, as a project manager, you manage other people all the time. But have you ever thought about managing yourself?

_Self-management_ is an integral part of emotional intelligence. It enables you to prevent your emotions from taking charge of your behavior.

Emotions can easily take over, largely because the _amygdala_, the reactive part of your brain, is faster than the _neocortex_, the part that controls logical thinking. Your brain can push you to react to something before you've even been able to consciously think it over!

That's how emotional breakdowns happen. So, avoid a breakdown by pausing to give your neocortex some time to process what's going on. If you can't identify and consciously think about your own emotions, they'll take over.

If you're afraid of failing at something, for example, you might start procrastinating in attempt to avoid the project altogether. But you'll only end up creating a self-fulfilling prophecy: you'll definitely fail if you don't try. Managing such a situation is a matter of managing your own fear.

Effective self-management also requires _self-control_. Self-control is the ability to stay calm, even when your emotions are going wild. It's certainly difficult at times, but there are some good techniques that will help you stay cool:

First off, identify what triggers your biggest emotional reactions — those times when your emotions try to take over. You can't manage your triggers if you don't know what they are.

You can also reduce your stress by avoiding long workweeks. Take care of yourself, both mentally and physically!

Finally, make sure you have a healthy network of support. Talk to your friends when you need help managing your emotions and ask them for feedback on your behavior.

> _"Tough times tend to reveal our true emotional nature."_

### 5. Understand the people around you through social awareness. 

The next key part of emotional intelligence is building strong relationships with other people, and that requires _social awareness_.

Social awareness is about understanding the emotions of others. It's made up of four parts: _empathy, organizational awareness, seeing others clearly_ and setting _emotional boundaries_.

_Empathy_, the first component, refers to your ability to understand how other people feel. You're able to recognize another person's emotions — whether positive or negative — and engage accordingly.

As a project manager, that means you need to listen to your team members using _empathetic listening_. Don't just listen to their words alone; try to figure out what they're _really_ feeling.

You also have to master the ability to _see others clearly_. That means understanding people and accurately assessing their strengths and weaknesses. It's more challenging than it seems. Even if you don't like the person, strive to think objectively; otherwise, you might impose your own bias. Take the time to really _look_ and _think_.

_Organizational awareness_ is closely linked with seeing others clearly. Organizational awareness is the ability to understand the emotional context of your particular company, project team or other organization. What's the power structure? Which emotions does it generate?

The last step is setting _emotional boundaries_ that protect you from the emotions of others. It's important to relate to other people's emotions, but you shouldn't allow them to control you.

Altering your language is a good way to help with this. So if you're angry with someone, for example, say you _feel_ angry, not that they _made_ you angry. Taking responsibility for your feelings is always better than blaming them on someone else.

> _"Leading others is all about relationships."_

### 6. Use relationship management to build positive relationships between teams and stakeholders. 

Project managers have to build relationships more often and more quickly than most people do. That's why _relationship management_ is crucial!

Relationship management has two key components: establishing _stakeholder relationships_ and _developing others_.

There are four steps to establishing strong stakeholder relationships:

First, identify the _project stakeholders,_ anyone who affects or is affected by the project: the team members, vendors, sponsors and so on.

Next, gather as much information as you can about them. What role does each stakeholder play? What are their goals and interests? How do they communicate?

After that, develop _relationship strategies_ for them. Figure out what your most effective relationship with them would be and start working toward it.

Finally, keep managing the relationship — it's an ongoing process. Check regularly that things are going well and your relationships are producing the results you want. You might do this by establishing regular meetings to check up on how everyone feels.

The second part of relationship management is _developing_ your team members. That means acknowledging and commending their strengths, mentoring them and providing them with useful feedback.

Developing others is ultimately a form of investing in the project team. So, first off, acknowledge their strengths: make them aware of their potential and always say "thank you" when they contribute to the project.

Next, give them _targeted feedback_. Targeted feedback is clear, objective and aimed at helping the person improve, not at putting them down. So don't emphasize someone's laziness; instead, focus on where they could improve. It's always better to talk about what could make someone's performance better, as opposed to what made it bad. 

Finally, never stop mentoring and coaching your team members. Offer advice and encouragement whenever you can. Make sure they feel that they can always talk to you and that you'll listen to what they have to say.

> _"The success of the team is largely a result of the strength of the relationships among its members."_

### 7. Use your emotions to successfully lead your team. 

The final aspect of emotional intelligence for project managers is _team leadership_. Team leadership is the ability to lead the project team toward their goals in a healthy and effective way. It's how you overcome conflicts and keep everyone on track.

_Communication_ and _conflict management_ are the two most important skills here.

Positive communication allows you to set the right emotional tone in all exchanges. It's a big part of your job as a project manager, whether you're communicating with stakeholders, team members, in an interview or over lunch.

Project managers have to communicate in a way that creates the right emotional atmosphere and avoids any feelings of negativity.

Imagine you're about to hire new staff, for example, and you're nervous about conducting the interviews. If you don't come to terms with your fear, it could cause you to overlook the right candidate or hire one that isn't right for the position.

Instead of succumbing to fear, create a comfortable setting and approach your interviewees with empathy. Look for cues about what they're feeling and share your own emotions when appropriate. Afterward, be sure to ask them how they felt about the exchange.

Emotional intelligence will also help you manage any conflicts that arise. That's where skills like _compromising_ come in. Compromising means solving the problem by openly discussing it and getting both sides to give up some of their demands.

You need to be self-aware and manage your own emotions in order to hold a truly open discussion; if you're not, your emotions might take over. You also need to listen empathetically and communicate carefully with everyone involved. Compromising requires a lot of emotional intelligence!

### 8. Final summary 

The key message in this book:

**Emotional intelligence is important for everyone. But it's especially important for project managers, as their job rests on human communication and solid connections with others. So work on your** ** _self-awareness,_** **manage yourself and your different relationships and listen empathetically to your team and stakeholders. Emotional intelligence doesn't just make the workplace more pleasant; it gives you a competitive edge and helps you work toward your goals, too!**

Actionable advice:

**Have lunch!**

Take a lunch break with a stakeholder instead of eating a sandwich at the office. You'll get to know them better and learn more about them in a relaxed setting. It's a good way to strengthen your relationships with the people who matter most.

**Suggested further reading:** ** _Scrum_** **by Jeff Sutherland**

Learn all about Scrum, the project management system that has revolutionized the technology industry. This is a uniquely adaptive, flexible system that allows teams to plan realistically and adjust their goals through constant feedback. By using Scrum, your team can improve their productivity (and the final result) without creating needless stress or working longer hours.
---

### Anthony Mersino

Anthony Mersino is an Agile Transformation Coach and an IT Program Manager.

