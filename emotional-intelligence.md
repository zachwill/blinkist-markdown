---
id: 52fa3823666261000c0a0000
slug: emotional-intelligence-en
published_date: 2014-02-12T09:16:20.000+00:00
author: Daniel Goleman
title: Emotional Intelligence
subtitle: None
main_color: 212BAA
text_color: 212BAA
---

# Emotional Intelligence

_None_

**Daniel Goleman**

_Emotional Intelligence_ is a #1 bestseller with more than 5 million copies sold. It outlines the nature of emotional intelligence and shows its vast impact on many aspects of life. It depicts the ways emotional intelligence evolves and how it can be boosted. It poses an alternative to the overly cognition-centered approaches to the human mind that formerly prevailed in the psychological establishment. It presents the reader with new insights into the relationship between success and cognitive capabilities, and a positive outlook on possibilities to improve his life.

---
### 1. What is in it for me? 

Some people think emotions play a role only in romantic situations or in the heat of a physical fight. Yet in fact, emotions are everywhere: they form our decisions, help us understand the world and are crucial in any interaction with others.

This book explains in detail what impact emotions have on your everyday life. It shows how they can help you, but also how they lead you astray. It also highlights the role that emotional intelligence plays in allowing us to use emotions to create positive outcomes and avoid situations where they can harm us.

It explains how emotional intelligence makes it possible to create a balanced interaction between the emotional brain and the rational brain. It also shows us how this capacity can be acquired and expanded.

Finally, it answers these interesting questions: How does emotional intelligence develop in individuals and why is this capacity so important for society as a whole?

### 2. Emotions are important; they help us learn new things, understand others and push us to take action. 

Do our emotions hold us back? Would we do better if our emotions were removed and we became unfeeling, logical creatures?

In fact, emotions are vital to us as they provide us with advantages that help us to lead fulfilled lives.

One such advantage is the way emotions help us learn from our memories.

When our brain stores experiences, it doesn't just collect facts. It also records our feelings and these feelings help us to learn from our experiences. For example, if a little boy touches a hot stove, he will experience intense pain. The thought of touching another stove in the future will carry with it the memory of that searing pain. Thus his emotions will hopefully keep him from doing it again.

Another value of emotions is the way they help us to interpret the feelings of others, which can aid in predicting their actions. For example, imagine you're faced with an angry man. From his body language — maybe his clenched fists or loud voice — you can tell his emotional state. Knowing this, you can predict his future actions; he might, for instance, be ready to hit someone.

The final advantage that our emotions give us is the drive to act. We require them in order to react quickly to a situation. Take that angry man from the earlier example. If we feel that he may be close to a violent outburst, our emotions will make us feel threatened or even angry, thus preparing us to react quickly if he looked like he was about to attack.

People who have lost their capacity for emotion also lose this drive to act. For example, in the previous century, many psychiatric patients went through a brain surgery called a _lobotomy_, which separated two regions of the brain that are vital for emotional processing. The result of the surgery was that patients lost their initiative and drive to act, as well as much of their emotional capacity.

### 3. Sometimes our emotions can impede our judgment or make us act irrationally. 

Our emotions are important tools for understanding and interacting with our environment. However, they are also flawed and can lead us to make mistakes.

One such mistake occurs when we become overly emotional. In order to make sound judgments we need to think clearly. Like a juggler, our minds can handle only so many items at once. And when we're in a state of heightened emotion, our minds are bombarded with alarming thoughts and disturbing images. Thus, there is no room for rational thought and our judgment is clouded.

For example, when you are frightened you may find yourself overreacting to situations, thinking they're more dangerous than they actually are. This is why when you're scared, you might mistake a sheet on the washing line for a ghost.

Another mistake caused by our emotions is when we act suddenly before we have the chance to judge a situation clearly. When information enters our brain, a fraction of it bypasses the region responsible for rational thought — the _neocortex_ — and directly enters the emotional brain. If it perceives this information to be a threat to us, the emotional brain can trigger us to act suddenly, without consulting our thinking brain.

This is why you may jump out of your skin when you're in a dark forest and you see a strange figure out of the corner of your eye.

The final way our emotions can lead us to act irrationally is when we are affected by obsolete emotional responses.

Our emotional mind reacts to situations in the present based on past experiences, even when the conditions have changed. For example, a boy who was physically bullied at school may grow up to be a strong man, but still feel threatened by his former bully.

So although emotions are important, they can take control of our minds and disrupt rational thinking. We therefore require something to help us manage them effectively.

### 4. Emotional intelligence enables you to manage your emotions and leverage them to reach your goals. 

So how can you use the power of your emotions without them overwhelming you?

You need _emotional intelligence_ (EI), as it allows you to recognize and manage your feelings without being controlled by them.

The first aspect of emotional intelligence is being able to recognize and name your feelings.

This step is vital to being able to manage your emotions. Studies show that people who aren't able to recognize their own feelings are more prone to violent outbursts.

Once you are able to recognize your emotions you need to become aware of what causes them.

Often your feelings in a situation depend on how you think about it. For example, if a friend of yours passes you on the street and doesn't acknowledge you, you might immediately think that they're ignoring you on purpose. This may make you upset or even angry.

But, if you stop to think why he failed to acknowledge you, you might find other reasons that leave you less upset. The friend might, for example, not have seen you because they were lost in thought and weren't concentrating on their surroundings.

When you begin to recognize and manage your feelings, emotional intelligence can help you concentrate on achieving certain goals.

For example, imagine that you need to write a paper for university. You don't really like the subject matter and you'd much rather go to a film festival instead. Emotional intelligence can help you manage these various feelings.

Although the subject matter bores you, you could try to look at it from another angle. Maybe there is one aspect of the subject that enthuses you. Also, knowing how the film festival will make you feel, you can defer the gratification and save your potential enjoyment until you have time for it.

Students who manage their workloads in this way tend to do well at school even if they have average IQs.

### 5. Emotional intelligence is the capacity that helps you navigate the social world. 

Unless you live on an island, it is unlikely that you'll develop a happy life just by managing your own mind. Other people play a large role in your existence and only by managing your social interactions with them can you hope to live a fulfilled life.

Once again, emotional intelligence can help in achieving this.

Emotional intelligence fosters good social interactions because it helps you put yourself in other people's shoes. Knowing how you'd feel in a certain situation helps you to gauge how others will feel in a similar environment.

Your emotional intelligence also helps you discover the emotions of others by analyzing their nonverbal signs. This means you can judge a person's mood just by looking at cues like their facial expressions or body language.

For example, if you see someone with a face as white as a sheet and with their mouth wide open, you will probably conclude that they have been shocked.

What's more, you'll probably identify such cues automatically, without any conscious effort.

Because it allows you to empathize with others, emotional intelligence enables you to behave in ways which evoke favorable reactions from others.

For example, imagine you are the manager of a company where one member of staff is constantly making the same mistakes. You'll need to tell him about this and get him to change, but you have to do it in the right way. If you hurt his feelings, he may become angry or defensive, and less likely to make the changes you desire. If you empathize with him and imagine how he will feel, you can act in a way which makes him more willing to change.

In general, people with emotional intelligence can develop social aptitudes such as the ability to teach others, resolve conflicts or manage teams of staff. And these aptitudes help them to maintain relationships in the social environment.

### 6. Emotional intelligence requires a balance between the emotional "feeling brain" and the rational “thinking brain.” 

The way that we think and feel are intertwined. This is because the _thinking brain_ — where we develop our rational thoughts — and the _feeling brain_ — the birthplace of our emotions — are linked. They are connected by strong _neuronal pathways_.

Our emotional intelligence is dependent on these connectors between _the_ thinking and feeling brains, and any damage to these neuronal pathways can elicit emotional intelligence deficits.

For example, a person whose emotional brain is severed from their thinking brain will stop experiencing feelings. Their deficiencies in this area will include a loss of _emotional_ _self-awareness_, which is an important component of emotional intelligence. Evidence of this can be seen in lobotomized patients. After the connections between their two brains were severed, they lost their emotional capacity.

Another example of the importance of the connections between our two brains is the thinking brain's role in correcting the workings of the feeling brain — a process essential for _emotional_ _self-regulation_.

Emotional self-regulation works in the following way: stimuli, such as a sudden loud bang, will often send your emotional brain into overdrive. The feeling brain will automatically perceive the stimulus as a threat, and so it will respond by putting your body into a state of alert.

We use our thinking brains to help regulate this process. After we hear the loud bang, and while our emotional brain is sending alarm bells ringing around the body, our thinking brain is checking the stimulus to see what threat is there. If it sees no danger, then it calms down both the feeling brain and the body, allowing you to think clearly again. This is why we aren't constantly overacting to every sudden noise we hear.

If you break the link between the thinking and feeling brains, this process isn't possible. For example, patients with severe damage to the thinking brain have difficulty regulating their feelings.

### 7. Emotional intelligence makes you healthier and more successful. 

What is the key to leading a successful and fulfilled life?

You may think it's high IQ — that the brightest people will stand the best chance of leading happy lives. Yet emotional intelligence is just as important as high IQ in achieving this.

Evidence suggests that people with high levels of emotional intelligence are more likely to be successful.

For example, studies show students with high levels of empathy get significantly better marks than less empathic pupils with comparable IQs.

Students who can control their impulses will also be more successful than their peers. One study conducted by Stanford University called "The Marshmallow Challenge" tested the ability of a group of four-year-olds to resist eating a treat. Years later, it turned out that those who had been able to control their impulses at age four proved superior academically as well as socially throughout their youth.

This success continues into the adult world. Managers who are more socially adept also tend to be more persuasive.

What's more, emotional intelligence can also help us lead healthier lifestyles.

This can be seen by looking at stress. When we go through stressful periods, our hearts are put under immense strain because our blood pressure is raised. This leaves us at risk of cardiac arrest.

Stress can also weaken your immune system, as demonstrated by a study that suggested that people under stress are significantly more likely to catch a cold.

However, emotional intelligence can help us avoid the dangers of stress. This is because if you learn to mitigate stressful feelings like anxiety and anger, you'll reduce those feelings' harmful effects on your health. For example, in a clinical study, people who had already suffered one heart attack were coached to manage their anger, which significantly lowered their risk of further attacks.

Given the great impact of emotional intelligence on success and health, there is remarkably little emphasis on emotional skills in the typical school curriculum.

### 8. The future of American society will depend on its children’s emotional intelligence. 

While high emotional intelligence makes people happy and healthy, poor emotional intelligence can have society-wide negative effects. For example, the tripling of the US teen murder rate between 1965 and 1990 can be linked to diminishing emotional intelligence.

There is strong evidence that suggests that deficits in emotional intelligence can cause delinquency — a major factor in increasing crime rates.

For example, studies show that violent teen delinquents find it difficult to control their impulses and also find it hard to read other people's facial expressions — deficiencies which are also found in adult sex offenders. Drug addicts also display emotional intelligence problems. For example, heroin addicts have difficulty regulating anger even prior to their addiction.

There's little doubt that a child's welfare is determined by emotional competence. Children who grow up in an environment surrounded by emotionally intelligent people will also display high levels of it. This was confirmed by a study that showed that children of emotionally intelligent parents are better at regulating their own emotions, show lower stress levels, are better liked by their peers and described as more socially adept by their teachers.

It's perhaps not surprising that a child's own emotional intelligence is also related to its welfare. Children with deficits in self-awareness, empathy or impulse control are at risk of developing mental health problems and tend to have more problems at school.

All this evidence shows that the emotional intelligence of children is central to the future of our communities. The children of today are the parents, managers and politicians of tomorrow. In short, many will have a big impact on future society, and it would help any community if the people in charge were empathic, good at resolving conflicts and not prone to blindly acting on impulse.

Several very diverse societal factors shape a future community's well-being, but obviously emotional intelligence is among the more influential ones.

### 9. There are several ways to boost your emotional intelligence. 

As we have learnt that emotional intelligence can lead to a fulfilled life, you may now be asking if it is possible to boost it.

The answer is yes, and a series of exercises can help you achieve this.

If you want to enhance your self-awareness and self-management, you can practice using inner dialogues. This will assist you in identifying and naming your feelings.

For example, if your friend tells everyone but you about his marital problems you may feel upset. But inner dialogue can help you resolve this. You should ask yourself, "Why am I hurt? Because my best friend confided his marital problems to everyone but me."

Now, having identified this feeling and its cause, you can dampen its power. You could instead tell yourself, "I may feel left out but it's possible that he didn't want to bother me because he knew I was busy drawing up the annual accounts." In this way, you'll feel less upset.

If you want to improve your empathy, you can try to mirror another person's body language. This is helpful because body language doesn't solely express emotions — it also evokes them. So, for example, by mirroring another person's tense posture, you may induce tenseness in yourself.

If you want to enhance your ability to self-motivate and think more positively, follow this advice:

The way you explain your successes and failures has an impact on your ability to motivate yourself. To become self-motivated, start thinking like this: people who can convince themselves that failures are due to something they can change don't give up so easily. They continue to try because they believe that a successful outcome depends on their own actions.

In contrast, those who attribute a setback to some permanent personal deficit are likely to give up soon. They are convinced that there is not much they can do about their success anyway. If you want to be successful, try to avoid this thought process.

### 10. You can use emotional intelligence in all kinds of settings, from the office to your love life. 

You may now know quite a bit about emotional intelligence. But you may also be asking yourself — how can I use this knowledge practically? 

Here is some advice that can help you use your emotional intelligence in your everyday life.

The first piece of advice is: you can avoid misunderstandings in a relationship if you take into account the different ways that men and women deal with emotions. Typically, girls are raised to talk about their feelings and connect through intimate talk, while boys learn to minimize feelings that might make them appear vulnerable.

For example, if a female partner complains about a problem, the male reaction might be to instantly offer advice. But this could be wrong; often when a woman complains about a problem she is seeking validation. She wants her partner to listen and show that he understands. So an immediate solution might be misinterpreted as a rejection of her anguish rather than an attempt to help. It would be better to listen to her carefully.

You could also follow this advice. If you are very upset during a dispute, try to take a break to calm down. Strong emotions tend to distort your thinking so you will likely say or do something you might regret. Fortunately, a cool-down period may help.

Some marriage counselors even advise couples to monitor their pulses during a quarrel. A pulse rate that overshoots the person's average rate by more than 10 bpm indicates that the person is getting too emotional to think rationally and needs a rest period.

The final piece of advice is: if you have to criticize someone, be specific and offer a solution. By picking an incident and pointing out exactly what should have been done differently and what was done well, you'll make yourself clear and keep the recipient from feeling devalued and confused.

### 11. Final summary 

The key message in this book:

**Our emotions are important as they serve as indispensable sources of orientation and push us to take action. However, they can also make us act irrationally. That is why we need emotional intelligence. Emotional Intelligence is a set of skills that enable you to recognize and manage emotions — both your own feelings and those of other people.**

**Emotional intelligence is perhaps the most important factor in helping us lead fulfilled lives. Compared to IQ, it is a better predictor of academic success, job performance and life success. And, unlike IQ, we can increase our emotional intelligence throughout life.**

Actionable advice:

Emotional intelligence is of paramount importance for a child's welfare. Thus, if you are a parent or a teacher, you might want to help children develop their emotional skills by using one of the following simple measures:

**Encourage them to talk about their feelings.** This will enhance their emotional self-awareness. If they experience difficulties with this task, provide them with the necessary vocabulary to label their emotions.

**If they succeed in something, praise them for their efforts and for showing self-control.** This will teach them to trust in their ability to improve their lives through their own actions.

**Be specific and constructive.** If you have to criticize someone, pick a specific incident and point out exactly what should have been done differently and what was done well. That way you'll make yourself clear without offending the recipient.

**When you are very upset during a dispute, try to take a break to calm down.** This may help you to circumvent a hurtful overreaction that could otherwise be triggered by rage.
---

### Daniel Goleman

Daniel Goleman is a psychologist and an accomplished writer. During his long career he has received numerous awards including a Lifetime Achievement Award from the American Psychological Association and has been nominated for the Pulitzer Prize twice.

His numerous publications cover a vast array of subjects ranging from meditation to ecology. The main focus of his writing lies on the intertwinings and interrelations of emotion, performance and leadership.

