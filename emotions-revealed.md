---
id: 57ada49299b53c0003855477
slug: emotions-revealed-en
published_date: 2016-08-17T00:00:00.000+00:00
author: Paul Ekman
title: Emotions Revealed
subtitle: Recognizing Faces and Feelings to Improve Communication and Emotional Life
main_color: E06568
text_color: 944345
---

# Emotions Revealed

_Recognizing Faces and Feelings to Improve Communication and Emotional Life_

**Paul Ekman**

_Emotions Revealed_ (2003) puts emotions under the microscope, revealing where they come from and how to recognize them, whether they're yours or someone else's. If you've ever wanted to know if someone was being dishonest or trying to deceive you with a friendly smile, these are the blinks for you!

---
### 1. What’s in it for me? Enable your emotion detector. 

We often hear that body language says as much as, if not more than, the words we speak. Given that we so often non-verbally communicate what we can't, or don't want, to say out loud, it makes sense to gain fluency in this other, unspoken language.

This is a language every human was once fluent in. Before the development of verbal language, our bodies and faces were the only means of human communication. And, of course, it was the face that had to signal our most nuanced emotions. Deciphering facial expressions is something of a lost art — but we still know quite a bit about how emotions manifest as facial expressions. That's what these blinks are all about.

You'll also find out

  * that the human eyebrow has an emotional range;

  * why you should look at the jaw if you want to avoid a fight; and

  * how to distinguish the two feelings easily detected in the eyes.

### 2. Our emotions are the product of evolution and are still attuned to the concerns of our ancestors. 

If you've ever been embarrassed after crying your eyes out during a Pixar movie, you may have wished you could make your emotions less visible. But outward displays of emotion like these are actually very useful evolutionary tools.

In fact, these sometimes uncontrollable emotional reactions are innate, and they're still triggered by the same things that our ancestors had to deal with.

For instance, animals were a main threat to our ancestors, and some of these predators have been around so long that they've become imprinted on our brain, always eliciting a fearful reaction.

Swedish psychologist Arne Ohman tested this theory in 1993. He wanted to determine whether people today are more sensitive to things like spiders and snakes, which would have been a threat to our ancestors as well.

So, Ohman showed his subjects an image of a spider accompanied by an electric shock. He then repeated this procedure with an image of a pretty flower, also accompanied by a shock.

A single electric shock coupled with the picture of the spider was sufficient for people to react with fear when shown the image of the spider alone. In contrast, it took many more shocks to condition people to fear the image of the flower.

Okay, but what about a more modern threat, like guns?

Ohman repeated the experiment, this time pairing a shock with an image of a modern gun and then with an image of a spider.

Again, the ancient fear proved more powerful: test subjects took just as long to react fearfully to the gun as they did to react to the flower, while the spider remained innately frightening.

This is strong evidence, suggesting that our emotions, especially fear-based ones, are hand-me-downs from our ancestors and their experiences.

> _"My will and reason were powerless against the imagination of a danger which had never been experienced."_ \- Charles Darwin

### 3. Childhood trauma can lead to volatile emotions, especially when it repeats ancestral patterns. 

Sometimes our emotions can defy logic. If you have a fear of heights, then you probably know that, when standing atop a tall building, even if the roof is skirted by a high railing and a safety net, you'll always be afraid of falling.

Irrational emotions like these can be extremely powerful, especially if they're the result of a childhood trauma.

Just consider Tim, one of the author's clients. As a young boy, Tim's father had a habit of teasing him in a way that, to some, may seem harmless enough; to Tim, however, it came across as cruel and insensitive.

For example, while learning to ride a bicycle, Tim, like most children, would occasionally fall off. When this happened, his father would laugh at him and tell other people how Tim barely managed to get on the bike before falling off again.

This trauma stuck with Tim and made him extremely sensitive to any form of teasing. So, if Tim gets playfully teased for losing a board game, he tends to irrationally lash out in anger.

If we take this a step further and trace more general human trauma back to our ancestors, we'll find that our emotional reactions can be even more volatile.

Such primal reactions occur in unexpected places. For instance, a dean at the author's university explained how he could remain calm, cool and collected when dealing with faculty members who reject proposals that he put a lot of effort into.

When he gets stuck in traffic, however, it's a different story. The dean gets extreme, irrational road rage. Believe it or not, though, this is a reaction that goes back to our ancestors.

Of course, our ancestors didn't have cars — but territory was very important to them, as was being able to move. Being held back by an opponent was a common and upsetting experience that could have deadly consequences.

That's why a traffic jam can trigger an ancient emotional pattern and cause those familiar irrational outbursts.

### 4. Sadness is revealed though raised inner eyebrows, contracted cheeks and widened lips. 

Now that you have a better idea of where your own emotions come from, how do you recognize the emotions of others?

Let's examine one particular photograph to see what we can determine.

In 1971, the press published a photograph of Bettye Shirley, who had just lost her son to a pedophile ring in Texas. The expression on her face is clearly one of sadness, but what exactly makes that so obvious?

One of the clearest signs is the raising of the inner points of the eyebrows, just above the nose. This is one of the most reliable signs, since it's an involuntary reaction.

Furthermore, even if someone is trying to hide their sadness, you will often be able to pick out this telltale sign.

And you can tell that someone's sadness is overwhelming when a vertical crease appears between the raised inner eyebrows, making the expression even more dramatic.

In the photograph, the intensity of Bettye Shirley's sadness can be read on her forehead alone; you can see the upturned eyebrows and the vertical crease.

But other parts of her face also betray her sadness.

In the same photograph, Bettye's lips are horizontally widened out, forming a sad grimace.

Such a grimace often widens until the mouth is open, and the sadness is then further expressed through crying and wailing. While this is likely the purpose of the initial widening of the lips, it's more common nowadays for people to try to contain the emotion and stifle the urge to wail.

Another sign of sadness is Bettye's contracted cheeks — the cheeks are slightly raised and are more prominent on her face. This contraction can also raise the corners of the lips, which is why people gripped by sadness often look as if they're smiling.

To accurately identify this sad smile, pay attention to the cheeks: If the lips are being pulled up by contracted cheeks, the person is expressing sadness, not happiness.

> _"Some experiences are so devastating that the sadness may never completely fade away."_

### 5. Anger is displayed by lowered brows, glaring eyes and thin lips, though signals can vary culturally. 

To examine the signs of anger, let's turn to a different subject.

In 1997, a picture was taken of Maxine Kenny while she was at the trial of the man convicted of raping and murdering her 38-year-old daughter. Maxine, restrained by police officers, shouted insults and tried to attack the man who took her daughter away from her. Her face was the very picture of anger.

Let's start again at the forehead: Maxine's eyebrows are lowered and drawn together in a scowl. Beneath this scowl, her eyes glare at the man who murdered her daughter. Her jaw is clenched and her lips are thin and drawn back, exposing clenched teeth.

Of all these signs of anger, the most reliable is the thinning of the lips. This is another automatic occurrence that can't be controlled voluntarily, making it useful when trying to detect whether someone is hiding their anger. It's one of the first signs that anger is brewing inside someone and can even appear before the person is consciously aware of it.

That being said, not everyone with narrow lips is angry; the true sign is when someone's lips become thinner than they normally are.

You should also be aware that not all signs of anger are universal. They vary from culture to culture.

Take the indigenous people in Papua New Guinea, for example. When expressing uncontrollable anger and potential violence, they close their mouths and tighten their lips.

This is the opposite of what we see in the Western world, where people usually open their mouths and shout to express extreme anger.

On the other hand, when angry Papua New Guineans show an open mouth, this means their anger is controlled and they are ready to talk rather than resort to physical violence.

Once again, this is the opposite of Westerners, who show controlled anger by closing their mouths and bottling up the insults they'd otherwise let loose.

> _"Charles Darwin, more than a century ago, noted that we press our lips tightly together whenever we engage in any strong physical exertion."_

### 6. The eyes are where to look for fear and surprise; subtle clues will help you tell the difference. 

Imagine living in a high-rise apartment building and sitting peacefully on your balcony, when all of a sudden a body falls past you on its way down to the ground. Chances are, your emotional reaction would be an expression of either fear or surprise.

But how would an observer tell the difference?

This can be tricky, since signs such as raised eyelids are ambiguous and could indicate both fear and surprise. 

So, let's look at another photograph that actually captures the incident described above. In 1978, a woman doing a promotional stunt on a New York high-rise lost her footing and fell to the ground below, severely injuring herself.

Amazingly, photographer Lou Liotta captured the woman as she was falling past two men who were standing on their balcony.

Also captured was the expression on the men's faces, and, in this case, the most prominent features are their raised eyelids. Given the situation and the look in their eyes, the men are most likely expressing surprise; if they were expressing fear, their eyelids would be raised even higher.

The difference between a look of fear and a look of surprise is very slight and varies from person to person, but there are specific signs to look out for.

When you see tense lower eyelids that make the eyes appear big and frozen, this is a sure sign of fear. 

We see this in a 1973 photograph that captured roller-derby skater Charlie O'Connell in mid-air, just before he's about to fall. O'Connell is clearly terrified. His lower eyelids are tense and his upper eyelids are raised extremely high; his eyes seem to bulge out in cartoonish fashion.

However, people can work hard at hiding their emotions, especially when it comes to fear. But tension in the lower eyelids is something we can't control, so be on the lookout for this feature. It will always betray someone's true emotions.

> _"In a moment surprise passes as we figure out what is happening, and then surprise merges into fear."_

### 7. Someone’s smile and voice can indicate different kinds of happiness, but not all smiles are genuine. 

If you've ever seen the faces of friends and family welcoming back a loved one from a tour of duty in the military, then chances are you've seen the beaming smiles that accompany expressions of true joy.

While someone's smile is the most recognizable and universal sign of happy emotions, the voice also plays an important part. 

After all, a smile can signify many variations of enjoyment — from amusement, relaxation and sensual pleasure, to relief, exhilaration and gratitude. So, in order to better identify the true emotion that is being felt, we must turn to the voice.

English psychologists Sophie Scott and Andrew Calder discovered that by paying attention to the voice, people found it easy to distinguish between the various shades of enjoyment mentioned above.

And this is key, because the smile is a complex expression, and not one that always signifies enjoyment. 

For example, many people will use a smile simply out of politeness, without feeling any enjoyable emotion.

But this doesn't mean it is impossible to read someone's smile. For instance, nineteenth-century French neurologist Duchenne de Boulogne developed a curious method of electrically activating various facial muscles. He experimented on willing patients and observed some interesting results.

Duchenne found that when he activated the _zygomatic major muscle_ — the muscle that makes our lips form a smile — the patient didn't really look happy. But when his patients responded to a joke, he discovered that, in addition to smiling, the orbicularis oculi — the muscle around the eye — was also activated.

And this is the real clue to a genuine smile: the circular muscle around the eye can't be voluntarily contracted, so pay special attention to it if you want to be sure that someone's smile is the real deal.

### 8. Lies are difficult to detect, but micro-expressions can lead the trained eye to the truth. 

Distinguishing between the truth and a lie can sometimes be a matter of life and death. Imagine being a psychiatrist who has to figure out if a suicidal patient is telling the truth when he claims to be feeling much better.

In many instances, determining what is the truth and what is a lie is quite difficult. 

The author actually filmed doctors while they interviewed suicidal patients who wanted a weekend pass to leave the clinic.

In one of these interviews, a patient named Mary said she was feeling better and was granted a weekend pass. But, before she left, Mary confessed that she had lied and was indeed planning on going home and taking her own life.

Without Mary's confession, the doctors would have lost a patient due to their inability to determine whether or not she was telling the truth. Was there any way this could have been avoided?

While there are micro-expressions that can indicate when people are lying, these tiny movements are almost imperceptible.

Consider Mary's interview. Micro-expressions can escape the human eye under normal circumstances, but the author was able to spot them by going through the recorded interview frame by frame.

The author actually spent more than 100 hours examining Mary's 12-minute interview, and eventually he focused in on a moment when the doctor asked Mary about her future plans. Immediately following the question, an anguished expression flashed over Mary's face for just 1/12 of a second before being covered up by a smile.

This troubling micro-expression appeared three times throughout the film, revealing that Mary was hiding her true intentions. 

So, while these micro-expressions often escape the naked eye, screening filmed interviews in order to detect them could be a way to avoid future potential tragedies. Emotional detectives sometimes need to take advantage of the tools at their disposal.

### 9. Final summary 

The key message in this book:

**Being able to read facial expressions and detect the emotions people are feeling is a highly useful skill. Not only can this lead to detecting lies and recognizing when a smile is disingenuous, but, more importantly, recognizing the common emotions we all share can lead to more empathy.**

Actionable advice:

**Feel free to express your emotions.**

If you've repressed your sadness or anger in the past out of fear of appearing vulnerable, try to reconnect with those feelings in order to have a healthier relationship with your emotions. To help you do this, stand in front of a mirror and recreate the facial expressions that were described in these blinks. You should be able to recognize when you get the expressions right, which will help you connect to your emotions and overcome the unhealthy habit of bottling them up.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Spy the Lie_** **by Philip Houston, Michael Floyd, Susan Carnicero & Don Tennant**

_Spy the Lie_ reveals the typical strategies that liars use to try to deceive you, as well as the tools to help you detect them. This book draws on field-tested methods for lie detection developed by former CIA officers, which helps to spot the signs of a lie and ask the right questions to uncover the truth.
---

### Paul Ekman

Paul Ekman is a consulting psychology expert for police departments and security offices. He's also helped Pixar Studios bring animated characters to life by availing them of his expertise on emotions and how they're revealed through facial expression.

