---
id: 59bfab7ab238e10005c1fbe1
slug: empire-of-cotton-en
published_date: 2017-09-22T00:00:00.000+00:00
author: Sven Beckert
title: Empire of Cotton
subtitle: A Global History
main_color: B89835
text_color: 6B581F
---

# Empire of Cotton

_A Global History_

**Sven Beckert**

_Empire of Cotton_ (2014) chronicles the long and complex history of that fluffy plant — cotton. These blinks detail how the cotton industry connected the world from Manchester, England, to rural India, while describing the incredible impact that cotton production has had on the development of economic systems.

---
### 1. What’s in it for me? Learn the fascinating story of cotton. 

Whether a T-shirt or denim jeans, it's likely you're wearing some article of clothing that's made out of cotton. It's a basic fabric today, one that seems humbler than, say, silk or satin. But you might be surprised to learn how complexly interwoven cotton is with the development of the modern world — from Columbus's discovery of the New World to industrialization to the American Civil War and beyond.

Before Wall Street, there was cotton — a global mover and shaker, an economic powerhouse. These blinks give the whole story.

In these blinks, you'll learn

  * about the broader connection between slavery and cotton;

  * cotton's role in the divide between the developed and developing world; and

  * who the big players of cotton are today.

### 2. Europeans discovered cotton later than others, but when they did, they made it a global phenomenon. 

Today, cotton is everywhere you look. Chances are, you're wearing some at this very moment. With such a pervasive presence in society, it's little wonder that cotton also has a long and captivating history.

Cotton has been used by humans for thousands of years, beginning with peoples in Africa, Asia and America, who independently discovered cotton and its perfect suitability for cloth making.

By the early 1500s, people in twelve small villages on the Pacific coast of modern-day Mexico were using the plant to pay tribute to their Aztec overlords. In the same period, in Gujrat, India, and on the west coast of Africa, people were growing the plant. These growers would harvest the white fluff and make cloth by hand at home, sometimes transporting it to local markets to be sold. In fifteenth-century China, cotton cloth was even used to pay taxes!

Back then, cotton weavers were still growing and processing the plant independently. In other words, the cotton growers of Africa had no contact with those in Mexico. However, that all changed when Europeans entered the scene, elevating cotton production to a global scale.

Before this shift began, around the year 1500, Europeans wore wool and linen, totally unaware that cotton existed. As they began exploring the world and claiming new territory — as in the expeditions of Christopher Columbus — they were introduced to the wonders of cotton, which feels much better against the skin than wool or linen and is also easier to wash.

Between 1600 and 1800, European settlers, driven by their love for this new cloth, launched a violent but profitable _cotton triangle_ that bridged three continents.

Armed merchants would go to India to buy cotton. They would then trade it with African leaders for slaves and transport these slaves to the Americas where they would work in cotton fields on newly stolen lands.

In this way, the global cotton empire was born. But in the years that followed, it would grow and transform in innumerable ways.

> _By the 1790s, Europeans were exporting around 80 million yards of cloth per year from India, almost three times more than the 30 million yards they exported in 1727._

### 3. Mechanization split the world in two. 

In the year 1784, Samuel Greg made history when he opened the first mechanized cotton mill outside of Manchester, England. The mill, which sat on the banks of the river Bollin, was powered by the flow of the river's waters.

From that point, mechanization only improved and spread, and the world of cotton was transformed. In fact, mechanization had a major economic impact for British manufacturers as it enabled them to undercut the weavers of India.

Mechanization meant booming productivity and plummeting costs. This put Indian manufacturers at a serious disadvantage. Eighteenth-century Indian spinners, the world leaders of cotton production in their day, required 50,000 hours to spin 100 pounds of cotton. By 1825, British workers operating machinery needed just 135 hours to process the same amount.

Because of this incredible change, between 1795 and 1811, British cotton prices dropped by up to 50 percent, enabling Britain to emerge as the world's leader in cotton exports. In 20 years, the annual value of British cotton goods exports increased by a factor of sixteen, hitting £5,854,057 in 1800.

Many other countries also caught on, making the most of mechanization. France, Germany and Belgium each took active steps to protect their domestic cotton producers from foreign competition, especially from Britain.

From 1806 to 1814, the French blocked all British cotton goods, effectively kick-starting the continental European cotton market. Due to this market insulation, the cotton industry, along with other mechanized fields, became stronger on the continent, enabling these countries to industrialize before others.

Protecting the developing industry required a strong state, with a powerful police force and military to guard the border; countries also needed a stellar legal system, in case of disputes. Naturally, some countries, such as India, which had been weakened by British colonization, lacked such state development.

State strength made all the difference. The countries that had industrialized early experienced burgeoning cotton industries, while those that hadn't, like India and Brazil, fell behind, forming the divide that can be seen to this day between developed and developing nations.

### 4. At the turn of the nineteenth century, the Southern United States emerged as the world's leading exporter of cotton. 

So manufacturing changed the world and, as it did, the demand for raw cotton steadily grew. By 1790, Britain was spinning six times more cotton than it had been in 1781. But where did all this cotton come from?

Well, a huge amount originated in the European colonies, but, eventually, even they couldn't produce enough to meet demand. Here's what happened:

In the late eighteenth century, the vast majority of raw cotton imported to Britain was from expropriated lands in the Americas, worked by slave labor. For instance, Barbados, then a part of the British West Indies, exported 2.6 million pounds of cotton in 1789 alone.

But colonies like this were soon faced with problems. After all, an island only has so much available land. Further complicating matters, many colonies faced slave rebellions. St. Dominique is a good example. In 1791, the slaves there overthrew their French colonizers, creating what is now modern-day Haiti.

It was in this climate that the Southern United States emerged as the perfect solution for cotton importers. The South had lots of advantages for cotton growing, a perfect climate among them. By killing or evicting the indigenous people who lived there, the United States easily quelled any social resistance while gaining access to pristine agricultural land.

Meanwhile, a steady stream of slaves offered a source of low-cost labor. Between 1783 and 1808, 170,000 slaves were brought from Africa to Southern US plantations and, by 1830, one million people — one out of every thirteen Americans — were growing cotton.

To maintain this system, plantation owners imposed brutal conditions; as a matter of course, slaves were beaten and tortured. This bloody program catapulted the Southern United States into its position as the world leader in cotton production.

> In 1831, an estimated 45 percent of the world's raw cotton came from the Southern United States.

### 5. Wage labor fuelled the industrial age in the United Kingdom. 

Histories of the industrial revolution tend to focus on inventors like James Watt, whose steam engine transformed the face of the earth. Without a doubt, bright minds and groundbreaking inventions made progress possible. However, the real fuel of industrialization was the millions of workers whose stories didn't make it into the textbooks.

In the United Kingdom, the first industrialized nation, working conditions were atrocious. Ellen Hooton, a woman who, as a child, worked in a cotton factory in Wigan, England, is one of the few workers whose story has reached us. In 1833, she testified during a Manchester Commission investigation into child factory labor.

She began working at the age of seven. Her day started at 5:30 a.m. and ended at 8:00 p.m., and if she ever made a mistake or arrived late to work, she would be beaten or publicly shamed.

During a productive, week she would earn two shillings and seven and a half pennies, the equivalent of around $9.80 in 2005 dollars.

Her story is emblematic of general working conditions at the time. Millions of workers came to work in factories every single day, following a new rhythm of work — not the rhythm of the sun or the seasons like farmers, but the relentless beat of the machine.

Why were these workers willing to toil under such brutal conditions?

Well, most had no other option. After all, homemade cotton goods could never compete with the cheap cloth made by manufacturers. Factory production put household weavers out of business, forcing them to find work in factories.

Since these home-based weavers were largely women, women also predominated in the factories. For example, during the mid-1820s, 89 percent of the workers at England's New Hampshire-based Dover Mill were women. However, factories also recruited other vulnerable groups, especially children from orphanages and poor families desperate for income.

### 6. Networking middlemen became indispensable to a modernizing cotton industry. 

So industrialization was a major boon to cotton production and by the mid-nineteenth century, millions of people were working in the newly built factories that produced cotton goods. But that's not the only way the cotton industry was evolving.

Merchants were also transforming the way business was done. This emergent merchant class of middlemen was increasingly necessary in the early nineteenth century, as the cotton industry became ever more global.

More and more, merchants were coordinating the work of growers, slaves, manufacturers and investors across the world, from farmers in Gujarat, India, to spinners in the English town of Oldham, to the factories of Manchester and the bazaars of Istanbul. By facilitating this interconnection, they laid the foundation for the globalized, modern capitalist world we live in today.

At the same time, manufacturers were buying more and more cotton from brokers rather than dealers. The difference between the two was that the former, unlike the latter, didn't actually own cotton. Rather, they oversaw deals between manufacturers and cotton importers.

This development made networking essential to business success in the industry. In the rocky world of cotton trading, you had to be able to trust your broker and your merchant. It was a risky business; a single undelivered order or bad deal could mean bankruptcy.

The most successful players in this high-risk game were those who built trusting networks, often around family, religion or geography. Just take Nathan Mayer Rothschild, of the famous Rothschild family, who, in 1799, went to Manchester to enter the cotton game.

The majority of what he sold was bought by fellow Jews from his home city of Frankfurt. Rothschild knew these people would buy from him so, when he arrived in Manchester, he could buy in bulk. Since the people he did business with trusted him, they even gave him credit to invest in Manchester-based factories.

In no time at all, he tripled his initial investment, kick-starting his investment career.

### 7. The American Civil War transformed the global cotton industry. 

Around the same time that the cotton industry was kicking into high gear, two groups of US capitalists were also coming into heated conflict. A new class of American industrial capitalists — merchants, to be exact — were employing wage labor and wanted to expand their operations to cotton-plantation rich lands around the world.

But the old Southern planter elite wasn't about to part with its slaves without a fight. They wanted to maintain slavery and even extend it west. Both sides worked hard to sway the government and, soon enough, the mounting domestic tensions resulted in a civil war that shook the global cotton industry to its foundations.

This was the American Civil War, of course, whose first shots were fired at Fort Sumter, in April of 1861. Before the South's eventual surrender, the US cotton-export industry would be absolutely decimated; by 1862, British cotton imports from the United States were a whopping 96-percent lower than the previous year.

It was in this chaos that European manufacturers rushed to secure new sources, like those in India, where British colonial officials had recently constructed fresh infrastructure. In fact, during the first year of the Civil War, British spending in India on infrastructure, mainly railroads, nearly doubled.

Britain also reduced its import taxes from 10 percent to 5 percent for Indian cotton products, thereby boosting the Indian market. Naturally, India began exporting a lot more raw cotton as a result and, by 1862, 75 percent of British cotton and 70 percent of French cotton was of Indian origin.

But, however calamitous the Civil War might have been, it wasn't the end of US cotton. In 1865, the war came to a close. The end of American slavery deprived the Southern cotton powerhouse of its labor. However, the cotton growers soon found a new workforce.

### 8. New economic systems emerged around cotton after the end of slavery. 

As slavery ended, the American cotton workforce shrank dramatically. But the _demand_ for cotton was rising as fast as ever. In fact, between 1860 and 1890, global consumption of cotton doubled. Who was going to toil in the fields to meet this demand?

Well, it wouldn't be long before a new work relationship replaced slavery. Here's what happened:

Once released from the bondage of slavery, many recent slaves demanded land so they could grow crops for their own subsistence and live independently. However, most land in the American South belonged to plantation owners. Even land that was confiscated during the Civil War had been returned to its prior owners by 1865.

So a new system — called _sharecropping_ — was born. It was essentially a compromise. Freedpeople would work on land owned by former plantation masters and, in return, receive a share of the crops they grew.

Meanwhile, in other regions of the globe, property rights were being introduced, changing the nature of agriculture in the process. One such place was India. Traditionally, Indian farmland was owned communally, with those who worked on the land being entitled to a share of the communal harvest.

But that all changed in the second half of the nineteenth century when the British began enforcing property rights. All of a sudden, every farmer owned a smaller plot individually and could keep all the crops he grew.

The problem was that individuals didn't have enough money to buy the seeds and farm tools they needed. As a result, they were forced to take out loans from _sowkars_, or moneylenders, basically mortgaging their newly acquired land. For these loans, they paid absurd interest rates of up to 30 percent, which forced them to grow more and more cotton to pay off their debt.

Of course, new systems of this type greatly benefited cotton-buying manufacturers. By 1894, the price of cotton for a US buyer was seven cents, even lower than the pre-Civil War cost of eleven cents.

### 9. Colonial powers massively expanded cotton growing in the early twentieth century. 

Around the turn of the twentieth century, new world powers — namely, Russia and Japan — had begun entering the cotton game. Both of these nations grew cotton in their colonies and, for Japan in particular, cotton demand soared in the first years of the twentieth century.

For instance, in 1893, the country imported just 125 million pounds of raw cotton. By 1902 that number had increased to 446 million, the majority of it coming from India and the United States. This trend continued and, by 1920, Japan was importing over 1 billion pounds of cotton a year.

That being said, the Japanese were wary of depending on imports from other countries. They sought to secure "raw material independence" for themselves, working to get their cotton from their own territories. To do so, they began farming cotton in their own colonies.

Korea was one such colony. Between 1916 and 1920, 165 million pounds of raw cotton were grown in Korea, compared to just 30 million pounds between 1904 and 1908.

Simultaneously, Russia exploited its colonies in Central Asia, like Turkestan, and the United States stole new land from indigenous Americans for cotton production. Because of these colonial cotton expansions, between 1860 and 1920, at least 55 million acres of new land became cotton fields in Africa, Asia and the Americas.

But in order to make such expansions, colonial powers first had to undermine the pre-existing industries in these places. This process, known as _deindustrialization_, involved the imposition of tariffs and duties on colonies, which effectively squashed the local production of cotton goods. The colonizers then built infrastructure, like massive railroads, that were geared toward global cotton growers rather than home weavers.

In India alone, between two and six million people employed in traditional cotton spinning or weaving lost their jobs between 1830 and 1860. The results were catastrophic. Locals soon came to depend on the global price of cotton, putting them at the mercy of world market fluctuations. For example, in the 1890s, global cotton prices tanked while Indian food prices steadily increased, resulting in 19 million people dying from famine.

### 10. China and India emerged in the early twentieth century as the leading cotton producers and remain so today. 

The twentieth century was a time of sweeping changes and the cotton industry was no exception. In large part, that's because during this time conditions became favorable for newly industrializing nations.

After all, old industrial countries like Britain and Germany had powerful labor unions that increased the rights and wages of workers. For instance, in Germany, spinners took home 53 percent more in 1913 than they had in 1865.

Meanwhile, in countries new to the industrial map, like China and India, wages were absurdly low. Naturally, a new class of capitalist merchants emerged to take advantage of this fact and they were bolstered in their efforts by government support.

For example, in China in 1910, a spinner earned just 6.1 percent of what his American counterpart did. To maintain this difference, the state protected and assisted mill owners. Just consider the 1920s, when the government stood by as thousands of left-wing labor leaders were assassinated in a coordinated effort by mill owners in Shanghai.

It's no wonder that these industrializing countries soon dominated the cotton industry. Though China didn't adopt mechanized cotton manufacturing until late in the game, Chinese production was booming by World War I.

The timing was perfect for China. The Great War knocked the Western manufacturers out of the game, effectively insulating Chinese industry from competition. As a result, between 1913 and 1931, the total number of spindles in China rose by 297 percent, the fastest growth worldwide.

At the same time, Britain, once the world's leading manufacturer of cotton, was experiencing the opposite trend. Between 1919 and 1939, 43 percent of all British cotton looms were put out of commission, largely because of rising wages.

This history is still visible today. China remains the world's largest producer in cotton, making up 29 percent of global production. India ranks second, with 21 percent, and the United States is still a major player, with 14 percent. But in cotton, as you now know, nothing is fixed. Who can say how the global playing field will shift in the coming decades.

### 11. Final summary 

The key message in this book:

**Who would think that a simple plant like cotton could shape the course of history? Well, that's precisely what it did; for centuries, cotton production has transformed global economic relations. It's launched wars and, arguably, it laid the foundations of global capitalism.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _White Trash_** **by Nancy Isenberg**

_White Trash_ (2016) retells American history from the perspective of the poor whites who were by turns despised and admired by the upper classes. These blinks trace the biopolitical, cultural and social ideas that have shaped the lives of white trash Americans from early colonial days to the Civil War, through the Great Depression and up to the present day.
---

### Sven Beckert

Sven Beckert holds a PhD in History from Columbia University and is now Laird Bell Professor of American History at Harvard University. _Empire of Cotton: A Global History_ won the 2015 Bancroft Prize and ranked as a finalist for the 2015 Pulitzer Prize for History.

