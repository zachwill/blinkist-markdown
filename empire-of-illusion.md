---
id: 5677bc18fba8510007000009
slug: empire-of-illusion-en
published_date: 2015-12-21T00:00:00.000+00:00
author: Chris Hedges
title: Empire of Illusion
subtitle: The End of Literacy and the Triumph of Spectacle
main_color: 936A4A
text_color: 614631
---

# Empire of Illusion

_The End of Literacy and the Triumph of Spectacle_

**Chris Hedges**

_Empire of Illusion_ (2010) offers a close examination of declining literacy levels in the United States, and the disastrous effects that this educational catastrophe is having on the country. These blinks will explain how TV is pacifying the US citizenry, how corporate power has taken over the country and what this means for the future.

---
### 1. What’s in it for me? Overcome your societal delusions. 

Was it ever a dream of yours to become the next reality TV superstar? Maybe you've even applied to be a contestant on MTV's _Real World_. Well, if so, you are clearly not alone. In fact, the average American is more likely to apply to a reality show than they are to apply to a prestigious university. Why is that?

In short, it's because mainstream American society is delusional — at least that's what Chris Hedges argues in _Empire of Illusion_. Beginning with a look at dire statistics on illiteracy, the author tells the story of an American society whose people are blinded by illusion. By covering topics like education, the economy and the military, the author shares his theory on how and why American society is in such poor shape.

In these blinks, you'll discover

  * how many years the average American spends watching television during their lifetime;

  * how positive psychology has a negative impact on an already delusional American society; and

  * how people get on Harvard's Z-list of applicants.

### 2. Widespread illiteracy is cutting Americans off from reality. 

Believe it or not, the United States is currently experiencing an illiteracy epidemic. This serious educational failure is helping make Americans the most illusion-prone people in the world. 

In North America, _functional illiteracy_ — the inability to accomplish everyday reading and writing tasks — is rising to alarming levels. In fact, approximately one-third of the US population is barely literate or entirely illiterate. For instance, one study found that 7 million Americans are illiterate, another 27 million can't read enough to complete a job application and 50 million read at a fifth-grade level!

Americans generally aren't interested in books. Research has shown that after graduation, about one-third of high school students don't read another book for the _rest of their lives_. And the same goes for 42 percent of those with a college degree. 

This trend was actually foreshadowed by two classic works of dystopian fiction. In the first book, _1984_, George Orwell painted a picture of a totalitarian regime in which books were off-limits and information strictly controlled.

But it was one of his contemporaries that really hit the mark. His name was Aldous Huxley and his novel _Brave New World_ portrayed a future society obsessed with entertainment, one in which banning books wasn't necessary because nobody wanted to read anyway.

However, a lack of interest in reading doesn't mean that Americans don't get enough information; the only problem is that the country's primary form of mass communication, television, is excellent at manipulating images and distorting reality.

One study found that a TV is on for approximately seven hours a day in any given American home. The average american watches TV for about four hours a day — so, by the time a US citizen is 65, they'll have spent nine years of their life in front of a television!

TV continues to be hugely popular because it communicates through familiar clichés, presents predictable and easy-to-digest content, such as reality shows and sitcoms, and gives viewers the illusion of an exciting life while comforting them in their passivity.

### 3. A low literacy rate is a direct threat to democracy. 

Since Americans don't read much, newspapers and books have been relegated to the margins of a culture dominated by entertainment and images. As we saw in the last blink, the main culprit is television, which produces a dangerous culture of celebrity.

Television is seen as a powerful authority, instantly validating anyone or anything that appears on a screen. On the other hand, people who don't appear on TV are written off as insignificant, their ideas ignored.

This power and authority of television is utilized to build a celebrity culture that encourages viewers to think of themselves as exceptional, as having unique talents or gifts. The result of this cult of appearances is a society of narcissists, striving to attain illusions of success like money, sexual conquest and fame, at all costs and instead of the common good. 

Beyond all this, American TV culture has propagated the idea that it's normal, even enviable, to constantly be watched. For instance, one study found that over two times as many people apply to MTV's Real World TV series than to Harvard University; the MTV applicants are actually competing for the privilege of living under perpetual surveillance! 

But another effect of such shows is their contribution to normalizing society's constant monitoring at the hands of corporations and governments, entities which routinely infringe upon the constitutional rights of citizens. 

And politicians know how to use this culture to their advantage. Modern-day politics have come to follow the same priorities as TV: empty slogans and smiles instead of meaningful content and facts. 

In fact, the highly edited personal narratives that politicians sell reproduce the false sense of intimacy so prevalent between TV viewers and stars. 

Take President Obama and the incredible coverage that innocuous aspects of his life garner, like his attempts to quit smoking. This so-called "information," sold to the public as news by political commentators who no longer report on real issues and ideas, further distracts the public from reality.

### 4. The American education system supports a wealthy elite. 

So how exactly did illiteracy become such a crisis in America? Well, this major societal failure is itself inextricable from the fact that higher education has sold out to serve the interests of an elite few. 

These days, college presidents spend a huge percentage of their time and energy on fundraising — time that should be spent on matters related to education. The result has been the signing of countless lucrative deals with major corporations.

For instance, at the University of California at Berkeley, Coca-Cola won a monopoly on the soft drinks sold at football games, while British Petroleum negotiated a $500 million agreement to use the school's research facilities. 

Dormitories at Berkeley are even assigned according to family income, reserving the nicest rooms for the wealthiest students. 

In general, well-endowed schools focus on promoting traditional elites, no matter how poorly they perform. Plus, elite schools are only required to meet diversity standards based on race, not class, allowing them to charge insanely high tuition fees that effectively exclude the working classes. 

This is how people like George W. Bush, by no means an intellectually brilliant man, use money and influence to secure spots at top schools like Andover, Yale and Harvard: as long as you're rich enough, you can actually buy your way into Ivy League schools.

Just take Harvard's so-called "Z list," a special waiting list for 50 well-to-do, but academically borderline, applicants, many of whom have one thing in common: a parent who attended Harvard.

Elites are thus able to live their lives in a permanent state of total self-delusion. They go to school, graduate and work together, barely making contact with the outside world. As a result, they rarely encounter true differences of opinion and never question their role in society, nor the blatant inequality that made their success possible. They even erect physical walls around their lives in the form of gated communities like Short Hills and Greenwich.

### 5. The decline of the humanities is damaging society’s morals. 

The American education system may be highly inequitable, but the consequences of broader education policy in the United States are felt well beyond university campuses. In fact, the country's economic and political difficulties can be directly traced to a lack of interest in the humanities.

Post-secondary education used to be a way to expand one's mind; today, it is merely seen as a prerequisite for employment. As a result, the humanities have in recent decades lost standing in colleges across the United States. The number of people graduating from college with a degree in the humanities is now half what it was in the 1960s. Meanwhile, the number of undergraduate business students has surged, rising from 13 percent in 1970 to 21 percent today.

This not only reflects a change in the interests of students, but also a change in mentality. The fact is that people view education as a way to prepare for a career.

One could even argue that education has focused so much on skills rather than ethics that our culture is in danger of extinction. Training someone how to manage an investment trust solely requires the teaching of skills. Therefore, to prepare someone for such a job, there's no need for them to, say, debate existential and humanist issues, a key step toward building values and morals.

A culture that ignores the connections between morality and power, allows management techniques to replace wisdom and measures itself by its ability to consume rather than its compassion, is bound for collapse.

It's therefore doubtful that the American elite are capable of handling the country's current crisis. Students who aren't taught to question the foundations of their decaying culture will soon be the ones in power, incapable of envisioning paths to alternative futures.

As such, the current decay of the humanities is setting the stage for generations of timid, rule-bound bureaucrats and managerial experts like the ones who already run congress and Wall Street, and who failed to properly handle the most recent financial crisis.

### 6. Positive psychology gurus only offer the illusion of happiness. 

In the context of American illiteracy and media spectacle, a practice has arisen that promises happiness through passivity. It's called _positive psychology_, and all it really does is closes people's eyes to reality. 

So-called "happiness specialists" and practitioners of positive psychology argue that to get desirable things like money, love and a good job, it's key to focus on desire — to visualize what you want and believe in it firmly. For instance, Tony Robbins, a famous public speaker and theorist, entices readers to, as one of his books is titled, _Awaken the Giant Within_. 

However, this type of self-delusion is about as helpful for problem solving as saying your prayers. It prevents people from calling into question the structures that surround them, or the things that are actually causing their problems.

The result is a diminished capacity to realistically interact with the world, as this fantasy obscures the truth. The belief that problems will solve themselves through positive thinking offers people an escape from realities that can often be too depressing or frightening to bear. 

For instance, when reality becomes too difficult, people can simply step into a dream world, a tactic that puts us on a sure path to a delusional society. 

But positive psychology isn't just popular among individuals — it's also prevalent in corporations as well. In the corporate world, the movement primarily teaches that fulfillment is attainable through a complete surrender to authority. Corporate culture maintains a compliant workforce by controlling behavior, much like a cult. 

This is instilled through corporate trainings, staff turnover and reward systems, all managed by human resource departments. Positive psychology is used to turn employees into a "happy" collective, devoid of critical perspectives and centered on an enforced corporate identity.

In this environment, independent thinking is sacrilege, and is seen as an attack on the harmony and happiness of the company. Naturally, such constant pressure to exude false enthusiasm despite one's actual feelings causes a stressful work environment, which values image over authenticity.

### 7. America’s economic decline and rampant militarism are closely interconnected. 

You probably know that America has experienced a lengthy economic crisis. But did you know that it's pushing the government to maintain control by force?

First, let's back up to the economy's steady decline:

It all began when the country shifted from an empire of production to one of consumption. By the end of World War II, the United States controlled two-thirds of all global gold reserves and over half of global manufacturing capacity.

But by the end of the Vietnam War, crippled by huge military expenditures and declining oil production at home, America began transforming from a country that mostly made things to one that mostly consumed them. The result was a borrowing spree to maintain a lifestyle that the country could no longer afford. 

The solution to America's economic troubles was rampant militarism — but it actually ended up damaging the economy. The United States built a huge network of military infrastructure around the world, but the country's 761 military bases became a severe drain on the economy. 

And, when you consider that the US army spent $623 billion in 2008 alone, it's easy to see why! That's more than every other military on earth combined. In fact, over half of all federal discretionary spending goes toward defense; meanwhile, new and advanced weapons, often expensive and redundant, are constantly being designed to replace the old but still effective ones.

The United States has also made alliances with other countries to help increase sales of American weapons. For instance, the United States gives Egypt $3 billion in aid annually, but Egypt is required to buy $1 billion of US military equipment every year.

The result of this system is a downward economic spiral that America cannot escape. In fact, this decline has been steady since the end of World War II. By spending more than it brings in, the country has consistently had a negative trade balance and has hemorrhaged manufacturing jobs.

As a result, total public debt today is $11 trillion, or $36,676 per capita!

### 8. The United States’ moral decline is mirrored by its physical decay. 

So, the United States is in dire financial straits. Yet, as the Department of Defense continues to receive huge checks from the state, much less money is spent on the public good, and public investment in infrastructure and healthcare is in sharp decline.

As a result, public buildings, as well as infrastructure like roads, bridges and airports, are severely dilapidated and are not getting the repairs they desperately need. For instance, the Department of Education has stated that one-third of US schools are in such disrepair that their condition actually interferes with teachers' ability to teach!

Over time, a lack of public investment has sapped energy from what could be a healthy economy and has taken away the nation's ability to support itself. These days, the United States is struggling to manufacture useful products and is losing sustainable jobs.

For instance, in 2003, the New York City Transit Authority set aside $3 billion to replace new subway cars and put out a call for bids. However, not one American company answered the call. Why?

Well, it could be because the American industrial base isn't focused on producing items that maintain, improve or build the nation's infrastructure. This led the New York authorities to agree a contract with companies from Japan and Canada.

And in the same way that defense companies work hand-in-hand with the government, powerful corporate lobbies influence every aspect of the economy. In addition to weapons manufacturers, the pharmaceutical industry, for example, also wields considerable power, as demonstrated by the failed attempts to extend healthcare to all Americans.

Obviously, drug companies running the existing for-profit health system would suffer if free universal healthcare was adopted nationally. They feared that, as was the case in Canada, a single-payer system would enable the government to negotiate lower drug prices. The result was that these corporations became adamantly opposed to such an initiative, and have used heavy lobbying and campaign contributions to see it defeated.

### 9. Corporate reign is unbridled and is a serious threat to the country. 

Corporations are an unmistakeable and omnipresent facet of society. In fact, there are few aspects of our lives that they aren't somehow involved in, including countless public services. The problem with this is that for-profit companies are independent entities, and aren't accountable to the country or its workers.

For instance, corporations externalize all costs that governments will let them outsource. This goes for everything from manufacturing to finance to engineering, tasks that are increasingly being outsourced to low-wage workers in Southeast Asia.

For their part, American workers are not organized enough to respond. After all, only eight percent of private-sector workers in the United States are unionized, approximately the same percentage as in the early twentieth century. 

Meanwhile, corporations are growing stronger by the day. In fact, they now control both the Republican and Democratic spheres of the country's political leadership, as politicians are desperately reliant on the private sector for campaign contributions. 

As a result, America is governed by a wealthy elite that advances an anti-regulatory agenda. For instance, the Glass-Steagall Act, a measure borne of the Great Depression, was designed to prevent investment banks from speculating with savings deposits. But this essential legislation was rolled back by the Clinton administration in 1999, paving the way for the 2008 financial crisis. 

However, as we know from history, economic and financial catastrophes also open the door for political extremism, particularly because a society based on artificial structures is prone to totalitarianism. For instance, the fall of the Weimar Republic allowed for Adolf Hitler's election.

The cycle tends to flow as follows: 

Generally, corruption and stalled politics produce cynicism, disengagement and eventually anger. The people who lose the most from an inert political world also lose faith in the nation and turn to violence. As a result, the concept of the common good ceases to exist.

For example, all the great civilizations of the past, from Rome to Egypt to Byzantium, have risen and decayed for various reasons. But every single one of them was at some point controlled by a corrupt elite and, before they collapsed, experienced a deterioration of their morals.

### 10. Final summary 

The key message in this book

**America is facing an educational crisis. Illiteracy has reached unprecedented heights and the fallout is proving disastrous. Every major aspect of the country, from educational institutions to the economy to the government, have come to be controlled by a wealthy elite that have never learned the importance of ethical thinking and a just society.**

**Suggested** **further** **reading:** ** _Death Of The Liberal Class_** **by Chris Hedges**

_Death of the Liberal Class_ is a serious indictment of modern liberalism and today's liberal leaders. It offers a scathing critique of the failures of contemporary liberal institutions while still providing a glimmer of hope for the future of American democracy.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Chris Hedges

Chris Hedges is an American journalist. He has worked for _The New York Times_ as a foreign correspondent in Central America, the Middle East, Africa and the Balkans, and is also the author of the best-selling books _War is a Force That Gives Us Meaning_ and _Death of the Liberal Class_.

