---
id: 57cdcc3f2b2b0d0003fa29d0
slug: enchanted-objects-en
published_date: 2016-09-08T00:00:00.000+00:00
author: David Rose
title: Enchanted Objects
subtitle: Innovation, Design and The Future of Technology
main_color: 2675BF
text_color: 1C568C
---

# Enchanted Objects

_Innovation, Design and The Future of Technology_

**David Rose**

_Enchanted Objects_ (2014) explains that it's high time to move away from screen-based devices toward a world of more intuitive, useful and efficient devices, designed for a specific purpose. The author and successful entrepreneur draws on his experiences at MIT Media Lab to explore our technological wants and how we can achieve them through enlightened design.

---
### 1. What’s in it for me? Better understand the role design plays in the development of personal technology. 

A few decades ago, the most important piece of technology was probably the personal computer. Just a decade ago, a person might say he couldn't live without his Blackberry. But today? There's no question that smartphones are a must-have device.

With each jump in technological discovery and development, the role that tech devices play in our lives expands. So the question is, where do we go from here?

These blinks show you where technology will take society tomorrow, exploring the deep questions of what we want or even need technology for in our modern lives. The technology of the future should be more than about tracking our steps and apps to count calories; indeed, tomorrow's technology should be enchanted.

In these blinks, you'll also learn

  * how a crystal ball could tell you when to shut off the air conditioning;

  * why a talking bathroom scale is better than a life coach; and

  * how a glowing table might finally silence your overly chatty colleague.

### 2. Enchanted objects free society from the tyranny of the screen, sharing information simply and intuitively. 

What will technology look like in the future? Perhaps we'll be served by personal robots, or maybe we'll all become touch screens!

There's one idea that could change life as we know it. And it's already in motion: _enchanted objects_ are the technology that will shape our future.

What is an enchanted object? It's technology that makes our lives more convenient without distracting us. That's right, not like a smartphone!

Enchanted objects offer information at a glance, unlike screen-based devices that require your full attention as you read and respond to messages.

Why is the difference between screen-based technology and enchanted objects so crucial? It comes down to neurology. In the human brain, two systems exist in fierce competition with each other.

The first system allows us to make quick decisions, based on impulse and experience. The second system encourages us to make rational, calculated plans. The first system is the "faster" of the two, which explains why people can recognize and process colors, faces and shapes much faster than they can read text.

Much of today's technology relies heavily on the brain's second system; we read or navigate blocks of text on phones and computers. As a result, such technology is inherently slower and less intuitive.

Enchanted objects, in contrast, offer the benefits of technology without forcing a user even to think about it. Imagine that you're waiting at a bus stop. An enchanted object — in the form of a pole at the bus stop — features a glowing countdown clock to tell you how soon your bus will arrive.

Enchanted objects are simple and intuitive, and the way we interact with them is too.

Many enchanted objects make use of natural human gestures. Rather than tapping or swiping a screen, you could give an enchanted object a kick to tell it what to do. Seriously!

The Amazon Trashcan, for instance, uses cameras and sensors to track waste and reorder stock. Want to cancel an order? Just gently kick the can, and you're done.

Enchanted objects offer society a more natural and effective way to engage with technology. But that's not all. We'll learn about the significance of enchanted objects in the next blink.

### 3. Enchanted objects can turn fictional ideas of guardian swords and crystal balls into reality. 

From crystal balls that can divine the future to mystical swords that warn of danger, people have shared stories and ideas of magical "devices" for ages.

Yet today the dream of objects with supernatural powers isn't just make-believe. New developments in technology are making this dream a reality. Let's look at a few cases.

Humans, by nature, are curious animals. We want to know how the world works! Which is a good thing, as the more informed a person is, the better the chances of survival in the world. It's no wonder that we find the mythical idea of a crystal ball, a device that can see into the future or answer any question, so attractive.

The author used the idea of a crystal ball as his inspiration when developing a device called the _Ambient Orb._ This device glows different colors to indicate a change in information or state, for things such as stock prices to wave height at the beach!

One California power company uses the Ambient Orb in its business decisions. The orb tells a customer when using energy is the least expensive, so customers can avoid using energy-hungry devices at peak times, potentially avoiding a costly disruption such as a brownout.

Another new idea was inspired by the fictitious world of Harry Potter. Remember the Weasley Clock? The hands of this "clock" showed the state of the Weasley family, whether they were home, on the way or potentially in danger.

Google engineers inspired by this idea have created the _Latitude Doorbell._ This device chimes once a GPS tracking system registers that a family member is heading home.

Finally, we have always been attracted to the idea of enchanted objects as guardians or protectors. Frodo's sword, Sting, glows blue when orcs are near in the magical world of The Lord of the Rings, for example. The author's idea for an _Ambient Umbrella_ applies a similar idea: it glows if rain is expected, so you won't leave it behind on your way out the door!

### 4. Technology based on enchanted design can help you live well, travel safely and be more creative. 

Did you know that half the entire US population takes prescription medications? And that some 50 percent of those individuals fail to take their medication on any given day?

Given such statistics, the US healthcare system has a real challenge on its hands. We all need to remember to take the pills that we're prescribed, yet that's often easier said than done.

Some people simply forget, just like the character Neville Longbottom in the Harry Potter series of books. Luckily, Neville had a Remembrall, a ball-shaped device that reminded him when he'd forgotten something crucial.

The author thought that a Remembrall would be a wonderful thing for the medical industry. So he created an enchanted device that would remind a patient, using light and sound, to take a pill.

Called the _GlowCap_, this device attaches to a standard medical pill container and can even order refills for a patient automatically.

Enchanted objects could do more than just help people stay on track with medication, however. They can even revolutionize the way we travel.

Take self-driving cars, for instance. Self-driving cars are safer as they reduce the chance of accidents caused by human error. Self-driving cars can also relieve the pain of looking for parking!

Imagine this: a car drops off its passenger at work, drives itself out of town, parks, then returns to pick up the passenger on time. No need to walk for ages to a parking spot, no need for unsightly parking garages in the middle of a city!

Finally, enchanted objects can help humans do what we do best: express ourselves!

By giving people the tools to learn and develop new skills, enchanted objects can take a person's creative potential to the next level. A great example of this is Guitar Hero, an enchanted object that inverts the learning curve, making learning to play the guitar initially easy and gradually ramping up the difficulty level as a person progresses.

Enchanted objects can even help children learn through play. LEGO Mindstorms feature LEGO bricks with built-in sensors that react to touch, sound and light. Children manipulate the behavior of their toy designs while also learning a mode of thinking essential to computer programming languages!

### 5. The most effective enchanted objects will be wearable, functional and cute, too! 

We've all been there: you look at your smartphone to notice that the battery is at three percent. At any moment, you'll lose access to your camera, your maps and the tools you use to communicate.

Let's face it: we're dependent on our smartphones. But this may change in the future. Enchanted objects will allow us to distribute essential tools across many devices, rather than collect them all in just one.

Some companies are taking advantage of fast yet miniature technology and creating sensors that can be worn on the body. The _Sunsprite_ is one example of this tiny tech.

Developed for people with Seasonal Affective Disorder (SAD), the Sunsprite is a small, wearable sensor charged by the sun that measures whether the wearer is receiving sufficient sunlight.

Enchanted objects are designed based on technological advancements that put durability and longevity first. Many popular tech devices today sacrifice these traits for portability and aesthetics.

An enchanted object is designed for a single purpose — and its goal is to fulfill this purpose as effectively as possible, for as long as possible. The Facebook Coffee Table is a good example of this: a sturdy table with one function, to display photos of friends mentioned in passing conversation on a built-in screen.

Finally, enchanted objects can reach their full potential when designed to connect emotionally with a user. After all, we're far more likely to use an object that's "cute" or which we find somehow loveable.

When creating interactive medication packaging, the author teamed up with artists to design a product for which a user might feel affection. The team decided to use cute animals to give the packaging a sense of emotional appeal and thus encourage their use.

So now you're familiar with a few examples of what enchanted objects can do. Let's look at how we can create such technological wonders today.

### 6. The data devices collect about you could be used to create more personalized experiences. 

What if your bathroom scale could do more than display a few numbers on a monochrome screen? By linking enchanted objects to the internet, we could create a household full of interconnected devices to help us organize and improve our lives.

To "enchant" an object, you need to help the device get to know you. Using smartphones and tablets has already given us a head start, as we already use cloud-based systems to connect screen-based devices to the internet. This connection then enables a device to combine the information it's collected about you with internet resources, and even artificial intelligence.

By connecting household items in the same fashion, we could create a truly "smart" home. Your bathroom scale could show you your progress toward losing weight, perhaps even offer you much needed encouragement, for instance!

Devices collect a wealth of data about us, so why not use this data to create personalized experiences?

Rather than offering conventional health tips, an enchanted bathroom scale would know our medical history, age and even attitude toward losing weight. This would allow it to offer advice that would genuinely help you, and deliver it in a way that would encourage you to listen.

If technology such as this were implemented in homes across the world, the health care sector of many countries would save billions in needless costs every year!

### 7. Enchanted objects that encourage sharing and friendly competition are the most engaging. 

What's more satisfying than reaching a personal goal? Telling friends how you got there, of course!

When we connect enchanted objects to social networks, we can use the power of communities and sharing to stay motivated.

Enchanted objects that encourage social interaction will be the ones people use most. Research has shown that a person is far more likely to achieve a goal when the person feels he belongs to a group that cares about him, whether it's a community recycling meet-up or a men's walking group.

We can also use enchanted objects to stay motivated by giving devices the latitude to turn progress into a game. The basis of the video game industry, after all, is the human drive to compete and improve.

Points systems and leaderboards work well to push players to do better. An online community recycling leaderboard, for example, could encourage an entire neighborhood to be more conscious about the environment.

Rewarding players with medals or acknowledging winning streaks are other techniques used by game developers to give users the feeling that they're gaining experience and making progress. Posting data that points to a winning streak in losing weight, for example, could be integrated into an enchanted bathroom scale to stop you from abandoning your exercise routine after just two weeks!

> _"Your recycling bin and solar panels will post data and compare your progress with your environmentally minded friends."_

### 8. Homes, workplaces and even cities can benefit from enchanted objects. 

Most urban homes may seem modern, but really, they aren't. Cramped, inefficient, often expensive and constantly in need of repair, our "modern" homes rarely make the most of what technology has to offer.

Building more flexible _transformer homes_ is one place to start. The modern home should be intuitive, adjustable and efficient. Limited space can be made more useful by using the same space for different purposes. City homes can convert a living space into a dining room, a study, a workout space or even a bedroom, depending on the need at a given time of day.

Yet having to unfold a couch bed when you're exhausted can be a drag, so why not automate it? A wall clock or remote control could be programmed to time such space transformations in a home.

Enchanted objects could also help us get the most out of workspaces by improving communication.

Think of the last business meeting you attended: the extrovert in the room dominated the discussion, right? To ensure that other team members share thoughts too, the author developed the _Balance Table_.

This sleekly designed table has a built-in constellation of LED lights to show attendees sitting at it who's been chatting on the longest. With the obvious cue of the table's lights, team members are encouraged to keep their input balanced.

We could enchant whole cities, too! Future urban areas can work to improve living standards by connecting city infrastructure with the cloud. Linking important city services to a wealth of online data could allow for a more informed, efficient use of resources.

We've already seen this in effect, as BigBelly trash cans are used today in some US cities. BigBelly trash cans have built-in, solar-powered trash compactors. The trash in a BigBelly takes up less space, so it doesn't need to be collected so frequently. These trash cans are also connected to the cloud to alert garbage collectors when they should be emptied, saving time and money.

### 9. Connecting and customizing enchanted objects will allow us to “hack” our daily workflows. 

In the future, we'll all be hackers. Not necessarily with the goal of revealing classified government documents but with the drive to make enchanted household objects fit our needs perfectly.

Enchanted objects will be programmable, giving users true control over functionality. Most products today can only do what a designer created them to do. A watch tells the time; a thermometer measures temperature. But enchanted objects will allow users to choose from a range of functions.

A recent example of a flexible enchanted object is _Twine_, a small plastic box containing a microprocessor. Created by two MIT graduates, Twine can be programmed to measure speed, moisture, temperature and vibration, as well as many other variables.

Twine is also connected wirelessly to the internet. Put a Twine device in the basement to inform you via email when humidity levels rise, potentially signaling a water leak; attach a Twine device to a bird feeder to receive an alert every time a bird comes to snack. The possibilities are endless!

By creating a web of connection between devices in the cloud, we'll be able to streamline and automate many time-consuming tasks. Doing so will leave us more time for what's important, whether being creative, learning new skills, excelling at our job or simply spending more quality time with those we love.

### 10. Final summary 

The key message in this book:

**Today's screen-based devices slow society down. The arrival of enchanted objects, devices with a single design purpose and connection to the cloud, brings what was once seen as science fiction closer to reality. From enchanted bathroom scales to crystal balls, we'll soon be able to upgrade the objects in our lives, making them automated, more efficient and intuitive.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Design of Everyday Things_** **by Donald A. Norman**

This book explores the cognitive psychology of good design and what makes a product that responds to users' needs. The author develops the common barriers to good design, how to reduce and fix errors, and how to bring users and technology closer together.
---

### David Rose

Inventor and entrepreneur David Rose is a faculty member at the MIT Media Lab. He is also the creator of several innovative, award-winning devices; in the late 1990s he invented and patented online photo sharing.

