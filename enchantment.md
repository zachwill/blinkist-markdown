---
id: 53c6724338393400073b0100
slug: enchantment-en
published_date: 2014-07-15T00:00:00.000+00:00
author: Guy Kawasaki
title: Enchantment
subtitle: The Art of Changing Hearts, Minds, and Actions
main_color: CE3C2D
text_color: 9C2D22
---

# Enchantment

_The Art of Changing Hearts, Minds, and Actions_

**Guy Kawasaki**

Guy Kawasaki's _Enchantment_ teaches you how to change the hearts, minds and actions of the people around you, turning them into spirited advocates of your cause. It also provides key steps on the path to building a lasting fascination with your product and company, as well as useful advice on how to resist the enchantment of others.

---
### 1. What’s in it for me? Learn how to enchant others with your passion. 

In his tenth book, _Enchantment_, Guy Kawasaki looks into the question of how you can win people over to your cause, entice them to buy your product and truly love your brand.

Although modern technology and international connectivity have made it easier than ever before to reach out to millions of people across the globe, it remains difficult — and is in fact even harder today — to convince them to try something new.

The market has grown, but so has the competition. And consumers aren't affected by advertising the same way they once were.

In order to reach consumers, we need to quit thinking about how to just get them to buy now. We need to figure out how to _enchant_ them: to get them enthralled with our cause, and turn them into arbiters for our mission, free-roaming salespeople who are true to our cause.

These blinks will teach you all about the art of enchantment, and how you can use it in order to create a fanatic fanbase who just can't wait for your next product or service. It will also provide you with some tips for how to avoid becoming enchanted yourself by those who just want to make a quick buck.

In addition, these blinks will show you:

  * how inviting Filipino militants inside for coffee totally changed one woman's life,

  * why one company has so much faith that their customers won't try to rip them off, and

  * why it's better to not just enchant your customers, but anyone and everyone.

### 2. Enchantment is more than manipulation: it’s about triggering actions through emotions. 

Have you ever been in a situation where someone was just so nice or captivating that you really just wanted to help them? Likely, you felt _enchanted_ by this person. The greater your goals and the fewer resources are available to you, the more useful enchantment will be in winning people's hearts and minds and motivating their actions. But what is it exactly?

Enchantment is the ability to change the nature of relationships, and therefore influence other people's actions. Consider, for example, the story of Karin Muller, a filmmaker and author who worked in the Peace Corps, building wells and schools in a Filipino village during the late 1980s.

One day, seventeen members of the New People's Army (NPA), an armed wing of the Philippines' Communist Party, came to interrogate her. When she saw them, she exclaimed: "Thank God you're here. I've been waiting all day. Please have some coffee. Leave your guns at the door."

Muller was able to totally transform the dynamic of her situation, turning it from an obvious show of force to "conversation and delight," by appealing to the leader's emotions.

But enchantment isn't just useful for getting out of a pickle. In fact, it can happen anywhere, anytime. And it's not just about making money, or even personal gain. It's about filling others with the same delight and passion you experience about something.

For example, when Steve Jobs was developing his iPhone, he wasn't contemplating the best strategy to get consumers to put money in his personal bank account, or entice them to sign on to a two-year contract with AT&T. He, like all enchanters, was selling his dream for a better future — one in which people can have cooler social interactions and use technology to improve their lives.

This means that, if you want to enchant people, you have to have a dream that you're striving to achieve. But, there is more to the equation.

> _"The first follower is what transforms the lone nut into a leader."_

### 3. Likeability and trustworthiness are the foundations of enchantment. 

Have you ever felt enchanted by someone you didn't like or couldn't trust? Probably not. If you want to be able to enchant people, then you first have to get them to like and trust you. But how?

The fundamental key to enchantment is accepting and liking people as they are. Arrogance and self-centered behavior, therefore, are your greatest obstacles.

In order for people to like you, they first have to accept you. But in order for them to accept you, you have to take the first step by accepting them. Not everyone is immediately likeable, so keep in mind that we _all_ have strengths and weaknesses, before writing anyone off.

What's more, we often find that passionate people are more easily likeable and interesting. Thus, you should be very open with other people about your passions, as your excitement is infectious.

The easiest way to enchant someone is to connect with them on your shared passions. These connections aren't always easy to find, but don't give up! Assume you have something in common, and do your homework on the specific person whom you want to enchant.

However, if you want to be enchanting, then it's not enough that people merely _like_ you. As the American motivational speaker Zig Ziglar once said: "Every sale has five basic obstacles: no need, no money, no hurry, no desire, no _trust_."

In order for people to trust you, you must demonstrate that you are knowledgeable and competent, and understand that trust works in both directions.

American shoe retailer Zappos offers a great example of this two-way trust. Customers trust Zappos because of its money-back guarantee and free shipping for both deliveries and returns. Zappos, in turn, trusts their customers to not abuse this trust by sending back shoes that have been worn.

As with most things, actions speak louder than words. So Kawasaki recommends to "be a mensch." One who is authentic and transparent is showing proof of his or her good intentions.

So now that you know what enchantment actually is, the following blinks will focus on how to make your brand or product enchanting.

> _"Every sale has five basic obstacles: no need, no money, no hurry, no desire, no trust."_ — Zig Ziglar

### 4. In order to enchant people, your product or service must itself be enchanting. 

Do you often buy things just because you like and trust the salesperson? That may be true _sometimes_, but even the best salesperson will fail to enchant their clients with a mediocre product, service or idea that doesn't deliver.

If you want your product or service to be enchanting, then it must have these five qualities — it must be DICEE :

_Deep_ : A deep product demonstrates value and features at multiple levels, being able to fulfil the immediate and future needs of your customers and developing over time. Google, which started off merely as a search engine, has recognized user needs and has since developed an email service as well as a broad selection of online tools and services.

_Intelligent_ : An intelligent product solves problems with efficiency and elegance. Ford's MyKey allows the car owner to set driving speed limits, thus offering worried parents peace of mind when their teenager grabs the keys to the car.

_Complete_ : A complete product is one that provides a great experience for the customer during the entire life-cycle of the product. For example, the after-sale service of Lexus is as much a part of the Lexus experience as the car itself.

_Empowering_ : An empowering product makes us feel smarter, stronger, or more skilled than before we had it. It is this very feeling that makes many of us consider our computers or favorite search engines to be part of us.

_Elegant_ : Elegant products are ones that work _with_ or _for_ their users. The designers thought long and hard about how to design for optimized user experience. A great example of elegance in design is the simplicity and user-friendliness of Apple products, such as the single-button iPod.

By following DICEE, you have in your hands the blueprint for a product design that will not only deliver, but also enchant.

### 5. Launching a product is all about two things: story and testing. 

Having a high-quality, enchanting product is crucial to your success. But your product won't sell itself. You first have to present it to the public, and do it right.

Unfortunately, most launches simply convey vague information. But as Annette Simmons, author of _Whoever_ _Tells_ _the_ _Best_ _Story_ _Wins_, said: "People don't want information. They are up to their eyeballs in information. They want faith — faith in you, your goals, your success, in the story you tell."

Luckily, there are two great ways to achieve this:

First, you have to inject yourself into your product launch, thereby giving it a personal touch and story. Be transparent, open with your emotions and your story, all while being likeable and authentic.

Secondly, in order to truly infatuate people with yourself and your cause, give them a hands-on trial with the following characteristics:

_Easy_ : Make sure people can use your product or service without much training, expertise, time or guidance.

_Immediate_ : When people are eager to dive in, don't make them wait! Immediacy means forgoing things like making them fill out long forms or waiting for passwords or approval to get started.

_Inexpensive_ : When people are trying out your new product or service, they shouldn't have to pay anything more than their time. With the exception of serious fanatics, virtually no one is willing to pay for the "privilege" of being a customer during product trials.

_Concrete_ : When people use your product, they should notice concrete improvements in their lives. In order to become believers, they'll need to see the evidence for themselves.

_Reversible_ : People should be able to reverse their decision to try out your cause without trouble, similar to Zappos' no-cost, no-questions-asked return policy.

Finally, when you're in the launch phase, don't focus your attention on the so-called "influencers" at the expense of the "nobodies." Your goal should be to reach as many people as possible, delight them with your passion, and turn them into advocates.

### 6. People naturally don’t like change, so you’ll have to appeal to their psychology to win them over. 

Enchantment always means changing someone's beliefs and perspectives. But like every change, these don't happen easily. You have to overcome certain obstacles, most of which are buried deep in audience members' minds.

For starters, people usually resist change. They want to maintain the status quo and don't like making decisions. This hesitancy is due in part to a strong fear of making mistakes, but also to a lack of risk-taking role models to follow. So how do we remove these obstructions in order to entice people to get behind our cause?

One way is to offer them some sort of _social_ _proof_, i.e., show them that other people they trust are doing it, so it's safe for them as well. If you can show them that others have embraced your cause, then they will follow suit and embrace it too.

For example, Colleen Szot, a copywriter in the infomercial business, was able to increase sales simply by changing the script from "operators are waiting, please call now" to "if operators are busy, please call again." When people heard the new pitch, they assumed that the product was doing so well that the operators couldn't keep up with incoming calls, thus offering them immediate social proof and enticing them to call in.

Another way is to make your product seem scarce, so that people believe it to be more valuable. A perfect example of this is Google's email service, Gmail, where accounts were once authorized by invitation only. The desire for these invites became so hyped that people even bought them on eBay!

So which strategy should you use? Social proof or scarcity?

This depends on the degree of uncertainty and doubt people have about your product. In the face of high uncertainty, use social proof in order to alter their perception. When their uncertainty is minimal, emphasize scarcity to give your product hype, moving interested customers to more immediate action.

> _"Enchantment requires understanding why people are reluctant to support your cause."_

### 7. Entice your customers to internalize your brand by creating a space that fosters brand loyalty. 

If you are selling a product or service, would you rather have people simply buy your products, or act as unpaid advocates for your cause? Obviously the latter, but how do you get people to internalize your cause and carry your banner for you?

First, you need to create the feeling that your customers belong to a group of people who share the same interest and identify with what you have to offer.

Second, you have to motivate people to make an actual commitment to your cause, and honor that commitment. The best way to do this is to create an ecosystem — a community, via your website, blog or Facebook page — that fosters loyalty. Follow these guidelines so that people join your ecosystem:

First and foremost, _create_ _something_ that's actually worth joining. This means making sure that your ecosystem provides actual value to its community members and doesn't just serve _you_.

Next, find an _evangelist_ and champion for your cause, who will build and support your ecosystem and carry the flag for the community.

Make sure there is _room_ for people to do something meaningful in your ecosystem. In other words, don't set too many constraints.

Welcome _criticism_ _and_ _discussion_ about your cause or product. Don't try to cover up any failures, but be open and transparent, as this will help to cultivate trust.

Create a reward system for your ambassadors that reinforces your brand and doesn't include money. Maker's Mark, for example, rewards their whisky ambassadors by putting their names on a barrel and providing them the opportunity to _buy_ a bottle from the batch.

As you can see, enchantment is not a singular event, but rather a process that must be carefully tended. The next blink explains how you can use technology to help you enchant more people and _keep_ them enchanted.

> _"This is a golden age of enchantment because reaching people around the world has never been easier, faster or cheaper."_

### 8. Use push technology to reach out and deliver short but valuable information. 

Once you've created a product or service that encompasses your vision, you're going to want customers, and that often means using _push_ _technology_, technology where _you_ reach out to the customer.

Unfortunately, there is some major competition for your customers' attention. In order to maximize effectiveness of this direct communication, keep these principles in mind:

_Engage_ _fast_. We've grown so accustomed to the immediacy of technology that we also expect this in our correspondence. When your customers contact you, they too expect a fast response. Try to deliver your response in under 24 hours.

_Engage_ _many_. Don't just try to capture the attention of a few influential people. Remember that "nobodies are the new somebodies in a world of wide-open communications," so try to reach as many as possible.

_Engage_ _often_. Remember that "engagement is a process, not an event." Engage your customers often, and vary the way you package your messages — e.g., with different media, such as pictures, videos, live chats, and audio — so that you don't bore them with redundancy.

_Provide_ _value_. If you want to enchant people, then you can't start by immediately trying to sell or promote your cause. Instead, provide potential customers with immediate value in the form of helpful advice, or inspiring or entertaining content. You can also share your own personal insights or advice as a way to be more genuine.

_Be_ _short_ _and_ _concrete_. Presentations and email tend to overflow with information, and this can be distracting and confusing. Try to limit your emails to no more than six sentences. When making presentations, follow the _10:20:30_ _Rule_ : 10 minutes, 20 slides, no font smaller than 30 point.

So which technology should you use to push your message?

According to the author, Twitter is among "the cheapest and most effective ways to engage and enchant people." It's cheap, easy, and allows direct communication with potential customers. So try it out!

> _"Twitter is the most powerful enchantment tool I've ever used in my career."_

### 9. Use pull technology to provide potential customers with essential information about your cause. 

While push technology allows you to directly engage people and draw their attention, it can only transport a limited amount of information. This is where _pull_ _technologies_, like websites, blogs and Facebook, come into play.

In order to maximize the enchanting power of your pull technologies, be sure to follow these simple guidelines:

_Provide_ _good_ _content_. Just because you have nearly unlimited space, doesn't mean you shouldn't impose limits on your content. Tailor the content around your cause and the interests of your target market by only providing content with intrinsic value that is inspirational, entertaining, enlightening and/or educational. Make sure you update frequently and use many forms of media, such as graphics, pictures and videos.

_Provide_ _pages_ _for_ _FAQ_ _and_ _About._ People love FAQs because they cut right through all the fluff and answer their most pressing questions. In addition, no visitor should have to wonder what your cause is about. Be sure to present all the essential information about you and your cause in an About page.

_Be_ _aesthetic_. Express things as simply and naturally as possible, calmly yet energetically, without overloading viewers with information. Don't feel like you need to use every empty pixel on your page, and make sure your user interface is simple by approaching your design from the user's perspective.

Often, Facebook is a great place to put these principles into action. A Facebook fan page is fast, easy and inexpensive. As an example, the Facebook fan page for _Enchantment_ cost only around $1,750.

Not only that, but "if Facebook were a country, it would rank third in the world in population — behind China and India." It has a huge user base, and consequently an enormous amount of people who could be interested in your cause, so it only makes sense to make a presence there.

By now you should have a good idea of what tools you can use to attract customers to your cause. In these final blinks, you'll learn about other ways to use enchantment.

### 10. Money is important, but if you want to enchant your employees, you’ll need intrinsic motivation and celebrations. 

What is more likely to motivate you to work hard: a project that pays well but is ultimately useless, or a project that pays less but is deeply rewarding and intrinsically valuable? If you aren't hard pressed for cash, you'd probably be more interested in a personally rewarding experience, and you wouldn't be alone. If you want to motivate and enchant your employees, you need to provide MAP: mastery, autonomy and purpose.

_Mastery_ : If have to do something for eight hours every single day, then you'll want to be good at it, otherwise your work day will be miserable. Providing employees with new skills not only increases productivity but also helps them to take an interest and pride in their work.

_Autonomy_ : Management needs to trust that their employees are competent enough to do what needs to be done. Giving employees some freedom is essential to their happiness.

_Purpose_ : By far the most important element to motivation at work is the feeling that what you and your company are doing is intrinsically valuable. Above all else, an employee's work should provide her with a mission that she actually wants to complete.

Another important aspect of an enchanting work environment is celebrating success. Celebration is incredibly powerful, and can easily offset the damage caused by failure.

According to Brenda Bence, author of _How_ _YOU_ _Are_ _Like_ _Shampoo,_ celebrating success can motivate employees to work harder, and unifies them around common goals. In addition, it helps focus employees' attention on the work tasks, as well as communicates organization's values.

Creating an enchanting work environment is not just icing on the cake. It's crucial: the more that employees are totally turned on to your ideas, the more likely they are to secure enchanted customers.

### 11. Defend yourself against dubious enchanters by taking time to ask the right questions. 

Have you ever felt totally enchanted by someone, just to have them dupe you and take advantage? Unfortunately, not every enchanter has good intentions and genuine interests. So how can you defend yourself against the enchanting persuasion of others?

The most immediate way is to be aware of and avoid situations where you can be easily tempted. For example, the low prices of an outlet mall or the high-pressure of an auction can lead you to make regrettable decisions.

Moreover, if you're stressed, sick, tired or in a hurry, then you're especially vulnerable to rash purchasing decisions. Avoid or delay your decision-making if you've had to skip breakfast!

It's also a good idea to use a checklist whenever you encounter a tempting situation, to make sure your decision isn't totally unfounded. There's no shame in second guessing yourself: even the most successful and seemingly intelligent people make decisions out of temptation they later regret.

Next time you're making a big purchasing decision, take a step back and ask yourself the following questions:

  * Am I missing any information that would help me make a rational decision?

  * Am I making a decision based on facts? Or is it because people around me are doing it?

  * What will be the impact of this decision a year from now?

  * Would I still make the decision if I waited a week, a month, a year?

  * Have I made a similar decision before that I ended up regretting?

  * Am I fully aware of this decision's total cost (including hidden costs like the cost of installation, support, maintenance, upgrades, etc.)?

  * Would I still make this decision if _everyone_ knew about it?

We all have a capacity to withstand enchantment if we just take the time to pause and reflect, but that doesn't mean every decision has to be an ordeal. Don't waste your time fretting over small and harmless things. Save the deep thinking for the important decisions.

### 12. Final summary 

The key message in this book:

**In** **today's** **world,** **the** **best** **way** **to** **get** **people** **to** **sign** **on** **to** **your** **cause** **is** **not** **through** **persuasion** **or** **mere** **marketing,** **but** **through** **enchantment.** **It's** **about** **telling** **the** **entrepreneur's** **dream** **and** **getting** **people** **to** **follow.** **Today's** **technology** **makes** **it** **easy** **to** **convert** **others** **to** **your** **values** **and** **make** **your** **cause** **their** **own.**

Actionable advice:

**Cast** **a** **wide** **net.**

When you are trying to reach potential customers, don't spend all your time focusing on the "influential people." Instead, try to capture the attention of as many people as possible so that you can convert the most idle consumers into brand fanatics. Remember: in a world of unfettered access to communication, "nobodies" are the new "somebodies."
---

### Guy Kawasaki

Guy Kawasaki is the bestselling author of numerous books, including _The_ _Art_ _of_ _the_ _Start_ and _The_ _Macintosh_ _Way_. In addition, he was chief-evangelist for Apple as well as an advisor to the Motorola business unit of Google, and today works as chief-evangelist for Canva, a graphics-design online service.

