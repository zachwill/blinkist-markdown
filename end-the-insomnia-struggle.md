---
id: 59930812b238e100058f78c3
slug: end-the-insomnia-struggle-en
published_date: 2017-08-17T00:00:00.000+00:00
author: Colleen Ehrnstrom, Alisha L. Brosse
title: End the Insomnia Struggle
subtitle: A Step-by-Step Guide to Help You Get to Sleep and Stay Asleep
main_color: 1F6F9C
text_color: 1F6F9C
---

# End the Insomnia Struggle

_A Step-by-Step Guide to Help You Get to Sleep and Stay Asleep_

**Colleen Ehrnstrom, Alisha L. Brosse**

_End the Insomnia Struggle_ (2016) is a guide to getting a good night's sleep. These blinks are full of practical advice on how to handle sleeplessness. They explain, among other things, how to track your sleep problems, understand the science of insomnia and apply a variety of strategies that'll help you get the rest you need.

---
### 1. What’s in it for me? Discover techniques for a good night’s sleep. 

When you go to bed at night, do you fall asleep instantly and then wake up eight hours later, refreshed and ready to take on the world? Or do you struggle to get good-quality shut-eye?

If you often experience difficulties sleeping, staying asleep or getting the kind of restorative sleep you need to function well during the daytime, you might suffer from insomnia. Perhaps you have already reached out to friends and family hoping to find the golden advice that'll help you get rid of this exasperating condition.

If you have yet to do so, or if the help you've received so far hasn't been effective, now might be the time to try out _Cognitive Behavioral Therapy for Insomnia (CBT-I)_ — a treatment focused on affecting the physiological parameters of your sleep through behavioral and cognitive strategies. These blinks contain a wealth of advice based on this approach.

In these blinks, you'll discover

  * how you can get your sleep drive to work as it should;

  * why you should set a wake-up time and stick to it regardless of when you fall asleep; and

  * how modifying the way you think about sleep will affect your ability to get to sleep.

### 2. Maintaining healthy sleep patterns is a team effort between two distinct biological processes. 

Insomnia is a complex topic, so before we set off on a journey to understand it, let's take a closer look at the biological processes that regulate sleep. One of the key forces in this regard is known as the _sleep drive_, which regulates our sleeping and waking hours.

The sleep drive is essential in sending the body to sleep when necessary, making it crucial to our very survival. Simply put, it increases with every minute we spend awake, eventually becoming so strong that it tells us to get some shut-eye.

Another key sleep-regulating biological process is the _internal body clock_. This biological timer helps maintain a cyclical rhythm based on night and day, which regulates our sleep.

It accomplishes this incredible feat by adjusting the temperature of the body, impacting the nervous system and producing the sleep-related hormones _cortisol_ and _melatonin_. In doing so, it's supported by external forces like sunrise and sunset.

Back in the 1960s, German scientist Rütger Wever demonstrated how the internal body clock works by isolating study participants in a windowless basement. At a certain point, their internal body clocks fell out of sync, showing that humans rely on their environment to maintain their natural sleep rhythms.

So, the sleep drive and the internal body clock are both essential, but to function properly these two mechanisms need to work in close collaboration; if one falls out of sync, it'll easily wreak havoc on the other.

If your internal body clock gets thrown off, maybe from working in a windowless office, it's only a matter of time before your sleep drive starts to suffer as well. After all, it won't receive the proper environmental cues it needs to produce a rhythmic craving for sleep.

It's a lot to take in, but now that we've got those basic facts down, we're all set to take a closer look at insomnia — and, more specifically, how to overcome it.

### 3. There’s no set treatment for insomnia, but keeping track of your sleep helps. 

If you're like most people with insomnia or other sleep-related issues, you've likely asked for help and been met with an endless and diverse list of remedies and advice. This is actually quite normal; the fact is, there's no hard set of rules for dealing with insomnia.

So, while plenty of people will say, "just cut back on coffee!" or "get more fresh air," none of these simple approaches will fix all cases of insomnia. After all, every person is unique, and there's no one-size-fits-all treatment.

That's precisely why you should start keeping a _sleep log_ to help you ascertain your particular needs. This tool is essentially a journal of how you sleep, and it's instrumental in identifying patterns and tracking progress.

Ideally, you should write in it twice a day, once in the morning and once in the evening. When you wake up in the morning, you can log your _sleep cycle_ from the previous night: how many hours you spent in bed, how many hours you actually spent asleep and when you woke up.

Then, at bedtime, you can log all the information that might be relevant to your quality of sleep during the coming night. You can include things like what you ate that day, if you took a nap or anything else that feels relevant.

By tracking all of this information, you'll learn what works for you and your lifestyle. From there, you can use your sleep log to begin choosing strategies that are tailored to your needs.

For example, if you lie in bed at night for hours on end, incapable of sleeping, or wake up a lot during the night, _stimulus control therapy_ or _SCT_, might be for you. This approach works by reserving your bed strictly for sleep. You'll learn more about it in the next blink!

### 4. Your bed should be used for nothing but sleep, and it’s important to get up if you’re having trouble dozing off. 

As you just learned, if you had to sum up stimulus control therapy in a couple of words, they would be "bed = sleep." That means your bed is for sleeping and sleeping alone — well, with one important exception: you can use it for sex, too.

But how exactly do you realize this goal of giving your bed such a limited role?

Well, there are a couple of essential steps. First, you need to refrain from using your bed for anything but sleep in order to avoid counterproductive associations. After all, you don't want your mind to associate your bed with things like worry, stress, reading, paying bills, watching TV, checking Facebook or any other activity or feeling that might prevent you from falling asleep.

Second, you should avoid lying in bed if you aren't sleepy. This is tricky because a casual lie down can be pretty appealing; however, if you struggle to sleep, it's important to only lie down when you mean business.

Only getting into your bed when you're ready to sleep will help strengthen the connection in your mind between the place and the activity. Conversely, neglecting to follow this rule will foster associations between your bed and everything _but_ sleep, thereby making it harder for you to doze off.

And finally, if you wake up at night and can't fall asleep within 20 minutes, get out of bed, go to a different room and do something that's either relaxing or boring. That means steering clear of demanding housework or a midnight jog!

Try doing some knitting, listening to a relaxing audiobook or reading by a dim light without using any screens. Notice how the common theme between these activities is that they can all be immediately paused when the urge to doze strikes.

### 5. Only get back into bed when sleepiness returns and stick to your designated sleep schedule. 

Now you know to get out of bed if you can't sleep — but it's also important to stay out of your bed until the urge to sleep really hits. After all, it's essential to maintain a strong connection between your bed and sleep, and staying away from your mattress when you're not tired is the only way to do so.

So, if you wake up at night, no matter how many times it happens, you need to follow this same practice. Or, if you get back into bed and, 20 minutes later are still awake, you should get up and return to one of the boring and relaxing activities mentioned in the previous blink.

In this sense, there's no difference between not being able to fall asleep in the first place and waking up or being kept awake a second, third or fourth time.

Another key tactic is to wake up at a consistent time every day. In other words, even if you wake up several times in a night, you shouldn't just sleep in to make up the difference. Instead, set a specific time to be in bed and one at which you'll wake up. Failing to adhere to such a schedule will mess up your internal body clock and sleep drive even more.

Just take a 1996 study done by professor Rachel Manber and colleagues, which found that a fixed wake-up time encourages a regular rhythm of wake and sleep, which increases the quality of sleep and reduces daytime fatigue.

### 6. Your thoughts can be your worst enemy when fighting insomnia, but a creative approach can help you overcome them. 

Each hour, the average human has hundreds, even thousands of thoughts. This is certainly impressive, but when it comes to alleviating insomnia, some thoughts just mess everything up.

That's where _cognitive restructuring_ or _CR_ comes in. This strategy helps you identify the _distorted_ and _unhelpful_ thoughts that prevent you from sleeping. In this context, a distorted thought is one that has some truth to it but has been altered slightly.

For instance, take the phrase "I got zero sleep whatsoever last night." While it does indicate how poorly the speaker slept, maybe for just a few hours, it's distorted since she almost certainly did get _some_ sleep.

The other type of difficult thoughts, unhelpful ones — which can be distorted or not — likewise have a negative impact on your ability to get to sleep. For example, you may think "I might fall asleep eventually, but why does it even matter if I'm just going to wake up ten times during the night?"

While they're different in character, both distorted and unhelpful thoughts undermine your attempts to fall asleep. After all, they produce negative feelings like frustration and worry that activate your nervous system, since it senses a problem. In turn, this sends signals to the body, putting it on alert to recover from the stressful situation. It goes without saying that an alert body isn't one that's ready to fall asleep.

However, you can work to modify your negative thoughts, using altered versions to produce emotional and physical responses that limit their negative impact on your sleep. Just imagine you keep thinking that you'll never fall asleep; the negative word in this thought is "never" since it's an absolute concept and is clearly distorted. So, instead of thinking "never," try altering the thought to be "It's not easy for me to fall asleep."

Such an alternative version will prompt alternative responses. In other words, cutting out the distorted and unhelpful aspects of your thoughts will calm the emotional and physical sensations they produce.

Now you know the basics of CR. Next up you'll learn another powerful cognitive strategy for getting better shut-eye.

### 7. Protect your sleep from worries by putting them off for another time. 

What do you imagine confident sleepers think about when going to bed? Well, they probably don't think about very much.

However, if you're the kind of person that is full of worry at bedtime, you might benefit from _designated worry time_ or _DWT_. This cognitive strategy works to alter not _what_ you worry about, but _when_ and _where_ your worries occur.

This is an important distinction, since your worries may be perfectly rational, but the real problem in the context of your sleep is that you think of them at the worst possible time: right when you're lying down for the night. A common explanation for this problem is that people are overloaded during the day, and as a result, when they finally get into bed, all those repressed worries flood back in.

But this paradigm doesn't only apply to worry — it actually works for all different types of thoughts that might disrupt your bedtime, from problem-solving to planning. So, whether you're worried about work, school, laundry, your family or just can't stop thinking about a childhood memory, DWT can help.

To apply this strategy, begin by setting a designated worry time that's more convenient than bedtime. The only rules are that your worrying should be done in one chunk of time that's specifically set aside for this purpose. You can start out with ten minutes and, if that's too short, extend it up to 30. If you find that's too long, just adjust it to meet your personal needs.

The second step is to delay worries encountered outside of your allotted time. This is a crucial step because worries are bound to occur at any time they like, not just at bedtime but also while you're at work or at the gym. If this happens, simply acknowledge the worry and tell yourself that you need to put it off until your specifically designated time.

By applying this strategy, and the others detailed in these blinks, you can start to deal with your insomnia and begin the slow but steady march toward a blissful night of sleep.

### 8. Final summary 

The key message in this book:

**Insomnia can be a thoroughly exhausting condition, and the inability to get a good night's sleep can make you feel hopeless. But there's good news; by applying behavioral and cognitive strategies that tend to your personal situation, you can overcome insomnia and take back your regular, healthy sleeping patterns.**

Actionable advice:

**Don't treat yourself to a nap during the day.**

After a sleepless night, even a concrete floor can look appealing for a quick lie down. But the truth is, napping of any kind will work against you. Instead, consider your fatigue a power that, if kept alive by not napping, will help you fall asleep the following night, taking you one step closer to beating insomnia.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Sleep Revolution_** **by Arianna Huffington**

These blinks are about the importance of a basic human necessity that we often brush aside: sleep. Getting enough sleep isn't just about feeling better in the morning — it improves your work performance, health and even your personal relationships. Similarly, sleep deprivation isn't a by-product of hard work; rather, it _prevents_ you from reaching your full potential. _The Sleep Revolution_ (2016) explains why sleep is so critical, and what you can do to get more of it.

**This is a Blinkist staff pick**

_"These blinks are a sobering exploration of the dangers of sleep deprivation and workaholism. They're especially poignant today, where our studies and careers seem to demand more and more work and less and less sleep. I loved the tip about putting all screens away by 9 p.m. every night."_

– Clare, Editorial Quality Lead at Blinkist
---

### Colleen Ehrnstrom, Alisha L. Brosse

Colleen Ehrnstrom has a PhD in psychology, who currently works at the Department of Veterans' Affairs in Denver, Colorado where she specializes in Acceptance and Commitment Therapy (ACT) as well as Cognitive Behavioral Therapy (CBT).

Alisha L. Brosse also holds a PhD in psychology and is a licensed clinical psychologist who likewise specializes in ACT and CBT. She serves as an associate director of the Robert D. Sutherland Center for the Evaluation and Treatment of Bipolar Disorder at the University of Colorado.

