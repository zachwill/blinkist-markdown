---
id: 5739da7f0366f400033aebd5
slug: ending-aging-en
published_date: 2016-05-18T00:00:00.000+00:00
author: Aubrey de Grey, Ph.D.
title: Ending Aging
subtitle: The Rejuvenation Breakthroughs That Could Reverse Human Aging in Our Lifetime
main_color: 6DAAE3
text_color: 3C5E7D
---

# Ending Aging

_The Rejuvenation Breakthroughs That Could Reverse Human Aging in Our Lifetime_

**Aubrey de Grey, Ph.D.**

_Ending Aging_ (2007) puts forward a fascinating theory about how science may allow human beings to slow the hands of time and, therefore, the process of aging. Learn what happens in our bodies that leads to aging, disease and cancer — and how modern science might be able to put an end to these problems once and for all.

---
### 1. What’s in it for me? Discover what it takes to avoid aging and foil death. 

Most of us have witnessed our grandparents getting old. We've seen how their skin grew paler and thinner, how they suddenly weren't as mobile anymore and how their memory began to deteriorate. Aging is a difficult and delicate matter.

So, who wouldn't want to live forever? Or at least double one's lifespan? Most people would.

Drawing from his own intense wish to end aging — and his scientific knowledge — the author provides a thorough introduction to the seven-step program that he has developed as a means against aging.

In these blinks, you'll discover

  * why free radicals should be avoided if you want to live longer;

  * why graves aren't glowing in the dark; and

  * how to eliminate cancer.

### 2. Aging is a part of every human life, or is it? 

As the saying goes, two things are unavoidable in life: death and taxes. While it's true that there isn't much we can do to keep the government from taking a chunk out of our paycheck, we are becoming better at delaying death.

We've long accepted aging as an unavoidable, though unpleasant, fact of life. Most people don't think there's much sense in worrying about something we have no control over. Why waste time trying to prevent the inevitable? After all, how can you enjoy life if you're worrying about an unsolvable problem?

There are also those who believe that solving the problem of aging could be a bad thing; there are concerns that such a solution would contribute to overpopulation, or that the solution would only be available to the extremely wealthy.

But there are many — the author among them — who believe it _is_ worth regarding the problem of aging as potentially soluble, just like any other ailment. That's why the author has developed a program that aims to stop aging.

This program is called _SENS_ : Strategies for Engineered Negligible Senescence.

In the upcoming blinks, we'll see how this strategy could help prevent _mitochondrial mutations_ by clearing up the junk that plagues human cells. This junk includes disease-promoting proteins called AGEs and death-resistant "zombie cells." We'll also see how these steps can promote healthy cell loss as well as prevent DNA mutation, which leads to aging.

The author's program does face some challenges, however: there are concerns about insufficient funding to properly develop the technology and conduct the research that is needed to stop people from aging.

But if funding does come through, he predicts — with a 50 percent certainty — that it will be possible for us to stop people from dying of old age.

Most scientists agree that aging is inevitable. However, the bodily damage that causes aging may be reparable.

> _"There is no ticking time bomb — just the accumulation of damage."_

### 3. Normally, prevention is better than cure. Preventing aging, however, might be too complex. 

When we're presented with a problem, there are generally two paths to explore when looking for a solution: prevention or cure. In the field of medicine, most problems are solved by offering a cure rather than prevention. This approach, however, has major drawbacks.

For example, if someone has heart disease or diabetes, they can obtain medication to help cure their ailments without ever finding out what caused the problem in the first place.

But prevention isn't all that straightforward, either.

If you want to prevent an ailment, you need to pinpoint its cause. This can be quite complicated; all too often, there are many factors, each contributing to the main problem.

And this is the problem with preventing aging: there are so many influences, from cell damage to food consumption, that cause the body to age.

So, if prevention is uncertain, and a cure is only a theoretical possibility, the next best thing to focus on is repair. We can see the benefits of this approach by considering the treatment of a 40-year-old.

First, how would prevention work? In this scenario, let's say we could cut the speed of aging in half. That means our 40-year-old, expected to live to the age of 80, would live an additional 40 years and reach 120 years of age. Although this doubles the remaining lifespan, the total lifespan only increases by 33 percent.

Now, let's look at the other scenario: repair. We could cut the accumulated damage of a 40-year-old in half, providing a therapeutic treatment he could follow for the rest of his life. So, by the age of 80, he may have only accumulated the damage of a 50-year-old.

Following this treatment, we could increase his remaining lifespan by four or five times, approximately doubling the total lifespan. Instead of 80 years, he'd live 160.

As we'll see, repairing the damage in one's body is exactly what the author proposes, and it all starts with fixing mitochondrial mutation.

> _"Prevention-oriented approaches simply don't aim high enough."_

### 4. Mitochondrial mutation plays a huge part in aging and there might be ways to fix that. 

So, what exactly are mitochondria? They're commonly referred to as the "power plants of the cells," because they produce the energy we need to live. But you only need to learn a little about power plants to know that energy production usually comes with nasty side effects.

In our bodies, one of these is _free radicals_.

If you've ever heard of free radicals, you probably know that they are bad for our health. But what you may not know is that most free radicals are actually produced within our own cells.

In fact, free radicals are molecules that are based on oxygen but missing one electron. Having the right amount of electrons would result in a stable substance, but as soon as one is missing, the molecule becomes highly reactive. A free radical stays reactive until it finds a new electron, which it usually pulls from the nearest stable molecule, leading to a chain reaction.

Although free radicals can be the result of pollutants or toxins that come from your diet, the majority are actually produced in your mitochondria. They are harmful because these chain reactions result in mutations and damage in your mitochondrial DNA, which contributes to aging.

So, how do we stop this damage from happening? Well, many solutions have been proposed, the most promising of which is _allotopic expression_.

Allotopic expression is a form of gene therapy that involves keeping a backup of our mitochondrial DNA in the protective nucleus of our cells, protecting our genes from the constant exposure to free radicals.

And since less than one percent of our cells are influenced by genetic mutation, the likelihood that that stored gene would already contain a mutation is low.

So, there's one possible solution to the problem of mitochondrial damage. In the next blink, we'll take a look at the junk — both in and outside of our cells — that is another contributor to aging.

> _"Free radicals are a part of being alive."_

### 5. The junk found both in and outside of cells is another treatable contributor to aging. 

Just like a household, cells produce waste. And although cells recycle this waste in a better and more efficient way than most households, there is still leftover junk that can cause damage.

Part of this waste product is called _lipofuscin_, which the cells recycle via _lysosomes_.

Unfortunately, lipofuscin is something that can't be properly disposed of. Lysosomes are unable to get rid of all the lipofuscin that accumulates in the cells, which leads to aging and diseases such as _arteriosclerosis_, the buildup of plaque in arteries.

One possible solution to this problem is as crazy as it is simple, and it can be found in our graveyards.

Since lipofuscin accumulates right up until death, graveyards must be full of it. Furthermore, lipofuscin is fluorescent, but since we don't tend to see anything glowing in the dark in our graveyards, there must be something that is breaking lipofuscin down.

And there is: microbes. Microbes in the soil break down the lipofuscin into a usable material, and although there are hurdles to overcome, introducing these same microbes into the body could be a way to process the junk inside our cells that cause aging.

But along with the junk inside our cells, there is junk accumulating outside of our cells called _amyloids_, most of which consist of damaged proteins. As we age, these amyloids can accumulate in cells around the brain, leading to Alzheimer's disease.

The author also has a suggestion for getting rid of this outside junk: vaccination.

There's evidence that our brain's immune system can get rid of amyloids, but it does so at a very slow pace. A vaccination could increase the speed of this process by stopping the accumulation of junk outside of our cells and slowing down the rate at which the brain ages.

But there's more to repairing our bodies than just clearing out the junk. In the next blink, we'll learn about other ways we could keep our bodies young.

### 6. By changing processes in our body we could stop cell loss and mutation, the main causes of cancer. 

A central cause of aging is the gradual loss of cells. So it only makes sense that creating new ones would be a great way to fix this problem. And while there is a way to do this, politics and questions of morality are standing in the way.

For instance, one method involves _stem cells_. While we all contain adult stem cells _,_ only _embryonic stems cells_ can be used to produce new cells in our body. In fact, embryonic stem cells can be turned into any cell, including heart, lung and muscle cells.

The problem is that embryonic stem cells are only available in early-stage embryos.

Therefore, these methods are being hotly debated, especially in the United States. Most of this debate revolves around the moral question of when life begins and whether it is appropriate to medically use embryos.

But one thing everyone can agree on is that aging comes with many concerns — cancer chief among them, which begins when our DNA mutates with age.

The DNA contained in a cell's nucleus is like a blueprint for our entire biological design. But this can get damaged through UV rays, free radicals or other environmental toxins, leading to our DNA providing flawed and cancerous instructions.

These flaws can be replicated when cells divide, causing the cancer to spread.

So one possible solution to eliminating cancer is to get rid of the _telomerase_ gene. A telomere is a protective cap that sits on each end of a chromosome; when the cell naturally divides, the caps become shortened. However, the telomerase gene re-lengthens these caps, preventing them from becoming too short and, therefore, being able to die.

Since cancer is caused by the uncontrolled replication of cells, the removal of the telomerase gene could prevent this re-lengthening process from happening, which would allow dangerous cells to simply die off after enough divisions.

Deleting telomerase would put an expiration date on all of our body's cells, but this problem could be solved through stem cell therapy.

### 7. AGE’s and “zombie cells” could be treated with drugs or enzymes to stop their aging. 

If you've ever eaten meat that has been browned or blackened, then chances are you've eaten _advanced glycation endproducts (_ AGEs _)_ as well. But even if you haven't, the same process that blackened the meat is also happening naturally in your body.

AGEs are the result of a complex chemical process where sugars and other substances bind to proteins. These AGEs accumulate in your cells, decreasing their functionality, and can lead to disease and early death.

Several tests and studies have shown that there is no way to prevent the build-up of AGEs, so the author suggests using a drug to clear them out after they've built up.

One such drug, _alagebrium_, has shown positive results in animal tests. Unfortunately for humans, however, the benefits of the drug don't outweigh the negative side effects. The author hopes that, in the future, scientists will discover and create more drugs designed to break up AGEs.

As we get older, we also accumulate more "death-resistant" cells, called "_zombie cells_."

Our body can shut down most of our cells in order to prevent disease and cancer. But zombie cells don't remain "dead," they continue to cause problems, becoming toxic, damaging the surrounding cells and thereby aging us.

One possible way to eliminate these zombie cells is to stop them from dying in the first place. It might seem counter-intuitive, but we could do this by using telomerase to lengthen their telomeres, keeping them alive. This has been achieved in lab trials; however, this method always carries the risk of causing cancer, as we saw in the previous blink.

Another method for getting rid of zombie cells is to use gene therapy to create a _suicide gene_ that targets and destroys the cell.

As we'll see in the next blink, many of these therapies come with risks, but that doesn't mean they're not worth taking.

### 8. The current therapies for humans aren’t perfect for humans, but they are a good starting point. 

As we've seen, many of the first steps toward fighting aging have already been taken. But despite this fact, those at work on this issue are still in the minority.

This is why the author has come up with a plan to convince the majority. It's called the _robust mouse rejuvenation_ (RMR).

The author will use his proposed techniques on 20 mice of the species _Mus musculus_. He expects to find successful results after treating the mice and expanding their lifespan from an average of three years to an average of five years. He also plans to start the treatment when the mice are at least two years of age — when they'll have already accumulated the damage that comes from having lived over half their expected lifespan.

The author also hopes that by declaring war on aging, we can learn to accept the possible deaths that come with such advancements.

We've seen this happen before. For example, in 1999, a teenager undergoing gene therapy died of anaphylactic shock. This incident put gene therapy in a bad light, resulting in a year-long suspension of trails which set progress back and likely caused more deaths.

The author suggests that delays such as this, as well as the slow approval of drugs, do little to prevent casualties. In fact, the author believes that the current ratio of lives lost while awaiting drug approvals outweighs the lives lost through unapproved drugs by ten to one.

Hopefully, after the success of RMR, drugs will be quickly approved in order prevent more time being wasted, and more needless aging from occurring.

It's always possible that people could die as a result of treatment. The author hopes that shifting the aforementioned ratio to two to one will convince people that progress is being made, and help them accept the change.

It is possible: in a few decades from now, aging, which was considered unavoidable for thousands of years, will no longer be an inevitable fact of life.

> _"The first priority — even justifying considerable loss of life in the short term — is to end the slaughter as soon as possible."_

### 9. Final summary 

The key message in this book:

**Aging should no longer be considered inevitable. There are modern technologies and therapies that may allow us to stop the accumulation of damage within our bodies, which is the main contributor to aging. Furthermore, these techniques could also reduce the risk of cancer and several other fatal diseases.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _100+_** **by Sonia Arrison**

_100+_ explains the science and technology that will help us lead longer and healthier lives, and considers how society will handle a rapidly aging population. The implications of the forthcoming demographic shift are massive, and big changes lie ahead. _100+_ became a best seller and one of the _Financial Times_ ' best books of 2012.
---

### Aubrey de Grey, Ph.D.

Aubrey de Grey, PhD, is chief science officer and chairman of the Methuselah Foundation. As a biomedical gerontologist, he is one of the most active scientists in the fight to stop aging. He developed the SENS program to stop aging. Currently, he organizes conferences and workshops to promote further research.

Michael Rae is Dr. de Grey's research assistant. He was a board member of the Calorie Restriction Society and a major contributor to their how-to guide. He is also assisted with a partner study that tries to determine how calorie restriction might be feasible for humans.

