---
id: 53c63e723839340007fe0000
slug: energy-myths-and-realities-en
published_date: 2014-07-15T00:00:00.000+00:00
author: Vaclav Smil
title: Energy Myths and Realities
subtitle: Bringing Science to the Energy Policy Debate
main_color: 80CC57
text_color: 40662B
---

# Energy Myths and Realities

_Bringing Science to the Energy Policy Debate_

**Vaclav Smil**

These blinks provide an objective, science-based look into the global energy debate that is so often dominated by the misleading rhetoric of politicians, industry leaders and activists.

---
### 1. What’s in it for me: Get a sober view on energy politics. 

Are oil wells running dry? Will your neighborhood gas station soon only sell biofuel? Will the world be powered by giant wind farms? Can we stop global warming with carbon sequestration?

Depending on who you listen to, all four scenarios are either impossible or just around the corner.

These polar standpoints are what makes the global energy debate often so confusing: politicians, environmental groups and corporations are all shamelessly promoting their own agendas so it's hard to know what's fact and what's fiction.

In these blinks, you'll find several myths about global energy examined critically and scientifically.

You'll find out:

  * why civilization won't end anytime soon due to a lack of crude oil,

  * why adopting biofuels on a large scale would mean choosing between fuel and food, and

  * why you wouldn't want a carbon sequestration site in your backyard.

### 2. Civilization is not nearing its end. Peak oil theories claiming so are unsupported by evidence. 

In the 1990s, a group of retired geologists, in conjunction with the scientist Richard Duncan put forth a radical new theory known as _peak_ _oil_.

According to this theory, humanity was depleting the world's oil supplies at such a rate that a drastic shortage of oil was imminent. They also argued that the end of the oil era would be a turning point for humanity, prompting the end of industrial civilization as soon as 2025!

But in fact, these claims are completely unfounded and unjustified:

A big part of the problem in peak oil theories is that they are overly focused on the supply side of the equation, meaning global oil production, neglecting the demand side and how it is influenced by oil prices. They believe that progressively lower oil production we've seen over the years is indicative of a physical shortage of oil, when in fact it's merely due to a decrease in demand. This phenomenon was evident after the oil price hikes of 1978 and 2004, for example.

What's more, our future need for oil will likely decrease thanks to our increasing use of alternative fuels as well as technological innovations, efficiency improvements and better resource management.

So while humanity will eventually transition away from oil due to depleted supplies and the rising cost of its extraction, our reduction in crude oil usage will not mean the end of civilization, rather just a gradual transition to something else.

Peak oil theories are also unjustified in claiming to know how much oil is left in the ground, but really the amount of this _estimated_ _ultimately_ _recoverable_ _(EUR)_ _oil_ remains uncertain.

Recent assessments show there are still significant untapped oil resources, and global EUR oil is estimated at 400 billion barrels — almost triple what most peak oil theories claim. And this figure does not even include unconventional oil reserves like tar sands or bitumen, which further add to the potential reserves.

### 3. Carbon sequestration is a costly, inefficient and risky way to tackle global warming. 

Recently, there's been a great deal of debate about how to counteract the global warming effect caused by excessive emissions of CO2 and other greenhouse gases.

As China and many developing countries increase their energy consumption, it is clear that global CO2 emissions will rise significantly over the next decades.

Some experts have proposed so-called _carbon_ _sequestration_ as the solution: capturing and storing greenhouse gases so they don't affect the atmosphere.

But is this really a valid solution? No, it is costly and inefficient.

This method cannot remove enough CO2 from the atmosphere to even noticeably slow down the buildup of the gas.

What's more, artificial carbon sequestration would demand setting up approximately 160,000 CO2 capture towers, also known as "artificial trees," which would be very costly, not to mention the added expense of compressing, transporting and storing the gas..

Of course, plants are continuously sequestering carbon, as they inhale CO2. But to intervene in global warming with plant-driven carbon sequestration would demand a lot of land, resources and time for the plants to grow — 40 to 80 years.

What's more, storing the sequestered carbon is also difficult and risky. The acidity of the carbon could well eat away at the storage facility, eventually allowing the carbon to leak out again.

And at the moment, storage capacity only exists for a quarter of annual global carbon output, so many more storage sites would have to be built for large-scale sequestration. Herein is the problem that people don't want carbon storage sites near their homes because the sites are inherently hazardous: toxic metals could seep into the drinking water supply due to the stored carbon's high acidity.

### 4. Replacing crude oil with biofuel is neither feasible nor efficient. 

Lately, there's been a lot of buzz about _ethanol_ produced from plants as a perfect green energy source. Its proponents say it would make us independent from crude oil, lower our carbon emission and stabilize incomes for grain farmers, who would provide the raw material for its production.

Unfortunately, it's not so simple.

Producing biofuel from plants is costly and environmentally damaging.

It requires vast amounts of land to grow the crops. For example, if we were to use sugar cane as a source for biofuel to replace traditional fuel, we would need to marshall almost 40 percent of the world's cultivated land for this purpose alone.

What's more, by 2050 the global population will reach nine billion, and we simply can't devote so much land to producing biofuel when people also need to grow food crops to eat.

Nor can we clear more land for the purpose, since deforestation is already a problem, and clearing forest on such a massive scale would have severe climatological consequences.

Also, biofuel does not represent a sensible solution in many oil-based vehicles.

For example, in the US, many cars have such dismal fuel efficiency that from an environmental perspective, it would make more sense to improve this factor than to convert them to run on biofuel.

Also, many of today's road vehicles, ships and airplanes are built to use refined oil products that biofuel cannot replace.

Therefore before any kind of large-scale biofuel production is implemented, the overall transport system should be improved and optimized to address these issues.

### 5. Wind energy is too difficult to harness for it to power the world. 

If you've ever been caught outside in a storm, you know the wind has incredible power.

Indeed, simulations show that in terms of energy potential, winds at the altitude of 100 meters could generate as much as 78 terawatts globally.

In the US alone, wind energy potential is almost twenty times greater than all the electricity the country generates now.

Sound good?

Unfortunately, a closer look reveals that wind energy doesn't have what it takes to be a dominant energy source, 25 years from now.

Why?

First of all, the energy potential of the most powerful winds is difficult to harness because they blow in the jetstream, some eleven kilometers above the earth. Their location also tends to shift depending on the seasons, so utilizing them would be very inconvenient: it would require deploying massive amounts of flying generators tethered to the earth with aluminum lines.

Second, the traditional wind farms also have their problems: they require large amounts of land because the turbines need to be spaced apart from each other, and this means they produce relatively little power per square kilometer.

What's more, locals may object to wind farms due to their appearance, feeling that the ugly things spoil their view of the countryside or waterfront. And turbines are also loud and pose a threat to local bird and bat populations.

It is for these reasons that in 2007, only about 1.25 percent of the world's energy was produced by wind turbines.

Third, wind energy is dependent on the wind, which is notoriously fickle.

Wind speeds vary depending on the time of year and geographic location, meaning that if wind was the world's predominant power source, a vast network of intercontinental high-voltage transmission lines would be needed to even out the shifting imbalances in global production. The wind fluctuations would create great price fluctuations.

### 6. Be patient: any new energy innovation will take decades to be adopted. 

As you are probably beginning to see, when it comes to energy policy, there is no silver bullet, no easy and quick solution. Patience and careful analysis are needed. Remember these few rules of thumb when someone claims that the world's energy landscape will change radically very quickly:

First, don't underestimate how long the conventional energy sources will linger. Shifting to new kinds of fuels or energy production methods is always a gradual process. Analysis has shown that established energy supply patterns can persist over generations. So rather than rushing to embrace a new energy source like biofuel on a large scale, we should first focus on optimizing efficiency in the production and use of the incumbent sources.

Second, be distrustful when someone claims a new energy source will be adopted very rapidly and enthusiastically on a global level. Even energy technologies that are clearly superior will be adopted only gradually.

What's more, various players in the energy debate have their own agendas, and therefore often deliberately use misleading arguments or misinterpret data from scientific studies to bolster their claim that something will happen quickly.

And even if you find an objective source, it's impossible to forecast in detail just how people and industry will convert to a new energy source. There are always unforeseen setbacks and obstacles.

Third, remember that any major adoption of a new energy innovation will probably require extensive and very expensive changes to the infrastructure. These changes demand large investments as well as overcoming legal and environmental issues, which takes time.

### 7. Energy policy decisions should be objective, with a premium placed on avoiding environmental damage. 

As humanity debates how to power our globe for the next decades, it would do well to keep in mind these three factors:

First, there are many competing ideologies and interests at play, but energy policy decisions must be made based on a rational and objective cost-benefit analysis.

For example, at the moment the oil and gas industry is lobbying powerfully for carbon sequestration as the answer to global warming, but they are hardly objective experts: the ability to store carbon would be a new revenue stream for them, while at the same time making them look less responsible for global warming.

Second, remember that there are vast regional differences in terms of energy demand and production. For example, it may be that some energy innovations are easier to implement in developing countries than in developed ones, because the latter already have fully fledged economies running on fossil fuels, and people used to having cheap power to charge their iPads.

Third, all decisions should adhere to the maxim that avoiding or minimizing environmental damage is always better than trying to neutralize it afterwards.

Case in point: In 2005 and 2006, biofuels were promoted grandiosely, only for later analysis to reveal how harmful their production is for the environment.

Meanwhile, instead of fixating on ways to sequester the carbon already in the atmosphere, affluent nations should focus on managing their own energy use so they become less dependent on fossil fuels and can reduce their greenhouse gas emissions.

### 8. Final summary 

Key message in this book:

**Civilization** **as** **we** **know** **it** **will** **not** **end** **due** **to** **oil** **wells** **going** **dry.** **Carbon** **sequestration** **will** **not** **magically** **stop** **global** **warming.** **Wind** **energy** **and** **biofuels** **are** **not** **the** **energy** **sources** **of** **the** **future.** **In** **fact,** **likely** **any** **change** **in** **global** **energy** **production** **and** **consumption** **will** **happen** **very** **slowly,** **and** **therefore** **the** **decisions** **we** **make** **about** **energy** **should** **only** **be** **founded** **on** **rational,** **patient** **analysis.**

**Suggested further reading: _This Changes Everything_ by Naomi Klein**

_This Changes Everythin_ g addresses one of the most pressing issues today: climate change. The book outlines exactly how we're harming the planet and why we've thus far failed to stem our destructive behavior. Author and activist Naomi Klein also points out how some early movements are meaningfully fighting climate change and what more needs to be done to prevent global disaster.
---

### Vaclav Smil

Vaclav Smil is one of the world's leading experts on global energy issues. In 2010, he was named one of the Top 100 Global Thinkers by _Foreign_ _Policy_ magazine.

