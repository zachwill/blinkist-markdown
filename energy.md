---
id: 55bf57c063646100070f0000
slug: energy-en
published_date: 2015-08-03T00:00:00.000+00:00
author: Vaclav Smil
title: Energy
subtitle: A Beginner's Guide
main_color: 23A7B1
text_color: 156369
---

# Energy

_A Beginner's Guide_

**Vaclav Smil**

_Energy_ (2006) offers insights into one of the most elusive concepts in the spectrum of human thought: energy. By understanding what energy is, how it has helped us get where we are today, and what dangers our reliance on certain forms of energy poses, we will be better equipped to handle the challenges faced by modern civilization.

---
### 1. What’s in it for me? Discover what energy really is and how it affects everything around you. 

What do you think about when you hear the word "energy"? Is it what you feel when you wake up after a good night's sleep? Is it what you get when you eat a chocolate bar? Or are you reminded of physics classes and the laws of thermodynamics?

Whether humans were inventing more efficient hunting tools, using oxen to till fields or learning how to burn fossil fuels, the discovery of new ways of harnessing energy has always been at the heart of our evolution as a species. It has allowed us to do things ever more effectively and to continually improve our technology.

But there's a downside to this story. We have now reached a point where our demand for energy poses serious threats to the environment. Luckily, as these blinks will show you, there are potential alternative energy sources that could help us meet the ever rising demand for energy in a sustainable way.

In these blinks, you'll learn

  * what energy really is;

  * how energy helped us build better societies; and

  * why leopards can run faster than wolves.

### 2. The concept of energy has developed over centuries. 

Looking at the way we use the word "energy," it's clear that most of us aren't entirely sure what energy actually is. For example, we say that charismatic speakers "energize the crowd," or that we feel more energetic after rigorous training for our upcoming half-marathon.

Yet, these are figurative at best. In fact, it's impossible for us to have _more_ energy after spending hours exerting ourselves. So if it's not science or physics, what informs our conception of energy?

In essence, when we talk about "energy," we're using a broad term that encompasses various types of energies. The term stems from the Greek word _energeia_, coined by Aristotle, which signifies motion, action or work. Today, energy is usually thought of as the capacity to do _work_, i.e., our ability to affect change.

This kind of work could be as simple as sitting in a room and contemplating the cosmos. Though you may seem inert, your body is indeed working — your heart pumps blood, your stomach digests food, etc. And that takes energy.

The first law of thermodynamics, which states that energy can neither be created nor destroyed, demonstrates that "energy" is often just an abstract concept that describes the conversion of more varied energies.

To illustrate this, try rubbing your hands together very quickly. You'll notice that they get warm. This isn't because your hands have become "energized." Rather, as you rub your hands together, kinetic energy (movement) is _converted_ to thermal energy (heat), thus causing them to feel warm.

Mankind — including scientists such as Joules, Watt and Einstein — has been poring over the essence of energy for centuries. Over the years, scientists have created systems to measure these seemingly mystical energies — such as the International System of Units — and have used them to gain a better understanding of how our world works.

Energy is indeed a complex concept. Over the course of these blinks, we'll unravel the mystery and show you the role that energy plays for all living things on our planet.

### 3. Geothermal energy and solar radiation shape the planet itself. 

Despite the immense size of our universe, we have yet to discover extraterrestrial life. In part, this is because virtually no other planet is like our own. Indeed, the Earth possesses a set of unique properties that make our planet habitable, all of which owe their existence to energy.

The first of these energies is solar radiation. The Earth orbits the sun at a perfect distance, where our atmosphere is able to protect us from intense solar radiation, but still allows enough of it through to foster the extremely rare combination of conditions that enable life, like our temperature range that allows the presence of both liquid and solid water.

We can see the influence of solar radiation most easily in our climate. When solar radiation reaches the Earth, much of it is absorbed by the atmosphere's greenhouse effect. By trapping this radiation in the ozone, the Earth's climate has remained relatively stable for millions of years.

However, an increase in human activity is turning this environmental boon into something much more dangerous. Today, as a result of increased CO2 emissions, more and more solar radiation is trapped inside the atmosphere, slowly increasing the Earth's temperature. As the Earth continues to heat up, we may face terrible consequences, like drought, desertification, melting ice caps and coastal flooding.

Geothermal energy has also played a role in shaping our planet. Deep within the Earth is its slowly cooling molten iron core. The energy released by the core shapes the Earth by transforming the ocean floor and moving the continents upon which we live.

Geothermal energy is immensely powerful, and causes some of the most violent displays of the power of nature that we can experience: volcanic eruption, tsunamis and earthquakes are all caused by the Earth's geothermal energy.

### 4. Energy plays a huge role in the survival of all organisms. 

Whether it's a tree in the Amazon, a lion in Africa or the bacteria in your stomach, they all need one thing to keep their systems functioning: energy.

Organisms can convert energy in our biosphere in many different ways, each of which influences the shape of the ecosystem.

One way that organisms can convert energy is through photosynthesis, the fairly inefficient process by which plants convert solar radiation into energy they can use to grow.

Of course, plants aren't the only organisms that need to convert energy. For instance, with the help of bacteria that transform sugar into lactic acids, animals and humans are able to metabolize complex organic compounds — like the plant leaves created by photosynthesis — and get energy from them.

However, the further removed a living organism is from the Earth's primary source of energy, solar radiation, the less energy is available to them.  That's why in a complex ecosystem, more herbivores will exist than carnivores or omnivores, as they are closer to that photosynthesis energy: they only have to eat vegetation, and not other animals that have eaten vegetation beforehand!

But organisms don't use energy equally. In fact, the way they use energy depends on their stage of life.

If you've ever had a kitten or a puppy, then you know how much energy they need to grow. Although adult animals still require energy in the form of food and water, they can survive and function on much less than their young. While they need to hunt and move around, their bodies no longer need constant energy supplies to continue growing.

Once grown, organisms use the energy from food or water to keep the body functional, e.g., by keeping the heart pumping and the lungs breathing.

Organisms will also use their energy in different ways depending on their "design."

Cheetahs, for example, have special muscle structures that allow them to use energy quickly, thus accounting for their high running speeds. Wolves, on the other hand, while not as fast, can run much farther while using less energy.

But what about humans? How has energy influenced the way that we've developed over the millennia?

### 5. Energy has played an integral role in the development of human societies. 

Humans are quite unique in the way that we have obtained energy over the millennia. The most rudimentary developments in harnessing new energy came in the earliest human communities.

Foraging hunter-gatherer societies sustained themselves on primarily vegetarian diets. The energy they gained from nuts and seeds was much easier to obtain than that from hunting small game. When they needed to hunt, they would choose large prey like gazelles or deer that, if killed, would provide the energy these societies needed for weeks.

It makes sense, then, that the first human settlements were usually near the sea, where people could fish for a steady source of easily obtainable energy to supply the needs of their families and communities.

Later in human history people began to employ tools and animal power, allowing them to develop the first rudimentary civilizations. Powered by the first domesticated animals, humans developed what we now know as traditional agriculture.

This animal power allowed people to work larger areas of land than they otherwise could with their own muscles. This enabled them to build larger settlements, and human inventiveness quickly led to the development of water mills, smelting and small cities.

But people weren't just settling, they were also traveling and trading. The first ships allowed farmers from the countryside to export their goods to the cities, which were in constant need of food supplies.

People eventually began to harness the energy of the elements with the invention of machines like waterwheels and windmills. These inventions gave societies access to wind and water energy, which likewise improved their production. For example, millers could now grind their grains much faster and with much less of their own energy.

Ultimately, the use of animal power and the first machines allowed humanity to concentrate their communities, giving rise to the first real civilizations.

### 6. The types of energy on which we are most dependent are not sustainable. 

Every civilization depends on solar radiation and the energy it provides, whether it's the photosynthetic production of food or the burning of fossil fuel. That was no less true for the Ancient Greeks than it is for our modern world.

Today, we depend on fossilized stores of solar energy like coal, crude oils, and gas. And we increasingly rely on this energy for electricity.

Crude oils or natural gas are types of energy that were formed over millions of years from the remains of plant and animal life, and today we burn through them at too fast a rate for them to replenish themselves.

We use more electricity by the day, and we acquire it by burning fossil fuels, installing solar panels or wind turbines, or by harnessing nuclear or geothermal energy. 

This kind of energy consumption has helped us develop in many important ways. For example, fossil fuels have enabled us to travel and ship goods across the globe, and have created entirely new forms of communication. In addition, fossil fuels have allowed us to urbanize and build civilizations with a high life expectancy and many wonderful opportunities.

While these technological advances have indeed helped many, they also bring a number of disadvantages. Access to technology and energy resources is often limited, making it hard for struggling nations to keep up with the standards of living enjoyed by the rich. 

Moreover, some technologies, such as nuclear weapons, would threaten the existence of the entire human race if they were used indiscriminately or fell into the wrong hands.

Finally, our unsustainable approach to energy consumption is changing our delicate climate. More than ever, we need to focus on new ways to obtain and consume energy so that we don't destroy the very source of our existence.

### 7. Our use of energy affects all elements of our everyday life. 

While many people are rightfully worried about the way human energy consumption affects our climate, we still have to live everyday lives that are very much reliant on energy.

Indeed, energy is essential to modern life. Household energy is claiming an increasing share of our global electricity consumption, with devices like TVs, mobile phones, iPods or electric stoves being integral parts of modern homes all over the world.

Obviously, powering these devices takes energy. But there's more to it than that. These devices also need to be produced and shipped worldwide, and that takes energy.

Furthermore, technologies that have increased our mobility likewise increase our energy consumption.

We can now travel in one day to any place in the world as long as it has an airport. While this has expanded our ability to connect on a global scale, it requires enormous amounts of energy.

As the demand for energy continues to grow, we need to take into account where the energy we use actually comes from. International distribution systems mean that the oil and gas that you burn for electricity or heat rarely stems from your home country. For example, if you live in Europe, it's likely that your gas came from nearby Russia, not an EU member state.

In addition, nearly 80 percent of crude oil is transported from the Middle East, Africa, Russia and Latin America to Western Europe, the United States and Japan. Conflicts between these nations can lead to severe consequences, such as acute energy shortages for huge parts of the population.

This ever-rising demand for energy is a problem — one that we need to face now so we can improve our chances of survival in the future.

### 8. We need to find sustainable ways to deal with the consequences of our energy demands. 

Unfortunately, it's impossible to accurately forecast the future of energy. While many have tried in the past, no one could have predicted events like the Iraq War or China's rapidly developing economy — events that have significantly altered our energy-consumption trajectory.

One thing is certain, however: demand will continue to rise, especially in rapidly developing economies like China. So how do we manage this growth in consumption?

For the time being we will remain dependent on fossil fuels.

The problem with renewable energy sources, like wind, water or solar — at least for the moment — is that they won't be able to sustain the growing needs of the energy market. So while we can work toward new solutions, we'll need to stick with older methods, such as fossil fuel consumption, for a little while longer if we want to provide for everyone's energy needs.

Nuclear energy will also be part of our energy plan for a long time. While contentious, nuclear energy is the cleanest way we have of obtaining energy. Dangers still exist, of course. Potential terrorist attacks on nuclear reactors and the safe disposal of radioactive substances are problems that we still face, but scientists have made vast safety improvements in recent years.

What really needs to improve is public acceptance of nuclear power.

In addition, it's possible that the future will reveal new methods of converting energy from biomass and water.

Past civilizations used biomass when they did things like burning wood for heat and cooking. But converting these energies is highly inefficient, and unless we find ways to efficiently obtain energy from biomass, there's no chance they can help us in the long run.

Another option could be hydroenergy, i.e., energy created by the flow of water. This very natural and largely untapped method of obtaining energy could be a boon to our global civilization.

History has shown that humanity can adapt to most changes. Now we just need to adapt to our ever-growing hunger for energy!

### 9. Final summary 

The key message in this book:

**Energy plays an integral part in everything from the existence of life on Earth to the ways societies organize themselves. The ever-growing demand for energy, driven by a rising population, is one of today's most pressing challenges. Luckily there are ways forward.**

**Suggested** **further** **reading:** ** _Energy Myths and Realities_** **by Vaclav Smil**

These blinks provide an objective, science-based look into the global energy debate that is so often dominated by the misleading rhetoric of politicians, industry leaders and activists.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Vaclav Smil

Vaclav Smil is a professor emeritus at the University of Manitoba, and has published over 400 papers and 35 books, including _Making the Modern World_ and _Should We Eat Meat_. Alongside his extensive academic career, he has worked as a consultant for several different US, EU and international institutions.

