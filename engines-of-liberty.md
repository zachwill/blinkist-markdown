---
id: 581df9d97882c20003e52c6d
slug: engines-of-liberty-en
published_date: 2016-11-08T00:00:00.000+00:00
author: David Cole
title: Engines of Liberty
subtitle: The Power of Citizen Activists to Make Constitutional Law
main_color: 58C3D6
text_color: 2E6670
---

# Engines of Liberty

_The Power of Citizen Activists to Make Constitutional Law_

**David Cole**

_Engines of Liberty_ (2016) is an exploration into the influence citizens can have on government, and the changes that can be brought about through activism, the spreading of information and the mobilization of one's peers. When it comes to the big issues of our time, like gay marriage, guns and human rights, it's passionate citizens who are speaking up for what they believe in and bringing about change.

---
### 1. What’s in it for me? Renew your belief in changing society. 

It is often said that one man can change the course of history. Presidents like Washington, Lincoln and the two Roosevelts have undoubtedly put their mark on American history — but, great as they were, their achievements weren't realized single-handedly. Something — or someone — else also helped push that change.

Washington was one of the founding fathers, but it's important to remember that this was a group — a collection of what we would call political activists who initiated the revolution that created a new, powerful nation. And, since the founding fathers, it's been civil activists who've driven significant change in the United States. So, let's take a look at some of the accomplishments these engines of liberty have made in the last decades.

In these blinks, you'll find out

  * how a widowed gay man's activism made gay marriage legal in the United States;

  * why Florida is the first state for pro-gun campaigns; and

  * how two lawyers revealed the reality of Abu Ghraib prison through legal activism.

### 2. A dispute over child custody led gay rights activists to win the right to marry in the state of Vermont. 

Civil rights activists were cheering in 2015 when gay marriage was made legal in the United States, but this landmark decision didn't happen overnight. To find out how we got there, we must go back in time and look to the small state of Vermont.

Surprisingly, the issue of gay marriage in Vermont actually began as a child custody case.

Susan Bellemare and Susan Hamilton lived together in Vermont as parents to Hamilton's biological son, Collin, who was fifteen months old. But, in 1989, tragedy struck when Hamilton died in a fatal car accident.

After the accident, Hamilton's parents didn't like the idea of Collin continuing to live with Susan Bellemare, so they filed a lawsuit to gain custody of their grandchild.

It took two years of litigation, but Bellemare won the case thanks to a will that Hamilton had prepared, stating that her partner should continue to raise Collin in the event of her death. Without this document, it's likely that Bellemare would have lost custody of her son.

This case made it clear that gay partners had very few rights in the eyes of the law, and it spurred a group of activists to begin campaigning for change.

Among them were three couples who sued the State of Vermont for refusing to grant them marriage licenses.

These couples were well prepared, and they defied all the negative gay stereotypes. They had respectable jobs; they were in steady, long-term relationships; and two of the couples were raising children.

It took years of political lobbying and court battles, but in December of 1999, the Supreme Court of Vermont made its decision: According to the state constitution, civil unions between a same-sex couple must be treated the same as any other union.

But it didn't end there. Activists continued to lobby and, in 2009, the Vermont House of Representatives voted on the issue of gay marriage. It was a narrow victory, but same-sex marriage was legalized in the state.

### 3. California’s battle for gay marriage led to fierce activism, and wins and losses on both sides of the debate. 

Of course, Vermont isn't the only state with activists fighting for civil rights. In California, the debate over gay marriage was sparked in 2004, when the mayor of San Francisco, Gavin Newsom, began issuing marriage licenses to same-sex couples, even though they weren't legally recognized.

Years of activism followed in order to make these marriages valid. Even though California has long been a haven for gay people, the issue proved difficult to resolve.

The first decision came in June of 2008, when the California Supreme Court approved the legality of gay marriage. However, a few months later, a conservative group gathered signatures in order to propose an amendment to the state's constitution that limited marriage to unions between a man and a woman.

After getting the required signatures, this bill became known as Proposition 8. And it was highly disputed before it could be voted on by California residents, with supporters and opponents each spending $40 million on their campaigns.

One particular television ad ended up tipping the balance in favor of conservatives: It featured a young child telling her parents what she'd learned at school that day — that she can grow up to marry a woman.

Ads like this were especially upsetting for some voters and prompted them to question whether gay marriage would impact the values being taught to school children. As a result, Proposition 8 passed with 52 percent of the vote.

However, the validity of Proposition 8 was immediately challenged, and the US Court of Appeals decided that it would not go forward since it would take away rights that gay couples had already been granted.

Eventually, the issue reached the US Supreme Court — but at this point there was no state representative willing to defend Proposition 8.

So, with Governor Arnold Schwarzenegger or Attorney General Jerry Brown unwilling to take the issue any further, the decision of the Court of Appeals stood and Proposition 8 was annulled. Gay marriage was now legal in California.

> _"And the sky didn't fall, and the world didn't end, and we didn't see a spike in the divorce rates of heterosexual couples."_ \- Molly McKay, founder of Marriage Equality California

### 4. With some reluctance, the US Supreme Court finally recognized gay marriage in 2015. 

After Vermont and California, other states began voting on the issue of gay marriage, including Maryland, where it became legal in 2013.

That year, John Arthur was living in Ohio with his partner James Obergefell. Since Arthur was dying from amyotrophic lateral sclerosis, and gay marriage wasn't legal in their state, the couple traveled to Maryland to get married.

Unfortunately, Ohio wouldn't recognize their union, so they decided to sue the state.

Even though Arthur died a few months after the lawsuit was filed, Obergefell fought on, taking the case all the way to the US Supreme Court.

Now the nine judges of the federal Supreme Court were faced with a big responsibility. If they decided in favor of Obergefell, they would be changing the constitution's traditional definition of marriage and every state in the country would then be obligated to respect the legal rights of a same-sex marriage.

Some judges were hesitant: Justice Antonin Scalia felt the decision should remain in the hands of voting citizens and state legislators, people he believed to be more representative of society's moral standards.

But despite some reluctance, the Supreme Court did make a decision. With a vote of five to four, they decided that the right to marriage, under the Fourteenth Amendment of the Constitution, is equally applicable to gay couples and heterosexual couples.

The decision was announced on June 26, 2015, with a ruling that provided four reasons why straight and gay couples deserve equal treatment.

First of all, the freedom to choose whom you spend your life with should apply to everyone.

Second, marriage also supports the right to intimate association, meaning the choice to enter into and maintain intimate human relationships.

Third, the legal protection marriage provides is in the best interest of gay parents and their children.

And, finally, marriage comes with a wide variety of rights and benefits, such as inheritance, medical decisions and taxes, which should extend to gay couples as well.

Activists played a big part leading up to this decision, but, as we'll see in the next blinks, it's not the only issue they've fought for.

### 5. Gun lobbyists have succeeded by knowing which states to challenge and by having presidential support. 

1968 was a tragic year in the United States. Both Robert Kennedy and Martin Luther King were shot and killed that year, and, in response, the government issued the Gun Control Act, which limited the sale of mail-order guns. But activists have been fighting for looser gun laws ever since, and they have found that the state of Florida is especially supportive of their cause.

The strong support for the pro-gun movement in Florida is due, in no small part to the influence of the National Rifle Association (NRA) and lobbyist Marion Hammer, who was once the organization's first female president. Hammer is now 76 years old and she never leaves her Florida home without her trusty Smith & Wesson .38 caliber handgun tucked away in her purse.

In 2005, Hammer's lobbying team worked hard to make Florida the first state to pass a _stand your ground law_.

This controversial law states that if someone is threatening you on your territory or in a public space, instead of fleeing, you have the right to "stand your ground" and use your gun to defend yourself. 

The passage of this law shows the power of knowing where to lobby for your cause. But having the president on your side doesn't hurt, either.

NRA lobbyists faced a tough challenge in Virginia, where a 2002 lawsuit filed in a district court threatened to ban the sale of guns in the area.

In 2007, while the case progressed toward the Supreme Court, a crazed student killed 32 people on the university campus of Virginia Tech. The situation looked pretty dire for pro-gun campaigners.

However, timing was actually on their side. Two judges had recently left the US Supreme Court, giving President Bush the opportunity to appoint John Roberts and Samuel Alito, two judges who are both pro gun.

As a result, in 2008, the Supreme Court handed down a huge win for the NRA by declaring Virginia's ban on guns unconstitutional.

### 6. Even when the Supreme Court makes a terrible error, decades of activism can bring it to light. 

You're probably familiar with what happened at Pearl Harbor in 1941; there have been countless books and movies covering every detail of the Japanese attack on the US military base.

Less known are the details of what happened to Japanese people who were living in the United States at the time and how the racially prejudiced actions that followed were supported by the Supreme Court.

It began in February of 1942: Shortly after the attack, President Franklin D. Roosevelt issued an order for every American of Japanese descent living on the West Coast to be placed in internment camps.

While most of the 110,000 people complied, Fred Korematsu did not. As an American-born son of Japanese immigrants, he fought back. After being arrested, his case was represented by the American Civil Liberties Union, who argued that the president's order was unlawful and discriminatory.

But when the Supreme Court ruled on the case in 1944, they decided that, under the circumstances, it was justifiable to treat all people of Japanese descent as suspects.

It took decades of activism, but thanks to the persistent efforts of human-rights organizations, an investigative committee was eventually launched by President Jimmy Carter in 1979.

And, in the 1980s, the committee insisted that President Ronald Reagan should acknowledge the error the Supreme Court made in 1944.

Reagan refused to comply, but when new evidence came to light there was no denying the truth.

Back in 1944, some of the most compelling evidence to justify the internment order came from Lieutenant John DeWitt. He submitted a report suggesting that Japanese communities on the West Coast were using radio signals to communicate with the Japanese army.

But new evidence proved otherwise. Withheld from the final report was the fact that both the FBI and the FCC (Federal Communications Commission) found no suspicious behavior when they investigated Japanese communities.

Faced with this new information, in 1988, Reagan finally offered an official apology to Korematsu, and provided $20,000 for every Japanese American who had been interned.

### 7. By shedding light on government secrets, human rights activists can change the way prisons are run. 

Unfortunately, human right violations during times of war didn't stop with World War II. And when the horrible details about how US soldiers were mistreating inmates at the Abu Ghraib prison began to leak out in 2003, activists were once again essential to actualizing change.

Central to their fight for human rights was a call for more transparency in government operations. 

Jameel Jaffer and Amrit Singh are lawyers who used the Freedom of Information Act to find out more about the secret activities of the US military in foreign lands.

And what they found in the process was shocking: In one document signed by President Bush just after 9/11, the CIA was given authorization to secretly detain and interrogate suspects without legal protection.

Other disturbing papers documented instances of prisoners dying after severe interrogations, and how the military personnel used techniques such as defecating on the Koran or subjecting detainees to sensory deprivation.

They also found evidence that soldiers who reported prisoner abuse received threats and were told to remain silent.

The shocking discoveries continued in 2006 when the lawyers shifted their investigation into the practices and conditions at the lawless Guantanamo Bay facilities.

According to the records they found, only eight percent of the detained prisoners were shown to have a credible link to Al Qaeda, and only 16 percent were Taliban fighters.

This evidence seemed to contradict the government's assertion that the prison at Guantanamo Bay was reserved solely for the most dangerous and extreme terrorists.

The lawyers also revealed how the suspects at Guantanamo did not have access to lawyers, and were instead represented by US military officials who often offered no legal defense.

All of this showed how basic procedures in the justice system were being violated — and, eventually, the Supreme Court took action.

They ruled that the actions at Guantanamo were in violation of the Geneva Convention, which contains laws to protect prisoners of war, and that these laws must be adhered to in the future.

### 8. By being persistent in the face of denials, activism can change government policy and save lives. 

Even though President Obama's election campaign promised hope and change, there were still practices during his administration that kept activists busy.

One of the most prominent fights involved President Obama's support of a secret program to use remote-controlled drones to attack and kill enemy targets. 

Some support the use of drones since they can be launched from relative safety to conduct operations, thus obviating the need to put US soldiers in danger. But without personnel on the ground to better judge a situation, these operations run an increased risk of incurring collateral damage and killing innocent people.

When the reports of a drone program first appeared, President Obama refused to acknowledge its existence. This was due to an agreement the United States had with Yemen and Pakistan, whose agreement to allow drones to launch from within their borders was contingent on the program's secrecy.

Ultimately, the only thing that got the government to reduce drone attacks was mounting pressure from civilian activists. 

The human rights organization New America Foundation kept track of the drone activity and even brought a 9-year-old Pakistani girl to testify in front of congress about the time her grandmother was killed by a US drone. 

Finally, in May of 2013, Obama directly acknowledged the program in order to announce new conditions: From now on, drones would only be used against those who posed a serious threat to Americans, and only then if they couldn't be dealt with by any other means.

Further, under the new conditions, drones would only be used when the government was confident that no innocent bystanders would be harmed.

As a result, the death toll from drone strikes in Pakistan went down from 471 deaths and 100 civilian casualties, in 2009, to only 35 people killed and no civilian casualties in 2014.

By remaining faithful to what they believe in and committed to the goals they wish to achieve, activists can have an enormous impact on the political and legal world around them.

> _"Public sentiment is everything. With public sentiment, nothing can fail; without it, nothing can succeed."_ \- Abraham Lincoln

### 9. Final summary 

The key message in this book:

**Contrary to what some people believe, politicians and leaders actually care a lot about what the public thinks of them. And in this regard, activists can have a huge impact on the decisions that get made in society. They can draw much needed attention to specific causes and create pressure on governments to come up with good solutions.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Justice_** **by Michael J. Sandel**

What is justice? How can we act in a just and morally correct way? Drawing on various examples from everyday life, Michael J. Sandel illustrates how differently the idea of justice can be interpreted, for example, by philosophers like Aristotle and Kant. Over the course of the book, he urges us to critically question our own convictions and societal conventions.
---

### David Cole

David Cole is a professor of Law and Public Policy at Georgetown University Law Center and an advocate for civil liberties. In addition to being a contributor to publications such as the _New York Times_, the _Washington Post_ and the _Wall Street Journal_, he is also the author of multiple books, including _No Equal Justice_ and _Enemy Aliens_.

