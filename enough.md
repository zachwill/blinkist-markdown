---
id: 547df33661393200081c0000
slug: enough-en
published_date: 2014-12-05T00:00:00.000+00:00
author: John Naish
title: Enough
subtitle: Breaking free from the World of Excess
main_color: D02A3F
text_color: 9E2030
---

# Enough

_Breaking free from the World of Excess_

**John Naish**

_Enough_ offers a scathing critique of the one rule that always seems to hold in Western societies: "more is always better." With the help of compelling biological and psychological studies, _Enough_ shows us how our obsession with "more" is actually the source of many of our woes, as well as what we can do about it.

---
### 1. What’s in it for me? Why our desire for more and more stuff is hurting ourselves and the planet. 

In the western world, we live under the paradigm of "more is always better." Although most of us have everything we could possibly need, we still believe we always need more to be happy and satisfied. This constant striving for more — information, food, happiness, etc. — causes problems: more and more people are overweight, depressed or in debt, and the ecological system of the earth is out of balance because of human behavior.

John Naish explains the shocking consequences that our culture of excess has on us, and our societies and environment, and shows what everyone can do to change his or her attitude toward consumption and the constant striving for more. By providing easy-to-follow, concrete advice, these blinks enable you to implement the idea of "enoughness" in your life.

In these blinks, you'll discover:

  * why stone age man also wanted lots of stuff;

  * when divorce lawyers are at their busiest; and

  * what is wrong with all-you-can-eat-buffets.

### 2. Clever marketing tricks exploit our natural desire for more and more stuff. 

From a big-screen TV to a fridge packed with food, a fancy sports car to the many household appliances that you've only used once: the average citizen in Western societies owns or consumes an astonishing amount of stuff.

The reason has to do with the legacy of our evolutionary history: in order to survive and develop as a species, human beings had to be eager to try out new things.

Throughout most of human history food was scarce, and most humans weren't getting enough. To compensate for this scarcity, our bodies evolved to eat as much as possible when food was available and store the energy when it wasn't.

Eventually, we learned that collecting and hoarding vast quantities of resources, like food, clothing or tools, would further help us survive times of scarcity.

Another part of this legacy is a desire for both material _and_ nonmaterial goods. Just like we collect material goods, we also collect as much information as possible: our ancestors had to be constantly aware of their surroundings and note every possible detail in order to survive.

Our brains even reward us for our awareness with chemicals called _opioids_ that make us feel good.

And while this desire to collect information and things was advantageous for most of human history, today's advertisers now know how to exploit these ancient mechanisms in order to motivate us to consume more than we actually need:

They can, for instance, exploit our fear of scarcity by creating "limited editions," thus tricking us into thinking that passing on a chance to buy now will mean losing out on a limited opportunity.

Or they can exploit our desire to emulate society's most successful members. In the past we would look to the strongest or best-fed members of the group as role-models. Nowadays we look to celebrities who we hope to become by buying the products they're hawking.

Clearly, the mechanisms that once ensured our survival are problematic in today's world. Our following blinks will discuss this disconnect in more detail.

### 3. We experience information overload every day. 

Consider, for a moment, that there are 10 million spam emails sent each day around the world. One consequence is that an undeserving amount of our time is squandered sifting through these unwanted nuisances.

Spam mail, however, is just one example of a wider malaise:

Every day we're confronted with a deluge of information so overwhelming that it causes us to suffer serious health problems.

An important milestone in our understanding of information overload dates back to 1996 with the discovery of _information fatigue syndrome_ by British neuroscientist Dr. David Lewis. He observed that information overload can lead to exhaustion, insomnia, poorer decision-making skills and can even temporarily lower your IQ!

Much of the information we consume is transferred over video screens, such as computers, televisions, smartphones, etc. This is further problematic, as studies suggest that this type of information consumption might increase ADHD, depression and obesity in children.

As you know from the previous blink, we constantly seek out new information. This ongoing search (especially online) can even transform into a full-blown addiction.

Think about how the conveniences of the virtual world can come to replace "real-life" interactions:

It's easier to send an email than discuss something face-to-face. Likewise, it's easier to play videogames than play sports, or watch sitcoms instead of finding stimulation with friends and family.

People can easily become so dependent upon this convenience provided by virtual information that they actually experience real withdrawal symptoms after being away from the web for a few days or even hours.

Fortunately, there are simple ways to avoid the siren song of online data: the _data diet_.

From time to time, you can take an intentional break from the virtual world in order to keep your sanity. Even Bill Gates does it on a regular basis!

Not only does this data diet help you avoid the pain of information overload, it also helps you be more productive: in essence, the brain is more effective at solving problems when it gets a break from its usual routine.

### 4. The availability of food has led to problematic eating habits and a number of health problems. 

The all-you-can-eat buffet serves as the perfect symbol for our modern attitude towards food, namely: eat as much as possible for the smallest amount of money.

In the past, taking advantage of any all-you-can-eat opportunity was the difference between life and death. Today, however, the same mechanisms that caused us to take advantage of these opportunities now pose serious health risks.

For example, humans prefer foods with high energy (lots of fat and sugar) — a relic from times when energy was scarce. When we eat high-energy foods, we are rewarded with a jolt of hormones, _endogenous opioids_, which then encourage us to eat more.

The energy that we can't immediately use is then stored as fat, providing us with extra resources in order to survive the next famine.

Today, however, these factors become a dangerous mix when food is both available and cheap. The availability of fatty foods means we eat more than we should, which gets stored as fat in our bodies.

It should be no surprise, then, that obesity is a problem in developed societies. In fact, one out of four people in Europe, Australia and North America is obese, and therefore at risk of heart disease, diabetes and cancer.

Adding to this health problem is that most of us have developed counterproductive eating behaviors. Rather than listening to our stomachs, most people rely on visual cues to decide when it's time to put down the knife and fork.

Take this experiment for example, in which scientists invited students to a dinner in which their big soup bowls were continuously and inconspicuously refilled. Interestingly, without the visual aid of an empty bowl, the students ate much more than they thought they had eaten.

Fortunately, we can easily trick ourselves into eating less. One easy strategy is to simply use smaller plates! Studies have shown that people eat less from smaller plates, as their portions appear larger in relation to the plate, thus making them feel as if they've eaten more.

### 5. The constant craving for new material things is indicative of unhappiness. 

We're genetically predisposed to consume as much as possible. Indeed, collecting and hoarding are as old as humankind. Just look at the millions of hand-axes discovered by archaeologists in neolithic caves. They weren't just tools, but also the status symbols of our ancestors!

But why would we even want a hand-axe that we don't need?

Studies have shown that when we're "on the hunt" for something — whether it's a deer or a new pair of shoes — our brains are flooded with the hormone _dopamine_, which causes us to feel excited.

However, in the case of shopping, our dopamine levels sink once we've made the purchase, which often causes us to often regret our decision. In order to remedy this disappointment, we often head back to the store to experience the positive feeling of buying something new.

Furthermore, we're also prone to consume more than we need because we link material possessions with self-improvement.

Whenever we buy something new, we expect it to make our lives easier, more interesting or just better. So, when someone buys a fancy new sports car, he likely hopes subconsciously that this purchase will also come with a new, successful, rich and up-to-date version of himself.

Most of the times, these expectations don't come true. And even when our new purchases _do_ immediately make us happier, we eventually return to our previous level of happiness through a process referred to by scientists as _hedonistic adaptation_.

After the short burst of excitement from purchasing a new sports car, our protagonist will likely realize that he is now neither richer, more successful nor happier than he was before. No worry! He'll just turn his attention to a new item — perhaps a yacht — to fulfill these expectations.

A consequence of this kind of consumption is a home filled with junk and rapidly increasing amount of waste. Just think that there's a massive island of waste three to four times as large as Germany floating around in the Pacific Ocean!

### 6. Work is our new addiction. 

In Europe and the US, _workaholism_, i.e., the tendency to work to the bone and define ourselves in terms of our jobs, is on the rise. Yet, despite the widespread belief that hard work and industriousness are signs of virtue, the reality is that all that hard work can actually be bad for our health.

For example, studies have shown that regularly working 41 hours per week or more increases the risk for high blood pressure.

There are even programs to treat workaholism, such as Workaholics Anonymous, which regards workaholism as a serious condition and has grown in popularity in Britain in recent years.

However, unlike other addictions (e.g., alcoholism), overworking can't be cured through cessation. We simply can't _not_ work: our livelihoods depend on our income, and society demands that we stay "busy."

But _why_ do we overwork ourselves? Well, workaholism is born from other problems: workaholics expect that working more will help them feel better — but this simply isn't so.

Consider, for instance, that divorce lawyers are busiest after the holidays. Psychologists explain this phenomenon by pointing out that people who have problems in their marriages tend to work longer in order to minimize their time at home. During the holidays, however — when people generally _only_ spend time at home — these relationship problems become glaringly obvious.

In these cases, even overworking can't save a collapsing marriage!

Unfortunately, the expectation that more work will lead to greater happiness and fulfillment rarely works out, no matter how successful we become in our careers. Even promotions don't generally make us happier. In fact, studies show that the satisfaction we get from our jobs hardly depends on where we stand within the hierarchy.

Similar studies show that even those who make it onto the _Forbes_ List feel only slightly better about their lives than the average person.

### 7. Our seemingly limitless options and search for the perfect choice makes us miserable. 

A hungry donkey stands between two equally large piles of delicious hay. Unable to decide which pile to choose, it eventually dies of hunger.

This silly thought experiment first posed by fourteenth-century French philosopher Jean Buridan illustrates how free choice can become a burden when the alternatives are too similar.

While free choice is generally a good thing, an overabundance of options can be exhausting, because we feel that with every choice we might miss another, possibly better, option.

This is especially problematic today, as our modern world is full of products which differentiate themselves with small added features and extras.

Imagine, for instance, that your camera broke and you need to replace it before your upcoming vacation. As you start your quest for a new camera online, you'll easily become confused by the sheer number of possible features a camera can have — most of which you wouldn't need anyway.

So how do you make the right choice? You could spend an eternity comparing cameras trying to find the right one!

Now compare this with choosing between spending your next vacation on the ski slopes or a tropical beach. Likely, this decision will be far easier to make. In fact, studies have shown that deciding between two very different kinds of things is far easier for us than decisions where alternatives are similar.

Unfortunately, we're hardly ever faced with two distinctly different choices. Instead we face the prospect of near infinite choice, and this has serious consequences for our behavior.

For example, people nowadays rarely keep long commitments — we changes job and apartments like the wind. When it comes to personal relationships, people are more likely now to not want to bind themselves to a single partner exclusively for a long period of time.

In the USA, more and more marrying couples don't use the traditional vow "till death do us part." Instead they want to stay together for "as long as our marriage shall serve the common good."

> _"The consumerist promise of 'always a better option' encourages us to believe that… something far more wonderful is just about to appear."_

### 8. Infinite happiness is neither possible, healthy nor desirable. 

The last few decades have seen the publication of an astonishing number of books on finding fulfillment: our goal in life seems to be becoming happy, fulfilled and satisfied.

Yet, nobody can be happy forever. In fact, depression, worry and sadness are a necessary part of the human condition.

Many studies have shown that, throughout history, people who demonstrate caution lived longer than those who didn't worry at all. Indeed, an attitude of _depressive realism_, i.e., a moderate dose of pessimism, can actually help you flourish:

While very optimistic people tend to overestimate both their skills and their luck — and thus take unnecessary risks (like BASE jumping) — depressive realistic people can better predict the outcome of a given situation, and therefore have better chances to live longer (don't jump off a cliff, and you won't die of a fall).

What's more, despite our popular understanding, unhappiness is not a failure, but a totally natural mechanism that warns us when things aren't going well. In order to move past it, you should use the opportunity to reflect on your current life situation, rather than turn to pills.

Our obsession with happiness is even more silly when we consider how little influence we actually have over it. Everybody is born with her own unique happiness threshold, which remains quite constant throughout life. In essence, you are born an optimist or a pessimist.

While your happiness is mostly influenced by your genes, it's also slightly influenced by childhood experiences. However, after the age of 25, there are hardly ever major changes in a person's personality, habits or attitude.

In fact, studies have shown that this is even true after life-changing events. For example, people who win the lottery, as well as those who lose legs in freak accidents, report the same degree of happiness one year after the respective incident than before!

Although we are expected to be happy and fulfilled all the time, we have to accept the fact that this is simply not possible.

> _"The vast majority of psychological research indicates that as adults we seem stuck with ourselves, whatever we are like."_

### 9. Never-ending economic growth and wealth are mere fairy tales. 

We've been told for decades that the solution to all the world's problems lies in economic growth. Only now, however, have we begun to see just how far this is from the truth:

In the early twentieth century, the iconic economist John Maynard put forth the idea that economic growth would help to end starvation and allow everybody to satisfy their basic needs. Since the 1940s political leaders have used this line of thinking by pursuing a policy of continued economic growth.

But then, in 1970, the influential think tank The Club of Rome published a book that grabbed international attention: it demonstrated that the earth's ecosystem was unable to cope with our rapidly growing population and our subsequent need for more resources.

They concluded that, due to the fact that the Earth's resources are limited, infinite economic growth is impossible.

A logical consequence of this must be a shift in thinking — from focusing on infinite growth to developing sustainable ways of using the planet's resources. Of course, this is easier said than done:

Firstly, our global economic system is based on the ideology of economic growth. Consequently, if we all consumed less, the economic system would collapse. Despite its environmental consequences, businesses continue spending enormous resources to encourage people to consume _more_, not _less_.

Secondly, to make meaningful change, we would have to motivate changes in behavior in huge sections of the population in order to alter social norms.

Look at the stock market as an example: when prices for a certain stock rise, the demand for it likewise increases — despite all common economic wisdom that says we should buy when the price is _low_.

We depart from this wisdom because we believe that what the majority of people do or believe is right, a phenomenon scientists call _herd behavior_. As long as the majority of people don't change their attitude towards consumption, it's unlikely that we'll make much progress in changing our dangerous habits.

Up to this point, we've explored the dangers of our continued search for "more." Our final blinks will offer some advice on what to do about it.

> _"By hook or by crook, the growth in human numbers and economic activity will slow, stop and reverse."_

### 10. Unlike material possessions, there are some nonmaterial things that we really can’t ever get enough of. 

So far, we've seen numerous reasons why the attitude that "more is better" is so dangerous. However, this applies _mostly_ to material goods. There are _nonmaterial_ things that we really _can_ never get too much of!

For example, we can never get too much gratitude or generosity. In fact, both of these things improve our lives!

Studies show that people who note the things they are thankful for in their diaries have both higher levels of optimism and energy as well as healthier hearts.

Studies on generosity also show that donating money activates the same brain regions as receiving money or meeting friends. So, by giving something to others, you indirectly increase your own happiness.

Another thing that we can never get enough of is _connectedness_, i.e., our connections to and communication with others. In fact, several studies have demonstrated that the happiest people are those who spend the most time socializing.

Indeed, stable relationships — whether they're with friends or a higher power — make people more content, and might even increase their longevity.

One way we can increase our awareness of these nonmaterial things is by keeping our focus on the present, rather than on the past or future.

Take the "Ladder of Life" study as an example, which asked people each year since 1964 how they evaluated their current life situation, their situation five years in the past and their situation five years in the future:

It became clear that people always considered their past situation worse than they had when they were actually experiencing them. Moreover, they always considered their future situations would be better than they actually ended up being.

From this data, scientists concluded that, while we always have high expectations for the future, they hardly ever come true.

> _"As a culture, we need to value different emblems of 'cool' — such as time, space and autonomy — rather than trinkets."_

### 11. In order to change our culture of excess, we need to tackle some taboos. 

As you've seen in our previous blinks, we can't continue a lifestyle of excess without serious consequences for both humanity and the planet we live on.

However, before we can even discuss any alternatives to our current culture of excess, we first need to understand the inadequacy of the assumptions that underpin that culture:

We assume, for instance, that there's no alternative to the pursuit of continuous economic growth under capitalism. But why should we believe that a system that has failed to bring prosperity to everyone and in fact increases inequality should be our ultimate truth? Wouldn't it be better to invent new ways of organizing society and economy?

Then there are major taboos, such as population control. But we _need_ to think about how we'll handle the growing human population, as well as what we can do to slow this process down.

Finally, we grow up with a materialist paradigm that tells us to value material possessions over spiritual or intellectual enrichment. However, as we've seen in these blinks, we should be doing the exact opposite!

Only once we tackle these assumptions can we begin to implement the idea of _enoughness_, i.e., abandoning our constant striving for more information, stuff, happiness and options, and instead cherishing what we have.

We must understand that "good" is sometimes good enough, and that achieving perfection in every aspect of our lives is neither realistic nor desirable.

This transformation, however, can't be dictated from above; it has to start with us as individuals.

One way to initiate this change is to lead by example and thus inspire others to adopt the same idea of enoughness. You don't have to completely revamp your entire lifestyle — sometimes even small changes and more reflection on personal decisions can make a significant difference in your life.

While it won't be easy, it's absolutely necessary to start rethinking our lifestyle and changing our behavior in a way that aligns with the concept of enoughness.

### 12. Final summary 

The key message in this book:

**We live in a world of excess: we're overworked, overfed, we buy too much junk and believe that the "grass is always greener on the other side." As a result of this excess, we've become mentally and physically unhealthy and severely damage the environment. We can only escape this vicious circle by realizing that "more" is not a solution to our problems, but rather their source.**

**Suggested further reading:** ** _How Much is Enough?_** **by Robert Skidelsky and Edward Skidelsky**

_How_ _Much_ _is_ _Enough?_ sets out to critically inform the reader about the moral, historical and economical backgrounds of modern day capitalism, its obsession with accumulating more and more, and its consequences. It also vigorously outlines an ethical alternative to this lifestyle, in which our obsession with "more" is replaced with "the good life."
---

### John Naish

John Naish is a journalist for the _London Times_ who writes about such topics as health, body and soul. He is also the author of _Put What Where: 2000 Years of Bizarre Sex Advice_.

