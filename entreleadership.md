---
id: 5550b24a6332630007460000
slug: entreleadership-en
published_date: 2015-05-13T00:00:00.000+00:00
author: Dave Ramsey
title: EntreLeadership
subtitle: 20 Years of Practical Business Wisdom from the Trenches
main_color: F7D24B
text_color: 786624
---

# EntreLeadership

_20 Years of Practical Business Wisdom from the Trenches_

**Dave Ramsey**

_EntreLeadership_ (2011) shows that by combining the traits of an entrepreneur and a leader, you too can build a successful company. Author Dave Ramsey draws on his some 20 years of entrepreneurial experience to share his professional secrets on how to start and grow a company from one to 400 employees from the ground up.

---
### 1. What’s in it for me? Learn how you can embody the best qualities of a leader and an entrepreneur. 

Can you pat your head while rubbing your tummy? It's not easy, is it? Trying to do very opposite things at the same time is a challenge not only for our bodies but also for our minds.

In today's competitive business world, leaders are often expected to do just that, perform tasks simultaneously that seem contradictory. On the one hand, a business leader needs to be a risk taker and passionate about her business, while on the other, she needs to be a calculating, rational manager.

How can any one person master both personalities? These blinks will show you the way to best combine the energy of a daring entrepreneur with the cool judgment of a savvy leader so you can take your venture to the next level and beyond.

In these blinks, you'll discover

  * how a simple to-do list can help you realize your dreams;

  * why taking on debt is only for dummies; and

  * why locking your staff out of the office isn't an effective motivation technique.

### 2. An EntreLeader combines the best qualities of an entrepreneur and a leader. 

What sort of qualities do you expect from a start-up founder?

A start-up founder certainly needs to be comfortable taking risks, and should have the go-getter attitude of an entrepreneur. What's more, she needs to be comfortable thinking outside the box, as that's how she'll create new products and find the right market niche.

It's also necessary for a start-up founder to be a great leader. She should be disciplined, yet humble. She also needs to keep her cool under pressure, especially during the hectic early days.

Ultimately, an effective start-up founder should embody just the right combination of qualities from great leaders _and_ great entrepreneurs. In other words, you need to be an _EntreLeader_.

An EntreLeader is a risk-taker who can spot gaps in a market. She is a passionate individual with a strong desire to succeed, but she's also a capable leader who can motivate a team and can make tough decisions with integrity.

A company's leader determines a company's culture. Thus a leader needs to have strong EntreLeadership skills if she wants to succeed.

Many CEOs today struggle with this. They don't always realize that strong leadership skills are just as important as strong entrepreneurial skills.

The author too struggled with being an effective leader. In one of his earlier ventures, he managed 14 employees. Although he was passionate about his work, he was often frustrated that his staff didn't share his passion equally. He often overreacted to small issues, such as employees showing up late.

Letting his anger overwhelm his reason, the author once forced his whole staff to sit outside on a chilly day, telling them that being late would "leave them out in the cold."

Unsurprisingly, this demonstration didn't go over well with his staff. And his failure to be a balanced leader didn't only bring down the spirit of his employees, but it also hurt his business.

The good news is that leaders can always learn and change. When you adopt the necessary skills to become an EntreLeader, you can bring your organization great success.

### 3. Outline a clear plan for realizing your dream business. Set goals and stick to them! 

When you imagine your dream business, of course you can't help but dream big. You know that through your ingenuity, innovative thinking and hard work, you'll achieve success.

Unfortunately, many of our dreams never become reality. So what can you do to realize your dream?

An EntreLeader breaks her dream into specific goals that each have a clear set of steps to achieving them. In doing so, an EntreLeader and her team always know what they're working on and where they're headed.

To keep your team on track, try using a well-known management practice called _management by objectives_, or _MBO_.

First, you agree to a set of goals with each member of your team. Next, you set a clear deadline for when those goals should be met.

Accountability is important. If team members are involved in the goal-setting process, they're also on the hook if things go awry; this fact helps to keep everyone motivated and on track.

And setting goals isn't a process just for your employees — you can and should adopt this process, too.

Have you ever avoided a task because you were afraid that you'd fail? This kind of hesitation can prevent your dreams from becoming a reality.

Avoid unnecessary delays by setting goals for yourself. When you set a well-defined goal with a clear deadline, it's hard to wander off course. You know the path; so you have to keep going.

The author's friend, a web designer, decided to start his own business. He struggled in the beginning, so he set a goal of earning $5,000 per month after 120 days — or close the business.

This self-imposed pressure energized him and forced him to take action. And as a result, he ended up earning $7,000 per month, much more than he had planned!

Your dream is the starting point of your business's success, but be sure you've set a clear plan for realizing it.

### 4. Manage your time wisely with prioritized to-do lists. Strive to make it a daily habit. 

Have you ever spent a whole day working on a task, only to reflect and feel you hadn't actually accomplished much? It's not a rewarding feeling.

You'll fall victim to this if you don't set a clear plan for what you want to achieve. A simple tool like a _to-do list_ can keep you on track.

A to-do list gives you control of what occupies your time during your busy day. Don't fall into a situation where you only work when you're asked to do so — work when things need to be done.

Think of it this way: you probably would rush out to help a friend if asked, right? While it may be a kind gesture, this sort of behavior just creates more work for yourself, as in doing so you're probably neglecting other important tasks.

A prioritized to-do list helps you avoid reactive behavior. When you have an orderly list of tasks to complete, you'll concentrate better. You can still help out friends and colleagues, but you'll remember what you really need to do — and know when your list tasks are more important.

A to-do list also gives you satisfaction when you look back at it at the end of the day, as it shows you all the goals that you've achieved and all the tasks now completed.

When you review your list and see a bunch of "finished" check marks, you'll know you've spent your time wisely. You'll also know that the tasks you've checked off are the most important things you had to do — otherwise they wouldn't be on the list.

A day spent on a well-planned family holiday feels much better than a day on the couch, right? That's because you've accomplished a list of meaningful tasks. Strive to make this a habit.

### 5. A great marketing strategy doesn’t happen immediately; work on your pitch to perfect it. 

What's the most important aspect of your business? You might assume it's your product. After all, you can't earn money without it!

A product is important, but it's meaningless if you can't convince _other people_ that it's important too.

This is where a marketing department comes in handy. Behind every successful business, there's a great marketing strategy.

A great marketing strategy connects your customers' needs with your product. In 1994, the author started a class about how to avoid bankruptcy. He called it "Life After Debt," marketed the class on the radio and offered a first session for free.

Only six people attended the class, however. The author realized that his course wasn't meeting his audience's expectations, as most weren't bankrupt but were interested instead in achieving financial stability.

So the author revised his class, renaming it "Financial Peace University" and focusing on how students could achieve their financial goals. It was a huge success, and to date, millions of people have taken the class!

This story also illustrates that building a strong marketing strategy is a learning process. While your first attempt probably won't be successful, you'll learn from your failures and tweak your strategy accordingly. And in doing so, your strategy will just get stronger and stronger.

And even if you _have_ crafted the perfect strategy, you still need to make sure you've hired the right sales team. After all, they're the ones who persuade your customers to buy.

Sales isn't about manipulating people into buying a product, but about giving people a positive experience by showing you care about their needs and offering useful information about how your product can meet those needs.

You can't force someone into buying anything, but if you earn their trust, it's likely they'll become a customer. So build trust by showing pride in your product and treating your customer with respect.

And remember: a happy customer doesn't just buy a product, they tell their friends about it, too!

### 6. Whatever you do, avoid debt. And to protect your company, be sure to build an emergency fund. 

Your company needs some capital to expand. But how do you find the money you need? A small loan wouldn't be risky as long as you're careful to pay it back, right?

Wrong. Debt destabilizes your company, putting you at greater risk of failure or even bankruptcy.

When you take on debt, you'll have to spend a period paying it off, which will reduce your cash flow. You'll have less money to pay your bills, your staff or buy stock. In fact, most small companies go under because of cash flow problems. Reducing your cash flow simply isn't a wise thing to do.

Here's a straightforward example of the perils of cash flow problems. The owner of a small but growing plumbing company obtained a commission from an important client to dig a ditch. To do the job, he needed a backhoe, but the rental costs were $450 per month. So the owner decided to take out a loan to cover the rent for the backhoe.

After a few months, the owner realized that the fees associated with the loan were crippling his profits. The lingering debt essentially plunged a once-profitable business into crisis.

Avoid the temptation of falling into debt by creating an _emergency fund_. An emergency fund is true to its name, in that it's where you store extra cash to cover yourself or your business in times of crisis.

The author benefited greatly from having an emergency fund. A bankrupt company was getting rid of its high-quality office furniture worth some $300,000. While the author didn't really need furniture at the time, the fire sale price of $21,000 was too good to pass up.

So he used his emergency fund to buy the furniture and stored it. Later on, he used the furniture for a new company, saving hundreds of thousands of dollars in the process, and importantly, avoiding going into debt.

Financial stability helps you survive in the long run, as you'll be more able to recognize problems and handle unexpected events.

### 7. Reward your employees fairly through fair salaries and bonuses. They’ll stay loyal and hard working. 

Every EntreLeader understands the importance of earning the respect of her employees. But what is the most effective way to earn respect? Here are a few pointers.

First, make sure you always reward your staff appropriately for their hard work. A company's compensation system reflects a leader's values. If you compensate your team fairly, they'll feel not only appreciated but also will work harder for you and the company.

And if you offer your staff incentives, such as commissions or profit sharing, they'll be even more motivated to help your business grow.

One company with a whopping $50 million in revenue employed some 120 workers. Yet despite the company's successes, the CEO didn't deem it necessary to pay even his top employees a salary of over $100,000.

Unsurprisingly, these employees felt their work wasn't being rewarded fairly, and started to leave for better offers. Eventually so many top employees left that the company had to shut its doors!

If you treat your employees with dignity, they'll repay you with loyalty and motivation. So respect them and go out your way to please them — it's always worth it in the end. People just work harder for someone who treats them well!

One of the author's employees, for example, sealed a million-dollar deal. The company's salary structure didn't stipulate that he should be rewarded, but the author rewarded him anyway.

The author knew his employee would keep working hard if he showed how much he respected his work with a reward. The employee was so overjoyed that he wept!

So when you hire the right people, you also need to build a culture of unity and loyalty. Your staff will perform better, and you'll enjoy much greater success.

### 8. Final summary 

The key message in this book:

**If you want a successful company, be both an entrepreneur and a leader — an EntreLeader. Managing your team fairly and effectively is just as important as risk-taking and passionate deal-making! Define your company's strategic plan, manage your time well, avoid debt and craft a thoughtful marketing strategy. Running a company is a challenging task, and only an EntreLeader is capable of success.**

**Suggested** **further** **reading:** ** _The Total Money Makeover_** **by Dave Ramsey**

_The Total Money Makeover_ (2013) is a step-by-step guide to turning your financial situation around, no matter how nasty it seems. By following these seven simple steps, you can put financial security back into your life and begin planning for a comfortable, contented retirement.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dave Ramsey

Dave Ramsey is the author of several bestselling books on finance and leadership, including _Financial Peace_ and _The Total Money Makeover_. He founded and is the owner of _The Lampo Group, Inc._

