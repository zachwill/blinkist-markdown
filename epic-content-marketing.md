---
id: 5563ac2a6461640007540000
slug: epic-content-marketing-en
published_date: 2015-05-26T00:00:00.000+00:00
author: Joe Pulizzi
title: Epic Content Marketing
subtitle: How to Tell a Different Story, Break Through the Clutter, and Win More Customers by Marketing Less
main_color: FDA233
text_color: 96601E
---

# Epic Content Marketing

_How to Tell a Different Story, Break Through the Clutter, and Win More Customers by Marketing Less_

**Joe Pulizzi**

_Epic Content Marketing_ (2014) offers you a step-by-step guide to mastering one of today's most innovative approaches to product marketing. Knowing your audience and assembling a top-notch content team are just some of the key elements to achieving a successful content marketing strategy and getting ahead in a highly competitive market.

---
### 1. What’s in it for me? Learn how to attract customers through creating content to improve their lives. 

Among his recent predictions, _trend curator_ Rohit Bhargava described what he called the "Reluctant Marketer." Bhargava believes that soon, marketing as we know it today — yelling louder than your competitors about how good your product is — will be a thing of the past.

Thoughtful marketing that offers advice and value will instead take the place of generic advertising noise; messages that focus on the needs of the consumer, not the virtues of the product, will be the norm.

The future of marketing will be _content marketing,_ and the future of business will belong to those companies that can master their message and tell a compelling story to an audience they know well.

These blinks will give you a leg up on the competition and help you craft your own successful content marketing strategy from the ground up.

In these blinks, you'll discover

  * why a good story is more powerful than a great product;

  * how the most effective marketing ideas are cheap, too; and

  * how sharing others' content will help you sell your services better.

### 2. Content marketing helps you grab customers’ attention by giving them the information they crave. 

Marketing is everywhere. You are, on average, bombarded by some 5,000 marketing messages every single day!

As a consumer, there are few moments of the day when you're _not_ being asked to consider a product or service. As a business, on the other hand, you've got to fight through this persistent noise to get your message heard.

To make sense of the chaos, you need to realize the fundamental mistake of traditional advertising. Your customers don't care about your products or your services. _Customers care about themselves._

If all you do is list your product's great features, your message will fall on deaf ears. But what if you instead helped your customers to understand _how_ those features could change their lives?

This is _content marketing_ — a new marketing approach that gives you the power to truly grab your customers' attention.

Agricultural specialist Deere & Company is already incorporating content marketing into its overall strategy. The company's publication "The Furrow" is no mere sales catalog but a magazine that informs farmers about the latest agricultural technology and how to successfully use it. By providing trustworthy advice, Deere also increases demand for its own products.

Content marketing can also benefit companies through strategies that can increase consumer trust for a far lower cost than traditional marketing.

Venture capital firm OpenView in 2009 launched a blog to share the company's insights about topics of interest to entrepreneurs, such as marketing and finance. Within three years, as many as 18,000 subscribers signed up.

Yet if OpenView had decided instead to attract clients through more traditional methods, such as billboard or television ads, the company's marketing costs would have been far higher. In contrast, operating a blog was very inexpensive.

And as OpenView expanded its collection of informational posts, podcasts and videos, it became a trusted resource for exactly the kind of entrepreneurs the company wanted to attract.

### 3. First, be content aware. Then, establish your expertise. Finally, inspire others with your story. 

Before you dive headlong into content marketing, it's important to think about what exactly your company wants to achieve with it.

There are three distinct levels to content marketing, and each produces a different result. Let's examine the three levels: being content aware, being a thought leader and storytelling.

The ground level of content marketing is being _content aware_. Here, your content works to cut through the noise of generic advertising to boost _awareness_ of your product.

The strategy of Tenon Tours, an Irish tour operator, is a good example of marketing that is content aware. The company operates a blog that highlights Irish culture and local events. The blog simply provides content that potential customers might find interesting, hopefully so much so that they consider a company-sponsored tour.

Was this content-aware strategy enough to boost Tenon's business? Amid a competitive market, Tenon upped its visitor numbers by 54 percent after starting its informational blog.

If you want to do more than just raise awareness, however, then you're ready for the next level: being a _thought leader_. Being a thought leader means that you, as a brand, create content that delivers value _beyond_ the scope of your product to establish your company as an industry expert. 

Venture capital firm OpenView is an example of a thought leader. The company goes beyond its basic services to provide answers that entrepreneurs can't find anywhere else. Importantly, the people who flock to the company's site for information are more likely to use its services, too.

While OpenView's thought-leading approach is sophisticated, there's still a higher level of content marketing: _storytelling_. With storytelling, you provide your customers with an emotional link to your brand.

Say you're a business owner deciding between two suppliers. Both firms are successful and respected, but one firm's brand message stands out to you immediately. What is it about the company's message that grabs you?

The company founder explains his passion for the environment since childhood, and explains his leadership in local environmental projects. The overall brand message is one that is inspiring, one that shows the company is looking to change the world in ways that people care about.

The company's story is emotionally compelling and honest — and as a result, you're sold!

### 4. Know your audience! When you do, you will find your content niche and then a dedicated following. 

Once you've grasped the basics of content marketing and know at which level you want to operate, it's time to start developing content.

Importantly, you need to figure out who you're talking to — who exactly is your audience?

Ask yourself questions to learn more about your _audience persona._ Who is he, or she? How old are they? What's an average day like in this person's life? How much money does this person make? And importantly, why might this person care about my product?

As an example, the _audience persona_ for a consumer financial services company might look something like this: a 40-year-old man, married with two children, employed full-time, travels a lot for work, has existing contacts with financial services companies.

Now that you know your _audience persona_, ensure the content you create addresses the issues with which your audience might be concerned. Our hypothetical customer might be interested in how to create a trust fund for his children or how to better invest his earnings for the future.

Knowing your audience persona will also help you discover your _content niche_, or your content's specific focus or topic that gives it particular value for your audience.

If you own a pet supplies store, for example, it's easy to simply write about pet supplies. But there's plenty about this topic already! You might discover, however, that your audience persona is both elderly and enjoys traveling with pets.

Bring these two topics together, and boom! You've found your niche. Start your journey today becoming the expert in retiree travel with pets.

### 5. Content needs to be created! Assemble a top content team and be organized to keep things on track. 

Knowing what you need out of content is important; but creating good content is a larger challenge.

To be a successful content marketer, you'll need to find talented people to fill three, specific roles.

The first person you'll need is a _chief content officer._ This person oversees all content marketing initiatives and makes sure they are in line with the company's overall marketing strategy.

Ideally, this person has journalistic experience and perhaps an MBA; what's more, your content officer should know how to create content for different target demographics and be able to coordinate any damage control, if and when needed.

The second person you'll need is a _managing editor,_ who will work with _content creators_. This team will actually craft your content. Often content creators are freelance writers who are skilled in writing stories based on your company's audience persona and selected content niche.

A managing editor makes sure work comes in on time, edits it and schedules it for publication. Also, a managing editor will ensure that your content is optimized for search engines, too.

And finally, you'll need a _listening officer._ This person ensures that all your great content has the desired marketing effect. The listening officer monitors how your audience is responding to your content, mostly by following comments on social media or other communication.

Importantly, you'll also need an _editorial calendar_ to keep everyone working in a timely fashion. A key part of the editorial calendar is a _prioritized list._ This list includes existing content that needs a redesign, content currently in development and content planned for the near future.

The editorial calendar of a company blog, for example, might include three edited posts waiting for final image correction; two posts being edited to be completed by Thursday; and the review and approval of all blog posts by Friday.

### 6. Use sharing strategies and SEO to promote your content far and wide. 

So you've outlined your strategy and you've assembled a content team. What's next? Now, you need to figure out how to promote your content.

Social media is key, but which services should you use? Do you want to use Facebook, to reach as many people as possible? Doing so would mean you'd face more competition.

Or would you rather use niche content sites with more limited, yet specific, audiences? This means you'll be addressing an audience that you know will want to hear what you have to say.

After settling on an approach that suits your goals, begin by creating a strategy that gives your work the best chance of being read and shared. You might want to consider the _social media 4-1-1_ approach, as created by Andrew Davis in his book, _Brandscaping._

It works like this. For every six pieces of content you share on social media, take four pieces from influencers, such as bloggers or media outlets, to build relationships with the top personalities in your industry. These individuals may very well share your content with their audience in return.

Next, share one piece of your original content. Then, share one sales piece, such as a coupon, a product notice or a press release.

Another way to ensure your content is shareable online is to make sure that it's searchable online. You can achieve this by using search engine optimization, or SEO.

Keywords help foster good SEO. Services such as SEMrush will help you determine which keywords you could use to ensure your content is found when people are looking for it.

A good strategy tested by the author requires simply one target keyword for every 150 to 200 words in your piece of content. In doing so, the author found that traffic to the piece of content doubled!

### 7. Use your tools to ensure your content is making its mark. Solid metrics will help you fine-tune. 

So you've executed a content marketing strategy by the book. What's next?

Your last step is to evaluate just how successful your strategy was. Content marketers can measure success through four metrics: consumption, sharing, lead generation and sales.

There are several tools to help you review your content's _consumption_. Google Analytics can track page views and downloads; YouTube Insights tracks video viewing numbers; and services such as Mention.net and Salesforce.com offer tools to track and measure your brand's impact in online conversations.

To find out just how "social" your content is, you'll need to investigate _sharing_ _metrics_, which include actions such as likes, shares, tweets, pins and forwards. Sharing metrics are the best indicator of whether your content is getting people talking. Sharing metrics can be tracked through Google Analytics; more specific software such as Open Site Explorer, Raven Tools and Majestic can offer deeper insight.

For example, if a post on the author's blog doesn't receive at least 100 retweets in the first 24 hours, or over 200 retweets in the first week, the content is revised to make it more shareable.

As well as shareability, the capacity for content to be monetized is also worth reviewing. You can gauge this using _lead generation_ metrics, through form completions, subscriptions and customer conversion rates. For example, if your overall conversion rate is 2 percent but one particular e-book is only converting 1 percent, then that content may need some work.

There's one more metric left, and that's _sales_. Fortunately, sales metrics are much simpler to examine, in that you can always count the amount of money you're making!

A solid understanding of all four metrics will allow you to accurately review your marketing strategy and improve it where and when necessary. Then you'll be using your content to its fullest potential.

### 8. Final summary 

The key message in this book:

**To be an effective content marketer, you don't just need more content. You need the right kind of content, expertly crafted, strategically released and tracked accurately for its effect on social conversations and sales.**

Actionable advice:

**Don't throw out the ad baby with the bathwater.**

Content marketing is an important piece of an overall marketing strategy, not the only strategy you should explore. Your content creates an interest in your product; yet once your customer is ready to buy, she'll need some concrete data and direct sales info to help her make her decision. It's here where traditional advertising earns its stars!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Everybody Writes_** **by Ann Handley**

_Everybody Writes_ (2014) gives you invaluable advice on how to create great content, from using correct grammar to crafting engaging posts, tweets and emails. With just a handful of simple rules, these blinks will help you gain a better understanding of how to use the right words to keep customers coming back for more.
---

### Joe Pulizzi

Joe Pulizzi is a content marketing strategist and speaker as well as founder of the Content Marketing Institute, which runs the largest physical content marketing event in North America. He's also a co-author of the books _Get Content, Get Customers_ and _Managing Content Marketing_.

