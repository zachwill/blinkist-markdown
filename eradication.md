---
id: 57026bf58fb8ff00070000a5
slug: eradication-en
published_date: 2016-04-08T00:00:00.000+00:00
author: Nancy Leys Stepan
title: Eradication
subtitle: Ridding the World of Diseases Forever
main_color: D82B32
text_color: BF262C
---

# Eradication

_Ridding the World of Diseases Forever_

**Nancy Leys Stepan**

_Eradication_ (2011) is about the health community's attempts to eradicate certain diseases from the face of the planet. These blinks trace the history of disease eradication, its successes and failures, and the complicated political issues it raises.

---
### 1. What’s in it for me? Join the struggle against disease. 

Less than a century ago, people were dropping like flies from diseases most of us never have to worry about, or that don't even exist anymore. 

The advent of vaccines in the eighteenth century was one of the greatest triumphs of public health. And today, armed with an ever-increasing knowledge of the way diseases work and spread, we have the possibility of totally eradicating many of them. But should we? 

It may sound counterintuitive, but many people are actually against the concept of total eradication. Not because they want people to die from diseases, but because the cost of these projects outweigh their benefits and take money away from other more important projects. So what should we do instead?

In these blinks, you'll learn

  * why the concept of eradication is so radical;

  * why you need to send out an army to kill mosquitoes; and

  * why people booed when Bill and Melinda Gates announced they wanted to eradicate malaria.

### 2. The eradication of disease is now theoretically possible, but it raises many questions. 

It would be a good thing if we eradicated all diseases, right? Thanks to scientific advancements, we're getting closer to being able to do that every day. 

The eradication of disease has been a possibility since the nineteenth century, when scientists first realized what causes them. Robert Koch, for instance, discovered in 1882 that tuberculosis was caused by bacteria called _tubercle bacillus._ Scientists came to understand that tiny organisms like bacteria, viruses and parasites were responsible for disease. 

The scientific community also realized that disease-causing organisms were often spread by insects, such as mosquitoes. At the end of the nineteenth century, Ronald Ross and Giovanni Batista Grassi found that malaria was carried by female mosquitoes of the _anopheline_ genus.

Once they uncovered a disease's origin, scientists were able to work on preventing it by developing new technologies like vaccines. Eradication was suddenly a possibility!

And if scientists were able to eradicate a disease, they should have, right? 

After WWII, when scientists undertook eradication campaigns against smallpox and malaria, we learned the answer was more complicated than we thought. Few would argue against the elimination of those diseases, but some questioned the integrity of eradication as a philosophical concept. 

Eradication raises a number of questions such as: How do we choose which diseases to eradicate? How do we allocate eradication funding? These questions are biological, political and logistical all at once, so there are plenty of places where mistakes could be made. 

Consider malaria. We've undertaken huge eradication campaigns against malaria but, according to the _World Health Organization_, it still affects 250 million people per year. Has this been a waste of resources? Would funds be better spent elsewhere?

> The word "eradication" derives from Latin and means "to tear out by the roots."

### 3. Disease eradication is historically and politically rooted in imperialism. 

The United States began gaining imperial power at the end of the nineteenth century, with its intervention in Cuba and the Philippines. Although this made the country more powerful, it also created new problems, such as an increase in cases of yellow fever.

Yellow fever was widespread in Cuba, prompting the United States to make its first attempt at eradicating a disease to improve the public health. In 1900, US physicians in Havana found that yellow fever was transmitted by mosquitoes, so the army undertook a campaign to wipe them out. 

It worked: yellow fever nearly disappeared. However, the situation wasn't without problems.

First off, Americans didn't eradicate yellow fever to help the Cuban people. It was for their own benefit, as people and goods moving from Cuba to the United States were bringing it in with them. 

The eradication of mosquitoes made it much easier for colonial powers to expand into the tropics, as white people had been more susceptible to the diseases there. That was part of the reason the United States expanded the mosquito eradication program into the surrounding countries after its success in Cuba. 

And after the US army left Cuba, the Cuban government wasn't able to keep up with the expensive program for spraying mosquitoes. The disease returned a few years later, which prompted the United States to intervene once again.

_Paraffin oil_, the chemical used to kill the mosquitoes, was also harmful to the environment. After the United States intervened in the Panama Canal area in 1903, the paraffin oil poisoned the water supply.

The United States discontinued these programs after WWI, but philanthropic organizations picked up where they left off.

### 4. The Rockefeller Foundation was one of the earliest proponents of disease eradication. 

Have you ever heard of the _Rockefeller Foundation_? Even if you haven't, you might have benefited from its work. 

The Rockefeller Foundation was founded in 1913 on the belief that diseases were the source of all societal ills such as poverty and crime. Fighting disease was thus seen as a prerequisite for building an advanced society. 

The Rockefeller Foundation was very successful: unprecedented amounts of money were spent on healthcare infrastructure in countries that had previously lacked strong public health systems. The Foundation also led the way in disease research and prevention. As a result, illnesses like malaria and yellow fever were sharply reduced in many parts of Latin America.

Alas, eradication once again proved more complicated than it seemed. Though the Foundation aimed at disease eradication, its goals were never fully achieved.

Part of the reason was that the Foundation never had enough information on the diseases they worked with, and chose not to listen to the people who did have it. They backed out of attempts to eradicate yellow fever, for example, based on information that was flawed and incomplete. 

Despite the fact that locals in the countryside notified the Foundation about the equally as deadly _rural yellow fever_, their message fell on deaf ears. Believing yellow fever was only an urban phenomenon, the Foundation focused all its efforts on cities and towns, so the disease continued to wreak havoc in the country. 

Eventually, the Foundation backed out of the project, believing it was too difficult to eradicate the disease. If they had listened to local experts or had aimed at controlling rather than eradicating the disease, they might have been more successful.

### 5. After WWII, eradication became a key component of international public health. 

The Rockefeller Foundation faced a lot of challenges with eradication but the idea still lived on in the global health community.

After WWII, the newly founded _World Health Organization_ (WHO) carried on the dream of eradication, with the Pan-American Health Organization (PAHO) serving as its first regional office.

The PAHO undertook eradication campaigns in the Americas against yellow fever mosquitoes in 1947, smallpox in 1950 and malaria in 1954. In the heat of Cold War paranoia, Western powers feared that diseased and impoverished populations would rebel and turn to communist forms of government. Eradication was thought to lessen the threat of revolution and strengthen the power of the West.

Not all Western powers supported disease eradication, however. Britain and France both resisted the WHO's attempts at eradication in their colonies, fearing that eradication campaigns might make the political situation in those places even more complicated. 

These early campaigns weren't flawless, either. Each campaign was focused on a single disease and based on what was thought to be the most straightforward solution, even if the solutions hadn't been thoroughly tested. 

In the years following WWII, large amounts of _DDT_ were used for the eradication of mosquitoes, for example, as local governments were desperate to eliminate them. Years later, in 1962, Rachel Carson published a book called _Silent Spring_ illustrating the devastating effects DDT had had on local animals. 

Numerous birds laid eggs with shells so thin they broke, killing the baby chicks inside. The survival of many species was threatened as a result. The point is: eradication techniques can have grave consequences, so if we use them before we fully understand them, we might not realize how harmful they are until it's too late.

### 6. Many major eradication campaigns have failed, such as the campaign against malaria. 

Malaria has long been one of the most widely feared diseases in the world. After observing the high death toll from malaria in WWII, the WHO decided to take measures to eradicate it once and for all. 

This proved to be a big challenge. When the war ended, people were filled with optimism for the future and had a deep faith in science. They believed that DDT might be a simple answer to the widespread, deadly malaria problem: DDT could take out the insects and people would be saved.

But malaria is different from yellow fever in two ways: first, it can be spread by different species of mosquitoes. Second, it was widespread throughout the tropics, whereas yellow fever only existed in Africa and the Americas.

The WHO used massive amounts of DDT, which wasn't only destructive toward the environment: it led to the growth of DDT-resistant insect populations, too. 

To make matters worse, the WHO's global campaign wasn't really global, as it put little effort into large swaths of sub-Saharan Africa, which it considered too difficult to access. 

And, although the WHO declared some countries malaria-free in the earlier parts of the campaign, new cases eventually sprung up. In 1968, the WHO rebranded the eradication program as a "control" program. Administrative problems, a misunderstanding of the nature of the disease, and inadequate finances and supplies stopped the program from achieving its original goal, and the WHO admitted the seeming impossibility of global eradication. 

Still, the program wasn't a complete failure. It did reduce malaria rates considerably, and is still saving the lives of many people.

### 7. The eradication of smallpox proved that eradication was possible – but complicated. 

If you were born after 1977, chances are good you've never even heard of smallpox. How come?

Smallpox is the only disease in history to ever have been successfully eradicated. The eradication of smallpox started as early as 1796 when the smallpox vaccine was developed: the first vaccine ever created.

The vaccine was developed after Edward Jenner, a surgeon, noticed that milkmaids who contracted _cowpox_, a milder form of smallpox, never suffered from smallpox. So he experimented by infecting a young boy with cowpox, and then with smallpox six weeks later. The smallpox didn't affect the boy!

It still took scientists several years to understand the way the vaccine worked and how it needed to be administered. On top of that, they also needed to make the vaccine more reliable and safe so it could be used to treat the disease all over the world. So it would still take some time to eliminate smallpox for good. 

The time came in the 1950s, when people starting making an effort to eradicate certain diseases worldwide. And as soon as the WHO threw its weight into smallpox eradication in 1966, the disease quickly started to disappear.

The last smallpox case occurred in 1977. In 1980, the WHO officially declared that the disease had been eradicated. 

The early success of the smallpox eradication campaign raised a lot of questions about the pros and cons of eradication. Chief among them was the issue of how to balance individual risks with national or global risks. 

Every vaccination poses risks for the people being vaccinated. There's a small risk that the vaccination will actually give them the disease, but the vaccinations themselves can have other side effects, too. If millions of people are vaccinated, it's possible these side effects could harm more people than the disease itself.

Some people argued that containing and controlling smallpox might be safer than trying to eliminate it. Those on the eradication side eventually won, however, and the campaign was completed. 

The campaign also forced poorer countries to focus their limited resources on smallpox, even if other diseases like malaria were a bigger threat to them. Eradication will always be a politically complex matter.

### 8. Eradication techniques have changed over time but the concept is here to stay. 

Since the late 1970s, one idea has dominated the concept of global health: _primary health care_, the belief that everyone should have access to basic medical treatment. 

There's debate as to whether or not eradication should be considered a part of primary health care. Some say it's inefficient to channel so much effort into certain diseases and it would be better to focus on larger issues, like sanitation or general health care. 

Others believe that, because we don't have enough resources to provide everyone with good health care now, we should focus on _selective primary health care_ by eliminating one disease at a time. 

In 2007, the complicated nature of this debate came to light when the _Bill and Melinda Gates Foundation_ declared its dedication to eradicate malaria. Not everyone in the medical community praised their goal; some said it wasn't the right goal in the first place.

These modern eradication campaigns are different than some of their predecessors. For instance, a campaign against polio was undertaken in 1988, and the Americas have been free of the disease since 1991. The polio campaign was based on mass immunization, surveillance and quick responses to any cases that appeared. In this way, it's more similar to selective primary care. 

The campaign against _guinea worm disease_ took a different approach. Guinea worm disease is only found in a few countries. It spreads through infected water and, if it goes untreated, it can lead to arthritis and paralysis.

The fight against guinea worm disease was based on cleaning the water supply rather than targeting the disease itself. Local mobilization and education about water safety were considered part of primary health care. These efforts haven't yet completely eliminated the disease but they've reduced it dramatically.

### 9. Final summary 

The key message in this book:

**It may seem that, if we can work toward a disease-free world, then we should. But there are a variety of complications that arise from eradication. Campaigns to eradicate diseases are very difficult to manage and carry out, and there are political workings at hand that do not always bode well for the people the campaigns ostensibly aim to help. While smallpox was successfully eliminated and other diseases have been sharply reduced, the main focus of global public health should not be eradication.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading: _A Planet of Viruses_** **by Carl Zimmer**

__A Planet of Viruses_ _(2011) takes you on a whirlwind tour into the hidden world of viruses. You'll discover how our understanding of these tiny, abundant organisms has evolved over time and how our lives are influenced by them, from their power to kill to their protective properties.
---

### Nancy Leys Stepan

Nancy Leys Stepan is a professor of public health history at Columbia University. She focuses on eugenics and Latin America.

