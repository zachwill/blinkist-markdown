---
id: 555b3bd56135330007ae0000
slug: evergreen-en
published_date: 2015-05-21T00:00:00.000+00:00
author: Noah Fleming
title: Evergreen
subtitle: Cultivate the Enduring Customer Loyalty That Keeps Your Business Thriving
main_color: 23AD4E
text_color: 0F4D22
---

# Evergreen

_Cultivate the Enduring Customer Loyalty That Keeps Your Business Thriving_

**Noah Fleming**

_Evergreen_ (2015) is about keeping your business fresh and staving off stagnation that threatens its survival. Fleming outlines a philosophy that puts customers at the center of a company's strategy, challenges conventional business wisdom and offers concrete advice for building long-term profitability.

---
### 1. What’s in it for me? Discover how to make your company evergreen. 

Most flowers wither and die during winter, but some, despite deep snow and heavy winds, just stay green all throughout the season. In many ways, it's the same with businesses. Some companies seem to have some magic formula for staying fresh.

What's their secret?

As these blinks will show you, having great content, an active customer community, and making sure people know what your company is all about are all important things for any company that wants to thrive in the long haul. Combined with a clear focus on your customer relationship, these actions will help you grow a truly evergreen business.

In these blinks you'll learn

  * what Spiderman can tell you about creating an evergreen business;

  * that there is no such thing as an average customer; and

  * why you should fire some of your clients.

### 2. Character, community and content are the three Cs that power an evergreen business. 

Bringing a Christmas tree home in the middle of winter is a beautiful feeling. Outside, most of the trees are bare, but you have the scent of sharp green pine in your own living room. That sense of renewal and fresh life are essential in business, too. Isn't there a way to build a more evergreen company, like that Christmas tree?

In the following blinks, we'll show you exactly how to create an _evergreen business_ by following the three Cs: The _Character_ is the company bedrock, which sustains the _Community_, a group that benefits from the firm's _Content_.

_Character_, the first C, refers to the brand's identity and personality. This is more than just the company's story.

Here's what we mean by "story." You know those guys who started building computers in their garage? They were both named Steve and their company was called — what again? Apple.

You might have also heard about a kid who got bitten by a spider on field trip. When he woke up the next morning, he had extreme climbing abilities. Who's that again? Spiderman. That's _character_.

The next C is _Community_, and it's about harnessing the human desire to connect with others. By instinct, we seek out people who share our interests, beliefs and values. Accordingly, companies should help build or nurture communities which are aligned with their brand.

Take the software developer, Adobe, which has several different communities online, serving as platforms for thousands of users to discuss the software and learn from each other.

And as for the last C? _Content_ refers to the company's products and services. This is the core return customers get for their money, but it's also much more. It's the customer service, marketing, logistics — all the intangibles that keep the company running.

So now that we've sketched an overview of the three Cs model, let's find out how they work.

### 3. A company’s motivation, values and beliefs define its character. 

Someone should be able to describe your company in pretty much the same way they would describe a friend. That's why you should define the firm's character in a simple and vivid way. Be as clear as possible, because that'll make it easy for your customers to trust your company and feel loyalty toward it.

To start building your firm's character, don't simply try to describe _what_ you do. Instead, ask yourself _why_ you do what you do.

Apple excels at this. The tech giant's iconic "think different" campaign, which featured heroes like Gandhi, helped the company communicate its character, and Apple's character is about thinking different and being different. And that's what customers are actually buying whenever they purchase an iPad.

Compare that to Blackberry, who launched their first tablet, Playbook, in 2011. The device was crammed with impressive high-spec technology, but failed to sell. The reason? Blackberry was focused on _what_ it was producing, and not _why_ it was churning them out. And since Blackberry wasn't sure of the answer to the second question, neither were its customers.

As you can see, articulating company character is hugely important. But this isn't about sketching a caricature of your business — you have to communicate something real and authentic.

Shoe retailer Zappos does this by producing product videos to run alongside item descriptions. These videos feature real company employees — not models — showing off every detail of the shoes, even those little weird things at the end of the laces.

These videos define Zappos' character in two ways, showing a company that's 1) passionate about its products and 2) employs people who care about providing great service. (Zappos' customer service also happens to have a phenomenal reputation.)

All in all, a company's character forms the basis for everything a company does. It guides relationships with customers and informs the firm's decision-making. And it's also the basis for building sustainable customer communities — something we'll explore in the upcoming blink.

> _"Fascinating characters are the crux of every compelling story."_

### 4. Build deeper relationships with your customers by fostering community. 

When you hang out with your closest friends, you're connecting on a deep level. Compare that to going to the supermarket, which is about participating in simple, mercantile exchanges — like exchanging money for a pound of coffee. The difference is, you and your friends belong to a _community_.

And actually that's something companies should strive for as well, because community can help a business build its brand through word of mouth.

CrossFit, for example, has a thriving community. Each day, millions of people visit CrossFit.com to see the Work Out of the Day (WOD). Anyone can try the WOD at home, post their results online and see how they measure up. And this process is totally free!

This kind of community building has strengthened the CrossFit brand, propelling it to new heights. The fitness company started with just one gym. By 2006, there were 18 gyms. And today, there are more than 8,500 CrossFit gyms all over the world. That's a 47,122 percent increase in just eight years.

In addition to supporting the company's brand, community also helps firms understand and address customer needs. Harley-Davidson has built up a strong community by hosting public events. This gives employees — and the company as a whole — a direct line to their motorbike-loving customers.

Harley-Davidson's focus on community started in the 1980s, a troubled time for the business. But embracing its customers proved to be the way forward: Today, Harley-Davidson is the biggest motorcycle company in the world, with a consolidated revenue of over $6 billion in 2014.

So as you can see, building strong communities plays an important role in creating the kinds of deep connections — not just transactions — that keep a company going.

### 5. Content encompasses everything the company does, and also the value it provides. 

What's the first thing you notice when you go into a restaurant? Probably not the food. And that's curious, since most restaurants would probably say they're in the food business.

That's how it goes for many companies: Too often, we focus on what we do — our products and services — and not how we do it. But a company's _content_ — the third C — isn't just about its products, it's also about the experience.

What about Chipotle. The casual Tex-Mex chain has always known that it wanted to offer more than other burrito chains. So at Chipotle, customers can customize their orders with fresh ingredients. When you combine those special touches with Chipotle's great locations and dining spaces, it results in an appealing experience for customers.

And this commitment to strong content has made the restaurant hugely successful: Steve Ells founded the company with an $85,000 loan from his father; today Chipotle boasts 1,595 locations and $3.21 billion in annual revenue.

Dell also offers plenty of customization options, so customers can create a device that meets their unique needs, with deeper value than an off-the-shelf computer.

This is a key point: Any product or service can be transformed into great content by changing the experience, and thus adding value.

For instance, in 2009 Uber set its sights on transforming the taxi industry. At its most basic, a taxi ride is a simple service that gets someone from point A to point B. Well, Uber does provide that service, but it's completely changed the experience. Instead of hailing a cab and paying with cash, you can order the car and pay automatically through the Uber app.

This has completely disrupted the taxi industry and changed the transport landscape in cities like New York. And in just six years, Uber has spread to 34 countries and more than 90 cities.

> _"Content is about offering a company's product in a way that reflects its character and community."_

### 6. Instead of seeing your customers in terms of averages, get to know who they really are. 

How would you describe your ten closest friends? As 37.3-year-olds with 2.5 children and 1.2 cars? Probably not. Unfortunately, that's exactly how most companies think about their customers.

But instead of thinking in terms of averages, segmentation and archetypes are a better way of getting to know your customers. And this is best done by combining demographics and behavioral data, which explains who customers are and what they do. As author Keith Eade put it, you're basically trying to create a customer straw man.

You can collect this information simply by asking questions about gender, age, education level, income, hobbies, spending habits, and so on. Of course, in order to get people to answer your questions, you have to give them incentives.

To that end, one of the author's clients hosts a lottery each month. The lucky winner receives free lunches for a month. And to enter, you have to fill out a short survey.

Of course, you'll also need some sort of database to organize all this information. That doesn't necessarily mean you have to buy expensive software. Instead, you can organize comment cards and data from application forms, or make a simple spreadsheet containing all the information.

You can use this database to track RFM, which stands for _recency_ (when the customer last did business) _, frequency_ (how often they do business) and _monetary value_ (how much they spend). This system provides valuable insights, which you can use to extend promotions to customers with high frequency purchases or reach out to customers with low recency, to retain their business.

Ultimately, the main goal is to understand who your customers are and how they behave. So instead of boiling people down into one overly generalized stereotype, get real insights.

### 7. Strategically build loyalty programs that create stronger bonds between customer and company. 

Your wallet is probably crammed full of membership cards from various businesses. Most of these are attached to loyalty programs that give rewards for spending. But do these kinds of promotions actually make customers more loyal?

The most effective loyalty programs focus on customers who could potentially become more loyal, and less on those who are already devoted.

In most membership programs, customers collect points and there's no differentiation between different groups. But when there are no progressive incentives, companies run the risk of losing both top tier customers and those who have the potential to become more loyal.

Because in some cases, collecting points can even make customers feel underappreciated. For example, the Delta Airlines bonus system has been called "sky pesos" because of the low value of the points.

On the other hand, Starbucks' loyalty program is effective. Borderline-regular customers are the company's main focus, and so these groups receive more special offers than regulars. Hardcore Starbucks users still receive rewards, just not as much.

And in a lot of cases, the top loyalty programs actually require customers to pay a membership fee. For instance, Amazon's "Prime" service costs $99 a year and offers members free two-day shipping, full access to Amazon's Kindle library and other special rebates.

Similarly, the sports bar and eatery Jack's Gastropub has something called The Mug Club, a reward program for beer aficionados. Members are recognized with a brass plaque in the bar, their own special beer glass and an extra four ounces with every order. However, receiving that special level of service comes with a $79 annual fee.

In sum, loyalty is a relationship, which means both the company and the customer have to work toward it. But rethinking your approach to membership programs will allow you to build a stronger relationship with your customers.

### 8. Customer service is about supporting loyalty and sorting out unprofitable customers. 

Everyone has that one friend that's never happy, no matter how much time you spend with them. Maybe you feel it's a relationship that takes way too much energy, but doesn't give you anything back. Well, customer relationships can often work the same way.

In other words, problem customers use up more time and resources than they actually bring in. And since they will never be profitable, these customers should be fired.

Amazon does this, literally closing customer accounts if they return an excessive number of items. The retailer notifies the customers via email, telling them that since they seem to have more problems than Amazon can solve, the relationship must come to an end. And a special department within customer service handles any complaints about these decisions.

Telecommunications company Sprint was thinking along the same lines when it closed 1,000 accounts. The cell phone provider noticed that these customers were calling customer service to an extreme degree, always trying (and succeeding) to get reimbursed for fees. Sprint basically said that these people were defrauding the company.

All in all, good customer service isn't about being cowed by your customers — it's about setting and meeting expectations.

And that's why Southwest Airlines is widely recognized as one of the top airlines when it comes to customer satisfaction. They advertise themselves as a low-fare budget airline, and customers get exactly what they expect. And so they're satisfied!

Tackling expectations from another angle, FedEx identified eight weak spots (such as late deliveries and packages being lost) that would be most frustrating for customers. And after measuring which caused the most problems, the company started reworking its processes.

These are just a few new ways to think about customer service, but the basic assumption still holds: Customer service is about attracting customers who fit the character and community for your evergreen business. And the customers who don't fit won't support your business in the long term.

> _"It's okay to fire customers who are insatiable or abuse your staff." — Tony Hsieh, CEO of Zappos._

### 9. An evergreen business focuses on reducing attrition and recovering lost customers. 

Imagine sitting down at a restaurant and waiting 20 minutes just to get a menu. You'd probably leave, right? Losing customers is a risk for any business, of course — but you can minimize it.

More specifically, there are two types of attrition you can avoid: Business losses related to 1) company screw ups and 2) changes in customer habits.

To the first point, if you make a mistake, you can either fix the process or deliver an apology. And even if the relationship has ended for good, you can always improve the terms.

For example, a publishing CEO noticed that his company was losing subscription customers after a few months. The reason? These customers couldn't retrieve their passwords. A simple fix to the website code could make the difference.

In other cases, customers simply forget to do business with a company because their habits changed. But companies can avoid that by staying in touch with their customers.

For instance, when a customer who spent the previous year shopping at your store monthly doesn't visit for a couple of months, that's a warning sign. But the RFM system could act as an alarm system, alerting you when a customer is about to leave. That way, you can reach out with a special offer.

But even when you try to retain them, customers still sometimes end their relationship with you. When that happens, trying to recover them is easier and more profitable than acquiring new customers.

For example, one of the author's clients did a reactivation campaign targeted at former customers. It went out to 3,000 former customers at a cost of $1 per person. 140 people came back and spent $350 each, on average. That's a five percent response rate (when it comes to new customer acquisition, a two percent response rate is typical). Not to mention, the total spending of the reactivated customers amounted to $50,000, far above the $3,000 advertising cost.

> _"Without customers, there is no business." — Peter Drucker, consultant and author of Concept of the Corporation_

### 10. Acquiring new customers isn’t as important as building a relationship with existing ones. 

Imagine you're trying to fill up a bucket and you discover it has holes in it. What's best, plugging the holes or adding more water?

You should think about your business the same way: Your company is the bucket and customers are the water. And placing too much focus on new customer acquisition can actually be detrimental.

That's what the bakery Need a Cake experienced when it attracted 8,500 customers in a single day due to a Groupon promotion. That might sound great, but the bakery didn't have enough staff to handle the orders.

As a result, many people didn't get their orders and there was a severe drop in quality. This misstep cost the owner almost a year's worth of profit — and also some existing customers, who weren't happy about having to wait in line.

Instead of going for volume, the first step of engaging new customers should be leveraging the company's character and community. And often, the point of sale is a great opportunity to start building customer relationships that promote long-term profitability, but many companies miss that point.

_You_ shouldn't: When someone places an online order, follow up with something beyond a generic confirmation. You could include a video explaining your brand or a gift certificate showing off your customer character.

The point is, once a prospect becomes a customer, companies should change their focus: It's no longer about trying to sell, it's about building a bond. Introduce the customer to the community and get them to feel that they're part of something bigger, more exclusive.

All in all, you may need to rethink how you're attracting new customers. Because the new sale isn't the most important part of a successful business: If you have an evergreen company, those new customers are just new leaves on a blooming tree.

> _"The Groupon campaign was the worst business decision I ever made." — Rachel Browns, Need a Cake_

### 11. Final summary 

The key message in this book:

**Companies should follow the three Cs — character, community and content — to put customers first and build a strong evergreen business with long-term profitability.**

Actionable advice:

**If you want to foster a lively community, you have to learn to handle criticism.**

When you host an online forum for people to discuss your products, every now and then you'll be sure to see some criticism of your company. Don't try to flush out these negative elements! It'll make your company seem oversensitive and drive the rest of your community elsewhere.

**Suggested** **further** **reading:** ** _Customer WinBack_** **by Jill Griffin and Michael W. Lowenstein**

Maintaining a happy customer base shouldn't feel like herding cats. _Customer WinBack_ (2001) reveals how companies can identify at-risk clients and win them back before they disappear from their databases. By using these savvy tricks, businesses can also refocus on their existing customers in order to cut costs and drive revenue over the long term.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Noah Fleming

Noah Fleming is a strategic marketing expert. He blogs for _Fast Company_ and contributes to _The Globe and Mail_ 's business section. He's one of just 36 people worldwide who's accredited to teach Alan Weiss's Mentorship Program and Growth Cycle.

