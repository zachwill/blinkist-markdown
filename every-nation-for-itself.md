---
id: 56ebe4b570f3820007000014
slug: every-nation-for-itself-en
published_date: 2016-03-21T00:00:00.000+00:00
author: Ian Bremmer
title: Every Nation For Itself
subtitle: Winners and Losers in a G-Zero World
main_color: 2688BD
text_color: 0F4E70
---

# Every Nation For Itself

_Winners and Losers in a G-Zero World_

**Ian Bremmer**

_Every Nation For Itself_ (2012) discusses the consequences of the lack of international leadership we face today. With no nation economically fit enough, or even willing, to head the response to global challenges, we live in what could be called a G-Zero world; these blinks reveal how we got here, and what comes next.

---
### 1. What’s in it for me? Get key insights into the geopolitics of a leaderless world. 

Imagine a world without gravity, or a zero-G world. We would all be floating in the air with no force to govern us. Now imagine that the world is in the same sort of power vacuum when it comes to politics — a leaderless world, where no single country or group of countries is leading the world. Sounds pretty scary, right?

Well, that's actually the world we live in today. Instead of a zero-G world without gravity, it's a _G Zero_ world without any form of global leadership, be it from a single country or a group of countries like the G8 or G20, two groups comprising the strongest and wealthiest countries in the world. In essence, the world has been without global leaders since the 1990s. So let's explore the reasons we came to be in this state and the possible scenarios for the future of geopolitics.

In these blinks, you'll find out

  * the part food protectionism plays in a leaderless world;

  * why Brazil and Turkey are so-called pivot states on the global stage; and

  * what a Cold War 2.0 scenario would look like.

### 2. Tough domestic problems make nations reluctant to take on global leadership responsibilities. 

Imagine this: your debt is piling up, your job might be getting axed, and you've still got to get your broken car fixed. Is this the time in your life that you'd sign up to lead a community group? Not likely! 

Many countries around the world find themselves in similar situations today, facing domestic problems on an extraordinary scale. Even leaders of developed nations such as Japan and the United States are struggling to cope with enormous debt and an aging population. 

The United States borrows almost $4 billion a day to manage its national deficit and will continue to do so to get the situation under control. Pension and health insurance for the elderly and poor currently account for 40 percent of the country's entire budget. 

On the other hand, emerging powers such as Brazil, China, Russia and India have shown competence in managing domestic issues with focused strategies. China, for example, has focused on developing its middle class and a social security system for its 1.34 billion citizens. However, despite being seen as an economic superpower, China's per capita income is equal to just a third of Portugal's. 

On the whole, the demands of internal challenges have made countries reluctant to assume leadership roles internationally; and yet, all countries are faced with very pressing _global issues_. The 2009 Copenhagen Climate Summit illustrated the problems with this widespread apprehension. 

Established powers, such as the United States and France, pressured emerging powers, such as China and India, to commit to achieving binding targets for the reduction of greenhouse gas emissions. 

Emerging powers made the counter-argument that the Western world has polluted the earth for 150 years; established powers pointed out that the bulk of future pollution will be caused by emerging powers. The result? Neither old nor new powers were prepared to take the lead. 

Today, there is no single country or bloc of countries with enough political and economic power to create the change our world needs to tackle impending international problems. Find out the serious consequences of this trend in the following blinks.

### 3. Environmental issues around the world require a level of international cooperation that we simply don’t have. 

Like it or not, we live in a globalized and hyperconnected world, in which nations and individuals around the globe share interrelated and complex problems. 

The failure of nations to cooperate on environmental issues leads directly to food price shocks and protectionism. And it's not surprising why: food production is dependent on weather. Droughts, floods, and more extreme weather conditions caused by global warming pose serious threats to agricultural industries around the world, causing poor harvests, food shortages and rising food prices. 

From 2007 to 2008 the prices of corn, wheat and rice rose significantly. Russia and Argentina kept local prices low and restricted exports to safeguard their own domestic food supplies. This is called _food protectionism_, a tactic politicians use to avoid internal unrest due to a hungry population. 

Food protectionism is risky. It hampers economic growth, harms diplomatic relations and prevents international cooperation. Sometimes food protectionism manifests as import barriers that "safeguard" domestic agriculture. These barriers discourage cross-border competition in food industries, which in turn disincentivizes the use of innovative technologies to increase yields and provide food at a lower cost. 

After an outbreak of E. coli in Germany, German officials made the accusation that Spain was exporting contaminated cucumbers and Russia banned the import of all fresh vegetables coming from the European Union as a result. Yet it was ultimately proven that the Spanish cucumbers were not contaminated at all. The Spanish minister for agriculture asked for compensations for the false accusations. Unsurprisingly, diplomatic relations between the countries became strained after the ordeal. 

How can we describe a world in which cooperation fails and countries only have their own interests in mind? It's a G-Zero world, where the protectionist decisions of world leaders make our world less and less likely to survive impending global challenges.

### 4. In a leaderless world, the winners will be the ones who can stay flexible. 

A world without international leadership presents serious economic and political challenges in the coming years. Some nations may be flexible enough to thrive in our G-Zero world, and others won't. So who will be the winners and losers?

Countries that can create opportunities independently will thrive, unhindered by dependence on a single ally. Take Brazil, for example. It's a _pivot state_, one that has succeeded in building multiple profitable relationships with other countries without relying on a single one. While the United States was once Brazil's largest trade partner, it was replaced by China, Brazil's major partner for imports, in 2009. 

Turkey is another pivot state, and one that is invaluable to both the European Union and the United States. Turkey's per capita income is twice as high as China's and four times greater than India's. The nation also maintains the best trade relations with Israel of all countries in the Muslim world. 

In contrast, nations that behave as if the world enjoys a barrier-free single market led by the United States will be less likely to flourish in a G-Zero world. Take Japan for example. Today, Japan receives significant military and economic protection from the United States. There are still US military bases in Japan, and the United States represents Japan's interests. As China's regional power continues to grow, this relationship may soon prove ineffectual. 

Mexico is another nation that currently enjoys the economic protection of the United States. Its major sources of wealth stem from selling oil abroad and tourism, particularly from US citizens. Mexico's standard of living and financial situation is tightly linked to the United States' economic health. 

The past three decades have seen states that exploited Western-led globalization become the winners. But G-Zero is an entirely different world. Nations that can adapt to a leaderless world will stay afloat during this turbulent time. Of course, G-Zero won't last forever. The problems it produces make a global leadership solution inevitable.

### 5. Two forms of global leadership can emerge if China and the United States cooperate. 

The United States and China are two nations that will play key roles in determining the future of global leadership after G-Zero. Let's consider two possible scenarios where China and the United States decide to cooperate. 

A G2 world awaits us the first scenario: cooperation between China and the United States, with other nations having little influence. 

China and United States are currently tied together through commerce. America forms China's largest client, while China is America's largest creditor. From 1998 to 2007, China and the United States accounted for 40 percent of global growth. 

Together, these two nations have the power to tackle global challenges effectively. Other nations, such as France and Germany, will be reluctant to renounce their influence. But in a scenario like this where they are weak, they're left with no other choice. 

But what happens if nations other than China and the United States become powerful? This gives us our second scenario, a G20 world. 

China and the United States would cooperate, while other countries would exercise their influence as a concert of nations — one that actually works, unlike what we have today. However, this scenario requires the existence of a single threat great enough to impact all powerful nations at the same time, to the same degree. 

Take, for example, a cyber attack by an unknown source that breaks down power grids in the EU, the United States and China in rapid succession. All affected by the same threat, these nations would join forces to identify and neutralize the threat. Such a scenario is no doubt likely. What remains uncertain, however, is how long such cooperation would last. As soon as the common threat is dealt with, collaboration would probably end. 

There are, however, two further scenarios. These are what we'd see if China and the United States fail to cooperate. What would it mean for our world? Find out in the final blink.

### 6. If China and the United States do not cooperate, our world faces another cold war or regional separation. 

So what happens if China and the United States are not able to cooperate? Two scenarios could emerge. 

In the first scenario, other nations are weak and the United States and China do not work together. This could lead to a _Cold-War 2.0_ situation, where weaker countries align themselves with one of the two antagonistic leaders. 

This time, instead of the US versus the USSR, it would be the US versus China, with NATO countries and other Western countries aligning with the United States, while Asian and African nations would align themselves with China. 

As the United States and China have significantly greater military, economic and political power than other nations, they have the capacity to wage economic wars against each other. 

But the United States and China are, of course, bound together through their commercial relationship. Conflict between the two would harm both Chinese and US economies. This is a key difference to the Cold War, where the United States and USSR could hurt the other without damaging their respective domestic interests. 

In the second and final scenario, China and the United States fail to cooperate while other states remain strong. This would lead to a world of _regions,_ where every state only cares about itself and its internal problems. Some stronger states would rise to head responses to regional and transnational problems. 

For example, Brazil would emerge as a leader in South America as the region's strongest country both economically and militarily. Similarly, Germany might become a leader in Europe. With one of the richest economies in the region, it is unlikely that monetary issues in the EU could be solved without its leadership. 

These regional leaders would tackle global problems such as greenhouse gas emissions — but would do so on a transnational scale only, while ignoring the activities of other regional powers.

### 7. Final summary 

The key message in this book:

**Today's world is marked by a troublesome lack of global leadership. Though we face significant transnational and international issues, no single country or group of nations is willing or able to lead our planet toward sustainable solutions. This power vacuum is temporary, and will lead to a new form of global leadership in one of four forms.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _World Order_** **by Henry Kissinger**

_World Order_ (2014) is a guide to the complex mechanisms that have governed international relations throughout history. These blinks explain how different countries conceive of different world orders and how they are held in balance or brought into conflict.
---

### Ian Bremmer

Ian Bremmer is a political risk consultant and the president of the Eurasia Group, the world's top global political risk consulting and research company. His other publications include _The End of the Free Market_ and _The J Curve_.

