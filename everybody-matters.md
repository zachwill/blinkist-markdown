---
id: 56b8ae70d119f9000700003e
slug: everybody-matters-en
published_date: 2016-02-09T00:00:00.000+00:00
author: Bob Chapman & Raj Sisodia
title: Everybody Matters
subtitle: The Extraordinary Power of Caring for Your People Like Family
main_color: F0F07E
text_color: 4D4D28
---

# Everybody Matters

_The Extraordinary Power of Caring for Your People Like Family_

**Bob Chapman & Raj Sisodia**

_Everybody Matters_ (2015) chronicles Bob Chapman's quest to find the best approach to business and leadership style. Traditional approaches to management often treat employees like cogs in the machine. The authors explain that by caring for employees as if they were family, you can not only experience unprecedented success but inspire company loyalty and allow all employees to reach their full potential as well.

---
### 1. What’s in it for me? Care about your employees and watch your company soar. 

Does the company you work for really care about you? Sure, they might claim they do. But do they really? What do they actually do for their employees? It's likely that their caring is merely lip service. In reality, they probably do very little aside from providing the obligatory suggestion box.

As well as annoying their employees, these uncaring businesses are harming themselves. These blinks show you how one businessman transformed and strengthened his company by putting his employees first. By giving them the freedom to innovate and the room to be happy, he made the business a huge success. Read on, and perhaps you can do the same.

In these blinks, you'll learn

  * why employees value actions over promises;

  * what a responsible freedom is; and

  * why the author's employees loved to ride around in his convertible.

### 2. To be successful, care for your employees like family. 

How would you measure the success of a company? Your first instinct might be to simply look to the number of products a company sells and the profits it generates — and, without a doubt, these are important factors. But to be truly successful, a company should always put the well-being of its employees first and foremost. 

So how do you create the kind of environment that leads to happy employees?

It begins with strong leadership. But don't think of it as strictly managerial leadership; think of it as _stewardship_. This means letting your employees know that you're truly looking out for their best interests. You should move beyond simply acknowledging their productivity and actually take the time to inform them that you value their work.

You can do this by putting aside some time each week to talk to your employees face-to-face and express your gratitude for their dedication and the hard work they do. 

By doing this you're not only giving them a sense of security and fulfillment in their job. You are also literally benefitting their health. A recent poll of American workers showed that those who said they love their job spent 62 percent less on healthcare than those who hated their job.

This leadership role is similar to good parenting. Being a parent means constantly checking in with the kid, verifying they are healthy and in good spirits. An employer should do the same by regularly asking employees some simple questions:

Do they feel secure? Do they feel fulfilled? Is their life meaningful to them?

When you consider these questions yourself you will realize the degree to which you're responsible for your employee's happiness. After all, it's not just a business you're building — it's also a home, a place that both you and your employees need to feel safe and secure in.

### 3. A company charter, with input from employees, can help create trust in management. 

A happy employee is someone who trusts company management. One way to foster trust is to craft a _company charter_ : a motivational mission statement for the values and goals of the company. But be careful; a company charter that doesn't value its employees can leave people feeling unhappy and underappreciated.

The first step toward creating an impactful charter is reaching out to your employees.

Co-author Bob Chapman, CEO of the manufacturing equipment supplier Barry-Wehmiller, knew the importance of sitting down with his employees when he wrote his company's charter, "Guiding Principles of Leadership." He asked everyone for input, using the opportunity both to learn about his employees' concerns and to acknowledge and honor their opinions.

For instance, many employees expressed concern that there might be a lack of trust between the employees and the management. So Chapman decided to prominently advocate creating an environment of trust at the top of the charter.

But of course, actions speak louder than words. 

Chapman knew that once the charter was in place he would have to back those words up and prove to his employees that he meant what he wrote. An opportunity presented itself when he caught wind of an employee who was annoyed that there were set times for bathroom and coffee breaks.

The very next day, Barry-Wehmiller set about dismantling all the company practices that could be viewed as management not trusting employees — such as set break periods and time clocks. Employees understood that the company was listening to them and the experience helped forge a bond of trust between them and management.

### 4. Empower your employees by giving them freedom and trusting them to make their own decisions. 

Have you ever worked for a company that was run by a dictatorial management? When companies rely on oppressive rules, they usually end up with unhappy employees. Edicts from above not only betray that important sense of trust but also hamper creativity, individuality and any motivation to be innovative. 

To avoid this outcome, leadership should instead cultivate an environment of _responsible freedom_, designing the workplace so that employees can reach their full potential.

At Barry-Wehmiller, the company fosters this kind of business environment with what they call the "_just enough_ " method. This is a goal-oriented approach to business that gives employees a clear idea of what "winning" is and what they should be working to achieve.

For example, a customer-focused company might define winning as scoring the best possible result on a customer satisfaction survey. And once success is clearly defined, employees then have the freedom to achieve that goal using the skills and talents that make each employee special. 

Empower your employees by trusting them and allowing them to use their own judgment to achieve goals and carry out tasks.

When Barry-Wehmiller adopted the "responsible freedom" approach, a team of employees was allowed to choose a new laser-cutting machine. It might sound like a simple task, but, actually, this was the biggest purchase in the history of this department!

The employees ended up embracing this responsibility enthusiastically, even working on weekends to do research and eventually overseeing the installation of the machine. They were so thorough that when a representative came over to teach them about the machine, they already knew more about it than he did!

This empowering approach received praise when a group from Harvard Business School toured the company. The story of the laser-cutter proved to be the inspirational highlight of their visit!

### 5. Motivate your employees with a strong cultural and business vision. 

Another common reason for unhappy employees is working for a company that is going nowhere. To cure this stagnation, and get your company growing and your employees happy, start _visioning_!

Visioning is a way of creating a path toward your company's future. Think of this process as shining a spotlight on where your company wants to go.

To get started, focus on answering these two important questions: Where is my company going? And why is it going there?

This second question pertains to the "higher purpose" of your business and the answer should reflect a desire to improve the lives of your employees. This is called _cultural visioning_.

When the consulting company Design Group did their cultural visioning they prioritized maintaining an inclusive work environment over increasing the size of the company. By following this vision the company made sure that their employees would continue to come to work inspired and happy.

But cultural visioning is only half of the solution. To maximize your chances of success, you've got to add a powerful business vision.

Now it's time to turn your attention to the question of where you want the business to go and how you want it to get there.

Design Group eventually realized that without a business vision in place, employees still felt that company momentum was lacking. So the company decided to add a bold business vision and created a goal of doubling the organization's size within five years.

With the announcement of both the inclusive cultural vision and the ambitious business vision, the employees of Design Group felt motivated and happy to be part of a growing and exciting company with a bright future.

As it turned out, with their visioning in place, it took the Design Group only three years to double in size!

### 6. To bring your team closer together, regularly celebrate their achievements. 

No one likes to see their hard work go unrecognized. A happy employee is one who gets acknowledged by their colleagues for their efforts. Every successful business leader should keep this in mind.

Barry-Wehmiller knows the importance of recognizing good work. They've created a system for employees to nominate their peers, instead of having management bestow awards. Nominees all receive letters of acknowledgment, praising their good work — even if they don't end up winning an award.

The employees put so much thought and care into writing the nomination letters that award winners have come to appreciate these letters more than the awards themselves. The praise that employees receive from co-workers leads to higher levels of motivation, often inspiring improvement.

This kind of personalized award system works much better than handing out money. A monetary incentive might help with aligning a company's goals with those of an employee's (for example, through sales-based bonuses), but personal and creative acknowledgements are more meaningful and carry more of an impact than money. Ultimately, the purpose of an award should be to affect the life of the recipient in a way that will be uniquely memorable.

When Barry-Wehmiller began their peer-nominated award program, the CEO decided to make one prize the chance to drive one of his luxury convertibles for a week. Talk about establishing trust!

Sometimes even family members get in on the action of the award ceremony. One employee was surprised when his family showed up, and explained that the award was made all the more special by the company inviting them to take part.

This is why the award program is still in place at Barry-Wehmiller today and why the award ceremony continues to be one of the most important celebrations at the company: it's not just about the award itself; it's about how the company can make its employees feel appreciated and proud of the work they've done.

### 7. Final summary 

The key message in this book:

**Caring for your employees as you'd care for your family will not only transform your business; it will also transform your employees' lives. By freeing them from oppressive rules and regularly celebrating their achievements, you can empower them to reach their full potential.**

Actionable advice:

**Make sure you honestly and actively listen to your employees.**

These days, it can be hard to put your own work aside and really give employees 100 percent of your attention. Being present and honestly concerned about what your employees have to say will not only allow you to better understand their thoughts and concerns; it will also contribute to a more open and inclusive atmosphere where all members of your company feel they can express themselves and be heard.

**Suggested** **further** **reading:** ** _Leaders Eat Last_** **by Simon Sinek**

_Leaders_ _Eat_ _Last_ explores the influence that neurochemicals have on the way people feel and consequently act, and examines the discrepancies between how our bodies were designed to function and how they function today. Ultimately, we need true leaders to direct us back on the right path.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Bob Chapman & Raj Sisodia

Bob Chapman has been CEO of Barry-Wehmiller, a global manufacturing company, since 1975. Thanks to his leadership, his company is valued at $1.5 billion.

Raj Sisodia is an author, corporate consultant and coveted keynote speaker. He's the professor of Global Business at Babson College, and he also co-authored _Conscious Capitalism_ with John Mackey, co-founder of Whole Foods.

