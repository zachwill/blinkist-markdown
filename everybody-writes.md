---
id: 552be9406466340007e10000
slug: everybody-writes-en
published_date: 2015-04-16T00:00:00.000+00:00
author: Ann Handley
title: Everybody Writes
subtitle: Your Go-To Guide to Creating Ridiculously Good Content
main_color: CC553D
text_color: 99402E
---

# Everybody Writes

_Your Go-To Guide to Creating Ridiculously Good Content_

**Ann Handley**

_Everybody Writes_ (2014) gives you invaluable advice on how to create great content, from using correct grammar to crafting engaging posts, tweets and emails. With just a handful of simple rules, these blinks will help you gain a better understanding of how to use the right words to keep customers coming back for more.

---
### 1. What’s in it for me? Learn why being able to write well is more important now than ever before. 

We live in the age of the smartphone, the viral video and social media. When a picture says more than a thousand words, who really needs to write anymore?

Yet writing well is now more important than ever. No matter how technologically brilliant or beautiful your company website or blog may be, if what you've written is dreadful, no one will stick around to read a word.

To write well, you need to dedicate time and attention to your craft. These blinks provide some handy hints and simple rules to help you not only improve your daily writing but also win customers with your words. And who knows? Perhaps you'll become the Shakespeare of our digital age!

In these blinks, you'll discover

  * why an "ugly first draft" is no reason for despair;

  * why writing less is better than writing more; and

  * why your "About Us" webpage isn't really about you, but about your client.

### 2. If you’ve a message you want to get out to the world, you need to know how to write it well. 

Do baffling text messages make you LOL? IMHO, we might all agree that #writingisadyingart.

But the opposite is in fact true. Today, writing well is a skill you cannot do without.

We write every day. Whether typing out an email, commenting on a blog or posting on Facebook, with the internet, words have become both personal and company currency.

This is why you need to pay particular attention to how you use your words.

On a homepage, for example, a company has a great opportunity to find customers and present itself in an interesting way. But if a company treats the words on its website as an afterthought, its success is threatened.

Companies that fill their websites with jargon or unclear language will find plenty of customers simply turning away.

Given that writing is so important, how can you make sure your scribbles are up to scratch? Can anyone write, or are some people just naturally gifted wordsmiths?

The good news is that we can all write well, if we follow a few simple rules.

Before you even put pen to paper, start by identifying your goal. What's the point of your text, and how can you find data to prove that point? Once you know what you need, go find your data.

Say you have to write an article about a restaurant opening. First, think about who is going to read the article: who is the audience? In this case, people who like to eat out, perhaps families or couples. Then search for your data: specific details about the restaurant and the menu, and importantly, the date when it will open. Now you're ready to write!

### 3. Your first draft will be ugly; embrace it! Then take the time to edit it, paying attention to word choice. 

So the first step to writing is defining your goals. Curiously, many writers don't get past this first step.

Why? Because _starting_ to write is incredibly hard!

The reason so many of us suffer from writer's block is because we expect too much of ourselves from the start. You just can't grab a pen and compose a masterpiece; it's just unrealistic.

Writing is a process. You begin with something rough, and polish it as you go.

So don't be intimidated by _TUFD, or the ugly first draf_ t. It's merely the lump of dirty marble from which your masterpiece will be eventually revealed!

Let's say you have to write a film review and you don't know where to start. The movie addressed a complex subject, and you're afraid you won't be able to perfectly address every element.

As you start, just note down your ideas as best you can, knowing that there will be gaps in your text, where you can write "insert example" or "need to add more." Once you do this, you'll start to get a feeling of the strengths and weaknesses of your very first draft, and what you need to do next.

So what is the next step? Once you have an ugly first draft, what sort of editing needs to happen?

There are two approaches you can take when editing your drafts.

With _developmental editing_, you take a broad view. You look for words which may be misunderstood or phrases which distort the general idea of your text. This is an important process, as sometimes you may contradict yourself or choose words which could mislead the reader.

With _line editing,_ however, you hone in on more specific aspects of your text. You comb through every line and delete words that don't fit or that are redundant or cliched.

### 4. To keep a reader, you need to grab him early. Get to the point, and quickly. 

The most important part of any sentence is how it starts. Mess this up and there's a slim chance your reader will read on.

So how do you start a text with a bang?

To grab the attention of as many people as possible, be sure to place the most important or striking words or ideas at the start of each sentence.

The first sentence shapes the first impression the reader will get of your text; it should entice them to continue reading. Aim to place the reader into the center of the story, and immediately!

Let's look at the first sentence of a text published by the Centers for Disease Control and Prevention.

"According to the National Assessment of Adult Literacy, released in 2006 by the U.S. Department of Education, 30 million adults struggle with basic reading tasks."

Now, did that sentence grab you? Were you engaged, and eager to read more?

Let's try again.

"Thirty million adults struggle with reading, according to the National Assessment of Adult Literacy report, released in 2006 by the U.S. Department of Education."

Much better!

Often when crafting a sentence, we give too much background before getting to the point. This is called a _running start,_ and it must be avoided.

Background information is useful, but most of the time it's used to pad a long introductory paragraph. The problem is, the reader doesn't appreciate this; they want to get directly to the point.

Let's say you have to write an article about using YouTube to market a business. You could start with a rambling text on the growing importance of social media over the past few years. Or, you could get to the point, stating in one concise sentence that while companies have succeeded in reaching customers via social networks, only a small fraction have explored the possibilities of YouTube.

### 5. Don’t just post to Facebook when it’s convenient for you. Post when your fans are paying attention. 

You can't be a successful company without a Facebook page; yet just having a Facebook page doesn't mean you'll be successful.

If you want to boost sales via Facebook, you have to already know your potential buyers, as it's easier to grab new customers if you tailor your posts and ads to a particular audience.

The vast amount of available demographic information, such as gender, income and ethnicity, coupled with behavioral information, such as how people react to certain topics and pictures, make it easier to effectively address a particular target group.

Prep Obsessed, a preppy accessory boutique, began using Facebook in April 2013. The founders invested $40 a day on Facebook ads and in analyzing their followers. In nine months, the company collected more than 55,000 fans and an actively engaged audience, with a cost of acquisition of less than 10 cents per person!

Timing is also vital. Be sure to post when your audience is online, not just when your business is open.

People spend the most time on Facebook on Fridays, on the weekend or in the evenings — that is, when the "normal" business day is over. Therefore, you can reach the majority of your audience if you post or blog during this time.

Furthermore, you can use the information you have about your followers to promote your company image by using specific language that will draw them in.

For example, Prep Obsessed addresses their posts to _ladies_ (not girls or chicks, for example) suggesting gentility and refinement. This reinforces the company image of being a classy brand.

So the next time your company posts something on Facebook, think of a unique or specific greeting to make your post stand out above the flood of other Facebook posts!

### 6. The best “About Us” page is not about your company but about your relationship to the visitor. 

A company's "About Us" page is often chock-full of stereotypes and self-serving facts. Yet your company's page doesn't have to follow this flawed model.

On your "About Us" page, you should always explain exactly what you do for your customers.

Sure, the page can include the who's who of your company, but subconsciously, your readers really want to know who is actually providing the services behind the flashy design and 800-number, to see whether they will become a potential customer.

For example, the "About Us" page for toy company Toys "R" Us is lacking. It presents a lot of text with an uninspired design, not to mention boring pictures.

Coca-Cola, on the other hand, does things differently. The company refers to its history, displays a variety of lively images and shows up-to-date reports about its community and so on. All this makes the page appear lively, fun and relevant.

Be sure to show your company's human side. Although your text should use a tone that suits your company's image, with the aid of videos, photos or pull quotes, you can make your page more active and show that your employees are real people!

### 7. Final summary 

The key message in this book:

**You are a writer, and you write more than you think! Importantly, your words affect your readers and your customers, for better or worse. The good news is, anyone can write well and draw in readers by following a handful of rules.**

Actionable advice:

**Begin your writing life now.**

Don't wait to start practising your writing skills. Whenever you post on Facebook or Twitter, or send an email to a customer, take the opportunity to hone your writing skills.

**Kick the school essay habit.**

Forget that five-paragraph essay you learned in high school. These conventional writing methods are not only boring to write but also they're boring to read.

**Suggested further reading:** ** _Bird_** **_by_** **_Bird_** **by Anne Lamott**

Told from the personal perspective of author Anne Lamott, _Bird_ _by_ _Bird_ is a guide toward becoming a better writer and improving your life along the way. Lamott's distinctive approach, honesty and personal anecdotes make this book a must for writers or anyone who wants to become one.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ann Handley

Cited in _Forbes_ as the most-influential woman in social media, Ann Handley is a columnist for _Entrepreneur_ magazine and the chief content officer of _MarketingProfs_. She is also the co-author of the bestselling book, _Content Rules_.

© [Ann Handley: Everybody Writes] copyright [2014], John Wiley & Sons [Inc. or Ltd. as applicable] Used by permission of John Wiley & Sons [Inc. or Ltd. as applicable] and shall not be made available to any unauthorized third parties.

