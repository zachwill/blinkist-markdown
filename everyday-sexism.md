---
id: 58261f1a7029760004d5ac9e
slug: everyday-sexism-en
published_date: 2016-11-14T00:00:00.000+00:00
author: Laura Bates
title: Everyday Sexism
subtitle: None
main_color: 25B5B9
text_color: 146466
---

# Everyday Sexism

_None_

**Laura Bates**

_Everyday Sexism_ (2014), explores why sexism is so deeply entrenched in society, from sexual assaults against women to the stark differences in the ways boys and girls are raised. These blinks show how sexism is harmful not only for women but also for men; and you'll learn how to combat sexism to create a more peaceful, equitable world.

---
### 1. What’s in it for me? See clearly the sexism that persists in society every day. 

We live in a progressive society. Compared to our parents' struggles, many of us live our lives free from discrimination or social humiliation.

Are these statements true? Not at all.

Discrimination is still a serious issue, and sexism is rampant. For most women, not a day passes when they don't have to suffer sexist catcalling, leering or groping, or worse, assault. 

These blinks show you some of the most common expressions of sexism and offer ways in which we as a society can stem it, if not stop it outright.

In these blinks, you'll find out

  * how women in leading positions are characterized;

  * what celebrity's gaining weight tells you about sexism; and

  * how sex education can combat sexism.

### 2. Sexist behavior exists everywhere in society, but it remains a largely unreported, invisible problem. 

It's rare that a day will pass without another headline about sexual violence against women or statistics that show the vast income gap between men and women.

Yet some people still argue that sexism is a thing of the past, despite plenty of indications that sexism is not only still present but ubiquitous.

Sexism comes in many forms. Millions of women are sexually assaulted, raped or killed yearly because of their gender. Sexism can also be a statement about a woman's unsuitability for a certain job, or derogatory comments made about a woman's body.

To collect stories of both major and minor sexual aggression, the author created a website named _EverydaySexism.com._ Here people can anonymously share their experiences of sexism.

Within one month after launching the site, the author had already collected hundreds of entries.

One shocking story was of a young woman who witnessed a man masturbating while looking at her on the subway. Worse, the woman explained that the other subway riders simply turned a blind eye, choosing to ignore the situation rather than assist in any way.

This is an extreme case of sexism in society. Other times, sexism can be hard to detect.

Why is this the case? Our society is inherently sexist, and as we've grown up, many of our thought patterns based on sexist ideas have become normalized. Many men often don't realize or even care if their behavior is sexist; women often fail to recognize sexist behavior for what it is.

Consider this: when a woman is attacked, people often say, "She was asking for it, wearing a skirt like that." Blaming the victim, instead of critically examining a perpetrator's actions, is typical sexist behavior.

> Only 27 percent of women who experience sexual harassment at the workplace report it to a supervisor.

### 3. Women in politics or leading positions in the business world suffer sexist behavior daily. 

For most of history, men have dominated the political arena. Politicians such as American presidential candidate Hillary Clinton or German Chancellor Angela Merkel are beginning to challenge the status quo, but despite this, female political leaders often face sexism in their positions.

The media often pays undue attention to what a female politician is wearing or how her hair is styled for an event — facts deemed irrelevant or outright ignored when it comes to male politicians.

People also complain when female politicians speak assertively. Hillary Clinton is often criticized for her tone of voice, whether for speaking too loudly or not sufficiently "lady-like." Male leaders, meanwhile, are praised for being firm, strong and loud — a double standard if there ever was one!

Female politicians are even disrespected by their colleagues. In the French National Assembly, when the female housing minister, wearing a dress, stepped up to the podium to speak, male members of the assembly began to whistle and catcall.

Another concern is that issues often fought for by female politicians, such as stronger policies over paid family leave, are frequently called "women's issues" and thus dismissed as unimportant.

The fact is that only about 20 percent of politicians worldwide are female. This deficit creates a culture that's inhospitable to future female politicians, creating a vicious, self-fulfilling cycle.

It's not just women in politics who are mistreated, of course. Women in all industries and all leadership positions face sexism. No woman is ever truly protected from sexual assault, sexist comments or outright threats.

To make things worse, many women fear losing their jobs if they report such incidents.

Consider these statistics: one out of eight women in the United Kingdom has left a job for reasons involving sexual harassment. And over 60 percent of women have experienced unwanted sexual advances from a male colleague, yet felt they couldn't report it for fear of putting their career at risk.

> _"At the time of writing, just 19 of the world's 196 countries have female leaders."_

### 4. Media images of female beauty can make many “normal” women feel bad about themselves. 

You don't have to see many films or advertisements to realize that thin white women with blue eyes and blonde hair are society's standard of beauty. Such images are everywhere, showing women that this is the ideal look for their gender.

But what if you're a woman who doesn't look like this?

You'd be hard-pressed to find a curvy, gay woman from South Africa, for example, on a billboard. Women who don't fit specific beauty standards are severely underrepresented in media imagery.

This is a serious problem. Many women feel bad about themselves precisely because they never see anyone who looks like them in magazines. Unfortunately, human diversity — not to mention individual style — is often portrayed as a negative, instead of a positive, reality. 

Part of the issue is that most stories are told from the perspective of a straight, white male, whether in a television series, magazine article or Hollywood movie. 

Women directors are behind the camera of just ten percent of all Hollywood movies. As a result, many films offer simplistic, one-dimensional portrayals of women. Women are frequently superficial "side" characters, while women of color are often cast as a "token" black or Asian woman.

Prominent female stars also struggle with the gender pay gap and face a seemingly constant stream of sexist media criticism.

If a female star gains a few pounds or breaks up with her significant other, for instance, the media has a field day. Remember Britney Spears's breakdown, when she decided to shave her head? The media pounced on the "news," as such instances are the bread-and-butter of the tabloid world.

### 5. Sexism in the media is especially damaging for young girls and teenagers; education can help. 

How do you think sexist tabloid stories impact the girls who read them? If you want your daughters, sisters and mothers to feel secure and confident about themselves, you need to be aware of sexism in the media.

Young girls are especially vulnerable to sexist expectations, given the onslaught of unrealistic body images they see and are taught to want and worship. Photoshopped images of sexualized young women are commonplace in advertisements. Such standards can trigger both mental and physical illnesses in women, such as anxiety or anorexia.

Girls are taught from a very young age that beauty is essential, as echoed in the compliments they receive about how pretty they are and reflected in the hypersexualized dolls they play with.

Girls as young as five years old worry about their weight and suffer from a warped sense of what a "normal" body should look like. By the time girls reach their teens, nearly half have already dieted.

When it comes to sex, things are even more confusing.

Despite overly sexual images in magazines, television and movies, girls are taught that one should not act like or dress like a "slut." The disconnect between what they see — beauty and sex — and what they're told to value — chastity and virtue — leaves girls confused about sexuality and identity.

Sex education can be a healthy, important way to teach children how to combat sexism, but unfortunately, it is not a priority for most schools. 

In the absence of rational and comprehensive sex education, pornography is often a boy's main source of learning about sex, relationships and intimacy. In general, both boys and girls are left in the dark about sexual safety or sexual pleasure.

A lack of open, honest discussions about sex only perpetuates sexist beliefs, such as that sexual groping, catcalling and even assault are just "things that boys do."

In worst-case scenarios, female rape victims blame themselves for dressing "inappropriately" or somehow "enticing" a man to pursue sex without her consent.

### 6. Women in the workplace are denied promotions and suffer from blatant sexual harassment. 

Do you feel safe and respected at work? If you're a man, you're more likely to answer yes.

If you're a woman, however, the situation is often very different.

Women commonly face sexual comments, groping and generalized harassment in the workplace. This is especially true when a man is in a position of authority over a woman.

One woman on EverydaySexism.com described how she was repeatedly harassed by a manager. When she complained to a manager higher up, _she_ was fired — and the harassing manager kept his job.

Many women suffer in silence while coworkers or bosses make sexual advances. If a woman does speak out, managers often fail to address the situation constructively, or view it as just another case of a woman who can't "take a compliment."

Sexism can also be observed in hiring practices, the shortage of promotions for female workers, and the gender pay gap.

Women are hired to positions in fewer numbers than men, in part because males are often seen as more dominant and serious workers. Also, don't forget that our sexist upbringing has conditioned many of us to equate authority with a male figure, so we often hold a bias against female leaders.

Another reason women aren't hired or promoted as much as men is pregnancy: many employers expect female workers at some point will leave work to start a family. Women between the ages of 28 to 35 are often overlooked for promotions as employers would rather avoid the inconvenience of managing maternity leave.

Once many mothers return to work, they're often told they have "baby brain." This is a blatant way to belittle women and say they're no longer able to be productive workers because of motherhood.

All of these sexist issues manifest in the gender pay gap. In the finance sector in the United Kingdom, for instance, there is as much as a 55 percent difference in wages between male and female workers!

### 7. Sexism can be harmful to both men and women; many even suffer from double discrimination. 

While women are the main victims of sexism, boys and men suffer the effects of sexism, too.

The narrow definition of how a "man" should act is damaging for both men and women. Many men are taught to be tough, unemotional high-achievers — and in doing so, also learn to objectify women. Many men also prioritize a career over a relationship, which often comes at the cost of a meaningful family life.

It's common for boys to be taught that being emotionally vulnerable makes one a "sissy," and that certain toys, like dolls or ponies, are "not for boys."

Boys are often insulted for "female" traits, such as crying or being sensitive, and learn early that fighting for dominance is the "correct" way for them to play.

Such conditioning affects the way boys behave once they are grown. Children raised by adults who act in sexist ways, especially boys, inevitably fall into similar patterns of behavior.

Women and men can also suffer from _double discrimination_, in which people are discriminated against based not only on sex but also on sexual orientation, disability, body shape and/or race.

As a rule, heterosexual, white, non-disabled, cis-gendered men hold the most privilege in modern society. (Cis-gendered means a person whose gender identity aligns with the person's gender assigned at birth.)

If you fall from this norm, life can present plenty of challenges. Many people on the LGBT+ spectrum report being victims of constant harassment, sexual assault or even death threats. People with disabilities, meanwhile, are often viewed as infantile and unable to achieve much in life.

When fighting sexism, you can't exclusively focus on just one issue or gender. The factors that inspire one population's abuse are echoed in another group's marginalization. This means we need to rally together to create a society that's more accepting and loving of everyone.

So if you see someone being treated unfairly, speak up! Remember we're all in this together.

### 8. Final summary 

The key message in this book:

**Sexism is harmful to society. It inspires violence against women, perpetuates stereotypes of people of color in the media, and preserves the dominance of straight, white males in politics and economics. It's time to fight sexism by starting a dialogue about gender binaries with each other and our children.**

Actionable advice:

**Don't encourage children to follow gender stereotypes.**

If your son wants to play with dolls or wear a pink dress, let him! And don't pressure your daughter to be more "girly" when all she wants to do is perform science experiments or climb trees. Allow your children to express their character in a way that feels comfortable to _them_. When we push children to conform to certain hypocritical and often arbitrary standards, we perpetuate sexism.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Men Explain Things To Me_** **by Rebecca Solnit**

_Men Explain Things To Me_ (2014) is a collection of essays that examine the range of misogyny in our culture, from everyday microaggressions to legal systems that fail to punish rape. Solnit explains how sexism perpetuates itself, and what we can all do to eliminate it.
---

### Laura Bates

British feminist writer Laura Bates founded the _Everyday Sexism Project_, a website that allows women to share their experiences of sexism in society.

