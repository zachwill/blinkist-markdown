---
id: 551898f93961320007380000
slug: everything-connects-en
published_date: 2015-03-31T00:00:00.000+00:00
author: Faisal Hoque and Drake Baer
title: Everything Connects
subtitle: How to Transform and Lead in the Age of Creativity, Innovation and Sustainability
main_color: EE3030
text_color: A12020
---

# Everything Connects

_How to Transform and Lead in the Age of Creativity, Innovation and Sustainability_

**Faisal Hoque and Drake Baer**

CEOs practicing meditation, the challenges of corporate politics, social differences between team members and customer satisfaction: it's all connected. Using a holistic approach, _Everything Connects_ (2014) will help you transform your working environment into one that understands such connections and supports as well as increases creativity and innovation.

---
### 1. What’s in it for me? Find out how to navigate today’s interconnected business world. 

What does a CEO and a secretary, a programmer and a creative director have in common? Everything.

In today's business environment, everything is connected. How successfully your company connects with not only your customers but also your employees and suppliers is the key to managing rapid market change and staying ahead of the competition.

In these blinks, you'll discover a holistic approach to managing the ever-increasing demands of today's market by understanding how to strengthen these connection points and build a company comfortable with innovation and inspired by creativity.

After reading these blinks, you'll know

  * why book retailer Borders went bankrupt;

  * why a mindful CEO is better than a bossy one; and

  * how a poorly designed office space will kill productivity.

### 2. In this modern economy, you need to be innovative and deal with change today or you’ll perish. 

Much like a surfer rides a wave, a company must ride the waves of economic change or it will go under.

In 1942, economist Joseph Schumpeter dubbed modern capitalism an _innovation economy_, with "violent bursts and catastrophes" as well as revolutionary inventions. He described how products and companies are constantly swept away and replaced by new ones, calling it "creative destruction."

To prevent being swept away, a company must innovate. Schumpeter argued that in an innovation economy, the market isn't directed just by efficiency but by innovations that can alter entire markets.

The old model of success had companies offering the best product for the lowest price. Today, while one company works to create a cheaper product than the competition, another firm is instead developing an invention that makes that original product obsolete.

For example, Nokia was an efficient producer of mobile phones, yet the company fell behind and lost customers once smartphones were introduced.

A successful company not only needs to invent things to change the market, but also be able to constantly adapt to the changes in the market.

Yet it's not just technology that is changing; we do things differently now, too. In many industries, a company that cannot recognize and adapt to the latest developments is going to fail.

Borders was once a major player in the retail book market. The company however failed to acknowledge the changes inspired by the internet, and chose not to enter the market for ebooks.

Other book retailers were able to adapt and thus stayed alive, while Borders went bankrupt.

> _"We have to assume that everything we think is right today will be wrong tomorrow."_

### 3. Increased, intricate connections are the norm of our modern business world. 

Innovation is a constant process and is happening all around us. Not only with smartphones or the internet but also with data systems and airplanes.

All this innovation has led to increased, intricate connections. Businesses are now interconnected within networks that transcend a single company, and are joined to an international web of suppliers and intermediaries.

This web is evident, for example, in how a single can of Coke is produced. Aluminum sourcing and processing happens in Australia; coca leaf harvesting happens in South America; vanilla is extracted from Mexican orchids; and on and on.

Connections are also appearing in ways that were previously impractical or impossible. The internet and social media, for example, allow customers to give direct feedback to companies.

Product design is another area where the type and strength of connections are important. Programmer Mel Conway in the late 1960s said that, as a rule, companies will design systems that mirror their own organizational structure. While an abstract idea, we can apply this concept to many organizations and products.

Let's say an IT company has two software designers named John and Nancy. John and Nancy dislike each other so much that they rarely talk. If each have to develop one of two software modules, the chances of them interacting efficiently or clearly when it comes to producing the final product is poor; and thus the final product itself will probably be poor.

What's more, the greater the number of employees working on a certain product, the more complex that product can potentially become. For instance, if you're compiling a medical diagnostic tool and a ton of specialists are contributing expertise, it's possible your tool will become quite complex, reflecting the variety of expert input received.

So we are all connected: leaders and employees and processes and products. But what can you do to make sure your business survives in such an interconnected world?

> _"The underlying architecture of our success, then, is the knowledge that everything connects."_

### 4. Practicing mindfulness opens doors to innovation, and focuses your actions for the better. 

Let's look at some techniques you can employ to ensure your company in an interconnected world continues to thrive.

Do you know about mindfulness, a practice that helps you focus on the present moment? Did you know practicing mindfulness can help you become a better leader?

Mindfulness heightens creativity and focus by allowing you to concentrate on the present without preconceptions or bias. This state of mind is conducive to creative work, as once you can see a subject or a situation in a new light or from an unbiased viewpoint, innovation follows.

Studies have shown that mindfulness and meditation also improve the quality of your thinking. One University of California study found that practicing meditation boosted working memory and reduced the brain's tendency to wander.

In business, mindfulness helps you ascertain whether your actions are out of alignment with your company's goals.

For example, a manager may want to narrow his store's product range, but he can't attend a trade fair without succumbing to the temptation of buying new products.

Being mindful teaches you to perceive a situation without immediately acting, helping you create some space between experience and reaction. Mindfulness also helps you perceive your own feelings more clearly, enabling you to understand the motivation behind your behavior.

So in being more mindful, the manager could attend a trade fair without giving into the urge to buy new products.

And thanks to his mindfulness practice, the manager may realize instead that he's actually bored with his daily routine, and that he craves the novelty of new products. With this understanding, changing the situation isn't difficult; he can plan to revamp his store by selling only the most interesting, novel items, for example.

### 5. Employees need to feel safe and secure to be creative and innovate. Don’t encourage “lizard brains.” 

Can noisy ventilation shafts in an office affect the creativity and productivity of your employees?

They certainly can!

Before we see why, first let's take a quick look at how humans evolved. Importantly, humans gradually developed multiple levels of consciousness, two of which we'll examine here.

First, there's what researcher Stephen Porges calls the "lizard brain," the part of our mind concerned with protection and survival **.** Our lizard brains scan our surroundings for indicators of danger, such as something burning or someone shouting angrily.

Second, there's the "mammal brain," a phylogenetically newer part that is concerned with connections. This level of consciousness is involved with social behavior, enabling us to coexist in groups, raise children, play with others and exchange knowledge.

It stands to reason that in the business world, you want your employees to use their mammal brain over their lizard brain!

Employees who have an overzealous lizard brain aren't particularly cooperative. They are also not particularly innovative, as innovation means change, which is scary for a lizard brain!

In contrast, when employees use their mammal brain, they're more likely to exchange ideas and come up with new connections, which as a result makes them more creative and cooperative.

Interestingly, a company's working environment can determine which "brain" is dominant and consequently, how cooperative and creative employees will be.

So eliminate things in the workplace that could trigger employees' lizard brains. Things such as noisy ventilation shafts or flickering lights create an uncomfortable environment that might make employees uneasy.

Also, defensive or dismissive managers, unhealthy competition within groups or poor job security all create an environment where an employee feels unsafe, thus allowing his lizard brain to dominate.

When we feel safe, however, our lizard brain stays in the background and our mammal brain is free to take control.

### 6. You can nurture creativity, but not force it. Treat your employees like people, not just resources. 

Farmers grow crops; but do they actually _make_ them grow? Not exactly. A farmer ensures the environment is just right for a plant to thrive, preparing fertile ground for a crop's seeds.

Much like farmers, managers can't enforce creativity, but they can certainly cultivate it.

Creativity doesn't happen just because a manager orders her employees to be innovative. Given the right environment, staff will develop the necessary motivation and creativity on their own.

If a manager interferes with or micromanages her employees' creative processes, all she'll end up doing is disrupting the process. Tedious brainstorming meetings help no one!

Instead, an effective way of enhancing your company's creativity is to nurture relationships.

The better your employees are connected, the more innovative your company will be. Employees with good relationships within the company have more opportunities to communicate ideas, gather helpful feedback and gain support and supporters for projects.

Activate Networks, a consultancy, discovered that good relationships with other people in a company are among the most powerful predictors of a team's success.

Remember, your employees are people and not just resources! It's far more enriching if you see your company as a constellation of partnerships between different people.

Similarly, your employees will be more loyal and view their work as more meaningful if they feel they're treated as people with lives outside of work. One way this can be achieved is by encouraging staff to explore educational opportunities, even if the subject isn't necessarily related to their job.

Teresa Amabile at Harvard Business School reports that employees are more creative when they experience their work as meaningful, and when they have respect for the organization.

### 7. Break down departmental walls. Use talent clusters to promote creativity and collaboration. 

Even if an organization acknowledges that its teams work well and that employees are responsible for certain tasks, often employees are still not expected to stray from set roles.

The trouble is that this kind of set-up will flounder in an innovation economy, as its structure is too inflexible to adapt quickly to change.

To break this cycle, make use of _talent clusters_, or teams especially put together for specific projects. Rather than comprising members from just one department, clusters gather employees from varied ranks and areas of expertise.

Talent clusters also don't limit the creative contributions of each member in the way that traditional teams might. In traditional teams, every person assumes a certain role; and as long as the team stays together, members may be stuck in a role of leader or innovator or expert, regardless of the project.

Since talent clusters break up after a project is complete, members aren't tied to one role. A person can be a leader in one project and an expert in another, according to their skills and expertise in relation to each project.

In an advertising agency, for example, an employee with some medical experience would make a great expert in researching a PR strategy for a pharmaceutical company. But when it comes to creating an ad for a fashion company, that same person may better assume a managerial role, considering how to fund the project.

Talent clusters are just a more effective and flexible way of connecting people and exchanging ideas.

When employees are gathered according to department or in fixed teams, most of their contacts will be limited to these teams or departments. On the other hand, if an employee works with multiple people in a broad range of short-term clusters, she'll build a network that breaks down departmental boundaries and enables fresh ideas to flow through the company.

> _"The ways in which we work need new definitions, though what they are and perhaps always have been are roles."_

### 8. Different is better! Diversity mixes things up, creating opportunities for innovation. 

Imagine you're hiring someone for a new position. You have a lot in common with one applicant and could picture them as a future friend. Another applicant is just as qualified as the first, but they're wearing unusual clothes and are reserved — a personality type very different from your own.

Whom should you hire?

Though you might be tempted to hire someone similar to you, diversity provides a team with different perspectives and can promote creativity. The more diverse a team is, not only socially but also based on qualifications, talents, gender, life experiences, ethnicity and so on, the more varied a team's perspectives will be on different work problems.

Each different, unique point of view may well lead to alternative approaches or innovative solutions. A team with diverse members will generate more solutions as well for each new problem.

Teams also tend to create products that meet their own needs and wants, hoping this will too satisfy customers' tastes. But different groups have different preferences. Some groups want a car to be affordable and easy to drive, but others may see cars as a status symbol or a luxury item.

So the more diverse your team is, the better its chances of developing a product that meets the desires of a wide array of people.

Diversity also enhances creativity. Even the most creative people among us never start from a void; they build on what's inside them, such as memories, long-considered concepts or older material.

Creativity builds on creativity, too. Inventors often create new things by consciously combining old things in new ways. When Kirkpatrick MacMillan invented the bicycle in 1839, for instance, he didn't need to invent the wheel first!

If you expose yourself to a wide range of new experiences, you increase the chances of new combinations. So get out there: attend readings, conferences and parties and meet interesting people! Consider broadening your scope of experience, with art and media that addresses different themes and subjects.

Discovering new concepts will better enable you to connect them and eventually to innovate!

> _"Making connections between disparate things is the key to creative thinking."_

### 9. Final summary 

The key message in this book:

**To run a successful company, you need to be able to roll with ever-changing business conditions and demands, invent new products, lead the competition, please a wide variety of customers and ideally influence your field. Understanding the impact of interconnectedness is paramount in achieving success!**

Actionable advice:

**Don't hire competitive star performers.**

The next time you're hiring, opt for candidates who have been lauded for their commitment and cooperativeness. These employees are more likely to stick around and support each other. According to a Stanford study, start-ups that choose committed employees are more likely to go public and less likely to fail, compared to businesses that prefer star performers.

**Suggested** **further** **reading:** ** _Leaders_** **_Eat_** **_Last_** **by Simon Sinek**

_Leaders_ _Eat_ _Last_ explores the influence that neurochemicals have on the way people feel and consequently act, and examines the discrepancies between how our bodies were designed to function and how they function today. Ultimately, we need true leaders to direct us back on the right path.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Faisal Hoque and Drake Baer

Faisal Hoque is an entrepreneur who founded MiND2MiND Exchange and B2B ForeSight. He has written articles for _The_ _Wall Street Journal_, _Fast Company_, the _Huffington Post_ and _Forbes_. He was also named one of the "Top 100 Most Influential People in Technology" by _Ziff-Davis Enterprise._

Having previously worked as a contributing writer at _Fast Company_, Drake Baer reports for _Business Insider_, covering leadership, strategy and organizational psychology.

