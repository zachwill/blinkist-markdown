---
id: 593e551bb238e100051fddae
slug: evicted-en
published_date: 2017-06-12T00:00:00.000+00:00
author: Matthew Desmond
title: Evicted
subtitle: Poverty and Profit in the American City
main_color: CEC1AF
text_color: 4F4A43
---

# Evicted

_Poverty and Profit in the American City_

**Matthew Desmond**

_Evicted_ (2016) tells the heartbreaking story of the individuals and families who struggle to get by in the United States' poorest cities. Despite their best efforts, many of these people have fallen into a vicious cycle of poverty that has left them at the mercy of greedy property owners who don't hesitate to evict families at the slightest provocation. To take a closer look at the details of their lives, we'll focus on the inner city of Milwaukee and the tenants and landlords who populate this deeply segregated area.

---
### 1. What’s in it for me? Get a glimpse into a world of poverty and eviction. 

Imagine being thrown out of your home; it's something most of us probably don't even want to think about it. For many Americans, however, this traumatic experience is a cold, harsh reality.

But if tenants cause trouble or don't pay their rent, isn't it fair for them to be evicted? Perhaps — but the whole story isn't always that cut-and-dried.

In these blinks, we will look at how eviction has become increasingly common over the last two decades in the United States, affecting more and more Americans. We'll look at the big picture to see why this is, while following some of the people in Milwaukee, Wisconsin, who find themselves in the unforgiving world of being poor and searching for housing, as well as the property owners behind the evictions.

In these blinks, you'll learn

  * how evictions were rarer during the Great Depression than they are today;

  * why poor tenants pay more than 50 percent of their income in rent; and

  * how minorities are more likely to get evicted.

### 2. Evictions have become an everyday occurrence in today’s low-income neighborhoods. 

Here's an all-too-common scene in cities and towns across the United States: clothes, toys and other personal belongings strewn across a front lawn or sidewalk — the scattered remnants of another family evicted by their landlord and forced from their home. 

This year, the number of people left without a home after running into difficulties paying rent could reach the millions.

You might think that a typical low-income family in the United States should be able to get by living in public housing or some form of housing assistance. But in reality, benefits like these are scarce; in fact, only one out of every four qualified recipients will receive any assistance at all.

But when it comes to evictions, it can be hard to calculate an exact number.

In Milwaukee, we know that over the course of three years, one-eighth of all the city's tenants faced eviction. However, census data can overlook a lot of evictions, since a considerable number aren't formally processed by the housing courts that handle disputes between property owners and tenants.

No matter how you count it, evictions are a national problem. In 2012, New York City courts handled almost 80 eviction cases _every day_.

That same year, one out of every nine individuals or households renting a property in Cleveland, Ohio, received an eviction summons to appear in housing court. And in Chicago, it was one in 14.

But the housing situation in the United States wasn't always this dire.

Even though the struggle to come up with rent money is nothing new, it used to be rare for landlords to resort to evictions — even during the Great Depression. During the 1930s and 40s, records show that an eviction led to the kind of community resistance that would cause a scandal for the landlord.

This is what happened in February of 1932, when a landlord tried to evict three families in the Bronx. A thousand people took to the streets in protest, and the _New York Times_ made a point of noting that it would have been a bigger crowd if it weren't for the freezing cold temperatures.

### 3. Due to unemployment, low wages and high rent, there’s a growing number of vulnerable tenants. 

So why are evictions more common these days than ever before?

One reason is highlighted in housing studies conducted by Harvard University researchers: in the years between 2001 and 2014, rents increased by an average of seven percent while incomes fell by nine percent.

A standard measure of comfortable living suggests that you shouldn't spend more than 30 percent of your income on rent, otherwise you won't be able to afford basic necessities like medicine and food. Yet census data shows that a majority of low-income households pay over 50 percent of their income on rent, while more than a quarter of these households pay over 70 percent.

In order to keep a roof over their heads, some struggling families are forced to sell food stamps, pirate their electricity from neighbors or hope that a friendly stranger will let them in.

Part of the reason for these struggles is America's unemployment rate.

Manufacturing jobs used to be a reliable source of income in cities like Milwaukee, which lost 56,000 of these very jobs between 1979 and 1983 to overseas competition. As a result, plants and factories closed down and left many inner-city African-American families without a source of income.

In Milwaukee today, 50 percent of working-age black men are jobless, and welfare benefits barely cover the costs of living. With such a limited budget, anything out of the ordinary can lead to falling behind on rent and, in turn, an eviction notice. 

This is what happened to Lamar, a legless veteran who received an unexpected second welfare check one month.

Normally, after paying rent, Lamar would have $2.19 a day left over for the rest of the month. But with the extra check, he decided to buy his sons some new shoes and school supplies. However, when the state realized their mistake, he was forced to repay the money from the extra check, which led to him falling behind on rent.

Lamar sold his food stamps at half their face value and even painted an upstairs apartment for his landlord, but it still wasn't enough. Lamar was evicted.

### 4. Landlords who prey on vulnerable tenants are putting profits ahead of safety. 

When you're in debt to your landlord, you not only live in a state of constant worry, it can also mean putting up with terrible and often dangerous living conditions. After all, if you complain about a broken toilet or window, it might be the final straw that leads to an eviction notice.

Once tenants fall behind on rent, they're immediately at the mercy of a property owner whose primary concern is often profit over safety.

Sherrena Tarver is one such landlady: when the house she was renting tragically caught fire one day, she knew the apartment was supposed to be equipped with smoke detectors, but she couldn't remember if they'd ever been installed.

After the fire caused the death of her tenant's baby, Tarver's main concern was whether she was financially or legally liable for the death. The following day, the fire inspector called to tell Sherrena that she was off the hook and wouldn't be held accountable.

This led Tarver to shift to her second major concern: Will she have to refund the tenant's rent for that month? Again, much to Tarver's relief, the fire inspector told her she wasn't required to pay back any money, even though she exploited a tenant who had no choice but to accept living in dangerous conditions.

Property owners like Tarver and Tobin Charney, a white man who owns 131 trailer homes in Milwaukee, have become adept at exploiting the financial benefits that come with the threat of eviction.

Charney makes $400,000 a year from his trailers, some of which are no bigger than a shed, while Tarver's numerous properties allow her to vacation in Jamaica and drive a luxury SUV. Meanwhile, their tenants can barely afford to feed themselves after paying rent.

Hardly any money goes into repairs or maintenance, and with no legal representation and the fear of eviction hanging over their heads, tenants are helpless against this exploitation — even when they're told an increase in rent is coming.

> Since 1995, the average price of rent has increased more than 70 percent.

### 5. Segregated inner cities and black women are the main targets for housing market exploitation. 

Milwaukee may not come to mind when thinking of the United States' most segregated cities. But it's certainly among them, and this racial divide is apparent in the city's poorest and most vulnerable areas.

This divide makes it clear just how egregiously black people are exploited in the housing market.

During a typical month in Milwaukee, 75 percent of the tenants summoned to housing court are black. Meanwhile, white people are shown desirable properties that are never offered to black people, who have no choice but to accept the worst housing in the worst neighborhoods.

Indeed, the greatest fear among the low-income white folks living in the city's trailer parks is that they'll one day be forced to live in the even poorer conditions of the black neighborhoods.

But it's black women who are hit hardest by evictions: even though they make up just nine percent of Milwaukee's population, they are subject to 30 percent of the evictions.

This means that more than one-fifth of the city's black women will face eviction. For Hispanic women, that rate is one in 12, while for white women, it's one in 15.

One reason for this is that women continue to be paid less than men for doing the same work. More importantly, however, a large percentage of black women in poor neighborhoods are also facing the challenge of raising kids on their own.

Being a mom is always challenging, but doing so in a tiny apartment, because landlords are unwilling to rent a larger one to a single mother with young children, makes it even more difficult.

In 1968, the _Fair Housing Act_ was enacted to prevent housing discrimination; unfortunately, it doesn't consider a single mom and her kids to be a target.

Tenants are also at a disadvantage in housing courts, since 90 percent of landlords have attorneys while only 10 percent of tenants do. Either way, tenants don't even show up 70 percent of the time — either because they can't afford to skip work, pay for child care, or simply because they know they don't stand a chance.

> _"Poor black men were locked up. Poor black women were locked out."_

### 6. Evictions put unbearable stress on families and keep them locked in poverty. 

It can be hard to imagine having to pick up your family and move from one dilapidated apartment to the next, all while trying to hold onto your possessions and keep your children fed, clothed and in school.

Yet this is the reality of a family trapped in a cycle of evictions — and it can take a heavy toll on your mental health.

It's no wonder that half of the mothers who have faced eviction report symptoms of clinical depression that can linger for years afterward, sapping their energy and optimism.

It's also why the number of housing-related suicides doubled when rents skyrocketed between 2005 to 2010. The trend prompted a group of psychiatrists to call evictions a "significant precursor to suicide."

The fear of eviction doesn't have to be solely related to money, either.

Even if rent is covered, any event that results in the police showing up can be used by the landlord as grounds for eviction. So, domestic abuse situations and other 911 emergencies can also lead to housing worries.

All of these concerns are well founded, since evictions play a significant role in keeping people trapped in poverty.

Even if someone is saving money, once she's hit with an eviction, she has to pay for storage or buy new possessions, and use whatever she may have to find new housing.

This kind of instability also increases the likelihood of job loss by 15 percent, usually due to the stress of eviction and the resulting housing search hurting an employee's performance.

And in the year following an eviction, families face a 20 percent increase in hardships such as hunger and sickness, as well as a lack of heat, electricity or a working phone line. Plus, these families might miss out on the important letters and benefit checks that may still be getting sent to the wrong address.

Ultimately, evictions are what push families further into the undesirable parts of a city, moving from one bad neighborhood to an even worse one, with more crime, drugs and violence just around the corner.

### 7. Housing is a human right, and a housing voucher program might be the solution. 

There's an important difference between a house and a home. A "home" implies more than just shelter — it's also a warm, safe and loving place for a family.

Other languages have similar connotations: one ancient Egyptian hieroglyph was used interchangeably for both "home" and "mother," while the Chinese word "jiā" means "family" as well as "home."

This is what makes a home the universal center of civilization, and is why it should be considered a basic human right.

When people are forced to live nomadic lives, social bonds and strong communities can't be formed; this takes a toll on children, who are forced to change schools, lose friends and live in the shadow of their parents' despair.

Psychological stability goes hand-in-hand with community stability, because without it, people aren't going to invest in their neighborhoods, build relationships or help one another.

So if the United States truly wants to improve living conditions for its citizens, it needs to reconsider the values the country was founded upon, such as the unalienable rights to life, liberty and the pursuit of happiness — none of which can happen without a stable home.

One possibility is to enact an expanded housing voucher program.

Housing vouchers could be given to every family under a certain income level, and could act like the food stamps that families can currently use to buy groceries.

In this case, tenants could spend 30 percent of their incomes on housing costs while the vouchers would take care of the rest.

This system is already being successfully implemented in Great Britain, where the Housing Benefit program is available to many households, and in the Netherlands, where housing allowances benefit nearly a third of the country's tenants.

Critics suggest that these programs can lead to people being disinclined to work, but only one study has found a slight reduction in work hours among benefit recipients. Multiple other studies have shown no negative impact whatsoever.

The undeniable truth is that something must be done to reduce eviction and homelessness rates in our society, because if we continue thinking of it as someone else's problem, it will only get worse — and society as a whole will continue to suffer.

### 8. Final summary 

The key message in this book:

**Evictions cause irrevocable damage to individuals, families and communities, and are part of the United States' systemic cycle of poverty, discrimination and exploitation. It's time for the United States to start a campaign of housing reform, address this social epidemic with the help of housing vouchers and recognize the basic human right to safe housing.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _White Trash_** **by Nancy Isenberg**

_White Trash_ (2016) retells American history from the perspective of the poor whites who were by turns despised and admired by the upper classes. These blinks trace the biopolitical, cultural and social ideas that have shaped the lives of white trash Americans from early colonial days to the Civil War, through the Great Depression and up to the present day.
---

### Matthew Desmond

Matthew Desmond is a sociology professor at Harvard University and co-director of the Justice and Poverty Project. In 2015, he was the recipient of a MacArthur Genius Grant. He is also the author of the award-winning book _On the Fireline_, and other books dealing with race and poverty.

