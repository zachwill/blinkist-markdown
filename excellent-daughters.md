---
id: 57923818b3b9b300032e80b8
slug: excellent-daughters-en
published_date: 2016-07-25T00:00:00.000+00:00
author: Katherine Zoepf
title: Excellent Daughters
subtitle: The Secret Lives of Young Women Who Are Transforming the Arab World
main_color: C9E6EE
text_color: 32393B
---

# Excellent Daughters

_The Secret Lives of Young Women Who Are Transforming the Arab World_

**Katherine Zoepf**

_Excellent Daughters_ (2016) is an inside look at the lives that young Arab women lead today, marked by restriction, segregation, violence and discrimination. These blinks also point out the signs of a slow but steady change in Arab countries, as young women fight for their rights to study, work and look toward a promising future.

---
### 1. What’s in it for me? Step into the modern world of Arab women. 

Many of us might loosely associate the act of flying with freedom. But for the thousands of young Arab women who are now making a living as flight attendants, this connection is quite concrete: when their planes take off, they are literally flying away from their often claustrophobic lives in controlling families.

But where, exactly, are they heading — and not just these flight attendants, but an entire generation of smart, hungry and courageous Arab women? That's just one of the topics that these blinks delve into. One thing's for sure: many of these "excellent daughters" aren't seeking a mere copy of the Western way of life. They don't intend to forgo the proud culture of their ancestors; instead, what they're aiming for is a new way of life, one that combines the traditions of Islam with modern-day individual freedom.

In these blinks, you'll find out

  * why it's a sign of huge progress when Saudi Arabian women sell lingerie in a mall;

  * about a law that absolves brutal murders; and

  * how Qur'an schools can sometimes contribute to girls' emancipation.

### 2. In Arab countries, it’s a woman’s moral duty to hide herself from the male gaze. 

Imagine you meet a beautiful man or woman and feel immediately attracted to him or her. Who's responsible? You might think that both are, or that it's just magic. But in Arab countries, when such an encounter happens, only one person is responsible: the woman.

From a young age, women in Arab countries are taught that it's their responsibility to prevent their faces, hair and bodies from tempting men. A woman's features are seen as so seductive that they would cause nothing short of chaos if exposed on the streets, driving men to commit physical assaults or sexual crimes. Women thus have a moral responsibility to prevent these crimes from happening.

This is the reasoning behind the _hijab_, a veil worn to cover a woman's hair. The hijab has recently been the subject of much religious and political debate in the Western world. But in Arab countries, it's a given that once a girl is old enough to be physically desirable, she is able to choose — or in some countries, will be forced — to cover herself.

This isn't the only way Arab women are expected to hide themselves from a man's gaze. While women in Western countries are free to go out and party, have male friends or boyfriends and attend college or university, it's a different story in Arab countries. After a woman shows the first signs of puberty, she is often treated as someone who shouldn't be seen in public or even interact with males at all, unless they're part of her close family. Generally, women are expected to be shy and hide away from society.

For many women leading these sheltered lives, marriage is their only shot at getting some degree of freedom — but this doesn't always play out as they hope.

> _"A woman is viewed as something to be protected . . . but also as a threat to the established order of the community."_

### 3. Marriage is a central concern for young Arab women. 

How important is marriage to you? Many individuals in the Western world today choose not to marry their partners, have multiple partners or are simply happy to stay single. But for young Muslims, marriage is seen as the beginning of a new, better life.

Many young Arab women wait anxiously for the day their father brings home a suitor, and anticipate their marriage day as the most important day of their lives.

Once married, a newlywed couple will move into a new house and take up new roles in society, too. After marrying, a Muslim man becomes the head of his new family. As for the woman, she is finally entitled to become close to a man she isn't related to, and care for a family of her own.

But none of this is possible for a woman deemed, for whatever reason, unsuitable for marriage. And this is one of the most pressing concerns for many young Arab women.

Many Arab men would refuse to take a bride who is no longer a virgin, nor one who has the reputation of being immodest. Even the tiniest of incidents can become blemishes on an Arab woman's reputation. Even just talking to a foreign man is considered a sign that she'd be willing to sleep with him.

Because of this, young women will do anything to protect their chaste image, and are constantly worried that shameful events will leave them unmarriageable — even if it's something as harmless as a man accidentally seeing their hair. Naturally, acting flirtatious is entirely out of the question.

> _'Just a few years ago, marriage was the only socially acceptable path for most young Arab women.'_

### 4. Saudi Arabian women and men live strictly segregated lives. 

Can you imagine having no friends of the opposite sex? For many of us, that's simply unimaginable. Yet it's the reality in Saudi Arabia, where men and women are rarely allowed to see each other, let alone talk to each other.

Even if they meet by accident, they'll do all they can to keep a respectful distance. If a man approaches an elevator already occupied by a woman, for example, the woman will turn her face while the man immediately retreats to wait for the next elevator. Men will also typically leave their seat if a woman sits down next to them on the train, to spare them the shame of sitting next to a man they don't know.

There are also situations where this strict segregation backfires and women are in fact forced to interact with men they don't know. Because so few females are able to have professional careers, women do see male physicians, purchase items from male clerks or speak to male policemen.

But slowly, women have begun to enter the world of work, starting with malls. Traditionally, even men worked as salespeople in women's lingerie stores. But recently, Saudi women have taken over the lingerie shops. How? By appealing to the notion of shame, something that everybody in their culture understands.

By convincing government officials that it would be inappropriate for male sales clerks to imagine their female customers with lingerie, these women ensured that only female staff could work in these shops.

This might sound like nothing more than further gender segregation, but it's actually a valuable opportunity for women to work outside the confines of their homes.

### 5. Muslim religiosity is on the rise, which actually has some benefits for studious young women. 

In the Western world, religiosity has been on the decline for centuries. But in Arab countries, it's on the rise, as is fundamentalism. For a time, it seemed that Arab women had a chance at a freer life with opportunities, with their husband's blessing, to study and have a career.

But with increasing instability in countries like Syria, many countries have turned back toward a fundamentalist approach to religion.

This is reflected in the rise of students attending Qur'an classes. Interestingly enough, many of these new religious students are women. In Syria, more girls than boys now study at Qur'an memorization schools.

In many ways, this is a great thing for women, as such schools create opportunities for an academic career.

Many schools enroll girls from a very young age, teaching them their first verses of the Qur'an not long after they learn to walk. By the time these female students reach adulthood, they're able to recite the entire Qur'an. By receiving a respectable education in Qur'an studies, many women stand a better chance of entering universities and starting a career.

More women in Qur'an studies are also beginning to engage with their religion more critically, questioning dominant interpretations of Islam. Rather than rejecting their religion altogether, these women use their extensive education to offer highly insightful reinterpretations of crucial passages, highlighting the potential for Islam to support freedom and equal rights.

### 6. Growing numbers of Arab women reject marriage to seek out professional and academic success. 

Arab women have traditionally grown up in the knowledge that it was their fate to bear children, care selflessly for their family and live a life of extreme modesty. But younger generations are now growing up with new and conflicting information. Thanks to foreign television and the internet, young Arab women know there's another life out there for them beyond their country's borders.

Today, increasing numbers of Arab women are breaking the status quo to seek financial and professional independence. Many move to countries like Lebanon, where women in the workforce are more accepted and receive support.

Others find work abroad in offices or in flight crews, where they can finally live outside the overbearing control of their husbands and families. In addition, a significant increase in female students at Arab universities means that women have come to unexpectedly outnumber their male peers!

Many Arab families still reject the notion of an educated and independent woman, though this is changing slowly. Typically, an educated woman is viewed as unfeminine and unsuitable for marriage, making it harder for these women to find a potential husband and be accepted by his family. An educated woman may be ostracized by her own family, too.

As a result, many women acknowledge that their choice to study may mean sacrificing a family life. Some choose to leave their job and education to commit to their husbands or families. But slowly, educated young men are starting to accept and support their wives in leading their own careers.

However, these men are a minority. There's a long road ahead for Arab society, and it starts with changing men's expectations of what a woman should be.

### 7. Tradition dictates that females who dishonor their family through no fault of their own must pay with their lives. 

Consider this for a moment: A young woman is raped by a friend of her father's. Her brother then murders her brutally and her entire family celebrates her death.

Unimaginable?

Unfortunately, it's not only not unimaginable, it really happened. This is the real story of Zahra, a girl who was killed at the age of 17. Why did her brother feel it was right to kill her after the horrifying ordeal of sexual assault? Because of Arab ideals of family honor.

In Arab countries, a family's reputation depends on the purity of its female members. If a woman is subject to a shameful incident, this stains the honor of her family, for which the woman bears full responsibility. So, when Zahra was raped by her father's friend, she wasn't seen as a victim; instead, she was viewed as a perpetrator who had forever tarnished her family's reputation.

Arab males, in turn, feel it is their duty to restore their family's honor. That could mean marrying a raped woman to her rapist, ostracizing her or leaving her to fend for herself. But in many cases, men decide that the shame is too great as long as the offending woman is still alive. So they murder her, as Zahra's brother did, to restore family pride.

These values are still unquestioned, and many men who commit these honor killings are acquitted. After a two-year process, Zahra's brother was found to have the acted under the legal right of restoring his family's honor. He was never punished. Another example is Article 548 of Syria's penal code. It allows a man to kill a female relative if he observes her committing an immoral act.

Today, many Arab lawmakers are attempting to overturn this statute, but they aren't always successful. Hundreds of honor killings still happen every year.

### 8. Even the Arab Spring perpetuated discriminatory treatment of women, but all hope is not lost. 

Life as an Arab woman is an unbelievable struggle. But, little by little, change is becoming more likely. Growing numbers of Arab women remain unmarried in order to dedicate themselves to influential careers as lawyers or university professors.

In fact, many young women today now reject marriage, seeing their unmarried life as a better option. They fight to have a proper education, earn their own income and ensure their daughters grow up in safety, with the respect of their male counterparts.

Other daring Arab women participated in the Arab Spring. Unfortunately, however, this revolution became known for its appalling treatment of women. The movement started with political revolutions in Tunisia (on January 14, 2011), before spreading to Egypt and many other Arab countries. The Egyptian protests were actually triggered by a woman named Asmaa Mahfouz, who posted a protest video online decrying the current political situation.

During the Arab spring, young men and women in Egypt stood side by side to protest for a change in leadership and push for greater human rights. And yet, women weren't treated equally by the revolutionary regime when it came to power.

Female protesters were often arrested and forced to undergo mortifying virginity tests. An unnamed Egyptian general told CNN that the regime aimed to prove that these women were no longer virgins, so that they couldn't blame men for assaulting or raping them following the protests. Samira Ibrahim Mohamed is one Arab woman who recalls the humiliating experience of being examined against her will by incompetent doctors as several male soldiers looked on.

Sadly, just a few months after the Arab Spring, women were being treated even worse than they were before their fight began.

Despite the current grim situation, there are many ways for young Arabs to join forces and bring their valued culture of family and tradition together with a liberal, open-minded view of the world. And to do this, they need support from around the globe!

### 9. Final summary 

The key message in this book:

**Many young women in Arab countries live their lives hidden away from society, under the strict control of families and husbands. But recent years have seen Arab women break out of oppressive gender segregation and violent cultural norms to expand their rights and gain access to academic education and independent careers.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Startup Rising_** **by Christopher M. Schroeder**

The Arab Spring uprisings left much of the Middle East politically unchanged, but they made a whole lot of noise. And yet there's a second, quieter revolution taking place in the same region — and it's all about the rise of tech and entrepreneurialism. Through stories of Middle Eastern entrepreneurs living in Beirut, Amman, Dubai, Istanbul and elsewhere, _Startup Rising_ shows an entire region reinventing itself as a center of economic opportunity.
---

### Katherine Zoepf

Katherine Zoepf is a _New York Times_ journalist who lived in Syria and Lebanon for several years, and traveled around Arab countries for many more. She is a graduate of the London School of Economics and Princeton University and currently a fellow at the New America foundation.

