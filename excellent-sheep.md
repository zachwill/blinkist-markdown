---
id: 543e72403065650008200000
slug: excellent-sheep-en
published_date: 2014-10-16T00:00:00.000+00:00
author: William Deresiewicz
title: Excellent Sheep
subtitle: The Miseducation of the American Elite and the Way to a Meaningful Life
main_color: C3AADD
text_color: 5C5069
---

# Excellent Sheep

_The Miseducation of the American Elite and the Way to a Meaningful Life_

**William Deresiewicz**

_Excellent Sheep_ casts a critical view on the most prestigious American colleges and calls into question the academic quality of elite institutions such as Harvard and Yale. Ultimately, _Excellent Sheep_ reveals not just how elite American colleges stifle independent thought, but also how they directly contribute to reproducing class inequality.

---
### 1. What’s in it for me? Uncover the critical failures of America’s elite educational institutions. 

In this age in which America is suffering from a crisis of leadership, whereby the poor are getting poorer and the rich are getting richer, it's time to review our faith in the current leadership, almost all of whom were educated by elite and prestigious American universities.

They're _supposed_ to represent the brightest minds of their generation — the very best of the best. However, with their evident failure, it's time to examine the institutions that supposedly prepared them for elite leadership.

In addition to issuing a scathing indictment of the practices and attitudes of elite universities, these blinks offer students practical advice on how to pursue their goals even within institutions that seem designed to break them.

Furthermore, it shows students that they don't _have_ to go into prestigious careers such as hedge fund management or consulting as a logical follow-up to their elite education. In fact, many employers are looking specifically for the kinds of well-rounded individuals molded by liberal arts education.

In these blinks, you'll learn:

  * why Mitt Romney is even more out of touch than you previously thought;

  * why elite universities treat students like customers; and

  * how having a liberal arts degree is actually more valuable than one in finance

### 2. Students in elite colleges are beset with psychological problems and graduate feeling aimless. 

Hollywood presents us with a very one-sided view of college life — one filled with parties, shenanigans, self-discovery and the best times of our lives.

However, while college life might seem attractive from the outside, studies have found that there is in fact a mental health crisis on campuses. In a study conducted by the _American Psychological Association_, nearly 50 percent of all college students reported experiencing a feeling of hopelessness, while 33 percent reported feeling so depressed that they struggled to function throughout the year.

This was corroborated by a recent long-term study, which found college mental health to be at its lowest level in the study's 25-year history.

The discrepancy between appearance and reality is especially true for elite college students, who outwardly appear to be _super people_, managing huge workloads while maintaining an aura of happiness.

And yet, students at Stanford talk of _Stanford Duck Syndrome_ : they see themselves as ducks, whose serene appearance atop the water disguises the frantic paddling underneath. One student at Yale made clear just how normal depression was at his school, stating: "I may be miserable, but if I wasn't, I wouldn't be at Yale." 

The stress of their studies might not be so bad if students were pursuing their dreams. But many aren't. In fact, students at elite universities feel forced into pursuing careers in areas like consulting or finance as a way to gain recognition from their peers.

Just consider that, in 2010, _half_ of Harvard graduates were going into either finance or consulting. This is surprising, seeing as how surveys of freshmen at elite colleges rarely indicate _any_ interest in either of these career paths. It's thus only natural that many question the value in their decisions later in life.

So where does this immense pressure to prove oneself — even after making it into an elite university — originate? The answer may be found in the home.

> _"Education is a self-inflicted wound."_

### 3. “Tiger parents” overidentify with and infantilize teenagers, robbing them of creativity. 

We've all seen a "tiger parent" at one point or another — mothers and fathers who oversee their child's life with constant pressure and criticism. 

Surely these parents mean well, but an overbearing parenting style is just as damaging to their children's well-being as one that is laissez-faire and overindulgent. While these parenting styles might seem like polar opposites, they're in fact both forms of _infantilization_, i.e., the process of treating a mentally capable adult as a child.

Think about it like this: tying your daughter's shoelaces when she's eight years old is just as bad for her as pressuring her to get an A on a test. While the actions themselves are different, both ultimately treat the child as if she can't do anything for herself.

Tiger parents inadvertently cause their children to lose the ability to express their own creativity through their interests and desires. For example, as one Stanford student remarked, if he switched from engineering to a liberal arts degree (which is what he wanted to do), then his parents would stop paying for his tuition!

Tiger parents also rob their children of aspirations by _overidentifying_ with their lives, viewing their children as an extension of their own will rather than as unique individuals with unique needs and desires.

Indeed, there is a growing trend in America to run families like for-profit companies: a child's achievements are reduced only to those that are the most recent, and normal academic failure is recast like something found in ambitious corporate culture, where a "bad sales-quarter," i.e., a bad semester at school, becomes totally unthinkable.

Children are no longer valued, but valuable. Their achievements are noteworthy not because they show personal achievement, but because they align with the parents' desires and regrets. A recent Yale graduate described it best, telling how his mother pushed him through all the elite doors _she_ wanted, without ever considering what _he_ wanted

### 4. Today’s elite colleges are run more like companies than like educational institutions. 

Most of us would assume that you'd get an elite education from an elite university. However, as former Harvard dean Harry R. Lewis once said: "Harvard no longer knows what a good education is."

Elite institutions today are more concerned with "efficiency" and revenue rather than with crafting genuine knowledge. While it might be hard to argue against making education "efficient," at large elite academic institutions, "efficiency" has less to do with education and imparting ideas and more to do with revenue-generating research.

An efficient college today isn't one with the most teacher-of-the-year awards, but the one with the most research output. We can easily see this when we look at specific departments within universities: liberal arts departments are being downsized, while the ones that bring in revenue, such as economics, are being expanded.

Moreover, today's professors are employed not for their teaching credentials, but for their research. But being an expert doesn't make you a great teacher! Adding to this is the tendency for top academics to be drawn to elite universities that exempt them from teaching undergraduates, who would benefit the most from their expertise.

Furthermore, the relationship between students and colleges has changed from pedagogical to commercial. In other words: students have become customers.

Consider that, in the United States, colleges are ranked annually in _U.S. News_, which then has significant consequences for the funding a college receives from donors and endowments. As a result, colleges have changed the manner with which they interact with students:

For example, one criterion for college ranking is _selectivity_, i.e., how many students get in versus how many apply. Elite universities aggressively inflate their selectivity by trying to recruit as many students as possible, knowing full well that those students have no chance of getting in.

Graduation rates are also a criterion, meaning that elite colleges almost never flunk their students. This leads to grade inflation, with the average grade point average (GPA) climbing from 3.1 in 1990 to 3.43 in 2007.

Now that you have a good grasp on the failures of elite educational institutions, the following blinks will explore what colleges should _actually_ impart to their students.

> _"Ambitious academics regarded teaching undergraduates as a distraction and a burden."_

### 5. The most important thing students should be learning at college is how to think. 

So what exactly is the point of pursuing higher education? Sure, there is the piece of paper, i.e., the degree, that you get at the end, but surely there is something more to it all.

College is supposed to impart students with critical thinking skills as well as help them to develop a habit of skeptical reflection and the aptitude to put the specific knowledge and skills they learn into practice.

Students arrive at college full of preconceived ideas and beliefs about the world, and it's precisely these assumptions that they must learn to critically reflect upon and engage. The philosopher Plato called these _doxa_ — the opinions that we've picked up from the world around us via our surroundings, e.g., through advertising, our family or political propaganda. He believed that it was our duty to liberate ourselves from these _doxa_ by recognizing them and critically engaging with them.

While college isn't the _only_ place to acquire these skills, if run properly, it is certainly the best.

In fact, one of the reasons that college is such a great place to acquire critical thinking skills is for the same reason that many people criticize it: it doesn't represent the "real world." For a short period of time, students can enjoy a certain distance from their family orthodoxy, or the demands of a career — an interval of freedom which allows them to challenge their _doxa_ and develop their own beliefs.

What's more, college also provides students with qualified professors. Sure, there are some people, _autodidacts_, that learn everything on their own. But professors provide something that independent learning cannot: they challenge your intellect and your critical thinking, and expose the flaws in even your most deeply ingrained beliefs.

Skilled professors can gently test why you hold certain ideas, thus forcing you to substantiate your beliefs by thinking logically and critically. In this way, college should be a place for great self-discovery, which ultimately will provide meaning to life's endeavors.

> _"The feelings that we have for the teachers or the students who have meant the most to us...can never go away."_

### 6. Introspection will help you find the career path that’s right for you. 

The best way to ensure that you find a fulfilling career is through critical reflection and a strong understanding of who you are as an individual. These days graduates are obsessed with finding the right vocation after college, but few think about what it is that they really want out of life. 

In the case of graduates from elite institutions, many choose prestigious career paths such as consulting, or go on a quest for fortune by becoming hedge fund managers. Ultimately, by focusing on fame and fortune, many also go on to feel unfulfilled or find that their work has no real meaning.

Today's students are missing the original meaning of the Latin word _vocation_ : calling. A vocation is the thing you are called to do and can't live without. But this calling won't be delivered on a silver platter! Only hard work, self-knowledge, the bravery to take risks and a strong _moral imagination_ will lead you to your vocation.

This moral imagination doesn't relate to what is right or wrong per se, but instead refers to the choices we make in relation to our personal orthodoxy.

For example, when you go to a coffee shop, you might choose to order a latte or cappuccino, or you may decide you don't want anything and leave. A similar set of choices is found at an elite school: you may decide to study medicine, law, music or nothing at all.

However, decisions regarding schooling are far more difficult than ordering coffee, and often require moral courage to make. This is because society deems certain careers and interests as being frivolous or unimportant. For example, writing poetry is often seen as self-indulgent, compared to the hard-toil of consulting. But is the pursuit of money really any less selfish?

It's precisely these kinds of questions that require moral imagination to answer in a way that is best for you.

Now that we've laid out all that college is _supposed_ to be, the following blinks will focus on an oft-overlooked educational path, at least at elite universities: the liberal arts.

> _"Change is driven by tension between is and ought — a tension that you have to feel inside your soul."_

### 7. You’re just as likely to land a job with your liberal arts degree as with any other – perhaps even more so! 

Common knowledge holds that graduates with liberal arts majors, such as history or philosophy, are less employable than their peers who study science or business.

However, liberal arts graduates are in fact highly valued in the workforce, and are even recruited _more_ than the majority of other disciplines. For example, a recent survey revealed that 30 percent of companies were recruiting liberal arts graduates specifically. This came second only to engineering and computer science, while beating out finance and accounting, which amounted to only 18 percent.

As we can see, there has been a shift in hiring practices, where those with _soft_ skills, such as writing, analytical and communication skills, are preferred over _hard_ skills, such as conducting tax audits.

But why? Well, specialized hard skills can be _learned_, while soft skills can only be developed over a long period of time.

Some prestigious career paths, such as consulting, are even more interested in liberal arts graduates, hiring them in droves. Consulting firms such as _McKinsey & Company_ choose to hire liberal arts graduates because they're well equipped to handle complexity, tackle ambiguity and think creatively.

A recent article in the _Harvard Business Review_ put it quite bluntly, asking their readership to consider the fact that they can either hire a liberal arts graduate themselves, or "pay through the nose for a big consulting firm to hire them to do the thinking for you."

What's more, liberal arts majors also outperform students from a variety of disciplines on graduate school entrance tests for prestigious vocations. For example, when applying to medical school, they outperform biology majors; on law school admissions tests, they best social science majors; and they even outperform business majors on business school admissions tests!

Clearly, liberal arts majors are justifiably hireable, and it's no wonder that companies want them.

### 8. The liberal arts don’t only have practical application – they also feed your soul and enrich your life. 

As our societies become more and more secular, many people are looking to alternative explanations to answer life's big questions. Scientific discoveries from the eighteenth and nineteenth centuries paved the way for modern science and has radically changed the way we understand the world around us.

These discoveries were in part made possible by the intense skepticism from the Enlightenment era of seventeenth-century Europe, which caused many traditional belief structures to crumble.

At the time, the Enlightenment was a radical force, as intellectuals promoted using reason and logic over adherence to dogma to solve big philosophical questions. This shift paved the way for the scientific method, which tests ideas against empirical findings.

Today, by appealing to logic and reason, the truth is now up for debate. We can now look places other than the Bible in search of truth, for example, in the novels of Dostoevsky or the music of Shostakovich.

The arts became the new religion for secular societies captivated by science. While art and science may seem as incompatible as oil and water, in actuality, each fulfills an important role in our lives.

The scientist Stephen Gould says that the arts and science are _non-overlapping magisteria_. In layman's terms, both art and science have something legitimate to say in their fields of knowledge, and these areas don't overlap.

In essence, science informs us about our external world, while art teaches us about our inner workings, and each plays a role that is just as important as the other. So, for example, while science gives psychiatrists the tools to determine whether a patient is addicted to alcohol, reading a D.H. Lawrence novel will help explain _why_ he acts or feels the way he does.

Yet, despite the arts' ability to help us grapple with life's questions, contemporary society tends to view them as trivial or even insignificant, especially at elite educational institutions.

Now that we have a good understanding of the importance of liberal arts education, our final blink will examine the aspect of educational institutions with the most far-reaching consequences: the quality of the leaders they create.

### 9. The glaring inequality at elite institutions affects the quality of the leaders they produce. 

Top colleges such as Harvard and Yale are known for sending graduates out into the world who become business leaders and even presidents. But do these graduates actually become good leaders? To answer this question, let's look at some statistics and economics:

Simply put, elite colleges lack economic diversity now more than ever. Even with their efforts to increase diversity, the majority of students at elite institutions are still immensely privileged. Consider, for example, that 40 percent of Harvard students come from households in the top six-percent income bracket in the United States! What's more, these figures are consistent across elite colleges: a 2006 study found that 67 percent of students at elite institutions come from families with incomes in the top quartile.

And not only are wealthy children more likely to go to college, but intelligent children from poor backgrounds aren't even applying. Alarmingly, nearly 50 percent of high-scoring SAT-takers from low-income families don't even enroll in college.

Elite college graduates inhabit the most important positions in America, despite being dangerously out of touch with the majority of citizens. Since 1988, all but two out of the ten major-party presidential nominees attended elite private colleges. Of those eight, all of them attended either Harvard or Yale at some point in their academic life.

We see the same trend in other key government positions. For example, eight out of the nine current Supreme Court justices earned their law degrees from either Harvard or Yale.

Clearly, the affluent elite are also the ruling elite in American society. This wouldn't be a problem if their realities weren't completely divorced from those of low- and middle-income families — the majority of society

We see this disconnect quite plainly in quips from elites such as Mitt Romney, who once suggested that to curb unemployment, unemployed graduates should simply borrow money from their parents and start a business!

> _"If the elite got any more inbred, they'd grow tails."_

### 10. Final summary 

The key message in this book:

**Common wisdom says that elite universities create elite leaders — but this couldn't be further from the truth. While elite educational institutions might pump out the highest number of successful businessmen and politicians, they aren't necessarily the most fit for the job, and they certainly aren't the happiest.**

Actionable advice:

**Do what you want, not what others tell you.**

While this seems like totally obvious advice, the pressure from our family, peers and society means that it's still advice worth giving: if you are pursuing a career or degree that you didn't choose, but was instead chosen for you, then abandon it immediately! The short-term pain of a disappointed dad cannot compare to the lifetime of anguish caused by locking yourself into a path that you find unfulfilling.

**Suggested** **further** **reading:** ** _Smart_** **_People_** **_Should_** **_Build_** **_Things_** **by Andrew** **Yang**

_Smart_ _People_ _Should_ _Build_ _Things_ explores the dangerous consequences of top students' career choices in the United States, and offers practical solutions to reset the country's course toward prosperity by encouraging students to adopt an entrepreneurial attitude. Along the way, the author provides solid advice for budding entrepreneurs on their first adventure into business.
---

### William Deresiewicz

William Deresiewicz is formerly a professor at Yale as well as a graduate instructor at Columbia university, where he received his PhD in English. He is frequent speaker at colleges across America and is a leading social commentator, contributing to publications such as _The Nation_, _The New Republic_ and _The New York Times_.

