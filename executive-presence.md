---
id: 56407b2138663300074e0000
slug: executive-presence-en
published_date: 2015-11-11T00:00:00.000+00:00
author: Sylvia Ann Hewlett
title: Executive Presence
subtitle: The Missing Link Between Merit and Success
main_color: F8D032
text_color: 786418
---

# Executive Presence

_The Missing Link Between Merit and Success_

**Sylvia Ann Hewlett**

_Executive Presence_ (2014) reveals the essential components of a true leader. From attitude to communication to appearance, today's most influential leaders share practical tips and examples to guide you in creating a powerful, convincing presence everyday.

---
### 1. What’s in it for me? Discover how to be a better leader by developing executive presence. 

It's been said of Queen lead singer Freddie Mercury that when he took the stage, whether in a small club or arena in front of thousands of people, not a single individual was left unmoved by his performance. His presence was simply spellbinding. 

This kind of presence, however, isn't just important for musicians or artists. If you want to be a successful leader, the right education and perfect resume just isn't enough. 

You have to have presence. And not just any presence, but _executive presence_. These blinks will show what this means and how you embody it to achieve your goals. 

In these blinks, you'll learn

  * why you don't have to hear a song to know an artist is a major talent; 

  * how chief executive Bob Dudley's gravitas saved BP from disaster; and

  * why you need to get rid of your verbal ticks.

### 2. If you aspire to success, you’ll need to develop executive presence. 

How can you rise to a top position or gain a significant following if you can't convince the world that you are the real deal? To put it bluntly: you can't. 

Whether you're a banker, a salesperson or a musician, having _executive presence_ is a precondition for success. It's a mixture of qualities that demonstrate that you are in command, or at least deserve to be. Executive presence alone is the make-or-break factor. 

The author regularly attends the final auditions of the Concert Artist Guild's international competition. There a huge pool of applicants is narrowed to just 12 talented young musicians who then compete before a very distinguished jury. 

Of course, these young finalists wouldn't have made it to the final round if they weren't outstanding in their musical craft. But in the finals, what separates one finalist from the next has little to do with music. 

So what is that special trait that makes a person stand apart?

Guild president Richard Weinert revealed that the key is in the way an artist walks across the stage; the way her clothes are cut; the spark in her eyes and the emotion written on her face. 

A recent study also demonstrates the importance of executive presence in the music world. Fascinatingly enough, people who were shown _silent_ videos of pianists performing in international competitions were better at picking out the actual winners than those who watched the videos and could hear the music, too. That's a powerful example of executive presence at work.

And it's no different in the workplace. The author's research team at the Center for Talent Innovation conducted a national survey to map out the traits that make up executive presence. 

The results? Executive presence appears to be based on three pillars: the way you act (gravitas), the way you speak (communication) and finally, the way you look (appearance).

### 3. Gravitas, or the ability to exude courage and confidence, is central to executive presence. 

Consider America's series of social crises over the past decade: the dot-com bubble, 9/11, fraud scandals, the mortgage meltdown. It's no surprise then that people praise the ability of a leader who is able to stay cool, calm and convincing amid a crisis. 

Gravitas, according to well over half of the leaders surveyed by the author, is at the core of executive presence. It's that _je ne sais quoi_ quality that makes some people seem like born leaders. Gravitas signals to the world that you have what it takes and can handle serious responsibility. 

The most important part of gravitas is your ability to exude integrity, courage and confidence in times of crisis. When asked to identify what constitutes executive presence, most senior executives felt it was projecting confidence and grace under fire.

So where can you see gravitas in action?

In May 2010, crude oil from a broken well spewed into the waters of the Gulf of Mexico. BP was held responsible for the spill; the then-managing director, Bob Dudley, was grilled by the press in countless interviews. Despite the pressure, Dudley didn't avoid or refuse to answer a single question.

In fact, Dudley assumed BP CEO Tony Hayward's position when the crisis broke. Hayward had created a public relations mess following a series of thoughtless comments. Any chance the company may have had to restore public opinion was obliterated with Hayward's infamous remark in response to the stress of the situation, "I'd like my life back." 

Respected for his cool, Dudley in contrast seemed to be a compassionate, considered and competent leader. 

As a leader, you will certainly make mistakes from time to time and suffer from the mistakes of others. Consider each of these moments an opportunity to demonstrate gravitas. Stay calm and confident amid a crisis to prove what you're really worth.

> _"I don't believe I've ever been able to judge or trust a person unless I can see what they're like under fire." — Bob Dudley_

### 4. Great leaders balance decisiveness with compassion. 

Can you be compassionate and make ruthless decisions at the same time? Most of us would say no. 

But is this true? Not necessarily. Good leaders can and should be both compassionate and capable of making tough calls. 

Decisiveness is another crucial aspect of gravitas. There's no question that we often look to leaders to make the difficult decisions. Sometimes it's not about making the best decision but simply making one when others don't dare. 

Bold moves are a mark of gravitas, because it takes courage to choose one path and then "own" the repercussions of the direction.

When Yahoo CEO Marissa Mayer declared that all employees were required to work from company offices, some leaders, such as former GE CEO Jack Welsh, applauded her decision. Others, including Virgin Group's Richard Branson, condemned the decision as a step backward. 

Either way, Meyer made a bold choice and in doing so, showed that she had grit. This boosted her gravitas and her shareholders' faith in her ability to rescue the struggling internet company. 

While decisiveness and toughness reflect a leader's courage and resolve, such qualities can be dangerous when empathy and compassion are absent. Many decisive but emotionally cold leaders can come across as egoistic, arrogant or insensitive. 

This is why _emotional intelligence_ was seen as an important part of gravitas by the majority of leaders the author surveyed. Having emotional intelligence signals that you possess not just self-awareness but situational awareness, too. 

Mayer's decisions showed her strength as a leader but also revealed her weaknesses. She was criticized not for being tough but for being out of touch with the struggles of working parents — as well as hypocritical. Mayer herself had created her own child-care solution by building a separate nursery near her office for her nanny and infant son.

Making unpopular decisions and enforcing them is indeed part of showing you've got what it takes to be in charge. But it's not the only crucial aspect at play — equally important is the way you communicate.

### 5. Strong communication skills are always a deciding factor in establishing executive presence. 

How do people know you have gravitas? It's all about the way you "talk the walk." 

A 2012 analysis of 120 financial spokespersons found passion, voice quality and presence were the characteristics that made a persuasive speaker, while content was rarely a deciding factor. 

So it's not all about what you say, it's how you say it. Superior speaking skills are often what mark you as a genuine leader. 

Executives interviewed by the author cited inarticulateness, poor grammar, overuse of filler words and an off-putting vocal pitch or accent as the verbal tics that undermine executive presence. 

Those they interviewed mentioned people with "shrill" voices, in particular women, who raise the timbre of their voice when they are emotional or put on the defensive. This shrillness is a turnoff for coworkers and clients, and can lead to lost leadership opportunities. 

So how can one modulate a shrill voice? It's not a matter of learning to sound more "like a man," but rather hitting the optimally pleasing voice frequency of 125 Hz, which is a low frequency, as discovered by scientists at Duke University. 

We humans seem to be hardwired for such lower frequencies, and of course, we usually pay attention longer to voices we find soothing and not irritating. 

It's obviously hard for you to know how you sound, as a recent _Wall Street Journal_ article pointed out, as the bones of your head distort the sound of your voice. So don't hesitate to ask for feedback on your voice from a mentor or a speech coach.

> _"Other people's perceptions of you are very much yours to shape."_

### 6. Learn to read and command a room to convince any audience of your executive presence. 

Whether you're in a TV studio, a concert hall or the team hang-out space, your executive presence enhances your ability to capture and mesmerize your audience. How hard can it be?

According to Suzi Digby, a British choral conductor, you've got to be fast: you only have five seconds to "touch the audience" or get them to engage with your message. Use this moment to show yourself as completely human. Don't overshare or indulge in confessions, but unveil just enough of your core to create a connection between your listeners and you. 

You need to get an audience to like you and root for you, but at the same time look like you don't _need_ to be liked. Then you're in a fantastic position. 

Now that you've grabbed your audience, how do you hold them?

Keeping it simple is key. Be brief, straightforward and tell stories, instead of using bullet points. Former US President Ronald Reagan, an actor by training, earned the title of "The Great Communicator" because of his colorful storytelling and natural inclination to entertain. 

But to command a room like Reagan, you've got to _read it_. Pay attention to the mood and make sure you absorb the cultural cues around you. Then adjust your language, content and presentation style accordingly. These tweaks prove vital to your success as a communicator, which is central to your overall executive presence.

The French company Sodexo's Rohini Anand was once in an especially high-pressure meeting in which she had to convince the firm's top leaders to let outside experts advise them on a sensitive workforce issue. How did she tackle it?

Well, she entered the boardroom ready to present all the evidence she'd amassed. At the last second, she sensed that the room wasn't interested in her research process. Anand changed tack and instead just gave a short summary of the benefits. It worked and she convinced her audience.

### 7. Be polished and groomed if you want to be taken seriously and establish executive presence. 

According to respondents in the author's survey, appearance isn't quite as vital as gravitas and communication. Still, appearance does have a role to play in your executive presence, because your gravitas and communication skills are filtered through it.

The truth is that nobody will bother to assess your communication skills or your ability to make decisions if your appearance shows you don't know what's going on. But if you look sharp, people will be far more receptive to what you have to say. 

For instance, when the author met with D'Army Bailey, a Memphis-based lawyer and former judge who walked with Martin Luther King, Jr., she was impressed with his appearance. He was fit, well-dressed and seemed very young. When the author asked for his secret, Bailey admitted he'd had multiple plastic surgeries. 

While surgery is a radical solution, Bailey explained that he'd long grasped the connection between looking good and looking capable. His appearance was instrumental in making him look confident, credible and trustworthy to his clients within seconds of them meeting him. 

Fortunately, a sharp appearance isn't something you have to be born with. It can be learned, and it starts with good grooming habits. Grooming isn't just key to making good first impressions, but it also signals that you're in control, both to your competitors — and yourself. 

The golden rule is to _minimize distractions_ from your performance. Your appearance should not distract from your professional competence, but rather emphasize it.

On the other hand, poor grooming signals you don't notice sloppiness or that you don't care enough to do anything about it, which doesn't reflect well on how you might manage a team!

Of course, your appearance will not score you a promotion on its own, but people respond visibly to it. A tidy appearance is essentially a statement of respect for your clients, colleagues and for yourself.

### 8. Take care of your body well to signal your ability to excel in a leading role. 

There's plenty of research proving the point that intrinsically attractive people have it easier. They get hired for jobs more, earn more money and even tend to get better verdicts in court than their unattractive counterparts. 

But just because you might not look like a movie star doesn't mean you can't have executive presence. Primarily, it's about embodying fitness and wellness. 

Leadership, after all, is demanding. Would you give a job to someone who looks like he's about to have a heart attack at any given moment? 

Both men and women tend to be seen as less effective in the workplace if they have larger waistlines and higher body-mass index readings, because they are perceived as lacking confidence and discipline. 

GE executive Deb Elam once remarked that being physically fit gives people the confidence that you will take care of what you are asked to do. Why? Because your appearance indicates you're also able to take care of yourself. 

So, all the more incentive to get enough exercise to tone your muscles and allow you to climb the stairs to the executive suite without being out of breath. 

Clothing is crucial too, so make sure your suits fit your actual size, not the size you'd like to be. The right clothes improve your look and also your confidence. But above all, make sure your choice of clothing is _appropriate_ for the audience and occasion. 

One pharmaceutical representative for Bristol-Myers Squibb sent home a member of her team who wore a sundress and open-toed shoes for a presentation at a Princeton, New Jersey, hospital. This summery, casual look was hardly appropriate for serious meetings with people grappling with life-and-death decisions. 

Remember that your work attire is like your armor — it should make you feel invincible and never insecure. With your executive presence in place and the clothes to match it, you'll be well on your way to a leading role.

### 9. Final summary 

The key message in this book:

**Executive presence is a winning combination of confidence, poise and authenticity. It is just as important as your skills and qualifications, and it will make or break your attempts to land a promotion or secure a deal. That's because your executive presence reflects whether you deserve the success for which you strive!**

Actionable advice: 

**Feedback is your friend!**

If you want to refine your executive presence, always ask for feedback. Better yet, ask for _specific feedback_. If you make a blanket request such as "How am I doing?" you'll get a blanket answer. Instead, focus on a recent encounter that required considerable executive presence, such as a meeting with a crucial client or powerful leader. Ask a superior to assess your body language, speech and delivery, command of the room and so on. This feedback will serve you well!

**Suggested** **further** **reading:** ** _What Got You Here, Won't Get You There_** **by Marshall Goldsmith**

Your people skills become increasingly important the further you climb up the ladder of success. _What Got You Here, Won't Get You There_ (2007) describes some of the bad habits that commonly hold back successful people and explains how to change them.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Sylvia Ann Hewlett

Sylvia Ann Hewlett is an economist and business guru as well as the founder and CEO of the Center for Talent Innovation. She has authored 11 critically acclaimed books.

