---
id: 564a119e6634310007c40000
slug: exodus-en
published_date: 2015-11-20T00:00:00.000+00:00
author: Paul Collier
title: Exodus
subtitle: How Migration is Changing Our World
main_color: D0DF80
text_color: 404528
---

# Exodus

_How Migration is Changing Our World_

**Paul Collier**

_Exodus_ (2013) offers insights into one of the most contested social and political issues of our time: human migration. By looking at the effects of migration on everyone concerned, you'll gain insights into the dangers and benefits that migration, and immigration, hold for our economies and societies.

---
### 1. What’s in it for me? Get a deeper understanding of how global migration works. 

Migration has taken center stage as one of the key issues of our time. The issue is debated in the US presidential primaries, it's discussed in the political arenas of Europe and news outlets of all sorts constantly offer us their views on the subject. But how does migration really work and what are its impacts on society?

Occurring primarily as a way for people to find better economic and social conditions, migration isn't a new phenomenon. It has been happening for millennia and has shaped the way our world looks today. It has economical and political impacts on the countries people leave and the countries they arrive in. 

The topic of migration divides entire countries, drives people to vote further left or further right, creates new jobs and threatens people's current ones. Read on for an in-depth look at this complex issue.

In these blinks, you'll find out

  * why migrants from Tonga were less happy in their new country than back home;

  * the positive impact migrants have on their new countries' economies; and

  * how migrants' home countries have received $400 billion from citizens abroad.

### 2. Though migration has increased in recent decades, few politicians want to talk about the issues surrounding it. 

Migration is one of the most pressing issues of our time. As a result of globalization, increased mobility and growing inequality between wealthy and poorer nations, more people migrate today than ever before. But why?

First, there is a significant disparity in income and living standards between "developing" and "developed" countries. Poorer countries often have far lower standards of living, and their social and political institutions tend to be weakened by corruption and don't necessarily work toward the betterment of the nation.

Second, migration has become easier for those who earn good money in their country of origin. For example, it's very expensive to migrate from Congo to Denmark; the entire process becomes a lot easier if you have some money to spare.

Finally, large, established diasporas, that is, groups of people living in a country other than their ancestral homeland, can help new migrants settle and find work in their country of arrival. The larger the diaspora, the more people it will attract. 

But despite this massive increase in migration, politicians don't seem to like talking about it. 

On the one hand, this has to do with the fact that there isn't a lot of data about worldwide migration, as researchers dread sifting through the huge data sets needed to produce scientifically meaningful results.

On the other hand, migration is a complex issue riddled with moral and ethical problems. For example, policymakers have to raise questions like: Is it right to let some migrants into the country, but not others? Will politicians be considered racist if they don't want poor migrants in their country? And what happens to the countries that the migrants leave behind? 

These are some of the questions that will play a decisive role in determining the future of migration policies.

### 3. Migrants have massive social effects on their host country. 

Depending on where they go, migrants receive different treatment when they arrive in a new country. Some countries are more or less welcoming, while others are less friendly. So what are the reasons for these differences, and what effects do migrants have on their new home country?

The degree to which migrants impact their host country largely depends on the speed of migration. A mass influx of migrants, for example, can make the native population feel overwhelmed and hostile toward the newcomers.

When many people migrate to a new country, the diaspora community in that country will grow larger, which can mean that not everyone who arrives feels the need to fully integrate into the host society. 

However, if the influx of migrants is relatively low and stable, more of them will be able to integrate into the dominant culture and society of their host country. In these cases, the host population tends to feel more welcoming of the new migrants, and is more likely to include them in social and economic life.

In the best case, a degree of trust and cooperation can develop between the host population and migrants.

In a society with a national history, a relatively stable economy and functioning government, there exists a certain amount of trust between the members of that society. This trust and cooperation is the foundation for a good relationship between migrants and local residents. 

Whether this happens largely depends on whether there is a _mutual regard_ between the parties. 

Mutual regard describes a feeling of sympathy or fellowship toward others, in contrast to simple respect that doesn't necessarily motivate people to engage with others.

Mutual regard is the basis for any social service programs that aim to help the weaker and poorer members of a society to have a stable and successful life, and it also allows cooperation to bridge the gap between host populations and migrants.

### 4. Migration also has an economic effect on the new host countries. 

Anyone digging deeper into the topic of migration will at some point come across critics as well as supporters, each side bringing economic arguments to the table to support their own views. So which is correct? It may well be both.

On the one hand, the critics have a point. Migration _can_ result in economic problems for a host country. There is evidence that suggests that some parts of the native population will experience problems due to a high influx of migrants.

For example, housing prices might rise and social services may end up being spread rather thin if more people need support from the state. This is the case in Great Britain, where housing prices have increased by over ten percent in a short time, largely due to migration.

Furthermore, immigrants tend to settle in places in their new country where there are open job niches for them to occupy. This can make it hard for the local low-income population to compete for housing with new, skilled workers from other countries.

On the other hand, there are benefits to be reaped from migration. Many economists argue that the overall long-term benefits of migration outweigh the short-term difficulties that some societies have to deal with.

In fact, a 2010 study on the effects of immigration across Europe by economist F. Docquier found that native workers can actually profit from highly skilled immigrant workers, and that migration can also improve the productivity and skill levels of less skilled workers.

Finally, migrants often bring their own wealth with them. While for some the thought of immigrants conjures up images of exhausted, hungry travelers who were barely able to survive in their home country, many migrants are actually well-educated and wealthy. 

And upon arrival in their new country, they will get new, better paying jobs with higher tax rates, and the host country will be able to profit from their labor.

> In London, around 70 percent of elite housing is now being bought by immigrants.

### 5. Many migrants gain enormous benefits by moving to a new country. 

As we've seen, the effects of migration on the host country can be mixed and unclear. So what is the economic advantage for the migrants themselves? 

Before we answer that, let's look at the cost of migration. In short, it's an expensive investment that not everyone can afford to make. Just getting to a new country and finding a place to live costs a lot of money. 

In some countries, the annual income is only about $2,000, so even a ticket on an international flight could eat up years of savings. But, assuming they have the money, what do they get when they arrive in their new home?

Once in their host country, migrants are able to use the existing social and economic system to their advantage.

One of the main problems poor countries face isn't that their workers are unproductive, but that their societies lack the functioning institutions that could enhance economic growth.

For example, if you lived in a country where you were unfairly taxed, there might be no worker protections to aid you; meanwhile, the fruits of your labor could easily be plucked from your hands by someone threatening you or your family. In cases like these, increasing productivity and taking on economic risks just doesn't seem worth it.

Rich countries, in contrast, have social and economic systems that allow people to live stable lives, earn more money and increase their own market value. When migrants enter this kind of system, they can rapidly increase their standard of living without having to build their own system of protection.

So, when a shop owner moves from a country where his very survival is under constant threat, to one where the system simply works, he'll probably be willing to invest more in his shops and grow the business without the fear of becoming low-hanging fruit for extorting oppressors.

### 6. Migrants themselves can also be the biggest losers in the process of migration. 

Though it might seem somewhat baffling, migrants aren't just the big winners in terms of greater income and productivity; they can also be the biggest _losers_.

Following an influx of immigrants, the greatest competition for jobs won't be between immigrants and the native population — it will be between immigrants themselves.

Counterintuitively, it's actually in immigrants' rational self-interest to vote for anti-immigration laws. Immigrants have the double disadvantage of having to compete in the job market as well as having to compete with the native population, which presumably speaks the local language perfectly, has grown up in the host country's culture and often demands higher wages. Migrants, on the other hand, often have to settle for lower-wage jobs.

In this sense, the biggest danger for migrants is a growing diaspora caused by the arrival of new migrant workers, all of whom compete for the same jobs and housing services. Thus, supporting stricter immigration laws would protect them from increased competition.

Moreover, the psychological harm caused by leaving one's home country could actually outweigh the benefits of moving. There are several reasons for this: 

For one, migrants leave family and friends behind to enter a new country in which they might not know anyone. Once there, they will have to deal with a new culture, a new language and finding a job, as well as the stress of keeping in contact with the family they've left behind. 

While it hasn't yet been the subject of much research, there is some evidence that migrants end up unhappier in their host country than before they migrated.

A 2012 study conducted by researcher Steven Stillman on the happiness of migrants from Tonga to New Zealand showed that migrants were 0.8 points lower on a five-point scale measuring happiness after four years of living in the new country.

So while many migrants may experience a higher income, that doesn't necessarily translate to increased happiness.

### 7. Migrants can have ambiguous political effects on their country of origin. 

So far, you've learned about the economic and cultural impacts that immigrants have on their new homes. But how do migrants affect the political landscape in their countries of origin?

As it turns out, diasporas can have a strong political influence. When migrants move to more democratic countries, many use their new position to try and change things for the better in their countries of origin. 

As a result of their new freedoms, they have an opportunity to put increased pressure on political regimes from the outside, without fear of repression or captivity for protesting against an authoritarian government.

For example, since 2000, over a million Zimbabweans have fled Zimbabwe to South Africa, where many have protested against the Zimbabwean regime from their new country. Strangely, however, this has had little impact. Why?

While diasporas can increase pressure for change in their home country, they can also reduce it.

Dictatorial regimes often don't care about these outside influences, as they can still control those individuals in their own country. 

In fact, the remaining population can be easily controlled. Those who leave are often the wealthy and educated — a demographic that is usually at the forefront of political change. This was demonstrated in a famous study by economist Albert Hirschman, which showed that those staying behind in a country tend to be more easily influenced and repressed.

Another way for migrants to influence politics is to get an education abroad and then return home. Many of the political leaders in struggling countries actually got their education in a different, usually Western democratic country. In fact, in 1990, a whopping two thirds of the political leaders of developing countries were educated abroad.

### 8. Migration can function as a serious drain on small countries. 

As we mentioned in the previous blink, it tends to be the wealthy and educated who leave oppressive regimes or hopeless economies. If enough educated people leave, a country can suffer from _brain drain_, or a severe loss of the educated class.

Brain drain is a serious danger for small countries. Research has shown that countries that lose too much of their educated human capital need decades to recuperate and catch up with new technological advances. 

When too many people leave at once, large diasporas form in their host countries, drawing even more migrants away from their country of origin. Haiti is one of the countries hit hardest by brain drain: at one point, it lost over 85 percent of its educated population, leaving the 10 million people behind with few educated workers.

However, there is a silver lining. As the educated population leaves, people in that country will try to educate their children and provide them with opportunities so they can later migrate when they grow up. But even if they get an education, migration is a goal they might never reach. If they stay (by choice or by circumstance), then they can use their education to improve their country.

But small countries do benefit from migration in the form of _remittances_, that is, money sent back to one's home country.

When people migrate, they leave family and friends behind. Naturally, they want to continue supporting them, and give part of their new wealth to their people "back home." These remittances aren't just a boon for family and friends — they also help strengthen the country as a whole.

In fact, in 2012, the remittances sent back from developed to developing countries amounted to over $400 billion!

However, because the average remittance payment is about $1,000 per year, the actual increase in income is often not much bigger than it would have been had the migrant stayed in her home country and increased her productivity and education there.

### 9. Migration policies need to change to deal with modern migration issues. 

Based on what we've learned so far about the impacts of migration, what can we say about sound immigration policy decisions, and what does the future of migration look like?

Right now migration is massive, but it won't stay this way forever. Many people today are leaving their countries because of income disparities, war, famine and other problems that the global community will need to continue working to solve.

In the coming century, we can expect that while world trade and communication will be more integrated, overall mass migration will have decreased. As wealth gaps between nations lessen, so will the need for migration, and the expansion of the internet to poorer regions will make it possible for people to work in markets outside their home country. Just think: even today, call centers in India serve the US market. 

In terms of policy, the burden of regulating immigration will ultimately fall on the shoulders of the host countries. These countries are in a position to regulate the influx of immigrants they will allow in, but countries of origin can't do much to prevent people from leaving, especially if they already have visas. 

But what kinds of migration policies should the host countries propose? They'll need to institute policies that bring migration to a moderate level, regulating the flow of new people — for example, with overall immigration ceilings or higher education criteria for permanent residency — while making greater efforts to promote integration.

Migration is a topic that won't be going away any time soon, and it shouldn't be decided by our morals, but rather by facts and research. And according to the author, research shows that if migration isn't regulated, poor countries will at some point face the ultimate exodus!

### 10. Final summary 

The key message in this book:

**Migration is one of the hottest topics of our time, but is unfortunately one that people tend to know little about. The impact of immigration, both on the host country and the migrant's country of origin, is nuanced and complex. There are no easy answers to the question of how to formulate sound immigration policy.**

**Suggested** **further** **reading:** ** _Immigrants_** **by Philippe Legrain**

_Immigrants_ offers a compelling case for a total revamp of the way most people view immigration and immigrants. It provides a detailed description of the case against immigration, while providing solid evidence for the great benefits, both social and economic, that migration provides.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Paul Collier

Paul Collier is an economist and professor, as well as the director of the Center for the Study of African Economies at Oxford University. He is the author of _The Bottom Billion_ and was named one of the top global thinkers by _Forbes Magazine_.

