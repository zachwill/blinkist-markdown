---
id: 5922e9fab238e10007b6af34
slug: experiments-with-people-en
published_date: 2017-05-26T00:00:00.000+00:00
author: Robert P. Abelson, Kurt P. Frey, Aiden P. Gregg
title: Experiments With People
subtitle: Revelations From Social Psychology
main_color: A5BDBF
text_color: 586566
---

# Experiments With People

_Revelations From Social Psychology_

**Robert P. Abelson, Kurt P. Frey, Aiden P. Gregg**

_Experiments With People_ (2003) is a survey of social psychology throughout the twentieth century, and everything we have come to learn from it. These blinks will teach you about yourself, the hidden sides of human nature, why we make the choices we do and how altruistic humans really are.

---
### 1. What’s in it for me? Catch up on the last century of studies on human nature. 

"Human nature" is an elusive concept often used to explain actions, emotions and behaviors that don't seem to make sense at first glance. The central question is still this: Why do we behave the way we do?

Through social psychology experiments, psychologists are able to understand the extent to which people are influenced by the group they belong to, or how little people understand about the underlying reasons for their actions.

In these blinks, we will get to know more about ourselves and our fellow humans through a number of the 28 most influential experiments in social psychology conducted during the twentieth century, experiments and studies that helped illuminate why we act and behave the way we do.

In these blinks, you'll learn

  * why people spend so much energy on projects that are doomed to fail;

  * how a given situation, not our personality, determines how we act; and

  * why gender roles exist partly as a way for us to make sense of inequality.

### 2. People often have misconceptions about their inner lives. 

Have you ever screamed at your partner in the heat of the moment, only to apologize later, saying you were stressed out about work?

Well, while you might think that stress explains your behavior, chances are you're wrong. The truth is that people are much less aware of their inner emotions than they think.

That being said, people often find justifications for their actions by taking a deeper look at their thoughts and feelings. This practice is known as introspection but, unfortunately, it doesn't work very well. Just take a study done in 1977 by Richard E. Nisbett and Nancy Bellows. In this experiment, participants were asked to rate a hypothetical job applicant named "Jill" based on characteristics like flexibility and likability.

Each participant received a file containing information about Jill, with some files stating that she had recently been in a car accident. Perhaps surprisingly, the experimenters found that knowledge of the car accident had no significant effect on the average likability scores given to Jill by the participants.

However, when the participants were subsequently asked about the factors upon which they based their likability scores — that is, when they were asked to be introspective — they mentioned the car accident as one reason for finding Jill likable. The fact that this self-assessment didn't match the statistical data shows that introspection can lead us to erroneous conclusions about our behavior.

Beyond that, people just aren't very good at remembering things. When we have a memory, we don't just replay a past event; we transform it based on our present beliefs.

A study done by Cathy McFarland, Michael Ross and Nancy DeCourville in 1993 offers a good illustration of this. The participants in this experiment wrote daily reports about their menstrual cycles. Then, when they were asked two weeks later to recall how they had felt, they consistently described having been in more pain and suffering more negative emotions than they actually had.

Why? Most people believe that menstruation is a painful process, or at least supposed to be, and the participants here were no exception. Holding this belief led the participants to retrospectively add more pain to their memories of menstruation.

> _" ...we are more of a mystery to ourselves than we realize."_

### 3. We value things we've made sacrifices for and those that are presented in a favorable light. 

We make countless choices every day, from how to spend our free time to what to eat for dinner. But what psychological processes impact these decisions?

Well, we tend to value things more if we've invested a lot to get them — even if they're not actually very good. Elliot Aronson and Judson Mills show this in a 1951 study: participants were made to endure an embarrassing initiation by reading a lewd passage out loud to an experimenter, as a means to gain entry into a fictitious sex discussion group.

Once these participants had passed this test, they were allowed to listen in on a group session that the experimenters made intentionally dull. The reward these participants received didn't match their expectations given their initial sacrifice, so they simply convinced themselves that the reward was better than it was, overrating the session compared to those who hadn't gone through the initiation.

The issue here is that valuing things based on the sacrifice necessary to achieve them tends to produce bad decisions. More specifically, it makes it harder for us to cut our losses.

Just imagine a government that has already spent $1 billion on a $2-billion dam project. At this point in construction, the engineers notice that the project is going to be incredibly inefficient once completed, and suggest it be abandoned. However, the government has already sunk so much money into it that they plow ahead, ending up with a dam that costs more money to run than it'll produce.

But what's most interesting is that the decisions we make in the first place are a product of how our options are presented. Consider Alexander Rothman et al's 1999 study, which had participants read one of two pamphlets about a mouthwash.

One pamphlet had positive language, like "mouthwash helps fight plaque," while the other had negative wording, like "failing to use mouthwash leads to plaque buildup."

Both said essentially the same thing, but 67 percent of those who read the positive one ordered a free sample compared to just 47 percent of those who read the negative one.

### 4. Situational factors often affect our behavior more than our character traits do. 

Would you describe yourself as always willing to help a fellow human in need? Well, if you answered yes, the truth might not be so clear-cut. In the end, even the most pious or compassionate among us will ignore suffering when in a hurry.

A good example is a 1973 study conducted by John Darley and Daniel Batson, in which theology students were asked to give a brief presentation in an adjacent building. Some of them were told to take their time, while others were told that they were already running late.

On their way into the room in the building next-door, they all encountered an actor who pretended to need help. Of those who weren't in a hurry, 63 percent stopped to help the actor, compared to just 10 percent of those who were rushing, regardless of how they described their own religiosity.

Another factor here is that people tend to forget that others are impacted by situations as well. Consider a study conducted in 1967 by Edward Jones and Victor Harris, which found that people generally explain their own behavior in terms of situational circumstances, but that of others in terms of character.

For instance, you might say, "I didn't call because I had three meetings," but also say "he didn't call because he's forgetful." For this reason, it's important to remember the many challenges that everyone faces, every day.

And sometimes, the situation a person finds herself in simply calls for acting against her intentions. For example, you might try to block out the noise from your neighbor's party when it's preventing you from falling asleep.

If you're stressed out, you won't have the willpower necessary to push the noise out of your mind, which will cause you to think about it more and will make the sound seem louder.

In such situations, it actually makes sense to do the opposite of what you want to do — well, at least according to a study conducted by Matthew Ansfield et al. in 1996. In this experiment, participants listening to loud music fell asleep faster when they made a conscious effort to stay awake!

### 5. Our mental processes cause us to misjudge certain situations. 

Every day, we encounter new and strange situations. So how do we deal with these encounters, and do our strategies work?

At first, we always take things at face value. According to a study done in 1993 by Daniel Gilbert et al, we accept everything we see and only later return to question its accuracy. It's at this point that we decide whether we should continue accepting it or change our minds.

To demonstrate this, the above study had participants read true and false statements about a defendant on trial, and then asked them to suggest a jail sentence. Some of the participants were tasked with simultaneously doing arithmetic, which meant they didn't have time to return to the false statements they had read and decide whether they trusted them or not.

As a result, when the false statements incriminated the accused, they suggested sentences that were, on average, eleven years longer than those of the participants who weren't doing arithmetic.

Another way our minds can lead us astray is by falsely interpreting new situations based solely on "good feelings." Just take a 1976 study by Fred Ayeroff and Robert Abelson, in which participants were put in pairs.

Without being able to see or hear the other person, one of the two participants had to guess which one of five cards the other person was selecting. This process was repeated several times. Afterward, the "guessers" had to estimate how often they had guessed their partner's card selection. Unsurprisingly, they estimated their success rate to be around 26 percent, which isn't much different from sheer chance.

But here's the strange part. One group of participants got to meet their partners before the experiment for a few practice rounds. During this session they were allowed to talk to each other and say what they were thinking out loud.

When these guessers were asked to estimate their success rate when guessing which card the other had selected, they reported a rate of 50 percent!

Meeting beforehand and talking to each other while doing the exercise had fostered a degree of intimacy between the participants, which the experimenters referred to as "good vibes." The participants who felt these "good vibes" believed they had a telepathic connection to their fellow participant.

### 6. We accept fiction as reality to help us deal with the world around us. 

You just learned how people habitually misjudge new situations, but what other processes assist us in dealing with the world, for better or for worse?

For starters, people tend to deceive themselves into believing everything is fine. Imagine that you suspect you have a serious illness. You've read that one symptom of the disease is a loss of appetite and it would be logical to go to the doctor once you suspect something.

However, as a study by George Quattrone and Amos Tversky in 1980 demonstrates, you're most likely to take a different route. Instead of seeing a doctor, you'll reason that, if you haven't lost your appetite, you can't possibly have the disease. As a result, you unconsciously push yourself to eat more than you want to, trying to prove to yourself that you're not sick.

Another way people make sense of the world is by forming gender stereotypes that allow them to rationalize social roles. A 1990 study by Curt Hoffman and Nancy Hirst presented two fictional alien races to participants. They called them "Ackmians" and "Orinthians," and told participants that one species was primarily composed of child-raisers, while the other was predominantly involved in business and industry.

The participants were quick to describe the child-raising species as sensitive and the one with mostly business types as the more self-confident and assertive of the two. Once the participants learned about the different societal roles of these species, they readily used the information to infer their personalities.

In part, this is what happens with gender stereotypes in our society. It's no secret that most CEOs are men, and we can rationalize this by assuming that men are simply more assertive than women, which is why they comprise the majority of CEOs.

But the alien race study points to a reality that's the other way around; we believe men are assertive simply because more of them _are_ CEOs.

### 7. Group loyalty and pressure have a major impact on our behavior. 

Do you act differently when you're alone compared to when you're with others? Most people do, which is why we view the world through the eyes of the group we belong to.

Consider a study conducted by Albert Hastorf and Hadley Cantril in 1954. The experiment asked Princeton and Dartmouth students for their views on a recent contentious football game between the two schools. Of the Princeton fans, 90 percent blamed Dartmouth for the instigation of foul play, while the Dartmouth fans blamed Princeton in comparably high numbers.

That being said, people are much less likely to identify with their team if it's losing, or if they are trying to protect their ego. This is what Robert Cialdini et al. show in their series of 1976 studies.

One of these experiments began by deflating the egos of some the participants by telling them that they had done poorly on a questionnaire they had been given. They were then asked about a recent sports match. It was found that these participants used personal pronouns like "we" and "us" three times less often when describing home defeats than when describing home victories. This goes to show that, when people are feeling low, they try to push away any negative affiliations.

However, some group pressures are practically inescapable, which is the case with _social norms_, the commonly shared rules that define appropriate behavior. An easy example of social norms is the way people tend to say "please" and "thank you."

These norms can be very powerful, and Solomon Asch's 1955 study proves it. In this experiment, participants were asked which of three lines on a single card matched a line on another. They accomplished the task successfully 99 percent of the time.

In a second part of the experiment, the participants were put into a group of actors, who pretended to be participants too. These actors had been instructed to give the same wrong answer when asked to match lines from two cards. In this group, the real participants gave incorrect answers 31 to 37 percent of the time! The wish to adhere to the group norm made them give answers they knew were wrong.

Interestingly, this number dropped to below 10 percent when even just one of the actors gave correct answers, proving that, while norms are persuasive, a single dissenting voice can can go a long way in breaking their spell.

### 8. Groups enable people’s poor behavior. 

It's clear that human behavior is different in groups, and group pressure can easily force us to do bad things.

For starters, the more people we're around, the less likely we are to be of help, primarily because we diffuse responsibility. If someone is gasping for air on the street, the more people there are around to help, the less responsible each of them will feel.

This fact was demonstrated in John Darley and Bibb Latané's 1968 study in which participants each sat alone in a room, listening to a recording of someone having a seizure. The more people they believed to be in the rooms adjacent to them, the less likely they were to halt the experiment and go offer assistance.

Another way this comes up is through _deindividuation_. This term, which refers to losing your identity in a group, makes people more likely to misbehave. Just take Edward Diener's 1976 study, which cleverly tested this idea on Halloween.

In Diener's study, a bucket of candy and a bucket of coins were placed on the front porches of 27 homes in Seattle. Trick-or-treaters were then greeted by an actor who would instruct them to only take one candy, before going back inside.

Do you think the kids followed this rule?

Well, when they were alone, or when the actor singled them out by asking their name, most of them did. But when the kids were part of an anonymous group, they readily disobeyed, taking more candy, coins or both a whopping 57 percent of the time.

Finally, when people are excluded from groups, it can lead to extreme antisocial behavior and even violence. The tragic case of the Colorado students Eric Harris and Dylan Klebold is a prominent example. On April 20, 1999, they went on a shooting spree at Columbine High school, killing 13 and injuring 24 before taking their own lives. In their diaries, it was found that both boys had been ostracized by their peers.

### 9. Attraction and prejudice are motivated by subconscious factors. 

It's pretty obvious that we like some people and aren't so excited about others. But why is this?

As it turns out, we often prefer certain people just because we spend time with them. This phenomenon is known as the _exposure effect_ : the more we're exposed to a person or thing, the more we enjoy it. This concept was demonstrated by Theodore Mita in his 1977 study on people's faces.

Mita presented the participating couples with two pictures of the female partner's face. One was normal, while the other was inverted. The partner who was pictured preferred the inverted picture because she was used to seeing herself in the mirror, while her partner preferred the normal picture as he was used to seeing her face-to-face.

Beyond that, when it comes to finding people attractive, _believing_ that they're good looking is all it takes. This is known as _behavioral confirmation_, a phenomenon in which, if you believe someone holds a particular quality, you'll behave in a way that encourages it.

In other words, if you think someone is attractive, you'll be warmer and more attentive toward them, making them more likely to respond with warmth and charm, which, in turn, will make them attractive to you. However, this also works the other way around, by confirming the negative opinions we hold about people.

Prejudice is also much more common than most people are willing to admit. Take a study done by John Dovidio et al. in 1997.

In this experiment, white participants were briefly shown a picture of a man's face, either white or black. They were then given adjectives that pertained either to a person, like "cruel" or "kind," or to a house, like "drafty," and asked to answer whether they described a human or a building.

On average, the participants who had been shown a black man's face were much faster to answer when given a negative adjective, like "cruel," than those who had been shown a white man's face. In other words, in their minds, "black man" was more heavily associated with a negative word than "white man."

After the experiment, the participants were asked to fill out two questionnaires with questions about ethnic groups. Their survey answers showed that they weren't consciously being racist and that they were generally unaware of their prejudice.

### 10. People can be easily influenced – sometimes even to carry out evil acts. 

We often wish we could get people to do or think what we want, and there are a few good tactics to actually make this happen.

For starters, we can change people's opinions by getting them to willingly take an action that contradicts their views. These contradictions between what we do and what we believe create a feeling of _cognitive dissonance_.

Just consider a study done by Leon Festinger and James M. Carlsmith in 1959. Participants in this experiment, right after completing a tedious experiment, were asked to tell the next participant if the experiment had been fun. The researchers hatched an elaborate story to convince the participants that passing along a positive impression to the next participants, who was really an actor, would be a huge help for the study.

When the first participants lied about their view of the experiment, they experienced cognitive dissonance as they were saying one thing, but had experienced something entirely different. To handle this dissonance, the participants simply modified and improved their originally held belief. Later on, they reported that the experiment had not been so terrible after all.

In this way, by getting participants to willingly act against their own views, the experimenters had effectively changed them.

Authority figures are another means of getting people to act differently — and sometimes even to perform unspeakable deeds. This is what Stanley Milgram's famous 1963 study showed.

In Milgram's study, an actor in a labcoat falsely informed participants that they would be giving a series of shocks to a supposed fellow participant as part of a study on teaching and memory. The fake shock machine used for this had a series of switches, which supposedly ranged in output from 30 volts to 450 volts, and even included an option marked "XXX."

When the coparticipant, also an actor, incorrectly answered a question, the participant at the controls would administer the shock and then raise the voltage. If they hesitated in carrying out the task, the actor in the white coat would encourage them to continue.

So, just how far did they take it?

Well, following the 315-volt shock, the supposed coparticipant flew against the wall before falling dead silent. Nonetheless, 65 percent of the participants behind the controls went all the way, giving a 450-volt shock and even going as far as using the "XXX" function.

This just goes to show the depths to which people will sink when blindly obeying orders.

### 11. Altruism and love do exist. 

Humans can clearly be made to do horrible things — but is there no saving grace for the human species? When you give a homeless person some spare change, are you actually acting altruistically, or do you just want to feel better about yourself?

Well, you can rest assured that altruism does exist. Consider C. Daniel Batson et al.'s 1988 study, which instructed participants to listen empathetically to a recording of a woman named Katie relating the struggles of her life. Afterward, they had the option to commit to helping her for a certain number of hours.

For some people, the sign-up sheet showed that other people had already pledged their time, but despite this opportunity for an excuse, as many as 60 percent of the participants signed up to help. They just wanted to assist someone, even if they knew someone else would if they didn't.

In this sense, they were genuinely concerned for Katie and were acting altruistically.

So, altruism is real, but a pressing question about human nature remains: what is love?

Well, experiments have pointed to a potential definition. These studies suggest that, when you love somebody, you make them part of your sense of self. In other words, their struggles become your struggles and their joy becomes your own.

Arthur Aron et al. put this theory to the test in 1991 in a social psychology laboratory. Their experiment asked participants to sort a series of descriptions into one of two categories: "me" or "not-me."

When asked to sort something that was true for their spouse but not true for them, like "I grew up in Baltimore," they hesitated before putting it into the not-me category.

Why did they hesitate?

The experimenters argued that this was a result of participants associating their spouse's personal description with their own — that is to say, they saw their spouses and their experiences as part of themselves.

### 12. Final summary 

The key message in this book:

**Social psychology has much to tell us about the way we relate to ourselves and others, as well as the human condition in general. By looking at studies from throughout the twentieth century, we can better understand why we act the way we do, what makes us tick and how our minds work.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Influence_** **by Robert B. Cialdini**

_Influence: The Psychology of Persuasion_ (1984) explains in detail the fundamental principles of persuasion that get us to say yes, including how they are used against us by compliance professionals like salespeople, advertisers and con artists. Knowing these principles will allow you both to become a skilled persuader yourself and to defend yourself against manipulation attempts.
---

### Robert P. Abelson, Kurt P. Frey, Aiden P. Gregg

Robert Paul Abelson was a Yale University psychologist and social scientist, as well as an elected fellow of the American Academy of Arts and Sciences.

Kurt P. Frey is professor of psychology at Bridgeport University in Connecticut.

Aiden P. Gregg is associate professor in the Department of Psychology at Southampton University, United Kingdom.

