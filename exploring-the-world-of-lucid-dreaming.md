---
id: 5589206c3466610007200000
slug: exploring-the-world-of-lucid-dreaming-en
published_date: 2015-06-23T00:00:00.000+00:00
author: Stephen LaBerge and Howard Rheingold
title: Exploring the World of Lucid Dreaming
subtitle: None
main_color: 5978CC
text_color: 435A99
---

# Exploring the World of Lucid Dreaming

_None_

**Stephen LaBerge and Howard Rheingold**

_Exploring the World of Lucid Dreaming_ (1990) presents a step-by-step guide to the fascinating world of lucid dreams. It introduces various techniques on how to evoke lucidity and how lucid dreaming can be used to enrich your waking life.

---
### 1. What’s in it for me? Learn how to “wake up” in your own dreams and improve your waking life. 

Do you dream? It's difficult to remember our dreams, as the vivid world of our subconscious all too often disappears with the first light as we open our eyes.

Yet with some training, entering the world of dreams can be a true adventure, and can have positive effects on your overall well-being.

_Lucid dreaming_ is when you "wake up" in a dream and are conscious of what is happening within the dream. These blinks explore how you too can learn the art of lucid dreaming and shows how living in your dream life can enrich your waking life.

In the following blinks, you'll also learn

  * how being more active in a dream will keep you asleep;

  * how practicing jump shots while dreaming will improve your game; and

  * why it's important to ask "Am I dreaming?" when you're awake.

### 2. What you see, touch or smell when you are awake is stored for use later in your dream state. 

We all dream. Yet few people are able to dream lucidly, or consciously "wake up"_within a dream_.

Lucid dreaming may seem strange, yet anyone can do it!

It can actually be beneficial, too. In fact, those who can dream lucidly report that doing so has made a positive difference in their waking life.

But let's start our exploration with dreaming in general, and specifically how our _dream state_ differs from our waking state.

The world you experience when you are awake is communicated through your senses: sight, touch, taste, sound. Through your senses, your brain perceives your environment.

When you take a walk, for example, your brain collects sensory input such as listening to birds, seeing people lounging around and smelling the grass. Your brain then processes this external information and informs you that you're walking through a park.

In dreams, however, it's a different story. The information that your brain uses to generate a dream originates _internally_ because as you dream, external sensory input — like smells or sounds — is limited.

A dream brings together the same process of perception that your brain uses to perceive the outside world when you are awake. The only difference is that in the dream state, there are no external factors to limit what you experience in your (dream) world.

Dreams are created solely by information already in your head, such as memories, desires and expectations. The possibilities of what can happen in your dreams then are endless!

### 3. Keep a dream journal to help you remember your dreams, and better identify your dream signs. 

The first step toward lucid dreaming is learning how to remember your dreams.

Keep a dream journal, as it's the best method to remember your dreams and better understand what they mean. Place the journal by your bed, and when you wake up each morning, immediately note down the things you remember. If you wait to write down your dreams, you might forget details or that you had a dream at all.

So as you're still in bed, try to bring to mind the ideas or subjects in your dream. If your memory is hazy, examine how you feel and what you are thinking of at that moment. This should help trigger the memory of your dream. 

After you've written down the details of a few dreams, go back through your journal and identify your _dream signs._ Dream signs are things in your dream that signal that you're actually dreaming.

For instance, did you realize that you were dreaming at the point when elephants started flying? Or was it when you started to walk through fire?

Dream signs are important, as most dreamers become lucid when they notice an unusual occurrence in their dream, such as time moving backwards.

If you're able to identify a dream sign, you can then keep this information in mind the next time you get ready to sleep.

By jotting down your dream signs in your journal, you will enhance your ability to identify them in the dream state, and recognize that when you see your dream signs, you know you're dreaming.

Once you are able to remember at least one dream per night, and have filled your dream journal with at least a dozen dreams as well as noted several dream signs, you're ready to explore lucid dreaming induction techniques. Read on to learn more!

### 4. Ask yourself, “Am I dreaming?” when awake, and you’ll ask the same question when you’re asleep. 

Do you ever ask yourself, "Is this a dream?" during the course of your day?

Even if you've never done this, if you want to become a lucid dreamer, it's a handy trick to learn. Make it a habit to ask yourself if you're dreaming during your waking hours. The habits you follow when you're awake are the same as those you follow when you're dreaming.

Research has shown that if you ask yourself the question, "Am I dreaming?" at least five to ten times while you are awake, it helps to develop a _critical-reflective attitude_, which in turn prepares you to become lucid in your dreams.

In essence, this habit works its way into your dream state, encouraging you to ask the same question — "Am I dreaming?" — while you're actually dreaming.

But what happens when your dreams seem real? Perhaps the people in your dream argue with you when you say you're dreaming, and then you mistakenly conclude that you are awake. In this case, use _critical state testing_ — that is, you test your reality.

Ask yourself questions while in your dream, such as: "Do I have super powers?" Or see if you can notice whether the content of a book is the same, the first and second time you open it.

Exercising a critical-reflexive attitude is a good start toward dreaming lucidly. But what are the steps to becoming actually lucid?

Just before you drift off, keep in mind that you _intend to remember to recognize that you are dreaming_. This is known as the mnemonic induction of lucid dreams (MILD).

MILD is about creating a way of thinking while awake that will travel with you into your dream state. Be patient, however. There's no guarantee that you'll immediately become lucid, as some people need more practice than others.

It's also good to know that there is more than one way to induce lucid dreams, as you'll learn next.

### 5. If you can keep your mind active while you fall asleep, you can dream lucidly without losing consciousness. 

There are two different types of lucid dreams: dream-initiated lucid dreams (DILDs) and wake-initiated lucid dreams (WILDs).

In DILDs, lucidity is achieved after losing consciousness when you fall asleep. In WILDs, lucidity is achieved when you keep your mind active while you are falling asleep — that is, you fall asleep without losing consciousness.

There are a number of ways you can induce WILDs, such as through hypnagogic imagery and focusing on your breath, your heartbeat or yourself. The most popular way is through _hypnagogic imagery._

Hypnagogia is the experience of the transitional state from being awake to falling asleep. Hypnagogic imagery are what you see in your mind's eye during this transition.

What do these images look like? They typically manifest in light flashes or patterns which gradually develop into faces and shapes, and then come together to form the scene of a dream.

With hypnagogic imagery, your brain stays active and therefore you don't lose consciousness.

So how do you use hypnagogic imagery to induce WILDs?

Close your eyes, relax your entire body, breathe slowly and try to release both mental and physical tension. Continue doing this until you experience a feeling of serenity.

Next, focus your attention on the images that surface in your mind's eye. Observe in a _neutral_ way how the images unfold; don't force them to take shape into anything or anyone specific.

Finally, as the images start coming together to make a scene, let yourself passively be drawn into the dream. Don't enter the dream actively; the key is to stay neutral.

### 6. To control your lucid dreams, you have to learn to stay asleep, retain lucidity and to wake up at will. 

When you're just starting to dream lucidly, it may be challenging to take full control of your dreams. You might abruptly wake up right after realizing that you're dreaming, lose lucidity and return to a deep, unconscious sleep.

This isn't ideal, as doing so blocks the pathways to the many possibilities of lucid dreaming. So how do you avoid this?

To keep from waking up too early, you need to engage in some kind of dream activity as soon as you feel the dream starting to disintegrate.

Some people find that it helps to stimulate their senses within the dream. You can perhaps touch an object, look at your hands or even just turn around in the dream, for example. If you stay active in your dream, you are less likely to wake up.

But waking up isn't the only thing to avoid while lucid dreaming. You might also start to lose lucidity and enter an inactive, unconscious sleep. To prevent this, try talking to yourself in your lucid dream. For example, you might repeat, "This is a dream," out loud if necessary.

Scott G. Sparrow concludes in his book, _Lucid Dreaming: Dawning of the Clear Light,_ that if you rehearse and establish this dream affirmation when you are awake, you'll increase the chances of remembering it during your dream state.

When you want to wake up, you should do the opposite of what you need to do to stay asleep. That is, you must withdraw as much focus and participation from your lucid dream as you can. To do so, try thinking distracting thoughts or saying things like, "I want to wake up."

### 7. Lucid dreams can help improve psychological health by integrating “imbalanced” personalities. 

Being healthy is not just about avoiding disease. Your mental health also plays a huge role in your overall health.

But what does this have to do with lucid dreaming? Lucid dreaming can do wonders for your psychological health.

This is important if you feel your life is out of balance. Your personality, for instance, might be troubled if you've repressed thoughts or emotions that are too difficult to deal with on a daily basis.

For example, when aspects of our personalities are in conflict, or we deny certain aspects of who we are, this can make us unhappy or even antisocial.

This is where lucid dreaming can help. Lucid dreaming can integrate conflicting parts of your personality, thereby improving your psychological well-being.

According to influential psychiatrist Carl Jung, the rejected or unaccepted parts of our personality often appear in our dreams symbolically, as monsters for example, or other creatures. By practicing lucidity, you can learn to accept these shadowy figures so that they unite with your personality and bring about a whole, more robust version of yourself.

But how do you actually do this?

One way is to approach the shadowy figures and take part in a friendly conversation, or greet them as long-lost friends. Rather than resisting these symbols or running away from them, try instead to embrace their presence. You'll find that they'll become less threatening, and as a result, your waking life will achieve better balance too.

### 8. Lucid dreams can inspire problem-solving creativity and solve issues in your waking life. 

You can actually improve skills from your waking life while you are lucid dreaming. The way you can do this is by practicing mental imagery.

Studies have shown that _mental imagery_ and _mental rehearsal_ are techniques that can help enhance performance at work and with leisure activities, such as playing football.

The more time you give to these techniques in your dream life, the more likely you are to improve your performance overall in your waking life.

As an example, Peter is a tennis player preparing for an upcoming tournament, one that he knows he has to win. He is expecting some tough competition, so he decided to focus in his dream state on practicing. During the tournament, he performed better than ever and won all his matches.

Through lucid dreaming, you can also catalyse creative skills to help you solve problems.

If there's a particular issue you want to solve in your waking life, you can approach it creatively when you are lucid dreaming. Using creativity within dreams however is still a young field of research, yet some benefits have been observed.

As an auto mechanic, Alex had a car with a particular problem that he just could not solve. So he decided to dream about the problem to see if he could find a solution. In his dream, Alex took the liberty of experimenting with a number of ways to solve the problem. And just before waking up, the answer came to him.

The wonderful thing is that you aren't limited to the types of problems you can solve while lucid dreaming. You are free to tackle any waking issue that may be bothering you.

### 9. Nightmares are created by fear. When you face your fears while dreaming, they’ll disappear. 

We all suffer from nightmares on occasion, and for some, falling asleep is a stressful time as recurring nightmares are undesirable.

So how do you rid yourself of nightmares?

The first thing to know is that whatever fears you carry in your waking state, these fears may likely manifest in your dream state as a nightmare.

Nightmares are often created by fears that you haven't faced. If, for instance, you're scared of being attacked, and in your dream you're walking down a dark alley alone, it's highly probable that your fear of being mugged will express itself as a mugging in your dream.

If you want to conquer your nightmares, you must face your fears as they crop up in your bad dreams. Rather than dismissing the event as "just a bad dream," you should acknowledge that even though it _was_ just a nightmare, the fear is vivid and real.

In your nightmares, resist the temptation to run away or wake up at will. If you face a scary situation every time one comes up, you'll learn by experience that you cannot be harmed.

For example, you often dream of a terrifying demon. One night, you decide to face it. And when you wake up, you realize that you're safe in bed and the demon didn't hurt you.

The next time the demon shows up, you know that you'll be safe so you decide to face it again. Finally, by the third encounter, the demon will have vanished.

As your expectations of harm dissipated, so did your fears. And as your fears vanished, so did your nightmare!

### 10. Final summary 

The key message in this book:

**Conscious — or lucid — dreaming is something anyone can learn how to do. By practicing some straightforward techniques, you too can reap the benefits that lucid dreaming can give, such as being more creative and learning how to combat your fears.**

Actionable advice:

**If words don't work, draw your dreams.**

If it's too hard to put your dreams into words when you keep your dream journal, then draw! You could jot down figures, symbols or maybe even entire scenes. This way you are likely to improve your dream recall, and gain a better understanding of your dreams.

**Suggested** **further** **reading:** ** _Dreamland_** **by David K. Randall**

_Dreamland_ is a journey into the world of sleep. By examining scientific evidence it explores why we and all other animals on the planet need sleep to survive, as well as how modern-day life can fly in the face of our natural sleeping patterns.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Stephen LaBerge and Howard Rheingold

Stephen LaBerge holds a Ph.D. in psychophysiology from Stanford University, where he conducts research on lucid dreaming. He is the author of the bestselling book _Lucid Dreaming_, and the founder of the Lucidity Institute, which works to further knowledge on consciousness.

Howard Rheingold has studied and written on the augmentation of the human mind. His books include _Excursions to the Far Side of the Mind_, _Higher Creativity_ and _The Cognitive Connection_.

