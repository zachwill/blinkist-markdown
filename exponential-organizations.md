---
id: 54e20703306266000a2a0000
slug: exponential-organizations-en
published_date: 2015-02-16T00:00:00.000+00:00
author: Salim Ismail, Michael S. Malone, Yuri van Geest
title: Exponential Organizations
subtitle: Why New Organizations are Ten Times Better, Faster and Cheaper than Yours (and What to Do About It)
main_color: FED046
text_color: 806823
---

# Exponential Organizations

_Why New Organizations are Ten Times Better, Faster and Cheaper than Yours (and What to Do About It)_

**Salim Ismail, Michael S. Malone, Yuri van Geest**

_Exponential Organizations_ offers an expert look into this new, critical form of company organization that the authors contend will soon become an industry standard. You'll learn exactly what an exponential organization, or ExO, is and how you can build your own. Companies like Uber and AirBnB are some top examples of ExOs; if your company wants to survive, you've got to adapt.

---
### 1. What’s in it for me? Learn what an exponential organization is and how you can transform your firm. 

Think a standard business model will make you billions? Think again.

Huge, slow-moving organizations are no longer the dominant force in business. Today, new companies are agile and achieving growth rates believed impossible in previous generations.

How do these companies do it? Exponential organizations achieve exponential growth, that is, growth that skyrockets in a short period of time. Five-percent-a-year growth rates are a thing of the past; we're talking about exponential growth that starts at 5 percent and doubles every period!

Today's exponential organizations like Uber and AirBnB are defining the shape of tomorrow's companies. If you don't know how to turn your firm into an ExO, then these blinks will show you how!

In the following blinks, you'll learn

  * how ExOs can teach a government a thing or two;

  * why video rental company Blockbuster couldn't defeat Netflix; and

  * why the biggest risk for a company is to take no risks at all.

### 2. While still a new concept, exponential organizations are the future of business organizations. 

Have you heard of companies like _Uber_ or _AirBnB_? If you have, then you've also come face to face with an _exponential organization_.

An exponential organization, or ExO, describes an organization that because of its ability to leverage new technologies can claim _production, output or overall impact that is at least ten times larger_ than a regular organization in the same field.

In short, an ExO can do more, produce more and dominate its market niche with less people or resources when compared to a non-ExO company.

ExOs are experts in adaptation. These companies are part of a new business paradigm, in which competition doesn't necessarily mean heavy hitters based in China or India. In fact, competing firms could come equally from a start-up based in your neighbor's garage!

How long a company has been around or how many employees it has just doesn't guarantee success anymore. _Kodak_, for example, was a historic, large organization that went belly up almost overnight.

This new, finicky nature of the business world has other benefits, too: a company can now experience unprecedented rates of growth and success _with a minimum of resources_.

Exponential growth as a concept is often not completely understood. Take the _Human Genome Project,_ the goal of which was to sequence the human genome. In the project's first seven years, researchers were able to only sequence one percent of our genome; yet in the following seven years, researchers were able to complete the remaining 99 percent!

This sort of rapid progress is what sets ExOs apart — companies like Uber, AirBnB and Google are just some examples of successful ExO firms.

Ideally small and flexible, the ExO model doesn't just apply to businesses, but works also for NGOs and governmental bodies.

Even the scientific field has embraced ExO ideas. Publications such as _Researchgate_ and _Figshare_ are now open source, so their work can reach even more people.

The ideas behind exponential organizations have reshaped much of what we know about business today. And it's only the beginning...

### 3. Old-school organizations are too linear in their thinking. ExOs are agile, flexible and fast. 

Traditional companies like _Coca-Cola_ and _GE_ are still at the top of their industries. Yet if they don't change, their reign will soon come to an end.

Let's look at the reasons why.

First, companies are no longer restricted by traditional limits on growth. _Linear growth_ — the idea that a company will grow consistently by a certain percentage each year — is definitely over.

With technology, this is especially true. _Moore's Law_ states that the computing price-to-performance ratio doubles every 18 months to two years. This means that you as a consumer pay less and less for more and more powerful technology.

Noted futurist Ray Kurzweil maintains that this idea holds true for any organization that is based in information technology. Therefore, traditional companies based on linear thinking and growth are in danger.

The life span of an _S &P 500_ company, for example, has fallen from 67 years to 15 years.

Moore's Law has huge implications for countries like China. While China's economy is still largely based on producing cheap plastic parts mechanically, this process may be disrupted by advances in 3D printing within a few years.

In short, we're in dire need of new ways of thinking. For too long traditional organizations have stuck to linear ways of thinking and linear production processes.

Consider the _waterfall method_ used in software development. This method begins with developers detailing the requirements of a project, then moving to design, implementation, verification and maintenance. It's a straight line that could be easily disrupted by the smallest of problems.

As we find new ways of sharing ideas and building projects, such linear thinking will die out. The developers of _Iridium_ learned this the hard way. While their project, creating a satellite net for cell phones to access global communications, was worthy, the company's planning was slow and organization convoluted, thus it failed.

By working dynamically and avoiding physical infrastructure costs, ExOs can avoid such failures.

### 4. Becoming an ExO is more than just understanding technology. It’s a whole new way of thinking. 

So why would you modify your company to adopt the ideas and structure of an ExO?

First, consider the fundamental advantages of ExO companies. Linear organizations are dependent on having assets and a large number of employees. ExOs have few employees and are thus more flexible, and they can make use of resources that they don't own.

Online rental company AirBnB, for example, has a small team of employees, but they can access a very large number of apartments and houses.

The most important asset for an ExO is information. Customer and market data have to be stored and analyzed. This doesn't necessarily require much manpower, but it does require experimentation and flexibility. Many ExOs manage data automatically with the help of technological innovations.

While not all ExOs share the same characteristics, any organization hoping to become an ExO needs to have a _massive transformative purpose_, or MTP.

An MTP charts an ExO's aspirations. It outlines an organization's higher purpose, showing what it stands for.

Your MTP gives you a competitive edge. Granted, only Google is bold enough to aim for organizing all of the world's information!

An MTP also makes consumers feel good about using a product. _Red Bull_, for example, is more than just an energy drink — when you drink it, you're part of the community that "grows wings."

If you aren't ready to develop an MTP, you can aim first to achieve a clear plan for _corporate social responsibility_. This means offering something more meaningful to both your customers and employees aside from a great product or a comfortable working environment.

For many ExOs, being socially responsible is a required strategy.

### 5. Disruption, unpredictability, risk taking and short-term planning: ExOs love it all. 

ExOs are a big part of society's changing landscape. These companies have emerged from several dynamic shifts in the way modern businesses function.

Huge improvements in information storage and processing technologies have accelerated the speed at which companies can do business. Product cycles are shorter, and new forms of competition have arisen. And today, one search engine (Google) dominates its field, while one e-commerce site (Amazon) has revolutionized online retail.

_Demonetization_ is also on the rise. It's now possible for a business to function with virtually no marketing or sales costs. There's also been a trend toward renting items rather than owning them. That's why the small company AirBnB is overtaking huge hotel chain Hyatt without owning a single hotel!

Another change is that it's now rather advantageous for a company to be small. Small teams are more flexible, can take more risks and can be more specialized. We no longer need experts with Ph.Ds; untrained talent can offer a fresh perspective without being hindered by traditional dogmas.

In 2012, the _Hewlett Foundation_ sponsored a competition to develop an automated scoring algorithm for student-written essays. Interestingly, all three winners had no prior experience with natural language processing. Their lack of experience helped them come up with more innovative ideas!

Disruption and unpredictability are also the new norms. It's no longer practical to plan too far ahead, because it's simply impossible to accurately predict what's going to happen.

Instead of adhering to a plan, ExOs adhere to their massive transformative purpose (MTP) and don't plan things more than a year ahead. Doing so keeps an ExO ready to adapt plans to unexpected changes.

Trust, too, is more essential nowadays. You have to trust your employees to manage themselves if you want to maximize their creativity. Simple tasks can be automated; and micromanaging isn't efficient anymore — if it ever was!

The last development is a fundamental one: the idea that today, everything is measurable and knowable. This is a cornerstone of ExO thinking.

> _"You have to disrupt yourself, or others will do it for you."_

### 6. Every ExO shares five external characteristics. Incorporate this SCALE into your own organization. 

So what does an ExO actually look like? Each company is different, yet each shares some external characteristics.

These five characteristics — _staff on demand, community and crowd, algorithms, leveraged assets_ and _engagement_ — when combined, make the acronym _SCALE_.

Having the right _staff on demand_ is crucial for an ExO. Often ExOs maintain a small, permanent workforce that's diverse, creative and flexible, while hiring contractors when needed. For example, AMP, Australia's largest insurance company, uses contractors for nearly half of its IT department.

Maintaining a _community_ and a _crowd_ both lower the costs and risks of doing business. Your _community_ is your core team, your users, your customers, company alumni, company vendors, partners and fans. The _crowd_ is everyone outside your on-demand staff, or people you use for validation, crowdsourcing or even crowdfunding.

_Algorithms_ aren't as scary as they sound. Search algorithms (a set of automated operations) made Google the world's leading search engine; some human resource departments use algorithms to prevent biases in filling open positions.

The L in SCALE stands for _leveraged assets_. It isn't necessary to _own_ assets anymore — you just need access to them, and be flexible about it, too. While some companies still need to own some assets (like Amazon owns warehouses), if your company is information-based, the situation is different.

_TechShop_, a Silicon Valley-based ExO, has leveraged its assets well. TechShop manages expensive machinery and offers companies access on a subscription basis. Even companies like _GE_ and _Ford_ use TechShop!

_Engagement_ is crucial for an ExO. Networks, games competitions and positive feedback loops are all ways of engaging people. Engagement helps inject your company with new ideas, paving the way for innovation.

> _"No matter how talented your employees, chances are that most of them are becoming obsolete and uncompetitive right before your eyes."_

### 7. Each ExO has lots of IDEAS, including a dynamic interface and dashboard and an autonomous staff. 

ExOs also share certain internal characteristics, which should support the company's external ones.

These characteristics can be broken down into IDEAS: _interfaces, dashboards, experiments, autonomy_ and _social technologies._

_Interfaces_ connect the SCALE externalities — such as your community — with the company's internal workings. ExOs need interfaces to manage work and projects and send them into the world.

Apple's App Store is an example of a fully developed interface, as it allows customers to see what apps are available, and what's more, it's indispensable to Apple and all its products.

_Dashboards_, on the other hand, allow people in an organization to access data in real time. Think of the dashboard in a multiplayer game like _World of Warcraft_ — you always know where to go, how to get there and what to do once you're there.

ExOs also need to _experiment_. While traditional businesses sought to reduce risks, today, the biggest risk for an ExO is not taking any risks. If you want to succeed, you have to experiment.

_Mondelez International_, a multinational confectionary company, employs "garages" to help employees experiment with innovative ideas. Disconnected from outside contact, participants can focus without distraction on the problem at hand, coming up with new solutions.

The ideal ExO employee is comfortable with _autonomy_. ExOs need autonomous, multidisciplinary and decentralized teams if they want to satisfy growing customer expectations.

Technology company _High Fidelity_, for instance, implements a rather extreme form of autonomy in its company charter. Every quarter, employees vote on whether they want to keep their CEO.

And finally, _social technologies_ keep communication channels open. More communication allows for greater transparency, and importantly, this keeps the organization connected to its massive transformative purpose (MTP).

### 8. ExOs need leaders too, but not any old CEO will do. You need to become a chief exponential officer. 

Anyone wanting to change an organization into an ExO must first change themselves.

This starts with the C-level staff, as these individuals will experience the most pressure and be responsible for the results, good or bad.

ExOs are still a new concept. Technologies and trends are moving the business world toward them, but managers and CEOs will feel these changes earlier and more profoundly.

Their actions will determine if the organization is to survive. So you don't have time to hesitate; you need to become _an exponential executive_.

Exponential executives are particularly responsible for detecting _meta-trends_, or trends consisting of many other trends. Trends are crucial. Most contemporary technology wasn't around ten years ago, and new technology is already making today's tech obsolete. 3D printing and autonomous vehicles, as just two examples, are meta-trends.

The CEO of an exponential organization plays a vital role, as she needs to be constantly on alert for new competitive pressures or disruptive start-ups. Staying abreast of information is crucial; yet determining what data is important and what can be ignored is never easy.

In Buenos Aires, for example, market researchers couldn't figure out why people had dramatically stopped using car washes. It was eventually discovered that as weather forecasts had become more accurate in predicting rain, people knew better when they could forgo a wash. An interesting lesson: sometimes explanatory data is available, but you just don't know where to look.

In sum, if you are a CEO, you need to become a _chief exponential officer_. You need to reform your own understanding of your leadership position, as what was done before just doesn't fly in an ExO.

With the right team members and the right attitude, C-level start-ups as well as fully developed organizations can transform themselves into ExOs.

### 9. Don’t just build a platform, be a platform. Live your MTP and conquer your market niche. 

Small, start-up organizations need to follow certain steps in becoming an ExO.

If you're at the head of a start-up, you should start by determining your massive transformative purpose (MTP). This is fundamental. Selecting a MTP is a personal journey, not business decision, as profit isn't an MTP.

So ask yourself what it is that you're meant to do. An organization's MTP should define it and outline the path it will follow.

Next, build a team and community. But remember that while a great team is essential, what you really need is a breakthrough idea.

Don't work with an idea that's limiting or based on technology that's already been developed. Instead, strive for passion and creativity, as that's where great ideas will come from.

The most critical step is establishing the right culture in your ExO. Culture is the glue; it's what holds things together when the boss leaves. Culture also ensures that you adhere to your MTP and any groundbreaking ideas are given their proper attention.

Finally, you need to not only develop a good platform but your company essentially needs to become the platform for your ideas. Facebook, for example, is a great platform. Earlier competitor _MySpace_ failed because its platform was weak, and it lost its community and customers to Facebook.

### 10. So you want to transform your dinosaur company into an ExO? It’s challenging, but can be done. 

Traditional organizations are like dinosaurs; they're going to go extinct if they don't adapt.

While it's possible to change an existing organization into an ExO, it requires a lot more work.

Bigger, traditional companies often rely too much on their own internal processes for innovation. This is why video rental giant _Blockbuster_ lost its market share to online upstart _Netflix,_ for example.

Blockbuster failed to anticipate the opportunities with online movie streaming. It focused on improving its shops, but didn't offer streaming services. Blockbuster's company culture was fixated on store distribution, and crucially, it didn't have a leader willing to take a risk.

Don't be a dinosaur! There are four main paths you can take toward becoming an ExO.

The first is to transform your leadership by educating board members and hiring new, young and inspired C-level employees.

Alternatively, you can partner with or acquire an ExO. If you take this route, timing is everything, because you need to buy an ExO just when it's emerging as a market leader.

And if you do decide to merge, allow the ExO independence. You'll be the one learning from it!

Google did this when it bought _YouTube_ for $6 billion in 2005, at just the right time.

You can also become an ExO by developing disruptive technologies yourself. Keep the tech independent from the mother company, then use it. Amazon's _Kindle_ is a good example of this strategy. Even though Kindle essentially cut into Amazon's physical book revenues, it brought in even more revenue from e-books.

Finally, if you're not able to transform your company into an ExO, you can still adopt some ExO characteristics. Coca-Cola, for example, moved toward creating a massive transformative purpose (MTP) working with the inventor Dean Kamen. The company leveraged Kamen's water purification device to take a stand for cleaner drinking water.

### 11. Final summary 

The key message in this book:

**Exponential organizations are the future of business. If your company is to survive, you must adapt ExO thinking and practices. So whether you're founding a start-up or transforming an existing organization, outline a clear massive transformative purpose, get a flexible team and don't rely on linear planning. Expect the unexpected and always rely on your creativity!**

Actionable advice:

**Don't plan too far ahead.**

Remember that most successful ExOs don't outline plans more than a year out. The five-year or ten-year plans huge companies used to create will only weigh you down in today's business world. Instead, be prepared to face unexpected challenges and tackle them with a creative and open mind.

**Suggested further reading:** ** _Good to Great_** **by Jim Collins**

_Good to Great_ presents the findings of a five-year study by the author and his research team. The team identified public companies that had achieved enduring success after years of mediocre performance and isolated the factors which differentiated those companies from their lackluster competitors.

These factors have been distilled into key concepts regarding leadership, culture and strategic management.
---

### Salim Ismail, Michael S. Malone, Yuri van Geest

Formerly a vice president at _Yahoo_, author Salim Ismail is now the founding executive director at Singularity University. Michael S. Malone is a prominent technology writer who has written nearly 20 award-winning books and has been featured in _The_ _Wall Street Journal_. Yuri van Geest is an international keynote speaker and has served as a consultant for _Google_, _Heineken_ and _ING Bank_.

