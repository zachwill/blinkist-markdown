---
id: 595a1d84b238e100069fb36d
slug: extreme-productivity-en
published_date: 2017-07-06T00:00:00.000+00:00
author: Robert C. Pozen
title: Extreme Productivity
subtitle: Boost Your Results, Reduce Your Hours
main_color: F04B2F
text_color: A33320
---

# Extreme Productivity

_Boost Your Results, Reduce Your Hours_

**Robert C. Pozen**

_Extreme Productivity_ (2012) is a guide to boosting your productivity through time management and expert control over the scope and requirements of your work. These blinks will teach you how to prioritize important tasks, end procrastination and generally become more efficient.

---
### 1. What’s in it for me? Crank your productivity up to the max. 

People tend to think that increased productivity is going to be exhausting. They believe that they'll have to work all the time, at full capacity, to fulfill their ambitions.

But in fact the opposite is true. The more productive you are, the faster you can finish your work. That means more time to do the things that matter to you. And, as a consequence, you will be more happy and refreshed, which will make you even more productive.

So how can we enter this positive cycle? It's really all about simplicity. Productivity is about developing a certain Zen sense of busyness, expending minimal effort for maximum results.

In these blinks, you'll learn

  * how to plan your work efficiently;

  * how to stop losing time and find focus; and

  * why you really can work less than eight hours a day.

### 2. Prioritize your tasks and invest your time accordingly. 

Planning ahead, whether it's for next year, next month or even next week, can be a daunting task when you're drowning in work. But looking down the line will pay off big time in the long run.

To get yourself thinking more long-term, begin organizing your work. Split up tasks into _aims_, _objectives_ and _targets_ so that you can begin to prioritize. A good strategy is to divide tasks according to how long they take to accomplish.

For instance, career aims take five years or more. These are things like expanding your business network or landing an executive-level promotion.

Objectives take between three and 24 months. For example, if you work for a shoe company, an objective might be to re-brand a sneaker line.

And finally, targets are anything that will take three months or less, such as writing progress reports or finishing part of a major project. Objectives and targets are both essential. After all, if you don't focus on these baseline endeavors, you'll never reach your larger career aims.

But it's also important to prioritize aims, objectives and targets that both you _and_ your employer are on board with. You should first focus on the things that both you and your boss want to achieve; once that's done, you can address the tasks that concern only you.

For example, say you want to meet more people in your industry and also want to re-brand your firm's sneaker line to boost profits. Since your employer also wants the second thing to happen, you would do best to focus on the re-branding first.

Then, once you've parsed out your tasks, it's time to set some top priorities, that is, the projects on which you'll spend the majority of your time. To ensure you're doing this effectively, you should track your work days, monitoring how you allocate your time.

You might discover that you spend lots of time on activities that don't have much to do with your objectives or targets. Maybe you're going to too many internal meetings and failing to prioritize the important ones.

How you allocate your time is crucial to your productivity, which leads us to another issue: procrastination.

### 3. Fight procrastination with mini-deadlines that hold you accountable. 

Have you ever found yourself with tons of work to do and no idea where to start? Situations like this can be paralyzing, so it's essential to know how to deal with them.

One approach is to break your projects down into smaller targets, for which you can set mini-deadlines. After all, if you're like most people, you'll only start working when the pressure begins to build.

But instead of waiting until the last minute to churn out a sloppy piece of caffeine-fuelled work, a better approach is to set smaller deadlines that split tasks up into more manageable chunks. That way you can finish your work step-by-step, well ahead of its due date.

When using this approach, you should set your deadlines at similar intervals and avoid having them all pile up at the end. For instance, say you have to write a 6,000-word report in four weeks. Your deadlines could be to write 2,000 words every week for the first three weeks and to spend the final week proofreading and adjusting.

From there, you can hold yourself to your deadlines by sharing them with your boss, which adds some extra pressure and makes you more accountable. For instance, you could tell your boss that you're working on an analysis of your company's competitors and that you've split the task into four chapters.

From there, you could say that you're going to begin on Monday and that you'll have the report in her mailbox by Friday. You could even make a calendar event that you invite your boss to so that she gets a reminder about the due date.

Such an approach guarantees that you'll deliver on your promises, since you won't want to disappoint your boss.

But remember, it's also key to reward yourself after successfully meeting a deadline. So treat yourself to a nice meal or an interesting book to let yourself know you've done well.

### 4. Quickly handle low-priority tasks by keeping your perfectionism at bay. 

At one time or another, you've probably spent so much time on small, simple tasks that you never got around to completing the tasks that truly matter. That's why, when doing work, it's important to get the smaller pieces out of the way right off the bat.

After all, spending too much time on a small, low-priority task will waste both your time and your patience. So instead of allocating a lot of time to work that's not super important, do the simple things quickly to free up more time for the meatier tasks.

For instance, each day you'll be faced with different requests from your colleagues, your boss and your family. As these tasks come in, it's important to decide as quickly as possible whether or not you're going to handle them. If you are, it's then up to you to do so hastily.

A good approach here is to apply the _OHIO principle_, also known as "Only Handle it Once." For example, say you receive an email inviting you to a conference. You quickly browse the email then set it aside. Three days later, you remember the message, but don't remember the name of the sender. As a result, you spend countless minutes scrolling back through your inbox. From there, you've got to read it again, wasting yet more time!

On the other hand, if you were using the OHIO principle, you'd check if the date was free and the topic interesting and make a decision straight away.

Another way to move forward with your work, is to fight the temptation to be perfect when it comes to your low-priority tasks. Remember that not everything needs to be done flawlessly. You should reserve this privilege for your top-level work, since this is what your boss will see and judge.

Nobody is going to be pleased if you spend loads of time answering meaningless emails, so handle such tasks accordingly: get through them quickly and move on to the important stuff.

### 5. Write with efficiency by finding structure and quiet. 

Anytime you read anything, whether it's a novel or a blog post, you get a glimpse inside the mind of the author. Pretty quickly, you can ascertain whether that mind is neatly ordered or dramatically chaotic. And, if you yourself are a writer, your readers will learn much about the inner workings of your brain from the flow of your prose.

So to improve the efficiency and quality of your writing, you need to add structure to the process. You can do this by brainstorming, categorizing and then outlining.

In the brainstorm phase, write down whatever thoughts come to mind about the subject. Don't worry about order and structure; paint in the broadest strokes possible.

For instance, imagine you've been tasked with writing a pitch to your company's executives about how to make the firm more environmentally friendly. You could begin by brainstorming things like company image, product package redesign, energy costs, carbon emissions and LED bulbs.

From there comes the categorizing phase, during which you add some order to your ideas. Group your ideas into categories and then into subcategories. In the case of the environmental pitch, your categories might be intangible gains, such as company image and values; waste and energy, into which packaging and LEDs would fall; and profits and losses, which covers energy costs, carbon emissions and strategic positioning.

Once you've done that, you're ready to outline your piece by arranging your categories in a logical sequence. In the above example the categories are already in the correct order and all you've got to do is write them out, adding an introduction and conclusion.

But efficient writing isn't _just_ about structure. It also requires time and quiet.

After all, how are you supposed to focus with your phone ringing, emails constantly flying into your inbox and colleagues pestering you?

Try going to the office early, when nobody is around, or staying a bit later, after your coworkers have left. Or if your job involves traveling by train or plane, take this excellent opportunity to do some focused writing!

### 6. The product you deliver matters more than the time you spend on it. 

Do you judge the quality of a book on how long it took the author to write it? Of course not. And the same is true of work.

Results are what count. The number of hours spent on any given project is irrelevant. After all, can you think of a single customer or colleague that would want you to spend more time on something if that extra time led to worse results?

For instance, say you write two reports. The first one, on which you spent eight hours, turns out bad because you started it late and were anxious while writing it. The second, which took you only three hours to write, is excellent because you prepared for it and knew just what to write. Which one do you think your boss will prefer?

The point is, it's totally irrational to value the number of hours you spend on something over the quality of the result. That being said, while working faster doesn't mean you always leave the office early, it does mean should feel free to take some time off.

It makes no sense to embrace a culture that favors long hours over productivity. Unfortunately, lots of managers still put more emphasis on hours worked than on results produced.

They tend, often subconsciously, to value workers who put in overtime and weekend hours. As a result, many managers claim to favor results but in practice prefer employees who work long hours.

Given what you now know about productivity, this is obviously a problem. However, it's one that you can change by preventing such a culture from taking root. To do so, simply avoid making remarks that reinforce the importance of working long hours.

For instance, if a colleague leaves early, don't say things like, "Hey, banking hours?" And, if a coworker comes in late, avoid comments like, "Look who finally decided to show up!"

By avoiding such snide remarks you'll be improving your workplace culture. Of course, there are other benefits to cutting back on long hours. For one, it gives you more time for your private life, whether that means an outing with your family or getting some exercise.

### 7. Prioritize your private life and find a flexible place to work. 

Most people just work and work and work, thinking that's the most productive approach. But productivity is about much more than work. It's also closely connected to your personal life and general lifestyle.

After all, the point of being efficient at work is to make time for your life outside of work.

It's important to prioritize non-work related activities and to stick to the standards you set. This is essential for protecting your personal and family time.

For instance, you could make a rule that you'll always be home by a certain hour. If something urgent comes up that absolutely can't be postponed, make a plan with your boss to finish it from home or come in early to finish it up.

Remember, making sure you have time to spend with your family is essential, whether that means cooking together, sharing a conversation over dinner or, if you have kids, reading them a bedtime story. However you do it, it's key to make sure that you're there for the ones you love.

But to make such a goal a reality, you need to show your boss that you're reliable. Build trust with her and she'll allow you to go home earlier or make up time another next day.

If possible, learn to say no to late meetings. Simply postpone them, making a strong point that you need to go have dinner with your family.

And in the best-case scenario, just take a job at a company with flexible working hours. A 2008 survey found that flexible hours are the single most important determining factor for employees when considering a job offer, which, as these blinks hopefully made clear, makes perfect sense.

For instance, imagine your partner suddenly falls ill, or that your kid is in a play that starts at 5 p.m. You wouldn't want to be stuck at work during such important times and, the truth is, you don't have to be.

### 8. Final summary 

The key message in this book:

**Productivity relies on efficiency. That means nothing will stop you from becoming a faster, smarter worker if you set clear goals and priorities. This results-driven approach will do away with procrastination, break up big projects and clearly define your working hours.**

Actionable advice

**Increase your productivity by reading faster.**

The key to reading faster isn't to read more words per minute. It's to read fewer words in total. To try this out just pick up a book on a topic that interests you. Read the introduction, then the conclusion and end by skimming the body for the information you want. You will almost certainly have gleaned the most important information you were looking for. And saved yourself a lot of time.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Getting Things Done_** **by David Allen**

In _Getting Things Done_ (2001), David Allen introduces his famous _productivity system_, aimed at helping people work on multiple projects at once — and to do so with confidence, clear objectives and a sense of control.
---

### Robert C. Pozen

Robert C. Pozen, in addition to being a senior lecturer at Harvard Business School, is the author of six books and a number of articles. Previously, he served as the chairman and vice chairman of various large financial companies and worked on the Bush administration's Commission to Strengthen Social Security.

