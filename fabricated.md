---
id: 58999a0895ee030004e5dd33
slug: fabricated-en
published_date: 2017-02-09T00:00:00.000+00:00
author: Hod Lipson, Melba Kurman
title: Fabricated
subtitle: The New World of 3D Printing
main_color: AFD24F
text_color: 596B28
---

# Fabricated

_The New World of 3D Printing_

**Hod Lipson, Melba Kurman**

_Fabricated_ (2013) offers a detailed view of the nuts and bolts of additive manufacturing — or, as it is more commonly known, 3D printing. In addition to exploring some of the technology's more far-out possibilities, these blinks also provide insight into its more personal implications.

---
### 1. What’s in it for me? Discover how 3D printers will shape our future. 

At first glance, 3D printing may seem like a niche market. Maybe you know someone who uses a 3D printer to manufacture cute little action figures — or something equally inconsequential — in his living room. Sure, 3D printing is neat, but it doesn't seem very revolutionary.

Well, what we're seeing today is actually just the tip of iceberg. Over the next few decades, 3D printing is going to change many areas of our lives — from the working world to cooking to the ways we treat serious illnesses. These blinks will tell you more about the latent power of this emergent technology.

In these blinks, you'll learn

  * why you soon won't have to be rich to become a producer;

  * about designer legs; and

  * whether 3D printing is going to save the environment.

### 2. By means of a computer and a design file, a 3D printer can create any kind of object, one layer at a time. 

Even if you're not very computer literate, you probably know how to use an everyday inkjet printer. You click "print" and the machine spits out a piece of paper with some words or maybe a photograph printed on it.

A 3D printer is a whole other story.

Rather than imprinting a two-dimensional piece of paper with ink, a 3D printer is designed to build objects, varying in size and shape, out of a wide array of materials.

Like a standard printer, a 3D printer can't do anything without a computer to tell it what to print.

The computer is needed to send the 3D printer information from a design file, which is basically an electronic blueprint for what you want to build.

So if you want to print a copy of your favorite seventeenth-century brass vase, for instance, you have to first get the right design file. With that, the computer can tell the printer to fabricate a replica that is an exact match in each and every way.

The printer does this by painstakingly building a three-dimensional object, one layer at a time.

For your brass vase, it would start with the bottom, creating a thin, flat layer of brass that represents the base. Once that solidifies, another layer is added to the top of the first one. And this process continues — a new layer being added after the solidification of the last one — until the entire vase is finished. By changing the dimensions of each layer, an object of any shape can be created.

Using this process, anyone can fabricate any object, as long as the details are contained in the design file.

In the blinks that follow, we'll explore the major impact 3D printing will have on our lives, the economy and the environment. But first, we'll take a closer look at the software that makes it all possible.

### 3. Design software creates a three-dimensional environment to help you realize your object. 

Did you know that most of the furniture in your home was probably designed digitally with the help of sophisticated computer software? In all likelihood, that lamp on your desk, or even the desk itself, was once a concept that only existed as ones and zeros in a computer file.

What's great about design software is that it offers designers and engineers the opportunity to shape and model their designs in a three-dimensional environment.

This is extremely useful since it offers a world filled with geometric possibilities that two-dimensional blueprints simply lack.

Take a look around. Everything you see — the entire world — is made up of countless interacting shapes. Obviously, it's hard to accurately represent the three-dimensionality of all these shapes on a flat piece of paper. But with the help of 3D design software, you can create a perfect model of whatever object you wish to create — be it a house, a car, a coffee mug or even a flower.

The software creates these accurate reproductions by using an XYZ coordinate system and programing that involves highly complicated mathematical equations. This allows you to move your object around, observe it from any conceivable angle and design the tiniest details.

If you want to turn your object upside down or look at it from directly above, you can; the XYZ coordinates will readjust to whatever angle you desire.

You can also adjust the proportions of your object. Widening your vase's aperture, for instance, is just a matter of stretching it out on the screen. Making it smaller is just as easy. All of this can be done with ease; it's not necessary to redesign the entire thing.

Thus, 3D design software provides an invaluable service. It gives you a safe environment to experiment and realize your vision.

In the next blink, we'll see how 3D printing differs from traditional production methods.

> _"Computer design files are the language of modern engineering."_

### 4. 3D printing offers major advantages when compared to mass production and artisanal craftsmanship. 

You might be asking yourself: What advantages does 3D printing really provide? After all, traditional production methods have worked so far, right?

Sure, today's methods of mass production enable the efficient production of specific products. But we run into problems when the product needs to be changed.

When a production line is set up, every piece of machinery and each worker is assigned a very specific task so that work can be done precisely and at a low cost.

This works well if you want to churn out millions of copies of the same product. On the other hand, it also makes it difficult to change even one design detail.

Say you make toothbrushes. One day, inspiration strikes, and you have a genius idea for a new and improved model. Well, unfortunately for you and your company, this would mean major changes: production couldn't get rolling until personnel were retrained and assigned new tasks. As you can imagine, the more complicated the new product, the trickier the reorganization process.

Okay, but let's say you're an artisan of some kind. Artisanal production is the polar opposite of mass production. Products are usually hand crafted and one-of-a-kind — things that can't be made on an assembly line.

Artisanal products take more time and money to produce but they also give the producer more freedom. It's easy enough to alter a design or add custom touches. If a customer wants to order a personalized vase, for instance, there's no problem. The sculptor could even add a unique inscription.

The downside is that artisanal businesses can only produce so many products. But there are compensatory perks: less overhead, for example, and the fact that customers are usually willing to pay more money for a unique product.

But wouldn't it be nice to find the middle ground between artisanal and mass production? Well, that's where 3D printing comes in.

Like mass production, 3D printing allows you to make an endless number of precise reproductions. Though the printing process can't be broken down into simple steps that would increase output and lower the price per unit, it does allow for freedom of design changes. At any time, product design can be easily altered without fear of business repercussions.

### 5. 3D printing makes innovation safe and affordable, and it may revolutionize the manufacturing world. 

Over a century ago, the assembly line disrupted the manufacturing status quo. It forever transformed the job market and the way companies do business.

And this is exactly the kind of disruptive potential 3D printing has.

First of all, traditional production systems could be revolutionized by _cloud manufacturing_.

Nowadays, the majority of manufacturing is done by a few giant factories. But cloud manufacturing works by decentralizing production and spreading it over a network of small contributors.

If you have a product, you can simply add your design file to the cloud and the system will match it with the appropriate manufacturer and make sure that they receive your order and deliver it to your address.

With this method, you can have a wide array of options. You can get a big order printed, a single part or anything in between.

Maybe you spent the past few months designing your own bike and want to test it out. Using cloud manufacturing, you could get the parts of your bike delivered the next day and assemble them yourself to give it a test ride.

This business model shifts the focus away from big factories, expensive machinery and the engineers needed to operate them. It allows small-scale manufacturing companies to enter the marketplace, which will inevitably change the way the business world operates. We'll no longer have to rely on mass production; cloud manufacturing gives anyone with a design file the chance to start a business.

This business model will also lead to more innovation since it greatly reduces risk and cost.

Since today's big manufacturing companies use enormous, complex and expensive equipment and technology, any attempt at innovation requires a huge investment. Such an investment represents a considerable risk since any failed attempt can leave a company with severe losses.

3D printing removes the costs and risks of experimentation. If you want to print a test run of a new product, just prepare the design file, click "print" and see what happens.

> _"3D printing will make Makers, consumers, and small companies into ants with factories."_

### 6. We are already beginning to print life-saving objects such as body parts and food. 

Perhaps the most exciting thing about 3D printing is the limitless possibilities. In addition to making it easy to print standard material goods, the technology also has the potential to save lives.

People are already climbing what's known as 3D printing's "ladder of life."

The ladder of life is a model that represents complexity, with each rung signifying how difficult it is to print a certain thing. The lower rungs are occupied by things that are relatively easy to print — inanimate objects such as hearing aids or artificial joints. Halfway up the ladder, things get more complicated: living tissues, bones, skin and veins. After that come organs like the liver, kidney and heart. At the top of the ladder are living creatures.

We're a long way from the top of the ladder of life, but we're getting pretty good at printing things from the lower rungs, such as replacement bones that are designed to fit perfectly into a person's body.

There's even a start-up in California called Bespoke Innovations that prints custom-made prosthetic legs that not only fit an individual's body but are designed to complement that person's lifestyle and personality.

In the future, we'll probably start 3D printing food. Sometimes, after a long day's work, the last thing you want to do is prepare a meal. Well, imagine sending an email from work that tells your 3D printer to have lasagne and tiramisu ready for you when you get home.

That idea isn't so far-fetched: 3D food printers are already being tested in laboratories. One design team by the name of Zigelbaum & Coelho has created Cornucopia, a project consisting of four different 3D food printers, each devoted to a different type of "cooking." One of them even prints out chocolate.

In our final blink, we'll take a look at some of the concerns raised by 3D printing.

### 7. In regards to the environment, there are both advantages and disadvantages to 3D printing. 

3D printing has given rise to a slew of concerns. Foremost among them is its carbon footprint.

Today, most manufacturing companies still use petroleum-based fuels to manufacture and transport their products and, as a result, harm the environment with their large carbon footprint.

These companies also produce enormous amounts of non-recyclable waste that pollutes the earth and the ocean.

Unfortunately, 3D printing doesn't solve environmental problems related to regular ways of manufacturing. In fact, when compared with current mass-production standards, its carbon footprint and waste production can be worse.

Of all wasteful materials, plastic is among the worst. It's everywhere and just about everything comes packaged in it.

And plastic, though versatile and cheap, isn't environmentally friendly. This remains a major issue for 3D printing.

According to researchers at the University of Nottingham, 3D printers are voracious consumers of energy. For example, if you tell an average 3D printer and a traditional production machine to make an object of equal weight, the 3D printer will consume up to 10 times more electricity than its more traditional counterpart. And since many industrial-sized 3D printers use thermoset plastic, a material that can't be recycled after it is heated, they actually create _more_ waste.

It's not all bad news, however. The researchers at the University of Nottingham did spot some benefits too.

After traditional manufacturers heat up their plastic and press it into molds, they use "release agents" to pry the plastic out. These substances are generally toxic chemicals that add to the harmful waste that gets produced. 3D printing avoids the need for release agents since the plastic never reaches the same degree of heat in the process.

Another good example of an environmental advantage to 3D printing is metal. Traditional metal manufacturing results in literally tons of wasteful byproduct. When making airplane parts, for instance, manufacturers will process 15 kilograms of metal to make a part that weighs just one kilogram.

In contrast, nearly 100 percent of the leftover metal powder that 3D printers use can be recycled.

Clearly, 3D printing can't solve all the world's problems, but it's still early days and there are benefits and exciting possibilities that make the technology a fascinating tool that will continue to be explored.

### 8. Final summary 

The key message in this book:

**The age of 3D printing is upon us, and it has the potential to change our lives. Even though the technology is still in its infancy and comes with disadvantages in certain areas compared to mass production, it will certainly provide great opportunities and innovative remedies in the future.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Makers_** **by Chris Anderson**

_Makers_ (2012) outlines the radical changes that are taking place in the manufacturing world, made possible by the internet and digital manufacturing technologies, and explores its implications for business and society.
---

### Hod Lipson, Melba Kurman

Hod Lipson is a professor of mechanical engineering at Columbia University, where he directs the Creative Machines Lab. He specializes in robotics and asking questions about whether they'll ever become self-replicating. His work has appeared in notable publications such as the _New York Times_ and the _Wall Street Journal_.

Melba Kurman is a technology writer and blogger who is interested in the impact technology has on our lives. She holds degrees from Cornell University and the University of Illinois. Her other books include _Driverless: Intelligent Cars and the Road Ahead_ and _Tech Transfer 2.0_.

© Hod Lipson, Melba Kurman: _Fabricated_ copyright 2013, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

