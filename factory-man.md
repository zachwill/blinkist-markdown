---
id: 54fdbdea626661000a250000
slug: factory-man-en
published_date: 2015-03-10T00:00:00.000+00:00
author: Beth Macy
title: Factory Man
subtitle: How One Furniture Maker Battled Offshoring, Stayed Local and Helped Save an American Town
main_color: CC2941
text_color: CC2941
---

# Factory Man

_How One Furniture Maker Battled Offshoring, Stayed Local and Helped Save an American Town_

**Beth Macy**

_Factory Man_ unveils the dark side of globalization; that is, the horrific impact it has had on American business and the lives of factory workers. In its detailed examination of twentieth-century furniture manufacturing, it reveals how to fight against the death of the local economy and, more importantly, why this fight is worth it.

---
### 1. What’s in it for me? Learn how offshoring hurts furniture companies, and how one fought back. 

Few threats are as serious to American manufacturing as offshoring. Cheap production in China, Taiwan and Indonesia, combined with government support, has driven down the prices of manufactured goods in America.

Furniture manufacturing took a particularly hard hit.

American factories closed. Mass swaths of employees were laid off. Entire towns were decimated.

These blinks are the story of offshoring's attack on American furniture manufacturing, through the lens of a Virginian family business, that fought back. It's about how an industry came together to fight the low prices of Chinese furniture, and learned that their brand was more than just a sticker on the bottom of a kitchen table.

In these blinks, you'll learn

  * how to fight offshoring;

  * why making prices as low as possible isn't a good strategy; and

  * how China's government helps its companies get the upper hand.

### 2. American furniture manufacturers experienced a golden age in the early to mid 1900s. 

On the tail end of the industrial revolution, the early 1900s were marked by innovation in technology and manufacturing. Computers, automation and global networks were yet to come, and people willing to break a sweat were the driving force of new potential.

It was the ideal climate for traditional American industry, such as furniture-making.

As people relocated from rural areas to towns and cities, to be closer to the new factories and industry in which they worked, demand for mass-produced furniture boomed.

The expansion of the railroad networks made it possible to transport these large pieces of furniture easily, such that a furniture manufacturer could service customers across the United States.

John David Basset Sr., who began making bedroom furniture in Virginia in 1902, profited greatly from the era's economic climate.

Seeing the potential of using his family's land and forests to build furniture, he procured a loan from his uncle and began producing bedroom furniture, shipping it as far as Canada.

At the time, furniture-makers profited from cheap labor, which kept the production costs at bay.

Although factory jobs offered low, often unfair wages, they were preferable to the alternative jobs of the time — working outdoors in fields or mines. The first workers in Basset's factory worked for only five cents per hour!

African-American workers were a cheap source of labor, having been freed from slavery only half a century prior and still subject to discrimination, alienation and unequal treatment.

Until 1933, the Basset Factory was one of the few that hired black workers, albeit at a fraction of the hourly wage given to their white counterparts. In any case, employing cheap labor allowed the Basset family to produce at a lower cost than its competitors.

### 3. Since the early 1980s, Asia has been gradually overtaking the furniture-making business. 

During the first half of the twentieth century, the Bassets and other American furniture-makers competed almost exclusively within the American market.

However, the cost of manufacturing in America increased, partially due to regulations around factory conditions and labor laws; and simultaneously, less-regulated, cheap manufacturing emerged abroad.

In the late 1970s American companies started _offshoring_ to Asia, taking advantage of cheap labor abroad.

The first foreign-owned factory in China was a purse manufacturer, which opened its doors in 1978. At the time, no one thought that offshoring was feasible for products as large and unwieldy as furniture.

But within only one year it proved possible: small pieces of Asian furniture, such as cocktail tables, entered the American market.

In the late 1970s, a furniture worker in America earned $5.25/hour, whereas his Taiwanese counterpart earned only $1.40/hour. Furniture workers in China earned even less: 35 cents/hour.

One by one, American furniture manufacturers started the process of offshoring, buying certain parts or even finished products from Asian companies and then selling them under their own trademark in the US. 

Eventually, Asian factories began producing the furniture completely independently and selling it in America.

Soon, Asian furniture factories were providing more than just cheap labor. They were now in direct competition with American factories.

Larry Moh was the first to build a furniture factory in Hong Kong with the intent to export that furniture to the American market. He practically copied models of American companies like Basset, selling them for 20 to 30 percent less, even after considering the transportation costs.

In less than a decade, his company Universal Furniture became the fourth largest furniture-maker in the world!

Things only got worse for American manufacturers when China joined the World Trade Organization in 2001. In fact, from late 2000 to 2002, Chinese furniture exports jumped by 121 percent!

> Fact: The minimum wage in Indonesia in January 2014 was $200/month.

### 4. Asian products are welcomed by consumers and companies. 

When Asian companies started selling their products in America, Americans could have used their purchasing power to support America. But they didn't.

Consumers soon welcomed the Asian furniture because of the lower prices, and shifted their attitudes to be ok with buying from overseas.

Of course, shipping from Asia to America proved difficult at first. Customer service, for example, was under-developed: how could you have defective furniture repaired from across the ocean?

But American customers soon became hooked on the lower prices. Why pay $1,000 for a bookshelf when you can pay $500?

In addition, the disposability of lower-cost furniture entered the consumer mentality. Even if the Asian products weren't as high quality or durable as the American-made ones, consumers didn't care. After all, no one expected to leave their bed frame to their grandchildren.

These attitudes took a considerable toll on the American furniture manufacturing market. In fact, by 1998 one third of all wooden furniture sold in the US was imported.

American companies, too, began embracing Asian products. Retail chains profited greatly from Asian products, as their cheap prices outweighed their shipping costs. Asian factories were likewise eager to export their wares, so they offered competitive prices.

Even American furniture-makers had an interest in shifting production from the United States to Asia. After all, offshoring gave them deep discounts in labor costs and netted them enormous profits, thus allowing them to compete with their Asian counterparts.

Asia offered a way for American companies to produce more cheaply, and pass these benefits on to the consumer in the form of low costs. So what's the problem?

As you'll see in our following blinks, offshoring and importing actually hurts _everyone_.

### 5. Importing often excludes fair competition. 

The problem American companies faced when competing with Asian imports wasn't simply that they had to compete with cheaper producers. Rather, the nature of competition was simply unfair.

Cheap imports can create a cycle of price competition that eventually drives businesses into the ground.

Domestic companies are essentially _forced_ to import in order to match the prices from foreign competition. As a result, many companies simply can't compete and are driven out of business.

Take Kincaid Furniture, for example. In the early eighties, Kincaid had 12 factories and employed over 4,000 workers. However, it soon ended up on the wrong side of a price war, and was forced to start importing.

Their best-seller, a dining room chair selling at $220, was copied by Universal Furniture and sold for a faction of the price: only $39. Even after Kincaid started importing, they simply couldn't compete with Universal, and in 1989 they were finally bought out.

Further complicating the issue is that Chinese factories are sponsored by their government — an advantage that American companies aren't typically afforded.

Asian manufacturers are clearly advantaged in terms of labor, but they still have to pay for materials and transportation. That's where the government comes in:

In China, for instance, export policy centers around improving the standard of living for locals. To do that, local companies have to enter the American market, meaning that their prices had to be very low to compete. Keeping prices so low meant possibly losing money, so the government created subsidies to keep these export businesses profitable.

How can American companies possibly compete in this scenario? Even if they move their production offshore, they still need to break even, and without government support, that means selling at a higher price than Chinese companies.

> Fact: The Chinese government used to provide financial support to companies that exported furniture to America at a loss.

### 6. Offshoring and importing have had dreadful effects on the plight of American workers. 

American manufacturers have it bad when it comes to competition with foreign imports. But the real loser is actually the American factory worker.

As American companies offshore more and more aspects of production, large numbers of American factory workers are finding themselves without work.

For this reason, it's important that conversations about foreign trade don't focus on management and board of directors, at the expense of the people who are actually working at the factories.

One after another, American furniture producers have been driven to close their local plants, leaving thousands of factory workers without jobs.

Since China's admittance into the World Trade Organization in 2001, 63,300 American factories closed their doors, leaving five million American factory workers without a paycheck.

Adding to this misery, most of these displaced workers are unable to find jobs after a factory closure.

Some argue that getting laid off is simply a risk that everyone faces in the job market. You simply have to pull yourself up by your bootstraps and find another job. The reality is different for factory workers, who find themselves out of other options.

If you're an accountant, for example, and you get laid off, you can find another accounting job somewhere else. But if you get laid off as a factory worker, _where_ do you go when _all_ the factories are closed?

While work training programs exist to help people change industries, most former factory workers don't take advantage of them. Having worked a factory job their whole lives, many aging workers lack the skills or self-confidence to find work in another field, or are simply too busy trying to scrape some money together through part time jobs.

Despite these depressing facts, there's still good news: falling victim to a globalized economy isn't a matter of fate. Our final blinks reveal how American factories can get back on their feet.

> _"Between 2001 and 2012, 63,000 American factories closed their doors and five million American factory jobs went away."_

### 7. American business has a right to be protected by law. 

Something feels awfully immoral about the prospect of factory closures and widespread unemployment, due to unfair competition. But it's not just immoral; it's illegal!

According to the standards adopted by members of the World Trade Organization, it is illegal to _dump;_ that is, it is illegal for a country's government to actively try to run companies in another country out of business by setting prices that are lower than the actual production costs, or lower than the ones set by its own local market.

If a country can prove that dumping has forced domestic companies to close factories or has led to higher rates of unemployment, then that instance of dumping is considered illegal by WTO standards.

In essence, local factories have the right to protection from artificially-priced Asian imports.

If American businesses can prove an instance of illegal dumping, then they can even have grounds to seek government aid.

The protections offered by the WTO work in concert with laws such as the Continued Dumping and Subsidy Act of 2000, which levies compensation fees, or _duties,_ onto the worst dumping offenders. The fees gathered then flow right back to the American businesses that were affected.

So, for example, if the three biggest factories in China hurt 20 American factories by dumping, these 20 factories can receive the new duties imposed on the three Chinese factories, and thus repair the harm done to their businesses.

All in all, the law is on the side of American businesses when it comes to competing with cheap foreign imports.

### 8. American producers can fight against competition from Asia. 

Knowing that the law is on their side, why don't American companies take their legal claims to the courtrooms?

Well, American companies certainly _can_ work against dumping by taking legal action, but it's not easy.

To initiate a Commerce Department investigation on dumping, companies need to gather petitions from 51 percent of the industry, and must also dig deep in their pockets for research and legal costs.

These costs are especially difficult to surmount when competing companies are already so concerned with cutting prices and lowering costs.

But that doesn't mean it's not possible!

A prime example of this is John D. Basset III, the grandson of the original Basset founder, who fought tirelessly to initiate an investigation against the People's Republic of China. Many of his colleagues mocked his efforts, and former business partners cancelled deals out of anger that he was trying to stop the low-cost parade.

But John persisted. He spent hours on the phone, at meetings and in conferences, giving interviews and speeches and trying to mobilize people to be as passionate about keeping American jobs as he was.

It took him about a year, but eventually, in October 2003, he managed to get 57 percent of the remaining American furniture producers to file a petition against the People's Republic.

As a result, the Department of Commerce started an investigation and determined that Chinese factories were indeed dumping, and thus granted John and his co-petitioners the right to collect on the duties imposed on the biggest dumpers.

With enough determination, American companies can compete foreign companies no matter how low their prices. Instead of moving offshore the minute things get tough, companies can think of more imaginative ways to fight cheap imports.

In John's case, he was able to use the duties he received to modernize his plants, and found ways to increase efficiency and thus regain a competitive edge.

### 9. Offshoring can be good for business's bottom line, but so can protecting local jobs. 

What motivated John D. Basset III to fight so hard against dumpers? Wouldn't he have been better off closing the local factories, moving production offshore and counting his earnings on a beach somewhere?

A business doesn't just make money; it delivers value. As the leader of a company, you have to ask yourself what your company does: does it generate profit or does it do something more?

Since the 1980s, the majority of the furniture manufacturers have moved production offshore, closing almost every factory on American soil. They've gone from importing certain parts of their wares to moving the entire production process to Asia, stamping it with their brand name and shipping it to the US.

At that point, whose product is it really? Can an American brand be considered American based on the brand name alone? Even if it was produced on the other side of the world by foreign workers using foreign materials? Probably not.

Companies have a duty, both to their workers and to the community. Furniture-making companies have traditions, and in many cases, these factories have employed generations of factory workers, all relying on the company to provide for them.

Consider the Bassets, whose business started in 1902. Although family quabbles caused John D. Basset III to distance himself from the original family company and take the reins of his uncle Vaughan-Basset's company, he continued to feel responsible to his workers and fought hard for the company's best interests. By his hard work and determination, he saved 700 jobs in Galax, Virginia alone.

### 10. Final summary 

The key message in this book:

**The uncontrolled import of furniture from Asia has led thousands of American factories to close, and has left millions of Americans without a paycheck. But it doesn't have to be this way: companies can take a stand for their workers and communities, and combat the negative effects of globalization.**

**Suggested** **further** **reading:** ** _Small Giants_** **by Bo Burlingham**

There's a new phenomenon emerging in the American business world: _Small Giants_. These are privately owned companies that don't follow the usual corporate dogma of growing revenues at any cost. Instead, they're driven by their heart-felt enthusiasm for their product, and focus on factors like quality and caring for their workforce. The author has examined 14 small giants to explain how this strategy has made them successful.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Beth Macy

Beth Macy is an American author and journalist who has won more than a dozen awards for excellence in journalism. As the daughter of a factory worker, she has devoted her work to the lives of regular people.

