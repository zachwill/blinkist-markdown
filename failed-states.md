---
id: 54a98b2c62333200093a0000
slug: failed-states-en
published_date: 2015-01-06T00:00:00.000+00:00
author: Noam Chomsky
title: Failed States
subtitle: The Abuse of Power and the Assault on Democracy
main_color: 2A6AC9
text_color: 2A6AC9
---

# Failed States

_The Abuse of Power and the Assault on Democracy_

**Noam Chomsky**

In _Failed States,_ author Noam Chomsky details the ways in which the United States has used its power to relentlessly pursue its own geopolitical and economic interests. The book cites examples from throughout history to demonstrate why the United States' stated goal of promoting democracy is inconsistent with its own actions, at home and abroad.

---
### 1. What’s in it for me? Learn why the United States is not a force for good in the world. 

US leaders and politicians never tire of telling the rest of the world that the United States helps to keep the globe safe and free. They claim that their country should be the world's policeman; protecting liberty and fighting evil, wherever it is found. 

Yet actual US foreign policy doesn't deliver on these lofty promises. In fact, far from being a safeguard for freedom and democracy, the United States and its involvement in foreign affairs often makes the world more violent and less fair.

These blinks outline just how the United States has worked to impoverish nations, start wars, commit war crimes and even reject peace plans — how, in sum, the United States is a danger to democracy.

In the following blinks, you'll learn

  * why we are on the brink of a nuclear holocaust;

  * why the United States is hindering the Middle East peace process; and

  * why the United States is not a true democracy.

### 2. The United States claims a special status that allows it to ignore international law. 

People often think of the United Nations (UN) as an international democratic body in which all the nations of the world get to have a say, more or less equally.

This is hardly the case. In fact, some aspects of democratic procedures at the United Nations are hardly democratic at all. Certain countries, most notably the United States, have a much larger say in this international organization than any other country.

This is due partly to the United States' role as a permanent member on the _United Nations Security Council_, a special group of nations responsible for keeping conflict at bay in the world.

There are five permanent council members — France, Russia, China, the United Kingdom and the United States — and this permanent membership allows these countries to sometimes ignore international law.

Such freedom can easily lead to corruption, as was seen in the council's _Oil-for-Food Program_. The program was designed to allow Iraq to sell the country's oil on the world market in exchange for food, medicine and other humanitarian necessities — but that's not all it did.

An investigation in 2005 found that former Iraqi leader Saddam Hussein had received an astounding $1.8 billion in kickbacks under the program. Many US corporations were involved in these kickbacks, and there is no doubt that the US government knew what was happening.

Despite this, the United States' powerful position at the United Nations meant that it could use its influence to avoid sanctions.

The United States enjoys special treatment even when it comes to how the United Nations defines words — like "torture."

According to the US Justice Department's Office of Legal Counsel, "torture" describes only acts that are analogous with the physical pain resulting from organ failure or even death. Anything less intense, therefore, is not considered torture.

Contrast this with the definition codified by the Geneva Convention, which states: "Torture means any act by which severe pain or suffering, whether physical or mental, is intentionally inflicted on a person" in order to extract information or confession, or intimidate others.

The discrepancy here is glaring — and frightening.

> _"Treaties are not 'legal' obligations for the United States, but at most 'political' commitments."_

### 3. The United States follows its own rules when it comes to punishing its enemies, real or perceived. 

The United States' special status extends far beyond definitions of torture or illegal economic transactions; it even gets special treatment when it comes to waging war.

The charter of the United Nations states that force may only be deployed after being authorized by the UN Security Council. However, the charter also permits "the right of individual or collective self-defense if an armed attack occurs against a Member of the United Nations, until the Security Council has taken measures necessary to maintain international peace and security."

In other words, you can respond if you or your ally is attacked, without waiting for UN approval.

Yet the United States often ignores both of these stipulations.

For example, former US President George W. Bush justified his "war on terror" by arguing that it was an act of _anticipatory self-defense_. According to his administration, terrorism posed such a grave threat to the United States that it _had_ to act to preemptively fight terrorism by sending troops into Afghanistan.

The United States, in essence, may attack any nation or target that it _believes_ might attack it first.

By this logic, other states should likewise have the right to anticipatory self-defense, even against the United States!

Think back, for example, between 1960 and 1961, when the US Central Intelligence Agency (CIA) smuggled tons of explosives and weapons into Cuba to plant bombs, carry out dynamite attacks as well as raze factories and plantations.

Such tactics undoubtedly qualify as terrorism, and would have justified an armed attack against the United States!

Or consider one British journalist's investigation conducted shortly after the 9/11 attacks, which revealed that Osama bin Laden and the Taliban had received threats of a possible American military strike against them, even before 9/11.

Following the logic of anticipatory self-defense, these threats would have legitimized the group's attack on the United States.

### 4. Economic interests are more important to the United States than fighting climate change. 

Do you lay awake at night thinking about the end of the world?

No? Perhaps you should.

There are two threats that threaten our existence: a nuclear attack and climate change.

Hand-wringing over a nuclear attack might seem like a passé relic of the Cold War, but the threat is more severe now than ever. Nuclear weapons can be found in many states' arsenals around the world, and there is a real danger that they could be used.

In fact, according to former US Senator Sam Nunn, the chances of an accidental or unauthorized nuclear attack carried out by trigger-happy rogue commanders is increasing.

What's more, we're now at greater risk of nuclear bombs landing in the hands of terrorist groups. For example, terrorist organizations could create a bomb using nuclear waste material. In Russia, where nuclear waste is transported via railroad, it is thus more easily accessible.

The United States does very little in terms of policy to alleviate the global threat of nuclear weapons, least of all the one thing that would put the issue to rest altogether: scrapping all nuclear weapons.

Then, there's the very real threat of climate change.

At the 2005 G8 summit in Scotland, scientists warned of the danger posed by climate change and urged policymakers to take action in fighting greenhouse gas emissions. Of all the G8 countries, only one refused to take immediate, cost-effective steps to reduce emissions: the United States.

President Bush insisted that it would be more prudent _not_ to act, as he considered the evidence on climate change to be limited.

So _why_ does the United States consistently ignore such obvious threats? In essence, acting against these threats would weaken its economic interests.

The economic power of the United States must be protected at all costs. And to maintain this power, the country is even prepared to risk war.

> _"The risk of nuclear destruction highlighted by Russell and Einstein is not abstract."_

### 5. The United States engaged in a deliberate campaign to antagonize Cuba for not bending to its will. 

In 1959, Fidel Castro came to power in Cuba, where he established a communist government.

Previously, the United States and Cuba maintained good relations, but the creation of a communist state so close to US borders (a mere 90 miles away by sea) disturbed US politicians so greatly that they sought to overthrow Castro's government.

In March 1960, US Undersecretary of State Douglas Dillon explained that, to overthrow Castro, the Cuban population would serve as legitimate targets. Dillon's reasoning led the US government to enact an embargo against Cuba, which has lasted for decades.

In addition to economic warfare, the United States also employed harsher methods: US agents had plantations and factories burned, and docks and ships likewise destroyed.

But why such relentless aggression? According to Latin American scholar Louis Pérez, the answer was clear: "The US could not tolerate the refusal of the Castro regime to submit to the United States."

The United States feared that Cuba's refusal to bend the knee might encourage other states to become more independent, and thus less reliant upon the United States for financing and security.

The United States over the years has invested heavily into continuing to antagonize Cuba. For example, the Office of Foreign Assets Control (OFAC) dedicates significant resources in investigating "suspicious financial transactions" that might finance terrorism — disproportionately focused on Cuba.

In April 2004, for example, as little as four out of 120 employees were tracking the financial dealings of Osama bin Laden and Saddam Hussein, while almost two dozen employees were investigating transactions to and from Cuba.

From 1990 to 2003, OFAC collected fines from 93 terrorism-related investigations, amounting to $9,000. Contrast this with the 11,000 investigations into Cuban financial records, which led to a total of $8 million in fines.

Despite its stance on the dangers of financing terrorism, it appears that the United States is more interested in limiting a perceived communist threat rather than actually combatting terrorism!

The next blinks will examine the contradictions between the United States' words and actions when it comes to promoting democracy abroad.

> _"When history is crafted in the service of power, evidence and rationality are irrelevant."_

### 6. US economic desires often overshadow its stated policy goals of fostering democracy abroad. 

Do you believe that every individual should have the right to voice his opinion and live according to his own values? The United States certainly claims to believe this, too.

In fact, promoting democracy abroad is allegedly one of the central tenets of US foreign policy.

For example, in analyzing the central foreign policy principles of the Bush administration, a scholarly article from 2005 argues that promoting democracy was an important pillar of the "Bush Doctrine," both to fight terrorism and as a part of the country's overall foreign policy strategy.

Looking further back into history, we see that promoting democracy abroad was a defining goal for former US President Ronald Reagan as well. The National Endowment for Democracy, for instance, was founded under Reagan in 1983 to promote democracy internationally.

Yet in practice, US economic interests clearly take precedence over the promotion of global democracy, which often leads to curious contradictions in public relations.

For example, at the 2005 opening of an oil pipeline designed to carry Caspian oil westward without passing through Iran or Russia, former US Energy Secretary Samuel Bodman delivered a speech in which he credited the efforts of Azerbaijan in building the pipeline. According to Bodman, such efforts constituted an important step toward economic prosperity and a just and democratic society.

These comments came on the heels of a _New York Times_ article, however, that detailed how Azerbaijani police beat pro-democracy demonstrators who were advocating for free elections as part of a larger government crackdown on dissent.

For all the United States' lofty talk about the values of promoting democracy, such rhetoric often appears to be little more than a cover to maintain the country's global economic interests.

### 7. The United States wants peace in the Middle East, but only if it benefits, too. 

The conflict between Israel and Palestine is among the world's most complex conflicts, and many people and nations have struggled to bring an end to the violence that has destroyed so many lives.

The United States has long counted itself among those committed to finding a solution, yet it refuses to do anything which goes against its _own_ immediate interests.

Consider, for example, the United States' campaign to make Palestine "more democratic" after the death of long-time Palestinian leader Yasser Arafat in 2004. Following his death, the United States pushed for free elections in the territories, arguing that such a move would strengthen Palestinian institutions overall.

Now consider this question, posed by _The_ _New York Times_ : Why wait until Arafat's death to promote democratic elections?

The answer lies in the fact that, had Arafat been democratically "elected," both he and his brand of politics (which did not coincide with American interests) would gain credibility and legitimacy.

So, it would seem that elections are a viable option only when the election result is the "right" one!

In much the same vein, the United States uses its considerable weight to undermine solutions with which it does not agree.

Consider a resolution initiated by Syria in 1976, which proposed a two-state solution in which both Palestinians and Israelis would each have their own sovereign territory. This compromise was widely accepted within the international community, and likewise had the backing of the Palestinian authorities.

The United States, however, blocked Syria's resolution, while simultaneously supporting the expansion of Israel, its ally, further into the occupied territories.

The United States still continues to block two-state compromises. In December 2003, for example, after much hard work between prominent but unofficial Israeli and Palestinian negotiators, the Geneva Accord was finally made public.

The accord outlined the logistics of a two-state solution, and although the plan was widely supported by an international consensus, the United States did not explicitly support it. This allowed Israel to more easily ignore international pressure and ultimately reject the accord.

> _"Palestinian concerns are as irrelevant as international law."_

### 8. The US-led invasion of Iraq was legally and morally dubious – not to mention a failure. 

Many people protested against the Iraq war. In fact, the US-led invasion of Iraq inspired some of the largest political protests in contemporary history.

The rationale behind the war was to bring democracy to Iraq via a radical break with the old regime. Yet the peaceful democracy that Iraq was promised did not follow in the wake of a toppled Saddam Hussein. Rather, the war was followed by more violence and terror for Iraqi citizens.

For example, Iraq's 2005 draft constitution was more theocratic than the country's previous document, suggesting that a future Iraqi government would likely place more emphasis on Islamic principles than on democratic practices.

But the US invasion of Iraq wasn't simply a failure; it was also inspired by legally dubious strategies.

In November 2004, for example, American troops launched a second attack on the city of Fallujah. Part of the plan was to drive out all city residents, except adult males, by bombarding the city but blocking its exits, forcing men and boys between 15 and 65 to return to the city.

But according to an Iraqi journalist, this tactic penned in not only men but also families, pregnant women and babies — a reprehensible act that quite possibly may have also been illegal under international conventions.

Moreover, US warplanes bombed one of the city's central health centers, killing 35 patients and 24 staff members. According to the Geneva Conventions, "fixed establishments and mobile medical units of the Medical Service may in no circumstances be attacked," thus defining the health center as an illegal target.

All in all, the offensive resulted in the death of about 1,200 rebels — with as many as 700 civilian casualties.

The Iraq invasion was a failed attempt to establish democracy in the country, and essentially worsened living conditions for Iraqis overall.

The final blink will examine the status of democracy within the United States itself.

> _"The achievements of Bush administration planners in inspiring Islamic radicalism and terror are impressive."_

### 9. The United States has a democracy deficit, and can thus be called a failed state. 

If the United States is so gung ho about promoting democracy across the globe, then surely its version of democracy must be beyond reproach.

This is far from the truth.

Contrary to the foundational principles of a democratic system, US public policy doesn't necessarily reflect public opinion. In fact, there are plenty examples that demonstrate this discrepancy.

Take the Kyoto Protocol, for example, an international agreement shepherded by the United Nations that commits signatories to reducing greenhouse gas emissions. Despite broad support among the general US population, the Bush administration nevertheless disengaged from the protocol in 2001, and it eventually expired in 2012.

Moreover, a large majority of the US population believes that the United Nations should take a leading role in international crises, and that economic and diplomatic measures are better than military engagement when it comes to the "war of terror."

There is even a slight majority that wants to abolish the veto power and dominance enjoyed by the five permanent members of the UN Security Council!

Yet the United States has taken a total of zero steps to make any of these ideas a reality. How can we consider the United States to be a functioning democracy if the actions of the government don't match the will of the people?

In fact, you could even consider the United States to be a _failed state_.

The original definition of a "failed state" was one in which some of the basic responsibilities of a sovereign government, such as protecting citizens or providing public services, were abandoned.

Under the Bush administration, however, this definition was expanded to include all aggressive, arbitrary or totalitarian states — those countries with a _democracy deficit_, lacking the institutions that work to fulfill the principles of democracy; countries in which citizens can't elect a leader to represent the desires of the people.

But the United States _itself_ has a democracy deficit! By President Bush's own definition, the United States can be considered a failed state.

### 10. Final summary 

The key message in this book:

**The United States extols democratic principles and claims to promote the spread of democracy around the world. A closer look at US policy at home and abroad, however, reveals that the country is concerned much less with democracy and more with securing its own economic and geopolitical interests.**

**Suggested further reading: Rogue States by Noam Chomsky**

In _Rogue States,_ Noam Chomsky holds a critical lens to the nature of state capitalism and to American Foreign Policy, providing an alternative view to the one proposed by government rhetoric and mainstream media.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Noam Chomsky

A prominent cultural figure and political thinker, Noam Chomsky is a world-renowned American linguist who is also Professor Emeritus at the Massachusetts Institute of Technology. He has authored over 100 books, and was voted the "world's top public intellectual" in a 2005 poll.

