---
id: 5850567c3e967900044cb0c6
slug: fair-pay-fair-play-en
published_date: 2016-12-16T00:00:00.000+00:00
author: Robin A. Ferracone
title: Fair Pay Fair Play
subtitle: Aligning Executive Performance and Pay
main_color: ED2F38
text_color: BF262D
---

# Fair Pay Fair Play

_Aligning Executive Performance and Pay_

**Robin A. Ferracone**

_Fair Pay Fair Play_ (2010) sets out the basic principles of fair executive compensation. These blinks explain what goes into making compensation reasonable, why executives are often paid disproportionately higher salaries than other employees and which concrete strategies you can employ to set a fair pay scale for your company.

---
### 1. What’s in it for me? Discover how to find an equilibrium for executive pay. 

Are you familiar with the notion of _aligned pay_? If not, here's a chance to get acquainted with an important element in business compensation.

Aligned pay has to do with the amount of compensation a company pays its executives. And crucially, that amount, whether stocks, bonuses or otherwise, needs to be fair.

But what is fair? These blinks will help you better understand what you should do when setting executive compensation and show you how you can bring your company's pay scales back to earth.

In these blinks, you'll learn

  * when a generous compensation package does more harm than good;

  * which factors you should consider when setting executive compensation; and

  * why, for successful executives, cash is just one element of a great position.

### 2. Executive compensation plans should take into account CEO performance and industry standards. 

Imagine you're an account manager on a team of three employees. Even though you all work equally diligently, you find out that your two colleagues are paid a higher salary than you.

Such a situation would be unfair, right?

Unfair pretty much explains the state of executive compensation today. Executives are routinely overcompensated, and compensation plans seldom take into account CEO performance.

Consider that John Chambers, the CEO of Cisco Systems — one of the world's biggest telecom companies — each year "earned," in addition to his $300,000 salary, from $5 million to $6 million worth of stock options and a $400,000 bonus.

That's way too much money — no executive can perform _that_ well! But performance isn't all a compensation plan should consider to be fair. It also should reflect what other CEOs earn in the same market.

Industry standards vary, and each industry is subject to different external forces. For instance, the energy sector is sensitive to oil prices, while the tech industry reacts to inventory levels in the IT supply chain.

Such external factors are important, as they can affect the overall performance of any executive. That's why it's important for CEO pay to reflect the compensation of peers in the same industry.

This way, if an oil crisis drives prices through the roof, the CEO of an energy firm won't be blamed for the company's poor returns. Even if the CEO's performance was above reproach, it wouldn't make sense to base the executive's compensation on that of technology CEOs working in a booming market.

### 3. To compensate fairly, stick with pre-arranged agreements and focus on overall business strategy. 

We all make decisions on a whim. As a result, we set goals that we can't keep, and thus abandon those goals without thought. This sort of behavior works to distort executive compensation schemes.

Decisions made randomly and outside original plans undermine fair pay strategies. Here's why.

Executive compensation should be based on a plan that dictates how much a person's pay will change based on certain future events, such as a company merger. Such plans are obviously set well in advance; yet even so, events or arbitrary decisions can easily unhinge a well-made plan.

For instance, let's say a sitting CEO decides to retire after 20 years. While her contract already had a specific plan set out in case of retirement, the company board, on a whim, decides to grant the CEO extra stock options. The result? A far too generous compensation plan!

In a similar way, adjusting compensation based on economic events without considering a company's long-term business strategy will also lead to compensation that is unfair.

This is why, instead of reacting to externalities and shifting plans, companies should always stick with the established plan and overall corporate strategy.

Here's a telling example. One company decided to change its executive pay scheme from a generous package including a base salary, bonuses and stock to just payment in stock — a choice that resulted in a dramatically lower total compensation.

Why the change? The company made the decision in 2008, the year of the global financial crisis. Executives were responding to external events and ignoring overall business strategy.

So now you know if you don't stick to a plan, your compensation scheme will suffer.

### 4. Executive compensation plans often protect CEOs from mistakes or short-term risks. 

Have you ever heard the term _illusory superiority_? This is a psychological phenomenon in which an individual believes that his successes are a result of his hard work but his failures the fault of external forces.

The same tendency can apply to companies. Corporations often believe that management is cleverer, more capable and generally "better" than employees at lower levels. This belief translates into executives earning far more than they really should.

As a result, while a company with mediocre management may perform well just because the overall economy is robust, executives will still "earn" a bonus as a reward. Yet that same company will attribute any difficulties to other factors, such as an overall economic slowdown — not to company leadership!

Another assumption that stilts executive pay is applying a one-size-fits-all mentality to compensation methods.

This was the case in the 1990s and 2000s when many firms across the United States believed that executives of publicly traded companies should be paid similarly to managers at top private equity firms.

This was a huge mistake; public and private firms are not the same beast. For instance, the evaluation of a public company is determined on a daily basis in the open market. Executives of public firms need to satisfy the demands of shareholders over an extended period, independently of when they purchased stock.

Privately held companies, on the other hand, give executives a fraction of company equity when they come on board, which they can sell at a later date.

Failing to account for this difference means that executives in publicly traded companies were overcompensated. They were compensated on a compensation model designed for short time periods (the private firms) when they should've been compensated based on a longer-term strategy that public firms require.

### 5. Companies often overcompensate executives to keep them on board, yet cash is a poor motivator. 

We often think that there's no problem money can't solve, especially when it comes to compensation. If you pay someone a sufficient amount of cash, that person will strive to perform well, period.

As a result, executive committees tend to overpay executives just to keep them in the top office, even if it means creating economic chaos within the company.

During the 2008 financial crisis, for instance, some companies, fearing executive flight, refrained from cutting high executive salaries even as company revenue plummeted.

Worse was that some companies promised executives generous stock option packages to stay the course, even though doing so would cause the company severe economic stress.

Such decisions are unreasonable, as they inevitably lead to a misallocation of resources. Money used for executive pay could be channeled toward more productive projects, such as research and development.

In fact, money is rarely as essential to retaining top talent as companies think.

While many companies believe compensation is the greatest motivating factor, it's just one of many elements that make an executive feel satisfied with her position. Other factors include how challenging the job is, how much the position allows her to grow professionally and whether and her reputation will improve with the new job.

In fact, the majority of people hired for top positions aren't interested in just money. Rather, such executives come on board because they believe in the work the company does, the vision it has and the people who work there. They want to help cement the company's place in history.

As a result, few executives change jobs for a bump in pay. When they do leave, it's usually to take the next career step.

### 6. A couple of tools can help you calibrate executive compensation to make sure it’s fair. 

We now know that executive pay is plagued by fairness problems. But what can be done?

Just like a street map tells you whether to turn right or left, an _alignment report_ can help guide your decisions regarding executive compensation. This report helps you determine whether a particular level of compensation is "aligned" with an executive's overall value.

An alignment report shows how much value an executive creates for a company and determines how much the executive should be paid relative to competitors in the market.

A report might compare Company X to Company Y, for example, two companies involved in the auto industry.

In 2015, Company X performed well under talented leadership, increasing revenue by 23 percent; its CEO was paid $180,000.

That same year, Company Y, of comparable size and market share to Company X, didn't do as well. Revenue grew by 8 percent; its CEO was paid $175,000.

By comparing performance and industry, it's plain to see that Company Y's CEO was overcompensated.

An alignment report can also show you if a given _compensation design_ is fair. Compensation design is the way a company decides how its executives are paid, whether through a system of bonuses, stock grants or a combination thereof.

To remain fair, such a design should compensate an executive according to performance relative to other executives in the same field.

Thus if most CEOs at competing companies are paid a fixed monthly salary, your CEO is overpaid if he's receiving a fixed monthly bonus on top of his salary — that is, as long as he's generating the same amount of value as other CEOs in the same industry.

Simply put, to determine whether executive compensation is fair, consider the value created by your executives and compare it to that created by those at competing firms.

### 7. Final summary 

The key message in this book:

**Executive pay is out of hand, and something needs to be done. For the compensation of top management to be fair, it should be based on performance and be relative to that of executives in similar positions and at similar companies, catering to similar markets.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _What Your CEO Needs to Know About Sales Compensation_** **by Mark Donnolo**

These blinks explain how compensation can be optimized to motivate sales representatives and ensure the growth of the company. It offers advice on finding the right people, crafting a strong sales compensation plan that drives your team to hit their targets, and implementing that plan effectively.
---

### Robin A. Ferracone

Robin A. Ferracone has over 30 years of experience as an executive compensation consultant.

©Robin A. Ferracone: Fair Pay Fair Play copyright 2010, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

