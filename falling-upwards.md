---
id: 5a8c19aab238e10007431795
slug: falling-upwards-en
published_date: 2018-02-23T00:00:00.000+00:00
author: Richard Holmes
title: Falling Upwards
subtitle: How We Took to The Air
main_color: 27AAC5
text_color: 186778
---

# Falling Upwards

_How We Took to The Air_

**Richard Holmes**

_Falling_ _Upwards_ (2014) details the surprisingly rich history of hot-air balloons. It begins with the first successful human attempts to take to the air using balloons and goes on to chronicle their clandestine role in escape attempts and military ventures. From daring balloonists from the golden age of ballooning to the literature they inspired, it's all covered here.

---
### 1. What’s in it for me? Take a breath of that high, clear air and be inspired. 

These days, it's not unusual for people to board an airplane and take to the sky. The food may be bland and the legroom limited, but flying from one place to another is otherwise pretty uncomplicated.

However, humanity's first experiments with flight were more than complicated; they were life-threatening. Balloons and balloonist were on the cutting edge. It was the early days of modern scientific discovery, and the voyages of these captains of the skies often resulted in death.

The history of ballooning is an inspiring one. Sure, these days you're more likely to associate hot-air balloons with the Pixar movie _Up_ or with slightly eccentric hobbyists than with real adventure or feats of daring. But in their heyday, balloons weren't just inspirational in the abstract; they really impacted the world around them.

It's even more incredible when you consider that the basics of balloon technology not only gave rise to new forms of literature, but enabled people to escape totalitarian regimes in the twentieth century. These blinks will help you find inspiration in a technology that has long been dismissed as irrelevant.

You'll also learn

  * why you should name your balloon after an English queen;

  * which earthbound mode of transport killed off ballooning; and

  * what kind of hat to wear if you want to look like a nineteenth-century balloonist.

### 2. Ballooning is not for the faint of heart. 

It there's one characteristic still associated with ballooning, it's eccentricity. After all, it's hardly a common pastime. But piloting a hot-air balloon is by no means a whimsical whooshing amid the clouds. Getting airborne and flying off into the sky is risky, even to this day.

In 2008, a Brazilian priest and experienced balloonist, Father Adelir Antonio de Carli, had an idea. He wanted to raise money for the poor. His means were unconventional: ballooning for charity. He strapped himself into a chair lashed to hundreds of colored helium balloons and started his ascent.

At first, all went well. Father Adelir climbed to 19,000 feet. But then his GPS navigator failed and he lost radio contact. The wind drove him out to sea. A rescue party was dispatched, but to no avail. In July, the remains of Father Adelir's body were found floating about 100 kilometers off the Brazilian coast. In all likelihood, some of the helium balloons had burst at high altitude. Father Adelir would have descended gently to the ocean, where sharks swam waiting for him.

And, of course, what's risky now was dangerous in the past, too. In fact, even Father Adelir's charity balloon flight wasn't the first of its kind. In 1875, Major John Money took to the skies in his balloon in England for the local Norwich and Norfolk hospital.

The major's experience was similar to the priest's. The launch was pitch-perfect. Once in the air, however, the balloon was caught by a gust and pulled out to sea.

Thankfully, however, Major Money survived. When the rapidly deflating balloon sank to sea level, he cut off the weighty basket and hoisted himself into the balloon hoop. Like a modern day kite surfer, he was dragged through the water. Hours later, a rescue boat found him. Needless to say, the donations to the hospital were substantial.

> _"Someone asked me what is the use of a balloon? I replied, what's the use of a newborn baby?"_ — Benjamin Franklin

### 3. Balloons can be used to make daring escapes, though sometimes multiple attempts are needed. 

The sheer simplicity of a hot-air balloon's design means that even the most enthusiastic amateur can build one. That's especially true if your motivation for flight is driven by a need to escape.

In 1978, two families living in the East German countryside attempted to escape to the West using a balloon. Peter Strelzyk and Günter Wetzel, both handymen, decided to attempt to construct their very own balloon in a hidden attic.

After a few unsuccessful tries, a frightened Wetzel resigned himself to failure. Strelzyk, however, kept going, and on July 4th, 1979, he and his family attempted to fly over the border in their small balloon.

But the balloon wasn't up to the task. As the rain poured down, it became increasingly sodden and they began to descend. They came to earth in a no-man's-land in front of the frontier fence.

Thankfully, the rain also diminished visibility and they weren't spotted. They made it home, more determined than ever.

Successful balloon launches often take multiple attempts and this was no exception. Wetzel and Strelzyk joined forces again, using old clothes to make a much larger balloon. Once inflated, the balloon stood at nearly 90 feet tall. It needed four gas tanks to power it.

In the early morning on September 16th, 1979, the families took off from their secret location in the forest. One huge blast took them up to 6,500 feet. But surrounded by darkness, they lost all sense of orientation. When they spotted searchlights below, they risked firing the burner to avoid being caught, even though the flame was sure to make them more visible.

At that point, the top of the balloon burst. They sank, rapidly, crashing to earth once more. They were lost. Had they made it? An electric pylon marked with the name of a West German company provided the answer: they were safely in the West.

### 4. Shortly after they were invented, hot-air balloons enjoyed a brief period of military glory. 

In 1783, the first successful hot-air balloon flew over Paris. It was manned by Jean-Francois Pilatre de Rozier and Francois Laurent d'Arlandes. And soon enough, the French realized that their balloons might also be useful as strategic military weapons.

In June of 1794, at the Battle of Fleurus, the French faced down a coalition of armies drawn from across Europe. A French balloon, piloted by Captain Charles Coutelle, ascended above the battlefield.

The balloon's vantage point gave the French army vital information. They could trace enemy movements and positions. The French employed this tactic in several subsequent battles, and thanks to the strategic use of balloons they emerged victorious.

The scheme was not without its problems, though. If the wind was strong enough, it proved impossible to keep balloons securely tethered to the ground. Furthermore, getting information from the balloon to the ground, or vice versa, was slow. First, the balloon had to ascend, and then it had to descend so the balloonist could communicate his observations to his commanders.

On top of that, it was a perilous situation, since the balloon attracted fire from enemy troops down on the ground. And with good reason. hot-air balloons not only provided intelligence; they also acted as effective psychological weapons.

Soaring on high, they gave the impression that they were watching every enemy soldier's move. Soldiers found this demoralizing. Consequently, as soon as a balloon was launched, it became the target of anything that fired projectiles, from pistols to cannons.

In many ways, this danger simply added to the glamour of being a military balloonist.

Nonetheless, no perfect solution for effective use of balloons on the battlefield was found. They remained an imprecise instrument. By the end of the nineteenth century, their day was over, and in the twentieth, airplanes proved much better suited to the task.

### 5. Sophie Blanchard delighted crowds in the early nineteenth century with her ballooning antics. 

In their heyday, the popularity of balloons wasn't limited to military affairs. In nineteenth century France, air balloons were used in aerial shows. Acrobats, for instance, performed daring parachute jumps, throwing themselves from balloon baskets.

Of all these performers, the bravest and most celebrated of all was a woman named Sophie Blanchard. She was born in the French port of La Rochelle, in 1778, and later encountered the famous balloonist Jean-Pierre Blanchard, whom she went on to marry.

Blanchard, usually so timid and frail when earthbound, changed the moment her future husband took her up in his balloon. She was overwhelmed with excitement. Once aloft, she became a confident commander, and a charismatic entertainer with a streak of recklessness to boot.

News of her abilities soon caught the ear of the Emperor Napoleon, who thereafter engaged her services. On the birth of Napoleon's son, in March of 1811, Blanchard was commissioned to fly over Paris and throw out leaflets announcing the occasion. She took to the skies once more for the boy's baptism, this time dazzling the masses by shooting off fireworks from her basket.

Blanchard actually developed a style of ballooning all her own.

Rather than a large basket, Blanchard used a miniscule, beautifully crafted silver gondola. The silk balloon was small, and the edge of the gondola barely reached her knees. All this meant that, when flying, Blanchard was exposed to the terrifying abyss below.

To round off the image of vulnerability, Blanchard dressed herself in white, and perched elaborate feathered bonnets atop her head.

Then, in 1819, it all came to a sudden — and perhaps unsurprising — end. Blanchard's balloon caught fire and she came down like Icarus upon the Parisian cobblestones.

### 6. The growth of the railways relegated ballooning to a purely recreational activity. 

By 1830, balloons were no longer a novelty. And earlier hopes of global balloon navigation had been dashed.

At a stroke, the growth of railways put an end to that dream.

In 1825, the first 26 miles of track were laid between the English towns of Stockton and Darlington, in England and by the 1830s, railway travel proper had arrived in the shape of the passenger line between Manchester and Liverpool.

Consequently, it was the steam engine of Victorian England that became synonymous with travel. Its robustness, revolutionary speed and dependability all meant that a reliable timetable could be developed.

The train stood in stalwart contrast to the yielding and unpredictable balloon. Railways became the instruments of business and urban life. By comparison, ballooning exuded an almost bucolic romance.

So it was that the second half of the nineteenth century came to represent the epoch of recreational ballooning.

Increasingly larger, more comfortable and more reliable air balloons were developed. These carried groups of passengers over the countryside — at a price, of course. However, though these enterprises were commercial at heart, they nonetheless had a romantic and nostalgic air about them. Interestingly, it was fashionable to name these balloons after Queen Victoria, who was crowned in 1837.

Previously, balloons had been fueled by hydrogen, which is volatile and unreliable. Ironically, commercial ballooning was only able to succeed because of the cheap and reliable supply of coal that supported the railway industry.

The fashion for recreational ballooning was soon adopted in several large European cities. For the first time in history, people could enjoy a bird's-eye view of their hometowns.

But it was a depressing as well as a beautiful experience. You could see the sprawl of factories and slums surrounding churches and parks, and the vein-like railway lines radiating from booming metropolises. Ballooning provided ample proof that the divide between rich and poor had never been wider.

> _"No man can have a just estimation of the insignificance of his species, unless he has been up in an air-balloon."_ \- Benjamin Robert Haydon, painter

### 7. The romance of ballooning helped create the first science fiction of the modern era. 

As the nineteenth century progressed, ballooning continued to capture the popular imagination.

Charles Green's crossing of the English Channel, for instance, was an inspiration to many. Such feats of prowess even galvanized the creation of a new form of literature: science fiction.

Let's consider a famous example. Edgar Allan Poe's _The Unparalleled Adventures of One Hans Pfaall_ was published in _The Southern Literary Magazine_ in 1835.

It tells the story of a man who supposedly flew to the moon in a gigantic air balloon. The specificity and realism is incredible, and the narrative is laden with technical details.

The construction of the balloon is reported in minute detail. Poe describes the instruments used to measure temperature and pressure. The craft has trumpets and bells to facilitate communication with vehicles that might appear and fly alongside during the journey. Poe even mentions that a cat and a pigeon are brought along.

As the balloon ascends, an explosion throws Pfaall out of the basket. He is left dangling from a rope. Pfaall, though, manages to climb back in. Once the balloon is beyond the reach of Earth's gravity, Pfaall's breathing is assisted by a machine that condenses air.

We're told that Pfaall lands on the moon, where he encounters its inhabitants, frightening small creatures with lopsided grins, with whom he can barely communicate.

There, from his vantage point amid the stars, Pfaall can see the Earth rise. There is a poetic beauty to the image. Pfaall ends up staying on the moon for five years. In an ironic twist, he's unable to persuade the moon creatures that the Earth is inhabited. Once he returns back home, of course, the humans are equally dismissive of his stories.

The tale did have some impact, however. It's quite possibly the first story to contain all the qualities of the modern science fiction genre.

### 8. In 1871, Paris was encircled by Prussian forces, and balloons gave hope to the besieged French. 

We've already seen how balloons made escape from East Germany possible, but that wasn't the first time such a daring escape had been made.

Over a century earlier, the French army in Paris found itself in a similar dilemma.

In 1870, Emperor Napoleon III of France declared war on Prussia. However, the French army was quickly crushed. As a result, Napoleon fled to England, and the Third Republic was declared. Given Prussia's clear position of superiority, General Bismarck offered the French leaders terms for an armistice. Incredibly, though, they refused.

Consequently, Bismarck gave the order for the Prussian army to invade France. The Prussians stormed through the French countryside, and the local population retreated to Paris. Within two weeks, the Prussians had surrounded the city. Paris was under siege.

French morale could not have been lower; Paris was cut off and Prussian disinformation about the situation was spreading fast.

However, at this low point, hopes were lifted by the possibility of balloon assistance.

In September 1871, the Parisians remembered they had a stock of air balloons in the city. They dispatched them over the surrounding Prussian army to communicate with the outside world.

The first balloon sent out was manned by Jules Duruof. He managed to avoid Prussian fire by cutting away his balloon's ballast as soon as possible. This sent him hurtling up into the air.

Duruof was successful. He even managed to evade the pursuing Prussian cavalry by flying over rivers they could not easily ford.

Even though the French ultimately lost the war, the boost to morale provided by these symbolic air balloon flights, as well as the effect of reestablishing communication with the rest of the country, allowed the French resistance to continue for far longer than anyone had thought possible.

### 9. In the face of rapid technological advance, ballooning’s impending decline was clear. 

By the end of the nineteenth century, it was plain that the impact of the industrial revolution was monumental. Technology had advanced rapidly, and it was obvious that powered flight would be the next great step for air travel.

For generations prior, people had tried to take to the skies by imitating birds. They'd affixed wings to their arms and jumped to their deaths. But it was only after the structure of birds' wings was understood that flight became a real possibility.

Birds' wings are naturally concave in shape. This means that the top surface area of each wing is larger than that of the underside. This ensures that air passes faster over the top surface of the wing, and slower under it.

This results in a natural buoyancy. Even heavy machinery such as planes can benefit from this natural lift.

The other great advantage to this form of flight is that it's less dependent on the vacillations of the wind. By subtly changing the curvature of their wings, birds can alter their flight, as can planes.

There was no way ballooning could compete. By the early twentieth century, ballooning had become merely a hobby for the well off. It was an old-fashioned sport, reserved for eccentrics and aristocrats.

Balloon races and champagne parties in the sky became quite the thing in aristocratic circles. Think of it as the equivalent of yachting or golfing today.

By the end of the twentieth century, ballooning had become merely the preserve of hobbyist. It's still with us, however — a reminder of our early aspirations to flight and the progress we've made since.

### 10. Final summary 

The key message in this book:

**Hot-air balloons shouldn't be thought of as just something that tourists use for fancy sightseeing. They have, in fact, fired the human imagination for centuries. They have both saved lives and contributed to victories in bloody battles and given us new forms of literature and helped citizens escape from peril. They are, in short, a testament to human ingenuity and a reminder of what it took for us to take to the air.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Box_** **by Marc Levinson**

_The Box_ (2006) tells the tale of modern transportation's poster child, the container, and how it revolutionized the shipping industry and enabled globalization. These blinks will take you on a detailed journey through this seemingly simple but revolutionary change in global systems of trade.
---

### Richard Holmes

Richard Holmes is a prize-winning author, best known for his nonfiction book _The Age of Wonder_, which details scientific development at the end of the eighteenth century. He has also written numerous celebrated biographies, including a life of the poet Percy Bysshe Shelley.

