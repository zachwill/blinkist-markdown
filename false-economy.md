---
id: 57da983de4ac3900031ce2e4
slug: false-economy-en
published_date: 2016-09-16T00:00:00.000+00:00
author: Alan Beattie
title: False Economy
subtitle: A Surprising Economic History of the World
main_color: F44131
text_color: BF3326
---

# False Economy

_A Surprising Economic History of the World_

**Alan Beattie**

_False Economy_ (2009) offers a fresh perspective on how and why some nations of the world have become economic powerhouses and others have ended up as financial disasters. You'll see that nations aren't handcuffed by fate. Rather, their economic success or failure is based on the choices they make.

---
### 1. What’s in it for me? Understand what makes the world’s greatest economies tick. 

Did you ever wonder why some countries prosper while others fail? Is this all due to climate, natural resources, and the different mentalities of the people in those nations? Or can it all be chalked up to choices made by their respective governments? Why is it that the United States and Argentina followed extremely different trajectories even though they started out with similar economic conditions?

These blinks will explore all these questions as you learn about the economic histories of several globally important nations.

In these blinks, you'll also learn:

  * about parasitic cities;

  * why there's so much asparagus in Peru; and

  * how oil can make a society extremely volatile.

### 2. Important choices, not fate, determined the economic state of Argentina and the United States. 

Have you ever wondered why a country like the United States is a giant in the world economy, while a country like Argentina has continually struggled? As we'll see, this was not something that happened by chance.

As a matter of fact, Argentina and the United States started out in very similar situations.

The United States officially became an independent nation when its constitution came into effect in 1789. Argentina was particularly inspired by the American revolution, and in 1816 Argentinian rebels took control of their government to establish their own independence. And as these two countries started out, both nations had similar economic outlooks based on rich agricultural promise and plenty of fertile land.

But the two paths diverged when the countries made different choices on how to develop their land.

America chose to divide their land up between skilled individuals and families. Capable farmers from Europe arrived to make the most of the opportunity, and they helped expand and settle America's western frontier.

Conversely, Argentina decided to divide its land among a select few rich and powerful landlords. As a result, its agricultural potential was stunted because the policy failed to attract skilled laborers and farmers to develop the land.

The two nation's paths continued to diverge following twentieth-century industrialization.

While the United States embraced the manufacturing industries and the foreign trade possibilities they presented, Argentina shut itself off in an effort to become self-sufficient. Argentina rejected the interests of foreign investors and the risky business of globalization, preferring instead to protect its government monopolies.

So, as the US economy flourished in the twentieth century, Argentina continued to seal off its manufacturing businesses by keeping out imports and heavily taxing exports. In 2001, in its efforts to be self-reliant, Argentina was forced into one of the largest government bankruptcies in history.

> _"It was history and choice, not fate, which determined which became which."_

### 3. Cities can play a major role in determining a nation’s economic success. 

You may have noticed that major cities can serve as a good representation of a nation's economic success or failure. That's because the tricky business of development and urbanization can play a big role in a country's economic prosperity.

For instance, nations with unstable governments will often have a bloated capital cities that reflect their problems.

During the time of the Roman Empire, Rome was a city that represented both abundant growth and the instability of the government. As well as being the center of commerce and industry, it was also filled with citizens demanding food, jobs and welfare. To support the outsized city, the surrounding areas were heavily taxed and made unsafe by constant warfare.

Today, overpopulated cities like Buenos Aires and Mexico City are similar in the way they reflect the lack of opportunity elsewhere in their countries.

In fact, 35 percent of the entire nation of Argentina resides in Buenos Aires. This statistic shows how the country's failed agricultural programs have provided citizens with few options for prosperity other than by finding work in the city.

The same situation is visible in many of Africa's cities, as well.

When a country rushes towards urbanization, it can often happen at the expense of the rest of the nation. The development of Zambia's capital city of Lusaka was paid for by taxing the countryside. As a result, farmers were driven out of business, and forced to move to the city and live in slums known as shantytowns.

A successful city, by contrast, is one that contributes to a diversified national economy in more than one way.

Cities like Madrid and Chicago have thrived and remained healthy because their success is not dependent on one kind of business or technology. This is in stark contrast to a city like Detroit which relied too heavily on the automotive industry and ended up in economic despair when the industry dried up.

> _"A successful city is a hard thing to build, and a world-class one even harder."_

### 4. Farming choices and shipping routes can dramatically influence a nation’s trade and economy. 

Have you ever been grocery shopping in the United States and wondered why the asparagus comes all the way from Peru? It actually goes back to the 1980s when the two countries made a trade agreement. As a result, the United States gets tens of millions of dollars worth of asparagus every year and Peruvian farmers have an incentive that keeps them from making cocaine.

Decisions like these can play a big part in determining a country's economic stability.

Take Egypt, for example. Centuries ago, the country grew its own wheat. But doing so today would take up a sixth of Egypt's water supply, making it an economically unwise decision. Instead, the country, with a population of around 80 million, is the world's second biggest importer of wheat.

On the other hand, crops like herbs and vegetables take far less water to grow. So Middle Eastern countries like Egypt find stability by growing these for export while importing water-depleting foods like grains and meats.

For some countries, instability can prevent them from having a lucrative export business.

Many African nations could be reaping the benefits of the booming coffee industry. But civil wars and dangerous military regimes have closed off shipping routes, making it increasingly difficult to get goods out of the countries.

Just to get across the border from Uganda to the shipping ports of Kenya can take 24 hours. Therefore, the price of keeping things refrigerated and getting safely through dangerous border crossings can be too costly for many businesses to earn a profit.

> _"Creating the conditions for supply chains to lengthen and trade routes to be established is neither easy nor routine. But it can be done."_

### 5. Natural resources can end up being more trouble than they’re worth. 

You probably wouldn't think that finding oil or a diamond mine in your backyard would be a bad thing. But for many countries, this kind of discovery has brought more economic chaos than prosperity.

Oil can bring economic stability, but only if you know how to manage it properly.

The oil-rich nation of Saudi Arabia might appear successful with a per capita income of $15,000. But it hasn't been using that money to generate jobs. A closer look reveals an unemployment rate of 25 percent, a figure that is affecting many of the country's young men.

And since half of Saudi Arabia's male population is under the age of 22, the country is dealing with a large group of frustrated people. So it isn't surprising that the nation's society has become uneasy and volatile.

Let's compare to Norway: the country has managed its oil resources well, putting any excess revenue into a national oil stabilization fund. The nation of Chile also uses a similar system for money earned from its copper resources. Stabilization funds like these allow for responsible spending and ensure that one natural resource isn't relied upon as the only source of revenue.

African nations also reveal how a natural resource such as diamonds or copper can result in prosperity or unending turmoil.

The African nation of Sierra Leone is one of the world's top diamond-producing nations. But the abundance of their natural resource led to illicit trading that helped prolong a civil war for eleven years.

In an effort to get rich quick, the African nation of Zambia pushed out foreign investors interested in their copper mines. But their inexperience and mismanagement of the resource has led to a highly unstable government.

The diamond-rich African nation of Botswana made a better decision by partnering with the foreign company De Beers. They established a long-term revenue sharing agreement, with De Beers handling the difficult task of managing the diamond mine. This allowed Botswana to create a national fund and achieve economic stability through a stable source of revenue.

### 6. Economy doesn’t care about religion. 

How big a role does religion play in determining a nation's economic success? It's a question that has been written about since 1905, when German sociologist Max Weber suggested that Protestants were more successful in business than Catholics.

But history has gone on to prove Weber wrong.

Weber's theories were based on the 19th-century successes of predominantly Protestant countries like England and the Netherlands. But, since then, the advancements of predominantly Catholic nations such as Italy, Spain and Ireland have shown these ideas to be flimsy.

Similar theories about the economic effect of religion on Asian and Middle Eastern countries have also been disproven.

In the 1980s, it was fashionable to look at the cultural traditions of Hong Kong and Taiwan as having an influence on their economic boom at the time. It was thought that their values, which emphasized social solidarity over individualism, were better suited to capitalism. Yet, the Asian financial crisis of the late 1990s proved that these countries were just as vulnerable to collapse as any other.

There have also been theories suggesting that Islamic countries like Afghanistan struggle with economic success more than others.

While it is possible that some Islamic societies are resistant to change and reform, the poor choices that these governments make are largely unrelated to religion. For example, the governments of largely Islamic countries like Malaysia and Indonesia made markedly wise economic decisions.

It is too simplistic to look at religion as being the deciding factor in a country's economic success or misfortune. Ultimately, it comes down to the important choices that government leaders make.

> _"Muslim societies can choose to succeed just as Christian or Jewish societies can without losing their beliefs."_

### 7. Even under corrupt leadership, policy choices are what lead to a successful economy. 

Political corruption can't benefit an economy, can it? Well, just because a nation's leader is honest, it doesn't mean he'll make the right choices that will lead to a prosperous economy.

Take Indonesia, which thrived under a corrupt president, and Tanzania, which crumbled under an honest one.

In 1968, Indonesia had a fractured government and a struggling economy. That year, President Suharto seized control in a terrifying takeover, killing thousands who opposed him while rebuilding the government with his own appointed personnel.

But, as brutal as he was, his conservative political policies proved to be economically wise. He opened up the Indonesia's borders to international trading and shipping, attracted foreign investment, reduced poverty and quickly balanced the budget of the country.

As for Tanzania, from 1964 to 1985, the nation's president was Julius Nyerere: a former teacher who was honest and quite unlike many other corrupt African rulers.

Unfortunately, his poor decisions as president led to economic stagnation. Nyerere tried to create a self-sustaining Tanzania and built policies that closed the country off to trade and investors.

He also socialized the nation's farmers which resulted in widespread extortion and bribery over the distribution of goods and services. This resulted in a nationwide disaster and Nyerere was eventually forced to give up on his ideas of collectivization.

This shows that even political corruption doesn't automatically determine a nation's economic prosperity. It comes down to the choices and policies the governments make.

### 8. By making the right choices, nations can change their economic fate. 

In the animal kingdom, natural evolution can lead to a dead end like the giant panda: a species that can barely survive or reproduce and has no hopes of changing. Fortunately, in world economics, it's never too late to change.

Let's take a look at why, of two countries with similar backgrounds, one managed to change and the other didn't.

Russia is a good example of a country that tried, but ultimately failed, to change.

In the 1990s, after centuries of Tsarism and Communism, Russia attempted to transition into a free-market economy. But the nation quickly learned that it was ill prepared for this abrupt economic shift and ended up defaulting on its economic debt in 1998.

Russia then experienced a backlash against the free market and its unpredictability. As a result, Vladimir Putin restored the nation to a Tsarist-like state: he drove out foreign companies and placed the nation's energy industry back under governmental control.

Though much of the economy hinges on its oil and gas reserves, the centralization of power has restored a sense of stability to the Russian citizens. Though it has come at a price, with the loss of free media and fair elections.

To see how a country can actually make fundamental changes, we can look at China.

China took a different approach to entering the free-market economy. Despite China's history of being suspicious of foreign investors and businesses, the financial success of Hong Kong in the 1980s and 1990s proved that a diversified and open marketplace can lead to success.

So, when Hong Kong reverted back to Chinese control in 1997, the nation was able to follow the economic path of similar governments, including Taiwan and Singapore. Unlike Russia, China didn't seal itself off from opportunities: it recognized the advantages a diversified economy has for creating potential revenue.

As with the other countries we've looked at, it's clear that the economic outcomes of Russia and China were not written in stone. They are, in fact, the result of important choices the nations have made.

### 9. Final summary 

The key message in this book:

**The current world economic landscape is the result of ongoing globalization. Some countries have put policies in place to take advantage of this and found economic success, while others have closed themselves off and found economic hardship. History shows us that it's never too late to change and adapt, and that we should never resign ourselves to fate.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Kicking Away The Ladder_** **by Ha-Joon Chang**

_Kicking Away The Ladder_ takes a historical look at how the Western powers have grown economically and politically. It shows that the principles and tactics that have made them rich and powerful are counter to those they propose developing countries should live by.
---

### Alan Beattie

Alan Beattie has a master's degree in economics from Cambridge University. After working as an economist for the Bank of England, he joined the _Financial Times_ in 1998, and became the newspaper's world trade editor in 2004.

