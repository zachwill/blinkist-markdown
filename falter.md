---
id: 5ced4f436cee0700075a7477
slug: falter-en
published_date: 2019-05-29T00:00:00.000+00:00
author: Bill McKibben
title: Falter
subtitle: Has the Human Game Begun to Play Itself Out?
main_color: FDDF33
text_color: 807119
---

# Falter

_Has the Human Game Begun to Play Itself Out?_

**Bill McKibben**

_Falter_ (2019) provides a rather sobering counterargument to the rash of optimistic books about the present and future that have been appearing on the market lately. Instead of everything being pretty good and set to get even better, author Bill McKibben argues that things are already pretty bad and are on a course to get even worse, due to the threats posed to humanity by climate change, genetic engineering and artificial intelligence. He then goes on to provide some suggestions for how we can deal with these threats before it's too late.

---
### 1. What’s in it for me? Learn about the looming threats to our species’ survival and what we can do about them. 

Imagine, if you can, the sum total of human life on Earth — not just the individual people who compose it, but the organizations, institutions, enterprises, cultures and religions they create together. Then add the social, political, economic, cultural and religious activities through which they interact with each other and the world around them.

There's no pre-established word for this unfathomably complicated web of people, groupings and interactions, but let's call it the human game. Why a "game"? Well, like a group of children playing a game of tag, we, as a species, "play" the game of human life for its own sake. Ultimately, there's no "point" to the game (at least from a non-religious standpoint). In the grand, cosmic scheme of things, the outcome doesn't really matter; after all, the universe is indifferent as to whether our species survives, thrives or perishes.

But most of us are anything but indifferent. We want the game to keep going — indefinitely. And we also want it to keep going in a recognizably human way; for example, we don't want our survival to come at the cost of our society turning into some dystopian nightmare. But, unfortunately, both of these objectives are currently in peril, and we have a very small window of time in which to prevent the human game from ending.

In these blinks, you'll learn:

  * how global warming already presents an unprecedented threat to our species;

  * how artificial intelligence and genetic engineering represent further threats waiting in the wings; and

  * what we can do to address this terrifying trio of threats before it's too late.

### 2. The threat of global warming and environmental destruction is already here. 

Unless you've been living under a rock for the past decade, you already know that global warming and environmental destruction in general present a very real threat to the human game. But what you might not realize is the extent to which the threat isn't something that lies in the future; it's already here, in the present, damaging our world as we speak.

Let's start with some sobering facts and statistics.

Twenty out of the past 30 years have been the hottest ever recorded. As a result of climate change, an entire third of the Earth's landmass has already been significantly degraded in terms of its ability to support animal life.

At the same time, there have been continually declining trends in the Earth's ability to provide the plant-based energy upon which that life ultimately depends for sustenance. And since 1970, the overall population of land-based animals has been cut in half.

Now, even if you're somebody who couldn't care less about the beauty of nature or the well-being of animals, these facts should be of deep concern to you, if only from a purely selfish standpoint. For instance, rising temperatures have already led to increased wildfires, droughts and lethal heat waves around the world.

And these problems can have huge ramifications for humanity. 

In Syria, for example, one of those droughts caused economic instability that helped to spark the Syrian Civil War, which led to the mass migration of a million Syrian refugees to Europe. That, in turn, provoked the rise of extreme right wing political movements in many European countries, where certain segments of the "native" populations felt threatened by the arrival of so many "foreigners." 

Meanwhile, in the Earth's oceans, the average water temperature has risen by up to one degree Fahrenheit in certain locations, such as the coastal waters of Texas.

That might not sound like much, but it was enough to increase the amount of water in the local atmosphere by about three to five percent. As a result, when Hurricane Harvey hit Texas in August 2017, it dumped 127 billion tons of rainwater onto the state — the worst rainstorm in US history.

With that amount of water, you could fill 26,000 stadiums. The water was so heavy that the city of Houston actually sank by a couple of centimeters.

Now, let that fact sink in for a moment — and then realize that it's just a taste of things to come.

### 3. In the worst-case scenario, global warming poses an existential threat to humanity and most life on Earth. 

If the world's temperatures continue to rise at their current projected rates, then by the end of the century, yada yada yada, blah blah blah — you've heard it all before, right?

Well, here's something you might not have heard: if the Earth's oceans keep getting warmer, they might get so hot by 2100 that the phytoplankton they contain won't be able to engage properly in photosynthesis any longer. As a result, they'll stop producing oxygen.

Now, there's one little problem with that: phytoplankton provide the Earth with two-thirds of its oxygen. In other words, by 2100, every animal on the planet could be dead. That includes us.

Game over.

Here's another plot twist: Sure, you probably already know that ice and permafrost are melting above the Arctic Circle — but do you know what's buried beneath the permafrost?

Well, in 2016, there was a heatwave in Siberia, and it melted the tundra's permafrost to reveal the carcass of a reindeer — and that reindeer happened to be infected with anthrax, which then spread into the surrounding water and soil, infecting 2,000 other reindeer and several humans, including a 12-year-old boy who died as a result.

But anthrax is just one of the nasty surprises waiting for us under the Earth's permafrost. The dark, cold and oxygenless environment beneath its surface provides a sort of Noah's Ark for other deadly microbes and viruses, such as smallpox, the Spanish flu and even the bubonic plague.

But wait; it gets even worse. Have you ever lifted a big block of ice? It's heavy. And all that ice that's melting in the Arctic Circle is _really_ heavy — which means that a lot of weight is going to be shifting from the surface of the Earth into the oceans as more and more of it melts.

The increased weight on the ocean floors will end up bending the earth's crust, which will in turn lead to an increase in a plethora of natural disasters — earthquakes, increased volcanic activity, submarine landslides and even tsunamis.

And that's not just speculative; there's already increased seismic activity happening in Greenland and Alaska, where ice is melting fast.

Admittedly, these are among the worst-case scenarios for the future of an Earth ravaged by global warming. There are some more likely, less apocalyptic possibilities — but unfortunately, they're not much better as far as the human game is concerned.

### 4. Even in the better-case scenarios, global warming still poses a serious threat to humanity. 

On the current trajectory of global warming, we're heading toward a near-term future in which the human game might not necessarily end, but in which the playing field will get much, much smaller. This is because the amount of habitable, arable land on Earth is set to shrink significantly in the coming decades.

Imagine we somehow manage to meet the global warming slow-down targets set by the Paris Agreement — the international pact that most countries on Earth made in 2016 to combat climate change. In this relatively rosy scenario, the average worldwide temperature would rise by "only" two degrees Celsius by 2050.

But even then, a quarter of the Earth would face severe droughts or desertification. For example, crop yields in the US Grain Belt — the country's main agricultural region — could plummet by 22 to 49 percent.

Unfortunately, even this scenario rests upon an optimistic assumption — after all, the success of the Paris Agreement is hardly a sure bet. Yes, nearly every country on Earth signed it, but it's a non-binding pact without any enforcement mechanism. Even worse, Trump's America is set to leave the agreement in 2019 — and the US is one of the world's leading contributors to global warming.

As a result, barring major changes to our current trajectory, a four, not two, degree rise in temperature is altogether likely.

And as you've probably already guessed, that means even more damage is likely to happen. For example, the overall level of corn production in the US will decrease by nearly half — and that's not just an American problem: the US is the world's main producer of corn, which is the most widely-grown crop on the planet.

Meanwhile, two percent of the Earth's land area will be submerged under the rising oceans. Home to megacities like Shanghai, New York and Mumbai, those coastal lands harbor 10 percent of the world's population and produce 10 percent of its overall economic output.

People will head inland — but many of the cities there will be practically uninhabitable due to extreme summer heat waves. In Iran and Pakistan, temperatures soared past a record-setting 129 degrees Fahrenheit in major cities in summer 2016. Similar temperatures await more than 1.5 billion people currently residing in that area of the world, as well as in India and other parts of the Middle East.

And as we'll see in the next blink, there will likely be tremendous societal repercussions to these effects of global warming.

### 5. On our current trajectory, the societal consequences of climate change will be extremely disruptive and dangerous. 

When a million refugees left war-torn Syria and migrated to Europe, political shockwaves were sent around the continent, fueling the rise of right wing extremism. Now, imagine taking the destabilizing effects of the climate change-fueled Syrian conflict and multiplying them by hundreds of times.

That's one of the darker sides of the probable future we're facing in the next few decades. By the year 2050, there could be between 200 million and one billion climate refugees, according to the International Organization for Migration.

And if that organization's name doesn't carry much weight with you, here's another one that might: the US military.

Some of their top brass are already anticipating climate-fueled conflicts on the near horizon. For example, when he was the chief of the US Pacific Command in 2013, Admiral Samuel Locklear called climate change his "greatest worry," stating that global "security will start to crumble pretty quickly" in the face of the mass migration ahead of us.

Now, add to the mix some other factors we touched on earlier: coastal megacities like Shanghai, New York and Mumbai under threat from rising sea levels; water shortages in major population centers like California; declining levels of food production in agricultural heartlands, like the US Grain Belt; and rising temperatures that will force many inland cities in large swathes of India, the Middle East and Western Asia to face unbearable summer heat waves.

And now, whisk all of those ingredients together, bake them in the oven of global warming and then get ready to put the icing on the cake: the price tag. For the generation of people who are currently children, some climate scientists estimate the costs of dealing with all of these repercussions of global warming could be upward of $535 trillion.

That's to say nothing of the cost in lives and suffering, the latter of which is incalculable, of course.

And then there's another immeasurable loss: that of our basic sense of security. For example, many residents of California can no longer wake up in their homes, look around their neighborhoods and safely assume that everything will still be there come next year. There have simply been too many climate change-fueled wildfires ravaging the state in recent years.

In California, as in many parts of the globe, nothing feels sure and stable anymore — and the resulting sense of uncertainty and instability will only increase in the year ahead.

### 6. Genetic engineering poses a second threat to the human game. 

For a change of pace, let's be optimistic for a moment and assume we find a way of weathering the literal and metaphorical storms created by climate change.

Now what? We've overcome the most daunting challenge that our species has ever faced. We've won the human game. Now we can collectively pop a gigantic bottle of champagne, right?

Nope — now the fun's just beginning. As in a video game, global warming is just the first boss we have to defeat; there's another one lurking right behind it — and this one comes straight out of a dystopian science fiction movie: high-tech genetic engineering.

The field is already getting hot, thanks to the relatively recent discovery of the potential usefulness of a genetic phenomenon known as Clustered Regularly Interspaced Short Palindromic Repeats. That's quite a mouthful, so it's usually denoted by its acronym, CRISPR, which also refers to the genetic engineering tools that tap into the phenomenon.

The precise mechanics of CRISPR are a bit complicated, but basically, it provides scientists with an easy way of editing our genes. Now, there's good news and bad news here. The good news? CRISPR means we'll soon be able to eradicate genetic disorders, such as cystic fibrosis and Down syndrome.

The bad news? Two words: designer babies.

There's already a lot of economic inequality and social division in the world — but imagine a future in which affluent couples can go to CRISPR-equipped fertility clinics that offer genetic upgrades for their would-be offspring. Want to boost their IQs? How about their attractiveness or their physical strength? Well, sure, just about anything is possible — as long as you can afford it.

Here's the nightmare scenario to which this could lead: a world that's divided between the rich and poor — not just in an economic sense, but a genetic one as well.

We could call this new elite the "GenRich" class, while the poor could be called the "naturals" — people who simply cannot afford genetic enhancements. Eventually, the differences between these two classes of human beings could become so great that they might not even be able to interbreed — at which point, they'd essentially become separate species.

That wouldn't be the end of the world per se — but it would be the end of the world as we know it. We'd still be playing some sort of game, but it would no longer be recognizably human.

### 7. Artificial intelligence poses a third threat to the human game. 

Let's be optimistic one more time and now assume that we're able to avoid the threat to humanity posed by genetic engineering. Is the coast clear now?

As you can probably guess by this point, the answer is no. There's yet another boss awaiting us as we play out the human game — and this one also comes straight out of science fiction: artificial intelligence, or AI.

An AI system is any form of computer technology that can perform feats of human-like reasoning, planning, learning and/or problem-solving. Now, when it comes to specific tasks, like playing chess, AI has already surpassed human intelligence. This form of AI is called Artificial Narrow Intelligence, or ANI. 

According to a survey of hundreds of AI experts, some time between the years 2040 and 2075, ANI itself will probably be surpassed by Artificial General Intelligence, or AGI. This form of AI won't be confined to a narrow task; it will be able to outperform our human minds in a broad range of areas, from creating original works of breathtaking art to solving the most perplexing problems of quantum physics.

Like already-existing ANI systems, an AGI system will be able to learn from its experiences and improve its intelligence on its own, without human assistance.

And it will be able to do that at lightning-fast speed, thanks to the sheer velocity of the electrons through which a computer carries out its operations. Over a given distance, those electrons can travel one million times faster than our neurons can send messages to each other in our brains. 

As a result, in just 2.5 hours, an AGI system could potentially evolve from the intelligence of a four-year-old to that of a godlike entity 170,000 times smarter than a human being.

What such an entity would be like or do is anyone's guess, since it's pretty much unfathomable to us. But it seems safe to assume that something that intelligent could easily wipe us off the planet, if it so desired.

But why would it want to do that?

Well, imagine an AGI system that just wanted to replicate itself as much as it could. To do that, it might decide it should try to hijack every other AI system in the world — but it wouldn't take superhuman intelligence to figure out that human beings might not like that and would try to prevent the system from achieving its objective. Unfortunately for us, there'd be a simple solution to that problem: get rid of the pesky humans.

Once again: game over. And unlike in a video game, there are no restarts.

### 8. If you want to save our species from annihilation, don’t count on space travel. 

So far, the outlook for humanity seems pretty bleak. But don't worry, if things get too hot — literally or metaphorically — we can just blast off into outer space, right? Develop the requisite technology, leave our desiccated husk of a planet behind and go find a new Earth. Problem solved.

Well, maybe in science fiction books and movies, but in reality, space is probably _not_ the final frontier, as Star Trek once put it. If anything, it'll probably be our final graveyard.

Here's the problem with outer space and our science fiction-fueled dreams of colonizing it: the cosmos might be a wonderful, amazing spectacle to behold through a telescope — but when you get up close to it, you find out that it's also a really, really inhospitable place for human life.

For example, just to get to Mars, you'd have to risk exposing yourself to the tremendous profusion of radioactive cosmic rays that suffuse the vastness of space stretching between the Red Planet and Earth. As a result, you'd be at an even greater risk of dying from cancer than of being killed by your spacecraft malfunctioning.

But let's say you get lucky, and your 50-million-mile journey to Mars is successful.

Now what? You're stuck on a lifeless rock of a planet that's so unsuited to human life that you'd probably have to live in an underground bunker just to survive. You could do that right here on Earth if you wanted to, but there's a reason most people avoid it: it's a miserable, cramped existence, unfit for human life. 

Okay, you might say, but maybe there's a better planet out there to colonize than Mars. And you'd be right. The closest one probably resides in a solar system named Trappist, and it's "only" 39 light years away from Earth.

Now, to date, the fastest object we've ever sent into space was the Helios 2 probe, which traveled at a speed 100 times faster than a bullet. That's super speedy by human standards — but by cosmic standards, you might as well be crawling. Traveling the same velocity as Helios 2, it would take a spacecraft 180,000 years to reach the Trappist system.

Fortunately, there's another planet that's much closer to home, and it's already furnished with everything we need to survive and thrive.

The planet's name is Earth. We just need to stop ruining it.

### 9. There are some relatively simple, practical steps we can take to address the threats to the human game. 

At this point, you'd be well within your rights to say, "All right, all right already. I get the point! Now stop with all of this doom and gloom, and just tell me what we can do about these problems!"

Well, there's no panacea for the threats of global warming, AI or genetic engineering that we've covered in these blinks, but there are some relatively simple steps we can take to improve our odds of survival with each of them significantly.

For artificial intelligence, one of our main objectives should be the development of some sort of fail-safe switch — a way to turn off an AI system before it becomes too smart and potentially dangerous for us to deal with. Such an effort is already underway on Wall Street, where AI systems currently do most of the trading in the American financial sector. Scientists are trying to create safeguards to prevent those systems from crashing the US stock markets.

For genetic engineering, we need to put in place strong regulations regarding what can and cannot be altered in our genetic codes. Heritable diseases? Yes, please get rid of them. Genetic enhancements for the rich and powerful? No, thank you.

Of course, it won't do us much good if one country puts these regulations in place while another one doesn't, so there will need to be many international agreements and extensive cooperation to ensure that everyone is abiding by the same rules in the human game.

Now, when it comes to global warming — and you're probably already familiar with this fact — some of the main culprits are the carbon emissions that result from the burning of fossil fuels. To cut these emissions, we need to replace fossil fuels with renewable sources of energy, through solar, wind and hydroelectric power.

Now, we've known that for a long time, but until recently, the financial costs of acting upon our knowledge were pretty prohibitive. Fortunately, those costs have been greatly reduced over the past few decades. For example, from the 1960s to 2018, the price of producing electricity through solar power plummeted from $100 to just 30 cents per watt.

As a result, if we got our act together, we could pretty easily replace virtually all forms of fossil fuel-derived electricity with renewable sources of energy by 2050.

Unfortunately, that's a pretty big "if," as we'll see in the next and final blink.

### 10. Powerful people and corporations stand in the way of us doing anything about global warming, genetic engineering and AI. 

You didn't think you were coming to the end of these blinks about the human game without facing off against one final boss, did you? Well, here comes the ultimate nemesis — and plot twist: Guess who it is?

Ourselves.

In the end, the biggest threat to our species is our species itself — or, to be more precise, the rich and powerful members of that species, along with the even richer and more powerful corporations they control.

For decades now, the fossil fuel industry has been fighting tooth and nail to prevent us from doing anything about global warming. For example, back in the late 1970s, Exxon was the world's largest oil company — and their scientists already knew that global warming would result from the carbon emissions created by burning their company's product.

Internally, the company took this knowledge very seriously; for example, it started adjusting the heights of its new oil drilling platforms to take into account a future rise in sea levels. But Exxon kept its awareness of the impending threat of global warming a secret, and it wasn't until 1988 that this threat started to become public knowledge — wasting an entire decade in which we could have acted upon that knowledge.

Even worse, instead of sounding the alarm, Exxon and other members of the fossil fuel industry did quite the opposite; they initiated a deliberate disinformation campaign.

In an internal memo written in 1988, a public affairs manager at Exxon said the company should "emphasize the uncertainty" in the data that supports the scientific consensus about the reality of climate change and its relation to fossil fuel consumption. To that end, the company started investing in television ads, billboards, think tanks, research papers and government lobbying efforts to sow seeds of doubt in politicians and the general public.

And it worked. In late 2017, a poll found that nearly 90 percent of Americans didn't know there was a scientific consensus on climate change.

Meanwhile, AI and genetic engineering are being pushed by an industry that's already rivaling the likes of Exxon in terms of wealth and power: the tech industry. The charge toward developing AI is being led by tech giants like Google, while genetic engineering has become a hobby horse for Silicon Valley billionaires like Peter Thiel and Jeff Bezos, who have invested in start-ups aimed at "curing" the phenomenon of aging.

With so many powerful people and corporations standing in the way, it's going to take some major political willpower and mass organizing to overcome these threats to humanity.

The stakes couldn't be higher. But, by the same token, neither is the potential reward: saving our beautiful planet and, in the process, ourselves.

### 11. Final summary 

The key message in these blinks:

**Global warming, genetic engineering and artificial intelligence pose major threats to the continuation of human life as we know it. If they don't outright annihilate our species, they could fundamentally change, for the worse, the way we live our lives. There are certain practical steps we can take to avert disaster, such as investing heavily in renewable energies, but powerful people and corporations stand in the way of our doing so. To preserve human life as we know it, we will need to organize against them.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _This Changes Everything_** **, by Naomi Klein**

After emerging from these rather morbid blinks, you've probably lost a bit of hope, while also gaining a pretty good idea of what we're unfortunately doing to our planet. But why are we doing it? Why don't we stop ourselves? How can we change the tragic trajectory we're currently on with climate change? And what can we do to avoid the impending disasters we face?

The renowned author, filmmaker and political activist Naomi Klein can provide you with some straight answers to these difficult questions — answers that don't sugarcoat anything, but also leave you with some rays of hope. To receive those answers and warm yourself back up with those rays, check out our blinks to _This Changes Everything_ (2014).
---

### Bill McKibben

Bill McKibben is a professor of Environmental Studies at Middlebury College in Vermont, a founder of the environmental organization 350.org and the author of 17 books. Those books include three bestsellers: _The End of Nature_, _Deep Economy_ and _Eaarth: Making a Life on a Tough New Planet_. He's also received the Gandhi Prize, the Thomas Merton Prize, and the Right Livelihood Prize for his political activism and advocacy on climate change.

