---
id: 576d60a565d3da0003a7def1
slug: farmageddon-en
published_date: 2016-07-01T00:00:00.000+00:00
author: Philip Lymbery with Isabel Oakeshott
title: Farmageddon
subtitle: The True Cost of Cheap Meat
main_color: 24B356
text_color: 177337
---

# Farmageddon

_The True Cost of Cheap Meat_

**Philip Lymbery with Isabel Oakeshott**

_Farmageddon_ (2014) is an in-depth guide to the dark reality of cheap meat. These blinks explain how industrial farming has replaced traditional methods and how it's draining our resources, poisoning the environment and making us unhealthy.

---
### 1. What’s in it for me? Discover the grim consequences of factory farming. 

We have come a long way since the very beginning of agriculture thousands of years ago — a very long way. Even the simpler forms of advanced agriculture that our grandparents witnessed seem to be vanishing at a steady pace. What has taken their place? Farming on an industrial scale.

Just think, when you escape the urban jungle into what should be a rural idyll, what do you see? Gigantic, ugly farms. Places where animals are born, live and die in a horrific, albeit efficient, manner. These places ensure we have enough meat, milk and eggs, but at what cost?

These blinks show you how grimly the techniques of agriculture have evolved, and the consequences our planet, and its people, now face.

In these blinks, you'll learn

  * how many bathtubs of water you'll need to produce one kilo of beef;

  * why so many children in California's Central Valley suffer from asthma; and

  * what you can do to counterbalance the unfortunate popularity of factory farms.

### 2. Factory-like, “mega” farming of meat, milk and eggs is replacing traditional methods. 

When you think of a farm, it's easy to imagine a pastoral wonderland full of lots of different types of animals that joyously roam around in the sunshine and grass. But nowadays, such traditional, diverse farms are practically extinct, replaced by industrial agriculture.

That's because agricultural techniques have experienced a massive revolution over the last several decades. Human labor has been almost entirely replaced by machinery and traditional farms that employ farmhands are mostly a thing of the past. What's more, just 8 percent of England's farms raise more than one type of animal.

But the disappearance of these farms isn't as scary as what's replaced them: _factory farming_. This refers to farms that grow only one type of animal and lots of them, cramming them into tight spaces, overloading them with drugs and using machines to handle them just to maximize profits. At this point, a whopping two-thirds of the world's 70 million farm animals are raised in such conditions.

And factory farming isn't just for meat; it's increasingly being used for milk production as well. In _mega-dairies_, vast quantities of cows are stabled together and milked with industrial methods. A big one can contain up to 10,000 cows in a single space.

Not just that, but they're popping up all over. The first mega-dairy was built in California in 1994. Now there are 1,620 dairy farms in the state, collectively housing a total of 1.75 million cows and producing six million dollars' worth of milk a year.

And our eggs?

The vast majority are from factory-raised hens. Unsurprisingly, these chickens are stuffed into overcrowded factory farms to produce more eggs with the lowest possible cost. So 60 percent of the world's eggs are laid by caged hens.

### 3. Factory farms pollute both the water and the air. 

While factory farming is a relatively new form of food production, its devastating environmental effects have already been laid bare. For starters, it pollutes the water. That's because industrial animal farms churn out tons of animal waste — that is, excrement — and all into one area.

For instance, a mega-dairy of 10,000 cows produces about as much fecal matter as a city the size of Miami, which contains 400,000 people. To put it another way, the mega-dairies of California, altogether produce as much waste as the entire population of the United Kingdom. That's about 70 million people.

So, what do farmers do with all this animal waste?

They store it in artificial lagoons attached to the farms. The problem is that these lagoons leak, and as waste seeps into the ground it contaminates the water supply. In fact, a survey of water supplies in the Lower Yakima Valley of Washington found that water sources within range of mega-dairies had higher concentrations of _E. coli._ bacteria.

Water isn't the only thing factory farms are polluting. They're also ruining the air. That's because industrial farming produces loads of polluting gasses. These noxious gasses and nitrates come from the same animal waste lagoons that contaminate our water.

This means that the air near factory farms is extremely unhealthy, and pollution-related illnesses are much more prevalent in the areas surrounding such farms. For instance, in the Central Valley of California, home to many of the state's mega-dairies, three times as many children suffer from asthma compared to the national average.

So, factory farming is clearly poisoning our water and air, but its detrimental environmental effects extend well beyond that.

### 4. Factory farming is killing off the birds and the bees. 

Now you know how factory farming is destroying the quality of our air and water, but you're probably wondering what effect it has on local wildlife. Well, the destruction of animal habitats is another major consequence of industrial farming.

That's because hedges and unplowed borders, which formerly divided fields in smaller farms and offered habitats for birds and bees, are obsolete in factory farms. As a result, between 1980 and 1994, 100,000 km of UK hedges vanished, along with the birds and insects that used to reside in them.

But factory farms are also bad for the food supply of birds. That's because they use tons of fertilizer to grow crops, thus damaging earthworms, a vital part of a bird's diet.

And the bees?

Well, the heavy use of nitrogen fertilizer has stopped clover from growing in a natural rotation, which is alarming because bees feed on the plant.

So, it's plain to see that factory farming results in a declining population of birds and bees. In fact, a 2011 report from The Nature Conservancy claims that of the 1,000 species of birds in the United States, a quarter are endangered or are facing conservation issues. Not just that, but in the United States some bee species that existed as recently as the 1990s have gone extinct entirely.

However, bees are essential, and their decline has made many industrial farms resort to importing them. In fact, one-third of agriculture is dependant on bees for pollination. Furthermore, the UN estimates that about 63 percent of the world's _food_ relies on bees for this essential action.

This means that when the bees disappear so does the pollination and the crops. The only solution for factory farms is to bring in the bees from someplace else.

For instance, Californian almond growers import bees all the way from Australia, ordering about 40 billion bees each year!

But the bees have to come from somewhere, and as industrial farming spreads to every inch of the globe, we'll soon run out of habitats for these essential creatures.

OK, we've covered the situation on land, but there are plenty of aquatic animals too. So next we'll explore how factory farming is leaving its mark on the oceans.

### 5. Fish farming is wreaking havoc on the oceans. 

Our planet is largely composed of water and that water is home to millions of fish. But not all of them can be found in the wild depths of the ocean. In fact, many fish are farmed and live truly miserable lives.

Industrial fish farms are nothing like a fish's natural environment: fish are crammed into jam-packed cages, sometimes 50,000 to a single enclosure. For example, trout are generally caged at a density of 60 kg of fish per cubic meter of water. To put it another way, that's like putting 27, 12-inch long trout in a single bathtub.

Naturally, living in such cramped conditions makes the fish more vulnerable to lice and diseases. In fact, 10 to 30 percent of all farmed salmon die before they can be eaten.

Their suffering is all the more tragic due to the sheer scale of fish farming: about 100 billion fish are farmed worldwide every year. That's more than all the cows, pigs, chicken and other land animals raised for food combined.

And fish farming isn't just bad for the fish themselves. The production of fishmeal for this purpose is also wreaking havoc on the oceans. Here's how:

Fishmeal is made by drying lots of tiny fish or by crushing them into oil. It's then used to feed farmed animals like pigs, cows, chicken and even bigger fish. This is problematic because every year, millions of tons of tiny fish are vacuumed up for this purpose. The United Kingdom alone purchased 135,000 tons of fishmeal in 2010.

To make all this fishmeal takes a lot of tiny fish, the depletion of which has horrendous effects on the ocean and the birds that live in it.

For instance, off the coast of Peru, the world's leading producer of fishmeal, seabird populations have dropped from 40 million to just 1.8 million. That's because the birds eat anchovies, most of which are being killed for fishmeal and shipped overseas.

### 6. Cramming more animals into small spaces isn’t even space efficient. 

So, it's clear that factory farming is terrible for the environment, but doesn't it at least save space?

Nope.

While cramming animals into tiny spaces means less land is needed to raise them, these animals still need to eat something. And the amount of land it takes to run even a small factory farm is tremendous: one 892 square meter chicken shed that produces 150,000 chickens per year requires 90 hectares of "ghost acres," that is, land needed to feed those chickens.

As a result, acres and acres of grassland is upturned and planted with soy and cereal grains like corn.

In fact, one-third of the globe's cereal grains are used to feed livestock, and in wealthy countries, the proportion can be as high as 70 percent. What's more, 90 percent of the world's soy meal is used to feed factory-farmed animals.

But space isn't the only issue with this system. Many people around the world are suffering from food shortages, and yet we're using precious farmland to grow grain for animals! In fact, all the cereal grain produced for animals could feed 3 billion people. Given how much of the world's population struggles with hunger and malnutrition, this is a travesty.

To understand the scale of the problem, consider that the UN estimates that in 2009, 45 million hectares of land were acquired for this reason worldwide. Many of these acquisitions occurred in developing countries in Sub-Saharan Africa or South America.

Just take Argentina, where 200,000 hectares of forest are lost every year to soy production. Some of this land is even stolen from indigenous people. For example, in 2009, 40,000 hectares of Qom tribal land was acquired by a soy company. The land was fenced in and surveilled, making the tribe's own land off-limits to them.

### 7. Factory farming, unlike organic farming, is exhausting our oil and water reserves. 

Everybody knows that oil and water are precious and diminishing resources. But did you know that factory farming is greatly accelerating the loss of both?

That's because factory farming uses way too much water. As much as one-quarter of the world's freshwater is used to produce meat and dairy. That's because making a single kilogram of beef requires nearly 90 bathtubs of water.

And, on average, producing meat uses ten times the amount of water per calorie required by vegetables or grains. That means the easiest way to save water is to cut meat production.

To understand how bad the situation is, consider that by the early twenty-first century, the world's reserves of groundwater were being depleted at more than twice the rate they were in 1960. To put that in perspective, if the Great Lakes of America were shrinking at this speed, they would be gone in 80 years.

But water isn't all that factory farming is wasting. It's burning loads of oil as well. In the United States, modern farming techniques use 6.3 barrels of oil for every hectare of crops grown. That's about $200 worth of oil per hectare. Two-thirds of this oil goes into making chemical fertilizers, pesticides and other chemicals that produce agricultural products, which in turn feed livestock.

Organic farming is much more energy efficient. For instance, the production of organic milk and beef require almost 40 percent less energy to produce than their non-organic counterparts.

In fact, a 2006 study found that if just 10 percent of US corn production were switched to organic, the country would use 4.6 million fewer barrels of oil per year. That's a saving of about $143 million annually!

### 8. Factory farming results in more famine around the world. 

OK, factory farming is wrecking the environment, taking up more space and burning precious resources. But at least it's cheaper, right?

Well, not when you look at the big picture. Here's why:

As industrial farming proliferates, demand for livestock feed skyrockets and crop prices rise. But that's not all. When commodity speculators see prices rising they often buy up loads of grain in the hope that the upward trend will continue. This hoarding causes food prices to soar even higher.

For instance, in Lusaka, Zambia the price of bread rose by a whopping 75 percent between September 2010 and April 2011.

The natural result is poverty and famine. In fact, according to a UN estimate, 50 million people ended up in poverty because of rising food prices in the latter half of 2010 alone.

And, as developing countries begin using factory farming, its negative effects will multiply. That means it will burn more oil, waste more water and cause greater pollution.

But beyond that, the rural farmers in developing countries will also suffer, because they will be forced to adopt grueling industrial farming techniques. For instance, Maharashtra, India is now known as the "suicide belt" because, since 1995, over 250,000 farmers there have taken their own lives. That's the equivalent of a suicide every 30 minutes.

And the most common reason?

They can't keep up with the hectic and stressful industrial farming techniques they have to use.

### 9. Factory-farmed meat breeds bacteria and obesity. 

What about the effects that factory farming has on you, the consumer?

Unfortunately, factory-farmed meat is also bad for your health, because diseases flourish in the farms. After all, if you cram loads of animals into tight spaces, viruses and bacteria will thrive as they spread and evolve much more easily within a concentrated group of hosts. Not just that, but the immune systems of factory-farmed animals are also weak because of the atrocious conditions in which they live.

What's worse, humans are prone to the same infections. The United States alone sees 9.4 million cases of food poisoning every year. A third of these are caused by salmonella and 15 percent by the bacteria Campylobacter, a pathogen that thrives in poultry and pig factories.

So, factory animals are prone to disease and, to prevent their animals from falling ill, farmers often drug them with large doses of antibiotics. Over time, the bugs that live in factory farms become resistant to these antibiotics, so when they make it into a human host, they're much harder to treat. As a result, 25,000 people die in the EU from antibiotic-resistant infections annually.

But antibiotic-resistant pathogens aren't the only health issue with factory farming: it also causes obesity. That's because factory-farmed meat is much less healthy than its organic counterpart.

A 1968 study by Professor Michael Crawford found that in factory-farmed animals the ratio of "bad" fats, like saturated ones, compared to "good" fats like omega-3 and omega-6 fatty acids is 50:1. In organically raised animals that ratio is 3:1.

This disproportionate ratio means that increasing numbers of people suffer from obesity. In fact, based on current trends, by 2030, half of all US citizens will be obese.

It's no surprise then that eating less factory-farmed meat would bring clear health benefits: a 2009 state study in the United Kingdom found that if people reduced their intake of saturated fats from meat by 30 percent, coronary heart disease rates would drop by 15 percent.

### 10. Genetically modified crops carry unredeemed potential. 

Did you know that we're facing a worldwide food crisis?

The UN has estimated that to meet our needs in 2050, global food supplies need to increase by 70 to 100 percent.

Meanwhile we're using ever-greater quantities of resources like land and water to produce our food. So, how can we tackle this looming challenge?

Well, there are many potential benefits to be gained from genetically modified, or GM, grains. These are grains that have had their genetic makeup changed by scientists, and they offer one option for meeting the world's rising demand for food.

One GM grain, Golden Rice, contains extremely high levels of beta-carotene, a precursor of Vitamin A. It's estimated that every year, Golden Rice could save 1–2 million people from a fatal Vitamin A deficiency caused by malnutrition.

On the other hand, GM crops raise lots of issues. They're often designed to be resistant to strong pesticides, which means farmers can spray their crops with hyper-potent chemicals, potentially destroying the surrounding environment even more drastically than "regular" industrial methods do.

Not just that, but over time, GM crops harm traditional farmers through what's known as genetic drift. If a GM seed gets blown onto a non-GM farm, it could sap all the nutrients from the ground, leaving regular crops to starve.

And those aren't the only problems. Right now, GM crops aren't helping the food crisis at all because, for the most part, humans don't eat them. Instead, GM crops are mostly used as animal feed. Close to 40 percent of US corn is used to feed animals and 85 percent of it is GM. In fact, just 30 percent of the calories in the animal feed wind up being eaten by the people in the form of meat.

### 11. Modifying and cloning animals can increase profits – at the expense of animal rights. 

Do you remember Dolly the Sheep? In 1996, she became the first mammal ever to be cloned from the cells of another. Now, cloning is touted as a route to producing super-animals and serious profits.

That's because, while selective breeding can't guarantee that the resulting offspring will be in as prime condition as their parents, cloning can. So, if you cloned a prize pig, the new hog would be just as prize-worthy.

Not just that, but cloning an animal with mutations that are particularly helpful, like resistance to diseases, would boost profits because such animals would stand a greater chance of surviving till slaughter.

However, cloning does raise some serious animal rights issues. That's because cloning reproduces animals that have already been bred to extremes.

For instance, say you breed a "super cow" that produces loads more milk than a normal one. The cow will suffer because you've pushed its body to a level of production beyond what it can healthily handle. Naturally, the cloned animals will suffer just as much as the animals from which they were cloned.

But beyond that, cloning is simply an ugly process. In fact, lots of cloned animals die or are born with such severe deformities that they must be put down. Out of 277 attempts, Dolly the Sheep was the only successfully birthed embryo and, at present, just 30 of every 1,000 cloning attempts are successful.

Besides cloning, animals are also being subjected to unnatural modifications. For example, the geneticist Avigdor Cahaner bred a featherless chicken. This is beneficial because without feathers the bird will stay cooler, enabling factory-farmers to pack even more chickens into a single cage.

Or consider the Chinese scientists who are working on a cow that can produce milk similar to that of a human mother. They've inserted human genes into 300 cows, and the cows' milk has taken on some of the important properties of breast milk.

### 12. China has adopted industrial farming techniques, and consumers are facing the health consequences. 

China is home to over 1.3 billion people and they love to eat meat, especially pork — the average Chinese person eats about 34 kg of pork every year, compared to the 25 kg average for British people.

A few major Chinese food companies are seeing how profitable it is to raise pigs using industrial techniques. One of the biggest pig farmers in the country is Muyuan, based in Henan Province. Its goal is to raise 9 million pigs per year by 2017 and it's using industrial techniques to meet it.

Other farms are going so far as to import high-yielding, selectively bred sows from the West. These pigs make the 9,000 km journey on Boeing 747 airplanes at a cost of about £330,000 per trip. And the Chinese pig farmers are willing to pay that because the pigs produce a high number of piglets per litter. Naturally, more piglets mean more meat and eventually more profit.

So, the Chinese are going whole hog, so to speak, when it comes to industrial pig farming, and this includes some dubious practices.

In 2011, it emerged that some farmers were injecting their pigs with the steroid Clenbuterol. It was reported that 300 people were poisoned by meat treated in this way. This has prompted many consumers to start asking questions about how their pork is produced.

And the pig farmers are not the only ones under scrutiny. Back in 2008, the Chinese dairy industry was hit by a scandal in which companies mixed toxic melamine into their milk to cheat protein level tests. As a result, consumers started demanding more information about their food. But despite such scandals, malpractice in food production is still rampant.

### 13. Reducing food waste and returning to traditional farming techniques can undo the damage wrought by factory farming. 

OK, given what we know about industrial agriculture, it's clear that our methods of food production need to be transformed. But what's the best and most sustainable way to meet humanity's increasing food demands?

For starters, we need to cut back on food waste. Worldwide, an enormous amount of food is wasted every day. In fact, US consumers waste about 30 percent of the food they purchase. And globally, we waste the equivalent of 11,600 million chickens, 270 million pigs and 59 million cows a year!

Obviously cutting these numbers will help with food shortages as food that's saved can be used to feed the hungry. But that's not the only reason to reduce waste. Wasted food also means wasted resources.

For example, the UN estimates that 28 percent of all agricultural land worldwide is used to produce food that ends up being wasted. By merely cutting this percentage in half we could feed another billion people. Not just that, but the water that would have been used to produce that wasted food could satisfy the domestic needs of 9 billion people.

So, cutting down waste is essential, but returning to traditional methods of farming would also make a huge difference. Here's how.

On non-industrial farms, ruminants like cows are raised on grassy pastures. That means they eat grass, which is inedible to humans, and turn it into tasty and edible meat and milk. It also means that they don't need to be fed grain that was shipped across the world, or fish that could be eaten by humans.

Traditional farms also replenish the soil. This is important because soil erosion is already affecting about a third of the world's arable land and eroded soil is less productive, requiring more chemicals for successful cultivation. But with traditional practices of crop rotation and free roaming animals, the soil maintains its optimal nutritional balance naturally.

### 14. Consumers have the power and information necessary to make meaningful choices. 

Chances are, you're a consumer like the vast majority of people. You're also probably not a hugely influential politician or a fairy godmother. So, can a normal person do anything to change the way food is produced?

Absolutely. In fact, as a consumer, you wield tremendous power with the decisions that you make and the products you consume. That's because retailers only stock what turns a profit. So, the more traditionally produced products you buy, the more demand you'll create for them. This will make traditional farming more profitable and therefore more popular.

However, you can only make the right decision if you're properly informed. And thanks to existing initiatives, consumers already have a helping hand when it comes to obtaining such information. For instance, most labels and packages show the content and origin of meat very clearly.

You can also check for evidence of traditional rearing on meat labels in the United States by looking for beef labeled as "grass-fed" or "pasture-raised" and chickens certified Animal Welfare Approved, Global Animal Partnership or Certified Humane. Labels like "farm-fresh" or "all-natural" are generally misleading and don't really mean anything.

Finally, when it comes to milk and eggs, in the United States you should look for packages certified Animal Welfare Approved. Eggs in the United States might also be labeled Certified Humane. In the United Kingdom, "barn eggs" are from hens that live in a big barn but aren't allowed outside.

Of course, some changes require governmental action, like winding down the subsidy programs of the United States and the European Union that actually _encourage_ factory farming. For instance, the 2008 Food Bill in the United States portioned out the most subsidies to corn, the key crop in livestock feed. It will take lobbying and government willpower to make a change here.

### 15. Final summary 

The key message in this book:

**Factory farming is cruel, an environmental disaster and horrible for our health. Our entire food production system needs to be transformed, and as consumers we all have a role to play in making that happen.**

**Actionable advice:**

**Don't fall for advertising gimmicks!**

When shopping for groceries, you might see fancy packages advertizing "corn-fed" chicken, but this is a gimmick. In fact, most factory-farmed chickens are fed corn. So don't be fooled. Instead, always keep an eye out for certified labels and remember, ambiguous labels like "farm-fresh" or "natural" have no legal significance with regards to how the meat was raised.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Meat Racket_** **by Christopher Leonard**

_The Meat Racket_ (2014) tells the bleak story of how a few giant corporations managed to monopolize the entire meat market in the United States. Through cunning business strategy, massive loans and a fair amount of bullying, companies like Tyson Foods rule the meat industry virtually unchallenged.
---

### Philip Lymbery with Isabel Oakeshott

Philip Lymbery is the chief executive officer of Compassion in World Farming. He is also an animal rights activist and the former communications director for the World Society for the Protection of Animals.

Isabel Oakeshott is a political journalist and nonfiction writer.

