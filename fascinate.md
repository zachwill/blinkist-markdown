---
id: 5405d5666632650008240000
slug: fascinate-en
published_date: 2014-09-02T00:00:00.000+00:00
author: Sally Hogshead
title: Fascinate
subtitle: Your Seven Triggers to Persuasion and Captivation
main_color: 30C4F1
text_color: 196880
---

# Fascinate

_Your Seven Triggers to Persuasion and Captivation_

**Sally Hogshead**

In _Fascinate,_ author Sally Hogshead helps us realize our potential for fascination. By explaining in vivid language exactly how fascination works and how you can trigger it in others, _Fascinate_ provides you, your company and your brand with the tools to fascinate. These "seven triggers of fascination" can help you to increase the odds of success, both in your personal life and in business.

---
### 1. What’s in it for me? Discover how to make yourself or your company more fascinating in just eleven blinks! 

Most people would like to become more fascinating — as would most companies and brands. Behind this desire is an awareness that fascinating others is essential to attracting and persuading them. In other words, we know that fascination can help us get what we want.

Author Sally Hogshead identifies the seven "triggers" of fascination — Lust, Mystique, Alarm, Prestige, Power, Vice and Trust — and explains that learning how to use these triggers is key to maximizing your fascination potential, and thus becoming more persuasive.

It's easier than you might first think, as each of us has the innate ability to be fascinating. And, perhaps surprisingly, we're all using these triggers already, without even knowing it.

In these blinks, you'll read about how popular companies make use of fascination triggers. You'll also find a three-step guide to adapting these triggers to support your company's goals.

Also in these eleven blinks, you'll discover:

  * which restaurant kicks out paying customers for ordering the "wrong" meal;

  * how a secret recipe was instrumental to Coca-Cola's success;

  * why you often spend a silly amount of money on a single piece of clothing; and

  * why a teenager might fear losing his driver's license more than dying in a car crash.

### 2. Each one of us naturally has the ability to fascinate and be fascinated. 

We've all been _fascinated_ by something: a piece of music, a meteor shower, an intelligent and beautiful person.

But what _is_ fascination?

To be fascinated can be defined as a moment of total fixation on a particular thing. Where, though, does fascination come from? And why do we experience it?

Our capacity for fascination is a product of human evolution — put simply, it evolved to help our ancestors survive.

Consider the rest of the animal kingdom. To survive, animals too have to fascinate and be fascinated. The peacock's colorful fan is noticed and judged by the peahen, and the most impressive fan — the one which _fascinates_ her — will belong to the peacock that will father her peachicks.

A capacity for fascination is a trait that continues to play an important social role: it helps us to develop social ties.

In a study, researchers showed babies a series of pictures, some of them depicting human faces. Interestingly, whenever a picture of a face came up, the baby stared at it much longer (up to twice as long) than at the other pictures.

Why? We're fascinated with faces at such a young age because it helps us to establish and develop close social bonds with others.

However, our innate capacity for _experiencing_ fascination is just one side of the story; we have also an inborn desire and ability to _be_ fascinating ourselves.

Just like the peacock, we desire and aim to attract others by fascinating them. Flirting, for example, is a universal and innate ability to fascinate others. It doesn't matter which culture, language or religion we examine; everyone knows instinctively exactly how to fascinate, without needing to be taught.

Yet, just because we all _know_ how to be fascinating, this doesn't mean that we always do it well! You can reach your full potential for fascination by learning about the _seven_ _triggers_ _of_ _fascination_.

### 3. Lust is what we feel when we anticipate a pleasure; it also makes us act irrationally. 

You're at a dinner party. As the evening winds down, you notice there's one piece of chocolate left on the table. No one else seems to want it, but you don't want to be the one to take it. Yet you can't stop thinking about how the chocolate would taste, how it would feel as it melts on your tongue.

What's behind that craving? _Lust._

Lust is the anticipation of pleasure. What's more, the feeling we get from this _anticipation_ is often far stronger than that we feel when we actually fulfill our desire.

In a study, researchers scanned the brains of monkeys while offering them a single grape. The researchers observed that when the monkeys spotted the grape, their brains showed increased stimulation. This stimulation increased further when the monkey held the grape.

Yet when the monkey finally got to eat the grape, the stimulation decreased. The experience of consuming the grape wasn't nearly as fascinating as was the _anticipation_ of eating it.

Clearly, lust is very powerful. It's no surprise, then, that it can cause people to make irrational decisions — something which can be exploited by brands and companies.

The reason that people will sometimes pay unreasonably high prices for clothing is that companies intentionally trigger lust with their products. They do this by investing these products with the promise of pleasure.

For example, a jacket might be made from a soft material that's very pleasant to touch. When shoppers notice this jacket in a store and then touch it, they experience pleasure. So even though the store might be filled with less expensive jackets, this particular jacket will _fascinate_ the customer, prompting them to pull out their credit card and buy it.

In short, lust causes us to ignore our inner rational voice and leads us to do things that normally we might not do.

> _"Lust conquers the rational evaluation process, freeing us to stop thinking and start feeling."_

### 4. Mystique fascinates us, as we’re always interested in figuring out an unsolved mystery. 

Whether we're engrossed in a "whodunit" or scanning the morning headlines, an unsolved murder is something that captures our imagination.

A lack of facts around a case prompts speculation, engaging your imagination and desire to figure out what might have occurred. But why does this happen?

The answer is _mystique_ _–_ another of the seven triggers of fascination.

Our fascination is triggered when we don't know the answer to a particular question. Driven by the need to fill gaps in our understanding, we'll then try to solve the problem or answer the question ourselves.

Because this desire is extremely powerful, it can be exploited — for instance, by companies that construct entire marketing strategies based on mystique.

Take, for example, Coca-Cola. For decades rumors and stories have spread about the secret formula of the company's flagship soft drink. Even though fans of Coca-Cola really want to know what's in it, the company refuses to reveal its secret — a strategy which makes the product fascinating to fans who cannot stop speculating about the secret recipe.

For many companies, there are plenty of ways to maximize this sense of mystique — one of which is to limit access to a product or service in a way that makes people eager to know more about it.

Take, for example, a restaurant in Los Angeles called Crustacean, which has become famous for its garlic crab dish. Indeed, people travel far and wide just to taste it for themselves.

What is it that makes the dish so fascinating? Its _mystique_. The dish is prepared in a special kitchen that has been built _inside_ the actual kitchen, and only a few chefs have access to it. This ensures that the preparation of the dish remains a secret to most people.

> _"We're fascinated by mystique because of our natural desire to fill in missing information."_

### 5. We are fascinated by things that threaten or alarm us. 

Filling out a tax form isn't the most enjoyable thing to do — which is precisely why so many people put it off until the last minute! However, when you realize that you have just two days left to file, that tax form and the task of completing it becomes instantly fascinating.

Why? Because as soon as you feel the imminent threat of serious consequences, you become _alarmed_, and this triggers your sense of fascination.

Sometimes, though, the threat which fascinates us most isn't the most obvious; an alarm is triggered not by the _biggest_ threat, but by the most _relevant_.

Consider this example: To prevent teenagers from drinking and driving, an advertising campaign showed images of gruesome car accidents that occurred when the drivers involved were intoxicated.

Though designed to scare teenaged drivers into staying sober behind the wheel, the campaign did not have the desired effect.

Why?

Because teenagers weren't threatened (and thus not _alarmed_ ) by the possibility of dying. It simply wasn't an issue in their lives.

Once other campaigners noticed this, they launched a different campaign that focused on a far less obvious threat: if you drive while drunk, you'll probably lose your license.

Surprisingly, this alternative campaign was much more effective, as teenagers as a whole were far more concerned with losing their driving privileges than with the possibility of death or injury.

So how do companies exploit the alarm trigger to increase sales? One tactic is to introduce the threat of an impending deadline.

Say you're watching a teleshopping channel, but aren't too interested in what's being sold. However, if the hosts inform you of an immediate deadline — "Our vacuum cleaners are incredibly popular, but hurry, there are only 50 left!" — this might trigger your fascination.

### 6. Prestige is the fascination with a promise of raised rank and respect in society. 

Have you ever noticed how many popular magazines and TV shows feature vastly wealthy people with massive houses and wildly expensive sports cars?

If so, you might also wonder why it is exactly that such shows and magazines are so incredibly popular.

Their popularity is due to our fascination with _prestige,_ or the desire to be "better" or seen as more valued than others in society.

Prestige is a trigger which causes us to act irrationally in our pursuit of higher social status, a fact that companies are always ready to exploit.

One way that companies do this is by limiting the availability of products to increase their desirability, thus making them _luxury_ _items_.

Indeed, "quality over quantity" is the rule when it comes to prestige, as employing this trigger puts your products out of many customers' reach.

Consider what it is that makes a Michael Kors purse so much more prestigious and desirable than one you can buy from H&M. The answer is _expense_ : the price of a Kors purse means that most people simply cannot afford it.

But if you happen to be part of the minority who _can_ afford the product, owning that purse would result in you feeling you have a higher status than the countless others who don't own one.

Another way for companies to employ the prestige trigger is to use brand emblems or symbols on products. This enables customers to publicly display their commitment to a brand and in turn the prestige of owning such a product — as with designer clothing that has the designer's label clearly displayed.

This tactic is also used by companies aiming to encourage their own employees' commitment to the brand.

The cosmetics brand Mary Kay allows top-performing employees to drive a pink Cadillac — a prestigious status symbol that strengthens an employee's commitment to the company. This increased commitment is due to the pride that a top employee feels at having performed so well.

Also, such a tactic provides potential employees with the incentive to join a particular company. In this way, the prestige of being one of the few who get to drive a pink Cadillac triggers fascination in workers who want to attain that high status for themselves.

### 7. People are fascinated by power, whether it’s used to dominate or merely persuade others. 

For millennia, _power_ has triggered our fascination: philosophers spend time trying to define it, and governments spend money and energy finding ways to monopolize it.

But we don't have to think of power in such grand terms. Instead, we can use the word to refer to _any_ attempt to control someone else. With this, a dictatorship that limits its citizens' right to free assembly exists on the same spectrum as parents who limit their teenager's access to the family car.

It should thus not be surprising that companies are more than happy to use the power trigger to persuade you to purchase products.

Companies can actually make themselves appear powerful to fascinate potential customers.

For example, in one restaurant in Los Angeles, staff will sometimes yell at a customer for making the "wrong" choice when ordering — or even kick them out of the restaurant entirely!

While such an approach might seem strange, it actually works. Customers keep coming back for more because they're fascinated by the restaurant's sensibility and its unconventional use of power.

However, using power as a trigger for fascination doesn't necessarily mean _dominating_ others. There are other, more subtle ways to demonstrate power. For instance, you could control certain aspects of the environment, to ensure that the focus is solely on you.

Imagine you're a CEO, and you've noticed that employees aren't focused enough during meetings. How could you employ the power trigger to change this? One way would be to ensure that all attendees have comfortable chairs, so that they don't become restless. Another is to eliminate any noise outside the meeting room, to prevent any distractions while the meeting is underway.

As this shows, you don't need to use the power trigger at its extreme, as subtle changes too can have a big influence on your power to fascinate.

### 8. Vice is the trigger that makes us crave what is forbidden or frowned upon. 

In the 1920s, the United States government enacted Prohibition, making alcohol illegal to consume or sell. Was this move effective in dulling people's desire to drink alcohol, to make it less fascinating?

Certainly not! In fact, during Prohibition, the consumption of alcoholic beverages actually _increased_.

Prohibition is a great example of the _vice_ trigger, which illustrates that forbidden things fascinate us the most — a fact that successful companies know all too well.

Indeed, a great way to make people desire something is to forbid it, or add a forbidden aspect to it.

For example, imagine you're in a room with another person and a plain, black box. The other person says that she has to leave the room for a while, and as she exits, she tells you not to open the box.

Even though you've been told not to, odds are you'll give into temptation and open the box anyway, because you wonder what is it that you're not supposed to see.

Companies can use our desire for breaking the rules to their advantage.

Imagine that a certain headphone set has a "secret" volume setting, which enables you to increase the loudness of your music beyond an official set limit.

Despite that doing this would probably damage your hearing, you'd most likely find it hard to resist the temptation to crank the volume, as the idea of doing something forbidden is fascinating to you.

Another way to employ the vice trigger is to behave contrary to expectations.

In 1982, a contest was held in Vietnam to find the best design idea for a war memorial. At that time, war memorials in Vietnam were always completely white. The young student who won the contest designed instead a huge _black_ memorial. Because her entry was unconventional, it fascinated the judges.

So even though when you play with others' expectations you risk causing public outrage, it's definitely one way to get everybody talking about you, and stimulate their fascination with you or your company.

> _"Rules are not often fascinating, but bending them, very much so."_

### 9. Trust makes us appreciate reliable and consistent options. 

What's the secret to the global popularity of McDonald's, Burger King, Starbucks and so on?

In a word, _trust_ — the last of the seven triggers of fascination which we'll examine. Along with trust comes the comforting feeling of _familiarity_.

How do companies like McDonald's earn the trust of millions of customers all over the world? They earn that trust and feeling of familiarity through repetition and consistency.

Imagine you find yourself in an unfamiliar town, at night, after a long, tiring drive. You're famished but because you don't know where you are, you're unsure of which restaurant to choose.

Suddenly you notice the logo of a familiar fast food franchise, and you know immediately where you're going to eat.

In a study, researchers observed children as they ate chicken nuggets. The trick? One set of nuggets was wrapped in plain packaging while another was wrapped in McDonald's branded packaging.

The two sets of nuggets were identical, yet the children rated the ones wrapped in McDonald's packaging more highly. They believed that the nuggets tasted much better, simply because they trusted McDonald's food to taste good.

However, companies sometimes lose their customers' trust. If this happens, whatever the cause, you must ensure that you quickly get that trust back.

Establishing trust is something that takes time, but losing it can happen almost overnight. If this happens, then it is important to immediately find a solution.

Take the juice company Odwalla. Odwalla established trust with customers through the taste of their natural juices, by ensuring that the flavors of their juices weren't dulled by the pasteurization process.

However, the absence of pasteurization meant that the possibility of bacteria contaminating the juice was always there. And so a batch of juice was eventually contaminated, and a young girl died.

The company had a big problem. On the one hand, they had to ensure that such a tragedy would never happen again. But on the other, they couldn't compromise on the promise of unadulterated taste.

The company's solution?

Odwalla invented a new method of pasteurization that maintained the flavor of their juice while also offering increased safety for their customers, which ensured that the company regained customers' trust.

> _"The brands that we consistently trust are those that fulfill our expectations."_

### 10. Evaluate yourself or your company to discover your fascination potential. 

Now that you've learned about the seven triggers and how they can be used to improve a company's reputation, you're probably eager to try them out for yourself.

There are three steps necessary in the process of making your company more fascinating: Evaluation, Development and Execution.

Let's take a look at the first step — evaluation — which is a fundamental step toward assessing your current level of fascination.

Evaluation involves considering whether and how your customers talk with others about your company. Think about what distinguishes your company from other, similar ones. Are your customers given the opportunity to participate and connect with your brand, for example?

This last point is crucial, as customers' fascination with a company increases as they get to interact with it. Take, for example, a whisky company, which gave its customers the possibility of having their own personal barrel. They would then receive updates, photos and samples from the barrel, which fascinated the customers and gave them a sense of membership and belonging.

By surveying the successful strategies of other companies, you might be able to come up with a strategy of your own.

Evaluation also involves identifying the fascination trigger that your company activates the most.

As we've learned in previous blinks, we all have an innate ability to be fascinating; we're all already using the seven triggers.

But the key to success is using them in the right way. To ensure you achieve the results you want, you first have to find out which of the seven triggers is your primary trigger.

Let's take a look at Disney World Theme Parks. Entering Disney World gives people a familiar feeling; they know exactly what to expect from their upcoming experience. This suggests that the main trigger activated by Disney World is _trust_.

What's your primary trigger?

> _"Once you understand the triggers that drive your customers' behavior … you can direct this force of attraction."_

### 11. Find the perfect way to fascinate by getting creative with triggers and developing ideas. 

Now that you've discovered your current level of fascination, it's time to develop ideas that will make you even more fascinating.

This part of the process is a creative one, in that you are aiming to find the perfect combination of triggers to increase your fascination.

First, consider all seven triggers and brainstorm specific ideas on how to make them work for your company. Even though it may not seem that every trigger is appropriate for your company, you should evaluate them all to ensure that no opportunity is missed.

Then, consider what makes your company interesting to people. Is it the history behind it? Or perhaps its core values are what draw people to it?

Nike, for example, decided that a trivial fact about its company's history might fascinate potential customers: on its business cards is an image of a waffle iron.

Why? The image refers to an amusing story about the company's history, in which Nike's founder poured rubber into a waffle iron, thereby inventing Nike's signature racer sole design!

Nike's waffle iron image generates _mystique_, as people are always curious about stories and myths.

However, employing not just one but _several_ triggers will further increase your potential for fascination. For instance, it can be a great idea to support your main fascination trigger with another trigger.

Let's take another look at Disney World. As we've seen, its main trigger is _trust_. However, the allure of Disney World is also based on its _mystique_.

As we've seen in the Nike example, one of the foundations of mystique is story. There are so many stories associated with Disney that people will always use their imagination and be excited to explore new things when they visit Disney World.

### 12. To execute your ideas, you must convince others and establish specific goals. 

At this point, having discovered your most effective triggers and developed some great ideas for combining them, you now know exactly how to use your strengths to become as fascinating as possible.

There's just one final thing for you to do: bring those ideas to life!

First, convince others of their merit toward your common goal **.** It's essential that you get your team to support your ideas, as doing it alone is rarely as effective.

To ensure that your team members will be convinced by your concept, you must be as concrete as possible in your presentation. Don't hold back: demonstrate that you've considered your idea in depth and from many different angles. A great way to eliminate any doubt within your team is to create a project timeline, or an estimation of costs.

Another way to persuade your colleagues, your boss or your clients is to show them examples of similar concepts which have already successfully increased another company's fascination level. You've already seen plenty of examples in these blinks. You could even present studies that prove your ideas are reasonable.

Once you've gotten the support you need, you can begin to execute your idea.

But how will you know whether the idea is a success? Be sure to choose _specific_ goals, as these enable you to measure your progress effectively.

Establishing specific goals, instead of general ones, is the best way to check whether the fascination level of your company is increasing.

For example, rather than looking for an increase in general awareness, try looking for more specific, detailed mentions of your company on Twitter.

Setting yourself and your company smaller, more specific goals will enable you to adjust your concept whenever necessary.

### 13. Final summary 

The key message in this book:

**Each** **of** **us** **has** **an** **innate** **ability** **to** **be** **fascinating.** **By** **using** **the** **seven** **triggers** **of** **fascination,** **you** **or** **your** **company** **can** **fulfill** **your** **potential** **for** **fascination.**

Actionable advice:

**To** **trigger** **fascination** **for** **something,** **make** **it** **a** **vice.**

Forbidding people to do or have a particular thing is a surefire way to stimulate their desire to have exactly that thing. This is the vice trigger. So if you want to persuade an individual to do something for you, don't ask them directly. Instead, skirt around the subject at first, then ultimately reveal what it is that you need. Then _forbid_ them to do it! At the very least, they'll be tempted.

**Suggested** **further** **reading:** **_Hooked_** **by** **Nir** **Eyal**

_Hooked_ explains, through anecdotes and scientific studies, how and why we integrate certain products into our daily habits, and why creating such products is the Holy Grail for any consumer-related company. _Hooked_ gives tangible advice on how companies can make their products habit-forming, while simultaneously exploring the moral issues related to such products.
---

### Sally Hogshead

Sally Hogshead is one of the leading marketers and advertising writers in the United States and develops marketing strategies for leading companies such as Starbucks and Microsoft. Hogshead graduated from Duke University and founded her own advertising agency when she was just 27 years old.

