---
id: 57753b563588860003e823f7
slug: fashionable-nonsense-en
published_date: 2016-07-05T00:00:00.000+00:00
author: Alan Sokal and Jean Bricmont
title: Fashionable Nonsense
subtitle: Postmodern Intellectuals' Abuse of Science
main_color: 62B875
text_color: 396B44
---

# Fashionable Nonsense

_Postmodern Intellectuals' Abuse of Science_

**Alan Sokal and Jean Bricmont**

In _Fashionable Nonsense_ (1998), we dive into some of the problematic aspects of postmodernism, a fashionable intellectual trend in universities worldwide. Learn how inaccessible, complex language does not always translate into profound ideas, and discover how the popularity of postmodern nonsense can actually harm society.

---
### 1. What’s in it for me? See how an -ism can ruin science. 

Would you be surprised to hear that a scientific journal published a text without any empirical evidence or a scientifically sound hypothesis? Impossible, right? Yet this is exactly what Alan Sokal accomplished. He managed to get a nonsensical and completely unfactual paper published in a journal. What made it possible?

Postmodernism.

Postmodernism is an intellectual philosophy that sprang up in the late 1970s and has since been adopted widely — in the arts, social sciences and humanities (as well as other sciences). This philosophy throws overboard previous standards for scientific progress, claiming that there is no objective truth. Let's look at what postmodernism is and how it poses a real problem for science.

In these blinks, you'll discover

  * that postmodernism treats Native American creation myths as possible realities;

  * why one man's mumbo-jumbo seems like proper science to a postmodernist; and

  * how postmodernists use mathematical language to give their theories false credibility.

### 2. Postmodernism is a wide-ranging term that challenges the concept of objective truth. 

To understand what makes postmodernism such a controversial subject, let's first take a close look at the concept's defining features.

Postmodernism denies that there is any objective "truth" to be found in the world.

Postmodernists believe that everything, including science, is a social construct. According to their view, all traditional forms of knowledge contain limited and biased ideas about the way things are. This facet of postmodernism is often called _relativism_, because it suggests that everything we know is relative and based on the individual.

For example, several Native American tribes have origin myths that claim their people have lived in the Americas ever since their ancestors "emerged onto the surface of the earth from a subterranean world of spirits."

Some postmodernists argue that such beliefs are just as "valid" and "true" as the scientific evidence showing that the first humans entered the Americas by crossing the Bering Strait from Asia between 10,000 and 20,000 years ago.

In this way, postmodernists suggest that there is no objective reality and that everything is an individual interpretation of the world. So, from a postmodern perspective, the Native American creation myth is as valid as the scientific evidence. From a scientific and factual perspective, however, only one can be considered "true."

That said, postmodernism can balance out the negative effects of extreme _modernism_.

Modernism was an early twentieth-century movement designed to analyze every aspect of our existence in order to find out what was holding us back from "progress." Sometimes this caused us to over-romanticize technology or Western values.

So, when postmodernism arrived in the late twentieth century, it showed us that such idealistic views often lead to our diminishing the value systems of other cultures.

And making room for a broader and more diverse range of voices is, of course, beneficial to our culture. So, in the following blinks, we will limit our look at postmodernism to cases involving scientific discourse — where it can do the most damage.

### 3. The Sokal hoax was a parody and critique of postmodernism. 

You may have heard of the infamous _Sokal hoax_, in which physics professor Alan Sokal had an article published in the fashionable journal, _Social Text_.

While that may sound innocent enough, Sokal's paper was actually intended as a parody, written in the style of postmodernist academics.

The paper was entitled "Transgressing the Boundaries: Towards a Transformative Hermeneutics of Quantum Gravity," and immediately after its publication, Sokal revealed that it was full of postmodernist mumbo-jumbo that had no meaning whatsoever.

Sokal used an extreme form of relativism to make the illogical case that even the physical "reality" of our world is a "social and linguistic construct."

To provide evidence, Sokal used cryptically vague and obscure language in his text, such as: "the pi of Euclid and the G of Newton... are now perceived in their ineluctable historicity," or, "the putative observer becomes fatally de-centered."

If that sounds completely unintelligible to you, that was Sokal's point. His hoax illustrated exactly how nonsensical certain areas of postmodernism had become.

Yet, by using meaningless jargon to form grammatically correct sentences, and being sure to cite major names in the field of physics, Sokal succeeded in getting his paper published.

He used words like "hermeneutics," "privilege," "transgressive," and "Lacanian," which journalist Gary Kamiya calls the "not-so-secret passwords" of postmodernism.

Also, Sokal knew he could improve his chances of getting published by stroking the right egos and quoting from books and articles written by the editors of the journal.

Ultimately, the publication of Sokal's paper showed how editors can be easily seduced by fancy words and quotations from popular writers. Most critically, it proved that even postmodernist "experts" don't know what their peers in the field are talking about.

The Sokal hoax also triggered a heated debate in the academic world.

Notable thinkers like Richard Dawkins, Barbara Epstein (editor at _The_ _New York Review of Books_ ) and philosopher Thomas Nagel applauded the effort. They saw the paper as refreshing criticism of an intellectual trend that had devolved into meaninglessness wrapped up in pretty packaging.

> _"Sokal's piece uses all the right terms. It cites all the best people… And it is complete, unadulterated bullshit."_ \- Journalist Gary Kamiya

### 4. Postmodernists use science incorrectly, both intentionally and unintentionally. 

Fooling the editors of a science journal was a point in itself — but Sokal was also trying to make a larger point about how postmodernism misunderstands and misuses science.

As a professor, Sokal had seen how postmodernist scholars in the humanities and social sciences employed scientific concepts in irrelevant and incorrect ways.

For example, the French psychoanalyst Jacques Lacan claimed to use mathematical theory in his psychoanalysis. Once, he suggested that the penis is "equivalent to the square root of negative one of the signification."

You don't have to be a math expert, or a physics professor, to realize that this statement is complete gibberish.

Another example comes from psychoanalyst, linguist and feminist Luce Irigaray. She manipulates language to address the influence that cultural, ideological and sexual factors have on science.

Unfortunately, it's hard to take her seriously when she makes absurdly postmodernist suggestions, such as calling Einstein's equation between mass and energy (E=mc2) sexist since it "privileges the speed of light over other speeds that are vitally necessary to us."

This is another example of the postmodern tendency to misuse superficial knowledge of mathematics and physics to serve the postmodern agenda.

But perhaps the most famous star of postmodernism is philosopher Jean Baudrillard, who absurdly uses scientific terminology about chaos theory in an attempt to shed light on history.

Here's an example sentence: "Perhaps history itself has to be regarded as a chaotic formation, in which acceleration puts an end to linearity and the turbulence created by acceleration deflects history definitively from its end, just as such turbulence distances effects from their causes."

If it seems like mumbo-jumbo, that's because it is.

This deliberately confusing formulation suggests that one of postmodernism's intentions is simply to misuse scientific jargon so that certain ideas sound more profound than they really are.

> _"The advance of knowledge sooner or later undermines the traditional order. Confused thinking, on the other hand, leads nowhere in particular..."_ \- Stanislav Andreski

### 5. Postmodernism’s fashionable reputation has prevented clear-headed assessments. 

By now, you might be wondering how popular postmodernist thinkers like Jacques Lacan and Luce Irigaray have been getting away with spouting such balderdash for so long.

One possible reason is that people can sometimes confuse science with _scientism_.

Scientism is a dogmatic belief that empirical sciences provide us with the truest and most "objective" understanding of the world. But this belief comes _at the exclusion of all other viewpoints_.

Since problems in society are often complex ones that empirical sciences alone can't solve, social scientists have a distaste for scientism. And this has caused them to move away from sciences and the scientific method altogether.

For example, in 1968, the French reacted against the scientism to be found in Marxism, where Marxist supporters believed that their theories on society were complete and all-encompassing. The French rebellion against this scientism planted the seeds of postmodernism.

The same can be seen in former Communist countries during the 1990s, where people equated the dogmatic beliefs of repressive regimes with scientific thinking.

Given this kind of history, it's no wonder that postmodernism is often considered progressive in its repudiation of all-encompassing ideas while its critics are considered conservative-minded.

But when it comes to the understanding why postmodernist writing is so convoluted, the answer might be pretty straightforward: being incomprehensible sometimes has its advantages.

For instance, people are often eager to seem intelligent, and telling someone that you understand postmodernist theory can seem like a good shortcut.

Thus begins a vicious cycle of nonsense: postmodernism becomes "cool" and "profound" through being incomprehensible, which causes more people to write convoluted nonsense in an effort to cash in.

We've seen as well that the difficulty of such texts can even deter qualified people from questioning their validity.

After all, everyone can understand a synopsis of, say, _Romeo and Juliet_. But an explanation of the third rule of thermodynamics will probably go over most people's heads.

Sadly, it's not so difficult to get away with writing scientific nonsense.

### 6. Postmodernism is detrimental to critical thought, progressivism and the social sciences. 

The natural sciences have a pretty strict code of conduct, which has helped keep them safe from the abuses of postmodernism. The social sciences, on the other hand, are more tolerant and have inflicted great harm by accepting and advancing the trends of postmodernism.

Just think what would happen if we adopted into our daily life the aversion to science that postmodernism and extreme relativism have.

Our criminal justice system is a prime example of the importance that the scientific method plays in the functionality of our society.

We accept the fact that forensic evidence isn't foolproof and that our criminal justice system has flaws. Still, we feel confident that most investigations have results that correspond to reality. We also agree that judicial cases must be proven beyond a reasonable doubt and that we don't have to take into consideration _un_ reasonable doubt.

But since postmodernists argue that there is always room for doubt, we might as well abandon the search for any sort of truth.

Furthermore, methodical and empirical principles — such as replicating experiments and using controlled environments — add clear value to our lives.

Consider what the state of medical research would be without these methods. And these are the very scientific principles that are missing from postmodernist theory.

Furthermore, this abandonment of rational thought among postmodernists only adds to the anti-intellectualism that can be found everywhere in society.

Refusing to acknowledge the difference between fact and fiction, or between truth and fantasy, only strengthens the irrational arguments spread around by reactionary nationalists and religious fundamentalists.

If all knowledge is equally "valid," does that mean everyone should give full consideration to the next racist or sexist hate speech?

Granted, postmodernists are not deliberately supporting the rise of such irrationalism, but it is a very real consequence of their behavior.

This kind of extreme skepticism, and the use of convoluted language and confused thought, simply ignores the importance of having evidence and facts in our lives. Progress toward an improved world begins with clear and honest conversations — not jumbled and misleading discourse.

> _"So long as authority inspires awe, confusion and absurdity enhance conservative tendencies in society."_ \- Stanislav Andreski

### 7. Final summary 

The key message in this book:

**Postmodernism is an intellectual trend in universities all around the world. Essentially, it argues that all bodies of knowledge are socially constructed. This means that there are no objective truths. Some very influential and renowned postmodernist philosophers employ problematic tactics that use and abuse science, which ends up negatively impacting society.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How Mumbo-Jumbo Conquered the World_** **by Francis Wheen**

_How Mumbo-Jumbo Conquered the World_ (2004) takes a detailed look at irrational tendencies and how they have come to pervade and pervert the modern world. These blinks walk you through bogus philosophies, from neoliberal political and economic dogma that predominated in the 1980s to New Age gurus peddling hollow advice and false hope.
---

### Alan Sokal and Jean Bricmont

Alan Sokal is a physics professor at New York University and the author of _Beyond the Hoax_.

Jean Bricmont is a professor of theoretical physics at the Université de Louvain, in Belgium. He also co-wrote _Humanitarian Imperialism_, with Diana Johnstone _._

