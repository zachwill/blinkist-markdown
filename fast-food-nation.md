---
id: 534d480b6633610007000000
slug: fast-food-nation-en
published_date: 2014-04-15T05:46:09.000+00:00
author: Eric Schlosser
title: Fast Food Nation
subtitle: The Dark Side of the All-American Meal
main_color: FEDD33
text_color: 806F19
---

# Fast Food Nation

_The Dark Side of the All-American Meal_

**Eric Schlosser**

_Fast Food Nation_ shows how the fast food industry has massive consequences on many other aspects of our lives, including our education, health and working conditions. The book reveals the terrible methods and working conditions — caused in great part by the fast food industry's focus on profit — that are used to create our food.

---
### 1. What’s in it for me? Learn the dark secrets of what’s really hidden in your fast food. 

When we're not at home and hunger strikes, we're often tempted by cheap, fast food. We see the perfect burger in the ads, the free toys, the unbeatable prices; but behind the bright and shiny image, there's a dark side to the fast food industry.

The fast food industry has an influence on food production worldwide, affecting the food all of us eat — even if we don't eat fast food. The industry's drive for profit reduces meat quality, increases the chances of eating contaminated meat, and oppresses and exploits the weakest members of society.

In these blinks, you'll find out about the devastating consequences the fast food industry has had on towns, workers and health.

You'll learn to not trust your taste buds when you realize what actually goes into a burger.

You'll learn about the fast food industry's business tactics that disempower workers and spread poverty and crime.

Finally, you'll discover how fast food chains have altered how most of our food is produced today — for the worse.

But watch out: after reading these blinks, you may not ever want to eat your favorite burger again!

### 2. American fast food owes its success to the McDonald brothers’ revolutionary adoption of factory production principles. 

Like it or not, practically everyone has eaten at McDonald's at some point or another in their lives. The company's golden arches are the iconic symbol of fast food and American culture around the world. But what made McDonald's and other fast food chains so successful in the first place?

First off, fast food was originally served by waitresses on roller-skates in Southern Californian drive-ins. In Southern California in the 1950s, drive-in restaurants, movies and even churches had become increasingly popular due to the availability of cars and the development of suburbs.

In this setting, the mix of cheap food, cars and pretty waitresses soon turned fast food restaurants into popular hangouts for teenagers.

Then the McDonald brothers came along — and revolutionized the fast food business.

Their main focus was on efficiency and speed: they served only a few simple meals that could be easily eaten without cutlery, packaged their food and drinks in simple, paper packaging, and stopped serving people in cars.

But, most importantly, they saved time and money by adopting the principles of a _factory_ _assembly_ _line_ : each employee was assigned one easy-to-learn task — like flipping burgers or dressing salads — which reduced costs and optimized speed.

This new speed and affordability started to attract different kinds of customers. McDonald's moved beyond a teenage hangout to a place where families could finally afford to take their kids to a restaurant.

These developments made the so-called "Speedee Service System" incredibly successful: between 1960 and 1973, the number of McDonald's restaurants grew from 250 to 3,000, and was soon imitated by many other fast food chains.

Today's major chains, like Burger King, Wendy's or Kentucky Fried Chicken, all owe their success to quickly catching on to the McDonald's model.

The success of the assembly line style of food production has radically transformed the way we work, eat, and live — in America and all over the world.

### 3. Fast food chains target children and teenagers as potential customers – even in schools. 

When we think of selling something, we probably think of appealing to the people who earn money, i.e., adults. But the fast food industry was one of the first to discover a much more responsive audience: children.

Children have proven to be the perfect customers because they can pressure their parents into buying things. Since the 1980s, parents have tended to spend less time with their kids while spending more money on them to compensate. Simply put, by getting children to want a product, companies can get parents to buy it.

Kids are also more likely to believe the promises made in commercials, so they're easy to convince. And Fast food chains and other corporations exploit this vulnerability by creating products and commercials that target kids.

For example, McDonald's attracts kids by building playgrounds at their restaurants and bundling free toys with their meals — the so-called "Happy Meals".

Obviously an effective approach: one study showed that if a popular toy is given away with a meal, it can double or triple the sale of that meal in a week.

As a result of these efforts, 90 percent of American children between three and nine visit a McDonald's every month.

But catering ads to children doesn't just happen on TV, at the movies or on billboards. Since the end of the 1990s, ads have penetrated America's schools.

Due to reduced public funding, many school districts could only finance themselves through contracts with soda companies, fast food chains or other big corporations.

For example, at least twenty school districts in the U.S. have their own Subway franchises today, while 1,500 districts have delivery contracts with the same brand.

And because many schools use corporate-sponsored school materials, even the content of textbooks has been infiltrated by corporations.

For example, a study guide sponsored by the American Coal Foundation stated that the carbon dioxide released by the coal industry — which is known to promote the greenhouse effect — is not harmful, but beneficial, to the planet!

### 4. The fast food industry uses employment policies that exploit the weak. 

Would you like to work at a fast food restaurant? You get free food and a friendly atmosphere, so it'd be a good deal, right? Think again.

Fast food industry jobs are rife with problems, and many of them stem from their assembly line method. Because assembly line work comprises only simple individual tasks, fast food workers don't require or receive intense training.

This makes them cheap, interchangeable labor, which leads to a high turnover rate: on average, a fast food worker is fired or quits every three to four months.

What's more, the fast food industry hires mainly the weakest members of society — e.g., teens, migrants and the poor — whose suffering is exacerbated by the poor working conditions.

For example, about two-thirds of U.S. fast food workers are under twenty because teens work for lower pay and insist less on their employee rights than more experienced workers.

And because fast food chains often disregard labor laws, many teens work so much it harms their well-being.

Studies show that while up to twenty hours a week of work can be positive for teens, any longer is harmful because it distracts them from their education and social life.

Another danger of working in the fast food industry is the extremely high risk of being a victim of crime, as fast food restaurants are frequent targets for robberies. What's more, two-thirds of the robberies are committed by former or current employees whose poor working conditions and low wages drive them to commit crimes, and they often choose the familiar restaurant as their first target.

Perhaps the most disturbing part of this is that the fast food industry does everything it can to maintain these conditions.

For example, restaurants constantly develop machines that are easier and easier to handle, which additionally reduces the amount of training employees need.

The industry also employs harsh anti-union methods, including mass firings or closing whole restaurants when workers try to form a union.

The result? Not a single fast food worker in North America is represented by a union.

### 5. The franchise business model benefits corporations more than franchise owners. 

Many people dream of opening their own business, but are afraid of failing.

The _franchise_ model allows individuals to purchase a license to open up their own restaurant as part of an already existing chain, and promises the mix of independence and security that has helped fast food chains spread all over the world.

What's the catch?

Franchises are typically portrayed as a shared risk — the company delivers the brand and the business model, and the _franchisee_ delivers the initial capital.

But, in reality, franchisees bear much more risk than the corporations: they have to invest a lot of money just to get started, and are forced to follow the corporation's punishing rules.

For example, a McDonald's franchisee is unable to stop McDonald's from allowing another franchise to be opened just around the corner. One more restaurant increases the corporation's profits while the increased competition eats up the franchisees' profits.

Although some studies have claimed that most franchises are successful, they omit franchises that went bankrupt. When these are included, experts find a very different result — franchises are actually more likely to go bankrupt than regular individual enterprises!

On top of running less risk than the franchises, the fast food corporations also enjoy better protection by the law. Although franchise owners have to invest their own capital like a business owner, they have to obey the fast food corporation's directives like an employee.

But they are neither covered by the laws protecting employees, nor those protecting independent businessmen.

For example, despite having to purchase their own supplies, they are not covered by consumer protection laws. And they take orders from the corporate headquarters without being covered by employment laws.

It seems, then, that franchises are no guarantee for success, instead providing yet another advantage for the fast food corporations.

### 6. When you eat fast food, what you taste is not what you get. 

Have you ever eaten strawberry yogurt? If so, how many strawberries do you think it contained? Chances are not a single one. Because nowadays it's less about the real deal and more about the flavor.

That's because the flavor of a product often determines its market success — or failure.

Originally, our ability to taste different flavors helped us distinguish whether a food was edible or harmful.

And today, our desire for the right flavor still drives the food choices we make. This tendency has led corporations to massively invest in a whole new industry: artificial flavoring.

And artificial flavoring can now be found in most of the food consumed in the U.S. today.

Processed foods, such as canned goods, frozen products and microwave meals, make up 90 percent of all food expenditures made by Americans today. And since food loses most of its original flavor during the canning and freezing procedures, flavor needs to be added artificially.

The fast food industry also uses a lot of artificial flavoring because they know that flavor drives their customers' food choices.

For example, McDonald's has admitted that the flavor of their fries derives in part from a flavoring they call "animal products," while the chicken sandwich from Wendy's paradoxically contains beef extract.

These added flavors are commonly differentiated as either "natural" or "artificial" flavors. But they actually consist of the same components, differing only in the way they're produced. And even then, both are usually produced in chemistry labs.

Therefore, natural flavors aren't necessarily healthier or more natural than artificial ones. For example, almond flavoring "naturally" produced from sources like peach or apricot pits can actually contain traces of the deadly poison hydrogen cyanide!

So next time you're eating a Big Mac, remember that what you're tasting has more to do with a test tube than anything else. Bon Appétit!

### 7. The fast food industry has had devastating effects on the lives of U.S. farmers. 

When we think about the U.S., we imagine the "all-American" farmers working on vast fields in the Midwest. But the reality of today's farmers is very different.

For example, today only a few giant suppliers compete to dominate the potato, poultry and beef markets.

This trend is driven by mega-buyers like McDonald's, the nation's biggest purchaser of beef. McDonald's used to buy beef from 175 local suppliers, but, in order to make sure every burger is the same everywhere, it cut that number down to five suppliers — which has lead to larger monopolies.

Now try some more numbers on for size: today, four meatpacking firms slaughter 84 percent of the country's cattle, eight chicken processors control two-thirds of its market and three companies control the whole American market for frozen french fries.

And it's these monopolies that make farmers more dependent on their buyers.

For example, now that chicken is a food processed in massive quantities, the processing companies have gained greater influence over chicken farmers.

The result: chicken farmers are usually in contracts where they contribute their labor, land and equipment, but don't own a single chicken. They belong to the companies — and that gives the companies leverage: if a farmer protests against any conditions or prices, the company can take away all the chickens, terminate the contract and leave the farmer stuck with debt.

This situation is a result of farmers having fewer buyers, which gives them no alternative except to accept the prices offered.

And, as a result, farmers often earn so little that they go out of business. For example, when one portion of french fries sells for $1.50 at a fast food restaurant, the potato farmer earns two cents.

These miniscule dividends mean that farmers have to work more and more to earn a living. And when they don't earn enough, many are forced to sell their property, which is often bought up by the big companies who end up hiring the same farmers to work as employees on the land they just sold!

### 8. The meatpacking industry has ravaged American towns. 

Have you ever wondered how a burger can cost only $1? One major reason is the treatment of cattle and poultry. But there are also human beings who suffer the consequences every day: the meatpacking workers.

The roots of their suffering trace back to the 1960s when the meatpacking industry adopted the assembly line style of production.

Just like in the fast food industry, assembly line production reduced the need for trained workers, and the employers started hiring cheap labor, reducing wages and fighting against unionization.

Since employers only pay health insurance and paid holidays to workers who have been there for at least half a year and most workers leave before that, employers can also save money.

In addition, because they only need unqualified, cheap labor, meatpacking companies often employ illegal migrants, homeless people or refugees.

For example, one-quarter of all meatpacking workers in Nebraska and Iowa are illegal immigrants. They usually work for lower pay and, like fast food employees, are less likely to form unions.

But not only the workers are affected by these practices: so are the towns and cities where these meatpacking industries are located.

Meatpacking used to take place in urban areas like New York or Chicago. But to get away from the grip of unions, meatpacking companies started moving to smaller towns in rural areas. And wherever it went, the meatpacking industry brought its ravaging influence.

Poverty and criminality usually increases in such towns due to the rapid influx of poor and uneducated people attracted to the jobs.

One such case occurred in 1990, when a big company opened a slaughterhouse in Lexington, Nebraska, and within ten years, the rate of serious crimes had doubled, as did the cases where state-subsidized medical care was needed. Gangs started controlling the streets and the town became a center for illegal drugs.

Just like the fast food industry it supplies, the meatpacking industry cares more about cheap and efficient production than human well-being.

### 9. The never-ending hunt for profits makes working in the meatpacking industry especially dangerous. 

Can you imagine working in a slaughterhouse? It's not only a dirty, hard and poorly paid job — it's also extremely dangerous.

In fact, a slaughterhouse is the most dangerous place to work in the U.S., with an injury rate three times higher than in a normal American factory.

This is due to the varying size and weight of the cattle, which means production cannot be fully automatized, and much has to be done by hand. As the most important tools for this work are knives, stabs and cuts are common injuries. And the fact that untrained workers do the job makes the risk of injury even higher.

The fast pace of the assembly line further reinforces these dangers.

Because of high competition and small profit margins, the meatpacking companies rely on fast production to make a profit. That means they're always looking for ways to speed up the work, which increases the workers' risk of injury.

Workers commonly abuse drugs like methamphetamine to keep up. And because it makes them less careful and less in control, it increases the risk of injury even more.

The meatpacking industry continues abusing their workers even when they're injured. For example, many workers are pressured to either not report their injuries or to come back to work before they've recovered — or they might lose their jobs.

And the compensation employers have to pay to an injured worker is minimal: a lost finger costs between $2,200 and $4,500.

But for the meatpacking industry, less money spent on safety measures and faster production lines are appealing enough to pay for a lost finger once in a while.

### 10. Fast food production is largely responsible for the increase of foodborne illness in America. 

Most people know that fast food is unhealthy. But while we normally think obesity is the worst health consequence, something much worse, and much more dangerous, is happening behind the scenes.

Deadly bacteria, like E. coli 0157:H7, proliferate in the world of cheap, quick beef production.

E. coli 0157:H7 is a mutated bacteria that can release a deadly toxin, and infected people can suffer from stomach cramps, bloody diarrhea — or even die.

When beef gets contaminated by coming into contact with the cattle feces, it can transmit the bacteria. But that couldn't ever happen, right?

In many slaughterhouses, meat is regularly brought into contact with cattle feces due to a combination of unsanitary working conditions, the assembly line's speed and unskilled workers.

Another source of disease is cattle feed itself. Even if it's prohibited today to feed cows dead dogs and cats from animal shelters (like companies used to), cows, which should be eating grass, are still being fed dead horses, pigs and even poultry feces. This foul food causes cattle to catch dangerous bacteria and parasites.

The centralization of meat production is yet another factor that has amplified the problem of foodborne diseases.

Until recently, food poisoning would occur because of isolated production mistakes, like bad storage, and would only affect a few people, e.g., at a company picnic.

Nowadays, the meat produced by a few giant corporations reaches supermarkets and fast food restaurants everywhere, so one contaminated batch can affect millions of people.

And so, around 200,000 people in the U.S. get sick from food poisoning every day.

This increase in foodborne illness is a direct consequence of the fast food industry's demand for massive amounts of cheap meat.

And, despite rising food standards in the fast food industry (mostly due to lawsuits in the 1990s), meat quality eaten in households still remains poor throughout the U.S.

### 11. The fast food industry has conquered the world – literally. 

When we take a vacation abroad, we never know what to expect: strange languages, unknown places, wondrous adventures. But one thing is pretty sure: at one point or another, we'll see a McDonald's.

But how did the fast food industry gain such widespread influence?

By being the first multinational industry to enter a country when it opens its markets to international investors.

For example, when Turkey opened its economy in the 1980s, McDonald's was the first foreign corporation to open franchises.

This first-mover strategy is why fast food restaurants have become a symbol for Americanization and Western-style capitalism, in both a positive and negative sense.

Fast food companies also influence other nations by importing new agricultural techniques.

They do this to diminish the fear of "American imperialists" who invade food markets with imported food. Instead, they import their own agricultural methods and produce supplies locally.

For example, before McDonald's launched its first restaurants in India, it taught local farmers how to farm lettuce — an unnatural crop in most of India — and gave them lettuce seeds specially engineered for the Indian climate.

These expansive business methods have led to the global export of fast food culture — and wherever fast food goes, its negative consequences follow.

For example, fast food has made the U.S. population the most obese in the world, with more than half of all American adults and one-quarter of American children being obese or overweight.

But, with the globalization of fast food, other countries have started to face similar problems.

Between 1973 and 1993 in Great Britain, for example, the number of fast food restaurants doubled. And, in that same time period, the adult obesity rate doubled as well!

All these negative consequences have led fast food chains like McDonald's to face worldwide protests by environmentalists and animal rights activists, as well as politically motivated attacks aimed at the symbol of America.

### 12. Final Summary 

The key message in this book:

**Fast, cheap food comes at a price: exploitative working practices, obesity epidemics and decreased meat quality. By cutting every corner possible to make the maximum profit, the fast food industry does damage to people's jobs, health and education worldwide.**

Actionable advice:

**Avoid processed foods.**

Processed food usually contains flavoring agents that have origins you can't — and in most cases don't want to — trace. So if you'd like to know what exactly it is you're eating, avoid any and all processed food.

**Resist the temptation of fast food advertising.**

If you see ads for fast food, just think about how these seemingly delicious products are really made — and you won't want to eat them anymore.
---

### Eric Schlosser

Eric Schlosser is an American investigative journalist. He is a contributor to _The_ _Atlantic_ and has received several prizes for his writings including the National Magazine Award. His other books include _Reefer Madness_ and _Chew On This_.

