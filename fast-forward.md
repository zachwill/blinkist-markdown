---
id: 583aeb2ed4507000044fa581
slug: fast-forward-en
published_date: 2016-11-28T00:00:00.000+00:00
author: Melanne Verveer and Kim K. Azzarelli
title: Fast Forward
subtitle: How Women Can Achieve Power and Purpose
main_color: F03049
text_color: B22436
---

# Fast Forward

_How Women Can Achieve Power and Purpose_

**Melanne Verveer and Kim K. Azzarelli**

_Fast Forward_ (2015) is about one of the most underappreciated resources on earth: women. These blinks, which argue that women will be the key to this century's greatest achievements, outline many of the myriad problems that women face, as well as the steps we can take to solve them.

---
### 1. What’s in it for me? Get inspired to end gender inequality! 

Over the course of the last century, women have gained considerable economic and political power; they've inched closer to achieving equal rights and started conquering the workplace. Indeed, women today are a force to be reckoned with, and powerful women in all spheres of public life use their influence to push women's rights even further. In other words, it seems like we're approaching a truly equal society, right?

Unfortunately, that's not really the case. Globally, women must struggle for the rights and recognition that men take for granted — such basics as respect, freedom and independence. But the situation isn't hopeless. As more and more women occupy positions of economic power, and as they gain recognition as a demographic brimming with economic potential, we may soon witness the fast-forward change that will put women where they should be: on an equal footing with everyone else.

In these blinks, you'll find out

  * that having three or more female board members increases returns by 50 percent;

  * that a mere 4.4 percent of all US business loans went to female entrepreneurs; and

  * how Grameen Bank has helped women increase their independence.

### 2. Women are the driving force of change in the modern world. 

Just a few decades ago, it wasn't unusual to hear statements like, "Women shouldn't vote or drive cars and God knows they don't belong in politics." Disturbingly, such sentiments still predominate in some countries. The world is changing, however: women are gaining more power and having a greater impact on society.

In recent years there have been more female leaders, such as German chancellor Angela Merkel, American Presidential Nominee Hillary Clinton and Christine Lagarde, head of the IMF. Those women fought hard to make it in a male-dominated world and they're leveling the playing field with their success.

Women are also making strides in the world of business. In 2014, the United States had around nine million women-owned businesses that employed over 7.9 million workers and made over 1.4 trillion dollars in revenue. If, for instance, Egypt's workforce contained as many women as men, the country's GDP would rise by about 34 percent.

In fact, women are likely to be the force that drives this century's global development. About a billion women are ready to enter the global economy — a number comparable to the population of China or India. These women will start new businesses, change the political landscape and increase the quality of life for both their families and their communities.

In the future we'll also have more female entrepreneurs and business leaders, which will change the business environment. This is already happening and having a positive impact: studies show that Fortune 500 companies with three or more female board members have a 50 percent higher equity return than those all-male boards.

We're on the brink of a major shift in global power. Women are truly poised to change the world.

> Women own or lead over a quarter of private businesses worldwide and are in charge of 20 trillion dollars in customer spending each year.

### 3. Women are still oppressed in most countries. 

Women may have massive global potential, but, in most countries, they're still barred from opportunity and considered inferior. This mistreatment and subjugation comes in many forms.

Sexual violence, for instance, is still one of the foremost dangers faced by women. Assault, rape, human trafficking, female genital mutilation, child marriage and sex slavery are more widespread than most would think — even in Western countries.

And in some countries, such as Afghanistan, women can be raped as a form of punishment — sometimes for things their male family members have done, and sometimes by their male family members. Women are even sometimes killed to maintain their family honor. Even in safer countries like the United States, a man can stalk or assault a woman and face virtually no consequences. In fact, over 30 percent of American women have suffered some form of violence — such as assault, rape or stalking — at the hands of males with whom they were intimate.

Women themselves play the main role in fighting such malicious social customs and phallocentric double standards. Though facing horrendous odds, women across the globe are standing up and striving for a world without gendered violence.

Female leaders have succeeded in criminalizing sexual violence with motions like the US Violence Against Women Act of 1994. Other organizations, such as Girls Not Brides, which aims to empower young girls and women, are fighting to end the culture of child marriage.

The continent with the highest incidence of child brides is Africa. According to the United Nations Population Fund, about 75 percent of all women in Niger were married before their eighteenth birthday.

But despite the fantastic work of such organizations, and the major progress toward global gender equality they've made possible, there is still a lot of work to be done.

> _"Rape continues to be a heinous weapon of war, wielded with virtual impunity."_

### 4. Companies benefit from employing more women. 

Most people subconsciously picture a man when they hear the term "CEO." That's not only because of how we're socialized; it reflects reality, too. Most CEOs _are_ men. There are more than a few reasons this needs to change.

Companies actually become stronger and more profitable when they place women in executive positions, a fact that few companies take advantage of.

Few Fortune 500 companies have women in high executive positions. For women, there's a so-called _glass ceiling_, an invisible and unofficial barrier that stands between them and a company's top positions. In the United States, only five percent of CEOs and 3.1 percent of board chairs are women.

Companies that resist this trend reap the rewards, however. Studies have shown that Fortune 500 companies with strong female senior partners are between 18 to 69 percent more profitable than other Fortune 500 companies in the same industry.

Women should also have more representation in middle management, where they could foster greater innovation and inspiration.

That's because new and innovative projects usually start in middle management. Middle management employees are the ones who interact most directly with the customers and thus get most attuned to their wishes. They also maintain contact with employees at higher levels, serving as a bridge between the customers and the executive branch.

That means women in middle management are in a unique position to promote more gender-oriented strategies across the entire company. They're the ones who know how to connect with female customers and represent their ideas to those at the top.

Bea Perez, the chief sustainability officer at Coca-Cola, believes wholeheartedly in this strategy. She argues that groundbreaking ideas come from all levels of the company — not just the upper management.

### 5. Women across all levels of society need to be empowered – not only those at the top! 

We certainly need more women in leadership positions; however, that doesn't mean neglecting those at the base of society. Women at lower levels of the workforce — or those not in the workforce at all — are just as important to global development.

There are millions of impoverished female farmers in rural areas and millions of women toiling away in garment factories for virtually no pay. Their lives and well-being are constantly under threat; many can't even read. In fact, women account for around 70 percent of the global population that lives in poverty.

Women need financial support and job training to break this cycle. Many people have come up with potential solutions. One such solution, _microcredits_, aims to give women economic autonomy by granting them small loans.

The Grameen Bank in India offers microcredits as low as $27 for women who want to start small businesses. They've experienced great success: the Grameen Bank's return rates are through the roof and participating women report that the microcredits massively have improved their quality of life as well as the living standards of their communities. The bank has distributed over 16 million dollars in microcredits to female entrepreneurs.

Companies and other organizations are also starting to recognize the power of their female clientele base. Avon, the world-famous beauty company, has started training women sellers, known as the Avon Ladies, to do more than sell products: they also learn to spread information about health, domestic violence and other important issues to women in rural areas.

In Brazil, for instance, the Avon Ladies went to areas with limited health-care services and taught women how to examine themselves for breast cancer.

### 6. Female empowerment has a number of positive effects on society. 

Female empowerment isn't only good for women. As more and more women gain power in society, something important and marvelous happens: _the_ _multiplier effect_.

The multiplier effect says that when you empower one woman, you empower many. Since, until recently, so many women have had so few chances, the empowerment of one woman triggers a chain reaction. Female leaders are more likely to help other women gain access to education and financial security; this, in turn, will create more female leaders, who will then empower more women.

Helena Morrissey, the CEO of Newton Investment Management, is one such powerful leader. She founded the 30% Club, which aims to raise to 30 percent the number of female board members in FTSE-100 companies, the companies on the London Stock Exchange with the highest market capitalization.

Female empowerment also means that women will have more capital to spend on those around them. When a woman in a poor, rural area is able to earn more money, she's likely to invest it in her children's future by providing them with a better education and health care than she herself had access to. And this has a positive impact on the economy at large.

Unfortunately, in some parts of the world, starting a business or earning credit are privileges denied to women. But there are programs aimed at combating this problem.

The Gates Foundation and Coca-Cola, for instance, started a joint project called Project Nurture. Project Nurture provided 53,000 farmers in Uganda and Kenya with basic training in business and mango production. In just four years, the project helped some women increase their income by over 140 percent.

### 7. Women can develop powerful new enterprises when they overcome obstacles in the business world. 

One might think that it's only women in poor countries who truly struggle. However, women in developed countries also face serious obstacles in the business world.

Many female entrepreneurs have difficulty securing sufficient credit, for example, which is a vital part of founding a business. Many assume that banks treat men and women equally in Western countries; that, however, is a patently misguided assumption.

Denied the credit so liberally granted to their male counterparts, female entrepreneurs often struggle to make ends meet. In fact, only 4.4 percent of all business loans in the United States go to female business owners. In other words, they granted one dollar for every 23 dollars loaned to male business owners.

Fortunately this gap is starting to close, largely because female leaders are working to provide their fellow women with more capital. Belle Capital, for example, is a women-led venture capitalist group that focuses on funding new businesses with female leaders or owners. In order to qualify for Belle Capital, businesses need at least one female chief officer or a clear commitment to recruiting more women for senior positions.

Women can truly build and inspire life-changing new enterprises when given the chance. When young, educated women start new businesses, they're often more socially responsible than male entrepreneurs. Some develop female-oriented programs, like opportunities for young mothers-to-be, while others spearhead initiatives aimed at protecting the environment. Still others try to find ethical solutions to global problems.

Fresh Paper, founded by Kavita Shukla, is an example of this. Fresh Paper is an environmentally friendly way of keeping food cool. NGOs often order it in regions that lack electricity to prevent food spoilage and illness.

### 8. Women need leeway to find their purpose and build a network of like-minded people. 

Having a sense of purpose in life is just as important as finding happiness. Everyone wants to do something meaningful that will make the world a slightly better place.

Naturally, this applies equally to women and men! Women also need space to figure out what they want to do with their lives, whether that means becoming a successful leader, a politician or a stay-at-home mom. People who live without a sense of purpose never truly feel fulfilled; they might while away their life doing something they don't truly care about.

Pam Seagle, a former senior marketing executive for Bank of America, found her purpose after a rather arduous journey. After miraculously surviving a plane crash, she radically shifted the direction of her life. Instead of focusing on her own career, she committed herself to helping other women further theirs.

Today, Seagle uses her knowledge of the banking sector to mentor young female leaders who want to make it in the business world.

Once you find a purpose, you'll naturally find like-minded people. Seagle didn't launch her mentoring project at Bank of America on her own. She networked and held numerous meetings with other feminist activists until she had enough support to get her project up and running.

Seagle collaborated with Anne Finucane, the global chief strategy and marketing officer, and her deputy, Rena DeSisto. Today, their Global Ambassadors Program helps women all over the world, in countries like Haiti, South Africa and India.

Once you know what it is you're working toward, you'll naturally find a network of like-minded individuals. Building a strong network is a key part of achieving any meaningful goal.

> _"One way to live life with purpose is to try and infuse one's work with meaning."_

### 9. The three keys to ending gender inequality are education, technology and media. 

A young girl from a remote village in Kenya will have few opportunities to learn about the global fight for gender equality. She's more likely to be married off at a young age, undergo female circumcision and then have children before she's mature enough to care for them.

Such are the obstacles to global gender equality. However, we have to start somewhere, and education is key. Education enables girls to understand their own rights and power, and equips them to empower those around them.

Education has its risks, however. Malala Yousafzai was shot in the head by the Taliban for fighting for female education rights in Pakistan. She miraculously survived and now fights even harder for women's rights in education. For her efforts, she was awarded the Nobel Peace Prize in 2014.

After education, the next step is greater access to technology. Technology becomes more important every day in our modern world and it can enable women to educate themselves, enter the global economy and bring about meaningful change for their families and communities.

The project Girls Who Code, for example, offers science and IT classes to young women; the aim is to afford them greater access to future technologies and industries. Girls Who Code's goal is to reach over one million young women by 2020.

Women should have greater representation in the media, too. Influential media players have made great strides in this area, but they still need to step up their game.

Actors like Meryl Streep and Angelina Jolie, for instance, are using their power to advocate for more gender equality in the arts. They hope to inspire more women to stand up for themselves by pushing for more books, plays and films depicting the struggles of women.

All in all, global feminism has made amazing progress, particularly in the last few decades. Nonetheless, there's still much to be done before we achieve a truly equal world.

### 10. Final summary 

The key message in this book:

**Women, with their potential to fuel economic and social growth, are sure to be the most powerful force of the century. But despite this power, many women are still held back by widespread sexism and social dogmas — even in Western countries. Women face challenges all across the globe, from sex trafficking to assault to glass ceilings. Fortunately, we can overcome these problems with education and productive uses of technology and media. Women's rights have come a long way, but still have a ways to go.**

Actionable advice:

**Women! Have confidence in your own strengths and skills.**

You are capable of more than you realize. Do you want to start a business? An education initiative? Another kind of project? Feeling scared? Don't be. The only way to know if you can succeed in those endeavors is to try. You never know — you might end up changing the world.

**Suggested further reading:** ** _The Politics of Promotion_** **by Bonnie Marcus**

_The Politics of Promotion_ (2015) offers insights into the ways women can prime themselves for promotion in any line of work. Filled with actionable tips and strategic career advice, it provides the political savvy you need to maneuver within the workplace and secure your next promotion.
---

### Melanne Verveer and Kim K. Azzarelli

In 2009, Melanne Verveer was appointed the very first US Ambassador-At-Large for Global Women's Issues. A co-founder of Seneca Women, she's the executive director of Georgetown University's Institute for Women, Peace and Security.

Kim K. Azzarelli is also a co-founder of Seneca Women and chair of Cornell Law School's Avon Global Center for Women and Justice. She's held senior positions at Goldman Sachs, Avon, _Newsweek_ and _The Daily Beast_.

