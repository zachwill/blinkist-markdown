---
id: 55062345666365000a160000
slug: fault-lines-en
published_date: 2015-03-16T00:00:00.000+00:00
author: Raghuram G. Rajan
title: Fault Lines
subtitle: How Hidden Fractures Still Threaten The World Economy
main_color: 29BFCE
text_color: 1A7982
---

# Fault Lines

_How Hidden Fractures Still Threaten The World Economy_

**Raghuram G. Rajan**

In _Fault Lines_, author Raghuram Rajan unveils the global economy's hidden fractures that led to the 2008 financial crisis. These blinks show that greedy bankers weren't the only ones to blame; our economic system had deep systemic flaws as well. Importantly, they outline what we can do as a society to prevent similar crises in the future.

---
### 1. What’s in it for me? Learn the real reasons that caused the global financial crisis of 2008. 

Even though years have past, many of us are still feeling the effects of the financial crisis that rocked the world in 2008. There are still unanswered questions about the crash, and prominent among them is how exactly the crisis happened in the first place.

While it's easy to point fingers at bankers or mortgage lenders, the "blame" for the crisis goes deeper still.

Try to think of the crisis as an earthquake: what makes the earth tremble and buildings fall has little to do with individuals, but something bigger, something deep beneath the earth's surface.

As these blinks explain, _fault lines_ — deep, systemic flaws, not unlike the rifts that cause the earth to tremble — were rife within not only the U.S. economy but also the world economy. At the peak of the crisis, these fault lines gave way; yet they had been lurking underneath the surface for some time.

You'll learn what exactly caused these fault lines to appear and become more dangerous as time went on, through the lens of U.S. unemployment, global economic decisions and much more.

In the following blinks, you'll discover

  * how German and Japanese auto manufacturing contributed to the crisis;

  * how to tweak bonuses so bankers are encouraged to tone down risky behavior; and

  * why the world was blind to the financial crisis growing right before its eyes.

### 2. The advent of cheap loans was one fault line of the crisis, and banks and politicians were complicit. 

One of the most significant fault lines that led to the global financial crisis was growing income inequality in the United States.

In the years leading up to the crisis, the wage gap grew. While the average income of top earners increased, the average median income stayed nearly the same. For example, in 1997, the median household income was $51,704, yet by 2009, it had barely budged to $52,196.

Why the growing gap? Basically, the country's workforce couldn't meet the labor demands of the market. America needed more and more highly educated people to work in the growing technological sector, but schools weren't producing enough qualified candidates.

This drove the top income level up while the median stagnated. It also explains why the income gap was strongly related to education level: In 2008, the median income of a high school graduate was $28 per hour, while the median for a college undergraduate was $48, some 72 percent more.

Every worker is a potential voter, of course; so U.S. politicians sought to do something about the problem. Seeing that their electorate needed more money, they encouraged cheap loans.

With the support of politicians, banks began expanding easy credit, especially to low-income households. This so-called s _ubprime_ lending, which we'll look at more closely later __ became popular, while interest rates were lowered.

The immediate positive effects were straightforward: more spending meant more growth. Yet the growth was fueled by debt. People were essentially just postponing paying their bills.

And as you'll see in the following blinks, this American spending spree didn't just affect the United States but also the rest of the world.

### 3. Exporting countries tried to shove their surpluses at the U.S., but the U.S. could only absorb so much. 

You're probably well aware that your cell phone was made in China, your car in Japan and your shirt in India. Yet did you know that global manufacturing was another fault line in the 2008 financial crisis?

In the years before 2008, there existed a huge imbalance in the number of _import_ and _export nations_, meaning countries that produce more or less than they consume, exporting or importing the difference. The problem at the time was that there were too many exporters.

This situation began after World War II. The economies of Germany and Japan were in tatters, their populations impoverished and infrastructure destroyed. To recover economically, these countries decided to focus on production, becoming exporters and selling goods to richer countries, like the United States. As exports grew, the countries profited and became wealthier.

Germany and Japan set an example for other developing nations, such as China and India. These nations could harness cheap labor to produce goods competitively, and became exporters as well.

These exporting countries gradually earned big surpluses from exports, and so wanted to invest them. Yet following the financial crisis in Asia in 1997, investors were wary of Asia, given the risks of a financial system that was not entirely transparent to foreign investors.

Thus investment flowed into the United States, a country already overstimulated and consuming more than it should; while the rest of the world wasn't consuming enough, nor receiving sufficient stimulus to correct the global imbalance.

The American economy was faced with the burden of stimulating global growth by absorbing all the exporting countries' goods in addition to their investment capital.

> _"What is best for the individual actor or institution, is not always best for the system."_

### 4. Low interest rates caused a boom in cheap mortgages, overheating the U.S. housing market. 

Prior to the crisis, the world's economies relied on the United States to stimulate growth, so U.S. citizens had to keep consuming and buying.

Of course to keep buying, they needed income from jobs, but at the time there weren't any.

In earlier recessions, the American economy had bounced back quickly, both in terms of economic growth and employment. After 1991, however, recoveries changed shape into so-called _jobless recoveries_. While growth and production came back strongly, job creation did not.

In the 1991 recession, production bounced back after just three quarters; in 2001, it came back after just one quarter. Yet it took nearly two years to recover the jobs lost in 1991, and over three years to recover those shed in 2001.

Job creation and loss is a concern for the _Federal Reserve Bank,_ which is tasked with ensuring high employment. One way it does this is by keeping interest rates low, to entice businesses to invest in growth and hire new employees.

After the crisis , politicians also supported low interest rates, as they were under pressure to create more jobs, and this message was made clear to the Fed. No central banker would dare raise interest rates when job growth was meager at best.

But low interest rates also put the Fed in a pickle, as its other major responsibility is to guarantee price stability and try to prevent inflation or deflation.

Low interest rates meant cheaper mortgages, an opportunity which raised the demand for housing and drive housing prices up, eventually causing a bubble. Investors across the globe saw American real estate as a very profitable investment, pouring money in. And the pop of the housing bubble triggered the crisis.

So, let's review the fault lines: debt-fueled overspending, trade and investment imbalances, and the overheated U.S. housing market.

So how did these fault lines converge in the global financial sector, and importantly, how did no one see the dangers ahead?

### 5. Subprime mortgages served the short-term needs of politicians and people, but did great damage. 

While one financial fault line is a potential threat, the convergence of many can cause a full-on crisis, as we experienced in 2008.

So what brought these fault lines together, to such disastrous effect?

The introduction of subprime mortgages was a key harbinger of the coming crash. They were designed especially for people with bad credit ratings. Essentially second-class loans, subprime mortgages also came with very high interest rates.

Subprime lending was heavily promoted by both Democrats and Republicans. It was seen as the answer to the country's stagnant pay and low employment rates. Even though income wasn't increasing, at least more people could buy a home, is how the thinking went.

Yet spending was based on high levels of debt. While subprime lending served short-term needs, it was the decisive stroke in setting other economic fault lines trembling.

Stuck with stagnant incomes, American citizens still wanted to consume. Subprime mortgages allowed them to do so, even if they had bad credit.

This, in turn, satisfied politicians who'd promoted subprime mortgages. Sure, they hadn't created more jobs, but voters were at least placated for the moment.

Foreign exporters too had export surpluses burning a hole in their pockets. The United States was ripe for investing, as consumption (fueled by cheap loans) was so high. Investors could also take advantage of the booming housing market, by buying _subprime mortgage-backed securities_ — clusters of subprime mortgages that banks sold to investors.

What at first glance looked win-win for everyone, came with extreme risk. In retrospect, the bursting of this exuberant bubble should've been predictable. So how did no one see it coming?

### 6. Financial models use past behavior to predict the future. Yet this time, we had no data at all. 

The gears of the global economy were running smoothly before the crisis, and it seemed as if there was nothing to worry about.

If there was an issue, warning signs such as price changes would have popped up. If a financial product or service is risky and the probability of a loss is high, the price for that product should fall.

Prices are an important economic indicator, because a functioning market relies on accurate prices. Before the crisis however, prices weren't accurate, but distorted!

This distortion was the result of too many foreign investors financing subprime lending. These investors bid up the prices for subprime mortgage-backed securities, which made those securities _look more attractive than they really were_.

These investors typically came from export nations looking to invest their surpluses. Germany was a leading export nation at the time, and its _Landesbanks,_ for instance, made huge investments, and thus incurred huge losses, in the course of the crisis.

Mathematical financial models can also assess risks. Older market data is fed into programs to calculate the probability of future losses. Yet according to the calculations, everything was fine.

How did we get it so wrong?

The problem was that the old market data didn't fit the new financial situation. Subprime lending was an entirely new concept, so there was no data for it at all. Calculations thus were completely arbitrary, as there was no way to accurately predict what could happen in the future.

### 7. Rating agencies also miscalculated the risks of subprime mortgages, saying they were a safe bet. 

So aside from prices changes and computer models, what are the other alarm bells that should sound if the economy is drifting sideways?

_Rating agencies_ are also tasked with assessing the risks of financial products. So why didn't these agencies see the crash coming?

Interestingly, rating agencies gave subprime mortgage-backed securities _very_ good ratings. According to them, such financial investments were completely safe.

During the lending boom, roughly 60 percent of subprime mortgage-backed securities were rated AAA, meaning they were considered as safe an investment as a _U.S. Treasury Bill_, which is among the safest investments in the world!

On the surface, it may seem like rating agencies were involved in a bit of a con game. Yet the ratings were legitimate, according to the design of the securities.

The trick was that bankers were packaging subprime mortgages from different parts of the United States and from different points of origin, such as banks or brokers, together. This practice is called _diversification_.

Diversification supposedly made the mortgages safer by minimizing risk. It meant that a mortgage could only default if a large number of people failed to pay it. At the time, it seemed unlikely that so many people would be unable to pay back their mortgages at once.

So there were a number of factors that led to the 2008 financial crisis. Yet even though all these problems converged, we still want to point a finger at someone. But at whom?

### 8. Financial incentives pushed all parties to grab all they could, without raising a red flag. 

We all want a scapegoat for the crisis. Weren't the greedy bankers to blame? Well, yes and no.

In a sense the crisis _was_ the fault of the banks, because of the enormous investment risks bankers took. The crisis took place in the financial sector, after all.

At the heart of dealings in the financial sector is _risk management._ It is a banker's job to monitor potential risks and avoid extreme risk whenever possible. Thus only a fool could say that bankers didn't know what they were getting into.

They should have curbed their greed and been more skeptical of figures that were too good to be true. So in this regard, bankers did fail to do their job properly.

Bankers are not only to blame, however. The financial sector was merely the epicenter where the fault lines met. There were many other factors that contributed to the crisis.

The government was responsible as well. Its intervention into the economy — in particular the promotion of subprime lending — made risk-taking much more attractive. Naturally, that made a crash much more likely. And instead of putting a halt to excessive risk-taking, they applauded the bankers for their efforts.

There were other culprits too: Central bankers share some responsibility as they kept interest rates so low, while foreign investors financed the whole charade. And many others irresponsibly took out mortgages they couldn't afford.

Furthermore, economists whose very job is to assess the economy failed to see the warning signs.

Yet all these people were simply doing what they thought was best for themselves. The entire system was flawed, really, as no one could tell that they were doing something harmful.

The economic system is supposed to balance all these varying interests and actors. Unfortunately, responsible economic actors caused huge losses, yet taxpayers had to pay for their mistakes. This is wrong: people who aren't responsible for the problem shouldn't have to foot the bill!

### 9. Irresponsible risk-taking needs to be curbed in the financial sector to foster long-term stability. 

Since 2008, very little has been done to eliminate the systemic problems that caused the crash in the first place. Everything is now "back to normal," which unfortunately means a crisis could easily happen again!

The financial sector needs reforming. We shouldn't forget, however, that a healthy financial sector is absolutely vital to a well-functioning economy. It allocates resources and stimulates growth.

Thus the system needs to be fixed in such a way that society can still harness the financial sector's benefits while limiting its potential destructive effects.

One of the main causes of the crash was that bankers were _incentivized_ to take big risks. Instead, we need an incentive structure that _penalizes_ dangerous risk-taking.

Prior to the crisis, it didn't matter if bankers engaged in risky behavior as they received their huge bonuses based on short-term profits. If those risks caused massive losses years later, it didn't matter.

We've now learned that bonuses should not be paid out immediately, but instead after a few years, to better assess the long-term risk and benefit of such actions. So if a banker earns a bonus, part of it should be held back. This will discourage those in the financial sector from taking long-term risks.

Another way we can prevent banks from taking excessive risks is to make their risk exposures public. If markets could see what kind of risks financial firms were taking, firms would be under more pressure to explain their behavior or simply avoid such strategies.

This data is sensitive, however. We can't start making such data public during difficult economic times, as it could just trigger more market panic. When an economy is stable, such revelations are much easier to digest.

### 10. Better schooling, more reliable safety nets: Sustainable growth requires long-term solutions. 

You'll remember that all of this trouble started because median income in the United States wasn't rising and job recovery was slow.

Politicians gave people cheap loans and subprime mortgages to appease them. What they didn't do, however, was address the actual roots of the problem: the lack of education and social safety nets.

Growing income inequality in the United States was caused by a scarcity of well-educated employees. The more educated you are, the more you get paid.

America needs a better education system, and it needs to be more accessible — especially to people from low-income backgrounds.

People who grew up in poverty are less likely to attend or graduate from college. Only 34 percent of people from low-income families (meaning families in the bottom 20 percent of the income range) make it to college. For people from high-income families (families in the top 20 percent), that figure is 79 percent.

This means financial aid programs for youth from disadvantaged backgrounds are important. Studies suggest that these kinds of programs help boost college attendance.

The American social system also needs to change, as it doesn't adequately insure people against long periods of joblessness.

Unemployment benefits in the United States run out after an average of six months, but following the recession of 2000-2001, it took more than three years for jobs to return. Americans have good reason to be worried if they lose their job!

Yet politicians nearly always respond to long periods of high unemployment by extending short-term benefits. Workers still suffer, as they can't trust that the benefits will last enough long to cover them while they're looking for work. They also can't know if they'll qualify for extended benefits, either.

Instead, benefit extensions should be tied to a predetermined formula, and not depend on the whims of politicians. People need to know they can rely on support when times are tough.

### 11. Final summary 

The key message in this book:

**The 2008 financial crisis wasn't the fault of a single actor but a coming together of many factors. These "fault lines" included historically low interest rates, a global economy that relied on the consumption habits of one nation, an irresponsible binging on subprime loans and a systemic failure to assess market risks. If as a society we want to avoid similar future crises, we need to fix these deep financial fissures in our global economic system.**

**Suggested further reading:** ** _Beyond Outrage_** **by Robert B. Reich**

_Beyond Outrage_ provides a sobering analysis of what has gone wrong in American politics and economics. Looking at the distribution of wealth and income imbalance, it convincingly argues that we must wrest government from the hands of the regressive right.
---

### Raghuram G. Rajan

One of the few experts to have seen the 2008 crash coming, Raghuram G. Rajan, formerly chief economist with the International Monetary Fund (IMF), is a professor of finance (on leave as of 2013) at the Graduate School of Business at the University of Chicago and the governor of the Reserve Bank of India.

