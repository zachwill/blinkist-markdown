---
id: 5919aa22b238e100066a4f2e
slug: fear-en
published_date: 2017-05-19T00:00:00.000+00:00
author: Joanna Bourke
title: Fear
subtitle: A Cultural History
main_color: 6CBCED
text_color: 3E6B87
---

# Fear

_A Cultural History_

**Joanna Bourke**

_Fear_ (2005) explores how fear has shaped cities, parenting, and culture over the past centuries. From the role of fear in war and sickness, to the design of public buildings and the response to the threat of nuclear power, these blinks give us the historical context we need to understand the nature of fear in contemporary society.

---
### 1. What’s in it for me? Discover the long and fascinating history of being afraid. 

Are you petrified every time you step onto an airplane? Do you live in fear of the thought of a house fire? Perhaps it's spiders or snakes that make you tremble most of all? Most of us have something that stirs up our greatest fears, and the same has been true throughout history; fearful people have existed across societies, continents and generations.

But this doesn't mean that we've always been scared of the same things — or in the same way. Fears that used to dominate society in the past, be they the thought of nuclear war or witches, no longer grip us in the same way. So, how has fear changed throughout history? These blinks will take a look.

In these blinks, you'll also find out

  * how mass panic affects the design of concert halls;

  * why scaring soldiers can sometimes make them braver; and

  * which fear has remained constant across history.

### 2. Death is our greatest fear, and one that worsens when poverty strikes. 

Many people grow uncomfortable after spending too long in hospitals, retirement homes and cemeteries. These environments draw our minds toward the greatest human fear — mortality.

Nearly all human fears can be traced back, in one way or another, to a fear of dying. Those afraid of spiders, snakes and crocodiles don't fear the creatures per se, but rather the prospect that these animals could kill them.

Similarly, people who live in fear of losing their jobs struggle with the deeper fear that they might lose their livelihoods, their homes and, in the worst case scenario, ultimately die after being forced to live on the streets.

For thousands of years, rituals, ceremonies and beliefs celebrating the afterlife negated the human fear of death. But in the nineteenth century, these comforts were taken away from the lower class in the West, exacerbating the fear of death and creating _pauperization._

The bodies of deceased paupers were piled into mass graves, without tombstones or inscriptions to commemorate the dead. In addition, their bodies were covered in a caustic quicklime solution to speed up their decomposition.

Graves of nineteenth-century paupers were also unprotected, making them easy prey for Victorian bodysnatchers who made a living from selling corpses to anatomists and medical students. Knowing that this was the gruesome fate that awaited their bodies after death, people grew more afraid of dying than ever before.

Indeed, the fear of dying was enough to kill people in itself — an elderly woman by the name of Susan Starr died from shock in 1871 after social relief services threatened to cut off her financial support.

### 3. The human tendency to panic in fear shaped modern public architecture and engineering. 

With their narrow rows of seats, dim lighting and tightly packed, cozy atmosphere, old-fashioned cinemas can make us quite nostalgic. And yet, there are good reasons for the spacious layout of a modern theater.

Modern public buildings are designed to be quick and easy to evacuate should the people inside panic. Unfortunately, it took many tragedies before we learned the danger of mass panic in crowded spaces.

On the 16th of June in 1883, 1,200 children gathered in Sunderland's Victoria Hall, England, to watch a stage performance. The children had been told a gift would be given at the end of the performance, so when the show finished, children raced toward the stage.

A swing door leading down to the stage had accidentally been left locked with an iron bolt. The first children to arrive at the door fell after crashing into it. The children behind them, worried they'd miss out on receiving a present, kept pushing forward. In the ensuing panic, as many as 183 children were trampled to death.

Similar incidents occurred when fires started in public theaters. A fire in Chicago's Iroquois Theater in 1903 led to a massive loss of life: 600 people were killed in the stampede to escape the burning building.

It was clear that the human tendency to panic necessitated stringent safety measures in public buildings, which sparked a series of new design innovations. In Indianapolis, inventor Carl Prinzler developed the first doors that could be opened by pushing against a panic relief bar.

In Britain, firefighter William Paul Gerhard championed the design of theaters that could be evacuated in as little as four minutes in the case of a fire. Emergency exits located on both sides of aisles of seats, as well as wider aisles, stairways and doorways, were established as new standards for buildings.

### 4. Society disapproves of fearful children, and typically blames mothers. 

Children have many fears, not least of which are vicious monsters living under their bed. Should parents provide their kids with a night light to help them cope, or leave them alone in the dark to toughen them up? Opinions vary among educators, families and psychologists.

There's a long-standing belief that children must be taught not to fear things. Parenting guides from the 1950s and 1960s argued that a fearful child was an embarrassment; fears were thought to prevent children from growing into healthy, independent adults.

In turn, parents had the responsibility to help their children overcome their fears. But when they failed, mothers often came under fire.

For example, in the first half of the twentieth century, it was widely believed that overly gentle and protective mothers would lead children to become shy, fearful and lonely. This was particularly unacceptable when it came to young boys, whose mothers were accused of emasculating them.

Shyness and fearfulness weren't the only "flaws" that mothers were blamed for. In 1941, psychologist Adelaide Chazan argued that children who refused to go to school were psychologically ill. The cause of this illness lay with the laxity, dependency, protectiveness and instability of the mother.

But as more and more mothers began to work in the 1950s, educators came to recognize the value of maternal protection. Their subsequent worry was that children left alone by their mothers would grow up fearful. New parenting guides such as _Home and Children_ insisted that mothers were to stay by their child's side for their first five years of life.

So, whether they left their children alone or remained highly protective of them, mothers would be blamed for a fearful child either way.

### 5. Fearsome nightmares were blamed on a lack of blood flow to the brain until Freud argued that they arose from our own psyche. 

Have you ever dreamed about something terrifying happening to the ones you love? Fear is most powerful at night, especially in nightmares, and scientists and psychologists have long sought to understand the causes of night terrors.

At first, nightmares were believed to arise from mere physical discomfort.

Having rejected old beliefs that nightmares were the work of demons, nineteenth-century physicians advised patients to avoid eating too much before sleeping, sleeping on their back or sleeping with the window closed to stave off nightmares.

According to these physicians, a full stomach put pressure on the diaphragm, the lungs and the heart, which slowed blood flow to the brain. This poor blood flow was seen as the chief cause of nightmares.

At the turn of the twentieth century, however, Sigmund Freud developed a new, groundbreaking interpretation of dreams, viewing them as the result of mental processes that gave an insight into repressed desires, primal urges and hidden emotions.

Freud argued that when we dream, we let our guard down. This, in turn, allows thoughts we'd normally suppress to rise to the surface and express themselves in surprising ways. For instance, a person who resented his mother might dream of watching the mother being eaten by a predator or murdered by someone else.

Freud also believed that many aspects of dreams were symbolic of activities in our waking life. Running into a house or up and down a staircase were, according to Freud, both symbols for sexual intercourse.

By analyzing his patients' dreams, Freud sought to reveal their desires and perversions so they could finally come to terms with them.

### 6. Unstable societies are breeding grounds for fear and panic. 

With the rise of terrorist attacks around the world and their extensive coverage in the media, people are prone to panic at the sight of an unattended bag or feel nagging suspicions when they see foreigners. This isn't the first time that insecurity has spiked among populations.

History is filled with shifting periods of stability and relative instability. It's during unstable times that emotional insecurity, anxiety and fearfulness rise.

Consider Great Britain in the 1920s as an example. With 1.5 million people unemployed at the start of the decade, and many others struggling to get by on part-time employment, tensions were high. It was also during this time that British miners began to strike against their low wages and appalling working conditions.

The working class began to question the privileges of the middle and upper classes, who in turn began to fear an impending revolution. Their growing insecurity turned out to be the perfect environment for panic.

A satirical radio broadcast in 1926 by the BBC illustrated this situation perfectly; the program sounded like a normal news broadcast that was interrupted by live reports of protests by a working class mob in the city of London.

The broadcast was filled with outlandish details that underlined its fictional nature — the leader of the crowd was named as the chairman of the Committee for the Abolishment of Theatre Queues. The mob was said to have destroyed the Big Ben, after which the radio host announced that Greenwich time would have to be measured according to the clock of popular children's writer Uncle Leslie. In spite of these bizarre details, the unstable nature of society led people to panic. The BBC was inundated with concerned telephone calls from fearful listeners.

### 7. Intense fear during combat leads to both lasting illnesses and courage-boosting adrenaline rushes. 

Soldiers are typically portrayed as fearless fighters. But the reality can be quite different on the battlefield.

Nearly all soldiers admit that fear is the predominant emotion experienced in combat. Back in 1947, a medical study interviewed soldiers from two infantry divisions deployed during World War II. Just 7 percent of men stated that they didn't feel afraid during battle, while 90 percent of men reported suffering a range of health problems linked to traumatic experiences of fear.

Soldiers went home with trembling limbs, sleeplessness and sweating palms, as well as digestive issues such as diarrhea and constipation. Spending time in constant fear of an attack derailed soldiers' nervous and digestive systems, seriously weakening a large part of the armed forces.

At the same time, fear can be a driving force behind heroic actions in battle, giving soldiers an adrenaline boost that leads them to act recklessly. In 1944, a young American soldier named William Manchester was fighting on the island of Okinawa in Japan when a sniper began to shoot down his fellow soldiers.

Observing the trajectory of the shots, Manchester realized that the sniper had to be hiding in a fisherman's cabin at the foot of a nearby hill. Shaking with fear, Manchester began to sprint and duck his way toward the shack, only to realize that he'd left his steel helmet behind.

His jaw began to twitch and his eyes could no longer focus. But he kept moving, smashed his way into the shack and managed to shoot the Japanese sniper. After this, Manchester vomited and wet himself due to the sheer intensity of his fear, which had ultimately provided him with the adrenaline he needed to act.

### 8. The threat of nuclear war terrified entire nations. 

While WWII was terrifying, it was a later event in the twentieth century that would raise popular fear to unprecedented levels. As the Cold War set in and the nuclear arms race began, strategic moves of world powers were more threatening than ever.

The successful launch of Soviet satellite Sputnik in 1957 was a serious cause for worry among Americans. The installation of missiles in Cuba by Soviet President Khrushchev five years later, just 90 miles off the coast of Florida, caused panic.

Fear and anxiety among the US population continued to grow. The 1980s saw another spike in fear, as US President Ronald Reagan began to develop a space-based nuclear weaponry system, which exacerbated tensions among existing nuclear powers.

The prospect of nuclear war and Armageddon struck fear into the hearts of many during this period. A 1983 survey by TV Times in the UK revealed that 75 percent of respondents believed a nuclear war was imminent.

Perhaps it's no surprise that people of the West were so fearful. Government initiatives intended to better prepare civilians in the event of a nuclear war largely increased fear in populations.

On February 8, 1951, the US government simulated a nuclear attack on the city of New York, with such drills eventually becoming commonplace. Even children in schools were trained to throw themselves under their desks when teachers yelled at them to take cover.

Not only were these drills ineffective, they also instilled terror in children and teachers. One teacher at a school in Queens, New York reportedly screamed at a child who failed to take cover correctly: "Now your right leg is burned off, your left arm is amputated and your skin is burned away!" Entire generations of children grew up in these fearful environments.

### 9. Fears of life-threatening illnesses change as medicine evolves. 

Today, people wearing surgical masks on the street might be thought of as hypochondriacs. But the fact is that health-based fears are always shifting — what might seem hysterical at first may soon become the norm.

In the nineteenth century, it wasn't cancer that people feared most, but infectious diseases such as smallpox or consumption. In one 1896 survey in the _American Journal of Psychology_, just five percent of respondents described cancer as an illness they feared.

But as infectious diseases became less of a medical threat in the twentieth century, chronic diseases began to stir fears in the popular imagination instead. Among these, cancer was, and still is today, the illness feared most. A public survey held in 1954 in Manchester, UK, showed that 70 percent of women feared cancer more than any other disease.

For cancer sufferers, battling the disease is much like battling fear itself. Take the story of Edna Kaehele from Denver, Colorado; she was diagnosed with cancer and was told she had six months to live in 1946.

Kaehele commenced radiation treatment and medication. Her doctors reassured her, but informed her relatives that there was nothing that they could do. Though Kaehele was overwhelmed with fear at the prospect of dying, she wasn't ready to be defeated by cancer so easily.

Embracing the belief that cancer could be cured with a protein-based diet and refusing to let her illness scare her, Kaechele's fear disappeared. Twelve years after her diagnosis, Kaehele published an inspiring book on her battle with sickness called _Sealed Orders._ We can't know whether Kaehele's refusal to fear death helped her live longer, but it certainly made her final years more enjoyable.

### 10. Final summary 

The key message in this book:

**Since the nineteenth century, the human fear of death has played a key role in the popular imagination. From nuclear war to cancer to parenting and public architecture, fear and the struggle to cope with it shape many aspects of our societies and day-to-day lives.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Gift of Fear_** **by Gavin de Becker**

_The Gift of Fear_ (1997) provides insight into the mechanisms of fear, explaining how our instincts protect us from criminals by attuning us to universal signals and warning signs. Violence rarely comes out of the blue, and by recognizing some telltale signs, you will be better equipped to keep yourself out of harm's way.

### 1. What’s in it for me? Get an inside look at the Trump White House. 

Back in the seventies, author Bob Woodward raised the bar on investigative journalism in Washington, DC, when he teamed up with Carl Bernstein to cover the Watergate scandal. Their work played a significant role in bringing impeachment charges against President Nixon, which led to his resignation.

Now, Woodward has turned his eye to the Trump administration. Though he doesn't name his sources, he claims his reporting comes from hundreds of hours of witness interviews and those who participated firsthand in the events.

In these blinks, you'll learn about the difficulties this administration faces and get an understanding of the interpersonal dynamics of the White House.

In these blinks, you'll also discover

  * which literary titan Trump compared his tweets to;

  * which member of the Trump cabinet called the president a "moron"; and

  * how many hours of cable news Trump tends to watch every day.

### 2. Things were looking bad for the Trump campaign in 2016, and the Trump team wasn’t prepared to win. 

Back in 2010, Steve Bannon was making political films to support the Tea Party, a right-wing populist movement that had taken American politics by surprise. That year, conservative pundit David Bossie took Bannon to meet Donald Trump, who was thinking of running for president as a Republican.

At the time, Bannon regarded the meeting as pointless. But he also found it highly entertaining, as Trump struggled to comprehend how his record of Democratic donations and pro-choice support could affect his chances as a Republican candidate. So, like many others, Bannon was surprised when Trump managed to secure the Republican nomination in the 2016 election.

By then, Bannon was running Breitbart News, an online outlet for far-right political commentary, and on August 13, 2016, he placed a call to Rebekah Mercer, one of Breitbart's financial backers. Bannon saw that the Trump campaign was in trouble. That morning's newspapers had Trump 20 points behind Clinton, and there were plenty of stories about the campaign being in disarray.

Indeed, Mercer told Bannon that Trump's current campaign manager, Paul Manafort, was "a disaster." She then advised Bannon to go meet with Trump and save the sinking boat. Mercer knew Trump was no stranger to Breitbart and that he liked and respected Bannon. Most of all, she knew Bannon was someone Trump would actually listen to.

Remarkably, with 85 days left before the general election, Bannon came on board and brought the campaign some much-needed focus. He laid out an easy-to-follow agenda centered around three things: immigration, jobs and foreign wars.

As Bannon saw it, these were the three areas where Trump's stance differed most from Clinton's. Soft on immigration, Clinton was both a globalist who would lose jobs to overseas competitors and part of the establishment that had resulted in too much military involvement on foreign soil. These were three positions she couldn't defend, and as long as Trump kept hammering these points, he had a good chance of winning.

Sure enough, Bannon was right. But even Trump seemed surprised when the votes came in. He had spent zero time preparing a transition team or giving any thought to the 4,000 jobs that would need filling when he won. As Bannon put it, Clinton had spent her whole life preparing for the presidency; Trump didn't spend one second.

### 3. As Trump put together his cabinet, he was shaken by news from the intelligence community. 

During the campaign, the liaison between the Trump team and the Republican Party was Reince Priebus, who then became the White House chief of staff. But this was just one of many positions that needed filling.

For his secretary of defense, Trump favored the retired general Jim "Mad Dog" Mattis, even though the pick didn't sit well with Steve Bannon, who joined the administration as "chief strategist," an invented position.

Bannon was against anyone who seemed even slightly globalist. And while Mattis had been fired from the Obama administration for being too aggressive on Middle Eastern policy, Bannon felt he was too liberal and globalist when it came to social policy. But Trump made a campaign promise to get rid of ISIS, and on this subject Mattis said he believed in a "war of annihilation," not a war of attrition, which Trump loved.

Next was his pick for secretary of state, Rex Tillerson, the former CEO of the ExxonMobil oil company. Trump wanted people who knew how to make deals, and Tillerson had a long track record of billion-dollar dealings around the world. In 2013, Vladimir Putin had even awarded Tillerson the Russian Order of Friendship for his business in Russia.

For his economic team, Trump brought in two former associates of the renowned investment bank Goldman Sachs. He selected Steve Mnuchin, a former hedge fund manager, as secretary of the treasury and Gary Cohn, Goldman's former president, as head of the National Economic Council.

Trump picked up another person who'd been fired by Obama, Mike Flynn, as his national security advisor.

A good number of Trump's cabinet eventually resigned — but Flynn was the first to go. He resigned after it was revealed that he'd negotiated sanctions with a Russian ambassador before his role as advisor even began.

One of the people handling the Flynn case was James Comey, the director of the FBI.

Trump was no stranger to Comey. Before his inauguration, Trump had been given an intelligence briefing by Comey and two other men, James Clapper, the director of national intelligence, and John Brennan, the director of the CIA.

During this brief, one of the main subjects was evidence of organized Russian interference during the 2016 elections, including a data breach of the Democratic National Committee.

But what really shook Trump was what Comey told him: there was intelligence, compiled in what was known as the Steele dossier, suggesting that the Russian government had evidence, from 2013, of Trump engaging in compromising behavior with two prostitutes in Moscow.

### 4. From day one, the Trump administration has been in conflict over North and South Korea. 

Trump did not get off to a good start with the intelligence community. Before he even took the oath, the FBI, CIA and Office of the Director of National Intelligence were telling him the Russians interfered with the election. And then, on January 10, 2017, the details of the Steele dossier were published on Buzzfeed. Day one of Trump's presidency hadn't yet dawned, and his feelings toward the intelligence community were probably already less than friendly.

Once Trump did move into the White House, he needed a new national security advisor, so he hired another military veteran, Lieutenant General H.R. McMaster. According to Woodward, it was impulsive and done without going through the proper procedures for removing an active-duty army officer from the military. This was typical for Trump, who had little concern for the way things were done.

Another case in point is the longstanding relationship between the United States and South Korea. Right from the start, Trump couldn't make heads or tails of why the US was spending $10 billion on a state-of-the-art missile defense system only to have it installed on some, as Trump called it, "piece of shit land" in South Korea.

On top of that, Trump was flummoxed as to why the United States had an $18 million trade deficit with South Korea. In his eyes, the United States was being screwed over by people that he was paying to protect. It made no sense.

One advisor after another, including McMaster, Mattis and Cohn, tried to explain how the relationship with Korea was really advantageous to the United States, especially in early 2017. At the time, North Korea was threatening the world on a regular basis with increasingly aggressive tests of their nuclear capabilities.

The expensive missile defense system, known as THAAD, did protect South Korea, but it could also detect, in seven seconds, a missile headed for the United States. Trump suggested putting the system in the United States, where the estimated first response would be in 15 minutes — a huge difference. Trump's other suggestion was to use THAAD as a bargaining chip for renegotiating _KORUS_, the Korean–US trade deal, much to the frustration of his advisors.

Mattis, Cohn and Tillerson all tried to explain how intelligence from South Korea and a US presence in the area were vital to US national security, but Trump would have none of it. After Trump walked away from a meeting suggesting all military personnel in South Korea be brought home, Secretary of State Tillerson could only shake his head and mutter, "He's a fucking moron."

### 5. Afghanistan was another point of contention for the Trump White House. 

Back in 2011, Trump was tweeting for troops to be taken out of Afghanistan, calling the whole mission there a "total disaster" and a "waste of billions." This message was kept alive during his campaign, so he felt it was a promise to his voters that needed to be kept.

However, despite Trump's demands, the issue of Afghanistan would be a persistent conflict throughout the administration's first year.

While Trump and Bannon, as well as the national security council chief of staff, Lieutenant General Keith Kellogg, were arguing for a complete withdrawal of troops, H.R. McMaster suggested a new approach. He wanted to focus on reinforcing, realigning, reconciling and regionalizing — it was called the R4 plan, and it was meant to put money, troops and equipment to better use in relatively stable areas. This way, they could bolster the Afghan government and try to cut back on corruption while also getting regional powers like India more involved.

Bannon, meanwhile, was pushing a different agenda. He wanted to shift control of Afghanistan from the military to the CIA. He wanted paramilitary forces to step in, train local soldiers and move forward from there. But when it came time to present ideas to Trump, the CIA was not willing to or capable of taking responsibility for Afghanistan. Plus, Trump was told that if the United States pulled out, its absence would leave a power vacuum, which would all but guarantee Afghanistan's becoming even more of a hub for every terrorist in the area.

This left Trump with McMaster's R4 plan, which sounded better than the idea of being held responsible for the next 9/11 if he didn't do _something_. Bannon was crestfallen, as it meant sending 4,000 more troops to Afghanistan.

McMaster put his plan into action, but the situation didn't improve. If anything, it got worse. Trump believed McMaster had tricked him into going along with his plan and had no qualms about berating him in front of others.

Then, in September of 2017, Trump heard that the Chinese government was extracting copper out of Afghanistan. He was livid. How, he yelled at his advisors, can we be getting screwed like this? Spending billions in Afghanistan while China is swooping in and getting rich off its minerals? Why aren't we taking minerals?

Ironically, the news of China taking copper would later prove to be fake news.

> _"Mattis was particularly exasperated and alarmed, telling close associates that the president acted like... 'a fifth or sixth grader.'"_

### 6. Trump’s advisors have found him to be immovable on most subjects, especially trade deals. 

According to the Bannon worldview, which Trump largely shared, politicians are responsible for the decline in good factory jobs in America. These were politicians who favored "globalism over Americanism."

Trump's economic advisor, and former Goldman Sachs president, Gary Cohn, didn't believe this, nor did 99 percent of the economists in the United States. Cohn had countless pages of data printed up to prove that free trade was good for the United States, and that the nation had moved from manufacturing to a service-based economy.

Unfortunately, Trump refused to read data. So Cohn had big, simple charts printed out. They showed that by importing cheap TVs, people could buy a state of the art TV for a couple hundred dollars and still have money to spend at a restaurant or on a Starbucks coffee. But it was useless. Once Trump made up his mind, it was impossible to get him to see the other side of things.

At the heart of this phenomenon was the Trump philosophy of never showing weakness. The central tenets of this philosophy are never to apologize or admit you're wrong.

A cornerstone of Trump's business practices, this philosophy seems to apply to Trump's relationships as well. Trump once gave relationship advice to a colleague, telling him that real power comes from fear and never backing down. You have to be strong, keep pushing forward, always deny and never admit you were wrong.

Trump certainly followed this advice. No matter how many people told him that his views against free trade were wrong, he would declare, "I don't want to hear it!" He repeatedly called for someone to draft a document to get the United States out of KORUS as well as NAFTA, the North American Free Trade Agreement that laid the foundation for $1 trillion in trade between Mexico and Canada.

Cohn and Sonny Perdue, the secretary of agriculture, tried to explain that the United States produced a surplus of products worth $39 billion that was exported to Mexico and Canada every year. Without NAFTA, this could end up going nowhere and devastate families in the heartland that may have supported Trump in the election.

Trump's response? "But they're screwing us, and we gotta do something." He made his staff secretary draft a letter giving the mandatory 180-day notice to pull out of NAFTA. Fortunately, there was a plan to stop that letter.

### 7. As staff secretary, Rob Porter kept the president from committing dangerous actions and tried in vain to establish order. 

Rob Porter was a Harvard-educated lawyer who started out in politics as a Senate intern. Later, he worked as chief of staff for Senator Orrin Hatch, of Utah. As Cohn saw it, Porter was one of the few stabilizing forces in the Trump White House.

Being staff secretary meant that Porter worked relatively closely with Trump, and Porter used this proximity to establish some order amid the general chaos that ruled in the White House. This meant protecting the president from distractions, negative influences and his own worst tendencies.

On more than one occasion, Porter, Cohn and sometimes Mattis, worked together to keep dangerous paperwork, such as an order to terminate KORUS or NAFTA, off the president's desk. So, after Trump demanded the 180-day notice to leave NAFTA, Porter informed Cohn when it was on his desk, and Cohn then used his Oval Office access to remove it before Trump had a chance to sign it.

This only worked because Trump had the remarkable ability to forget about something the moment it stopped competing for his attention. If Porter could keep him away from anything related to NAFTA, it could be weeks before Trump would hear the word NAFTA on TV and then demand the drafting of another letter — thereby restarting the whole process.

If it sounds as though the Executive Office of the United States has fallen into complete dysfunction, make no mistake. That's exactly what Woodward believes has happened.

Reince Priebus had a plan to establish an orderly system, so that Trump's on-the-fly style would cause less chaos. He set up directives specifying that all appointments had go through Porter and that Trump's verbal directives were not final until they were written down, filed by Porter and signed by Trump. But this plan didn't last long.

The staff had other ideas. Some people, like Ivanka Trump, felt that they had special White House privileges that exempted them from the normal rules. This led Bannon to blow up and scream that she was just a staffer and had to go through Priebus like everyone else.

Ivanka believed there was a "First Daughter" status, and her advice to Trump, seconded by her husband, Jared Kushner, was often the opposite of what Bannon would suggest. Trump liked to call them "moderate Democrats from New York" and leave it at that, but Bannon was enraged that Ivanka believed she was on the same political level as he was.

> _"'It's not what we did for the country,' Cohn said, 'It's what we saved him from doing.'"_

### 8. The Russian investigation has caused presidential meltdowns. 

May 18, 2017, was a dark day in the White House. It was the day after former FBI head Robert Mueller was appointed special counsel by the Department of Justice to investigate "any matters that arose or may arise" from the ongoing investigation into Russian interference during the election. This broad directive essentially gave Mueller carte blanche to look into whatever he wanted.

The day after the news of the special investigation, Trump spent the day wallowing in uncontrollable anger. No one had seen him this angry before, but, unfortunately, the investigation would prove to be a continuing distraction that could set him off on a tirade at any moment.

It all started after efforts to stall Trump's impulsive behavior backfired on May 9, 2017. That day, Trump's spontaneous urge was to fire the director of the FBI, James Comey. The president thought he was a "grandstander" and "out of control" with the Russian investigation. Recently, the FBI had gone looking into Kushner's past business deals and now Trump wanted Comey gone.

Bannon didn't like the idea. He thought firing Comey would only make the FBI more eager to dig up dirt on Trump. But once Trump had an idea, he would shut out any advice to the contrary. In the end, though, Trump was convinced that he should talk it over with Donald McGahn, the White House counsel.

McGahn referred him to Rod Rosenstein, the deputy attorney general, since he's the person who's really in charge of the FBI. McGahn thought Rosenstein was sure to be sensible about things and cool Trump down. Stalling tactics like this were becoming the common way to avoid serious damage from Trump's impulses.

Alas, Rosenstein surprised everyone by agreeing to immediately write up a memo explaining the reasons for Comey's firing. Oddly enough, the reasons focused on Comey's handling of the investigation into Hillary Clinton's private email server during the 2016 election. Trump was extremely pleased and before anyone knew what was happening, the memo was filed and Comey was fired.

Not long after the firing, it was revealed that Comey had kept notes of every meeting he had had with Trump. The notes suggested that Trump might have tried to influence the FBI to go easy on Mike Flynn, the former national security advisor. This possible obstruction of justice was one of the reasons Rosenstein assigned Mueller to further investigate Trump's actions.

To Trump, it was like a conspiracy to stab him in the back. Completely unfair!

### 9. Trump takes his tweets very seriously. 

One of Trump's closest aides since the campaign has been Hope Hicks, a former professional model who rose in the ranks to become his public-relations specialist. On June 29, 2017, Hicks felt she had to say something about Trump's use of Twitter. As she explained it, "It's not politically helpful."

That day's Twitter-related crisis was caused by a 6 a.m. tweet that said Mika Brzezinski, the cohost of MSNBC's _Morning Joe_, came to a party at Trump's Mar-a-Lago estate with a botched face-lift that was "bleeding badly."

Perhaps unsurprisingly, this tweet didn't go over well, especially with the many women he'd outraged with his comments about grabbing women "by the pussy." Trump had to be reminded that votes from female senators were needed if his legislation to repeal Obamacare was going to pass. And sure enough, senators Susan Collins of Maine and Lisa Murkowski of Alaska were particularly upset about the latest tweet.

But, as Trump put it, "This is who I am. This is how I communicate." Trump liked to refer to his Twitter account as his "megaphone" for speaking to voters directly and unfiltered. He believes it played a big role in his winning the election.

Just how important is Twitter to Trump? So important that he had people print out tweets that received over 200,000 likes. Trump studied this collection of hits and looked for commonalities so that future posts would be even more effective. However, the common thread he noticed was that the most popular tweets were also the most shocking.

When Twitter expanded the character limit from 140 to 280, Trump was a little conflicted. He understood the reason behind it, and felt that he might be able to explain himself better, but he also thought it was "a bit of a shame because I was the Ernest Hemingway of 140 characters."

Trump's social media tendencies were ultimately more worrying than the fallout from insulting a daytime talk show host. In the days of North Korea's escalating nuclear tests, Trump and Kim Jong Un began taking jabs at each other online, with Trump eventually writing, "...I too have a Nuclear Button, but it is a much bigger & more powerful one than his, and my Button works!"

Trump loved this kind of antagonism. Meanwhile, his staff was trying to prevent him from starting World War III with a tweet.

### 10. Despite his best efforts to rein in Trump’s behavior, Reince Priebus resigned, followed by Bannon. 

Chief of Staff Reince Priebus called Trump's bedroom the "devil's workshop," since it was the place where he liked to watch TV, get worked up over the news and send out disturbing tweets. Priebus considered Sunday evenings the worst for this kind of behavior and called it "the witching hour."

Priebus tried to curb the damaging tweets by scheduling Trump less TV time on Sunday nights, but Trump still put in six hours a day in front of cable news. Priebus also tried to bring some order to Trump's impulsive style by setting up his Rob Porter-centric system, but people still found ways around it.

Then came the vote on repealing Obamacare. With Trump still considering Priebus his liaison to the Republican Party, much of the vote's success depended on the chief of staff, so when it ultimately failed, Priebus knew his time was up. On July 28, 2017, he handed in his resignation.

His last moments with Trump did not sit well. Aboard Air Force One, he and Trump discussed what needed to be done to find a replacement. Priebus offered to help, and Trump said he'd think about it over the weekend. Moments after he left the plane, Trump tweeted that John Kelly, who was previously acting as secretary of homeland security, was his new chief of staff.

It was clear to Priebus that Trump had ended their working relationship with a lie, but it had also become clear that Trump was a man without pity or empathy.

Not long afterward, Steve Bannon resigned, though Trump liked to say he was fired. Trump's animosity toward Bannon surfaced quickly, as Bannon soon began making disparaging comments in the media about Trump's willingness to play along with the establishment. But it also became apparent that Bannon was a primary source for Michael Wolff's highly critical book, _Fire and Fury_, which drove Trump to issue a statement saying Bannon had "lost his mind."

For his part, Bannon thinks Trump watches too much TV and has failed at what they set out to do together, especially in foreign policy.

### 11. Cohn helped get a faulty tax plan passed but resigned over Trump’s misguided tariffs. 

The end of 2017 was marked by Trump's tax bill, which would prove to be the only significant piece of legislation the Trump administration passed in its first year. It was aimed at cutting taxes for middle-income families, but Trump's primary concern was cutting the corporate tax rate, which he believed would bring more money back to the United States.

Many big US companies were moving their headquarters to nations with lower corporate tax rates, like Ireland. They would then make their US offices a subsidiary and save money, a practice known as "inversion."

Ireland's corporate tax rate is a remarkably low 9 percent. At the time, the United States' stood at 35 percent. Trump wanted his bill to lower the rate to 15 percent, if not 10 percent. He loved nice round numbers like these, because he could easily remember them and sell them to the voters. Cohn had to keep telling him that changes like this aren't so easy to make.

Indeed, in crafting the tax bill, Cohn found out just how hard it is to deal with taxes in Washington. Every voter in the Senate had a demand if they were to agree to vote for it. For example, Senator Marco Rubio wanted the tax credit for children to be raised from $1,000 to $2,000, or else he wouldn't vote for it.

When all was said and done, the various loopholes were wildly convoluted and any benefit to the economy was questionable. It was clear the real benefit was to the highest earners and corporations, which saw taxes reduced from 35 to 21 percent. Interestingly enough, some small businesses and partnerships like the Trump Organization saw a whopping 20-percent reduction in taxes.

In March of 2018, Trump returned to one of his obsessions, tariffs, and this time Cohn couldn't take any more. Trump ignored his advice and passed tariffs on imported steel and aluminum, even though Cohn was sure it would hurt businesses, including the car manufacturers that Trump would often say he cared about.

Cohn was willing to keep fighting a losing battle, but the chaos was too much. On one hand, Trump was trying to get better trade deals with China — but then he messed up the deal by putting tariffs on imported steel. It was hopeless and Cohn was fed up. He resigned on April 2, 2018.

> Cohn gave his resignation after Trump said "both parties" were at fault in the death that followed a white supremacist rally in August 2017. Trump convinced him to stay until the tax bill was finished.

### 12. Trump refused to follow his lawyer’s advice, so his lawyer resigned. 

Some days, it didn't even make sense to try and talk to Trump because he was so infuriated by the ongoing news about the Mueller investigation.

Fortunately, Trump had a very loyal lawyer on his side, John Dowd, a 76-year-old expert in criminal defense. While many other seasoned lawyers turned down the opportunity to represent Trump, Dowd jumped at the chance to cap his career with such a high-profile case.

Dowd dutifully took Trump's calls at all hours of the night, letting him vent his frustration about the perceived injustice of Mueller's inquiries. Dowd agreed that there could very well be political motivations behind the investigation, telling Trump that it was "a royal fuck job by a bunch of losers."

But even though Dowd was full of reassurances and advice, he had trouble, just like everyone else, getting Trump to change his mind once he'd made a decision.

By March of 2018, Dowd had tried to prepare Trump to give testimony. The two men sat in a room, with Dowd feeding Trump questions. To many questions, Trump said he couldn't remember the details of the situation. And when Dowd mentioned Comey and the Flynn case, Trump flew into an angry rant about how unfair it all was.

Dowd had a strong feeling that Trump should avoid giving testimony. He also knew of a story from a lawyer friend in Florida who had to question Trump once. When asked what he did for a living, Trump gave an answer that took 16 pages to transcribe.

Nevertheless, Trump was adamant that he should testify. He was convinced that it would look bad to the public and the media if he tried to avoid it. For Dowd, this was a deal breaker. He couldn't successfully protect his client if the client refused to take his advice, and so, on March 22, 2018, he tendered his resignation.

Ultimately, Trump understood his opinion and thanked him for doing a good job.

Dowd believed that Mueller had little evidence of Trump's campaign team having colluded with the Russians or obstruction of justice in the Flynn case. But after spending around nine months as Trump's lawyer, he'd also seen the president's fatal flaw. Whether it was flip-flopping on political matters, phony denials, bizarre tweets or accusations of "fake news" — he knew the man was a liar.

### 13. Final summary 

The key message in these blinks:

**According to Bob Woodward, the Trump White House has been a swirling pool of chaos and confusion from the very beginning, and much of it has stemmed from the man at the center of it all, Donald Trump. He surrounded himself with a cabinet that is often at direct odds with each other or with him. He refuses to be advised on any subject when that advice goes against the opinions he's already formed. Trump is also prone to rash decisions, causing his staff to try and undermine his orders in an effort to prevent catastrophes.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Fire and Fury_** **by Michael Wolff**

_Fire and Fury_ (2018) gives a fly-on-the-wall account of the Trump administration's early days. With his insider access to the West Wing and over two hundred conversations with senior staff under his belt, Michael Wolff paints a fascinating portrait of an administration he claims is wholly unprepared to govern.
---

### Joanna Bourke

Joanna Bourke is a professor of history at the University of London specializing in the history of warfare, and gender and class relations. Her other works include _Wounding the World_ and _Working Class Cultures in Britain._

