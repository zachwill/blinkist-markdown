---
id: 59773cc1b238e10005084241
slug: feathers-en
published_date: 2017-07-27T00:00:00.000+00:00
author: Thor Hanson
title: Feathers
subtitle: The Evolution of a Natural Miracle
main_color: FFF4C9
text_color: 806F19
---

# Feathers

_The Evolution of a Natural Miracle_

**Thor Hanson**

_Feathers_ (2011) is all about the evolution and significance of our quilled comrades, the birds. These blinks explain how feathers originated, why they're unique and how they have affected everything from human culture to technology.

---
### 1. What’s in it for me? Learn why the feather is an evolutionary miracle. 

When we're small, we're amazed by birds, especially by the wonderful ways in which they swoop and fly through the sky. However, we soon get used to their amazing feats. We forget just how astonishing they actually are.

It's time we took another look at birds and flight. In particular, we should examine just what makes birds so special — their feathers. Feathers are the key to flight and the reason birds can live on every continent of the world in such wonderful, varied colors.

So, let's probe deeper into the feather and learn how it evolved to be such a powerful force in the animal kingdom.

In these blinks, you'll discover

  * the link between dinosaurs and birds;

  * why birds love to _lek_ ; and

  * why Las Vegas showgirls and birds of paradise face similar problems.

### 2. Rare fossils of feathered dinosaurs show the link between these ancient creatures and birds. 

Do you ever wonder how birds evolved to look the way they do today?

Well, it's actually a long-debated topic. One of the major sticking points in this discussion is that feathers, a major evolutionary characteristic of birds, are fragile and therefore rarely preserved in the fossil record.

Before such evidence was recovered, it was difficult to unravel the ancestry of these winged creatures. But that all changed in the nineteenth century, when a fossil was discovered which linked modern birds to their ancient, evolutionary ancestors: the dinosaurs.

The fossil was an _Archaeopteryx_, a primitive bird-like dinosaur. While it exhibited many dinosaur- and reptile-like features, it also clearly displayed an imprint of feathers along its arm. Since feathers are entirely unique to birds, finding a fossilized dinosaur with them suggested a connection between these two groups of creatures.

From there, even more feather types, like quills and down, were found in the fossil record; with each discovery, new light was shed on the evolutionary story of the feather.

Some of the most groundbreaking finds occurred in the 1990s in the Liaoning province of northeastern China. Here Chinese scientist Xing Xu discovered a variety of feathered _theropod_ dinosaur fossils.

These specimens were game changing because of the manner in which they were preserved; the animals had been smothered by ash during a volcanic eruption, perfectly burying their bodies and preserving the feathers.

From these incredible fossils, we learned a key fact: feathers developed in stages. They started out as simple unbranched quills that gradually grew in complexity and eventually became the much more complicated, asymmetric flight feathers — the type we now recognize.

Not just that, but at each intermediary stage of development, it became clearer that the dinosaurs who bore these feathers were the evolutionary ancestors of modern birds.

Now that we know how birds got their feathers, we'll learn how they began to fly.

### 3. There are two theories to describe how birds began flying – and the jury is still out. 

Dinosaurs developed feathers that enabled them to fly, and birds eventually began using them for the same reason. But how?

Well, scientists have two major theories. First, there's the "ground-up" camp which believes flight began with the fastest theropods, which flapped and leaped much like your average chicken. After all, learning to fly from the ground up is a relatively risk-free evolutionary path compared to jumping from an enormous height and finding out the hard way if you soar or plummet.

But there's a drawback: the incredible difficulty in taking off. Such a leap requires specialized flight muscles and extremely flexible shoulders that provide the necessary force and angle for flying. Both of these features were absent in the feathered theropods.

Then there's the "tree-down" theory, which argues that forest-dwelling creatures first learned to fly by jumping from branch to branch.

This theory overcomes the problem of the lack of flight muscles because simple gravity is enough to get one airborne. In fact, dropping from above is a common way to take to the skies. Consider bats, flying squirrels and all the other creatures that get airborne in this manner.

However, none of these examples use feathers. Whether they're mammals, marsupials or amphibians, these forest leapers all fly by way of membranes — skin flaps that stretch across parts of their bodies and enable them to glide. By contrast, feathers seem overdesigned for such an action. Why evolve such a specialized bit of equipment if a flap of skin could do the trick?

In other words, both theories have their merits as well as their flaws, and scientists are still unsure which to believe. It might even be that feathered flight is a product of both.

### 4. Birds use color to impress, and some even change their looks according to their diet. 

When humans set out to find a mate, they tend to put on their flashiest, most attention-grabbing clothing. Birds are no different. They utilize their bright, beautiful feathers. Sexual selection is the primary driver of the wide array of hues and mating rituals seen in birds the world over.

After all, in the wild, there's incredible competition between males for the best females. Birds can see an extremely broad spectrum of color, so males often use bold and bright plumage to impress potential mates.

They undertake a ritualistic, communal display of flaunting to display themselves and their plumage for consideration. This process is known as _lekking,_ and one of the most famous routines is performed by birds of paradise, who showcase their breathtaking colors in a complex dance.

But why do females prefer bright colors in the first place? Well, there are two schools of thought here: genes and aesthetic appeal. The first is based on the belief that fancy feathers entail a high energetic investment to develop and maintain, which suggests that the bearers of such finery are strong and healthy.

The second is the aptly named "fashion icon" theory that females prefer bright colors and displays purely because they're gorgeous to look at.

But achieving such incredible plumage can be difficult. While many feathers are pigmented naturally during cellular development, colors like yellow and red depend on substances the birds must consume as food.

A good example is flamingos. These famous birds only retain their pinkness by eating algae and crustacea that are packed with beta-carotenes, the same molecules that make carrots orange.

### 5. Birds have a unique way of withstanding freezing temperatures while maintaining their ability to fly. 

Few creatures can survive the severe temperatures and ruggedly icy winds of the Antarctic, but somehow the local penguins manage just fine. How do these incredible flightless birds keep warm in such an unwelcoming habitat?

For starters, they're insulated by a down coat that slows their rate of heat loss by retaining air between its feathers, thereby creating a barrier that heat must pass through to escape. Pockets of warm, dry air are trapped right on the surface of the skin, enabling the birds to handle major fluctuations in temperature. The more static air it can hold, the better insulated a penguin will be.

But if the feathers get wet, this insulative property is immediately lost as the air pockets collapse. That's why there's a layer of _contour feathers_ on the outside of the down coat which protect it from moisture. This is especially important for penguins since they spend a great deal of time in the ocean.

But that's not the only factor making avian insulation unique. Feathers don't merely keep birds warm; they also enable the bird species that _do_ fly to get off the ground.

After all, animals are typically covered in fur for warmth, but this comes with drawbacks. For example, consider the Inuit people who wear furs as insulation. These coats are incredibly heavy and, when they become wet from rain or snow, the weight increases even further.

That makes fur entirely unsuited to birds who fly since the amount of energy required to take off with so much deadweight would be inconceivable. Fluffy, air-trapping down feathers provide a light, waterproof insulation that doesn't get in the way of flight.

### 6. Birds and feathers still provide inspiration for engineers in aviation technology. 

When humans began to attempt flight, they took a hint from birds. This type of approach is known as _biomimicry_ or the emulation of natural patterns and strategies.

The curved or _airfoil shape_ copied in many human flying machines is essential to a bird's ability to fly. Here's why.

If something is going to get off the ground, it first has to overcome a number of problems. Air doesn't pass smoothly around a wing; rather, it spirals unpredictably, changing dramatically with temperature, pressure, angle and the shape of the wing.

Each of these forces drags on the flying object, requiring more power to keep it airborne. To overcome this issue, each feather in a bird's wing is airfoil shaped. Not just that, but together they make up a wing which is itself an airfoil.

That's precisely what Otto Lilienthal found almost 150 years ago. It was the late 1800s, and the Lilienthal brothers had created their first flying machines, setting out to emulate birds. However, they may have been a bit too literal at first, painstakingly gluing every feather in town to their early prototype.

Pretty soon they found that the natural avian airfoil was much more important than every other aspect and, after experimenting with a variety of designs, devised a curved upper surface. As a result, Otto Lilienthal became the first aviator, making over two thousand successful flights before dying in 1896 in a crash.

In the years that followed, aeronautic technology was improved to create a smoother, more efficient ride. Such changes also mimicked nature. Birds can spread their wing tips apart individually, which enables them to make small adjustments that soften turbulence and adjust speed. Engineers used this to develop small flaps on wings to help the aircraft rise and fall in the air.

### 7. Humans use feathers for sexual selection, fashion and performances of various kinds. 

From the men of Papua New Guinea's Obena people to the showgirls of Las Vegas, humans have worn feathers across history and cultures. We even use them for sexual selection — just like birds.

Obena tribesmen in New Guinea are a great example. They adorn themselves with rare feathers taken from birds of paradise for events called "sing-sings." These gatherings provide a platform on which young men can show off to prospective partners. In this way, they're analogous to the lekking in which birds engage.

Once the men have performed, the women evaluate them based on the variety and value of the plumes each was able to gather. In short, the more impressive the display of plumage, the more desirable the man.

Las Vegas showgirls also wear elaborate plumes to advertise their sex appeal. During their shows, the performers wear towering feather headdresses as they dance. Such performances are also similar to the displays offered by birds at a lek.

The dancers even face a similar problem to the male bird of paradise: wearing such an elaborate costume is no easy feat. They can easily weigh over 35 pounds, and if a performer gets her balance wrong, she could lose her headdress, leaving her embarrassed in front of the audience.

The bird of paradise would understand: if he invests too little in plumage he won't be able to attract a mate, but if he invests too much, he'll be too weak to compete for resources or survive attacks by predators. That's why these birds aim for a balance between the quality of their plumage and their skills as dancers, never evading the evolutionary pressure to produce ever more extravagant feathers and attract a superior mate.

### 8. Final summary 

The key message in this book:

**Feathers are ancient and unique structures that have dramatically impacted the course of history and culture. From the first winged dinosaurs to the showgirls of Las Vegas, feathers are one of nature's greatest and most complex innovations.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Genius of Birds_** **by Jennifer Ackerman**

_The Genius of Birds_ (2016) is all about the incredible intellect of these winged creatures. These blinks explain the many ways birds display their intelligence, from tool making and navigation to memory and song.

This is a Blinkist staff pick

_"From pigeons being able to pick out a Monet from a group of paintings to sparrows lining their nests with cigarettes, these blinks enlightened me about the winged beasts we share our world with."_

– Clare, Editorial Quality Lead at Blinkist
---

### Thor Hanson

Dr. Thor Hanson is an award-winning writer and biologist. He's the author of _The Triumph of Seeds_ and _The Impenetrable Forest_. He's been presented with the John Burroughs Medal, the AAAS/Subaru SB&F Prize and two Pacific Northwest Book Awards.

