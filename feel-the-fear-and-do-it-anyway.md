---
id: 53285fcd3635380008c20000
slug: feel-the-fear-and-do-it-anyway-en
published_date: 2014-03-18T08:41:36.000+00:00
author: Susann Jeffers
title: Feel the Fear and Do It Anyway
subtitle: Dynamic Techniques for Turning Fear, Indecision and Anger into Power, Action and Love
main_color: None
text_color: None
---

# Feel the Fear and Do It Anyway

_Dynamic Techniques for Turning Fear, Indecision and Anger into Power, Action and Love_

**Susann Jeffers**

_Feel the Fear and Do It Anyway_ (1987) explains how to avoid letting our fear hold us back from living the life we want and how to begin leading a committed and empowered life instead. It argues that the way we choose to perceive the world and our lives largely determines our reality, and suggests that taking responsibility for our situation and happiness can lead us to find total fulfillment.

---
### 1. What’s in it for me? Learn how to overcome your fears and live the life of your dreams. 

Are you afraid? Does your fear get in the way of you pursuing the life you truly desire?

You're not alone.

In today's climate of global financial insecurity, massive unemployment and general uncertainty about what the future holds for humanity, our fear is the one thing we can count on.

Perhaps even more relevant today than when it was published over 25 years ago, Susan Jeffers' _Feel the Fear and Do It Anyway_ is a self-help classic that over the decades has helped countless people strip their fear of its debilitating power.

Jeffers (once nicknamed "the queen of self-help") presents a course of action for dealing with the fears which cripple us on a daily basis. Avoiding unwieldy terminology, she draws on her own experience and from her knowledge of psychology to share with the reader a plethora of techniques for handling whatever life throws at you.

In these blinks, you'll find out why no decision you make can ever be "wrong." You'll learn how to shape your reality by minding your language. And you'll discover the truth about so-called "fearless" people. Finally, you'll find out why taking action is the key to empowering yourself and commitment is essential to overcoming all fear.

To set the stage for these revelations, let's start by examining what fear really is.

### 2. Excessive fear is harmful, but we can overcome it through education. 

It's hard not to feel envious when we see apparently fearless people, even when those people are actually fictional characters like superheroes or action movie stars. If only we, too, could overcome our fears and rise to any occasion!

The good news is that we _can_ learn how to handle our fears. But to do this, we first have to understand why we have fear at all.

Originally, fear functioned to warn us of potential danger. If, for example, a wild animal approached you, your fear alerted you to the imminent danger and prompted you to either flee or prepare to fight. In this case, the role of both responses is to _protect_ you from harm.

On the other hand, if your fear of strangers holds you back from attending a party, that fear isn't protecting you but is instead an obstacle to living your life as you'd truly like — in this case, as an actively social person.

One reason for the presence of such debilitating fear in adults is that we've been conditioned as children to be afraid. From a very young age, our parents remind us continually of the threats and dangers of the "world outside," and teach us that we must take care to remain safe from harm.

Fortunately, it's possible for us to learn about our specific fears and the basic truths of human fear in general and to develop certain methods to control them.

If, say, you're nervous about your first day at a new job, you could examine the nature of that particular fear. In doing so, you'll likely find reassurance as you come to the realization that nearly _everyone_ is nervous in such situations. And you can probably even come up with examples of people who successfully overcame a similar fear.

As you can see, fear is something we all feel and that all of us can learn to cope with — if we just learn some effective techniques.

### 3. Every fear is based on one general fear of not being able to handle what life throws at you. 

There are as many different fears in the world as there are people, but if we examine these fears, it appears that they're all rooted in one major fear. To find that one fear, we must learn to analyze our fears in terms of three levels. As you'll see, once you get to the lowest level, you'll arrive at the root fear.

At the first level, we determine the thing or situation that we're afraid of. Here there are two categories of fear to consider:

  * The fear of something happening, like the fear of death

  * The fear of something that requires us to take action, like the fear of ending a relationship

In the second level of analysis, the fears that belong to level one can be categorized under more general types. These categories of fear have nothing to do with _external_ situations — they're connected to our inner state of mind.

The categories of internal fear include, for example, the fear of being vulnerable or helpless, or the fear of rejection or failure.

The fear of death from level one belongs to the more general level two category, "fear of being vulnerable." On the other hand, fear of ending a relationship belongs to the category "fear of failure."

As we reach the third level, we can see that behind each of these fears is one major fear: that we won't be able to cope with the challenges life brings.

For instance, fear of being vulnerable or of failing stems from the fear of being unable to deal with that situation should it occur. Which invites the question: Would you still have fear if you believed you could handle everything that came your way?

The positive upshot of arriving at this root fear is that we can overcome every single fear by establishing trust in our ability to deal with any challenge that life may present to us.

In the next two blinks, you'll find out why it's imperative that you push through your fears.

### 4. Pushing through the fear of any situation is the best way to cope with fear. 

You know those people who seem to be fearless, taking risks at every opportunity and never shying away from trying something new?

Well, those people are _not_ fearless. They're just able to confront their fears.

There are a few lessons we can take away from this:

The first is that we cannot avoid fear if we want to experience new things in life. This is because fear is a reflex that's triggered when we're faced with unfamiliar experiences, which means that ridding our lives of fear is an impossible task.

For example, it doesn't matter whether you're a veteran bungee jumper or a professional extreme biker, if you're doing something new and scary your heart will probably pound in your chest. While with experience it's possible to overcome our fears in particular situations, we can't rid ourselves of the feeling of fear in general.

The second lesson is that the feeling of fear will remain until you act on it. For example, the first time the author taught a class, she was afraid, but nevertheless she saw it through. Gradually, after a few more classes, her fear subsided and she began to enjoy teaching.

Finally, confronting and acting on your fears can, in general, dramatically improve your quality of life. Going through life tortured by fear is far scarier than pushing past your fears whenever they arise.

For example, you might choose to never attend parties because you're scared you won't know anyone there. But this decision might also lead you to live a very lonely existence, in which you never meet anyone new. Isn't that prospect far more frightening than simply facing your fear and going to that party?

Also, by pushing through your fears you gain greater self-confidence. For example, not only did the author conquer her fear of teaching, but the very fact of overcoming her fear gave her a new-found sense of self-respect and confidence.

As you can see, the key to overcoming fear is to push through it. Or, in other words, feel the fear but do it anyway!

### 5. If we handle our fears by taking action in the right way, it will no longer pose a problem. 

To reach the point where dealing with fear is no longer a problem you have to first become powerful and self-determined.

Here, being a powerful person means having power over yourself and the power to control how you see the world and react to different situations. In this sense, it means being able to live your life as you want.

Consider, for example, people who complain incessantly about their jobs but do nothing about this problem. Such people behave like victims of their environments. If, however, they empower themselves they'll begin doing something to improve their situation.

For empowered people, the fact that fear cannot be eliminated from their lives poses no problem because they are self-reliant and able to cope with whatever challenges life brings.

So how do we gain such power?

In short, we must _take action_ as inactivity can lead us to think of ourselves as victims of our environments and also to a lack of control over our lives. For instance, if you wish to have a successful career you must start working on it immediately by taking a course or applying for positions. By taking action and control of your life, you'll begin to feel a strong sense of empowerment.

However, taking action also means actively transforming your attitude and one way to do that is to change the vocabulary you use. The specific language we use to express ourselves has a powerful effect on our subconscious, which tends to register whatever it "hears" rather than the actual truth of a situation. So, for instance, if you say "I won't" instead of "I can't," or "next time" rather than "if only," you will feel a boost in confidence and act accordingly.

In the last seven blinks, you'll discover how you can push through your fears and lead a powerful, fulfilled and fearless life.

### 6. By expanding your comfort zone you empower yourself to deal with fear. 

If you think back to when you were a child, you'll probably remember many things you were afraid to do. But as you progressed through childhood, you faced and overcame many of these fears, ultimately outgrowing them.

So why does this seem to stop in our adult lives?

As adults, each of us has established a personal _comfort zone_, which strongly influences the actions we take and the decisions we make in everyday life.

For instance, if making social contact is outside of your comfort zone, you might feel quite comfortable with asking a stranger for directions yet very uncomfortable when striking up a conversation with a stranger at a party.

But if we actively expand our comfort zones we change our perspective on what we're capable of — and this increases our power.

In order to expand your zone you have to do something that feels uncomfortable every day. For example, at the next party you attend, try talking to someone you've never met before. Doing that will show you that you can handle this situation and thus you won't feel so uncomfortable the next time you do it.

But what if you take the risk and step outside your zone, only to see it backfire because, say, the person you talked to was unfriendly? In that case, you can be satisfied that you tried your best and took action. Because a larger comfort zone leads you to be more active and independent, expanding it by taking action means you inevitably become more powerful. In other words, the more unfamiliar and frightening things you try, the more power and confidence you will acquire.

As we've seen, becoming more powerful and gaining more control of your life can be accomplished by pushing past your fears and expanding your comfort zone and this, in turn, makes it easier to deal with new and frightening situations in the future.

### 7. To empower yourself you need to take full responsibility for your life. 

We've all wondered what our lives would be like if we'd had better luck. And many of us wish we had better jobs, happier relationships and, in general, a more exciting life.

But no matter what hand life has dealt you, you have to take full responsibility for your current situation. This means not blaming anyone else for your life or how you feel about it. Ultimately, your happiness in life depends on how you _choose_ to feel about any given situation.

For example, if you despise your work there's no point in blaming your boss or your colleagues as it's you who has decided to keep the job. Rather than pointing the finger you should make a decision: either you're going to quit or start focusing on its positive aspects.

Taking full responsibility means that you accept even these negative aspects of your life as just another part of an ongoing learning process. Try not to allow them to hold you back from doing those things that will help you to grow and thrive — such as taking risks or being active.

For example, if you feel that you're not being treated fairly by others you can still take responsibility for how you interpret the situation and instead accept it as a challenge and an opportunity to show what you're capable of.

But if changing your perception of such a situation isn't enough to empower you, you have to be prepared to make active choices to improve your life. While it's true that you deserve to be treated with respect, ultimately you're the only one who can decide to be happy or not and the only one who can take action to attain that happiness. So if you want to have healthier relationships, put an end to the harmful ones in your life. If you want more friends, put more energy into your existing friendships or get out there and meet some new people.

You are in complete control of your life.

### 8. To be free of fear, we need to learn how to truly give. 

Most of us think of ourselves as generous people who give freely, but if we're honest with ourselves, we realize that we usually expect something in return.

This is because we're often afraid of not having enough for ourselves. We greatly fear lacking things like money and love in our lives, so giving freely is difficult for us. For example, if we invite a friend over to dinner, we're — perhaps subconsciously — expecting the gesture to be returned, or even just a simple "thank you."

Yet despite this fear it is possible to become a truly generous person.

How?

First, you have to learn to see how abundant your life is. By realizing how lucky you are to have so much, you enable yourself to give genuinely. For example, giving money to people occasionally should not trigger your fear: even if the worst happened and you lost every last cent you had, you'd probably be able to survive thanks to your skills and your friends.

To notice how fortunate you are it helps to keep a daily record of everything that's good in your life and the positive things that happen to you, like the kind smile a stranger gave you on the bus.

Once you've made yourself aware of how fortunate you are, the next step is to practice giving. Only by practicing will you improve at being a giver and eliminate the fear of not having enough for yourself. If you regularly give thanks, time and money, you'll begin to see the positive effect it has on you and others. In turn this will reduce any fear you have that giving means having less.

Freeing ourselves from the fear of scarcity not only enables us to be genuine givers, pleasing ourselves and others in the process, but it's also a significant step towards conquering fear in general.

### 9. Being positive empowers you and helps you to shape your own reality. 

Many people worry constantly about all kinds of things that might go wrong in their lives. They worry about getting stuck in rush hour traffic on the way to work. They worry that something horrific might happen to someone they love.

But why do we worry so much? Studies have shown that only ten percent of what we worry about actually transpires, which means that most of our worries are completely unfounded. If you think about it, adopting a positive attitude towards what might happen is actually a more realistic perspective on life than being a pessimist and worrying all the time.

In fact, the attitude we take has a great effect on whether a situation turns out well or not. For that reason, adopting a positive attitude actually empowers us to create our own positive reality.

Using positive words, for instance, makes us physically stronger, while negative ones weaken us. For example, if you hold your arm out and repeat the words "I am weak" over and over, then have someone try to push your arm down, your resistance to their efforts is weaker than if you affirm your strength beforehand by repeating the words "I am strong."

But overcoming your negativity and becoming a positive thinker requires a lot of commitment and practice. You have to use certain "tools" and integrate them into your daily life — like reading self-help books or collecting self-affirming quotes.

Yet our lives will always contain negative things like problems and challenges. Positive thinking doesn't mean denying the reality of those things; rather, it provides you with the strength to actively engage with them. In doing so, you generate in yourself the power to change your life for the better and the belief that you can lead a happy existence whatever your circumstances.

So, by committing to positive thinking you empower yourself, which reduces your worry, eliminates fear and allows you to enjoy life.

### 10. To change your life for the better, you need to surround yourself with strong and supportive people. 

As you begin to improve your life by using the tools and techniques we've discussed, the people in your life will start to notice the change in you. Some may even be disturbed or upset by it. But there's no need to fret if this happens: they'll probably begin to change for the better themselves. In the meantime, however, it's important that you do what you can to help them deal with the change.

One way to do this is to note their feedback: if they are supportive of you, then you should acknowledge that support. If they respond negatively to your ongoing transformation, try to use a win–win strategy in your discussions so that everyone involved can find positivity in the change.

For example, if your mother has concerns about the changes she sees in you, instead of attacking her thank her for her concern, assure her that she needn't worry about you and ask for her support in your efforts to change. This will make her feel included in the process and the self-confidence you demonstrate will increase her own confidence in you as well.

If you find that some friends or partners won't give you their support, it's likely they will just end up holding you back from accomplishing the positive change that you desire. If you notice they are simply unable to come around to your more positive way of thinking, it's probably time to end the relationship. There's nothing to be gained from clinging to it as you'll eventually begin to resent them for not giving you their support.

Just as it's important to let go of negative relationships, it's equally valuable to let new people who inspire and support you into your life. It's extremely useful on your path towards change to have someone in your life who's more advanced in their development than you. For example, if you meet such a person in a self-help class, he or she can serve as an inspiration for you and advise you in times of struggle.

### 11. If you see every choice in life as a positive one, you will lose your fear of making decisions. 

One major fear we have is that of making the wrong decisions in life. Should I end this relationship or continue trying to improve it? Should I take that new job or keep looking for something better? Until we begin to view our decisions in a different light we'll never be free of that fear.

The main problem is that most people consider their decision making from a negative perspective: they focus on what might happen if they make the "wrong" choice.

Imagine, for example, that you've been offered a job. In this case, you'd probably consider the potential negative consequences: "If I take the new job, perhaps I won't like my colleagues," or "If I stay at my old job, I'll probably never get a promotion."

The assumption behind such thinking is that there's a right decision and a wrong one. Furthermore, since we're unable to predict how things will turn out — like whether or not our new colleagues will be friendly — we'll always fear making the wrong decision.

However, by accepting that there are _no_ wrong decisions, just opportunities to learn and grow, you'll understand that you cannot lose. Every path we take in our lives offers new experiences and opportunities to learn about ourselves, regardless of its actual destination. For example, if you do decide to take that new job and the result is that you're unhappy there, you've still learned something new and valuable about what kind of job you want.

This win–win approach eliminates the fear in making decisions. Seeing opportunities in whatever life brings us allows us to be less afraid in general as we'll have learned that we can handle _any_ situation. For example, if you happen to get fired from your new job, the following stretch of unemployment will show you that you're able to survive and thus any fear of being fired will be eliminated.

### 12. Commit 100 percent to every area in your life to become less fearful. 

What would your idea of perfect life look like? Perhaps you imagine a life filled with friends, interesting experiences and activities.

Why, then, aren't you already working towards creating such a life?

By filling your life with many different activities that you're fully engaged with and committed to, you won't be as afraid of losing just one of them. For instance, if what's important to you are your family, friends, hobbies and work, be fully committed to all of them. This will create a strong, safe structure for your life, so the idea of losing just one of these will not cause such fear.

For example, if you're emotionally dependent on your partner and neglect your friends, hobbies and work, the idea of losing that partner will always be extremely frightening. If, on the other hand, you're fulfilled by every single aspect of your life because you've fully committed to them, you're less likely to be devastated by losing your partner as everything else in your life is still intact.

But what if you simply don't find a particular area of your life fulfilling? In that case, just _acting as if_ you were fully committed will help to enhance the quality of it.

For example, temporary jobs in particular can be unfulfilling because we find it difficult to commit to a job that we'll soon have to leave. However, you might find working there more enjoyable if you at least _act as if_ you have an important role to play. You could approach it more positively by, for instance, decorating your office, engaging seriously with your colleagues and giving your daily tasks your full commitment. Commitment doesn't have to mean that you'll stay in that job forever.

Commitment is the key to a rich and happy life; it shields you from fear and makes you stronger in every way. Even if you just _act as if_, you will see astonishing effects in all aspects of your life!

### 13. Final summary 

The key message in this book:

**Learning how to handle fear will allow you to reach your goals in life. To do so, you need to gain trust in your ability to cope with whatever life brings you. Being active and committed will empower you to gain that trust in yourself and help you to overcome the negativity that holds you back from accomplishing your goals.**

Actionable advice:

**Start thinking positively.**

Use positive vocabulary, listen to affirmative audiobooks, repeat affirmations and quotes that make you feel good. Committing to positivity in this way will not only empower you and teach you confidence, but will also help you to worry less and enjoy life.

**Leave your comfort zone.**

Try to do something that's outside your comfort zone every day, as the more unfamiliar and scary things you try, the more your comfort zone expands. If you overcome each small fear one at a time there will be less and less that holds you back in the future.

And even if your efforts don't pan out as you planned, the fact that you took the risk and did something unfamiliar and challenging means that you've increased your power. The next time you try to do the same, frightening thing, you'll find your confidence has improved tremendously.

**Suggested further reading: _The Six Pillars of Self-Esteem_ by Nathaniel Branden**

_The Six Pillars of Self-Esteem_ shows us that building confidence in ourselves is a just matter of taking a few simple steps. Healthy self-esteem is something we can achieve not just as individuals, but in our relationships and communities too.
---

### Susann Jeffers

Susan Jeffers, PhD, was an American psychologist, public speaker and bestselling author who published many books about fear, including _Feel the Fear and Beyond_ and _Feel the Fear Guide to Lasting Love_.

