---
id: 58186cfd9c08690003cbdc8c
slug: felt-time-en
published_date: 2016-11-06T00:00:00.000+00:00
author: Marc Wittmann
title: Felt Time
subtitle: The Psychology of How We Perceive Time
main_color: 71BDCA
text_color: 46757D
---

# Felt Time

_The Psychology of How We Perceive Time_

**Marc Wittmann**

_Felt Time_ (2014) examines how your brain processes time. These blinks present fascinating facts and theories about how our bodies perceive time, and offers advice on how to make the most of the present moment, deal with boredom and control the pace of our lives.

---
### 1. What’s in it for me? Find out how your body’s internal clock ticks. 

Tick, tock, tick, tock — whatever we do and wherever we go, it seems that the constant ticking of a clock is following us. We may lose track of time for a while, say, when we have an exciting conversation or watch a thrilling movie, but the ticking will then get louder, slower and more irritating when we are in a doctor's waiting room, sitting and waiting . . . and waiting.

We all possess an internal clock that sometimes runs quickly and unnoticed, while at other times it dawdles at the pace of a tired donkey, carrying the heavy burden of time. And indeed, we carry this same burden every day!

In these blinks, you will learn about your inner psychological and physiological clock. You will learn why we sometimes feel that time is flying by, and why sometimes the minutes seem to last forever. Explore how your brain perceives time and, last but not least, how you can best organize your time.

You'll also learn

  * how long chickens can delay gratification;

  * why life is a series of three-second intervals; and

  * how we perceive time during a car crash.

### 2. We possess psychological and physiological clocks that help us stay tuned to the passage of time. 

Before the invention of clocks, how did humans gauge time? After all, clocks have only existed for a tiny fraction of human evolution's long history.

Luckily, we have psychological mechanisms that help us keep track of time.

Oxford psychologist Michel Treisman put forward the pioneering idea of a psychological clock in the 1960s. According to Treisman, our brains have a pacemaker that emits pulses at regular intervals, and the pulses are collected by a counter in our minds. Our brains then determine the passage of time by the number of pulses the counter detects.

Treisman argued that the counter registers time pulses only when we're paying attention to the time — for instance, when we're waiting for something. When we're distracted and not thinking about the time, on the other hand, fewer pulses are counted, making time seem to pass more quickly.

A more recent scientific theory of our psychological clock posits that we can estimate the temporal duration of events by gauging the amount of intellectual and emotional exertion they require.

Consider how new experiences often feel like they last longer because they demand greater faculties of perception, thought and emotion.

And guess what? We've been blessed with not just a psychological clock, but also a physiological one, known as our _circadian rhythm_.

The circadian rhythm is a biological process of changes that happen to our bodies in response to daily light cycles. Cognitive performance is one of the things affected by our circadian rhythm — for example, many cognitive operations hit their peak before noon and slow down as the day progresses.

Fascinatingly, our bodies follow circadian rhythms even if the sun isn't visible.

In the 1960s, psychologist Jurgen Aschoff conducted a set of experiments in which volunteers were kept in a room with no natural light. Despite being shut out from the outside world, Aschoff found that the volunteers' sleep schedule and body temperature still followed a circadian rhythm.

### 3. Humans are able to trade present gains for future satisfaction, and this ability to wait reaps many rewards. 

Imagine you have the choice of getting one slice of pizza now, or two slices of pizza later. What would you choose? We frequently consider decisions like these — about how long we'll have to wait until our next reward — in our day-to-day life.

Within the animal kingdom, this ability to delay gratification is most developed in humans. Pigeons and chickens, for instance, can at best wait for a few seconds. Only the great apes like chimpanzees can delay rewards for several minutes like we can.

You delay gratification like this all the time. Say you decide to skip happy hour with friends tonight and instead go running to train for an upcoming marathon; you're forgoing immediate rewards for potentially greater rewards in the future.

But certain situations make us more or less likely to swap, and the nuances of our willingness to trade present pleasure for future satisfaction can be studied through monetary dilemmas.

For example, research has shown that when offered $45 now or $50 next week, most of us will go for the $45 now, effectively paying a $5 premium for the privilege of getting the money immediately. As you might imagine, though, the bigger the premium, the higher the chances that we'll be willing to wait.

Our ability to wait seems to have big consequences: studies have shown that adults and children who are better at delaying gratification tend to do much better in life overall.

In 1988, American psychologist Walter Mischel tested the impulsiveness of 500 children between the ages of four and five. In the study, he gave them a marshmallow and told them they could either eat it immediately, or wait and get a second one in 15 minutes.

Mischel found that the children who were able to wait for the greater payoff went on to do far better in life than their more impulsive peers, achieving higher test scores in school, and eventually ending up with better career paths too.

### 4. For our brain, life is a series of three-second intervals, which are connected by our short-term memory. 

If you've ever seen the musical RENT, you might remember the famous song that asks how one measures a life — in daylights, in sunsets, in midnights or cups of coffee?

Well, researchers believe that our brains conceive of time as a series of intervals, each one lasting approximately two to three seconds.

And according to analyses performed by German psychologist and neuroscientist Ernst Poppel in 1988, many verses in songs and poems are comprised of spoken units that also last approximately three seconds.

Why? Our brains seem to be wired to find verses of this length most appealing, for both visual and auditory experiences, because this is the very length of time that our brains register as a "unit of now."

Our short-term memory acts as a bridge that connects the string of present moments into long-term memories. So, when you listen to a song, the three-second verses link together to gradually build the sum of its parts, thus becoming the complete song.

Or when you're reading a book, the information you're reading now connects to a broader story.

Interestingly, the short-term memory of what you read only lasts for a few minutes after reading it. After that, the short-term memories are stored in our long-term memory, structured into the broader idea.

> Long-term memory is essential for our sense of self, as it enables us to know who we are.

### 5. In terms of how time is processed in the brain, people may differ, but situations don’t. 

People live their lives at wildly different paces: some scurry around frantically in a whirlwind of action, while others take forever just to get ready for work in the morning.

Research indicates that this is not just a perception: we all seem to process time at our own pace!

Cognitive psychologists Ira Hirsch and Carl Sherrick conducted an experiment in the 1960s, in which subjects were played two notes in quick succession. Participants were then asked whether the lower or higher note had sounded first. If they answered correctly, the time interval between the two notes was reduced, until the subjects could no longer detect which note had been played first.

With this methodology, the researchers discovered the threshold at which subjects could accurately discern the order of the two events — usually around 20 milliseconds. But they also found out that this threshold varied from subject to subject.

In other words, one person might recognize two sounds played with 23 milliseconds between them as two separate events, while another person will hear them as just one. This seems to indicate that even after experiencing the exact same amount of minutes and seconds, the first person would have perceived more time to have passed than the second.

Though we may each process time at distinct speeds, objective observation shows that our individual perception of time doesn't depend on the context.

This may seem counterintuitive at first, because when evaluated subjectively, the passage of time does seem context dependent. For example, many people who have experienced a car crash report that the moments before and after the crash felt like they happened in slow motion.

To investigate the variance between our subjective and objective perceptions of time, neuroscientist David Eagleman took subjects to an amusement park. There, he measured how quickly they could take in and process visual information while they were falling from a 31-meter-high tower. The idea was that if time were to objectively slow down when they fell from the tower, their performance would be better than in a laboratory setting.

While Eagleman couldn't detect any significant differences between the subjects' performance in the two situations, the participants still reported that time spent falling from the tower felt longer, highlighting the difference between objective and subjective perceptions of time.

> Some brain-tumor patients report that everything around them happens very quickly. That's because their brain is unable to process information rapidly.

### 6. The quicker the perceived passage of time while we experience it, the longer it feels in retrospect. 

In other words, the length of a certain time period when we recall it from memory is different from how it feels when we actually experience it.

When you reminisce about vacations, you might recall how the first days usually feel longer than the last ones. Why does that happen?

When we look back on past events in our memory, a period of time full of activity and change feels much longer than the same amount of time spent routinely.

On the first days of vacation, you're getting adjusted to new surroundings and it's a flurry of change, which takes up more room in our memory. By the last few days, however, you feel you've settled into a routine, and we thus recall them as passing by more quickly.

Or if you think back to an hour you spent in a doctor's waiting room, that memory isn't going to take up much space in your mind. After all, waiting time is often "dead" time from which you can't really recall anything in particular — because not much happened.

But let's say you'd met an interesting stranger and struck up a conversation during that hour. Then you'd think back to that memory and it would feel longer because you were having a fuller experience from which you might recall certain conversation topics or jokes.

During the experience itself, though, the opposite is true. Everyone knows that time flies when you're having fun, while a dull, routine day sometimes feels like an eternity.

Let's go back to the vacation example — but this time, think back to how time felt during it. The first few days, when everything was new and interesting, probably seemed to sprint by. But by the last few days, when the new had already become the familiar, time seemed to pass more slowly.

And when you're actually sitting in the doctor's waiting room with little to distract you, time really seems to come to a complete halt.

> _"It is not that we have a short time to live, but that we waste a lot of it …"_ — Seneca, On the Shortness of Life.

### 7. Get organized, separate work from free time and learn the art of mindfulness to feel more at peace with the pace of life. 

In the modern age, life can seem to move at breakneck speed. Everyone's rushing to meet deadlines, attend events, make time for hobbies and so on. And while this frenzied pace has its rewards, it can sometimes create a toxic climate of stress.

Help yourself out by recognizing that stress is the result of feeling out of control. Then, use tools like to-do lists, schedules and manageable goal-setting to help you expertly navigate the tasks at hand.

Another key to better controlling the pace and quality of life is separating work from free time.

Say you spend all day at the office, then go home and still continue to perform job-related tasks, like reading and writing work e-mails.

You might feel like you're getting ahead by getting such tasks out of the way, but in fact, it's much healthier to clearly distinguish between work and free time. Relaxation and leisure are not only essential to your health and happiness, but when you do go back to work, you'll be ready to take on challenging tasks again. Finally, use mindfulness meditation techniques to concentrate on the present moment. This means paying attention to experiences as they happen and acknowledging the good and bad thoughts that arise in our minds without evaluating them.

If you've ever taken a yoga or meditation class, you may have heard something along the lines of, "imagine your mind is the ocean, and you see different boats sailing by — maybe jealousy, anger, insecurity — but instead of fueling any of the boats, just wave and let them sail right by you."

Many medical psychology studies show that mindfulness like this increases tolerance to pain, reduces stress and decreases the effects of aging on intellectual performance.

### 8. Final summary 

The key message in this book:

**Humans have a series of physiological and psychological capacities to perceive time and cope with its challenges. Short- and long-term memory are crucial to transforming these perceptions into subjective experiences and give rise to numerous fascinating phenomena concerning our perception of time.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Time Warped_** **by Claudia Hammond**

_Time_ _Warped_ is about that enduring mystery: our perception of time. Using research from the fields of neuroscience, biology and psychology, Claudia Hammond investigates the many reasons why, on one day, time appears to pass rapidly, while on another, it seems to grind to a halt. In addition, _Time_ _Warped_ suggests ways in which we can control our individual experience of time.
---

### Marc Wittmann

Marc Wittmann is a German psychologist conducting research at the Institute for Frontier Areas of Psychology and Mental Health.

