---
id: 5e3a81c26cee07000682afed
slug: figuring-en
published_date: 2020-02-06T00:00:00.000+00:00
author: Maria Popova
title: Figuring
subtitle: None
main_color: None
text_color: None
---

# Figuring

_None_

**Maria Popova**

_Figuring_ (2019) traces the intricate web that connects important figures from human history, from German poet Johann Wolfgang von Goethe and inventor Nikola Tesla to America's first female astronomer Maria Mitchell and poet Ralph Waldo Emerson. These blinks pick up the tapestry of these different lives, trace the impact that they had on the course of history, and reveal the secret driving force that unites them all.

---
### 1. What’s in it for me? Discover a world full of surprising connections. 

We never know how far our words and deeds might travel. How could Johannes Kepler, the world's first astrophysicist, have anticipated in 1603 that his allegorical story about a moon voyage would lay the groundwork for future science fiction? How could Maria Mitchell, America's first female astronomer, have known that she would become an icon for women in the sciences when she was simply following her passion?

The truth is, our lives echo long after we're gone, ringing out in myriad directions. In these blinks, we'll follow these echoes and trace the lives of a number of historical figures, and discover how they interweave in often surprising ways. 

In these blinks, you'll learn

  * what Johannes Kepler foresaw about the Apollo mission;

  * how Maria Mitchell achieved fame as an astronomer; and

  * what Lord Byron's daughter accomplished as a mathematician.

### 2. Human fates are interwoven, across time and space, in an ever-expanding web. 

If you picture your own life story, and those of others, as straight lines, moving only forwards, then you have the wrong idea. In reality, our lives and collective histories branch out in myriad different directions, in an infinitely connected web. We intersect with other people and things in unimaginable ways.

One reason we overlook this complex web is that we fail to recall how we're all fundamentally connected. Everything that exists is made of the same star stuff. _Everything_. From every idea that Albert Einstein had to the preservatives that keep his brain floating in its jar. From every cell of Galileo's pointing finger to the molecules of gas and dust that form the rings of Jupiter. From the opening notes of Beethoven's fifth symphony to the sound of a voice you love. All of it exploded into being with the Big Bang, from a single source, 13.8 billion years ago.

If we know this essential truth, why do we think of ourselves as completely distinct, separate, _atomised_? For instance, some of us become so focused on personal development that we forget that we are also shaped in important ways by other people in our lives. Indeed, the poet Walt Whitman knew how connected we are when he wrote: "every atom belonging to me as good belongs to you."

Our great ideas and advancements, in art, science and philosophy, didn't evolve separately, but through great webs of connectedness. Many of these connections are invisible and barely traced. 

For instance, the prototypical "science fiction" stories of satirist Lucian of Samosata, from 2AD Roman Syria, may have permeated Western consciousness and set in train reflections about traveling to outer worlds, in many different minds. Perhaps, finally, these reflections culminated in actual space travel.

Many of the ideas that bloomed into revolutions or shifts of understanding first developed in obscurity, gestating over many ages, between different disciplines. Collecting like individual drops of water along different channels, they eventually made a great torrent. 

This is best exemplified by the struggles for equality between races, genders, classes, and sexualities. Pioneers like the first American astronomer Maria Mitchell or early abolitionist Frederick Douglass sparked movements that, over the course of history, contributed to great change, affecting causes far beyond their own.

### 3. Through his scientific discoveries and his science fiction, Johannes Kepler shaped the future. 

The life and work of Johannes Kepler, the world's first astrophysicist, is striking in its own right. But the way his work in the 1600s would link up with other scientists and writers over the following centuries is equally remarkable.

Kepler's astronomical discoveries fed into the future of many different scientific fields. At a time of widespread ignorance and superstition, he was one of the first adherents to Copernicus's heliocentric model of the universe. He contributed key insights to the field of astronomy. For example, he was the first to suggest that the planets orbit the sun, not in circles, but in _ellipses_, the first to develop a scientific method of predicting eclipses, and the first to demonstrate that physical forces move the planets and stars in calculable paths through the night sky. 

Sixty years later, Newton went on to refine Kepler's thesis on physical force to develop the foundations of Newtonian gravity. Then centuries later, the mathematician Katherine Johnson drew on the laws that Kepler had first discovered. She used them to compute the trajectory that landed Apollo 11 and the first man on the moon.

Remarkably, though it transpired a long way into the future, Kepler foresaw much of that moon landing in his "science fiction." In his story _The Dream_, a young astronomer voyages to the moon and discovers a race of lunar dwellers who believe that Earth orbits their planet. The story is a gentle, allegorical way of reminding denizens of Earth that their certainties about their own planet's place in the universe might not be certainties after all.

Kepler's work was one of the first science fiction stories, before the genre even existed. But he also accurately predicted that a "spaceship" would need to factor in the gravitational field that would weigh it down to Earth when it took off, but would require hardly any force once in the gravity-free "aether" of space.

In conceiving the idea of lunar travel, Kepler set in motion the forces that would eventually make it a reality. Centuries later, in a 1971 conference on space exploration, American author Ray Bradbury articulated this historical gestation process when he said: "It's part of the nature of man to start with a romance and build to reality."

### 4. Her unique upbringing and surroundings allowed Maria Mitchell to become America’s first female astronomer. 

Born on the island of Nantucket in 1818 into a family of Quakers, Maria Mitchell was a brilliant child captivated by the cosmos. But in the 1800s, science was still dominated by men. In order to become a serious astronomer, Mitchell had to fight an uphill battle.

But on the evening of October 1st, 1847, she made a discovery that would transform her life. Scouring the heavens with her beloved telescope that night, she spotted a tiny, brilliant speck moving across the sky — a new comet! She knew the cosmos like the back of her hand and she'd never noticed this bright little intruder before. Earlier that year, Europe's chief patron of the sciences, the King of Denmark, had announced a prize for anyone who discovered a new comet. The victor would be awarded with a gold medal worth 20 ducats — a fortune back then.

Mitchell was first reluctant to claim the medal, but her father encouraged her to convey her discovery to the Harvard Observatory, the prize-giving authority. However, stormy weather delayed the postal service and before her letter could arrive, an astronomer in Europe also observed the comet. Thankfully, the authorities were generous enough to permit Maria her victory.

Maria would go on to become America's first female astronomer, the first woman admitted into the American Academy of Arts and Sciences and the first woman to be employed by the government for a specialized non-domestic skill — as a special "human calculator" for nautical purposes.

Her background and family contributed enormously to Mitchell's success. She was lucky enough to grow up in an uncommonly loving family, with a very erudite mother and an unusually present father. These were all untypical for the time. Her father treated her as an intellectual peer and indulged every curiosity she had, while being forward-thinking enough not to privilege his less gifted sons. And as Quakers, the equal education of boys and girls was insisted upon.

Finally, growing up on seafaring Nantucket meant living in a place where mathematics was a practical necessity for sailors and whalers navigating the Atlantic, not just a lofty intellectual pursuit. During its long, dreary winters, Maria also had plenty of time to cultivate her interest in astronomy indoors. Mitchell's unique upbringing and surroundings remind us that no one is an entirely "self-made" person — all of us are shaped by an intricate web of family, place, and culture.

### 5. Across the ages, many people of genius have considered beauty to be allied with truth. 

The 19th century poet Elizabeth Barrett Browning thought that there were different types of beauty: in the arts, obviously, but also in physics and in morals. They were all reflections of an essential truth about the universe. Throughout the ages, many have shared this belief.

Galileo, too, was someone who considered the truth he found in his science to be beautiful. He refuted the prevailing geocentric model of the universe that held that the God-created Earth was at the center of everything. He discovered the first evidence against the unscientific dogma of the time when he was observing the different phases of Venus. If the prevailing opinion had been correct, then Venus could not be illuminated from the vantage point of Earth, as that would have put it on the far side of the sun. But through his telescope Galileo could see Venus all aglow in dark space. And representing such a powerful, controversial truth, he found the planet all the more beautiful.

The connection between beauty and truth was also expressed by the 19th century American poet Ralph Waldo Emerson. In his lectures on the subject, he argued that beauty naturally attracts curiosity and wonder, inviting us to enquire what lies beneath its surface. He saw beauty as something akin to a language — a code for transmitting truth, as intrinsic to the universe as mathematics or physics.

Maria Mitchell, who we met in the previous blink, attended Emerson's lectures. She too sometimes thought that she was drawn to gaze at the cosmos in search of its scientific truth mainly _because it was so beautiful_.

The meeting of beauty and truth can also be found in the struggles for social justice. Take for instance, Frederick Douglass, the famous abolitionist. In the 1860s, he delivered a lecture entitled "Pictures and Progress", which examined the links between figurative art and political reform. In it, Douglass argued that the emergent medium of photography would help upend inequality, because it allowed people to present the "real" in contrast to the ideal. Photography could make visceral the rift between our flawed reality and the one we aspire to. Douglass saw a new kind of beauty in this power — a power that would stir us into taking action.

### 6. Throughout history, the truth of people’s amorous natures have always been more complex than the labels put on them. 

In the 19th century, people who were deemed to have "irregular," non-heteronormative relationships were sometimes described as "Uranian." Derived from the name of the Greek goddess Aphrodite Urania, who was created out of the god Uranus' testicles, Uranian referred to someone of the "third sex." Today we'd recognize people described by this term, broadly speaking, as _queer_. 

However, because of the strictures of the past, many people throughout history have concealed their natures from others and even _from themselves_, too. History is full of relationships that were never quite realized, or defy easy definition.

The authors Herman Melville and Nathaniel Hawthorne developed a bond of this sort. Reading Hawthorne's short story collection "Mosses from an Old Manse in the Literary World" in 1850, Melville became enchanted with the older writer. Reviewing the book, Melville wrote: "A man of a deep and noble nature has seized me…" Melville was infatuated with Hawthorne's work and, by extension, the man himself, whom he'd come to know at a literary gathering not long before. 

On Hawthorne's side, he was likewise drawn to the younger writer, and was moved beyond words when Melville dedicated his novel _Moby Dick_ to him. But the friendship cooled when Melville's letters to Hawthorne became more feverish. His ardour was too much for the more reserved Hawthorne, who began to withdraw. The warm conversation and mutual adoration between the two men is preserved in their letters to each other, and what happened between them can be described as a sort of romance; but we'll never know if the era and convention kept it to this, or if there was more to their story.

Maria Mitchell's deep bond with society beauty Ida Russell was equally undefinable. Mitchell had a highly reserved nature and her personal relationships seemed to have been kept on a strictly platonic level. Her relationship with Ida Russell, a beautiful, intelligent socialite, however, was different. Unlike her many other female friends, Ida inspired possessiveness and jealousy in Mitchell, traits normally out of character for her. But aside from the obvious attraction to Ida revealed in Mitchell's letters, nobody knows what happened between them. Like many people of their time, the precise shape of their relationship was nebulous, as convention forbade a more open connection.

These impulses — Melville's, Hawthorne's and Mitchell's — are what we might describe today as queer. Many great minds throughout history have formed such mysterious, mutually inspiring and highly unusual relationships with each other. One thing is certain: the complicated business of intimacy can't be summed up in a word like "queer" or "Uranian."

> _"No one ever knows, nor therefore has the grounds to judge, what goes on between two people..."_

### 7. The meeting of literature and science was personified by mathematical prodigy Ada Lovelace. 

The child of the Romantic poet Lord Byron and a mathematically brilliant aristocrat, Baroness Annabella Milbanke, Ada Lovelace, was born in 1815.

Byron and Milbanke's marriage didn't last long, however, after Byron was found to be having an affair with his half-sister, Augusta. Byron fled to Italy, then Greece.

Meanwhile, Ada was raised by her mother, who discouraged the poetical side of her nature and pushed her towards science and math. Ada was a quick learner. By the time she was a teenager, she'd already devised a plan for engineering a flying apparatus. However, her mother couldn't erase her poetical side entirely, which was an equal part of her.

Far away in Greece, her father, the great English poet of his age, would sometimes pick up a portrait of her and sigh deeply, wondering if his daughter would ever come to know him. Back in England, Ada felt this too. In a bout of defiance, she told her mother: "You will not concede me philosophical poetry. Invert the order! Will you give me poetical philosophy, poetical science?"

It was this refusal to give up her dual nature that equipped her for her life's great achievement. When she was 19, Ada began attending salons hosted by an eccentric mathematician, Charles Babbage. Not long after they'd met, Lovelace and Babbage began working on something called the "Analytical Engine", something that is widely considered to be the world's first computer. 

During their collaboration, Lovelace translated a scientific paper by an Italian military engineer entitled "Sketch of an Analytical Engine." Written in her 65-page long footnote to this paper was, essentially, the world's first computer program. She was 27 years old and the year was 1843. The feat had involved Ada Lovelace's world-class talent for mathematics — in the formulae she developed — _and_ her imagination. Without this latter component, the ability to see things that don't yet exist, it is unlikely that she'd have conceived of such a radically forward-looking invention. 

Her ability to create mixed with her mathematical brilliance — the components she'd inherited from both of her parents — and contributed to this leap forward for science and humanity.

### 8. Goethe left behind a surprising scientific legacy. 

The famous German poet Goethe also considered himself a scientist. Like many of the other people we've met so far, the two facets of imagination and reason came together to form Goethe's genius. Although he left his mark mainly on the world of literature, his scientific legacy is surprising.

For instance, he is partly responsible for the names for clouds we use today.

When Luke Howard, a young amateur astronomer, published a classification system for clouds with Latin names instead of English ones, he was met with furious opposition by the British scientific establishment. Goethe, who followed scientific developments closely, stood in solidarity with the young Englishman, arguing that clouds were common to all humanity and deserved to have names accepted in all languages rather than "local translations." He even adopted parts of Luke Howard's essay in a series of short poems that made use of the Latin names he had given them. Through Goethe's powerful patronage, these names survived as the ones we use today all over the world: _cumulus_, _stratus_, _cirrus_, etc.

However, despite his interest in science, it wasn't Goethe's true calling. He was a scientist in the same way Einstein was a violinist — passionate but mediocre. His great achievement, if not as a scientist, was as someone who popularized science and broadened the imaginations of scientists. Indeed, though some of the scientific essays he wrote during his life were flawed, he drew interest to areas where aesthetics and science came together.

One of the most famous examples of his scientific legacy can be found in an invention by Nikola Tesla. The idea of the self-starting alternating current motor came to Tesla in a vision — almost a hallucination — while he was sitting in Budapest Park, in Budapest. As he sat in the park, the sky lit up with a magnificent sunset, which inspired him to recite a stanza from Goethe's _Faust_. As he spoke the lines, he had a sudden vision of a rotating magnetic field, an integral part of the motor he invented.

In his life, Goethe railed against what he saw as the limiting divide between poetry and science, in an essay on forms. Frustrated by the way the two disciplines had been separated by the scientific and literary establishments, he declared "the two can meet again on a higher level as friends."

### 9. Even the most scientific minds are prone to the mystical in the face of mortality. 

When James Gleick was composing his biography of Richard Feynman in 1988, he discovered a letter that upended the late physicist's public persona. The world had known Feynman as a rationalist immune to unreason, yet the letter revealed a whole new, sentimental side to the sober scientist. To understand its origins, we need to travel back to Feynman's youth.

As a teenager, Feynman had fallen in love with a girl called Arline, who was as passionate about philosophy as he was about math. They pondered a life of bliss together.

But then Arline began to develop a mysterious illness. A lump appeared on her neck and she broke into fevers. She recovered, temporarily, but the illness returned. To Feynman's horror, medicine, a branch of science he'd put so much store in, for a long time couldn't find anything to help Arline. Finally, her illness, a rare form of tuberculosis, was discovered to be terminal. Devastated, the couple decided to get married in an impromptu service, fulfilling what they had long dreamt of.

As World War II reached its climax, Feynman, who'd become one of the nation's preeminent physicists, was recruited to work on the Manhattan Project, which produced the first nuclear weapons.

Meanwhile, Arline, wrote him love letters from the sanatorium in complicated code to amuse him. Their romance persisted like this, until the very end. Arline died on June 16th, 1945. It was then that Feynman — a fastidious rationalist who'd sometimes been frustrated with Arline's passion for metaphysical theories that allowed for God — turned to the "irrational." 

He wrote the letter that Gleick discovered all those decades later. It was addressed to Arline, after her death. In the beautiful, poetic address, he speaks to her as if she were alive and tries to anticipate her response. It's not the work of the skeptic that everyone knew, but reveals someone who, though a scientist, wished with all his heart for something beyond the exactitude he filled his life with. In it, he wrote: "I have met many girls and very nice ones… but... they all seem ashes. You only are left to me. You are real." Like many of the people we've considered here — astronomers, inventors, poets — he was dumbstruck by life's mystery.

### 10. Final summary 

The key message in these blinks:

**Many significant historical figures have left legacies that branch out in surprising, unforeseeable ways, connecting with other people from different disciplines, places, and times. Sometimes these connections have led to great cultural change, to major scientific discoveries and groundbreaking innovation. From astronomers to poets, political activists to inventors, the driving force that connects the great minds of history is their shared pursuit of beauty and truth.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Curate This!_** **, by Steven Rosenbaum.**

You've just explored one take on our interconnected world, in which disparate stories and fragments were collated until they formed a greater whole. In its purest sense, this is what we call _curation_. It is the simple act of sifting through content and selecting the most relevant, appealing, meaningful items. 

Curating isn't limited to art gallery directors. In his book _Curate This!_ Steven Rosenbaum considers how curation has become a vital skill for many today, from marketers to Buzzfeed journalists. If you've ever thought of yourself as a natural curator, you'll want to dig into the blinks to _Curate This!_
---

### Maria Popova

Maria Popova is a Bulgarian-born American author famous for her blog _Brain Pickings,_ which features her diverse writing on books, art, philosophy, and culture. She has served as an MIT "Futures of Entertainment" Fellow, as the editorial director at the higher education social network Lore, and has written for _The Atlantic_, _Wired UK_, and _The New York Times_.

