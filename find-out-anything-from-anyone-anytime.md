---
id: 5aba4577b238e100066bfcf7
slug: find-out-anything-from-anyone-anytime-en
published_date: 2018-03-29T00:00:00.000+00:00
author: James O. Pyle and Maryann Karinch
title: Find Out Anything From Anyone, Anytime
subtitle: Secrets of Calculated Questioning From a Veteran Interrogator
main_color: F87E42
text_color: AB572D
---

# Find Out Anything From Anyone, Anytime

_Secrets of Calculated Questioning From a Veteran Interrogator_

**James O. Pyle and Maryann Karinch**

_Find Out Anything From Anyone, Anytime_ (2014) is a guide to asking questions that will elicit the responses you seek. The authors draw on decades of experience to show that everyone — from teachers to journalists to doctors — can benefit from asking the right questions in the right way.

---
### 1. What’s in it for me? Learn the questions to get the answers you need. 

It's not only CIA interrogators who need to know how to ask good questions. Indeed, anyone with a teenage son or daughter knows how challenging it can be to pry information from an unwilling respondent! Or say you're recruiting new personnel or trying to find the problem in a department — or you just want to know how your loved one's day was.

There are always questions we want to have answered.

Here, you'll learn some techniques and tricks that'll help you find out what you want to know from any conversation with anyone.

In these blinks, you'll find out

  * what _W-words_ are and why you should use them;

  * the types of question you should avoid; and

  * how one question can give you the answers you need.

### 2. Questions that start with who, what, where, why, when and how produce the best answers. 

Do you get frustrated when people reply to your questions with a simple yes or no? To encourage a person to respond with full, illuminating answers, you need these six magic words: who, what, why, where, when and how. Let's call them _W-words_.

Questions that begin with W-words are excellent because they don't allow for simple yes/no answers. Consider this conversation between Jennie and her friend:

Jennie: "Did you go somewhere last night?"

Friend: "Yes."

J: "Where did you go?"

F: "The cinema"

J: "But you hate the cinema, don't you?"

F: "No"

J: "Why did you go?

F: "I was on a date with Bob, but it was raining, so we decided to go to the cinema."

Did you notice that certain questions made it much easier for Jennie's shy friend to reply with either a simple yes or no? When Jennie didn't begin her question with a W-word, her friend was inclined to give a yes or no answer. Contrarily, when Jennie _did_ use a W-word, her friend gave fuller, more satisfying responses — Jennie even managed to gather some juicy gossip about her date!

Questions that employ W-words are better because they're less leading and confrontational.

The worst question that Jennie asked is, "But you hate the cinema, don't you?" This question is very confrontational, forcing Jennie's friend to disagree in order to respond.

In most situations, the person responding will merely agree with a leading question such as this. That's because it's the easiest way to respond and it often gives the questioner the answer they expect.

### 3. There are four types of bad questions to avoid. 

In the previous blink, you learned that certain types of questions are great because they are very open and invite a full response. In addition, you also discovered that other forms are less effective because they're "leading."

The first type of bad questions, then, are _leading questions_. The next three are: _vague questions_, _negative questions_ and _compound questions_.

Vague questions have no focus and are often too broad for the respondent to answer effectively. A prime example of a vague question would be: "What do you think about the modern world?"

The next type is negative questions, which are confusing. If you run too many negatives or double negatives together in a question, you'll make it too difficult for the person you're asking to answer. In other words, when asking questions, avoid overly complex locutions, such as "Am I not wrong in assuming that you didn't _not_ enjoy the film?"

The fourth type is compound questions — which are multiple questions disguised as one. Say you're listening to a press conference and a journalist asks: "Who have you given the state secrets to and should the public be worried about this?" Notice that the journalist is really asking two questions.

Compound questions such as this one rarely receive an answer to both parts because the respondent forgets one half of the question! After the politician has denied giving away any secrets, he's unlikely to remember to say that, of course, the public shouldn't be worried about secrets he never shared. A better interviewer will ask one question so that the person being questioned can remember it and easily address the topic at hand.

### 4. “What else?” is often the best question you can ask. 

So far, you've learned that some questions lead to poor answers, while others prompt satisfactory ones. Here's another question that tends to yield particularly informative answers: "What else?"

If there's one golden nugget of advice to retain from these blinks, it's to remember to use this phrase when you're questioning someone.

Those who work in tech support know to ask "What else?" in order to solve a problem. Imagine a scenario where your computer just won't resize an image. Here's the conversation you would have with a person from tech support:

Support: "What's the problem?"

You: "I can't resize an image."

S: "What else?"

You: "I can't save images."

S: "What else?"

You: "My image editing software keeps crashing."

S: "What else?"

You: "Nothing, that's it."

In this situation, the tech support worker was able to quickly discover that it's not just the process of resizing images that isn't working; it's your image-editing software that's the real problem. They were able to work all of this out by simply asking, "What else?"

For military investigators, this phrase can help in many life-threatening situations. A suspect may be working with a team, so by asking "What else" questions, the investigators can get a wider view of an incident. An interrogation may go like this:

Investigator: "What were you doing?"

Suspect: "Planting mines."

I: "What else?"

S: "Lookout duty."

By asking "What else?," the investigator has discovered that the mines which were being planted are not simply offensive — they were being placed near an important site as a defensive measure. Taking a wider approach has caused them to locate an enemy base and potentially uncover the location of a high-ranking general.

### 5. Reframe and repeat questions to get closer to the truth. 

Imagine that you have the opportunity to interview your favorite football manager for a sports blog. You've prepared a list of questions, and, in order to provide the best content, you want them all answered.

The best way to get the fullest answers is to delve deeper into your current question rather than rapidly moving from one to the next.

And one of the most effective techniques for going deep is to reframe your question.

Let's say you ask, "How many players will be playing in the next World Cup game against Brazil?" And you get this response: "Eleven, last time I counted." But this isn't what you need; you want to know how many people are involved in the tournament, so you go deeper, asking, "And how many hotel rooms have you booked?" And you get this reply: "Twenty-three for the squad and ten for the management team."

Notice how your initial question did not provide you with the complete picture — there's a difference between the team on the pitch and the entire squad at a tournament.

It's also a good idea to ask for the same information twice in order to uncover the truth. Say you're putting on a trade show in three months' time, and you speak to a client who has an innovative new product.

You ask: "When do you think it will be ready for release?," to which she replies, "Within three months, definitely." You then decide to press her further and ask, "So can I book you in for a stand in June to launch your product?" She replies, "We might not be out of beta testing by June, and we're still waiting on the patent confirmation."

The decision to ask for the same information twice has allowed you to uncover a more truthful answer. So next time you're frantically trying to get through a list of questions because you think it's the best approach, reframe a couple and use the phrase "What else?" instead.

### 6. It’s important to consider your respondent’s motives and point of view. 

Remember that a conversation is a two-way exchange. Therefore, to get the most information out of someone, it's best to put yourself in their shoes.

When asking questions, determine whether the respondent has something to lose or to gain. Different respondents will have varying motives for either answering or refusing to answer certain questions.

In war, for example, a captured enemy combatant may have received training for resisting questioning. Such a person may prove almost impossible to crack — because, say, his answering your questions will put his friends or family in danger.

At the other end of the spectrum, an elderly medical patient may be feeling uncomfortable and eagerly discuss at length every ache and pain they've ever felt. They'll be motivated to reveal all in order to stop their own suffering.

Another key aspect to consider is your respondent's personality type. Some people are naturally private and evasive, while others are open and talkative. There are even those who are overconfident and dish out their opinions as if they're hard facts. If you're aware of these various personality types, you'll be better at posing questions.

People who like to deliver personal opinions as though they're facts tend to have a dictatorial personality. This is the category that many politicians fall into; if you ask them a question, their opinion will be presented as if it were plain fact. Therefore, it's up to you as the questioner to discern whether what they've said is true or not.

Certain people — let's call them "evaders" — avoid questioning and don't like to be interrogated. They may have been raised by very strict parents whose cultural views differed from their own. In general, evaders tend to hide a lot from their families. Naturally, they fear questions of any kind as they perceive them as intrusive.

### 7. Your profession influences the type of questions you ask. 

Just as respondents will have different motivations for answering or avoiding certain questions, a person will have varying reasons for asking a question in the first place. And, generally speaking, a person's profession can tell you a lot about what kinds of questions they'll ask.

Teachers tend to ask penetrating questions to get their students thinking. The most effective practice is to ask questions that encourage students to discover the facts and then to meaningfully engage with them.

Where a poorly equipped teacher might ask three broad questions — "How many people live near the coast in California?", "Why is it dangerous to live in California?" and "What are the main types of commercial activity?" — a competent teacher will ask one narrow question: "What are the ten biggest threats to livelihood in the coastal regions of California? Rank them from 1-10."

To answer this second question, students must know all the information relative to the three questions that the less-skilled teacher asked. They'd also have to actively engage with the information and figure out a response for themselves.

Another profession to consider is medical practitioners; their questions will vary, depending on their role. Take these two different types of medical hotline. There is the 911 emergency line which deals with urgent issues and there's a general-health hotline for ailments that aren't life-threatening.

If you call the general health hotline, you'll be asked a list of scripted questions. Scripts are a fantastic diagnostic tool because they allow the nurse to reach a probable diagnosis fairly quickly. If they discover that you're not bleeding, have no problems breathing and that the pain you're experiencing is sporadic, they'll know that your condition is probably not urgent. However, if you call 911 and can hardly speak, the scripted questions are forgotten and an ambulance is sent immediately.

### 8. Focus on events in time if you want to be a good reporter. 

When a big news event breaks, you'll notice that the journalists covering the live event focus on what's happening in real time, rather than speculating about motivations or trying to provide explanations. The reason for this is because journalists are trained to ask "What?" and "Where?" rather than "Why?"

When the Twin Towers fell, the news reports focused on the planes crashing into the towers at 8:45 a.m. and 9:03 a.m. This was then followed by coverage of the Port Authority closing the bridges at 9:21 a.m., President Bush speaking in Florida at 9:30 a.m. and, finally, footage of the other planes crashing, and the emergency services responding. It wasn't until several hours later when reporters first started mentioning Al-Qaeda and speculating about the motivations of the terrorists.

During an ongoing event, the "What" and "Where" questions are less biased than the "Why" question. They provide the full picture before speculation or thoughts of punishment can surface.

In addition to these questions, it's vital to look for "What else" events happening in conjunction. After the 9/11 attacks, the journalists needed to know what else was going on in order to unearth the whole story.

Look at it this way. If you only witness one tower collapse, you might think that it's an awful accident. If you then see the other collapse, you'll think it's deliberate. If you take the initiative to ask "What else?" and are informed of the Pentagon, you'll have a fuller picture when it comes to understanding the motivations behind the attack. By continuing to ask "What else?" you'll begin to notice similar attacks in the years leading up to 9/11 — and, gradually, you'll be able to put two and two together and realize that this was the work of Al-Qaeda. Therefore, it's important to ask the right questions at the right time.

### 9. Final summary 

The key message in this book:

**Whether you're interviewing formally or chatting with a colleague, guide people to provide detailed, informative answers to your questions by asking W-words; "What," "Where," "When," "Why" and "How." Moreover, using the invaluable question "What else?" and reformulating your questions will garner additional information and paint a fuller picture of the topic at hand.**

Actionable advice:

**Keep a question journal**

The next time you ask a question at work, make a note of it and see how your colleague responds. Record the question-and-response interactions you hear around you. Eventually, you'll build up a picture of why certain questions receive better answers than others. This will improve your interpersonal skills and may even advance your career because of it.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Spy the Lie_** **by Philip Houston, Michael Floyd, Susan Carnicero and Don Tennant**

_Spy the Lie_ reveals the typical strategies that liars use to try to deceive you, as well as the tools to help you detect them. This book draws on field-tested methods for lie detection developed by former CIA officers, which helps to spot the signs of a lie and ask the right questions to uncover the truth.
---

### James O. Pyle and Maryann Karinch

James O. Pyle is a veteran interrogator and intelligence-training instructor. He has worked for the Pentagon and the Defence Language Institute.

Maryann Karinch is a body language expert and author. Her 19 previous books include _The Body Language Handbook_ and _Get People To Do What You Want_.

