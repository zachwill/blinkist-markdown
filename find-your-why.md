---
id: 5a1ea316b238e100062d5511
slug: find-your-why-en
published_date: 2017-11-30T00:00:00.000+00:00
author: Simon Sinek, David Mead, Peter Docker
title: Find Your WHY
subtitle: A Practical Guide for Discovering Purpose for You and Your Team
main_color: 2A93D3
text_color: 1F6B99
---

# Find Your WHY

_A Practical Guide for Discovering Purpose for You and Your Team_

**Simon Sinek, David Mead, Peter Docker**

_Find Your WHY_ (2017) offers something that every person and business is looking for: a true purpose. The authors provide strategies and exercises that individuals and teams alike can use to discover their most powerful motivations, and their reasons for getting up in the morning and starting the workday. This is a useful guide if you're searching for the right job, trying to hire the right employees or hoping to gain a better understanding of yourself and the people you live and work with.

_"I think understanding your own why — your raison d'être — and ensuring your actions are consistent with it is a big part of long-term happiness and fulfillment."_ — Ben H, Head of Content at Blinkist

---
### 1. What’s in it for me? Discover WHY you do what you do. 

Do you know why you get up in the morning, go to work, eat, sleep and repeat? In other words, do you have some sense of direction and purpose in your personal and professional life?

If you can't answer a clear and loud "yes" to these questions, don't fret; many people don't know why they do what they do and, as a result, don't return home from work at the end of each day feeling fulfilled.

These blinks are about finding your WHY, such that you can wake up in the morning feeling inspired to go to work. You'll discover how to realize your WHY and how to start living by it, whether it means gathering stories from your past to discover your WHY, or sharing it with a group of strangers.

You'll also learn

  * how getting your seat neighbor to talk about his WHY can turn tedious small talk into intriguing conversation;

  * why specificity is the key to the WHY process; and

  * how "hows" are the actions that bring a WHY to life.

### 2. Knowing your WHY means having a clear purpose, and this makes you and your business more appealing. 

If you've ever felt lost or unfulfilled in life, it might be a result of not identifying purpose in your life — or, as the author Simon Sinek likes to call it, not knowing your _WHY_.

Finding your WHY can be challenging, but once you have it in your life, you can wake up each morning with purpose and determination.

Sinek discovered his WHY after he'd lost all passion for his work. At the time, he was feeling depressed and getting lots of advice from people, but none of it felt applicable to his situation. After some soul-searching, he realized his WHY was to inspire others, and once he took this to heart, he began to see his life more clearly and with more optimism.

Knowing his WHY gave Sinek's life direction, and he soon found that this confidence was appealing to others.

This not only applies to people, but also to companies. A case in point is Apple, which has strong competitors offering cheaper products with more features. But Apple's customers are loyal and inspired by their motto "Think Different," which perfectly describes their WHY. Customers would rather give money to a business with a progressive identity than save a few bucks buying from a more generic company.

To see how compelling a strong purpose can be in the marketplace, let's look at two advertising pitches for a paper company:

The first one highlights the paper's superior quality and affordable price; the second one highlights the company's mission of creating a fine product that allows people to document their ideas and share them with the world. This second pitch is far more compelling and attractive, simply because it explains the company's WHY.

Sometimes we need to advertise ourselves as well, especially when we're in a job interview. When a client of one of the authors named Emily found herself shortlisted for a job and sitting in front of a panel of executives, she was asked what made her unique. This was Emily's chance to share her WHY, so she spoke about her eagerness to work with others and help people become the best version of themselves.

With her clear understanding of her WHY, it's no surprise that Emily got the job.

### 3. Unlock your WHY by looking to your past. 

If you dread going to work and find your job exhausting, then you're clearly doing something that does not speak to your WHY. Once you find a job you're passionate about, even the most repetitive and banal tasks become easy to tackle.

A great way to find that passion is to work your way through the _golden circle_.

The golden circle was introduced in Simon Sinek's previous book, _Start With WHY_, and illustrates how we tend to operate on three levels. The first level, the outer circle, is all about "WHAT" we do, while the middle circle is "HOW" we do it. Finally, at the center is the golden circle, which determines "WHY" we do it.

When all of these circles are aligned, you'll find passion for your work.

While on a flight, the author met a man named Steve who'd been at his job for 23 years and was still excited by his work. How is this possible? Well, WHAT Steve did was produce steel; HOW he did it was by manufacturing a pure product that is easier to recycle and creates less pollution; and WHY he does it is to keep the environment clean for future generations.

Steve's WHY has kept him energized and passionate for over two decades — but if you're still wondering how to find your WHY, try looking into your past.

In helping one young woman identify her WHY, the authors performed a discovery exercise that involved the woman sharing personal stories from her past. This revealed painful, but nevertheless useful, information, as many of her stories highlighted her desire to protect her younger sister from their abusive father. So a strong WHY for this young woman would be a career in protecting vulnerable people who can't protect themselves.

In the next blink, we'll move on to look at other ways you can get help finding your WHY.

### 4. An outside perspective can help you uncover your WHY. 

If you've gone over a dozen stories from your past and still can't find your WHY, don't panic. Sometimes it's difficult to identify a common theme in the things that are important to us, and if this is the case, it might be time to bring in a fresh perspective.

Another person who knows you well can be a valuable resource in identifying your WHY.

This person doesn't need to be someone you're intimate with, just someone who's curious and observant. It can help if the person you're talking to isn't overly familiar with your background and is someone who will ask thoughtful questions — and even take detailed notes.

Asking specific questions is very important to finding your WHY, and these questions often lead to vital details and intense feelings.

With the right probing questions, something like "As a child, I loved visiting my cousins during summer vacation," can become, "I loved visiting my cousins because we could explore the woods along their property and find fascinating things in nature." With these details, common themes can emerge and point the way to your WHY.

A good listener who is engaged and asks questions will draw parallels between different stories, even when they might seem like completely unrelated stories to you.

This was the case with Todd, who shared three very different stories. The first one was about losing his basketball scholarship due to his addictive behaviors; the second was about feeling frustrated and useless while working as a bartender; and the third was about coming home from work and giving all his tips to a local girl who had set up a lemonade stand, which made him feel useful.

Listening to his stories, the authors identified a running theme that pointed to Todd's WHY, which is his desire to do work that really matters and to help others do more with their life.

### 5. WHY Discovery Workshops can help businesses and teams find their WHY. 

Even if you've found a profession you love, you can still end up feeling frustrated and disheartened if you're working for a company that doesn't have a clear vision, or WHY. Even worse, that company might be your own!

To prevent this from happening, there's a simple WHY Discovery Workshop you can run to share different stories and form a strong WHY for your company.

One particularly memorable workshop was held by the espresso machine company, La Marzocco. Employees were urged to share stories that reflected how it felt to work for the company. One employee recalled a photography event at the store that brought the staff together and made them feel connected.

This story triggered many others just like it, and La Marzocco's WHY statement soon came into focus: to bring people together over coffee for lively conversation.

Another situation a company can find itself in is to have a WHY, or mission statement, that was once clear but has since changed with the times or fallen by the wayside. A WHY Discovery Workshop can help here, too.

Cuestamoras is a self-service supermarket that Enrique Uribe brought to Costa Rica in the 1950s. Recently, however, he felt his business was no longer in touch with its initial vision.

So Uribe and his brothers decided to take part in a workshop that helped them re-identify and clarify what their business stood for now. By sharing their stories of what the store meant to them today, the WHY became a spark for innovation and helped create opportunities for everyone in the community.

Uribe isn't so young anymore, but he can now rest easy, knowing that his brothers and children can always look back at his original concepts for WHY Cuestamoras began. His vision will continue to inspire future generations and help them stay on the right path.

A WHY Discovery Workshop can help a business define their corporate culture, as well as their mission statement and vision for the future — all of which will make it easier for employees to make the right decisions on the ground.

### 6. HOWs bring the WHY to life and understanding them can improve teamwork. 

So far, we've paid a lot of attention to the WHY, but it's also important not to lose sight of the other parts of the golden circle. So, once you have an understanding of your purpose in life, it's time to look to the HOWs.

After all, HOWs are the actions that bring the WHY to life, which make them a crucial part of the broader process.

To uncover your HOWs, think about your day-to-day activities and how your behavior is helping you achieve your WHY.

Co-author Peter Docker's WHY is to help make it possible for others to do extraordinary things, and he has a few HOWs to make this happen. These include reaching new frontiers, creating strong relationships, simplifying things and never losing sight of the larger context.

Knowing your coworkers' HOWs can be a major advantage as well.

Peter often works with David Mead at "Start with WHY" events. At these events, everyone's "WHY" is the same, since the goal is to help people unlock their potential. But Peter and David really make these events a success by knowing how the other works and how to combine their skills to have the greatest effect.

For example, there was once a client who wanted to bring 150 people to a workshop that is normally designed for a maximum of 40 people. What's more, the workshop generally lasts a full day, but the client only had four hours to spare.

Peter and David put their heads together and each one relied on the other's HOWs to bring 150 WHYs to life.

David knows how to be innovative, and he used this HOW to adapt the content to fit 150 people in four hours and still have it be a transformative event. Peter knows how to make things simple, which was extremely useful in this situation, particularly with getting everyone on board with the instructions and requirements of the workshop.

To know someone's HOWs is to understand the way they work, and with this knowledge, you can play to their strengths, divide tasks and collaborate effectively so that every challenge can be turned into a success.

### 7. HOWs can help you in everyday decision-making. 

Every now and again, we're presented with a tough decision, whether it's a project proposal, partnership or job offer. Having a keen understanding of your own HOWs is key to avoiding disasters and making the right choices that will allow you to flourish.

To really understand your HOWs, you need to break them down to their core essence.

The author, Simon Sinek, knows how to focus on the long term, but this HOW can be broken down — more precisely, Sinek focuses on creating products, services and ideas that will continue to be used long after he's gone. It also means he's less focused on deadlines and ways to make a quick profit, and more interested in building lasting momentum for his company.

Getting to the bottom of your HOWs isn't just about self-awareness — it can also reveal where opportunities lie.

By being aware of his HOWs, Sinek can better determine which opportunities are aligned with his WHY, or his overall goals.

A few years ago, Sinek was approached by an executive of a major corporation to start a new company that would "put people first." Since his WHY is to inspire others to achieve their dreams, this was a tempting offer. But Sinek dug deeper into his potential partner's motivations and began to realize he was more interested in making a quick profit, which is not how Sinek does things.

Another one of Sinek's HOWs is to look at things from a different or unconventional perspective, which was yet another approach his potential partner wasn't interested in. So, knowing that this project wasn't aligned with his HOWs or his WHY, it was obvious that this opportunity wouldn't be a good fit for him. In the end, he was able to make the right choice and politely declined.

Sure, the partnership would have looked great on his resume and expanded his list of clients, but he also knows it would have been a bad experience.

If you find yourself in a situation that doesn't feel right, you can use your HOWs to identify an imbalance and hopefully make the necessary adjustments to straighten things out.

### 8. Once you discover your WHY, it’s important to share it. 

Part of the journey to reaching a fulfilling life is uncovering your WHY and understanding your HOWs. But the hard work doesn't end there; next comes the task of sharing it with the world.

Start by offering your WHY statement to those who ask, "What do you do?"

This is a common question in any social situation, and it's an opportunity to start getting comfortable with expressing your mission in life. Try it with the person sitting next to you on a plane, a fellow guest at a party or a stranger in a waiting room.

You might be a little uncomfortable at first, but that's the good thing about strangers — chances are you'll never see them again. It also gives you the chance to fine-tune your WHY into a message you feel comfortable and confident saying.

More importantly, sharing your WHY with the world will push you to commit to it and back up your words with actions. The more you give voice to your intentions, the more likely you'll be to follow through with them.

Also, if your WHY statement is something like "helping people become the best version of themselves," you could imagine that some might roll their eyes when they hear it. This is why you'll want to have some actions that back up this bold assertion.

As for your business, there are a number of reasons to constantly refer back to its mission statement.

For starters, it will make it readily apparent when a product or service becomes outdated, or no longer relevant.

But it's also useful in matters of human resources, such as knowing what to look for during the hiring process, or recognizing when an employee's WHY isn't in line with the company's.

Finally, you'll be sure to find the most productive arrangements possible when you can match the WHYs of your staff with the roles that best suit them.

So the more others are familiar with your WHY and the more familiar you are with the WHYs of others, the better off we'll all be, both personally and professionally.

### 9. Final summary 

The key message in this book:

**Not knowing your purpose in life, or your WHY, can be a frustrating and confusing experience for individuals, businesses and teams. But there are ways to look within, understand your WHY and begin to live a motivated, passionate and productive life. When you fully realize your WHY, you can start thriving in both your personal and professional life.**

Actionable advice:

**To find your WHY, look to your most formative stories.**

Our stories are an important part of the WHY discovery. When sharing your stories with someone who can help you discover important personal themes, it is important to share both good experiences and bad ones. They could be from your childhood or adulthood, as long as they played an important role in shaping the person you have become.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Start with Why_** **by Simon Sinek**

_Start with Why_ gets to the bottom of why certain people and businesses are far more innovative and successful than others — even in situations where everyone has access to the same technology, people and resources. The book shows you how to create a business that inspires customers and has satisfied employees.
---

### Simon Sinek, David Mead, Peter Docker

Simon Sinek is a motivational speaker whose innovative thinking has led him to consult with some of the world's biggest corporations, including Microsoft and Intel. He is also the best-selling author of the 2009 book _Start With WHY: How Great Leaders Inspire Everyone to Take Action_.

David Mead is a corporate trainer who has been working with Simon Sinek since 2009. He now runs popular workshops specializing in organizational leadership and motivation. He has helped people on five continents and worked with teams ranging from professional sports squads to governments.

Peter Docker is a former senior officer in the Royal Air Force, who now works as an in-demand business consultant and a key member of the _Start With WHY_ team. He has played a central role in transforming Simon Sinek's theories into a practical training manual.

