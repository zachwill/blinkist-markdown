---
id: 59ee6971b238e1000737fe5b
slug: finding-my-virginity-en
published_date: 2017-10-24T00:00:00.000+00:00
author: Richard Branson
title: Finding My Virginity
subtitle: The New Autobiography
main_color: 78BDE4
text_color: 436A80
---

# Finding My Virginity

_The New Autobiography_

**Richard Branson**

_Finding My Virginity_ (2017) is the long-awaited sequel to Richard Branson's first autobiography, _Losing My Virginity_. It picks up right where the earlier book left off, at the start of a new century with the digital marketplace opening up and an array of new business possibilities presenting themselves, including the opportunity to launch a company to take people into space.

_"This is a remarkable book-in-blinks with both insight and inspiration."_ — Sophie, Blinkist user, Kenya

---
### 1. What’s in it for me? Learn what the last 20 years have been all about for Sir Richard Branson. 

Sir Richard Branson hardly needs an introduction. His long career at the head of the ever expanding Virgin business empire has made him a modern day business legend and a go-to person for anyone wishing to learn what makes a truly great entrepreneur and how to keep a business thriving in a constantly changing world.

In this follow up to his classic 1998 biography, _Losing My Virginity_, we pick up the thread and get Branson's own version of the events that have shaped Virgin, and himself, since then; from Virgin Atlantic's epic battle with British Airways, disrupting the cellphone market with Virgin Mobile and Branson's highly personal mission to take people into space with Virgin Galactic.

In these blinks, you'll learn:

  * that the best kind of marketing is fun and cheeky;

  * how we may someday thank a paper napkin for space travel; and

  * why none of Branson's successes would have been possible without Mom.

### 2. Virgin Atlantic’s battle with British Airways presents a classic example of Richard Branson’s resourcefulness. 

In 1999, Richard Branson wasn't sure what lay ahead. The trendsetting, billionaire entrepreneur had just wrapped up his latest hot air balloon adventure, attempting to travel all the way around the world only to end up in need of rescue near the Hawaiian Islands.

At this time, Branson's Virgin Group was on the verge of being the global brand he'd always dreamed of. And while there had been missteps along the way, such as the failed attempts during the 1990s to get into the cola, vodka and cosmetics markets, he was constantly learning and becoming more focused on what exactly made a perfect fit for the Virgin Group.

One of the companies that Branson fought hardest for is Virgin Atlantic, the international airline whose history shows how determined Branson can be to compete.

Virgin Atlantic was founded in the early 1980s, and as with most of Branson's projects, it was born in an attempt to offer people a better experience. In this case, an alternative to British Airways (BA), which wielded an extremely powerful influence at London's Heathrow Airport but offered bad food, bad entertainment and bad service.

But BA would not go down without a fight. Ever since Virgin Atlantic started doing business, BA has tried to squeeze it out of the competition, even using "dirty tricks" that involved printing libelous remarks about the company.

Though the libel suit against BA was settled with Branson being awarded damages, he prefers to win simply by being the best airline in the business, not by being in the courtroom.

Branson is also always keen to improve and take a shot at his competition. At one point Virgin Atlantic began offering in-flight massages. Branson advertised this by putting up a sign at Heathrow that read, "BA Don't Give a Shiatsu."

And when BA was spending huge amounts sponsoring the London Eye in the 1990s, and running into all kinds of technical problems that left the giant ferris wheel stranded on its side, Branson jumped at the chance to hire a blimp displaying the message, "BA CAN'T GET IT UP."

Cheekiness and great service; that's the Virgin way.

### 3. Virgin Mobile found success early on by disrupting the cell phone market with a pay-as-you-go plan. 

Richard Branson was one of the first Britons to use a cellular phone back when they were the size of your head. And though few then would have guessed they'd eventually be the preferred method of communication, by the turn of the century it was clear they were here to stay.

But how did Branson get involved in the mobile phone market? It all came down to one expensive telephone bill.

Branson's business partner Will Whitehorn showed up one day with a prize: British Telecom had awarded him the honor of having the most expensive phone bill in Britain.

This got Branson thinking: in 1998, mobile phone carriers were making a killing with expensive and lengthy contracts that customers had no choice but to accept. That year, phone sales reached $162.9 million, which was more than double the year before.

This is the kind of scenario that Branson loves to work with as it presents an opportunity for him to come up with a better service. So he founded Virgin Mobile, partnered with Deutsche Telekom, the parent company of T-Mobile, and reduced costs by using their pre-existing network. Virgin Mobile then offered pay-as-you-go plans, letting customers pay just for the time they used rather than get locked into expensive contracts.

At the time, Branson had 381 Virgin Megastores, which were perfect locations to sell Virgin Mobile phones to the target audience, young people who wanted their own phone but didn't need big bills or confusing contracts.

The strategy worked. By November 2000, Virgin Mobile UK had 500,000 subscribers, won a Network of the Year award and had already been estimated as having a value of £1.36 _billion_. Soon, the service was reaching Australia, the United States and Asia, while Virgin Mobile UK became the fastest growing mobile startup in UK history. As for Virgin Mobile USA, it reached a billion in revenue in only three and a half years and continues to grow.

### 4. Virgin Active set out to improve the overpriced gym, but it faced a disastrous start. 

Richard Branson gets ideas pitched to him all the time. Many don't fit the Virgin brand, but sometimes they're just perfect, like the time he was pitched the idea for Virgin Active.

Gym membership is another service that people often complain about but essentially accept: the equipment is old, the locker rooms are tiny, and the sign-up fees and membership prices are ridiculous. In other words, it's the perfect market for Virgin to enter and improve, but it almost didn't happen thanks to a disastrous beginning.

After two years of preparation, the flagship gym was nearly ready to open in August 1999 when it caught fire, causing a significant delay.

The location was Preston, Lancashire, especially sought out for its demographics. Nowadays it's common to use technology to pinpoint desirable locations for a business, but in 1999 this was cutting-edge work by Frank Reed and Matthew Bucknall, the two brains behind the Virgin Active concept.

They also designed a fantastic facility: a big spacious gym with great atmosphere; showers that were better than anything people had at home; attention to the smallest details, like towels being ready and available for people and the pool being the right temperature.

But it all literally went up in smoke, and they had to pay staff while no money was being made. Luckily, though, while the gym was being repaired the staff remained genuinely enthusiastic and did a great job of promoting and keeping the business in the minds of the community.

The main attraction was that it would be affordable. The local competing gym, LivingWell, was charging £300 just to sign up! At Virgin Active, there were no outrageous membership fees or long-term contracts. So when the doors finally did open, the customers were still there and they now had a gym with which they could fall in love.

> In 2015, Virgin Active was valued at £1.3 billion.

### 5. Virgin Trains has proven the benefits of privatization for UK public transportation. 

Just like British Airways, many services in England were traditionally run by the government. For phones, you had British Telecom, and for trains you had British Rail. This is on top of British Gas, British Steel and British Coal.

But Richard Branson was set to change this for rail, ever since he'd experienced the famous bullet train ride from Tokyo to Kyoto in 1991. When the Department of Transportation privatized British Rail and invited businesses to bid to run parts of the system, Virgin Trains was ready.

It wasn't easy. Introducing new and better trains meant upgrading and installing new infrastructure that ended up costing four times as much as predicted. But eventually the two parties reached a deal, and by the early 2000s, Virgin Trains was operating its new West Coast Main Line service. Improvements were apparent right away.

On a test run of the new Pendolino train that Branson introduced to the United Kingdom, the London to Manchester route was 15 minutes faster. And in the first week, 82 percent of trains were on time, with only four cancellations — and it just got better from there.

Virgin Trains improved their part of the messy, poorly scheduled nationalized rail service. And as a result, the number of passengers has nearly tripled, and the network is booming.

Some people, such as Labour leader Jeremy Corbyn, see these bigger crowds as a reason for Britain to return to nationalization. Corbyn even released a video in 2016 of himself sitting on the floor in an overcrowded Virgin Train to prove his point.

But the video, Branson claimed, was misleading: there were 140 seats available on the train, and Corbyn ended up sitting in one of them after his video shoot. But he also simply missed the point. More people are using trains because of the improvements made over the past 15 years.

Meanwhile, 65 brand new and improved Azuma trains are being introduced soon. And as always, Virgin Trains will be making every effort to improve in every possible way.

### 6. Virgin Unite brought together an amazing group to help make the world a better place. 

One of Richard Branson's dearest friends was Nelson Mandela, a rare sort of person who could walk into a room, transform its energy and bring a smile to everyone's face.

After Mandela died in 2013, it was difficult for Branson to adjust to a life without this tremendous force for good, but he's forever grateful that he got to work alongside him as part of Virgin Unite.

Virgin Unite is a charity that Branson developed with the intent of bringing together different leaders in philanthropy. The basic idea is that those who want to make a difference can make an even greater impact by joining forces. And at the heart of Virgin Unite is a group of knowledgeable advisors and leaders Branson calls "the Elders," inspired by the concept of village elders.

Nelson Mandela was Branson's first choice for an Elder, and his second was UN Secretary Kofi Annan. Once on board, Branson let Mandela create a list of 12 other peers he believed would be great assets to their cause.

Virgin Unite and the Elders had no specific goal other than making the world a better place. Branson wanted a group of powerful independent minds who would be able to respond to a crisis faster than the bureaucracy of the UN would allow.

Mandela's list included former US president Jimmy Carter and Desmond Tutu, as well as Ireland's former president, Mary Robinson, and Nobel Peace Prize winner Muhammad Yunus of Bangladesh. Everyone was united in their desire to improve the state of human rights around the world, and it was inspiring to have these great minds sharing ideas and traveling to places like Darfur, Somalia, Palestine, North Korea and Russia.

These are just some of the places where human rights violations are ongoing, and Virgin Unite needs the world's brightest minds and most powerful humanitarians to work together on a mission to find solutions to these crises.

### 7. Virgin America fought hard to become one of the world’s best airlines, and its sale was heartbreaking. 

Sometimes you have to sell one concern to keep another going. This was the case in 1992 when Richard Branson sold Virgin Records to support Virgin Atlantic, and in 1999 when he sold 49 percent of Virgin Atlantic to kickstart the Virgin Active gyms.

But one sale really hurt: the Virgin America airline, which began in 2004.

At the time, airlines around the world were still figuring out how to move forward after the events of 9/11, and most experts thought a new airline wouldn't stand a chance, even if it were a recognizable brand like Virgin.

However, Branson noticed that most American airlines were offering services and experiences that passengers endured rather than enjoyed. So he knew there was a perfect opening for the Virgin brand to enter and offer something better.

It took three years before Branson collected enough petitions and convinced enough people at the US Department of Transportation to approve Virgin America for take off, and it wasn't long before the new airline was winning over the public. Virgin America was the first to offer WiFi in every plane and on-demand dining accompanied by great entertainment choices.

And as a result, publishing giant Condé Nast named Virgin America the best domestic airline for a remarkable ten years straight.

But Branson by law could not keep full control of Virgin America. Since Virgin America was American and Branson was not, he was limited to no more than 25 percent of the company's voting shares. So when the airline was made public in 2014, it wasn't long before other airlines tried to buy it up.

Just 18 months later Alaska Airlines made an offer of $2.6 billion that the board at Virgin America unanimously approved. The sale broke Branson's heart since it turned out that Alaska had no interest in keeping the Virgin spirit alive. In fact, they're planning on completely retiring the brand by 2019.

As some angry customers have rightly pointed out, now it's just "another bullshit airline."

### 8. Virgin Media brought the brand into people’s homes like never before. 

What do you do when one of the worst businesses in town asks you to partner up?

This was the predicament Richard Branson faced when Simon Duffy, the CEO of NTL, the international cable communications giant, approached him about joining Virgin Media. NTL was commonly referred to by customers as "NT Hell" and ranked at the bottom of every customer service survey.

Since the Virgin brand was built on great customer service, this wasn't the kind of partnership Branson would normally get involved with, but Duffy's idea was too good to pass up. It was a chance to get Virgin Media into people's homes for cable service, internet service and both cellular and landline phones. Duffy called it a "Quad Play" package, but this being Virgin, Branson preferred the more irreverent name, "Four Play."

A quick look at NTL's customer service revealed the problem right away: all the service operators were using scripts. Branson tore the scripts up and got everyone on board with the Virgin way: loosen up, be yourself and never try to help a customer by using a script.

Branson was excited to be taking care of so many customer needs with one service, and before long customers were pretty satisfied as well. Virgin Media became the number one broadband provider and the second biggest pay-TV and home-phone provider in the United Kingdom.

The only person who wasn't happy was Rupert Murdoch, who owns Sky TV, the United Kingdom's largest pay-TV provider. Murdoch bought up a 17.9 percent stake worth £940 million in popular British commercial station ITV just to block Virgin Media from merging with it. Unfortunately for Murdoch, the Office of Fair Trading didn't take too kindly to this violation and forced Sky to sell over 10 percent at a £348 million loss.

Murdoch also owns media company News Corp, which has a subsidiary that was found to be responsible for hacking the phones of Branson, his family and neighbors in 2011. As a small consolation after the privacy violation was sorted out, Branson received a letter of apology.

> In 2013, Liberty Global bought Virgin Media for $23.3 billion, but Branson still controls the brand.

### 9. Virgin Money has been a great and natural addition to the Virgin Group. 

As time has passed, the Virgin brand has become clearer for customers and Branson himself. Nowadays, most of what Virgin offers falls into one of four categories: travel and leisure, telecom and media, music and entertainment, and health and wellness. But there's a fifth category that ties all these together: money. And that's where Virgin Money comes in.

If you had told the 19-year-old, spliff-smoking Branson, who had just started Virgin Records as a mail order service in the back of a magazine, that one day he'd be a banker, he might have asked you what _you_ were smoking.

However, when the global financial crisis of 2007 and 2008 left the United Kingdom badly shaken, it was clear that financial services were another area where customers were being royally screwed over. Not only that, but one of the United Kingdom's most iconic banks, Northern Rock, was in desperate need of saving.

Branson took this as a cue to spend £1.25 billion to rescue Northern Rock and become a banker. But the origins of Virgin Money actually go back much further to 1997, when Branson was sold on a simple idea: no one trusts banks, but they trust Branson and the Virgin brand.

So, for 20 years now Virgin Money has been offering credit cards, savings and investment services. Now, with Northern Rock, the service has grown into full banking services, including mortgages. But what Branson really delights in are the Virgin Money Lounges.

These are lounges around Britain where Virgin Money members can relax for a moment during a busy day, enjoy some free refreshments or a little peace and maybe meet up with a friend. They're community spots that offer a little breathing room in today's hectic, fast-paced world. Plus, the Virgin Money branches with a lounge do 300 percent more business than those without. Clearly they're working well!

### 10. Virgin Galactic is a project dear to Richard Branson’s heart but was nearly undone by a tragic accident. 

Many dream of being an astronaut when they are young, but few people are as dedicated to making that dream come true as Richard Branson.

For a long time, Branson thought this dream might remain a fanciful wish until he accidentally ran into aerospace engineer Burt Rutan at a hangar in the Mojave desert and heard his plan for SpaceShipOne.

In 2003, 12 years of searching later, Branson found a workable spaceship design drawn by Rutan on the back of a paper napkin. His amazing design led to the formation of Virgin Galactic, which became one of the main focuses of Branson's life for the next 14 years.

Branson immediately showed the design to his friend Paul Allen at Microsoft, who agreed to chip in $25 million to build the first spaceship, and its mothership, WhiteKnightOne. The basic idea is that the mothership takes the spaceship up to a certain height and releases it. The shuttle then launches up into space where it spends some time before gliding back down to earth.

Things were going smoothly until the fourth powered test flight for SpaceShipTwo in 2014. After the mothership released it up in the skies, a lever was pulled too soon, opening the ship's tail to a glide position just as it was about to launch. As a result, massive force broke the entire structure apart, killing one pilot, Mike Alsbury, and injuring the other, Peter Siebold.

The press had a field day with the tragic incident, and it almost stalled the project completely.

Completely inaccurate headlines like "Eyewitness Reports Explosion after Ignition" were run by supposedly reputable agencies like the Associated Press. In truth, there was no problem with the engine or any explosion at all. The engine and gas tanks were found in one piece, and the investigation eventually revealed the true cause, though the media were still eager to paint the project in a bad light.

As a result, difficult questions had to be asked. Could Virgin Galactic survive this tragedy?

### 11. Branson hopes to take customers into space very soon, and his desire isn’t ego driven. 

Unlike the SpaceX program being developed by entrepreneur Elon Musk, Branson's Virgin Galactic isn't aimed at reaching Mars. Branson wants to take people to space so they can see earth from a new perspective, which is an experience they'll carry with them for the rest of their lives.

You can already make a reservation to be one of the first astronauts, and Branson is hopeful that commercial trips will soon take place.

There's even a new SpaceShipTwo, which resumed testing in February 2016. It took time to build it and to reassess the project in general after the accident, but things are now back on track, and commercial flights will be underway in the near future. In fact, on December 5, 2016, the fifth powered test flight of SpaceShipTwo went off perfectly.

But though Virgin Galactic is pressing on, the 2014 crash did cause Branson to question his motives — something everyone should do from time to time. Some people were saying that the pilot's death should be blamed on Branson's ego. But Branson saw this for what it was — bad reporting — and he has only become more determined than ever to reach his goal.

It took time to mourn the loss of Mike Alsbury, but there was never any doubt among the engineers and team behind Virgin Galactic that the best way to honor the loss was to learn from what happened and move forward. Space exploration is for the benefit of humankind, not personal glory. For Branson, as well as his peer Elon Musk, picking up where NASA left off on space travel is about reaching new frontiers and showing the world the endless possibilities that lie ahead.

There is a quote from the brilliant physicist Stephen Hawking, the only person to be offered a free ticket, which sums up Virgin Galactic well. He said, "[Virgin Galactic] has my utmost respect for enabling more of humanity to experience the true wonder of space."

### 12. Branson is also excited about Virgin StartUp loans and creating a worldwide broadband network. 

While helping the public see the wonders of space is certainly a big project, Branson still finds time to help out fellow entrepreneurs.

One of the projects he's most passionate about is Virgin Startup, which was created to help innovative people get the money they need to turn their ideas into reality.

Branson knows he was fortunate to have parents who supported him unconditionally when he was starting out as a teenager with his _Student_ magazine and then Virgin Records. In fact, his mother once gave him £100 when he was flat broke. Without this gift, he might never have built his global empire.

Branson wants others to have the same opportunities he had, so he has been lobbying the British government to put aside some of the money it uses for student loans into a Youth Investment Fund to help young entrepreneurs. This is how Virgin Startup began, as a way to get business loans to young people who don't yet have the kind of collateral that regular banks require.

As of May 4, 2016, Virgin StartUp has provided over 1,000 loans in less than three years, which adds up to around £10 million in funding and tens of thousands of hours in mentoring.

But that's not all Branson's been up to. Another project he's excited about is OneWeb. This is the brainchild of Greg Wyler, a technological innovator who plans to launch an interconnected system of satellites that would provide broadband internet connection to the entire world, including the four billion people who currently have no connection.

This isn't just a matter of getting more people on social media sites; it's about providing people with the means to help themselves out of poverty and access basic internet coverage. And it's an idea that fits perfectly with Virgin Galactic since the mothership, WhiteKnightTwo, is equipped with LauncherOne, which can send a 500-pound satellite into orbit much more easily than any current land-based launching system.

Currently, OneWeb is on target to become active in 2019, at which point we'll have a communications network that is ten times bigger than anything we've seen before.

### 13. Branson isn’t afraid of speaking up on political matters, especially when the consequences are dire. 

Richard Branson is the first to admit that he sometimes has a hard time staying neutral, especially when people are spreading falsehoods and misinformation about topics that are close to his heart, like climate change, prison reform, HIV/AIDS prevention and human rights.

It's no secret that he can cause a stir. Branson has spent a lot of time in South Africa trying to provide access to HIV/AIDS medication and slow the spread of this preventable disease. So it angered Branson in 2014, when the South African health minister publicly stated that a mixture of beetroot, potatoes, garlic and lemon was a fine treatment.

Branson made a public statement, calling it a genocidal crime for the health minister and South African President Thabo Mbeki to continue denying proper treatment and real medication to the country's citizens. These remarks opened a fruitful dialog between Branson and Mbeki, though the president was forced to resign before he could make the improvements they'd been planning, which included the opening of a publicly funded Centre for Disease Control.

Other subjects on Branson's mind are climate change and prison and drug reform.

It's sad that the scientific facts of climate change have become politicized, but the Virgin Group is still dedicated to replacing fossil fuels. And it's also a shame that $100 billion is spent each year enforcing drug laws, while the criminal drug industry remains a strong $320 billion-a-year enterprise with no signs of being slowed down by the war on drugs.

Branson points to Portugal's decriminalization of drugs as a model. The Portuguese now treat drug addiction as a public health issue and, as a result, heroin addicts are rejoining society rather than crowded prisons.

There's still a lot of work to do, and Branson is full of passion for these causes. The sky is no longer the limit for what can be accomplished.

> Branson is still active with the Bhubezi Community Health Centre in South Africa, which continues to test people for HIV/AIDS and has provided treatment for over 325,000 people.

### 14. Final summary 

The key message in this book:

**Since the fledgling days of Virgin Records in the 1970s, Branson has continued to find ways of providing exceptional customer service and experiences. Wherever there's room for improvement, Branson is eager to enter the market and get stuck in. But it's not just airplanes and entertainment, with charitable organizations like Virgin Unite, Branson is dedicated to making the world a better place though prison and drug law reform, reducing carbon emissions and fighting HIV/AIDS in Africa.**

Actionable advice:

**Take notes.**

Branson often reminds his colleagues that no one is above taking notes, and he is always stupefied at CEOs and executives who refuse to take out the old pen and paper to jot down ideas. In his research, he's found that the executives who take notes are more productive than the ones who don't since they're more apt to remember those fleeting moments when a brilliant plan reveals itself.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: _Losing My Virginity_** ** __****by Richard Branson**

_Losing_ _My_ _Virginity_ is the autobiography of self-made businessman Richard Branson. He details his lucrative adventures, beginning with dropping out of school, founding a record label and crossing the Atlantic ocean on a speedboat. In essence, the book is about how cleverness, determination and an adventurous mindset played a role in the making of one of the richest men on earth.
---

### Richard Branson

Richard Branson is one of the most recognizable entrepreneurs in the world, heading one of the biggest and best-known brands: the Virgin Group. Over the years, he's remained a respected business leader in travel, entertainment, health and fitness, media, and communications.

