---
id: 547c891c6363300009580400
slug: finding-the-next-steve-jobs-en
published_date: 2014-12-02T00:00:00.000+00:00
author: Nolan Bushnell
title: Finding the Next Steve Jobs
subtitle: How to Find, Keep and Nurture Creative Talent
main_color: 2B96D6
text_color: 1C618A
---

# Finding the Next Steve Jobs

_How to Find, Keep and Nurture Creative Talent_

**Nolan Bushnell**

This book is about the most important element in any business: creativity. Companies need it to succeed, or even just survive. In this book, Nolan Bushnell reflects on his experiences working with people like Steve Jobs to explain how to recruit, retain and nurture creative talent.

---
### 1. What's in it for me? Learn how to make the most of creative talent. 

The pace of the business world is changing rapidly: the only way you can stay ahead is by constantly moving forward. Now — more than ever — creativity is absolutely vital for a successful work environment. If you want your business to succeed, you need to know how to recruit talented and imaginative people, and how to support them once you've got them.

_Finding the Next Steve Jobs_ is about that process. These blinks will teach you how to find creative individuals — sometimes in unusual places. You'll learn how to foster an atmosphere that can support your employees' imaginations, and pave the way for them to realize great ideas. You'll also learn:

  * how hiring high school dropouts exclusively for a month can boost your company;

  * why Atari put couches and showers in their offices;

  * how hiring arrogant or annoying people can be a good idea;

  * why the author hired the inventor of the joystick because of his love for model trains;

  * why you should reward a person's failures;

  * how the Atari employees got one of their best game ideas while at a party; and

  * how managers are most effective when they never say no.

### 2. Create a work environment that attracts creative people. 

A good work environment is vital for creativity. A cold, bland environment just can't inspire people like an interactive and exciting one can.

There are many ways a company can foster a creative atmosphere. First off, the workplace needs to be fun and interesting for the employees.

One good way to spice up the workplace is to make use of positive and exciting secrets, because creative people have a particular love of secrets.

Apple employees, for example, aren't allowed to discuss the next Apple product with their friends outside the company. This builds anticipation, and also makes the employees closer; it's fun to be part of a group that shares secrets.

The workplace must also be free of stifling rules. Instead, rules should be flexible and accommodating.

Atari adjusted their policies to accodomate their employees when Steve Jobs and Steve Wozniak worked for them. They had initially prohibited sleeping on the premises, for security purposes. Jobs and Wozniak, however, insisted on it. They liked working late into the night and sleeping only a few hours, then continuing in the morning.

So Atari changed the rules for them. Soon other employees wanted to stay late as well, so Atari added showers and couches to make them comfortable. Companies can attract top talent by being fun, flexible and engaging in this way.

It's also crucial to promote the company's creativity when advertising vacant positions. Highlight the most compelling parts of the job. The author, for example, gave Atari employees a summer off every seven years. The advertisement for the position read, "Frolic a whole summer every seven years, full pay."

You can also use bold strategies to reach out to those who work for rival companies. _Red 5 Studios_, a computer game developer, poached employees from their competition by sending them iPods with personalized messages encouraging them to join. Unusual or intriguing strategies like this are certain to engage creative people.

> _"A company name should have the gravitas of a Hewlett-Packard or an International Business Machines, people said. Apple? Silly."_

### 3. Invest time and resources into searching for talent. 

Have you ever met the perfect waitress? The author did once, in a pizza restaurant. He was so impressed with her social skills that he hired her on the spot to develop new marketing programs. She brought her positive energy to the new job and performed very well.

Sometimes you can find creative and passionate people right under your nose, just like this. Most of the time, however, you'll need to go out and search for them.

Luckily, there are many good strategies for searching.

First, keep a look out for _creative communities_ — places where creative people seek each other out. Companies can use them as a source of inspiration, and a place to find new employees.

The annual _Hackers Conferenc_ e, a creative community, is a meeting point for some of the smartest people in technology. The author attends the event regularly himself, and also sends employees there to pick up new ideas, promote Atari and scout for potential employees.

You can also use Twitter to find people who are especially passionate about certain topics. For example, imagine you need a new public relations employee who's good at communicating technical information. You can simply look for someone on Twitter who fits the profile, and go over his or her Tweets to decide if you'd like to contact the Tweeter.

Finally, use your existing employees as sources of referrals. Creative people tend to associate with other creative people, so employees and ex-colleagues can be helpful for finding new talent. The author hired three of his best employees, including the inventor of _Pong_, from his previous employer, _Ampex_. Steve Jobs also hired people from Atari when he left the company.

> _"My associates at Ampex took me aside to tell me that my idea of playing games on a video screen was truly ridiculous."_

### 4. Don't be afraid to hire unusual people. 

Employers often seek out pleasant people to hire, believing they make good employees. That's often true, but sometimes _unpleasant_ employees are even better.

Arrogant people, for example, can be extremely annoying — especially if they act like they're smarter than everyone else. However, if they actually _are_ that smart, they may be able to achieve incredible feats.

Steve Jobs fit the bill. He always believed he was right. That could be frustrating to his colleagues, but it gave him the willpower to push for his ideas, and make them a reality.

So, if you want to find the best workers, you've got to look for all sorts. Don't be afraid to hire people who are a bit bizarre.

Creative people and their innovations may seem absurd sometimes, but some of the greatest business ideas in the past few decades were initially considered ridiculous. For example, when Sara Blakely came up with _Spanx_ — pantyhose with the bottom cut off — people thought she was crazy. Today she is a billionaire.

Some of the author's best employees were people who wouldn't have been able to get a job in more traditional companies like IBM. Harold Lee, for example, who went on to develop the chip for Pong, drove a Harley Davidson, and never washed his long hair or beard.

So remember to look for diversity when you're hiring. Seek out diversity on the inside. Many companies strive for "diversity," but only as it relates to the employees' appearance, race, religion or sexual orientation. Instead, look for diversity in creative skills.

One interesting way is to have the HR department only hire high school dropouts for a month. They'll be forced to consider different characteristics that make the candidates great.

### 5. Conduct interviews that help you identify creativity and passion in the candidates. 

How much can you remember from what you studied in high school or university? If you're like most people, not much.

In fact, a study in 2008 showed that university graduates forget most of what they learn for their majors within two years. So don't judge candidates too much by their formal education — dig a little deeper.

Examining a person's passions and interests gives you a much better understanding of his or her creative potential. Hobbies — particularly complex and time-consuming ones — can be particularly revealing. For example, the man who developed the joystick for Atari was initially hired because he impressed the author with his passion for model trains.

Creative people are enthusiastic about reading, so interviewers should ask candidates about their favorite books. The fact that someone reads at all is more important than the type of books they read.

Effective interviews also include strange or complex questions. This gives interviewers an insight into the thought-process of the person they're talking to. The answer itself isn't the most important part — it's how the person thinks.

The authors like to ask interviewees to estimate the number of chewing gum pieces under the table, for instance. This lets them demonstrate their ability to tackle unusual problems.

Also, don't be satisfied with the person's first response when asking questions — keep digging.

People often use the word "we" when describing successes in their previous workplaces, for instance. Don't just accept that: figure out exactly what they contributed to the project they're speaking of. Were they a leader? Did they play a big or small role?

> _"I've never met a creative person in my life that didn't respond with enthusiasm to a question about reading habits."_

### 6. Create an atmosphere that's conducive to innovation. 

In his younger days, Steve Wozniak loved pranks. He once rewired the phone numbers of hotel and taxi services at airports with those of his friends. Imagine their surprise at receiving calls asking for rooms and taxis!

Good pranks like Wozniak's play a significant role in a team. Pranks lighten up the atmosphere and help people take themselves a bit less seriously. That's very important, because when people feel more relaxed, they'll feel more comfortable taking risks. That's vital for creativity.

There are many ways to create this kind of atmosphere. One good way is to have parties. Parties are great for fostering creativity. Remember that innovation doesn't always come from the top — it can come from the chairman of the board or the janitor. At parties, people freely exchange their ideas, which can lead to great things.

Once at a party at Atari, for example, the employees began thinking about how fun it would be to compete against each other at something. This led them to develop _Indy 8_, the eight-person racing simulator. Indy 8 was once rumored to be making one million dollars per year.

Parties also relieve tensions in the workplace. If your employees are anxious or stressed out, they won't be able to come up with great ideas. So be sure to give them enough holidays. Better yet, give them days off spontaneously every once in a while. Only tell them a few days in advance — they'll be more excited if it's a surprise.

You also need to promote fairness in the company. This is crucial for any kind of creative environment. The author always tries to give Atari employees as much credit for their ideas as possible, for instance. Giving credit where it's due makes employees feel validated and encourages them to keep contributing in the future. They'll like contributing to projects more if they know their hard work will be recognized.

### 7. Use a management structure that promotes creativity instead of stifling it. 

Have you ever had a manager that supervised employees like they were children? How did it feel? That sort of management style never works, and it also destroys the employees' creativity.

Managers need to treat those under them like adults, and help them realize their ideas. They need to _remove_ obstacles to creativity, not create them.

One effective way managers can reduce obstacles is by telling everyone about future projects well in advance. Creative people are always thinking about a variety of problems at the same time, often subconsciously. If they have ample time to consider a new project, they'll already have great ideas for it by the time it starts.

The author told his team six months in advance before they had to start adapting their games for the European market, for instance. When the starting time came, they'd already studied European market standards and thought of some strategies for it entering it.

It's also necessary to have managers _themselves_ contribute to the creative process. They need to do more than simply veto ideas. Managers who just reject their employees' ideas all the time are not helpful.

The author had a policy at Atari that managers weren't allowed to say no to _any_ ideas. Instead, they had to offer suggestions on how to improve them, or find the idea's positive qualities. The managers were nervous at first to adopt this policy, but it ultimately forced them to be more imaginative.

If managers have any objections to anything, have them write their thoughts down and take personal responsibility for them. The objections will be more thoughtful and precise, so they'll also be more constructive.

### 8. Encourage your employees to take risks, and reward their bad ideas and failures. 

When was the last time you took a risk? Did you feel frightened? Most people avoid risks, because they're too wary of the possible outcomes.

People feel the same way at work. This doesn't make sense at all, because risk-taking is the _only_ way a company can succeed — or even survive. Luckily, you can inspire a productive culture of risk-taking without ruining the company if you do it the right way.

Risks are absolutely necessary if you want success. James Dyson, the founder of _Dyson_, took a huge risk when he went four million dollars in debt while creating his innovative vacuum cleaner. It paid off: today, his company is worth two billion dollars.

Companies also need to celebrate their failures, so people won't be afraid of taking risks. At _Chuck E. Cheese_, a restaurant chain founded by the author, regional managers received an unusual award for the biggest screw-up of the previous quarter: a tin turkey. When you can laugh at a failure, it'll be less painful.

Failures are also great learning opportunities. The first Apple computer, _Lisa_, was a failure. It was slow, expensive and didn't sell. Apple learned from the failure, then went on to create the improved model, Mac. It was a big hit.

There are ways to make sure risks don't threaten the company's existence. Try to take lots of small risks, instead of a few big ones. Assess your risks to figure out how much you stand to lose.

Bushnell learned this lesson in 1984, when he put all his money into developing a robot that served as a pet and helper. The project failed because of technical difficulties, and he suffered an almost complete loss. Since then, he has never risked more than 10 percent of his budget on new ideas. So if something fails, don't give up — let the experience teach you how to work better next time.

### 9. Promote strategies and habits that induce creativity. 

Have you ever felt like you ran out of ideas or imagination? Most people have. Luckily, there are strategies you can use to jump start creativity. Companies and individuals alike can implement them in the workplace.

When you run out of creative steam, change something in your daily life. Mixing things up activates your mind, which can pave the way for new ideas.

When Frank Zappa felt short on ideas, he made himself wake up an hour earlier every day. After twelve days, he was waking up at eight in the evening. He said this shift in his schedule was enough to get his imagination working again.

Sometimes a change of scenery can do the trick. Bushnell got his idea for the first automotive navigation system while on a boat in the Pacific, for instance.

You can also kick-start creativity by trying something new. Why not roll a dice to decide what you're going to do next? Sometimes that can lead you to try great things you otherwise wouldn't.

Bushnell once wrote a list of things he could potentially do, and rolled a dice to pick one. His list included very unusual activities, like climbing Mount Kilimanjaro and going skydiving. He rolled "write a book," so he did.

Finally, when you're coming up with new product ideas, don't worry if they're expensive for consumers. Market the first versions to wealthy people.

Rich people can pay any price, so initially design your products for them. Once the product is successful, you can bring the price down to something more people can afford.

This happened when the bicycle was invented in Paris in the 1860s. It was expensive and mostly sold to nobles, and remained a luxury item until the early twentieth century. The bicycle had to go through this period before it could become the basic form of transportation we know today.

### 10. Final summary 

The key message in this book:

**Creativity is the most important part of any business. If you want to succeed, go out and look for creative people, and don't be concerned if they seem strange on the surface. Create an environment where they feel free to express themselves and test their ideas. Make their workplace enjoyable. If you nurture talented people in the right way, the rewards are potentially life-changing, for both you and them.**

Actionable advice:

**Reward your employees' failures.**

Never punish someone for taking a risk — risks are the only way a company can succeed. Instead, encourage risk-taking, and if something doesn't work out, view it as a learning experience. If you reward people's failures, they'll feel more comfortable trying new ideas in the future. Laughing at a failure also makes it manageable.

**Suggested further reading:** ** _Steve Jobs_** **by Walter Isaacson**

This book chronicles the audacious, adventurous life of Steve Jobs, the innovative entrepreneur and eccentric founder of Apple. Drawing from Jobs's earliest experiences with spirituality and LSD to his pinnacle as worldwide tech icon, Steve Jobs describes the man's successful ventures as well as the battles he fought along the way.
---

### Nolan Bushnell

Nolan Bushnell is the founder of _Atari_, _Chuck E. Cheese_ and over a dozen other companies. He's also one of the inventors of _Pong_. At Atari, he hired Steve Jobs and Steve Wozinak, and helped launch both of their careers.

