---
id: 5534f9e066386300075b0000
slug: finite-and-infinite-games-en
published_date: 2015-04-21T00:00:00.000+00:00
author: James P. Carse
title: Finite and Infinite Games
subtitle: A Vision of Life as Play and Possibilities
main_color: EE4889
text_color: B23667
---

# Finite and Infinite Games

_A Vision of Life as Play and Possibilities_

**James P. Carse**

_Finite and Infinite Games_ (1986) offers two contrasting viewpoints on how to live your life, whether you're engaging in sexual relationships or warfare. Carse argues that any activity can be seen as either a finite or an infinite game, the former being end-oriented and the latter leading to infinite possibilities. He reveals how the world appears through the eyes of those who play with the finite or infinite in mind, and concludes that how and what games we play are our own choice.

---
### 1. What’s in it for me? Discover how all of life can be seen as one big game – then choose how to play it. 

What comes to mind when you hear the word _game_? Monopoly? Super Mario? Chess?

Games are so much more than this. In fact, all of life can be seen as one big game. Unlike the board games you played as a kid, these life games can be played in two very different ways.

These blinks explain the differences between games that have clear goals and rules, and games that don't. Some people see life as a zero sum game with clear winners and losers and a fixed amount of time to play. Others see it as a series of moments that all carry endless possibilities and no winners or losers. From your career to your sex life, the way you choose to play will have a huge impact on your life.

In these blinks you'll learn

  * why some people are so obsessed with titles;

  * why your colleague has to win every argument; and

  * why your idea of time is completely wrong.

### 2. You can see almost every part of life as a finite or an infinite game. 

When we think of games, we usually think of hide-and-seek, truth-or-dare or similar children's games. But if you reflect a little further, we adults have our own games too.

The games of adults, however, can be _finite_ or _infinite_.

Finite games have specific temporal, spatial and numerical boundaries. A finite game always has a clear beginning, a particular playing field and a certain number of players. Besides these external restrictions, there are also internal limitations that mean that the rules of the game must be agreed upon in advance by the players. Then the players compete in line with these rules with the aim of winning.

Then the game is over.

If this doesn't really sound like the real world to you, consider a general election. The rules of an election are clearly outlined; a winner is decided by the amount of votes on a given day, there are certain practices that are or are not permitted (for example, vote rigging is banned) and only one candidate is allowed per party.

Infinite games, on the other hand, are the polar opposite of finite games.

While people play finite games to win, infinite games are played with the goal of continuing the game. Therefore, infinite games don't have external or internal restrictions. Anyone can participate in the game anytime and anywhere.

Consider music composition: there will never be a best symphony, because there will always be new composers writing inspiring music. No composer makes music to win or be the best, but because the compositions she writes are invitations for even more people to join in the game.

All this might sound a bit abstract at first, but read on to get a better grasp of what life looks like according to the finite and the infinite player.

> _"Infinite play remains invisible to the finite observer."_

### 3. Finite players are limited by the world observing them, while infinite players know no boundaries. 

We've seen that finite games necessarily have temporal, spatial and numerical boundaries. But other boundaries are needed, too.

Imagine if the rules for an election stated merely that there should be two candidates and that the one with the most votes would win. On their own, these rules would mean the two candidates could be common criminals, and you could have new elections every day if you liked!

This is why finite games are also regulated by the _audience_ observing them. The audience determines when the game occurs and who the players are. In the example of the election, the electorate is the audience who decides who the two candidates are and when the election will be held.

This means the finite player is not free to play the game at his or her leisure, but must depend on the time allotted by the audience.

The finite player is under pressure from the audience to play and finish the game. For instance, the finite game of an exam entails the player working to create something — answers in this case. She then spends the two allotted hours working because that's the amount of time her teacher has given them to complete it.

In contrast, infinite games have no boundaries, so infinite players govern their own time.

As infinite games have no beginning or end, time doesn't pass for an infinite player. Instead, each moment presents a new beginning with new possibilities for the game to unfold and develop.

Instead of consuming time, the infinite player fills it with play. He won't finish a work project in two hours simply because that's the allotted time; he will work for as long as he desires, inviting others to join in.

> _"Time does not pass for the infinite player. Each moment of time is a beginning."_

### 4. Where finite players see society, infinite players see culture. 

Whenever we play games, we need others to play with us, whether they're our teammates or our opponents. However, there is a difference in how finite and infinite players perceive their game in relation to other players.

Finite players see society essentially as one big finite game consisting of smaller games such as school or a profession, with every player trying to win against others and be awarded a _title_.

This title can simply be recognition for having won a past finite game. For example, for the finite player, a priest would be "Father," as this is the title earned for having completed specific training and, as a result, winning the game of becoming a priest.

This title is then displayed through property, and society is obliged to honor it. For example, imagine you attend a respected college and become a successful lawyer. To show your success, you might then purchase an expensive car.

Infinite players are different. They see society as a continually developing culture.

Focusing on titles means concentrating on previous victories. For infinite players, these are irrelevant because their attention lies on the future and the possibilities it holds. Therefore, infinite players are more concerned with offering a vision that could encourage others to join in and work to develop that vision, rather than offering an ultimate solution to a problem.

For instance, an infinite player wouldn't fight poverty by struggling to provide citizens with a specific amount of goods. He would see more value in sparking a discussion on where poverty stems from in the first place.

> _"It is a principal function of society to validate titles and to assure their perpetual recognition."_

### 5. Finite players strive to dominate through winning, while infinite players strive to coexist through playing. 

The desire to dominate accompanies finite players everywhere — from their behavior in the bedroom to conversations with friends.

Players of finite games aim to win in order to gain control over others. By flaunting their titles they exhibit their superiority and overshadow others. They endeavor to do this in every way possible, and this typifies their gameplay.

Finite players see even basic conversations as a game to be won — they don't want to discuss, they just want to give explanations and convince the opponent of their truth. In these cases, knowledge becomes the title to be won.

For the finite player, sexual relationships are also a way to take command over their partner and conquer them. This might involve concealing their own sexual desires through elaborate courting rituals until a desired end is achieved.

The infinite player, on the other hand, is not concerned with winning over others, because to them, only the game matters. As infinite games don't have winners, infinite players don't care about demonstrating their superiority. They play for the sake of the game.

For the infinite player, conversations are interactions based on give and take, where listening is valued as much as talking. She sees them as a discourse, rather than a chance to give an explanation. Infinite players don't assert that what they say is truth. Instead, they offer their conversation partner a perspective, and are open to any new knowledge that the conversation might bring.

Infinite players also aren't fixated on a particular outcome in the game of sexuality, whether it's obtaining physical pleasure or conceiving a child. They play in order to explore and understand themselves and each other.

### 6. Finite players are a product of their past, while infinite players transform theirs. 

How much does your past weigh on you? Finite players are compelled to play by their fixation on the past.

Finite play is actually a game of the past where finite players eagerly assume the roles given to them and thus play a part that was designed _in_ the past and _for_ the past. For example, if you were born as the eldest son of a wealthy family, as a finite player you will adopt the role of being the heir of the family, safeguarding its property and reputation.

Consider the psychology behind entering a finite game. As it's really all about winning, the players must feel the urge to prove themselves. How often a finite player wins or loses is irrelevant because she dwells on their past, where she's still a loser. If she wasn't so wrapped up in the past, she wouldn't enter a finite game in the first place.

See it this way: if you feel like a deadbeat in front of your classmates, you might be driven to become successful so that you can prove them wrong. However, no matter how successful you become, you still feel like the same loser, because it's your motivation to keep grinding away at your job.

Infinite players view things differently. They liberate themselves from their past using their uniqueness. They can make peace with the past by acknowledging their own genius and originality. For an infinite player, the past is history. It doesn't determine the future, because the future of an infinite game is open-ended and rife with possibility.

For an infinite player, being born as the eldest son of a wealthy family is not a blueprint for life; it's the starting point of an infinite number of possible games.

> _"The more negatively we assess ourselves, the more we strive to reverse the negative judgement of others._

### 7. You can choose what kind of game you want to play and what kind of player you want to be. 

There are numerous differences between finite and infinite games, but this doesn't mean that there aren't finite games _within_ infinite ones, or that you can't opt out of them altogether. After all, you have the power to choose what kind of game you want to play.

Remember that participation in each and every game is voluntary.

Finite players often feel they are obliged to play, and feel pressured to play in a certain way. But in the end, no one can force you into the game if you don't want to join it.

It's true that finite players are chosen externally. You can't be a lawyer until you have passed the bar, for example. It's also true that you have to do predefined things to stay in the game as a lawyer, such as go to court and attend meetings. However, no one can force you into being a lawyer if you don't want to be one.

We all opt to play and adhere to the rules of the game by our own free will, so you can choose which game you want to take part in.

Finite players feel trapped because they have to put on a particular guise in order to play certain roles properly. For example, you should be seen as wise and just to be a lawyer.

The problem is, sometimes these masks become too convincing even to ourselves, and we get lost in the game, forgetting that we're only assuming a role and can always quit the game. So the lawyer who feels bound by her duties must remember she can always leave her job and move to Jamaica if she wants to!

At the end of the day, the game you play is your personal choice. As long as you don't hide behind masks and or let yourself be duped by the masks of others, you can play as you wish.

### 8. Final summary 

The key message in this book:

**Life can be seen as a finite or an infinite game. Finite players focus on the outcome of the game, such as winning and losing, whereas infinite players center on allowing the game to continue and give rise to multiple possibilities. However you choose to play this game is up to you.**

**Suggested further reading:** ** _Beautiful_** **_Game_** **_Theory_** **by Ignacio Palacios-Huerta**

_Beautiful_ _Game_ _Theory_ shows us how applicable economics is to our daily lives by looking at the fascinating world of professional soccer. By examining compelling statistics and studies on shoot-outs, referee calls, and ticket sales, _Beautiful_ _Game_ _Theory_ offers some interesting insights into the psychology of behavioral economics.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### James P. Carse

James P. Carse is a professor emeritus of history and literature at New York University and was featured in the CDC radio series, _After Atheism: New Perspectives on God and Religion._

