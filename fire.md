---
id: 5713b1825236f20007c16f6c
slug: fire-en
published_date: 2016-04-19T00:00:00.000+00:00
author: Dan Ward
title: F.I.R.E.
subtitle: How Fast, Inexpensive, Restrained, and Elegant Methods Ignite Innovation
main_color: F46C31
text_color: A84B22
---

# F.I.R.E.

_How Fast, Inexpensive, Restrained, and Elegant Methods Ignite Innovation_

**Dan Ward**

This book (2014) outlines the _F.I.R.E._ method to jumpstart innovation, a guaranteed process that will stimulate you and your team by streamlining resources and creativity. At its core, F.I.R.E. is about setting restrictions so you can produce much greater results.

---
### 1. What’s in it for me? Learn how to successfully manage innovative projects with a new method. 

Project management is tough. You have to juggle many issues, such as budget, time frame, supplier contracts and team concerns. Luckily for you, you can refer to earlier projects to get an idea of best practices or trouble spots. 

Or can you? If you're working on a truly innovative project, this might be difficult — you and your team may be going where no one else has gone before. Other projects might be interesting but not helpful; where, then, can you turn for guidance to keep budgets trim and deadlines met?

These blinks are your guide to managing innovative projects on deadline and under budget. They'll show you how to carry your project to success, by following just a few rational and logical guidelines. 

In these blinks, you'll discover

  * when "stormdraining" can be more useful than brainstorming; 

  * how borrowing technology keeps NASA projects on time and under budget; and 

  * why a US fighter jet project missed engaging the Soviet "threat" by a good decade.

### 2. The F.I.R.E. method gets you high-quality results in the fastest, most efficient way possible. 

When you head a creative project, plenty of challenges will come your way. Luckily, there's a fail-safe method that innovative project managers can use to stay on track, and it's called _F.I.R.E._

F.I.R.E. stands for _Fast_, _Inexpensive_, _Restrained_ and _Elegant_. Everyone can use the F.I.R.E. method to improve their processes and achieve their goals.

Make your project _fast_ by dividing it into smaller tasks that can be completed quickly. Big projects often drag, and sometimes it's hard to feel you're making progress. When you design for yourself a smaller group of shorter tasks, you can focus on each task one at a time, and set clear, achievable deadlines. 

This means you have to stick firmly to a schedule, however. And being fast isn't just about working quickly — you have to focus as well on quality. If you work quickly but sloppily, you'll only create more work for yourself further down the line. In the end, you won't really have saved much time.

Keep your project _inexpensive_ by maintaining a small budget, and aim to solve problems with your smarts before throwing money at them. Inexpensive doesn't mean cheap, however — it's about efficiency. Learn to make the most out of what you have. 

Being _restrained_ is about staying in control. When you're restrained, you don't allow unexpected circumstances to determine the direction in which your project heads. Instead, _you_ control the situation by holding regular meetings, maintaining short schedules, organizing small teams and keeping a tight budget. 

Finally, being _elegant_ is about focusing on simplicity. Remember: less is more. Simple projects, if done correctly, are better than complicated ones, as they're easier to work on and ensure high-quality results.

> _"The most successful project leaders ... tend to deliver top-shelf stuff with a skeleton crew, a shoestring budget, and a cannonball schedule."_

### 3. To solve a specific problem, first work on generalizing the problem to identify your general needs. 

Often when you have a question, you "ask" Google. And in turn, the Google search engine provides you with a host of answers, whether you're looking to lose weight, fix a stove or consider space flight. 

How does Google do this? The company's engine simply searches through the tons of research stored online, considering the millions of questions that have already been answered. Innovation works the same way; you just need to know what to search for. 

To find a solution to a specific problem, however, the first thing you need to do is _generalize_ it. Here's another handy method to help you do this. 

_TRIZ_ is an acronym that describes a Russian method of inventive problem-solving. This method explains how any technical problem can be solved in _four_ steps. 

First, you identify your specific problem. Second, you generalize it. Third, you find a _general_ solution to the general problem. Fourth, you use the general solution to develop a _specific_ solution to your specific problem. That's TRIZ!

Let's say you're trying to design a larger engine for a more powerful airplane. You want the plane to fly further and higher, but it can't lift off properly because it's now heavier. That's your _specific_ problem. 

Next, you focus on generalizing your problem, which in this case, would be a problem of the plane's power-to-weight ratio. Once you find a general solution that addresses this, you can figure out how to apply this general solution to the design of your _specific_ plane. 

When solving problems, you should also identify your _needs_. It's not always obvious what resources you have and which new resources you might need to complete your task. 

If you're trying to design a plane engine, you probably have tons of different materials available to you — but which ones would work the best? Once you identify that you need a lighter material, you can focus your efforts on finding it.

> _"Sometimes we don't know we need a wheelbarrow until we see it. But other times we honestly don't see it until we know we need it."_

### 4. Avoid racking up extra costs and annoying delays by sticking to a set schedule and a limited budget. 

Have you ever tried to experiment with a favorite recipe, changing details here or there, and ended up with something you had to throw out? Sometimes it's best to stick to the original recipe.

Innovative projects can work the same way. When you have a time frame and a budget, try not to deviate from your plan, as you'll just trip yourself up if you decide along the line to complicate things. 

In 1981, US engineers began work on the F-22 Raptor stealth fighter jet, an advanced piece of machinery conceived to fight Soviet technology. The project however was completed in December 2005 — years after the Soviet Union had collapsed. 

So what happened with the project?

The F-22 project ran into trouble in 1989 when designers extended the deadline for its completion by six months. They wanted the fighter jet to be "perfect," so they kept tweaking features, and in the process, adding more costs and further delays. 

As a result, the project kept changing to accommodate the added functions, and delays eventually snowballed until the project was a good ten years late. 

You can avoid this! Stick firmly to a schedule and budget once they are set. Don't aim to include every possible feature in a product or project. Try to make something that solves your particular problem, or your project will become irrelevant, just like the F-22. 

While designers toiled on the soon-to-be-useless F-22, another invention surpassed it: the _unmanned aerial vehicle_ (UAV), or as we know it today, the drone. 

A drone embodies the F.I.R.E. method: it is fast, inexpensive, restrained and elegant. 

The drone project succeeded where the F-22 failed because the project as a whole was focused on answering a specific question in a short time period and on a limited budget. The Dragon Eye drone, for instance, only has one function: surveillance. It is also inexpensive; a Dragon Eye can be produced for roughly $60,000.

> _"Start anywhere; constrain anything. Tighten the budget, and the schedule will constrict. Simplify the process and the architecture, and we'll see the cost drop."_

### 5. NASA missions focus on simplifying and accelerating projects, innovating only when necessary. 

When you hear the word "innovation," you might assume it describes something complicated and expensive. That's not necessarily the case, however. The US space agency NASA shows why. 

The US National Aeronautics and Space Administration (NASA) has launched many innovative missions, given its dedication to _simplifying_ and _accelerating_ projects. 

In 1999, for instance, NASA began a spacecraft project called _Stardust_, aiming to collect particles from the tails of comets in the galaxy and to bring them back for study. 

NASA set a tight schedule and budget for Stardust, and completed it on time and under budget, for some million dollars less than what the team had been allocated. 

How did they achieve such success? The Stardust team had a clear set of mission requirements, and members focused on the three most important tasks: encounter the comet; collect 1,000 particles; and bring the particles back home. 

There were other goals (such as photographing the comet) but these goals were merely _desirable_ — meaning they were secondary to the project's three main goals. 

The success of Stardust also illustrates the importance of focusing innovation to specific needs. 

NASA could have built all the elements of the Stardust project from scratch, but the team decided to keep things simple. Project manager Ken Atkins cut costs by using tools developed for previous missions. Stardust's Motorola Radio, for instance, was originally designed for the 1998 Mars Surveyor mission. 

Using established solutions allowed the team to focus efforts on developing tools they didn't have. In fact, the only real innovation that came from the project was a material for collecting particles, called aerogel. The rest of the project creatively used innovations from the past!

> _"Who cares if … the radio is borrowed, so long as the final product is awesome at doing something nobody has ever done before."_

### 6. When it comes to true innovation, less is always more. 

Imagine how awkward it would be to wear every piece of clothing you own. It's nearly impossible, and even if you did succeed, you'd just look ridiculous. 

One shirt, one pair of pants: it's better to keep things simple. 

Innovation is the same. You are guaranteed to stumble if you insist on too much of it for yourself or your project. Keeping it simple doesn't just look better, however; it's cheaper and faster, too. 

Google's _Chromebook_ is a good example of this. The laptop comes with only the most commonly used Google features, like searching the internet or using Google Drive. 

While Google could have outfitted its laptop with every feature it offers, it would have just cost the company more money and taken more time to develop. A few extra features weren't enough to justify costs and delays. 

Finding the right balance of complexity is called _stormdraining_. Stormdraining is the opposite of brainstorming: it's about letting go of concepts or features that don't provide significant benefit to your project. 

To stormdrain, you locate the midpoint between simplicity and complexity; and this point will vary depending on the project. Start by removing one feature from your product at a time. If the system still works, take out another piece. Keep going until you can't strip any more features and still have a functioning product. 

This process takes time and creativity, but it's much more efficient than building a product that is unnecessarily complex and expensive! 

So remember to be critical of your project — get rid of anything that eats up resources and doesn't provide significant reward. Focus your efforts on developing the tools and products you really need.

> _"At some point, we need to re-engage our critical thinking skills and get rid of all the stupid ideas we created so proudly."_

### 7. Final summary 

The key message in this book:

**Effective innovation is about focusing energy and resources on your most important tasks. You need to work** ** _fast,_** **keep your project** ** _inexpensive_** **and stay on track by** ** _restraining_** **your budget and time frame. Aim to produce something** ** _elegant_** **, not something overly complex. When you streamline your efforts with the F.I.R.E. method, you won't just finish your project on time, you'll produce something of higher quality, too.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Scrum_** **by Jeff Sutherland**

Learn all about Scrum, the project management system that has revolutionized the technology industry. This is a uniquely adaptive, flexible system that allows teams to plan realistically and adjust their goals through constant feedback. By using Scrum, your team can improve their productivity (and the final result) without creating needless stress or working longer hours.
---

### Dan Ward

With three engineering degrees and over 20 years with the US Air Force, author Dan Ward's expertise on defense acquisition reform has been featured in publications released by the US White House, the US Senate and the British Parliament.

