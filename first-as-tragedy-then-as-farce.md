---
id: 56cc44189bb2160007000087
slug: first-as-tragedy-then-as-farce-en
published_date: 2016-02-21T00:00:00.000+00:00
author: Slavoj Žižek
title: First as Tragedy, Then as Farce
subtitle: None
main_color: 7D93CD
text_color: 4E5B80
---

# First as Tragedy, Then as Farce

_None_

**Slavoj Žižek**

_First as Tragedy, Then as Farce_ (2008) sets out to uncover the hidden ideology that surrounds us in our everyday lives. In examining how capitalist society affects our lives and permeates the way we think, the book ultimately offers a new and better alternative to the way our world is structured today.

---
### 1. What’s in it for me? See how the ideology of capitalism seeps into every aspect of society. 

Karl Marx once remarked that, in history, events happen "first as tragedy, then as farce." Although he was writing in the nineteenth century, we can still see this observation playing out if we look at more recent events. 

First, we have a tragic event such as 9/11, followed by a farcical one like the financial crisis of 2007-8. Both events were at least partly borne of the same global system; the first one was truly horrendous, the second almost absurd.

Why does this happen? Why, after tragic events, do we persist with the methods that eventually produce farcical ones? To put it differently, why do we continue with a capitalist system, which over the last decades has led to so many crises?

These blinks, based on the thinking of influential modern philosopher Slavoj Žižek, try to explain why. They also provide a hint of what could be a better system: communism. Curious about that? Let's get into it.

In these blinks, you'll discover

  * why, although you have many permissions, you have very few rights;

  * why crises in capitalism actually make it stronger; and

  * why communism is nothing like you think it is.

### 2. Capitalism isn’t a mechanism, but an ideology that is strengthened by crisis. 

People who have grown up under capitalism view it as the perfectly natural evolution of social organization; but they would be wrong.

While capitalism is often presented as a completely neutral method of organization that makes the economic system work like a well-oiled machine, the fact is that capitalism is an _ideology_ — a system of ideas that makes us do what we do.

Too often, capitalism is portrayed in technical terms as something that simply _works_, and not as something that is imbued with ideas about how the world works.

However, the fact that capitalism can function in any sort of civilization with _systems of meaning_, such as Buddhism, Christianity or the belief in the welfare state, is proof that capitalism doesn't just _work_. These systems of meaning serve as a type of security that people can rely on when their dreams of self-made success — dreams embedded in the capitalist ideology — inevitably crumble, unfulfilled.

If capitalism were natural or neutral, we wouldn't need these systems of meaning to fall back on.

So why do so many of us consider capitalism to be natural and desirable, and continue to trust in a system that goes through crisis after crisis? Well, each crisis of capitalism acts as a type of _shock therapy_ that keeps us dreaming.

While crises can sometimes make us challenge our beliefs, they more often make us go _back to basics_. Rather than critically reflecting on the dominant ideology, we instead view crisis as the result of not adhering to the ideology strictly enough.

For example, when socialist regimes were faced with the crises that led to their demise, the response from the leaders was, _it's not socialist enough_. When the people of Eastern Europe rebelled against their Soviet-supported governments in the twentieth century, they were met not with compromise, but with even more ardently socialist regimes. 

A more contemporary example is the 2008 financial crisis, which has been described as the result of too much state intervention, laws and regulation. In reality, it was a result of woefully inadequate regulation.

> _"Like love, ideology is blind, even if the people caught up in it are not."_

### 3. Capitalism falsely credits itself for society’s freedoms while blaming systematic errors on individuals. 

Conventional wisdom states that free trade and open markets — both of which are essential tenets of capitalism — are the basis for freedom and democracy. But is this really the case? As you'll soon find out, not really.

In the capitalist society we live in, many of the freedoms and rights we enjoy are portrayed as a result of capitalism, when they are in fact the result of revolutionary politics.

Universal suffrage, labor laws and freedom of the press, just to name a few examples, are all results of revolutionary, emancipatory movements — to which capitalists in power consistently stood in fierce opposition.

Similarly, the list of rights demanded at the end of Karl Marx's and Friedrich Engels's _Communist Manifesto_ were achieved (with the notable exception of the abolition of private property) through pressure from left-wing groups.

In the same way that capitalism steals credit for things it never accomplished, it allows us to turn a blind eye to our own responsibility for its errors.

For instance, we see our roles in public life (like our job or role as a parent) as something removed from our sense of who we "really are." We believe that there is something innate in us, our truer self, that is different from the things we do, our jobs or capitalistic self.

This outlook removes our responsibility, and indeed everyone's responsibility, for the injustices within the system; our public behavior is not considered part of our private, human self. 

An example of this is a report of an Israeli soldier who was helping remove Palestinians from their appropriated homes. When confronted, one soldier tried to explain that he was only human, showed pictures of his baby daughter and insisted that he was just doing his duty. 

By removing his private self from his so-called duties, he could carry on without having to think about the consequences of his actions. 

Moreover, the idea of the private self makes collective action difficult, as it is seen as oppressive to the inner self and our individualism.

### 4. Capitalism has entered a new phase that can be misconceived as egalitarian. 

Despite offering some hopeful prospects, the freedoms we've achieved in spite of capitalism have failed to establish an equal society. 

Capitalism has reached a new stage that has less clearly defined hierarchical structures than in the past. Since the 1970s, capitalism has turned away from an organizational structure whereby the boss at the top controlled everything below. Instead, modern-day labor is organized into teams and projects, which at first glance appear to constitute a more egalitarian and equal distribution of power. 

But this structure just serves to conceal the fact that there is still someone at the top of the ladder, like the CEO or department head, even if the rest of the organization doesn't follow the classical capitalist model.

In this stage of capitalism, there hasn't been a real shift in power; rather, we simply have permission from the ones with power to do new things.

_Permissions_ are different than _rights_, in the sense that permissions can still be revoked by those who have the rights to do so — that is, those who have the power.

We can see this in collaborative and participatory systems of labor organization, where workers have permission to be involved in decision making, yet never have the final say. Instead, that power still lies with boss, who is the ultimate decision maker. 

Similarly, LGBT people, minorities and other marginalized groups have gotten more permissions in recent history, like the legitimation of same-sex marriage, without having attained any actual power. 

For example, when Proposition 8, the law approving same-sex marriages in California, was revoked, it was clear that extending the right to marry to same-sex couples still hadn't given same-sex couples the power to truly exercise that right. 

The result has been a society that is perceived to be more egalitarian, but has not undergone any real restructuring of the allocation of power.

### 5. The hidden ideology of capitalism shifts focus away from our real enemies. 

What if the conflicts around the world actually weren't the real conflicts at the root of global problems?

In capitalism, we latch onto ideological fetishes that hide the harsh truths behind other ideas. In modern usage, the word _fetish_ evokes thoughts of bizarre sexual activities. The older meaning of the word, however, is simply "an object that has been given a greater value or quality." For example, when someone's grandmother dies, they might keep an object that is somehow connected with the deceased, like a book or a piece of jewelry, through which their grandmother can "live on."

Western Buddhism offers a perfect example of an ideological fetish. Buddhism enables us to take part in the frantic, fast-paced capitalist society that surrounds us while maintaining the lie of mindfulness and inner peace.

Fetishes can also be used to cover up real conflict by acting as a conduit for people's frustrations.

For anti-Semites, the fetish is "the Jew," who is portrayed as an almost mythical force that rules the world. This fetish — which in this case takes the role of a scapegoat — buries a very real problem, namely the class conflict between the capitalists and the rest of society.

Convincing people from the working class that this particular fetish only masks their actual, continual conflict with the ruling class is very difficult. For example, in the 1920s, many German Communists joined the Nazi party, but the reverse was extremely rare. 

We find a similar example in the way Jews and the State of Israel are used by Islamo-Fascist ideology. Here, the State of Israel is a fetish that represents the true problem of an impoverished region where all revolutionary politics have failed. 

The real problem, of course, is the fact that the people in these countries have been robbed of all hope of a true revolution and an egalitarian society. Once the fetish is removed, however, the real system and the harsh reality it perpetuates is uncovered.

### 6. Ideology is hidden in the new ways we consume and buy things. 

What motivates you to buy one brand of, say, toothpaste over another? While it may be the material or economic benefit that one brand has over the other, in all likelihood the reality is far more complicated.

Consumption has shifted from a _symbol_ of culture, experiences and identity to actually being an _experience_ in itself. In other words, the act of buying and choosing what to buy is the real experience; the usage of the product is secondary.

In this way, the act of consuming is less about procuring something useful and more about demonstrating our beliefs.

Take one Starbucks advertising campaign, for example, which portrayed their coffee as both environmentally friendly and good for the workers in the field; the tagline was "ethical coffee." Of course, this tagline downplays the fact that Starbucks is a huge capitalist corporation, designed to accumulate wealth for its capitalist owners rather than for the people who work for them (least of all the poor souls who pick the coffee beans). 

When we buy Starbucks's "ethical coffee," we're really buying an experience: the experience of making a seemingly positive ethical decision. We can buy coffee anywhere, but Starbucks gives us the opportunity to have the experience of doing something good — even if the political impact of our action is negligible.

But consumption isn't just about the inward satisfaction of positive experiences. It also emanates outward, as our consumption decisions show people who we are.

When companies ask us what we want from them, they're really asking us who we are, or who we want to be. We respond by buying products or brands that demonstrate how we perceive ourselves and want others to perceive us.

This quest for authentic experiences transforms consumption into a way to find meaning in our existence. When we consume organic food, for example, it's not just for the taste — it's an expression of who we are: caring, environmentally conscious people.

### 7. A revamping of the communist idea is the only real remedy for capitalist ideology. 

What comes to mind when you hear the word "communism"? For most people, the word conjures up atrocities committed by Joseph Stalin or other communist leaders. But could there ever be a viable communist alternative to the capitalist system?

If this is to be the case, as the author believes, communism must be re-thought from the ground up in order to lay the foundation for collective action.

Vladimir Lenin himself wrote that the only viable versions of communism "begin from the beginning," rather than trying to build upon the systems that preceded them. In this way, an egalitarian revolution isn't a movement of gradual change; instead, it repeatedly starts from scratch in order to become better than previous attempts.

Philosopher Alain Badiou calls this approach the _communist hypothesis_. According to Badiou, the idea of communism is worth pursuing, but the historical focus on the state's expropriation of property as well as other implementations of communism might be misplaced. 

Still, the fundamental drive behind communism is collective action, without which there is no possibility for a better, egalitarian society.

The question of _how_ we relate to property and ownership is what makes communism both unique and ultimately the only real means of achieving an egalitarian society.

Whereas capitalism is the support and defense of private property, and socialism of state property, communism questions the very idea of property in any form. This is a crucial insight, as there are some things that we can probably all agree shouldn't be owned by anyone due to their importance for survival. For example, most people would agree that the air that we breathe shouldn't be a commodity.

The question of property comes into focus when we look at the problems in today's world; the following blinks will reveal exactly how.

### 8. Today’s world contains four great antagonisms, three of which concern culture, environment and humanity. 

So which problems in our capitalist world act as _antagonisms –_ essential battlegrounds and arenas for resistance — that communism is best positioned to respond to?

The first three antagonisms in today's world center around our survival. They all involve the shared resources, or _commons_, of our societies

The first, the _commons of culture_, are things like language, means of communication and education, as well as shared infrastructure. Today these commons of culture are privatized under the banner of "intellectual property," which restricts access to culture.

The second is _commons of external nature_, that is, our natural habitat. Environmental disaster threatens our very survival, be that through climate change, the extinction of species or limited access to clean water. This particular problem can't be solved by national governments or private companies, as it is a global problem that all humanity faces.

Finally, there is the _commons of internal nature_, which refers to what it means to be human. Today's advances in genetics have opened up endless possibilities for remodeling our very nature. Imagine what could happen if a state or a company was the sole owner of such technology?

These antagonisms create the need for a new global organization. If they are not handled in a way that is good for _all people_, we risk destroying the planet's resources and forcing ourselves to genetically enhance ourselves to cope, all the while consuming culture to maintain some semblance of humanity. 

And guess who will ultimately profit from these modifications and cultural consumption, made necessary by the very havoc they wreaked. Yep. You got it: capitalists. 

But even if we address these three antagonisms, we still won't move toward an equal and functioning society without looking at the fourth, which we'll explore in the next blink.

> _"A revolution never occurs when all antagonisms collapse into the Big One, but only when they synergetically combine their power."_

### 9. The conflict between the Included and Excluded lies at the heart of the need for communism. 

While the previous three antagonisms focus on the survival of the human race, the fourth is the division between the _Included_ and the _Excluded_. It is this antagonism that is most crucial, and also the one that requires a radically new way of thinking.

The Excluded comprises all those who don't have a place in the social order and are thus excluded from decision making. This group forms a "part of no-part": their place is exactly that they have _no_ place in the public sphere of debate.

Once they're included in society, however, they cease being the Excluded.

One good example is paperless immigrants. They are truly a "part of no-part," as they legally don't belong to any country. As long as they are in this position, they essentially have no rights at all.

Once they acquire papers and are finally granted citizenship, they stop being Excluded and are then _Included_. Once Included, the paperless immigrant virtually disappears. Thus, paperless immigrants are either Excluded from the ruling group or simply cease to exist in society.

The _proletariat_, or the working class, is similarly Excluded from the socio-political process. Those who want an open liberal democracy try to bring in as many voices from the Excluded as they can. However, as soon as the proletariat is included in the political decision-making process, they cease being Excluded, cease being proletarians and become part of the same oppressive political machinery that oppressed them to begin with.

Communism's solution is to prioritize the perspective of the Excluded instead of the Included. Rather than enact reforms that try to make as many people Included as possible, it instead tries to transform society to fit the needs of the Excluded by making them the base for politics. 

By giving political power to the most marginalized groups in society, the institutional distinctions between Excluded and Included begin to disappear.

### 10. Forces that seem opposed to capitalism will not result in an egalitarian society. 

Many people dismiss communism as something that belongs in the dustbin of history. After all, hasn't it already been proven not to work? On the contrary! In fact, no truly communistic society has ever existed. 

The failure of the USSR, often touted as the final nail in the coffin proving the inadequacy of communism, was actually a result of socialism, not communism. In a socialist society, the commons are owned by the state, and within the socialist state people are assigned a role in the community. We call this _communitarianism_.

This, however, is not to be misconceived as the egalitarian collective of communism. Under socialism, bureaucrats are still the decision makers (the Included) while the proletarians remain Excluded, thus making true equality impossible. 

Moreover, many of the "communist" societies throughout history have, in actual fact, greatly benefited capitalism. The economic models employed in Asia offer a perfect example.

Lee Quan Yew, Singapore's first prime minister, introduced the concept of _Capitalism with Asian Values_, which has since become the model for China's economy. This type of economic system might not look like capitalism at first glance, as it features heavy planning from central authorities and totalitarian regimes.

The foundation this model in China was achieved through the extreme _shock therapy_ of Mao Zedong's cultural revolution, during which millions were persecuted, abused and even killed. Instead of providing a legacy that challenges capitalism, it paved the way for it.

As we can see, capitalism does not necessarily lead to democracy. Both Singapore and China are capitalist, but also totalitarian non-democracies.

In the long run, the only way capitalism will survive is through socialism in one way or another. To avoid the communistic revolution that will abolish property altogether, capitalists need to appease people through strong government programs. These can be hidden, as is the case with communitarianism, or overt, like the state planning found among the Asian capitalists.

> _"Socialism failed, Capitalism is broke. What comes next? The answer is: communism."_

### 11. Final summary 

The key message in this book:

**Capitalist ideology permeates the world around us, constantly influencing the way we think and act. And yet, it remains hidden from view most of the time. The only real way to create a free, egalitarian society that can handle today's critical issues is to challenge this ideology, and ultimately replace it with something better.**

****Suggested** **further** **reading: _After the Music Stopped_ by Alan S. Blinder****

_After The Music Stopped_ explains and analyzes the causes of the last decade's great financial crisis. It details the mechanics of the underlying problems as well as the sequence of events as panic began to set in. Finally, it also explains how the US government managed to halt the chaos and rescue the economy. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Slavoj Žižek

Slavoj Žižek is a philosopher and cultural critic from Ljubljana, Slovenia, and is a senior researcher at the University of Ljubljana. He is also Global Distinguished Professor of German at New York University and international director of the Birkbeck Institute for the Humanities in London.

