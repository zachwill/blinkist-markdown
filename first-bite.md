---
id: 57344a4ce2369000032901be
slug: first-bite-en
published_date: 2016-05-16T00:00:00.000+00:00
author: Bee Wilson
title: First Bite
subtitle: How We Learn to Eat
main_color: 55995C
text_color: 39663D
---

# First Bite

_How We Learn to Eat_

**Bee Wilson**

_First Bite_ (2015) reveals the real root of eating problems: our very first childhood experiences with food. Backed by fascinating scientific studies, these blinks explain the perils of marketing food to children and the negative influence of gender norms and well-intentioned families. Finally, they direct us toward positive dietary change.

---
### 1. What’s in it for me? Understand why you eat the way you do and how you can change bad habits. 

Which types of food do you love the most? Do you prefer pizza, sushi or chicken and dumpling soup? If you do have a favorite dish, try now to articulate why you love it. Perhaps your preference has to do with flavor, texture or something more elusive?

More often than not, the foods we love are those with which we grew up, dishes that often appeared on our childhood tables. They somehow connect us to memories of other meals we ate when we were younger.

From your very first bite to your last, what and how you eat influences and changes your palate, teaching your body to crave the flavors that your mind comes to love. Let's dive into the wondrous world of taste and flavor to figure out what makes your tummy rumble!

In these blinks, you'll discover

  * why less than 25 percent of food marketed to children is good for them;

  * what Japan can teach the rest of the world about eating; and

  * how nearly half of American men suffer from a delusional self-image.

### 2. Think you were born to hate certain foods? Think again. Your palate is built through experience. 

Have you given up on the possibility of your child ever liking broccoli or brussels sprouts? It might seem as if children are hardwired to hate certain foods, but this simply isn't the case.

Researchers, whether neurologists or biologists, agree that our palate isn't something we're born with but something we _learn_.

The mainstream consumer, however, is mostly unaware of this fact. Many of us believe that our love for sweets is an evolutionary phenomenon. Humans learned to seek out sweet foods because, unlike bitter foods, they generally weren't poisonous. So as sugary treats are everywhere, we can blame our brains for being unable to resist the sweet temptation.

And yet here's an interesting wrinkle. While humans supposedly crave sweet treats, what one person considers sweet may be thought of as bland and tasteless to someone else.

A 2012 study revealed that some individuals don't get their sweet fix from sugary cereals, but rather prefer a ball of mozzarella or a sun-ripened cob of corn.

There is plenty of sugar in foods that aren't immediately thought of as sweet. Consider that nearly a third of the population in Western countries doesn't reach for sweetened cereals for breakfast.

The most important factor in your palate's development, far more important than your biological makeup, is your _food environment._

Which sorts of foods did you grow up eating? If you didn't consume a lot of sugar as a child, fresh corn on the cob tastes as sweet as can be. But if you gobbled a lot of processed salty snacks and sweet treats, that ear of corn won't satisfy your craving for sweetness.

In short, taste isn't something we're born with but rather something we learn through eating.

### 3. Try not to pressure your children at the table. They’re perfectly capable of feeding themselves well. 

Remember how much you hated it when your parents demanded you finish every last bite, even though you were stuffed?

Many of us do. Memories like this highlight another important element of our food environment. It's not just the food you eat, but _how_ you eat it.

The mealtime routines you maintained as a child are often carried into your adult life. If you were allowed to snack, for instance, you'll probably still crave snacks as an adult. Even the habits of your parents often become your habits, such as eating a sweet dessert after a heavy meal.

Another influence on our palate is parental pressure to eat or like certain foods. Why do parents force children to eat vegetables? Often parents think that children won't eat them otherwise.

But it's long been proven that children make more sensible eating decisions on their own than you might think.

In 1929, Dr. Clara Marie Davis of Cleveland, Ohio, conducted an experiment investigating children's eating behaviors. Babies starting at the ages of 6 to 11 months were allowed a special _self-selection diet_. In other words, the children were allowed to eat whatever they wanted, choosing from a list of 34 foods, from sweet milk to kidneys.

Importantly, the supervising nurse never placed any pressure on the child to choose certain foods. Whichever dish a child reached for or preferred, he or she could eat. The experiment lasted six years, during which nearly every child willingly sampled all 34 foods.

So much for children never wanting to try something new!

What's more, the children in the experiment even demonstrated a capacity to self-medicate, choosing nutritional foods such as raw beef, beets and carrots if they had a cold.

Somehow the children knew that they would get needed nutrients through eating a range of different foods. But importantly, the study showed that pressuring a child to eat wouldn't encourage him to explore new tastes — it would only stress him out.

### 4. Food marketed to kids today is unhealthy, yet attempts at reform have failed. 

The foods that are marketed to children these days might seem harmless, but only until you read the nutrients table on the packaging. The ingredients in many of these food items are nothing short of a nutritionist's nightmare.

How do companies get away with it? In short, parents let them.

A friend of the author's son came to their house for a sleepover one night, and the boy's mother explained that he liked "normal kid food." But by this, she meant items such as potato chips, chicken nuggets, ketchup and pasta — _not_ vegetables.

The issue of "kid food" isn't just a private one but also a public challenge. In 2000, the most popular school lunches provided in British schools were hamburgers, pizza and french fries.

While most parents keep children from eating too much candy, at the same time they seem to have no problem feeding them breakfast cereal or processed dinners that contain a lot of sugar.

It's almost too easy for food manufacturers to make unhealthy children's snacks, as doing so is also inexpensive. A 2013 study found that almost 75 percent of food marketed to children had shockingly low nutritional value.

Of course, there have been attempts to turn this around, but to date, no program has achieved success outright. From British chef Jamie Oliver's school lunch program started in British schools to US First Lady Michelle Obama's "Let's Move" campaign, innovative programs have not achieved their goals.

Why not?

Although such initiatives promoted healthy eating, they failed to realize that children had never learned to see food as a source of nourishment in the first place. Until reforms address this factor as well, children will keep dumping healthy school lunches in the trash.

> _"A child will only benefit from a healthy balanced lunch when he or she has developed a taste for healthy balanced food."_

### 5. Grandparents that lived with famine or food shortages are prone to fattening up their grandchildren. 

Did your grandparents ever encourage you to eat "one more bite"? You might have felt perfectly stuffed, but you certainly didn't look it to them.

Why? Your grandparents, growing up in an earlier generation, may have had a different experience of food — and potentially of hunger — during their childhood than you.

Memories of scarce supplies and rationed portions often inspire those from older generations to worry about their children getting enough food. But while an extra helping of ice cream may be well intentioned, such habits can harm children.

This is the case in China, where childhood obesity has increased nearly fivefold, in particular for children living in cities. How has this happened?

Many urban parents need to work all day. Their child or children are often cared for by grandparents, who believe that chubby children will fare better if food supplies dwindle or famine strikes.

But while famine was a threat for their generation, for their grandchildren it's not as much of an issue. Rather than making them stronger, those extra servings of food are simply unhealthy.

Good intentions can even harm a child's eating habits in infancy. How? By feeding kids to calm them down.

Too many parents think a crying infant is hungry, and will readily feed the baby to keep it quiet. All this does, however, is create a connection in the child's mind between eating and stifling emotions.

Unfortunately, this habit can follow people into adulthood as a tendency for _comfort eating_, which often takes the form of binge eating to cope with anger or sadness.

Even forcing a child to eat everything that's on her plate can lead to problems in adulthood. She may well learn to eat without thinking, not stopping when she's full but only when her plate is empty.

As you can see, many poor eating habits begin in early childhood, at home. As we'll find out in the following blink, some bad patterns impact boys and girls in different ways.

> _"China's obesity is a symptom of the fact that attitudes to eating have not changed fast enough to adjust to a new situation."_

### 6. Gender norms influence how parents feed children, setting the stage for future health problems. 

Boys eat steak; girls eat salad. Unfortunately, this well-known cliche runs counter to what children actually need.

In fact, it's girls who need steak more than boys, and in turn boys could certainly benefit from more vegetables in their diet.

Nearly every woman has to cope with iron deficiency, because blood loss during menstruation means a loss of iron, too. Girls need to compensate for this deficiency, but social mores dictating that red meat is a food for boys often make it socially unacceptable for girls to order a steak! All the while, growing boys are deficient in the vitamins and nutrients found in vegetables that they should be encouraged to eat.

The idea that boys need more food than girls is equally damaging. While boys would be better off eating less, they're told to take as much food as they like — while girls are often chided to watch their weight.

Overweight girls don't need less food; they need more _nourishing_ food. So why don't parents recognize this?

The real problem is that most parents don't know when their child has a weight problem. A survey in Scotland revealed that parents will only consider a child overweight if the child is severely obese. Children that weigh more than they should for their age and height are seen as normal and healthy.

This problem tends to carry over into adulthood as a self-image issue: another survey in the United States found that 43 percent of overweight men didn't think they needed to lose any weight.

But it's not just extra pounds that we fail to recognize. As you'll see in the next blink. we can no longer accurately detect feelings of hunger, either.

> _"Girl food and boy food are dangerous nonsense that prevents us from seeing the real problems of feeding boys and girls."_

### 7. Learn to eat the correct amount by distinguishing appetite (or boredom) from hunger. 

Ever thought you were hungry, but then found you felt full after just a few bites?

Today we tend to respond to any hint of hunger by immediately reaching for food. But the truth is that most people in the Western world hardly know what real hunger feels like.

The purpose of eating is to satisfy hunger. But it's our increasing tendency to mistake _appetite_ for hunger that causes problems. While hunger is an instinctive reaction we experience from birth, appetite is the mere desire to eat particular foods.

While in generations past feeling hungry was normal and something to tolerate until the next scheduled meal, today it's something we feel need to nip in the bud immediately.

As a result, children learn that they can justify eating at any time simply by saying that they're "hungry," which is just boredom in disguise.

Luckily, children can be taught how to understand and manage hunger more accurately.

Pediatrician Susan L. Johnson has explored studies to help children improve their understanding of fullness. Children from four to five years old spent six weeks learning how to regulate the amount of food they ate. A crucial element of the exercise was for children to ascertain when their stomachs were empty.

The results? Children who ate too much learned to eat less while undereaters learned to fill themselves appropriately when they were truly hungry.

Adults can also recalibrate how they perceive hunger. For obese adults in the Netherlands, all it took was a seven-day workshop in which participants used a "body scan" technique to distinguish between physical hunger and emotional dissatisfaction.

Overall, it's important to remember that feeling hungry isn't a bad thing. In fact, it'll make you appreciate your next meal that much more.

> _"We are not the starving children."_

### 8. Dietary change is possible not just on a personal level but a national level. Japan is a key example! 

If you've ever visited Japan, you may have noticed how few obese Japanese people there are. This isn't a coincidence: the Japanese diet has made its population one of the world's healthiest and longest-living.

But there's more to this example than just the virtues of eating healthily. Japan's experience shows us that eating habits can be changed for the positive on a nationwide basis.

The Japanese diet as we know it today is only a few decades old. For most of Japanese history, the national diet was rice, a few vegetables and miso soup.

But things began to change in the nineteenth century. During the period of the Meiji Restoration, Japan opened its borders to the outside world and began to compare its diet to that of neighboring countries.

Noticing that the national diet was lacking in a few areas, Japan began to promote dietary change, such as introducing more protein into a mostly vegetarian diet.

For example, people borrowed techniques from Chinese and Korean dishes, such as stir-fry and barbecue, and then gave them a particular Japanese twist. Such dishes were served in smaller portion sizes and adapted to suit the structure of a traditional Japanese meal.

So if serving an omelet, a Japanese cook would not include the typical Western side of fried potatoes, but instead include Japanese staples of miso soup, rice and vegetables.

Slowly but surely, Japan developed new, improved eating habits that were still deeply Japanese.

Could you help your family, community or country create a change like this? Seems impossible, right?

Well, not entirely. If you want people to embrace change, you can't simply tell them what to do or how to eat. This only makes them defensive and likely to stick more stubbornly to old habits.

Instead, you need to help people realize their desire for change. Next time a friend mentions that she'd like to become fitter or learn new recipes, don't ignore her. Listen instead and encourage her!

Change across a whole nation is possible; why shouldn't it happen in your home, too?

### 9. Final summary 

The key message in this book:

**We aren't born with eating habits but rather learn them in childhood. The foods we eat during these early years and the experiences we have while eating can create negative patterns like comfort eating that can linger through adulthood. But if we find a way to spur interest in improving our eating habits, we can make a change for the better.**

Actionable advice:

**Be the best example!**

Want your kids to eat more healthily? Don't just tell them what to eat, but start snacking on carrots and other vegetables yourself. Add some healthy recipes to your dinner repertoire and show your children how much fun nutritious eating can be. If you believe it, then they'll believe you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Consider The Fork_** **by Bee Wilson**

Eating and cooking have always been crucial to our survival, but over time they have also become a subject of cultural and scientific interest. In _Consider the Fork_ (2012), author Bee Wilson blends history, anthropology and technology to tell the fascinating story of the evolution of cooking, while also taking a closer look at the creation of cooking tools and how they have shaped our culture and eating behavior.
---

### Bee Wilson

Bee Wilson is a historian and author of many books, including _Swindled_ and _Consider the Fork_. Also an acclaimed food journalist, Wilson was named food writer of the year in 2002 by BBC Radio and food journalist of the year in 2004, 2008 and 2009 by the Guild of Food Writers for her _Sunday Telegraph_ column, The Kitchen Thinker.

