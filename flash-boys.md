---
id: 5360d10d6233360007920000
slug: flash-boys-en
published_date: 2014-04-29T10:42:35.000+00:00
author: Michael Lewis
title: Flash Boys
subtitle: A Wall Street Revolt
main_color: None
text_color: None
---

# Flash Boys

_A Wall Street Revolt_

**Michael Lewis**

_Flash Boys_ chronicles the story of one Wall Street broker's detective work into how some traders were scamming money from virtually all US investors. This compelling narrative explains just how the SEC, public stock exchanges and large brokerages were allowing this to happen while investors paid the price.

---
### 1. What’s in it for me? Find out how two milliseconds cost US investors up to $160 million a day. 

Have you ever seen a stock market trading floor in an eighties movie? You know, that one big, bustling room with traders wrapped up in phone cords shouting out orders to buy and sell shares based on what their clients call in and tell them?

Well, those days are long gone. Today, the US stock market trades inside computer servers: little black boxes that match buyers with sellers in a fraction of a second.

These blinks tell the story of how one broker from a small-time bank noticed he was getting scammed on his trades and began to investigate. Through meticulous detective work, he discovered that _all_ of Wall Street was falling prey to a new kind of predator — and the banks, stock exchanges and even the SEC were sitting back and letting it happen.

In the following blinks, you'll find out

  * how a time span of two-thousandths of a second allowed certain traders to scam investors for as much as $160 million a day,

  * why on one day in May 2010, Procter & Gamble shares traded at prices from as low as a penny to $100,000 in the span of minutes and

  * how a new, safer exchange was built, but banks refused to send trades their way.

### 2. Brad Katsuyama sold large batches of shares for big investment firms on public exchanges. 

Our story begins in 2007 with Brad Katsuyama, the head of the Royal Bank of Canada's (RBC) stock brokerage team in New York.

Brad's job was to sell shares on public stock exchanges on behalf of large investment firms. These large firms typically wanted to sell huge quantities of shares at once and this was problematic because there usually wasn't enough demand on the open market to buy that many shares.

Imagine, for example, an investment firm wants to sell three million shares of stock in a company called _Solectron_, but the stock exchanges only have demand for one million shares at, say, $3.70 a share. The investment firm could offload the first million shares at this price, but would then need to spend time and energy trying to sell the remaining two million without the price dropping too much, as it tends to do when large amounts of shares are sold.

Instead of doing this themselves, the company would sell the shares to Brad at RBC, who would handle the resale. The investment firm might sell those three million shares to RBC for $3.65 a share, giving Brad a $0.05 discount compared to the market price for helping them out. Brad would go on to sell the first million with no problem at $3.70 and then try selling the remaining two million at an average price of at least $3.63, as this would allow him to profit on the deal.

By helping match supply to demand, Brad is actually performing a valuable service to the financial system: he's facilitating a liquid market where sellers can sell and buyers can buy with ease.

### 3. Brad found a problem: someone had rigged the market so he couldn’t sell the shares he had planned to. 

Brad was perplexed. He found that in the middle of making trades — e.g., unloading the first million shares of Solectron for $3.70 as discussed in the previous blink — something mysterious was happening: the market moved against him and the price of a Solectron share fell before he could complete the transaction.

He wasn't even able to sell the first million shares at $3.70 and only managed to get rid of a few hundred thousand. It was as if someone on the market had reacted to his desire to sell before he had even fully expressed it.

And this phenomenon happened time and again: a stock price would be relatively stable, and then suddenly collapse the instant Brad pressed the "Enter" button on his computer to execute a trade. This was a big problem for Brad because he couldn't predict the price at which he could resell shares anymore, and this made him reluctant to buy them from big firms in the first place.

What's more, this sudden market unpredictability was making RBC hemorrhage tens of millions of dollars, as more and more trades turned unprofitable.

It soon transpired that RBC was not alone: brokers all over Wall Street were seeing the same thing. Brad's problem was actually Wall Street's problem.

It looked suspiciously like the stock market was being rigged somehow so that it showed demand that actually wasn't there. But it was unclear who was doing it and how.

To get to the bottom of the matter, Brad managed to persuade his superiors at RBC to let him and his team conduct a series of experiments to figure out what was going on. They duly agreed, and told Brad to run his experiments as long as he lost no more than $10,000 per day in the process.

### 4. Over the past two decades, stock markets have multiplied and become automated. 

Before we dive further into Brad's investigation, it's important to understand how US stock markets function today.

It used to be that New York only had two stock exchanges: the New York Stock Exchange (NYSE) and the NASDAQ, and the shares of a public company could only be traded on one or the other, not both. What's more, all trades were made on the hectic trading room floor through human _floor brokers._

This situation has changed dramatically over the past few decades: there are now thirteen public stock exchanges in New York, as well as over forty private stock exchanges known as _dark pools._

These dark pools are so named because they don't reveal their inner workings to outsiders. In a public stock exchange, everyone sees everyone else's buy and sell orders; in a dark pool, buyers and sellers are matched off quietly. The idea is that this prevents major price shifts when investors want to make big-volume trades. These dark pools were mostly run by big brokerage houses similar to RBC.

Another big change is that a company's stock can now be traded on multiple exchanges at once. In fact, by 2008, virtually every public company's stock traded on all thirteen public exchanges: a company might have 200,000 shares on one exchange, 100,000 on another and so on.

Finally, human floor brokers who trade their customers' shares with other floor brokers are a thing of the past. These days, trades are made by computer servers called _matching engines_. Buyers and sellers submit orders to the exchange by typing them into a computer that sends it to an exchange's matching engine, which in turn looks for a counterpart for the trade.

### 5. Someone was using the varying travel times of trade orders to preempt Brad’s moves. 

So what did Brad's experiments reveal?

It turned out that someone was taking advantage of the fact that the various exchanges were in different geographical locations, which produced variance in the amount of time that it took for a trade order to reach different exchanges.

When Brad gave the order to buy shares, it would go out to all the exchanges at once, reaching the first exchange in about two milliseconds (i.e., thousandths of a second) whereas it took four milliseconds to reach the last one.

Though these differences were small, they were long enough for someone to profit through so-called _front-running:_ some trader was picking up the order arriving at the first exchange and racing to the other exchanges before Brad's order made it there to take advantage of the knowledge that Brad was about to buy.

Though Brad's team didn't yet know how this was being done, they did come up with a remedy: a computer program that staggered the orders Brad sent to different exchanges so that they arrived at their destinations at roughly the same time, eliminating the possibility of foresight.

This program worked wonders: Brad's team could once again trade properly, and the program rapidly earned the moniker "Thor" for its power. It turned out that using Thor could save the team about a tenth of a percent of the total value of the trade: a tax that the front-runners had effectively been levying on their trades till then. It may not sound like much, but if front-runners were levying that much on every trade on Wall Street, they'd be netting more than $160 million a day.

Unfortunately, even though Thor was obviously a powerful tool, it also proved inconsistent. That's because the trade orders' travel time to various exchanges was unpredictable due to things like network traffic and static. And when the actual travel time of the signal differed from their estimate, the front-runners could strike again.

### 6. High-frequency traders were scamming investors by moving faster than they did. 

As Brad's team refined Thor, Brad himself continued investigations into who was front-running his trades. He had a hunch: he had begun hearing about so-called _high-frequency traders_ (HFTs), who used computer algorithms to buy stocks and then sell them again in just a fraction of a second.

HFTs had become big players, but were quite mysterious. Brad was shocked to find that one HFT firm, _Getco_, transacted a whopping 10 percent of all trades in the United States — and yet Brad had never even heard of them.

He began to dig deeper, but no-one working for an HFT would talk to him. He suspected it was because everyone was making too much money to spill the beans.

Eventually, he found a disgruntled technology expert who would: namely, Ronan Ryan, who had helped set up equipment for HFTs but was becoming dubious about the whole business.

It turned out that the HFTs were investing tens of millions of dollars a year in _speed_ : they were paying stock exchanges to be able to set up their computer servers right next to the matching engines _inside_ the exchanges, as well as laying down their own, super-fast, fiber-optic cables through the city to communicate with other exchanges.

This privileged position next to the matching engine allowed HFTs to see a buy or sell order from another broker before anyone else, and the HFT could guess that the same broker would likely be doing more of the same on the other exchanges. The HFTs would then race to the other exchanges via their own ultra-fast cables in time to preempt the broker's order and skim a little off the top.

And that was but one of the scams the HFTs were running; an even bigger one was so-called _slow market arbitrage_. Here, an HFT would notice a change in a stock price on one exchange, and then race to other exchanges to take advantage of it before the prices changed there. Again, speed was key.

### 7. Brad fixed Thor, but investment firms still didn’t flock to RBC. 

In addition to providing information on how the HFTs operated, Ronan was also able to help Brad's team fix Thor. Basically, there were just too many unknown factors in the speed of the physical hardware like the cables and switches that conveyed the signal to the various exchanges for a simple software fix to work consistently. In effect, RBC had to lay down its own fiber-optic network. Which is exactly what they did.

Thor was now more than a tool: it was a product RBC could sell to customers as a way of protecting themselves from the new predator in the jungle, i.e., the HFTs.

When RBC presented its sales pitch to major investment professionals at big mutual funds, money management firms and hedge funds, it turned out no one else had discovered the HFTs yet. Sure, some investors had a hunch that something weird was going on, but no one had uncovered as much as Brad had. They were so appreciative of this new information that RBC rapidly became the number one ranked investment bank in terms of customer service.

Naturally, many investment firms began to direct more of their trades to RBC so they could be protected by Thor, and this produced a spike in RBC's sales. However, this spike was far smaller than you might expect, as the firms still kept making the majority of their trades via other brokerages. But why on earth would they willingly get scammed?

Because prestigious and established brokerages actually provide several valuable services to investment firms other than just brokering trades, like research and private access to corporate executives. The way investment firms paid them back was by trading through the brokerage, so the investment firms couldn't divert a majority of their trades for fear of losing those other services.

This means that even though RBC was the only brokerage that could protect its clients, those clients still only diverted a tiny fraction of their trades to RBC. Unfortunately, this tiny fraction would never be enough to put the HFTs out of business.

### 8. The HFTs were hurting the financial system and making it less stable. 

Though the HFTs were raking in a fortune from their various scams, they weren't performing any function from the overall financial system's perspective. They weren't providing liquidity, because the buyers and sellers would have found each other and the trades made anyway. In fact, the HFTs were making brokers like Brad reluctant to trust the market, thereby _reducing_ liquidity.

In effect, the HFTs were scamming investors by charging them a little additional tax on their trades. This would have been bad enough on its own, but HFTs were also making the market more volatile and unpredictable through their lighting-fast, algorithm-driven trades.

Nowhere was this more evident than in the _flash crash_ of May 6, 2010. In this bizarre event, for no obvious reason, the US stock market plummeted 600 points in just a few minutes, only to rebound again after a few more minutes. To highlight the scale of the turbulence, consider that Procter & Gamble shares traded at prices from $0.01 to $100,000 within the scope of those minutes.

The Securities and Exchange Commission (SEC) duly conducted an investigation into the incident and arrived at the explanation that an obscure Kansas City mutual fund had caused the crash by making one large bad trade.

But Brad and others knew better: in fact, the SEC had no way of knowing what caused the crash because they didn't have the necessary data. Trades were executed in microseconds, but the trading records were only kept by the second. It seemed clear that HFTs had exacerbated the crisis.

This event, and the SEC's wholly inadequate explanation of it, made investors wary of what was going on in the market.

### 9. The exchanges, the brokerage firms and even the SEC seemed to play into the hands of the HFTs. 

So how on earth had HFTs wound up being such a powerful force?

As mentioned before, the HFTs were paying public stock exchanges handsomely to get their servers right next to the exchanges' matching machines. At first, the older exchanges wouldn't allow it. As a result, the HFTs simply set up shop next door so they were still were close enough to have first access to the exchanges' information.

But the new exchanges were happy to let the HFTs right into their server rooms as long as the price was right. The HFTs began trading big volumes on the new exchanges, and so the older ones began losing market share, which ultimately convinced them to let the HFTs in, too.

At this point, brokerage firms were already being scammed on all their trades. Once it was clear that they wouldn't be able to respond to or compete with the HFTs, they decided to set up their own private exchanges instead: dark pools where HFTs would not have access.

The trouble with dark pools, though, is that they don't typically draw enough trades to be profitable. And so, many brokerage firms resorted to treating their dark pools preferentially: matching their clients' orders there even if they could have gotten a better deal elsewhere.

Eventually, even this wasn't enough, and the brokerage firms began to sell their dark pool access to HFTs after all. This allowed the HFTs to spot big, juicy orders in the dark pool and once again rush to public exchanges in preparation for them.

Perhaps most astonishingly, even the SEC itself seemed to be covering for the HFTs: once Brad had figured out the HFTs' scheme, he actually went to the SEC to inform them of his discoveries, but found the regulators actually took the HFTs' side.

One possible explanation lies in the fact that many former SEC employees had gone on to work for the HFTs in high-paying jobs. Might hindering the HFTs activities negatively affect their own "retirement plans"?

### 10. Brad launched his own exchange, the IEX, designed to be HFT-proof. 

Eventually, Brad decided that the only way to fix Wall Street was to start his own exchange, one that could not be gamed by HFTs. The major investors he talked to loved the idea, but told him straight up that, to be credible, the exchange could not be founded by any Wall Street bank, not even RBC.

So Brad quit his job and assembled a team to create the _Investors' Exchange (IEX)_, which had to do the following two things in order to make it immune to HFTs:

First, whenever IEX sent information to or received information from other exchanges, it needed to be received by the targeted exchange before HFTs could react. IEX managed to do this by simply putting distance between the IEX matching engine and the point of access that traders, including HFTs, used. This created an artificial delay in the time it took for traders to get information, which meant that the targeted exchange would always receive the signal before any trader could make a move.

Second, IEX had to ensure that all the information it sent to other exchanges would arrive there at the same time, so it needed to lay down its own communication lines, as was done with Thor at RBC. Ronan, the technology expert who had helped Brad unmask the doings of HFTs, helped IEX here by handpicking the routes for the fibers so that signals would arrive at exactly the same time everywhere.

Finally, to avoid any conflicts of interest, it was decided anyone who owned any part of IEX would not be allowed to trade directly on it, but rather would have to go through brokers just like normal investors. This would help align IEX owners' incentives with those of regular stock market investors.

By autumn 2013, IEX was ready to be launched. The team was not expecting an immediate success, and the trading volumes were, indeed, very small at first. By the end of the year, IEX was only trading 50 million shares a week and, to cover costs, it would need to trade that same volume in _a day_.

### 11. Big brokerages avoided IEX until Goldman Sachs came on board. 

It was clear that the big brokerage firms were steering clear of IEX because they wanted to direct trades into their own dark pools where they could make more money. What's more, they were even defying the instructions of customers who were explicitly asking them to route orders into the IEX because of its trustworthiness.

In practice, when clients told brokerage houses to buy or sell shares, the brokerages would pretend they couldn't find appropriate offers on the public exchanges, and instead "found" them in their own dark pools. Of course, investors ended up paying for this as the dark pools could rarely offer as good a price as all the public exchanges combined.

What's more, since the IEX had no control over how quickly information was relayed to dark pools, it left a window for HFTs to profit via so-called _dark pool arbitrage_. Basically, HFTs could buy shares on IEX and then sell them to unsuspecting investors in a dark pool at a higher price. This phenomenon proved to Brad that banks were selling dark pool access to HFTs.

When Brad presented this information to investors, it made them even more adamant that their brokerages direct trades to IEX.

However, IEX's big break didn't occur until they landed a really big fish: Goldman Sachs, where two partners became interested in IEX for two reasons.

First, they worried about the instability of the market due to HFTs, as seen in the flash crash. They felt that if another such calamity were to cause the entire market to collapse, Wall Street, and specifically Goldman Sachs, would be blamed.

Second, they also felt they had a moral obligation to right a wrong on Wall Street.

And so these two partners began directing more of Goldman Sachs' trades to IEX. This not only made IEX's trading volumes skyrocket, it also lent a lot of credibility to the fledgling exchange. Other brokerages couldn't marginalize IEX any longer when the great Goldman Sachs was vouching for its fairness and stability.

### 12. Final Summary 

The key message in this book:

**Over the past decade, Wall Street has been collectively scammed by high-frequency traders who used tiny communication delays to make trades before investors could fully make theirs. In many ways, they were aided by the public exchanges and the brokerages, while the SEC also seemed to turn a blind eye to them. In response, IEX — a new, safe exchange — was created.**

Actionable advice:

**Only trade on exchanges like IEX that are protected from HFTs**

If your work entails trading shares or anything else that might fall prey to HFTs, you should start investigating what the exchanges you use are doing to protect you from HFTs. Start demanding that your brokers route trades to exchanges like IEX that are protected from HFTs — and make sure you follow up on the instructions to see if they were adhered to.
---

### Michael Lewis

Michael Lewis is a financial journalist and author who has published fifteen nonfiction books, eight of which became national bestsellers in the United States. In 2011, Lewis' book _Moneyball_ was turned into a major film starring Brad Pitt and Philip Seymour Hoffman.

