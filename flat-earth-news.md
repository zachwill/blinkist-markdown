---
id: 57dd7d0bf9916500030ea769
slug: flat-earth-news-en
published_date: 2016-09-22T00:00:00.000+00:00
author: Nick Davies
title: Flat Earth News
subtitle: An Award-Winning Reporter Exposes Falsehood, Distortion, and Propaganda in the Global Media
main_color: E02D44
text_color: AD2335
---

# Flat Earth News

_An Award-Winning Reporter Exposes Falsehood, Distortion, and Propaganda in the Global Media_

**Nick Davies**

If you've ever entertained romantic fantasies about becoming a globe-trotting journalist, let _Flat Earth News_ (2008) serve as a wake-up call. Truth is, modern journalists are under extreme pressure from the media outlets they serve, which are mostly controlled by profit-minded corporations. These blinks reveal why news desks simply regurgitate stories and why it's so easy these days for spin doctors to manipulate the news.

---
### 1. What’s in it for me? Get a backstage tour of the media. 

Do you sometimes feel like you're missing out on the most relevant news? True, it's very entertaining to read about that cute cat that likes to attack postmen — but is it really to hear such trivial stories that we switch on the news? Isn't there something else you should be learning about, like the chemical plant that's being planned in your neighborhood right now, or the pension fund reform bill that will affect your entire generation?

Apparently, when it comes to selecting the news, relevance is no longer so relevant. And there's a reason for this. The media is now owned by massive corporations that only care about popular, and ergo profitable, stories — not _the truth_. So how do journalists work now that journalism has gone corporate? In these blinks, you'll find out.

You'll also learn

  * why you know much about Hurricane Katrina and little about Hurricane Stan;

  * what's wrong with "balanced" articles; and

  * that your weird neighbor isn't all that paranoid after all.

### 2. Today’s journalists are forced to churn out stories without time to check facts or verify sources. 

When you think of a journalist, you might get a very traditional image: someone rushing to the site of an event, jotting down what they're seeing, interviewing people, getting the scoop. But, these days, journalists rarely work like this.

Fewer and fewer journalists are out in the field investigating their own stories. Instead, they now tend to rehash stories that were originally issued by large wire agencies or in press releases.

When the University of Cardiff examined 2,207 stories from the most respected British media outlets, they found that 60 percent of these stories simply echoed previous wire-agency reports or press releases, adding hardly any new information. A mere 12 percent of stories, they found, were based on the personal research of reporters.

Journalists in national media simply don't have the time to provide in-depth investigations.

This is the upshot of national media corporations' cutting costs and reducing the number of employees, leaving the remaining journalists with the burden of shouldering the work of former colleagues. Journalists now typically write around ten stories per day, which adds up to less than one hour per story on an average workday!

No wonder the average reporter has little time to leave the office and investigate a story or speak face-to-face with an actual witness.

What's more, these cutbacks have drastically reduced the number of regional reporters.

Journalists in national newsrooms once relied on regional reporters across the country to help investigate stories; these days, however, many local newspapers have been bought out by large, profit-minded corporations that lay off most of the regional journalists to save money.

All these cutbacks have left the newsrooms with very few reporters who can actually investigate stories in the field.

A typical journalist today must search the internet for reports from wire agencies, looking for material out of which to produce as many stories as possible.

> _"We are deceived and obstructed by the very machines we make to enlarge our vision."_ \- Daniel Boorstin

### 3. While the media rely heavily on wire agencies, these sources are not always objective or reliable. 

Imagine you're a journalist who's just stumbled upon a great story — but, after you've pitched it, your boss rejects it. What can you do? It might sound ridiculous, but a common practice these days is to hand it over to a wire agency, wait for them to run it and then get assigned the story.

This strategy has proved successful because media outlets place great confidence in the accuracy of wire agencies.

Take the BBC's internal guidelines, for example: they state that journalists must have at least two sources for every story unless the primary source is the Press Association (PA), in which case they can run the story without question.

We see this internationally as well. The two foremost agencies are the Associated Press (AP) and Reuters, which are used by newsrooms to decide what's considered newsworthy around the world. Few outlets will scrutinize what they report.

But this can lead to problems, since wire agencies like these are actually ill-equipped to thoroughly verify and investigate stories.

In fact, wire agencies are undergoing the same penny-pinching corporate pressure as other media outlets, leaving the reporters with very little time to investigate stories.

So, when we look at AP and Reuters, we see that their coverage of international events is also heavily reliant on a few journalists working out of local offices all around the world. And since these offices are minimally staffed, the journalists primarily rely on press releases from organizations and the government.

They can also fall back on local media, but these sources are often also recycling press releases, a cycle that results in news agencies copying each other's content.

And it goes without saying that a press release isn't the world's most objective source of information.

A typical wire-agency journalist starts the day at 6:00 or 7:00 a.m. by rehashing stories from press releases and other papers, when it's too early to reach out to sources for confirmation of information.

### 4. Today’s media outlets pursue popularity at the expense of reporting truthful or relevant stories. 

There's no getting around it: most people love videos featuring cats or human-interest stories. This fact hasn't escaped the attention of traditional media, and even the most respectable newspaper websites are now filled with cutesy and clickable stories.

There's a simple reason for this: When media outlets decide what stories to cover, the predominant goal is to attract as many readers as possible. Therefore, the goal to inform the public about crucial issues takes a back seat.

This drive to grab the attention of the general public is what leads news outlets to sensationalize particular stories.

The media may extensively cover one tragic train collision that killed a hundred people but ignore a hundred separate car crashes that were just as fatal. 

Tabloid newspapers and TV outlets also often lead with reports of celebrities and sex scandals, because they know these topics will sell.

And to clinch the deal, they'll entice their audience by taking advantage of the reader's emotions rather than actually reporting or revealing crucial information. This is what happened when Princess Diana died. For weeks, the media on TV simply reported on the public mourning with photos of tear-stained faces accompanied by dramatic music.

While this may increase sales and ratings and generate clicks, it gets in the way of relevant and important news, whether it's about workers being exploited or national health-care reform.

Giving popularity precedence over truth and relevance has also led many journalists to cover stories in a way that simply pleases their audience. Truth is left by the wayside.

Therefore, if the opinion of the audience changes, the media will quickly adapt, molding its perspective to fit that of the populace.

For example, when English military forces got involved in Iraq, some newspapers quickly became pro-intervention in order to align with public opinion.

> _"If we can sell it, we'll tell it."_

### 5. News agencies give preference to low-cost stories and avoid risks by being impartial. 

Western journalists aren't at risk of being imprisoned for speaking their minds, but they aren't as free to express their opinions as you might think. After all, most of them depend on media corporations for their salary, and these corporations aren't going to pay them to write things that might harm profits.

That's why news outlets go out of their way to pick stories that have no risk of producing a negative reaction, whether it be a lawsuit or an angry protest, either of which could be harmful.

So, to avoid these risks and any accusations of being biased, publishers make sure they always present both sides of a story.

This kind of logic goes back to when newspapers were reporting on the possible dangers of smoking. Before there was undeniable proof linking cigarettes and lung cancer, papers balanced out warnings about smoking with quotes by the tobacco industry that denied any danger.

Newspapers also prefer stories that are backed up by official sources like the police or the army. By citing an official statement that suggests someone committed a crime, they avoid a possible lawsuit.

On top of all this, the media continues to avoid responsibility by giving preference to stories that are easy to cover.

In other words, how much coverage something gets doesn't depend on its relevance, but rather on how much work it takes to cover.

We saw this difference in action during 2005, when there were two hurricanes with comparable impacts: In August, Katrina devastated New Orleans, and in October, Stanley wreaked havoc in Guatemala.

But there's a good chance that you aren't that familiar with Hurricane Stan since the Western media gave far more coverage to Katrina. In the following months, UK papers referred to Katrina 3,105 times; Stanley only received 34 mentions.

This is because Katrina was easy to cover. Many Western correspondents were already in place to provide reports and photos, whereas, with Stanley, these resources weren't there for journalists to rely on.

> _"Balance means never having to say you're sorry — because you haven't said anything."_

### 6. Public-relations specialists generate their own content and the media eat it up. 

Have you ever read an interview about a new movie where the "reporter" asks inane questions, like "Could you please tell me again, why is your movie so amazing?" If that sounds familiar, then chances are you've read a fake interview.

Such interviews are actually staged by companies that handle press releases in order to influence the media. These interviews are conducted by PR advisers, not reporters trying to get to the bottom of things.

And this is only the tip of the iceberg; many news events are actually staged for the media. You might remember the time in 2003 when President George W. Bush announced the end of combat operations in Iraq. His advisers dressed him in combat gear and staged the press conference aboard an aircraft carrier.

These agencies even go so far as to manufacture evidence like surveys or polls and hire fake "experts" to support their claims and research. They usually release this kind of baseless news on Sundays so that newspapers have something to print on otherwise slow Monday mornings.

It's also common practice for PR agencies to cast independent experts who are actually associated with the company they're speaking about.

Just consider nutritionist Susan Jebb. Referred to as an independent researcher, Jebb warned the public about the health risks of a low-carbohydrate diet, despite having received funds from the Flour Advisory Bureau, a public-relations division of the United Kingdom's flour-milling industry. 

This is exactly the kind of material that media outlets are quick to publish. In fact, according to the University of Cardiff, PR material found its way into 54 percent of UK news articles.

What's worse, newspapers will simply reproduce this material without checking the facts.

In 2006, the _Daily Mail_ reported on a new remedy for hay fever. In the end, however, the article was merely rehashing a press release from the drug company Cytos.

And, as we'll see in the next blink, the private sector isn't the only one putting spin on the news.

> _"Nowadays you don't have to own a newspaper to manipulate it."_

### 7. Intelligence agencies influence the media to promote their own interests. 

You may have had one or two conspiracy theorists try to convince you that intelligence agencies are controlling the worldwide media. The scary thing is, that's not far from the truth.

This kind of government propaganda peaked during World War II and the Cold War that followed.

The CIA was very active during the Cold War: they planted around 400 agents in just about every nation in the world to work at media outlets disguised as editors or reporters. They also maintained contacts with willing journalists who were briefed about what to write.

Meanwhile, the CIA regularly fabricated stories to shape public opinion.

We saw this happen during the civil war in Angola during the 1970s. The United States and Cuba found themselves on different sides of this battle; Cuban troops supported the People's Movement for the Liberation of Angola (MPLA), which sought independence from Portugal, while the United States covertly supported two rival groups.

So, to tilt worldwide public opinion against Cuba, the CIA made up stories about Cuban soldiers raping Angolan girls.

And this kind of practice didn't exactly stop following the end of the Cold War. Indeed, the CIA still has its hands in many media companies around the world.

Recently, the CIA has been using phony corporations to buy media. According to a former CIA agent who spoke to the _New York Times_, the agency controlled at least one major media outlet in every capital of the world.

This goes for allied nations as well: The CIA owned or invested in _Paris Match_ in France, _Der Monat_ in Germany and _Encounter_ in the United Kingdom, to name just a few.

Even major outlets such as _Time_ magazine aren't immune to the CIA's influence, since people affiliated with the agency were actively involved in the magazine's day-to-day decisions.

So, once again, we see how falsified information and propaganda can end up being reproduced by overworked journalists pressured to produce multiple stories a day. As the saying goes, you can't believe everything that you read.

### 8. Final summary 

The key message in this book:

**Media corporations are businesses looking to sell as many stories as they can and to produce them as cheaply as possible. Naturally, this is bound to affect the quality of the reporting. What's worse, this is great news for people out to manipulate the news, whether they belong to the CIA or a PR agency. Nowadays, all they need do is provide a juicy and accessible story and the media will be hooked.**

Actionable advice:

**Check up on your favorite newspaper.**

To find out whether a story has been investigated by a newspaper or a news outlet, just check the version of the same story in Reuters, Associated Press or other wire agencies and identify the similarities and differences between the accounts. Many media outlets simply pick the stories from wire agencies, modify them slightly and release them for consumption. If the stories are very similar, you'll know that it hasn't been investigated independently by the last news outlet.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Hack Attack_** **by Nick Davies**

_Hack Attack_ details the riveting story of the phone hacking scandal that rocked the British media in 2011. Focusing on the rise and fall of Rupert Murdoch's _News of the World_, the books offers an inside look into the seedy world of tabloid journalism.
---

### Nick Davies

Nick Davies, an investigative journalist and the author of four books, has worked for some of the most prestigious English newspapers, _The Guardian_ among them. He also makes TV documentaries and has been named Journalist of the Year, Reporter of the Year and Feature Writer of the Year in British press awards.

