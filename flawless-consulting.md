---
id: 5550afce6332630007280000
slug: flawless-consulting-en
published_date: 2015-05-12T00:00:00.000+00:00
author: Peter Block
title: Flawless Consulting
subtitle: A Guide to Getting Your Expertise Used
main_color: FFD133
text_color: 665314
---

# Flawless Consulting

_A Guide to Getting Your Expertise Used_

**Peter Block**

_Flawless Consulting_ (second edition, 2000) gives you an inside look into the art of consulting with a step-by-step guide to all the phases of a professional consultation. Whether you're a consultant just starting your career or a business leader working with consultants, this book will show you how to successfully manage consultant-client relationships.

---
### 1. What’s in it for me? Discover the skills and strategies of top-quality consultants. 

Every day, somewhere in the United States, a business in trouble makes a call. The people who answer this distress signal are the superheroes of business strategy, the consultants.

Yet as important as consulting is to the business world, few know exactly what sort of role a consultant really plays in a struggling organization.

For many managers, a consultant is hailed as an expert troubleshooter, providing helpful answers to a company's myriad problems. Yet no matter how much a manager wants a consultant to provide that quick fix, a consultant can only advise and teach — not take over.

These blinks show you the strengths and limitations of consulting work and exactly how top consultants go about doing good work every day.

In these blinks, you'll discover

  * why every consultant must practice what they preach;

  * why consultants seem to spend most of their time chatting; and

  * why the meeting is a consultant's greatest weapon.

### 2. Being rational is good. Focusing on interpersonal dynamics and emotions is even better. 

Many believe that being rational is the key to making effective decisions. So managers often try to suppress their more impulsive, emotional sides and focus on being logical.

But really, this isn't always the best approach. If you're a consultant, being cold and rational will only take you so far.

Consultation happens on two levels: the technical, business level and the interpersonal level. And even amid the most technical, complicated systems, we can't forget that every employee is a human, too!

The bottom line is that a consultant can't truly grasp the problem if she doesn't understand the emotional and interpersonal dynamics of the situation at hand.

Consider a hospital where many different types of doctors and specialists see a single patient. An internist may draw blood; a nurse administers medicine; and so on. Although database software could streamline and help organize this multilevel process, this would only happen if everyone involved in the process used the software properly.

Thus as a consultant, you have to realize that introducing a new efficiency tool isn't just a question of adequate terminals or software proficiency. Each doctor or specialist will need to adjust how they work on a personal level, too. If you ignore this variable, the introduction of new technology is all but useless!

And while interpersonal dynamics are important, as a consultant, don't ignore your own feelings. Pay attention to your first response to a situation; it might shed light on a deeper issue, as well as effectively highlighting a potential solution.

So if you get the sense that management is too hard on employees, it's a sure bet that the employees feel this, too. Armed with this bit of intuition, a consultant can then discover ways to improve communication between staff and management, ultimately building a better team.

To sum up, don't ignore your feelings and instincts. Instead, use them as data to better understand the situation in front of you!

### 3. Top consultants not only talk the talk, but walk the walk. Be authentic and follow your own advice. 

Let's imagine a consultant — a confident individual in a sharp suit, strutting around your office taking notes. Although this person might look important, in reality, he has very little direct power.

A consultant can do two things: advise people and try to motivate them to implement new ideas.

So if you are a consultant, how can you be as effective as possible?

First, people won't listen to you at all if you come off like a hypocrite. Openness and honesty are the key characteristics a consultant needs to have. In short, you have to practice what you preach.

Let's say you're a line manager in a traditional company with rigid hierarchies. A consultant tells you that a flat hierarchy would be more effective, as employees are more productive when they have input. You argue that the pressure of the job requires that everyone do as they're told; having every decision open for discussion would just gum up the works and be counter-productive.

Now what would you think if your flat-hierarchy consultant responded with, "Do as you're told. I'm getting a lot of money from your CEO and he wants things done my way!"

Obviously, you'd be horrified. And what's more, you probably wouldn't want to hear another word from this bossy, unenlightened individual.

This situation gets at a crucial point. Behaving badly isn't merely socially unacceptable, it's also unproductive. Since people often learn by imitation, if you want someone to follow your advice, _you_ have to walk the walk, too. This way, people will not only learn from what you say but also from what you do.

What if that bossy consultant had instead responded by saying that he understood your concerns, and had already integrated them in his concept? You might not only be receptive to his ideas but also pick up some vital skills about how to better communicate with employees.

> _"Happiness is when what you think, what you say and what you do are in harmony." — Mahatma Gandhi_

### 4. A consultant is an independant advisor, not a surrogate manager. 

So your company has a huge problem that you as a manager can't solve. The light bulb goes on: _I can call a consultant and let him figure it out!_ Maybe you could even go on vacation while the consultant goes to work.

A dream scenario, sure, but the reality is different. Consultants and managers are both needed; a consultant comes up with a good solution, but a manager is the one who actually puts the solution into action.

This is an important distinction. Although a consultant might act as a surrogate manager and solve a problem quickly, no one would learn anything from the process. If the problem returned, the company would need to again hire the consultant, having not learned the first time around!

Thus a manager is a very important part of the consulting process. However, a consultant must be an independent agent with the latitude to objectively analyze the situation.

But in some cases, the client is positive that he already understands the problem and he just wants the consultant to _solve it_. This is problematic, as most managers don't have the analytical skills to correctly diagnose a problem. Consultants, on the other hand, are trained to do this.

Here's one example. Company A hired a consultant to solve the problem of trainees quitting the moment their training period ended. The company thought that high housing prices in the area were forcing potential employees to reconsider, and was considering boosting salaries or even building an affordable apartment complex just for employees.

But the skilled consultant saw the situation differently. She realized that trainees were quitting because they disliked the stilted, hierarchic company culture. The trainees didn't feel appreciated by management, and didn't develop any kind of emotional attachment to the company.

Once the consultant had arrived at this deeper understanding of the problem, she could advise Company A on better (and certainly cheaper) solutions.

### 5. Each new job needs to be tackled in a series of stages before a successful solution is discovered. 

Each consulting opportunity is built on a series of stages, from setting expectations at the beginning of a job to collecting feedback once a successful solution has been implemented.

The first stage is the _contracting phase._ Here is where a consultant and her clients establish the scope and general expectations of a project. This is an important step, as managers often are concerned that a consultant will exert too much control over the company.

During this first stage, both parties should clearly discuss mutual expectations. This will help the consultant understand the project's boundaries and will also soothe any client fears about how exactly the consultant will work and what sort of role she will have in the company.

The next stage is _discovery and data collection_. Here you conduct a thorough, time-intensive analysis of the business (even if the problem seems superficial at first glance).

What happens exactly during this phase will differ from job to job. Sometimes you'll investigate balance sheets, while other times you'll look at management-employee communication. How the company uses its software might be an issue; or, of course, you might look into all of these factors at once.

The final stage happens when you've gathered your data and come up with a solution. During this stage, you'll need to schedule feedback sessions to alleviate doubt and plan next steps.

This stage is also a great opportunity to get employees on board, convincing them that your proposed solution will be beneficial for all.

When computers were first introduced, for example, many companies treated the new technology simply as an expensive typewriter. At the time, a consultant's role might have been to educate a company about computers, explaining the technology's features and describing its revolutionary potential.

So as you see, consulting isn't just about finding a technical solution for a problem. It's also about winning people's confidence and getting them to commit to implementing your ideas.

### 6. Resistance is a natural response to the consulting process. Be open and deal with it directly. 

You know the saying: you can't make an omelette without breaking a few eggs. Well, a consultant can't change things at a company without upsetting a few people.

People can be scared of change, but consulting is _all about_ change! Resistance is only natural.

Consider this scenario: you've been at the same job for 15 years. Then some young guy comes in, looks over your shoulder for about a week, and tells you that you've been doing your job incorrectly for the past 15 years.

How would you feel?

Resistance is a natural emotional response. And it comes in many guises. For instance, it can manifest itself as constant questioning. When a client starts to probe every little detail, you can be sure that they are resisting your ideas.

So as a rule of thumb, if you are asked the same question twice, answer in good faith. If you are asked the same question a third time, however, take it as a clue that your client is resisting.

Non-verbal cues can also be a sign of resistance. Pay attention to whether people are making eye contact with you. If they're not, they might be feeling a bit of resistance to your work.

Yet how can you deal with resistance if it's unsubstantiated or counterproductive? Start by discussing it openly. Use neutral language and create an opening for the client to respond directly.

Consider using phrases like, "I'm sensing that you doubt my judgment. Do you have the feeling that my research wasn't thorough enough?"

Asking a question forces the client to articulate their resistance, which leads to a discussion. Once the client's feelings are out there, you have the opportunity to neutralize their resistance by explaining exactly how you arrived at your conclusions.

### 7. To effectively communicate your ideas, you have to create space for people to share their fears. 

Having the best ideas in the world doesn't make a difference if you can't communicate them to other people. And that's especially true in the world of consulting.

After all, even if a consultant develops a correct solution using sound logic, if she can't convince people to implement her solution, the consultation is basically a failure.

That's because logic just can't overcome emotions. We all know this intimately, as we all at one time or another have acted irrationally — like getting scared while watching a horror film.

As a consultant, you'll sometimes have to work your way through people's emotions. Be sure you don't ignore people's feelings or rationalize them. Instead, talk about them _all the time_.

Whenever you're in a one-on-one meeting, in the cafeteria or carpooling to work, create space for people to be authentic and share their fears. This will build trust and allow your clients to realize that you have their best interests at heart.

And in addition to private chats, talking about these matters in larger public meetings will also help support your work. Not the sort of meeting where one person drones on, but in a meeting where everyone is invited to take part. Have employees ask questions and air their views — they'll feel heard and you'll gain important data toward your eventual solution. 

Let's say you want to implement new software at a client's company, yet the staff has doubts about the software's level of complexity. How will you ever know this unless you hold a meeting and ask employees how they feel? And once people share their fears, you can reassure everyone by explaining the benefits of the software and describing a planned training program.

Open communication is the key to effective consulting. So go ahead, start a conversation!

### 8. A great consultant teaches a company how to do things on its own; or, how to not need a consultant! 

Good consulting is like raising a child. You deal with a lot of problems in the beginning, but if you do things correctly, eventually you'll have raised an adult that can resolve issues independently.

But unlike a parent, a consultant only spends a bit of time with each company. And she needs to have as much of an impact as possible in that very short time frame.

That's why convincing everyone on the team — and especially staff — to support your vision is such a crucial part of the job. If you can't do this, your legacy will be null.

Consider this scenario. A certain company decided it needed a consultant, and a top manager called a friend who worked for a well-known consulting company. The duo brainstormed and came up with a lot of great, albeit complicated, ideas.

But in their enthusiasm, they forgot to involve the company staff. And once the consultant left, employees had no idea how to follow all the new processes. So in the end, nothing changed.

You have to convince employees to believe in your ideas, as it's the only way your ideas can become reality!

Ultimately, being a great consultant is about making yourself superfluous. Because once a new system runs smoothly and autonomously and is supported by managers and employees, there's simply no need for a consultant.

And the more a client learns from a consultant, the more problems the company can solve alone in the future. So in other words, consultants work to put themselves out of future jobs!

Of course, teaching people how _not_ to need you isn't easy. But remember: new problems and new people are always just around the corner!

### 9. Final summary 

The key message in this book:

**Consulting is about objectively analyzing a business and implementing lasting change. In the best cases, the consultant teaches a client to solve future problems independently through a deeper understanding of the company's work and managerial roles.**

**Suggested further reading:** ** _Million Dollar Consulting_** **by Alan Weiss**

This fully revised fourth edition of the 1992 classic, _Million_ _Dollar_ _Consulting_, walks you through everything you'll need to compete — and win — in the highly lucrative and busy world of consulting. _Million_ _Dollar_ _Consulting_ offers you the tools you need to attract clients, organize your pipeline, and grow your current consulting business into a million-dollar one.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Peter Block

Peter Block is a well-known American consultant and has authored many books on consulting, empowerment and social issues. He won the 2008 Lifetime Achievement Award from the Organization Development Network.

