---
id: 5e6b7a986cee070007dabd3c
slug: flip-the-script-en
published_date: 2020-03-16T00:00:00.000+00:00
author: Oren Klaff
title: Flip The Script
subtitle: Getting People to Think Your Idea Is Their Idea
main_color: FED533
text_color: 6A570C
---

# Flip The Script

_Getting People to Think Your Idea Is Their Idea_

**Oren Klaff**

_Flip The Script_ (2019) helps you up your sales and negotiation game. Sales is all about the art of persuasion but we all know that nobody likes to feel manipulated. In order to sell your idea or product in today's world, your buyers need to feel like they're making that decision on their own terms. Flip The Script provides techniques that allow you to subtly convince anyone that your idea is the right one.

---
### 1. People will only listen to you if they think you’re an equal. 

Selling today is hard work.

We used to rely on salespeople for guidance and information but now we have the internet. These days when we go to make a purchase, especially a major one, we put in hours of research. Now that everyone's an expert, we've already made up our minds before we even reach the store.

So how do you, as a salesperson in the twenty-first century, convince someone to buy what you're selling? Well, it starts with establishing a rapport. And to do this, you need to raise yourself to the same status as your prospect.

**The key message here is: people will only listen to you if they think you're an equal.**

In general, social rank remains one of the most difficult things to change. For most people, it's fixed at birth. If you were born into a middle-class family, unless you get very lucky, you'll probably remain middle-class for the rest of your life.

But for the salesperson, things are slightly different. The salesperson _can_ change their perceived social status to match that of the buyer.

How?

Well, we pick up on someone's social status through external clues, the words they use and the image they present.

Take this rather extreme example. A couple is involved in a car accident. The husband is badly injured but the wife escapes unharmed. At the hospital, the doctor approaches to update her on her husband's condition. But before he can say a word, the wife asks about very specific injuries using medical jargon. It turns out that she is also a doctor. By using a status tip-off, incorporating language unique to the medical profession, the ER doctor can identify her status. He immediately knows that she is his equal.

Salespeople can use this technique. By using the right language and external cues, you can convince clients that you're their equal, and worth listening to.

Let's say you're in real estate and you're meeting with a client who is considering moving her office to another state. Start by showing you understand her industry by using her jargon. Then, move the conversation to yourself by mentioning a similar client that you recently helped facilitate a move for. Finally, show that you've done your research by talking about the pros and cons of moving to the state she's considering. This all takes work but, when done correctly, the end result is status alignment. Your client now believes you understand the complexities of both your own industry and hers.

Achieving status alignment gets your foot in the door. That's vital but it's only the first step.

### 2. Establish your credibility quickly and unquestionably. 

Imagine this scenario. You're a salesperson, talking to a potential buyer. You think you're close to convincing him. Very close. You ask him the clincher. "So, would you like to pay with cash or credit card?" 

"I'm not sure," he replies, "I'll have to think about it and talk it over with my family." 

Your heart sinks. Prospects who say this very rarely return. Your chances of a sale have collapsed. Why?

The reluctant customer has probably experienced what's known as a _certainty gap_. In other words, he's not convinced you'll be able to deliver what you're promising. 

How can you remedy this?

**The key message here is: Establish your credibility quickly and unquestionably.**

When a salesperson encounters a certainty gap, her instinct is to fill it with information. She'll rattle off a list of satisfied clients, boast about innovative features, and break down the price to show what tremendous value she's offering. But none of that addresses the client's core issue: can she specifically deliver what she's promising? 

The only way you can answer this question is to show you're an expert. And the fastest, most effective way to prove yourself an expert is by creating a _flash roll_. This is a rapid-fire explosion of information designed to showcase your absolute command of a complicated subject. You should be able to deliver it in about 60 to 90 seconds and it should win over even the most skeptical audience.

Here's an example of the flash roll at work. Let's say your computer crashes. You bring your laptop to the shop and explain the symptoms, telling the tech that you're sure your virus-protection software is up-to-date. The tech takes a quick look and says, "Sounds like the new Homestead bug. Unless you're updating your virus protection every couple of hours, it probably wouldn't catch it." Then he goes on to list exactly what the virus does in painstaking detail and closes with an equally technical solution. You might not understand all of it but you feel confident that he does and that your problem will be solved.

Your flash roll should have a beginning, middle and end. Start by assessing the problem casually, like it's something you deal with every day. Then describe it in detail, using every technical term available to you. Finish with your solution, the deal you're trying to make. Don't use words like "I think" or "in my opinion." Deliver it quickly and without emotion. Since you want it to be second nature, you should script it, practice it and commit it to memory.

In the next blink, we'll explore how to get your message to stick in the client's mind.

### 3. Addressing pre-wired ideas allow us to anticipate and satisfy questions before they’re asked. 

Whenever you receive new information, your brain goes through three distinct stages. You perceive it, you process it, and then you store it.

However, this only works for information you already have idea receptors for. Totally new information, which you can't relate to, won't get stored or processed correctly. How can you ensure that the information you're giving to prospective buyers makes it into their brains?

You need to focus on the pre-wired ideas that are common to everyone.

**The key message here is: Addressing pre-wired ideas allow us to anticipate and satisfy questions before they're asked.**

_Threats_ are the first of the pre-wired ideas. We snap to attention when we hear something that threatens us or our status. The second pre-wired idea is _reward_. We're always searching for a big payoff, especially when it takes little to no effort. Finally, we all respond to _fairness_. If we're asking someone to take a risk, they need to know that we're just as invested as they are.

In sales, these three pre-wired ideas are expressed as three simple questions from buyers: Why should I care? What's in it for me? and Why you?

Here's how you can answer these three key questions.

It's relatively easy to answer "Why should I care?" Show your client how much the world is changing, and tell her she's in danger of being left behind. Of course, the only way she'll be able to keep up with the competition is by purchasing your product.

When it comes to "What's in it for me?" the magic number is "times two." We don't buy a new TV every year. That's because the new TVs are generally only a small upgrade on current models. But when a TV comes out that offers twice the resolution? That's when we get interested. Your client will respond if you can double her productivity or, on the other hand, cut her expenses in half.

Finally, "Why you?" Your client wants to know that you have some skin in the game, that you're personally invested and aren't going to disappear the second the deal is closed. So, craft a short script that demonstrates how committed you are, whether it's financially, physically or contractually. Your level of investment should be at least as large, if not more, than what you're asking of your potential client.

Once you've satisfied these three pre-wired ideas, you can dive into the nuts and bolts of what you're offering. But here, too, there's a line between too much and too little information.

### 4. Frame your proposal in terms your clients are already familiar with. 

Everybody wants to be an innovator and a trailblazer. Salespeople are no different. There's an undeniable excitement to the idea that your product or service could revolutionize the world.

But your clients might not feel the same way. They're likely to be more hesitant. This is just human nature. Whenever we're presented with something that's completely unfamiliar, we put on the brakes. Your clients might be thinking, "Sure, this revolutionary product may seem attractive, but what are the risks?"

Rather than looking for something new, they're likely to fall back on the comfortable and familiar. So, what's the lesson here?

**The key message in this blink is: Frame your proposal in terms your clients are already familiar with.**

There are over 1,000 different ice cream flavors in the world. That's a lot. And yet, the most popular one remains plain vanilla. It isn't too hard to figure out why. The more exotic flavors may sound interesting once in a while but we know what we're getting with vanilla. It's a reliable flavor that we can dress up any way we choose.

When you're pitching your idea, product or opportunity, you should describe it in plain-vanilla terms. Sure, you're most excited about all of the features that make it unique and different. But your prospective buyers will be most interested in all of the ways it's familiar. This doesn't mean you need to hide all the exciting new features. But you should group them all together and present them in a way that demonstrates that what you're offering is the new normal.

Imagine you're trying to attract investors to a new bar and restaurant you want to open. You have a great location and what you think is a one-of-a-kind hook: the bar features nine holes of playable miniature golf. If you focus entirely on how exciting it is to play golf in a bar, you'll probably turn off more people than you attract. Instead, you should focus on all the ways your place will be just like every other bar: your extensive beer selection, the great chef you've got lined up, the parking, things like that. When it comes time to introduce the golf idea, you'll show how themed bars, like sports bars or arcade bars, are booming in popularity. People want more now. Themed bars like yours are becoming the new normal.

No matter how new or unique your proposal, it can always be traced back to something familiar. To close the deal, you'll need to hit the sweet spot between the new and the old.

### 5. Leveraging pessimism is the key to success in sales. 

Salespeople are the ultimate optimists and they'll do everything possible to project that optimism on to you. Whatever they're selling is the deal of a lifetime, and it will never break or go wrong. Whatever your concerns, they'll have an answer. But all this does is put pressure on you, the buyer.

A buyer's questions come from a healthy skepticism that you're being offered something too good to be true. They're a vital part of the buying process.

A great salesperson won't try and brush aside a buyer's pessimism. He'll use it to his advantage.

**The key message here is: Leveraging pessimism is the key to success in sales.**

The way to use your buyer's pessimism is by creating a script known as the _buyer's formula._ This encourages your buyer to feel she's in control of the process, while allowing you to direct the process to your advantage.

Here's how it works in action.

First, establish your value as an expert by saying something like, "I've dealt with this sort of thing hundreds of times." Next, outline some of the obvious ways your proposal can fail, anticipating some of your buyer's initial objections. Imagine you're a bike salesperson. You're selling a customer a value-for-money city bike. You can anticipate the buyer's probable concerns by reminding them that price isn't a measure of quality. Or you can tell them that this city bike can also be used for trekking.

Now you can move on to more counterintuitive things your customer might not have considered. You could point out that high-tech materials like carbon fiber are intended for professionals and are too fragile for most everyday bike riders.

Next, list some of the obvious actions your customer will likely take, like additional costs for safety equipment or other necessary add-ons. Then, spotlight some less obvious actions, things you'd only know if you've done this sort of thing before. The bike salesman can focus on investing in the right bike for the customer's immediate needs, then upgrading later once she's become comfortable with it.

Once you've done this, hand the process over to the buyer. Not by asking if she has any questions but by a detached statement, something along the lines of, "That's what I'd do." She's free to look at your proposal skeptically but you've already dealt with 99 percent of her concerns by focusing her attention on certain key areas. If she starts to veer off-track, you can easily redirect her simply by saying something like, "A lot of things go into a decision like this but only these three really matter. Focus on them and everything else will sort itself out."

By directing your client's concerns where you want them to go, you can easily overcome any objections and substantially reduce the likelihood of a missed sale.

### 6. Always be true to your own authentic self. 

They say charisma can't be taught. You either have it or you don't. That may be true but charisma isn't the defining trait of a successful, compelling salesperson. What buyers are looking for is someone they can believe in. This means being confident, relaxed and, above all, sticking to your guns.

**The Key Message here is: Always be true to your own authentic self.**

The typical salesperson can be represented with five different archetypes. See if any of these sound familiar.

She starts out as your _best friend_. "Great to see you! Where are you from? Oh, wow! I have family there!" Next, she'll shift into _huckster_ mode, bombarding you with every last detail about the wonderful product she's offering. If that's not enough to win you over, she becomes a _miracle worker_, producing completely unverifiable claims that her product will change your life. "You'll never look at french fries the same way!" The trial close is floated by a perfectly _submissive angel_ who will do anything and everything to get you to buy. But when you still have doubts, the angel turns into a _wolf,_ swatting aside your objections as utterly irrelevant. By the end of the encounter, you don't know who you're dealing with!

Savvy buyers can see through most obvious sales tactics, especially when you're constantly switching personas. It keeps them guarded and forces them to question your integrity. There are two things that buyers respond to consistently: authenticity and expertise. People will know when you're trying to put one over on them, so just be yourself. And when you really, honestly believe in your idea, your product or your service, that comes through. Whatever it is you're trying to sell, no one knows it better than you.

You are the expert, so draw confidence from that. Your belief in what you're selling will inspire confidence in the people you're selling it to. But if that confidence wavers, stick to your guns. Indecision can be seen as a sign of weakness. Buyers will respect you if you say, "I may not always be right, but I'm never in doubt."

By showcasing who you are and holding firm to your core beliefs, values and principles, you can present a compelling case for any idea you want to put across.

### 7. Final summary 

The key message in these blinks is:

**Sales isn't just about goods and services. Sooner or later, we all have to sell ideas, projects, even points of view. By flipping the script, controlling the conversation and establishing your credibility, you can present your ideas in a way that buyers perceive as natural and even inevitable.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Contagious_** **, by Jonah Berger**

Now that you've learned how to flip the script, you're ready to start landing new clients or customers. But in an ideal world, you wouldn't have to snag them one by one and wait for them to add up to a big boost in sales. Instead, you'd have a product or service that practically sold itself. In other words, you'd make it contagious. 

How do you tap into the power of word-of-mouth marketing? What are the secrets to the success of products and services that spread through the market like a virus? And how do you apply those secrets to your own product or service? To learn the answers to these and other questions, check out our blinks to _Contagious_, by Jonah Berger.
---

### Oren Klaff

Oren Klaff is one of the leading experts on sales and negotiation. As an advisor on raising capital and investment funds, his services are in demand in a wide array of industries around the world. His first book, _Pitch Anything_, has sold over one million copies to date.

