---
id: 5713b7cc5236f20007c16f94
slug: flourish-en
published_date: 2016-04-19T00:00:00.000+00:00
author: Martin E.P. Seligman
title: Flourish
subtitle: A Visionary New Understanding of Happiness and Well-being
main_color: C7282D
text_color: C7282D
---

# Flourish

_A Visionary New Understanding of Happiness and Well-being_

**Martin E.P. Seligman**

_Flourish_ (2012) reveals how optimism, motivation and character have the power to help us lead better lives. Based on extensive academic research and complemented by interactive exercises, these blinks introduce a new theory about what it means to live a good life.

---
### 1. What’s in it for me? Increase your well-being through the science of happiness. 

What should we aim for in life? Loads of money? Fame? Increased knowledge? For centuries, thinkers have grappled with this profound and difficult question. And many of the finest minds in human history, from Aristotle to Nietzsche, have tried and failed to understand what truly makes us happy.

But we may be getting closer to an answer. In these blinks, based on the thinking of one of the leading minds in the field of happiness, you'll learn about a truly scientific approach to well-being. By approaching the subject scientifically, psychologists have found multiple elements that, when combined, constitute human well-being.

These blinks help explain what these elements are and how they can be used to help us all find happiness and peace of mind.

They also explain

  * where you can find the positivity in King Lear;

  * why wealth is most definitely not the path to well-being; and

  * why IQ tests don't measure real achievement.

### 2. Unlike prescription pills, positive psychology provides long-term solutions. 

Prescription drugs are our society's quick-fix solution for people struggling with mental health issues. But, in the long run, such short-term solutions often do more harm than good. 

Countless studies have revealed that, in most cases, prescription drugs are only partially and temporarily effective. Drugs such as Prozac, Zoloft and Lexapro do have a significant impact on 65 percent of patients — but this number becomes less impressive when you consider that the placebo effect has the same impact on 55 percent of patients. 

Six of these studies were compared by a group of esteemed psychologists and psychiatrists, and they found that, overall, drugs are effective in cases of severe depression, but fail to help those with moderate and mild cases. And yet, these patients are also in need of help. If drugs can't provide them with the assistance they need, though, who or what can?

The answer is _positive psychology_, an approach centered on improving well-being, happiness and enjoyment in life. Studies have shown positive psychology treatments to be surprisingly effective. Nearly all participants in a week-long series of positive psychology exercises — focused on gratitude and kindness to strangers — reported feeling far happier afterward. After a single week's training, some patients even experienced positive results for up to six months. 

Positive psychology isn't just for combating mental health issues, either. Every individual, regardless of background or situation, can benefit from this approach. How? Find out more in the coming blinks!

> _"Many clinical psychologists have been blinded by Sigmund Freud and Arthur Schopenhauer, who taught that the best humans can ever achieve is to minimize their own misery."_

### 3. Scientific thinking is what makes positive psychology so effective. 

Boosting your happiness and well-being sounds like a relatively clear goal. But try to define what happiness and well-being really are, and the picture becomes a little more complex. 

Even philosophers struggled with the question of well-being and how to achieve it. While Aristotle argued that the central goal of human life was maximizing human happiness, Nietzsche believed that a hunger for power underlay all human pursuits, including that of happiness. Freud, on the other hand, was convinced that our desire for well-being was simply the result of our determination to avoid anxiety. 

Though each of these theories have a rather specific conception of what well-being is, they still fail to capture the subtleties and nuances of this incredibly complex phenomenon. Well-being simply cannot be reduced to single variables. It's neither the weather nor freedom that mainly influences our well-being. Rather, a whole range of different factors shape our happiness, often without our realizing it. 

Positive psychologists must seek out measurable scientific elements in this rich tapestry of variables in order to understand well-being. This is by no means an easy task. Many approaches to measuring human happiness have severe shortcomings. Life satisfaction questionnaires, for instance, which are used to measure happiness, have been proven to only measure the mood of participants at the time of response, and fail to tap into their underlying levels of happiness. 

Experiments, longitudinal studies, placebo-controlled studies and other reliable scientific methods are the positive psychologist's tools for shedding light on the true nature of well-being. With empirical methods and innovative experimentation, positive psychology is firmly grounded in scientific thinking. That's why it works. 

Feeling curious about what variables of well-being _have_ been identified? We'll learn about these in the next blink.

### 4. Positive emotion, engagement, accomplishment, meaning and social relationships are the five pillars of our well-being. 

How can we define well-being in a way that allows us to evaluate it objectively? Positive psychology has revealed five key elements that each play a crucial role in our happiness. The first three elements — _positive emotion, engagement_ and _meaning_ — have long been considered imperative. 

Positive emotion refers to those great feelings we all love to experience: warmth, pleasure, comfort rapture, ecstasy and so on. Is your life full of these wonderful emotions? If so, you're leading what's called a _pleasant life_. 

Engagement, on the other hand, has more to do with _flow_ — the experience we have while completely absorbed in a task. When doing something we love, whether it's playing video games, practicing an instrument or playing sports, flow makes us feel like time is flying by, and can even make us forget ourselves completely! 

Meaning entails a sense of belonging and the will to serve a cause you believe in. From fighting for social justice or environmental sustainability, to giving your time and skills to a community that you relate to, meaning is what gives you a feeling of fulfillment and pride. 

Alongside these three timeless elements, positive psychology has identified two other variables that shape our well-being today: _accomplishment_ and _positive relationships_. 

To put it simply, accomplishment is winning for winning's sake. Feelings of success and achievement are deciding factors in our happiness these days. Accomplishments in our jobs, in our passions and in our communities are sure to give our well-being a boost. 

Finally, positive relationships is about building a vibrant and varied social life filled with friends we love and trust. Great relationships are actually the best natural antidepressant, so it's no surprise that those who benefit from strong friendships lead far happier lives. 

Positive psychology aims to improve human well-being by ensuring that each of these five variables is at a high level. But how?

> _"Other people are the best antidote to the downs of life and the single most reliable up."_

### 5. Students benefit enormously from simple positive psychology exercises. 

Having investigated the five key variables of well-being, let's look at how positive psychology can be applied in schools to help children develop into happy, fulfilled adults. 

Even the simplest positive psychology exercises can have a powerful impact on a child's well-being. One of these is the _What went well_ exercise. For one week, right before going to bed, students should take ten minutes to write down three things that went well that day. 

After identifying these three things, students should answer the question, "Why did this happen?" Whether they focused harder or contributed to a class discussion, students will be able to recognize how their choices shaped their positive experiences. And this will help them repeat these positive choices in the future.

Another basic positive psychology exercise is simply called the _Kindness exercise_. All kids need to do is think up a random kind act to do tomorrow, and then do it. After the kind act, be it surprising Grandma with a visit or giving a gift to a friend, children should observe the impact this has on their mood. This exercise sheds light on the power of little gestures to make not only others happy but also oneself. 

Positive psychology can even be integrated into school studies. Sports, creative arts and academic subjects all provide ample opportunity for positive-thinking exercises. Literature courses at Geelong Grammar School, in Australia, for instance, promote well-being by teaching classic texts in light of the positive traits of every character — even of villains or tragic figures in plays like _King Lear_! 

These strategies have also been subject to studies, with great results: High-school students that took part in a controlled positive psychology study were less depressed, had greater ambitions and fewer conduct problems than their peers. The author himself notes that the most positive feedback received in his long teaching career came from his students, who often described positive psychology exercises as genuinely life-changing.

### 6. It takes more than a high IQ to achieve success. 

We learned earlier that _accomplishment_ is one of the essential pillars of well-being. So, what sort of metrics can help us evaluate accomplishment? Traditionally, intelligence was measured with IQ tests to predict chances of success. But this metric is far from sufficient. 

For one thing, IQ tests simply cannot capture the character traits that are crucial to success. There are four qualities typically tied to high-achievers: _speed of thinking, the ability to plan and revise existing work, fast learning rates_ and _effort invested in tasks._

IQ tests typically place a lot of emphasis on testing speed of thinking. Take the _choice-reaction time_ exercise, for example. Subjects seated before a panel with a light and two buttons, one on the left and one on the right, are told to press the left button when the light is green and the right button when the light is red. They're to do this as fast as possible. IQ results show a marked correlation with performance in this task. 

Yet the remaining three traits of success are nowhere to be seen in typical IQ testing. New approaches to testing have had to be developed to consider other important qualities that help us achieve our goals. 

Psychologist Angela Duckworth developed a composite measure with several tests for different characteristics: the Eysenck Junior Impulsiveness Scale, a 54-item yes/no questionnaire designed to measure impulsiveness; a self-control scale to be filled out by parents and teachers ranging from one for maximally self-controlled children to seven for maximally impulsive; and tests that investigate delay of gratification — a child's willingness to give up a small satisfaction today for a greater one in the future.

Students who scored higher on this new measure also exhibited higher grade point averages, better test scores and less absenteeism, making them more likely to achieve success after graduating.

> _Girls score higher in self-discipline than boys and, on average, receive better high-school grades, although the IQ of boys and girls is similar._

### 7. Wealth and well-being don’t go hand in hand. 

Richer people have got to be happier, right? With excellent living standards and a network of friends just as accomplished and affluent as they are, why wouldn't they be? Well, for lots of reasons. 

If you consider the difference between GDP and levels of well-being, you'll find a shocking discrepancy. Though GDP and life satisfaction generally share a positive relationship, a closer investigation reveals some strange anomalies. Latin countries achieve far better results in life satisfaction, despite lower GDPs. Post-communist countries with higher GDPs score below average on life satisfaction. And life satisfaction in the United States has not improved for 50 years, although the GDP has tripled in that time. Why is this?

It becomes clearer when we see how GDP is calculated. The GDP is simply a measure of goods and services produced and consumed, regardless of whether these products _decrease_ well-being. A spike in anti-depressant consumption implies more people being able to work, while a larger workforce and longer commutes to work lead to more travel expenses. Both raise the GDP, though they indicate a lower quality of life. 

While wealth can provide us with material goods and pleasant customer-service experiences that make us feel good, it cannot offer us any of the remaining elements of well-being. Experiences of meaning, engagement and accomplishment in our lives do not arise out of wealth alone. Human relationships can even be negatively impacted by wealth, as we learn to rely on money more than we do on each other.

So, don't favor money over these other elements, figuring you'll be able to buy them back later. That's simply not how happiness works.

### 8. Final summary 

The key message in this book:

**Prescription drugs, wealth and a high IQ aren't the factors that make us happy, successful people. Positive emotion, engagement, accomplishment, meaning and social relationships are what really matter. By exploring positive psychology exercises in our lives and within schools, we can give ourselves the well-being we deserve.**

Actionable advice:

**Give thanks!**

Simple exercises are all it takes to improve your well-being. The _gratitude letter_ is one of these. Write a letter of gratitude to somebody who helped you without asking for anything in return. Take 300 words to express your appreciation in a concrete way, detailing how their help affected your life, and why you still reflect upon it today. Let the recipient read it while you're there and watch as positive feelings unfold for both of you. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Learned Optimism_** **by Martin Seligman**

_Learned Optimism_ explains why so many people grow up to be pessimistic and what the negative implications of this habit are. Furthermore, it shows how our habitual optimism or pessimism influences us for better or for worse in all areas of life. Finally, it shows how pessimists can learn how to become optimists, thus greatly improving their health and happiness, and presents several techniques for learning this new way of thinking.
---

### Martin E.P. Seligman

Martin Seligman, a founding father of positive psychology, is a psychology professor at the University of Pennsylvania. A former chair of the American Psychological Association, he has written multiple books, including _Learned Optimism_ and _Authentic Happiness_.

