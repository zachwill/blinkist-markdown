---
id: 532839fc36353800084c0000
slug: flow-en
published_date: 2014-03-18T09:28:13.000+00:00
author: Mihaly Csikszentmihalyi
title: Flow
subtitle: The Psychology of Optimal Experience
main_color: 28BEC9
text_color: 197880
---

# Flow

_The Psychology of Optimal Experience_

**Mihaly Csikszentmihalyi**

_Flow_ (1990) explores how we can experience enjoyment in our lives by controlling our attention and strengthening our resolve. This is achieved by being immersed in an activity or subject that makes us neither anxious (if it's too hard), nor bored (if it's too easy). In this "flow state" we lose our self-consciousness, selfishness and sense of time. Using goal-setting and immediate feedback, we can achieve a state of flow that improves our relationship with work, increases our self-worth and gives our lives meaning.

---
### 1. What’s in it for me? To discover meaning in your life, just go with the flow. 

Why is it that some people enjoy a happy and creative existence while others seem to find themselves settling into a comfortable but frustrating rut?

Mihaly Csikszentmihalyi's _Flow_ has the answer. 

A seminal work in psychology, _Flow_ argues that in our increasingly anxious, distracted lives, we can become too focused on external rewards and opinions (for example, by compulsively comparing ourselves with our peers). As a much-needed remedy, the book offers techniques that enable us to focus instead on _intrinsic_ rewards, which can lead us to engage in our interests so totally that we enter a state of pure flow. In such a state, we simply don't care about external rewards like power or wealth and we don't even consider the opinions of others.

Grounded in years of empirical research, _Flow_ also taps into ancient wisdom, philosophy and modern psychology to provide countless examples of people who discovered how to "get into the zone," and thus lead contented lives and do their best work.

For instance, you'll discover that many scientists did some of their most revolutionary work in their spare time. You'll find out why a surgeon or millionaire footballer might be deeply bored, while a factory worker waxes lyrical about all manner of subjects. You'll also learn why going to jail can help you discover your goals and strengthen your resolve — as it did for Malcolm X. And, finally, you'll see how being more mindful of your surroundings can also help you to get the most out of listening to music. 

If you are dissatisfied at work or bored and unhappy at home, these blinks will jolt you out of your rut and drive you to make the most of your limited time on this planet.

### 2. We use religion and luxury to hide from an indifferent, meaningless world. 

When we view our lives from a distance, they seem insignificant. And when we examine them closely, we notice that we're unhappy and unfulfilled. To help us cope, most of us look for comfort in religion or we seek external rewards, like wealth or fame.

While this approach seems to make sense, it can also lead to us abandoning our critical faculties.

For example, while organized religions like Christianity and Islam have provided us with rules to live by and given our lives meaning, our firsthand discovery of our predicament in the universe has shown the principles of religion to be wrong. Still, many people continue to follow religious ideologies because they're more comfortable thinking of life as meaningful.

Also, many empires and cultures led their citizens to believe they'd mastered their fates — for instance, the Romans at the height of their power and the Chinese before the Mongol invasion. Although this belief comforted people, it proved completely wrong as each of these civilizations collapsed.

And if we're not hiding behind religion or political ideology to avoid the pointlessness of our lives, we're struggling to acquire external rewards like power, wealth or fame. But these don't satisfy us for very long either.

Certainly we live in luxurious times and people from the past wouldn't believe the conveniences that modern life provides. But having more money and acquiring more stuff doesn't seem to make us happier. As one study showed, satisfaction with life doesn't correlate strongly with being wealthy. You don't need to look far to see evidence of this: just think about the number of rich patients that psychiatrists treat regularly.

So in order to give our lives meaning, we try to change the environment around us, whether by displaying our wealth to impress others or chasing powerful positions. Yet these all fail to sustain our happiness.

### 3. Our genes impel us to seek basic pleasures, not the skills and challenges found in enjoyment. 

While our attention can manage only a limited amount of information during our lifetime, from this dwindling resource most of us choose instant gratification as compensation for the daily grind of our lives.

This is because we favor simple _pleasure_ over the more rewarding, yet more difficult to attain, _enjoyment_.

Pleasure provides simple restorative order — much like sleeping or eating: we have evolved so that when our blood sugar is low, we feel famished and are urged to eat something.

Enjoyment, on the other hand, involves us stretching ourselves, using our skills and concentration to transcend the apparent limitations of our genes. In this way, enjoyment helps us to accomplish ambitious goals that we set for ourselves and enables us to gain control over our attention.

This can be seen, for example, when we prepare a meal we've never made before. The patience and the willingness to experiment that this task requires contributes to the development of a sophisticated palate, which enables us to savor every bite.

Nevertheless, it's pleasure, not enjoyment, that we seem to prefer, often in the form of pain-free escapism and hedonism. Yet these lack novelty and the opportunity for growth.

For example, after a hard day's work, many of us sit watching TV, films or videos. This state of pure consumption is when we are at our most passive and easily distracted.

Furthermore, on the weekends, many of us unwind with alcohol or even other drugs. While these may promise relaxation or an expanded consciousness, the result is often that we damage our ability to concentrate and we lose control.

The formulaic storylines of TV programs and the artificial paradise of alcohol or drugs both require external stimulation, while neither allows us to exert skill or to focus fully on our goals.

Our minds often don't do what they can to achieve growth or complexity, but we shouldn't take the path of least resistance and most distraction.

### 4. The elements of enjoyment are available to everyone, but the goal is unique to each of us. 

Across different languages and cultures, people use the same terms to describe what they feel when they are "in the zone."

This feeling is one of enjoyment rather than pleasure and it comes when you are engaged in a task or activity that balances skills and challenges, has clear goals and immediate feedback.

Take surgeons, for example, who perform extremely skillful operations. They get immediate visual feedback on how well they are doing by the lack of blood in an incision, while the removal of a diseased organ can provide them with satisfaction due to the certainty that an operation has been a success.

But not everyone can have the same goals **.** Compare surgeons to practitioners of _internal_ medicine. Like surgeons, they have clear goals; unlike surgeons they can't get immediate feedback and so need to set other goals for _enjoyment_ — perhaps successfully identifying an illness and administering the correct medicine.

Being in the zone means that you're totally immersed in the task at hand. This combines action and awareness, which gives you a feeling of control.

Take rock climbers, for example. Obviously they face extreme danger in their goals, but what they _enjoy_ is using their expertise to quell their fears — for instance, by accurately estimating the difficulty of a climb. To do this, they have to devote their full attention to the task.

Such immersion and total concentration has also been observed in Melanesian sailors. Researchers found that these sailors, when blindfolded and taken hundreds of miles from their home island, were able to pinpoint their exact location just by focusing on the way water currents guided the boat.

This immersion that we can see in the surgeon, the rock climber and the Melanesian sailor is so powerful it can release us from our self-consciousness, worries and anxieties and allow us to lose track of time. Indeed, the rock climber focuses so deeply on the intricacies of the rock face that he forgets his problems and surgeons report having the sensation that their operating team is a single organism.

### 5. Developing new and interesting skills requires facing challenges that are tied to personal rewards. 

One morning in Naples, a US tourist walked into an antique store and asked to buy a sculpture. The owner quoted a steep figure, yet when he saw the tourist was about to pay he claimed the sculpture wasn't for sale.

Why?

He quoted the high price not because he wanted to exploit the tourist, but rather because he enjoyed bargaining and the battle of wits it involves, as it sharpened his mental dexterity and his selling skills.

Whenever we engage in something like this — something that's neither too easy nor too difficult — we tend to expand our personal limits and achieve more.

For example, if you're a beginner at tennis, you'll first simply enjoy trying to hit the ball over the net. As you improve, this easy challenge will start to bore you and you'll start looking for ways to further challenge yourself — probably by playing against another person.

If you choose an opponent who's far more skilled than you are, you'll soon begin to feel out of your depth and anxious. Because the challenge is so difficult, you may even give up the chance to acquire new skills. But if you choose an opponent who's _just above_ your skill level, your skills may actually improve.

Improvement also requires, however, that these skills be aligned with personal goals and passions and remain unaffected by external circumstances such as the promise of a reward if you do well, or the threat of punishment if you don't.

Consider the ceramicist Eva Zeisel, who was imprisoned by Stalin's police. Motivated by the personal need to maintain her sanity, she played chess against herself in her mind, memorized her own poetry and did gymnastics. She continued to improve her skills even in the worst conditions.

People like Zeisel, who had little else to keep them motivated, devised games for themselves to keep themselves sane, improve their skills and powers of imagination and control their consciousness.

### 6. With discipline, we can use our senses and movements to help us tune into a heightened state of awareness. 

For most of us, the idea of paying attention to our walking is an unusual one. Walking simply gets us from A to B.

But by paying attention to the variety of sights around you — the people, their interactions, historical relics, architecture and so on — even the most routine actions, such as walking, can be transformed.By practicing mindfulness of our surroundings, we can learn to perceive much more than our automatic response to the world allows.

Indeed, the world is ripe for inspiration. The sky is filled with wondrous and unusual shapes and colors. Being mindful of such wonders allows us to feel connected to the world and to see things anew.

Or consider the magic of music. Today we're spoiled for choice and can hear almost _any_ music at the click of a button.

Yet we rarely get lost in its full complexity. If we can learn to be mindful of the music we listen to, this can unlock other levels: the _sensory_, feeling the body responding to rhythm and bass; the _analogic_, when you see corresponding images in your mind's eye (perhaps Tchaikovsky driving you on a sleigh through a snow-covered forest); and the _analytic_, in which you analyze the structure of a piece and compare it to other versions and composers and so on.

But to become mindful, we need to strengthen our self-control — which can be achieved by tapping into ancient Eastern wisdom.

For centuries, yoga has been practiced as a method to free the self from the ego. Yet it can be used also to steer our attention in positive directions that are aligned with specific goals. The steps that yoga prescribes to focus our attention are the practices of nonviolence, obedience, cleanliness, disciplined study and the acknowledgment of a higher power.

It is indeed possible to have unprecedented control over your mind using nothing more than the body you inhabit.

### 7. Our memories and thoughts can be cultivated to focus on complex ideas rather than the flaws of the self. 

Many of us who play sports and exercise gain enjoyment from the focused attention these activities require. But it's not only through sports that we can achieve this: we can also use our minds to play games and get into the "flow state" which produces enjoyment.

Such a mental flow state can result from engaging in language and memory games and exercises. For instance, crosswords kill time on trains, but this pursuit is dependent on an external stimulus. Instead, try creating your own crossword puzzles. Not only can this lead to flow, but it also improves your wordplay skills, making conversations more fun by transcending the usual small talk and mundane exchanges.

You can engage your memory, too. Find a subject that interests you and absorb everything about it, such as lines of poetry you like, or the events of World War II. By doing this, you enable yourself to rely on your own memory to stimulate your mind and to feel a connection with the subject.

Furthermore, a flow state can be accomplished by focusing on external things, rather than on one's flaws. You could try doing what Bertrand Russell did to make himself happy: forget about your own flaws and focus instead on the external world, by immersing yourself in many fields of knowledge, or focusing on people you admire.

Indeed, even the complex worlds of science and philosophy can be enjoyed by both amateurs and academics, as they encourage contemplation and the use of logic.

Many scientists, in fact, achieved success because they simply enjoyed the act of improving their scientific skills. For example, Isaac Newton spent two lonely years living in a farmhouse and it was there that he formed his theory of gravity. And Gregor Mendel was a clergyman whose gardening hobby led to the birth of genetics. And let's not forget Einstein who worked by day in a Swiss patent office, formulating his theories in his free time.

### 8. Work that you treat like a game, with intrinsic rewards and varied skills, ceases to be “work.” 

Many people are dissatisfied with their daily routines and often their jobs are to blame. What makes matters worse is that their leisure time is spent recovering from their work in the laziest way.

However, work can be developed into something that provides a challenge, focuses our attention and reduces our anxieties.

Consider the elderly residents of a hamlet in the Italian Alps, who didn't see a distinction between their daily work and their free time. Every day they had to get up at 5 a.m. to milk cows, carry bales of hay for miles, tend the orchard or cook for their families. But when asked what they'd change about their lives if they were wealthy, they responded that they wouldn't change a thing.

Many people have described themselves as more often being in a flow-like state when they were working than when they weren't. They also reported an increased belief in their own creativity and concentration.

One way of getting into a flow state is to set yourself intrinsic rewards (that is, ones not motivated by cash incentives or extrinsic power), such as trying to surpass your usual performance level or learning as much as possible about the job.

Take, for example, the railroad car welder who was very popular with his colleagues. His popularity was due to the fact that he'd learned every essential task in his company's assembly line and he enjoyed performing all of them. He also refused promotions because he preferred to perform a variety of manual tasks and enjoyed turning each of them into a challenge. When the day was done, he didn't feel the need to escape but instead spent his free time cultivating his garden.

So, to get into a state of flow you should seek out new challenges in work, aiming to learn as much as possible about all of the essential tasks involved in keeping your company running, rather than just clocking in and clocking out.

### 9. Engaging with family, friends and community is vital for our happiness, self-expression and growth. 

Busy trains and open-plan offices can impinge on our freedom and individuality. Time spent alone allows us to give our undivided attention to something, but it can also lead to boredom. That's when we need the support of the people we know and trust.

In short, good family, friends and neighbors.

Good families provide honest feedback, unconditional acceptance and long-term goals.

Families that are conducive to enjoyable experiences are both _differentiated_, accepting each family member's distinct skills and traits for what they are, and _integrated_ — being honest, fair to everyone and neglecting no one.

For example, parents who take part in challenging, skillful tasks like carpentry or cooking rather than TV-watching or drinking are more likely to see their children try to emulate these positive traits.

We also require good friendships, as these are essential for strengthening our expressive side. The skills we have are either _instrumental_, like survival and professional skills,or _expressive_, communicating our personalities clearly. Compared with being alone, spending time with friends nurtures our expressive side: it produces much higher levels of happiness, self-esteem, strength and motivation — not to mention providing an audience.

Finally, we need neighbors and communities to provide us with the opportunity for novelty and growth. If we blank our neighbors or avoid contact with our community, we'll miss out on their help in the future and confine ourselves to our old habits.

Consider, for example, Indian tribes in Canada, who often find areas of rich food resources and establish permanent villages. However, every generation they up sticks and move to a different area where they start again from scratch, which means they have to learn new ways of finding and harvesting food. They do this to jolt themselves out of their routine lives and to regain new skills, health and vigor.

So make sure you invest in your relationships, as they will provide many opportunities for happiness and growth.

### 10. Focused attention distances us from our anxiety, helping us to gain perspective and find new ways to grow. 

We're all faced with misfortune at some time or other. Rather than simply giving up because we feel unable to handle the situation, we could employ the following three strategies:

First, we should let go of our egos and trust in our ability to handle situations as they arise.

For example, we've all had a computer stop working for apparently no reason, usually when we're in the middle of doing something important. And most of us have experienced being on trains that break down, disrupting our schedules for the day.

"Why is this happening to _me_?" we often ask ourselves. We feel this frustration because such situations seem to be in direct conflict with our intentions.

So we must learn to take into consideration and appreciate the laws that govern the computer or the train and not only our own personal needs and motives.

The second strategy is to practice being mindful of our environment.

Take, for example, Charles Lindbergh, the first person to fly solo across the Atlantic. Taking such an apparent risk would've struck fear into most people, but rather than focusing on his fear, Lindbergh paid attention to the intricacies of the cockpit — the levers, knobs and even the welding marks. By being mindful, Lindbergh was freed from his anxiety.

Thirdly, instead of giving up in the face of difficult situations, you should use them to discover novel solutions.

Say you've been working hard at your job, but your one chance of promotion is challenged by your boss's special relationship with your colleague.

One solution is to try to win favor, ingratiating yourself with your boss. However, you could also go in a more novel direction; for instance, you could take a job with a different company, find a new career or simply decide to spend more time on your own projects.

Neither of these solutions is better than the other, but the latter would provide you with far more enjoyable challenges.

### 11. Discover purpose in life through having unified goals and the resolve to put them into action. 

As Earth is not the center of the universe and our lives are manipulated by our genes, life can seem bereft of ultimate meaning. Yet we can certainly _create_ meaning and the beauty of this is that each of us can choose what that meaning is.

To find your meaning you need an ultimate goal in life to focus on. The end goal is irrelevant, as long as it immerses you fully in increasingly complex challenges, allowing you to disregard others' opinions.

For example, Renaissance artists strived for and immersed themselves in an _idealistic_ culture, by choosing freely from the best of two opposing cultures: one of physical health and the concrete senses, the other of abstraction and spirituality.

Once you've established your goal, you have to act on it and for this you need strong intentions and resolutions. It's all too easy to conceive of some life goal yet never realize it.

Indeed, many people remain "armchair activists," procrastinating by writing endless to-do lists. Antonio Gramsci, for example, might've become just another insulated academic, but instead he turned the illnesses and poverty of his childhood into a lifelong battle against the social conditions that challenged his family. Because of the strength of his resolve, he became stridently political and died in one of Mussolini's jails as one of Fascism's strongest opponents.

Finally, your goals and resolutions should be harmonious, expressing a life theme.

One person who mastered this was Malcolm X. He grew up in poverty, dealt drugs and went to jail. It was there that he discovered reading and reflection and gained the self-knowledge which drove his resolve: to become a civil rights activist and improve the lives of others.

Just imagine where we'd be without such clear goals and strong resolve. Would we be capable of fighting deadly diseases, crafting masterpieces or walking on the moon?

### 12. Final summary 

The key message in this book:

**To live an optimal life, try not to be influenced by external rewards or the opinions of others. You can attain enjoyment in life by focusing your attention on every moment, being mindful of your environment and immersing yourself in your interests. Finally, you should never avoid facing difficult challenges, as they can lead to personal growth and achievement.**

Actionable advice:

**Learn more about your job.**

Set yourself a challenge to learn as much as possible about your job, accept opportunities for new tasks and work better and faster than you have before. Not only will this lead to less procrastination but the time will pass much more quickly. You'll also become more popular with colleagues and you may even be offered a promotion, but don't let these be your central motivations.

**Turn off the TV and get creative.**

Instead of watching TV or movies in the evening, engage with your friends, flatmates or community and challenge yourself to, for example, create and act in your own plays or start a book club. When you feel like drinking away your weekend, why not instead lose yourself in salsa dancing, stand-up comedy or the Kama Sutra? These types of activities can improve your confidence and elevate your conversations beyond the usual small talk and chit chat.

### 1. What’s in it for me? Get to know your body on a deeper level. 

Did you know that women have on average about 450 periods during their lifetime? That may seem like a big number, but despite it being such a common occurrence, many women don't know what's going on during menstruation.

Historically, menstruation has been taboo, and thus the topic is often misunderstood. Today, we still find many women who are embarrassed to talk about this natural bodily function. Because of this shame, the knowledge surrounding periods and, to a wider extent, female sexual health, is lacking. This needs to change.

These blinks dissect the historical taboos about discussing periods and provide you with answers to basic questions, such as, "Is period sex normal?" and "Why do periods even exist?"

With greater insight into what's normal and what isn't, you'll be able to make more informed choices about your body.

In these blinks, you'll learn

  * why women experience cramping during their period;

  * what women did before tampons and pads; and

  * how drug companies use outdated perceptions to sell their products.

### 2. Since ancient times, menstruation has been taboo and surrounded by misperceptions. 

Before the age of scientific knowledge, myths provided explanations for why young girls and women bled from their vaginas on a monthly basis. Though these stories portrayed menstruation as a powerful process, ancient people also perceived it as a marker of women's inferiority. Thus, period blood was simultaneously understood as a sacred substance of life _and_ a toxic matter.

So, though they often believed that this vaginal bleeding was the sacred remains of an unborn child, ancient peoples also condemned it as evil and dangerous.

According to Roman philosopher Pliny the Elder in his book _Natural History,_ written in AD 77, period blood could cause a horse to have a miscarriage and the extermination of flowers, among other things. These assertions remained uncontested for more than a thousand years. Furthermore, the belief that period blood is toxic persisted well into the twentieth century. Even today, certain cultures still believe it.

Based on the ancient belief that menstruation is the process of the body cleansing itself from the toxicity of menstrual blood, doctors developed a procedure called _bloodletting_. Bloodletting was a process in which illnesses were treated via the draining of blood from a vein.

Bloodletting was used on both men and women. But since menstruation was a wholly feminine phenomenon, the myths and misperceptions surrounding it were used to subvert women's position in ancient society.

Back then, a woman on her period would have to go away to a menstrual hut. Unbelievably, this arcane act still exists in some parts of the world. Not only that, but _menarche_, or the onset of menstruation, would be followed by rituals. One such ritual in British Columbia forced girls out into the wilderness; one in New Ireland kept young women in cages for up to four years.

Menstruation was also used as an excuse to exclude women from different types of institutions. Even in the 1920s, for example, menstruating women weren't allowed to enter churches around the world, wineries in Germany or opium labs in Vietnam.

Today, menstruating women are banned from partaking in Islamic rituals. These outdated beliefs surrounding periods have had a significant effect on contemporary societies all over the world. We'll explore this more deeply in the upcoming blinks.

### 3. What we know as PMS today used to be diagnosed as hysteria. 

Throughout the Middle Ages, women exhibiting signs of "hysteria" were accused of being witches. These signs included anything from insomnia to random bursts of laughter or crying. In modern times, however, the same symptoms of hysteria are referred to as _premenstrual syndrome_ or PMS.

The diagnosis of hysteria had a great impact on women's history, illustrating the general ignorance of female anatomy and sexuality. For example, even the "Father of Medicine," ancient Greek physician Hippocrates, believed that hysteria was caused by the uterus snaking its way around a woman's body. The term was only dropped by the American Psychiatric Association in 1952, and the diagnosis of premenstrual syndrome developed a year later.

Treatment of hysteria involved everything from X-rays to using leeches on the patient's vulva. The most common treatment, however, was a doctor or midwife stimulating the patient's clitoris until they reached orgasm. As crazy and sexual as this sounds, it was viewed as a wholly medical procedure and was not to be performed at home unsupervised.

Similarly, today's understanding of PMS is as unclear as the outdated diagnosis of hysteria.

While we know some things about PMS, like how cramps are the result of the uterus contracting, there is a lack of research on the causes of other symptoms, such as insomnia or mood swings. In fact, there's no conclusive evidence showing that PMS is hormonal! Furthermore, PMS, and the more severe condition PMDD ( _Premenstrual Dysphoric Disorder_ ) are largely Western medical concepts.

Whether hysteria or PMS, one key question remains: should emotional and physiological reactions during menstruation be treated as problems that need solving or are they simply part of being human?

### 4. Menstrual sex carries negative connotations, but in fact, it’s safe and normal. 

Of all the many taboos related to menstruation, the biggest is probably period sex. Though it may be a messy activity, period sex has not been found harmful by modern science. By educating ourselves about where these taboos originated, we can begin to unpack the myths about sex and menstruation and start challenging the stigmas surrounding them.

Historically, most religions held the dogma that period sex led to contamination, and that women required cleansing after menstruation before they could have sex.

In Orthodox Judaism, for the two weeks covering and following a woman's period, she is deemed unclean. In these 14 days, the woman is considered a _niddah_, during which time she and her husband aren't allowed any physical contact, including sex. If there is no further trace of blood seven days from the onset of her period, the woman can bathe in a ritual bath — a _mikveh_ — to cleanse her body.

Orthodox Judaism is not alone in its harsh treatment of menstruating women. With the exception of Buddhism, many religions around the world share the same mistrust toward periods.

Muslim women are forbidden to have sex with their husbands, fast or handle the Koran during that time of the month. The Bible discourages men from shaking hands with a woman who's on her period. What's more, all tampon ads were banned from Polish television networks during Pope Benedict's visit in 2006.

But despite the widespread negative viewpoints that persist in modern society, the fact is that period sex is completely safe and normal.

A 2002 study at Yale University even suggests that women who experience orgasms while on their period are less likely to suffer from _endometriosis_, a painful disorder where the uterine lining grows outside the uterus.

Unfortunately, this isn't talked about enough. And, for many women, discussing anything period-related still causes feelings of deep shame.

### 5. The women’s rights movement gathered momentum with the development of femcare products. 

Can you imagine what it was like for women before pads and tampons were commercially available? Having a rag strapped between your thighs for roughly seven days a month, every month, would've made it challenging enough to walk, let alone fully participate in societal events and activities. Therefore, the development of femcare products accompanied historic political change for women.

It wasn't just the lack of tampons that was a challenge before femcare products were invented, it was also the lack of underwear. Until the twentieth century, underwear such as petticoats and shifts weren't worn for hygiene reasons but rather for warmth. Some women from wealthy families were afforded the luxury of wearing rubber-lined aprons and bloomers to stop their period from tarnishing their dresses, but most women had to resort to moss, rags, leaves and sheepskin.

In 1920, Kotex pads, which were made from the cellucotton leftover from WWI bandages, became available. In the same year, the Nineteenth Amendment was enacted, allowing women to vote. Although the pads were a great advancement, they were still bulky and had to be held together with elastic belts and pins, making them highly uncomfortable.

Later on, in the 1970s, self-adhesive pads were sold by brands like Carefree and Stayfree. This coincided with the women's liberation movement, where women were fighting for their rights on issues such as equal pay and abortion.

Today, in the West, menstruation no longer gets in the way of a woman's education or career. But over in Sub-Saharan Africa, for example, some young girls are absent from school 10 to 20 percent of the time because of a lack of femcare products.

### 6. Femcare ads perpetuate the ancient belief that menstruation is a shameful phenomenon. 

Thanks to television, magazine and billboard advertisements for femcare products, periods have stepped out of the shadows and become more visible than ever. But the spotlight shone on periods has only helped to serve the outdated viewpoint that menstruation is shameful and needs to be hidden.

The collective consensus on menstruation was built on successful marketing campaigns led by the two-billion-dollar femcare industry.

Femcare ads typically show beautiful women dressed in little clothing and surrounded by tranquil landscapes. No mention or reference to period blood is made. This suggests that menstruation is impure and that women require cleansing afterward.

Furthermore, though the ban on femcare ads was lifted by the National Association of Broadcasters in 1972, menstrual blood is still being represented by a blue liquid, suggesting that periods are too unclean to be seen by American audiences.

In addition to exploiting women's insecurities about their periods, marketing campaigns are now exploiting fears about vaginal odors to sell douches.

Jokes about vaginal smells have persisted for years, creating deeply rooted insecurities for many women. The fact is, healthy vaginas usually have no odor, and if they do, it's a sign that maybe something isn't right and you should visit a healthcare professional.

However, since the 1930s, the femcare industry has used ads to convince women to buy products that mask these non-existent vaginal odors, claiming that these treatments could solve marital problems. They were very dangerous: popular vaginal douches such as Zonite (a weaker concentration of bleach) and Lysol (the very same chemical you use to disinfect your kitchen), or even a mixture of water and vinegar, can disturb your body's pH levels and cause bacterial vaginosis, yeast infections and, worse yet, pelvic inflammatory disease.

Despite the proven ineffectiveness and risk of vaginal douche products, 20 to 40 percent of American women report regularly using them, showing just how deeply rooted these insecurities are.

### 7. Many women lack basic knowledge about menstrual cycles and pregnancy. 

We're all aware of the implied meaning behind the words "I'm late," either having heard it on a TV drama or from a good friend. What many people, including women, don't know is that a late period isn't necessarily indicative of pregnancy. It could indicate very different things like stress, perimenopause or even that you're still ovulating.

The absence of information surrounding menstrual cycles and pregnancy is a result of the collective shame manufactured and exploited by American culture when it comes to anything period-related.

For example, one overlooked fact is that women are able to menstruate without ovulating — and vice versa. In the two years following the onset of menstruation, up to 80 percent of menstrual cycles don't include ovulation. In contrast, _amenorrheic women_ who have gone for months without menstruating sometimes get pregnant because they were ovulating despite not having their periods.

Furthermore, you can become pregnant even if you're having sex on your period. Though many animals usually only have sex when they are in estrus — in heat — human beings have sex at any point of the female's menstrual cycle. However, many people are unaware that having unprotected sex in the weeks following your period exposes you to a high risk of pregnancy. This is because you are most fertile after your period, during ovulation.

Even fewer people know that sperm can remain in a woman's vagina for a few days, so if you have sex while on your period, there's a possibility that you can still get pregnant. Additionally, the heavy spotting women sometimes get when ovulating is often mistaken for their period.

Ultimately, there is a large gap in our knowledge surrounding menstruation. This is mainly due to the distribution of information by big businesses that are more interested in selling their products than they are in educating women about their natural bodily processes.

### 8. The bleeding that women experience while on the pill isn’t actually a period. 

Did you know that over 100 million women from all over the world use the birth control pill? Surprisingly, most of them aren't aware that the bleeding they experience while on the pill isn't their period. It just goes to show how many women don't know enough about menstruation.

Let's begin at the top. What _is_ a period?

When your cycle starts, your brain messages the _egg follicles_ that are located in your ovaries to begin ripening. On the thirteenth day, or thereabouts, the brain sends out another signal to let your body know it's time to ovulate. The remaining follicle (or follicles in the case of twins) opens and releases the hormones _progesterone_ and _estrogen_, which tell the uterus lining — or _endometrium_ — to expand so that the egg can travel through the _fallopian tubes_ to the uterus.

If the egg encounters any sperm on its journey, it will latch itself onto the uterine wall. Here, it will form a _placenta_, which will be nourished with blood via three arteries from the uterus.

If there is no conception, the egg will simply dissolve, and the arteries will stop pumping blood to the uterine wall, effectively killing the layers of _endometrial tissue_ on the uterine wall. Then, to get rid of the tissue, the arteries open up for a short period to let the sudden pressure of blood rush through and rupture the layers. During this process, gravity and uterine contractions — more commonly known as cramps — accelerate the passage of the dead tissue through the _cervix_ and out through the vagina. On the outside, what you're left with — and what period blood is made of — is blood, uterine tissue and mucus.

However, what happens when you're on the pill is something else.

Typically, birth control pills contain estrogen or a mixture of estrogen and progestin, which you take for three weeks to trick your body into thinking that it's pregnant. During the remaining week, you take _placebo pills_, which create a mild version of endometrium that breaks down and exits the vagina.

Therefore, you experience gentler versions of blood flow, cramps and other related symptoms when you're on the pill because it's not really menstruation at all.

### 9. Pharmaceutical companies try to profit from our negative view of menstruation and menopause. 

Have you ever tried to imagine life without periods or the side effects of menopause? Seems pretty great, right? But if you think about it a little more deeply, does it sound like it's a good idea to have these bodily processes eliminated for good?

To get right to the point, it's a sales pitch from drug companies. Remember the birth control placebo pills mentioned in the previous blink? They were originally included in birth control pill packages because drug companies feared that women wouldn't buy something that suppressed a natural bodily function.

However, today, we can stop or reduce our periods with drugs such as Yaz, Lybrel and Implanon, which are basically birth control pills without the placebo.

Understandably, women who suffer from extremely painful cramps and other menstrual symptoms might view these period-restricting drugs as a more tolerable alternative to having a _hysterectomy_ (the surgical removal of the uterus). But the truth is, most women don't struggle much or at all with the symptoms of menstruation and so these drugs don't deserve to be promoted as a worldwide "solution."

As with periods, pharmaceutical companies have also tried to exploit women's fear of menopause. For a long time, menopause was seen as a disease. Nowadays, we understand that it's nothing more than a natural process that female bodies undergo. With the average life expectancy as high as it's ever been, more and more women are going through it.

But despite growing exposure, women still fear the symptoms that accompany menopause, such as hot flashes, difficulty concentrating and mood swings, as well as signs of aging. Drug companies have capitalized on the fear of aging since the 1930s, convincing women that they can avoid it with _Hormone Replacement Therapy_ (HRT). Though long-term use of HRT has been known to increase the risk of blood clots, heart disease, stroke and breast cancer, drug companies still insist on marketing these drugs to consumers today.

> _"So little is actually known about menstruation that it's hard to predict what the unintended effects of widespread suppression might be."_

### 10. Final summary 

The key message in this book:

**For centuries menstruation was demonized, and people were afraid to discuss it in public. By exploring the historical and cultural context surrounding periods, we can begin to open up the discussion and arm women with knowledge about their natural and normal bodily processes.**

Actionable advice:

**Start a period diary.**

Record all the information about your cycles, either in a physical diary or digitally with an app. The best way to learn more about what's happening to your body every month is to be attentive to it. Make a note of whether you're feeling tired or emotional, hungry or bloated, whether or not your period was on time, when you had unprotected sex, and so on. Having a record of your body's changes will help you keep track of everything that's going on down there so that there won't be any more surprises or freakouts in the future.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Moody Bitches_** **by Julie Holland, MD**

_Moody Bitches_ (2015) is your guide to the female body and brain. These blinks explain some of the reasons behind the emotions and fluctuating moods that women can experience and how they can better tune into themselves, embrace their feelings and their bodies.
---

### Mihaly Csikszentmihalyi

Mihaly Csikszentmihalyi is a professor of psychology with a PhD from the University of Chicago. He has been described as the global leader in research on positive psychology, creativity and motivation. His other books based on this research include _Creativity: The Psychology of Discovery_ and _Invention and Finding Flow: The Psychology of Engagement with Everyday Life._

