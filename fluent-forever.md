---
id: 54bd1b54633234000a070000
slug: fluent-forever-en
published_date: 2015-01-19T00:00:00.000+00:00
author: Gabriel Wyner
title: Fluent Forever
subtitle: How to Learn Any Language Fast and Never Forget It
main_color: 27ADB6
text_color: 1C7C82
---

# Fluent Forever

_How to Learn Any Language Fast and Never Forget It_

**Gabriel Wyner**

_Fluent Forever_ unlocks the secrets of how to get the most out of your memory, so you can learn languages faster than you ever thought possible. It teaches you how your memory works and the precise techniques you can use to remember more words, more accurately, in a way that's efficient and fun.

---
### 1. What’s in it for me? Learn how to learn languages, fast. 

Why is learning a language such a struggle? It can feel like such a fight, between not forgetting new words, slaving through grammar rules, and tongue-twisting through pronunciation.

_Fluent Forever_ aims to turn all those setbacks upside down. By bringing neuroscience together with tricks on pronunciation and memory, Gabriel Wyner makes learning a language less of a fight than a game. It becomes fun to use his tips, the same tips that allowed him to learn German in 14 weeks.

After reading through these blinks, you'll also be able to attack new languages — or any memory challenge — with a much better chance at reaching your goals.

These blinks will teach you

  * how Google Images can help your word comprehension;

  * why DVD subtitles are evil; and

  * how Japanese people can learn to hear the difference between "rock" and "lock."

### 2. Images and personal connections make remembering easier for the brain. 

If you've ever tried to pick up a new language, then you are probably familiar with the infuriating feeling of learning a new word, only to forget it moments later! Luckily, there are some ways to get these new words to stick, and one of them is by creating connections in the brain.

Making connections is central to remembering, but some connections are more effective than others.

Connecting a word to a personal experience is most effective, as this activates numerous areas all over your brain.

The brain processes words on four different levels: _Structure_, _Sound_, _Concept_ and _Personal Connection_.

In one 1970s psychology experiment, students were asked questions based on these levels, such as, "How many capital letters are there in the word 'bear'?" (Structure); "Does 'apple' rhyme with 'snapple'?" (Sound); and "Is tool another word for 'instrument'?" (Concept). They then took an unexpected memory test to see which of the words they could recall. It turned out that the students were six times more likely to remember the word if the question called for a personal connection, like "Do you like pizza?" than if the question concerned the structure of the word.

So, say you want to learn the Spanish word for cat (gato). If you can connect the word to a memory you have of a cat, you'll be 50 percent more likely to remember it. This is also because our visual memory is an effective tool for remembering language.

In a memory experiment in 1960, college students were presented with 612 magazine ads and then shown a new collection of 612 images, and were asked to pick out the original images. The students were able to pick out the original images with 98.5 percent accuracy.

Evidently, anchoring an image to a word, even if it's an _unrelated_ image, makes the word easier to remember.

So the next time you meet someone named Edward, take a visual cue from the famous Johnny Depp movie and remember his name by imagining him with scissors for hands.

> _"Neurons that fire together wire together."_

### 3. When it comes to memory, recalling is better than reviewing. 

We all know that awful feeling of cramming for an exam. We go over the material until our heads feel ready to explode and then hopefully, we pass. But how much of what we learned do we remember after a week? A month? A year?

The truth is your memory is worse than you think and increasing repetition isn't the best way.

Take German psychologist Hermann Ebbinghaus, for example. In the nineteenth century, he devoted several years to memorizing lists of nonsensical syllables like _Nish_, _Mip_ and _Zhik_. He compared how long it took to learn a list with how long it took to relearn the same list later and discovered the "forgetting curve." He found that we can recall about 30 percent after 24 hours but only around 10 percent a year later.

But what about just repeating again and again what we studied? Well, this is _overlearning_, and experiments have shown that it's pretty useless for long term memory.

So what can we do? We can use the act of _recalling_. When you recall, or retrieve information from the past, this tells your brain that something is important to remember.

In one study, participants were given $20 for every Spanish word from a list that they could remember a week later. They could either study the list for ten minutes, or trade the list after five minutes for a piece of paper and a pencil and write down whatever they still remembered, and then hand the paper back after five minutes.

The participants who opted for writing remembered about 35 percent more words than the others a week later. Because they had to _recall_ the Spanish words, they were much better at remembering them.

More good news is that neuroscience has shown that each time you recall a memory, your brain is rewarded with a small release of the hormone dopamine, which makes you feel good, and encourages additional memory storage.

> _"I've heard that hard work never killed anyone, but I say why take the chance?" — Ronald Reagan_

### 4. Spaced repetition systems (SRS) make memorization more effective. 

We know that our long term memory doesn't exactly perform at the level we want, so what can we do about it? We can use good timing. When acquiring a new language, timing is everything.

Surprisingly, the more difficult a word is to remember, the longer it will stick around in your long term memory.

Take the following scenario: you run through a list of words until you understand them all. Then the following week you try to recall as many of them as you can.

Studies have shown that, after one week, you have a 20 percent higher chance of remembering the words that took a bit longer to recall and a 75 percent higher chance of recalling words that were really challenging to recall. But there is also a third category of words: the ones that were at the tip of your tongue right before you remembered them. These are the words you're about twice as likely to recall one week later.

The trick, then, is to recall a word at the moment when you are about to forget it.

The _spaced repetition system_ utilizes this knowledge to improve your long term memory. The spaced repetition system uses flash cards at different intervals and tells you which words to practice, and when.

Memory researchers discovered that one month is the ideal interval to recall a word so that you can remember it in the long term. Most spaced repetition systems use this interval, so you can learn the word today and be reminded of it in one month, just before you're due to forget it.

This system works brilliantly and can be used for words as well as grammatical concepts. For example, in only four months, you could memorize 3600 flash cards with about 90 to 95 percent accuracy.

### 5. Learning the sounds and pronunciation of the language is key. 

As adults, it feels like such a chore to remember new words in another language, yet children seem to pick them up effortlessly. Why?

Children learn languages by _listening,_ not by studying. If you really listen to a language, you will not only learn words, but grammar, too. This way is also more fun than poring over grammar books.

Start with sound. Learning the sounds of new words makes them easier to remember.

In one experiment by researchers at Stanford University and Carnegie Mellon, Japanese adults sat with headphones, in front of screens and were asked to press a button labeled "lock" when they heard the word "lock" and a button labeled "rock" when they heard the word "rock". As the Japanese language doesn't have an "L" sound, most Japanese speakers cannot detect the difference between L and R. Therefore, as expected, the participants performed poorly in this task.

How could they master English if they couldn't pick up the difference in sounds? To them, "rock" and "lock" would be written the same way.

But the experiment revealed something interesting. If the students were shown whether they were right or wrong by a sign on the screen every time they pushed a button, they learned to hear the difference after only three twenty-minute sessions.

Another advantage of you learning a language's sounds first is that it will help you learn grammar faster.

The classic "Wug" test verified these results. In the test, children are presented with a drawing of a bird-like figure and are told it's called a "Wug." They are then presented with two of them and the researchers say "Now there are two. There are two . . . ?" By the time children are around the age of five, they'll know that English forms plurals by adding "s" to the end of nouns and so will answer "Wugs."

If the children hadn't already understood the "s" sound, they wouldn't be able to learn this grammatical pattern.

### 6. Word games help build vocabulary. 

Okay, so learning sounds makes remembering easier. But there's still a problem. Once you know the sounds, how do you use words effectively while making sure they stay in your head? Simple. By playing word games.

A great game is _Spot the Difference_ and all you need is Google Images.

Type in any word and you'll get pages upon pages of images. But the images don't just show you one or two examples of what the word means. They show you all the subtleties and nuances of it. For instance, if you key in the German word _Schrank_, you'll find it has various meanings (e.g., cupboard, cabinet or closet). By noting its various meanings, the word becomes easier to learn.

The aim is to spot the difference between what you expected to see and what image is actually on the screen.

Every image on Google also comes with captions in 130 languages (be sure to use Google Images Basic Version) and therefore is an excellent tool for language learners. Since these images are taken from websites in the language you're trying to learn, you can also then see how these words are used in different contexts.

Another great activity is the memory game, which makes new words more meaningful. Here's how you play:

Take a word you're trying to learn and personalize it. If, for example, you're trying to learn the French word for grandmother (grandmère), you could recall the summers you spent at your grandmother's house when you were a child.

Even if you can't find a connection, the action of searching for a connection will still help you remember the word, even if the connection is simply "that stupid word I can never remember!"

### 7. Languages should be learned in the order children learn them. 

Imagine you had a device inside your head that could take sounds and other input and transform them into what we understand as language. Well, we already have one; it's called _the Language Machine_. But it's rather particular about the order it does certain things.

Every language has a unique order of development.

You would probably guess that English learners from different language backgrounds learn English in a different order. But that isn't the case. They follow the _exact same_ developmental stages as children who speak it as their native language. Linguists now know that this order is how the human brain picks up English.

Take the tenses of English verbs, for example. Children learn the -ing form of the main verb before they learn to add the verb _to be_. So they will say "He running" before they say "He _is_ running." Children also learn the irregular past before the regular ( _sang_ before _jumped_ ). And only once they've grasped this do they then start using the present third person (He _likes_ ice cream).

Bear in mind, though, that when we bombard our brains with too much information, our "language machine" breaks down. The problem is, many learners start with material that is far too difficult.

In order to pick up grammar in the best way, we should feed our brain what linguists refer to as _comprehensible input_, that is, input that it can understand. For example, if you hold a cookie in front of a toddler and ask "Do you want a cookie?" they know what you mean, even if they haven't heard the word "cookie" before.

Therefore, a good way to begin is to learn the simplest sentences from your grammar book. This way you'll consciously learn a grammar rule at the same time as giving your brain some comprehensible input.

### 8. Simple stories make grammar patterns easier. 

One incredibly frustrating part of learning a language is that nouns, verbs, adjectives and other words come in many different forms.

A lot of languages have complex patterns of word endings, with rules that are difficult to decipher and exceptions to those rules! But don't panic: there's a better and more fun way to learn them than by repeatedly wading through long lists of endings.

What you should do is create a simple story for every form of a word you want to learn. Let's say you're trying to learn the forms for the English verb _to be_. Traditional grammar books will often give you examples such as _I_ _am_ _a student_, _He_ _is_ _a student_, and so forth. Of course these are useful to some extent, but forming your own examples is far better, especially when you're dealing with irregular verbs. So how do you make your own examples?

By using the _Person-Action-Object_ (PAO) story technique. This technique is so effective that memory champions often enlist it in order to remember patterns.

Let's take German as an example. Every German noun takes a masculine, feminine or neuter form (der, die or das respectively) and numerous ways of forming the plural. But instead of trying to remember the gender and plural for each noun by repetition, you can assign an _action_ to each gender and an _object_ to each way of forming a plural.

For example, you could assign the verb "throw" to the masculine form (der) and the object "chair" to the way of forming a plural by adding an "e" to the end of the noun. So, if you're trying to learn "der Hund" (the dog) in German, which is masculine and has this way of forming a plural, the story or image in your head would be "Dog throws chair," which is far more likely to stick in your brain than the words or tenses independent of each other!

> _"[I] would rather decline two drinks than one German adjective." — Mark Twain_

### 9. Learn languages faster by avoiding translations. 

When we're learning a new language, our knee-jerk reaction is to translate all words into our native language to understand exactly what they mean. While that's tempting and seems sensible, dropping the need to translate is essential if you want to speed up your progress.

First, try using monolingual dictionaries, as doing so builds vocabulary faster.

Monolingual dictionaries are dictionaries in which each word is explained using the same language as the word you looked up. Aim to use these as soon as you can when you're learning a new language. As you read the definition in the target language, you'll not only learn the word you're looking up, but you'll be exposed to the new words that explain it. If you don't understand these words, look those up too! That way, every time you read a new definition, you'll absorb more grammar and more words.

Abstaining from constant translation will also make you fluent faster.

When speaking a new language, you'll often find yourself in situations in which a native speaker will tell you: _What you said is not wrong, but we just don't say that_. This is because grammar and vocabulary are only part of the puzzle. You might say something grammatically correct that just isn't naturally said in that language!

If you want to become fluent, you have to hone your listening skills. And the only way is to steer clear of translations.

For example, if you're watching a movie in the language you're trying to learn, turn off the subtitles. Even if you don't understand everything, your knowledge of the language will gradually broaden. You won't just widen your vocabulary, but you'll also understand how the words are used in context.

You can also try reading a book along with the audiobook. This is great at teaching you the rhythm of your new language.

> _"Translations strip the music out of words."_

### 10. Final summary 

The key message in this book:

**You can become fluent in a new language faster than you thought possible by enlisting the following techniques: using a spaced repetition system, connecting words to images, personalizing new words and sentences, and listening carefully for rhythm and sounds.**

Actionable advice:

**Backchain those long, tricky words!**

Trying to nail the German word _Rechtsschutzversicherungsgesellschaften_, but can't quite get your tongue around it? Backchain it! This is when you say the last phoneme first, then the last two phonemes, the last three, and so on. Learning pronunciation back-to-front makes even long, scary, words far easier to say.

**Suggested** **further** **reading:** ** _The Language Instinct_** **by Steven Pinker**

_The Language Instinct_ provides an in-depth look into the origins and intricacies of language, offering both a crash course in linguistics and linguistic anthropology along the way. By examining our knack for language, the book makes the case that the propensity for language learning is actually hardwired into our brains.

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Gabriel Wyner

Gabriel Wyner is a polyglot who discovered the key to rapid language learning. He learned German fluently in 14 weeks, French in five months, Russian in ten months, and is currently working on Hungarian and Japanese. An accomplished scholar, Wyner also holds degrees in engineering, vocal arts and opera. He runs the website fluent-forever.com.

