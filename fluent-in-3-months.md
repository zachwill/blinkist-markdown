---
id: 58559c5b1219060004019e0a
slug: fluent-in-3-months-en
published_date: 2016-12-21T00:00:00.000+00:00
author: Benny Lewis
title: Fluent In 3 Months
subtitle: Tips and Techniques to Help You Learn Any Language
main_color: FB6135
text_color: A64023
---

# Fluent In 3 Months

_Tips and Techniques to Help You Learn Any Language_

**Benny Lewis**

_Fluent In 3 Months_ (2014) is a guide to mastering any language in record time. These blinks are full of useful methods to help you learn a new language efficiently and effectively. They debunk traditional forms of language learning, offer actionable strategies to get you speaking today and will set you on the road to being multilingual.

---
### 1. What’s in it for me? Discover the lightning-quick way to learn a new language. 

Language is a critical part of our everyday lives, and quite a useful one too. Without it, you wouldn't be able to order a bagel, hold a meeting or catch up with friends. Just imagine how long it would take if you had to play charades every time you wanted to pass on information.

And yet, many of us only know one or a very small number of languages, which can leave us in tricky situations whenever we go overseas. Ordering a coffee or a pastry can take ten times as long when you are struggling to communicate with the person trying to serve you.

Luckily, there's a road to fluid communication in any number of new languages that doesn't involve years of hard work and dull grammar lessons. Learning new languages is actually pretty simple if you know how — and that's just what these blinks are about. They are a practical guide to learning languages quickly and easily, with tips and tricks on how to become fluent in a new language in a mere three months.

In these blinks, you'll discover

  * why you don't have to go to China to learn Chinese;

  * the pitfalls of dressing like a tourist; and

  * why acting like a chicken can improve your communication skills.

### 2. Language learning success means overcoming prevailing myths, ditching vague plans and setting realistic goals. 

Mastering any new skill is difficult, whether it's web design or roller skating. But the idea of learning a new language tends to fill people with absolute dread. Why?

For starters, there are plenty of language-learning myths that prevent lots of people from even attempting to learn a new language. One of these myths is that learning multiple languages is beyond the ability of some people; in other words, children of monolingual parents are simply not genetically suited to becoming polyglots. People often trot out this excuse when learning a new language proves difficult.

But while such ideas can be off-putting or discouraging, they are myths that buckle under scrutiny.

Just take societies across the world, from Canada to Switzerland, where speaking multiple languages is the norm. Surely, people in Switzerland don't have some special language-learning gene that other people lack.

Nonetheless, when people are continuously confronted with myths like this, they start to believe them. As a result, they give up before they've really tried. In this way, myths can derail your language learning goals — but so can vague plans.

A vague plan like, "I'm going to learn Spanish" is daunting. After all, learning a new language from scratch is a huge undertaking. One way around this is to set specific tasks that contribute to a realistic goal, thereby giving yourself a sense of accomplishment.

To do so, start by deciding how fluent you need to be. If your goal is to speak with native speakers without straining, aim to reach level B1 or A2 in three months. But if you just want to be able to order food in a restaurant, A1 is probably sufficient.

Once you've set your long-term goal, break it into steps. For example, you might make a commitment to yourself to spend at least two hours a day on language learning.

After you've overcome debilitating myths and have set realistic goals, you'll be ready to broaden your vocabulary. Next, you'll find out how to learn lots of new words — and fast.

### 3. Learn new words quickly with a couple of easy strategies. 

All languages contain, on average, half a million words. So it's no wonder that your climb toward fluency can feel like an insurmountable task. It's therefore important to remember that you can take it a few words at a time.

That being said, learning words quickly is still desirable, and one way to do so is the _keyword method_. This strategy is all about connecting a visual image to the word you're trying to learn.

Say you want to learn the word "gare," which is the French word for "station." You _could_ repeat it a hundred times until it's drilled into your head, but if you do that for every word, it'll be weeks before you can manage a single sentence! Not only that, but a few days later, you'll have forgotten half or more of what you learned.

A better way to remember _gare_ is to think of a funny or weird image that you'll personally never forget. Since _gare_ sounds like Garfield, you might imagine an overweight cartoon cat running around a train station in a suit, trying to make it to work on time. Such an image will help the word quickly stick in your mind.

Another strategy for learning words is _spaced repetition_, which involves learning difficult words first. Here's how it works:

Say you've got a list of words you want to learn, and you spend time trying to memorize them every day. If you start at the top of the list every day and work your way down, you'll likely end up remembering just the first words you study.

A better approach is to ditch your list in favor of a deck of flashcards. This way, every time you remember a word, you can move it to the bottom of the deck; conversely, when you can't remember a word, you can move it to the top.

Then, the next day, you can start with the most difficult words first.

### 4. Speed up the language-learning process by interacting with natives without leaving your city. 

Learning words is key to building fluency, but to learn a language, you also need to speak it. After all, you'll only get so far holed up with just your flashcards.

But what if you live in the United States and want to learn Chinese? Where should you go to practice your new language?

The answer is "nowhere." When learning a new language, staying at home is actually more beneficial than traveling abroad.

Why? If you go to another country you won't _just_ be focusing on the language; you'll be immersed in the culture and dealing with the stress of moving, finding a place to live and settling down.

In addition, when people go to another country they often end up meeting speakers of their own language rather than connecting with locals. As a result, even if you come armed with the best intentions, you could end up with nothing but English-speaking friends months after arriving in a new country.

It's much easier to focus on language learning from the comfort of your own home. Here are a few of the many ways to do it:

First, use hospitality exchange networks to offer couchsurfers a place to crash. By doing so, you'll meet people from all over the world with whom you can practice your budding language skills. Naturally, you'll want to seek out guests who speak the language that you're trying to learn.

Another way to speak a foreign language at home is to find people from abroad who live near you. You can even get a bit of practice by approaching people on the street, speaking their native language.

While this might sound daunting, most people like it when others attempt to have a conversation with them in their first language. Just take Moses McCormick, a polyglot who speaks around 50 languages with different degrees of fluency and often utilizes this strategy.

When Moses learned Spanish, he went to a shopping mall and approached people who appeared to be from Spain by looking for telltale signs, like a T-shirt emblazoned with the Spanish flag.

### 5. Speaking from the start is essential and even an absolute beginner can find ways to communicate. 

Say you want to become a basketball star and you start lifting weights to get in shape, but when you play a game with your friends, you still struggle to keep up. As a result, you commit to playing every day and, suddenly, you're making real progress.

Well, the same strategy can be applied to languages: to speed up the learning process you've got to dive in and start speaking from day one.

But how?

Start by accepting your current skill level and speak using your whole body; that is, use body language to fill in the gaps in your vocabulary. This is crucial because one of the most important aspects of "speaking"_is_ body language. While words are obviously important, they aren't the only factor in human communication.

For instance, say you're a monolingual American on vacation in a non-English-speaking country and you want to order dinner. Since you can't speak a word of the language, you resort to doing your best chicken impersonation for the waiter, who chuckles and brings you your meal.

In this way, body language can help you communicate, but it also plays another helpful role: as you speak with your body, you'll grow accustomed to the new culture and start picking up words from the people you interact with.

Another great strategy for overcoming missing words in your lexicon is to use _cheats_. These could be notes or lists that you rely on when your vocabulary fails you.

Think of them like a grocery list that reminds you of the things you know you need but are prone to forgetting. You might include basics like "Hi, how are you?" and "My name is . . ." Then, by making a study plan and working on these phrases before trying to speak with others, you'll be able to interact with native speakers and build on your skills right from the get-go.

But isn't grammar also important? It certainly is, and that's what we'll look at next.

### 6. Become conversational before moving on to more technical aspects. 

Just like learning how to ride a bike, when learning a new language, there's a point at which you need to ditch the training wheels. And, if you want to become fluent, you'll need to buckle down and learn grammar — but this should only happen _after_ you've become conversational.

In fact, it should never go the other way around; learning grammar _before_ you can have a simple conversation will only frustrate you. So, while language courses often introduce grammatical concepts early on, grammar is totally useless with no vocabulary.

For instance, when trying to learn German, you might spend endless hours studying the differences between the articles _der_, _die_ and _das_. But, even after tireless studying, you still might find that it all just feels like a jumble of words.

It's smarter to focus on grammar only after you've reached a conversational level, like A2 or B1, and feel that you're no longer progressing. At this point, you'll already know the basics of your chosen language and its grammar will make much more sense. You might even find it fun and interesting to fill in the gaps in your knowledge.

Then, once you're at this level, you can use exams to help yourself learn the more technical aspects of the language. While jumping into conversation and saving grammar tests for later might seem odd and contrary to what you're accustomed to, just think of it this way:

If you wanted to be a professional artist, you would start by practicing drawing, and only after developing a portfolio would you apply to art school to learn more challenging concepts. In the same way, when learning a language, you should only begin taking exams after you've been able to converse for a while.

After all, testing is a great tool to help you master technical aspects of a language, like grammar and pronunciation, but only when you're ready. When you decide you are, just choose a test that's one level higher than the one you're at and set a reasonable time frame to study for it.

### 7. Adapt to the culture, always keep practicing and master one language before taking on another. 

Have you ever tried to say a sentence in a foreign language, only to find yourself stuck thinking in your native tongue?

It's a common issue and it can be a huge pain when languages get confused in your brain. So, here's how to avoid it.

First, if you're in a country whose primary language you don't speak, try to blend in. By adopting the local look, you're less likely to be viewed as a foreigner and people won't address you in English right away. In turn, speaking less English will make you less likely to confuse words in your head.

For instance, if you're a man in Egypt, you might grow a mustache, wear a sweater over your T-shirt, don dark shoes instead of sneakers and leave your baseball cap at the hotel.

Another way to avoid language mix-ups is to learn proper pronunciation. Just take French; the language is spoken predominantly using the front of the mouth and keeping this in mind when learning French words will both improve your accent and distinguish the language from others you might learn later on.

So, if you decide to learn Spanish, which is spoken further back in the mouth, it'll be hard to say a French word by mistake since you'll be used to speaking the languages in completely different ways.

This strategy will help you put more languages under your belt, but it's wise to continually practice the foreign languages you've become fluent in and to avoid adding another before you've mastered the first.

A good rule of thumb is to only move on to another language after reaching level B2 in the first. At level B1 you'll probably still be uncomfortable speaking the language, which means you'll be more prone to letting it slip away, even dropping a whole level over a short period of time.

But the most important thing is to get out there and start speaking. Who knows, maybe you'll even make some new friends along the way!

> "_Language-learning has truly changed my life, and it opened up doors for me in ways I cannot even begin to describe."_

### 8. Final summary 

The key message in this book:

**Contrary to popular belief, learning a new language can actually be a fast and fun process** ** _if_** **you follow a few key pointers. You'll have a much easier time if you start speaking right away, save grammar for when you're already conversational, and avail yourself of the language-learning opportunities in your native country.**

Actionable advice:

**Start out with free courses and travel books.**

When learning a new language, it's a great idea to start by signing up for free courses. Sites like Duolingo.com are usually just as good as, or even better than, expensive online courses. Another good idea is to pick up travel books from Lonely Planet or Berlitz. These books offer phrases and words along with short grammatical overviews that are great when you're just getting started.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Fluent Forever_** **by Gabriel Wyner**

_Fluent Forever_ unlocks the secrets of how to get the most out of your memory, so you can learn languages faster than you ever thought possible. It teaches you how your memory works and the precise techniques you can use to remember more words, more accurately, in a way that's efficient and fun.
---

### Benny Lewis

Benny Lewis is an Irish author and polyglot with a background in education and electrical engineering. He speaks over a dozen languages including Mandarin Chinese and American Sign Language, has presented at two TEDx conferences and was named _National Geographic'_ s Traveler of the Year in 2013.

