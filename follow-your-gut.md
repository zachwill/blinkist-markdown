---
id: 584e99b93a41200004701651
slug: follow-your-gut-en
published_date: 2016-12-30T00:00:00.000+00:00
author: Rob Knight with Brendan Buhler
title: Follow Your Gut
subtitle: The Enormous Impact of Tiny Microbes
main_color: 20A29C
text_color: 187A76
---

# Follow Your Gut

_The Enormous Impact of Tiny Microbes_

**Rob Knight with Brendan Buhler**

_Follow Your Gut_ (2015) puts the world of microbes under the microscope, showing just how much influence the little things — in this case, bacteria — have on our life. The fact is, we're crawling with bacteria, both inside and out, and if we weren't, life wouldn't be so great. Bacteria serve many important functions, like keeping us happy and healthy. It's time to learn how to treat them well!

---
### 1. What’s in it for me? Cherish the microbes that live in the Petri dish that is you. 

We've all been told to follow our gut. But what if our gut is in fact controlled by beings out of our control? And what if those beings govern not only your gut but the majority of your body, affecting everything from the way you smell to your mood and overall health?

These beings — bacteria — are numerous. If you were to count all the people who've ever lived on earth, that number would still fall short of how many bacteria live in one centimeter of your lower colon. This microbial life affects us much more than most of us know, from our first day until our last. So, germaphobes, put on your hazmat gear, because we're going to dive into the pool of microbes that live in the human body.

In these blinks, you'll find out

  * why a C-section birth has negative consequences for microbial life;

  * what part microbes play in how we feel; and

  * how battling germs with antibiotics is putting our health at risk.

### 2. There are more microbial cells than human cells in your body and they’re essential to your health. 

Have you ever asked yourself, "What makes me human?" If you think it's the cells that compose your body, you might want to reconsider.

While your body does contain around 10 trillion human cells, it also contains 100 trillion _microbial_ cells.

These microbes are single-cell organisms, many of which are bacteria, and together they make up what is known as the _human microbiota_ _._ The genes of all these microbes are called the _human microbiome_.

It would be accurate to think of yourself as the host of an ecosystem that is made up of extremely diverse, unique microbial communities. All of us share about 99.99 percent of the same DNA, but when it comes to the microbes living in our guts, however, we share only about 10 percent.

In fact, the microbes in and around your body are so uniquely yours that scientists could use them to identify your computer mouse with approximately 90-percent accuracy.

On average, about 85 percent of the microbes on your hand are different to anyone else's, giving you what is known as a "_microbial fingerprint._ "

Nor are these microbes only on your hands; they're all over your body's skin as well as in your gut. But this shouldn't worry you. These microbes are hard at work and responsible for a wide range of essential tasks.

The majority of microbes dwell in your intestines, where they do extremely important work. In addition to processing your dietary fiber, they determine two important things: how many calories you extract from your food and how your medication affects you.

Your skin's microbes also do important work, like determining how attractive you are to mosquitoes. These skin microbes feed on our body's secretions, which are responsible for our sweaty odors. Depending on your own specific microbial make-up, this scent might be more or less attractive to mosquitoes.

So we're covered in microbes. But this wasn't always the case.

### 3. Since natural birth provides babies with important microbes, caesarean sections can put babies at risk. 

While hanging out in your mother's womb, your body was free of all microbes.

But this didn't mean you weren't being _affected_ by microbes. The microbes in a pregnant woman's gut actually change in order to extract more energy from food, ensuring that her child gets important nutrients.

However, your body ceases to be microbe-free the moment you're born. In fact, you pick up your very first microbes as you pass through the birth canal.

Scientists have even found evidence indicating that certain kinds of microbes, such as _lactobacilli_, start to proliferate in the genitals of pregnant women.

As the baby passes through the birth canal, a variety of microbes are transferred to it; these are believed to act as a protective layer.

Babies delivered via caesarean section, or C-section, don't receive these protective microbes. This is worrisome, especially when one considers that C-sections, which are easy to schedule and can reduce complications associated with a natural birth, are gaining worldwide popularity.

As a result, C-section babies pick up microbes that resemble those found on the mother's skin, instead of the microbes naturally designed for a child's birth.

More studies need to be conducted, but early evidence suggests that children delivered via C-section may be more susceptible to diseases related to microbes and the immune system, such as asthma, food allergies and even obesity.

There are ways around this, though. The author's wife required a C-section, and so, once the child was born, the new parents transferred the important microbes to their newborn.

Using sterile cotton swabs, they took samples from the mother's vagina and applied it to different areas where the microbes would have settled naturally, such as the mouth, skin and ears.

This method could ensure that all newborns get the microbes they need.

### 4. The influence of microbes extends to determining how much you weigh and how anxious you are. 

There are a million diet plans out there with different theories about how you might be able to shed some pounds by eating certain foods.

But a truly important factor in how much you weigh are the microbes in your gut.

In fact, scientists have proven that microbes can determine the weight of a mouse.

They did this by transferring different microbes to a variety of mice. When a slim mouse received the microbes of an obese mouse, it became obese as well. And when they transferred the microbes of a slim human to a mouse living with an obese mouse, it prevented the obese microbes from spreading and kept the mouse lean.

In addition to your weight, the microbes in your gut can even influence your anxiety levels and how your brain functions.

Yes, there actually is a connection between your gut and your brain, and it's called the _microbiome-gut-brain axis_.

To bring this connection into focus, consider people experiencing depression; depressive phases are commonly correlated with inflammation of the bowels.

This inflammation can cause the bacteria _oscillibacter_ to produce natural tranquilizers such as _gamma-Aminobutyric acid_, which is a neurotransmitter used in sleep supplements that can calm the brain, but can also lead to depression.

But the impact of gut microbes doesn't stop there; they can also help to prevent autism.

In an effort to get mice to display autistic behavior, scientists at CalTech isolated one molecule — _4-EPS_ — that was responsible for this behavior.

Mice injected with this molecule all showed autistic behavior, but when they were given a strain of the microbe _Bacteroides fragilis_, it not only reversed some symptoms but also repaired cognitive and gut problems.

While this hasn't been tested on human subjects, it is further evidence of the power of microbes and a promising lead as we continue to search for treatment to help people with autism.

### 5. Probiotics and prebiotics are two ways to improve your microbiome. 

With all the beneficial effects microbes have on our health, you might be wondering what you can do to help improve the work they do.

One way to help promote a healthy microbiome is through _probiotics_.

Probiotics contain strains of bacteria or live microorganisms that are often described as "good bacteria" or "helpful bacteria," but most of the time these bacteria are already present in your gut.

You'll find probiotics being offered in supplements, suppositories and foods such as natural yogurt. Some of these will only contain one strain of bacteria, while others will contain several.

The US Food and Drug Administration (FDA) hasn't approved the health claims these kinds of products make, so they are sold as food supplements.

But despite the lack of FDA approval, there are serious studies showing that probiotics can help children with diarrhea and adults with _irritable bowel syndrome_, or IBS.

So, in order to find out how your health might benefit best from probiotics, it's recommended that you do your research or check with your physician beforehand.

Another way to improve the function of your body's microbes is through _prebiotics_, which are basically foods that microbes love to eat.

Most prebiotics consist of dietary fibers such as _inulin_, or nutrients such as _lactulose_ or _galacto-oligosaccharides_, all of which are beneficial to bacteria.

While there is no universal definition for prebiotics, studies show that they generally have positive effects on constipation, insulin resistance, Crohn's disease and inflammatory bowel disease.

In a way, prebiotics help our bodies get back to the naturally positive effects a high fiber diet can have.

While our early ancestors had a good amount of fiber in their diet, we have drifted away from this in modern times. This is unfortunate, since most of our beneficial microbes thrive on the kind of fibers that allow for the production of short-chain fatty acids, such as _butyrate_, which are beneficial for your gut and your overall health.

### 6. The overuse of antibiotics can be harmful to our health and result in dangerous antibiotic resistance. 

Now that we know what our microbes like, let's look at what they hate: antibiotics.

Without a doubt, antibiotics are one of modern medicine's greatest inventions; they can be found everywhere and they're used to cure all sorts of bacterial diseases.

However, this ubiquity can have some unintentional, and unfortunate, side effects. Antibiotics are indiscriminate. When we use them to kill disease-causing bacteria, they also kill a lot of beneficial bacteria and severely damage our microbiota.

Studies show that administering antibiotic treatment to a person within the first six months of their life may disrupt the normal development of _Bifidobacterium_, an important part of a healthy immune system.

This can result in people being more likely to experience weight gain, asthma and allergies later in life.

And then there's the danger of creating antibiotic-resistant bacteria in your body, which can happen whenever you neglect to finish a course of antibiotic treatment.

So, even if you start feeling better before it's done, it's important to finish a prescribed course of antibiotics; otherwise, some bad bacteria will remain in your system and end up stronger and resistant.

Antibiotics are also a cause for concern in the world of agriculture.

While antibiotics are normally used to treat illnesses, in the 1950s, farmers discovered that animals receiving low doses of antibiotics were gaining weight faster. And quicker weight gain meant more money, since animals could be raised and sent to slaughter in less time.

But, again, this can lead to trouble; low doses of antibiotics fail to kill all the bacteria that a normal dosage would, causing the remaining bacteria to become stronger and resistant to antibiotics.

Farmers are now faced with diseases that are no longer cured by normal antibiotics, and, despite the fact that the EU has banned all low-dose antibiotics treatments, other countries still practice these methods.

Hopefully, the entire world will soon realize the benefits of good bacteria and the life-threatening danger of overusing antibiotics.

### 7. Final summary 

The key message in this book:

**Microbes are an important part of a healthy mind and body, and we can help them do their job by using probiotics, changing our diet and introducing more prebiotics. To ensure we maintain a healthy community of microbes in our gut, we must also be wary of antibiotics and be conscious of the effects they have on our microbiome and the food we eat.**

Actionable advice

**Learn more about your own microbiome.**

The American Gut project encourages people from various countries to analyze their microbiome. Due to advances in DNA sequencing, the price of this has dropped to only $99, with proceeds going to further research. After your donation, they'll send you a sampling kit and then you're on your way to finding out what kind of community is living inside of you! 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Gut_** **by Giulia Enders**

_Gut_ (2015) takes an entertaining yet scientific look at an organ that is just as interesting and important as the brain — the gut. By tracking a piece of cake as it works its way through the digestive system, you'll come to appreciate the gut for the sophisticated and impressive ecosystem that it is.
---

### Rob Knight with Brendan Buhler

Rob Knight is a professor at UC San Diego's Department of Pediatrics and Department of Computer Science. He is also a senior editor at the _ISME Journal_ and co-founder of the American Gut Project.

Brendan Buhler is an award-winning science writer who has been featured in _Sierra_ magazine, the _Los Angeles Times_ and _California_ magazine. His story on biologist Rob Knight was selected for the 2012 edition of _The Best American Science and Nature Writing._

