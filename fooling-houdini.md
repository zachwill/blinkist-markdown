---
id: 560923818a14e60009000019
slug: fooling-houdini-en
published_date: 2015-09-28T00:00:00.000+00:00
author: Alex Stone
title: Fooling Houdini
subtitle: Adventures in the World of Magic
main_color: F93D32
text_color: C73128
---

# Fooling Houdini

_Adventures in the World of Magic_

**Alex Stone**

Alex Stone is a magic-enthusiastic and Ph.D. candidate in Physics at Columbia University. He's written for several magazines and newspapers. _Fooling Houdini_ is his first bestselling book.

---
### 1. What’s in it for me? Discover what science and magic have in common and what they could learn from one another. 

If you were like most other children, you probably loved magic. Whether it's a friend's new card trick or a birthday magician with a hat full of rabbits, there is something mesmerizing about magic.

As we grow up, many of us lose that sense of wonder, because we learn that — spoiler alert! — there is no "real" magic. It's all just tricks and illusions, and we come to assume that reason and science are completely different from magic. As these blinks will show you, however, magic and science not only have a lot in common; they also have a lot to learn from one another.

In these blinks, you'll learn

  * how you can have a gorilla right in front of you without noticing it;

  * whence "hocus-pocus" derives; and

  * why children are harder to fool than adults.

### 2. Magic has a lot in common with gambling and business. 

What is _magic_, exactly? Entertainment? Deception? Well, it's many things. Some of magic's characteristics are intuitive; others you might find surprising.

Magic is closely connected to gambling, for instance. A lot of skilled magicians have used their talent to gain a fortune through gambling, and many sleight-of-hand card tricks originate from gambling, as they were used in that field first (often illegally!).

In both gambling and magic, it's vital that you don't reveal your secrets or all of your skills at once. If you're too open about your trade, your audience will grow skeptical or lose interest.

Magicians and gamblers both have to see the world the way con artists do. Con artists look for the secret workings behind any game, be it politics, Wall Street or everyday life. They fool people; they trick them. Similarly, magicians also seek out such secrets and use them to their advantage.

There's a connection between finance and magic, too. Finance, gambling and magic have something significant in common: they all require a deep understanding of risk. They also all encourage you to bluff and throw off your opponents — and they're all partly based on luck.

Succeeding in magic (and in finance and gambling!) is largely about staying ahead of what others are thinking. You have to let them _think_ they know something. Then they'll want to prove they've understood, a typically human urge.

Magicians employ a lot of tricks to help with this. Some of these, like the _ego hook_, are more common than others.

The ego hook refers to the strategy of catering to people's egos by making them feel smarter than they really are. It's like tricking someone into thinking they should go for a particular investment because it's "safe."

### 3. Magic isn't about secrecy – it's about deception. 

You might be familiar with the Masked Magician, the first person to explain magic tricks on television. Giving away secrets like this might seem bad for the art, but, actually, it isn't — it can even help strengthen it.

A lot of prominent magicians live in constant fear of having their secrets revealed. Magic tricks are like quantum states: if you examine them too closely, you destroy them.

You have to keep your secrets safe. If the audience uncovers them, the magic disappears. So why is it so easy to research or buy new tricks?

There are standards in the magic world aimed at keeping the profession safe. Magic would go stale, however, if it was kept _completely_ secret; magic progresses when people share secrets, as it pushes them to develop new tricks when others become common knowledge.

That's why it's not unusual for magicians to share secrets, not only with each other, but outside the community as well.

Exposure pushes the art form forward. Nearly every classic trick has been exposed several times, creating the demand for new tricks. But this doesn't mean that old tricks aren't still important. After all, magic isn't about secrecy — it's about deception.

Consider the trick that fooled Houdini. At the height of his career, Houdini proclaimed that no one could fool him with the same trick three times. So in 1922, Dai Vernon found Houdini and performed his _ambitious card routine_ : Houdini picked a card, put it back into the deck without showing it to Vernon, then Vernon shuffled the stack.

Somehow, Vernon was able to make the card float to the top of the deck every time. The trick fooled Houdini seven times before he gave up.

There's a lot of writing available today about the ambitious trick, and experts still try to fool each other with new versions of it. It once stumped Houdini, but now you can learn it from a simple Google search.

### 4. Touch sensitivity is very important in magic tricks. 

Can you discern what something is just by touching it? This ability is called touch sensitivity, and it's a vital tool for magicians. When performing card tricks, they need to be able to arrange cards within a deck into a certain order without looking at them — and this happens in the few seconds that they seem to be casually shuffling them.

Let's explore touch sensitivity further:

We live in a visual world, but one group of people is particularly sensitive to touch: the blind.

Because of their heightened sensitivity, some blind people are particularly adept at magic. This has been known for a long time, though it's not well understood.

Research has shown that when people read braille, the part of the brain that processes tactile impressions lights up. So does the visual cortex. Indeed, blind people have another sort of visual sense: they "see" with their fingers.

Though blind people have naturally better touch sensitivity, anyone can learn to identify hundreds of small objects through touch alone.

This is because the human hand is one of the most significant evolutionary adaptations, almost on par with the brain. The hand is our main interface with the world, and the nerves on our fingertips are some of the most densely packed sensors in our bodies — that's why we can identify things so well with them.

Research has shown that when people lose their hands, it has a negative effect on their _spatial perception_, the ability to locate objects. That's because your brain locates objects based on their distance from your hands.

Magicians have to train their touch sensitivity in order to do card tricks well. But the kind of deck used makes a difference, too, because some are cut in a way that makes them easier to shuffle. Using a deck to perform tricks is like playing an instrument: you don't just train your fingers; you train the way you connect them to what you're playing.

### 5. Magic is taking advantage of the audience’s visual, tactile and verbal blindness. 

Have you ever heard of _visual blindness_? Even if you haven't, you've probably experienced it.

You don't truly _see_ something unless you're paying at least _some_ conscious attention to it. When you're not, you experience _inattentional blindness_.

To learn about this concept, the University of Illinois conducted an experiment that required research participants to watch a thirty-second film. In the film, people passed a ball back and forth. Before they watched it, the participants were told to count the number of passes.

The research participants were so focused on counting the passes that they didn't notice the strange thing in the film: a person dressed as a gorilla had walked through the middle of the game! They just didn't "see" the gorilla because they weren't paying attention to it.

Magicians play on this. They use tactile and verbal misdirections to take advantage of human blindnesses.

If a magician moves her hands in quick, back-and-forth motions around someone's wrist, she misdirects his tactile sense, allowing her to remove his watch without him perceiving it.

At times, we also experience _change blindness_. For example, an error occurs at the thirty-minute mark in _Pretty Woman_. Julia Roberts is eating a pancake; then, in the next shot, it's a croissant; and then it's a pancake again — a classic editing mistake. Few people notice this, however.

Change blindness is very useful for magicians. Try it yourself! Show someone a pair of cards, like the eight of spades and the nine of clubs, then put them back in the deck and shuffle. Next, show a different pair of cards on the top: the eight of clubs and the nine of spades. Most people will mistake the pairs and think you've done magic.

But remember: children have less focused attention than adults, because their brains haven't yet learned what to pay attention to. Fooling them is harder!

### 6. Magic can be emotional and frightening, but it still needs an element of comedy. 

Have you ever had someone read your fortune and conclude by telling you something so personal you thought it must be magic? That can be frightening!

_Mentalism_ — palm reading or fortune telling — is the branch of magic people tend to find the most emotional or scary. Mentalism often works by leveraging the _Barnum effect,_ whereby people assume that general things apply to them specifically.

Studies have shown that when people read general personality descriptions, they apply them to themselves, even if they could easily be applied to anyone. That's how horoscopes work; most people identify with simple statements like "There's something weighing on your mind."

You can do a pretty good cold reading of someone if you leverage the Barnum effect, do some demographic profiling and fish for clues. Houdini, for example, often visited the local cemeteries of the towns in which he performed, enabling him to mention names of the newly deceased, making it seem that he could channel their spirits. Cold readings like this often astonish or even frighten people.

However, a magic trick derives its power from the show around it. Magicians are performers; they're entertaining the audience by fooling them. Only a strong, tightly scripted and character driven performance can make a great magic show.

Entertainment also has to make you laugh — and this is why magicians have a lot in common with clowns. Instead of making fun of themselves or society at large, magicians make fun of the world's natural laws by appearing to break them. At its core, magic is comedic.

Magic and clowning have similar origins as well. The word "hocus-pocus" is a good example of this. It arose in the seventeenth century as a means of mocking the liturgy of the Eucharist, which contains the Latin words _Hoc est corpus meum_, meaning "this is my body."

So when you say the "magic words" — hocus-pocus — bodies turn into bread and rabbits spring out of hats. The term was originally a satirical re-appropriation of religious customs!

### 7. Magic has a lot in common with science. 

Science sometimes seems magical, and that's because — well — it is! Science and magic have a surprising amount in common.

People tend to regard magic and science as opposites: the former is based on superstition, the latter on rationality. But this binary sort of thinking is misguided.

For centuries, the line between science and what we would today call irrationality or "magic" was blurred. Newton and Gottfried Leibniz, the founders of modern science, were also interested in alchemy. Robert Böll, the founder of modern chemistry, spent half his life searching for the philosopher's stone.

Magic draws much from science, too. Magicians can use scientific discoveries (like mathematical principles) to create their tricks.

Just consider the _De Bruijn sequence_, named for the Dutch mathematician, Nicolaas Govert de Bruijn. It is a sequence of characters in which every possible subsequence of a given length appears only once. So if we say that _R_ stands for red cards, and _B_ for black, and every subsequence of three may only appear once, an eight-card combination could look like this: RRRBRBBB. Here the subsequences are RRR, RRB, RBR, BRB, RBB and BBB. Each appears only once.

How do you employ De Bruijin sequences in magic? Take three consecutive cards out of this eight card sequence, and, without looking at them, pass them out to the audience, and then ask everyone with a red card to stand up. You can now deduce what you have left from who stands up. If it's the first two people, you must have given out the cards RRB, which must leave you holding RRBBB. Though it's mere logical deduction, to the audience it seems as if you magically know which cards are in your hand.

Many magicians have also contributed to the sciences. The 19th century magician Jean Eugène Robert-Houdin, for example, pioneered experiments in electromagnetism and invented several devices to regulate electric currents.

Furthermore, most scientific discoveries arise from scientists experimenting and playing around. Leonardo da Vinci was obsessed with puzzles, games and tricks. Children learn through play, and magic offers adults a wonderful opportunity to return to an important, often innovative, mindset.

### 8. Final summary 

The key message in this book:

**Magic isn't just about doing neat tricks — it's about knowing your audience, training your senses and leveraging physical and mathematical principles. Its history and philosophy are closely linked to other fields and art forms, like science, finance and even gambling.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Alex Stone

_Fooling Houdini_ (2012) is about the author's personal journey to learning the art of magic. It outlines the roots and philosophy behind the world's most elusive art form — and explains its inextricable link with science.

