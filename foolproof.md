---
id: 56c1cd30587c820007000001
slug: foolproof-en
published_date: 2016-02-15T00:00:00.000+00:00
author: Greg Ip
title: Foolproof
subtitle: Why Safety Can Be Dangerous and How Danger Makes Us Safe
main_color: 56B5E7
text_color: 306682
---

# Foolproof

_Why Safety Can Be Dangerous and How Danger Makes Us Safe_

**Greg Ip**

Why do one third of Americans fear flying? After all, statistics show that you're 1,330 times more likely to die in a car accident than a plane crash. Are we taking the wrong measures to truly stay safe? _Foolproof_ (2015) explains why taking excessive precautions against danger can have terrible consequences, and why sometimes when we feel most in danger, we are actually quite safe.

---
### 1. What’s in it for me? Learn how to foolproof your life. 

We've all heard the story of how greedy banks, a rapacious financial sector and the lack of regulation caused the financial crisis of 2007–2008. But what if that's not the whole story? What if it was actually the measures taken by governments to keep the economy safe which caused the crisis?

As these blinks will show you, many of the things we do to make ourselves safer actually end up making us less safe. This is because they can lull us into a false sense of security. And then, when we least expect it, disaster strikes.

So what should we do instead? 

In these blinks, you'll learn

  * how helmets actually made football more dangerous;

  * how an economic crisis helped Thailand; and

  * why pilots need space.

### 2. Sometimes, the safer we feel, the more we’re actually in danger. 

Although safety regulations are put in place to protect us, they can sometimes do quite the opposite. How can this be?

When we make risky activities safer, we engage in them more often. Take driving a car, for example. In the late 1970s, anti-lock brakes were introduced in Germany to improve control over the car while braking. The government expected that this new safety mechanism would decrease the rate of fatal automobile accidents by 10 to 15 percent. Soon after, though, a study found that drivers in cars fitted with anti-lock brakes were more likely to engage in risky driving, such as driving faster and braking harder than drivers with no anti-lock brakes.

Further research found that, as drivers were placing a little too much faith in their new-fangled brakes, they were rounding curves more quickly, which increased the rate of rollovers and accidents when exiting roads.

A similar thing happened in American football. When helmets became mandatory attire in American football in 1943, the overall risk of injury was expected to go down. On the one hand, the helmets decreased the amount of broken jaws, teeth and noses. However, spinal and concussion-related injuries actually increased, with more than a 400 percent increase in broken necks.

The reason behind these disturbing statistics was said to be that, as the players felt more shielded, they began using their helmets as battering rams against the opposition!

The same happened in ice hockey too; when helmets were made mandatory in 1979, the prevalence of head fractures decreased while spinal injuries went up.

### 3. Although stability can make us feel safe, it can be deceptive. 

Just as helmets made football and hockey more dangerous, the introduction of safety measures to stabilize our economies inadvertently helped cause the financial crisis that began in the late 2000s. But how exactly?

In an attempt to deal with economic instability decades earlier, the US Federal Reserve averted a recession and, in doing so, laid the foundations for the 2007–2008 financial crisis.

At the start of the 1980s, the Fed began developing a means of dealing with economic instability, namely by regulating the banks. It was said that if the banks were stable, so too was the economy.

However, through these actions, Fed Chief Paul Volcker inadvertently encouraged _shadow banking_ mechanisms in the form of mortgage companies, investment funds and other financial institutions that were less closely regulated. So, by 2007, only 20 percent of US household and business credit was supplied by traditional banks, compared to 46 percent in 1979. These shadow-banking institutions made up the difference.

Even with the knowledge of historically elevated household debt, and hundreds of thousands of overvalued homes, the majority of observers believed that an ostensibly less risky banking system meant that the chance of a crisis was slim. But this illusion of safety paved the way for an increase in risk-taking that led to the 2008 financial crisis.

Similar consequences hit Europe with the introduction of the euro. The euro did indeed help its members avoid financial crises and encouraged economic stability, but unfortunately it was this stability that led to the European debt crisis in 2009.

Before the euro was introduced, the continent was rife with high inflation and speculative currency trading. The solution? One currency. 

As wealthier countries like Germany were no longer concerned about currency devaluation, they supplied their southern counterparts with billions in loans. However, this increased borrowing enabled countries like Greece and Spain to turn a blind eye to their domestic financial problems until 2009, when they were forced to face them.

### 4. It’s human behavior, not Mother Nature, that increases the damage inflicted by disasters. 

As we've seen, the preventive measures we put in place can bring about unforeseen trouble, even disaster. And this doesn't just apply to the world of finance. 

In an attempt to avoid natural disasters, we often end up exacerbating the damage caused by future disasters. Take forest fires. The advent of forest management undoubtedly helped save lives and extinguish fires. However, it's also one reason why forest fires are more extreme than they used to be.

Regularly extinguishing minor fires causes larger fires to be more hazardous since more leaves, branches and other dead foliage accumulate on the forest floor. In 2009, for example, a violent fire tore through the Australian state of Victoria, burning down thousands of homes and killing 173 people. Politicians and the media pointed the finger at climate change, but political scientist Robert Pielke Jr. thought the problem lay elsewhere.

Pielke researched a similarly destructive blaze in 1967 and concluded that, although climate change was involved, it was the decision to build and live in wooded areas that were susceptible to fires that caused the most damage. Although the 1967 fire destroyed only half the amount of homes, it would've been just as destructive as the 2009 fire had the region been as densely populated as it was in the sixties. 

By building up economic wealth in disaster-prone areas, it follows that the price we pay for future disasters will rise, too. Take the Great Miami Hurricane of 1926. At the time, the city had only around 100,000 inhabitants. After the hurricane hit, it cost the area $1 billion in today's terms.

Experts say that if a storm of a similar nature were to strike Miami today, the five million people who now reside in its metropolitan area would be faced with close to $188 billion in damages.

### 5. It’s often better to accept the risk of disasters or crises than to try to preempt them. 

Just as it's better to let smaller fires burn out on their own to avoid flammable forest debris building up, experts assert that it may do more good in the long term to accept that systemic risk is a part of life.

Moreover, there are actually benefits to doing nothing at all to prevent crises.

One group of scholars demonstrated this with research looking at the economies of Thailand and India between 1980 and 2002. Whereas India's economy was tightly controlled, Thailand thrived on barrier-free foreign investment and a largely privately owned banking system.

The result? The Thai economy grew too fast, borrowed too much, and fell into crisis. However, it still came out on top compared to India: its GDP per capita increased by 162 percent compared with India's 114 percent. The researchers concluded that combining free-flowing foreign capital with low interest rates in developing economies can still sometimes be the right way to go, despite the financial crises that invariably follow.

Perhaps it's better to resign ourselves to living with the chance of major disasters, because by reducing the risk of them occurring, we sometimes wind up elevating the risk of more frequent minor disasters. Nuclear power is a case in point. Although the idea of a nuclear meltdown conjures up horrific dystopian images in many people's minds, nuclear energy is actually a much safer source of power than burning coal or natural gas.

Take the following statistic: NASA experts estimate that between 1971 and 2009, 1.84 million deaths were prevented thanks to nuclear power. By deciding to slowly kill off nuclear power, countries like Japan, Germany and Switzerland will cause thousands of deaths owing to pollution when they return to fossil fuel. They will also be once more contributing to global warming.

### 6. Sometimes the more in danger we feel, the safer we actually are. 

Was Shakespeare right when he had Ophelia say to Hamlet, "the best safety lies in fear"? Could living in fear be the answer to protecting ourselves?

Most people fear activities like nuclear power generation and air travel, because they feel _inherently dangerous_ : when things go wrong, disaster ensues and lives are lost.

These fear are mostly irrational: an average American is 1330 times more likely to die in a traffic accident than in an airplane crash. Nevertheless, these fears have a positive effect: they drive us to take tremendous precautions to avoid disaster in inherently dangerous activities, thereby making them safer. For instance, one third of American adults have a fear of flying, and this has resulted in the current zero tolerance policy for risks of any kind when it comes to aviation.

In 1982, for example, British Airways Flight 9 was en route from Kuala Lumpur to Auckland when its engines failed. As the plane hurtled toward the ground, the crew managed to make an emergency landing in Jakarta. Investigators of the incident soon realized that the culprit was volcanic ash. Due to our zero tolerance for risks when it comes to aviation, since then, flying near volcanic eruptions is no longer permitted, as Europe was reminded in 2010 with the eruption of Iceland's Eyjafjallajokull. While the resulting flight disruptions cost an estimated $4.7 billion, not one person was injured.

Risk management systems such as a zero-tolerance strategy are highly effective at avoiding catastrophe. Such an approach could have prevented Exxon Mobil's Valdez oil spill disaster of 1989. But the company learned their lesson: after the disaster, they put a new system into action named OIMS, or Operations Integrity Management System. This encourages all employees to report every possible safety risk that they come across, including minor details such as an employee not holding onto the handrail when going down the stairs.

In addition, later in 2005, when beginning a new deepwater drilling project, they encountered a pressure problem that could have resulted in catastrophe. Using the knowledge from Valdez and their OIMS culture, they resolved to abandon the $187m project rather than risk the chance of another disaster.

### 7. We have to strike a balance between limiting danger and accepting the inherent risks we encounter in today’s world. 

We've seen how trying to foolproof our existence can make us engage in dangerous behavior, and that we sometimes feel secure when we're really at risk. But no one wants to feel constantly in danger, so it makes sense to opt for a balance between risk and security.

If an activity is inherently very risky, sometimes not engaging in it at all is better than trying to make it safe. This is illustrated by the zero-tolerance attitude to volcanic ash in air travel, but also in the world of finance as exhibited by the measures Toronto Dominion Bank (TD) took in 2005 to effectively eliminate risks from their business practices. As was standard practice at the time, TD was more concerned with buying up stocks, bonds and derivatives rather than traditional lending. But as it dawned on their CEO that the success of this practice relied on its inherent riskiness, he decided to cease trading in derivatives. In retrospect, this was smart thinking, as the bank, along with the Canadian banking sector, was only slightly harmed by the 2007–2008 financial collapse.

If you must foolproof, though, the most important notion to bear in mind is _space_. This is especially pertinent when it comes to natural disasters.

The Australian government, for instance, ensures that houses constructed close to areas at risk for fire are built far enough away from the bush to maintain a buffer of _defensible space_. In this way, they don't rely so heavily on frequent fire suppression, which, as mentioned earlier, can lead to more damage.

Using space in this way is also relevant for air travel. Pilots adhere to strict rules of space once they are at cruising altitude. That is, they always keep a distance of 1,000 feet vertically and three miles laterally from any other aircraft. So if they encounter turbulence, the risk of collision is far lower.

### 8. Final summary 

The key message in this book:

**Sometimes we can be lulled into such a false sense of security that we start to take more risks. When this happens, we put ourselves in harm's way. Conversely, activities that we perceive as dangerous can sometimes be much safer than we think.**

Actionable advice:

**One way to stay safe is to utilize the concept of space** ** _._**

Just as airplanes are required to cruise with at least 1,000 feet of clearance between them and any other aircraft, the same principle can be applied when driving your car. By always allowing a generous buffer zone between you and all other vehicles, you're less likely to be involved in a car accident.

**Suggested** **further** **reading:** ** _Emergency_** **by Neil Strauss**

_Emergency_ is the personal story of the author's transformation from a helpless urban dweller to an independent survivalist. It's a first-hand account of how to train for complete autonomy should society as we know it collapse.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Greg Ip

Greg Ip is an award-winning reporter and chief economics commentator for the _Wall Street Journal_. He is the author of _The Little Book of Economics._

