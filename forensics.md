---
id: 562498fb63613100070f0000
slug: forensics-en
published_date: 2015-10-22T00:00:00.000+00:00
author: Val McDermid
title: Forensics
subtitle: What Bugs, Burns, Prints, DNA, and More Tell Us About Crime
main_color: FFF233
text_color: 666114
---

# Forensics

_What Bugs, Burns, Prints, DNA, and More Tell Us About Crime_

**Val McDermid**

_Forensics_ (2014) provides an inside look at the morbid world of forensic investigation. Filled with fascinating history and anecdotes from real criminal cases, _Forensics_ gives you a complete, compelling overview of everything that happens during the investigation of a crime scene.

---
### 1. What’s in it for me? Discover how the evolution and power of forensics plays a crucial role in solving crime. 

Most of us, at some point, have either watched or at least flicked through some sort of series featuring forensics, be it CSI or something else. But have you ever thought about the actual forensic process and how it came to be what it is today? 

Ever since Sherlock Holmes used forensic evidence to solve crimes, we have been intrigued by the subject. And yet, most of us don't know that much about how it actually works. _Forensics_ is an in-depth exploration of the field's history — its different specialists and experts and the methods that they use.

In these blinks, you'll discover

  * how four bones led to the conviction of the "sausage king" of Chicago;

  * that blowflies can help determine time of death; and

  * how Japanese pottery inspired one of forensics most well-known methods.

### 2. For 100 years, crime scene investigators have been securing forensic evidence. 

If you spend any time in front of the TV, then you know that there's no shortage of detective shows out there. _Crime Scene Investigators,_ or CSIs _,_ have become an integral part of today's pop culture. But forensic science isn't just a made-for-TV phenomenon.

Investigators of real crime scenes have been helping police collect and interpret evidence since the nineteenth century, when evidence-based legal proceedings were established. However, forensic techniques remained quite rudimentary until Edmond Locard came along in the early 1900s.

Locard established the world's first crime scene investigation laboratory in 1910 in Lyon, France. Inspired by the fictional Sherlock Holmes, Locard's most influential contribution to forensic science is the aptly-named _Locard Principle_, which states: "Every contact leaves a trace."

But the principles of modern crime scene analysis were established later, in 1931, when Frances Glessner Lee founded the Harvard School of Legal Medicine. There, she created the _Nutshell Studies of Unexplained Deaths_, a series of intricate dollhouse-sized crime scenes that serve as a pedagogical tool for forensics students. 

But what do CSIs actually do? 

Once a police detective has secured a crime scene, it's time for the CSI to get to work. She starts with an initial inspection of the scene, collecting _inceptive evidence_, that is, clearly recognizable evidence, like weapons or a bloody fingerprint. This is done very carefully; full-body suits, gloves and hairnets are worn, in order to keep the scene uncontaminated.

The crime scene is then photographed from floor to ceiling and corner to corner. Every possible piece of evidence is bagged and clearly documented for a secure journey from crime scene to courtroom.

It's crucial that CSIs work quickly to ensure that the most compelling evidence makes it to the courtroom. Even if a prime suspect is apprehended, police can't keep them forever, so identifying inceptive evidence quickly is paramount.

In fact, speed is so important that modern CSI experts are considering introducing mobile investigation labs and mobile technology that would speed up the investigative process.

### 3. Fire scene investigators face a unique set of challenges that separate them from standard CSIs. 

Ever since the Great Fire of London of 1666, during which a fire at a baker's shop resulted in a conflagration that incinerated over 13,000 homes, there has been a need for _fire scene investigators_. But how do these investigators find the source of something as evidence-obliterating as fire?

A proper fire scene investigation starts from the outside and moves inward. After the firefighters have stopped the fire and a structural engineer gives the go-ahead, a fire scene investigator arrives to interview witnesses, photograph the scene and scrutinize the outside of the building for evidence.

Once inside, the investigator then approaches the fire like an archeological dig. In order to find the point where the fire originated, he needs to work from the least damaged area and move inward, toward the most damaged. This isn't only the most effective way to find the fire's origin; it also minimizes the amount of possible cross-contamination.

Some of the most common sources of ignition are faulty electrical wiring or matches left in fuel, like newspaper or a garbage bin. The diatoms found on the head of a match can actually outlive the effects of a fire and, much like the unique additives in gasoline, can be identified to pinpoint a specific brand.

Fire scene investigators can often find evidence of crime. Sometimes, however, the evidence simply goes up in flames. 

While structural fires can reach up to 1,100°C, they usually don't stay hot enough long enough to destroy evidence of foul play. But that's not always the case, as the 1981 Stardust Disco fire in Dublin makes evident. The fire killed 48 people and injured 240 others, and became a landmark case. After 25 years of inquiry, the case was still unsolved, and remains so today.

Even with 800 witnesses and teams of forensic investigators, there wasn't enough evidence to solve the mystery.

### 4. Entomology, the study of insects, can prove crucial in determining times of death. 

The idea of inspecting insects found on or in a corpse may be nauseating. But scrutinizing creepy-crawlies is in fact an ancient forensic tool. 

The first recorded example of _forensic entomology_ dates back to 1247, when Chinese official Song Ci produced a coroner's handbook.

Then, in 1893 Jean Pierre Mégnin published _Fauna of Corpses_, which recorded the different species of insects that are attracted to corpses, and in which order and stage of decomposition these insects appear.

Finally, in 1986 Ken Smith wrote _A Manual of Forensic Entomology_, which offered an even more in-depth account of how to determine a person's time of death based on the insects found on the corpse.

Forensic entomology is often used to determine the time of death if it's obvious that the person has been dead for more than 72 hours. 

The gold standard in forensic entomology is the blowfly, which is the insect that colonizes corpses the quickest, as it can detect the slightest traces of blood from over 100 meters away.

After a blowfly lays its eggs, it takes approximately 15 days for the maggots to transform into flies. Knowing this, scientists can examine the development of the maggots to determine when the blowfly laid its eggs, and thus when the person died. 

After this 15-day period, beetles arrive and remove whatever flesh remains on the body. Finally, moths and mites remove the hair, leaving behind a bare skeleton.

Of course, other factors, such as weather and the environment, aid decomposition. But when forensic experts combine all these variables, they're left with some powerful evidence.

For example, in 1935, Dr. Buck Ruxton was convicted of killing his wife and maid. The bodies, which had been cut into 70 pieces, were scattered across the UK. By analyzing the maggots found in these body pieces, entomologists solved the "jigsaw murders," the first UK case to be solved in this way.

### 5. Forensic pathologists’ examinations of corpses have a long legacy as a valuable forensic resource. 

The first recorded instance of forensic pathology dates back to Julius Caesar in 44 BC, and since then forensic pathology has been an indispensable tool in determining cause of death.

An autopsy, much like investigations of fires, starts outside and moves inward. Investigators start by collecting biological samples, like hair and fingernail scrapings, and then rigorously document every scar and physical feature before making a Y-shaped incision in the body to examine the organs.

They take samples from all major organs (to be microscopically analyzed by specialists) and then stitch the body back up, with the organs inside, in case a second autopsy needs to be conducted.

Once the experts, like neuro- and orthopedic pathologists, produce their reports, the coroner can then complete a final report.

Though effective, history has shown that forensic pathology isn't infallible. We can see this in the testimonies of the early twentieth-century forensic pathologist Bernard Spilsbury, who embraced the theatrical nature of the courtroom and became a very persuasive expert witness.

It is believed, however, that Spilsbury's prejudices influenced his testimonies, as was the case in 1923, when a soldier was almost put to death when Spilsbury failed to reveal some exonerating information. 

Forensic pathology aims to compensate for this by constantly developing new and improved methods of examination and analysis.

For example, hundreds of donated bodies, in many different settings, have been studied at the University of Tennessee's "Body Farm," giving us a better understanding of the factors that influence the decomposition of a corpse. 

In their studies, they've determined a number of interesting heuristics. For example, they found that one week of above-ground exposure is equal to eight weeks below the earth and two weeks in water.

They are also in the process of developing a new method of analysis, called _Decomposition Odor Analysis_, which estimates the time of death based on the 400 or so distinct vapors a body emits during decomposition.

### 6. Forensic toxicologists study one of the oldest and sneakiest murder weapons: poison. 

Throughout history, poison has been a choice murder weapon. It's up to forensic toxicologists to stay abreast of new lethal substances and be able to identify them and search for antidotes.

In fact, historically, toxicology has been as much about saving lives as convicting criminals.

For instance, in 1813, after years of testing poisons on thousands of dogs, Mathieu Orfila published the 1,300-page-long _General System of Toxicology; or, A Treatise on Poisons_, which provided the first encyclopedic catalogue of all known mineral, vegetable and animal poisons. 

Using what he learned, he published another book five years later on how to treat people who had been poisoned, making him the world's leading toxicologist.

Advancements in toxicology have led to other social benefits, too. Working conditions in factories improved, for instance, after Marie Curie discovered radium, polonium and thorium in 1898.

Toxicology also played a critical role in jurisprudence, evidenced by the success of the 1920s case of the "radium girls," female factory workers who were exposed to lethal levels of radium. Their case hinged on the advancements made in forensic toxicology and set a precedent: workers who contract occupational diseases can now sue their employers. 

Toxicology can also help reconstruct the circumstances around a suspicious death. For example, because of their knowledge of where concentrations of toxins can be found in the body, forensic toxicologists were able to gather enough evidence to convict Harold Shipman, aka "doctor death," who had murdered an estimated 210 of his patients. Knowing that the thigh muscle was the most stable tissue to examine, forensic toxicologists were able to discover lethal levels of morphine in his patients.

But there isn't always a happy ending. Between 1840 and 1850, there were 98 criminal poisoning trials in England and Wales, with arsenic being the poison of choice. But because arsenic poisoning resembles the slow deterioration caused by natural diseases, discerning it as a murder weapon wasn't easy.

### 7. Fingerprinting was one of the most groundbreaking forensic developments of all. 

While fingerprinting is ubiquitous in modern forensics, the story of how it came to be is quite curious.

It all started with Henry Faulds, a Scottish missionary in Tokyo who was inspired by the way Japanese potters marked their pots with finger imprints. He discovered that these subtle prints became visible when dusted with powder.

Faulds took this discovery to Charles Darwin, who passed it along to his cousin Francis Galton, who, in 1892, published the first book on the subject: _Finger Prints_.

Inspired by Galton, Argentine police officer Juan Vucetich began taking and cataloguing the fingerprints of men arrested in Buenos Aires. It was this sytem that resulted in the first-ever criminal conviction based on fingerprint evidence.

Word spread, and Edward Henry, the chief of police in Bengal, began adding thumbprints to all criminal records and introduced a fingerprint reference and filing system called the _Henry Classification System_.

In 1901, Scotland Yard made Henry the head of their Criminal Investigation Department, which, in his first year there, unveiled 632 criminals who had previously been using pseudonyms.

But as important and effective as fingerprinting is, it's also, like many other forensic techniques, imperfect.

There are two kinds of fingerprints: _patent_ prints, which are visible to the naked eye, and _latent_ prints, which can't be seen except with technological aid or with powders that make them visible.

The importance of this distinction was made clear after the Madrid train bombings of 2004, when one incomplete latent fingerprint led to 20 possible matches in the FBI database.

This latent print led the FBI to detain an Islamic suspect until the Spanish police, two weeks later, matched the fingerprint to the correct culprit. This wrongful detention resulted in a $2 million settlement, and served as a reminder that even fingerprint evidence can be skewed by contextual information, and must always be considered in combination with other forensic disciplines.

### 8. Analysis of bloodstains and DNA has revolutionized forensic science. 

Bloodstains do more than simply indicate that a crime was committed. They can actually tell us _what_ weapon was used and _where_ and _how_ the fatal blow was struck. It can also tell us _who_ did it.

Fittingly, the science of bloodstain analysis has some pretty bloody beginnings. In 1895, at the Institute for Forensic Medicine in Poland, Edward Piotrowski, in order to better understand blood patterns found at crime scenes, bashed rabbits on the head and had a painter document how the blood spattered. Using these paintings, he published the first paper on bloodstain patterns made from blows to the head.

But it wasn't until 1955, in the trial of Samuel Sheppard, that Piotrowski's findings were put to the test. Sheppard had been convicted of murdering his wife, but his conviction was overturned after testimony from a bloodstain analyst helped exonerate him.

In many cases, bloodstain analysis can shed more light on a crime than the pathologist's report. For example, a bloodstain analyst can determine where a fatal blow was made, and calculate the angle of impact by using the _stringing model_. By attaching a string to each of the stains and spooling them back, the strings will merge at the point where the blow was made, offering CSIs deeper insight into the nature of a struggle.

Blood is also useful because it contains DNA, which could belong to the perpetrator, the victim or a witness. 

The most significant advancement in DNA evidence came in 1999 with the development of _low copy number DNA profiling_. This new type of profiling required analysts to obtain a blood sample no larger than one millionth of a grain of salt, and has been used successfully in 21,000 serious cases, including many cold cases that would otherwise have gone unsolved.

But extracting DNA evidence isn't an infallible process, and has been proven to be susceptible to cross-contamination. As with any forensic evidence, DNA alone isn't enough to solve a case.

### 9. Forensic anthropologists – specialists in skeletal remains – are important in identifying the deceased. 

Could you identify your friend by looking at their skeleton? Probably not. When a skeleton is all that remains of a body, identification becomes extremely difficult — that is, unless you're a forensic anthropologist. 

Forensic anthropology was first put on the map for the general public in 1897, during the trial of the "sausage king" of Chicago, who murdered his wife and tried to dispose of the remains in an industrial sausage vat. Forensic anthropologists were able to identify four bones found at the crime scene as being from a woman's body. This evidence, along with the fact that his wife's wedding ring was also found at the scene, led to his conviction.

Since then, forensic anthropology has been used to catalogue and identify the skeletal remains of the dead. For example, during the Korean war, a database was created to catalogue the victims of war, which produced accurate predictions of height, weight and age, all based off skeletal remains.

More recently, the Argentine Forensic Anthropology Team has become the modern pioneers in forensic anthropology after the work they've done in trying to identify the tens of thousands of unidentified victims of the Argentine Dirty War of the 70s and 80s.

Surprising advancements in forensic anthropology have been made possible by modern technology. For example, Sue Black and the Center for Anatomy and Human Identification have successfully used photographs and video to convict a criminal by analyzing the individually unique vein patterns found on forearms and hands.

Then there's the advances made at Louisiana State University's Forensic Anthropology and Computer Enhancement Services, or FACES, which hopes to solve cases by merging the biological data of 600 missing person reports with the forensic details of 170 unidentified remains in a giant database.

And they've been successful: using skeletal analysis of a body found in the Gulf of Mexico, the FACES database was able to match it to a 65-year-old woman who had gone missing from Missouri in 1999.

### 10. Facial reconstruction technicians have developed methods for solving otherwise unsolvable mysteries. 

The first thing that comes to mind when trying to describe a person is usually that person's facial features. However, when someone dies, the body decays, causing them to lose these identifiable characteristics. Forensic scientists use _facial reconstruction_ to fit faces to the dead.

Facial reconstruction is based on an understanding of the 22 bones that make up the human skull; such anatomical expertise enables technicians to recreate an individual's face.

From the days of Leonardo Da Vinci, facial reconstruction has brought together art and science in a totally unique way. But the first properly scientific facial reconstruction was conducted on a Neolithic woman found in Switzerland in 1899, when Julius Kollmann sampled and measured the soft tissue thickness of numerous cadavers.

Later, in the 1950s, Mikhail Gerasimov developed the _Russian Model_ of facial reconstruction, a technique that took into account muscle structure, rather than tissue thickness, because the cartilage found in the nose and ears decomposes quickly after death. 

Today, with the help of X-rays, CT scans and computer programs, modern facial reconstruction is more accurate than ever. Computers are much quicker than humans at creating models, and are able to quickly create numerous iterations of a face (with different ages, hair patterns, and so on).

However, facial reconstruction is not strictly forensic, since it carries no weight in a courtroom. It is, however, one of the best ways to identify a body.

The first time facial reconstruction helped solve a crime was in the Netherlands in 2001, when a damaged skull was reconstructed to identify a five-year-old girl, leading to the arrest of her parents.

This is especially impressive considering that only one in six missing children is found due to someone recognizing a picture of the child, as their young age means their features are often unformed and indistinguishable.

### 11. Digital forensics, the analysis of computer evidence, is an evolving field that helps establish a crime’s timeline. 

When suspects are interrogated by police, they tell all kinds of stories, both true and false, to prove their innocence. But how can police discern which stories are true and which false? Today, digital forensics has proven crucial in establishing a timeline and validating or disproving an alibi.

In the 1980s, digital forensics was largely about fraudulent business activities. All that changed with the advent of Windows 95, which made it easy for criminals to use personal computers to commit crimes.

By the early 2000s, many police forces had established high-tech crime units, and with them came new rules and CSI techniques regarding the collection and storage of digital devices. For example, the magnetic brushes that were once used to lift fingerprints can destroy electronic devices. So today, these have been replaced with anti-static bags.

Moreover, digital information must be immediately preserved and diligently documented to prove that the evidence hasn't been tampered with.

Digital forensics is extremely good at pinpointing the location and time frame of criminal activities. Many of today's electronic devices come with GPS tracking, which can provide a digital footprint even after a phone has run out of battery or the user has disabled location tracking.

For example, the iPhone 5S is equipped with a specialized location chip that can run for up to four days off of reserve battery power. In addition, cell network providers can provide police with location data from transmitter towers, which can narrow down a person's possible whereabouts at a particular time.

Other files that might otherwise seem irrelevant at first, like digital photos or documents unrelated to a crime, contain all kinds of useful clues in their metadata, like time, GPS coordinates and the make and model of a device.

This is how police were able to apprehend fugitive John McAfee (of virus-scanning program fame) after a photograph of him surfaced containing the exact coordinates embedded in its metadata.

### 12. Forensic psychology is the science of making sense of crimes by getting inside the mind of the criminal. 

Sometimes an investigation dead-ends: all clues and leads go cold. At this point, investigators have to get creative. In such cases, they can bring in forensic psychologists, who, rather than identifying a particular suspect, build a profile of possible suspects and point investigators in the right direction.

Forensic psychology is a relatively recent addition to forensic science. While the first offender profile dates back to 1888 (in the case of Jack the Ripper), it didn't become a serious tool for the police until the conviction of George Metesky, the "mad bomber" of New York, who was captured after a profile created by psychiatrist Dr. James Brussel was released to the public in 1957.

In 1977, the FBI introduced profiling courses at their academy in Quantico, Virginia. Around the same time, law enforcement agencies worldwide were conducting interviews (upon which to base future psychological profiles) with serial killers and rapists.

By the mid-1980s, police forces around the world used offender profilers, and forensic psychology to revitalize what were once dead-end cases.

But how exactly does forensic psychology work? To get a better idea, we'll look at the 1986 case of the "railway killer" in London.

In addition to building a profile of the personality, lifestyle and habits of the criminal, psychologist David Canter was able to determine the approximate location of the killer's home by drawing a circle that connected the locations of the two crimes that had occurred farthest apart from each other; he then focused in on the center of that circle. Using this model, they were able to catch the killer.

Canter has since developed a computer program called the Dragnet, which uses relevant information to determine _hotspots_ — the areas most likely to be the home location of a killer.

### 13. Forensic evidence must overcome some serious challenges to be admissible in court. 

Finding the evidence is only half the battle. It takes an entirely different set of skills to present that evidence in court so that it will be intelligible to jurors. Unfortunately, lawyers sometimes present evidence in a manner that suits their purposes, rather than those intended by the forensic scientist.

Being able to provide good expert testimony in a courtroom is plagued by many challenges, the first of which being the _chain of custody_ of forensic evidence, that is, the path it took from the crime scene to the courtroom. If evidence wasn't handled with meticulous care and proper documentation showing all the steps, it could become inadmissible in court.

Moreover, cross-examination of forensic experts serves as both an opportunity to disprove an expert's opinion and as a form of character assassination that attempts to discredit an expert's reputation.

Ultimately, lawyers will only try to obtain the kind of expert testimony that suits the argument they are trying to make. This was evident in 1999, when faulty and biased expert testimony was presented regarding sudden infant death syndrome — testimony that ultimately resulted in the conviction of Sally Clark for the murder of her two children. 

However, after forensic evidence was discovered that had previously been withheld by the prosecution, this conviction was overturned. This successful appeal also led to a review of previous convictions under similar circumstances, thankfully resulting in the exoneration of two other mothers.

Though the system is far from perfect, the core process provides a high degree of scrutiny that strengthens forensic science.

Looking back to the anthropologist Sue Black: when she first tried to present vein pattern analysis evidence in court, the judge dismissed it. So, Black compiled more data and strengthened the science. And the next time vein analysis was entered as evidence, it led to the conviction of a pedophile who had videotaped his abuse of young girls.

### 14. Final summary 

The key message in this book:

**Forensic scientists are an important and creative team of experts whose contributions over the past 200 years have been critical for proper jurisprudence. From the early inspiration taken from Sherlock Holmes to the advancements in DNA and genetic fingerprinting, the leaps forensic science has taken are an amazing story.**

**Suggested** **further** **reading:** ** _What Every BODY is Saying_** **by Joe Navarro**

This book is all about the hidden meanings we can find in our body language. Our brains control our body movements without us being conscious of it, and sometimes those movements can be very revealing. This book goes into detail about the ways you can train yourself to become an expert in observing other people's nonverbal cues and uncovering their meaning.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Val McDermid

Val McDermid is an award-winning crime novelist who began her career as a bureau chief for a national UK tabloid. Her best-selling Tony Hill novels were the inspiration for the popular UK television series _Wire in the Blood_.

