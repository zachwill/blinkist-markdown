---
id: 5c87bb056cee0700077267ce
slug: founders-at-work-en
published_date: 2019-03-15T00:00:00.000+00:00
author: Jessica Livingston
title: Founders at Work
subtitle: Stories of Startups' Early Days
main_color: FFCB46
text_color: 806523
---

# Founders at Work

_Stories of Startups' Early Days_

**Jessica Livingston**

_Founders at Work_ (2007) is a revealing look at what went on in the early days of over 30 influential US startups. In their own words, the founders of landmark companies such as Hotmail and Blogger.com tell their stories about the many ups and downs and twists and turns it took to make their ideas a reality. They also share the lessons they learned and the insight they've gained looking back on the trials and tribulations of those chaotic early days.

---
### 1. What’s in it for me? Find out what the most successful startup founders have learned along the way. 

Back in the late 1990s, there was a gigantic startup boom that resulted in the bursting of the "Dot Com Bubble" in the early 2000s. A great many companies collapsed as funding dried up, but it wasn't long before a new wave of startups emerged, many of which were part of the Web 2.0 movement that focused on user-generated content.

Around 2006, author Jessica Livingston talked to the founders of many startups from the early days before the first Mac to early Web 2.0 companies like Flickr and Blogger. These casual conversations were transcribed for Livingston's book and focus less on exact dates and numbers, and more on the ups and downs and lessons learned from launching your own startup.

Although technology has changed in leaps and bounds since 2007, the stories of these pioneering entrepreneurs and the ideas that drove them are still highly relevant today. And while some of these businesses and technologies are the stuff of history, there's truth to the saying that we can't understand our future without first understanding our past.

In these blinks, you'll find

  * what completely different products Paypal and Flickr started out as;

  * how many millions Yahoo paid for a collection of website bookmarks in 2005; and

  * why investor money isn't always the best option.

### 2. Many startups, like Paypal and Blogger, didn’t end up with the same idea they started out with. 

In talking to the founders of numerous startups, the author found some interesting commonalities between some of the most successful of the past few decades. Among the most obvious is the fact that many big startups ended up with an idea or product very different than the one they started out with.

Take Paypal, for example. You know Paypal as one of the more popular ways to transfer money and make payments online, but co-founder Max Levchin didn't set out to help people do that.

In the late 1990s, Levchin was working on software for early handheld computer devices like the Palm Pilot. It all started when he created an emulator that generated single-use security passwords, which at the time, people had to buy bulky, expensive devices to do — or even several devices for different passwords and systems. Levchin's emulator software made all those devices obsolete, putting the technology right into your Palm Pilot.

But the market for this service wasn't that big around the turn of the millennium, so Levchin thought, what kind of information do people need to keep secure on their devices? The answer was credit card information, which then led to software that let you securely "beam" money from one Palm Pilot to another.

Soon, Levchin and his co-founder Peter Thiel were gaining around 300 users a day, but they maxed out at around 12,000 users. Far more popular was the online money transfer demo on their website. This demo had attracted around 1.5 million users, so it was clear where PayPal's attention needed to be: web-based money transfers. And once that shift was made, they were soon gaining 20,000 users a day, leading to Ebay eventually purchasing PayPal for $1.5 billion in 2002.

Blogger.com is another example of success coming in a different way than expected. Evan Williams and his co-founders started Pyra Labs in 1999 to make project management software. The blog was just one tool with which they were working.

But in working on that tool, they made it super easy for any person, from any computer, to log in, write and instantly publish their work for all to see. It had nothing to do with project management, and Williams nearly went broke in the process, but thanks to his determination and the generous fans who loved the software, Blogger.com ended up a success story. It gained a million users, started generating revenue and was acquired by Google in 2003.

### 3. Having an innovative idea can make it difficult for people to see the potential for success. 

Another commonality among many early startups was that their ideas weren't always so well-received, at least not initially. Innovative ideas can seem confusing or too tricky to understand for people, which is problematic if your idea needs funding to develop.

This was the problem for Steve Perlman and his WebTV idea. In 1995, Perlman was already a respected figure in Silicon Valley, having helped Apple develop the first Mac with a color display. Yet Perlman had difficulty selling his idea for interactive TVs that involved more than just changing channels.

At the time, TVs didn't even have program guides, and Perlman was finding there was something of a catch-22 problem: TVs didn't have interactive content because there was no hardware for it, and there was no hardware for it because there was no interactive content to prove there was a market for it.

Many people Perlman approached had no confidence that customers would want to do anything more interactive with their TV than changing the channel. He proved them wrong: after being bought by Microsoft, WebTV became MSNTV. In its first eight years in the market, the product generated $1.3 billion in revenue.

While interactive TV that you can pause, rewind and record may seem pretty basic nowadays, web-based email that you can access from any browser, seems like a no-brainer. But in 1996, most everyone accessed their email from work, where internal networks and firewalls prevented you from accessing it anywhere else. In fact, before Hotmail was founded that year by Sabeer Bhatia and Jack Smith, many people believed that email accounts were always going to be something you got from your employer.

Bhatia knew, however, that there would be more people like him, who'd benefit from being able to check their email from any web browser. Nevertheless, many investors shot down Bhatia and Smith's business plan until one was finally willing to give them just enough money to prove that they could indeed make a web-based email service.

It wasn't long before the idea was proven worthwhile. Thanks in part to word of mouth, and the clever idea of putting a link to the Hotmail site at the bottom of each email, they were gaining up to 5,000 new users every day. By the end of their first year, Hotmail boasted 7 million subscribers, and not long afterward, they were acquired by Microsoft for $400 million.

### 4. When launching a startup, a good team can be more important than a good idea. 

Not all startups began with a great idea. Sometimes they didn't have any idea at all!

This was certainly the case for Joe Kraus, co-founder of Excite, an early web search tool. In 1993, Kraus teamed up with five friends from Stanford University, and they proved that sometimes having an idea isn't as important as having a good team.

In fact, Kraus and his classmates had _no_ idea what their business would be, but Kraus wasn't worried because he knew his friends were all passionate and intelligent. It felt like just a matter of time before they found their way forward.

Sure enough, during a brainstorming session at their favorite taco shop, they realized that with so much information being published in digital formats, people were going to need a new way to search that digital info to find what they were looking for.

At first, they focused on searching things like digital encyclopedias, but their attention soon turned to the web. After showing that their technology could scale to search massive databases, they got $3 million in financing, and their new company, Excite, became the primary search tool for the dominant browser at the time: Netscape.

In 1996, another team was coming together without any idea. This one featured Arthur van Hoff and three others from the tech firm Sun Microsystems. They had all been involved in developing Sun's Java programing language and decided that they should each put in $25,000 and try their own startup business, despite a lack of ideas.

Van Hoff wasn't worried because he'd seen plenty of lousy ideas get funding, and he also knew that the first idea usually got scrapped in favor of a better second idea. In fact, having a team allowed them to pivot quickly from testing one idea to another before ending up with Marimba, a subscription-based software distribution model.

Nowadays, most software updates automatically, but at the time, it was pretty new to have a system that makes sure everyone receives their update at the same time. This is especially important for companies like the financial firm Morgan Stanley, with employees around the world. Imagine trying to contact 100,000 employees worldwide to make sure everyone installed their software update!

So rather than starting out by fixating on one idea, instead, make sure you have a flexible team that can pivot and run with the perfect idea when it presents itself.

### 5. Yahoo and deli.icio.us show how a personal project can become hugely valuable. 

The idea for Hotmail came when the founders were annoyed at not being able to access their email outside of the office firewall. Indeed, many startups began as solutions to personal problems, until the founders realized that millions of other people could also benefit from their idea.

The most famous example may be Yahoo, which started out as a collection of online footnotes in the form of web links for all the references being made in the PhD thesis papers of two Stanford grad students, Jerry Yang and David Filo. Quickly, the site grew, with new categories and links being added based on suggestions by enthusiastic fans.

It was growing so fast that Yang and Filo had to bring in their friend Tim Brady to help write a business plan so they could turn their hobby into the job it was clearly shaping up to be. Remarkably, as a business student, Brady was also able to submit this plan to some of his classes as a final project for graduation.

Within a few months, Yahoo was indeed a real business, receiving an initial $1 million in funding. A year later, in 1996, they went public and were on their way to becoming one of the web's pioneering empires.

The story of deli.icio.us is similar in many ways. This site began in the late 1990s as a private collection of online bookmarks made by Joshua Schachter, who was working as an analyst at Morgan Stanley at the time. By 2001, with the help of some suggested links from others, he'd amassed around 20,000 bookmarks, which proved highly popular at a time when finding cool and interesting things on the web wasn't easy.

Naturally, a collection this size was a challenge to keep organized, so he started tagging the links with short categorical descriptions, like "math" or "food." He then put the database on a server for the public to see. Within a year, the service had 30,000 users. It was growing to be so popular that he received $1 million in funding before the service was purchased by Yahoo in 2005 for $30 million.

For Schachter, tagging was a simple solution to a personal organizational problem, yet it was one that became extremely popular in helping others find what they were looking for as well.

### 6. Sometimes, as with Apple and ArsDigita, simpler is better. 

Oftentimes, the best solution to a problem isn't going to be some complex tool that comes with a 500-page user manual. Like Joshua Schachter using descriptive tags to help organize his bookmarks, the best solution is often simple and easy to use.

Simplicity has always been at the heart of Apple, even in its earliest days when engineer Steve Wozniak was working in his apartment, putting together the first Apple computers.

When Wozniak was a high schooler and teaching himself how gadgets worked, he wouldn't just take things apart and put them back together again — he'd take them apart and then figure out how to make them work with fewer parts. This way, they could not only be made more cheaply, but they could also be made more elegantly and with fewer chances of things going wrong.

For Wozniak, doing more with less is what being an entrepreneur is all about. A true entrepreneur finds a way to create something with whatever limited resources they have while still striving to make it better than whatever is currently being sold.

A similar philosophy was in the mind of Philip Greenspun when he created the web design company ArsDigita in 1997. After creating a simple yet popular community site called Photo.net, he started getting requests from companies to help build their websites.

After working on a few sites, he published a free design framework called the ArsDigita Community System, which anyone could use. But the requests for design work didn't stop. After he published a book called _Database Backed Websites_ (1998), they increased further.

What Greenspun built was a business that offered simple, quick and clean design solutions, with websites that didn't require the sort of fancy coding that took lots of time and created bugs. He was also trying to create a business that positioned programmers as problem solvers and not just human robots who kept quiet and wrote code all day.

ArsDigita had an annual growth of around 500 percent and was working for clients like Levis and Hearst Publications. But unfortunately, there is such a thing as growing _too_ fast. When Greenspun opened the door to venture capital, all but one of the founders got squeezed out, with new leadership wanting to turn the startup into a slow and expensive company like IBM. In the end, ArsDigita collapsed, becoming one of the more well-known examples of what can go wrong when venture capital enters the picture.

> _"Reduce. Do as little as possible . . . If you've got two things that you want to put together, take away until they go together. Don't add another thing."_

### 7. Too much investor money can hurt a startup, so find other ways to generate revenue or stay cheap. 

As Philip Greenspun found out, venture capital often comes with strings attached, whether it's the addition of investor-approved executives or handing over shares in the company or a percentage of the profits. So, if you're launching a startup and want to stay in control of your business, many founders recommend that you reduce costs or find a way to avoid bringing in new investors.

Joel Spolsky knew about Greenspun's dilemma with investors, and he founded Fog Creek Software with the aim of following ArsDigita's lead while avoiding their mistakes.

Like Greenspun, Spolsky wanted to have a consulting company that attracted great programmers. To do that, he had to create an environment that treated programmers well. So he made sure every programmer would have their own office with comfortable chairs, be able to travel first class and get four weeks of vacation annually. These are all perks that investors don't like to see since they consider it all to be an unnecessary extravagance — they'd prefer to see programmers lined up at desks like a sweatshop.

So when they were just starting out, Spolsky had a plan to make sure he didn't need venture capital: he had a useful piece of bug-tracking software called FogBugz. He even discovered an interesting thing about software sales: sometimes a high price would make the software appear to be more valuable and therefore more appealing. So when he raised the price from $199 to $999, he actually sold more units!

The other way to avoid having to take on venture capital is to stay cheap, which a lot of startup founders advise, including Paul Graham, the co-founder of Viaweb and the man behind the influential startup incubator, Y Combinator.

As Graham sees it, for every penny of investor money you take, the less autonomy you'll have and the more you'll be at the mercy of others. So spend as little as you can and embrace a minimalist, bohemian style rather than a lavish or luxurious one.

### 8. As Paul Graham has discovered, it’s best to be honest while making something people really need. 

Of all the advice given by the founders with whom the author spoke, one of the most repeated was how important it is for entrepreneurs to listen to their customers. With this in mind, the emphasis should be on creating something of real value to others.

Certainly, this was what Paul Graham, of Viaweb and Y Combinator, advises. In fact, "Make something people want," was the phrase printed on Y Combinator t-shirts. As Graham sees it, the very basis of a startup should be about creating a product or service that makes other people happy and then converting that happiness into money.

This may sound simple enough, but Graham has seen his fair share of people come up with an idea that they _think_ will make people happy. This is why it's important to listen to your customer and _know_ what they really want.

Graham also puts a high value on honesty. With Viaweb, Graham and his co-founder Robert Morris created web-based software for setting up online stores. What's more, they set out to create _the best_ e-commerce software available. And since they kept close tabs on their customers' satisfaction, as well as the quality of their competitors' product, they could say with all honesty that Viaweb did indeed offer the best software.

The added benefit of honesty is that when you, the entrepreneur, say your product is the best choice available, the client will be able to pick up on the fact that you're telling the truth. It's hard to put a value on truth and honesty, but if being a salesperson doesn't come naturally to you, having the benefit of honesty can be a huge asset in winning over clients.

Paul Graham was always a tech-oriented guy and never comfortable in the role of a salesperson. Nevertheless, he found that with Viaweb, he could be very persuasive thanks to the one simple trick of being honest. Other salespeople might suggest that you need to be eloquent and have tons of charm and finesse to sell your product, but Graham has found that honesty can indeed be a powerful tool for winning people over. ****

### 9. With Flickr, the timing was right for photo sharing. 

Sometimes the stars align, and your product comes out at just the right time to compliment all the other things that are going on in the zeitgeist. Certainly, this was the fortunate case for photo sharing software Flickr.

Caterina Fake and her husband Stuart Butterfield didn't plan on this timing, as Flickr is another case of a successful product not being the startup's original idea. In 2002, Fake and Butterfield began working on Game Neverending, a videogame partly inspired by the popular pet games like Tamagotchi, where a user takes care of a virtual pet and gives it toys while trading items with other users.

One of the planned features of Game Neverending was to allow users to share photos with one another. As they were waiting for some work on the game to be finished, Fake worked on this photo sharing feature and eventually enabled users to post their photos on a web page — this is where Flickr really took off.

Looking back, Fake can see where all the pieces fell into place for Flickr. After all, there had been other photo sharing programs before, but by 2004, personal blogs and the first wave of social media, such as Myspace and Friendster, were making it much more appealing to share photos. Plus, digital photography was also making it increasingly easy for anyone to get their photos on the web.

Until then, most online photo services like Shutterfly made their revenue by getting people to pay for a printing service. And for this reason, Fake feels fortunate that Flickr didn't start out as the main project, since the research would have suggested that there was no market for photo sharing that didn't ultimately aim to get people to make prints.

But in 2004, people were realizing that photo sharing was one of the primary activities that made social media platforms like MySpace and Friendster fun. Plus, the recent rise in personal blogs made people a lot more comfortable with the idea of sharing their personal photos with the public at large. Before blogs and social media, the standard practice was to keep any online photos private.

### 10. Final summary 

The key message in these blinks:

**Between the time when Apple made its first computers in the late 1970s and the early 2000s when blogs and user-generated content were all the rage, startups revolutionized business in the United States. In talking to some of the founders of the most successful startups during this time, we can see some interesting commonalities among the many stories. These include the fact that many startups began without any initial idea; that often finding ways to do more with less can lead to success; that solving a personal problem can end up being a million dollar solution; and that often the initial business plan doesn't resemble the final one.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Startup Playbook_** **, by David S. Kidder**

With _Founders at Work_, you get a good idea of what was going on in the days leading up to the great Dot Com Bubble of the 1990s and the inevitable bursting of that bubble in the early 2000s. So now's the perfect time to follow up on what happened in the decade afterward.

For many, _The_ _Startup-Playbook_ is considered the "bible" for anyone interested in launching a startup, as it talks to the founders of some of the biggest firms around and gets them to spill their secrets. So you'll not only know how startups get from launch to billion-dollar valuations in record time, you'll also find out how they keep from falling apart in the process. This is essential material for any would-be entrepreneur!
---

### Jessica Livingston

Jessica Livingston is one of the founding partners of the startup accelerator Y Combinator, which has advised and invested in a number of successful startups including Dropbox and Airbnb. In 2015, she became a financial backer for OpenAI, a nonprofit dedicated to the responsible and safe development of artificial intelligence.

