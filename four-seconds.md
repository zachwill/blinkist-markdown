---
id: 55641bf86265620007000000
slug: four-seconds-en
published_date: 2015-05-29T00:00:00.000+00:00
author: Peter Bregman
title: Four Seconds
subtitle: All the Time You Need to Stop Counter-Productive Habits and Get the Results You Want
main_color: F7B631
text_color: 785818
---

# Four Seconds

_All the Time You Need to Stop Counter-Productive Habits and Get the Results You Want_

**Peter Bregman**

_Four Seconds_ (2015) gives precise examples of how to rid yourself of self-defeating habits at work, at home and in your relationships. A four-second pause helps slow down hasty, unhappy reactions and is the first step to reworking the way you communicate with others and receive feedback from them. You really can be prepared for anything if you just take a breath first.

---
### 1. What’s in it for me? Discover why success is only four seconds away. 

Breathe in. Breathe out. It takes four seconds to take one deep breath — four seconds that are crucial to the quality of the decisions you make. Stress leads us to self-sabotage by, for instance, forgetting to prepare for important events or getting into arguments with those around us.

In _Four Seconds_, Peter Bregman explains how in just those four seconds, by taking a deep breath and allowing ourselves to think twice, we can start to turn around counter-productive habits and create the kind of results we want.

How can we create useful habits that connect us to others? How can we optimize our work habits?

You'll find answers to these questions and also discover

  * why goals can lead to cheating;

  * why listening is the best communication tool there is; and

  * why you should always share your successes.

### 2. To create better habits you have to pause, breathe, and identify an area of focus. 

When you're in a stressful situation, do you sometimes react in a way that ends up making you feel even worse? Many people respond to stress in self-defeating, counter-productive ways, such as yelling or starting an argument with someone. So how do we transform this unhelpful behavior into habits that save our time, energy and sanity?

The first step is pausing and breathing for _four seconds_.

Four seconds is the amount of time you need to take one deep breath in and out.

When you pause and breathe, this puts you in a state that allows you to make better decisions and consider the outcomes of your actions before you take them.

Say your kids are refusing to brush their teeth before school. It's the third time this week and you can feel the anger bubbling up inside of you. You're about to lose your cool and start yelling. But instead, you resist, pause and breathe. After doing so, you feel a little calmer and clearer. Maybe you even come up with a tooth-brushing game instead of succumbing to the counter-productive reaction of shouting.

In addition to the four-second breath, in a stressful situation it's a good idea to identify an area of focus, rather than a goal.

Goal setting is often accompanied by temptations to cheat or to take unnecessary risks, whereas an area of focus motivates you without offering up such negative temptations.

For example, you own a store that sells a certain product. In recent years, your goal has been to increase your revenue. This, however, has led to some of your staff using questionable methods to hit targets, such as lying about particular features of the product, because all they care about are the sales.

But this year, you've set an _area of focus_, which is to talk more with the customers. The results? At the year's end, you realize that the revenue has increased, thanks to focusing more on the customers than on increasing revenue.

### 3. Daily preparation focused on processes – not solutions – will help you develop good habits. 

Living in a busy world, we need to be prepared for everything life throws at us.

In fact, failing to prepare for the day ahead often results in costly blunders that waste time.

Imagine you arrive at work unprepared for the morning meeting, then it suddenly dawns on you that you have to chair it — you forgot this was decided last week! Of course, you neglected to read the agenda, and then to make matters worse, two new faces walk into the room. Totally caught out, you decide that you'll have to reschedule the meeting, resulting in wasted time for everyone involved.

But how can you go about preparing for the unexpected? The secret is preparing for a process, rather than a solution. It's impossible to be armed with a solution for an unpredictable event, but we _can_ be prepared to maneuver through ambiguity. To do so, use the following three-step process. It will help you when you find yourself in a situation you weren't prepared for, and are suddenly required to make a decision.

First: Pause, breathe in and out deeply for four seconds, and think. For instance, if you're out sailing in your boat and run into a storm, breathe, and think.

Next, assess your options. Ask yourself: given the resources and information I have available to me, how can I get to my desired outcome? In this case, you have two options: continue sailing or swiftly make your way back to land.

Finally, make a decision and stick to it. Say to yourself: even if this isn't ideal, it's the best option given the circumstances. So, based on the judgement that the storm will soon pass and that the wind would be more violent if you turned back, you decide to continue sailing.

Taking just four seconds to pause and breathe once in a while will better position you to alleviate your stress and figure out how to proceed.

### 4. To improve your communication habits, focus on the content of the message and avoid arguing. 

How often do you get distracted by _how_ someone communicated a certain point — their tone of voice, or their aggressive body language — rather than focusing on the content of the message? Most of us are pretty poor communicators, and you'll have to acknowledge this in order to improve your communication habits.

Really try to make a habit out of focusing on the content, instead of the packaging it comes in, to gain a better understanding of the message.

For instance, say you had a job interview and now expect a phone call from your potential employer to let you know if you were successful. Instead, several weeks later you receive an email stating that you didn't get the job. You feel pretty let down that the potential employer didn't even bother to call you personally.

This reaction is hardly helpful, though. Why not take a deep breath and reinterpret what the email actually said, rather than only giving your attention to the packaging? Find the value in the message. For example, do they mention other opportunities for you within the organization? After you've calmed down, respond with a different medium to the one that upset you. That is, call them rather than email them.

Another valuable tip for improved communication is to stop arguing.

Arguing is about nothing other than getting the upper hand. Even if you start to see some flaws in your viewpoint, it's unlikely that you're going to suddenly admit it if you're in the middle of a fight! The result is hopeless; you'll both stick to your point of view, making clear and helpful communication impossible.

Instead, you should use the best tool you have available: listening. Listening is a brilliant tactic as it doesn't threaten the other person. On the contrary, they will feel heard, piquing their interest to listen to you. So if you want to bring your point home and even sway the other person, drop the arguing and start listening.

### 5. To strengthen your relationships, don’t expect too much. Focus on what you appreciate about the people around you. 

In our hyper-connected world we communicate with a huge variety of people, some similar and some very different from ourselves. In either case, it's not uncommon to be taken aback by what people say and do. But have you ever thought that the problem might not be the other person, but instead our own habit of expecting too much of them?

If you can learn to alter your expectations, you'll find that you'll strengthen your relationships with those around you.

Expecting people to say and do what you would have said or done doesn't usually end well, since no one is a carbon copy of you! It's a far healthier habit to treat people how _they_ wish to be treated. If you do this, others won't fail to meet your standards and, in turn, you won't get frustrated.

To further reinforce your relationships, let people know what it is about them that you appreciate.

In the business world and in our private lives, we often wrongly believe that gifts or bonuses, in and of themselves, make people feel appreciated by those who give them. This is not the case, however. Picture coming to work one day and finding an envelope on your desk with a bonus inside it, but with no letter to go with it. You'd probably view the bonus as compensation for what you've accomplished professionally, but will it really make you feel appreciated for who you are as a person? Probably not, since appreciation is expressed through _people_ and not things like bonuses or lavish gifts.

Therefore, aim to replace this gift-giving with a habit of telling people that you appreciate them _just as they are –_ not for the favors they have done for you or the work they have done for your organization. This will encourage them to feel respected and loved, which will strengthen your relationship.

### 6. To get the most out of your work, accept failure and share success. 

We're continually learning new things in our working lives: a new language, a new technological skill or management technique. However, we often tap into bad habits that can hinder our own or others' learning. So how do we stop this?

First, we have to understand that, in order to learn anything, we must have the opportunity to fail.

Learning is not about being perfect. Quite the opposite. It's about recognizing when you've failed, and being able to adjust accordingly.

Let's say you're teaching your son to ride a bike, and every time he wobbles, you jump in and catch him. Several weeks pass and he's still not able to keep steady on the bike. Why? If he doesn't get the chance to lose balance and possibly fall, he won't be able to recover, maintain balance, and learn to ride the bike himself.

The same applies for any manager of an organization. As a manager, you have to protect the organization, but blocking the chance for failure and self-recovery is a weak strategy if you want your employees to acquire new skills and improve their work.

So if an employee struggles in an important meeting, you should assess how the employee performed in relation to your expectations of them, and what they can learn from this.

Besides making room for failure, you should also be prepared to share your successes if you want to optimize your work habits. This is because the achievements of an organization or any kind of venture doesn't just boil down to the work of one person.

Also, knowing that you've contributed to a success is highly motivating. And when you're motivated, you work more effectively.

So ditch the bad habit of praising only one or two people for a success. Instead, ensure that everybody is aware that they, too, contributed to it.

### 7. You’ll be able to optimize your work habits if you can neutralize negativity and accept criticism. 

Acting negatively is something we all do from time to time. Whether it's at home, at work, or just getting riled up during a football match, we all have our less than desirable sides! But we don't do our best when we hold on to negative feelings. Let's see how we can combat this.

You can start with never responding _against_ anyone's negativity. If you do so, people take it that you're contradicting their emotions, which will encourage them to hold on to that negativity.

Take the case of some employees who gravitate towards negativity. The manager reacted by gushing positively about the chance of bonuses or by making negative comments about how their mood was deadening his energy. Nothing changed.

A better way to respond would be to do so _with_ the person in question, in three steps.

First, show your understanding of the person's negative emotions. Then, if you've felt similarly before, tell them about it. Lastly, don't tell the person that they're wrong. Rather, try to uncover what the person is positive about and focus on that.

These steps will have a neutralizing effect on their negativity.

You can use yet another tool to complement the steps above and promote positivity in the workplace — accepting criticism.

To do this, regard criticism as a gift, rather than something that makes you put up your defenses. When receiving criticism, do the following:

Accept that you feel hurt or angry. Then place these feelings to one side. Next, realize that the person offering the criticism might not be the best communicator, so put your attention on the actual message, not the package. Then, try to take a neutral stance, by neither agreeing nor disagreeing. Then listen, and try to simply gather the information being offered. Finally, take a little break before you consider what the person said and decide after a little while if, what and how you can change.

### 8. Final summary 

The key message in this book:

**All you need for bad habits to weaken their grip on you is four seconds: the time it takes for one deep breath in and out. By taking this micro pause, you'll be better equipped to collect yourself, gather your thoughts and act in ways that benefit you, rather than work against you. Pause when you get criticism or have to deal with gloomy colleagues, and then turn that negativity around.**

Actionable advice:

**Choose content over delivery.**

The next time you feel your blood boiling over because of something someone said, try to listen to the content rather than how they packaged it. By really paying attention to _what_ they're saying, it's more likely that you'll uncover the message they were actually trying to get across!

**A word of appreciation goes a long way.**

If you are the manager of a business or the leader of a project, remember that everyone contributes to your successes, and you should share it with them. The best way to do this is by actually speaking to them, rather than offering faceless material rewards. This will encourage everyone involved to keep doing their best.

**Suggested** **further** **reading:** ** _Work Simply_** **by Carson Tate**

In _Work Simply_ (2015), author Carson Tate draws from her own career experience to show you how to become more productive. By understanding your own productivity style, you can make lighter work of the ever-growing pile of tasks and achieve your life goals.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Peter Bregman

Peter Bregman is the CEO of Bregman Partners, Inc., a coaching consultancy that helps leaders achieve their goals. He is the author of the _Wall Street Journal_ -bestselling book _18 Minutes_ and you can find his professional input in such publications as _Forbes_, _Psychology Today_ and _Harvard Business Review_.

