---
id: 57a8da241c2e11000357fb00
slug: fox-en
published_date: 2016-08-12T00:00:00.000+00:00
author: Martin Wallen
title: Fox
subtitle: None
main_color: FCB040
text_color: 7D5720
---

# Fox

_None_

**Martin Wallen**

To simply explain the fascinating animal known as the fox in biological terms wouldn't do it justice. This mysterious animal has long been the subject of countless stories and myths and cultures around the world have developed their own unique perception of the animal. _Fox_ (2006) dives into these many myths and stories, and also shows how the fox continues to influence our language and culture.

---
### 1. What’s in it for me? Step into the mysterious world of the fox. 

Foxes are sly and cunning and also a little cowardly, right? Well, not so fast. Most of us believe that we know a thing or two about foxes, even if we've never encountered one in real life, because we're all familiar with at least a few stories involving foxes.

Children love fables about the fiery red animal — as small and frail as themselves — but clever enough to trick all the other animals, even the lion king. On the other hand, it could be this very image of subversiveness that makes so many people in the West detest this furtive animal.

But does the fox really deserve its Western reputation?

In these blinks, you'll learn about the stories that shaped how we see the fox and that, in many other cultures, stories involving foxes are quite different from our own.

You'll also learn

  * how to make sure that you don't talk with a fox-spirit on the phone;

  * what Aristotle thought about foxes; and

  * what connects the fox and _Zorro_.

### 2. Aristotle was the first Westerner to classify the fox; later, his system was replaced by more scientific descriptions. 

If asked to name the most influential philosophers of all time, you'd probably mention Aristotle. But a lesser known fact is that Aristotle was also a pioneer of zoology — the first person to describe and classify nature in a systematic way.

This makes Aristotle the first Western thinker to categorize the fox, though he didn't have a very high opinion of the animal, describing it as an inferior creature, the direct opposite of man.

Aristotle had a unique way of classifying animals in relation to each other, which included grouping them into "cold and earthy" or "warm and fluid" categories. The "warm" category was considered superior and was made up of flesh and blood animals, like humans, while the inferior "cold" category contained more bony, sinewy and hairy creatures.

This classification also took into consideration the animal's habitat, and since the fox is a hairy and bony creature that burrows in the earth, Aristotle placed it in the less respectable "cold and earthy" category.

Creatures in Aristotle's "warm and fluid" category were also considered closest to divinity and perfection, whereas the "earthy" creatures, including the fox, were furthest from divinity.

Unfortunately for the fox, this classification would obscure our view of the creature for centuries.

It wasn't until around the eighteenth century, during the Enlightenment, that naturalists ventured outside their libraries and discovered the true nature of the fox.

Up until this point, Europeans were only aware of the red fox, _Vulpes vulpes,_ and the Arctic fox, the _Alopex._ But when they began to travel around the world, naturalists found new and exotic species of foxes everywhere and began to realize how incredibly adaptable foxes really are.

Today, we know of at least 21 species of fox, covering a broad range of habitats, and coming in many different sizes and colors.

Though some foxes may appear rather cat-like, they all belong to the biological family _Canidae_, which also includes dogs, wolves and jackals.

> _"Of all creatures, the fox seems to be the one whose most defining feature is ambiguity."_

### 3. Traditional stories depict foxes as either cunning and wicked, travelers between two worlds or fertility symbols. 

More than biology, it is the old tales, passed down through the generations, that have most influenced our perception of the fox.

Many cultures, including Christianity, have myths that portray the fox as a wicked, cowardly and cunning thief.

For example, there is a popular Greek myth that tells the tale of the never-ending fight between Teumessian, the fox that wreaked havoc on the city of Thebes, and Laelaps, the only dog powerful enough to keep it at bay.

Christian stories typically portray the fox in a negative light, as a clever, charming and wicked thief — and sometimes as the devil himself. There is the popular image of a fox dressed up like a priest preaching to geese, an allegory suggesting how the devil can deceive and lure humans away from goodness.

Other cultures have a more mystical view of the fox as a creature that can cross boundaries between the worlds of life and death, serve as a guide and even act as a shapeshifter.

In Siberian folk religions, foxes can serve as guides to lead shamans down transitional paths that are otherwise not open to ordinary people.

In Asian cultures, the fox is often represented as a shape-shifter whose spirit can take on human form to influence the affairs of people and even charm them into having sex. Here again, we see the idea of foxes being able to cross boundaries and move between different worlds.

But associating foxes with sexuality is common to many other cultures as well.

For ages, humans have associated the fox's fiery red fur with the earth's fertility. For this reason, a fox was sacrificed in Celtic rituals intended to help crops grow. 

Even the tail of a fox has been seen as a phallic object. In Japanese mythology, there are fox-spirits in fungi. Indeed, the name of a certain kind of mushroom, _kitsune-no-chimpo_, translates to the "fox's penis."

> _"The fluid power of shape-shifting foxes had been recognized throughout the world, except in the West…"_

### 4. Some Western cultures have a violent relationship with the fox, hunting it for sport and killing it for its fur. 

Foxes, though rarely eaten, have nonetheless been viciously hunted and often brutally killed.

Part of the reason for this is that foxes are considered a dangerous threat to farm animals. But, over time, the hunting of foxes turned into a prestigious and highly regulated sport for the upper classes.

The golden age of the fox hunt lasted from 1753 until 1914, and it took place primarily, though not exclusively, in Britain.

The fox hunt began as a brutal ritual that simply allowed privileged people to be violent and enjoy the excitement of the kill. This changed in the nineteenth century, when the hunt became more structured and the kill was only a means to an end.

At this point, the purpose of joining a hunt was to show that one belonged to high society and was able to play by its rules. It was all about speaking the language of the upper class and establishing yourself as a gentleman.

In this privileged world, the idea of "manliness" changed from being physically strong to having superior moral qualities.

Foxes were hunted so relentlessly that Britain began importing them — and a ban was put on killing foxes outside of a hunt. Fox hunting, the popular pastime of the elite, also influenced fashion.

As wearing fox furs became fashionable in the nineteenth century, the fur market grew rapidly and an entire industry developed around it. Red and Arctic foxes were raised on Alaskan islands that served as a natural prison and fur-production ground. And, eventually, after years of failure, foxes were raised in captivity in Canada.

All this led to fur becoming a highly profitable import in the twentieth century. By 1925, Arctic fox fur sold for $150 in Alaska; upon arrival in London, however, the price skyrocketed to $2,800.

> _"The persistence of fox-hunting depends wholly on the low regard in which the fox is held in Western culture."_

### 5. The influence of the fox has survived and flourishes in our language and in modern stories. 

Have you ever wondered why the Japanese begin a telephone conversation by saying "moshi moshi"? This started from the belief that this nonsense phrase couldn't be pronounced by fox-spirits, so when you say it to someone over the phone, you're proving you're a human and not a mischievous fox spirit!

There are plenty of other lasting influences of the fox to be found in modern stories around the world.

There's the popular character of _Zorro_, for example, whose name is Spanish for fox and signifies the cunning and fox-like qualities of the hero.

And in Korea, there's the 2004 television series _Gumiho_, which centers around the romance between an ordinary teenage boy and a girl who's a fox-spirit.

In fact, there's no shortage of shows and movies that continue to perpetuate the various cultural associations of what it means to be a human with fox-like characteristics.

This has also led to our language being full of "foxy" references.

Indeed, this trend began ages ago with our ancestors, who carried the strong image of the fox in their minds. They used "foxy" to describe other animals who shared their reddish color or triangular face with pointed ears, such as African bats.

The more common comparison, though, has nothing to do with physical appearance, but rather with the characteristics we associate with the fox.

Some famous people even earned the nickname fox for their sneaky behavior. Erwin Rommel, a senior German Army officer, was given the name the "Desert Fox" for repeatedly escaping defeat in North Africa during World War II. But just like the fox's mixed reputation, the name signified both admiration and shame; being "foxy" also meant employing cowardly and deceitful tactics.

Then, of course, there is Jimi Hendrix's classic song about a "Foxy Lady," which popularized yet another use of the word. Here, it refers to the thrill of the hunt, with the foxy lady being the prey in a sexual adventure.

So, while there's been many ups and downs in the history of the fox, it seems its influence on our culture is here to stay.

### 6. Final summary 

The key message in this book:

**What do we really know about the fox? While it may feel like a familiar animal, there are so many different cultural perceptions of the animal that it's hard to say whether anyone has an accurate point of view. While some cultures portray it as cowardly, deceptive and evil, others see it as sly and cunning or mythologize it as a shapeshifter or guide to other worlds. What is obvious is that foxes have been a permanent fixture in our cultures and will continue to be for generations to come.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _A Sky Full of Birds_** **by Matt Merritt**

_A Sky Full of Birds_ (2016) is a celebration of birdlife in Britain. These blinks offer fascinating insights into mating rituals and migration patterns and explain the cultural significance of murmurations, murders, swan songs, and more.
---

### Martin Wallen

Martin Wallen is a professor of English at Oklahoma State University and an expert on how our culture relates to animals, especially dogs and foxes. In 2004, he published the book _City of Health, Fields of Disease_.

