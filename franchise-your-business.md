---
id: 57f2957a90bf6100031a3faf
slug: franchise-your-business-en
published_date: 2016-10-07T00:00:00.000+00:00
author: Mark Siebert
title: Franchise Your Business
subtitle: The Guide To Employing The Greatest Growth Strategy Ever
main_color: F0514D
text_color: B23C39
---

# Franchise Your Business

_The Guide To Employing The Greatest Growth Strategy Ever_

**Mark Siebert**

_Franchise Your Business_ (2016) reveals the ins and outs of setting up a business franchise. From winning over franchisees to ensuring your brand's quality across branches, these blinks are a practical guide to franchising, helping you to scale up your business with a powerful growth strategy.

---
### 1. What’s in it for me? Learn about the franchising path to success. 

You probably already know that restaurants such as McDonald's and Subway or the convenience store 7-Eleven are franchise businesses. But do you know what exactly this entails?

More importantly, would a franchise be the right option for you if you were to expand your business? After all, franchising is only one of many options.

These blinks will help you to figure out if franchising is the right step for your business pursuits. They'll outline the key advantages of franchising, while also offering some insights as to why not all businesses will work as franchises.

In these blinks, you'll also discover

  * why franchisees tend to be very committed managers;

  * why barbecue businesses in the United States usually aren't franchises; and

  * three possible ways to structure your franchise business.

### 2. Franchises are built on mutually beneficial relationships between business owners and their franchisees. 

When it comes to modern business, franchise structures are everywhere. Many businesses today increase their market share and expand across new areas by creating a franchise.

So what is a franchise exactly?

A franchise is a partnership between a franchisor and a franchisee. A franchisor is typically someone who already owns a thriving business. Franchisors create a contract that allows another party, the franchisee, to copy and use the existing business model. And with that, a franchise is born!

Hold on, you might be thinking — why would any aspiring business owner want to join a franchise? After all, wouldn't it be more exciting to create a unique business of one's own? Sure! But starting your own business, as we all know, is tough. Becoming a franchisee, on the other hand, has a lot of perks that make those difficult first stages easier.

Franchisees can take advantage of the franchisor's brand, trademark and customer base, rather than having to build up their own from scratch. Franchisees also benefit from the training provided by the franchisor, helping them set up and run the business as smoothly as possible. Plus, franchisors provide ongoing support in challenging areas, including finance and operations. This way, franchisees can hit the ground running and get their branch off to a great start.

Of course, franchisors don't take care of everything for the franchisee. Franchisees invest their own money and are responsible for all loans taken to open up the new branch. They're also expected to pay a certain percentage of their sales to the franchisor and buy supplies, from ingredients to branded equipment, from them as well.

### 3. Expanding through franchising has several advantages over simply opening new company-run units. 

If you've got a great business on your hands and are ready to take the next step, what are your options? Business owners looking to expand might think about opening another business unit operating under their own management — but this can be expensive and time-consuming. Franchising offers a more cost-effective alternative to growing your business. Let's take a closer look at three key advantages.

First off, franchises require less money to get started. To set up another company-owned business unit, you'll need capital. To get the funds you need, you'll find yourself grappling with banks to set up increasingly expensive loans, or struggling to win the attention of investors in equity markets.

To avoid all this hassle, business owners can create franchises. As we learned in the previous blink, franchisees invest their own money and carry the risk of the business themselves. This means that franchisors don't have to raise anywhere near as much capital to open a new branch.

Secondly, franchisees make great managers. No matter how you expand your business, you'll have to delegate control to others. But managers hired to run new company-owned businesses are likely to act rather selfishly, since their livelihood doesn't directly depend on the survival of your brand and business.

Managers of franchise branches, on the other hand, carry the risk of the operation; they're personally invested in the business and will want to see it succeed. As a result, franchisees tend to be responsible and reliable managers.

Finally, creating a franchise will kickstart your expansion much faster than creating company-owned units ever could. Taking the latter path means a lot of managerial work ends up on your plate, from finding a new location to hiring staff, whereas franchisees will take on this managerial work themselves. All that's left for you to do is find the individuals willing to become your franchisees.

> _"And while it is not right for everyone, franchising is, without a doubt, the most dynamic growth vehicle ever invented."_

### 4. To become a franchise, businesses must show promise to franchisees and have a reproducible model. 

We now know that franchising has a whole host of benefits. But when deciding to start a franchise, there are a few more factors to consider. The truth is that not all businesses make good franchises — so how can you tell whether it's worth becoming a franchisor or not?

There are two essential conditions that any business hoping to jump into franchising needs to have covered. First, your business must look promising to franchisees.

You want potential franchisees to jump at the chance to put your name on their shop front, so your business needs to have an edge over competitors. So how can you differentiate yourself? Start out by working out your _unique selling points_.

Unique selling points can be anything from a viral marketing strategy to eco-friendly company values to a just plain brilliant product. Even the pizza franchise Domino's has unique selling points that help it stand out from the competition, such as its strong focus on delivery service.

If there are just too many competitors with similar products out there, your business can still win over franchisees with promises of great benefits. Many lawn-care companies in the United States are part of a franchise, having signed up because of the great marketing and support networks offered by the franchisors.

The second essential condition for a franchise is that the business must be easy to reproduce. Once you've sealed the deal with franchisees, they'll have to build up a replica of your business, which means learning everything about its operation — and they've got about three months to do it. If your business model is too complicated to be taught and reproduced within three months, then franchising may not be your best option.

Also bear in mind that not all products can be a hit in every region. A barbecue in South Carolina means a big plate of pulled pork, while in Texas, people expect slabs of beef. Different cities come with different customers, who in turn have different tastes.

After investigating whether your business is suitable for franchising, it's time to think about whether a franchise model suits you personally. Find out what a franchise means for your career and life in the next blink.

### 5. Before starting your franchise, consider whether it suits your future and personality. 

Making your business into a franchise is no easy task. It takes a lot of effort to build up a winning franchise system, and this should fit into your plans for the future, both professionally and personally.

Start by asking yourself where you want to be with your business five years down the line. Do you still plan on running it, or do you hope to have sold it by that time?

If you're planning to sell, you'll want to maximize the value of your business so you can get a good price. In this case, franchising is a sensible option: you can expand quickly, which means you can attain a higher valuation for your business sooner.

Next, ask where you are with your business now. Consult your finances, and consider some of the challenges your business faces. Is your business concept a valuable one? If it is, then there's just one last question to ask: are you the right person to start a franchise?

Franchisees are business owners themselves, often with experience and, sometimes, strong opinions. To build a successful relationship with them, you'll need to have some leadership skills up your sleeve. In the best-case scenario, you and your franchisees should be able to work together on new ideas and strategies to keep your brand moving forward.

As well as being a good leader, you need to be a great salesperson. As the franchisor, you call the shots when it comes to the direction of your business. You have to be able to sell your ideas to your franchisees, helping them see things from your perspective. Explain your choices, and make it clear how the whole franchise will benefit from them.

If you've gotten this far and have decided to enter the franchise business, it's time to start your preparations! This is what we'll delve into in the next blink.

> _"Just because a particular business can be franchised doesn't mean it should be franchised."_

### 6. Great franchises are founded on solid plans and fitting structures. 

There are over 3,000 franchise companies active in the US market today. So what does this mean for you? Well, put it this way: the days when a bright idea was enough to make a load of money through franchising are gone. Nowadays, a well-defined _franchise plan_ is what will ensure your business's survival.

A franchise plan should outline how you envision working with your franchisees. It's vital that you have a fixed set of rules to follow — this will help you stay on the right track and keep your franchise relationships strong. Like any good plan, your franchise plan must include goals, as well as the steps you need to take to achieve them.

Start by deciding on the kind of franchise structure you're aiming for. You could take the _single-unit_ approach, which allows each franchisee to open one business unit each. This strategy was behind the successful expansion of McDonald's to just about every city in the world.

Another option is _area development franchising_. This means creating a contract with an area developer, who then retains the exclusive right to open up a certain number of business units in an agreed-upon area. Pizza Hut and KFC are known for their use of this franchising strategy.

A third alternative is _sub-franchising,_ most often seen in international markets. This entails giving another individual the rights to act as a franchisor in a specific region. This company will take care of negotiations with new franchisees and provide support for them, in exchange for a fee paid to you, the franchisor.

Each of these options has its benefits and disadvantages. The single-unit franchise requires that you give your franchisees a lot of support, which could lead to high costs in training and management. If this doesn't sound feasible, then area development franchising could be a better bet. That way, you're able to rely on experienced franchisees to take care of support and management.

After setting up your franchise according to your plan, the job isn't done! You need to maintain your franchise so that it continually generates profit and grows. Find out how in the next blink.

### 7. Quality control is essential to upholding your brand values across branches. 

Franchisees are a little different from your garden-variety managers. You can't fire them easily and you can't control how they run their business as much as you'd hope. Because of this, you need to ensure your brand gets the protection it needs.

How? By creating a _quality control system_.

An effective quality control system starts with hiring professional and reliable franchisees. The right candidates need not only business chops, but a strong work ethic too. This way, you can be sure that they'll provide your customers with top-notch service. If a franchisee is a good businessperson but hurts your brand image by disrespecting customers, your brand will run into serious complications sooner than you think.

It's a good idea to create and maintain a franchise operations manual; this is a guidebook for your franchisees that details how each aspect of your business should be run. A franchise operations manual also serves as a legally binding document that you can write into your franchisee's contract. This way, you can ensure that every franchisee is committed to maintaining the standards of your brand.

In fact, one legal case illustrates just how powerful operations manuals are. In Ketterling vs. Burger King, a customer took the fast food chain to court because she slipped on snow in the parking area of one of its branches. But because Burger King had specified in their operations manual that day-to-day maintenance was the task of the franchisee and not the franchisor, they couldn't be held responsible for not removing the snow.

Finally, you can ensure that the quality standards of your brand are upheld by providing adequate training and support to your franchisees. The importance of these two aspects of franchising should not be underestimated! Training doesn't mean just one preliminary crash course in your business with a new franchisee; instead, ongoing training is required to ensure branches are consistently performing well.

You can make ongoing training cost effective by going digital. Online videos and training programs allow franchisees to brush up on their skills anywhere, anytime. Regional meetings and annual conventions are also great fixtures in a franchise's calendar, allowing teams to come together and get to know each other.

### 8. Ensure long-term success for your franchise through honesty, feedback and communication with franchisees. 

With all this talk of business standards and quality control behind us, it's time we take a closer look at the human side of franchises. A great relationship with franchisees is essential to your business's success and growth — and it all starts with healthy communication.

When establishing relationships with franchisees, roles should be clearly defined. They should know where they stand and feel comfortable with the fact that you're there to protect the brand and supervise your network of franchisees.

While certain sanctions may be necessary to communicate to franchisees that they've overstepped their roles, be careful not to slip into the role of a dictator; franchisees have the right to receive explanations for your business decisions, so let their voices be heard too!

By being open and honest with your franchisees, you can secure their trust and loyalty. Get in touch with them on a regular basis and be sure to respond to their e-mails and calls as quickly as possible. They'll appreciate it and will know that they have your support.

Finally, give your franchisees the opportunity to provide feedback. Unfortunately, many franchisors forget that their franchisees need a space where they can express their concerns. These constructive conversations are essential to successful long-term relationships, so be sure to make them part of your franchise operations. From open discussions with outspoken employees to anonymous surveys to help more reserved team members express their opinions, feedback is a cornerstone of thriving franchises.

### 9. Final summary 

The key message in this book:

**From winning over franchisees to creating a franchise business plan to establishing routines of quality control, setting up a franchise requires preparation and dedication. But with a strong brand, clear communication channels and an appropriately tailored model, you can ensure your business enjoys the many perks of a franchise strategy.**

Actionable advice:

**Never stop supporting your best-performing franchisees.**

Next time you think those franchisees whose numbers are always outstanding don't need that monthly meeting with you, think again! Though it's tempting to shift your time, money and support to branches that need the extra help, it's essential that you never stop caring about any of your franchisees. Rather than creating an imbalance in your attention and resources across branches, tailor different kinds of support to each franchisee's needs. Even successful branches have the potential to do better!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Small Giants_** **by Bo Burlingham**

There's a new phenomenon emerging in the American business world: _Small Giants_. These are privately owned companies that don't follow the usual corporate dogma of growing revenues at any cost. Instead, they're driven by their heart-felt enthusiasm for their product, and focus on factors like quality and caring for their workforce. The author has examined 14 small giants to explain how this strategy has made them successful.
---

### Mark Siebert

Mark Siebert is an expert in franchising and founder of iFranchise Group, which specializes in franchisor relationships. Throughout his career, he has worked with companies like McDonald's, Subway and Nestlé. He has also worked as a lecturer at Northwestern University and the University of Chicago.

