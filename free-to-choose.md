---
id: 52f113e23565360011000000
slug: free-to-choose-en
published_date: 2014-02-04T09:00:00.000+00:00
author: Milton Friedman
title: Free to Choose
subtitle: The Classic Inquiry Into the Relationship Between Freedom and Economics
main_color: E32D2F
text_color: C9282A
---

# Free to Choose

_The Classic Inquiry Into the Relationship Between Freedom and Economics_

**Milton Friedman**

_Free to Choose_ (1980) explores the relationship between freedom and the choices an individual is allowed to make in regards to the economy. Friedman reveals to us that economic freedom is an essential part of liberty. He details the myriad ways in which government regulations and interventions chip away at our fundamental right to make decisions in our own self-interest.

---
### 1. The power of the market lies in voluntary actions by individuals, not in government intervention. 

Today, many people believe that governments should determine the price or supply of goods in the market.

However, this is wrong; in reality, government intervention creates confusion in the functioning of the market.

This is because when the government intervenes in the market, it distorts the signals that tell consumers the true value of a good or service.

For example, during gasoline shortages in the 1970s, the government limited the price that gas stations could charge to keep gas affordable. The direct result of this was a flood of people buying gas at the same time, leading to increased demand and further shortages. Because the price did not rise naturally in response to the shortage, as it would have done in a free market, consumers did not choose to conserve or find alternatives to gasoline.

Government action therefore exacerbated the crisis.

Without the proper price transmitting information to consumers, they are unable to know how to spend their money in the most efficient ways.

So although it might seem strange, the best way to ensure that the market works effectively is to leave it alone. This way, the market is left to individuals voluntarily pursuing their own self-interests. Only by making individual consumers the main actors in the market will the proper value of goods and services, based on their demand and supply, be transmitted.

One of the most well-known examples of such voluntary action actually comes from outside the economic market: the development of language. This developed from the bottom up with no central control. People cooperated with each other because it was in the interest of the individual as well as the group to share and create common communication.

When people freely traded words, it benefited both parties — no control or planning by a central authority was needed.

### 2. Economic control by governments over individuals erodes personal freedom. 

When governments attempt to control parts of the economy, it often comes from a well-meaning assumption that they can level the economic playing field, making society fairer.

However, by controlling the economy, the government are in effect merely controlling the people who operate within it. And as the ability to choose how you spend your own money is essential to human freedom, government intervention actually leads to unfairer societies.

This can be seen with the use of government tariffs to "protect" jobs and workers from "unfair" competition. These tariffs hurt individuals because they reduce consumer choice. They add costs to everyday goods and prevent competition from companies who provide cheaper alternatives. This leads to fewer choices and higher prices.

Since buying from the cheapest source and selling at the highest price benefits every human being, the government is undermining your individual freedom by taking choices away from you.

The government may claim that voters want specific economic regulations. Yet so often after elections, governments spend taxpayer money in ways that may conflict with your own preferences. Aside from voting once at election time, you are not free to choose how the government spends your money. They make the decision for you; and once again your personal freedom is curbed.

Historically, the highest levels of free market economic activity correspond with the largest amount of individual freedom. Compare India in 1947 and Japan in 1867. In these years, massive political change allowed these nations to pursue the chance to massively expand their economies. Japan dismantled its feudal structure, increased social and economic freedom and relied on free markets. Conversely, India raised taxes, heavily restricted business operations, and controlled wages and prices. Where Japan relied on efficiency to dictate development, India relied on bureaucratic planning.

There was no question which route was best. Japan flourished, and its free population grew richer, whereas the economy of India stagnated and its population remained poor.

### 3. The Great Depression was caused by the very institution created to prevent it: the Federal Reserve. 

By the early twentieth century, large banks in the United States developed a system by which they could limit the damage and destruction caused by banking crises.

The system worked liked this: when the economy started to waver, consumers would rush to withdraw their money from banks, causing a panic and furthering the crisis. In response, healthy banks would band together to give loans to unstable ones and limit the amount of cash that was available for depositors. Though this strategy did have small negative effects on workers and businesses, it worked efficiently to prevent bank runs, and the typical crisis lasted only a few months.

Yet, after a series of bank runs in the early 1900s, political pressure mounted for government to intervene, and so the Federal Reserve (the Fed) was born.

This was a central bank that could lend to commercial banks during times of crisis by printing new money or borrowing money itself. Instantly, the Fed instilled a sense of confidence in bankers and citizens, as they presumed their banks and their money would be protected at all times by the Fed.

At first the Federal Reserve corrected some banking crises in the 1920s by lending money to banks in trouble. Yet when it came to the Great Depression, it approached the crisis in a completely different way. The Fed did the complete opposite of what it was expected to do after the stock market collapsed in 1929: it began to _decrease_ the amount of available money. Because banks were accustomed to emergency loans from the Fed, they never restricted funds and operated as if nothing was wrong. They had expected the Fed to offer economic help, so when it didn't they were left in serious trouble. Thus, between 1930 and 1934, there was a series of devastating bank runs, and the depression lingered on for ten years.

### 4. Inflation is a direct result of government manipulation of the economy and is a hidden tax on the population. 

Inflation occurs when the quantity of money rises more rapidly than the amount of goods and services available. When the total amount of money increases, it creates a greater demand for goods. And if the amount of goods does not increase, the only way to deal with demand is to increase prices.

The worst effect of inflation is that it is a hidden tax on individuals. For example, if you save $100 in the bank, hoping to spend it on $100 worth of goods, and the rate of inflation is 3 percent, then after one year it would cost you $103 to purchase the same amount of goods. You have effectively paid a 3 percent tax on holding the money.

Inflation is the direct result of government policy. It occurs because governments have the ability to print money. There is no example in history of a substantial increase in inflation that did not correspond with an increase in the quantity of money.

For example, the American government financed itself during the Civil War by printing money. Inflation grew 10 percent per month from 1861 to 1864, and the prices of all common goods skyrocketed. It was only after the war, when the government reduced the physical stock of money, that inflation subsided.

In America today, inflation is caused by a rapid growth in government spending and the mistaken policies of the Federal Reserve. The government has an incentive to create money from the printing press because this allows politicians to fund programs favored by the special interest groups who support them. And because the government controls the Federal Reserve, it can make the decision to print money without the need for citizens to vote for any tax increases.

The only cure for inflation is the reduction in monetary growth: restricting the amount of money created by government.

### 5. The modern welfare state is inefficient and routinely hurts the poor it was designed to help. 

Although the modern welfare state provides benefits for the poor and underprivileged, it fails to provide the most efficient assistance. This is because, despite the good intentions of helping the poor, when government bureaucrats take control of the purse strings, spending spirals out of control.

When money is given to bureaucrats to spend, it changes their behavior. They are spending money that's not theirs, on people other than themselves. This destroys the incentive to get the most value for the fewest dollars. In contrast, when individuals spend their own money, they have a natural incentive to get the best goods or services at the best price, even if their spending is for charity.

And the problems of the welfare state go further.

A government's attempts to redistribute income in fact disproportionately hurts the poor. Social security, for example, is a regressive tax that bears most heavily on those earning low wages. The younger and poorer pay a greater percentage of their income to these taxes, while the rich pay less and receive benefits in retirement they do not need. Astonishingly, this results in the transfer of wealth from poor to rich.

There are two further disadvantages for the poor when the government raises taxes and diverts money for charitable purposes. First, the poor have fewer political connections, which makes it nearly impossible to succeed in the political scramble for extra funds. Second, the amount of money used for welfare will be less than the amount taxed for the purpose. Funds are immediately siphoned toward legislators, political campaigns and regulators, all of which contribute nothing to the welfare of the poor.

Only human kindness or self-interest ensures that the money is spent in the most beneficial way. When people are free to spend their own money in charitable ways, they make better decisions and the outcomes are vastly more efficient.

### 6. Our education system has deteriorated as local and parental control has been replaced by federal bureaucrats. 

For the past few decades, public schools have declined in quality as costs have risen dramatically. The main reason for the drop in quality is that control of education has been taken away from local communities and centralized in Washington DC.

In the early 1800s, although school was neither compulsory nor free, it was nearly universal. Yet, in the 1840s, a movement was created not by parents but by teachers and government officials to replace the diverse system of private schools with free schools. Although they argued that government-run schools were in the "public interest," they knew that regulation would grant them higher pay, better job security, and a greater degree of control over the students and parents.

Unfortunately, this process has continued over time. More and more schools have been taken over by the state, decreasing the quality of education by producing three terrible effects: larger schools and classrooms, severe reduction in school choice, and the growth of administrative power. Distressingly, as money pours into education to "fix" these problems, it only serves to make them worse. Over the past 50 years, the number of students has increased only modestly, while the number of teachers, administrators and staff has skyrocketed.

Perhaps the saddest outcome of centralized control is that bad schools adversely affect the poor they were intended to help. With regards to schooling, wealthy parents have a number of choices. They can send their children to private school or move to a wealthier district with parent-supported schools. The poor, on the other hand, are stuck without the ability to move or to support their local school by providing extra fundraising.

As it stands today, parents and students have little say in the quality of their education even while the payrolls of teachers, administrators and staff still continue to expand.

### 7. The government’s desire to protect consumers impedes economic growth and causes prices to rise. 

The government often argues that if sellers are left to act via their own self interest they will deceive customers by overcharging them or selling them shoddy products. Governments therefore promote the implementation of rules and regulations to "protect" the consumer. Yet this often has the opposite effect: worse conditions for consumers.

Attempts to "protect the consumer" are inherently anti-growth, because complying with increasingly complex government regulations imposes heavy costs on industries, and these costs are passed on to the consumer. The rising cost of goods results in periods of slow growth, where unemployment rises and wages stagnate.

There are many historical examples of economic growth suffering when regulatory agencies are created. For example, the _New Deal_ — a package of progressive reforms in the 1930s — quickened the pace of federal intervention in the economy and was followed by slow growth. By the 1960s, there were 21 new regulatory agencies, and economic growth slowed dramatically.

Government intervention also limits competition by protecting powerful special interests.

For example, in the late 1800s competition between private railroad companies was fierce, and the industry grew and was highly innovative. However, activists and faltering railroad companies began to complain about unfair prices. They lobbied the government to intervene, which led to the creation of the Interstate Commerce Commission. This body was designed to regulate the railroad industry, but unbelievably the railroad companies themselves helped write their own regulations. They effectively placed themselves in control of their industry. Soon prices were raised and fresh competition was prevented from entering the market.

The best way to protect consumers is to increase competition and decrease regulations. With fewer regulations, competition drives down prices and requires businesses to sell better products. The very nature of competition helps protect consumers, because in a market with more choices they can easily choose to spend their money elsewhere.

### 8. Attempting to protect workers interferes with the natural protection provided by the free market. 

In the past two centuries, workers in economically advanced societies have seen their conditions improve drastically. But the government and labor unions, despite their rhetoric, are not the reason for this development.

In fact, counter to their mission to keep workers gainfully employed, unions themselves have an adverse effect on unemployment.

Because unions are granted special privileges from the government, they can give benefits to their workers at the expense of non-union workers, who are left unemployed or compelled to work for lower wages. Take the doctors union, the American Medical Association: they keep the number of doctors artificially low, which raises the cost of medical care and prevents competition by unlicensed doctors who then cannot practice.

Another goal of unions is to enforce high wages, but raising income this way is artificial and destructive.

The minimum wage in the United States is one great example. Though the minimum wage is designed to protect workers, it creates high unemployment and disproportionately affects poor black teenagers. These poor teenagers educated in sub-standard schools have no skills with which to earn a high-paying wage, yet they are prevented by the law from working for low wages, even though these positions could provide them with on-the-job training. Their job opportunities are therefore extremely limited. The minimum wage protects highly skilled workers at the expense of the less skilled.

As attempts to protect workers actually harms them by creating a shortage of both workers and employers, another approach is needed.

It is in fact better to leave the provision and price of labor to the free market. A freer market provides both workers and employers alike with natural protection from exploitation, because it expands the choice of jobs and employees. An exploited employee can easily move to another more suitable job, and employers can avoid exploitation because they have a large pool of workers to choose from.

### 9. We can regain our freedom by limiting the power of government and returning control to the people. 

The power and size of the government has continued to grow over time and has led to control over many aspects of our lives.

Unfortunately, increasing the size of the government has led to spiralling costs and less efficient spending. For example, in Washington DC, government employees are spending taxpayer money devising ways to get us to quit smoking, while on the other side of town, different government employees are spending taxpayer money to subsidize tobacco farmers. Unbelievably, we are paying huge amounts for people to write laws that effectively cancel each other out.

Another problem is that when government grows and more departments and legislation are introduced, citizens find it much harder to understand what their government is doing. This is because there are too many laws to understand and different agendas to follow. It is impossible to run a massive government without hiring bureaucrats to deal with the ever-expanding complexities, and this further separates the people and their government.

In order to return the power to the people, we must lay down broad rules covering what the government can and cannot do.

One idea is an _economic Bill of Rights_ that would serve to limit government power in economic areas. If we limit the total budget of the government, for example, this would limit the power of politicians to cater to special interest groups. Suddenly, everyone would have to compete for a piece of a pie that is fixed in size, instead of colluding with the government to make the pie bigger each year.

The greatest threat to human freedom is the concentration of power, whether that of the government or of other entities. We must remember that power granted even with good intentions can have a debilitating effect on freedom. Fortunately, we are still free to choose the direction of the country, and hopefully we will work to prevent further concentrations of power.

### 10. Final Summary 

The key message of this book is:

**Economic freedom is essential to personal freedom. Government attempts to control individuals through economic commands actually ends up hurting individuals and taking away their all important freedom of choice.**

Actionable ideas in this book:

**Voucher programs could help improve our schools.**

A voucher program in which parents are given a voucher worth the cost of their child's education would enable school choice and open a whole new range of options for the poor, who are presently stuck with crumbling schools. Vouchers would create much needed competition, forcing schools to improve in order to attract more students. 

**Support an economic Bill of Rights.**

Adding an amendment to the constitution to protect the economic rights of individuals would go a long way to ensuring that our freedoms are not continuously eroded by government regulations and spending. By limiting the federal government to a fixed budget every year rather than allowing them to forever increase spending, bureaucrats would be forced to spend taxpayer funds more wisely. Any way to increase the efficiency of tax dollar spending is good for the citizens that must pay them.
---

### Milton Friedman

Milton Friedman won the Nobel Prize for economics in 1976. He holds a PhD from Columbia University and was an economic advisor to Ronald Reagan in the 1980s. He has written over 11 books about economics and individual freedom.

