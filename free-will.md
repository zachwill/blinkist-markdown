---
id: 54b3a8596466330009270000
slug: free-will-en
published_date: 2015-01-13T00:00:00.000+00:00
author: Sam Harris
title: Free Will
subtitle: None
main_color: DE3A2C
text_color: B22428
---

# Free Will

_None_

**Sam Harris**

In _Free Will_, author Sam Harris explains that the concept of "free will" is essentially an illusion. While it might be hard to believe, what we think and what we do lies mostly out of our direct control. This book explains why exactly this is, and what implications it has for society at large.

---
### 1. What’s in it for me? Learn why the concept of “free will” is an illusion, and what to do about it. 

Are you really in control of your every action? Is the fact that you're even skimming this text something that you consciously chose to do, out of your own so-called free will?

The answer is no. Free will is an illusion, and the following blinks will explain why.

New insights from neuroscience research shows that how we think and what we do every day has very little to do with our "free will." We're not in control, and to better understand why we do what we do, it's important to better understand exactly how our minds work.

After reading these blinks, you'll learn

  * why it's your brain that's thirsty, not you;

  * why a seemingly bloodthirsty murderer doesn't consciously choose to kill; and

  * why conservative political policy is based on a scientific fallacy.

### 2. You can’t actually control what you do or why you do it. The concept of free will is an illusion. 

Here's a scenario: You realize that you're thirsty, so you decide to grab a glass of lemonade.

Where did this decision come from?

Many people would say you chose to drink a glass of lemonade out of your own free will. Free will happens when you make a decision — that is, make up your own mind — without being forced or coerced.

So if you want lemonade, and no one's threatening to kill you if you don't, you're exercising free will.

But really, you didn't decide to get that glass of lemonade — at least not consciously. Perhaps you felt thirsty, but that's a biological function that you can't directly control.

And why lemonade, exactly? And why now?

Such decisions don't originate from our conscious minds. We don't know what we intend to do _until_ we actually do it.

Researcher Benjamin Libet in his experiments found that he could detect activity in parts of peoples' brains that signaled movement _before_ they decided to move. While participants thought they controlled their own movements, their brains had already decided to move their bodies before they were even aware of it.

This means the underlying reasons behind our actions or decisions are hidden from us. The only way we could fully control our decisions would be if we had complete control over all our brain activity.

The feeling that we control our thoughts and behaviors is an illusion. Our thoughts are inspired by deep biological processes that we simply can't control, like our genetic makeup.

Think of it this way. Do you control the millions of bacteria in your body? Not at all. So why, then, should we think we are responsible for an equally random set of processes that occur in our brains?

> _"You are not controlling the storm, and you are not lost in it. You are the storm."_

### 3. While free will is an illusion, our awareness of what we think or do is something we can control. 

So if our thoughts and actions are inspired by brain processes we can't control, shouldn't we just go couch-potato and do nothing all day?

Not exactly. We do have _awareness_, and we can think deliberately.

If you realize your back hurts, you might unconsciously move in your seat to get more comfortable. You can't, however, unconsciously plan a trip to the physical therapist. This is a conscious decision you have to make yourself.

So if we're _aware_ of a sensation like pain, we can be motivated to do something about it.

This isn't exactly free will, however. The mechanics of the process that triggers the realization that you are experiencing pain and then making the decision to see a doctor are still mysterious. You didn't create the pain; you didn't create the thoughts about seeking help. Those thoughts simply appeared in your mind.

But we _do_ exercise some sort of control. Even though our actions are predetermined by biological mechanisms, our choices are still important. You can't _know_ why you wanted to choose lemonade instead of water, for example, but it still matters that you _did_.

We also have to remember that our choices have serious effects on the world, even if we don't create them. Don't start seeing every person on the street as a collection of atoms just going through a series of unconscious motions. Shattering the illusion of free will shouldn't make you fatalistic!

Instead of thinking everything is outside your control, it's better to realize what you _can_ control and influence in your life.

Consider the idea of self-defense classes, for instance. It wouldn't make sense to teach self-defense by emphasizing that an attacker is just a victim of his own subconscious mental processes. While this may be true to some extent, our decision to defend ourselves instead is the important part.

### 4. With no free will, we need to rethink how we treat criminals and how we conduct social policies. 

The idea that we have no free will holds major implications for society, especially regarding how we deal with crime as well as how we form public policy.

Our general understanding of morality depends on people having the ability to make decisions for themselves. If someone knows something is bad but chooses to do it anyway, we judge her actions as wrong. If a psychopath kills someone "for fun," we think he should be punished.

But as we know the psychopath has no free will, shouldn't this change our perception of appropriate punishment?

It's still logical to incarcerate someone who may threaten the safety of individuals in society. Yet it's immoral to incarcerate someone — even a bloodthirsty criminal — for being born unlucky.

We need to look at criminality and criminals differently. Imagine if a "normal" person had a brain tumor that was affecting her behavior, by making her more violent and impulsive.

Would you hold her responsible for her actions?

Criminals aren't much different. The psychopath who kills for fun and the person with a cancerous tumor both lack free will. So instead of seeking justice through punishment or retribution, we as a society should focus on deterrence and rehabilitation.

What does the absence of free will mean for politics? This primarily affects politicians who refuse to accept that people lack conscious control over their own lives. In the United States at least, this mind-set is usually found with people who identify as politically conservative.

Such politicians believe that a person has full control over their actions and can determine any future they want. They don't acknowledge the existence of simple luck inherent in anyone's success.

Even those "self-made millionaires" that conservative pundits love to lionize are still born with a unique genetic makeup, had particular unique experiences and perhaps even privileges that helped them get where they are — a series of events that conservatives often refuse to acknowledge.

Politicians have to demand change when possible, yet chart another course to serve the public when change is impossible or ineffective. Crucially, they need to understand that we all lack free will and so need to try to help society accordingly.

> _"If we could incarcerate earthquakes and hurricanes for their crimes, we would build prisons for them as well."_

### 5. Final summary 

The key message in this book:

**Our thoughts and decisions are determined by subconscious mental processes we don't control. This means simply that the concept of "free will" is a farce. We may** ** _feel_** **like we're in control, but we actually aren't. Because of this, we need to adapt our society so we can live together and help each other in the most effective way possible.**

**Suggested further reading: _The Moral Landscape_ by Sam Harris**

These blinks explore cutting-edge research in neuroscience and philosophy to explain human morality. Harris argues that morality can best be explored using scientific inquiry, rather than relying on religious dogma or theology.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Sam Harris

American Sam Harris is the cofounder and CEO of Project Reason, a nonprofit organization that works to spread scientific knowledge and secular values in society. A prolific writer and author, he received the 2005 PEN Award for Nonfiction for his work, _The End of Faith._

