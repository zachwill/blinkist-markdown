---
id: 528a32b9373263000c1a0000
slug: free-en
published_date: 2013-11-20T09:04:53.844+00:00
author: Chris Anderson
title: Free
subtitle: How Today's Smartest Businesses Profit by Giving Something for Nothing
main_color: E02D40
text_color: B22428
---

# Free

_How Today's Smartest Businesses Profit by Giving Something for Nothing_

**Chris Anderson**

_Free (2010)_ explains how, with the advent of the digital marketplace, our understanding of traditional economics is turned on its head. It shows how twenty-first century business has changed in an unprecedented way, and how in order to succeed we need to totally rethink our understanding of the flow of money. It explains why we need to let go of our old beliefs and accept this new business model, where free must be the starting point. Most importantly, the author shows how there are still plenty of ways to make money from 'free'.

---
### 1. Companies used to think that if they kept their product scarce, they could make more money. 

In the 1930s, record labels faced a problem: as the radio became a household item, hundreds of thousands of people started listening to music for free on the airwaves rather than paying for live shows and buying records. The labels' response? They actually tried to ban the radio stations from playing their records.

This reflects the twentieth-century perspective on free products. Businesses worried that if their product was available for free in some form, like music played on the radio, consumers would have no interest for paying for it in another form, such as live shows.

By trying to stop people from satisfying their craving for music by listening to the radio, they were hoping to protect demand for the live shows. To keep products scarce in this way was considered good business sense; they controlled supply to protect demand.

This policy wasn't limited to record companies. Many other industries felt the same way, and one of the most important tools they used to create scarcity was _copyright_. Copyrighting a product prevents other companies from producing it, thereby holding off the competition. When Microsoft released their Office software, their copyright gave them a monopoly, allowing them to charge a whopping $300 per disk. As a consumer who wanted office software, you had to pay whatever Microsoft asked.

Scarcity is artificially created by ensuring consumer demand is never fully satisfied and competition is held at bay. This was the way of thinking that dominated business in the twentieth century.

**Companies used to think that if they kept their product scarce, they could make more money.**

### 2. There’s no such thing as a free lunch – you always pay for “free” items one way or another. 

In the nineteenth century, New Orleans saloon bars struggled to attract drinkers during the daytime. So they began to offer free lunches, luring in customers who would pay for drinks. This practice spawned the saying "There's no such thing as a free lunch": even supposedly "free" lunches are actually paid for in drinks.

Giving items away free has always been a popular marketing gimmick.

But if you think about the free items you've been offered in your lifetime, say a recipe book or a razor, you'll notice that they don't work alone, they always oblige you to buy something else. The free razor is useless unless you buy some Gillette blades, and you can't use the free Jello recipe book unless you buy some Jello. The cost of the free product is simply moved to another item, which must be paid for. The free item is known as a _loss leader_ : though sold at a loss, it leads the consumer to pay for other products that subsidize it. Sometimes the subsidy can also take the form of a subscription. A mobile operator, for example, may give you a free phone but charge you a monthly fee to use it.

In some cases it is not you, the consumer, who ends up directly paying for the free item but rather a third party. This is how free media, newspapers, magazines and radio work. Advertisers subsidize the broadcaster or publisher and in return you are exposed to advertisements. It is merely a different form of "cross subsidy."

When it comes to supposedly free products, someone always pays in one way or another, whether it's you buying another product (the Jell-O), you paying for it later and over time (the phone fees) or by a third party paying to get your attention (the free newspaper).

**There's no such thing as a free lunch — you always pay for "free" items one way or another.**

### 3. For companies, free giveaways can be problematic: consumers love free things but don’t tend to value them. 

Everyone knows the allure of a free item. Say you're offered a free cereal bar. Most likely you won't even think twice about taking it, hungry or not, but just grab it.For consumers, "free" is irresistible because there's absolutely no financial risk involved: even if the product turns out to be disappointing, you lose nothing.

Companies can exploit this psychology in their marketing: when they give away a free product, they know there'll be a surge in consumer interest.

However, this strategy comes at a cost: lowered consumer appreciation. Because the consumer didn't invest any money or decision time into the free product, he or she will likely perceive it as less valuable and less desirable.

This means the free cereal bars will be taken but probably only half eaten.

The lack of appreciation for free items stems from the fact that acquiring them demands no mental effort. As soon as a consumer has to hand over money for something, it means they must go through a mental decision-making process, weighing up the risks and making a choice. This effort makes people value what they buy.

This effect also explains why advertising space in paid-for magazines is five times as expensive as space in free publications; if the readers _chose_ to pay for the content, they will therefore hold it in higher regard and be more receptive to the advertisements. On the other hand, far fewer people will buy a magazine than take a free one.

In the twentieth century, this trade-off was an inescapable part of marketing: giving products away for free meant a huge audience but low commitment; charging a price for products meant high commitment but low participation by consumers.

**For companies, free giveaways can be problematic: consumers love free things but don't tend to value them** **.**

### 4. The price of digital products will inevitably drop to zero because they can be produced and distributed at no additional cost. 

All physical products cost something to produce. This is because they are made of some physical material, like wood or plastic, which necessarily carries a cost.

_Digital_ products, however, are fundamentally different — they're made of _bits_ : pieces of information that form to make digital content such as software and music files. Because of this, digital products need not necessarily cost anything at all, and in fact their price will eventually reach zero.

Why?

First of all, the technology needed to store, transmit and process bits cheaply has greatly advanced over the past decades. Earlier, a device like an iPod that can store tens of thousands of songs would have been unimaginable even for a millionaire, but today nearly everyone can afford one. Similarly, your Internet connection is faster and cheaper than ever before.

Second, because digital products consist of bits, you can _reproduce_ them infinitely with zero additional cost. It does not matter whether you make and supply one or one million copies — the cost is the same.

Finally, the Internet has provided a free and competitive marketplace in which to sell digital products. It costs nothing to distribute them online and there are no powerful retailers to cajole for shelf space. Apple's App Store, for example, has enabled thousands of individual programmers to sell their products to anyone with an Internet connection. This has created a highly competitive market where companies must keep their prices as low as possible: close to the marginal cost of producing a digital product.

These three factors — cheaper technological capabilities, zero marginal costs, and the free and competitive online marketplace — will slowly but surely drive prices of digital products down to zero.

**The price of digital products will inevitably drop to zero because they can be produced and distributed at no additional cost.**

### 5. The Internet has created a new gift economy where content is free and its value is measured by the attention it gets. 

If you've ever written or read a blog, a Wikipedia article or an Internet Movie Database (IMDb) entry, you will know that writing articles is no longer just for journalists. The Internet provides a platform enabling millions of amateur writers to create and share content, linking enthusiasts with their interests. For example, a film aficionado may be disappointed at the lack of like-minded people in his small town, but when he contributes to IMDb, his in-depth knowledge of films and filmmakers can reach millions of his peers. But crucially, nobody pays for the content he creates; the author's only reward is the satisfaction he or she gets. In this newly emergent online _gift_ economy, content is created and shared with no financial gain.

Normally, people's willingness to pay for something indicates its value, but in a gift economy this no longer applies. One blog post does not differ from another in its monetary worth: they are both free.

Yet in every "economy" things become valuable when there are not enough of them and they become scarce. In the online gift economy, there is so much content and information freely available and easily accessible that what has become scarce is _attention_ for that content. A blog post that no one reads is worth nothing. Thus, in the gift economy, the value of something is indicated by the attention afforded to it — more specifically, the "likes," "followers," "ratings" and "retweets" it gets.

In such an economy, there is more to life than money; it really is possible to create and receive something for absolutely nothing.

**The Internet has created a new gift economy where content is free and its value is measured by the attention it gets.**

### 6. Piracy is a fact of life: either take advantage of it or negate it by releasing your product for free. 

The 21-year-old Chinese pop star Xiang Xiang is a sensation: she has sold over four million albums. The catch? Not one album was paid for; they were all pirated.

Piracy, the illegal and unauthorized copying and distribution of a product, is an unintentional form of "free" which has emerged out of the twenty-first-century marketplace. Because digital products can be copied at very little cost, consumers today can often easily obtain pirated copies of music, videos and software for free online.

This is especially true in China, where around 95% of all music sold is pirated. Chinese musicians have learned to accept it because it actually turned out to be free marketing that helped them expand their fan base.

For Xiang Xiang, exposure has come in an unlikely form but she's still profited from it. Those four million people who pirated her albums are now dedicated fans, meaning she can profit from personal appearances, product endorsements and concert tours.

Though in the West artists are still fighting piracy, some have found a way of disarming it by offering their own products for free as well. Monty Python are a perfect example: They were sick of low quality versions of their work being pirated and uploaded on Youtube, so they decided to release all of it for free themselves. This tactic not only quelled piracy but also actually benefited Monty Python: three months later their DVD's topped bestseller lists because the free, high-quality clips online had stirred demand for the original product.

Dealing with piracy is an inescapable part of producing digital products, but savvy companies and artists find ways to either disarm pirates or even profit from them.

**Piracy is a fact of life: either take advantage of it or negate it by releasing your product for free.**

### 7. Offer your product for free to build an audience and then study that audience to find out what they’ll pay for. 

Most people know Google as an innovative company. Few know, however, that at Google HQ, employees are actually paid to come up with new products to offer consumers for _free_. The goal is not to make money by selling the products but rather to build a large user base by offering their main products free.

In doing this, Google has secured a vast audience of participating consumers who use, for example, Google Maps, Google Mail and Google Docs. Because the products are free, the audience allows Google to collect information about their lifestyles, interests and online habits, and Google uses that information to develop even more products. Because the products are based on extensive consumer research, many of them then turn out to be hits.

So where does Google actually make money? They take that huge audience they have built and advertise to them, generating some $20 billion in revenues, even though most of their products are free.

This principle — growing a customer base to be able to design better products and make money later on — can be used by digital companies of all sizes. They can offer free products to gain access to consumers to gather data, try out new ideas and see how they are received. The next step is to work out what people will actually pay for.

Smart digital companies build their business model around "free" because they realize that popularity is just as important as revenue. The audience gained with "free" can help you find out what to charge for.

**Offer your product for free to build an audience and then study that audience to find out what they'll pay for.**

### 8. Once you have built up an audience with a free version of your product, you can offer a premium version at a cost. 

What do Google Earth, Spotify and Flickr have in common? They all offer two versions: one free and one at a price that includes extra features. You can pay for unlimited storage for your photos on Flickr, for music uninterrupted by advertisements on Spotify or for a professional-level 3D view of the earth on Google Earth.

These companies understand that to build a large audience, they must offer their basic product for free. This means they must offer something extra on top of that free version to be able to charge for it. In these cases, what they can charge for is added storage, lack of adverts or additional features.

Another way to profit from two different versions is by customizing the paid-for version. For example, the author released this book for free as a digital download to gain popularity and reach a broad audience, but he charges for consultations where he provides his tailored expertise in person.

Arduino, an open-source hardware company, goes one step further. As well as offering a finished version of their hardware at a price, they released its blueprints for free, encouraging consumers to try constructing it themselves and then sharing experiences and tips on a digital forum. Some tried to build the devices with the free blueprints, but Arduino knew that most were happy to pay to save some time and get the already-assembled product.

From the companies' perspective, offering both free and paid-for versions allows them to build a large audience and then profit; for a customer, the free version is a great way to get to know the product before making an informed decision to upgrade.

**Once you have built up an audience with a free version of your product, you can offer a premium version at a cost.**

### 9. Final summary 

The main message in this book is:

**The twentieth-century concept of "free" was a marketing gimmick; there was always a cost. The digital revolution has created products that really are free; there's no catch and all companies will eventually have to follow suit. The best way to succeed in this new market is to release your basic product for free and charge for a premium version.**

This book in blinks answered the following questions:

**How does "free" work in a traditional market?**

  * Companies used to think that if they kept their products scarce, they could make more money.

  * There's no such thing as a free lunch — you always pay for "free" items one way or another.

  * For companies, free giveaways can be problematic: consumers love free things but don't tend to value them.

**How has the digital revolution created new concepts of "free"?**

  * The price of digital products will inevitably drop to zero because they can be produced and distributed at no additional cost.

  * The Internet has created a gift economy where content is free and its value is measured by the attention it gets.

**How can a business take advantage of "free" in this changing landscape?**

  * Piracy is a fact of life: either take advantage of it or negate it by releasing your product for free.

  * Offer your product for free to build an audience and then study that audience to find out what they'll pay for.

Once you have built up an audience with a free version of your product you can offer a premium version at a cost.

### 10. Actionable ideas from this book in blinks 

**Consider offering a superior version of your main product, which you charge for, while the free version acts as the marketing.**

Most businesses and artists worry that offering any version of their product for free will damage sales. On the contrary: offering your main product for free can bring you vast exposure. For example, say you have written a fantastic book on marketing. You could try to sell the book itself online, and probably make a bit of money. If, however, you offer it for free instead, it will likely reach a lot more readers. Those readers in turn may be so excited by your ideas that they invite you to consult them on their own marketing strategy, for which you can charge a fee.
---

### Chris Anderson

Chris Anderson is the author of three successful business books, has been editor in chief at Wired Magazine and editor at The Economist, and is also an enthusiastic entrepreneur. Passionate about remote-controlled technology and hardware, Chris founded an open-source 3D robotics company, for which he famously offers the instruction manuals free online. In 2007, Time Magazine named Chris as one of the Top 100 thinkers. He lives in Berkeley, California, with his wife and five children, with whom he spends his weekends trying to build flying robots!

