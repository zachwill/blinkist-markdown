---
id: 5d21171f6cee07000704d9c9
slug: from-here-to-financial-happiness-en
published_date: 2019-07-09T00:00:00.000+00:00
author: Jonathan Clements
title: From Here to Financial Happiness
subtitle: Enrich Your Life in Just 77 Days
main_color: 517FB1
text_color: 395A7D
---

# From Here to Financial Happiness

_Enrich Your Life in Just 77 Days_

**Jonathan Clements**

_From Here to Financial Happiness_ (2018) presents you with simple tools to help overcome anxiety about your finances, showing you how best to put aside enough money to lead a comfortable life and for retirement.

---
### 1. What’s in it for me? Shore up your finances and prepare for a comfortable life. 

Financial stability. To say that everyone wants it would be to point out the painfully obvious. But too often it can seem like a distant dream. In reality, though, the desire to have enough money to lead a comfortable life — and enough to see you through retirement — is fulfillable. It's also probably a lot easier to achieve than you think.

In this book-in-blinks, we've boiled _From Here to Financial Happiness_ 's 77 chapters down to their essentials. You'll learn what it takes to set you on the path to not just earning more, but making yourself secure as you do so. It might seem overwhelming, but break down the task into its components and it really isn't so scary.

In this book-in-blinks you'll learn

  * why regularly using credit cards might not be so bad;

  * how much to set aside for retirement; and

  * why you can blame your impulse purchases on your ancestors.

### 2. There are simple routes to healthier finances, and it all begins with understanding “saving.” 

You don't have to be a financial advisor to know that no two earners are the same. That means that financial advice is never universally true. However, there are some simple rules that apply to everyone, and it's these general principles you should follow — if you aren't already doing so, that is!

Before we get down to business, let's cover one of the basics: _compound interest_.

There's a good reason saving money makes sense: the amount will rapidly increase thanks to a phenomenon known as _compounding_.

So, say you have savings of $1000 earning 6 percent interest every year. Without compounding, you'd only receive $60 interest every year. You'd only get up to a paltry $2800 after thirty years.

But thanks to compounding, the interest you earn each year gets added to the base savings amount. It's from this new amount that your next year's interest is then calculated.

That means that, after 30 years of compounding, you'd end up with $5,743 rather than only $2,800.

The lesson here is clear. Start saving and start earning that interest as soon as possible. It really is worth it.

Now that's out the way, let's turn to those simple rules for healthier finances.

If you're an employee, the first thing to do is to make sure your employer's retirement savings plan is working for you.

Typically, for every dollar you contribute to your retirement plan, your employer will give 50 cents, for up to 6 percent of your salary.

So if you contribute the full 6 percent of your salary to the retirement plan, your employer's contribution will knock that up to 9 percent. Over time, that starts to add up.

The second basic rule is never to carry debt on your credit card. That's because credit card companies often charge up to 20 percent interest on unpaid balances.

It might seem obvious, but it's an easy thing to forget. Pay off your card, otherwise, you'll be quite literally throwing money away.

We're off to a good start, but now let's move to the next blink, where we'll examine even better reasons for saving money.

> _"Things that make us feel good today — spending, eating junk food, boozing it up — often leave us feeling worse tomorrow."_

### 3. Prepare a financial safety net in case of unemployment. 

If there's one thing that keeps people from sleeping at night, it's sure to be financial worries. And that's understandable _–_ living from paycheck to paycheck is a recipe for stress.

That's why it's critical that you try your hardest to set up an emergency fund.

Ideally, you should have enough in an emergency fund to cover three to six months of average living expenses. Be sure, also, to keep this money out of easy reach — preferably in a savings account or in the form of very safe investments.

The main reason for having a fund such as this is so that you have something to fall back on in case you lose your job. In contrast, if your car or washing machine were to break down, you'd most likely be able to scrape that money together from somewhere.

But losing your job is different — losing your major source of income means that you'll need something to see you through while you look for new employment.

You also need to bear in mind that, if your line of work is quite specialized, you're going to need longer to find a job that fits your specific skill set; simply put, an executive is going to need more time for the job hunt than a waiter. That means you're going to need a larger emergency fund to see you through. 

Thankfully, it's not that complicated to create an emergency fund. You just need to follow a few basic rules.

First off, look at your monthly living costs and work out how big you'll need your emergency fund to be.

Then, apply for a high-yield savings account.

Finally, set up your checking account so that it pays an automatic contribution to your new savings account every month. Keep this going until you hit your goal.

This method is the easiest way to get your emergency fund growing and ensures that you won't be spending your liquid assets on too many luxuries like theatre tickets or fancy clothes.

The other thing to consider is which expenses you should immediately cut out if you hit hard times. Cutting out restaurants is an obvious solution, but you might even need to consider moving to a cheaper apartment if financial issues seem likely to go on for a while.

> _"If we want to feel better about our finances today, we should spend more time thinking about how we'll pay for tomorrow."_

### 4. Deep-seated evolutionary behavior can result in bad financial decision-making – but good habits can overcome this. 

If we lived our lives meticulously evaluating every decision along the way, making decisions at all would quickly become utterly exhausting. That's why humans have evolved to operate and react _instinctively_ to the world around them on a day-to-day basis.

That's all well and good, but unfortunately some of our ingrained instincts aren't such great news for our wallets.

Just consider one habit that's been passed down in our genes. To fully utilize resources, our nomadic ancestors had to forage for food and supplies whenever they had the opportunity. This was necessary for survival in an environment where food and shelter were hard to come by. But we're still evolutionarily conditioned to do the same, and it's those same instincts that lead us to overindulge.

In a similar vein, our deeply lodged survival instincts mean that we have also come to overvalue hard work as a positive attribute. That's because our ancestors had to expend huge quantities of energy on hunting and foraging, making them believe in a direct relationship between effort and success. 

Now, working hard is, of course, generally a good thing. But if the result is thoughtless over-enthusiasm, then we risk making poor investments. There are plenty of people, for example, who've started their own business without proper planning and backing, only for it to end in disaster.

So it's a good idea to counter those misplaced evolutionary impulses that lead to inadequate planning by checking that your finances are in good shape on a regular basis.

To do this, you have to first calculate your fixed monthly expenses. This includes things like rent, a mortgage and utilities. These costs should not exceed 50 percent of your total income before taxes.

After that, add up your fixed expenses, your monthly tax contributions and the amount you're setting aside to save each month.

As a rule of thumb, at least 12 percent of your income should be going toward your retirement.

Now, subtract that combined overall total from your pretax income. What's left is what you can use for hobbies, vacation and the like. It's up to you!

### 5. Frugality results in better finances as well as in improved health. 

If you started in on this book-in-blinks hoping to discover a secret to becoming an overnight millionaire, you're going to be sorely disappointed.

The thing is, there is no secret to financial success — it's nothing more than developing a good set of habits. You just have to learn and implement those habits.

An essential component to maintaining sound finances is frugality. You have to keep your expenses low. This can mean anything from getting a less expensive internet package to ditching your gas-guzzling car. But you have to begin somewhere.

In addition to this, you have to curb your spending on unnecessary items. These are luxuries that you may not benefit from in the long-term.

So there's no need for a designer couch when a secondhand one will do. And, as for that new hobby you've started, maybe it's better to wait before splurging on that oh-so-essential surfing gear. Money often gets wasted on impulse buys, so think good and hard about what you're doing before you indulge.

There's also another good reason to be frugal. Odd though it may seem, thriftiness can improve your health.

That's because poor spending habits are often linked to things that are bad for us. These include gambling and drugs such as alcohol, coffee or cigarettes. All of these are addictive in their own way.

Now, take a moment to pause, and think about your guilty pleasures and their monetary repercussions. If you often eat more than is healthy, calculate the amount it costs you each day to indulge. And consider the fact that when it comes to smoking, you're basically burning your money away each day.

Sit down and do the math: how much goes out on these unhealthy habits over the course of a year?

Once you face the fact that you're throwing away thousands of dollars each year on unhealthy habits, you might have the incentive you need to finally quit.

### 6. Insurance can be important, but it isn’t always necessary. 

To an outsider, insurance and insurance policies can seem incomprehensible and confounding. It sometimes comes across as a lot of administrative and technical jargon.

But at heart, insurance is both practical and pretty simple. It's just a way to pool risk.

Policyholders contribute money to a pool. This means that if one of the holders gets unlucky, the costs of the misfortune are borne collectively from that pool.

However, not all insurance policies are necessary. 

If you're a 40-year old professional with a spouse and children, life insurance is a sound idea. If you die early, your family will undoubtedly benefit from any insurance payout and may be able to live a more comfortable life.

But once you've hit retirement age and your kids are all grown up, paying into a life insurance scheme makes no sense.

This model of assessment applies to all your insurance decisions.

There are many different types of insurance out there, and very few that are actually applicable to you.

For instance, if you're a young professional, you shouldn't go without disability insurance. In the event of accident or illness, who's going to pay the bills when you can't work? Social Security support takes a lot of work to apply for, and you might be waiting months for that first payment to come through. Short-term or long-term disability insurance is, therefore, worth having.

However, your employer may already be paying for your insurance disability costs. In that case, it's totally unnecessary to buy your own additional disability insurance.

Equally, if you're close to retiring and have upwards of $1 million in your savings account, you can probably do without disability insurance.

In those circumstances, should you become disabled, you'll have enough to cover the extra costs of disability without putting your finances under too much strain. Just remember that each case is individual, and you'll have to judge it for yourself.

### 7. Cars can drain money away, but, thankfully, there are measures you can take against this. 

Think of a purchase that symbolizes career success and what will likely come to mind is an expensive and luxurious car. Sure, it's a status symbol, but for many the experience of driving a first-class vehicle is simply a lot of fun.

As a result, it's all too easy to overspend on them! For decades, the Bureau of Labor Statistics has been tracking how American families spend their money, and their data illustrates this point: A whopping 16 percent of the average American family's budget goes to transportation - which, in the US, usually means cars.

That spending lags in second place, notably, behind housing, which makes up an average of 33 percent of a family's expenses.

Spending too much on driving is clearly a risk. But it's relatively simple to work out if you're overdoing it.

First off, work out the annual cost of your car. Add up what you spend on it, whether that's paying for it in installments, gas, insurance, repairs or registration.

You should now compare that total with what you make each year. If more than 15 percent of your budget goes toward costs associated with your car, then you're definitely overspending.

And, of course, if you're throwing away money on your vehicle, that means less is going toward more important items — like your savings.

So, what should you do if you're over that 15 percent threshold? Thankfully, there are some simple enough solutions.

First off, take care when the time comes to replace your current vehicle. New cars are expensive, but their value depreciates rapidly during the first two or three years. If you buy a slightly older secondhand car, you can sometimes snag yourself a pretty good deal. It just takes some patience.

The further advantage of owning a second-hand vehicle that isn't worth nearly as much as a new one is that your insurance costs won't be as high.

With that said, don't switch cars too often, even if the new one runs cheaper: you'll still have to dish up the cash for sales taxes and new registration fees.

Ideally, then, you should be on the lookout for a second-hand vehicle that's about three years old and has fewer than 30,000 miles on the clock.

### 8. Increase your wealth through everyday prudence, and by cautiously purchasing company stock. 

If you ask anyone about the best methods of increasing wealth, they'll most likely suggest you look for solid investment opportunities. High-yield savings accounts for retirement are often what spring to mind.

But investing isn't the only way of securing your wealth.

To keep your finances healthy, you have to think about how you handle your cash on an everyday basis.

First off, take advantage of the rewards that some credit cards offer. When you spend money, use them. Lots of cards give you cash back once you've reached a certain threshold. Others reward you with travel points.

Needless to say, don't fall into the trap of spending more just for the points!

Another thing to keep an eye on is your checking account. These are low interest, so there's no point leaving a ton of money in there. Have enough there to cover your daily costs, but anything above that needs to be dispatched into a savings account.

There's a double benefit to transferring that excess checking account money: you'll both be earning interest on it and less likely to spend it.

A savings account is a great start, but that earned interest soon gets eaten away by taxes; a savings account will make you richer, but not significantly so.

If you really want your money to work for you, you'll have to buy some company stock. This simply means that you become a partial owner of a business, as you own a stake in it.

But buying stocks is really only an option if you already have robust savings, like a healthy pension and emergency fund. Simply put, if you're the kind of person who gets into financial difficulties, then you might need to sell off your stocks in a rush. And if you do that at a point when the company is experiencing a slump, you're going to lose a lot of money overall.

However, if you're able to support yourself and still buy company stock on the side, then it's certainly worth it. Stocks can have relatively high returns and, if done right, you'll be increasing your wealth before you know it.

### 9. Planning for your retirement means saving now and looking forward to the future. 

Usually, when people get to thinking about money, their thoughts head to the next big thing. That could be putting a down-payment on a house, buying a car or setting up a college fund for the kids.

But your first financial priority should actually be about what comes last. At the end of your professional life comes retirement.

The reason for that is plain: retirement isn't optional — not like buying a house! Almost everyone is going to retire, and the gap between retirement and death can be very many years indeed.

So there's no reason to put off saving for retirement. If you wait too long, you'll need to put aside an awful lot towards your retirement fund for it to function as it should. If you wait as long as your late thirties or early forties, we're talking around 20 percent of your monthly earnings. And that's a lot of financial stress.

It doesn't matter if you have student debt, and it makes no difference if you want to save money for a property investment — what comes first should be your retirement plan.

Start by paying at least 12 percent of your pretax income toward your plan every month. If that means cutting costs elsewhere, those we looked at earlier, you'll just have to do that.

But of course, saving for your retirement is only one part of the equation. You have to work out what you'll actually do during that time.

It might sound great to be a retiree who goes to the beach or lounges in the sun. But after only a few weeks, that kind of life can get pretty dull. It can even lead to depression.

So plot out ahead of time which hobbies you'd really like to get involved in when you've got more time on your hands.

Ideally, these should be activities that you lose yourself in, where time just flows.

So, for, example, if you think playing an instrument might be what you need in retirement, you should start practicing now. By the time you retire, you'll be proficient enough to enjoy it fully. It will feel like a constructive use of your time and, more than this, you'll have less chance of getting frustrated by learning an entirely new skill at an advanced age. 

So now you've got the lowdown. You now know how to increase your financial happiness, how to plan ahead and how to put aside what you need for your savings. Whether it's an emergency fund or planning for major expenses, you know how to approach the challenge and keep yourself financially happy.

### 10. Final summary 

The key message in these blinks:

**The secret to financial happiness is simple, but it isn't always easy to put into practice. At its most basic, it's about saving your money — whether by reducing your expenses or putting a set amount into a high-yield account every month. Thankfully, you can achieve this. By reducing your fixed costs, such as your car expenses, giving some thought to your insurance policies and living frugally, you can soon start to amass a healthy stash of money. If you're then savvy about managing your cash, and invest extra wealth in stock, you can put your money to work for you. Done right, you'll have not just enough for a comfortable life but enough to last you through retirement, too.**

Actionable advice:

**Automate your monthly payments.**

Set up your e-banking account so that payments for rent, insurance and all regular bills are automated. Automatic transfers force you to be realistic about your finances, but they also ensure you don't fall behind on your credit card payments, which would negatively affect your credit score. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Sam Walton: Made in America_** **, by Sam Walton with John Huey**

In this book-in-blinks you've gotten to grips with the basics of securing financial stability. But what you probably need now is a little bit of inspiration — something to aspire too. Saving is a wise habit, but another clear route to financial stability is entrepreneurial success. 

So learn the story of one of the most successful businessmen in the United States, and how he went from rags to riches. _Sam Walton: Made in America_ tells the inspirational story of Sam Walton, who founded what would become one of the biggest companies in the world: Walmart.
---

### Jonathan Clements

Jonathan Clements is a financial planner who was previously a financial consultant at Citigroup, the global investment bank. He has been a frequent contributor to _the Wall Street Journal_ and is the author of several other books, including _How to Think About Money_ (2016). He is also the founder of HumbleDollar.com, a financial guidance website.

© Jonathan Clements: From Here to Financial Happiness copyright 2018, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

