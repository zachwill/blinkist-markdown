---
id: 5899943695ee030004e5dd15
slug: from-silk-to-silicon-en
published_date: 2017-02-09T00:00:00.000+00:00
author: Jeffrey E. Garten
title: From Silk to Silicon
subtitle: The Story of Globalization Through Ten Extraordinary Lives
main_color: FBE8C1
text_color: 706856
---

# From Silk to Silicon

_The Story of Globalization Through Ten Extraordinary Lives_

**Jeffrey E. Garten**

_From Silk to Silicon_ (2016) tells the stories of several key figures who influenced the globalization of the world economy, from Andrew Grove to Genghis Khan. These blinks take you through centuries of history to meet the major players who shaped the development of human societies, employing everything from unbridled free trade to iron-fisted authoritarian rule.

---
### 1. What’s in it for me? Learn how to become a global player. 

Human societies as we know them didn't appear overnight. Over the course of centuries, great politicians, entrepreneurs and explorers fashioned the best systems for human beings to live in and thrive.

From the emperor Genghis Khan to the British East India Company, from the famous Rothschild banking family to business magnates such as John D. Rockefeller, let's take a closer look at the big players of world economic history — the people who set the stage for the globalized, interconnected world we know today.

In these blinks, you'll learn

  * about the bank that had more power than all of today's major banks combined;

  * how brutal conquerors and politicians also bring about prosperity and progress; and

  * how China became a global power poised to rival the United States.

### 2. The great conqueror Genghis Khan launched the first golden age of globalization. 

Temüjin, more commonly known as Genghis Khan, was one of history's most famous emperors. Living during the twelfth and thirteenth centuries, his military conquests helped the Mongolian Empire grow to include nearly 20 percent of the world's land mass. But this obviously didn't happen without a fight.

Genghis Khan was an especially brutal commander, and his troops murdered tens of thousands of people. In this way, he differed from many of his contemporaries.

For instance, when conquering a city or village, many emperors would give people the option to live under their rule. But Genghis Khan tended to go a different route: he would kill as many people as was necessary to crush the existing social hierarchy and establish his own rules without resistance.

Word of this practice spread quickly and many believed that he'd kill every grown man in a village if it could help him advance his goals even a little bit.

So, while Genghis Khan was indeed a violent war monger, he nevertheless established a firm and highly effective administration. Meanwhile, he went to work improving the Silk Road, the world's most famous trading route, which stretched from East Asia to Europe and marked one of the first steps toward globalized trade.

The Silk Road also played a key role in consolidating his immense empire. Khan established trading posts on the route, encouraging people from all over his empire to travel and trade knowledge and innovations. He used the resulting information exchange to increase his military power by incorporating knowledge from the Mongol, Islamic and Chinese armies.

Beyond that, he was a profound believer in religious freedom and abolished many of the antiquated social structures present in the territories he conquered. This, in turn, served his own expansionist interests: people were no longer granted high social status because of wealth or family ties, but because they were educated and thus useful to the ever-expanding empire.

### 3. During the fifteenth century, a Portuguese prince discovered much of the world – and established the slave trade. 

Prince Henry of Portugal, also known as _The Navigator_, lived from 1394 to 1460, during which time he spread European knowledge around the globe. To do so, he pushed his captains to sail far beyond the limits of contemporary maps, eventually discovering new fortunes and lands.

But exploring the uncharted regions of the world wasn't all he did. As a younger man, Prince Henry dabbled in warfare, albeit with mixed results.

In 1415, Henry managed to conquer a key Moroccan trading center, the city of Ceuta. But as he pushed further into African and Islamic territories, it quickly became evident that his military prowess couldn't match that of his opponents.

For instance, when he attacked Tangier in 1437, the city was well defended. His army was not only pushed back and surrounded in a matter of days, but his brother Fernando was taken hostage.

He tasted victory and suffered defeat, but Prince Henry's true legacy lies in his exploration and exploitation of the African continent. After facing harsh criticism from Portugal's high society, Henry turned his attention to exploring unknown parts of the world.

One of his more important accomplishments came when he ordered his men to sail beyond the _Bulging Cape_, also known as Cape Bojador, on the northern coast of what is today Western Sahara. The order was a bold one, since the area was feared by superstitious sailors who refused to go beyond this point.

His contribution to new and indispensable maritime knowledge paved the way for Columbus to discover the Americas and for Vasco de Gama to chart a maritime route to India.

Yet despite such bold decisions, his expensive explorations yielded only modest financial rewards for years. While Henry was driven by a deep religious need to convert people to Christianity, he also needed a way to pay for such ventures. In Africa, he found a solution — and a horrific one at that.

He became the first person to establish slave trading, importing somewhere around 15,000 to 20,000 African slaves into Portugal.

### 4. The British East India Company’s immense power came to rival that of the United Kingdom itself. 

To this day, the British East India Company evokes an image of tremendous power and, during its heyday, it was so important that it was known simply as _The Company_. This international trade powerhouse once employed tens of thousands of British as well as Indian citizens and, while many people contributed to its meteoric rise, no one played as active a role as Sir Robert Clive.

While most people know of India as a former British colony, that only happened after a long struggle between French, Indian and British forces. In the 1740s, as these events were unfolding, Robert Clive was living a tumultuous life in England, burdened by depression and a disapproving father.

When he arrived in India as a teenager in 1744, he came with nothing but debt and the ragged clothes on his back. But his luck would soon turn.

After joining the army and becoming a merchant soldier, he helped win impossibly difficult battles for the British, helping to build an administrative empire for his country and a small fortune for himself. The vast majority of this wealth came from plundering Indian riches and maximizing agricultural profits at the expense of the local population.

Two of Clive's most important victories were at the battles of Arcut and Passay. These triumphs paved the way for the East India Company to gain power over huge swathes of Indian territory.

As a result of such conquests, the East India Company became so powerful that it ruled India for several decades. Even after Clive's death, the company's power and influence continued to grow, and it came to control major ports, ships and even insurance companies.

The Company was not only one of the largest sources of income for wealthy Brits at the time, it also collected taxes from India and maintained an army of over 100,000 troops. In fact, by 1757, it had become so powerful that the British parliament became wary of its influence as an independent entity. To keep it in check, an act of parliament established Britain's sovereign authority over the company and brought its administration firmly under control of the British crown.

### 5. Amsel Rothschild changed the face of global banking, while Cyrus Field enabled transatlantic communication. 

While banking giants and international finance organizations like the International Monetary Fund now span the globe, there's never been a bank in history that rivaled the power of the Rothschild Bank. It was one of the first international banking empires, and it had a tremendous impact on the development of global finance.

Founded in the mid-eighteenth century by Mayer Amschel Rothschild of Frankfurt, this once humble banking business developed into the largest financial company of its time. Rothschild's family went on to run a variety of businesses and fund everything from the Napoleonic wars to the construction of railways and modern roads.

In every major European city from London to Vienna, the family had a bank that lent money and funded projects, thereby influencing the development of entire countries. They even gave financial advice to kings and queens across Europe.

Around the same time as the Rothschilds were revolutionizing the world through finance, a second major event took place: the transatlantic sea cables were laid, enabling virtually instant communication between continents. This massive project, undertaken by the American businessman Cyrus Field, took over 13 years to complete, cost millions of dollars and necessitated several trips across the Atlantic.

Field and his company faced tremendous obstacles, from torrential storms to snapping cables to ships that came close to capsizing and investors who lost faith in the colossal project. But when it finally came to fruition in 1866, the impact was incredible.

All of a sudden, people on opposite sides of the Atlantic could communicate in real time, instead of waiting weeks or months for mail. The change was sensational and, in just its first month of operation, the cable couriered over 1,000 messages between the two continents.

### 6. John D. Rockefeller and Jean Monnet transformed America and Europe, respectively. 

There are few names so intimately tied to wealth as that of John D. Rockefeller, and that's no coincidence. Rockefeller was both a ruthless businessman and a great philanthropist. His company, Standard Oil, essentially controlled the entire US oil market and was well ahead of its time in terms of its treatment of workers.

For instance, employees were encouraged to make time for both business and family, rather than spending every waking moment working for Standard Oil. But the company wasn't all cookies and cream. In fact, Standard Oil forced the majority of its rivals out of the market and had to be broken up by the US government, which feared the oil giant's sheer power.

Nonetheless, Standard Oil's successor companies, like Chevron and ExxonMobil, continue to rank among the largest multinational companies in existence.

But Rockefeller was more than just an oil tycoon. He also supported countless philanthropic projects, and the Rockefeller Foundation funded a variety of research groups like the Rockefeller Institute for Medical Research, which focused on curing diseases like pneumonia and typhoid fever.

While Rockefeller was building his fortune and giving money to charity, on the other side of the Atlantic, a French political economist was hard at work trying to overcome national boundaries and unite Europe. His name was Jean Monnet and he grew up in a Cognac trading family, which meant he spent his childhood in several different countries.

During the First World War, he worked for the allies, helping them improve their international trade. Although he never officially held a political position, his work radically transformed modern Europe.

He spent the majority of his life working to establish supranational organizations that could extend beyond national boundaries and unite Europe. His most impressive achievements include the 1951 establishment of the European Coal and Steel Community, or ECSC, and convincing the United States to join the Allies during the Second World War.

As a result of his efforts, the ECSC, now known as the European Union, has enjoyed almost 50 years of peace and prosperity.

### 7. Margaret Thatcher revived Britain’s free markets – at the cost of thousands of jobs. 

When it comes to British politics, there's probably no woman quite as controversial as Margaret Thatcher, also known as the Iron Lady. She served as Britain's first female prime minister, from 1979 until 1990.

So, why is public opinion about Thatcher so divided?

Well, her politics were harsh. She abhorred consensus and pushed determinedly to implement her conservative ideas and neoliberal ideology. Her most well-known political moves include a harsh military reaction to the invasion of the Falkland Islands by Argentina in 1982; dramatic cuts to spending on social welfare; opening up London's financial district to the international market and privatizing publicly owned companies.

These policies were defined by her highly autocratic style of governance. Thatcher made every decision and never allowed the opposition Labour Party to so much as participate in most decision-making processes. Through her domineering leadership, she pushed the conservative ideals of a highly capitalistic society that could only prosper if the government took a step back and allowed private competition to rule the market.

This won her the support of the companies and business leaders to whom she gave opportunities, but Thatcher's tough policies also produced tremendous problems. A good example is the infamous year-long fight she waged against striking miners' unions.

Before Thatcher's time in office, the mining industry lobbied the government to support new mines, which eventually led to a massive overproduction of coal. In 1984, when Thatcher began cutting support and applying pressure to the mining companies, the unions, fearing for their livelihood, fought back and went on strike.

The Iron Lady treated this situation like a war, which she ultimately won, causing thousands to lose their jobs. Many other similar situations meant that the Thatcher government put tens of thousands out of work while making drastic cuts to social welfare programs.

### 8. While Deng Xiaoping opened China up for business, Andrew Grove was revolutionizing technology practices at Intel. 

Nowadays, if you buy anything from a plastic toy to a flat-screen TV, there's a good chance it was manufactured in China. But this wasn't always the case. So how did China become the manufacturing powerhouse it is today?

It all began with Deng Xiaoping, a Chinese politician who transformed his country into a modern global player.

He began his political career at a relatively young age and spent many years working with hardline communists, even serving as a primary advisor to Mao Zedong. But when Xiaoping took over the party leadership 1978, he opened Chinese markets to foreign investments.

To bolster the country's economy, he invited foreign companies to invest in China and actively supported them in moving their operations there. He also pushed China to export goods across the globe and helped the country join the World Trade Organization.

As a result, China became — and has remained — one of the fastest-growing economic markets in the world, with Chinese factories now producing nearly half of the products sold worldwide.

But Xiaoping was no pioneer when it came to global capitalism. He had already learned a great deal from a trip he made to the United States in 1979, at a time when businessmen were reforming their companies.

One such man was Andrew Grove, a Hungarian-American businessman who transformed modern technology and management in his adopted homeland. Grove fled his native Hungary in 1956 and immigrated to the United States. After completing his studies in chemical engineering, he joined the digital technology firm Intel in an upper management position.

Grove's leadership style was tight and controlling, and he made employees accountable for both their successes and failures. He knew that the biggest problem within any production system wasn't the shoddy work of particular departments, but rather miscommunication between them.

Based on this understanding, he rebuilt the company's structure, making it more horizontal, and made a point of investing in research and development, even during recessions. In fact, he was the one behind Intel's focus on microprocessors, small chips that serve a variety of functions and which are now central components in everything from smartphones to spaceships.

> _"Deng effectively bound the Middle Kingdom to the rules of an open world economy."_

### 9. Final summary 

The key message in this book:

**Globalization has been an essential force in human history, influencing billions of lives. Over the course of this multifaceted development, certain key individuals stand out for their tremendous influence on the connectivity of the world and their role in bringing us to where we are today.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Originals_** **by Adam Grant**

In _Originals_ (2016), Adam Grant taps into the field of original thinking and explores where great ideas come from. By following unconventional rules, Grant gives us helpful guidelines for how we can foster originality in every facet of our lives. He also shows that anyone can enhance his or her creativity, and gives foolproof methods for identifying our truly original ideas — and following through with them.
---

### Jeffrey E. Garten

Jeffrey E. Garten teaches global economy at the Yale School of Management. He's worked in senior positions in the Nixon, Ford, Clinton and Carter administrations. He's also the author of _The Big Ten: The Big Emerging Markets and How They Will Change Our Lives._

