---
id: 548f00976532350009c30000
slug: from-the-ruins-of-empire-en
published_date: 2014-12-29T00:00:00.000+00:00
author: Pankaj Mishra
title: From the Ruins of Empire
subtitle: The Revolt Against the West and the Remaking of Asia
main_color: A9CFE4
text_color: 5D717D
---

# From the Ruins of Empire

_The Revolt Against the West and the Remaking of Asia_

**Pankaj Mishra**

In _From the Ruins of Empire_, author Pankaj Mishra examines the past 200 years from the perspective of Eastern cultures and how they responded to Western dominance. The book charts in detail the colonial histories of Persia, India, China and Japan in the nineteenth century to the rise of nation-states in the twentieth century. Select stories of cultural figures help to humanize the often violent clashes of cultures, showing the powerful influence of individuals in the course of history.

---
### 1. What’s in it for me? Discover how the East fought Western aggression while adopting Western ideas. 

While the last 200 years are mostly seen as a period of economic and political progress in Western nations, rarely is this progress linked to Western imperialism and its violent policies in Asia.

These blinks explore the East's intellectual response to Western modernism, with a look to recent history and the movements and wars that shaped Eastern nation-states as we know them today.

Importantly, the story is told not from a colonialist's perspective but instead from the colonized, with a close look at a few personalities who worked to ensure that Eastern traditions would not be totally subsumed by Western influence.

In the following blinks, you'll discover

  * how a single naval battle changed the course of history;

  * how a Persian intellectual helped form the philosophy of pan-Islamism; and

  * how pork fat acted as the spark of a bloody mutiny in India.

### 2. Trade interests drove a Western wedge into Asian countries, followed by political dominance. 

The "subordination" of Asian countries to Western powers began in 1798, when Napoleon led a 40,000-strong French army into Egypt, ostensibly to protect French trade interests. 

Shortly afterward, issues of trade also drove a Western wedge into China and India, as European powers dominated the Chinese mainland following two wars sparked by the opium trade.

What exactly were the concerns of Western powers? In the nineteenth century, economic relations between China and Western countries were imbalanced. China exported more than it imported; so to combat this shortfall, traders from the West (in particular the British) introduced an addictive narcotic — opium — to China.

As more and more Chinese became addicted to opium, Western traders raised their prices and made a killing, while correcting the trade imbalance at the same time. Slowly, the influx of money reversed, and European powers came to dominate the Chinese economy.

Yet the Chinese knew the situation was untenable, and what followed was the country's attempts to end the lucrative yet damaging opium trade. Two Opium Wars followed, from 1839 to 1842 and from 1856 to 1860.

China essentially lost its fight. As a result, Western powers benefited from a further loosening of trade restrictions and more favorable treaties.

Around the same time, the suppression of the Indian Mutiny of 1857 led to increased Western control of India.

Since the seventeenth century, the British East India Trading Company had dominated trade and exercised increasing influence over many Indian regions.

The mutiny was an attempt to curtail this dominance, and even though the mutineers outnumbered British troops, they too lost their fight. The British prevailed as their soldiers were better trained and better armed than local mutineers.

This ended native Muslim rule in India, to be replaced by de facto British rule, as the Western power then redesigned the country's political, economic, and social environments.

Importantly, British representatives sliced the country into separate governing regions, which helped solidify British control over vast areas, and necessitated more British soldiers on the ground.

> _"European subordination of Asia was not merely economic, political and military. It was also intellectual, moral and spiritual."_

### 3. As Japan defeated Russia, Eastern powers took heart that Western dominance could be curbed. 

The defeat of the Russians at the hands of the Japanese in 1905 signaled a shift in Western dominance in Asia.

Ruling from 1868 and 1912, Japanese Emperor Meiji combined traditional Eastern values with Western ideas of progress. The emperor consolidated control in Japan and industrialized the nation, in a bid to compete with more advanced Western powers.

Such a drastic change was set in motion in 1852 when American warships arrived in Japan, demanding to set up trade relations. Meiji was astonished at the superiority of the American naval fleet in comparison with Japanese armaments; and from this moment Japan was set on a course that would alter its economic, political and social makeup.

Significantly, a stronger Japanese military led in 1905 to its victory over Russian forces.

This conflict, started in 1904, determined which country would control the lands of Korea and Manchuria (northeastern China). When the small Japanese fleet proved victorious, it increased Japanese territory considerably and helped to reestablish Eastern dominance in the region.

In response, other Eastern leaders were inspired by Japan's actions and worked to establish themselves amid the West's waning power. The founder and first president of the Republic of Turkey, Mustafa Kemal Ataturk, looked to Japan as a model.

Ataturk believed Japan's way of combining nationalism and traditional values with Western technological advancements was the key to staving off intrusion by Western powers.

In his policies, Ataturk emphasized the modernization of technology and politics as well as secularism, and favored nationalism as a way to unify people, rather than religion or socialism.

### 4. An influential Muslim thinker called on a pan-Islamic force to stem the tide of foreign influence. 

As he traveled through the regions of the Middle East, the nineteenth-century thinker Jamal al-Din al-Afghani sought to establish a pan-Islamic force against an ever-encroaching West.

Born in Persia in 1838, al-Afghani witnessed firsthand the influence of the West when he visited India. There, his local political interests developed into more global concerns.

In particular, he believed the East should staunch Western interference with nationalism, and more generally, with pan-Islamism. He dreamed of bringing together the Muslim nations in the Ottoman Empire, and believed that only a united Muslim force could rise to meet the threat of the West.

Since the Ottoman Empire was the heart of the Muslim world and as such could most effectively propagate his ideas, al-Afghani became the ideological advisor to Ottoman Sultan Abdul Hamid II.

Yet after witnessing the failures of various national resistance movements, al-Afghani's vision changed, as he saw the need to strengthen the ties between Muslim nations so they could work together, instead of compete against each other.

As well as evolving his pan-Islamic ideas within the Ottoman Empire, al-Afghani also emphasized the power of Persian nationalism by warning his native Persians of foreign control of the country's natural resources, such as tobacco.

Selling a natural resource to foreign powers made a nation susceptible to economic exploitation, he stressed. For example, he explained how Persians working for foreign companies were essentially taking money from the hands of their countrymen and putting it in the hands of foreign powers.

### 5. Thinker Liang Qichao juggled modernity with Confucianism to respond to Western encroachment. 

An influential intellectual, China's Liang Qichao inspired many of his countrymen with his critiques of Western power while advocating at the same time the adoption of some Western practices.

Born in 1873, Liang's particular emphasis on the tenets of Confucianism helped form his ideas about Western influence and how to combat it.

Established from the teachings of Chinese philosopher Confucius around the fifth century BCE, Confucianism is a humanistic worldview that advocates learning, familial piety and loyalty. These principles help to create strong bonds between families and communities.

Although Liang saw Confucianism as an abstract philosophy and not necessarily applicable to contemporary Chinese issues, he believed its ethical and social norms offered a powerful defense against Western influence.

Specifically, Liang thought the Confucian concept of _ren_, which encourages harmony and compromise, was a necessary reaction to Western economic dominance and capitalist competition.

In addition to using Confucianism to guide his ideas, Liang believed it was paramount for China to modernize. He felt that the only way the social and ethical tenets of Confucianism could hold up to the West was to combine them with select Western values and economic systems.

To further cement his ideas, Liang read works by Adam Smith, which explained how, by enlisting the methods of capitalist production, nations could increase their wealth.

He also drew inspiration from John Stuart Mill, whose utilitarian ethics offered a framework in which the greatest good for society and individuals could be accomplished.

### 6. The poet Tagore used his art to oppose Western colonialism in India and unite the divided populace. 

An intellectual and spiritual leader, Rabindranath Tagore was born in 1861 into a divided world, with British colonial power on one side and Indian nationalists on the other.

Through his art, he managed to oppose both the xenophobia of local nationalists and the dominance of Western values.

As a painter, poet, musician and novelist, Tagore fought against colonialism and sought to unite people. He believed art manifested and accentuated the common element in every person.

As opposed to writing polarizing treatises, Tagore wrote novels and poems that addressed the damaging effects of nationalism on people. In his novels _Gora_ and _Home and the World_, for example, he laid bare the middle-class obsession with violence and politics.

In this way, Tagore used art and literature to bring people together to address such topics.

Interestingly, Tagore became the first non-European to win the Nobel Prize for Literature in 1913. The spotlight was then on the poet to present his views on the international stage.

He traveled extensively and lectured on the cultures of the East. Promoting the spiritual unity of people, he supported a non-nationalist cosmopolitan worldview and was equally disparaging of European colonialism and nationalism.

Tagore thought that while nationalism could help a colonized people unite in the fight against a foreign power, using xenophobia — or fear of foreigners — as a basis for such unity was wrong.

He believed that Indian nationalism was built on the same xenophobic logic as the colonialist system. Thus his non-nationalist worldview sought to form a spiritual unity that acknowledged and centered on the common goals of all people.

### 7. Traditionalists and modernists clashed as Western ideas stirred up the cultural pot. 

Napoleon's invasion of Egypt in 1798 was a wake-up call for Eastern empires. Many rulers asked themselves how they could preserve traditional values in the face of an evolving, modern world.

The Ottoman Empire felt the pull between the traditional and the modern particularly strongly. On the one hand, modernists embraced the lessons of the French Revolution, the Enlightenment and the Industrial Revolution.

Yet on the other hand, traditionalists were loathe to abandon Islamic law for a Western model; they shrank from technological advancements that would diminish local industry; and they could not imagine trading traditional garments for Western fashions.

Thus from 1839 to 1876, Ottoman leaders enacted a series of reforms to combat the negative influence of the West while retaining some positive ideas, known as the "Tanzimat movement."

One particular social reform included allowing non-Muslims to become soldiers, meaning a larger, more inclusive Ottoman army where everyone played a part in protecting the empire's future.

Another reform centered on the abolition of slavery as well as providing citizenship for all Ottoman subjects. This policy was inspired by the ideas of the French Revolution, and helped to make the Ottoman Empire more cosmopolitan. Importantly, technological improvements led to a more modern armaments industry, to keep up with Western weaponry.

Yet the reforms were too little, too late. While some old habits changed and institutions were modernized, religious communities and imams stressed the need to stick to traditional practices.

Despite their protests, however, they were still too late to stem the ascent of Western influence.

### 8. China’s ancient facade was torn asunder by opium trade wars and a militaristic Japan. 

In the nineteenth century, China's ancient civilization was forced open to the modern world by trade pressures, which led to war.

China produced and exported tea, silk and porcelain to the West in quantities, while importing few if any Western goods in return. To address this imbalance of trade, Western nations, in particular Britain, began to send addictive opium to China.

In an attempt to remove the yoke of opium as well as Western trade influence, China fought two separate wars with Britain in the mid-1800s, both of which were lost. The toll was even more opium trade, as well as more liberal treaties with victorious Western nations.

Interestingly, increased Western trade not only brought more Western goods into China but also more Western ideas. Slowly but surely, China was modernizing.

One such concept that took root in China was economic production methods as established during the Industrial Revolution. Such methods were the death knell for specialized trades; now Chinese factories would focus on producing more goods quickly and at lower prices. What's more, with increased production, newer and larger markets to sell goods were also required.

By the turn of the century, Japan's growing military prowess led the country into war with China over territory in Korea. During the conflict, it became clear that China's military was embarrassingly weak when compared to its more modern neighbors. Cowed, China sued for peace. 

While China previously had seen itself as the most advanced and powerful country in the East, its defeat at the hands of the Japanese brought shame, not only domestically but also internationally.

Chinese thought leaders and budding revolutionaries at the time felt that if China's culture was to endure, aside from building a stronger army, the Chinese people had to come together as a nation. This stance echoed that of Liang Qichao, who believed only national unity could stand up to the West.

### 9. The violence of Western nations in the East undermined any claims of being a “civilizing” force. 

Many a colonialist believed Eastern peoples were barbaric, and the West was a civilizing force.

Yet when we look at history, however, there are plenty of examples where Western powers were no more civilized than the so-called barbaric East.

Britain's savage suppression of the Indian Mutiny in the mid-1800s is just one example.

Indian soldiers fought against the influence of the British East India Company, a company that controlled trade and increasingly politics on the Indian subcontinent.

The mutiny was inspired by the imposition of high taxes as well as an affront to many soldiers' religious beliefs in the line of duty.

British gunpowder cartridges were said to be greased with pork fat, so that when a soldier had to bite the cartridge open to release the gunpowder, he would essentially be violating the tenets of the Muslim faith (which forbids consuming pork products).

The mutiny was quashed, and British soldiers got their revenge by executing captured mutineers by strapping them to the mouth of a cannon before it was fired — an old Mughal practice.

Thus the "civilized" West proved its superiority in the land of the "barbarians."

Fast-forward to the twentieth century in India, when Mahatma Gandhi inspired Indians to fight Western aggression and domination through peaceful means.

Gandhi supported Eastern traditionalism and nationalism, feeling the need to stand up against what he felt were nihilistic, economic motives that inspired Western nations.

He organized a march to collect salt naturally from the sea, as a protest to the salt sold by Britain that was subject to high taxes. Consequently, Gandhi was arrested for breaking British taxation laws.

What followed were mass protests against other British taxation laws, such as those governing land and forests, as well as a boycott of British goods.

Importantly, Gandhi's non-violent protest deflated Western powers without resorting to Western violence.

### 10. Some Eastern countries adopted Western ideals while fighting Western political dominance. 

Even as Eastern cultures fought Western influence, at the same time many countries observed and learned from Western experience.

At the twilight of Western dominance, however, Eastern countries began to better calculate how to handle Western influence and demands.

The concept of the nation-state is one critical point. Whereas the East had empires — the Ottoman Empire, the Persian Empire, the Empire of Japan, the dynasties of China and the kingdoms of India — Western cultures organized themselves into nation-states, with individuals identifying as citizens.

The concepts of a "people" or a "nation" were essentially new to Eastern cultures.

The centralized power of an empire kept rulers at a distance from their subjects and kept borders to the outside world mostly closed. In a modern world, however, it made sense for Eastern powers to become more involved in global politics and embrace the idea of the nation-state.

Even while adopting the nation-state model, Eastern countries still stood in opposition to the political dominance of the West.

The actions of Iranian Prime Minister Mohammad Mosaddegh in the 1950s offer an interesting example of a hybrid stance, using Western methods against Western aggression.

Democratically elected in 1951, Mosaddegh established a secular democracy to unite Iranians against the West. He was responsible for nationalizing Iran's oil industry, a move which gave the country more control over its oil reserves and raised prices considerably for Western buyers. 

As a result of this, however, in 1953 Mosaddegh was overthrown in a coup orchestrated in part by the U.S. Central Intelligence Agency (CIA), and was sentenced to three years in prison.

So as Mosaddegh adopted various Western models of government — such as the concept of nation-states and social reforms — trying to use these tools _against_ the West ultimately meant the end of his rule and his government.

### 11. Final summary 

The key message in this book:

**We can't form a clear picture of the world today, with the rise of India and China, the economic superiority of Japan, or the relationship between Muslim countries and the West, without acknowledging and understanding the West's violent, colonial relationship with the East in history.**

**Suggested further reading:** ** _Civilization_** **by Niall Ferguson**

There seems to be a crisis of confidence in the West. In the face of the rising power of China, and with a seeming lack of interest in its own history and civilization, many fear that the West has somehow lost it way.

_Civilization_ aims to explain why the West grew so powerful and dominated the rest of the world. The answer lies with six _killer applications_, which enabled the West to overcome the rest. Yet vital questions arise: Has the West forgotten these killer apps and will this lead to its collapse?
---

### Pankaj Mishra

Pankaj Mishra is an author and journalist, and has written for _The_ _Guardian, The New York Times, London Review of Books_ and _The_ _New York Review of Books_. His books include _Butter Chicken in Ludiana, An End to Suffering_, _Temptations of the West_ and _The Romantics_.

