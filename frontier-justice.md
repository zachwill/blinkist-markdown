---
id: 56661168cb2fee0007000028
slug: frontier-justice-en
published_date: 2015-12-11T00:00:00.000+00:00
author: Andy Lamey
title: Frontier Justice
subtitle: The Global Refugee Crisis and What to Do About It
main_color: E78D3F
text_color: 9C5F2A
---

# Frontier Justice

_The Global Refugee Crisis and What to Do About It_

**Andy Lamey**

_Frontier Justice_ (2011) offers a detailed historical account of the plight of refugees. It also presents viable solutions that could improve the lives of refugees while ensuring a higher degree of safety for their host countries.

---
### 1. What’s in it for me? Get a deeper understanding of the laws that regulate and affect refugees. 

Over the last century, refugees and refugee crises have become global phenomena and challenges, from people fleeing Nazi Germany before and during World War II, to major crises in Haiti and the Balkans, to the current conflicts raging in the Middle East and Syria. So what has affected the way we think about refugees and the way they are treated?

The answer, simply, is laws. With the emergence of modern nation-states and borders as we know them today, the laws governing who can cross them — and how — have been instrumental in determining how refugees are assigned their legal status and how they are accepted, or not, into a new country. 

These blinks will look into the origins of these laws and regulations, the consequences they have had up until today and what a possible future for these regulations could be.

In these blinks, you'll learn

  * why there were 9.5 million refugees in Europe in 1926;

  * how the Haitian refugee crisis in the 1980s set a new precedent for refusing asylum; and

  * why Canadian refugee policy could be a model for the rest of the world.

### 2. Hannah Arendt’s struggle in war-torn early twentieth-century Europe still typifies the modern refugee experience. 

Though the story of German-Jewish philosopher Hannah Arendt's time as a refugee dates back to the 1930s, her story nonetheless continues to resonate today. But which parallels can we draw between her story and that of the modern refugee?

The rise of nationalism following World War I caused millions of people to become displaced. International borders were being redrawn, a revolution was taking place in Russia and Turks were slaughtering Armenians. As a result, there were 9.5 million refugees stranded across Europe by 1926.

Moreover, the nationalization of welfare provisions and national responsibility for economic well-being led to a sharp distinction between what constituted a "national" and a "foreigner."

The rise of nationalism in Europe that followed led the fascist Nazi party to seize power in Germany. Their anti-Semitic laws and rhetoric caused 25,000 people, including Arendt, to flee the country and become refugees.

Arendt first fled to Czechoslovakia, then France and eventually the United States. During her travels, she experienced firsthand the way many nations see refugees fleeing persecution as "undesirables" and potential threats to their way of life. In this way, Arendt was a quintessentially modern refugee.

Inspired by these experiences, her writings pose questions about the conflict between the rights of citizens versus human rights, something refugees struggle with to this day.

Citizens are granted rights through their belonging to nations. Refugees, however, lose these rights once they cross the border into another country. Arendt proposes that without the benefit of citizenship, refugees can't expect to receive _any_ rights, which of course inspires the question: does a set of fundamental human rights exist beyond borders? 

When people show up at the border of a new country, the most they can hope for is a moral response to their humanity.

These questions were compiled in her landmark book, _The Origins of Totalitarianism_, which, since its publishing in 1951, still holds up as the definitive account of the injustices that refugees face.

> _"The ultimate explanation of the interwar crisis of asylum, Arendt concluded, was the simple fact that the word is divided into states."_

### 3. The American response to Haitian refugees serves as an example of modern refugee policy. 

So how did the view of human rights versus citizens' rights evolve over the course of the twentieth century? 

A prime illustration was the response to the 1980s Haitian refugee crisis, during which 25,000 Haitian refugees arrived by boat in Florida in an attempt to flee the violent dictatorship of presidents Doc Duvalier and his son Baby Doc Duvalier, both of whom would leave the corpses of their critics hanging in public areas.

But the Ronald Reagan administration was not sympathetic. Rather than accepting the refugees with open arms, they stopped the boats before they landed ashore, sending the people aboard back to Haiti. 

This practice, called _interdiction_, was extremely controversial due to its contravention of the 1951 UN Refugee Convention, which stated that refugees couldn't be sent back to their countries of origin if their lives were in danger.

Critics of interdiction considered the practice racist, as the administration treated refugees differently based on where they came from. For example, refugees from Cuba often received favorable treatment by immigration services upon entering the United States.

The administration responded by conducting interviews with refugees on the decks of Coast Guard ships in order to determine who should be considered for asylum and who should be sent back.

Places like the now-notorious Guantanamo Bay also made it easier to deny people their rights. Refugees who weren't immediately returned to Haiti were brought to Guantanamo Bay, a military base on Cuba.

The legal jurisdiction of Guantanamo is conveniently confusing: the land is leased from Cuba, though the United States controls the area. This allowed the United States to withhold the basic rights of the refugees held there.

News of their situation eventually reached lawyers and politicians, who came to the aid of the refugees. But there wasn't much they could do: the Supreme Court decided that the UN Refugee Convention, which granted these basic rights, only applied within US borders, and thus not to refugees at Guantanamo.

### 4. All too often, refugees like Mohammad Al Ghazzi are treated as criminals once they arrive in a new country. 

In the 1990s, Mohammad Al Ghazzi and his family faced persecution under Saddam Hussein's regime due to Al Ghazzi's brother's involvement with the Islamic Dawa Party, which opposed Hussein.

After two years of imprisonment following his arrest, Al Ghazzi decided to flee Iraq in hopes of finding asylum in Australia. His story demonstrates the reliance that many refugees have on smugglers, and the dangers it poses.

To get to Australia, Al Ghazzi first flew to Malaysia, where smugglers patrol the airport looking for refugees to offer passage via boat. Al Ghazzi payed $2,000 for a spot aboard a crowded and barely seaworthy boat, which, not surprisingly, almost sank during the near two-day voyage into the Indian Ocean toward Australia.

The same trip would later result in the death of Al Ghazzi's family, four adults and ten children, all of whom drowned at sea when their boat sank after deciding to follow him a year later. In total, of the 400 people who were on the boat, only 45 survived.

But Australia's response to refugees was callous; refugees there were treated worse than criminals.

After arriving at Australia's Christmas Island, Al Ghazzi was taken to the Curtin Detention Center, where refugees spent hours in the baking sun without access to lawyers or any contact with the outside world — conditions that drove many detainees to attempt suicide.

Eventually the detainees started a hunger strike — some even sewed their mouths shut — until the authorities finally started processing their asylum claims.

Between 1992 and 2005, refugees in Australia were often held in detention even longer than serious criminals would be. Hannah Arendt commented on this sort of treatment 50 years earlier, writing that since criminals still hold some rights as citizens, they're actually treated better than refugees, who have no such rights.

After 11 months of detention, Al Ghazzi finally received legal assistance and his asylum claim was eventually recognized.

> _"Between 1999 and 2005, there were 15 instances in which rejected asylum seekers were killed after being deported from Australia."_

### 5. Europe has become a web of boundaries, restrictions and complicated transit zones. 

Post-World War II Europe saw one of the greatest refugee crises of our time. But have things changed for the better in terms of refugee rights? When looking at so-called "Fortress Europe," it seems that not much has changed at all.

European immigration laws have become increasingly restrictive in recent years. For example, from 1992 to 2005, the British government passed six asylum laws, each making it more difficult to meet the requirements necessary to prove the persecution that refugees face.

Or consider West Germany, which after World World II constitutionally established that "persons persecuted on political grounds shall enjoy the right to asylum." However, after the fall of the Berlin wall, the influx of asylum seekers, which reached 438,000 in 1992, spurred violent neo-Nazi reaction and social unrest. The government caved and implemented constitutional amendments that restricted refugee claims.

Then there's the European Union's Dublin Regulation, which states that asylum claims must be made at the first European country of entry. As a consequence, countries at Europe's external borders, like Greece, Ukraine and Poland, have the strictest policies and highest rates of denial.

In addition, inexpensive air travel has made it easier for refugees to leave their countries. This has resulted in lawless airport transit zones lying at the center of numerous claims of human rights violations.

In Europe, these transit zones are much like Guantanamo: refugees are cut off from access to legal systems or the right to appeal decisions made about their petitions against ill treatment.

For example, in 1999 a 40-year-old Algerian woman seeking refuge from persecution and repeated rape by Algerian police was detained at Frankfurt airport due to her insufficient documentation. After more than 100 days of detention, she eventually hanged herself in the bathroom of the airport's transit zone.

Then there's the case of a Palestinian refugee who spent seven months in the Prague airport's transit zone. There, he was forced to clean himself in public toilets and live off of airline meal tickets before finally being granted asylum.

> _"If human rights were born in Europe, they now occupy an autumnal twilight there in regard to asylum."_

### 6. Canada’s refugee system could be taken as a future model for refugee policy reform. 

In 1989, Canada altered its refugee policy after refugees from Rwanda and India filed appeals against the government's decisions on their cases. The case, _Singh v. Minister of Employment and Immigration_, eventually made it to the Supreme Court, which sided with the appellants. The court's decision was monumental: it gave refugees a constitutional right to a face-to-face hearing.

Canada's policy still isn't perfect, but it nonetheless represents a breakthrough in refugee rights. For example, the policy doesn't state which country the hearing will take place in, nor does it guarantee rights to appeal or legal aid, but the fact that it gives refugees any rights at all is indeed extraordinary.

And since the hearing can be conducted in any country, the policy is considered "portable," in the sense that it can be enacted in any country that honors these standards.

However, the case of Ahmed Ressam shows that, for policy reform to work, it needs a well-funded deportation program and passport security.

Ressam entered the immigration system in Canada in 1994, but his application was rejected. Canada's deportation department was underfunded, however, leaving Ressam stranded for years. Unable to work, Ressam resorted to crime and was eventually recruited by al-Qaeda.

Weak passport laws allowed Ressam to obtain a Canadian passport with little more than a forged baptism certificate under a fake name, and he was eventually arrested trying to transport a bomb into the United States.

Nevertheless, current Canadian immigration policy does make it easier for authorities to manage terrorist threats.

Terrorist suspects who enter the immigration system are subject to security certificates, a tool that allows authorities to immediately deport those found to engage in terrorist activities. As a result of these policies, as of 2009, no terrorist has been able to make it through Canada's refugee system in order to carry out an attack in North America.

### 7. History and a modified version of the Canadian policy give us hope for the future. 

Canada's approach to asylum is one borne out of both liberal and democratic conceptions of human rights. A modified version of their approach, which we can call the _portable-procedural_ approach, is highly compatible for worldwide use.

The portable-procedural policy essentially honors the concerns of Hannah Arendt, while adhering to a commitment to honoring human rights.

In addition to the right to an oral hearing, the portable-procedural approach to refugee policy also grants refugees the right to legal aid and judicial review in case they want to appeal the decision.

After Arendt's terrible experiences, she developed the belief that nations might _never_ apply human rights to people who don't fit under the umbrella of citizenship. By granting these rights to refugees — rights which many nations consider fundamental to their citizens regardless of their criminal history or character — we would be making progress toward proving Arendt wrong.

Indeed, history has demonstrated that nations can evolve and improve their policies in a way that better respects human rights. While there are critics who continue to believe that refugee policy reform will never happen, there are nevertheless precedents that prove that human rights can cause nations to evolve for the better.

The abolition of the transatlantic slave trade in the nineteenth century is a prime example of the value of human rights prevailing over the financial and nationalistic concerns of nations.

In this case, human rights were deemed even more important than individual nations' political and economic concerns, such as when racial apartheid was finally put to an end in South Africa during the 1990s.

These developments should give us hope that, eventually, human rights will prevail in the current refugee crisis.

### 8. Final summary 

The key message in this book:

**Since the earliest political refugees began seeking sanctuary, it has been widely considered impossible for their host countries to offer them the same fundamental rights that they would their own citizens. However, recent developments in immigration and refugee law give us reason to be optimistic.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Andy Lamey

Andy Lamey is a professor of philosophy at University of California, San Diego and a journalist whose work has appeared in the Canadian publications _National Post_ and _Maclean's_. In addition, Lamey has produced numerous radio documentaries for the CBC series _Ideas_.

