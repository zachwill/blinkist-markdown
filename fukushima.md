---
id: 554786553666320007fe0000
slug: fukushima-en
published_date: 2015-05-08T00:00:00.000+00:00
author: David Lochbaum, Edwin Lyman, Susan Q. Stranahan and the Union of Concerned Scientists
title: Fukushima
subtitle: The Story of a Nuclear Disaster
main_color: 459EB0
text_color: 2C6570
---

# Fukushima

_The Story of a Nuclear Disaster_

**David Lochbaum, Edwin Lyman, Susan Q. Stranahan and the Union of Concerned Scientists**

_Fukushima_ (2014) tells the story of how one of the biggest tsunamis in Japan's history combined with government neglect, corporate interest and propaganda to create the most serious nuclear disaster since Chernobyl. The book was written by the Union of Concerned Scientists, a nonprofit that brings together science and political advocacy.

---
### 1. What’s in it for me? Discover what went wrong in the Fukushima disaster, and why it could happen again. 

On 11 March 2011, a monster earthquake struck off the coast of Japan. Its reverberation caused a massive tsunami, which barreled across the ocean intent on destruction. When it hit Japan, it caused a flood at the Fukushima Daiichi nuclear power plant, leading to a disastrous meltdown.

All told, the Fukushima Disaster of 2011 claimed the lives of almost 20,000 people and did unimaginable damage across Japan. Yet today, it seems as though few authorities have learned any lessons from it.

How did the backup and emergency systems fail so completely? Why wasn't there adequate tsunami protection? Why didn't nuclear energy authorities heed the lessons of Chernobyl and other meltdowns? And most importantly, what can we do to prevent it happening again?

These blinks explain what exactly went wrong at the Fukushima plant in March 2011, how Japan reacted, and why we might not be safe from the next meltdown.

In these blinks, you'll learn

  * how the inspector who found a crack at the Fukushima plant was silenced;

  * why the meltdown was like a crime scene; and

  * why American inspectors aren't sufficiently protecting their citizens.

### 2. The 2011 earthquake off the coast of Japan was one of the biggest in the country’s history and caused terrible destruction. 

In ancient times the Japanese thought that the earthquakes typical to the region were caused by the movements of a giant catfish under the islands that make up Japan. Today, we have much more precise scientific knowledge of earthquakes, but the effects of the earthquake that rocked Japan on 11 March 2011 and the following tsunami surpassed even our current understanding.

The 2011 earthquake was one of the biggest in Japan's history. It hit roughly 40 miles east of Japan, as one tectonic plate slid under an adjoining plate, a process called "subduction," which released enormous energy — so great it even tilted the earth's axis by a few inches!

After the 1995 earthquake in Kobe, which claimed 5,000 lives, Japan developed one of the most sophisticated earthquake warning systems in the world. It is powered by a network of some 1,000 motion sensors all over the country that pinpoint the exact location where an earthquake takes place.

After the 2011 earthquake hit, initial estimates put it at 7.9 on the _Richter scale_, a metric used to measure an earthquake's magnitude. Over the next few days the Japan Meteorological Agency discovered that it was actually 9.0 — that's _45 times_ more energy than their first estimate.

This meant it was the biggest earthquake ever detected by Japanese instruments, and among the five biggest in the world since we began to measure them.

The resulting tsunami was enormously powerful, far exceeding all previous calculations. When the waves from the tsunami reached the Antarctic (roughly 8,000 miles from the earthquake's epicenter), they were still powerful enough to break off a mass of ice-shelf the size of Manhattan.

The human cost of the earthquake is just as staggering: in the end, the Fukushima disaster, as it became known, claimed over 18,000 lives.

### 3. The Fukushima Daiichi nuclear power plant was ill-equipped to manage such an enormous tsunami. 

A few hours after the earthquake, tsunami waves began crashing against the shores of Japan. But as people clambered to reach safety among destroyed buildings, Japanese authorities soon realized that they might have an even bigger problem on their hands.

In addition to tearing down homes, the tsunami flooded the Fukushima Daiichi nuclear power plant, cutting off its power. This power was desperately needed to cool the plant's nuclear reactors. If the cooling system fails, the fuel in the reactor will start melting after only 30 minutes. When this happens, a slough develops that is so corrosive it can eat its way through the six-inch steel wall of the reactor as well as the building itself, releasing radioactive material into the environment. In other words, it can cause a _meltdown_.

Even worse, the flooding also took out the emergency power generator as well as the instruments in the control room. The controllers were left with no way of knowing what was going on in the reactors.

Compounding the problem, the contingency plan for disasters had serious flaws.

During a meltdown, operators could vent the reactor as a last resort by opening a special valve that released a controlled amount of radioactivity into the environment. These measures should have ultimately staved off the potentially catastrophic outcome of high pressure in the reactor. Worse than a meltdown, high pressure can actually cause the reactor to explode.

But the emergency plan contained no information on how to open the valves manually, and with the instruments out of service, venting was off the table.

The plant also had no way to communicate with authorities. According to the plan, a notification should be sent from the plant via fax to various authorities and to the surrounding cities in case of emergency. The lack of power made this impossible.

### 4. In the days following the disaster, the public struggled to get reliable information. 

How did the public react to the tsunami? At first, they couldn't. The tsunami caused considerable confusion, and reliable information was hard to come by.

In 1986 Japan introduced the System for Prediction of Environmental Emergency Dose Information (SPEEDI). It uses a combination of meteorological data and real-time data from the nuclear plants to make predictions about the severity of radioactivity emerging from a plant and where that radioactivity would be.

But because Fukushima Daiichi was without power, it couldn't provide the system with any data, making SPEEDI's predictions unreliable. Among other things, this made it difficult to determine how large an area around the plant needed to be evacuated.

In addition to these technical difficulties, the Japanese government as well as traditional Japanese media hindered the flow of accurate information to the public.

Japanese government agencies are in bed with the nuclear industry, and journalists from traditional Japanese media are likewise in bed with the government, and often avoid confrontation. If they write too critically, they lose their coveted access — a clear conflict of interest.

A damning report by the Investigation Committee on the Accident at the Fukushima Nuclear Power Stations of Tokyo Electric Power Company criticized the government for keeping valuable information hidden, allegedly because it "hadn't been verified."

The time taken to "ensure accuracy," according to the report, compromised the expediency necessary to solve many crucial problems related to the accident.

The government refused to even call the accident what it really was. In order to prevent panic, they chose not to use the word "meltdown," even though this was exactly what was happening. Instead, they elected to use the more opaque "fuel pellet melt."

> _"It was like trying to investigate a homicide and not having access to the crime scene." — Charles Casto, NRC_

### 5. Japan has the greatest number of nuclear plants in the world, but also has inadequate oversight and regulation. 

After the Second World War, nuclear power offered Japan a means to avoid reliance on other countries for electricity. The country built nuclear plants faster than any other nation in the world, but its regulation and oversight lagged behind.

Today, strong ties between industry and government make adequate regulation and oversight difficult.

This was clear in a 2012 report by the Japanese newspaper _Asahi Shimbun_, which reported that 22 of the 84 members on the government's Nuclear Safety Commission had received donations from the nuclear industry amounting to $1.1 million over a five-year period.

Moreover, bureaucrats on regulatory committees know that when they retire, they can get a cushy, well-paying job within the nuclear industry. Knowing not to bite the hand that feeds them, they have little incentive to be stringent in their oversight of the nuclear companies.

For example, before the disaster, when Tokyo Electric Power Company's (TEPCO) nuclear inspector Kei Sugaoka noticed a crack in one of the reactors at Fukushima, TEPCO directed him to conceal the evidence. Sugaoka notified government regulators anyway, and they in turn ordered TEPCO, owners of the Fukushima plant, to deal with the issue themselves.

Their solution: firing Sugaoka.

More generally, accurate information about the dangers of nuclear power in general is often understated.

For many, the nuclear disaster in Chernobyl warranted a re-examination of the dangers of nuclear energy. But the Japanese government and news media told the public that the Chernobyl disaster was the result of low quality Soviet equipment and poorly trained operators. An accident like that, according to them, couldn't happen in Japan.

A 2011 _New York Times_ report revealed that 14 major lawsuits were filed against the Japanese government (all of which failed) regarding inadequate nuclear reactor safety. Often, these suits pointed to operators who downplayed sizeable hazards.

According to seismologist Katsuhiko Ishibashi, if Japan had simply dealt with the concerns raised in these lawsuits, the Fukushima disaster could have been prevented.

### 6. The Fukushima disaster devastated the economy and led to an outcry against nuclear power. 

The accident at Fukushima Daiichi left unprecedented destruction in its wake, and not just in terms of human cost. The accident had, and still has, a huge negative impact on the Japanese economy.

Only two weeks after the accident TEPCO was already requesting a $25 billion loan from Japanese banks to help finance the necessary repairs. By mid-April the government estimated that the accident would make a $317 billion dent in the economy as a result of clean-up costs and reconstruction.

The disaster also took its toll on the food industry. For example, about a week after the tsunami hit, there was an announcement that the milk from cows in the Fukushima area contained radioactive iodine-131, and was therefore unsafe to drink.

Similarly, the Fukushima prefecture had long been home to a sizeable fishing industry. When it was discovered that radioactive contaminants had leaked directly into the sea, people feared — rightly — that it would devastate the fishing industry, which is only now beginning to recover.

The accident likewise led to civil unrest, exemplified by the biggest demonstrations in modern Japanese history.

When Yoshihiko Noda assumed office as prime minister in mid-September 2011, he thought he could convince the public that Japan needed nuclear power despite its apparent dangers. He was wrong.

In just a few days, thousands of people took to the streets in Tokyo demanding that every single reactor throughout Japan be shut down immediately. But the government didn't listen.

Instead, despite considerable public opposition, the Noda government decided to restart two reactors that had been shut down in the wake of the Fukushima disaster, reactors three and four of the Ohi nuclear power plant, which lie around 60 miles northeast of Japan's third biggest city, Osaka.

### 7. US authorities’ assurances that a similar accident couldn’t happen at home are unconvincing. 

As the consequences of the Fukushima disaster became clear, the American people turned to their government with one burning question: _Could it happen here?_

In short: yes. While the conditions in the United States are certainly different from those in Japan (for example, no American plant has the same flood risk, nor does the United States see the same magnitude of earthquakes), this doesn't exclude the possibility that similarly catastrophic accidents could happen.

The question of whether what happened in Japan could happen in the United States was put to the Nuclear Regulatory Commission in March 2012, a year after the catastrophe. All five of the commissioners said it was highly unlikely that a similar event could occur in America. According to them, the circumstances were too different.

But that's not entirely true. Specifically, there are several American reactors located downstream from large dams. In the event of an earthquake or a terrorist attack, a burst dam could lead to circumstances very similar to those at Fukushima. The NRC has known that it's been underestimating this possibility for many years, yet they've still done nothing about it.

This could be because the NRC's decision-making process is built on flawed logic, and its standards are intentionally vague.

It has always been wary of taking action that could call its previous decisions into question. This is not totally unreasonable. If, for instance, the commission were to demand higher safety standards for new plants, people would be less willing to live near an "old" one that is perceived as less safe.

When the NRC was founded, its stated mission was to "provide adequate protection of public health and safety." Over the years this became watered down to "reasonable assurance of adequate safety." This change in language reveals their desire to absolve themselves of responsibility in case of disaster.

They've never even defined exactly what "adequate protection" means!

With nuclear energy, there will always be risks. We should make a serious effort to be aware and work to minimize them.

> _"How safe is safe enough?"_

### 8. Final summary 

The key message in this book:

**The earthquake that caused the Fukushima disaster in Japan couldn't have been predicted, but that doesn't mean the fallout was unavoidable. Corruption, short-sightedness and willful neglect all contributed to the severity of the disaster. Unfortunately, the lessons from the Fukushima disaster haven't made much of an impact in Japan or the United States, and reactors are still at risk of meltdown.**

**Suggested** **further** **reading:** ** _Normal Accidents_** **by Charles Perrow**

_Normal_ _Accidents_ delves into the accidents that can and have occurred in high-risk environments, like nuclear plants and dams, airplanes and even space. It shows us how mind-bogglingly complex modern systems have become, and that no one could possibly predict the trivial failures that cascade into catastrophe.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### David Lochbaum, Edwin Lyman, Susan Q. Stranahan and the Union of Concerned Scientists

David Lochbaum is the director of the Nuclear Safety Project for the Union of Concerned Scientists (UCS) and one of the United States' top nuclear experts.

Edwin Lyman is a senior scientist in the UCS Global Security Program. He specializes in nuclear proliferation, nuclear terrorism and nuclear safety.

Susan Q. Stranahan is an award-winning journalist who has written about the environment and energy for over three decades. Some of her other books include _Sesquehanna, River of Dreams_ and _Beyond the Flames_.

