---
id: 5894fc4a0be3560004037170
slug: galileos-middle-finger-en
published_date: 2017-02-06T00:00:00.000+00:00
author: Alice Dreger
title: Galileo's Middle Finger
subtitle: Heretics, Activists, and One Scholar's Search for Justice
main_color: FEF433
text_color: 666214
---

# Galileo's Middle Finger

_Heretics, Activists, and One Scholar's Search for Justice_

**Alice Dreger**

_Galileo's Middle Finger_ (2015) tackles head on the controversial issue of transgender research and the conflicts that have arisen between academics, scientists and activists. These blinks offer a behind-the-scenes look at just how dangerous an idea can be when it challenges a familiar narrative or an established ideology. They remind us that, in the face of harmful threats and accusations, it's important to be open, honest and persevering.

---
### 1. What’s in it for me? Get a nuanced perspective on transgenderism and the plight of research. 

In Saint Peter's Basilica in Florence, Italy, the middle finger of Galileo Galilei is enshrined in a small glass dome: a relic symbolizing the integrity of a scientist who remained profoundly committed to the idea of empirical research. But Galileo's commitments got him in trouble. In 1633, he was forced to recant the theory of heliocentrism that he'd fought so hard to forward. But that was a more benighted time. Today, we don't go around imposing backward opinions on forward-looking thinkers, do we?

Prepare yourself for the story of one activist and researcher who experienced severe backlash while defending controversial research on transgender people. These blinks explore what it means to be transgendered and explain why a psychology professor who offered new perspectives on that identity was threatened and protested against.

In these blinks, you'll learn

  * why the common view on transgender is simplistic;

  * that one psychology professor wanted to change that common view; and

  * about the difference between transgenderism and intersex.

### 2. Transgenderism describes a distinctly different identity than intersex. 

When discussing sexuality, many people get confused by all the complex terminology. You might hear the terms "transgender" and "intersex" and wonder what, exactly, the difference is.

First of all, the term intersex is directly related to biology and anatomy.

It describes someone whose anatomy corresponds neither to standard biological definitions of male _or_ female. An example would be someone who is born with a set of ovaries and a pair of testes.

Through the ages, the lives of people straddling the gender divide have often been difficult. Society has tended to stigmatize intersex people, inducing in them feelings of guilt, shame, grief and trauma.

Over the years, the intersex community has been subjected to countless sex "normalization" procedures designed to make individuals adopt whichever sex the doctor assigns them. The procedures are often horrific. People deemed to be more male than female might have their clitoris removed, for instance, and those deemed to be more female might be injected with hormones.

Brian Sullivan is a friend of the author who was nineteen months old when doctors discovered that he had both a uterus and ovotestes, sex glands that contain both ovarian and testicular tissue.

The doctors, reasoning that Brian might become a fertile woman, decided to remove his phallus, and so Brian became Bonnie. In her teens, she became a sexually active lesbian and realized that she was missing a clitoris and was unable to achieve orgasm.

There are plenty of other case histories, as well as plenty of evidence, that shows how normalization efforts are dangerously harmful and can lead to severe dissatisfaction in life.

The other term, "transgenderism," is related to a person's gender identity — the way _they_ identify sexually, regardless of biological definitions of sex. This usually means rejecting the gender assigned to them at birth.

In a way, the difficulties faced by people who are transgendered are the opposite of those faced by people who are intersex. Many want to undergo sex-change surgery and take hormones, but access to these resources is often very hard to secure.

Good examples of transgenderism can be seen in Bruce Jenner's public transition into Caitlyn Jenner, or the television show _Transparent_. 

The medical establishment remains heteronormative. It continues to control what gender a person does or doesn't get to be. And this presents challenges for both transgendered people as well as intersex people.

### 3. Transgenderism isn’t an either/or issue; science shows that it is a more nuanced topic. 

When someone comes out as transgender, such as when Bruce Jenner became Caitlyn Jenner, it creates an illusion. People tend to think that the transgender individual has finally found his or her true self, that, beneath the facade of Bruce, Caitlyn had always been waiting to emerge. But it's much more complicated than that.

Gender identity isn't black and white. There's a common misconception that a person's brain dictates that person's identification with one or the other gender — that a female brain can be "trapped" inside a male body, for instance.

According to this way of thinking, there are only two possibilities — you're either born with a male or a female brain. Sometimes, the theory goes, a person's physical body doesn't align with the gender of that person's brain. Thus, a person who decides to transition from one gender to the other is, in fact, revealing his or her "true inner self."

However, the gambit of dichotomizing the brain to give two categories — male or female — has but scanty scientific support. But dichotomies are simple, and since this one chimes with traditional conceptions of gender and identity, it gets a lot of approval.

Of course, there are many who reject this narrative. Psychology professor J. Michael Bailey, of Northwestern University, is one of them.

In 2003, with the publication of _The Man Who Would Be Queen_, he offered a more nuanced perspective, based on the classification system of sexologist Ray Blanchard. 

Bailey argues that transgenderism isn't an identity people are born with. Though he concedes that such people are born with gendered behaviors and sexual preferences, he regards the desire to transition as a result of multiple external factors.

This stance flouts mainstream opinion. Bailey believes that culture and society play as much of a role as biology in determining whether a person decides to remain a closeted homosexual or come out as openly gay or transgendered.

For example, an effeminate gay male growing up in a tolerant and open neighborhood is more likely to feel safe and satisfied, and thus more likely to be openly gay.

If that same person had grown up in a homophobic environment — say, in the less all-embracing community across town — he might feel that he can only survive by fully transitioning into a woman.

### 4. Bailey’s book was met with controversy and a series of damning allegations. 

As you might expect, Bailey's book met some serious resistance. It went against the standard, familiar ideas people tend to have about sex and gender, and some people were outright disturbed by its suggestions.

At the heart of what many found so troubling was the twist that Bailey's book added to the tangle of debate surrounding transgender sexuality. In addition to homosexual males who wish to transition to being female, Bailey identified another group of males — males who identify as men but are sexually aroused by the very _idea_ of being a woman.

This was termed _autogynephilia_, which is a combination of "auto," for self-directed, and "gynephilia," which means "love of females."

This term describes a male with a stereotypical boyhood: playing sports, liking cars or motorcycles and perhaps even dreaming of someday joining the military. Autogynephilic men, like more stereotypical men, are also primarily attracted to females; they typically get married to women and have children before making the decision to transition. 

After Bailey's book came out with this information, there was an avalanche of angry responses calling the work blasphemous.

The very idea of autogynephilia was offensive to many, including transgender women, but the controversy eventually came to center around the author's allegedly unethical actions.

There was a landslide of libel. According to his detractors, Bailey had violated federal regulations by failing to get approval from the Board of Ethics for his study on transgenderism, disregarded patient confidentiality, practiced psychology without a license and slept with a trans woman while she was a research subject.

Bailey was also terrorized by multiple transgender activists.

One of them was Andrea James, who sent Bailey a particularly disturbing photo of his family. His children's eyes were blackened out and the caption posed the question of whether his wife was a "cock-starved exhibitionist, or a paraphiliac who just gets off on the idea of it." The activist even posted the photos on her own website. 

In the next blink, we'll take a closer look at exactly why transgender groups were so furious with Bailey.

### 5. Transgender activists were particularly outraged by Bailey’s emphasis on sexuality. 

Threatening an author's family is unjustifiable. But the backlash against Bailey's theories is understandable. Activists had spent lifetimes trying to desexualize transgenderism and reduce the stigma it tends to carry. These same activists had worked hard to improve health care access and establish some basic human rights for trans people.

Part of this effort was getting the term trans _sexuality_ changed to trans _gender_ in order to emphasize gender identity and de-emphasize questions of sexuality and sexual attraction.

The progress they'd made was hard won, the battle ahead uphill. Bailey's focus on sexuality was seen as a big step backward.

Much of the fight against the sexual aspect of the trans movement has to do with health care. The medical establishment can be over-reliant on heterosexist ideas, tending to think that only naturally feminine males could ever convincingly pass as straight females. To think otherwise would utterly disrupt traditional conceptions of what is and isn't female.

It's this kind of narrow thinking that results in masculine autogynephilia patients being denied the hormones they request.

Others members of the medical community still consider male-to-female transgenderism mere fetishism; some go so far as to call it a form of mental illness, and argue against people having any access to hormones or surgeries.

Paul McHugh, a psychiatrist at Johns Hopkins University School of Medicine, has drawn invidious comparisons between sex reassignment and "liposuction on anorexics." (Evidence indicates that most people are actually healthier and happier after transitioning.)

It's opinions like McHugh's that transgender activists are especially concerned with. They see the sexualizing of their cause as central to the perpetual violation of their rights.

These violations extend into all areas of life. In many states, trans people are regularly — and legally — denied housing, employment and schooling.

For example, disability discrimination is illegal under American federal law, but the legislation specifically excludes coverage for transgender people. Although some states, like Washington, have ruled that such laws should be extended to protect transgender people, such protections are not applicable nationwide.

Police have also been known to refuse to investigate hate crimes against trans people — or even murders. And emergency workers sometimes opt not to treat trans people with life-threatening injuries.

> In 1969, one clinician claimed a single instance of cross-dressing induced arousal should disqualify a person for sex-reassignment surgery.

### 6. Closer inspection of the allegations against Bailey revealed no wrongdoing. 

With all these complaints against Bailey, the author assumed that he was probably guilty on at least a few counts.

But when she began looking into Bailey's work, she couldn't find any evidence to back up the claims against him.

One claim, filed by three trans activists, accused Bailey of practicing clinical psychology without a license. Yet Bailey never claimed to be a clinical psychologist or someone who was offering any therapy at all. In all of Bailey's letters and conversations, he clearly explained his credentials as being strictly those of a scholar and non-clinical psychologist. 

More surprising still was the author's discovery that Bailey had helped some of the subjects in his book obtain sex-reassignment surgery by writing letters of recommendation. These surgeries are difficult to get and are one of the central things that trans activists fight for. It suddenly seemed to the author that the activists should have praised Bailey, not ruined his career.

The state regulations also showed that a license isn't required if a person doesn't charge money, even if it appears that they're offering "clinical psychological services." And since Bailey never asked for money, the licensing department threw out the claim against him.

But all these claims and attention had already taken their toll on Bailey's reputation.

The activists even managed to turn his research subjects against him, convincing them to file complaints that charged him with unethical behavior.

When the author talked to Bailey's subjects, she learned that the activists used them to tarnish Bailey's reputation, and then kicked them to the curb. Ironically, some of them felt far more exploited by the activists than by Bailey.

### 7. The reactions of activists show how science can be politicized when the facts don’t fit the agenda. 

When the author criticized the harmful tactics of Andrea James, she soon got her own taste of what it's like to be the target of an angry trans activist.

After discovering that her employer, Northwestern University, had invited James to speak, the author wrote a critical post on her blog, entitled "The Blog I Write in Fear."

As the title makes clear, she expected to receive criticism for the post. But she couldn't help expressing her opinion that James wasn't good for the school's reputation or for trans rights.

Sure enough, emails from James soon arrived. In one of them, James called the author's son a "precious womb turd," and another closed with a menacing message, telling her that, "we'll chat in person soon."

Still, the author decided she wanted her research to be publicized. So she called the _New York Times_. Soon enough, the journalist Benedict Carey published a piece about her findings.

James wasn't the only person who'd moved from attacking Bailey to attacking the author.

One activist wrote a letter to the _New York Times_ claiming that "the Bailey group" had also funded the author's work. This was patently untrue. The author had used her own tiny income to fund her work, and, in an attempt to protect her employer and her job, she had even kept her research on Bailey a secret from Northwestern University.

Perhaps more than anything else, this conflict between scholars and activists, with its messy trail of lies and accusations, shows how much more patience and understanding is needed when discussing transgenderism.

It certainly became apparent to the author that progressive causes can easily become inflexible ideologies, privileging a single agenda above all else.

She also noticed how politicized science has become. Even ostensibly objective scientific fields such as biology are in danger of being dragged into political and ideological conflicts.

If people want to really be progressive, they need open hearts and open minds.

### 8. Final summary 

The key message in this book:

**When it comes to certain issues, like those surrounding transgenderism, both the traditional medical establishment and progressive activists can be guilty of sticking too adamantly to convenient narratives and ideologies. When this happens, facts get bent out of shape or neglected altogether, and, in the end, this damages evidence, science and progress in general.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Demon-Haunted World_** **by Carl Sagan**

_The Demon-Haunted World_ (1995) helps the reader distinguish between dangerous pseudoscience and real, hard science by exploring the critical-thinking tools scientists use to make their discoveries. The author argues for science's place in education and popular culture, and offers his advice on how we can incorporate more critical thought into our society.
---

### Alice Dreger

Alice Dreger is a historian of medicine and science. Her work has been the subject of articles in the _New York Times,_ the _New Yorker_ and _Science_ magazine. Her other books include _Hermaphrodites and the Medical Invention of Sex_ and _One of Us: Conjoined Twins and the Future of Normal_.

