---
id: 56cdb38674766a000700007c
slug: games-people-play-en
published_date: 2016-02-26T00:00:00.000+00:00
author: Eric Berne, M.D.
title: Games People Play
subtitle: The Psychology of Human Relationships
main_color: 27719E
text_color: 1A4D6B
---

# Games People Play

_The Psychology of Human Relationships_

**Eric Berne, M.D.**

_Games People Play_ (1964) explores the fascinating and bizarre world of psychological games, where players unconsciously manipulate each other into acting in alienating and self-destructive ways. Eric Berne dissects the hidden dynamics beneath the games people play — and shows how to escape from them and find true intimacy.

---
### 1. What’s in it for me? Learn all about the games people play. 

Are you a game player? Although you might not be aware of it, the answer to that question is almost definitely yes.

After reading these blinks, however, you should be able to understand yourself, and others, a bit better. Drawing on his invaluable knowledge about the psychology of human relationships, the author provides insights on how to spot the games that are taking place all the time, in all kinds of settings and between all kinds of people.

In these blinks, you'll discover

  * the games couples start playing when the honeymoon ends;

  * why people play _Now I've got you, you son of a bitch_ ; and

  * about the opportunities a life without games can offer.

### 2. Every person has three ego states: Parent, Adult and Child. 

You might have noticed that, despite the general chaos of human behavior, there are certain recurring behavioral patterns.

The author certainly noticed this.

After observing thousands of patients, he posited that, when interacting, people act from one of three _ego states_ — _Parent_, _Child_ and _Adult_. These states comprise systems of feelings, thoughts and behaviors, and are developed over the course of a lifetime. Which ego state you're acting from at any given moment depends on both your past and the present moment.

For example, while growing up, each child imitates its caretakers — and that's where the Parent ego state comes from. Say your mom got angry with you when you did something wrong, and showed her anger by shouting. As an adult, you may unconsciously adopt this kind of behavior, raising your voice when your child misbehaves. Of course, the Parent state needn't be negative — just an unconscious imitation of your parent or parents.

Then there's the Adult ego state, the source of our rational thinking. It develops as we learn how to reflect on our experience throughout childhood, and allows us to make decisions based on what is present in the here and now. It's the state that processes information and tackles problems with assertive, logical thinking. It emerges when you ask someone to stop crunching popcorn in the cinema, for instance, or when you analyze a broken engine to see what needs to be fixed.

Finally, the Child ego state is the spontaneous way of being that we're born with. It's the origin of our emotions, creativity and intimacy. But, over time, the Child can get buried beneath the Parent and Adult states; it is possible, however, to free the Child of these influences, and return to the spontaneity of the natural Child. 

For instance, we often act from the Child state during sex — an activity that isn't taught by parents or consciously learned.

### 3. Games are predictable interactions between ego states. 

So people have ego states — now what? Well, understanding ego states is the foundation of understanding the many kinds of _games people play._

Whenever you communicate with someone, you interact from one of your ego states. For example, you might be acting from your Parent state and your interlocutor might be reacting from her Child state, or you might both be communicating from an Adult state.

Sometimes this is obvious, like when you're scolding your partner for not doing the dishes (Parent-Child), or when you're planning a trip with a friend (Adult-Adult). In both cases, the ego state and goal are clear. But sometimes it seems as if you're acting from one ego state, when in fact you're acting from another. And what appears to be the goal on the surface isn't the goal at all.

When that happens, you're playing a _game._

Consider a man flirting with a woman. At the end of the evening, he says that he'd like to show her his record collection. She replies that she loves records. Even though it sounds like two Adults having an innocent conversation, it's actually two Children enjoying the spontaneity of flirting. And, of course, the goal of perusing the collection of records is veiling the real aim — sexual intercourse.

In this case, the man and woman both know what game they are playing. But it's not always like that. Many games are played unconsciously, with neither player knowing what is being played — nor why they're playing it. 

That's why it's only by understanding the games people play that you can free yourself from the ones that are holding you back. Read on to find out more about the games _you_ play.

> _"Parents, deliberately or unaware, teach their children from birth how to behave, think, feel and perceive."_

### 4. Some games are designed to last a lifetime. 

When you hear the word "game," you probably think of something brief, like a soccer match, or a round of _Call of Duty_. But games can last much longer — and be deadly serious, too.

Consider the game of _Alcoholic_. If you look closely, alcoholic behavior is in fact a complex game, with hidden motives and specific goals.

When an alcoholic asks for help, it seems like she is acting as a rational Adult. But, more often than not, she is in fact challenging the other players of the game — like her children or partner — to try and stop her drinking. This is the action of a rebellious Child. And while the other players appear to be Adults reasoning with the alcoholic, they are actually playing the role of the Parent scolding the Child.

The alcoholic then gets what she wanted all along: the anger of other people, which allows her to fuel her self-pity and self-hatred — which leads to more drinking.

Another game you can play your whole life is _Now I've Got You, You Son Of A Bitch._

The main character in this game typically has a past of unexpressed anger waiting to burst out. To fulfill his need to rage, he seeks out every tiny possible kind of injustice — and pounces on it.

For example, if he gets accidently overcharged by, say, a plumber, he would act infuriated, but secretly be delighted. Here is a chance for him to unleash his anger, and he will gladly scream at the poor plumber for hours on end about the scandalous mistake.

The plumber may well be stuck in his own game, acting like a naughty Child who always gets caught for his misdoings. This sustains his own narrative of victimhood — the "reason" for his unhappiness.

### 5. Spouses often play games with each other. 

Marriages are notorious for mangling even the most solid relationships, especially once the honeymoon phase is over. Partners have to make more and more compromises, while simultaneously trying to find ways to fulfill their conflicting needs. 

No wonder they start playing games.

One game spouses play is called _Courtroom._ In this game, a married couple goes to a therapist to work on their relationship. 

Or so they think.

On the surface, it looks like the couple and the therapist are all in the Adult state, looking for a way to solve the couple's problems. But in fact, one spouse acts as a Child, and complains to the therapist about his or her partner. The therapist then takes on the role of the Parent, and judges the partner's behavior as unacceptable. 

This allows the therapist to take the position of moral superiority, and the Child-state spouse gets a Parent who validates his or her complaints.

Another game married couples often find themselves playing is called _Frigid Wife_.

In this game, the woman sexually provokes her husband, but rejects all of his advances. For example, she might walk around half naked, but, when he makes a move, she accuses him of being obsessed with sex. 

So what states are at play here? First, the wife offers herself sexually, but not as an Adult. Instead she acts as a Parent, saying "you can kiss me if you want." The husband then replies enthusiastically as a Child — yes, please! — only to be rebutted by the wife still acting as a Parent: "that's all you want isn't it?"

The result is a wife who can reinforce her prejudice against men — that they are all obsessed with sex — and a husband who has no sex with his wife. Because there's another twist to the game: often the men don't even want sexual intimacy — that's why they chose someone to play Frigid Wife with.

### 6. Social gatherings often make people play games. 

Some party games, such as charades or _Catch Phrase_, are perfectly harmless. Others, however, are more subtle and devious.

Take _Schlemiel_, for instance — a game about forcing someone to forgive you. The main character in this game "accidently" breaks things when invited to someone's house for a party. It might be spilling wine on the carpet or clogging the toilet.

On the surface, it looks like the Schlemiel apologizes for their mess as an Adult, and the host gracefully accepts the apology. But, in fact, the Schlemiel is trying to control the host, forcing them to be a model of self-control and forgiveness as a Parent. The Schlemiel can then continue being an irresponsible Child, knowing that the host will have to extend forgiveness, no matter what.

Another typical party game is _Why Don't You - Yes But._

The player starts by sharing a problem with a group. For instance, this person is having trouble figuring out which car to buy. The group then offers suggestions: this model is good for long distances, a new car is worth the higher price, and so on. But the suggestions are actually irrelevant, because the player always comes up with some reason to dismiss each solution.

At first glance, it looks like the main character and the group are speaking as Adults, trying to find a rational solution to a real problem. But, actually, those making suggestions are assuming the role of a Parent, trying to help a Child who repeatedly answers: "no matter what you do, you can't help me." This allows the player to maintain her sense of being a Child burdened with an unsolvable problem.

### 7. Sexual relationships often provoke games. 

The bedroom is a great place for fun and games. But it's also the source of many complex psychological games that aren't about sex or fun at all.

A twisted example is the game of _Rapo,_ where sex becomes a means of exacting revenge.

In Rapo, the main character incites a sexual act, then accuses her sexual partner of assault. The confrontation seems to happen between Adults: the main character demands compensation for the assault, and the violator apologizes profusely for taking things too far. But the interaction is actually happening between Children: the violator secretly rejoices in the experience of being sexually irresistible, while the main character confirms preexisting prejudices about the bestial nature of sex.

But, fundamentally, this game is about guilt. By placing the responsibility for the sexual act on the violator, the main character gets to have sex without feeling any guilt about being a sexual being.

Another sexual game is _Uproar._

In Uproar, the players attempt to diffuse uncomfortable sexual tension by getting into a fight. For example, a father and his teenage daughter might have unwanted sexual feelings — especially in a house with a Frigid Wife. To avoid the sexual attraction, one of the players starts an argument that quickly escalates into a quarrel. Soon, one player has had enough, and leaves the room, slamming the door behind them. This is the desired outcome of the game: by ending up in different rooms, the players emphasize the fact that they are sleeping apart, and not together.

### 8. People with rule-breaking tendencies often play games. 

Consider all the famous Hollywood movies about con men, thieves and drug dealers. Obviously, we're pretty fascinated by criminals. But have you ever wondered what drives people to lead such dangerous lifestyles?

One motivation is the desire to play _Cops and Robbers._

In Cops and Robbers, it seems like the criminal is acting as an Adult, calculating ways of taking a shortcut to luxury. But, in fact, the desire for a lavish lifestyle hides the real motive: to get caught.

Criminals playing Cops and Robbers actually want to get caught, because being captured will confirm them as losers, which is exactly how they see themselves. This is why many criminals "accidentally" leave clues or act arrogantly after a crime: so the police can finally catch up with them.

Once the criminal has been sentenced and been put in jail, he might start a new game: _Want Out._

This game involves prisoners who pretend they want to escape from jail, when, in fact, they just want to stay longer. They know that chances of escape are next to none — and that, if they get caught, their sentence will only be extended.

Yet they take the ridiculous risk. What at first appears to be an Adult yearning for freedom is in fact a Child who doesn't want to return to the unpredictability of the outside world. So the prisoner tries to escape, gets caught and is secretly happy to stay within the safety of the prison.

### 9. Psychotherapy offers plenty of room for games. 

After reading about all these games, you might be thinking that everyone could do with a good dose of psychotherapy. Unfortunately, the field of psychotherapy is _also_ a place where strange games abound.

Take the game of _Indigence_, where the psychotherapist and client work together to make sure everything stays the same.

The client starts by bringing a problem to the therapist, like being unemployed. So they discuss the issue, supposedly as two Adults looking for a rational solution, but, in fact, neither of them wants anything to change.

Why?

Because they both have more to gain from keeping things the way they are. The therapist gets to keep a client and keep acting as a caring Parent, and the client perpetuates his role of incompetent Child and doesn't have to make any frightening changes, such as venturing out into the world of work.

Therapists also play the game of _I'm Only Trying To Help You._

This game is all about the therapist's ego. A patient comes with a problem, and the therapist offers a solution — seemingly this is a Child asking a Parent for help. But in I'm Only Trying To Help You, the therapist proposes a solution that they know won't work. When the patient comes back and reports that the solution didn't work, the therapist secretly condemns the patient's incompetence. This allows the therapist to reinforce her self-image: she is a competent Parent living in a world of incompetent Children.

We've seen how much of our behavior is dictated by the games we play. So what happens when we manage to stop playing them?

### 10. A life without games offers an opportunity for closer relationships. 

Our journey through the intricacies of human psychology has made one thing clear: games can make life miserable. So, why do we continue to play them?

First of all, we never consciously decide to start playing them. Games develop over long periods of time; most of them were invented long before our birth and, as we grow up, we're taught to play them without even realizing it. Each culture and even each family has its own games, which accounts for the mind-boggling range of bizarre moves players can make.

Secondly, games have an important function: they allow people to interact without getting intimate. Most people are uncomfortable revealing their true selves to other people. By playing games, the players can slip into comfortable routines and hide behind different roles, instead of really getting close to each other. This allows them to be social without being vulnerable.

But vulnerability and intimacy are necessary for true human connection. And even though it's difficult, if we want to truly connect, we have to give up our games.

How?

First we must learn about the many different games people play. This is a matter of becoming aware of our ego states and of paying close attention to our interactions with other people. We then need to disrupt the games by dropping our masks and allowing ourselves to be vulnerable, and to do our best to see through the masks of others.

Don't be fooled — it won't be easy. But what's the alternative?

> _"Games are passed on from generation to generation."_

### 11. Final summary 

The key message in this book:

**People play games with each other every day: complex, often unconscious interactions that hide the true motives and goals of the players. Fuelled by their fear of intimacy, players can remain stuck in games all their lives. But by learning about the many games and their hidden dynamics, we can break free of their bonds and create honest, meaningful human connections.**

Actionable advice:

**Help a friend help himself.**

When a friend who always rejects your advice asks for help, your game alarms should start ringing. You know she's not really looking for help, and just wants you to act like a Parent. Instead of offering a list of solutions, just say that the problem really is a hard one, and ask her what _she_ thinks she should do. That will simultaneously get her thinking and preempt her usual game.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Influence_** **by Robert Cialdini**

_Influence: The Psychology of Persuasion_ explains in detail the fundamental principles of persuasion that get us to say yes, including how they are used against us by compliance professionals like salesmen, advertisers and con artists. Knowing these principles will allow you both to become a skilled persuader yourself and to defend yourself against manipulation attempts. To find these blinks, press "I'm done" at the bottom of the screen.
---

### Eric Berne, M.D.

Dr. Eric Berne (1910-1970) was a Canadian psychiatrist who developed the influential theory of transactional analysis in the mid-twentieth century. He wrote over 30 books, including _What Do You Say After You Say Hello?_ and _Transactional Analysis in Psychotherapy_.

