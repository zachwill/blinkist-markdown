---
id: 5e70d0d96cee070006c42c21
slug: gamification-for-business-en
published_date: 2020-03-19T00:00:00.000+00:00
author: Sune Gudiksen, Jake Inlove
title: Gamification for Business
subtitle: Why Innovators and Changemakers Use Games to Break Down Silos, Drive Engagement and Build Trust
main_color: 66BFAD
text_color: 3D7363
---

# Gamification for Business

_Why Innovators and Changemakers Use Games to Break Down Silos, Drive Engagement and Build Trust_

**Sune Gudiksen, Jake Inlove**

_Gamification for Business_ (2018) explores how businesses can use games to overcome organizational challenges and optimize performance. Drawing on their extensive experience in game design and innovation, Sune Gudiksen and Jake Inlove show how games can be leveraged to encourage teamwork, boost employee motivation, and map out new pathways for progress and change.

---
### 1. What’s in it for me? Overcome organizational challenges with the power of games. 

It's common for businesses to hit a bump in the road at some point in their journey. Often it's because communication between teams has broken down, employee motivation has run dry, or leaders have become less able to produce imaginative ideas. But what if there were a fun way to get the cogs of the company turning again?

This is where _gamification_ comes in — applying the principles of games to a non-game context. Beyond being simply entertaining, games are highly interactive, inclusive, and immersive. They can help businesses solve key organizational challenges — from lack of clear business goals to unproductive meetings — and create a powerful learning experience for managers and employees. 

These blinks will lift the lid on typical problems businesses come up against and give concrete examples of games that can be used to help solve them. You'll learn how games can help organizations open up honest and constructive dialogue among employees, unlock creative ideas, and discover new pathways for innovation. 

Along the way, you'll also learn

  * the holistic benefits of incorporating games into your business;

  * why games level the playing field and encourage more collaboration; and

  * how games can help managers improve their leadership.

### 2. Games facilitate learning and level the playing field. 

Think back to when you were a child and the games you used to play. Maybe you had a favorite toy that accompanied you on imaginary expeditions through blistering winds and scorching deserts, or a castle built entirely from Lego. Or maybe getting lost in the trees in a game of hide-and-seek was more your thing. 

Whatever these weird and wonderful games were, they helped you learn and develop through tactile experiences and actions. 

This _learning-by-making_ is sometimes referred to as _constructionism,_ a term coined by MIT professor Seymour Papert and entrepreneur Idit Harel. According to their research, combining multiple senses — for example, seeing and touching — helps us to process and absorb information better.

This is what happens in games. Often, we're given physical objects — like colorful cards or game pieces that we have to maneuver or arrange — and scenarios that force us to think outside the box. In short, we learn and get better by _doing_.

That's why games are useful to businesses: they help employees build new skills and create an environment in which they can explore and formulate new ideas. 

Games can also help to shake up the usual hierarchy of an organization and create a space where people can meet on a level playing field. Imagine being part of a soccer team. When you're out on the field, it doesn't matter who you are, where you come from, or what you do for a job — all that matters is your ability and willingness to play.

This is what gamification experts Katie Salen and Eric Zimmerman refer to as a _magic circle._ When we play games, we become part of a new reality where different social rules apply. Games pull us out of our usual ways of operating, allowing us to move in and out of different roles and imagine new ways of working. 

As the authors explain, businesses often squeeze the fun out of meetings and training sessions, creating a stale environment that lacks creativity. Why? Because they believe play isn't a productive way to help employees learn and acquire new skills. 

But as the following blinks will show, using games can help businesses solve an array of organizational problems and yield multiple rewards.

> "_Business games are a vital ingredient in innovation pursuits and the process of change."_

### 3. Create an identity for your company based on core values with the Align game. 

Picture this: you're the founder of a tech company that's expanded to multiple European countries. So far, things have been going well. Your product has been easy to sell and your growth has been rapid. So, what could go wrong from here?

Well, a few things. For starters, working with international clients means that your teams, which are spread across different countries, will need to work more collaboratively and cohesively. Secondly, as multiple competitors enter the market, your need to frequently improve your product, increase customer satisfaction, and ramp up sales will become even greater.

What's more, amid all of this growth, your employees could start to become disconnected from your original vision for the business, which will affect their motivation. 

With all of this in mind, it's important to regularly check in with your employees. Are they happy and motivated? More importantly, do they understand and align with your company's overall purpose? If the answer is no then it may be time to reevaluate. 

One way businesses can ensure that employees stay motivated is by creating a company identity based on clearly defined values. So how can companies go about this?

One way is to play _Align_, an online values game. The game aims to help organizations explore and implement core values that employees can get on board with, by steering participants into recognizing the connection between daily challenges, core values, and success. It also encourages employees to make decisions based on core values. 

Here's how it works: four to eight players, sitting around a game board, are each given a tablet. Each participant receives at random either a challenge, event, dilemma, or chance card. Each type of card describes a situation and players read these aloud to the group before deciding with their team members how they should respond to it.

Each player is scored based on how well their response aligns with both the game's core values of courage, passion, and customer focus, as well as the three organizational areas of employee satisfaction, customer satisfaction, and total sales. 

Ultimately, the aim of the game is to collaboratively produce the highest score possible for the organization as a whole. The result? Participants experience living out company values in their daily work.

### 4. LinkXs can help businesses improve team performance and generate collaborative behavior. 

Whether you're working for a small start-up or a large corporation, having a solid team that works cohesively is essential. And since a lot of games are designed to encourage people to work together to solve a problem or beat an opponent, they're a great way to improve teamwork.

One standout game that helps individuals get more comfortable with collaboration is _LinkXs_. It's played by eight to twelve people, who are divided into groups. Each group is given a task to complete, but information about how to complete all the tasks is divided up and spread across the different teams. Since each group only has access to partial information, they must negotiate with each other to obtain what they're missing. 

Doing this helps participants discover new ways of working collaboratively. Takeaways from the game may include ways to optimize productivity, help others succeed, deal with conflict, and balance personal goals with those of the wider team. 

A lot of these things come down to one crucial aspect of teamwork: effective communication. Luckily, there are games designed to help develop communication skills. 

Some of these can be played quickly on a company day or as part of a training program to encourage open dialogue and get people mingling. One such game is _Sudden Survey,_ designed by Indian learning expert Sivasailam Thiagarajan.

The aim of this game is to get teams of three to five people to explore a topic related to the content of the meeting or training session as quickly as possible. Players are given four minutes to build a strategy for collecting data about the topic at hand from other participants; they then have five minutes to execute it. 

It's like speed dating, but in a business context: players have to jump from one person to the next, asking questions and listening for key information. 

Afterward, participants are given a further five minutes to analyze the data and write three key findings on a poster. 

Sudden Survey is a great way to open a group session and set an energetic pace for the activities ahead. It allows participants to hone skills such as networking, gathering knowledge about peers, and asking questions sensitively and constructively.

> "_Excitement in games is motivating, but it also helps us to learn and remember."_

### 5. Use elements of play to optimize your business meetings. 

Anyone who has worked for a business has probably had to endure their share of uninspiring meetings — the kind where a presenter is doing a lot of talking and people are nodding their heads politely in agreement, but they're not really listening.

The problem in this scenario is that nothing important is sinking in. An array of organizational goals and strategies are being thrown in the air, but they'll probably be forgotten about by the time everyone leaves the room. The majority of attendees aren't participating directly in the conversation. People's viewpoints will likely be left out and the most sleepy attendees will miss out on key information. 

Author Dan Roam calls these unproductive assemblies _blah blah blah meetings_. He suggested that while words are powerful, they're not enough to identify and solve the problems an organization is facing. What businesses need are concrete tools that help them take action. 

One such tool is the _Meeting Design Game_. It aims to help businesses plan upcoming meetings with a visual, clearly thought-out structure.

The game can be played either alone or in teams, and the rules are simple. If you're playing it as a team, spread the _design elements_ cards on a table. The cards cover 12 elements: objectives, content, meeting flow, evaluation, social activities, participant involvement, physical set-up, venue, sustainability, technical solutions, food and beverage, and local inspiration. 

Then decide together which design elements are the most pressing to discuss in the meeting. Order them in a row in terms of priority, with the most important to the left and the least important to the right. 

Next, move cards that you personally have ideas for. If you have an idea for how to explore objectives in your upcoming meeting, for example, then move this card closer to you; if not, move it away. Repeat this process with the rest of the cards.

Now, take a look back at the design elements that you moved toward you and that you wish to explore further. For those elements, assemble a visual action plan of what you will discuss during the meeting about the topic. 

The result? A meeting with clear priorities where everyone is engaged and involved.

### 6. The Business Branching game can help you balance ongoing projects and new opportunities. 

For many businesses, keeping existing projects going while also seeking out new and innovative opportunities can seem impossible. 

But according to business thought leader Rita McGrath, it's increasingly difficult for companies to secure long-term competitive advantages, meaning that they also need to regularly score quick wins to compete. 

McGrath describes long-term business advantages and quick wins as different branches; businesses need to find ways to re-allocate resources from old business branches — that is, ongoing long-term projects — to new ones, while still exploiting the opportunities of the old. Some of these old business branches may also have to be revised or closed down altogether. 

Think of it like moving house. Your old home will probably stay open for a while as you move essential things over to the new place. In the process of sorting through your belongings, you might find items you'd forgotten about which are still in good condition and can be reused, or things you can simply throw away. 

There's no doubt about it: finding this balance between allocating time and resources to old projects and new initiatives at the same time is tough, but it's key to a company's long-term profitability. So how can businesses practice this?

Enter the _Business Branching_ game. This game is based on McGrath's _value cycle_ theory, known as _upstream flow_ in the game, which deals with how businesses can deploy the right resources in a business branch's five stages of development: launch, ramp-up, exploit, reconfigure, and disengage. 

The aim of the game? Launch new branches before old ones die out. Otherwise, the business will face bankruptcy.

Throughout the game, participants have to create concrete initiatives to move "upstream" from one stage of the value cycle to the next — for example, by creating a new sales plan that will help take a business branch from the launch to the ramp-up stage — or sideways, which means jumping from an old business branch to a new one. Along the way, they also have to decide what resources are needed at each stage in the value cycle.

This game helps businesses strike the balance between focusing on existing branches and giving their attention to new ones. It also helps players see that launching too many new initiatives without the necessary resources and skills to back it up is also a bad idea.

### 7. Get unstuck and generate new ideas with Innovate or Dinosaur. 

_Innovate or Dinosaur_? The name may sound bizarre, but this collaborative game helps weary organizations that are fresh out of inspiration unlock their innovative potential.

How? Well, that becomes clear when you consider the reasons businesses get stuck in a rut. 

As neuroscientist Donald Hebb points out, it's easy to become fixed in our thinking. When we get used to thinking of things in a certain way, our neural pathways become wired accordingly; our brains automatically ignore information that doesn't align with our existing thinking patterns. 

The good news is that we can rewire our neural pathways quickly — and Innovate or Dinosaur is a great way to do it. The game is played in two parts. The first is played on the _Explore_ board, where players are challenged to outline key problem areas in their organization. They then receive several different cards — determined by rolling dice — and these prompt them to propose innovative ideas for tackling the problems. 

In the second half, the Explore board is exchanged for the _Evolve_ board, and participants are asked to refine their ideas, put them into action, and note how much effort that took.

The game takes its name from Professor Eric J. Romero, who urged companies to ditch their tired "dinosaur" mentalities in favor of unconventional thinking — a must for survival in today's competitive business landscape.

Take an international property management company that the authors worked with. In order to stay ahead of their competition, it needed to make its asset classes better, faster, and cheaper. 

After dozens of company ideation sessions, the process had lost its sheen and the company was no closer to a concrete solution. 

Innovate or Dinosaur helped them get unstuck and generate ideas that helped company teams optimize their performance and improve the quality of their service. One Director of Operations, initially skeptical of the game's potential to help the company break out of its innovation rut, said he was shocked by the speed with which his team began generating creative ideas. 

Innovate or Dinosaur shows players what it really takes to make a business idea a reality. It also builds the capacity of employees to innovate in their working lives, preventing them from sinking back into dinosaur thinking.

### 8. Improve your management skills with game-based leadership development tools. 

Sometimes, being a manager is no picnic. In addition to all the rewarding aspects of leading a team, you also have to shoulder a lot of problems, have difficult conversations, and make unpopular decisions for the good of the organization. 

Regardless of whether you believe you're a good manager or not, it's important to check in with yourself now and again, and consider ways you can improve your leadership. Fortunately, there are several games that can help you do this.

One of these is _Wallbreakers_, which helps teach one of the most effective facets of leadership: the ability to implement long-lasting, positive change in an organization by motivating employees to get on board with change rather than resist it. 

Wallbreakers starts with a made-up scenario in which a large IT services company buys a smaller one. In groups of three to six, teams have to help their employees navigate a change process in three stages: start-up, implementation, and anchoring. Along the way, the team has to consider a handful of factors, including change intensity, available resources, and the personality types and individual needs of employees. 

Each team is represented on the gameboard by a small model bus. Each bus contains pawns representing different employee personalities. In each round, participants choose the pace of change by picking up one of four "gear" cards. Then, each team is invited to consider how they will balance the pace of change while also focusing on their department's daily tasks. 

The catch? Each employee will likely respond differently to the proposed changes. If an employee reacts negatively, the team can elect to kick him off the bus, leaving him behind. 

This may sound ruthless, but leaving the bus in the game is an analogy for how being resistant to change can hamper an organization's progress and prevent your individual development. 

The ultimate point of the game is that players get to see how leadership skills can be used to deal with different personalities and manage people's concerns sensitively. They also get to witness strategies that can be used in real life to get employees on board with change.

### 9. Final summary 

The key message in these blinks:

**While games have often been viewed by businesses as irrelevant and time-consuming, playfulness can be a central part of learning and innovation. When applied to specific circumstances, games can have far-reaching effects in helping employees practice alternative roles, open up new ways of thinking, and bring about positive change. Far from simple tools for entertainment, games can help organizations unlock their innovative potential.**

Actionable advice:

**Recruit a facilitator to keep order during gameplay.**

Healthy competition in a game scenario is great, but not when it leads to conflict. That's why it's a good idea to have a facilitator on hand to defuse heated debates and keep tabs on how each player is feeling. It will also ensure participants stay focused on the game and its benefits, rather than wasting time on arguments. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Gamification Revolution_** **,** **by Gabe Zichermann and Joselin Linder**

In these blinks, you've learned how businesses can use games to iron out internal problems, boost employee motivation, and enhance managers' leadership skills. But what about other important things — like your customers or potential hires?

The blinks to _The Gamification Revolution_ (2013) explore how you can engage consumers in the age of social media, attract and recruit the best talent, and get the best out of your employees — all by using the latest innovations in game design. They also include advice from a leading figure in the gamification industry on how to generate customer loyalty on a budget, ramp up sales growth, and stand out in saturated markets. 

So, if the idea of innovating through gamification has got you interested, check out the blinks to _The Gamification Revolution._
---

### Sune Gudiksen, Jake Inlove

Sune Gudiksen is an associate professor of design and innovation management at Design School Kolding in Denmark and the founder of the Biz Games community platform. Jake Inlove holds a master's degree in Education Science from the University of Aarhus and is a game designer and innovation consultant. Together, they founded Gamebridges, a company that creates business games to help organizations stimulate innovation.

