---
id: 5623dce632646600071f0000
slug: gemba-kaizen-en
published_date: 2015-10-20T00:00:00.000+00:00
author: Masaaki Imai
title: Gemba Kaizen
subtitle: A Common Sense Approach to a Continuous Improvement Strategy
main_color: A12022
text_color: A12022
---

# Gemba Kaizen

_A Common Sense Approach to a Continuous Improvement Strategy_

**Masaaki Imai**

_Gemba Kaizen_ (1997) is an introduction to the Japanese business philosophy of Kaizen, which revolutionizes working standards to reduce waste and increase efficiency at little cost. Author Masaaki Imai reveals the aspects of Kaizen that are crucial to building lean business strategies.

---
### 1. What’s in it for me? Achieve more with less. 

Imagine you're an industrialist and your country has just been ravaged by a war. You have lost half of your staff, several of your factory buildings have been destroyed and supplies are scarce. 

How do you manage?

That's exactly the situation many Japanese industrialists faced after the World War II — and manage they did. In fact, they found a way to elicit superb results with minimal resources, and Japanese industry experienced a meteoric rise as a result. As a manager, you'll always want to achieve more with less. Luckily, it's possible right now, anywhere, without an expensive upgrade in technology. 

Many Western companies already emulate the thrifty Japanese modes of production, and thrive while doing so. So what's the secret? In these blinks, you'll learn about one of these techniques: Gemba Kaizen. You'll find out how to analyze your work protocols, cut out anything that's not necessary and involve all your employees in the process. 

You'll also learn

  * how red tape can help you become more productive;

  * how to get to the bottom of every problem with just five questions; and

  * why it can actually be a good sign if a manager is not in her office all that often.

### 2. Gemba Kaizen is a low-cost strategy that increases productivity by involving employees to help eliminate nonessential steps. 

So, you want your company to become more productive. Perhaps you'll hire a specialized consultant, or invest in some new equipment. But increased productivity isn't necessarily achieved by _adding more._ In fact, it's often quite the opposite. 

The Gemba Kaizen strategy guides you to greater productivity through _elimination._ When we analyze working processes, we often find several elements that consume time and energy without contributing anything to the end product. 

These wasteful elements are called _muda._ By eliminating _muda,_ we can free up resources, which in turn can be put to more effective use. For instance, muda could be overproduction, or the waiting time created by inefficient transportation. Even the time you spend walking from A to B is muda!

In fact, the notion that walking is wasteful sparked radical improvements in a US hospital. Through clever design, the distances staff had to walk were reduced by several miles. Employees subsequently completed 100 percent of their tasks in half the time!

Employees don't just benefit from Gemba Kaizen efficiency — they're also responsible for creating and implementing strategies. Employees, not managers, are in charge of the day-to-day work that presents the most opportunities for improvement. 

That's why Gemba Kaizen regards employees as the real experts. With employees playing the central role in boosting your company's productivity, there's no need for an external consultant or costly innovations. Instead, Gemba Kaizen expects every employee and every manager in a company to stay on the lookout for possible ways to improve working processes.

> _"Although improvements under kaizen are small and incremental, the...process brings about dramatic results over time."_

### 3. Gemba Kaizen can be applied to a variety of different businesses. 

Most people who hear "Gemba Kaizen" or "lean business" think about conveyor belts, car manufacturing and the like. But that's only part of the story. Let's look at the beginnings of Gemba Kaizen, when it emerged as the only strategy that could save Toyota. 

Japan is a country with few natural resources, and after World War II, much of its industrial infrastructure was destroyed. Companies couldn't afford to waste any assets or resources. Japanese employees were hired for life, so their continuous improvement became a vital factor for economic success, too.

Thriftiness was vital for the economic success of any company, Toyota included. So, Toyota's owner Toyoda Kiichiro and industrial engineer Taiichi Ohno devised the now-famous Toyota Production System according to Kaizen principles. 

Even today, Gemba Kaizen is mostly used in industrial contexts. However, its basic idea — to improve productivity by eliminating unnecessary steps — is applicable to a wide range of disciplines and fields. Today, Gemba Kaizen is frequently employed by insurance brokers and hospitals.

One method called _clinical pathways_ is tightly connected to Gemba Kaizen. Clinical pathways is a plan used by hospitals to improve outcomes and reduce costs. It details all the essential tasks involved in treating patients with specific diseases. 

Administrations employ Gemba Kaizen, too. Many Gemba Kaizen ideas are used in what is called "lean administration." Administrative acts, such as drawing up birth certificates, can easily be standardized and improved with the aid of a Kaizen approach. A few Romanian cities even used the Kaizen philosophy to keep their downtown areas safe and clean. 

It's no wonder many companies in different sectors use Gemba Kaizen nowadays. The idea of improving and reducing waste can be realized everywhere, from our workplaces to our personal lives. So how can you begin practicing Gemba Kaizen too? Find out in the following blinks!

> _"Any motion of a person's body not directly related to adding value is unproductive."_

### 4. Flexible standards help companies thrive. 

Though many corporations strive for efficiency, few are so outstanding in their organization as nature's humble ant colony. So what's the secret behind these incredibly effective insects? Ants achieve order through a set of _simple standard processes,_ and we too can benefit from standards. 

Standards represent the best known way to do things at a given time. With firm standards, everyone knows what to do and what's expected of them. This eliminates mistakes caused by the idiosyncrasies of individual workers in one clean sweep. Think about it: when you're new at a job, the first thing you look to are the workplace standards, so you can find your footing. 

Standards work best when they change and improve as your company grows. Whenever a worker in your company finds a viable solution for some problem, you can adapt your standards accordingly — all while making sure that the solution doesn't get lost in the shuffle and that everyone else profits from it, too.

Imagine that workers on an assembly line have to constantly turn around to get tools from a box behind them. One of them puts the box in front her. Since she now needs no time for turning around and grabbing tools, she can produce more, and faster. It's a good solution, so why not make it a standard procedure?

Turning solutions into standards also empowers employees. Knowing that their best ideas can be implemented to help the broader workplace will give them a stronger sense of commitment to their job. And, of course, standards make a powerful impact on efficiency, even when the same equipment is used in a different way. 

In 1961, two electronics manufacturers, one European and one Japanese, embarked on a joint venture. While both used exactly the same machines, the Japanese workers reached a higher productivity rate of 99.2 percent — solely through the use of standards — while the Europeans only achieved 98 percent productivity. This is just one example of the power of standards! But they're not the only tool that's central to Kaizen; self-discipline is vital, too.

> _"Maintaining standards is a way of ensuring quality at process and preventing the recurrence of errors."_

### 5. Self-discipline allows workplaces to create and sustain efficiency. 

Self-discipline plays a key role in Japanese culture, especially in the workplace. The workplace, _Gemba,_ is where value is generated. Kaizen philosophy influenced five powerful practices that use self-discipline to keep the Gemba streamlined. These are the five Ss.

The first of the five Ss of proper workplace organization is _Seiri_, which means to _sort out_ all unnecessary items from the Gemba. It's a popular Kaizen technique, in which a leader will walk through the Gemba and mark everything that's not essential with red tape. Next, the workers have the opportunity to object to any labelling and prove that a tagged item is necessary. After that, all items still tagged will be removed.

After Seiri comes _Seiton_, which means to _straighten_ things out. All necessary items maintain an orderly place, so that everyone knows where to find them at all times. The third S is _Seiso_ : _sweep_ the work floor, keeping everything clean and tidy.

_Seiketsu_ is the fourth step. It's about _systematizing_ the first three steps, which means ensuring your workplace practices Seiri, Seiton and Seiso every day.

Going through these four steps finally builds _Shitsuke_, the last S. Shitsuke is about maintaining and _standardizing_ what you've achieved so far. It implies becoming self-disciplined and doing the other four Ss as a habit.

Some Japanese companies take the five Ss very seriously. On one occasion, a group of Japanese managers inspected a German factory that their company wanted to buy. But upon seeing workers smoking in the Gemba, they decided not to buy the factory, convinced that the workers had no self-discipline.

The five Ss are the backbone of creating a Kaizen philosophy on a daily basis. A well-organized workplace is the starting point for other important Kaizen tools like visual management and reducing waste. More on those tools in the next blinks!

> _"Management manages employees so that employees can control the process."_

### 6. Visualizing work processes allows for quicker troubleshooting and stronger employee motivation. 

These days you often hear people claiming to be visual thinkers. Well, the truth is that we all are! Pictures and charts are great aids for understanding abstract situations and concepts. And that's where _visual management_ comes in. This strategy employs a combination of simple visual tools to provide information on working processes. 

Visual management helps identify problems and makes it easier to maintain standards. When a manager makes her rounds, she doesn't have the time to play detective and look for hidden problems. Instead, visual tools will provide her with all the essential information.

You can use symbols to indicate where a worker has to stand. If the manager walks through the Gemba and sees that the worker isn't at his place, she'll know that there's something wrong.

Visualizing also highlights the potential for improvement. If, using a whiteboard, you map out the day's goals, the tasks already accomplished and the work in progress, you can tell with a single glimpse if there's still something left to improve. 

Imagine you have two production lines. One manufactures front tires, the other rear tires. If every completed pair is noted on the whiteboard, it becomes clear when one line is operating much faster than the other. To better synchronize both lines, simply exchange fast workers with slow workers and neither line needs to wait for the other.

Visual management can also motivate employees by _visualizing aims_. It's very rewarding for us to see our progress visualized — think of that great feeling you get from looking at a to-do list with every item checked off! To motivate your employees, why not plot each day's progress on a chart? It's a simple trick that yields a great positive impact.

Visual management is an invaluable tool in an increasingly complex work environment. It's cheap, commonsensical and allows us to save our time for the real work that needs doing.

With the principles and practices of Gemba Kaizen in place, it's time to consider the role of a company's upper management.

### 7. A Kaizen approach is not complete without a CEO who practices what he preaches. 

Gemba Kaizen isn't just a strategy: it's a mind-set. It must be practiced by the head of the company, and practiced in a way that provides a leading example for employees. Think of it this way: a hand couldn't possibly carry out a motion without the necessary nerve impulse from the brain.

A CEO must make it totally clear that Kaizen will be implemented and that everybody has to come on board; otherwise, the goals of productivity and efficiency just won't be fulfilled. But people don't always respond well to change. Sometimes employees need a little convincing, and the CEO is the best person for that task. 

Take lean manufacturing legend Art Byrne as an example. When he enters a company as its new CEO, he personally trains the management in Kaizen, so everybody knows he's behind it and that it has to be done. And he's strict: reluctant managers get fired.

A CEO also has to immerse himself in the Gemba, or workplace. For example, one CEO of a well-known company cleans the office toilets every morning, setting a model for self-discipline. That's definitely one way to show that you walk the talk as a leader!

CEOs should also remember that a focus on the future is more beneficial than dwelling on the mistakes of the past. A significant portion of a CEO's time should go toward building an environment of _constant improvement._

For example, one goal might be to reduce the amount of time spent on a specific task by five percent. Once that's achieved, another goal has to be set to reach a new level of efficiency. 

This redefining of goals helps to establish a process of constant improvement. Of course, it's important for the CEO to be a "lean zealot," but that's not all. Read on to find out about another vital aspect of Kaizen leadership.

> _"The most crucial element in the kaizen process is the commitment and involvement of top management."_

### 8. Good managers work in the midst of the Gemba. 

To understand problems and maintain standards, the manager needs to be in touch with the Gemba. Since standards are the backbone of the working process, they must be maintained constantly. 

Picture a pizza manufacturer where the mozzarella keeps spoiling over the course of each afternoon. The solution is for all workers to wash their hands more frequently — this consequently becomes a standard. 

New, improved standards can also arise from problem solving. But if the standards aren't maintained, such as if the manager fails to check whether workers do indeed wash their hands, these solutions will quickly be forgotten.

Constant improvement secures the future of the company. Every manager has to do his or her part to keep improvements coming. The place for improvements and innovations is the Gemba, so this is where managers should be. Only in the Gemba can one see where elements of Muda exist, and where there is room for improvement.

Soichiro Honda, the founder of Honda, never had a real office because he was always walking through the Gemba. If the workers see that their managers are in the Gemba, demonstrate Gemba Kaizen and are self-disciplined, their own motivation will increase.

Both maintenance and improvement occur within the Gemba. Therefore, the manager needs to be there to do his or her job right. Of course, if the managers are active at all levels of the Gemba, they know their workers much better and both sides have more faith in each other. This is integral to the effective practice of Gemba Kaizen.

> _"Anyone in the company who is not actively trying to improve the way value is added is waste."_

### 9. Final summary 

The key message in this book:

**Gemba Kaizen is driven by constant improvement and self-disciplined employees, with managers engaged at all levels of the workplace. Through a handful of simple, common-sense techniques, you can use Kaizen philosophy within your company to streamline, revolutionize and thrive.**

Actionable advice:

**Ask five questions to get to the bottom of any problem.**

The next time you try to get to the bottom of a problem, use the five-why technique. One should always ask why at least five times to get to the bottom of the problem. For example: Why is there an increase in absenteeism? Why are so many workers hurt in the Gemba? Why is the floor slippery? Why does the machine leak? Why is the valve not tightly closed? Soon enough, the answer will be right under your nose!

**Suggested** **further** **reading:** ** _Leading Change_** **by John P. Kotter**

_Leading Change_ offers readers a guide to implementing successful transformation processes. It describes the various steps of change management, how to see them through, and the circumstances and obstacles to take into account along the way. In addition, the book paints a picture of the globally competitive company of the 21st century.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Masaaki Imai

Masaaki Imai is an accomplished writer of books about Kaizen. In 1986, he founded the Kaizen Institute Consulting Group and has worked as an author and consultant around the world.

