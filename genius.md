---
id: 58ce6b8a1714b70004eb6a8b
slug: genius-en
published_date: 2017-03-21T00:00:00.000+00:00
author: James Gleick
title: Genius
subtitle: The Life and Science of Richard Feynman
main_color: 2EB7E4
text_color: 1E7996
---

# Genius

_The Life and Science of Richard Feynman_

**James Gleick**

_Genius_ (2011) charts the life and career of brilliant physicist Richard Feynman, from his formative upbringing to his remarkable and lasting contributions to science. Though he's not as renowned as Albert Einstein, and has no groundbreaking theories to his name, Feynman did change the way scientists look at the world.

---
### 1. What’s in it for me? Meet Richard Feynman – one of the twentieth century’s greatest physicists. 

This is the story of a brilliant physicist, a man named Richard Feynman. Though most people have never heard of him, Feynman played a significant role in the development of twentieth-century physics. Indeed, his influence is palpable to this day.

Born on May 11th, 1918, Feynman would go on to have a major influence on physics in general, and quantum physics and superfluidity in particular. He was also a part of one of the twentieth century's most famous projects — a project that both pushed science forward and resulted in mass destruction. So let's take a look at the life and work of this obscure genius.

In these blinks, you'll find out

  * that Feynman's father groomed him for science from an early age;

  * what part of Feynman's method would help him win the Nobel Prize; and

  * why Feynman's lectures remained famous after his death.

### 2. As a child, Richard Feynman was encouraged to think scientifically. 

Before Richard Feynman was even born, his father, Melville, made a prediction: if the unborn child turned out to be a boy, he'd become a great scientist. And sure enough, the prophecy came true — but this had a lot to do with how Richard was raised.

Melville was a second-generation European immigrant who'd settled in upstate New York. Though he had scientific aspirations, he felt that, as a middle-class Jew, his options were limited. So instead of pursuing his own dreams, he worked as a salesman and pinned his hopes on his son.

As a result, Richard was raised to see the world with a scientific mind.

Before Richard could even talk, his father stimulated his son's developing mind with tiles that contained blue-and-white geometric patterns.

Later, Melville would take Richard to museums and translate the facts and numbers on display into images that allowed his son to visualize, and therefore retain, the details. So, when describing a _Tyrannosaurus rex_, he told Richard that the dinosaur was tall enough for its head to reach his bedroom window, but that the head would be too wide to fit through it.

Melville also made sure Richard understood how and why things work — and how important this understanding is.

One day, on a mountain hike, Melville asked Richard to identify each bird they encountered. Whenever Richard was stumped, Melville would recite the bird's name in Chinese, Portuguese and Italian.

This may seem like mere showboating, but there was a larger point. Depending on where you are, different people will have different names for the same bird, names that tell you nothing about the bird itself. Real knowledge comes from observation and an understanding of what the bird actually does.

Much later, Feynman shared this insight while working on a school advisory board for science textbooks. The textbooks were written in vague language, such as this: "friction causes shoe soles to wear away." Feynman pushed for more detailed explanations, such as "the grooves on a sidewalk grip chunks of shoe leather and tear them off." It infuriated him that children were being taught unscientific information that didn't include how things actually happen.

> "_I was born not knowing and have only had a little time to change that here and there."_

### 3. Feynman won math competitions using the same methods he would later apply to complex physics problems. 

Even though he could breeze through his algebra exams in high school, Feynman had significant problems figuring out baseball and girls.

He felt much more comfortable taking part in the math competitions held by the Algebra League.

Math classes generally place great importance on students showing the process by which they've arrived at a math problem's solution; math competitions, on the other hand, require the exact opposite. What's important is the solution, not how you got there.

And since these competitions are extremely fast-paced, using traditional methods to arrive at an answer is not the best path to victory. In other words, competitors have to use shortcuts. Feynman, who'd been raised to use visualization as a problem-solving tool, was great at this. Math competitions were the perfect place for him to shine.

While other students, pens flying, would try to blaze through a series of calculations, Feynman would only write out one thing: the answer.

As the question was being read out, Feynman would often be struck by an insight and, with a dramatic flourish, he'd write a number and circle it as his answer.

One relatively simple problem involved a hat falling from a rowboat going upstream. Competitors were asked how long it would take the rowers to retrieve the hat if its loss went unnoticed for 45 minutes. The velocity of the water and the boat were given, but these details were distractions that Feynman didn't need. He visualized himself as the hat and immediately realized that it would take the same amount of time to go back: 45 minutes.

This unique ability to visualize problems would serve Feynman well throughout his life and career.

Fellow physicists often remarked on how Feynman would solve problems by putting himself in the place of an atom or an electron, essentially asking himself what _he_ would do if he were an atomic or subatomic particle.

### 4. At college, Richard’s obsession with physics became all-encompassing. 

During his first year at the Massachusetts Institute of Technology (MIT), Feynman asked the chairman to explain what good his studying mathematics would do. Feynman was given the age old response: "If you have to ask, then it's the wrong subject for you."

Feynman was a math whiz, and was even allowed to teach a class during his senior year in high school. At college, however, he grew tired of math, as it became too abstract for him. As a result, Feynman turned his attention to physics as an undergrad.

During this time, Feynman's love for solving physics problems grew, as did his ability to internalize the formulas in order to intuitively arrive at a solution. He hungered for more problems to solve and would even stop people in the corridors and ask them to show him whatever problems they were working on.

This deepening knowledge of theoretical physics did affect his performance in other subjects, however.

He did worst in art history and English — but that certainly wasn't the end of it. Feynman despised music so much that hearing it caused him to feel something akin to physical pain. His dislike for music was only topped by his disdain for philosophy, which he regarded as "an industry built by incompetent logicians."

These reactions came from his upbringing. He believed that there was nothing more to mastering English or philosophy than learning words and names that humans had made up.

To get by, he cheated on exams by copying the work of nearby classmates. Nonetheless, his low scores almost cost him his career. When Feynman applied to Princeton for graduate studies, he was nearly rejected.

> Feynman was one of only three students to take the first ever course offered by MIT on the new field of nuclear physics.

### 5. In 1942, Feynman joined the Manhattan Project and led a team that helped build the atomic bomb. 

During the early years of World War II, many people were talking about the possibility of splitting the atom and thereby creating an atomic bomb. For the world's top physicists, there was no debate about whether or not this would be possible — it was just a matter of time.

Many of these scientists were working at a top-secret facility in Los Alamos, New Mexico, to develop the world's first nuclear weapon.

While finishing up his graduate studies at Princeton, in 1942, the twenty-five year-old Feynman was recruited into a nuclear research group known as the Manhattan Project — a team of scientists working on one question in particular: How much uranium is required to kick off a nuclear chain reaction?

Feynman was brought on board due to his reputation as a brilliant young physicist. But, soon enough, he was made the leader of his own team, a role that was generally reserved for veteran physicists.

Feynman gained their respect by pushing the team to find the most unorthodox solutions to the problems at hand. This process resulted in some complaints. To some, his theories and hypotheses seemed too outlandish — but, time and time again, the team's subsequent work would prove his initial theories to be correct. After a few such successes, the team placed their full faith in Feynman.

The team found it rather easy to come up with the calculations required to build a bomb; it was dealing with the physical materials that presented the real challenge. 

Feynman was no longer visualizing abstract scenarios. If the calculations for the melting point of a certain metal were even slightly off, it could spell death and nuclear disaster.

These concerns resulted in Feynman's team producing important work on how to prevent bombs from detonating prematurely as well as calculating the precise critical mass of uranium for the nuclear chain reaction to start.

And, as we all know, all this careful work bore fruit. Just before sunrise on July 16, 1945, the sky over the New Mexico desert was set ablaze by the first detonation of an atomic bomb.

### 6. Feynman’s method of visualizing problems helped win him the Nobel Prize. 

Some people say it takes between ten thousand to twenty thousand hours of practice to master a musical instrument. After all that time and work, the musician has an intuitive relationship with her instrument and improvising a melody becomes second nature — practically effortless.

Feynman's genius worked in a similar way: the endless hours he spent visualizing problems left him with a superb knowledge of algebra combined with an intuitive feeling for physical forces.

Feynman's understanding of mathematics became so thorough that it allowed him to work quite freely within the field of theoretical science. He could effortlessly translate a physical interaction into a formula, or vice versa, by visualizing an endless array of objects interacting across time and space.

Feynman was once asked if color played a role in his scientific process, and he said he would often see colors such as dark X's or violet N's when composing a formula. (He admitted that he had no idea whether his students experienced similar visuals.)

In 1947, Feynman made things a lot easier for students of quantum physics when he introduced his _Feynman diagrams_.

At this time, Feynman published the results of work he'd been doing on electromagnetic fields and how they interact with charged particles.

It became immediately apparent that the diagrams he included in these findings introduced an ingeniously simplified way to understand complex equations. This was especially the case for quantum physics, where his diagrams obviated the need for every academic article to include a series of painfully complex calculations.

Nevertheless, Feynman had a hard time explaining his diagrams when he was awarded the Nobel Prize for physics in 1965.

It was the middle of the night in New York when the recipients were announced. But, by dawn, reporters were at his door, asking him to explain his contribution to science. He did his best, but it still went over everyone's head.

And when one reporter asked him to sum it up in one minute, he gave an appropriately gruff reply: "Listen buddy, if I could tell you in a minute what I did, it wouldn't be worth the Nobel Prize."

### 7. The few classes that Feynman taught became legendary among students and professors. 

Even though Feynman is recognized as a brilliant teacher, he did an impressive job of shirking his pedagogic duties. But those who did manage to attend one of the few classes he ever taught paid witness to something special.

In his final university position, Feynman spent two years teaching the introductory course to physics at Caltech, providing an experience unlike any other.

Feynman took his students on a whirlwind tour through the world of physics, reimagining the entire field from his own unique perspective.

Naturally, his course wasn't about learning the names of things. Instead, he ensured that students understood concepts by encouraging them to visualize forces at work.

As the course went on, freshman and sophomore students struggled to keep up, and some dropped out. But it's unclear whether Feynman was concerned about losing his target audience since professors and graduate students were more than eager to fill any empty seats.

Since his classes were so fascinating, and since Feynman never wrote any books on his work, many people recognized the importance of transcribing his lectures. These notes were eventually published in a series of little red books entitled _Feynman Lectures on Physics_. People took to calling them "the red books."

Many universities tried to include the red books in their curricula, but, by and large, they were too complex for beginning students, and so were dropped. Nevertheless, many of the professors who read them cited the books as being instrumental in reshaping their views on physics.

This is largely due to Feynman's insistence on teaching pragmatic knowledge about how to solve problems.

The emphasis he placed on the process of problem-solving — as opposed to teaching the same route to the right answer — is a big part of Feynman's legacy and his contribution to quantum mechanics.

And we still use the tools he left behind to measure things such as the light that is emitted by an atom. He also gave us many of the analytical methods used by physicists today, methods that shape how judgments about experimental data are made.

### 8. Feynman cultivated a reputation as a joker, but his legacy is that of a truly original thinker. 

There are a number of reasons Feynman is remembered. Strangely enough, one of these is his bongo playing.

When you think of geniuses and their musical instruments, you probably don't think of the bongos. Nonetheless, while he was on sabbatical in Brazil, Feynman decided to pick up a pair of bongo drums. Though he hated most modern music, Feynman liked the drums because they provided him with a way to improvise and create original music. And he even got good enough to play with a local band.

Feynman also had a reputation as a joker. He liked to tell fantastical stories, many of which are collected in the books _Surely You're Joking, Mr. Feynman!_ and _What Do You Care What Other People Think?_ Much to the surprise of his publisher, both books became best sellers.

Like all good storytellers, Feynman stretched the truth here and there, but what his colleagues were more concerned with was the way in which the books played down the serious nature of scientific inquiry. It's undeniable, however, that the books really do reflect a significant part of his character.

Another unique part of Feynman's character was his willingness to ignore the work of others in order to think originally. He had serious worries about being influenced by other people's ideas; he feared they would interfere with his ability to innovate.

Therefore, he did his best not to read scientific papers, especially their first page, which contains the results. The only thing Feynman wanted to know was the first parts of a problem, so that he could solve it on his own.

This behavior didn't sit well with some of his peers. This wish to solve problems alone was seen by some as irresponsible. Many of Feynman's own brilliant ideas were never published since he didn't think they were groundbreaking enough.

Other people found this habit of his discouraging after learning that he could solve a problem overnight, when it took others almost half a lifetime.

But taking the road (much) less traveled is exactly what Feynman did and it's what he taught his students to do as well. Such independence of thought may not be a sure road to success — but it's essential for true originality and, thus, true innovation.

### 9. Final summary 

The key message in this book:

**Every scientist who met Richard Feynman regarded him as one of the sharpest minds in the field. But, despite his brilliance and his many contributions to physics, he isn't a household name. He wasn't a man with a single major theory; rather, he offered physicists a different way to look at the world and various methods for finding solutions through practical approaches. This contribution, though seemingly humble, has helped make physics what it is today.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Information_** **by James Gleick**

_The Information_ takes us on a journey from primordial soup to the internet to reveal how information has changed the face of human history and shaped the way we think and live today. New technology continues to accelerate the speed at which information is transmitted, and to have lasting consequences for society.
---

### James Gleick

James Gleick is a historian and bestselling author whose work often delves into society's complicated relationship with technology. His other acclaimed books include _Isaac Newton_ and _The Information: A History, a Theory, a Flood_.

