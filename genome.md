---
id: 55d1dc47688f670009000118
slug: genome-en
published_date: 2015-08-20T00:00:00.000+00:00
author: Matt Ridley
title: Genome
subtitle: The Autobiography of a Species in 23 Chapters
main_color: 6BBF43
text_color: 407328
---

# Genome

_The Autobiography of a Species in 23 Chapters_

**Matt Ridley**

_Genome_ (2006, second edition) takes you on an exciting journey into your own body, exploring the genetic building blocks that make up not only who you are but also all life on earth. You'll examine the basics of genetics and discover what genes influence, from aging to illness to even your own personality. Importantly, you'll better understand why the future of healthcare and wellness may be found in the human genome.

---
### 1. What’s in it for me? Discover the wonders of the human genome and what it says about our species. 

The birth in 2003 of Dolly the sheep, the first genetically cloned animal, sparked a worldwide debate over the definition of biological life and whether we could allow ourselves to "play god." 

In essence, should we manipulate Mother Nature? Or would there be dire consequences in doing so?

In recent decades, the study of genetics has shed light on the building blocks of all life. To be sure, the story that genes tell us is a fascinating one. We're much more similar to other animals than we previously thought; and human evolution is still happening. 

And now that we've assembled the human genome in its entirety, the whys and hows of human life are becoming ever clearer. These blinks will take you through the history of the gene and show you what's in store for our species according to our DNA. 

In these blinks, you'll learn

  * how each of your cells are actually mini-libraries;

  * why bacteria are natural adapters, and we're not; and

  * why cancer can never die — it just multiplies.

### 2. The human genome is the most important “book” in the history of our world. 

When British Prime Minister Tony Blair and American President Bill Clinton announced on June 26, 2000, that the human genome had been roughly mapped out, it was a major turning point in human history. 

For the first time, we had access to the "book" that described the "story" of human life!

To understand the gravity of this scientific feat, we first need to grasp the basics of genetics.

The _human genome_, or the full genetic map of a normal human being, is made up of 23 _chromosome_ pairs. A chromosome is shaped like the letter X, and genes stack up on each of the letter's "arms." Each chromosome pair differs in size and in the number of genes found on each. 

A gene itself lives on a long strand of DNA (that is acid), which itself is composed of a series of molecule pairs, called nucleotides: adenine, cytosine, guanine and thymine. These molecules are often referred to simply by their first letters, A, C, G and T. These pairs are "written out" in the genome, which can be "read" in order to replicate itself. 

Think of it this way: the genes on our chromosomes are like short stories, making the human genome the biggest and most informative "book" on the planet!

With over a billion "words," the human genome too may be one of the longest, most complicated books in the world. In it, every chapter of life ever written can be discovered. 

And what's more, the tens of thousands of genes that compose the human genome — all this information — fit into a tiny nucleus of a tiny cell that could easily sit on the tip of a pin.

In our genome, you can discover the progression of the story of life's evolution from bacteria, fish and apes to finally human beings. We can learn about how illnesses such as Huntington's disease came about, or why people love the feeling of cool ocean water.

The following blinks will open up this book of human life and examine a few chapters that have defined the essence of every single person on the planet.

### 3. While humans are unique, we’re not the end point of evolution. There’s more to come. 

We might think that as humans, we're the end point of natural selection — that is, evolution has stopped with _Homo sapiens_. Other similar species, such as chimpanzees, are endangered, yet humans continue to thrive. 

But while humans are indeed unique, are we really all that special?

From a genetic perspective, no. In fact, we share most of our genes with other animal species.

For example, only two percent of our genetic makeup differs from our closest genetic relative, the chimpanzee. The fact that we're less hairy and bigger-brained is due to just a tiny bit of our genome's "story." 

The most striking difference in our genome, however, is that we have one fewer pair of chromosomes than chimpanzees do. Instead, two chromosomes fused together to become one. 

So why are our genes so similar to those of chimpanzees? It's likely that several million years ago, there was one group of apes that evolved along two separate paths. One group developed into the chimpanzees we know today. The other, after several generations and genetic dead ends, became us, _Homo sapiens_.

Adaptation is another interesting theme. Genetic development doesn't just happen randomly; rather, genes evolve over generations, allowing each generation to better adapt to new conditions than the previous.

Curiously, bacteria are more genetically adapted than humans are. As humans have a relatively long life span, we require more time (or more generations) to genetically adapt to a changing environment.

Thousands of years ago, humans were much smaller. But with improvements in food and living conditions, natural selection has favored humans who could make the best use of these new conditions. As a result, humans have grown taller on average. 

In contrast, as bacteria have shorter life spans than humans, their genetic development happens much faster. In fact, the number of human generations over the past five million years can be replicated by bacteria in less than only 25 years! As a result, bacteria can adapt very quickly to a changing environment.

### 4. Understanding how the genome works has helped discover the basis of fatal genetic illnesses. 

Genes don't just make you look the way you do, they also determine how long you'll live.

For example, genes can cause the mutations responsible for Huntington's disease, an incurable illness that affects the motion center of the brain and slowly causes a person to lose control of his motor functions. 

Sadly, Huntington's disease is always fatal, and can be inherited by the children of those affected. 

Yet everyone carries the gene that causes Huntington's disease. The only difference is that some people have more repetitions, that is, exact copies of this particular gene repeated over and over, than others do. In fact, if you have more than 39 copies, you will contract Huntington's disease.

To put this in perspective, however, imagine if we enlarged the human genome and stretched it out, so that your genes, end-to-end, wrapped around the earth's equator. In this hypothetical 24,000 miles of genetic code, the deadly collection of genes that trigger Huntington's disease would be less than one-inch long. 

Thus the power of genetic material: this inch is the deciding factor between perfect health and a slow, painful death.

The genetic "word" that describes Huntington's disease — the series of nucleotides that instructs the body to act — is CAG (cytosine, adenine, guanine). Thus if this "word" repeats itself enough times in your genome, you'll most likely contract this fatal disease. 

Interestingly, so far researchers are aware of six diseases caused by this particular CAG repetition.

Repeat mutations are especially problematic, as the more times a genome replicates, the easier it is for genes to slip up and repeat a sequence incorrectly, potentially causing a fatal mutation.

With Huntington's disease, only very few people choose to have the illness properly diagnosed. Because even if you know your fate, how will that help you escape the inescapable? ****

### 5. Our genome is full of junk, nonsense “paragraphs” that seem to mean nothing. 

While your genome is certainly filled with valuable information that makes you uniquely _you_, it's also loaded with a whole lot of junk.

Indeed, contrary to what scientists had previously thought, our genome "book" is full of useless and often illegible paragraphs.

When scientists started decoding the human genome, they thought everything they'd find would be important, each gene crucial to guiding the body's many functions. 

Instead, they found a whole lot of what they called _junk DNA_, useless strings of genes repeated over and over again, often resembling corrupted versions of useful genes. 

As it turns out, only about three percent of our genes are actually "useful."

But junk DNA isn't just "junk." It can actually be dangerous. Many of these junk genes were originally viruses that were embedded in the genome as the body worked to suppress them.

As we've already learned, repetitions of certain "words" in a genome can cause abnormalities. Sometimes these repetitions can cause tumors and potentially damage other parts of the genome, which could cause illness and even death.

But sometimes junk can be useful, too. One of the first practical applications of our knowledge of genes was _DNA fingerprinting_. This process identifies the repetitions within junk DNA that make each person genetically unique, and thus easily distinguishable.

Though family members may have similar DNA sequences, genes duplicate at different rates depending on the person, thus creating distinct genetic markers with which we can "fingerprint" an individual. 

This leaves the question: What other possible applications could "junk" DNA have?

### 6. Genes determine more about your personality than you might be comfortable with. 

Now you better understand the role your genome plays in the diseases you could contract or how long you'll live. Yet your personality exists outside this genetic "story," right?

Think again. As it turns out, you may have less control over your character than you know.

Your brain chemistry as well as your genes can dictate certain personality traits. Consider, for example, the gene that lives on human chromosome 11. This gene codes for the dopamine receptors in your brain. Dopamine is a neurotransmitter that in essence helps control your brain's reward and pleasure centers. 

The number of times this particular gene repeats itself determines just how effective your dopamine receptors will be. A shortage of dopamine, for example, causes you to feel indecisive; an extreme shortage can actually lead to Parkinson's disease. Excessive dopamine, on the other hand, could make you a schizophrenic. 

In fact, there might be _at least_ 500 different genes that determine certain innate personality traits. 

Yet before you panic about your pre-written genetic plot line, remember that the environment also plays a crucial role in how your personality develops. Research has shown that even shy children will open up if they're raised by parents who are outgoing. 

Though many of your personality traits are certainly influenced by your genome, we as a society should never neglect the importance of the environment's influence on personality.

So even if your brain's chemistry might increase your propensity for criminal behavior, your peers and your parents still play an important role in curbing your genetic predisposition.

However, accepting innate traits could actually help an individual better work on them. By accepting these traits, we might be less likely to try to rationalize them or feel distressed or even embarrassed by behaviors that we think are bizarre or unbecoming.

### 7. Understanding the process of natural selection can offer insights into the reason we age. 

Getting old is no cakewalk. Bones start creaking; wrinkles deepen; getting around becomes a chore. But why do we age? This unavoidable process is part of natural selection.

If children are to survive and reproduce, they need their parents to stick around long enough to protect them until they reach adulthood and can fend for themselves. 

The generational process of natural selection — of genetic trial and error — has so far ensured that people stay alive long enough to provide for posterity.

But once children leave the nest, there isn't much reason for parents to stick around. This is why people don't age significantly until about 55, when the process sets in for real. By this point, ideally, our children have reproduced, and the older generation becomes redundant.

This is one of the reasons why we don't contract many serious illnesses when we're young. The genome tries to keep harmful genes in check until after reproductive maturity, but then takes the brakes off once we begin to age. One particular result of this process is cancer. 

Each time a chromosome replicates, a tiny bit at the end of the sequence gets cut off. These ends, called _telomeres_, are composed of repetitive genetic sequences designed to protect a chromosome. 

As we grow older, our chromosomes have replicated so many times that the telomeres become depleted, increasing the risk for errors in the replication process. It is this depletion that essentially causes aging. 

Many species have a gene that produces _telomerase_, which protects telomeres by adding a small genetic sequence to the end of a chromosome at every replication. Unfortunately, normal human cells don't have this gene; but cancer cells do.

This makes cancer cells immortal, since they've found a way to duplicate over and over again, without ever falling victim to the aging process!

### 8. Gene therapy, genetically modified humans. We are on the cusp of a new era in genetics. 

In previous blinks, you've learned how genes are the cause of deadly diseases such as cancer and Huntington's disease. But genes don't just trigger disease; they could also help us cure disease. 

Genetic modification may hold the key to curing the most aggressive illnesses. Indeed, in recent decades, we've witnessed rapid developments in the field of _gene therapy_.

In essence, gene therapy takes the genes the body needs to battle a particular disease and then introduces these genes into diseased cells, effectively stopping the disease.

Most gene therapy research is focused on curing cancer, as each minute of every day, someone in the United States dies of it. Scientists and the medical community are committed to ending this cycle. 

Yet while there have been many successful gene therapy experiments on humans, there is a downside to this pursuit. 

In part, the downside is related to the ethical and moral debate surrounding genetic modification. When scientists came up with the first genetically modified plants, many people were shocked and equated the technology with the "Frankenstein" monster — that is, the unnatural.

And yet, at the time of this book's publication, it was estimated that by the year 2000, some 50 to 60 percent of all crops in the United States would be genetically modified.

Scientists have explored the genetic engineering of animals to increase production yields, such as cows that produce more milk, chickens that lay more eggs and pigs that grow very large to provide more meat.

The debate too has become political, as society in general wonders whether genetic modification could eventually be applied to humans.

Yet genetically engineered humans are still years away, after all, we're still working to parse out our genome.However, we shouldn't dismiss the idea out of hand. Instead, we should focus on discussing what the genetic modification of humans might mean for humanity.

One day we _will_ be able to cure genetic diseases. But the question remains: should we?

### 9. The horrific history of eugenics in the twentieth century has cast a shadow over gene therapy. 

The term _eugenics_ was coined in 1885 by Francis Galton — a first cousin of Charles Darwin — and describes the breeding of "better" humans, fulfilling certain criteria to achieve genetic "purity."

You may be familiar with the term from the horrific policies of Nazi Germany during World War II. Yet the practice of eugenics wasn't confined to the Nazis, nor did it originate with them. 

In 1904, US scientists, with public support, began a campaign to identify "bad stock" in American society. This included people considered "feeble-minded," as well as alcoholics and drug addicts. Such individuals were seen as second-rate, and according to the thinking of eugenicists, shouldn't taint the nation's gene pool by breeding.

In 1911, US officials allowed the forced sterilization of those individuals considered mentally unfit to foster the nation's posterity.

Eugenics was in fact a publicly approved practice for decades in America and abroad. It found many supporters, not only within the scientific community but also with prominent authors like H.G. Wells and politicians like US President Theodore Roosevelt and Britain's Winston Churchill, who during his early years in Parliament, proclaimed that the "multiplication of the feeble-minded" was a danger for any nation. 

Churchill even tried to pass eugenics laws in Britain in 1910 and 1911, but failed.

The shadow of eugenics prevails even today. When embryos are tested for Down Syndrome and parents are presented with a choice to end the pregnancy, one could see this as practicing eugenics. Yet most prefer to think instead of this situation as protecting both the child and parents from an unnecessarily difficult life.

Yet importantly, the difference between this and true eugenics is a question of choice. Parents have a choice to continue the pregnancy or not; no one is forcing them to end it. A eugenicist would say the child is not fit for the world and would not allow the parents any say .

> _"America, bastion of individual liberty, sterilized more than 100,000 people for feeble-mindedness."_

### 10. Do you have free will? What’s more plausible: the tyranny of society, or the rule of genes? 

Are you free to think and do as you please? Can you develop your personality according to your desires, or do genes dictate the way you think and act? 

In other words: do you really have free will?

You might not like the idea that your entire life may be dictated by your genetic makeup. As individuals, it's hard to fathom being a "victim" of biological determinism — that is, the belief that our genes determine our thoughts and actions.

We're more likely to be comfortable with the idea of social determinism, which holds that the causes for events like divorce, child abuse or alcoholism are found in our past and present environments. 

Social determinism assumes that the children of divorcees, for example, are more likely to divorce their own spouses, having been influenced by this pivotal moment in their development.

But if we accept that society shapes our character — which it certainly does, in part — isn't our sense of free will just as illusory as it is under biological determinism? 

And yet, instead of questioning the influence of society, we readily embrace that society is the cause for everything we have ever done, and in doing so, neglect the influence of genes.

Accepting that genetics plays as much of a role in our behavior as society does can make it easier to accept ourselves for who we are. Even if biological determinism is real, that doesn't in any way imply inevitability. You can still choose what to do, even if your genes tell you otherwise. 

But by choosing to accept social determinism, we choose to surrender our free will for the freedom shirk responsibility of our actions.

It is strange that so many people are willing to accept and embrace social tyranny — the immense influence that society has on our thoughts and actions — and yet fight so forcefully against the idea that human behavior is likewise influenced by our genes.

### 11. Final summary 

The key message in this book:

**Mapping the human genome may be the single most important event in the history of modern science. With a complete roadmap of the human genome, scientists may hold the key to a new age of health and wellness, characterized by a disease-free world.**

**Suggested** **further** **reading:** ** _Your Inner Fish_** **by Neil Shubin**

Drawing on findings from paleontology, genetics and developmental biology, _Your_ _Inner_ _Fish_ describes the evolutionary history of the human body, tracing it back to the fish. The author shows how studying fossils, genes and embryonic development can help us understand our complex evolutionary past.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Matt Ridley

Matt Ridley is a member of the English House of Lords and an award-winning author of several books, including _The Red Queen_, _The Rational Optimist_ and _The Agile Gene_.

