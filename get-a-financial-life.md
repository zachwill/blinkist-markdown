---
id: 57b2dd75381bcc00031e060d
slug: get-a-financial-life-en
published_date: 2016-08-18T00:00:00.000+00:00
author: Beth Kobliner
title: Get a Financial Life
subtitle: Personal Finance in Your Twenties and Thirties
main_color: DF2D41
text_color: C42839
---

# Get a Financial Life

_Personal Finance in Your Twenties and Thirties_

**Beth Kobliner**

_Get a Financial Life_ (1996) is a beginner's guide to managing your money. These blinks provide essential financial advice on everything from managing debt to growing your savings, picking investments and choosing an insurance provider.

---
### 1. What’s in it for me? Handle your finances like a grown-up. 

Just how grown-up do you want to be? If you're in your twenties or thirties today, you have the unique opportunity to choose for yourself. No one is going to force you to settle down and become all serious.

But there are a few things you need to deal with as an adult, and one of them is handling your personal finances. Maybe you just graduated and need to pay back your student loan? Or perhaps you've realized that your life could become more convenient if you had an online bank account? Or you've started wondering how to fulfill your dreams, like buying your first car or financing a cruise in the Caribbean?

No matter what your financial situation might be, these blinks will guide you, and you'll discover that taking charge of your money is much easier than you might think.

You'll also find out

  * how to save thousands of dollars in insurance costs;

  * why paying back credit card debt is a spectacular investment; and

  * how one single sticker can tell you whether your money is safe with your bank.

### 2. Take control of your financial life by costing your goals and reprioritizing your expenses. 

You don't have to be a Wall Street trader to know that the global economy has been a bit dicey lately, but that doesn't mean your finances can't be in order. In fact, it's never too soon to start planning your financial life, and it's really not as hard as you might think.

Here's how you can get started.

First, determine how much money you'll need to fulfill your dreams. Maybe your goal is to shed all your credit card debt or buy a new car, but whatever it is, if you're going to realize your dream, you'll need to know how much it's going to cost.

For instance, say you want to buy a new $30,000 car. Sellers tend to require a down payment of about 10–20 percent, meaning you'll need between $3,000 and $6,000 in the bank to bring your new baby home.

Once you've figured out the dollar value of your dream, you need to start saving toward it. But to save money you first need to know how you _spend_ money.

Start by looking at your normal expenses. This is best done by keeping a diary for a month and writing down the details of all your expenses.

Eventually, your spending habits will be clear and you'll know what you need to change to free up some money to save. For example, maybe you buy a lot more books than you actually read or are overpaying for cable.

Once you've worked out your spending, you'll be amazed at how much potential for saving there is, even if you currently feel like you're barely making ends meet.

Now that you know how to start saving, it's time to explore a financial topic that many people find more intimidating: debt.

> _"Can you get by on $50 a week for eating out and entertainment? Is it worth it to you to forgo the latest cell phone or boots for a long-term goal? Once you start, it becomes easier than you think."_

### 3. Pay off your credit card debt straight away and, if necessary, negotiate a lower rate with your lenders. 

Are you in debt? If you are, you might be one of those people that just pays the minimum amount necessary every month and tries to avoid thinking about it the rest of the time. If that's the case, you might want to rethink your strategy.

In fact, all credit card debt should be paid off as soon as possible, because it costs a fortune!

For instance, say, like many people, you have $3,500 of credit card debt at an interest rate of 17 percent. Since you don't have much money, you've been skating by, paying the minimum amount required.

The trouble is that at this rate you won't pay off your debt until age 65! And by then, you will have paid a whopping $11,162 in total — $7,662 of which is pure interest.

So, credit card debt is expensive and you should get rid of it as soon as you can.

But if you're in more serious debt and struggling to pay your bills, you should speak with your lenders.

While it might be scary, a conversation with the bank is exactly what's called for. After all, they have an interest in making it possible for you to pay back your loan, and they'll likely be more flexible than you think. They may agree to reduced monthly payments or a lower interest rate.

If you're not up to doing this on your own, you can even hire a credit counselor, preferably a non-profit one. Both the Association of Independent Consumer Credit Counseling Agencies and the National Foundation for Consumer Credit will help borrowers negotiate with lenders. You might also find help from unions in your area.

But regardless of how you do it, if there's any way you can pay off your debt, you should make it a top priority. Next, we'll explore how to go about doing just that.

### 4. Use your savings to pay down your debt, refinance your loans and pay your bills on time. 

Besides talking to your lenders, there are also three other principles you should keep in mind to effectively manage your debt.

First, if you have any savings at all, the best way to invest them is by paying down your debt. This should be a no-brainer since you already know how expensive credit card debt is, and the same goes for car loans.

If you compare the returns you can expect from investing in stocks to the interest rate of your debt, the better investment is obvious. If your car loan comes with an interest rate of 17 percent, you'd have to find a stock that would promise a return of at least that much to make it a better investment than paying off the car loan — and good luck doing that. In other words, your debt likely costs you more than most investments can produce.

Another strategy is to transfer loans with high interest rates to loans with lower rates. Such a transfer is typically known as _refinancing_ and, for obvious reasons, it's often a good idea. By refinancing you'll get a lower interest rate — say 8 percent instead of 18 percent — and will, therefore, reduce the cost of your loan.

So, if you've got a car loan or some credit card debt that you can't pay off right away, just apply for a _low-rate credit card_. This will allow you to pay off your previous creditor with a new loan that demands a lower interest rate than the original.

And finally, always pay your debts on time, because stalling is never a good idea.

How come?

Well, in this day and age, it's not difficult for lenders, landlords or employers to peer into your credit history. And if you have late payments, they'll protect themselves by demanding higher interest rates or denying you opportunities, so it's better to be seen as a reliable debtor.

But debt isn't everything, and next up is an exploration of the world of banks and how to choose the right one.

> _"The number one factor that goes into your credit score is your record of paying bills on time. Read that sentence again and commit it to memory. It's key."_

### 5. Choose a bank that insures your money, offers online banking and has ATMs close by. 

Many people choose a bank just because it's local or because their parents use it. But these aren't exactly the wisest criteria.

Instead, you should first make sure that the bank insures your money. In the United States, most banks are covered by what's called _federal deposit insurance_, which protects your money — generally up to $100,000 — if the bank goes bust.

This is guaranteed by a governmental agency known as the _Federal Deposit Insurance Corporation_ or _FDIC_. So, when choosing a bank, be sure to look for the stickers that denote FDIC certification or just go to the FDIC website and check if your account is covered and what the limit of the coverage is.

You should also look for banks that offer online banking and have nearby ATMs. That's because these tools are essential for managing your finances. After all, online banking lets you easily manage your money whenever you want, but it also helps you keep tabs on all your account activity. It's important to check your transactions every week to determine how much you can spend, but also to spot any errors.

Say your paycheck of $500 was deposited into your empty account yesterday, but your insurance company accidently charged you $800 instead of $400, and now you're $300 in the red. If you don't bother to check your transactions, you might never discover this.

Another thing to keep tabs on when choosing a bank is the distribution of its ATMs. That's because you can withdraw money from your bank's ATMs for free. So, if you choose a bank that has lots of ATMs, you can save money by avoiding fees.

But maybe you don't want to just plant your money in a bank account and would rather invest it to get a positive return. Well, investment advice is right around the corner.

### 6. To easily diversify your investments, go with a mutual fund. 

If you only follow one rule for your investments, it should be "don't put all your eggs in one basket"!

This simple guideline can go a long way, and it's easy to diversify your investments by putting your money in a _mutual fund_.

A mutual fund is basically a pool of money to which thousands of people contribute. That money is then invested in different _securities_, like stocks and bonds. Investing in such a fund is highly beneficial because even small investments will be thoroughly diversified.

This is important because investing all your money in just a couple of companies could mean losing everything if they fail. But with a mutual fund, the money is spread across many stocks, meaning that if one plummets, you still have all the others to prop you up.

So, mutual funds are a good choice, and you can buy shares in them in all kinds of places like brokerage firms, banks and _mutual fund companies_. But the last option is the best. That's because mutual fund companies are firms that specialize in selling such funds, and they usually offer some great perks.

If you invest in a mutual fund through a brokerage firm or bank, you'll often pay steep fees and find yourself held to high minimum investment requirements. With mutual fund companies, on the other hand, you'll pay low fees and won't have to invest very much if you don't want to.

Not just that, but if you stick with one mutual fund company, you'll get a monthly overview of all your investments, making it easy to move your money between different funds.

OK, now that you're squared away on savings, debt and investments, let's move on to the often tricky field of insurance.

> In the United States alone, you'll find more than 500 mutual fund companies, each offering dozens or even hundreds of funds.

### 7. When it comes to insurance, it pays to shop around. 

Most people pay way too much for insurance. To avoid being one of them, you need to do a bit of research on insurance providers.

That's because different insurers can charge very different premiums for identical policies. One study found that a young man could buy identical car insurance policies with premiums ranging from $808 to $3,441! So how can you best shop around for insurance?

For starters, specialist websites will compare the databases of insurance companies for you. They'll then provide you with a free list of cheap policies. Buying a policy suggested by one of these sites won't cost you any more than buying one from an insurance agent.

You might also choose to talk with a couple of agents. After all, since they tend to work on commission, they'll be eager to make a deal and can be flexible with premiums and options.

But finding a cheap policy isn't the only important consideration. It's also key to know how much coverage you need. Otherwise, you might end up paying for unnecessary insurance.

So, while you might think it's a good idea to buy lots of small, seemingly cheap insurance policies — just to be safe — this isn't necessarily a good idea. That's because you might end up paying a lot of money for coverage you already have.

For instance, say you're on vacation and decide to rent a car. You might spend $10 a day for collision insurance without realizing that you're already covered by your credit card or your own car insurance, which happens to extend to rentals.

This is why it's always worth mapping out your current coverage before buying new insurance.

> _". . . don't feel obliged to buy from an agent just because he or she did some research for you. That's the agent's job."_

### 8. Final summary 

The key message in this book:

**Seize control of your personal finances in your twenties and thirties. Save for your future goals by paying off all the debt you can, making diverse investments and choosing the right bank and insurance for your needs.**

Actionable advice:

**Do some ATM research!**

You probably don't pay to use your own bank's ATMs but might well be charged a fee for using those of other banks. It could cost you as much as $3 per transaction, and since banks aren't too keen on explicitly informing customers about these fees, it's essential to do the research yourself. Just imagine the hundreds of dollars you could save every year simply by favoring your own bank's ATMs!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The One-Page Financial Plan_** **by Carl Richards**

_The One-Page Financial Plan_ (2015) makes budgeting easy: once you know why money matters to you, it's just a matter of making sure you've got enough to do what you want with it, whether it's creating a stable income or saving for the future. This simple planning solution will give you all the tools, tips and tricks you need to realize your financial dreams.
---

### Beth Kobliner

Beth Kobliner is a journalist on a mission to teach young people about personal finance. She served on President Obama's Advisory Council on Financial Capability for Young Americans and co-starred in a _Sesame Street_ video with Elmo that explained money to little kids.

