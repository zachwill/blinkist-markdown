---
id: 58de68f12b329600041cc1a2
slug: get-momentum-en
published_date: 2017-04-04T00:00:00.000+00:00
author: Jason W. Womack & Jodi Womack
title: Get Momentum
subtitle: How to Start When You're Stuck
main_color: FBE23D
text_color: 7A6E1E
---

# Get Momentum

_How to Start When You're Stuck_

**Jason W. Womack & Jodi Womack**

_Get Momentum_ (2016) guides you through simple yet effective strategies for you to work sustainably toward your personal and professional goals. From managing time in the short and long term, to monitoring progress, modifying your approach and celebrating little victories along the way, these blinks outline the keys to gaining and maintaining motivation.

---
### 1. What’s in it for me? Get unstuck and get going. 

Do you have a dream project collecting dust on a shelf somewhere? Or do you feel that your life and your core ambitions just aren't moving forward the way you'd like them to? If so, don't feel bad — we all get stuck now and then.

Luckily, there are some useful and efficient strategies to help you get unstuck. As you'll see in these blinks, by taking a step back and examining your true motivations, learning the noble art of tracking your progress and finding good mentors, you'll be well on the way to regaining your initiative.

Let's get into it and give your life the momentum it deserves!

In these blinks, you'll learn

  * why you should always have some champagne on ice;

  * what Benjamin Franklin has to do with gaining momentum; and

  * how the 90/90 rule will help you reach your goals.

### 2. Decide what you want to be known for and start tracking your progress. 

Motivation is what makes great achievements possible. So what do you do if you have lofty goals but lack the motivation to see them through?

While most motivational writers tell you not to care about what others think, striving for a certain reputation can be a powerful motivator. This was the case for one of the authors, business leader Jodi, who would often meet up with other female executives in her industry. Unfortunately, these meetings were far from empowering, as the women tended to discourage, rather than support, one another.

Soon enough, Jodi realized that she wanted to be known not just as a successful businesswoman, but as a woman who empowered her female peers. This motivated Jodi to found No More Nylons, a social and professional network where women could connect, collaborate and encourage each other to thrive in a business world still dominated by men.

Discovering the kind of person she wanted to be gave Jodi the motivation she needed to kickstart a new project. But to stay determined and focused, she made use of another motivating tool: progress tracking.

Signs that we're getting closer to achieving our goals are what keep us from giving up. In Jodi's case, her goal was to help as many women as possible help each other. Each meeting, she tracked her progress by recording the number of women attending, which gave her an idea of how quickly her project was growing. Seeing in real, tangible numbers how more and more women were coming together through No More Nylons kept Jodi motivated.

Even so, she had to consciously take a moment to stop and observe her progress — otherwise, it would pass by unnoticed. Pausing to reflect upon and celebrate your milestones along the way is essential to keeping your morale high.

So, if your goal is to launch a new product, take the time to celebrate every step that brings you closer to achieving that goal, from coming up with a winning design, to finalizing your prototype, to finding your first loyal customers. Keep a bottle of champagne at the ready for these occasions — hard work is impossible without a little fun once in a while!

### 3. Seek inspiration and actionable advice from mentors and role models. 

To maintain the momentum you need to achieve your goals, it's vital to have someone around to give you a little push when you need it. This person is your _role model,_ or your _mentor_. They aren't there to nag you to keep going when you feel like giving up; instead, they'll give you momentum by inspiring you through their own work.

The author, Jason, was inspired by Benjamin Franklin and devoured books about his life and work. Franklin was not only a brilliant inventor and natural scientist, but a man who played a central role in the birth of the United States.

He didn't lead the great life he did through luck — Franklin was also a master of productivity and self-improvement. He created a set of rules to live by to ensure lifelong learning, and also developed his own pros and cons chart for better decision making. Jason learned a great deal from Franklin and still quotes him today.

While the space of a few centuries barred Jason from actually spending time with Benjamin Franklin, you might be lucky enough to find a potential role model or mentor in your own industry. If you can't think of anyone you know, it's time to start networking!

In your search for a mentor, conferences are a great place to start. If you connect with someone you think might be a good candidate, simply ask them whether they'd like to mentor you — you've got nothing to lose! You can also discover potential mentors at smaller meet-ups and other social events like Jodi's No More Nylons.

You might even find ways to reach out to influential or famous figures beyond your local network. In such cases, be prepared to have your dedication tested. You're also much more likely to get a response when you ask for advice on specific issues, rather than general words of wisdom.

> _"If you're the smartest person in the room, then you're in the wrong room."_

### 4. Divide big goals into manageable projects with regular milestones along the way. 

In the first blink, we touched on the importance of celebrating the goals you achieve on the way to success. To take full advantage of this technique's motivational power, you'll need to be smart about how you define your milestones.

Start by creating a schedule for your long-term project that comprises several _subprojects_, or significant and clearly designated milestones that can be achieved at roughly 30-day intervals. These, in turn, can be organized into 90-day work cycles, with three milestones to be achieved during this time.

Let's return to the product launch example we used in the first blink. Completing your first prototype could be a single 90-day work cycle, with the three key milestones of completing your final design, creating your first prototype and undertaking rounds of testing. After this, you'll be up for another three milestones in the following 90-day work cycle. This way, you'll always have something to work toward and a path to follow, making big projects seem a lot less overwhelming.

Following the 30/30 and 90/90 rules will also help you keep your momentum going.

If you've ever felt like there simply aren't enough hours in a day to get your projects off the ground, the 30/30 rule is for you. When working on one particular stage of a project, the upcoming phase might seem increasingly daunting as it approaches.

However, if you work 30 undistracted minutes a day for the 30 days before you get officially started on the next milestone, you'll already have put a total of 15 hours toward it by the time you get to work on it in earnest. This will ensure that even if you're juggling a day job and family responsibilities, you'll always find a way to push through to the next phase of your project.

For those who find themselves feeling lost in the middle of projects, the 90/90 rule is another helpful tool. On the first working day of each month, spend 90 minutes looking at the tasks that lie 90 days into the future, considering what needs to be done in the interim and making a bit of headway wherever possible. This will give you more breathing room down the line, in case you need to deal with unforeseen problems or day-to-day annoyances.

### 5. Monitor your progress to catch crises early. 

Even if we diligently plan our 90-day work cycles and follow the 90/90 rule, the odd slip-up is still possible. For instance, you might realize just a week away from a key deadline that you're entirely off track. What's the best way to avoid this unwelcome surprise?

You can monitor your progress toward your milestones by identifying small indicators that highlight how you're doing.

Say you're training to run a marathon. Each week of training can provide you with a whole host of different indicators to give you an idea of how on track you really are, from miles per run and running days per week to the number of days you slept well and ate healthily.

Consider how each of these indicators should look as you work toward each milestone in your project. Then, monitor these key indicators to identify a loss of momentum before it becomes a problem. You can even make things visual with posters, a whiteboard or sticky notes to help you track these indicators and map your progress.

However, keep in mind that you can't monitor everything. Excessive monitoring might initially feel quite satisfying, giving the impression that you've got a handle on all the variables influencing your productivity. But the natural variations that appear when keeping track of minute details might discourage you and distract you from far more important indicators.

Do your research and work out which indicators tell you the most about the health of your project; these should be monitored on a regular basis. By keeping up a good rhythm of monitoring your progress, you'll save yourself time and energy down the road by nipping approaching crises in the bud.

### 6. When you face setbacks, don’t give up – modify instead. 

The final tool in your motivational toolbox is the ability to _modify_ — in other words, being able to adjust your approach when you've figured out that something isn't working.

Often, when we run into problems with our projects, it's tempting to just give up on the whole thing. By resisting this temptation and learning to modify instead, you'll be able to find new routes to your goal that you'd have never seen before. Modification is not about changing your overall aims; it's about modifying the strategies you apply to get there.

Say you're attempting to run ten miles once a week as part of your training for a marathon next month. After noticing that you're only hitting 7.5 miles at your best, you opt for a marathon coming up in two months instead.

By _modifying_ your plan to give yourself a bit more time, rather than pushing your body past its limit or giving up on the marathon entirely, you're able to continue on a path to the same goal of running a marathon without damaging your momentum.

But beware of the temptation to modify too many things at once; when it comes to modification, less is more. The challenge of increasing your weekly miles is a good reason to give yourself a longer time to train, but this doesn't mean that you should start altering your sleeping habits or caloric intake. Start with one small change, and if you don't see an improvement after some time, take on another modification. The better your monitoring system is, the easier it'll be to spot aspects that need changing.

Above all, remember that modifications to your strategy and your timeline aren't signs of failure. You're simply maintaining momentum as you move toward what's most important — your goals.

### 7. Final summary 

The key message in this book:

**Finding the motivation to see our projects through to the end is challenging. By chasing a positive reputation, seeking inspiration, managing our time, monitoring our progress and, finally, modifying our strategies as needed, we can create and sustain the momentum we need to achieve our goals.**

Actionable advice:

**Keep up your momentum and help others do the same by being thankful.**

The next time someone you're working with does something well, point it out to them! Not only will this encourage them to work with you in a way that benefits you both, it'll also create an environment where celebrating each other's minor victories is the norm.

On top of this, boosting someone else's motivation can boost your own, as you witness the positive impact you can have on other people. Taken together, this will help you and your team gain and maintain momentum.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Your Best Just Got Better_** **by Jason W. Womack**

_Your Best Just Got Better_ (2012) outlines the most effective techniques for setting your goals and using your time to work toward them. These blinks reveal which parts of your life are draining your motivation, as well as what you can do to bring your ambition to the next level.
---

### Jason W. Womack & Jodi Womack

Jodi and Jason W. Womack are a husband-and-wife team with 20 years of business consultancy experience through their firm, The Jason Womack Company. Jason is a business leader and coach specializing in time management and motivation, as well as personal and professional development. Jodi is an activist for women in business and the founder of the women's business network No More Nylons.

© Jason W. Womack & Jodi Womack: Get Momentum copyright 2016, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

