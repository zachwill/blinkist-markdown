---
id: 5e5fabd46cee0700069aea8d
slug: get-out-of-your-head-en
published_date: 2020-03-07T00:00:00.000+00:00
author: Jennie Allen
title: Get Out of Your Head
subtitle: Stopping the Spiral of Toxic Thoughts
main_color: None
text_color: None
---

# Get Out of Your Head

_Stopping the Spiral of Toxic Thoughts_

**Jennie Allen**

_Get Out of Your Head_ (2020) is a guidebook for those seeking to break free of negative thoughts. Jennie Allen's remedy? Submitting ourselves and our minds to the embrace of Christ and God's love. By doing so, the author believes, we can all lead lives filled with thoughts that steer us down a positive path — and avoid the negativity that is all around us.

---
### 1. What’s in it for me? Break free from negative thought patterns through your faith in God. 

Everyone has negative thoughts, and not just sometimes — all the time. _Am I good enough? Am I wasting my life? Do I deserve happiness?_

But not everyone knows how to deal with these thoughts. In fact, sometimes, we let them consume us. And the big problem is that one negative thought swiftly leads to another, until we're caught up in a terrible spiral of negativity that just gets worse and worse.

It doesn't have to be that way, though. In the following blinks, you'll discover Jennie Allen's philosophy of climbing your way out of negativity by embracing your Christian faith. According to the author, building a closer relationship with God and Jesus can disrupt your toxic thought spirals and set you on a happier, godlier path.

In these blinks, you'll learn

  * when you need alone time with God, and when you need community;

  * how to be humble and grateful — even when we're not happy with God's plans; and

  * how to put other people before ourselves.

### 2. We all let negative thoughts spiral out of control – but we can choose not to. 

Jennie Allen might seem like she's got it all sorted. The author is a loving wife and mother with a successful career spreading the word of God. To those around her, Allen appears to be a model of success. But even she struggles with negative thoughts. In fact, there was a period of a year and a half when she actually started to question her faith.

Allen's period of doubt began with a visit back to her home town of Little Rock, Arkansas. She was giving a few talks at a Baptist church. In her first talk, she told the thousands-strong audience about demons. Demons were real, she told the crowd, and they were determined to steal your faith. But she wasn't prepared for the demonic encounter she had herself after the speech. She was approached by a mysterious and angry stranger who said the following sinister words: "We are coming for you." And then, just as she was about to go back on stage for her second speech, the building experienced a freak power cut.

This, the author was sure, was the work of the devil.

The incident sent Jennie into a dark pattern of negative thoughts that would spiral out of control every day — and every night — for a year and a half. She would wake up each night at the same time — 3 a.m. — filled with worries and fears. Was God even real, she wondered? Was she wasting her time? Every negative thought led to a new one, spiraling up to wild and terrifying conclusions.

The thing is, we all have thoughts like these — even if, thankfully, they don't always last for 18 months. But there's a way out. By thinking _about_ our thoughts, we can break out of these negative spiraling patterns. You just need to interrupt yourself with one simple alternative thought: _you have a choice_.

According to the author, so long as you believe in Jesus and submit to Him, the power to choose is within you. That means the power to choose a positive spiral of thoughts instead.

That's what Jennie realized — and that's how she broke out of her 18-month spiral of negativity.

If that all sounds too good to be true, don't worry — in the next few blinks, we'll dive deeper into the specific choices the author made that helped send her into these positive, godly thought spirals.

> _"How we think shapes how we live."_

### 3. Embrace the power of silence, and listen to your thoughts so that you can turn them into something positive. 

These days, it's easy to get distracted, whether your personal vice is scrolling through Instagram, binge-watching Netflix, or whatever else. We even distract ourselves with things that seem positive, like too much work, or too many friends. But the truth is that we all need a little bit of silence and stillness in our lives.

That's because, sometimes, we need something that other people just can't provide. A friend of the author's once confided in her about everything that was going wrong in her life. It was such a big and scary list, that Jennie knew there was only one thing that her friend truly needed — and it wasn't her. It was some alone time with God.

So that was her advice to her friend, and it's her advice to you, too: embrace silence, and confide in God.

The power of silence is scientifically proven. Prayer and meditation are known to physically alter your brain. You can literally rewire your brain through devoting time to quietly, calmly thinking about God.

So why is it so hard to do? Sometimes, it seems like we choose distractions because we're actively trying to avoid being alone with our thoughts. Perhaps we're afraid of realizing that we need to change — or perhaps we're afraid that we'll feel all alone.

It certainly can be scary to really listen to yourself. But listening to all your negative thoughts is the first step in breaking free of them.

When you do start to listen, you may well find that the thoughts that run through your brain sound like this: _I'm feeling overwhelmed, because I have too much work_. Or: _I'm feeling angry, because someone was rude to me_. But think about how those thoughts are structured. Does the "because" really matter?

Stop emphasizing the _reason_ you're feeling down, and instead, add a positive, proactive decision to the end of each thought. _I'm feeling overwhelmed and I have too much work… so I will choose to thank God for all of my opportunities. I'm feeling angry and someone was rude to me… so I will think about the kindness of God._

Negative thoughts are a chance to make a positive choice — a choice about God. So don't be afraid of silence: embrace it, as an opportunity to set yourself onto a better, godlier path.

### 4. Immerse yourself in your community, because we all need to be seen and loved. 

Alone time with God is crucial, but that doesn't mean _all_ the time. In fact, something that's just as important as alone time is the opposite: community. Even God is a community, after all: the community of Father, Son, and Holy Spirit.

The modern world isn't always much help with this. We're taught to prize independence as a goal in life, as if it's good to succeed on our own. But at heart, we all need to surround ourselves with other people. We need to be seen, and we need to be loved.

The apostle Paul, one of the author's idols, said this in the Bible: "Follow my example, as I follow the example of Christ." Look around you for people who live their lives like that — and befriend them. We all need strong friends who love Jesus.

Opening yourself up to other people can be scary. Perhaps you don't know many people in your neighborhood, or perhaps you're an introvert. Perhaps you've had bad experiences with friendships that have scared you off.

But the simple truth is that you have to push through. Don't let the enemy win: God wants you to make friends.

If the idea of asking someone new for a coffee makes you scared, that's actually all the more reason to do it. You need to practice asking for stuff until you no longer feel uncomfortable. The same goes for saying yes when people invite you somewhere. Be open to new experiences: they are new opportunities to harness the power of community.

And don't hold anything back. You need to trust in your friends so much that you can tell them truly everything. We all tend to hold back a tiny portion of our fears, even from our closest friends — that little final detail that makes us feel truly ashamed. One friend of Jennie's, for instance, confided in her about feeling attracted to a man who wasn't her husband. She even shared the detail that made her feel worst of all: she had started to message him.

But guess what — the moment she had told her full story out loud, her sinful thoughts just vanished. She stopped thinking about him altogether, and her marriage was saved. It really was that simple.

That's the power of community — we're all stronger together. Choose to let yourself be seen and loved, and break the power of your negative thoughts.

### 5. Surrender your fears to God, and choose to delight in His goodness instead. 

As we've just seen, simply acknowledging a negative thought — even just by saying it out loud — can break its power over you. That's why Paul, in the Bible, told us to "take every thought captive to obey Christ."

Taking your thoughts captive can be as simple as writing them down. Try it. Write down a negative thought that's bothering you, and really think about it. Ask yourself if it's really true — and what God thinks of it. You can do that by checking your Bible, and asking people in your religious community for advice. Finally, decide to make a choice about that troublesome thought. Are you going to continue letting it bother you, or are you going to break free?

Taking your thoughts captive means beating them in battle and surrendering them to God. You need to trust that God can make those thoughts disappear, and replace them with thoughts that'll make you stronger.

If trusting God is a problem, one thing that can help is to look around us and delight in the beauty of the world. When Jennie was still in her junior year of college, she led a Bible study for a group that were letting their negative thoughts take control. So she quickly ran outside and grabbed a leaf from a tree. She asked the group to study the leaf closely, in all its amazing detail — its colors, its shape, its design. It was a reminder of all the work that God had put into creating it, and a reminder of just how much more work he put into creating us and our lives.

This is something that science supports: experiencing awe makes us less selfish. Studies have shown that letting yourself be overcome by the beauty around you helps turn your attention away from yourself and become more interested in others. It can also help you be more generous.

What's more, contemplating beauty is a great way to fight cynicism, which is another manifestation of a lack of trust in God. If you're feeling cynical, you'll be looking around you at other people and everything that might be wrong with the world. Instead, try fixing your gaze upward, to God. He won't disappoint you.

### 6. Choose to be humble and grateful, whatever life throws at you. 

A friend of Jennie's was feeling frustrated. Even though she had a college degree, she was stuck in a job she found boring, working in a fashion store. Didn't she deserve a better career than this?

But one day, she started listening to the Bible during her drive to work. A passage from Philippians 1, written by Paul, jumped out at her. He was expressing his gratitude: "I thank my God in all my remembrance of you," he wrote. At the time, Paul was living under house arrest.

Imagine that: being banned from leaving your home, and still seeing fit to express your gratitude. Jennie's friend realized that, comparatively speaking, she had nothing to complain about at all. So she shifted her attitude, and chose to see what she could be grateful for about her job — and how she could make it work for her. So she made an effort to befriend her colleagues, started to relish talking to customers, and before she knew it, she was loving it.

Gratitude is good for you. It'll make you feel better about yourself and your life, whatever your situation. And it'll help you see everything from the positive perspective which you need to break free of negative thought spirals.

Choosing to be grateful, rather than feeling like a victim, can even help people in extreme circumstances. Another friend of Jennie's was diagnosed with ALS, a rare and incurable disease of the nervous system. But the effect it had was to redouble his faith. It did the same for his wife's faith, too. Even after he passed away, she stayed grateful for God's plan, and still had a desire to find out what God had in store for her next.

We don't always have to like God's plans for us. But we do have to accept it, learn from it, and be grateful.

And it's important to remember another virtue, especially if we're feeling unhappy with God's plans: humility. Being humble means not expecting good things, not seeking praise, and staying calm in times of trouble. It's about remembering, in other words, that no individual is the most important person in the universe.

That humble realization might sound like admitting failure, but it's the opposite. As we'll see in the next blink, we were built to serve God and others — not to achieve for ourselves.

### 7. Reject complacency and work to put other people first. 

How do you think Jesus spent most of his days? It wasn't all miracles. Plenty of the time, he was just doing good, low-key, charitable work — discussing forgiveness and grace, helping the poor, and so on. It's not stuff that would've made the headlines.

You don't have to spend your whole life performing miracles, either. In fact, it's normal that you should devote lots of your time to service, whether that's helping your children out, cleaning up, or doing some other task that you might not get thanked for. You shouldn't be doing it for thanks or praise — you should be doing it for God.

That might sound tough, but actually it's a truly joyful way to live, because you'll be helping others as well as yourself. Think of it like this: your job is to follow God, and God's job is to make everyone's lives better.

Plus, it's how our brains are wired: we thrive when we have purpose. Serving others is beneficial for our brains in all sorts of ways, reducing stress and even helping us sleep. When we help someone, our brains act as if we've been given a reward. Because, in a way, we have.

This may not come naturally to you. That's quite normal! Devoting your life to service isn't something that people wander into by chance. More likely, people drift in the opposite direction — toward complacency. It's so tempting to sit on the sofa all day, glued to your phone, indulging yourself with worldly pleasures. But don't you get that strange, niggling feeling after a while staring at the screen? Isn't there a little voice in your head urging you to get up and _do something_?

"It is more blessed to give than to receive," Jesus said. That's why you should always choose to reject complacency and work to help others, for the glory of God.

> _"We are set free to serve so our lives will point all people to the joy we have now and the joy that is to come."_

### 8. Get out of your own head, and give your mind to Jesus. 

Eighteen months is a long time to spend in a negative spiral of thoughts. But Jennie managed to get out of that spiral — and out of her own head — by turning her focus to Jesus. By choosing to give her mind over to God, she managed to get herself onto an upward spiral of positivity that culminated in her writing the book these blinks are based on — which is designed to help others do the same thing.

The thing to remember is that we all have negative thoughts — in fact, we're all plagued by self-doubt and feelings of inadequacy and insecurity. The key question is what we do with those thoughts. Remember: the choice is yours.

If you choose God, you'll find that all that negativity goes away, once you truly listen to your own thoughts, share them with good friends, and surrender your fears. You just have to trust that God will get you out of it.

If this all sounds tough — well, it is. It's a lifetime's work, because nobody can rid themselves of negative thoughts completely. But on the other hand, it's not so hard to make a new start in our minds. Science tells us so.

Inside the neurons or nerve cells that make up our brains are tiny things called _microtubules_. These are the bits that join together when our brains are in action, giving the neuron its particular structure. And do you know how long it takes for microtubules to put together a neuron's structure? Ten minutes.

That's literally all the time it takes to rewire a piece of your brain. This might sound too good to be true, but the science doesn't lie. The significance of all this is that you're only ten minutes away from a clean slate, a fresh start. And this time, you can launch off onto a positive spiral of thoughts that will lift your mood, benefit those around you, and bring you closer to God.

### 9. Final summary 

The key message in these blinks:

**Don't let yourself be consumed by negative thought spirals. Instead, choose to surrender your thoughts to God, and trust in God's plans for you. Choose a positive, humble, and grateful attitude, to get closer to God.**

Actionable advice: 

**Capture one negative thought.**

With a pen and some paper, write down just one negative thought that's on your mind. Don't hold anything back — write the whole thing down, even if it makes you embarrassed. Now think: what does God think about that? No matter what the thought is, it's always possible to make a positive choice about what you'll do next. Trust in God's plans for you, and you can transform your negative thought into a spiral of positivity.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Purpose Driven Life_** **, by Rick Warren**

In these blinks to _Get Out of Your Head_ you learned about how to use your faith to transform negative thoughts into something positive, through choosing to think about God. But God isn't only there when you're trapped in a negative thought spiral — faith is something that informs your whole life, whether you're feeling negative or positive, doubtful or confident.

In _The Purpose Driven Life_, Pastor Rick Warren tackles the biggest question of all: why am I here? It's no accident, Warren suggests — God chose to give you life. This means that thinking about God is something for always. Head over to our blinks to _The Purpose Driven Life_ to learn how to fill each day with worship and community so that you stay on a firm and purposeful path.
---

### Jennie Allen

Jennie Allen founded IF:Gathering, a huge organization and series of events that help women spread the word of God. She is known for her media work and has written several books, including _Restless_ and _Nothing to Prove_. She also speaks at many events and has a popular podcast.

