---
id: 55f6ea9f29161a0009000114
slug: get-some-headspace-en
published_date: 2015-09-18T00:00:00.000+00:00
author: Andy Puddicombe
title: Get Some Headspace
subtitle: 10 Minutes Can Make All the Difference
main_color: EF7733
text_color: A35123
---

# Get Some Headspace

_10 Minutes Can Make All the Difference_

**Andy Puddicombe**

_Get Some Headspace_ (2011) paves an easy path to understanding meditation and the ways it can benefit us. Drawing on his own experience as a Buddhist monk, Puddicombe offers a strong case that even the busiest people can take ten minutes a day to get some much needed headspace and live better because of it.

---
### 1. What’s in it for me? Discover how meditation creates the daily headspace that will improve your life. 

Do you find your mind wandering, restless, stressed and even chaotic? You're not alone. The tempo of modern life can make you feel overwhelmed with things to do, places to see and life-affirming moments to be had, so how do you squeeze in some peace of mind? Maybe it's not about doing _more_, but instead finding an opportunity to tune out the buzz and appreciate what you're experiencing.

Meditation is about being aware and mindful of what you're already doing. It's about creating headspace in everything you do, instead of cramming in one hour of meditation when you think you have the time for it. In the following blinks you will not only learn how meditation can help you, but also how to combine it with your current lifestyle. It can be easier than you think.

In these blinks you'll discover

  * why focused meditation makes you care more about others;

  * how more headspace helped nine out of ten people improve their sleep; and

  * the mindful possibilities of something as simple as a walk.

### 2. Headspace means living just as you already do – with an added sense of peace and fulfillment. 

Imagine a life in which you can relish the moment and are free of extraneous thoughts, worries and distractions. This clear-headed state can be referred to as _headspace_. Sounds blissful, right? But how do you get there?

As you may have guessed — through meditation. By practicing regular meditation, you can teach your mind to relax back into its natural, chaos-free state.

So what exactly is meditation? It's a gentle unfurling of the mind in order to cultivate a more direct insight into the moment. It's being conscious of how and why we feel the way we do. Engaging in meditation offers respite from all the pressures and distractions of the modern world that make a perfect breeding ground for anxiety, and in turn affect our happiness and our relationships.

Furthermore, meditation is the skill of knowing how to step back and not let yourself get caught up in endless spirals of unproductive negative thoughts. A frantic, anxious mind is like a wild horse: to train it, you must be easy on it, cut it some slack and give it the space it needs. Gentleness is key. 

Great techniques to ease into meditation include simply sitting still and allowing your thoughts to drift by without becoming attached to them. You can elaborate on this exercise by focusing on a particular sense, such as sight or sound, and if you find yourself getting hijacked by your thoughts, simply refocus your attention to that particular sense.

If you're looking for moments of clarity, they will come from "not doing" and stepping back to allow the mind to wind down when — and how — it wants.

Contrary to popular belief, there is no such thing as a good or bad meditation. There is only a distracted or undistracted meditation or an aware or unaware person.

### 3. Knowing how to approach the practice of meditation is key to getting results. 

Where do you start when you want to incorporate mindfulness into your life and get some headspace? Well, don't rush in and set lofty goals from the very first time you sit down. Set some expectations which will get you into the right frame of mind. 

When you start to practice meditation, it's useful to realize that the more you try to hold on to pleasant feelings, the more scared you'll become of losing them.

You need to learn to let go. 

Meditation provides happiness by dropping resistance and heightening awareness. With little resistance, we feel comfortable regardless of the emotion we are experiencing, whether it's positive or negative. You can experience a quieter, more relaxed mental state when you give up the desire to constantly experience pleasant things and stop struggling to always avoid unpleasant things. 

A stressed mind is often the result of repressed emotions or the inability to deal with challenging feelings in a situation, an event or environment. That's why you should approach meditation intending to rest in the moment and not judge any emotions you feel. It's not about digging up old memories, thoughts or emotions. Nor is it about _controlling_ the mind. It's actually about relinquishing control, focusing attention in a more passive way and allowing the mind to unwind at its own pace in order to let go.

Imagine you are at the side of a busy highway, and all your thoughts are represented by the passing cars. When meditating, it's not your job to jump into the road and start directing traffic. Your job is to allow your thoughts to pass by.

### 4. Meditation doesn’t have to be an hour-long session, just ten minutes a day gives results. 

Want to dive straight into an hour-long meditation class complete with chanting, burning incense and chiming bells? Perhaps not, but is there another way? Of course!

The _Take 10_ meditation is a brief session that gently gets you into the right frame of mind. Here's how to do it.

Take five to ten minutes to slow down, then find a comfortable chair and set a timer for ten more minutes. A relatively unobtrusive timer is preferable. Nobody wants to be startled back to reality with a rooster call ringtone.

Next, it's check-in time. Begin by bringing your mind and body into alignment. As you let your eyes close, take five minutes to draw your awareness to your body. With your focus, scan your body from head to toe. Now create an image of how your body feels and allow your thoughts to drift in and out. If you get distracted by a thought — no problem — simply return to the part where you got distracted during your body scan.

Now it's time to begin focusing on breathing. Take 30 seconds to fully observe everything about your breathing as you breathe in and out. Silently count the in-breath as one and the out-breath as two and continue breathing and counting until you reach ten. Repeat this exercise until the timer goes off. If you get distracted again, simply go back to one.

To round off your Take 10, continue to free up your mind. Try not to focus on anything in particular, and for ten to 20 seconds gently give your mind some space. Then bring your focus back to the physical sensations of your body and your environment. Allow this awareness and newly created space to stay with you for the entire day.

### 5. You can find headspace no matter what you are actively or passively doing. 

Sitting down to meditate now and then is great, but how can you integrate meditation more deeply into your life?

You can start with walking. Walking doesn't need to be a passive, auto-pilot activity that leaves the mind trailing off into unproductive thoughts. You can use the time for a _Walking Meditation_ exercise.

Begin by focusing on your body and observing the sensations you feel when walking.

Take 30 seconds to take in your environment visually. Then turn your focus to sounds, and then to what you can smell. Finally bring your attention to physical sensations such as the warmth of the sun or the cool breeze.

After a minute or two, notice your body's movement and your feet as they hit the ground, just as you focus on your breathing during your sitting meditation.

Walking is only one way you can incorporate meditation into other parts of your life. You can also use meditative techniques with running and exercising.

The _Running Meditation_, for example, brings a balance of focus and relaxation to your exercise routines and helps you do them with far less effort.

Before you start exercising, check in with yourself mentally and physically to get a sense of how you're feeling. Focus and ground yourself by taking some deep breaths.

As you begin running, bring your awareness to your body, your breathing and the ways your mind is reacting to the initial sensations. As you settle into your run, observe your rhythm and whether your body feels tense or comfortable. The aim is not to try to change any of these sensations, but to simply be aware of them. If you feel discomfort, use it as an effective object of focus, just like the sensation of the foot striking the ground with the walking meditation. Observing these sensations can be central to helping you be mindful and remain in the moment.

### 6. Meditation can be integrated into all aspects of your life and improve your relationship with others. 

You don't need to carve out more time to retrain your mind, because when you're able to quit desperately rummaging around in your head and trying to control every little thing, you'll realize you've already created all the time and space you need to be more at peace. 

Use this clarity and headspace to provide a foundation from which you can handle the next 24 hours.

To retain your focus, view your day as a series of dots to connect. Move from one dot to the next instead of trying to run through the whole thing at once. This will make it easier to have purpose and awareness.

As an added benefit, meditation and mindful living don't only affect your own life, but lead to increased compassion and understanding in your relationships.

By concentrating less on your own worries and issues and more on the potential happiness of others, you will also do yourself a favor and create more headspace. Being present and living in the moment improves your relationships as it helps you to listen to what someone else is actually saying, rather than just tracking what you want to add to the conversation next. 

Your increased headspace enables you to become more patient with others, too. Impatience feels like an inevitable part of modern life, but meditation shows us that when we're aware of our unwillingness to relax and be tolerant, impatience loses its power over us.

### 7. A more mindful life can lead to better sleep and being able to control eating disorders. 

Practicing meditation and applying mindfulness techniques to other routines in your life doesn't just calm you, it can also improve your quality of sleep and your eating habits.

To drift off faster and dodge insomnia, you can use the _Sleep Meditation_.

Begin by checking in with yourself and observing how you feel. Then hone in on your breathing for a couple of minutes and walk yourself through your day from waking up to getting into bed. Then spend some time attending to each part of your body with the intention of "turning it off." This winding down meditation will help prepare you for restful sleep.

General meditation can aid your sleep, too. One study at the University of Massachusetts Medical School found that 58 percent of insomniacs experienced significant improvements in their sleep and a massive 91 percent cut down on or stopped taking medication as a result of practicing meditation.

There are more benefits to be had with eating, too. The _Eating Meditation_ can foster a healthy relationship to food. It helps reduce the impulses and thoughts that dictate our choices and actions when it comes to what we put in our bodies.

Begin the meditation with the _Take 10_ method by focusing on your breathing, slowing your mind down and settling in. Acknowledge where the food on your plate came from, recognize its value and the fact that it's available for you. Look at the food and notice its texture, color, smell and how your mind reacts to these characteristics. Then start your meal, maintaining the focus of being in the moment as you eat. If your thoughts begin to trail off or you get impatient, softly coax yourself back to the food and your breathing.

Again, even practicing meditation in general can have remarkable effects on eating habits. After battling an eating disorder and obsessive compulsive tendencies, Amy, a 24-year-old patient at the author's clinic, used meditation to return to a healthy lifestyle.

### 8. Regular meditation reduces stress and anxiety and leads to an enhanced quality of life. 

With all the distractions and pressures of today, it's increasingly common to fall prey to stress and anxiety. Luckily we know that living mindfully has been scientifically proven to be invaluable to our well-being. So how can you achieve mindfulness and experience less stress?

By adhering to your daily meditation.

Research shows that meditation can lower blood pressure, heart rate, breathing rate and oxygen consumption at the same time as boosting the immune system.

An Oxford University study found that stress can play a role in the ability of women to conceive and suggests that techniques like meditation could be key to supporting fertility.

Research from the University of Massachusetts Medical School even found that stress-related skin conditions like psoriasis cleared up four times more rapidly when a meditation routine was practiced.

Studies have also shown that meditation can significantly ease symptoms of anxiety and depression. That's because it allows emotions to come and go without building up into a negative filter that distorts our perception of the world. A year of meditation enabled Pam, a 51-year-old patient at the author's clinic, to come off her anti-depressants.

Furthermore, neuroscientists at UCLA confirmed that those who engage in mindfulness techniques are able to identify their emotions, are more aware of them, and are therefore less affected by negative ones. Similarly, after eight weeks of meditation practice, 90 percent of participants in a University of Massachusetts Medical School study displayed a significant reduction in both anxiety and depression.

### 9. Final summary 

The key message in this book:

**Meditation doesn't have to involve hour-long sessions in a room full of incense and chanting. The key is approaching it with gentle curiosity. Meditating for just ten minutes a day will help you to become present, to be aware of your emotions and to lead a calmer, more mindful life.**

Actionable advice:

**Make it as easy as possible.**

Finding a tidy, quiet place that you can use everyday for meditation, as well as wearing comfortable clothes, can make your meditation far easier. As far as time is concerned, the morning is often the best time to meditate as you can carry the clarity you gain with you throughout your day. Finally, noting your experiences in a journal can be extremely helpful for observing your progress, especially when you are starting out.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _10% Happier_** **by Dan Harris**

_10% Happier_ demystifies the ancient art of meditation by explaining recent cutting-edge scientific research into how meditation affects your body and mind. Importantly, it shows you just how valuable meditation can be in coping with the chaos and stress of modern life.
---

### Andy Puddicombe

Andy Puddicombe is a former Buddhist monk who founded Headspace in 2004 with a view to demystifying the preconceptions many of us hold about meditation.

