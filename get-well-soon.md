---
id: 5a6d0f9eb238e1000973200d
slug: get-well-soon-en
published_date: 2018-01-31T00:00:00.000+00:00
author: Jennifer Wright
title: Get Well Soon
subtitle: History's Worst Plagues and the Heroes Who Fought Them
main_color: 21A65C
text_color: 177340
---

# Get Well Soon

_History's Worst Plagues and the Heroes Who Fought Them_

**Jennifer Wright**

_Get Well Soon (2017)_ tells the story of the diseases and epidemics that have plagued humans from the distant past right up to the twentieth century, detailing the theories that people once had about certain diseases and how to treat them. There's room in the story too for the heroes who made breakthroughs in the treatment and prevention of diseases, or who helped sufferers when others shunned them.

---
### 1. What’s in it for me? Shine a light on the ailments that have wreaked havoc upon humanity. 

Modern medicine is a wonderful thing indeed. Even so, it's often easy to forget how lucky we are to be vaccinated at birth, or how normal it seems to have professional doctors tend to our needs when we get sick.

But when you think about it, this is a relatively recent phenomenon. Go back just a century or so, and the chances of succumbing to a deadly disease increase massively.

Throughout much of human history, we've been in the dark about how disease works, and the history of medicine is littered with examples of people getting it just plain wrong.

However, people have learned from these mistakes and healthcare has continuously improved. In these blinks, we look at how these improvements happened and what they say about human development in general.

In these blinks, you'll discover

  * what milkmaids had to do with curing smallpox;

  * what certain dance moves have to do with St. Vitus; and

  * how not to route your water supply.

### 2. Don't go letting Saint Vitus inspire your dance moves. 

Nowadays, the notion of someone dancing is quite a pleasant thing. In our mind's eye, we see smiles, a wedding band starting up or maybe a few drinks being knocked back. But it wasn't always this way.

In sixteenth-century Europe, a region bedeviled by plague, famine and wars, there was the chance that something more ominous was at play.

Famously, one day in 1518 in Strasbourg, then part of the Holy Roman Empire, a woman started spontaneously dancing in the street, and she only stopped dancing when she collapsed from exhaustion.

When she awoke, she started again, and before too long other townsfolk began joining in as well, jerking their limbs in every which way.

It wasn't as fun as it might sound. A collective delirium had descended, and more and more people danced until blood flowed from their shoes and bones ruptured the skin on their feet.

Strasbourg's elders were of a single mind: they theorized that the dancing was a punishment from heaven for the sins of _everyone_ in the city. The answer they arrived at was to assuage God by banning gambling and prostitution.

It was no use though. The "dancing plague" persisted. Soon, 15 people a day lay dead from heart failure, dehydration or infections from the wounds on their feet

The authorities hypothesized that Saint Vitus, the patron saint of dancers, had caused the mania. They posited that by taking the dancers to worship at Saint Vitus's shrine in nearby Hellensteg, he might forgive and cure them. At the shrine, each sufferer was given a pair of symbolic red shoes stained with the sign of the cross made in holy oil.

Remarkably, the cure worked. The afflicted stopped dancing and simply returned to their daily lives.

But, of course, faith cures don't really work. Community care and concern had most likely been the true antidote.

### 3. No one knew what caused the plague, and the attempted cures for it were often a little odd. 

The author's fear of Alzheimer's is so great that she often finds herself clicking on internet pop-up ads that proclaim a cure for the disease, despite knowing full well no such cure exists.

Perhaps it was precisely this same mentality that led medieval Europeans to start believing in all sorts of impossible "treatments" for bubonic plague.

The first symptom of bubonic plague was an apple-sized boil in the armpit or groin. Soon after, fever, vomiting and death followed. In the fourteenth century, bubonic plague carried off around 30 percent of the European population.

The worst part was that no one knew what caused it; consequently, some terrible ideas for treatment emerged.

Some people suggested living in the sewers. They reasoned that the body would become so accustomed to the filth that the plague would no longer harm them. However, as we now know, the plague was carried by rat fleas — and if you lived in a sewer, your chances of meeting a rat only increased.

Other people claimed eating more vegetables would ward off the plague. This sounds sensible to us at first, but the logic used at the time wasn't at all sound. People in the middle ages believed that vegetables would help because they were a substitute for other foods, particularly those like meat and cheese, that would smell bad if left out in the sun.

Rather than assuming that the disease was a result of bacterial infections in the body, many believed that bad smells were to blame.

However, the improvised cures weren't all so absurd. The famous French physician Nostradamus recommended regularly washing the body and clothes to reduce contagion.

And he had the right idea, since improved hygiene reduced the risk of a flea infestation. However, most people couldn't be persuaded; in the middle ages, people actually believed that bathing increased susceptibility to disease. They thought warm water expanded the pores, thereby letting the plague enter the body more easily. People generally bathed only twice a year and they changed their clothes even less often.

> Most people died within four days of exhibiting symptoms of bubonic plague and some even died within the first 24 hours.

### 4. The Spanish colonialists’ conquests were aided by smallpox. 

Horrifying as the bubonic plague was, European civilization did not collapse. Life continued much as did before.

But then, at about the same time Shakespeare was writing his plays, a quite different sort of disease emerged. It went on to ravage the continent of America and it destroyed almost all the civilizations that had existed there before.

It was called smallpox.

Smallpox was a virus that covered the sufferer with unpleasant, pus-filled sores. The virus deceived the immune system into attacking the body's own organs, eventually leading to an agonizing death.

Historians believe that, in 1525, a single diseased Spaniard infected Incan society with smallpox. Within a year, almost nothing remained of the 7,000-year-old civilization that had ruled over an area as large as Italy and Spain combined.

In fact, Incan civilization was so devastated by the disease that, just seven years after the arrival of smallpox, the Spanish conquistador Francisco Pizarro was able to conquer the Incan empire with only 168 men.

Theoretically at least, the Incas had an army of 80,000 at their disposal — but smallpox had incapacitated them en masse.

Most Spaniards had some immunity to the disease, as it had already been present in Europe for many years. However, the indigenous American peoples had no such immunity, and the virus would eventually go on to kill 90 percent of them.

Although it arrived too late to save the indigenous Americans, a smallpox vaccine was eventually developed in the eighteenth century.

The vaccine's development began with an observation made by Edward Jenner, an English physician. He noticed that milkmaids tended not to contract smallpox, and realized that they had effectively inoculated themselves against the more severe smallpox virus by catching the less severe cowpox from the milk herd.

It was from this discovery that Jenner was able to design the world's first smallpox vaccine using the cowpox virus. Vaccination proves a massive success; by the end of the twentieth century, smallpox had been eradicated.

### 5. Compassion, rather than isolation, helps leprosy sufferers. 

Imagine losing your sense of touch. It would be devastating, as you'd no longer feel the breeze on your skin or the kisses of a loved one. But at least it wouldn't be deadly, would it?

But now imagine you couldn't feel it if you were walking over broken glass, or if your hand was burning on a raging stove. Sounds a lot worse, right?

Unfortunately, that's the story of leprosy: it starts relatively innocuously but gets far worse.

Leprosy is a bacterial disease, and sufferers are often easily recognizable as they have lost fingers, toes or limbs. However, appendage loss isn't actually caused by the disease itself.

Leprosy prevents nerve signals from being sent from affected areas of the body, usually on the skin's surface, which causes the skin to lose sensation. Sometimes, for example, a sufferer may walk for miles in shoes that are too tight without realizing it. Blisters and infections may develop, and over time this can result in the affected foot rotting and falling off.

Because sufferers of leprosy are so identifiable, and because of the stigma attached to the disease, it has been historically common for societies to ostracize sufferers.

In 1856, for instance, the Hawaiian government forcibly quarantined its lepers on the remote island of Molokai.

Father Damien was a Belgian priest who, at great risk to his own health, moved to Molokai in 1873 to care for the leper community.

It was risky: leprosy can be caught through any open cuts or wounds.

But Father Damien built an orphanage on the island and he even didn't mind changing sufferers' bandages with his bare hands. He had an immense impact on the community and the lives of those suffering from the disease.

Sixteen years later, however, he died of leprosy himself. He first realized he had caught it when he dropped a cup of hot tea on his foot and felt nothing.

Father Damien's compassion helped lift some of the stigma attached to leprosy, and for his work, he was canonized by the Catholic Church in 2009.

### 6. To avoid cholera, don’t have your water source next to your sewage trench. 

It can be tough to change people's mind; sometimes, even a wide array of facts and figures aren't enough to sway a firmly held belief. Usually, this sort of stubbornness is just annoying, but in nineteenth-century London, the medical establishment's refusal to shift their opinion proved deadly for thousands of people.

Specifically, the issue was cholera, a disease that causes the sufferer to experience uncontrollable diarrhea until he dies of dehydration.

Prior to the nineteenth century, British physicians were convinced that cholera was caused by bad-smelling air. This was known as the _miasma theory_.

The authorities reasoned that the number of cholera cases could be reduced if they improved London's air. After all, the city stunk to high heaven: people were used to disposing of both human and animal waste in cesspools placed in basements or on streets.

As a result, the local government ordered everyone to throw their sewage into the River Thames — which also happened to supply most of the city's drinking water.

Unfortunately, cholera isn't caused by bad smells, but by drinking water contaminated by the feces of other sufferers.

It was only during an outbreak of cholera in Soho, London in 1854, when ten percent of the neighborhood died in just a week, that the real cause became evident.

In the face of such disaster, a doctor named John Snow became convinced that the medical establishment was wrong about miasma theory.

Snow set about interviewing people in all the affected households. He realized those who had contracted cholera had been drinking water from the Broad Street water pump, while the uninfected had gotten their water from elsewhere.

The medical establishment refused to buckle, however. They stood by their miasma theory, and even declared that Snow lived in a sewer.

However, during the next cholera outbreak in 1866, they started belatedly advising people to boil their drinking water.

After this, there was never another Cholera outbreak in London again, and John Snow was rightly proclaimed a hero.

> Miasma theory was so pervasive that people believed clean, good-smelling air could be collected from the top of the Eiffel Tower, and then released at ground level for the benefit of Parisians' health.

### 7. Censorship kills, especially when it comes to the Spanish flu. 

So far, we've mostly been looking at diseases in the distant past. But actually, there was a disease that killed around 50 million people as recently as the twentieth century.

We're talking about the Spanish Flu. And what's even more terrifying, we still have no idea how to treat it — or whether it will return.

In March 1917, an American doctor in Texas noticed that a deadly flu was rapidly spreading, mostly killing people between the ages of 25 and 29.

He warned the medical establishment, anxious that servicemen training for the war in Europe would become infected and carry the flu all over the United States. Unfortunately, no one listened.

Despite the fact that the flu was killing young, healthy people the world over, neither newspapers nor the medical establishment would acknowledge its existence. This was because strict laws passed during the war made it illegal in places like the United States or Britain to report on anything that might undermine the public's morale.

The virus became known as the Spanish flu, as Spain's neutral status meant no such censorship existed there. Thus, newspapers were free to report on the outbreak of the disease and it soon became associated with the country.

There were consequences to this lack of acknowledgment, particularly in that no preventative public health measures were enacted. In total, 25 to 100 million people died of Spanish flu between 1917 and 1919, including 675,000 Americans — that's more than died during the American Civil War.

By summer 1919, the disease had become less severe and fewer people were dying. Oddly enough, we still don't know why this happened. Scientists today are experimenting with reverse genetics to reproduce the disease, so that they'll have a vaccine ready if it returns. But given the speed with which we know flu viruses mutate, a proper vaccine may well prove elusive.

It's been a rollercoaster ride through some of history's most interesting and fascinating outbreaks. There's certainly been some luck involved, but with a bit of scientific know-how and care, major catastrophes can often be averted. However, we shouldn't get complacent. As the Spanish flu epidemic shows, deadly outbreaks can happen at any time; humanity has to be ever vigilant.

### 8. Final summary 

The key message in this book:

**There are hundreds of examples in history of the deadly diseases that have beset humans. While these can make for some fascinating stories, it's likewise important that we learn from our history. Governments and communities should be aware of the dangers that may emerge, and when diseases do arise, authorities should respond with transparency and compassion, as well as with scientific-based treatments. In this context, there is no place for superstition, prejudice or censorship.**

Actionable advice:

**Don't tackle diseases by chopping up raw onions and strewing them around your house.**

Because people thought that bad smells caused the bubonic plague, some also hoped that putting onions around their homes would purify the house's air and prevent the plague's entry. This obviously didn't work, but beliefs about the health properties of chopped onions are still shockingly prevalent to this day. In fact, the National Onion Association has a frequently asked questions section on their website that clarifies that placing chopped onions around your home will _not_ prevent disease!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Why Nations Fail_** **by Daron Acemoglu & James A. Robinson**

_Why Nations Fail_ revolves around the question as to why even today some nations are trapped in a cycle of poverty while others prosper, or at least while others appear to be on their way to prosperity. The book focuses largely on political and economic institutions, as the authors believe these are key to long-term prosperity.
---

### Jennifer Wright

Jennifer Wright is the author of _It Ended Badly: Thirteen of the Worst Break-Ups in History._ She has also written for the _New York Observer_, _Cosmopolitan_ and _Maxim_.

