---
id: 55b7e2ff3235390007550000
slug: get-whats-yours-en
published_date: 2015-07-29T00:00:00.000+00:00
author: Laurence J. Kotlikoff, Philip Moeller and Paul Solman
title: Get What's Yours
subtitle: The Secret to Maxing Out Your Social Security
main_color: 396E35
text_color: 396E35
---

# Get What's Yours

_The Secret to Maxing Out Your Social Security_

**Laurence J. Kotlikoff, Philip Moeller and Paul Solman**

We all dream about the day we can retire and benefit from a lifetime's hard work. Social security should make that possible, but its rules are so byzantine that investing in risky stocks seems like an easier deal. _Get What's Yours_ (2015) walks the reader through America's perplexing social security system and explains everything you need to know to get the guaranteed benefits you deserve.

---
### 1. What’s in it for me? Maximize your social security revenues. 

What do you think of when you hear the words "social security"? Most of us will be thinking of poor retirees, struggling to live on tiny state pensions. We think that if we want to be comfortable in our old age, we must invest in the stock market, or buy bonds. Anything to avoid surviving on social security.

And yet, this view is completely wrong. When we get close to the age of retirement, social security will be worth far more to us than our savings or investments, if we play it right!

But how do you pull that off?

These blinks will show you

  * why multiple divorces will help you when it comes to retirement age;

  * why, if you can, you should wait as long as possible before claiming; and

  * just how many conditions there are in the social security rule book.

### 2. Social security is an essential income source for countless people but its system is mired in confusion. 

If you look back a hundred years in many countries, reaching old age meant economic strife, because people who were forced into retirement lost their incomes and struggled to survive. Those who didn't have family or friends to support them were faced with frightening prospects, even starvation. So why don't we experience this problem any more? Because of two little words: _social security_.

At present, social security has expanded into a massive enterprise. In 2014 it paid over $812 billion in benefits while accommodating 180,000 visitors for consultations and 450,000 phone calls daily. But isn't social security just for poor people? Absolutely not, it's for every tax-paying citizen who invested in it and everyone who is entitled to should reap its benefits.

Why?

Because income from social security is safe. You could lose your home to a mortgage default, your investments could plummet in value, but social security is guaranteed to retain value. For instance, over the two decades between 1991 and 2011, the average US investor actually incurred a net loss after accounting for costs and inflation. But over the same period, social security grew — it's designed to increase with inflation.

So social security is a safe investment, but there's a problem. It can be confusing and people often find themselves perplexed as to what they're entitled to. For example, the social security system abides by 2,728 core rules. For married couples alone, the formula for calculating the benefit of each spouse consists of ten complicated mathematical functions! Not just that, but social security staff aren't exactly the most helpful. In fact, they can only provide you information, never advice.

So how can you be certain to get what you're entitled to?

### 3. Get the most from social security by understanding its basic principles. 

Social security is best understood from the bottom up and that means learning the basic terms and calculations of the system.

For instance, while you can file for social security at age 62, your Full Retirement Age — or FRA — when your benefits are the highest, depends on when you were born. Since most people retiring today were born between 1943 and 1954, they have an FRA of 66. But you don't _have_ to take social security upon reaching your FRA. You can keep working until 70. In fact, for every year after your FRA that you don't accept social security you'll accrue a Delayed Retirement Credit, or DRC, of eight percent interest!

Now, who's eligible for social security?

Anyone who worked a social security-covered job for 40 quarters of a year in total. If you meet this requirement you can collect benefits not just for yourself, but for your spouse or dependants too. The amount you receive is based on the Average Indexed Monthly Earnings, or AIME, the average of your 35 highest annual incomes adjusted for inflation. But for years that you weren't covered by social security your income will be considered $0. Therefore, the more years you work a social security-covered job, the better.

For example, a person earning $50,000 a year for 40 years is better off than one earning $100,000 for 20 years. Because the first person's data amounts to earning $50,000 a year for 35 years while the second's amounts to earning $100,000 a year for 20 years and $0 for 15 years.

So, you've reached FRA and you start collecting social security. You'll receive what's called the Primary Insurance Amount, or PIA, a figure based on a system wherein the more you earn, the progressively less you get in social security. For instance, for the first $826 of wages that your AIME marks, you'll receive 90 percent back in social security, for anything between $826 and $4,980 you'll get 32 percent and for anything higher you'll get 15 percent.

### 4. Three basic rules can maximize your social security. 

Did you know that your social security eligibility changes depending on how many times you were married? Many people don't. But even if you were never married it's key to understand some general guidelines.

The first rule to maximizing your social security is patience, because the longer you wait, the more you'll collect. For instance, any benefits you claim before reaching your FRA will be reduced. But it's not just benefits from _your_ work that you're trying to maximize, spousal benefits are 43 percent higher at FRA than when collected at age 62, and benefits taken by surviving relatives are 40 percent higher at FRA than at age 60. However, there are limits. Neither of these benefits increase further between your FRA and age 70. The only additional money you accrue during this time is a result of your rising retirement benefit.

The second rule is to take all available benefits. These could be from your spouse and even your ex-spouse. But first it's key to know what you're eligible for. For example, to receive spousal benefits you must have been married for at least one year, while for ex-spouse benefits you're required to have been married for ten years.

And remember, it doesn't matter whether you were married a decade ago as long as you meet the requirements. A simple jog of your memory could unearth a gold mine!

Finally, rule number three is to get the timing right.

Why?

Because you can't collect two benefits at once. Therefore, good timing is essential to reaping certain benefits while letting others grow. For instance, if your spousal benefit exceeds your own, you're only entitled to that from your spouse. For this reason it's often a good idea to collect your spousal benefit upon reaching your FRA, while leaving your own benefit to grow bigger. But don't jump the gun — filing for any benefits before reaching FRA requires you to take all that are available to you. So be sure to wait for that magic age!

### 5. Wise family planning can drastically increase your social security benefits. 

Have you planned for the future? Are you going to settle down and get married? While most of us make decisions like this based on love, it might be in your best interests to consider how life decisions can affect your social security benefits.

For example, spousal benefits can be a major source of income if you handle them correctly. In fact, a spousal benefit can amount to as much as half what your partner would receive upon reaching FRA. It doesn't even matter if they filed for benefits earlier as long as _you_ wait for FRA to collect.

Here's how it works.

To get spousal benefits your spouse must first file for social security, which as you now know is best done upon reaching FRA. Once your spouse files they can suspend collection of _their_ social security, thereby letting it accrue value and allowing you and your dependants to enjoy what you're entitled to.

However, life isn't always so simple and sometimes you need to take retirement _before_ reaching FRA. This is especially true when your spouse needs the money in advance of their retirement age. But this doesn't mean settling for a considerably smaller benefit, as long as you use the _stop–start strategy_.

How?

Start by claiming your benefits early; you'll receive a smaller amount but when you do reach FRA you can suspend your collection. This will let your benefits appreciate a few final years of interest before you reach 70.

Unfortunately, despite the various combinations of benefits for spouses and dependents, like children or disabled relatives, there is a limit. It's called the _Family Maximum Benefit_ or FMA, a collection amount which, once passed, results in reductions to your various sources of social security until you collect less than the FMA threshold once more.

### 6. When getting divorced and remarried it’s essential to keep social security in mind. 

Say you've been married for almost ten years and decide you want a divorce. While some things just aren't meant to be, if you wait the few extra months to reach that magic decade you'll be thanking yourself for years to come. But that's just one way your marriage choices can affect your social security.

Actually, social security might even incentivize you to get divorced. For instance, when married couples claim benefits, only one is entitled to spousal benefits. However, divorced couples who were married for at least ten years can both claim spousal benefits. Not just that, but claiming your ex-spouse benefit does not require your ex-spouse to file for social security as your current one would have to. As long as you've been divorced for more than two years, your ex-spouse only needs to turn 62 for you to cash in.

However, to get the most you'll need to know how much your ex earns. Unfortunately, if you're not on good terms with them it will probably be hard to get the accurate information you need and you can't rely on the social security companies because, due to privacy regulations, they're barred from divulging this information.

But divorce and marriage can affect your social security in other ways. For example, every time you remarry you lose the benefits you're entitled by your previous marriages. However, if you get divorced again you're once again eligible for all ex-spousal benefits. So remember, if you want to claim ex-spousal benefits, divorce is the only option. That is, until you reach age 60, at which point your current marriage no longer affects the benefits of your prior ones.

Another thing to keep in mind is the importance of survivor benefits, to which you're entitled after nine months of marriage. The amount you can collect is based on the PIA of your deceased spouse. So make sure you get the benefits that ten million surviving spouses did in 2013!

### 7. Protect your retirement from small, specific rules that can decrease your social security. 

Hang in there, you've nearly mastered social security. There are just a few more details you'll need to receive maximum benefits.

One such detail relates to the circumstances around same-sex marriages. For instance, if the marriage was officiated in a state where same-sex marriage is legal, but the social security filing made in a state where it's not, the benefits will be put on hold. In fact, unless the status of gay marriage in that state changes, it will be turned down entirely. So, to ensure you collect the social security benefits you are entitled to via your same-sex partner, it's essential to file in a state that has legalized same-sex marriage (although thanks to the Supreme Court decision of June 2015, this may have changed!).

But even if you receive benefits you may be required to pay taxes. For example, in cases where your combined income, meaning your non-social-security income plus any tax-exempt interests, such as you would get from government securities, plus half your social security benefits, totals over $25,000 per year, or $32,000 for joint filers, you're required to pay federal income taxes.

The rules get even more complicated if you had non-covered income, meaning income that did not pay into social security. For example, for work done in or for foreign countries.

In such cases, even if you have worked enough to be eligible for social security, it is likely that you'll not receive a high amount if you also have a pension or other benefits from a non-covered role. This is because of the _Windfall Elimination Provision,_ a rule that results in decreased social security for people with non-social-security pensions who didn't work covered jobs for at least 30 years _._

But having these kinds of non-social-security pensions also limits what your spouse is entitled to. For this reason, it's essential to be cautious about non-covered earnings and pensions to _Get What's Yours_!

### 8. Final summary 

The key message in this book:

**Social security is an important source of income for retired Americans and essential to ensuring that you spend your last years in peace. But to make sure you reap the full benefits you're entitled to you'll need to take a professional and well-timed approach.**

Actionable advice:

**Don't gamble your retirement.**

Some people think it's wise to take their social security benefits early and invest them, assuming that the reduced payments they receive will be easily replaced by their appreciating stocks and bonds. But the fact is, the average investor loses often and you don't have the time to spare. So keep your money in social security where it's safe.

**Suggested** **further** **reading:** ** _Millennial Money_** **by Patrick O'Shaughnessy**

Although technological advances have made it easier than ever to invest in the stock market, today's Millennials (young adults born between 1980 and 2000) tend to be risk-averse. But this kind of thinking is misguided. Given that benefits like Social Security and retirement pensions are imperiled, it has never been more important for young people to start investing in their future financial security.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Laurence J. Kotlikoff, Philip Moeller and Paul Solman

Laurence Kotlikoff is the president of a financial planning software company called Security Planning Inc. and professor of economics at Boston University. He is also the author of multiple bestsellers.

Philip Moeller is a journalist and research fellow at the Center on Aging & Work at Boston College. _Get What's Yours_ is his second bestseller.

Paul Solman is a long-time business and economics correspondent on PBS' _NewsHour_ and a professor at both Yale University and Gateway Community College _._

