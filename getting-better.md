---
id: 53c6431538393400071a0100
slug: getting-better-en
published_date: 2014-07-15T00:00:00.000+00:00
author: Charles Kenny
title: Getting Better
subtitle: Why Global Development Is Succeeding and How We Can Improve the World Even More
main_color: F2C030
text_color: 735B17
---

# Getting Better

_Why Global Development Is Succeeding and How We Can Improve the World Even More_

**Charles Kenny**

As pessimists talk of an economic development crisis, author Charles Kenny is optimistic in his assessment that in fact, all over the world, we've made enormous progress in overall quality of life. _Getting Better_ shows that the spread of technology and ideas has fostered a revolution of happiness and standard of living unprecedented in human history. Kenny provides evidence to make us enthusiastic about the progress we've attained so far, and offers suggestions on what is to be done if we want to keep this progress alive.

---
### 1. What’s in it for me? Understand how the world – contrary to common belief – is becoming a better place. 

People around the world are horrified by the extent of poverty and the growing gap between the rich and the poor. And it's true: rich countries are getting richer while the poor are getting poorer.

However, it's wrong to proclaim that things are bad and only getting worse with each successive generation. In fact, the average quality of life is at an historical high. People might not be equally wealthy in all parts of the world, but wealth might not be everything that matters.

In the following blinks, you'll learn:

  * why our average quality of life is steadily getting higher and higher,

  * why there is no one method to help a country grow its wealth,

  * why pessimism is not the only possible answer to current global economic development, and

  * what we can do to make the world an even better place.

### 2. The income gap between rich and poor countries around the globe is nothing new. 

Most people know that there is not only an income gap within economies but also across different countries. But do you know how large this gap actually is?

Here's a fact that'll shock you: around 1 billion people in the world live on less than a dollar a day.

But how is this possible when Western countries have developed so much over the past fifty years?

One reason is that there has been a forty-year stagnation in African incomes since the 1960s. For example, Senegal had an income per capita of $1,776 in 1960 and $1,407 in 2004. The gross domestic product (GDP) per capita in the United States, in contrast, was around seven times greater than in Senegal in 1960 and around twenty-six times larger in 2004. And while the average rural Zambian can expect to earn an approximate lifetime income of about $10,000, the average resident of New York City will earn $4.5 million.

Another reason why the gap is so large is that rich countries have always grown more rapidly than poor countries have.

In fact, the richest-poorest gap between countries grew from around thirty-three-fold in 1950 to around 127-fold today.

For example, in 1850, the Netherlands was the richest country in the world, with an income per capita of $2,371. In 2008, income per capita in the Netherlands was $24,695, while in that same year, in Congo-Zaire, income per capita was only $249. Or consider India in the 1990s: in 1993, India's bottom 40-percent had an income comparable to that of an English peasant in the 1400s!

Now that you know more about the actual gap between the richest and poorest countries in the world, you understand how lucky you are if you live in one of the richer countries.

### 3. There is no golden rule to help a country get on the path toward economic development. 

Did you know that economists do not have a master plan for launching poor countries toward sustained economic growth?

This is because economic growth theories simply do not work. The main problem is that economic growth theories generally assume all countries work alike. But in fact, most theories do not accurately fit the evidence on the ground, and therefore don't tell us much about the causes of economic growth.

Let's take a look at the relationship between investment and growth. Countries often see high investment and low growth, or low investment and high growth. But if we look at Zambia, according to an early "investment-to-growth" model, the country should be rich by now: considering their high investment rate from 1960 to 1994, by the end of the period they should have had a GDP per capita of around $20,000 — but in fact it was only $600.

Another reason why economic growth theories are weak is that it's way easier for us to locate causes for our current wealth in the past than to make strong statements about even medium-term future growth.

A final problem with economic growth theories is that the cause of economic growth in one country is unique to that particular country.

This is because economic growth is a highly complex, context-dependent phenomenon; so complex even that economist Francisco Rodríguez argues that there is only one valid scientific paper with empirical merit that can be written using cross-country growth data. All this paper would state is that the answer to the question, "What causes economic growth?" is not "X" or "Y," but — "It depends"!

When even economists don't know how to foster economic growth, it seems like the only way out for poor countries is trial and error. But does that sound satisfying to you?

### 4. We are globally experiencing a rapid and historically unprecedented progress in quality of life. 

Have you ever considered that most of us today are living lives that our ancestors only a hundred years ago couldn't have even imagined?

In fact, when we look at almost any measure of quality of life — except income — we observe rapid and massive worldwide progress.

This observation is true for measures of health, education, civil and political rights, access to infrastructure, communication and culture.

For example, since 1960 the global average infant mortality rate is down by half and the number of people who say they are happy has been rising everywhere. Furthermore, between 1970 and 1999, the literacy rate in Sub-Saharan Africa has doubled, from less than one-third of the population to more than two-thirds. And in only forty years, between 1962 and 2002, life expectancy in the Middle East and North Africa has increased from around forty-eight years to sixty-nine years.

And this development isn't just happening in specific parts of the world — quality of life is increasing around the world.

In fact, there is a growing equality in quality of life across countries. Not only does today's global population share the highest average level of health at any point in history, it also shares the most equitable distribution of good health at any point in the last few hundred years. And there has been a close to universal increase in global average life expectancy, from around thirty-one years in 1900 to sixty-six years by 2000 — a statistic that holds even in the poorest of developing countries.

So next time you grumble about your quality of life, think about how amazing it is to live today and not in the Middle Ages!

> _"Life has been getting better, pretty much everywhere at pretty much the same rate."_

### 5. What we need to improve quality of life is now cheaper and more widely available than ever before. 

Did you know that the good life has never been less dependent on high income than it is today?

Why is this the case?

Because the global supply and demand of technology and ideas have reduced the costs of living.

A vital technological factor for the global decrease in the costs of living — as well as the threat of widespread famine — is the improvement in agricultural productivity. This has led to food prices falling by approximately 50 percent in the second half of the twentieth century. And access to sufficient calories has become so inexpensive and easily available that even in low-income regions, obesity is becoming an epidemic.

Another way our quality of life has been recently improved is by the global spread of certain ideas concerning hygiene, practices that have helped radically improve people's health at a low cost.

For example, programs that encourage people in developing countries to add small amounts of bleach to their drinking water have helped reduce diarrhea cases by 50 to over 80 percent. And oral rehydration with a solution of sugar, salt and water has effectively treated cholera, and prevented deaths from other causes of diarrhea.

On top of these improvements, the cost of a good life is still declining.

This is due to governments improving the delivery of services such as education, health care and access to political organizations. And in the future, new technologies and ideas will make services like these even more accessible and affordable.

### 6. Quality of life is more important than income development. 

Most people think that income growth is the central factor when it comes to measuring development. But aren't they forgetting about the development of quality of life?

In fact, income growth is not the best indicator of progress. This common misconception is due to the confusion of economic growth with "development."

Development is better measured in terms of quality of life, such as life expectancy, education or political liberties. For example, in 1870, the life expectancy for someone living in a country with an income of $300 per capita was one-third of that of a person living in a country with an income of $30,000 per capita. But by 1999, even though incomes hadn't changed, the life expectancy of the poorer country had increased to half that of the richer country. This shows that even though income is an important factor for development, it is not the central factor.

So if development is about more than just economic growth, what is it exactly?

What we really want from development are improvements in health care, education and security, while income is only a means to those ends. And despite still existing global economic disparities, we are getting there: humanity has never been in better shape than today, and is still getting better.

For example, rates of absolute poverty have fallen dramatically on a global level, and infant mortality has become a rarity while life expectancy has significantly risen. This means that if we want to get an accurate picture of human progress, we need to set aside our pessimistic prejudices and take a broader view of "development" and the good life.

> _"Smile and more of the world than ever will smile with you."_

### 7. Development policies should support quality-of-life improvements. 

If there is no golden rule for economic development, what rules are there for creating economic policy?

The answer is simple: policies should be centered on developing quality of life.

However, some people worry that focusing on quality of life can damage long-term economic growth. In fact, even though improvements in health care, education or political rights may not immediately lead to economic growth, there is at least some evidence for these elements affecting economic results over a longer period of time.

For example, the reduction in child mortality that results from improved health care leads to a reduction in birth rates. The resulting demographic transition toward more adults per child can then be linked to better economic performance over the medium term.

But even if these policies fail to promote economic growth, they are still a great success if they lead to an accelerated progress in quality of life. For example, supporting governmental structures allows for the better dissemination of knowledge about health and hygiene. And a quality education leads to more opportunities in life.

Above all, policymakers should avoid policies that call for sacrifices in quality of life in the name of rapid income growth.

This is because we do not have knowledge we can trust about growth-inducing reforms, and because fundamentally, income is only a means to an end for a better quality of life. For example, history shows that extreme economic philosophies, such as communism or neo-liberalism, that are centered around rapid economic growth actually have a relatively low impact on long-term economic growth.

So even if there is no golden rule for economic growth, there is one for economic development in general: policies should aim at quality-of-life progress.

### 8. The global community – and especially rich countries – should support the developing world. 

So what can be done on a global level to help accelerate the trend toward better quality of life, without disrupting global markets?

The rich countries should promote useful trade, migration and military policies.

For example, since migration is the driving force for overall income convergence within countries, it is indispensable to follow reasonable migration policies and allow a freer flow of people across borders.

The global community should also try to achieve on a global level their nationally acknowledged universal beliefs, such as providing a peaceful society, solid infrastructure, quality health care and education.

On a national level it appears to be a widely unquestioned principle that we should reduce inequality, and facilitate access to universal goods such as health care and education. And since the logic guiding such principles doesn't just stop at any border, everyone around the world should also be allowed to engage in healthy, sustainable and productive activity.

Furthermore, just like on a national level, the costs for ensuring such a state of affairs would be remarkably lower than the benefits.

Importantly, a certain level of uncertainty regarding the best approaches to improving our global quality of life should not prevent us from doing our best to help.

However uncertain we are about the most efficient way to do this in developing countries, this does not diminish the imperative to act. For example, a dollar spent on another flat-screen TV may not have any impact on the wellbeing of a child in the United States. But we do know the likely impact of a dollar spent on an insecticide-treated bed net on the health of a child in Africa: a better life.

### 9. Final summary 

The key message in this book:

**Contrary to common belief, we have actually made serious progress toward improving quality of life on a global level. This becomes clearer when we accurately measure development not by income growth but by quality-of-life improvements. Therefore, even if we do not understand how to foster income growth, we can still improve the world by supporting programs that develop quality of life.**

**Or as the author puts it: "Humanity has never been in better shape — and despite growing challenges of global sustainability, the future should be even brighter."**

Actionable advice:

**Put things into perspective.**

Next time you start worrying about the world, think of the immense overall progress in quality of life we have already experienced.

**Invest your money where it's needed.**

Before you spend your money on stuff you don't need, think about supporting the poorest countries in the world — and improve quality of life for those people instead.
---

### Charles Kenny

Formerly with the World Bank, American economist Charles Kenny is a senior fellow at the Center for Global Development and the New America Foundation. He writes for the magazine _Foreign Policy_ and has published many articles in various publications addressing the correlation of economic growth, health and happiness.

