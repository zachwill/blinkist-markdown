---
id: 582b18354fe8520004998ba5
slug: getting-everything-you-can-out-of-all-youve-got-en
published_date: 2016-11-22T00:00:00.000+00:00
author: Jay Abraham
title: Getting Everything You Can Out of All You've Got
subtitle: 21 Ways You Can Out-think, Out-perform, and Out-earn the Competition
main_color: EE3B34
text_color: BA2E29
---

# Getting Everything You Can Out of All You've Got

_21 Ways You Can Out-think, Out-perform, and Out-earn the Competition_

**Jay Abraham**

_Getting Everything You Can Out of All You've Got_ (2000) is a guide to spotting new opportunities, securing new clients and succeeding, no matter what your area of business. These blinks explain how to make more out of what you already have and how best to use it to your business's advantage.

---
### 1. What’s in it for me? Boost your business success with five easy strategies. 

Your business is full of untapped potential. The Ray Krocs and Ted Turners of your industry would take just one good look at it and spot an amazing opportunity. Maybe this opportunity is disguised as a problem, or maybe it looks like an outlandish new way of doing things; either way, whatever it is can propel your company to the top of its business sector — and beyond.

So how do _you_ spot it? How do you motivate your employees and leverage all the necessary support to get you there?

These blinks will take on just these types of questions, teaching you some of the best ways to outcompete your competitors and introducing you to the _top five_ strategies that can make your company flourish.

You'll also learn

  * how 1,400 can openers turned a small radio station into a big business;

  * why you need to be like a pilot fish; and

  * how a massive failure led to the Post-it note.

### 2. To grow your business, reward your customers and your salespeople. 

Can you remember the last time you bought a pair of shoes? Chances are the salesperson recommended some products to go with your new pair of sneakers or high heels, like polish or insoles. While such upselling might be annoying, it often works. In fact, your business will expand dramatically if you can convince just one out of three clients to buy a little bit more.

But that's not the only way to expand your business. Another strategy is to convince clients to remain loyal to your company.

How?

Well, there are plenty of ways to keep customers coming back, but premiums and loyalty programs work especially well.

For instance, chain stores like Sears have loyalty programs that reward customers for frequent purchases. In such programs, customers usually earn points for every item they buy and, as these points accumulate, they can be cashed in for discounts. As a result, the client will want to keep buying from the same store rather than miss out on this bonus.

Another way to make your business grow is to expand your client base by giving your salespeople a piece of the profits. For example, lots of companies pay their salespeople a percentage, say 10 percent of the profit from every sale they make. Naturally, the prospect of a commission incentivizes your staff to recruit new clients — but you can motivate your salespeople even more by letting them keep 100 percent of the profits from their first sale to every _new_ client.

This might seem like a pretty big reward if a sale averages something like $200, but if the average client spends that amount five times per year, your total profit from them would be $2,800 over three years, even after the $200 bonus you gave the salesperson.

Not only that, but such an incentive can motivate sales staff to recruit ten times as many customers.

> _"You are surrounded by simple, obvious solutions that can dramatically increase your income, [. . .] and success."_

### 3. Leap ahead to business success by seeing opportunities in any situation. 

People tend to assume that the road to success is necessarily a rocky one, and therefore needs to be walked in tiny, manageable steps. But if you truly want to succeed, baby steps aren't the way to go. Instead, you need to think big and take _quantum leaps_.

A quantum leap is that bold move into an area that your competitors haven't thought of yet. Instead of focusing on what's difficult and what doesn't go according to plan, look for paths that others haven't spotted.

For example, a chemist at the 3M company was trying to develop a super-strong glue, but ended up with a very weak one instead. Everyone thought of it as a useless failure; that is until a fellow scientist found the perfect use for the product. He applied it to pieces of paper, in the process inventing the Post-it note — and we all know how that particular invention turned out.

The point is, to make breakthroughs you don't necessarily need to invent something from scratch or even revolutionize marketing. All you need to do is find a novel approach, discover a great application for something that already exists or simply introduce an existing product to a new market.

Just consider the innumerable cappuccinos bought at Starbucks locations across North America this morning. The coffee chain didn't invent this beverage; they just copied it from everyday European cafés and it became a huge success when introduced to US markets.

So, to find the breakthrough that will carry you soaring forward, you need to think boldly. And that means you should only consider your competitor's approach as a reference point, so you know how to diverge from it. This way, you'll discover opportunities that can make all the difference for your business.

### 4. Cutting back on your customers’ risk will earn you a competitive edge. 

It's probably clear that a thriving business is built on clients, and lots of them. But how can you outcompete the rest of your field and bring in the customers you need to succeed?

Well, a tried and tested way to make your product or service more attractive is to minimize the risk to your customers. After all, people don't like uncertainty and, to make potential clients choose you over the competition, you need to eliminate as many risk factors as possible.

For instance, imagine you're in the market for a pony, and find two ponies of equal quality. One costs $500 and the other $750. However, the seller of the more expensive pony offers you a 30-day money-back guarantee to see if the horse gets along with you. He also offers you free hay and training in pony care, and to top it all off, if you _do_ change your mind about the pony, he'll come pick it up and clean out the stable, free of charge.

All of a sudden, the $250 difference doesn't seem so big and the more expensive horse becomes the obvious choice.

So, to win over as many clients as possible, try offering them a _better-than-risk-free guarantee_ or a _BTRF_. A BTRF is way more attractive than any standard guarantee you've seen because it doesn't just promise the customer a total return of their money if they're dissatisfied, it also offers to compensate them for the time and effort your product cost them.

Such a sweet deal makes it very difficult for anyone to say no. For example, say the pony you buy eats too much or keeps bucking off its rider; with a BTRF, you can get your money back, plus compensation for any inconvenience.

### 5. Symbiotic relationships with other companies can help you build your client base, but be sure to choose your collaborators wisely. 

Sharks are famous for lots of things, but certainly not their friendly demeanor. After all, they eat just about every other creature in the sea. Well, with one exception: they get along great with pilot fish!

Pilot fish feed on the leftovers stuck in between a shark's teeth, thereby keeping their fearsome chompers squeaky clean. This is known as a _host-beneficiary relationship_ or, in layman's terms, a win-win. It's just this kind of agreement that can help you secure clients without investing much effort or money.

Most companies, probably yours included, spend loads of money, time and effort trying to secure new customers. In doing so, they generate new marketing strategies, create ads, hire new salespeople and invest in all manner of other things. However, there is an easier way.

Create a win-win situation by getting host companies to introduce you to a pre-established audience or clientele. For the host company, the recommendation helps cement a strong customer service reputation, while for you, the beneficiary, it's cheaper, easier and faster.

For instance, one landscaping company asked a real estate firm to recommend their services to clients who had just purchased a home. After striking this deal, the landscaper's sales rose by 40 percent!

Just remember, if you've got a choice in the matter, go with a host that sells a product or service that's related to yours, but with which you're not in competition. A potential host will likely reject your offer if you're a competitor of theirs, or he might agree to the deal, but only at a major additional cost.

That's precisely why the agreement with the realtor was such a perfect fit for the landscaping company; their client base overlaps since recent homebuyers are likely to need a landscaper, but they offer completely different services.

### 6. Money isn’t all you’ve got, and trading can be wildly successful. 

Did you know that the great novelist Charles Dickens traded his first story for a handful of marbles? It may seem ludicrous, but it's actually not such a bad idea to barter with what you've got — although you'd probably want something more than just marbles.

In fact, bartering your products or services can help you secure some pretty great deals and make your business thrive. Normally, when a business is strapped for cash, they might think they're nearing the end of the road. But even if you're short on cash, you can exchange something else that another person is interested in.

For instance, a small radio station in Florida was going through such a difficult period that they didn't even have enough money to pay their employees. Instead of looking for a way to get money directly, say, by taking out a loan, they reached out to a local hardware store and offered to advertise for them.

The store accepted the offer and paid for the ad with 1,400 can openers. The station sold the can openers on air with such success that they eventually expanded to television. Maybe you've heard of the station; it's called the Home Shopping Network.

So, trading can work wonders, but what if the person or business you want to barter with isn't interested in what you have to offer? Just bring in a third, or even fourth, party who _is_ interested.

Imagine you're a butcher who wants to trade his meat to a radio station in exchange for advertising. The only problem is that the owner of the station is a vegetarian.

What do you do?

Just look around and find a meat-lover, perhaps a carpenter who'll exchange your meat for a few wooden boxes. Now you can return to the radio station owner, wooden boxes in hand. Chances are he'll be overjoyed by the new offer, since he can grow vegetables at home in the boxes.

Be creative and consider what assets you have that don't come in the form of money — it might just lead to an unexpected miracle.

### 7. Final summary 

The key message in this book:

**Business success isn't just for those with unlimited resources. There are lots of unconventional ways to build a company you can be proud of, and plenty of incredible opportunities hiding in plain sight — you just need to know how to discover them.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The ONE Thing_** **by Gary Keller and Jay Papasan**

_The ONE Thing_ (2013) helps you to discover your most important goal, and gives you tools to ensure you can use your time productively to get there. The book reveals that many of the maxims we accept as good practice are actually myths that only hinder our progress. It also provides advice on how to live your life with priority, purpose and productivity without sending other aspects of life out of balance, because this is the way to perform the kind of focused work that leads to great success.
---

### Jay Abraham

Jay Abraham is the founder and CEO of the Abraham Group, a highly successful business and marketing consultancy that has helped countless major corporations become what they are today. He's also a frequent speaker at conferences and the author of several others books including _The MasterMind Marketing System_ and _The Sticking Point Solution_.

