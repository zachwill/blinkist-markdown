---
id: 566fde00cf6aa30007000010
slug: getting-more-en
published_date: 2015-12-15T00:00:00.000+00:00
author: Stuart Diamond
title: Getting More
subtitle: How You Can Negotiate to Succeed in Work and Life
main_color: FAE63A
text_color: 7A711C
---

# Getting More

_How You Can Negotiate to Succeed in Work and Life_

**Stuart Diamond**

_Getting More_ (2010) lays out precisely how to negotiate your way toward a fuller, more satisfying life. The strategies and tools described in this book can be used in any situation, from finding a happier outcome when sparring with a partner to convincing your boss that you're long overdue for a raise.

---
### 1. What’s in it for me? Become an expert at negotiating and get what you want. 

Every day you interact with different people in a number of unique situations. You buy a loaf of bread at your local bakery; you talk with your children about completing their chores; you discuss your latest work project with your boss.

You may not know it, but each of these interactions is a form of negotiation. Negotiating isn't just something you do when buying a house or a trinket at a flea market, but something you do every single day. The question is, are you good at it? If the answer is no, then these blinks can help.

Here you'll learn how you can become a great negotiator by following just a few simple steps.

In these blinks, you'll discover

  * why a poor negotiator might win a battle but lose the larger war; 

  * how your own high standards can raise everyone's standards; and 

  * why getting yours depends on if you know what the other side wants, too.

### 2. Every interaction in life is a negotiation; you need to learn how to get more from them. 

What do buying a trinket at the flea market and selling a multimillion-dollar company have in common? Both scenarios involve negotiation _._

In fact, all your daily interactions involve some form of negotiation — though you might not realize it.

So let's examine the four different forms of negotiation. 

First, there's _forcing_ a person to do something. This strategy is really only a fraction of most negotiation processes, if it's used at all, as it requires power — which is costly and often counter-productive.

Second, there's getting a person to _believe_ what you want him to believe. This is preferable to using power, but when you have an emotional attachment to the person, it's a bit tricky to achieve.

Third, there's getting a person to _see_ things the way you want him to see them. 

And finally, there's persuading a person to _feel_ what you want him to feel.

Now that we've defined the different forms of negotiation, let's look at its overall benefits. It's simple — negotiating for _what you want_ will net you more in every situation.

To negotiate effectively, you need to determine your goal. If you don't know exactly what you want at the outset, you can end up negotiating a short-term solution that actually leads you further away from your goal.

If you're stuck in the hospital after a knee surgery, for example, you might be eager to convince the doctors you're fine so you can return home. Yet if you negotiate an early leave, missing needed physical therapy, you might end up injuring yourself again — thus compromising your goal of having a healthy knee so you can play basketball.

The more aware you are about how and when negotiations happen, the more you will get out of every negotiation you have, thus bringing you closer to your goals. 

The truth is, we all spend each day wanting things. Being aware of the negotiations that determine how you interact with others can help you get what you really want, instead of going through life moaning about what you don't have.

> _"Every time people interact, there is a negotiation going on: verbally or nonverbally, consciously or unconsciously."_

### 3. To negotiate well with people, you need to establish an honest and trusting connection with them. 

So the essence of negotiating is trying to persuade a person to see and feel things the way _you_ want. To do this successfully, you need to establish a connection with your negotiating partner. 

But how can you do this? For starters, you need to value the person and do your best to show it. 

You'll be amazed how the simple act of thanking a person for his time can shift the dynamics of a negotiation. The humble "please" and "thank you" make a bigger impact than you think! A word of warning, though: be sincere. If you're putting on an act, the person will know. 

Yet if you aren't able to build trust, then at least make sure you have ways to get your negotiating partner to commit to you in a way that is natural to him. For example, don't try to force a person to sign a contract if deals are sealed in his country with a handshake. 

If you're struggling to build a connection with your partner because you simply can't stand each other, however, enlist a third party to speak on your behalf. 

Although each negotiation is different, building a relationship with a person is usually not that difficult. Just be sure that, even if you use the same basic tools for all your negotiations, you let the context dictate how you use them.

For example, it's important to value your negotiating partner, but not all charms work for all people. A simple "thank you" might work for some; for others, a handwritten card or even flowers will do much more to establish trust. 

Importantly, never assume that people from certain groups or cultural backgrounds have the same preferences. Do away with any stereotypical views you might hold, and remember to treat others as the individuals they are. Learn their preferences and then work to build a connection.

The bottom line is _always be nice_. People who feel a connection to you will be much more forthcoming than someone you've insulted!

> _"Even if you hate the other side, you need to connect with them."_

### 4. Assume nothing! Be aware of your partner’s perceptions and put yourself in his shoes. 

Each person's differences and preferences are important. Let's explore how these differences can affect a negotiation.

Take perception. It's essential to bear in mind that each person holds his own views on any given topic. Yet we tend to assume that most people see the world in the same way that we do.

Consider the well-known query: is the glass half full or half empty? The answer depends on the person looking at the glass. It's always the same glass; yet each individual's perception is different.

Perceptions rule our actions, so it pays to find out more about them. To do this, and to ensure you're on the same page as your negotiating partner, you must communicate effectively.

Use good communication to find common ground. This means asking questions and putting yourself in your negotiating partner's shoes.

For example, if you've been waiting for your dinner at a restaurant for over an hour, don't immediately assume your server is at fault. Instead, ask him if there's a specific issue that's causing the delay.

Another tip is to summarize what you hear from your negotiating partner as often as possible. This way, you can maintain a mutual understanding as the dialogue continues. 

And be sure to keep your discussion calm. Getting angry will only work against you, even if the other person wants to shout and rant. Instead, ask why the person is so worked up, and how you might have caused the misunderstanding. 

Lastly, pay attention to subtle signals. For most people, saying "I can't help you right now" is a brush-off. But instead of losing your cool, ask instead, "Do you know anyone here who can help me?" or "When would be a better time?" 

In sum, move away from the assumption that your own perceptions are correct, and start asking and understanding how other people see things. When you locate a gap between your perceptions and your negotiating partner's perceptions, you should focus on closing it.

### 5. Make progress toward your goal by using the other person’s standards as part of negotiations. 

Now you know how focusing on your negotiating partner and understanding his perceptions can get you closer to achieving what you want. But what if he's just not listening?

In such cases, try to negotiate using your partner's standards — particularly if he drives a hard bargain. By holding up your partner's standards as an example of right behavior, your negotiating position is strengthened when you catch him in the act of going against those standards.

Here is how this strategy works. Imagine you're staying in a luxury hotel and you discover the bathroom's filthy, with lots of hair in the shower drain. When notifying a staff member, you might consider reminding her that the hotel prides itself on high-end accommodation and hygiene — a standard set by the hotel chain itself. 

This tactic might not be enough to get someone to act, however. Unfortunately, not all people think they need to treat others as they'd like to be treated themselves. If this is the case, shame can be your friend. 

If a staff member refuses to do anything about the state of your shower, try pointing out that she's compromising her own standards by acting contrary to the hotel's best practices — in other words, she's simply behaving badly. You'll be surprised how effective this approach can be when delivered in a decent and polite tone — and in front of other people.

It's important to reach your goal bit by bit, so that the person can easily follow your logic.

So don't just march up to a hotel staff member and announce, "The hair in my shower is against your hygiene standards." Rather, lead the employee to the conclusion step by step, allowing her to make the decision to help you. 

For instance, you could ask whether the hotel is one of the best in town, then ask about the hotel's hygiene policy. Only then should you tactfully ask how this policy applies to the state of your disgusting shower!

> _"It is a fundamental tenet of human psychology that people hate to contradict themselves."_

### 6. To gain an edge in negotiations, figure out what your partner values and set up an exchange. 

Good negotiators know that each person sees the world differently. Let's take this a step further and explore how different perceptions are also responsible for the value individuals place on things.

People in early societies used a barter system to acquire goods and services; people would trade things they had for things they needed. A hunter, for example, might offer two rabbits to the baker for a loaf of bread. With the introduction of a monetary system, this process was simplified. But we as a society still retain some of those older values, even today. 

This means that many people value things without relation to their actual purchasing price. Consider an art auction, for example, where two individuals might be willing to pay two very different prices for the same painting. 

What makes two people value the same item so differently? 

One bidder might think the painting would look perfect in the living room of her new home. Yet the other bidder might feel a pang of nostalgia for a blissful childhood when looking at the painting. For the second bidder, the painting is priceless!

You can use this knowledge to your advantage. To get what you want, find out what the other person values and use this information to make an exchange.

Making a connection with someone and getting to know them is a formidable negotiation strategy, as we've seen. Now you can depart from the small talk and start looking for things the other person values that might not mean too much to you. With this knowledge, you can make your exchange.

If you're applying for a job, for example, it could be useful to find out what the employer is currently working on. Perhaps they're launching an initiative that employees find boring. 

If it's interesting to you, however, use this information as part of your interview. Now you're not only offering a great set of skills but also some much needed help on a floundering initiative. And just like that, your value as a potential employee has soared!

### 7. Don’t ignore emotions, as they can easily derail a negotiation. 

Humans are emotional. Yet sometimes our emotions can get the better of us, and can also get in the way of successful negotiations.

Emotions can prevent you from thinking clearly and take your focus away from your own interests. Because emotions can so easily derail a negotiation, you need to find a way to deal with them.

If emotions start to take the reins of a negotiation, you ‒ or your negotiating partner — might shut down and suddenly become incapable of listening to any rational argument. 

In one divorce case, the author, who mediated the case, saw this sort of behavior firsthand. The husband, frustrated that the drawn-out process was costing him time and money, offered his wife nearly all his current assets just to get it over with. Yet the wife was so angry at him she refused to even sit down to the negotiating table. 

In such a case, to be a successful negotiator, you need to acknowledge other people's feelings and find a way to make _emotional payments._ Essentially, you have to find a way to provide a person with emotional relief.

With this in mind, the author explained to the wife that if she agreed to a settlement that included all the husband's assets, he would be left with nothing — and that such a condition would no doubt be painful for him. The wife, realizing that the settlement provided her with the emotional relief she needed — that her husband would suffer like she had suffered — finally agreed to it. 

An essential thing to remember when acknowledging an individual's feelings, however, is to be sincere. Feigning an emotional connection just to calm someone down might work initially, but will ruin any chance of a long-term relationship if the person realizes you've been disingenuous.

> _"People who are emotional stop listening."_

### 8. Prepare yourself for every negotiation with well-planned and practiced strategies. 

Now that you've filled your toolkit with useful negotiation strategies, how do you best apply these strategies to ensure you are as prepared as possible for your next negotiation? 

Try using the _Getting More_ model, which helps you give structure to your negotiations. Here's how it works. 

First, outline the basics of a negotiation. Determine your goals, and the problems that are preventing you from achieving them. List all the participants in the negotiation, including the decision makers, and any third parties you could use for reference.

Say you want a reporting position at the _New York Times_. In this case, define exactly what stands between you and your goal. Is it lack of experience? Or maybe you need special training? Identify who will be reading and filtering job applications to better inform yourself.

Second, consider which negotiation techniques you'll need to use. This means listing the needs and interests of both parties — including any irrational ones. Consider the views of the other negotiators and try to put yourself in their position. Think about which kinds of negotiation approaches you should explore, and be sure to learn about the personal standards of the other party. 

Third, prepare yourself for various potential outcomes by brainstorming ideas on what you will say or do if certain problems arise. 

Also, how might you divide your goal into smaller steps, so that the negotiating party can see your point of view more easily or clearly?

Fourth, focus on the particular actions that will lead you to your goal. Which options are the most likely to get you there? How should you present your proposal, and to whom? In what time frame and in what form?

Finally, consider how a commitment from the other side should look and what the next steps should be after you've presented your proposal.

### 9. Final summary 

The key message in this book:

**Every interaction is a negotiation. To get the best out of each and every negotiation and achieve your goals, you must be prepared, communicate clearly and effectively, and understand the standards and values of your negotiating partner.**

Actionable advice:

**Make emails more plastic.**

Emails are imperfect communication tools because they make forming a connection with another person tricky. So if you absolutely must communicate via email, make sure you make your message as close to a real interaction as possible. State upfront how you'd like the person to understand the email. For example, you might say you're being jovial, or say you hope they understand your frustration. Also, make sure you're not being overly negative. If you're replying to an email that upset you, wait and answer it an hour later, when you've cooled down.

**Suggested** **further** **reading:** ** _You Can Negotiate Anything_** **by Herb Cohen**

_You Can Negotiate Anything_ shows that negotiations occur in every walk of life and that it is vital to have the skills and understanding to deal with those situations. The book outlines the key factors affecting negotiation success, as well as ways of negotiating for win-win solutions.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Stuart Diamond

Negotiation expert Stuart Diamond is the president of Global Strategy Group. He holds a degree from Harvard Law School and a MBA from the Wharton School of the University of Pennsylvania. Diamond has worked with a number of executives from _Fortune 500_ companies, and his negotiation model has been used in training for U.S. Special Operations forces. He also holds a Pulitzer Prize for his earlier work in journalism.

