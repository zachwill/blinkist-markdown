---
id: 596b2882b238e1000601886d
slug: getting-naked-en
published_date: 2017-07-18T00:00:00.000+00:00
author: Patrick Lencioni
title: Getting Naked
subtitle: A Business Fable About Shedding The Three Fears That Sabotage Client Loyalty
main_color: 1F4B91
text_color: 1A3E78
---

# Getting Naked

_A Business Fable About Shedding The Three Fears That Sabotage Client Loyalty_

**Patrick Lencioni**

_Getting Naked_ (2010) is about vulnerability and the incredible power it holds. These blinks explain how to build trust with clients and overcome the three most common fears that prevent you from showing your true vulnerability.

---
### 1. What’s in it for me? Learn the power of vulnerability. 

Building trust with other people is an important skill in all walks of life. For people working as consultants, however, the ability to forge strong relationships is essential.

In these blinks, we examine the importance of making oneself vulnerable in order to create a lasting working connection with clients. The blinks will also show you how to overcome the three fears that prevent many consultants from becoming truly vulnerable and open.

In these blinks, you'll learn

  * why you should never hide the fact you're sweating;

  * why "stupid questions" are rarely stupid; and

  * why you shouldn't be afraid of doing your client's dirty work.

### 2. Vulnerability may be frowned upon, but it’s essential to embrace it. 

Most people hesitate to let their defenses down, and nobody _likes_ feeling vulnerable. But that discomfort runs contrary to human nature. In truth, the ability to feel vulnerable is essential to building deep and lasting relationships. Naturally, a commitment to relationships is key if you work with others, and it's especially important for consultants.

So what might that vulnerability look like in a work context?

Take sweating as an example. People are taught never to let others see their perspiration, but that's impossible. Worse still, when people notice that you're sweating _and_ trying to hide the fact, you lose credibility.

That's why, if you find yourself in a situation in which sweating is inevitable — say a meeting that's stressful and hot — it's best not to hide it. By being open about the fact, you'll relieve others who will feel comfortable showing that they're sweaty too.

From then on, you'll be treated like a member of the team, rather than an outsider trying to make a sale. This essential change will enable you to focus on your real job, which is to help, not to sell your opinion as a product.

So, vulnerability is important, but it's definitely not easy. After all, humans naturally fear appearing weak.

However, that's all the more reason to overcome the fear, because only by being undaunted by phobias can you embrace vulnerability. By learning how to stand "naked" in front of other people and bare your true, honest self, you'll discover transparency, credibility and dedication.

It sounds great, right? Well, next up you'll learn how to overcome your fears and make that confidence a reality.

### 3. Pay more attention to your clients and less attention to your business. 

As a consultant, it's only natural to fear any loss of business. The problem is that you can't show that fear. If you seem to care more about your own business than the needs of your clients, you'll only exacerbate the issue.

So, what's a better approach?

Tackle your fear of losing clients head on by putting their needs first. As you might have guessed, that's going to mean getting vulnerable. It'll be scary, but rest assured it'll pay off in the end.

For instance, you should never begin a relationship with a discussion of fees. It's smarter to just do your job and present the value you offer. Such a humble and self-confident style will be sure to impress any client.

And don't worry, they won't take advantage of you. When customers see you truly looking out for them, they'll be happy to pay and won't worry about micromanaging you. So as long as you focus on your consulting and the needs of your clients, you'll have nothing to fear.

But just to be safe, there are two other rules to follow. First, it's important to be honest, yet kind with your clients. If they have a business idea that just doesn't convince you, let them know, but respect their feelings. Recognize the work they've done and communicate how much you care.

Second, keep in mind that it's better to face danger than to put it off or ignore it. Just consider discussing a risky business move, like the introduction of a new product. There are always going to be people who only see the danger of such a choice. Often, these people are ignored, and the discussion continues as if they'd never opened their mouths.

The problem here is a fear of confrontation. But if you can attend to the people who are afraid of launching a new product,you'll please everybody with your ability to acknowledge reality and handle conflict. You need to overcome your fear if you want to be able to face risks!

### 4. There’s no such thing as a dumb question or suggestion, as long as you know how to handle it. 

Imagine a scenario in which you have zero medical knowledge but are consulting for a hospital. Every third word you hear sounds like gibberish, and you don't know what to do. Is it better to pretend you understand everything or to embarrass yourself by letting your ignorance show?

Well, whatever you do, it's essential to be prepared to ask dumb questions. After all, there will always be others in the room who don't understand and, even in a hospital, not everybody knows every medical term. Not just that, but your questions might not be as dumb as you imagine.

In fact, when all is said and done, your clients will forget the "stupid" questions you asked and remember the gems. Every question keeps the discussion going and is, therefore, valuable.

So, you shouldn't fear dumb questions, and the same goes for dumb suggestions. Feel free to shoot out ideas and celebrate the mistakes you make.

Just like dumb questions, suggestions that you consider stupid likely aren't stupid at all. Others are probably thinking about them too, and people will only remember the good suggestions you make. For example, suggesting that a company work with a competitor that operates in a different market might sound insane, but it could also offer a great path toward growth.

And finally, if your question or suggestion really is dumb, be comfortable celebrating the silliness. That means being OK with laughing at yourself.

Just take the hypothetical scenario at the hospital. Maybe you forgot that it's a non-profit. It's definitely a dumb mistake, but you'll earn the trust of the group by being able to acknowledge and celebrate it. You can apologize in a friendly way — say, by offering to get lunch for everyone. They'll see that the issues they're working to address are more important to you than saving face.

> _"Clients don't expect perfection from the service providers they hire, but they do expect honesty and transparency."_

### 5. No task is too small when it comes to serving your client. 

Most people want to be considered important and to be treated as though they're worth their salary. Therefore, it makes sense that you shouldn't take on petty tasks like setting out chairs or picking up coffee, right?

Well, actually, that fear of inferiority is a huge hindrance. It's not like the simple fear of embarrassment because overcoming it doesn't mean taking an intellectual risk. Instead, conquering this fear is about getting over your pride and your desire to be the center of attention. It's also about being ready to take a figurative bullet for your client.

Say your client poorly organized a conference and you need to interrupt every other speaker to keep things on schedule. You've got to fall on your sword and take responsibility for the hassle. Then, after the conference, you can tell your client the honest truth, which is that the conference wasn't well organized. Rest assured that she'll be grateful both for your honesty and for your loyalty.

However, doing the dirty work goes beyond taking the blame; you also need to honor the work your client does.

After all, if you fail to show enthusiasm and respect for your client's work, you'll never be able to muster the will to do the dirty work in the first place. For example, if you consider gambling and alcohol to be immoral, you'll never be able to effectively consult for a casino or a bar. That means you should strictly avoid such clients as the outcome will always be poor.

And finally, doing the dirty work is about downplaying your own role through an attitude of service. This is important since there will always be clients who need you to wait on them. If you can rise to the challenge, they'll see how helpful you are. Rather than viewing you as inferior, they'll see you as dedicated.

Just keep in mind, you're being paid to help. So, show your clients that you'll do whatever needs to be done and build a relationship on a sturdy base of trust.

### 6. Final summary 

The key message in this book:

**Vulnerability is not something to fear; it's actually an incredible strength. While fear prevents us from making ourselves vulnerable, by overcoming such trepidations we can succeed like never before. It's uncomfortable, but getting figuratively "naked" in front of others is well worth it.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Advantage_** **by Patrick M. Lencioni**

These blinks outline the key principles for building a healthy organization where all the employees pull together in the same direction following the same objectives. This enables organizations to achieve their full potential, while unhealthy competitors waste resources in internal squabbles.
---

### Patrick Lencioni

Patrick Lencioni is founder and president of The Table Group, helping organizations optimize teamwork and engagement. Besides consulting and public speaking, he's written bestsellers including _Five Dysfunctions of a Team._

_©_ Patrick Lencioni: Getting Naked copyright 2007, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

