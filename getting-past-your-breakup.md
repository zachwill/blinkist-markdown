---
id: 59e4a7a6b238e1000617b3f6
slug: getting-past-your-breakup-en
published_date: 2017-10-19T00:00:00.000+00:00
author: Susan J. Elliott
title: Getting Past Your Breakup
subtitle: How to Turn a Devastating Loss into the Best Thing That Ever Happened to You
main_color: C62842
text_color: C62842
---

# Getting Past Your Breakup

_How to Turn a Devastating Loss into the Best Thing That Ever Happened to You_

**Susan J. Elliott**

_Getting Past Your Breakup (2009)_ provides practical advice for coming to terms with the end of a romantic relationship. It emphasizes the importance of self-care and generating happiness for yourself so you can thrive in life — with or without a significant other.

---
### 1. What’s in it for me? Get a grip and get ready to grow. 

Most of us have been there — a relationship fizzles or implodes, and a heart is left in tatters. Maybe it was a mutual decision, an amicable parting, or maybe it was a betrayal that ended with vitriolic word-bombs exploding everywhere. Whatever form they take, breakups are never easy, and they can leave you feeling like you've been run over by a bus. A hole has opened up in your life where there was once cuddles and hand-holding.

Here's the good news — just like the darkness breaks into the dawn of a new day, a breakup is a time of change. But positive change can only happen if you deal with your breakup in a specific way. In these blinks, you'll learn

  * why you should put down the phone;

  * why you probably can't "just be friends" (at least for a while!); and

  * how setting boundaries can expand your world.

### 2. In order to grow after a breakup, make sure you take care of yourself and grieve. 

Think back to your first breakup. It was most likely a tortuous, heart-wrenching experience, right? But fear not — breakups can actually bring some positive things with them.

All in all, going through a breakup can be the perfect opportunity for personal growth. To achieve this, you have to look after yourself and properly mourn your old relationship.

When it comes to addressing a breakup, there are three distinct ways to handle it. First, there's the option of investing a lot of emotional energy into trying to win your ex back. Second, there's simply pretending that nothing's happened by continuing with your life as normal. Third, there's taking the time to reflect on your past relationship and your life in general to deal with your emotions so that you can fully heal. The best solution is the last one, but it's by far the hardest. Here are some initial guiding principles to get you started.

From the very beginning, you need to put yourself first. This can prove difficult, especially if you're used to catering to your partner's needs. The trick is to treat yourself lovingly; boost your self-esteem by getting a new haircut or an expensive manicure. You could also take on a new hobby or work on expanding your circle of friends.

Most importantly, don't try to block out the pain by engaging in behaviors like drug taking or substituting your ex with someone new. Instead, stay in touch with your emotions and work to process through them. Dealing with your feelings in this way will lead to you becoming a stronger person; you'll trust in your ability to overcome pain and look after yourself.

Next, you'll learn more about the process of self-care and how to properly grieve — it all begins with putting an end to that toxic relationship.

### 3. Cut off all contact with your ex and don’t make excuses to communicate. 

You and your partner have officially broken up, yet you still spend an unhealthy amount of time waiting for them to message and routinely checking their Instagram page. This behavior is understandable, but it's not doing you any favors.

To orchestrate a successful breakup, you need to cut all lines of communication completely. That way, your ex will have no influence on your life; you'll be able to eradicate them from your mind and steadily move on.

It'll be a challenge to start off with, because you'll be accustomed to communicating with that person, so the absence of that interaction will inevitably leave a void. Don't despair, though, as most people reap the benefits of this procedure in no time.

The author had a client who felt as if she couldn't breathe without her partner. After a mere 48 hours with no contact, she discovered she could breathe easier _without_ him.

However, eliminating contact is only the first part of the battle; the second part is refusing to make excuses to contact your ex. This may prove problematic if your ex-partner wants to continue as friends — you'll have to just explicitly say no. If you both invested as much energy into the relationship as you could, and still found that it wasn't working out, then the best course of action is to take time apart to start afresh.

Social media also makes the concept of ending all contact a particularly trying task. You have to fully commit to not contacting your ex by any means — don't dupe yourself into believing that following their posts on Facebook is inconsequential.

It's essential to stop communicating with your ex-lover if you want to move on to the next step. This is the process of grieving, which you'll learn about in the upcoming blink.

### 4. Losing a relationship can come as a shock, and there are strong emotions to deal with as you grieve. 

Strength is seen as a desirable quality, so most people usually choose to ignore feelings of pain and confusion after a breakup. However, this isn't going to negate suffering. You need to grieve your relationship so you can learn how to process disappointment.

First, you have to deal with the feeling of shock which comes with losing a relationship. When you're with someone, you feel safe; hearing another person say they love you every day brings security. Suddenly, that person is out of your life without much explanation.

Those feelings of trust and safety quickly disappear, as well as your plans for a future together. You realize that you invested time and positivity into a relationship that wasn't right — this is difficult to comprehend, so you're bound to spend a while in disbelief. It's normal to initially feel this way, though; eventually, your emotions will surface.

Throughout the grieving process, you'll have to deal with a number of complex emotions, including regret. Once the feeling of shock fades, you're likely to be hit with a sense of abandonment and loneliness. This is to be expected — you've lost the affection and companionship of someone you hold dear. You may also have to deal with the loss of mutual friends and your ex's family members, to whom you'd grown close.

These unhappy emotions will come in waves. It's almost like having a bad cold — you feel ill, dizzy and confused. This experience is mixed with obsessive thoughts like What went wrong? Who's to blame? It's both natural and healthy to go through this, so don't suppress these thoughts; ruminating over the past is your brain's way of moving forward. This can all be exhausting, so know your limits and look after yourself — more on this in the next blink.

### 5. To help you take care of yourself, employ these simple methods. 

As you've learned, the grieving process can involve spending a lot of time thinking about your past relationship. The key is not to dwell on this for too long — remember to shift your focus back to your own life.

It's testing to go through the grieving process to get over your relationship. That's why it's so important to balance this experience with positive actions.

Think about where you'd like your own life to go. How was your previous relationship holding you back? In what ways would you like to exercise your newly acquired freedom?

Now that you're no longer with your ex-partner, you may find that you've got a lot more time to spend on educating yourself and developing your career. You may even want to use your newfound freedom to meet up with friends or take the daring step of moving to your dream city.

There are some helpful tools for transitioning into this positive way of thinking. Start keeping a journal or schedule "date nights" with yourself to take care of your well-being.

Documenting your feelings in a journal will help you identify how to make your life happier and address all the thoughts that are impacting you negatively. Say you catch yourself thinking that you're undateable — stop, write the thought down, and think of a positive affirmation to counteract it. "I'm a supportive partner with a great deal to offer," is wonderfully specific. When it comes to date nights with yourself, designate time for doing activities you enjoy: read a book, take a relaxing bath, or head to the movies.

Now that you understand how to handle yourself after a breakup, you'll learn how to think about other people who might be affected, such as your children.

### 6. To help your family cope with the breakup, stay organized and talk to your children. 

As most people know, a divorce can have a devastating effect on the kids involved. If your breakup concerns children, help them process the situation by communicating with them and being patient.

Sit down with your kids and explain that as parents, both you and your partner will continue to love them unconditionally. If they ask any questions, refrain from being too specific — your kids don't need to know details. Instead, keep your answers vague; explain that you're no longer happy together and don't want to fight, as it's not pleasant.

If your children are of an age where they can fully comprehend the situation, you can use this opportunity to illustrate the importance of making changes when a predicament has become unhealthy.

It's vital to let your children react to the news as they please. If they want to sulk or throw a tantrum, let them. They also need time to grieve and sometimes don't know the best way to express their emotions.

Organization is essential for maintaining a healthy family life throughout your breakup. Remember to engage in fun family activities and allow your children to partake in the planning; this will create an environment of stability and security for them.

Breakups can be exceedingly painful, but they can also provide an opportunity to find fresh ways to function as a family. Ask your children how they would like to organize living together in this newfound setup.

It's also important to schedule activities that you can enjoy with your children and without your ex-partner. Reward them if they're actively contributing to making this new family life a success.

You should now have the tools to help your children through the process, so it's time to return to focusing on yourself.

### 7. Make relationship inventories to discover harmful patterns in your relationships. 

Presently, you should fully understand how to process a breakup, so it's time to think about your relationships in general.

A _relationship inventory_ will help you discover unhealthy patterns. Start by noting down all the positive and negative aspects of your relationship and that of your ex-partner.

Think back to the attributes which initially attracted you to your partner but ended up ultimately proving to be detrimental. You might've marveled at how organized your partner was, but that soon turned into your partner being both highly controlling and overly critical. Were there any warning signs which surfaced at the beginning of your relationship signaling that there'd be trouble in the future?

It's essential to write down the elements of the relationship that hurt you most and any wrongdoings that you were responsible for as well. Soon, you'll be able to identify themes in your relationship: was your partner incredibly distant while you were needy? Did they frequently interrupt you?

Next, make a life inventory which features all of your major romantic relationships so that you can discover the source of these harmful patterns. It'd prove beneficial to you to make a similar inventory which assesses every close relationship in your life.

If your ex was critical of you, you might notice that your mother was once critical of you in much the same way. You may have spent your childhood pointlessly striving for your mother's approval, and as an adult, you've simply transferred that pattern. You're immediately attracted to demanding, critical partners who you have to continually appease.

The sooner you're able to identify the source of these harmful patterns and understand how they're affecting your life today, the easier it'll be for you to let them go. In the following blink, you'll learn exactly how to do this.

### 8. Identify the situations in which you struggle to set boundaries and learn how to stand your ground. 

Think back to your past relationship. In situations where you and your partner didn't see eye to eye, were you reluctant to say no or disagree? Unfortunately, allowing yourself to be treated like a doormat is pretty common.

Now, consider the current relationships in your life — are there any boundaries that need to be set? To do this, you need to ascertain when and with whom you're failing to make sure that your interests are heard.

Say a friend always wants you to go shopping with her on Saturdays. This is not an activity you enjoy, and it's preventing you from satisfying your own needs. Therefore, this is the prime opportunity to stand up for yourself and just say no, without lying or making excuses.

It's essential that you know how to set boundaries effectively. Remember that a new boundary will only be accepted if there are consequences for not meeting your requirements, as words often fall on deaf ears.

Imagine you have a friend who's always late when you've made plans together. You've frequently tried to set a boundary by saying that this is not OK, but they've neglected to change their behavior. You're not required to accept this, so explain to your friend that you're no longer going to wait. If you've planned to drive to a party with your friend and they don't show up at the allotted time, head to the event without them. This will explicitly show your friend that you're committed to your interests and that they need to respect them if they want to be around you.

This blink has hopefully taught you how to be more forthright with your own needs. In the next blink, you'll learn about the importance of retaining your independence, especially when you're in a new relationship.

### 9. Learn to be independent, and if you do find love, don’t sacrifice your passions. 

After a breakup, it's normal to start thinking about when you'll be ready to return to dating. The answer to this question may seem a little counterintuitive: you'll be prepared to get back into dating when you're able to enjoy yourself and no longer feel that you need others to make you happy.

When you're feeling strong, joyous and independent, you're more likely to attract the right person than when you're in a needy and insecure state. If you're in a place where you're dependent on a relationship to make you feel socially validated, then you're likely to settle for much less — you'll end up with someone who can potentially have a damaging effect on you.

The best course of action is to embrace the loneliness — that way, you can freely choose who you want to associate yourself with next. But when you do find love again, don't completely abandon the attitude you had while single.

It's easy to get wrapped up in a new romance, but remember to maintain your own life, too. Continue to meet up with friends and partake in the activities you discovered when you were on your own. It's important to keep spending time alone as well, as thriving on your own is such a vital, hard-won skill. A relationship is also destined to become boring if you spend every waking moment together; solitary time is good for you and your relationship in general.

Essentially, learn to indulge in self-care and truly get to know yourself. Experiencing a breakup is tough, but if you approach it the correct way, it can be an incredible chance to grow. Whether you get into a new relationship soon afterward or not, you'll find that you're a stronger, more independent person because of what you've been through.

### 10. Final summary 

The key message in this book:

**In the long run, going through a breakup can be highly beneficial to you. Instead of ignoring the situation or addressing it in a self-destructive manner, use it as an opportunity to take a good look at your life. Learn how to be happy with yourself first before embarking on a new romance.**

Actionable advice

**Find a hobby that you enjoy.**

If you're at a loss after a breakup, it's a sensible idea to preoccupy yourself with a simple activity that won't be too much of a challenge for you. Consider taking up knitting or learning about woodwork to take your mind off the situation and get past the shock.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _When Things Fall Apart_** **by Pema Chödrön**

_When Things Fall Apart_ (1997) is a guide to dealing with the biggest challenges life throws at you. These blinks explore a range of concepts and strategies, from meditation to self-compassion to breathing techniques, that'll help you develop resilience in the face of adversity and a deeper appreciation for living in the moment.
---

### Susan J. Elliott

Susan J. Elliott is a New York-based attorney and grief counselor. She's reached millions with her seminars and runs a highly popular blog entitled Getting Past Your Past.

