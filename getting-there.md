---
id: 568b0535de78c00007000038
slug: getting-there-en
published_date: 2016-01-07T00:00:00.000+00:00
author: Gillian Zoe Segal
title: Getting There
subtitle: A Book of Mentors
main_color: F36533
text_color: A64523
---

# Getting There

_A Book of Mentors_

**Gillian Zoe Segal**

_Getting There_ (2015) provides inspiring portraits of successful entrepreneurs, artists, writers and CEOs who through hard work and perseverance pursued their true passions against all odds. This collection of first-person stories aims to show how you too can "get there" and live your dreams.

---
### 1. What’s in it for me? A mentor can inspire you and push you to pursue your dreams. 

There are so many people in life who inspire us — teachers, managers, artists, even historical figures. Individuals such as these are called mentors, and their advice and experience can be invaluable. 

Yet while not everyone is lucky enough to find a mentor to personally guide them, we can seek out the stories of the lives of others to help us in our own quests. 

In these blinks, you'll meet six successful people and learn how they made their way in the world. Each has walked along a different path and conquered individual challenges. 

Yet the thing that connects all these stories is the inspiration behind each experience — and it's this inspiration that can be your "mentor" as you explore your own road to success. 

In these blinks, you'll discover

  * how the values of a soldier can shape a successful artist;

  * how being bad at communicating inspired the creation of Craigslist; and

  * how Teach for America found its start with one determined undergraduate.

### 2. The child of a successful engineer follows his own path to become dean of Harvard Business School. 

Did you look up to your parents as a child? Many of us did and still do. It's not uncommon for people to even follow in the footsteps of their parents when it comes to a career. 

That's exactly what Harvard Business School dean Nitin Nohria set out to do. 

Nohria had a great role model in his father. Born in a humble village in India where few had access to education, Nohria's father was one of the first to graduate from high school. He then studied abroad, and once back in India, built upon his engineering degree to become the CEO of a major company. 

Hoping to make his father proud, Nohria too pursued a degree in engineering, yet struggled with poor grades at school. The reason for this was a passion for literature that consumed most of his time. 

Nohria's grades in his university science classes continued to be average, but he excelled in elective courses — history, literature and economics. 

This got him thinking that his path perhaps didn't lie with engineering after all. So he decided to try his luck in a new direction. Excitingly, he received a fellowship to study finance at the Massachusetts Institute of Technology (MIT). Nohria then packed up and moved to the United States. 

Once at MIT, Nohria unsurprisingly excelled in his humanities classes. Inspired by his successes, he switched to a degree in management, ultimately completing a doctorate in the subject. 

The crowning moment of all his hard work came when he was offered the position of dean at the Harvard Business School. 

Nohria believes he's lived the American dream. Like his father before him, he never stopped working to create new opportunities. And though he might not have followed his father's exact path, Nohria is glad to have had such a mentor in his life. 

Unfortunately, not all parents inspire their children in the same way. However, this doesn't mean that these children can't achieve great things on their own.

### 3. Performance artist Marina Abramović channeled a troubled childhood into emotionally charged art. 

Unloving, controlling or absent parents don't just lead to a troubled childhood, but are almost always at the root of psychological difficulties in adulthood. 

Nevertheless, hardships people experience when children can motivate later on in life. Performance artist Marina Abramović is an incredible example of this.

Abramović was born in Belgrade, Serbia, to parents active in paramilitary groups. They were frequently absent; yet when they were home, control and order were their preferred parenting methods. 

Abramović's mother was a germophobe, and was extremely strict with her daughter. She would shake her awake if Abramović was sleeping crookedly in bed, and she also forbade Abramović to play with other children. 

Abramović spent most of her early years alone, but could entertain herself, learning how to draw and paint at age three. At just 12 years old, Abramović held her first art show. 

She continued to pursue art, graduating from art academies in Belgrade and in Zagreb, Croatia. It was during this time when she made a huge discovery about her relationship with art. 

Abramović one day saw high-speed planes fly above her, leaving contrail lines in the blue sky. These streaks fascinated Abramović, bold drawings that appeared only to quickly disappear. She realized that art could appear from anything in the world, even from her own body. These ideas formed the core of Abramović's long, illustrious career as a performance artist.

Yet when Abramović started out, performance art wasn't considered a legitimate art form. She was subject to harsh criticism, even ridicule. But she continued to work, creating performances where she pushed her body and mind to the limit. Today she is a worldwide success.

> _"If you don't get love from your family, you turn to other things to get it. I get the love I need from my audience."_

### 4. Craig Newmark’s experiences as a nerdy outsider led him to foster community building online. 

Teenage nerds have it tough. Fascinated by electronics yet burdened with awkward social skills, many a clever yet geeky young adult often feels excluded and alone. 

Yet plenty of nerds have made it big in the modern world. Some, like Craig Newmark, got their break by combining technology with something they were never very good at — communication. 

Newmark was a self-described "computer nerd" growing up. Social events were to be feared. Although a nice guy underneath, he didn't have many friends, and this baffled him. Yet one day during a class discussion, he realized what had been holding him back: poor communication skills.

Newmark knew that while he'd missed out on years of socializing in high school, all was not lost. The internet — a fairly new technology in 1995 — could give him a second chance to connect with people. 

He noticed that the internet was particularly useful for connecting people and letting people help each other. Newmark decided to take the opportunity to create something online that made others feel involved and included. 

So he started by simply sending emails to friends with tips about upcoming art and technology events. People got word of Newmark's list, and asked if they could sign up. Soon list members suggested he also include information about job openings, services and even sales ads. 

Not six months later, Newmark had an email with 240 listings, way too many to include in a single email. So he decided to start his own list server, named _Craig's List_. In 1997, Craig's List was getting one million page views a month. Just two years later the list became a company, called Craigslist.

Newmark still runs Craigslist according to his original motto, "Give people a break." His goal is to develop a sense of trust and belonging in urban communities across the world, and help people connect with other people to solve everyday problems.

### 5. Wendy Kopp proved that being idealistic doesn’t necessarily mean being unrealistic. 

How can you judge if you're just ambitious or downright crazy? Although people may tell you that your goals are unreasonable, only you and your gut can say if you're on the right track. Wendy Kopp is one leader who believes that if you feel something is worth doing, _you do it_. 

It all started during Kopp's freshman year at Princeton University. Eager to make a difference, she joined a student organization, and together with other members approached the CEO of a major St. Louis-based company in an effort to raise funds. 

After the group's presentation, the CEO gestured to the local, poor neighborhood visible from his window. He asked the students why he should support their goals when others had far more urgent needs. These words resonated with Kopp.

Kopp then decided she would dedicate herself to a greater cause: equal education. She devised a project to improve public education by recruiting and training America's top graduates to become teachers in low-income school districts.

She estimated the project's budget at $2.5 million, a sum her professors balked at. They insisted that she was too idealistic and there was no way she could raise the needed funds. Kopp didn't give up and put her plan into action. 

To raise money Kopp sent hundreds of letters to CEOs and business leaders. The few that responded simply told her that she was aiming too high. But this didn't deter her. 

Kopp began to recruit graduates directly. After getting more than 2,500 interested students, the media picked up on her project. Once the story went public, donors got in touch. Kopp quickly raised the funds she needed — even a $500,000 grant from former presidential candidate Ross Perot.

Kopp's program, Teach For America, commenced its training phase with 489 teachers. By moving past rejection, Kopp was able to keep her project moving until it finally got off the ground.

> _"You don't need everyone; you only need a few people who really believe in you and your ideas."_

### 6. Spanx’s Sara Blakely learned from her past failures to discover her calling purely by chance. 

Do you remember what you wanted to be when you grew up? Was it perhaps a doctor, a ballerina or a fireman? Many people are still holding on to childhood dreams — and that's okay! Yet sometimes destiny has something even more special in store. 

All her life, Sara Blakely wanted to become a lawyer. She'd seen her father work in court as a child, and was determined to be just like him. But when it came to law school admission tests, Blakely failed not once — but twice. 

Devastated, Blakely gave up on her dream, instead taking a job selling fax machines door-to-door, often to rather unwilling customers. 

While not necessarily a fulfilling profession, this job gave Blakely one thing — an immunity to the word "no." Customers would tear up her business card in her face or even call the police to escort her from the building, and still Blakely wouldn't take no for an answer. 

Yet Blakely wished that she was selling a product she actually cared about. Perhaps she could come up with one herself. But what would that product be? 

While getting dressed to go to a party, she cut off the feet from a pair of pantyhose so she could wear them with sandals. Seeing how flattering the hose made her under her outfit, Blakely realized that this was a product she could develop and sell: comfortable, invisible shapewear. 

She researched, patented and networked in secret for a year. Many local producers refused to make a prototype for her, but Blakely was so used to rejection that nothing could stop her. One factory owner however thought how his three daughters might like this product, and decided to give Blakely a chance. This was her break!

Blakely's Spanx has become a cult item, referenced on popular shows like _Sex and the City_ and _The Oprah Winfrey Show_. Just imagine if she'd been admitted to law school — she'd never have become an entrepreneur or one of America's youngest female billionaires.

> _"I believe that defeat is life's way of nudging you and letting you know you're off course."_

### 7. John Paul DeJoria experienced astonishing success but never forgot where he came from. 

Can you imagine a business leader that takes showers at public tennis courts and only has $2.50 to spend each day? Sounds unbelievable, but that's really how tough starting a new business can be. Luxury brand owner John Paul DeJoria can attest to this. 

If there was anything that DeJoria's mother taught him, it was that success has nothing to do with money. Rather, true success stems from working hard and being happy. 

There was a time in DeJoria's childhood when just a few dimes were all that he, his brother and his mother had to their name!

Despite the adversity they faced, the family knew they were happy. They had food to eat and a small garden. His childhood was the foundation of DeJoria's approach to business. He started from the bottom, working all manner of odd jobs.

But whether he was making flower boxes, driving tow trucks or selling encyclopedias door-to-door, there was one thing DeJoria always strove to do: help his customers make the right decision. This principle helped DeJoria gain more responsibility in management roles. 

After working his way through different jobs, DeJoria joined forces with his friend Paul Mitchell to create one of America's biggest professional hair-care companies, John Paul Mitchell Systems. 

Yet even in the early days, DeJoria had to live out of his car! But he and Mitchell never lost faith in their enterprise, and eventually it paid off. With the success of John Paul Mitchell Systems under his belt, DeJoria has gone on to found many other businesses. 

His brand Patrón is a worldwide tequila brand, frequently name-dropped in pop music; DeJoria is also active in the diamond trade. Yet regardless of venture or field, DeJoria has always stood firmly by his principles of working hard and helping customers to make the best choice.

### 8. Final summary 

The key message in this book:

**The path to a successful venture is a lifelong journey, as stories of influential mentors show. Whether their childhood is full of inspiration or adversity, mentors learn from their experiences, discovering their true values and skills that would serve them well down the line.**

Actionable advice: 

**Speak up!**

The next time you're in a meeting and find that people aren't paying attention to you, consider how your voice sounds. Is your tone nervous, sarcastic or pessimistic? If so, it's no surprise that people would rather tune you out. Try to convey optimism and enthusiasm, and watch as you shape the discussion. 

**Suggested** **further** **reading:** ** _The Creator's Code_** **by Amy Wilkinson**

_The Creator's Code_ (2015) unlocks the essential skills needed to take a big idea and turn it into a successful company. Drawn from 200 interviews with dozens of leading entrepreneurs, including the founders of LinkedIn, eBay, Tesla Motors, Airbnb, PayPal, JetBlue and others, this book will help you better see what areas you need to address to turn your passion into tomorrow's market leader.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Gillian Zoe Segal

Gillian Zoe Segal started a career in law before pursuing her true passion of photography. She wrote _Getting There_ to showcase stories of people who took risks and worked hard to live their dreams. Her writing has appeared in _Forbes_ and the _New York Times_.

