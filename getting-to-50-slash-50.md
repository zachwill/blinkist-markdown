---
id: 56c05c3d4f33cc0007000000
slug: getting-to-50-slash-50-en
published_date: 2016-02-16T00:00:00.000+00:00
author: Sharon Meers and Joanna Strober
title: Getting to 50/50
subtitle: How Working Parents Can Have it All
main_color: 6680BB
text_color: 415278
---

# Getting to 50/50

_How Working Parents Can Have it All_

**Sharon Meers and Joanna Strober**

_Getting to 50/50_ (2009) exposes the myths surrounding traditional male and female parental roles and provides actionable techniques that allow both mothers and fathers to be independent earners, enjoy quality time with their children and share responsibilities in the household.

---
### 1. What’s in it for me? Become equals, become happy! 

Here's a question for the fathers out there: when was the last time you took out the trash without being asked to? How often did you offer to look after the kids, to give your partner a break?

Hopefully that's not too hard to answer. 

Nevertheless, men and women still don't enjoy equal rights — neither at home nor at work. But we hardly hear anything about what steps can be taken in order to enjoy the benefits of a _50/50 life_. A 50/50 life is a life in which partners equally share the responsibilities of childcare, work life and each other.

What you'll find out is that an equal division of tasks is totally possible and, actually, it's the best way to have happy and healthy children, maintain physical and mental health and enjoy a healthy relationship (including sex life!). 

In these blinks, you'll discover

  * three myths that prevent equality among men and women;

  * that being a stay-at-home mom isn't all that healthy; and

  * what research reveals about the attention we give our children.

### 2. Childcare doesn’t hurt your family – it can help you spend more quality time with your children. 

Remember Julie Andrews in Mary Poppins, flying into the Banks' home, making everything prim, proper and perfect? All parents wish they could have that ability, but more than ever, families are struggling to find the best way to care for their home and children. Many rely on childcare, and fear accusations that they're lazy or bad parents.

Some parents even believe that childcare can hurt their children's development. But contrary to what you may have been told, childcare on a part-time basis does no harm to the well-being of your children.

In fact, 15 years of research carried out by the National Institute of Child Health and Human Development revealed no difference in emotional well-being between children who spent all their time under their parent's supervision, and children who spent part of their time in childcare. It's the proportion of time that matters. So if you overdo it with childcare, it may have unfavorable effects on your children, such as an increase in tantrums. 

But seeking out extra help should never make you feel like an inattentive parent. Actually, today's children generally aren't attention-deprived. Sociologist Suzanne Bianchi from the University of Maryland found that mothers and fathers spent more time with their children in 2000 than they did in 1965.

Also, don't forget that the quality of your parenting time outweighs the quantity of it.

Mothers who stay at home don't spend much more time interacting actively with their children than mothers who work outside of the home. A Texas University study showed that mothers who worked outside the home shared only 20 percent less social activity with their children than stay-at-home mothers. 

Stay-at-home mothers sometimes don't appreciate the extra time they have with their children, whereas working mothers tend to cherish the bedtime stories and bathtime they can share.

### 3. Both partners enjoy many benefits when the wife works outside the home. 

Some men fantasize about coming home from work to find their wife in a sexy outfit, dishing up a home-cooked meal. But really, husbands and wives are happier and more interested in one another when they both work outside the home. They also have stronger relationships and more sex.

Studies show that after the birth of a child, husbands and wives tend to disengage and gravitate away from each other if the mother stays home and the father goes to work. This is because they lead entirely different lives and don't have much in common. 

But when they share responsibility — like if the father helps with household chores and the mother works outside the home — partners are more attracted to one another.

A 2006 survey found that the more household chores the husband was willing to assume, the better his sex life was with his wife. And the more hours a wife worked outside the home, the more sex she had with her husband. 

What's more, sharing the household duties and the breadwinning can protect a marriage from divorce. 

One 2006 study by sociologist Lynn Prince Cooke showed that couples who shared household and income-earning duties 50/50 reduced their risk of divorce by 50 percent compared to the average divorce risk rate. On the contrary, for more traditional families where only the man worked outside the home, the risk of divorce climbed to 13 percent _above_ average. 

In addition to lowering the risk of divorce, two working partners also means you can enjoy the benefits of earning two incomes. If a family relies on the husband's income alone, then he can't afford to lose it — even if he hates his work. Having two incomes eases the pressure and offers both partners a chance to find work they really love.

### 4. Working women are healthier and wealthier. 

As a woman, does the idea of staying at home and curling up with a good book while your husband goes out to work seem like a dream setup? Not so fast. If you believe there's nothing better than being a stay-at-home mom, here are a few points to consider:

For starters, women who work are independent.

Stay-at-home moms told the authors that their husbands set budgets for them and supervised their expenditures. Of course, that seriously minimizes independence. Working to earn your own money is a powerful thing, especially with regards to your own specific needs and desires; needs and desires that your husband might not understand your spending money on!

More importantly, though, working and earning your own money is the only path to being financially independent.

This is especially important for widows. As a 2004 study at the Boston Center for Retirement Research found, women face a 50 percent drop in their living standards after the death of their husband. 

In addition to being financially more secure, women who work are both physically and psychologically healthier.

One UK research study spanning 50 years revealed that stay-at-home moms were the most likely women to experience poor physical health. Conversely, women who combined the three roles of wife, mother and worker enjoyed the best physical health. 

Furthermore, a 1989 study found a 30 percent increase in psychological issues such as depression in women who concentrated only on their home, and not, for example, on work.

So it seems that, in general, women who participate more equally in their family's responsibilities are better off — especially when it comes to working outside the home. And yet there's still disproportionately few of them in offices. Let's take a look at three myths that are blocking women's progress.

### 5. Myth #1: Mothers are incompetent and want to work less after they have children. 

If you're an expectant mother or have recently had a baby, you may have been confronted with some aggravating comments along the lines of "Well, say goodbye to all your big career plans."

Sadly, when it comes to work, people don't have a lot of confidence in mothers.

A Princeton study in 2004 showed that mothers were generally perceived to be very _nice_, but not very competent. In the study, students had to rate fictional characters as potential clients for a consultancy firm. Among the characters were Kate and Dan, identical character profiles with the exception of their gender. They were both described as new parents working from home. So how did the students rank them? Kate was evaluated as the least competent of _all_ the candidates, while Dan was ranked as one of the best. 

Why? One reason could be that mothers don't challenge the assumption that they must want to stop working once they start a family.

When women have children, employers often presuppose that new mothers want to cut down to part-time work. As a result, they receive less responsibility, lower status and reduced pay. 

Often, instead of confronting their employer, new mothers jump ship and try to get a better job where they can continue to do work they enjoy. But the problem is that by jumping ship, they never confront their boss, and the myth endures.

So what can you do as a mother? Well, before changing jobs, you can stand up for yourself.

One woman — a political science professor — didn't receive parental leave after her first child was born. Later, upon researching the university policy, she found out that she was indeed entitled to parental leave. So when she became pregnant with her second child, she requested leave not only for the current pregnancy, but retroactively for the first. 

Because of her assertiveness, she was granted all the leave she was owed.

### 6. Myth #2: In order to be successful, you need to work around the clock. 

You're having a heart attack and need to be operated on immediately. A respected surgeon is available, but she's been working for 16 hours without a break. Would you put your life in the hands of someone so fatigued and overworked? 

Probably not, and you'd be right not to! Overworking can be dangerous.

Studies carried out on truck drivers and nuclear plant workers revealed that shifts over six to eight hours lead to more mistakes — and can be extremely unsafe. Studies have also shown that hospital interns working 30-hour shifts were _six times_ more likely to deliver a false diagnosis, and made 60 percent more medical mistakes than interns who worked more manageable shifts. 

It's not only safer for employees to avoid overworking, it's also good for a company's bottom line.

Electronics retail company Best Buy lost many of its employees due to their grueling working hours. To remedy the situation, they implemented a program where employees could set their own hours instead of adhering to a fixed schedule. This reduced employee turnover _and_ boosted productivity. 

So how does this all relate to working parents? 

Mothers can eradicate this myth that only the people who work insane hours are valuable employees. A mother can convince her boss to focus on achievements, not on working hours. 

One female employee at a security firm asked to work part-time for four days a week. Her boss initially refused, but she vouched that she would make as much money for the company as she had when she was working full time, so they agreed to let her try. Eventually, her boss had to admit that she was outperforming her peers, even though she was working fewer hours than they were.

### 7. Myth #3: Women and men are treated equally in the workplace. 

Some of us observe more traditional couples and wonder: why is she cooking for him all the time? Why is he taking her housework for granted? Unfortunately, sexism and gender stereotyping die hard — and it's the same at work.

Inequality in the office is alive and well, but thankfully, some progress has been made.

For the most part, company structures are male-dominated, and when a manager is hiring, he typically looks for someone he has a lot in common with — as in another man. So in order for a hiring manager to hire a woman, she needs to be not only equally competent, but _superior_ to a male competitor.

When the University of Michigan recognized that despite having many female Ph.D. candidates, almost none were moving on to secure a faculty job after finishing their dissertation, they resolved to make a change. They tutored their hiring panels. They showed studies demonstrating that women had to be superior to male candidates to be considered for employment. 

Consequently, women in faculty positions increased from 14 percent to 30 percent. 

The best part is, hiring more women isn't just a short-term solution. Once there are enough women in a position of influence, the system will self-regulate.

Still, inequality is sometimes reinforced by a tendency of women not to stand up for themselves.

Take professional tennis, for example. Since 2006, tennis players at the U.S. Open have been able to challenge an umpire's decision a limited number of times per match. Some 30 percent of the challenges are successful. But the women have challenged umpire decisions only _half_ as often as the men!

Simply put: not speaking up harms women professionally. 

In the last three blinks, we'll look at ways in which women and men can get closer to a 50/50 solution.

### 8. Collaboration between partners is crucial to a fair partnership. 

Picture a happy couple from the 1950s. The husband comes home from work, his wife welcomes him with a home-cooked meal, and later they probably watch hours of TV. 

Today, the scene is different. The husband gets home after 9 pm, exhausted, and goes straight to bed, too exhausted to hold a conversation with his partner. 

Not surprisingly, neither wife nor husband wants this kind of life.

Michael Elliott, the author of _Men Want Change Too_, says he also wanted the 50/50 dream of sharing responsibility with his partner. But after their children were born, his wife gave up her career and took on part-time work to free up time to care for the children. In turn, Elliott worked more intensively, first because he enjoyed it, but then because they needed the money. 

So how do couples avoid taking a wrong turn? To avoid falling into traditional gender traps, both partners have to think ahead and stay adaptable.

But this takes a bit of work. Take Sharon and Steve, a young couple who sought counselling to deal with their relationship. Steve argued that women were naturally more nurturing and should therefore be the primary caregivers during the early years of a baby's life; Sharon wanted both of them to be equal caregivers. Through the counseling, they agreed that what appeared to be natural wasn't always the answer, and that an equal distribution of responsibilities would be healthier for their relationship. 

They also worked through other potential problems, such as what would happen if one of them began earning considerably more than the other. They established that they would both still take on half of the housework. 

Thinking ahead in this way made it much easier to live 50/50, because it heightened their awareness of traditional roles that can so easily creep up on a relationship — and threaten to tear it apart.

### 9. Fight for your maternity leave and make sure you have a job to come back to. 

Consider how aristocratic women of the past dealt with their children: no sooner had they given birth than the infant was handed over to the nanny so the lady of the house could return to her social obligations. 

New mothers these days aren't desperate for a full-time nanny from day one so that they can socialize, but they could certainly benefit from some support from the state. 

Incredibly, maternity leave is still not guaranteed for all working mothers. Even in the US, it's not mandatory to give new mothers paid leave. Knowing that you won't be able to take sufficient time off work to recover from childbirth and care for the newborn — that's a strong incentive to quit your job!

Thankfully, though, society is making headway. In 2004, California granted women paid leave for up to 55 percent of their wages for six weeks after giving birth. Although it leaves much to be improved, California at least recognizes that women are an integral part of the workforce and that paid maternity leave is necessary to ensure women remain in their jobs. 

If you're an expectant mother, it helps to make it clear you're coming back to work before you take your leave.

Male superiors are often unfamiliar with motherhood and may be concerned if mothers display uncertainty about their future plans. So have a plan before talking to your boss about your pregnancy. Make sure they hear about it directly from you, and do your homework on maternity leave. You should know what you're entitled to — even if they don't. 

  

Most importantly, make it clear that you plan to return to work as soon as possible. You can even let your boss know what date you can be expected back in the office. That way, you'll reassure your boss. You'll be making it obvious that work really _is_ important to you, and that you're looking forward to coming back to the office!

### 10. To get to 50/50, both men and women need a lot of support. 

Getting to 50/50 means actively encouraging fathers. You know those overprotective moms who chastise their husbands, even when they're just trying to help? Don't be that person. Failing to encourage the father will only steer him away from engaging with his children.

You can support your husband by acknowledging the various ways of caring for your children. 

It might drive you crazy if your husband uses a different hold to comfort your screaming baby, for example. Or your blood might boil at the number of wipes he wastes when changing a diaper. But remember, each unsolicited piece of advice you offer can seriously diminish his motivation. 

Remember, too, that your way of doing things isn't the only or best way to do them.

One study in the 1960s found that fathers are just as good at caregiving as mothers. For instance, in the study, babies drank as much milk when the father held the bottle as the mother, and fathers were just as capable at figuring out what to do when their child was upset or needed burping. 

But men can also do their bit to support mothers. All men, for example, should be kinder to them when they return to the workplace.

While maternity leave plays a critical role in retaining female employees, showing empathy and patience when the mother returns to work will help her to get resituated. The women the authors talked to said that the first few months of juggling a job and a baby are the toughest, but if the boss is supportive during this time, it makes for a far smoother transition.

### 11. Final summary 

The key message in this book:

**We aren't forced to play out the traditional housewife-businessman roles of the generations before us. By planning ahead, talking through disagreements and divvying up responsibilities fairly, couples can enjoy a healthy, equal partnership.**

Actionable advice:

**Leave your husband alone with the baby.**

If you're a wife and mother, prove that your baby can survive without you by leaving it with your husband! This way, he can demonstrate his capabilities as a caregiver. If you don't give him a chance to develop a relationship with the baby, you're in no position to complain when you end up stuck with all the baby duties yourself. 

**Suggested further reading:** ** _Lean In_** **by Sheryl Sandberg**

Through a combination of entertaining anecdotes, solid data and practical advice, _Lean In_ examines the prevalence of and reasons for gender inequality both at home and at work. It encourages women to _lean into_ their careers by seizing opportunities and aspiring to leadership positions, as well calling on both men and women to acknowledge and remedy the current gender inequalities.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Sharon Meers and Joanna Strober

Sharon Meers is a former Managing Director at Goldman, Sachs & Co. and holds a managerial position at Ebay. Together with her husband, she founded the Parity Center at the Stanford School for Business, which supports research on gender equality in the home and at work.

Joanna Strober was a Managing Director of an investment firm, and is the CEO and co-founder of Kurbo, an online company working to prevent childhood obesity.

