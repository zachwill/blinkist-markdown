---
id: 5534f2746638630007070000
slug: getting-to-yes-with-yourself-en
published_date: 2015-04-20T00:00:00.000+00:00
author: William Ury
title: Getting to Yes with Yourself
subtitle: and Other Worthy Opponents
main_color: 27A3C4
text_color: 186478
---

# Getting to Yes with Yourself

_and Other Worthy Opponents_

**William Ury**

Think you could do with some help from the people who have to smooth out the thorniest political situations of our time? _Getting to Yes with Yourself_ (2015) draws on professional mediator William Ury's impressive résumé as peace broker in conflicts from the Midwest to the Middle East. Learn how to solve personal clashes more effectively, improve the important relationships in your life and gain more positive influence over yourself and others.

---
### 1. What’s in it for me? Be more influential in the conflicts and circumstances you face every day. 

No matter how much of a peacenik you think you are, you face difficult conflicts, conversations and negotiations every day. Shoot, even Ghandi had to talk his way out of stand-offs on a regular basis!

These blinks apply some of the incredibly popular and successful tips from the negotiating handbook _Getting to Yes_ to situations we all face on a regular basis. That means dealing with difficult relationships both at the office and at home, as well as finding solutions to the recurring problems we find ourselves trapped in throughout our lives.

Nobody can avoid conflicts entirely, so these blinks are for everyone.

In these blinks, you'll find out

  * how Tylenol dealt with their biggest PR disaster perfectly;

  * why you have to give to receive; and

  * why someone with no legs is as happy as a lottery winner.

### 2. If you follow your first instincts during conflicts, you’ll become your own worst enemy. 

Think back to the last conflict you had with another person. Maybe it was a big fight or a simple disagreement. Can you remember what your first reaction was? Did you snap at the other person right away?

Reactionary responses like this are very difficult to resist. They're also highly detrimental and counterproductive.

For example, imagine a mother who wants to have a close relationship with her four-year-old daughter. She loves her, but the little girl refuses to go to bed each night and it makes her mom extremely angry.

If the mother gives in to her anger by yelling at her, the child will probably distance herself out of self-protection. Mom'll end up undermining her goal of fostering a loving relationship with her daughter.

When we judge others instead of seeking to understand them, we often forget about our real interests. Reacting out of anger or because your pride has been hurt will only make things worse.

You can avoid this by focusing on _self-understanding_ — observing yourself from a healthy mental and emotional perspective driven by empathy and self-control.

Self-understanding allows us to identify our passing feelings and thoughts, then neutralize their effects. It helps us maintain a state of balance and calm.

So even if the mother is frustrated, self-understanding could help her recognize her daughter's patterns and choose not to react to them.

A key part of self-understanding is _self-observation_, which means listening to your own thoughts with empathy and patience, just as you would with a loved one. This lays the foundation for _self-mastery_.

In the mother's case, self-observation would help her acknowledge her anger, remain calm and stay committed to her real goal of building a positive relationship with her little girl.

### 3. Don’t blame other people or circumstances when thing go awry. 

It's tempting to blame others when we get upset. Just like reacting angrily, however, this is counterproductive.

Blaming other people is easy because it makes us feel innocent, righteous and superior while deflecting any guilt or responsibility — but it ultimately undermines our power.

The author's friend Sam is a good example of this. He got into car accidents with the family van, then the family jeep, then finally his own car. Each time he blamed the accident on circumstances outside his control — the other driver, road conditions or unclear signs. _He_ was never responsible.

This kind of blame game comes at a cost: it makes us dwell on our victimhood instead of harnessing the power to change things. Sam's accidents and his lack of responsibility led to a lot of conflict in his family.

Owning up to your faults instead of denying them can actually be empowering. Embrace your negative qualities along with your positive ones — you'll become more centered when you do.

Fortunately, Sam managed to listen to his underlying feelings. He realized his accidents were really caused by his aggressive driving, which arose from feelings of insecurity and anger. When he accepted those feelings, he recognized that he was responsible for his own life.

Once he _got to yes_ with himself, he could also do it with his parents. He also stopped getting into car accidents!

Taking responsibility isn't just important on a personal level — it matters in business, too, as we can see from a tragedy that occurred in 1982, when six people died after ingesting Tylenol painkillers laced with cyanide. Instead of trying to avoid blame, Tylenol's CEO took full responsibility, withdrew the product and didn't try to stop the company from losing millions of dollars. Just months later, Tylenol successfully launched a new, tamper-proof bottle. Because the company took responsibility, people began to trust it again.

### 4. You have the power to foster happiness and positivity through your attitude and actions. 

There are a number of counterintuitive tricks that can help us in conflicts. One powerful trick is to maintain respect for your opponent — it'll make them respect you in turn.

Conflicts arise because we fundamentally see the world as a hostile place with scarce resources. When we view others as competitors for those resources we naturally develop adversarial attitudes toward them — we feel distrustful or afraid. This leads to social conflicts.

You can change that attitude by restructuring your internal view of the world so that it's more optimistic and cooperative. This will allow you to gain control of your own happiness.

When you view the world as a fundamentally friendly place, you'll start to see others as potential friends and partners rather than adversaries.

Who do you think would be happier: someone who won the lottery or someone who lost both their legs? This is the question Harvard psychologist Daniel Gilbert asked. You might think the answer is obvious, but research actually shows that after a year, both people will be as happy as they were before the event.

This suggests that major events or traumas have little to no effect on our present happiness. Happiness is something we determine ourselves — it's a state of being.

We assume that happiness is determined by outside influences, but it's actually something we make internally. This is why changing your attitude about the world is so powerful.

When you've adjusted your attitude, you can start viewing your environment in a healthier way. You'll naturally want to connect with others in more positive ways.

This is why modern police forces don't respond to hostage situations with guns and tear gas. Instead, they strive to negotiate politely with patience and persistence. Respectful negotiation is much more likely to save the hostages — it's the kind of negotiation even criminals really respond to.

> _"Do I not destroy my enemies when I turn them into my friends?" — Abraham Lincoln_

### 5. Accept the past, don’t worry about the future, and live in the present. 

Many people struggle to really live in the moment. Why is this such a challenge for us?

Our fears and anxieties are usually much greater than the actual problems we think are causing them. For example, Ury knew a woman named Judith who had a strained relationship with her teenage son. He was in his rebellious phase and harshly rejected her any time she tried to connect with him. No matter how hard she tried, he wouldn't accept her.

Judith became so anxious that her conflict with her son started to affect her husband, who had to mediate between them. The real problem was just her son's rebelliousness, but it snowballed into a huge family fight.

The present is the only time when we have the power to improve things and experience true satisfaction. Fortunately, Judith was able to ask herself, "What's the worst that could happen?" and she realized the situation wasn't as dire as she thought. She recognized her son was just going through his teenage years and let go of her need to win him over.

Once Judith managed to stop clinging to her goal of having the perfect mother–son relationship, she freed herself from the shame and guilt of coming up short. Her son then realized she accepted him and he found it easier to approach her and enjoy spending time with her.

Judith managed to get to yes with herself by embracing the present for what it was, rather than what she wished it was. She made peace with herself, and more importantly, she fixed the strained situation with her son.

> _"My life has been full of terrible misfortune, most of which has never happened." — Michel de Montaigne._

### 6. When negotiating, try to create value for the other person – you’ll both end up winning. 

We've seen some ways we can look inward to solve outer problems. But it's also important to look inward and reward yourself and others.

When you discover the inherent joy of _giving_, you'll shift your focus away from _taking_ and set the stage for inner satisfaction, success and general win–win situations.

Li Ka-Shing, a Chinese billionaire, was able to find inner peace and happiness this way. He was once asked in an interview about his business secrets for success. He said that one key was to always give his partners more than he took for himself. This made everyone want to be his partner, which helped him become so successful.

In short, he gave instead of taking. He made deals that other people benefited from, instead of aggressively trying to make gains for himself at their expense.

This didn't just improve his business — it also made him feel better about himself. He managed to become successful and bring joy to others at the same time.

Giving absolutely doesn't mean losing. Giving can be about mutual gain: helping others while you help yourself. That's how you start the beautiful and unending cycle of giving and receiving.

Research backs up this approach. Carsten De Dreu, a Dutch psychologist, led a comprehensive analysis of 28 different negotiation simulations. He found that the most successful negotiators where those who used a cooperative approach that focused on meeting the needs of both parties. The people who tried to get the upper hand over each other were much less successful.

When you get to yes with yourself and others, everyone can win big!

### 7. Final summary 

The key message in this book:

**If you want to have positive relationships with other people, you first need to have a positive relationship with yourself. So listen to your thoughts, don't judge yourself and accept your faults along with your positive traits. When you get to yes with yourself, you'll lay the groundwork for getting to yes with others. You'll bring more joy to the people in your life and feel happier.**

Actionable advice:

**Change the way you view the world.**

Your thoughts become your reality. When you start viewing the world as a friendly and loving place, you'll feel more satisfied and find it easier to build and maintain meaningful relationships. Happiness is a state of mind — make sure your mind is in the right place!

**Suggested further reading:** ** _Getting to Yes_** **by Roger Fisher, William Ury and Bruce Patton**

_Getting to Yes_ is considered _the_ reference for successful negotiations. It presents proven tools and techniques that can help you to resolve any conflict and find win-win solutions.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### William Ury

Social anthropologist William Ury has advised governments around the world on conflict situations and can even claim to have helped stop two civil wars. He's the coauthor of the international bestseller _Getting to Yes_ and a cofounder of the Harvard Negotiation Project, Harvard's University's world-renowned negotiation department.

