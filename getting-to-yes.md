---
id: 52a6e3e73839360008200000
slug: getting-to-yes-en
published_date: 2013-12-11T09:03:59.000+00:00
author: Roger Fisher, William Ury & Bruce Patton
title: Getting to Yes
subtitle: Negotiating an Agreement Without Giving In
main_color: E32D43
text_color: C9283B
---

# Getting to Yes

_Negotiating an Agreement Without Giving In_

**Roger Fisher, William Ury & Bruce Patton**

_Getting to Yes_ (1981) is considered _the_ reference for successful negotiations. It presents proven tools and techniques that can help you to resolve any conflict and find win-win solutions.

---
### 1. Learn to negotiate well; everything is based on negotiations. 

This is sometimes hard to imagine, but just a few decades ago decisions were rarely made as a result of discussions or negotiations. They were usually made by one person: whoever was in charge.

Back then, the world was a place of hierarchy: at home, every decision concerning the family was made by the "wise father," and at work, everybody adhered to the path dictated by the company's boss.

Today, such authoritarian structures are increasingly rare. Hierarchies are flatter, information is more accessible, and more and more people participate in decisions at all levels.

Hence, it has become much more important for us to talk to others and include them in our decision-making processes. Politicians now talk to their voters, and companies encourage their employees to participate in company decisions.

Even parent-child interactions are becoming more democratic. In the age of Google, parents can no longer simply say, "Don't do this; it's unhealthy," because their child can just go online, find counterevidence and argue their claim.

Today, finding agreements in any area of life means negotiating. Arguing with friends about which movie to see is very different to haggling over prices with suppliers or negotiating international arms embargos, yet in many ways all negotiations are similar to each other.

By arming yourself with the right knowledge and tools, you can vastly improve the outcomes of all your negotiations. And since every day of your life involves some kind of negotiating, it's well worth your time to do so.

**Learn to negotiate well; everything is based on negotiations.**

### 2. Avoid trench warfare. It costs a lot and brings very little in return. 

Too often, conflicts turn into trench warfare: both parties take a position, defend it fiercely and only make concessions if they have to. In such a situation, finding a solution is not the result of negotiation; either the more stubborn side wins or a compromise is found that both sides can, more or less, live with.

The problem with these types of conflicts is that both parties have fixated on their initial positions. Instead of searching for a good solution together, they both want to "win" or at least avoid an embarrassing defeat. Such a mindset precludes a win-win solution.

This usually results in an open battle, which costs a lot of time and energy. Even worse, often both sides will take unnecessarily extreme positions because they expect to have to make concessions. In fact, this only results in longer and more painful arguments.

Trench warfare not only makes it more difficult to resolve the conflict at hand, it can also harm the relationships between the two parties: "If that 2% discount is more important to you than our long-term business relationship, maybe you should find a different supplier!"

Trench warfare is bad in many ways: it results in sub-optimal solutions (at best), consumes a lot of time and energy and harms relationships.

**Avoid trench warfare. It costs a lot and brings very little in return.**

### 3. Keep in mind that you’re negotiating with people. 

It is shortsighted to consider a negotiation as a factual discussion between perfectly rational individuals. In negotiations, there is never _one_ reality; there are always at least two subjective interpretations of reality. Both sides always bring their personalities, experiences, values and emotions to the table.

This means they will often look at things differently and have their own interpretations of the "facts." Sometimes two people will talk about totally different things without even realizing they are doing so.

Also, people can respond very differently to the same situation, especially if it's a stressful one. A long, intense discussion can make one person aggressive, which might irritate the other person and make them defensive. When this happens, any further discussion becomes effectively pointless.

In every negotiation, the combination of differing perceptions and strong emotions are pure poison to finding a mutually satisfactory result. And, unfortunately, rational arguments are not the cure.

Every negotiation actually happens on two distinct levels: the level of factual arguments, and the level of human perceptions and emotions.

It's never possible to completely separate these two levels, of course, but keep in mind that there's always an interpersonal level at play in addition to the facts, and that this can be the source of many conflicts or misunderstandings.

Hence, address emotions like anger or fear. This requires empathy and the willingness to consider not only the facts but also the people you are dealing with.

**Keep in mind that you're negotiating with people.**

### 4. Fight the problem, not the person you’re negotiating with. 

The goal of a negotiation should never be to "win," as if the other party were an opponent. Rather, both parties should focus on finding a long-term solution together.

This is why negotiators should always separate the factual level of an argument from the interpersonal level. If you want to negotiate successfully, always remain on the level of facts.

Of course, this only works if both sides are willing to approach the problem rationally rather than emotionally. This means both have to see each other as partners striving for a win-win solution, not as enemies in a battle that only one side can win.

Ideally, you should both distance yourselves from the topic at hand and look at it together from a somewhat neutral perspective. Sometimes it helps to simply sit on the same side of the table. This way, the issue is no longer perceived as a battle between you but rather as a problem on the table in front of you, and one that can be solved together.

Both parties should strive to use neutral language and stick to the facts. Never stoop to attacks of a personal nature, and never accuse the other person of being unreasonable, no matter how absurd you find their position. When you do, you create distance, which can cause the other person to lose sight of the facts and respond on a purely emotional level.

For example, a separated couple shouldn't argue about who is to blame for a failed marriage; rather, they should discuss which future arrangements are going to be best for their children.

**Fight the problem, not the person you're negotiating with.**

### 5. Before you search for solutions, understand both parties’ underlying interests. 

Often the positions of the two sides in a negotiation seem incompatible. Take a married couple's holiday plans, for example: "I want to spend the holidays by the sea," versus, "I want to go to the Alps."

But when you look below the surface at the interests behind these two positions, you may be surprised at the new solutions that can arise, often without necessitating any compromises. If the husband wants to go the sea so that he can swim and his spouse wants to go to the Alps to rock climb, why not spend the holidays by a mountain lake?

However, often a position is based on a myriad of interests — not just one. In the example above, the couple's differing positions could be the result of many different expectations about geography, accommodation, food, etc.

To find a constructive solution, try to understand all the relevant interests. Once you have identified the differences, it's much easier to prioritize and discover where painless concessions are possible. What's their goal? Where do you agree? Where are the main differences between your position and theirs? And above all, where do those differences come from?

Often people's main drivers are their basic needs for recognition, control, security, and belonging. If you don't know what is driving the other person, ask them: e.g., "Why do you want to go to the Alps?" or "What makes you object to this?"

At the same time, make sure you understand your own drivers and interests. Express them openly before you come up with suggestions. Only when both sides' interests are clear will you find a solution that both sides are happy with.

**Before you search for solutions, understand both parties' underlying interests.**

### 6. Outline options before you search for solutions. 

When two people negotiate, they typically already have a clear vision of what kind of outcome they would like. Often they bring draft contracts, hoping to convince the other person to agree to them.

Such "solutions" are bound to fail: they are inherently imbalanced because they're based on one position only. They're just too narrow to provide a realistic foundation for a joint solution.

Rather than bringing such one-sided proposals to the table, be open to discussing all kinds of potential solutions, and only accept one that both parties happily agree with.

Consider someone asking you, "Who do you think will win the Nobel Prize in Literature next year?" Most likely, you wouldn't immediately come up with one name; instead, you would assemble a list of candidates and choose one after consideration.

This is exactly how you should search for the outcome of a negotiation.

Think of the negotiation consisting of two distinct phases: first you outline potential solutions, and only _then_ you actually agree on something. Do not mix up the two.

Start by highlighting the most extreme positions, go through different scenarios and consider the details. Be creative: scribble sketches, hold brainstorming sessions, ask experts, etc. Consider making a good-natured game out of coming up with the most extreme positions (e.g., by asking, "What would the liberals say? What about the hard-core conservatives?").

You will end up with many potential solutions, and, as you move into the second phase of discussing them, hopefully some will prove acceptable for both sides.

**Outline options before you search for solutions.**

### 7. Always find objective criteria to base your decision on. 

No matter how good your intentions, surprising your negotiation partner with a fully formulated suggestion for a solution is not a good way to move the negotiation forward. The other person will usually disagree and respond either defensively or aggressively.

Instead, first find the right criteria to base your decision on. These should be unambiguous and objective so there is no room for misinterpretation on either side.

For instance, the fair price for a house is not just the target price either the seller or the buyer had in mind. A fair price should be estimated based on things such as the average price per square meter, the condition of the building, and the prices for similar homes in the area. All these criteria are objective and testable.

When negotiating, both sides should openly state their criteria for evaluating the quality of the solution. The criteria don't have to be identical, but they should be objective and understandable.

Never give in to pressure or to an ultimatum. If someone gives you a figure and says, "That's my last offer," simply ask what the underlying criteria are; e.g., "Why do you consider that a fair price?"

Often there will be no tried-and-tested yardsticks, gold-standards or precedents you can rely on. Nevertheless, always try to find objective criteria to base your decision on.

Whenever it's impossible to find the right criteria, make sure you at least have a fair decision process. This aspect of negotiation is understood even in kindergarten, where they teach the "I cut, you choose" method: when a cookie is to be divided among two children, the first child cuts the cookie, but he'd better be fair because the second child gets to choose which half goes to whom.

**Always find objective criteria to base your decision on.**

### 8. To negotiate well, you have to be well-prepared. 

Never attend a negotiation unprepared.

Preparation means knowing as much as possible about the facts beforehand. Gather all the information you can, and study your data closely.

Learn about both the people you'll deal with as well as the specific context of the negotiation. What drives the other person? What are their interests and their goals? Are they independent in their decision, or do they have to consider the interests of their bosses, partners, or spouses? Are there certain personal, political or religious conditions you should know about?

The more things you know, the more you will understand the other person, and the more likely you will both find a constructive solution. The less you know, the more likely that you will end up arguing about non-factual matters based on prejudices, speculations and emotions.

Also, don't underestimate the logistical details of negotiations. When and where should the negotiation take place? At your office? At home? On neutral ground? What about the meeting itself: How should it take place? By phone? Face to face? Or even in a larger group? What role does time play? Will pressure in the form of a deadline help or harm the negotiation?

By investing time in studying the details and preparing thoroughly, you help to create a positive setting where both sides will feel comfortable. This vastly improves your chances of having a constructive discussion rather than becoming stuck in your initial positions.

**To negotiate well, you have to be well-prepared.**

### 9. Negotiation is communication: listen and stick to talking about facts! 

It is no secret that most conflicts and problems would never arise in the first place if there was better communication. Often, misunderstandings and knowledge gaps are what lead to arguments, and better communication can help avoid both these problems.

Even if there is an open conflict, make sure your communication is positive and solution-oriented. Keep the discussion going, and don't let the flow be interrupted by both parties becoming fixated on an argument.

To do this, you must first of all _listen_. Don't hear only what you want to hear but what the other person is really saying. An easy tool is to rephrase what you hear: "If I understand you correctly, your point of view is…." This way, you show that you're listening, and you avoid misunderstandings from the very start because the other person can immediately clarify if something has been misunderstood.

Once you've understood the position of the other person, state what your own interests are. Don't talk about what you consider to be their mistakes and fallacies in his position; instead, talk about your own expectations and hopes.

Never respond emotionally, but, when necessary, give the other person space to vent their anger or other emotions. When such emotions arise, explain them; e.g., "I see why you are angry, and I myself was disappointed because…."

The goal is always to bring the discussion to the level of facts and to keep the discussion flowing. Silence is the end of every negotiation.

**Negotiation is communication: listen and stick to talking about facts!**

### 10. Even the best tools can’t always guarantee success. 

In theory, a negotiation should always lead to better results if both sides are open, structure their negotiation correctly, use objective criteria and strive to find an ideal solution together.

But you can never force the other person to act in a certain way or to give up their extreme position or irrational expectations. You can only try.

It can help to open the discussion by outlining the matter at hand as well as the process you would like to follow in the negotiation: agree on how you want to negotiate and how you will make a decision.

If the other person refuses to follow such a process, or if they use dirty tricks such as the classic "good cop, bad cop" or the insidious "I would love to, but my boss…," then address this openly. Make it clear you will only accept a discussion based on an understanding of both sides' interests and focused on objective decision-criteria.

In addition to the process of your negotiation, there are other factors that can impact the outcome of the negotiation. For instance, whenever there is an imbalance in power between the two sides, like when you discuss a wage raise with your boss, you can only point out why you think it would be beneficial for both you and her to negotiate at eye level. But in the end, she will decide on how the negotiation goes and you will have to accept it.

And remember, though most things in life are up for negotiation, some remain completely non-negotiable. Not even the best-trained and most experienced negotiators would be able to buy the White House, for example.

**Even the best tools can't always guarantee success.**

### 11. Final Summary 

The key message in this book is:

**Don't consider conflicts a zero-sum game. Avoid trench warfare and instead try to understand and address the underlying interests of all parties. Stick to the facts, remember you're dealing with humans, and stay open-minded when it comes to solutions.**

The questions answered in this book:

**Why is it valuable to learn how to negotiate well?**

  * Learn to negotiate well; everything is based on negotiations.

  * Avoid trench warfare. It costs a lot and brings very little in return.

**What does it mean to negotiate?**

  * Keep in mind that you're negotiating with people.

  * Fight the problem, not the person you're negotiating with.

  * Before you search for solutions, understand both parties' underlying interests.

**Which tools and tricks can you use?**

  * Outline options before you search for solutions.

  * Always find objective criteria to base your decision on.

  * To negotiate well, you have to be well-prepared.

  * Negotiation is communication: listen and stick to talking about facts!

  * Even the best tools can't always guarantee success.
---

### Roger Fisher, William Ury & Bruce Patton

Roger Fisher (1922–2012) was an American professor at Harvard Law School. With his co-authors, he founded the _Harvard Negotiation Project_.

William Ury is an anthropologist who works as a peace negotiator for corporations and governments worldwide.

Bruce Patton is a Harvard lecturer and co-founder of _Vantage Partners_, an international consultancy firm that helps companies improve their negotiations.

