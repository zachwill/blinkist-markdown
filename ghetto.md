---
id: 59c11d8eb238e100052dbdd9
slug: ghetto-en
published_date: 2017-09-19T17:00:00.000+00:00
author: Mitchell Duneier
title: Ghetto
subtitle: The Invention of a Place, the History of an Idea
main_color: 6D8CB9
text_color: 4A5E7D
---

# Ghetto

_The Invention of a Place, the History of an Idea_

**Mitchell Duneier**

_Ghetto_ (2016) traces the socio-ideological development of the word "ghetto" — particularly how it's been applied to black neighborhoods in America — and takes an unflinching look at the complex ways in which race, prejudice, policy and sociology interact. When it comes to fighting for racial equality, there are no easy answers.

---
### 1. What’s in it for me? Understand the origin, history and logic of the ghetto. 

What do you think of when you hear the word "ghetto"? Elvis's classic song, "In the Ghetto"? Jewish ghettos run by Nazis? Or do you think of impoverished, predominantly black, inner-city neighborhoods?

Today, the word "ghetto" usually evokes thoughts of black ghettos in America, even though the notion of American urban black ghettos has existed for less than 10 percent of the term's 500-year ­history.

In these blinks, we'll look at how the ghettos came to exist in the first place, and especially what they mean in the American context.

You will also find out

  * that the ghetto got its name in the sixteenth century;

  * how a Swede made an everlasting impression on the debate about race issues in American; and

  * how views on the ghetto from black academics have been repeatedly suppressed.

### 2. The Italian Jews were the first ghettoized people. 

The term "ghetto" probably makes you feel a bit uncomfortable. Today, the word seems not quite politically correct, a slur directed at inner-city neighborhoods in the United States that, usually, are predominantly black.

But the word itself has some surprising origins.

In fact, the Jews were the first ghettoized people.

In sixteenth-century Italy, city rulers and the Catholic Church perceived Judaism as a threat to Christianity. In 1516, Venice issued a decree confining Venetian Jews to the Ghetto Nuovo, a high-walled district in the city. The word itself comes from the Venetian word for its copper foundry, the _ghèto_. Following Venice's example, the city of Rome soon formed a ghetto of its own.

The Jews were forced to live separately in these ghettos, away from the rest of the population. Though they were still free to interact with other residents, their separation had consequences. It resulted in a strong culture and community, but the ghettos were also overcrowded, had high mortality rates and were rife with disease.

When Napoleon Bonaparte invaded Italy in the early nineteenth century, he tried to bring ghettoization policies to an end. However, official and state-sanctioned ghettos persisted into the late 1800s.

The ghetto's origins established a circular logic that continues to this day. The argument goes like this:

The Jews in the ghettos lived in worse conditions, and were less affluent, than the Christian citizens living outside the ghettos. Thus, generations of Christian Europeans concluded that the ghettoization of the Jews was natural and God-sanctioned — a physical manifestation of a moral order.

In reality, the poor conditions in the ghettos were due to the policy of forced separation. Consequently, a circular "justification" arose. Christians saw the ghettos as necessary to contain "innate" and "natural" Jewish squalor.

### 3. After World War II, the word “ghetto” was used to describe African-American neighbourhoods. 

By the early twentieth century, a "ghetto" no longer meant a formalized legal separation of peoples. It instead became associated with voluntary Jewish neighborhoods in general. These ghettos, however, were still located in crowded, impoverished, urban areas, like New York's Lower East Side or around Leopoldstadt in Vienna.

German-Jewish author Louis Wirth, in his 1928 book, _The Ghetto_, noted that these neighborhoods were not instated by government officials. Instead, they were ethnic enclaves and self-segregated slums, where immigrants grouped themselves.

Later, the term was appropriated by the Nazis. They used the label to denote the urban enclosures, surrounded by barbed wire, in which they forcibly impounded Jews. In these ghettos, Jews were starved and tortured.

It was only after World War II that African-Americans started using the word "ghetto" in reference to their own urban neighborhoods.

In 1945, Horace Cayton and St. Clair Drake, two black University of Chicago graduate students, published their book _Black Metropolis: A Study of Negro Life in a Northern City._

The authors used the imagery of the recent Jewish ghettos to criticize racism in Chicago, describing how white neighborhoods blocked out aspiring black homeowners and renters. The authors' goal was to show that racism was as prevalent in the American North as it was in the South.

They cited white Northerners who spoke freely about their prejudices against black people and their desire to restrict jobs for black Americans and maintain a black ghetto.

Even the president of the University of Chicago, Robert Maynard Hutchins, openly advocated whites-only neighborhoods and even used university funds to finance them.

Cayton and Drake regarded black Americans as "America's Jews." To them, black skin was akin to the yellow Star of David badges that the Jews of Warsaw had been forced to wear by the Nazis.

****

### 4. Structural racism against black people went hand in hand with housing discrimination. 

Cayton and Drake's study on racism is well worth a closer look.

Of the many discriminatory maneuvers they drew attention to, one in particular stands out: white people made pacts with each other which specified that they wouldn't let their properties be occupied, leased, sold or given to black people. These pacts were called _restrictive covenants_, and they effectively kept black people from living in white neighborhoods (a fixed quota restricted non-white residency to two percent).

It was explicit racial discrimination. The president of the University of Chicago was himself a fan. And in greater Chicago, these covenants remained in force for two decades. They were automatically renewed unless 75 percent of property owners voted to rescind them.

On top of that, whenever owners felt threatened by an imagined "invasion," they resorted to violence. New black neighbors had their houses bombed and set on fire. Chicago police turned a blind eye to these crimes, and not a single arrest was made.

Real-estate organizations also participated in this disgraceful behavior. For instance, the National Association of Real Estate Boards had over 15,000 members and was extremely influential in national real-estate policy regulation. Its code of ethics stipulated that realtors should never introduce racial minorities "whose presence will clearly be detrimental to property values" to a neighborhood.

Consequently, black people were forced into just a few areas. Overcrowded and squalid, these districts only reinforced the circular "justification" for ghettos.

The housing shortages in these communities were acute. Single-family homes were converted into rooming houses and larger apartments were split into one-, two-, and three-room units. As a result, property depreciation rapidly increased. This persuaded white people that black people were unsuitable to be tenants or homeowners, which, of course, was the exact circular logic that had been used to "justify" ghettos in the past.

There was little chance of escape from such structural racism.

### 5. Gunnar Myrdal wrote the standard, but flawed, text on American race relations. 

The work of Cayton and Drake, however, was soon overshadowed by that of a white foreigner named Gunnar Myrdal, a Swedish economist and sociologist. Myrdal's 1944 book, _An American Dilemma: The Negro Problem and Modern Democracy_, became the definitive statement on racial problems in the United States — the mid-twentieth century's race-relations bible. Immensely influential, the book was even cited in the Brown v. Board of Education decision to desegregate schools, in 1954.

Myrdal was hired by the industrialist Andrew Carnegie's Carnegie Corporation, which thought Myrdal could bring a foreigner's impartial eye to America's race issues. Myrdal was well-intentioned and used a world-class research team that included many high-level black academics.

Myrdal saw American racism as the result of Southern prejudice. He put his faith in the conscience of white citizens, assuming that most educated Northerners found racism unpalatable.

The "American dilemma" of his title alluded to what he saw as an inherent contradiction within white people. He thought they displayed a cognitive dissonance: they held egalitarian principles but didn't apply them when it came to thinking about the condition of black citizens.

Myrdal thus believed a solution would require publicizing the poor living standards of black people.

However, the history of the ghetto is marked by a failure to acknowledge systemic racism — and Myrdal's work was no exception. He only mentioned the word "ghetto" twice in his 1,400-page book, and he utterly failed to grasp the importance of the systemic, enduring Northern racism that cemented anti-black housing discrimination. For him, Southern racism was the culprit.

These errors could have been avoided. Cayton's findings could have filled the gaps in Myrdal's work. But a possible collaboration fell through when they couldn't agree on fair working terms.

### 6. Kenneth Clark saw government policy as contributing to the continued existence of black ghettos. 

Have you heard of the doll experiments? Conducted by Columbia-educated psychologist and black intellectual Kenneth Clark in the 1940s, the experiments produced an unsettling finding: black children preferred white dolls to black dolls. Clark used this finding to bolster his argument that segregation had resulted in internalized feelings of inferiority in the black community in the United States.

This bleak vision was further developed and contextualized in Clark's 1965 book, _Dark Ghetto: Dilemmas of Social Power_.

He argued that America's black ghettos were the result of what he termed the "institutionalization of powerlessness." An aspect of this powerlessness was that black people suffered "redlining," the practice of denying loans to people living in certain districts. White people could, therefore, control the cycle of powerlessness among black people, blocking them from social, economic, political and educational advancement.

Clark also highlighted that racist government policies and capitalist developers had ultimately shaped the ghettos.

For example, federal agencies legitimized redlining practices, which influenced the operations of private lenders. The result was that funds designated for mortgages were directed away from black neighborhoods, and used to build large public-housing projects instead. These buildings, often high-rises, further cemented social isolation in an already unequal society.

Black people were funneled into these projects as their former neighborhoods became centers of urban regeneration and culture, plush with new parks, concert halls, universities and hospitals.

These improvements were, in fact, a pretext for the massive, structural relocation of the black urban poor.

On top of this, economic prospects in the projects were gloomy. In times of economic downturn, black jobs were the first to go and the last to return. Unions were also discriminatory. Additionally, ghettos were poorly situated geographically for finding work; since the mid-1960s, jobs had been moving to the outskirts of metropolitan areas.

Sadly, despite Clark's insights, his work curried little favor with policy makers of the day.

### 7. Daniel Patrick Moynihan’s assessment of the ghetto became the accepted view in the 1960s. 

In the 1960s, in keeping with America's long history of paying little heed to the voices of the black community, black experts had little impact on the government when it came to understanding and forming policies on black ghettos.

Instead, an Irish-American White House policy adviser, Democrat Daniel Patrick Moynihan, took the stage.

In 1963, President Kennedy asked Moynihan, his assistant secretary of labor, to draft what would become the Economic Opportunity Act of 1964. The act ushered in what was to become known as the War on Poverty.

Moynihan's report compiled statistics on black families. Adducing a matriarchal family structure and large numbers of illegitimate births, he concluded that black ghettos and communities were in decay. He saw the breakdown in family structure as a central cause of the increased dependency on welfare, cautioning that if this "damage" was not repaired, all efforts to fight poverty and discrimination would end in failure.

This report, of course, was a classic case of victim-blaming, as it totally ignored the realities of deeper white structural racism. Basically, _The Moynihan Report_ blamed black people for the conditions in the ghettos.

On August 6, 1965, President Lyndon B. Johnson responded by signing the Voting Rights Act. The idea, he claimed, was to create conditions for black equality. Speaking at the historically black Howard University in Washington, DC, Johnson made the case that strengthening the family should be the main priority. In his view, a strengthened family unit would break the cycle of black disadvantage and poverty.

### 8. The misapplication and misinterpretation of sociological studies continue to this day. 

There have been some occasions when ideas of black sociologists have been listened to. But this hasn't always brought about positive change.

The African-American sociologist William Julius Wilson advocated that economic inequality, rather than race, was the largest social problem facing America, even in the ghettos.

In 1978, Wilson published _The Declining Significance of Race_, a book promoting a race-neutral approach to social problems. He defined "ghetto" as a neighborhood where more than 40 percent of the population lived in poverty. For him, race and systemic power imbalances had nothing to do with it. He believed pervasive joblessness was caused more by the deindustrialization of urban centers than by racism, and so strongly recommended federal-jobs programs as the solution.

The backlash was huge, especially from black scholars and leaders. After all, in the 1970s, schools were even more segregated than they had been in 1954 when the Supreme Court ruled against educational segregation. Wilson's hope was that white Americans would be more responsive to "race-neutral" policies. However, no mass-jobs programs ever emerged.

Quite the contrary, in fact. His work was twisted by Reagan administration conservatives and later inspired Democrat Bill Clinton to declare he wanted to "end welfare as we know it."

Government policies, which co-opted and distorted Wilson's ideas, introduced harsher criminal sentences, channeled more funds to police departments and imposed work requirements on welfare recipients.

Clinton's Personal Responsibility and Work Opportunity Reconciliation Act (1996), for example, has faced heavy criticism for forcing mothers to seek work, no matter how poorly paid, after a time-limited period of welfare support. Its aim was to promote "personal responsibility" over structural inequality.

Such policies reflect how much the structural poverty and racism impacting black ghettos has been sidelined in American politics.

To this day, those trapped within the ghettos are still blamed for the circumstances they find themselves in. One can only hope that, some day, the blame will be placed where it belongs — not on the victims of white suppression and structural racism but on its perpetuators and proponents.

### 9. Final summary 

The key message in this book:

**The ghetto has a long and troubled history, both conceptually and in reality. Scholars have held conflicting opinions on it, especially in regard to its importance in perpetuating racial inequity in America. To understand the ghetto, it's important to understand the intersectional complexity of its history and modern status. Politics, race and poverty all play their part and there are no easy solutions.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: _Evicted_** ** __****by Matthew Desmond**

_Evicted_ (2016) tells the heartbreaking story of the individuals and families who struggle to get by in the United States' poorest cities. Despite their best efforts, many of these people have fallen into a vicious cycle of poverty that has left them at the mercy of greedy property owners who don't hesitate to evict families at the slightest provocation. To take a closer look at the details of their lives, we'll focus on the inner city of Milwaukee and the tenants and landlords who populate this deeply segregated area. ****
---

### Mitchell Duneier

Mitchell Duneier is Maurice P. During Professor of Sociology at Princeton University. He also wrote the award-winning urban ethnographies _Sidewalk_ and _Slim's Table_.

