---
id: 55e465502852cc0009000082
slug: ghettoside-en
published_date: 2015-09-03T00:00:00.000+00:00
author: Jill Leovy
title: Ghettoside
subtitle: A True Story of Murder in America
main_color: C7282F
text_color: C7282F
---

# Ghettoside

_A True Story of Murder in America_

**Jill Leovy**

_Ghettoside_ (2015) is an incisive look into the failure of inner-city American police to protect the black communities that they are supposed to serve. These blinks explore the problem of high rates of homicide in black communities. They provide historical background, grapple with the social implications of violence and attempt to find a practical solution.

---
### 1. What’s in it for me? Discover why the police fail so often in black communities. 

We've all seen the tragically persistent news in recent years about police shootings in black communities across America. Although some might say these shootings aren't a particularly new phenomenon, what has changed is that we're now paying more attention to them. They've also raised a critical question: Why is the relationship between the police and black communities so terrible?

Journalist Jill Leovy has spent a large part of her career following cops — especially the Los Angeles Police Department — and brings a new approach to considering the police and the relationships between police and the communities they work in.

What is that approach? Stop trying to prevent all crimes, and put more effort into investigating and solving them. This is the only way forward.

In these blinks, you'll find out

  * why most killers in black neighborhoods are never convicted;

  * how solving crimes can help prevent them in the first place; and 

  * what "No Human Involved" meant to racist cops in the LAPD.

### 2. A disproportionate number of black men in the US end up being the victims of homicide. 

What if you were told that in certain parts of the world, the chance of getting murdered was as high as 1 in 35? What if you found out that this wasn't the reality in some impoverished, war-torn country, but in one of the world's most developed nations?

That frightening statistic is a harsh reality for black males, both old and young, living in inner-city districts in the United States. 

Black males become victims of crimes at a disproportionately higher rate than any other demographic in the US. In Los Angeles today, young black men are murdered somewhere between two and four times more often than young Hispanic men, even though the two populations often live in the very same neighborhoods. Although they constitute just six percent of the country's population, they account for 40 percent of its murder victims. 

The early 1990s marked a peak in homicides in the US; in Los Angeles county, on average, black men in their early 20s were murdered at an astonishingly high rate: 368 out of every 100,000. This is similar to the per capita death rate of American soldiers in Iraq in the aftermath of the 2003 invasion. 

What could possibly cause such high homicide rates? That's a difficult question. These rates are independent of nationwide trends. Over the last two decades, homicide rates in the US have plummeted. Despite this, the discrepancy between the rate of black male deaths and that of the rest of the population remains as high as ever. A historical analysis shows that this has been the case all the way back to the late 19th century.

Faced with such a large, senseless loss of life, you'd think that the authorities would be doing everything they could to solve it. But they're not.

### 3. In most cases, the perpetrators of homicides are never prosecuted. 

With violence at every turn, you'd think that cops must be churning out case after case. But they're not. As it turns out, most homicides involving black male victims go unsolved and unpublicized, despite (or perhaps because of) their prevalence. 

The _conviction rate_ — that is, the number of convictions divided by the number of cases — for black homicides is significantly lower than those of other demographic groups. For instance, in the early 1990s in Los Angeles, around 36 percent of perpetrators of murders of black people were convicted. That's not much better than the 30-percent conviction rate in Jim Crow-era Mississippi!

When we compare this conviction rate with others, the disadvantage of black homicide victims becomes even clearer. According to a _Los Angeles Times_ investigation, a killer of a black or Hispanic person was less likely to be convicted than a killer of a white person. Even if the killers were convicted, the associated penalties were less harsh than those brought against killers of whites. 

The low conviction rate of cases involving black victims dovetails with an indifference to crimes against black people in general — among the police force as well as the general public.

Shockingly enough, jargon once used by the Los Angeles Police Department referred to murders of black people as NHI — "No Human Involved." Moreover, murders of black people are rarely reported. And when they are, the report is often slightly slanted and cloaked in coded language.

For example, murders are often portrayed as incidents of "gang violence," a term that obscures much of the trauma, sorrow and injustice — in other words, the humanity — associated with them. 

So how did this situation come about? It's something we're all wondering. The answer is both historical and structural, and may not be what you expect.

### 4. Throughout history, black communities have lacked a state monopoly on violence. 

As citizens of our respective countries, most of us feel confident that the State will intervene when our personal safety is violated. That's because the State has an exclusive right to practice legitimate force. After all, it's the State, not your neighborhood that runs the police.

Sociologist Max Weber called this _a state monopoly on violence_, and it's what fundamentally guarantees both rule of law and individual autonomy. A state monopoly on violence is why you can feel safe walking down the street — you know the police will protect you.

But for historical reasons, there's no such monopoly on violence in poor black communities in the United States. 

The origins of this phenomenon can be traced back to the American South of the Reconstruction and Jim Crow periods; in this place at this time, a racist hierarchy went through the motions of applying a system of law, but did little to protect the safety of all its citizens equally.

These conditions led to vigilantism, informal justice and, gradually, an arrangement whereby the all-white police forces refrained from intervening in self-policing black communities.

But, starting in 1910, large portions of southern black communities began moving to northern industrial cities. Policemen in the North were willing to aggressively patrol black neighborhoods and react with violence to those who fought back. Accustomed to having jurisdiction over their own communities, blacks in northern cities clashed with police, resulting in major riots in the 1960s, such as those in Watts, Newark and Detroit.

Since then, a certain skepticism toward bureaucratic justice developed in black communities, and lingers on to this day.

### 5. Lawlessness caused alternate justice systems to emerge in black communities. 

We now understand why black communities today don't have a state monopoly on violence. But why does this matter? Well, it ultimately leads to the formation of an _alternate system of justice_ — one that cultivates violence and resists the involvement of law enforcement. 

Without the threat of State intervention, violence becomes a legitimate tool in interpersonal relations.

When someone feels their safety is violated, and that they can't rely on legal recourse, they might attempt to settle the matter by personally using violence. 

This sheds some light on the question of why many homicides in communities that lack the rule of law seem to be sparked by infractions as trivial as not paying a small debt or trying to crash a party — issues that could otherwise be solved through legal channels.

Moreover, these shadow systems of justice are comprised of unwritten codes of etiquette and allegiance, which come to replace official law. Once formed, these systems make effective intervention by law enforcement a considerable challenge.

For example, homicide detectives within the black community in South Central L.A. have great trouble getting witnesses to talk. The difficulty arises from the fact that within the street justice system of South Central, "snitching" (telling on somebody) is considered the ultimate offence.

As a result, many potential witnesses refuse to give evidence out of fear of retribution against themselves or their families. As witnesses play a make-or-break role in homicide cases, the reluctance of informants is a significant factor in the low conviction rates for homicides in South Central.

All of this provides a surprising glimpse into how high-crime areas work: high homicide rates and shadow systems of justice organized around gangs are a consequence of lawlessness, not a cause of it.

### 6. The US police forces’ focus on preventative measures is ineffective and damaging. 

So far, we haven't touched on another reason for high crime and low conviction rates in black communities. It's the factor that explains why things still have yet to change: misguided police policies.

Rather than focusing on investigations, police forces in the United States have centered their strategies around patrols, mass arrests and other "preventative" measures. Often, large urban police departments with long patrolling traditions like the LAPD simply see crime prevention as a more effective practice, while detective work is looked down upon as merely reactive.

On one hand, it makes sense: given the choice, one would rather direct resources toward rooting out possible sources of crime, rather than toward investigations that can only happen after crimes occur. But this focus on prevention has had a markedly debilitating effect on both the police and the communities they serve.

Because investigations are regarded as lower-level work than patrolling, detectives are often underfunded and understaffed. This has proved deeply problematic during large waves of crime. Homicide investigations require time and resources — to build relationships with locals, collect witnesses and develop a case. 

But with limited resources and officials, this is practically impossible. In Los Angeles, in the early '90s, many murders were only pursued halfway before a wave of new cases forced detectives to move on. 

Moreover, aggressive preventative police measures unsurprisingly lead to mistrust in black communities. By busting black youth for small crimes such as marijuana possession or shoplifting, while failing to make progress on real threats of murder and violence, prevention-minded police forces reinforce the idea that the law doesn't care for people of color, leaving individuals in those communities more reluctant to cooperate.

It's clear that prioritizing crime prevention at the cost of investigation does more harm than good to police efforts and the black community. So how to move forward? Well, there is another way.

### 7. By solving murders, police can begin to build a trusting relationship with black communities. 

Despite the widespread idea that the homicide epidemic in poor black communities is permanent, re-establishing a state monopoly on violence is possible. It's pretty simple: police must start solving murder cases. All it takes is some persistence. Murders that occur within shadow systems of justice are often easy to solve if you know where to apply pressure.

Shadow justice cases are shaped by a tit-for-tat exchange of offences and retributions, and the details of a murder are often well known by members of the community, who exchange rumors and gossip. This network of rumors is so active that officers in Southeast Los Angeles have a name for it: the GIN, or Ghetto Information Network.

Cracking most homicide cases comes down to convincing enough witnesses to talk, reassuring them of their safety and building their trust in the law enforcement system — something that can be done with enough determination, patience and commitment.

By focusing on clearing homicides, police forces can demonstrate that the lives of victims are valued, no matter the color of their skin. This, in turn, would be a first step toward mending ties with local communities and regaining their trust in the ability of the law to protect them, rather than harm them.

Of course, it's not easy to argue that law enforcement pays insufficient attention to black Americans in a time when overzealous policing and high incarceration rates for black men are making headlines. But if police departments bring justice to the foreground of their activities, they can show that things don't have to be the way they have been in the past.

### 8. Final summary 

The key message in this book:

**When a criminal justice system fails to respond swiftly and vigorously to violence and death, more violence is sure to follow. This is the cause of high homicide rates, shockingly low conviction rates and persistent shadow justice system in underprivileged black communities in the US. But by refocusing their priorities, by solving homicides instead of preventing crime by harassing residents, police forces can start to change things for the better.**

**Suggested further reading:** ** _Gang Leader For A Day_** **by Sudhir Venkatesh**

_Gang Leader For A Day_ is based on author Sudhir Venkatesh's ten years of personal, in-depth research conducted on-site at the notorious Robert Taylor Homes public housing projects in Chicago. Ignored by city government and law enforcement, residents in the close-knit community rely only on local gangs and each other for basic services and social support.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jill Leovy

Jill Leovy is an award-winning reporter for the _Los Angeles Times_, where she works as a crime correspondent. In 2007, she started the Homicide Report, a blog that, unlike any other blog before it, attempted to report on every single murder incident in Los Angeles with as much personal detail as possible. This is her first book.

