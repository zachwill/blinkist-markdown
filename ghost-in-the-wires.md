---
id: 59c8f3d4b238e10005c201c2
slug: ghost-in-the-wires-en
published_date: 2017-09-28T00:00:00.000+00:00
author: Kevin Mitnick
title: Ghost in the Wires
subtitle: My Adventures as the World's Most Wanted Hacker
main_color: 9ADB54
text_color: 415C23
---

# Ghost in the Wires

_My Adventures as the World's Most Wanted Hacker_

**Kevin Mitnick**

_Ghost in the Wires_ (2011) is the wild story of one of the greatest hackers and social engineers of all time: Kevin Mitnick. Mitnick started by making phony phone calls in the 1970s, but quickly graduated to computers, hacking some of the largest companies in the world. Along the way, he got his hands on vast amounts of information, just to prove to himself that it could be done. This is a tale of technical brilliance and expert escape artistry so unbelievable that it must be true.

---
### 1. What’s in it for me? Immerse yourself in the amazing life story of a notorious hacker. 

In the popular imagination, there are two types of computer specialist. On the one hand, you've got the greasy-haired geeks of academia who speak in zeroes and ones. On the other, you've got the hackers.

There are good hackers and bad hackers, of course — both greedy hackers who plunder bank accounts and heroic hackers who unveil the misdeeds of corrupt regimes. Yet they all have one thing in common: they're cunning and smart and their life is exciting.

Well, obviously it's not that simple. A hacker's life isn't all excitement and intrigue. But there are some people who certainly make it seem that way.

Kevin Mitnick is one such person. Mitnick's life story is chock-full of exciting escapades. So let's dive into the personal history of this notorious hacker.

In these blinks, you'll learn

  * how Mitnick's passion for magic got him started with hacking;

  * how he outwitted the FBI; and

  * how he eventually became an ethical hacker.

### 2. Kevin Mitnick grew up with an interest in magic and manipulation. 

Born on August 6, 1963, Kevin Mitnick was an only child raised primarily by his mother, Shelly Mitnick. They lived an itinerant life in Southern California, as Shelly looked for work and entangled herself in a string of bad relationships.

The constant movement meant Kevin didn't get much of a chance to make friends. Plus, his mother's boyfriends had a bad habit of being abusive to Kevin, which probably contributed to the development of his anti-authority attitude.

Kevin managed to get decent grades in school, and he excelled in sports, but his imagination was captured by an extracurricular activity: magic.

When Kevin was ten years old, a neighbor showed him a magic trick. It was love at first sight. Kevin loved the element of deception and became obsessed with practicing tricks and discovering ways of manipulating the audience, all of which would influence his later escapades.

When he was 13, Kevin could often be found at the Survival Bookstore, perusing the pages of _The Big Brother Game_, a step-by-step guide on how to gain access to people's property records, their driving history and even their bank account details.

These were techniques that would continue to come in handy as Kevin got further into the world of _social engineering_.

As the young Mitnick would come to learn, social engineering is the subtle art of manipulating people so that they do what you want. And the first key is to establish trust.

Say you want to call a company and acquire some specific information. You'll sound more trustworthy if you use the correct terminology and lingo — the words and phrases used by everyone else at the company.

Using this technique, Mitnick was able to pick up a phone and, by saying the right things, get records and personal information on friends, teachers, and even some people he'd never met. It might sound unlikely, but it worked more often than not.

And this was all before his seventeenth birthday!

### 3. Mitnick began his life as a hacker in the early 1980s, when he was still just a teenager. 

The 1980s were the perfect time to exploit the digital world; digital records were increasingly popular, yet security systems were still rather simple.

But before he moved to computers, Mitnick sought to find out just how far he could get with a simple phone call.

On one occasion, he called the California Department of Motor Vehicles (DMV) and gained access to all the files he wanted.

All he had to do was call up the offices and pose as a police officer. Naturally, the DMV clerk asked for his "Requester Code," which was exactly the sort of specific terminology Mitnick needed.

He could now call the local police station, posing as a DMV representative, and ask to confirm the Requester Code of a particular officer. Mitnick simply made up a code on the spot, and the person at the police station corrected his mistake by giving him the right code.

It was as easy as that. He could now call the DMV back and gain access to any address, license plate number or driving record for anyone in the state of California.

However, it wasn't long before Kevin had his first run in with the authorities.

What got him in trouble was his attempt to get a phony computer administrator account at a research company where his friend's father worked.

Everything went fine for a while. He used the father's account to bypass the shoddy security system, and once he was in the system, he easily gained access to an administrator account.

The problem arose when his friend Micah accessed the admin account on his own and forgot to cover his tracks. The company noticed the security breach and had to notify the FBI, thinking that Micah's father was the hacker. The father questioned Micah about the incident and Micah eventually pointed the finger at Mitnick.

The FBI soon paid Mitnick a visit, but there was little they could do. Not only were there no laws on computer hacking in the 1980s; Mitnick wasn't yet 18 years old.

So all Mitnick received was a stern warning, and it wasn't long before he went right back to his mischievous ways.

### 4. Some unfortunate decisions by Mitnick’s friends led to his first arrest. 

Kevin Mitnick liked to see how far he could go with a simple phone call or a modem — he was never into hacking for personal or financial gain. But this distinction didn't prevent him from getting into trouble with the authorities.

This time, no mistakes were made. It was a scorned woman who put him in hot water.

Mitnick wanted to see if he could hack into the giant company US Leasing, and he recruited a friend named Lewis to help him.

The job started with a phone call. Mitnick posed as a network technician calling to warn the company about a computer bug that was putting their data at risk of being lost. This was enough to convince the company to provide an admin account so that they could "fix" the system.

Everything was fine until Lewis broke up with his girlfriend, Susan, who happened also to be a hacker.

Using the account data that Lewis left lying around, Susan entered the US Leasing system and had every possible machine in the company print out pages with Kevin Mitnick's name on them.

And that wasn't all. When Susan learned that Lewis and Mitnick had also worked together to steal the employee manuals from a local phone company, she immediately called the cops, leading to Mitnick's arrest.

Mitnick was not yet 18, so he was sentenced to 90 days in a juvenile detention center.

The judge overseeing Mitnick's case was thoroughly confused by what had happened. Here was someone who was going through all this trouble to get his hands on personal data, but he wasn't doing anything with it. Since hacking for the mere thrill of it was unheard of in the early 1980s, the judge assumed Mitnick was profiting from it somehow.

Mitnick turned 18 in juvie. And even though he'd be tried as an adult if he were ever caught again, he didn't stop.

### 5. Mitnick’s hacking got him into further trouble in his twenties. 

Kevin Mitnick was already making a name for himself in the local IT community; his juvenile offense had been reported on in the _LA Times_.

This newfound notoriety brought some new problems. For a start, being known as a hacker meant he'd probably have trouble putting his skills toward legitimate computer work. After all, which company would want to give a talented hacker access to their system?

Luckily, however, after completing a six-month computing course at a trade school, Mitnick was able to use the school's job placement program to find work at the telephone company GTE.

If you guessed that GTE was one of the companies Mitnick had hacked, you were right. Luckily, the HR department never picked up on this, and it looked as though Mitnick was going to get paid for doing what he loved. That's right, the job was to hack the company's system on purpose in order to identify weaknesses and build stronger security.

If this sounds too good to be true, sadly, it was. Since the job would involve getting access to sensitive information, GTE required the filling out of a security form. Mitnick knew this spelled doom for him; once they ran a background check, they'd realize who they were dealing with.

Sure enough, after only nine days on the job, Mitnick was let go.

And, unemployed, Mitnick went right back to his old ways.

He had a girlfriend at the time, and though he told her he was taking night classes at UCLA, he was actually spending his evenings hacking.

When Mitnick hacked into the software company, Santa Cruz Operations (SCO), he was caught red-handed, and this time he faced more than just criminal charges. SCO filed a lawsuit against him and his girlfriend for 1.4 million dollars — _each_!

However, when the dust settled, it came to light that SCO was actually using the lawsuit as a way to pressure Mitnick into revealing the methods he'd used to hack into their network. Mitnick cooperated and SCO dropped the charges. This time, he managed to get away with just three years probation.

### 6. A mysterious meeting put Mitnick in danger of ending up in jail. 

In 1989, Kevin Mitnick was again ratted out by a friend, and this time he spent four months in jail.

Though he initially saw this as a much needed wake-up call — the warning that would get him back on the straight and narrow — when he got out of jail, he was hit with bad news right away. While he was locked up, his girlfriend had been cheating on him with his closest friend, Lewis.

But what really inspired Mitnick to jump back in the hacking game was Eric Heinz, a hacker he'd heard about through a friend. Supposedly he was the new whiz kid on the block and Mitnick was eager to find out more.

When they met, Mitnick was immediately fascinated by Heinz and he could tell that he really knew his stuff. But Heinz was also extremely paranoid, which made Mitnick uneasy.

The other oddity about Heinz was that he seemed to be getting by financially without having any discernible job. And when Mitnick got back home after the meeting, he tried to find some trace of him through the usual channels but could come up with nothing. The mystery deepened.

But one thing was certain. Heinz told Mitnick that he had his hands on access codes that could be used to hack into any phone system on the west coast. In other words, they had the power to listen in on any phone call in California.

It was intriguing, to say the least, but Mitnick couldn't shake the feeling that Heinz was acting suspiciously. Furthermore, after their first meeting, Heinz kept on coming up with excuses to cancel future meetings.

Mitnick was sure something was up, and was starting to become confident that whoever he'd met wasn't a real hacker. Mitnick eventually began listening in on Heinz's phone calls and, sure enough, he was right.

This imposter was actually an FBI agent, and he overheard him talking to another agent about getting a search warrant for Mitnick's house and setting up a sting operation to put Mitnick back behind bars.

### 7. Under FBI surveillance, Mitnick tried to stay one step ahead of the law. 

Luckily, the FBI had no clue that Mitnick was eavesdropping on their phone conversations. This gave Mitnick the upper hand — and possibly a way to avoid their trap.

At this point, it was the early 1990s and the 28-year-old Mitnick began a careful dance with the FBI, listening to their plans and trying to stay one step ahead of them at all times.

He knew the FBI was staking out his apartment, watching him from neighboring windows and hoping to gather enough evidence for a search warrant.

So he carefully cleared his apartment of anything that might be used against him, and then he stuck a note on his refrigerator that read "FBI doughnuts" to let them know he was well aware of their plan.

It would seem that the FBI didn't take kindly to this message since a search party soon came knocking and ransacked his apartment.

But Mitnick had been thorough in his clean-up job and the FBI came up empty handed. It was a hollow victory for Mitnick, though. They'd found nothing this time, but it was likely that they had no intention of stopping their investigation.

Shortly afterward, Mitnick's three-year probation was finally up, making it legal for him to travel. It was just in the nick of time, too, since three days later the FBI went to his mother's house, where he'd recently been staying, to try and serve a warrant for his arrest.

Clearly, if he had hoped to stay out of prison, Mitnick needed to get out of town.

He decided to start over, with a new identity, and so he moved to that metropolis of dreams and deceit, Las Vegas, Nevada.

Putting his social engineering skills to use, Mitnick got a birth certificate and social security number for a name that he idolized: Eric Weiss, the actual birth name of master illusionist, Harry Houdini.

### 8. Mitnick’s life as a fugitive was full of fake identities and close calls. 

While establishing his new life as Eric Weiss, Mitnick kept all his money and his ID on his person at all times. But there's a reason people shouldn't walk around with $11,000, which is how much Mitnick lost when a thief broke into his gym locker.

That was every penny Mitnick had; it was time to get a real job.

And so Mitnick decided to move to Denver, Colorado, where he set about sifting through the job ads in the local newspaper.

This led to an interview at the law firm Holme, Roberts & Owen.

While the interview went well, there was the matter of coming up with a résumé and references. But Mitnick did this without breaking a sweat; he simply invented a fake company in Las Vegas with a PO box and phone number connected to an answering machine. Then he called up the law firm posing as his former employer and gave Eric Weiss a glowing recommendation.

Three days later, Mitnick had a legitimate job working in the firm's computer department. But this certainly didn't bring his hacking days to an end.

Cell-phone technology was making leaps and bounds in the early nineties, and Mitnick was eager to unravel the programming codes, or _source codes_, that ran these new devices.

Motorola, Nokia and NEC, the industry leaders at the time, were at the top of Mitnick's hit list.

Motorola and Nokia went smoothly. He got in, found the source code and covered his tracks on the way out. But with NEC, he wasn't so lucky. While he was in the process of double checking to make he wasn't being tracked or followed, he intercepted an email from an NEC admin that said the FBI was aware that someone had transferred their source codes to a server in Los Angeles.

This was exactly what Mitnick had just accomplished! Mitnick's heart was racing. The FBI was still on his trail, and if he wanted to stay out of jail, he'd need to take better precautions.

### 9. Things began to go downhill for Mitnick after he got fired from his Denver job. 

In early 1994, Mitnick was enjoying his new job with the Denver law firm. Little did he know it was about to come to an abrupt end.

He had gotten into the habit of using his lunch break to make social engineering calls on his cell phone and his bosses were starting to take notice. They believed Mitnick was using company time to conduct outside consultancy work.

In the spring of 1994, at the age of 30, Kevin Mitnick was once again unemployed.

So he did what he'd done before. He hit the reset button by relocating and changing his name. This time, he became Brian Merrill of Seattle, Washington.

Mitnick didn't have any problems finding a job in Seattle's booming tech industry, but his life as a fugitive from the law was starting to take a toll on his psyche.

His paranoia had gotten so bad that a helicopter flying overhead would trigger a panic attack. He was becoming convinced that the authorities could use his cell phone signal to track his whereabouts.

But, in an ironic twist, it was Mitnick's method of covering his tracks that finally caught the attention of Seattle law enforcement. He'd been using cloning methods so that he wouldn't have to pay for his phone calls and to make it appear that he was calling from a different phone number.

And what began as an inquiry by local authorities into illegal cell-phone use soon became a matter for the FBI.

Mitnick was unaware that an old hacker buddy was now an FBI informant, and after Mitnick called him up, the FBI soon learned that Brian Merrill of Seattle was actually Kevin Mitnick, the wanted fugitive.

Mitnick was aware that the feds were closing in, and so, once again, he packed up and got out of town — this time, to Raleigh, North Carolina.

But it was no use. The FBI was now able to use his cloned cell phone as a tracking device and they were soon knocking on the door of his Raleigh apartment with a search warrant in hand.

Mitnick was hoping his fake identity might hold up, but the agents found what they were looking for. In an old ski jacket that he hadn't worn in years was an old pay slip with the name Kevin Mitnick printed right at the top.

### 10. The injustice in Mitnick’s trial resulted in the “Free Kevin” movement and a plea bargain. 

After three years on the run, Kevin Mitnick was finally arrested on February 15, 1995.

Standing in a North Carolina courtroom to hear the charges being filed against him, Mitnick realized that the feds were going to go after him with everything they had.

Every call he made with a cloned cell phone came with a maximum sentence of 20 years. And since they had 23 calls on record, he was looking at 460 years in jail.

Mitnick had never committed a violent crime, but the feds wanted to make an example out of him and set a precedent that would put fear in the hearts of other hackers.

In addition to the cell-phone fraud, they found him in possession of 20,000 credit card numbers and billions of dollars worth of trade secrets, such as the source codes from the mobile-phone companies. Yet Mitnick had never made an attempt to profit from this information; for him, such information was a sort of trophy.

But this didn't seem to matter to the corporations Mitnick had hacked; they demanded $300 million in damages.

The prosecutors treated Mitnick like some sort of super villain and the judge even denied Mitnick the right to a bail hearing. This shocked Mitnick's defense attorney since he'd never heard of a prior case in the history of American law that didn't include a bail hearing.

The judge also denied Mitnick's right to review the evidence against him since it would mean he'd be near a computer.

People outside the courtroom were also shocked by the treatment Mitnick was getting. A "Free Kevin" campaign was quickly launched. Stickers were made, and news articles popped up everywhere to protest the injustice.

And it seems that the protesters and pro-Kevin articles made a difference because, soon enough, a much friendlier plea agreement emerged.

The damages would be reduced from $300 million to $4,125, he would be forbidden the use of any electronic devices for three years and he'd be put under tight supervision during this time to make sure he cooperated.

Mitnick knew that a better offer was unlikely to appear, so he took the deal. By the time all was said and done, Mitnick ended up spending five years in jail.

### 11. Following his release, Mitnick experienced fame and the chance to use his skills for good. 

Mitnick found a new life for himself after the trial.

It all began with a letter from US Senator Fred Thompson, inviting Mitnick to take part in a Senate hearing on how safe the US government is from cyber attacks.

Mitnick agreed to testify and offer his expertise, and soon thereafter he was being bombarded with requests for similar speaking engagements.

Obviously, since he was no longer allowed to even check his email, this offered a great opportunity to use his computer savvy to earn a living. He was quickly making the rounds at companies, government agencies, think tanks and television news shows, speaking his mind and offering advice.

The Hollywood producer and director J.J. Abrams, who was an active supporter of the Free Kevin movement, even gave him a cameo role on his TV show _Alias_. (His character was, ironically, not a hacker, but a CIA agent. And to get around the rules of his probation, the prop master made sure his keyboard was disconnected.)

So Mitnick was experiencing something of a celebrity moment, which is a bit unusual for a convicted criminal who is still being closely monitored.

Mitnick was well aware of the fortunate turn his life had taken, and he was eager to keep it going by finding work as a technology consultant and writer.

Thankfully, his parole officer agreed to allow him use of a laptop, so long as it wasn't connected to the internet, to write his first book, _The Art of Deception_.

The book was an instant bestseller in 2002, even in Poland, where it outperformed a book by the Polish-born Pope John Paul II.

Mitnick has found that consultancy work suits him perfectly. It lets him do what he's spent his entire life training for: ethical hacking. Companies around the world pay Mitnick to hack into their systems and advise them on how to improve their security measures.

It's a dream come true.

### 12. Final summary 

The key message in this book:

**Kevin Mitnick is among the world's most notorious hackers. But unlike many others, he never used his talents with phones and computers as a way to make personal gains. He did it all for the thrill, not unlike a magician performing a seemingly impossible escape routine. The law finally caught up with Mitnick, but it was probably the best thing that could have happened to him. He is now a free man, no longer living in fear, and able to help companies improve their online security by doing what he's best at — finding a company's weak spot.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Tubes_** **by Andrew Blum**

_Tubes_ (2012) traces the origins of the internet, from its humble origins at a few US universities to its current superstructure status. You'll find out about the physical components of the internet, including fiber cables, hubs and massive internet exchange points.
---

### Kevin Mitnick

Kevin Mitnick was a prolific hacker who managed to stay one step ahead of the authorities for years. He now makes an honest living as a writer, public speaker and computer security consultant. He is also the author of the best-selling book _The Art of Deception_.

