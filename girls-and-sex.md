---
id: 59293c1fb238e10006000677
slug: girls-and-sex-en
published_date: 2017-05-31T00:00:00.000+00:00
author: Peggy Orenstein
title: Girls & Sex
subtitle: Navigating the Complicated New Landscape
main_color: F0569E
text_color: A33A6B
---

# Girls & Sex

_Navigating the Complicated New Landscape_

**Peggy Orenstein**

_Girls & Sex _(2016) illuminates the shadowy landscape of sex and sexuality that girls and young women struggle to navigate today. Many developing women are trying to find the right balance between who society wants them to be and who they want to be themselves. This is no easy task, but we can make it easier by changing the way we talk about sex and by empowering girls to shape their own future.

---
### 1. What’s in it for me? Un-learn the treacherous lessons taught to growing girls. 

Cute, hot, sexy, virgin, prude, slut, whore — these are but a few of the epithets that American girls have to deal with. They are called these things by parents and friends, by boys and men, and, often, they begin to think of themselves in similarly sexualized terms.

Boys, in the meantime, continue to get a free pass when it comes to sex, sexual pleasure and sexual privilege. How did we get to this point, in which discussion of female sexuality so easily leads to moralizing and condemnation?

In these blinks, you'll learn

  * the contradictory lessons girls are taught about sex;

  * why girls don't prioritize their own pleasure; and

  * why abstinence education needs to go.

### 2. The sexualization and objectification of women in the media has a harmful effect on young women. 

If you have a daughter, you surely want her to grow into a bright, confident and happy woman. And for this reason, you may already be concerned about her exposure to certain aspects of modern media — in particular, the way it portrays young women.

All too often, the media portrays women as sexual objects, which can affect the way young women go about their daily life and deal with their own sexuality.

Many of the high school- and college-aged women the author spoke to readily compared themselves to the women they'd seen in movies, on television and in magazines.

The young women in ads, blockbusters and TV series are rarely given any sort of personality or deep characterization. Rather, they serve as the object of desire and lust for older men. Because of this, many of the young women interviewed said they felt unsure of how to behave around other boys and girls.

Sadly, this sexual objectification is commonly reinforced by celebrities, such as Beyonce and Miley Cyrus, who wear tight clothing and make blatant sexual gestures onstage in front of hordes of young female fans.

To be fair, these singers often suggest that this is a way for them to reclaim their sexuality and present themselves as strong, independent women. But this could just as easily be seen as a clever way to use marketing and modern media to increase their own popularity.

For instance, back in 2013, Miley Cyrus created a media storm by suggestively twerking on stage against Robin Thicke. Her popularity skyrocketed, and she sold millions of records overnight.

Keep in mind, these singers are part of a music industry that wants to promote slim, half-naked women. One rarely sees a fully dressed or rather plump pop diva.

### 3. To keep girls from being confused and vulnerable, we need to talk openly about sexual issues. 

Most parents dread having to sit down and talk to their child about sex. Sure, it might be an uncomfortable situation, but that discomfort is far less trouble than the situations your daughter might find herself in if you ignore it.

When knowledgeable parents refuse to have an open dialogue about sex, girls can easily end up confused about what love and healthy relationships are all about.

Being a teenager is already confusing enough, and girls especially are expected to behave in often contradictory ways.

On the one hand, it's a time when they and their friends will want to explore their individual sexuality, but, on the other, girls are expected to remain virtuous and not be seen as overtly sexual beings.

This sets up a double standard that can easily lead to bullying, stress and confusion.

Girls may be called "a prude" or "a square" if they don't wear revealing clothes or are shy about their sexuality. But if they are open to these things, then they'll run the risk of being slut-shamed or sexually harassed by their peers and adults.

The same sort of conflict applies to the term "hot": This label can make a girl feel good about herself since it's often used in a positive way. However, when it comes to actually experiencing a sexual act, many girls feel a strange disconnect from their "hot" body and quickly lose any confidence that might have accompanied the "hotness" label.

It's also common for girls to feel obligated to perform sexual acts for boys.

This attitude can be reinforced by exposure to porn, where one of the most common scenarios is men disrespecting, mistreating and degrading women, only to be rewarded for their behavior by receiving oral sex.

This portrayal distorts the way teenagers approach oral sex: Many boys expect it to happen on dates and many girls feel that this act is impersonal and simply a way to get out of an uncomfortable situation without displeasing their partner.

> _"To me, purity and hypersexualization are flip sides of the same coin."_

### 4. We need better sex education to help young women grow up to have healthy sex lives. 

When is sex satisfying for you? If you're a man, satisfaction probably means reaching climax. But for many women, satisfying sex often comes to mean an experience that didn't hurt too much and allowed their partner to climax.

This is due to an age-old problem in society, where people feel far more comfortable discussing male pleasure than female pleasure.

Instead of discussing female pleasure, society emphasizes the "correct" way women should look and behave in order to appeal to and attract a partner. This leads to women being neither able to masturbate or to show their partners how to pleasure them.

In fact, many women will even fear that their genitalia might come across as ugly or smelly to their partner. This is especially unfortunate since most women report that they achieve orgasm through oral sex more often than vaginal stimulation.

By avoiding the subject, young women grow up to be uncomfortable in their own skin, which results in their having few meaningful and fulfilling sexual encounters.

To start fixing this problem, we need to start re-defining what it means to be "intimate."

Currently, the term "intimacy" is highly ambiguous and raises questions, like: Does it even refer to sex? If so, what kind?

Meanwhile, the average US girl will lose her virginity at age 17, and will usually become sexually active and start performing oral sex at an even earlier age.

Too often, girls will feel pressured to lose their virginity and will regret the experience afterward because they weren't ready. This is why it's important for girls to understand that real and meaningful intimacy doesn't have to include sex.

Let's make it okay for these girls to have open and honest conversations with their partner about personal needs, desires, boundaries and how they can feel both safe and satisfied.

### 5. College life can offer healthy sexual liberation, but it can also lead to confusion and date rape. 

College is a time for pushing boundaries and exploring all that life has to offer, and this explorative period often involves experimenting with sex and sexuality.

For many young women, this includes an active and sexually fulfilling life within the "hookup culture" that college life provides.

The term "hooking up" implies a wide range of activities, from just kissing to engaging in oral or even penetrative sex, all outside the traditional framework of a steady relationship.

For many girls, the casual hookups of college are an opportunity to spend time with boys as well as experiment with partners of the same sex.

This is often facilitated by the sorority and fraternity culture of many college campuses, where alcohol-fueled parties tend to encourage sexual activities.

Statistics show that 72 percent of all college students have hooked up at least once by the time they reach senior year, and have had an average of seven partners altogether.

But this environment isn't all happy experimentation; it comes with an insidious "rape culture" as well.

Parties can often end with people being blackout drunk and legally unable to give sexual consent. Yet, for some, the exact definition of "consent" is confusing.

Some forms of communication may appear to communicate consent, even when the other person had no intention of such an agreement — this is what happened with Megan and Tyler, who both got drunk and chose to hook up.

After some initial kissing and oral sex, Tyler pushed Megan further, despite her repeated protests. Later, confused by the alcohol and losing control, Megan even lied to him about enjoying that night, only later did she realize that his actions constituted rape.

This isn't an isolated incident. It's estimated that rapes at colleges are only reported around 20 percent of the time. A big reason for this is that many young women are confused about whether nonconsensual sex with a guy they've previously hooked up with can be defined as rape.

### 6. The internet is a great resource for LGBTQ teens, but parents also need to be educated about sexuality. 

As we've seen, growing up as a girl can be complicated and confusing. Add to that a growing attraction to other girls, and you can understand why many LGBTQ teens consult the internet, rather than their parents, to figure out what's going on.

Indeed, the internet can be a great resource for getting a better understanding of yourself, especially if you're growing up in a conservative environment.

A large percentage of young women identify as bisexual or queer, yet any identity other than straight is still dangerously taboo in many places — and it's certainly not something parents want to talk to their children about.

Thankfully, curious or confused girls can turn to the internet and find millions of posts, sites and videos about sexuality, gender identity and how to come out to friends and family.

Amber was one such teenager. When she entered high school, her parents and peers pressured her to be a perfect girl, while, on the inside, Amber felt horribly stifled.

The internet helped Amber begin to understand her feelings. She read fan fiction that coupled same-sex characters and she even started talking to a lesbian girl online — a girl who would later become her partner. The internet made it possible for Amber to stop pretending she was someone she's not.

These days, youths are coming out sooner than ever before, so it's extremely important that parents be educated and learn to support LGBTQ issues.

A few decades ago, the average age that people came out was 25; that age is now between 14 and 16, making parental support incredibly important.

Young LGBTQ teens already have a much higher suicide rate than that of straight teens and many are thrown out of their home after coming out. Many parents are surprised and confused, and have no idea how to deal with an LGBTQ teen.

Hopefully, education will solve this problem, and LGBTQ children will no longer have to cope with a lack of parental support.

### 7. Proper sexual education can also lead to lower teen pregnancy rates. 

So why aren't we talking more openly to our kids about sex? Well, conservatives fear that this will somehow increase the number of teenage pregnancies and STDs, despite the fact that other countries are proving that it reduces these things.

In the Netherlands, children receive responsible and meaningful sex education. Adults there realize that children will learn about sex one way or another, so it's better to make sure that the information is accurate and properly relayed.

Dutch parents and teachers have proper conversations about both the dangers and benefits of intercourse, and the numbers show that this method is helping their kids have healthier sexual experiences.

Dutch teens have sex later in life than their American counterparts and over 80 percent describe losing their virginity as being pleasant and well timed. Meanwhile, over two-thirds of American teens describe their first experiences as regretful.

It would appear obvious that Americans should shift away from the abstinence approach to having a more honest and open dialogue about sex. 

For generations, we've encouraged young teens to abstain from sex, but we now know that this harms more than it helps. And it clearly doesn't stop anyone from having sex. It only makes teens incredibly unprepared for these natural experiences.

In fact, students of abstinence-only classes are up to 60 percent more likely to have an unwanted pregnancy.

Charis Denison is a teacher of students in various Californian high schools, and she works on building a trusting relationship with her students by answering any question the students may have.

This leads to honest and beneficial conversations about sex. Denison encourages her students to openly talk about their bodies and sexual experiences, and many students will approach her after class or contact her weeks later to express their gratitude. This is because the normal US approach to sex education usually ignores any chance to offer honest and helpful answers to these pressing questions.

### 8. Final summary 

The key message in this book:

**Young women are approaching sex and sexuality in a different way than their parents did. They have more casual hookups, but, due to a lack of dialogue with adults, these sexual encounters are usually unsatisfying and confusing. Meanwhile, society is still trying to dictate female sexuality, leading more and more women to educate themselves about their own needs and wishes.**

Actionable advice:

**Establish a trusting relationship with your children.**

Sex is an uncomfortable topic and many teenagers don't feel like they can — let alone want to — talk to their parents about their sex drive and first experiences. But young people can be especially insecure about how to experience sexuality in a safe way without getting hurt in the process. This is where you as a parent come in. Try to build an open and trusting relationship with your children. If they ask about sex, don't shy away or just tell them not to have sex at all. Instead, try to educate yourself and your child so when the day comes that they have their first experiences, they don't have to go in blindly.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Raise an Adult_** **by Julie Lythcott-Haims**

_How to Raise an Adul_ t (2015) reveals the ways in which the most common parenting method today, helicopter parenting, is doing more harm than good, both for parents and kids. These blinks outline a better way to parent — one that actually raises children to become truly independent adults.
---

### Peggy Orenstein

Peggy Orenstein is a prolific and popular writer whose work can be read online at Salon or in the pages of the _New Yorker_, _USA Today_ and the _New York Times_ magazine. She is also the author of multiple best-selling books, including _Cinderella Ate My Daughter_, _Flux_, _Schoolgirls_ and _Waiting for Daisy_.

