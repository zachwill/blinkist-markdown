---
id: 52822104346138000c230000
slug: give-and-take-en
published_date: 2013-11-13T14:30:57.438+00:00
author: Adam Grant
title: Give and Take
subtitle: A Revolutionary Approach to Success
main_color: 3298D2
text_color: 206085
---

# Give and Take

_A Revolutionary Approach to Success_

**Adam Grant**

_Give and Take_ offers a breath of fresh air to traditional theories of what it takes to be successful. Backed by ground-breaking research, _Give and Take_ demonstrates how giving more to others, rather than competing against them, may be the secret to profound success and fulfillment.

---
### 1. Takers are self-centered, and focused only on what benefits they can get from others. 

We all know people who seem to care only about themselves, completely disregarding the needs of others. These people are _takers_.Everywhere they go, it seems their primary concern is hogging as much money, status and admiration as possible.

Typically, takers promote themselves eagerly, favoring words like "I" and "mine" rather than "we" and "ours". They also tend to be domineering, using forceful language to persuade others, while unabashedly flattering powerful people to get ahead.

So why are they so selfish?

They simply see the world as a competitive place. To a taker, life is a ruthless game where you take everything you want, helping others only if the personal benefit of doing so outweighs the cost.

A prime example of a taker is Kenneth Lay, former CEO of the energy giant Enron. Along with taking out colossal loans from the company, Lay sold off $70 million in stocks in a well-timed effort to take care of himself before the company went bankrupt, leaving 20,000 people unemployed.

But the average taker is not necessarily evil or corrupt. A less caustic example is legendary basketball player Michael Jordan. As a player, Jordan spoke out in favor of increasing the share of team revenues given to players, only to argue for the opposite when he became an owner. His philosophy: "To be successful, you have to be selfish."

Although their underlying motives may differ, all takers hold the same worldview: they believe that there is a limited amount of "pie" for everyone and it is up to them to take the biggest slice for themselves.

**Takers are self-centered, and focused only on what benefits they can get from others.**

### 2. Givers are driven by the desire to help others and create success for the group. 

Most of us can recall a time when we were touched by the generosity of another person. Perhaps they gave us advice, offered a job opportunity or helped us with a difficult task, all without expecting anything in return. These people are _givers_.

The keystone trait of the giver is that in most transactions they give far more than they get. They are generous with their knowledge and time, and they will often forgo personal credit for the good of a common goal. Their main focus is to provide value for others; having their help reciprocated is either irrelevant or a bonus. The way givers see it, helping other people is its own reward as it makes the givers themselves feel good too.

George Meyer, Emmy Award-winning writer for _The Simpsons_, is a classic example of a giver. Meyer routinely encouraged other writers to use his ideas without asking for personal credit. So, although he helped shape over 300 episodes of _The Simpsons_, he is only credited for 12. Nevertheless, what mattered more to him than keeping count of personal credit was seeing the show succeed. Meyer also invented the word 'meh', an expression of boredom, which was first used by Bart on the show and can today be found in the dictionary. However, even here Meyer was so unconcerned with claiming credit that he actually forgot it was he who had invented the word. The success of the show truly was his main objective.

Givers understand the benefits of collaborative achievement and strive to create abundance for as many people as possible.

**Givers are driven by the desire to help others and create success for the group.**

### 3. Somewhere in between givers and takers, matchers strive for equal, fair exchanges with others. 

"I'll do something for you if you do something for me." This is the calling card of _matchers_ : people who endeavor to give as much as they get. Matchers are easily recognizable for their tit-for-tat mentality. They can be seen striking a deal with their kids about mowing the lawn or enlisting the help of a friend who owes them a favor to drive them to the airport. However, contrary to takers or givers, the matcher's goal in their negotiations is being fair and equitable to all parties, not just one.

To matchers, the world should be a level playing field where people exchange knowledge, skills and resources equally. Matchers are uncomfortable when exchanges are unbalanced, so if they help someone, they expect reciprocation or else they feel disgruntled. This works both ways, however: if a matcher receives a favor, they will feel obliged to promptly return it. This desire for reciprocity also means that if a matcher offers to help someone, there is usually an agenda to "cash in" the favor later, which they believe is entirely justified.

Because matching seems fair to most people, the majority of us are in fact matchers. Matching is a logical way to deal with others, especially in the workplace where matchers will help themselves and their colleagues by trading skills and expertise. Matching is also the most common interaction style on websites such as Craigslist, where users exchange goods, money and services in an equal fashion.

Whether those who match are trading goods or exchanging favors, they view it as an elegant balance between unconditional giving and reckless taking.

**Somewhere in between givers and takers, matchers strive for equal, fair exchanges with others.**

### 4. How much we give or take is shaped by who we interact with. 

When was the last time you felt influenced by a group to behave in a certain way? Although each one of us has an individual governing style of either giving, taking or matching, we also adapt our behavior to suit specific people and circumstances.

It is part of our nature to conform to what we believe is expected of us in a group situation. For example, takers are usually more generous in public, as they don't want to appear miserly to others. On the other hand, givers may suppress their generosity at work when they don't want their kindness to be perceived as a weakness.

One community that influences its members is Freecycle.org, where members give away unneeded items such as cameras or baby supplies for free. When takers sign up to the site and interact with the community, the expected norm of giving causes them to give more than they normally would.

In addition to group pressure, another factor affecting our generosity is how much of ourselves we see in the other person: the more similar he or she is, the more likely we are to give. In one study, Manchester United soccer fans came across an "injured" runner. If the runner was wearing a Manchester United T-shirt, 92 percent of the fans stopped to help him. This percentage plummeted to 33 percent when the runner wore a plain T-shirt. The results of this study demonstrate that we are more likely to give to those who seem like us.

In conclusion, it is likely that both you and those around you considerably shape their behavior according to who they are with — without even realizing it.

**How much we give or take is shaped by who we interact with.**

### 5. Persistent takers lose respect and damage their reputations. 

Common knowledge insists that to succeed, you must take what you want. However, historical examples show that when people take excessively, they lose respect. When this happens, their reputations sour, endangering their further chances of success as others no longer wish to interact with them.

Take, for example, the scientist Jonas Salk who, with the help of his research team, other scientists and thousands of health professionals and volunteers, developed the polio vaccine. During an infamous press conference announcing the achievement, Salk failed to thank those who had worked with him, upsetting his team to the point of tears. Eventually, his selfishness backfired: Salk was never inducted into the National Academy of Sciences — though many other polio researchers were — and was never awarded the Nobel Prize. Some claim it was his failure to acknowledge others that contributed to him not being considered for these honors.

Revered architect Frank Lloyd Wright also had a reputation for excessive taking: He did not pay his apprentices and made them cite his name on all their work. Even more shockingly, when his own son worked for him and requested payment, Wright _invoiced_ him for all the living costs he had accrued as his son. One of Wright's clients even stated that he preferred hiring his apprentices rather than Wright himself due to his disregard for those who worked with him — thus costing Wright business.

The examples of Salk and Wright show that takers are susceptible to _taker tax_. Taker tax involves spreading the word of a taker's bad behavior, which in turn damages their reputation _–_ a common method people use to punish takers.

So, while some takers may enjoy periods of success and even make valuable contributions to society, they eventually tend to be penalized by taker tax, greatly hindering their chances of continued success.

**Persistent takers lose respect and damage their reputations.**

### 6. Givers often achieve the top positions in society because they focus on the greater good. 

Many believe that when it comes to achieving professional success, taking is more effective than giving. This is particularly true in traditionally cutthroat professions like business and politics. Interestingly though, it turns out givers often succeed in such environments, as their interest in helping others benefits them too.

Abraham Lincoln is a perfect example of someone who cared more about giving for the greater good than taking for himself. Before he was president, Lincoln dropped out of a senate race to allow competitor Lyman Trumbull to win. Lincoln relinquished his position because both he and Trumball shared the goal of abolishing slavery and Lincoln believed Trumball had a better chance of winning. Abolishing slavery was more compelling to Lincoln than personal advancement. Later on, Trumball repaid the favor by eventually becoming an advocate for Lincoln when he ran for senate again.

A more current example of how focusing on the greater good helps givers can be seen by the selflessness of Jason Geller at Deloitte Consulting. Geller invented an information management system to gather and store data on the company's clients and competitors, and instead of hoarding the information so he alone could benefit from it, he made it available to all his colleagues hoping to help the entire company do better. This generosity made a big impression on his supervisors who soon promoted him to partner — one of the youngest at Deloitte.

Givers take interest in the greater good. Because of this, they can rise to the top, achieving powerful and influential positions.

**Givers often achieve the top positions in society because they focus on the greater good.**

### 7. Successful givers cultivate and use their vast networks to benefit others as well as themselves. 

Have you ever felt awkward asking a favor of someone you haven't seen in years? People who give tend not to feel this way. Although they may lose touch with some connections over time, a sense of trust and a willingness to help is maintained. This makes it easy for givers to ask favors for themselves or someone they know, even after long periods of being out of contact.

Adam Rifkin, a perpetual giver and Fortune Magazine's 2011 best networker, co-founded the 106 Miles network, a twice-monthly gathering where entrepreneurs come together to connect and pool their knowledge. Through this event, Rifkin helps people get jobs, gives them feedback on their business ideas and connects them to others in his vast network.

But Rifkin's keen interest in helping others through building networks also benefitted himself. Because he's so highly regarded for his generosity, he was easily able to get advice for a new company he wanted to start from Excite co-founder Graham Spencer — someone he hadn't seen in 5 years. This is a typical benefit for givers: when they need help from even a seemingly dormant network, re-connecting is easy because the other party knows the giver isn't after reciprocation or selfish benefits.

Givers believe resource and knowledge pooling is beneficial to everyone. Their networks are vibrant due to their glowing reputations and their willingness to pay forward the help they receive.

**Successful givers cultivate and use their vast networks to benefit others as well as themselves.**

### 8. Givers see potential in everyone they meet, making them formidable at finding and nurturing talent. 

When offered an opportunity to mentor someone, many people look for seeds of talent first to see whether it is worth investing their time. Givers, however, differ in this respect: rather than waiting for proof of ability, givers assume potential in everyone and nurture it from the start. Because of this early-stage support, their protégées are often highly successful, and this success is usually reflected back onto the givers.

Renowned NBA basketball manager Stu Inman provides an interesting illustration of a successful giver in the way he chose players for his teams. Although Inman passed up on some basketball legends including Michael Jordan, he found massive success in drafting the previously overlooked Clyde Drexler, who later made the Basketball Hall of Fame, ten All-Star teams and the Olympics. Inman was deeply respected for his ability to find undervalued players and for his dedication to those he helped develop.

Another person who has found success by believing in those who did not seem promising at first is C. J. Skender, an award-winning accounting professor. Skender's success lies in nurturing and mentoring his students. Having now taught nearly 600 classes, at one time Skender wrote letters to each of his students who sat the Certified Public Accountant exam, congratulating those who passed _and_ those who didn't.

The result of this effort?

Over 40 of Skender's students have won medals for their CPA performance. One former student, Reggie Love, even rose to become Barack Obama's personal assistant.

By recognizing the greatness in everyone, givers provide fertile ground for the success of others, which also creates success for the giver.

**Givers see potential in everyone they meet, making them formidable at finding and nurturing talent.**

### 9. Powerless communication puts givers at a powerful advantage. 

If someone asked us what kind of communication creates the most success, we would probably say it requires confident, assertive language. But recent studies show that in contrast to raising voices and displaying conviction, we can succeed by communicating in a _powerless_ way.

Powerless communication involves focusing on the other person; for example, by seeking advice and asking questions. Rather than being domineering, which evokes resistance, this softer approach has a remarkably persuasive effect. This technique comes easily to givers, as they are naturally interested in others.

The persuasiveness of powerless communication was compellingly demonstrated by a study of optometry companies: opticians who were most clearly givers were also the top sellers. The reason was their communication style. For example, optician Kildare Escoto differs from most salespeople in that rather than pitching a sale, he asks customers about their needs and lifestyle. Asking questions instills trust in customers and enables him to better serve them. As a result, Escoto is LensCrafters' number one giver _and_ salesperson.

Another example of powerless communication is illustrated by "Annie", a scientist who worked at a Fortune 500 company while studying for her MBA. When the plant she worked at closed, she was offered a transfer to another location, but this would have meant stopping her studies. Instead of demanding a solution from the company, Annie sought advice from her HR manager, asking, "What would you do?" Annie was a valued employee, and this powerless approach resulted in her gaining unlimited access to the company's private jet so she could transfer to the new location while still studying at the old location.

Powerless communication can be extremely advantageous. Rather than forcing demands on others, this classic giver approach persuades others to be more receptive to us.

**Powerless communication puts givers at a powerful advantage.**

### 10. Givers are only successful if they can avoid burnout or being abused by takers. 

Many givers flourish thanks to their generosity. Unfortunately, this is not always the case and some end up exhausting themselves trying to please everyone. To avoid such burnout, givers must learn how to stay energized and deal with takers that exploit their generosity.

Surprisingly, recent investigations show that the remedy for burnout lies not in reducing the hours spent helping others but in being able to _witness the impact_ they have.

Conrey Callahan, an overworked schoolteacher, was burning out until she decided to form a mentoring program. At first glance, this seemed counter-intuitive as the program actually _added_ hours to her schedule. However, it also enabled her to work closely with mentees as they prepared for college, and therefore witness firsthand the benefits of the help she gave. Consequently, she felt re-energized in her mentoring _and_ in her teaching.

In addition to burnout, givers can feel "walked over" by takers who abuse their generosity, hence the term "doormat". To avoid this, givers must find a strategy that allows them to indulge their instinct to give but also protects them from abuse. Mathematical biology identifies such an interaction strategy as _generous tit for tat_. That is, "never forget a good turn but occasionally forgive a bad one." In practice this means givers match takers' behavior most of the time, yet indulge their own generous disposition by offering the takers the occasional kind gesture. This provides givers with a sense of control while encouraging positive behavior in others in response to their kindness.

These two factors — witnessing the impact of their efforts and reigning in their over-zealous instinct to give in the face of persistent takers — help givers achieve long-term success.

**Givers are only successful if they can avoid burnout or being abused by takers.**

### 11. Final Summary 

The key message in this book:

**Giving more than you get can result in great individual and group success.**

**Traditionally we have been taught to believe that in order to succeed, we must compete with others and take what we need. However, recent research and historical evidence shows that it is not necessarily the takers who win in the end. Those who give can reach great heights and, as opposed to those who take, they create success for others along with way.**

This book in blinks answered the following questions:

**What are the three main styles for how people interact with others?**

  * Takers are self-centered, and focus only on what benefits they can get from others.

  * Givers are driven by the desire to help others and create success for the group.

  * Somewhere in between givers and takers, matchers strive for equal, fair exchanges with others.

  * How much we give or take is shaped by who we interact with.

**Why does taking not work in the long run?**

  * Persistent takers lose respect and damage their reputations.

**How do you become a successful giver?**

  * Givers achieve the top positions in society because they focus on the greater good.

  * Successful givers cultivate and use their vast networks to benefit others as well as themselves.

  * Givers see potential in everyone they meet, making them formidable at finding and nurturing talent.

  * Powerless communication puts givers at a powerful advantage.

**How do you avoid being a failed giver?**

  * Givers are only successful if they can avoid burnout or being abused by takers.
---

### Adam Grant

Adam Grant is an award-winning organizational psychologist and a professor at Wharton Business School. He has over 60 journal publications and has presented for many prestigious organizations, such as Google, Facebook and the United Nations.

