---
id: 5a09774eb238e100065bcc89
slug: glass-house-en
published_date: 2017-11-14T00:00:00.000+00:00
author: Brian Alexander
title: Glass House
subtitle: The 1% Economy and the Shattering of the All-American Town
main_color: 67917E
text_color: 435E52
---

# Glass House

_The 1% Economy and the Shattering of the All-American Town_

**Brian Alexander**

_Glass House_ (2017) tells the cautionary tale of Lancaster, Ohio, a town that went from boom to bust over the course of the past fifty years. At the heart of this downfall is the Anchor Hocking glass factory, a major source of employment that turned into a bitter disappointment. This story is essential reading for anyone wishing to understand the current state of affairs in American society and politics.

---
### 1. What’s in it for me? Get behind the scenes of a Trump-supporting town. 

Many people point to the people of Midwestern towns and swing states when trying to make sense of how Donald Trump was elected president, blaming, in particular the white, working-class culture for the political turn. But by looking at the development of the all-American town of Lancaster, Ohio, you'll see why this is an overly simplistic explanation.

These blinks reveal how a town could go from bustling with growth and community bonhomie to a brutal demise. When the wellspring of the town's success, the Anchor Hocking Glass Company, landed in greedy, corporate hands, things quickly took an ugly turn. As it turns out, believing in the American Dream is not enough for a town to persevere. Indeed, a toxic combination of bad management and private-equity financiers can wreak havoc on a whole city.

You'll also learn

  * that in Lancaster, having a drink at the bar could get you a job;

  * how Trump's key economic adviser initiated the small Midwestern town's downfall; and

  * why the people who ripped the heart out of the town are still celebrated by the townspeople.

### 2. Lancaster, Ohio was a quintessentially American town in the decades following World War II. 

For a while, Lancaster, Ohio, was the perfect American town. A place that B.C. Forbes, the editor-in-chief of _Forbes_ magazine, once held up as a perfect example of what could be accomplished without the meddling of "left-wingers."

At its height, Lancaster was a thriving, industrious town, and at its center was the Anchor Hocking glass factory, which employed over 5,000 of Lancaster's residents.

Lancaster also looked the part, with a picture-perfect town center where neighbors would greet each other in passing.

Herb George was a typical Lancaster resident. Like many others, he got a job at Anchor Hocking in the years following World War II and worked his way up the ranks. His wife, Nancy, was active in the community, volunteering at hospital fundraisers and campaigning to get new schools built.

Lancaster was typified by such families throughout the 1940s, 1950s and 1960s: the husband worked at the glass factory while the wife was busy with community causes, whether it was city-council meetings, vaccination drives or making sure the sidewalks got repaired.

Lancaster was the kind of town where kids were free to run around on their own and friendships crossed class lines. The vice president of Anchor Hocking would drink at Old Bill Bailey's tavern, shoulder to shoulder with the factory workers, and it wasn't uncommon for an Anchor executive to hire a new employee from among the bar's patrons.

It was also common for a teenage boy to graduate from high school and start work at Anchor Hocking the very next week, well aware that he'd spend the next 40 years there and then retire with a comfortable pension.

Lancaster wasn't the seat of luxury, but people lived worry-free lives and held their heads high with the pride that comes with making an honest living.

Like any other small town, Lancaster had its share of minor scandals, alcoholism and instances of poverty, but it was as close to Hollywood's version of idyllic small-town America as you'd find outside of a movie. In fact, the town was used to film the 1948 movie, _Green Grass of Wyoming_.

### 3. Today, the town is a ghost of its former self, and its history is essential to understanding why. 

Like those of other once-prosperous factory towns, Lancaster's halcyon days are sadly in the past. What was once a rural idyll is now home to rampant drug addiction, crippling unemployment and bankruptcy.

Houses and buildings are falling apart due to neglect and lack of funding. So many of the town's parents have ended up in jail that it's not uncommon for children to be living with their grandparents.

In Fairfield County, Ohio, which Lancaster is part of, 58 percent of the children in the social service program have parents with an opiate addiction. In the neighboring Hocking County, that number is over 79 percent.

For many, drugs seemed like an escape from how bleak life in Lancaster had become, but then they became fuel for even more problems.

When the man in charge of the city's Major Crimes Unit was telling the author about the many drug-related arrests he's had to make, he couldn't hold back his tears. These are people he grew up with; some were even on his high-school football team.

There are old-timers in Lancaster who still cling to the ideals of small-town decency — ideals that now seem to belong to a bygone era. Certain cable TV news programs have given them the idea that the blame lies with freeloading outsiders, but the uncomfortable truth is that the real trouble came from within.

In the 1980s, the Anchor Hocking factory began to suffer from a toxic combination of bad management and greedy private-equity financiers. Fueling their bad decisions was the unregulated free-market capitalism promoted by the Reagan administration.

At the forefront of all was the corporate raider Carl Icahn who used a tactic known as "greenmailing" (as opposed to blackmailing). This is when someone buys up as much stock as they can in a company and then demands a seat on the board so they can push for change. The only escape for the target company is to buy back the shares at a higher price.

Anchor Hocking paid up, and the raid earned Icahn between two and three million dollars — but far worse for Lancaster was that it signaled to other corporate sharks that there was money to be made by going after Anchor Hocking.

### 4. Once Anchor Hocking was put into play as a company to be bought and sold, the lives of workers changed. 

The Anchor Hocking glass factory is a perfect example of how harmful a corporate buyout can be — not just for the business, but for an entire town.

After Icahn's raid, Anchor Hocking faced turbulent times.

First, in 1983, an executive at the factory organized a buyout of part of the company and subsequently relocated the container division to Tampa, Florida. This forced many executives to choose between their jobs and their community, and left the workers of the old Lancaster plant in a state of uncertain, highly precarious employment.

Anchor Hocking then proceeded to be bought by one investment firm after another, always with borrowed money, and always pushing the company one step closer to bankruptcy.

In 1987, for instance, the company was subjected to a hostile takeover by the Newell Corp, whose first act of business was to clean the slate by firing the executives while slowly defunding and winding down the plant in Lancaster. Such gestures, as one Lancasterian put it, "ripped the heart out of this town," and destroyed any notion of the company being personally invested in the local community.

All the owners had the same idea: flip the company and make a tidy profit. But for this to happen, they had to force cost-cutting measures and demand concessions from the union. Renovations and repairs were deemed too costly, and the facilities grew steadily out-of-date, preventing Anchor Hocking from making state-of-the-art products.

Fixed pensions vanished as well, replaced by 401(k) retirement plans that required employees to invest part of their salary toward the pension. The employer is also required to put a portion of money into this plan, but, over time, the amounts grew smaller and smaller, and it soon became apparent that a comfortable retirement was no longer guaranteed.

### 5. Despite blame being cast on the residents, the real cause of Lancaster’s demise lies with politicians and executives. 

The blame for Lancaster's downfall should be apparent to anyone who investigates the subject, yet nonetheless, some people point the finger in the wrong direction.

For example, Kevin D. Williamson, a reporter for the _National Review_, has characterized the white working-class people of Lancaster as selfish and crude — people who forget their sorrows with heroin. In his opinion, the local residents are entirely to blame for their town's suffering.

But what would you expect after 35 years of being financially exploited?

If not for the private-equity vampires, who relocated the company headquarters and effectively removed the invested leadership, surely the outcome would have been different.

By moving the headquarters, executives were no longer socializing with the factory workers or contributing to the town's taxes. Nor were their wives around any longer to lead the way for fundraising and improving the infrastructure.

Instead, the town's despair was exploited by the new owners, who obtained free land from desperate politicians who feared the company would otherwise take away more jobs. This happened in 2003 when a tax incentive cost the town $50,000 that had been budgeted for the public schools. But the only one benefiting from this was the executives, who already owned private jets and million-dollar apartments overlooking Central Park in New York City.

Lancaster's politicians were also making matters worse by borrowing funds from predatory short-term lenders and taking loans that came with a 636-percent interest rate.

Some critics put the blame for Lancaster's downfall on unionized workers who demanded to be paid three times as much as some foreign workers, but this is far from being the root cause of the problem.

### 6. Lancaster has a lot to teach us about the far-reaching effects of the free market. 

You might be asking: What's so important about a small Midwestern town that fell on hard times?

For starters, Lancaster sits in the important swing state of Ohio, which is now seen as being partially responsible for the election of Donald Trump.

As a matter of fact, 61 percent of Lancaster's Fairfield County voted for Trump in the 2016 election. It's no mystery that the residents of Lancaster feel an acute despair over their situation, which played a big role in their voting decisions. But to understand the despair that drove Lancaster and many other cities to vote the way they did, we must look beyond the simple explanations of drug addiction or racism and identify what gave rise to them in the first place.

It's also important to recognize the irony inherent to Lancaster's current situation. Many people in these ailing Midwestern towns celebrate figures like Ronald Reagan and Milton Friedman, who promoted the supposed benefits of a free market that followed a survival-of-the-fittest philosophy. Yet it's this exact philosophy that led to the financial fiasco that landed Lancaster in such dire straits.

The politicians and champions of personal responsibility that they so admire are the ones who set the stage for the financial vampires like Monomoy Capital Partners, Cerberus Capital Management and several other blood-sucking firms that drained Lancaster dry.

These firms were led by executives only looking out for themselves. Their lawyers charged $1,200 per hour and had only one directive: to milk as much money out of Anchor Hocking as possible. Whatever happened to Lancaster was none of their concern.

It's no coincidence that Carl Icahn is one of Trump's key economic advisers. He is also the founder of Cerberus Capital Management, one of the firms that got rich off of Anchor Hocking's demise.

Remarkably, many in Lancaster are still in denial about what caused the town's decay, thanks in large part to _Fox News_ and conservative mouthpieces who've succeeded in obscuring the reality of their situation.

If we're ever going to understand why the American Dream has died, we need to understand the nuance behind what's happened to towns like Lancaster, where that dream was once so vibrantly alive.

### 7. Final summary 

The key message in this book:

**There are a lot of factors at play in the story of Lancaster, Ohio. Once a place where the American Dream was alive and well, with a community that represented the ideal version of what hardworking people could accomplish, it ended up a broken town, full of drug addicts and unemployment. Though the residents of Lancaster have been blamed for their own misfortune, the real responsibility rests on the shoulders of the blood-sucking financiers and politicians who made one bad deal after another, until it all came crumbling down.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _White Trash_** **by Nancy Isenberg**

_White Trash_ (2016) retells American history from the perspective of the poor whites who were by turns despised and admired by the upper classes. These blinks trace the biopolitical, cultural and social ideas that have shaped the lives of white trash Americans from early colonial days to the Civil War, through the Great Depression and up to the present day.
---

### Brian Alexander

Brian Alexander is a former contributing editor for _Wired_ magazine and an award-winning reporter on American culture. He is also a proud former resident of Lancaster, Ohio, where he was born and raised. His previous books include _America Unzipped_ and _The Chemistry Between Us_.

