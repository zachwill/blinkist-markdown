---
id: 5678696b0bac9b000700001f
slug: global-content-marketing-en
published_date: 2015-12-24T00:00:00.000+00:00
author: Pam Didner
title: Global Content Marketing
subtitle: How to Create Great Content, Reach More Customers, and Build a Worldwide Marketing Strategy that Works
main_color: 349EBA
text_color: 267387
---

# Global Content Marketing

_How to Create Great Content, Reach More Customers, and Build a Worldwide Marketing Strategy that Works_

**Pam Didner**

_Global Content Marketing_ (2015) is your comprehensive guide to marketing in the digital age. These blinks offer strategic insights into implementing and maintaining a powerful content-marketing strategy and explore what the future holds for global content-marketing and the role of the marketer.

---
### 1. What’s in it for me? Learn how to make your content a hit worldwide. 

So your business offers a product or service that may have worldwide appeal. Good! But how good is your marketing? And how good is the content that you're seeking to market?

For a business that transcends borders, the need for a global content-marketing strategy is paramount. Using the same content marketing in every market just won't work. After all, different markets comprise different people, from different parts of the world, and that means you have to consider certain variables, like culture, values, religion and so on.

With _Global Content Marketing_, Pam Didner offers her valuable knowledge on how to build the great marketing strategy that your business needs. These blinks will help you make your content (and thus your brand or product) resonate with as many customers as possible, whoever they might be.

In these blinks, you'll discover

  * how the four Ps will help you connect with customers;

  * why headquarters and regional offices have to work together; and

  * which roles a successful marketer should fulfill.

### 2. Content is the cornerstone of innovative marketing strategies. 

From shopping to entertainment to education, the digital age has revolutionized nearly every aspect of everyday life. Clever marketers are aware of this and have adopted new, innovative strategies to match. 

Today, content is produced and distributed rapidly across blogs and social media. And advertising teams use this to their advantage by employing _global content marketing._ Put simply, these advertisers share content that is useful and relevant to their target audiences around the world.

Think about it: what's the first thing you do when confronted with a problem? More often than not, you'll hit up Google in search of answers. There are currently many forums, blogs and websites that make it their job to provide you with the information and solutions you need, be it in the form of product reviews, listicles or DIYs.

So, what if your company leveraged this demand for information to promote your product? Well, that's exactly what content marketing does. By offering content that helps out your potential customers, people value you as a source of information and, soon enough, as a provider of goods and services they're interested in. 

Costco, for instance, publishes a free cookbook once a year. It's full of delicious recipes and doesn't contain any ads. Yet, it's a very successful piece of marketing. Why? Because customers appreciate how useful it is. The cookbook shows that Costco isn't always after profit — they also want to make life easier for their customers. This boosts customer loyalty to the Costco brand. 

Of course, if you're in the web-design business, a cookbook isn't going to reel in the clients. Your approach to content marketing has to be tailored to your target audience, and that requires market research. If you have a clear understanding of what information your audience is seeking, then you'll be able to produce valuable and profitable content that gives your brand both depth and appeal.

> _"In today's content-rich world, the ability to connect different ideas and experiences is a prerequisite for marketing."_

### 3. Internal communication is key for content marketing. 

We all know that success and teamwork go hand in hand, and this nugget of wisdom holds especially true when it comes to content marketing. Of course, working together is sometimes easier said than done. Luckily, there are some simple guidelines that will get your content marketing off to a fine start.

To come successfully together as a team, follow the three As: _align, assemble_ and _act_. 

Before establishing your team, you must first _align_ your objectives by determining exactly what you want to achieve. Once that's done, you're ready to _assemble_ your team. Who will fill which role? Who are the key players?

Then comes _acting_. But be sure that all responsibilities are well defined before you act; it should always be crystal clear which decision-making processes are centralized and which are decentralized. For instance, McDonald's delegates the task of designing country-specific menus to their regional offices. 

With your team established, you should turn your focus to maintaining healthy relationships within it. And the three Cs can help you out. Simply remember to _collaborate, communicate_ and _compromise_. 

Ensure all your team members _collaborate_ by agreeing on plans and processes that inform the overall business. This is crucial, because if you can't get it right, you'll end up with confusion, lowered motivation and even a higher turnover rate.

_Communication_ will boost your collaboration, too, and it's as simple as bringing your team together on a regular basis. Meeting frequently can be complemented with a weekly or monthly newsletter and continuous calls for feedback. This will keep everyone on the same page and ensures every team member's voice gets heard. 

Finally, keep your team strong by _compromising_ when necessary. If prioritizing and explaining the reasoning behind your decisions does away with all problems, then great! But if your team members are still in disagreement about something, use their feedback to create solutions together. This will both cut tension and prevent small issues from turning into big problems.

> _"When it comes to a successful, global team, there is no such thing as over-communicating."_

### 4. Strengthen your bond with the customer by using the four Ps of global content marketing. 

After establishing tight bonds between your team members, it's time to create a personal connection between your business and your potential customers. How? With your content, of course! But with so many competitors out there already, how can you make sure your content hits home?

The four Ps will help you create content marketing that resonates. It starts with _planning_ your content strategy. This means working together with stakeholders and regional offices to define your target audience and your most popular editorial topics, all of which should be in alignment with the objective of your business.

Next, you're ready to start _producing_ content according to your strategy. Market research is again useful here not just to uncover _what_ your audience wants to read but _how_ they want to read it. Does your target group like long articles? Or do they prefer tweets? It's up to you to find out!

Once you've created your content, you'll need to distribute it, by _promoting_ it through different channels. Social media is a fantastic new channel for reaching younger demographics, while other, more traditional channels such as TV and newspapers are still the most effective way to reach older target audiences.

With the first three Ps achieved, all you need to work on is _perfecting_ your content strategy. Do this by getting an idea of what your audience thinks about your content, and measure your success using different metrics, such as how many views or likes you've received or how often your content was shared. 

You can also engage your customers by asking for feedback about what they liked and what they didn't like. This information will help you tweak your strategy to optimize its impact. 

Now that you've received your primer on content marketing, let's delve into the details. The following blinks will explore how you can put the four Ps into practice and create a powerful and dynamic marketing strategy.

### 5. Decisive planning is the foundation for any successful content marketing strategy. 

Whether it's our daily to-do list or our New Year's resolutions, we've always got things to plan out. If we're well organized, our daily tasks may even align with our life goals and dreams. And that's precisely how global content marketing functions: small steps toward major achievement.

If you're not willing to waste money while getting your content marketing strategy off the ground, then you'd best be sure that your content is clearly aligned with your broader business objectives. Producing a big-budget YouTube campaign might be entertaining for internet users — but if your potential buyers don't even use YouTube, then that's money wasted.

If you want to get returns on your investments, you'll need to gather information about your company's overall objectives, target groups and priority countries, as well as ensuring that all your branches follow the same goals. This will inform your marketing plan.

This marketing plan can then be fleshed out with important details. Your first priority is to develop a high-performance editorial calendar with all content-relevant topics on it — such as seasonal sales periods and product launches — on it. Your content should always anticipate the upcoming events in your company. 

Next, you'll need to create profiles for each of your target groups. These should outline the demographics, attitude, behavior, desires and preferred distribution channels for all your audiences. For instance, some users only use social media after work. So, if you release posts after 5 p.m., you're more likely to get more hits. 

Finally, keep your global content-marketing plan fresh. This means ensuring that it's not too long (three to five written pages or ten to fifteen PowerPoint slides) and is easy to adapt and update according to current events and changes in the market.

> _"Planning is an active way of discussing the goals, objectives, strategies, and tasks that we need to accomplish."_

### 6. Win over your target groups with content that’s insightful and relatable. 

Who do you want to sell to? At first glance, this seems like a simple question — one that you may think you could easily answer. But content marketing demands a deeper understanding of this marketing than other forms of advertising, so it's worth looking a little closer at what it really means to know your audience.

First, ask yourself a few questions. Do you want to aim your content at the groups that buy the most? Or do you want to expand your customer base, and produce content that draws them in? 

Next, hone in on which of your target group's characteristics you could build your strategy around. For instance, if your main target group is young adults, then your content should be designed to keep up with rapidly shifting trends. 

If you're feeling ambitious, you might even consider developing content that has global appeal. Developing a universal message that requires no translation is quite the challenge. When done right, however, the result can be extremely powerful. 

Procter & Gamble's 2014 ad for the Sochi Olympics embodied this daring approach. Their video "Thank You Mom" took the global phenomenon of maternal love and turned it into a standout piece of marketing that had broad, yet also deeply personal, appeal. 

The best content definitely demands considerable time and effort. But the boost that content marketing can offer your business is unparalleled. Of course, you need to make sure that your carefully crafted content is seen by the world. How to do that? Find out in the following blink.

### 7. A calculated and creative approach to promotion will make your content stand out. 

Thanks to social media, just about anyone, anywhere in the world, can produce content that's easy to view, like and share. Today, the internet is home to a mind-blowing amount of content. So if you want your marketing strategy to be visible, you've got to promote with purpose. 

But first, keep in mind that although there are many ways to promote your content, none of them is free. If, like Proctor & Gamble, you have the opportunity to share your content during a special event such as the Olympics, then you can look forward to a powerful sales boost. But, you'll also be looking at some fairly daunting advertising fees. 

So what about social media channels that anyone can use, like Facebook and Twitter? Well, these actually aren't free either. As you know, creating good content takes time. And, more often than not, great content is produced by someone who is paid to produce it. 

But if you, like many before you, are working with a tight budget, don't fret! Limitation often catalyzes creativity, and content marketing provides lots of room for innovation. 

Take, for example, _search engine optimization_. This innovative approach is actually quite simple. All you need to do is identify popular search keywords that will link people back to your campaign, and then use these keywords as much as possible. 

This is a cheap and highly effective way to get your content out there, and it's easy to evaluate, too. How to evaluate the success of your content — and then improve it! — is what we'll focus on next.

> _"Promotion is about placing your content so it's appropriately accessible to the target audience."_

### 8. The best content-marketing strategies are constantly re-evaluated and optimized. 

After distributing your content, you're going to have one pressing question: Did it work? Great marketing strategies are more than just a campaign, and you'll need to do a follow-up evaluation to ensure continued success. So how can you learn from and improve upon your first forays into content marketing?

Well, there are three ways to measure your content's success. The first is, naturally, sales. Did your business experience a boost after your campaign? Or was there little discernable change? 

To answer such questions, you'll need to delve into the next metric, which is how people responded to your content. Which headlines, articles or tweets were most popular? What did these bits of popular content have in common? In this step, you're looking for patterns that you can take advantage of the next time round. 

Thirdly, your content marketing can be evaluated in relation to its impact on internal processes in your company. Perhaps the customer service department experienced a boost during the content marketing campaign, as new insights about your target audience left them better equipped to deal with customer complaints. 

Of course, none of your insights will help you if you don't collect them consistently. So it's vital that regional and head offices agree on two or three metrics to be employed nationwide. 

You want the data you collect to be analyzable, and only a consistent system will both ensure this and provide the insights that will help you optimize your content. Once you've followed these steps, rinse and repeat! The most effective content strategy features a constant feedback loop driven by goals, tools and metrics. Simply put, the optimizing never stops!

### 9. Modern marketers make use of both new and timeless strategies, while staying flexible and curious. 

Good marketers know that customers are fickle. As consumer behaviors and expectations shift and evolve, the marketer must develop her skills to adapt. Some marketing approaches are timeless; others fade quickly. So the key to good marketing is an ability to shift between approaches.

Marketing strategies that always work touch us on a _personal level_. The human need for love, attention and belonging never changes. So, if told right, stories that tap into these needs usually guarantee a powerful emotional response.

Keep in mind that the best way to tell a story is often the simplest. And that your best storytellers are usually your customers.

Think about it: We've all got those friends whose taste we respect and can usually rely on. If one of these friend tells you that something is cool or fun, you might want to go check it out, too. This is exactly what "likes" do on Facebook; in essence, they're a new and innovative form of word-of-mouth marketing. 

As well as being aware of new and old approaches to marketing, successful marketers are also always on the lookout to expand their personal skills. Marketers often have to fill several roles in their professional lives, whether as manager, analyst or advisor, so they must always be ready to put their communication and leadership skills to the test. 

And because their field is constantly in flux, marketers must also be ready to play the role of student. The best marketers recognize themselves as lifelong learners, and are willing to pick up skills they lack, even if it means venturing outside their comfort zone. 

After all, marketing in the twenty-first century is no easy task! But with new and innovative tools to complement the oldest tricks of the trade, you'll be able to draw customers in with ideas that always resonate.

> _"Marketers must use the right tools, processes and skill sets to keep up with the rapidly changing marketing landscape."_

### 10. Personalized content is the future of marketing. 

It's never been so easy to stay connected. Communication technology has advanced exponentially over the past decade and shows no sign of slowing down. Information is constantly at our fingertips, and exciting inventions such as Google Glass may soon revolutionize the way we engage with the internet. All of which means that the future's looking bright for content marketing!

Technological developments such as the smartphone, public wireless internet and voice-control interfaces allow us to share and consume information faster and more frequently. And this has actually shaped the way that information is shared on the internet. Most news sources, for example, offer bite-sized updates for online users rather than full-length editorial pieces. 

Today, our reading habits, online activities and attention spans are constantly evolving, and content marketing that evolves with them will be all the more effective. Some content marketing strategies are even already in the process of employing technological developments to create personalized experiences for their users. But how?

With the help of _big data_. These are the huge data sets made up of the information we share online. From things we post on Facebook to our memberships with certain apps and websites to things we enter into Google, big data knows all. These massive sets of data are what websites use to make sure you see content tailored to your taste. 

As we know, the best advertising makes a personal connection to the customer. Big data provides opportunities for personalization on a whole new level. The advertising industry is set for a revolution, and content marketing is just the beginning.

> _"Technologies will continue to play an important role in educating, entertaining and challenging our customers."_

### 11. Final summary 

The key message in this book:

**Global content marketing is the most effective way to win and maintain a loyal customer base. Content marketing promotes a deeper understanding of your target audience and their needs, while also opening up new opportunities for your company to optimize its marketing approach and strengthen internal processes.**

Actionable advice:

**Create a new story.**

Next time you don't know what to write about, think about the last thing that surprised/annoyed you. Even though sometimes it seems as if everything has already been said, there are always new ideas or new dots to connect, new ways to offer insights or surprising facts.

**Suggested** **further** **reading:** ** _Everybody Writes_** **by Ann Handley**

_Everybody Writes_ (2014) gives you invaluable advice on how to create great content, from using correct grammar to crafting engaging posts, tweets and emails. With just a handful of simple rules, these blinks will help you gain a better understanding of how to use the right words to keep customers coming back for more.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Pam Didner

Pam Didner was the Global Integrated Marketing Strategist for Intel, where she led global product launches and marketing campaigns. She is now an expert for global content-marketing, sharing her knowledge in workshops and conferences around the world.

