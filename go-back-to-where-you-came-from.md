---
id: 5a9d93d2b238e100073c055f
slug: go-back-to-where-you-came-from-en
published_date: 2018-03-09T00:00:00.000+00:00
author: Sasha Polakow-Suransky
title: Go Back to Where You Came From
subtitle: The Backlash Against Immigration and the Fate of Western Democracy
main_color: 39788F
text_color: 2C5C6E
---

# Go Back to Where You Came From

_The Backlash Against Immigration and the Fate of Western Democracy_

**Sasha Polakow-Suransky**

_Go Back to Where You Came From_ (2017) takes a look at the current international political landscape and explains how the increase in refugees in Europe has contributed to the rise of the right-wing populist movement. It also explains why Muslim immigrants are the subject of such political demonization, how this issue has strengthened political extremism and why the populist movement is a serious threat to democracy as we know it.

---
### 1. What’s in it for me? Get a better understanding of today’s fraught political climate. 

A question being asked a lot these days is, just how have politics gotten so divisive? Was there always this level of intense extremism, or is it a relatively new thing? To help answer these tough questions, journalists like Sasha Polakow-Suransky, who has been following Europe's political reaction to its ongoing immigration crisis, have set to work.

As it turns out, according to Polakow-Suransky, the current political climate shares some similarities with that of Europe around the 1930s — and the results then were not good.

In these blinks, you get a clear picture of how extreme right-wing parties across Europe have tapped into general fears of immigration and integration, while also describing the potential dangers to democracy and human rights that lay ahead.

In these blinks, you'll discover

  * how the events of 2015 and 1938 are quite similar;

  * what a "counter-citizen" is and how it can lead to extremist recruitment; and

  * how identity and politics have become so closely intertwined.

### 2. Since 9/11, Muslims have been seen as a group that threatens democracy and Western culture. 

The repercussions of the attacks on September 11, 2001, continue to be felt to the present day. In particular, we can still see how the attacks have negatively influenced the public perception of Muslims and Muslim migrants.

Even though acts of terrorism reflect the beliefs of a very small minority of the 1.5 billion Muslims across the globe, the world's entire Muslim population continues to pay the price.

Since 2015, Western Europe has seen a tremendous increase in Muslim immigrants. But due to lingering suspicions that have gradually increased since 9/11, Muslims have struggled to become accepted and integrated into European societies.

Meanwhile, immigrants from Eastern European countries and other non-Muslim parts of the world, despite also causing major demographic changes in Western Europe, are largely seen as nonthreatening.

At the top of the list for why Westerners are worried about Muslim immigrants is the concern that their religion represents a threat to Western culture and democracy.

A common argument is that Muslims coming to Western Europe will try to impose _Sharia law_, also known as Islamic law. Due to its strict adherence to scripture, people believe this will introduce female subservience, homophobia and other behavior that conflicts with Western, liberal values.

One person that has been vocal in endorsing this point of view is Marine Le Pen of France's Front National political party. She claims that secularism, one of the defining characteristics of the French Republic, is being dangerously threatened by Muslim immigrants.

Curiously, Le Pen does not see public displays of other religions, such as Christmas nativity scenes in a department store window, to be as threatening to secularism as a neighborhood with a Halal butcher shop.

In the same vein, there's also Soren Espersen, of Denmark's far-right People's Party, who suggests that every immigrant should have to publicly declare their allegiance to the Danish constitution, thereby placing it above their religion in order of importance.

### 3. The left has also fueled Islamophobia, while the right has adopted some liberal issues to gain credibility. 

It would be a mistake to think that only right-wing attitudes have harmed the public perception of Muslims. The liberal principles of gender equality and freedom of speech are also used to suggest Muslims are a danger to society.

In January 2015, terrorists with links to Al-Qaeda attacked the Paris offices of the satirical magazine _Charlie Hebdo_, killing 12 people. The magazine has self-described as politically "far-left," and the motivation for the attack was the prior publication of cartoons considered disrespectful toward the prophet Mohammed.

In the aftermath of the attack, there were left-leaning people who took it as proof that Muslims are a dangerous threat, ready to violently censor anything they disagree with. And for those Muslims who were clearly not angered by the cartoons, there were also liberals describing them as being "not proper Muslims," thereby reinforcing the hurtful stereotype that being a "proper Muslim" meant you had to be a fanatic.

As for the far-right, they clung to the narrative of being the guardians of society, here to uphold hard-won social equality.

This stance emerged again after an incident in Cologne, Germany on New Year's Eve 2015, in which a group of allegedly Muslim refugees sexually assaulted a large number of women. In reporting on the incident, right-wing media adopted a liberal, feminist perspective that ran completely counter to their traditional rhetoric, but was the perfect avenue to voice their Islamophobia.

Shortly thereafter, Laurent Sourisseau, a cartoonist at _Charlie Hebdo_ and a survivor of the January attack, published a cartoon depicting Alan Kurdi, the three-year-old refugee whose dead body washed up on a Turkish beach in 2015, growing up to become one of the attackers.

Granted, Sourisseau was likely still disturbed by the attack from just months earlier, but, according to the author, the cartoon was still a public statement that even a child refugee doesn't deserve sympathy if they are Muslim.

> _"[We] combine Muslims with radical Islam and terrorism, we combine immigrants with crime and rape."_

### 4. There are similarities between today’s Islamophobia and Nazi-era anti-Semitism. 

One of the most important reasons for learning about our history is to avoid making the same mistakes. And even though right-wing spokespeople may say there aren't any similarities between their Islamophobia and the anti-Semitism of the Nazi era, the author clearly thinks there is.

The common argument here is that, due to terrorist attacks, it is rational to fear and want to expel Muslims. Right-wing voices say this differentiates their views from the anti-Semitic fervor leading up to World War II, since there were no Jewish terrorist attacks at the time.

And yet, this view isn't true.

In 1938, the German diplomat Ernst vom Rath was killed in Paris by a 17-year-old named Herschel Grynszpan. As a Jew in Hanover, Germany, Grynszpan was unable to find work, and his anger only intensified after his sister and parents were deported to Poland.

The Nazis utilized this attack to intensify their propaganda suggesting that Jews were plotting the destruction of German society and needed to be locked up or expelled. In direct response to Grynszpan's act, the Nazis whipped the German public into a violent fervor that resulted in _Kristallnacht_, the night when authorities and civilians destroyed Jewish homes and businesses throughout Germany. Kristallnacht is now seen as an important turning point in the fate of European Jews.

And just as the Jews were demonized by the Nazis, the populist right-wing movement is doing the same to Muslims today by claiming that their Islamophobic agenda is intended to protect Jews — but this is really another attempt to gain credibility and liberal sympathy. If they cast Muslims as a threat to the Jewish people, they might fool voters into thinking they're actually guardians of minorities.

This tactic is being used by the Front National's Marine Le Pen, who has been hard at work trying to erase the stigma of anti-Semitism from her party, which was front and center when her father, Jean-Marie Le Pen, was its leader.

But the real lesson here is that when a political party points to an entire ethnic group as being responsible for the actions of a few within it, they are putting huge numbers of innocent people at risk.

### 5. Causing Muslims to feel marginalized only fuels further violence. 

Muslim immigrants face a no-win situation these days: if they go to work, they're accused of stealing jobs from native citizens; if they end up on welfare, they're seen as burdens on society who are stealing taxpayer money.

This situation is known as being a _counter-citizen,_ a term referring to people who have origins elsewhere and are made to feel separate from those within the country who have strong ancestral connections.

This scenario can be imposed on anyone who is Muslim — even if they were born in Europe — because it's their religion that defines them, and which makes people view them as a parasite rather than a valuable member of society.

The counter-citizen dilemma is often a result of a country's ineffective integration policies.

Many countries respond to new immigrants by immediately providing them with government support and neglecting to enroll them in vocational and language schooling. This opens the door for Islamophobic right-wingers to stoke the flames of resentment in white native populations and encourages the misperception that Muslims are coming to Europe to enjoy an easy life at the expense of hardworking taxpayers.

This is exactly what's been happening in Denmark and the Netherlands, two nations with strong populist movements and politicians who encourage "welfare chauvinism." The idea here is that the welfare state must be protected, but only for "us," not "them."

The blatant disregard for fair and equal treatment between Muslims and white European populations has also served to help radicalize them.

This double standard is perhaps best illustrated by France's "burkini ban" of 2016.

When Muslim women along the beaches of France began to wear burkinis, full-body swimsuits that also cover the neck and hair, they were fined, made to change their clothes or leave the beach. Conversely, French nuns, who were just as covered up as the Muslim women, were free to sit on the beaches undisturbed.

One of the most dangerous outcomes of treating immigrants like this, and making it impossible for them to play an active role in society, is that it makes them vulnerable to fundamentalist recruiters. These people prey on those who feel unaccepted by offering them a place where they can truly belong and make a difference.

### 6. The left’s neglect of the working class has driven voters to the right. 

The rise of the right-wing populist movement is not the result of immigration fears alone; it is also due to the political poaching of voters who feel abandoned by the left.

In particular, much of the momentum has come from working-class voters who have felt ignored and belittled by progressive political elites.

When immigration was on the rise in the 1990s and early 2000s, the European working class were the ones to immediately feel the impact. The new arrivals were settling primarily in their neighborhoods and increasing competition for blue-collar jobs.

When the working-class voters raised their concerns about the changes happening to their living situation, their political representatives — who were traditionally on the left — essentially told them not to make a fuss about immigration, because it would appear racist.

This was just the sort of opportunity the right was eager to exploit, and it's why many former liberal constituencies are now being represented by anti-immigration right-wingers.

What the leaders of the left failed to see back in the 1990s was that worries over immigration aren't always rooted in racism; sometimes, these concerns originate in feelings of insecurity, and this is something representatives should act upon.

A common sentiment among middle-class populations is that they feel economically secure enough to support globalization, whereas the working class long for a return to a more nationalistic time when they didn't have to fight as many people for their piece of the pie.

Among left-wing, middle-class populations, the European Union is seen as a continent of opportunity because of the plentiful opportunities it provides for making money and getting a good education.

Yet, for the underprivileged people within the European Union who live paycheck-to-paycheck, their economic stability depends on maintaining a certain status quo that appears to be disrupted when their nation opens its doors to migrants.

When these threatened working-class voters felt that they didn't have a say in their countries' immigration and refugee policies, it became another opportunity for right-wing representatives to poach voters. There was now an easy target for right-wing and blue-collar voters to unite against: the "others" who were coming in and disrupting the status quo.

### 7. Identity politics tap into fears that trigger an authoritarian response. 

The political battle over immigration has been fought using _identity politics_. Much as it sounds, this term refers to when debates focus on issues of identity, such as ethnicity, religion, gender, sexuality and homeland. So, instead of the usual questions about a candidate's qualifications and the strength of their economic policy, political arguments are now focused on ethnic, religious and sexual minorities.

The white working class has joined in the identity politics debate by positioning themselves as a minority, due to increased immigration.

In the United States, in 2015, white Christian Americans made up only 45 percent of the population. This marked the first time this demographic had fallen under 50 percent, and by categorizing every nonwhite, non-Christian American as an "other," this former majority were suddenly considered a minority.

And just as other minority groups have done, white Christians have started to redevelop and reassert their ethnic pride. Feeling as though their political power was declining, they began voicing their beliefs more strongly than before.

Curiously, research shows that as a group of people feel they're losing power, the response can be a call for authoritarian leadership.

This is something political psychologist Karen Stenner has noticed. She believes that the desire for authoritarianism is not actually a constant character trait in the majority of people, but instead something that emerges in response to the threat of potentially losing power.

The threat, in this case, manifests in the form of an influx of immigrants, thereby triggering the desire for strict authoritarian control, which will hopefully protect them from the newcomers.

This phenomenon is something populist parties are always on the lookout for. When they pick up on people's fears, they quickly validate and intensify them, while offering a strict authoritarian solution to eliminate that fear.

In Karen Stenner's research, she also found that fake news was often very successful in triggering a person's latent inner tendency toward authoritarianism, thus increasing the likelihood that they would support a party with a hard-line approach.

### 8. Populist parties can shift the political center to the right – but we can, and must, move it back. 

Given the success that right-wing populist parties have had in recent elections, the old establishment parties across Europe have had no other choice than to start adopting some populist policy stances. In an effort to win back their traditional working-class voters, this means tightening their approach to immigration laws.

As a result, the political center has shifted significantly to the right. But we still need to make sure that the left doesn't simply echo the agenda of the populist right.

Instead, the left must find a way to present viable economic policies that will peacefully ease voters' concerns and not let their fear turn into violence.

Another important point is that the populist parties don't necessarily want to be in a position of ultimate power. While they're great at amplifying the voice of the people and disrupting the political debate, a lot of far-right candidates don't necessarily want to be in the position of having to deliver on unrealistic campaign promises.

The best scenario for a populist party is to be in the position they are now: forcing the bigger parties to adopt their ideas and _not_ deliver on them. This plays into their narrative that the establishment is useless and that populist movements are the only parties that can save the day.

It's also worth noting that when left-leaning establishment parties take on more radical policies, it can isolate their base and cause these supporters to move to a party further to the left, not to the right. This makes it even less likely that far-right parties will come to power.

However, to make sure of this, we need our democratic institutions to keep the growing populist movement in check.

The process of voting is only one way to maintain a healthy political balance. Democracy also relies on the judicial system to uphold fairness and prevent the majority from endangering the minority. The present danger is that leaders will fire the members of the judicial branch and replace them with ones who will adhere to their agenda.

When this happens, we cannot stay silent. The reality is that populist parties, with their hateful rhetoric, blatant lies and abandonment of scientific facts, are the real threat to democratic societies.

### 9. Final summary 

The key message in this book:

**Democracy has been endangered by populist groups that target and undermine the rights of minorities and immigrants living in their countries. As these groups and right-wing political parties gain momentum and win over fearful voters, established political parties have become afraid to challenge them for fear of losing power. We must stand up for those being marginalized; if we fail to do so, we may lose the fair and just societies we claim to want to protect.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Immigrants_** **by Philippe Legrain**

_Immigrants_ offers a compelling case for a total revamp of the way most people view immigration and immigrants. It provides a detailed description of the case against immigration, while providing solid evidence for the great benefits, both social and economic, that migration provides.
---

### Sasha Polakow-Suransky

Sasha Polakow-Suransky is an acclaimed journalist and the former editor of "International Opinion" at the _New York Times_. He is also the writer of the book _The Unspoken Alliance: Israel's Secret Relationship with Apartheid South Africa_.

