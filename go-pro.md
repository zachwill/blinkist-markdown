---
id: 54bd1d75633234000a1d0000
slug: go-pro-en
published_date: 2015-01-19T00:00:00.000+00:00
author: Eric Worre
title: Go Pro
subtitle: Seven Steps to Becoming a Network Marketing Professional
main_color: 2AB4D0
text_color: 196C7D
---

# Go Pro

_Seven Steps to Becoming a Network Marketing Professional_

**Eric Worre**

_Go Pro_ offers a complete crash course in network marketing. It gives you a rundown of all the skills you need to become a network marketing professional, and inspires you to develop the attitude that will turn your toil into fortune.

---
### 1. What’s in it for me? Learn how to become a great network marketer. 

What do you dislike most about your job? Is it your evil boss, the late nights in the office or maybe the constraints of your company's strategy? If only there was a job where you could set your own hours, be your own boss and follow your own path.

In fact such a job exists: _network marketing._ Network marketers are both salesmen and managers; as well as selling their own products, they recruit others to sell for them. This strategy allows them to set their hours, have unlimited income potential, and work with whoever they choose.

But how do you get this dream job? These blinks show the path to untold riches and success as a network marketer.

In these blinks you'll discover

  * why being a professional beats being a poser;

  * how network marketing can take you from Las Vegas to Moscow; and

  * why success starts with a very long list.

### 2. Network marketing is a dream job most people may have never even considered. 

What's your dream job? If you could choose to do anything, what would it be? To be fair, most people would say things like "artist," "astronaut" or "professional beer taster," and probably wouldn't say "network marketer."

A _network marketer_ is someone who not only sells products, but also recruits others to sell on their behalf, in return for a commission. For example, if you sell cell phones as a network marketer, then you won't just be looking for customers to buy your phones, but also for other sellers to sell your phones.

No one seems to consider network marketing a "dream job," but maybe they should!

As a network marketer, you are your own boss. You set your own pay, depending on how hard you sell and recruit. The difference between _your_ pay and a traditional employee's pay, however, is that for you, the sky's the limit! Nothing can hold you back if you have the right skills and put in the effort.

In addition, network marketers also have the opportunity to meet some amazing people, both those they sell to as well as those they recruit. And in traveling to meet these amazing people, you'll also visit some amazing places.

Not only will you have the pleasure of leading a wonderful life, but you'll also have the opportunity to help others achieve the same success. As a network marketer, a large part of your time is spent coaching and inspiring your recruits to become successful network marketers themselves.

By helping them develop, you will likewise grow by sharing in their progress (both monetarily and personally).

So how do you set the gears in motion for network marketing success? Our following blinks will give you concrete advice how.

> _"You must accept a temporary loss of social esteem from ignorant people."_

### 3. Becoming a network marketer requires professionalism and an eagerness to learn. 

There is no such thing as a free lunch. If you want to succeed in anything, then you'll have to work hard in order to develop the necessary skills, and this is just as true in network marketing.

The amount of work you put in correlates directly to which tier of network marketers you end up belonging to: _poser_, _amateurs_ and _professionals._

Some people want short-term profits from network marketing, without having to put in the effort to develop skills: the posers.

Then there are those who have a knack for network marketing and want to learn, but who lack total commitment. In other words, they're amateurs.

Finally there are those with the drive to become the best network marketers that they can possibly be: the professionals. Only professionals, with their commitment to self-improvement, can become successful network marketing experts.

The path to becoming a professional network marketer is neither easy nor short. But it is at least predictable. The path of the network marketer follows the _1/3/5/7_ rule:

After one year you'll have learned enough to be competent and professional. After three you can start network marketing full-time. After five you will become a six-figure earner. And only after seven years in the business will you become a world-class expert.

Within this timeframe, you will need to make use of resources to learn the necessary skills.

A great place to start is by learning from others who have already "made it." Many of the most successful experts have great audio, video and online tools where they explain the art of presentation, recruitment and making the sale.

Be sure, too, to make the most of training courses, seminars and speeches.

But whatever resources you decide to use, don't get stuck on the theory. _Use_ these skills in order to develop a plan.

Remember, network marketing is all about doing, so put the theory into practice as soon as you have a chance.

### 4. To become a network marketer, you’ll need to find prospects and show them your product. 

Before you can do anything as a network marketer, you'll first have to build a network to market to.

This network is composed of both potential customers and sellers. In order to find these people, you need to create a contact list.

This list should be a huge, comprehensive list with _everyone_ you know, not just those you think might be interested in your offer.

Make it a priority to constantly expand this list through networking. There are many ways to, but one of the most sure-fire ways is through volunteer work, maybe at a school or gym.

When you're building your network, it's not enough to just get their contact details. You have to build a relationship. Make an effort to interact with the people in your network as much as possible: call them, meet for drinks, etc.

Once you've developed positive relationships with your contacts, it's time to get them interested in your product and in network marketing.

Start by sharing your tools, like DVDs or online videos, which explain and promote your products and sales techniques.

When you invite them to check out your product, always engage them personally or over the phone, and _never_ via text or email.

Follow this formula to engage people to look at your materials:

  * First, be in a hurry. People are more likely to listen if you're in a rush.

  * Next, offer them a compliment to put them in a good mood.

  * Then, ask a question with the form: "If I. . . would you. . ." For example: "If I gave you a DVD, would you watch it?"

  * If they yes, let them decide when they'll look at the material. Confirm when by asking: "If I call you the next day, will you have watched it?"

  * Finally, confirm their contact details and your invitation is complete!

Finding people to look at your materials are just the first steps on the path of network marketing. Next you have to get them hooked.

### 5. Once your prospects understand your product, get them hooked with presentations and follow-ups. 

Inviting prospects to review your product is a great start, but it's not enough to convince them to join your team. To accomplish this, you'll have to present your product personally, and follow up.

_Presenting_ isn't about selling yourself as an expert. Rather, it's about selling the product itself. The best way to do this is by telling a story.

People won't be interested in you, but they will be interested in your story. Thus, your story must be well-crafted. It should give a compelling account about what you didn't like about your life before, about how network marketing or your product changed your life, and how much better things are for you now.

Just think about all those infomercials you've seen for things like miracle diet pills: they get people interested by telling a story — the story of an overweight person who made himself thin and happy by using the product.

After your presentation, it's time to _follow up_ — to educate your prospects and help them come to a decision. The aim isn't to get a decision after one _exposure_ ; that immediacy puts too much pressure on your prospect. Rather, you should build trust over repeated follow-ups (four to six over a few weeks' time).

Prospects will always be skeptical at first, so be understanding. If you listen to them and ask the right questions, then they'll come around.

It's important to remember that, even in educating your prospects, you should never present yourself as the expert. Only talk about the project through the story you've crafted.

By this point, you're now ready for the final step in network marketing recruitment.

> _"In network marketing, it doesn't matter what works. It only matters what duplicates."_

### 6. The final steps make prospects into customers and new distributors. 

After presenting and following up, your prospects are finally ready to become your new customers or _distributors_, those who sell your products on your behalf.

Yet, this transformation from prospect into customer/distributor doesn't happen automatically. You have to actively help them with their transformation. You can't and shouldn't pressure your prospects into becoming distributors/customers. Even if you succeed in doing so, the relationship won't be long-lasting.

Rather, you must allow them decide for themselves. They have to love and feel a connection to the product. That is the only way to guarantee long-term commitment.

What's more, you should ensure that your prospects see you in the most positive light possible. Your prospects have to believe in you as a person, as you're the only face of the company as far as they see. So, do your best to keep them happy, and always be available to answer their questions.

You also have to manage their expectations. Many new recruits fantasize about getting rich quick or getting on the fast-track to becoming professionals. It's your job to educate your new distributors about what they can and should expect.

Start by asking them these four crucial questions:

  1. What would it make a career in network marketing worth it for you: i.e., how much money?

  2. How much time per week are you willing to work?

  3. How many hours can you work per month?

  4. How much time do you need to get started?

By asking these questions, you can make sure that their expectations are realistic. Referring to the 1/3/5/7 scheme, you can show them that the path to wealth is long. However, it's worth it in the end!

### 7. Once you’ve turned your prospects into distributors, it’s time to help them get started. 

A successful network marketer will help his distributors get off the ground and will go to industry events.

Network marketing is all about earning through your distributors. As such, you have to take special care to make sure that they're working intelligently, and this is especially true in the beginning.

Your distributors will need help getting started. It's especially important for them to understand that they work _with_ you, not _for_ you, even though you earn money from their work.

This means that they must have a great personal involvement in the business. All distributors should become personally attached to the product they sell by using it themselves. They need to discover for themselves why the product is worthwhile.

Your ultimate aim, however, should be to get your new distributors to the point where it is now easier to continue to sell than to quit.

Teaching, however, is an ongoing process. In order to improve their sales skills (as well as your own), you'll need to get your distributors to attend d _estination events._

These are events held in various locations (from Las Vegas to Moscow, to a farm in the middle of nowhere) where people in the network marketing business come together to meet other inspiring people, learn new strategies and hear success stories.

As a network marketer, you also have the opportunity to raise your prestige in the network marketing community, for example, by delivering a talk or giving a presentation.

And there is one more benefit for you: after attending many destination events, you'll notice that there are fewer familiar faces from the first time you attended. Being one of the last still standing is a truly rewarding experience.

> _"The Law of Association says you'll become the average of the five people you spend the most time with."_

### 8. Final summary 

The key message in this book

**Anybody can become a network marketing professional, if you're willing to invest your time to learn the seven necessary skills. Your time will be rewarded over the course of several years, when your work translates to distributors making you a fortune.**

**Suggested further reading: A/B Testing by Dan Siroker and Pete Koomen**

We all know that the internet has changed the way we do business. After all, today nearly every company has a website, but how many businesses manage to effectively optimize their online presence? Well, _A/B Testing_ shows you how to develop and refine your website through simple experimentation and evaluation — all to attract more customers and boost your bottom line.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Eric Worre

Eric Worre is a highly successful network marketing professional, whose 25-year career has earned him millions. In 2009 he founded NetworkMarketingPro.com, the most-watched marketing training site, attracting a global audience. _Go Pro_ is his first bestselling book.

