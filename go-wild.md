---
id: 593e7517b238e10005ae415a
slug: go-wild-en
published_date: 2017-06-15T00:00:00.000+00:00
author: John J. Ratey & Richard Manning
title: Go Wild
subtitle: Eat Fat, Run Free, Be Social, and Follow Evolution's Other Rules for Total Health and Well-Being
main_color: 4AA8E1
text_color: 316E94
---

# Go Wild

_Eat Fat, Run Free, Be Social, and Follow Evolution's Other Rules for Total Health and Well-Being_

**John J. Ratey & Richard Manning**

_Go Wild_ (2014) provides a timely look at why human beings shouldn't be sitting in front of a computer all day. Evolution did not craft our bodies and minds for today's sedentary lifestyle and diet. Our move from wilderness to cubicle is likely responsible for our increasing susceptibility to a number of new diseases.

---
### 1. What’s in it for me? Get in touch with your wild side. 

If you live in the city, you probably experience the occasional yearning for vast open spaces. Perhaps you sit in your office daydreaming about riding on horseback across the plains of Mongolia. Or you remember the last mountain-climbing expedition you took and the exhilarating feeling of reaching the peak and surveying the land far below.

At the very least, you probably sometimes get the urge to go to the local park or climb a tower to get a view of the city. And there's an evolutionary reason for this urge. Our bodies have evolved to be active, lithe, alert and to live in the wilderness; that's how they're designed. However, modern society has taken away almost all of the conditions that our bodies need in order to thrive.

But despair not! Even in urban settings, there are ways to reconnect with the wild.

In these blinks, you'll discover

  * how seemingly primitive tribes are less degenerate than modern humans;

  * that human beings really do have a tribal side; and

  * that a vegan diet may not be the best idea after all.

### 2. Humans evolved to actively live in the wild, not to be sedentary. 

You wake up, go to work, sit at your desk all day, come home, watch some TV, go to sleep and do it all over again the next day. If this sounds familiar, there might be a part of you that is yearning to break this cycle, escape the modern world and live "the wild life."

This is actually a very natural feeling because evolution didn't shape us to sit at desks all day — it prepared us to be wild.

We possess the same instincts as other animals: the natural impulses that drive, for instance, a mother bear to raise her cubs in such a way that they'll survive. The difference is that wild animals are much more in tune with nature, their surroundings and what's good for them than most humans are.

However, there are some people who have made a point of continuing to live wild and carry on the traditions of our hunting-and-gathering ancestors. And research shows that these people tend to be happier and healthier precisely because of this stronger connection with nature. They're spending most of their time outside — farming, hunting, breathing fresh air, eating fresh produce and meat. In other words, doing the very things our bodies have evolved for.

Yet some experts have suggested that we've somehow evolved beyond our wild nature. This theory is both untrue and incredibly unhealthy.

In fact, our modern sedentary lifestyle is likely a big contributor to many of our most troubling illnesses, including obesity, heart failure, autism and cancer.

As we stuff ourselves with junk food and spend our days staring at screens while barely moving or going outside, we further disconnect ourselves from the way we are supposed to live.

Just look at the San people of Southern Africa. They hunt, farm, live in tight-knit communities and have managed to stay both physically and mentally healthy.

So perhaps there are some lessons to be learned on how we can live healthier modern lives.

> _"We are designed to be wild, and by living tamely we make ourselves sick and unhappy."_

### 3. The human mind and body are built to be empathetic and agile. 

Humans evolved to be great hunters, but this ability isn't the result of human strength and physicality; it has more to do with _empathy_, a trait that sets us apart from other species.

As we stalk our prey, we can put ourselves in its shoes, so to speak, and understand how it might react. This allows us to stalk it, set traps and keep our family fed.

Family is the other thing that has led us to become the most empathetic species on the planet.

From infancy to puberty, children depend largely on their parents for care — and newborn babies are utterly helpless. This is why we have such a strong empathetic impulse to take care of our offspring, as well as our spouse. In the wild, it is doubtful that one parent would be able to raise a child on his or her own, and so in order to form spousal relationships, we evolved the ability to understand the needs and desires of those around us.

Our physicality also makes us perfectly suited for living in the wild.

Humans have evolved to be capable of a wide range of movements to deal with the demands of both hunting and gathering. So not only can we walk and run quite well; we can crouch, bend and jump, not to mention build and handle all manner of tools. 

These skills have allowed us to survive with relatively small guts. Other animals have digestive systems that allow them to survive on simple foods like grass, but since we need different foods to survive, we're wired to be active and use our athleticism to obtain that food.

So our ancestors hunted, fished and collected fruits and nuts in order to keep themselves and their tribe well fed. To be able to do this efficiently, humans developed a keen intelligence that allows us to observe our surroundings and make the best use of our energy.

Now you probably have a better sense of why sitting in front of a computer all day can lead to depression — you're built to do so much more!

> "_Humans are the Swiss Army knives of motion."_

### 4. Exercise can lead to higher intelligence, and “wild” conditions can help you sleep. 

After a long day at the office, you're probably exhausted by the time you get home. But there is a simple way to stimulate your body and mind: start running.

And the best way to really get both your brain and body going is to avoid the treadmill and find a challenging outdoor path to run along.

When you run through the woods or along a mountainside path, your brain gets a workout along with the rest of the body. Neural pathways buzz happily as you try to safely navigate the natural world — a physical process that actually builds new connections in the brain.

One Swedish study found an impressive connection between cardiovascular fitness, such as running, and brainpower.

The study compared the intelligence and physical fitness of both fraternal and identical twins, and they found that their IQs were influenced by cardio fitness more than any other genetic factor.

Another lesson we can learn from our ancestors is how to get a good night's sleep, a challenge that many people today struggle with.

Again, part of the problem is how different modern society is from the environment humans evolved in. We used to sleep outside and, for the longest time, there was no electricity — only campfires and candlelight. Plus, there used to be other people or a watchdog to keep an eye out and make sure we were safe when we slept.

So to help you get a full night's sleep, there are some simple changes you can make to recreate the ideal conditions. If you're living alone and experiencing sleep issues, try getting a pet. And no matter what, stay away from computers and devices for at least an hour before bedtime, as they will stimulate you just as you should be winding down from the day.

### 5. Modern agriculture and processed foods have led to many of our current afflictions. 

Back when our ancestors were hunters and gatherers, no one suffered from type 2 diabetes or asthma. That's why these ailments are called "diseases of civilization."

Even though humans generally benefitted from forming communities, the formation of more sophisticated civilizations had adverse effects on diets, which in turn gave rise to new diseases.

With the rise of agriculture, humans began to grow and eat a diet that was based on corn, rice, wheat and potatoes. This presented problems. The human body is designed to get its nutrients from a variety of different sources, including fruits, vegetables, nuts and meat. Cereals such as wheat provide only a fraction of what we need.

Let's take a closer look at just one important nutrient our body needs: _omega-3 fatty acids_. Our body can't produce these on its own, and the only way we get them is through fish and other healthy free-range animals. Without it, we tend to get sick and depressed. A shortage can even lead to heart disease and inflammation.

One reason we're not getting enough omega-3s is due to misinformation about what constitutes a healthy diet.

In the last few decades, people were obsessed with cutting fat from their diets, without realizing that fatty tissues are where animals store the very nutrients we need to stay healthy.

Meanwhile, they adhered to a diet heavy on ingredients that lead to disease and illness. The worst offenders are the refined carbs and sugars that make up most processed foods and drinks.

Fast-food and soft drinks are where you'll find the highest amount of these harmful ingredients, and nowadays even baby food will contain spoonfuls of sugar.

Refined carbohydrates are especially harmful. Since they make it harder for our bodies to burn fat and gain energy from food, they lead to a number of today's biggest afflictions, including depression, obesity and type 2 diabetes.

To help avoid these problems, pass on pizza and fries and eat more fruits, fish and vegetables.

> Today, the average person in the US consumes 152 pounds of sugar each year.

### 6. Meditation and exposure to nature can improve your health and happiness. 

Imagine being completely at ease with yourself, with your thoughts flowing freely and your breathing steady and calm. These are but a few of the positive effects that meditation can bring to your life.

But that's not all. Meditation can also make us healthier by bringing us closer to nature and those around us, as neuroscientists discovered after studying meditative monks, including the Dalai Lama.

In 1992, neuroscientist Richard Davidson was challenged by the Dalai Lama to come and study Tibetan monks — and he was shocked to find that the brain of someone who practices daily meditation is completely different from the brain of someone who doesn't.

In particular, the brains of these monks were found to be more open to changes, and the monks themselves were much more in touch with nature and empathetic toward other people and animals.

You can start benefiting today, as even short daily meditation sessions can affect brain activity, helping you become healthier and happier.

Another way to improve your health is to become more connected with nature. This is especially important if you're living in a big city.

Since our ancestors were often the target of large animal predators, we are instinctively afraid of dark corners, dead ends and routes with no easy exits — all of which can make life in a big city a source of stress and fear. It's also why an apartment with a nice view is so treasured since it gives us the illusion of escape.

That view can be even more useful at increasing our well-being if it also provides a connection with nature. In a Japanese study, researchers found that the mere presence of potted plants at the workplace can reduce sick days by 40 percent!

So do yourself a favor and bring some plant life into your home — but don't forget to water it!

### 7. Being social is in our DNA, but there’s also a dark side to our tribal nature. 

Which would you rather do: Sit around a campfire with friends, while people talk, eat and sing, or sit alone in front of a computer all night with a bag of potato chips? Many people will tend to pick the first option because our preference for tribal companionship over solitude can be traced to our genes.

There's a hormone called _oxytocin_ that drives us to lead social lives, and the more receptive our body is to the chemical, the more social we tend to be.

In the past, if community bonds were weak, children and other vulnerable members of the community would be neglected and vulnerable to predators. The hormone encourages the social behavior that's been crucial to our survival.

And oxytocin also strengthens family ties. We can see this in prairie voles, small mammals that become fierce defenders of their families. Once they meet a mate, their oxytocin-levels increase, and they transform from meek mice into soldiers ready to sacrifice themselves to protect their brood.

However, a tribe isn't without its dark side.

Our desire to defend our home against intruders often takes the form of discrimination, especially against immigrants or those who are seen as a threat to the family unit, such as homosexuals.

It's also been seen as the cause behind domestic violence. If men sense that their partner might leave them, they may feel that the sanctity of their home is being threatened — even though the perceived threat is the very partner they are supposed to protect.

As we can see, evolution wields a complicated, sometimes illogical influence on our behavior today. But that doesn't mean it doesn't have a lot to tell us about who we are and how we can make a better life for ourselves, even in today's modern world.

### 8. Final summary 

The key message in this book:

**By getting more in touch with our wild ancestors, we can live a happier and healthier life. Modern civilization might strive to keep us safe, but our bodies are becoming increasingly unhealthy. Not only are there more cases of obesity and diabetes; there's been an uptick in severe illnesses like cancer, which are also the result of today's poor diets and sedentary culture. Returning to the wilder side of life isn't hard. It can be done by anyone willing to get active and let a little nature into their life.**

Actionable advice:

**Go for a wild run.**

Do you have a forest or mountain trail nearby? Then your path to happiness is right in front of you. Going for a run in wild terrain not only produces endorphins, our happiness hormone, but it also allows your body to do what it was meant to do. Our brains and bodies are nimble and great at keeping us safe when we allow ourselves to pay attention. For example, if you run on a rainy day, you will notice your body instinctively adjusting its moving patterns to the conditions: your feet will move differently and your equilibrium will improve.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Wild Diet_** **by Abel James**

_The Wild Diet_ (2015) is your guide to using the biology of fat-burning to lose weight. These blinks explain what makes high-intensity exercise, plant-based, protein-rich diets and hydration so effective, and provide you with health hacks that you can start applying right away.
---

### John J. Ratey & Richard Manning

John J. Ratey, MD, is a clinical associate professor of psychiatry at Harvard Medical School and the coauthor of the best-selling book _Spark_.

Richard Manning is an author and award-winning journalist whose works include _One Round River_ and _Against the Grain._

