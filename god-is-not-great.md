---
id: 5101706fe4b0991857b92610
slug: god-is-not-great-en
published_date: 2013-02-13T00:00:00.000+00:00
author: Christopher Hitchens
title: God Is Not Great
subtitle: How Religion Poisons Everything
main_color: DCC933
text_color: 756B1B
---

# God Is Not Great

_How Religion Poisons Everything_

**Christopher Hitchens**

_God is Not Great_ traces the development of religious belief from the earliest, most primitive ages of humankind through to today. It attempts to explain the dangerous implications of religious thought and the reasons why faith still exists today. It also helps explain why scientific theory and religious belief can never be reconciled.

---
### 1. Religious texts are clearly man-made, as their historical inaccuracies and selective accounts prove. 

In 2006, after having been hidden for years, the Gospel of Judas was published.

This work was written around the same time as the four gospels of the New Testament and also describes the life of Jesus, but, like many other gospels, it was declared heretical by the Christian church and banned.

The canonical gospels contained in the Bible were cherry-picked by the Church to reinforce their view of Jesus's teachings. The Gospel of Judas obviously didn't fit their narrative.

Such selective inclusion of certain religious texts clearly demonstrates the Bible's man-made design. Far from God creating mankind, it is we who in fact created him.

Further strong evidence for human (rather than divine) creation can be found in the religious texts that do appear in the Bible: they are full of historical inaccuracies and self-contradictions, and are based on faulty source material.

Originally, religious stories were kept alive in mostly illiterate communities by word of mouth, a notoriously unreliable way to transfer information. The authors who collected and wrote down these oral tales mixed the various stories together, creating a historically inaccurate narrative.

Consider, for example, the story of the birth of Jesus: it references various historical events as occurring at the time of his birth, but in reality these took place many years apart. Also, the Jews' flight from Egypt — a major event in the Bible — is not mentioned in any other historical record.

The original traditions were further corrupted when later religious leaders developed and strengthened their own doctrines. Stories that didn't fit the desired mold, such as the Gospel of Judas, were discarded.

### 2. Miracles are not as miraculous as they first appear. 

Jesus once famously healed a blind man he touched. Yet, if Jesus was able to cure a blind man, then why did he not simply cure all blindness? The answer: he must have been either unable or unwilling.

Miracles such as this, along with other examples of God's power, do not and cannot stand up to closer analysis. Far from being examples of a divine judgment and omnipotence, they appear to be cheap magic tricks or the results of primitive misunderstandings of natural phenomena.

It is no coincidence that most miracles took place in the distant past. In earlier times, no one had the slightest clue why events like natural disasters or solar eclipses happened, so a religious explanation was offered. In contrast, when an earthquake happens today, we know it is not the result of God's wrath but of plate tectonics.

The other case against miracles is how weak and disappointing they often seem, and how easily pleased by them believers seem to be. Consider how religious people often rejoice when they see a resemblance of the face of God in a piece of toast, or when a religious statue supposedly bleeds. The might of their all-powerful God seems to have been reduced to performing cheap and irrelevant tricks.

This mediocrity of modern miracles shows how scientific knowledge has pushed religious belief toward the fringes of society. It also demonstrates how religious people continue to act irrationally and search for proof of their faith in the most obscure places.

### 3. Far from being the guardians of morality, religions are positively immoral in their teachings. 

Religions often claim that they are the source of morality in the world. To reject religious teachings, they say, would result in a society with no ability to judge between right and wrong.

Such statements are obviously false, as non-religious people do have morals as well. In fact, historically, it is religions themselves that have promoted and spread the greatest amount of morally corrupt teachings — they are positively immoral.

Religious books contain many examples of gods allowing and even promoting such morally abhorrent practices as slavery, genocide and rape. God himself perpetrates the murder of every firstborn Egyptian in the story of the Exodus.

It is hard to consider such teachings moral, and yet religious people have often used these teachings to justify murdering, torturing and humiliating those who do not conform to the desired standards, who worship other gods or who question religious authority.

Religions also compel their believers to adhere to unrealistic and sometimes impossible commandments. For instance, religion expects its believers to resist their own biology, especially in the area of sex and sexuality. It's as if our bodies have a faulty design that leads us to temptations we must actively resist. Yet, if we are the result of God's creation, any design faults must be his, and we are thus being punished by God for his own failures.

The punishment for the slightest misdemeanors against God's commands, even if committed in thought only, is eternal damnation, without any chance of redemption. The existence of such _thought crimes_ cannot be the work of a moral being, and the guilt and fear they incite among believers is often irreversible.

### 4. Like a dictatorship, religious belief is totalitarian. 

North Korea is a unique country because it is the only one in the world that has a dead man as its leader: Kim Il Sung is the president of the country, despite having died in 1994.

This bizarre situation is a clear example of a totalitarian regime. In such regimes — Stalinist Russia and Nazi Germany being other examples — every moment of an individual's life is directed toward worshipping and obeying an all-powerful leader.

No matter how irrational the commands issued, the citizen must follow them without question. Any deviance from official orders will be met with harsh punishment.

The similarities between totalitarian forms of government and religious belief are clear, and the two merge completely in _theocracies_ : governments run on religious principles.

Governments led by religious leaders have proved to be among the most controlling and violent in world history. They act in a totalitarian manner, demand total obedience from their subjects and heavily police all areas of citizens' personal life, from their clothing and employment to their sexual relations.

Yet theocracies are not the only connection between totalitarianism and religion. The very nature of religious belief is totalitarian.

Devout followers will themselves become the willing slaves of God. They ask for his constant surveillance and judgment, and they adhere to his commands blindly, never considering how irrational or ridiculous these commands may be.

As part of their worship of an all-powerful, infallible deity, they accept and condone the ultimate punishment: eternal damnation. At least in North Korea, one can escape the government in death; whereas in religious belief, death is only the beginning of the torment and subjugation.

### 5. The way religions treat children demonstrates how blind faith encourages people to act irrationally and bizarrely. 

Religious doctrine is so rigid and outdated that many bizarre and totally irrational practices still exist within it. Nevertheless, the unbending faith among believers makes them adhere to these practices unquestioningly.

Society as a whole is also largely prepared to overlook unusual and irrational behavior committed for religious reasons, even if such behavior is dangerous.

Take, for example, the relationship between religion and childhood.

The circumcision of infant boys in the Jewish and Muslim faiths is permitted in society because it is performed for religious reasons. But strip such an act of its religious element, and it becomes genital mutilation: a clear example of child abuse.

Religions also indoctrinate children with the idea of hell and eternal damnation. Such teaching is heavily immoral, as young children are frightened into behaving in a desired way with threats of never-ending pain and suffering. They are also taught that friends and relatives who do not share their beliefs are destined for hell. With such doctrines, religious teaching puts a tremendous emotional strain on children.

In any other context, society and the law would place heavy sanctions on such acts and teachings, but the outdated, faith-based teaching of religion is allowed to practice such irrational, harmful traditions. Its believers ignore rational concerns and permit — even promote — such acts.

### 6. Religions are incapable of being tolerant – they must interfere in the lives of others. 

Those who have faith should be happy, because they at least claim their beliefs are the key to bliss and everlasting happiness.

Yet, both in history and in the modern world, religious people have shown themselves to be angry, intolerant and violent rather than positive and peaceful. Instead of taking comfort in their own salvation, they seek to impose their beliefs on others, sometimes forcefully.

Religions act intolerantly because they are insecure about other faiths and systems of knowledge. At the heart of every religion is the claim that they can provide the answer to _all_ questions. The system relies on the dominance of religious doctrine in every area. Either God is the reason for all events or he is not all-powerful. Thus, permitting alternative faiths or explanations would admit the possibility that God is not omnipotent and undermine the religious position.

This inability to abide other views manifests itself as constant conflicts between different religions and as an intolerance of so-called heretics and non-believers.

From Belfast to Beirut, religious intolerance has led to violent conflicts between faiths as people try to force their own religious beliefs onto others. Religions may profess to provide bliss in the next world, but they demand power in this one.

In the areas of the globe where religion remains at its most powerful, the price of interfering in religious affairs remains harsh. In 1989, the author Salman Rushdie went into hiding after the Islamic ruler of Iran proclaimed it was the duty of Muslims across the globe to kill him. Rushdie's crime? Writing a novel that was deemed insulting to Islam.

### 7. Though persecuted by religions, freethinkers have always been a driving force in the progression of knowledge. 

When we wish to gain new knowledge and broaden our minds, we can do so with relative ease. We are free to seek information in libraries and on the internet without fear of repression or surveillance.

We are able to do this thanks to the efforts of people who, in the past, strove to learn about the natural world even under threat of punishment. The names of the vast majority of history's non-believers and freethinkers will, however, remain hidden to us. Living in more dangerous times, many concealed their true beliefs and thoughts out of fear.

It is through close observation of science and the natural world that we have become gradually more skeptical of religion. Often by looking closely at their own surroundings, people begin to question religious teachings.

Many of the pioneers of religious skepticism who did their part to advance religious doubt and freethinking are still famous today. These include Socrates in Ancient Greece, who challenged religious orthodoxies and was executed for it, and the scientific pioneers of the nineteenth century, most obviously Charles Darwin, who shocked the world with his theory of evolution. 

Although many were not atheists themselves, they were able to strike against the strict "fossilized" dogma of organized religion, thereby weakening its power and broadening the human mind further.

In the face of religious intolerance and violence, the work of these past freethinkers has given us a deep understanding of the natural world and set the majority of us free from the repressive dominance of religious thought today.

### 8. Religion and science are fundamentally incompatible. 

Despite claims to the contrary, religion and science are diametrically opposed to each other. They both claim to provide answers to understanding the natural world, but of course only one can be right.

As knowledge of scientific principles has become widespread over the last few centuries, the role of religious belief has become increasingly marginal in many societies. Just as rainmakers in more primitive societies became unemployed when climatologists turned up, so is religion becoming increasingly redundant in the modern world.

While some religions meekly try to assure us that religion can serve to fill in the gaps in scientific knowledge, the more devout followers find religion's demise in many spheres of influence difficult to take. Some even attempt to stand in the way of scientific progress and demand that religious explanations for the natural order of things be accepted.

This can have negative and often tragic consequences. For example, creationist groups continue to demand that school children be taught the biblical story of creation as a rival explanation to evolution. Even in the face of overwhelming evidence against their "theory," such groups continue to distort the education of children to bolster their waning religious power, which entrenches religious intolerance and ignorance in new generations.

Another tragic example is the Catholic Church. The spread of AIDS could be halted through the use of condoms, but the Catholic Church finds artificial contraception an offence against God. This stance has led to less condom use and hence to the unnecessary spreading of this disease.

Despite the scientific advancements that have made modern societies richer and safer places, religion continues to resist their influence and their encroachment on what used to be its own uncontested territory.

### 9. As long as religion exists, it will attempt to impose itself on the rest of society – it must be fought against. 

Religious belief will probably never totally disappear. Despite the fact that its explanations for the natural world have been almost totally discredited in many societies, it continues to survive.

Religion is the product of a more primitive and childlike period in human history, when our ancestors had no idea why or how things like natural disasters or illness occurred. Religion provided explanations and soothed our ancestors' fears of the unknown.

As human civilization matured, we discovered scientific explanations for most things. We are, however, still evolving as a species, and we are still filled with fears about the unknown, especially death.

Religion fills these gaps in our knowledge and exploits our uncertainties about other people, our own mortality and the insecure future. As long as we remain fearful as a species, religion will continue to exist and exert power. Consider that as recently as 2005, cartoon images printed of Muhammed sparked violent protests across the Islamic world because they were considered offensive.

Hence the rational fight against religion must continue. Just as freethinkers in the past questioned and mocked religious doctrine, skeptics must carry on the struggle today.

The false assertions, tribalism and backwardness of religion must be exposed and challenged.

There can be no peace between rational and religious thought — the two are irreconcilable. The advancement of the human race must continue through rational scientific study and exploration. Hence, to protect scientific principles as well as a free society as a whole, the battle against religion must continue.

### 10. Final summary 

The key message in this book:

**Religions were manufactured at a very early stage of human development to explain the natural world. Because they rely on blind faith and the control of thought, they are dangerous to scientific progress and to freethinking and tolerant societies.**

The questions this book answered:

**How can we tell that religion is a man-made phenomenon?**

  * Religious texts are clearly manmade, as their historical inaccuracies and selective accounts prove.

  * Miracles are not as miraculous as they first appear.

  * Far from being the guardians of morality, religions are positively immoral in their teachings **.**

**Why does religious belief often lead to violence, intolerance and closed thinking?**

  * Like a dictatorship, religious belief is totalitarian.

  * The way religions treat children demonstrates how blind faith encourages people to act irrationally and bizarrely.

  * Religions are incapable of being tolerant — they _must_ interfere in the lives of others.

**Why do scientific and religious approaches often find themselves in conflict?**

  * Though persecuted by religions, freethinkers have always been a driving force in the progression of knowledge.

  * Religion and science are fundamentally incompatible.

  * As long as religion exists, it will attempt to impose itself on the rest of society — it must be fought against.
---

### Christopher Hitchens

Christopher Hitchens (1949–2011) was an British author, journalist and broadcaster. He wrote for a number of publications including _The New Statesman_, _The Nation_ and _Vanity Fair_ and was a regular commentator on various issues.

He remained a popular but divisive figure on both the left and the right and was never afraid to take a stand on controversial issues.

