---
id: 547c843c6363300009f80300
slug: going-clear-en
published_date: 2014-12-02T00:00:00.000+00:00
author: Lawrence Wright
title: Going Clear
subtitle: Scientology, Hollywood and the Prison of Belief
main_color: 475D99
text_color: 3B4E80
---

# Going Clear

_Scientology, Hollywood and the Prison of Belief_

**Lawrence Wright**

_Going Clear_ offers a rare glimpse into the secret history and beliefs of Scientology as well as the conflicted biography of its founder L. Ron Hubbard. It also details some of the Church's darker qualities: a tooth and nail method of managing criticism and systematic approach to celebrity recruitment.

---
### 1. What’s in it for me?  Find out the real story behind Scientology. 

A famous science fiction writing founder; psycho-therapeutic techniques; rumors of abuse and lengthy battles with governments all over the world: Scientology has been an irresistible mystery for decades.

Is it really a church? How was it founded? Why is Tom Cruise such a devoted member? _Going Clear_ answers all these questions, and provides a pointed look deep into Scientology's history and practices.

In these blinks, you'll learn:

  * how Scientology's founding can be traced back to a dentist;

  * why L. Ron Hubbard earned a Guinness World Record; and

  * why Hollywood stars like John Travolta are so taken with Scientology.

### 2. Scientology considers itself to be a scientific kind of religion. 

Unofficially, the organization claims eight million members worldwide. In the United States, however, only about 25,000 people label themselves Scientologists.

In truth, it's hard to know how many there are. Without baptismal records or other ritual professions of faith, estimations on membership become difficult.

This brings us to a bigger question: is Scientology a religion at all? This question, like the one above, will prove difficult to answer:

The US government officially granted Scientology religious status in 1957, three years after the organization was founded. Only ten years later, however, the IRS ruled that Scientology was, in fact, not a religion, but rather a commercial enterprise that existed to enrich its founder, L. Ron Hubbard.

Unsatisfied with this outcome, in 1977, the Church enlisted experts on religious movements to affirm the religious nature of Scientology.

Their main expert, Frank Flinn, testified that, like other religions, Scientology possesses a system of beliefs spiritual in nature, a set of behavioral norms as well as rites and ceremonies. What's more, Scientology attributes extraordinary powers to their founder, such as his visions of a supernatural world, analogous to those of Jesus, Muhammad or Abraham.

So, wouldn't Scientology, then, fulfill the defining criteria for a religious organization? At the time, the US government disagreed, and rejected the Church's appeal.

In 1993, the tides turned when Scientology regained tax-exempt status as a charitable organization.

Paradoxically, Scientology also claims to be based on science, purporting that Hubbard developed his doctrines through stringent scientific research.

Recruits are told that they will necessarily come to share Scientology's worldviews, accepting, for example, the belief in immortality, through a process of scientific realization.

In his book _Dianetics_, which serves as a central tenet of Scientology's belief system, Hubbard calls his self-help method an _engineering science._

Scientologists' insistence that their beliefs are founded in science have not gone unopposed, especially from psychiatry, which is fitting, considering Hubbard scorned and mistrusted psychiatry for most of his life.

### 3. We’re actually immortal beings imprisoned on Earth. 

Long before he conjured up Scientology, Hubbard had a vision while under anaesthetics during a dental operation in 1938. After he regained consciousness, he was convinced that the secrets of the universe had been revealed to him.

Afterwards, Hubbard claimed to have had other visions, which would later serve as the basis for Scientology's belief system. But what exactly did he "see?"

Hubbard learned that we're all _thetans_, immortal spirits re-incarnated into mortal bodies. With enough training, he posited, people can escape the prison of their bodies and freely travel through time and space.

Furthermore, by using a psycho-techniques called _auditing_, members can gain access to memories from past lives. Hubbard, for example, was furious that Machiavelli, the famous Renaissance author and statesman, had stolen his line about the "end justifying the means," which Hubbard had presumably coined in a past life.

Scientologists who advance through the ranks learn the two "incidents" that led to thetans' imprisonment on Earth:

The first incident occurred as the material world was created and thetans lost awareness of their immortality.

The second occurred some 75 million years ago, when Xenu, the tyrannical leader of the _Galactic Confederacy_, crushed the rebellion against him.

Xenu, having discovered the mutiny planned by his officers, conspired with evil psychiatrists to lure all the thetans to administration buildings for an investigation. There, their bodies were paralyzed and frozen, shipped off to the "_prison planet"_ (i.e., Earth), dumped into volcanoes and finally blown up with hydrogen bombs.

Their spirits (thetans) were then trapped in front of a gigantic motion picture that etched images into their minds — images of virtually everything we see or experience today on Earth. Then, "triggers" were implanted into their minds, assuring that each civilization on the prison planet would self-destruct, for example, through war.

The only way out of this cycle of self-destruction is to get ourselves off the manipulations of the mind by means of Hubbard's psycho-techniques.

Scientology has many similarities to modern religion, but its teachings also resemble science fiction. Our next blinks will reveal just how closely scientology and science fiction are related by looking at the life of its founder.

> _"[Psychiatry was] the sole source of decline in this universe." - L. Ron Hubbard_

### 4. L. Ron Hubbard was a prolific science-fiction writer until his crisis. 

So who exactly was the man behind the creation of Scientology?

Lafayette Ronald Hubbard was born in Nebraska in 1911. Although both his parents were Methodists, Hubbard took a deep interest in magic and shamanism, as well as psychoanalysis.

He was a poor student with the exception of his texts, both fictional and factual, which began seeing publication while he was still only in college.

At the age of 21 he married his first wife. She soon became pregnant, and Hubbard took to writing pulp fiction as a means to support his growing family.

Hubbard was an astonishingly prolific science-fiction writer, averaging 100,000 words per month between 1934 and 1936 under various pen names. In fact, throughout his life he would continue to write so proliferously that he would posthumously receive the Guinness World Record in 2006 for having published more books than any other writer: a staggering 1,084 titles!

After serving in the Navy in the Second World War, however, things took a turn for the worse as he entered a state of crisis.

Following the war, he didn't return home to his wife and children. Instead, he took up residence at the mansion of the occultist Jack Parsons, where he soon began a series of affairs, including with Parsons' mistress, Sara Northrup.

Hubbard and Northrup eventually left Parsons' mansion, at which time he began to physically abuse her. He began suffering paranoid delusions that Northrup would hypnotize him in his sleep, and his struggle to find work led to financial trouble.

He frequently threatened to commit suicide, and was eventually advised by a physician to consult a psychiatrist.

However, Hubbard was deeply mistrustful of psychiatry, and, fearing that he might be hypnotized by someone in the medical profession, resolved instead to pull himself out of his crisis.

### 5. To heal himself, Hubbard developed a system of psycho-techniques called Dianetics. 

_Dianetics_ is Hubbard's theory of the human/thetan mind, armed with a system of techniques aimed at dealing with painful memories that supposedly impede our mental and spiritual progress.

So how exactly does Dianetics work? Let's use Hubbard's own experience as an example:

First, Hubbard would identify and confront painful memories. For example, he recognized the shame he felt in being only a mediocre soldier, so he forced himself to visualize his experiences as a soldier again and again, resolving not to deceive himself or others about his lackluster record.

Next, he would counter the negative effect of those memories with positive statements, using self-hypnosis to help them become ingrained in the mind.

He would remind himself, for instance, of his talent for writing, and tell himself that his physical pain wasn't real and that he would live 200 years.

Having developed his system, Hubbard moved to Hollywood, set up shop as a Dianetics coach and began treating others.

He began treatment with a process called _auditing_, in which he would hypnotize his clients, helping them to retrieve early painful memories, which he would then "erase" using post-hypnotic commands.

But we don't have to forget every traumatic experience in order to overcome them. Some subjects even reported discovering previously "forgotten" experiences from past lives, which they were able to overcome during hypnosis.

Hubbard eventually put his techniques to paper in 1950 with _Dianetics: The Modern Science of Mental Health_, in which Hubbard boasts that Dianetics can produce results in fewer than 20 hours of treatment.

_Dianetics_ was wildly successful. According to _Newsweek,_ 55,000 copies sold in the two months after publication alone!

Hubbard's system became essential to Scientology. However, without the Church, it could have easily fallen into obsolescence.

> _"[Dianetics] probably contains more promises and less evidence per page than has any publication since the invention of printing." — Nobel Physicist Isaac Rabi_

### 6. Once interest in Dianetics faded, Hubbard founded Scientology, at least in part to earn some cash. 

Religion had never been an integral part in the life of L. Ron Hubbard. Sure, he'd been interested in shamanism, had experienced strange visions, but what exactly motivated him to found his own church all of a sudden?

The answer is closely tied to the fate of Dianetics: at first, _Dianetics_ sold well — at least well enough for Hubbard to create six foundations with the purpose of training auditors. But Dianetics suffered from two problems.

The first was practical: Dianetics has a conclusion. Since the focus of Dianetics is to overcome trauma, people become "cured" once all the trauma has been managed. And while, according to Hubbard, thetans can suffer myriad traumas throughout their many years of reincarnation, auditing is no longer useful once all of those traumas have been managed.

Second, interest in Dianetics plummeted, and the foundations were forced to declare bankruptcy in 1952. As a result, Hubbard even had to sell the rights to the name _Dianetics_ in the same year _._

Then, Hubbard stumbled onto a realization that would change his life:

Whereas Dianetic therapy aims to overcome our problems, religious organizations make no such claims. Instead, they offer pre-packaged belief systems, along with a community that won't dissolve once their problems are solved.

What religions _do_ promise, however, is salvation, which makes them continuously relevant for their followers.

Hubbard was absolutely aware of this when he founded the _Church of Scientology_ in 1954, which he likely founded, among other reasons, to make money and gain popularity.

In a previous letter to one of his executives, Hubbard mused that creating a spiritual guidance center would be a good way to keep _Hubbard's Association of Scientologists_ solvent. He had always dreamed of becoming popular, and being the focal point around which the Church revolved was a promising means of achieving this.

It was against this backdrop that he founded the first two Churches, one in L.A. and another one in Washington, D.C.

At this point, you've seen how Scientology came to be. But that doesn't explain its continued popularity. The following blinks will explain.

> _"I'd like to start a religion. That's where the money is." — L. Ron Hubbard_

### 7. When Scientologists recruit prospective members, they leave nothing to chance. 

Long before Paul Haggis became the renowned director and screenwriter responsible for hits like _Crash_ and _Million Dollar Baby_, he was approached by a sympathetic young Scientologist who handed him a copy of _Dianetics_. The two soon became involved in a lively discussion which ultimately led to Haggis' conversion to Scientology.

But was this animated conversation a chance encounter in which two like-minded people simply clicked? Not quite.

Scientology has a methodical recruitment process composed of four steps: _make contact_, _disarm_ the recruit, _identify_ his problem and _convince_ him that Scientology has the solution.

Recruiters for Scientology methodically build rapport with prospective new members in order to increase the likelihood of conversion.

Before taking any action, recruiters are coached thoroughly on how to win over their targets by identifying and disarming any antagonism towards Scientology.

Haggis, for example, confided that he was an atheist. A recruiter, therefore, could expect him to resist dogmatic belief systems. Having correctly assessed this hurdle, recruiters assured Haggis that no one had the right to force him to believe anything.

Moreover, recruiters follow a predefined strategy to convince prospective members to enroll in their courses.

Recruiters, for example, are taught how to garner enough trust from strangers that they become comfortable disclosing their personal problems. It was this trust that got Haggis talking about his stormy relationship with his girlfriend.

Once the prospective member has aired out his deep troubles, it often becomes possible to convince him that Scientology can offer a viable solution. In Haggis' case, the recruiter recommended a course for the conflicted couple.

In addition to this active recruiting, the Church also wins new members through psychological testing, management seminars and the drug rehabilitation program _Narconon_.

> _"Scientology works 100 percent of the time when it is properly applied to a person who sincerely desires to improve his life." — Church of Scientology of Washington, DC_

### 8. Scientology has always set it sights on celebrities. 

More than any other religion, Scientology has a close relationship with stardom. Many celebrities, for one reason or another, were sympathetic towards Scientology, or even became members. The list of celebrity Scientologists is long and includes such notables as John Travolta, Kirstie Alley, Priscilla Presley, Anne Archer, Juliette Lewis and Tom Cruise _._

This is no coincidence; Scientology has always placed great emphasis on recruiting celebrities. Hubbard correctly figured that the best way to establish Scientology's image as a path to enlightenment would be to recruit captivating public figures, such as film stars.

As early as 1955 — only one year after Scientology's founding — members were urged to recruit celebrities. They were presented with a list of desirable targets, including such names as Marlene Dietrich and Walt Disney, and were promised a reward if they succeeded in "bringing one of them home."

Even today, celebrity members such as Tom Cruise act as advocates for the church, appearing at galas and workshops to add to the organization's allure.

In order to attract famous people specifically, Hubbard established his first _Celebrity Center_ in Hollywood in 1969. Today there are satellite centers in several large American and European cities.

The prestige of these centers is meant to attract stars as well as the aspiring young actors who might see the Celebrity Centers as prime opportunities to rub elbows with high-profile members of the movie business.

Generally, these star recruits are offered special courses and facilities as a way to hold their attention. For example, Scientology's most prominent celebrity, Tom Cruise, consults directly with David Miscavige, Hubbard's protégé during the 1980s and eventual successor as the leader of the organization.

When Miscavige first learned about Cruises' affiliation with the Church, he invited him to exclusive locales and assigned his best people to audit the star.

On one occasion, members of the organization were recruited to repair and renovate Cruise's home, and he even spent his forty-second birthday glamorously celebrating on board Scientology's cruise ship, the _Freewinds_.

### 9. Scientology goes to great lengths to gain political influence and fight adversaries. 

The Church of Scientology has been involved in some operations that truly sound like they could have come out of a spy novel.

For example, _Operation Snow White_ was a real attempt to gain influence over governments worldwide. Starting in the late 1960s, Hubbard felt that Scientology was facing steadily growing animosity coming from the American and British governments. France, too, was supposedly planning to indict the Church for fraud.

Hubbard's response was the initiation of Operation Snow White in 1973 _._ In this operation, 5,000 Scientologists infiltrated 137 government agencies worldwide in order to seek out any disparaging files on the church.

In Germany, these include major organizations such as Interpol, the national police and the Offices of Immigration. In the United States, Scientologists infiltrated major institutions such as the IRS, Federal Trade Commission, Justice Department, American Medical Association and several newspapers that took a critical stance on Scientology.

The conspiracy was uncovered in 1977, and resulted in prison sentences for eleven Church members.

The Church also goes to great lengths to ruin critics.

For example, when journalist Paulette Cooper wrote a critical article about Scientology in 1973, the Church responded with overwhelming force: among other things, they attempted to sue her nineteen times, followed and harassed her, tapped her phone and sent her neighbors letters purporting that Cooper was a prostitute and a child molester.

She was even accused of sending a bomb threat to Scientology headquarters.

In 1977 an FBI raid of Scientology's offices uncovered a file on _Operation Freakout_, outlining its goal: to get Cooper "incarcerated in a mental institution or jail."

According to the investigative reporter Richard Behar and many others, this incident is no exception.

In his 1991 _Time_ article "Scientology: The Thriving Cult of Greed and Power," he explains how critics of the organization often find themselves threatened by ligation, followed by private eyes and are subject to false accusations.

### 10. The history of Scientology is peppered with evidence of abuse. 

Scientology's harassment of critics is just the tip of the iceberg. Hubbard himself was violently abusive, verging on maniacal.

Hubbard's second wife, for example, recounts that Hubbard repeatedly kicked her in the stomach during her pregnancy with the intent to cause a miscarriage. Hubbard's first son, a child of another mother, relates similar stories of violence.

This abuse reached beyond his family, and carried over into the organization itself. For example, in the 1970s Scientology punished some of its members by confining them to a dark basement.

When the FBI raided buildings owned by the Church in 1977, they also searched through the Cedars of Lebanon Hospital, one of the Church's more recent purchases. In the unlit basement, they discovered a group of about 120 terrified people huddled together in small cubicles, whose degraded status was indicated by the rags they wore around their arms.

These quasi-prisoners were part of Scientology's so-called _Rehabilitation Project Force_ (RPF), which was comprised of a mix of hard labor and Hubbard's spiritual healing techniques.

The events that led to their incarceration, however, remain unclear.

David Miscavige, too, has been accused of abusive behavior. In fact, eleven former members of the church claim that Miscavige would beat them, and one former member claims that Miscavige knocked her to the ground with a flying tackle!

Another executive was sent to the RPF, where he was allegedly forced to run around a pole in the desert for 12 hours a day.

But why? The answer is unclear. The RPF exists in part as a way for members to pay off debts accumulated for courses they've participated in on credit. However, the physically and emotionally abusive aspects of the RPF, as well as its carceral nature, make clear that it is also an instrument of punishment.

Unsurprisingly, the Church of Scientology denies all allegations of abuse.

### 11. Final summary 

The key message in this book:

**Scientology is among today's most enigmatic organizations. Part religion, part secret society, part gulag and part celebrity meet-and-greet, Scientology is a strange mix of religious ideology and racketeering, whose history and legacy are thereby made all the more fascinating.**

**Suggested further reading:** ** _God Is Not Great_** **by Christopher Hitchens**

_God is Not Great_ traces the development of religious belief from the earliest, most primitive ages of humankind through to today. It attempts to explain the dangerous implications of religious thought and the reasons why faith still exists today. It also helps explain why scientific theory and religious belief can never be reconciled.
---

### Lawrence Wright

Lawrence Wright is an author and screenwriter, as well as a staff writer of the _New Yorker_ and a member of the Council on Foreign Relations. He has written a number of plays and critically acclaimed books, including _The Looming Tower: Al-Qaeda and the Road to 9/11,_ for which he won a Pulitzer Prize.

