---
id: 537232c962643900072a0000
slug: good-strategy-bad-strategy-en
published_date: 2014-05-13T12:41:51.000+00:00
author: Richard Rumelt
title: Good Strategy, Bad Strategy
subtitle: The Difference and Why It Matters
main_color: BFB75F
text_color: 736E39
---

# Good Strategy, Bad Strategy

_The Difference and Why It Matters_

**Richard Rumelt**

_Good Strategy, Bad Strategy_ dissects good strategies by using historical examples from a variety of fields, and offers insight into developing our own effective strategies through practical advice and a solid blueprint.

**This is a Blinkist staff pick**

_"My boss kept asking me, "Emily, what's your strategy?" and I kept coming up empty (#truestory). This title is a smart start to wrapping your head around what it means to build an effective strategy and do good work to reach your goals._ _"_

– Emily, Community & Engagement Marketing at Blinkist

---
### 1. What’s in it for me? Learn how to become an effective strategist. 

What separates winning strategies from losing ones? Ultimately, both set out to achieve the same goals, and yet their outcomes are vastly different.

As it turns out, part of this difference lies in what we actually consider to be a strategy; in other words, losing strategies may not even be strategies at all!

These blinks offer you insights into strategic thinking by drawing from the experiences of successful — and sometimes disastrously unsuccessful — cases. By examining the wisdom gathered from these different examples, you'll learn about the anatomy of a successful strategy, how to apply it to your life or business, and how to become an effective strategic thinker.

You'll also learn:

  * why you should be wary of sexy-sounding fluff

  * how one man's epiphany in Italy lead to a chain of 4,500 coffee shops

  * how to look beneath the obvious to find the hidden powers of a situation and maximize their impact

  * how a little geometry led to the death of 50,000 Roman soldiers

  * and finally, you'll grasp what all good strategies have in common — and what the bad ones lack.

### 2. Don’t confuse strategy with ambitious goal setting, visions or slogans. 

So what is a strategy, exactly?

Consider this: the "2005 key strategy" of a major graphic arts company was a 20 percent revenue increase and a 20 percent profit margin. Does this sound like a good strategy to you?

The short answer: No. In fact, these are simply goals — far removed from a working strategy.

A vision or a goal is simply a stand-alone idea. A _strategy_, however, is a set of different ideas that includes a plan to achieve these goals.

Often a goal or a vision can be a perfectly fine starting point for a strategy. However, the strategy itself must include precise information on how these goals will actually be achieved.

For example, if your football coach advises your team to win the next game, he isn't providing you with any useful information unless he tells you _how_ to win. In other words, he must provide a plan of action — a _strategy_.

It's not only our goals that are often mistaken for strategies; motivational slogans and buzzwords sometimes get passed off as strategies too.

This is usually made obvious by an absence of clear, simple words. In these cases, "fluff" — superficially restating the obvious while applying a heaping portion of buzzwords — takes on the appearance of high-level thinking.

The fundamental strategy of one major retail bank is a perfect example — in their own words, they offered "customer-centric intermediation."

Let's unpack this, shall we? "Intermediation" means simply that they take deposits and lend them to others, and "customer-centric" means that they focus on the customer.

By taking the fluff and unraveling it into simple, meaningful language for the layman, we quickly discover that their "fundamental strategy" for banking was simply "be a bank!"

What's missing in both these business examples is a plan of action. Essentially, if you have no plan of action, then you don't have strategy.

### 3. Every good strategy has the same foundation: a diagnosis, a guiding policy and a set of coherent actions. 

It's natural for each strategy to look different, since each is tailored to meet one specific need. However, there is one essential component to every successful strategy: the _kernel_.

The kernel is composed of three parts: the first two are the _diagnosis_ and the _guiding_ _policy_.

The _diagnosis_ is a simple analysis of an often complex set of circumstances, and the _guiding_ _policy_ lays out the approach that will be used to tackle this diagnosis.

One example of these components at work can be found with IBM in 1993. During that year, IBM was on the decline — their previously successful strategy of offering complete computers no longer worked in a fractured industry where many computer companies had started offering individual parts.

While many believed that they should adapt to this fragmentation, CEO Lou Gerstner developed a different diagnosis: instead of fragmenting IBM's different departments, they would instead integrate and centralize them to become new market leaders in IT consulting.

In order to enact this diagnosis, they then created a new guiding policy that focused company resources on customer solutions.

The final component of the kernel is a _set_ _of_ _coherent_ _actions_ that ensures that the guiding policy is effective. In other words, the actions needed to achieve the goals of the guiding policy can't contradict one another.

One example of a failure to establish coherent actions can be found in the Ford Motor Company.

When they acquired Volvo, Jaguar, Land Rover and Aston Martin, their new guiding policy was to exploit these brands and simultaneously take advantage of economies of scale. This led them to consolidate the design and manufacturing processes between the brands.

However, this approach lacked coherence, since the value of these brands lay in their unique qualities. Volvo buyers don't want a safer version of a Jaguar, just as a Jaguar buyer doesn't want a sportier version of a Volvo, and Ford's strategy suffered for it.

### 4. A good strategy demands that you make a choice to move in one specific direction. 

Most people don't like to choose between two things — they'd rather just have both. Unfortunately, this is rarely possible — when forming a strategy, the choice of one thing over another is simply unavoidable.

A good strategy demands that you prioritize what's most important and focus your resources there — trying to have it all will leave you struggling.

For example, in 1988 computer manufacturer Digital Equipment Corporation (DEC) was struggling to compete with a new type of PC. Executives couldn't decide how they should respond to this new situation: they were torn between focusing on building ready-to-use systems, customer solutions or new microchip technology.

They were told to find a compromise, but they just couldn't, and ended up failing to choose a single direction and act on it.

_Finally_, in 1992, their new CEO decided that they would focus on chips, but it was too little too late. By then they had missed the boat and been overtaken by competitors, and were eventually bought out by a rival company.

Unfortunately, tough choices will often damage other areas of the business, and thus encounter strong opposition. However, you must have the constitution to overcome this and drive through the decision anyway.

For example, Intel found itself having to make hard choices after many Japanese companies in the manufacturing market made it tough for them to compete. Their CEO, Andy Grove, was forced to make a decision and shift the company's focus to manufacturing microprocessors.

This move met opposition from many areas of the business, including the salespeople and researchers, who were attached to the old habits and processes.

While it might have been less stressful to back down in the face of opposition, Grove instead persevered with this new direction. And a good thing, too: by 1992, Intel was the world's largest manufacturer of semiconductors.

Now that you've learned about the anatomy of a good strategy, the following blinks will explore how these strategies can be applied for maximum effect.

### 5. In order to profit from your strategy, you need to ensure that it gives you leverage over your rivals. 

Once you have a strategy,how do you use it to gain an advantage over your competitors?

In short, you need to ensure that your strategy offers you _leverage_ over your competitors, i.e., that you anticipate opportunities before your competitors have a chance to act.

Anticipation, however, doesn't mean foreseeing the future; it means having an insight into the present that enables you to spot emerging possibilities and act on them.

We can gain some insight into leverage by looking at Toyota.

Even while they were profiting from booming SUV sales, Toyota invested more than $1 billion into engineering hybrid electric-gasoline technologies. Why?

Their insights had shown them that the dwindling supply of fossil fuels would eventually drive demand for hybrid cars, and that if they could pioneer the first acceptable hybrid technology, other manufacturers would then license their system rather than develop their own.

In other words: they would gain leverage.

In order to anticipate an opportunity (and thus gain leverage), you must identify your market's _central_ _pivot_ _point_ — meaning the best way to succeed in that field.

The convenience store chain 7-Eleven did precisely that in Japan when they discovered that their Japanese customers easily became bored with the same selection of soft drinks. To exploit this, they identified the central pivot point: variety.

To accomplish this, they created a strategy around this point.

The average 7-Eleven store can stock only 50 varieties of soft drinks — only a fraction of over 200 brands available in Japan — so the firm created a system to maximize soda variety on their shelves by researching and recording local tastes. Each store would then sell a range of brands that appealed to those local tastes, thereby allowing 7-Eleven to shift an enormous variety of drinks.

By identifying and focusing on the pivot point, 7-Eleven gained leverage over rivals whose stores lacked what the Japanese market wanted.

### 6. Find the right balance of resources and actions to fit your specific situation. 

By now you might be thinking that you've fleshed out a good strategy for your business. There is still some important work to be done! You should ask yourself: do I have the resources for this, and does it reflect my current situation?

A good strategy is composed of actions that are based on your current situation and all fit together to maximize your advantage.

One classic example of a pristine strategy can be found in ancient history with the military maneuvers of the Carthaginian general Hannibal. When Hannibal invaded the Roman Empire in 216 BC, he soon ran into trouble: at the battle of Cannae, he was met by a Roman army that outnumbered his troops by 85,000 to 55,000.

So what did he do? He created a strategy based on his limited resources and current situation.

First, he had his men form an arc, the middle of which then pulled backwards to feign a retreat as the Romans approached. The Romans then charged after the retreating soldiers and thus into the gap Hannibal's army had left. As more Romans poured forward, they became crammed into a smaller and smaller space until they couldn't properly swing their weapons. The sides of the arc then circled behind the Romans, and the slaughter began.

Over 50,000 Romans died that day, compared to Hannibal's 5,000 or so losses.

So what can we take away from this gruesome story? Hannibal carefully considered every single action in his strategy so that each one naturally followed the last. Thus his strategy won him a decisive victory in spite of the odds against him.

The best strategies, such as Hannibal's, recognize the trade-off between resources, possible actions, and the optimization of both. Like Hannibal, you should strive to ensure that your strategy utilizes your limited resources in the most efficient way possible.

> _"Good strategy and good organization lie in specializing in the right activities and imposing only the essential amount of coordination."_

### 7. You can use the dynamics of changing business circumstances to gain the high ground in a market. 

The world of business is one that is constantly changing; it's lucrative to develop strategies that take advantage of these shifts, but how do you make it happen?

In many cases, the biggest effects of changes will be so obvious that they can't offer you any clear advantages. However, with these effects come many other opportunities if you look for the less obvious _second-order_ _effects_.

For example, when television first emerged, it was easy to anticipate that free TV would produce huge competition for movie theaters.

Yet there were many second-order effects, which were much harder to predict. One involved the organization of Hollywood studios.

In the past, these studios had a captive audience. But that audience disappeared with the advent of the television, so they had to make up for this lost revenue by taking up roles financing independent films that could draw specialist audiences to the cinema.

Indie directors and writers therefore benefited from major studio funding to produce their specialist films, thus enjoying the hidden second-order effect of television's introduction to the market.

However, in some markets changes are relatively rare due to the prohibitive cost of improving the existing technologies. In these cases, you can _create_ changes through innovation.

For example, in the 1960s black and white photographic film had been improved upon to the point where investments in fresh research no longer provided a worthwhile return. This made it difficult for newer companies to challenge the Goliaths of the industry, such as Ilford in the UK and Ansco in the United States.

However, a few smaller companies, such as Kodak and Fuji, did challenge these giants' market share by developing color film, where the opportunities were far more plentiful. By further developing this new technology and making it more effective, these companies instigated a change in the market and managed to rise to the top.

### 8. A good strategy maximizes your competitive advantage by limiting your rivals’ opportunities and maximizing your resources. 

Now that you understand the nuts and bolts of a strategy, it's time to ask yourself _how_ a strategy turns visions into realities.

In most cases they do this by maximizing _competitive_ _advantage_ — namely the ability to produce more value at lower costs than competitors.

So how can you engineer a strategy that accomplishes this?

One way is to use _isolation_ _mechanisms_, which give you a competitive advantage in an area by limiting your competitors' opportunities.

For example, Apple's iPhone is protected by many isolation mechanisms working in concert: the brand name, the company's reputation, and then the complementary iTunes service with its huge media database.

These factors make it difficult for competitors to create rival products; not only are they competing with that product, but also with its marketing, its comprehensive operating system and its image.

Apple has so effectively cornered its market that the only opportunity for a competitor to enter would be to sell a comparable product at a lower price, which is certainly no small feat.

Another way to achieve competitive advantage is by creating a higher demand for any of the resources you have at your disposal.

One example of this is the marketing methods of the POM Wonderful pomegranate juice company. While pomegranates were originally only a minor crop in their orchards, the owners realized that they made more money per acre from pomegranates than from other crops.

They then invested in research, discovering previously unknown health benefits from pomegranates, and bought 6,000 acres for cultivating pomegranates, thus increasing US pomegranate production capacity _six_ _fold_.

They then starting selling their pomegranate juice as a fresh fruit drink, emphasizing its many health benefits. And because they were now the largest producer of pomegranates, they were able to reap all the rewards of the high demand that they had created, without having to share with other manufacturers or competitors!

Now that you've learned how to successfully implement your strategies, the following blinks will reveal how to become a great strategist.

### 9. Approach strategy like a science: form a strategic hypothesis to test and then refine your ideas. 

So you now know about the components of a good strategy and how they can and have been used in real-life scenarios, but how can you become an effective strategist?

You must first start making _strategic_ _hypotheses_ — educated estimations of how a given situation works or _could_ work, which help when you contemplate your strategy.

As an example, consider this hypothesis, developed by Howard Schultz after a visit to Italy in 1983: "The Italian espresso experience could be re-created in America and the public would embrace it."

Schultz was awed by the grace and flair of Italian coffee bars, where expensive coffee was served in a relaxed social setting, and struck by the contrast to the coffee culture in America, where the mass market was comprised of cheap, bland coffee.

Having developed his hypothesis he decided to test it by convincing his employers at a roasting company in Seattle to give him space to set up a small espresso bar.

That company was Starbucks.

Like Schultz, you too should test your hypothesis to gather new information, and form new hypotheses based on those results.

Schultz started by copying the Italian original, but he soon noticed that Americans preferred lounging in chairs to standing at the bar, so he introduced chairs and tables.

He then discovered that many American customers wanted their coffee to go, so he introduced paper cups.

What can we learn from his success?

He tested his hypothesis, which was recreating the Italian espresso experience — but needed to modify it for American preferences in order for them to truly embrace it.

His company then purchased Starbucks' retail and trademark in 1987, and by 2001 brought in $2.6 billion in revenue.

Indeed, strategy is much like a science: we must come up with a possible explanation, or working hypothesis, and constantly refine it.

### 10. Avoid fatal mistakes by looking at your situation from an outside perspective and learning from others’ past failures. 

Statistically, you're five times more likely to have a car accident if you're talking on a cell phone while driving — roughly the same as if you're drunk. However, though many people are aware of this fact, when faced with a decision they tend to think: "It won't happen to me, I am a good driver."

This phenomenon is called the _inside_ _view_ — the tendency to ignore lessons others have learned in a similar situation and to believe that our specific situation is somehow different.

Many of us follow the inside view, and so often the results are fatal.

The 2008 financial crisis is a perfect example. In the years preceding the crisis, it was widely believed that the economic histories of other nations were no longer relevant to modern America, and many thought that the Federal Reserve's expertise had finally eliminated economic booms and busts entirely.

This inside-view thinking caused people to ignore problems within the system, which then led to the greatest financial crisis in half a century.

In order to avoid this disastrous way of thinking, we should seek to gain perspective by closely scrutinizing a situation from the _outside_ _view_.

A good strategy does this by asking: _Why_ have other people in a similar situation to mine succeeded or failed? In addition, a good strategy recognizes that in the _vast_ majority of cases, our situation is far less unique than we might imagine.

The 2008 financial crisis could have been avoided if analysts had looked at financial history with an outside view, and realized that crises always happen. Similarly, how many accidents do you think could have been avoided if people looked at the statistics with an outside view?

Good strategies benefit from paying close attention to other people's experiences and the lessons they've gained from those experiences.

### 11. Final Summary 

The key message in this book:

**Luckily we can all learn how to become good strategists. By understanding what makes a solid strategy and by finding the hidden power of a given situation, be it leveraging, maximizing your resources or anticipating change, you too can become an effective strategist.**

Actionable advice:

**Get your priorities straight**.

Trying to accomplish all your goals at once makes for an ineffective strategy. Instead, make a list with the most important things that you can work on and start with them first. By producing this list, you ensure that your various objectives have unity in direction, and can then clear up conflicting issues.

**Take lessons from the past.**

If you find yourself in a situation that other people have experienced previously, pay close attention to how it turned out for them. Try to look at your own situation objectively rather than assuming that it's somehow different to what happened in the past.

**Think like a scientist.**

If you feel uncertain about what the deciding factors in a situation are, form a hypothesis and test it out. This test will offer you insight into which areas need to be readjusted, thus helping you move closer to a solid understanding of your situation that you can use to build an effective strategy.
---

### Richard Rumelt

Richard Rumelt holds the Harry and Elsa Kunin Chair in Business and Society at UCLA Anderson School of Management. He has also been named by _The Economist_ as one of the 25 living people with the strongest influence on management concepts and has been described as "a giant in the field of strategy" by _McKinsey Quarterly_.

