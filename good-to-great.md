---
id: 509cd3d4e4b0a13d41357def
slug: good-to-great-en
published_date: 2013-01-13T00:00:00.000+00:00
author: Jim Collins
title: Good to Great
subtitle: Why Some Companies Make the Leap...And Others Don't
main_color: E5322D
text_color: BF2A26
---

# Good to Great

_Why Some Companies Make the Leap...And Others Don't_

**Jim Collins**

_Good to Great_ (2001) presents the findings of a five-year study by the author and his research team. The team identified public companies that had achieved enduring success after years of mediocre performance and isolated the factors which differentiated those companies from their lackluster competitors.

These factors have been distilled into key concepts regarding leadership, culture and strategic management.

---
### 1. Good-to-great companies can teach others how to make the same leap. 

Jim Collins's previous best seller _Built to Last_ explains how great companies sustain high performance and stay great. Most companies, however, are not great. Hence the more burning question: How do companies go from good to great? What do they do differently from their competitors who stay mediocre at best?

To answer these questions, Jim Collins and his research team studied three groups of public US companies in a five-year project:

_Good-to-great companies_, which had been performing at or below the average stock market performance for 15 years, before making a transition to "greatness," in other words generating cumulative returns of at least three times the general stock market over the next 15 years.

_Direct comparison companies_, which remained mediocre or dwindled although they had roughly the same possibilities as the good-to-great companies during the time of transition.

_Unsustained comparison companies_, which made a short-lived transition from good-to-great, but slid back to performing at a level substantially below the stock market average after their rise.

Over the course of their research, Collins and his team examined over 6,000 press articles and 2,000 pages of executive interviews. The goal was to discover what the good-to-great companies had done differently, and thus help other companies make the same leap.

**Good-to-great companies can teach others how to make the same leap.**

### 2. Finding a simple “Hedgehog concept“ provides a clear path to follow. 

Imagine a cunning fox hunting a hedgehog, coming up with a plethora of surprise attacks and sneaky tactics each day to devour the tasty critter. The hedgehog's response is always the same: curl up in a spiky, unbreachable ball. Its adherence to this simple strategy is the reason the hedgehog prevails day after day.

Good-to-great companies all found their own simple _Hedgehog concept_ by asking themselves three key questions:

What can we be the _best in the world_ at?

What can we be _passionate_ about?

What is the _key economic indicator_ we should concentrate on?

At the intersection of the questions, after an average of four years of iteration and debate, good-to-great companies eventually discovered their own simple Hedgehog concept. After that point, every decision in the company was made in line with it, and success followed.

Consider the drugstore chain Walgreens. They decided simply that they would be the best, most convenient drugstore with a high customer profit per visit. This was their Hedgehog concept, and by pursuing it relentlessly they outperformed the general stock market by a factor of seven.

Their competitor, Eckerd Pharmacy, lacked a simple Hedgehog concept and grew sporadically in several misguided directions, eventually ceasing to exist as an independent company.

**Finding a simple "Hedgehog concept" provides a clear path to follow.**

### 3. Success comes from many tiny incremental pushes in the right direction. 

In retrospect, good-to-great companies seemed to go through a sudden and dramatic transformation. The companies themselves, however, were often unaware they were even in the midst of changing at the time: their transformation had no defined slogan, launch event or change program.

Rather, their success was a sum of tiny, incremental pushes in the direction of their simple strategy, the Hedgehog concept. Like pushing a flywheel, these small improvements generated results which motivated people to push further, till enough speed was gathered for a breakthrough. Unwavering faith and adherence to the Hedgehog concept was rewarded by a virtuous circle of motivation and progress.

Consider Nucor, the steel manufacturer which outperformed the general stock market by a factor of five. After battling the threat of bankruptcy in 1965, Nucor understood they could make steel better and cheaper than anyone else by using mini-mills, a cheaper and more flexible form of steel production. They built a mini-mill, gained customers, built another one, gained more customers, and so on.

In 1975, CEO Ken Iverson realized if they just kept pushing in the same direction, they could one day be the most profitable steel company in the US. It took over two decades, but eventually the goal was reached.

Comparison companies did not strive to consistently build momentum in one direction, instead trying to change their fortunes with dramatic changes and hasty acquisitions. Since these delivered lackluster results, they became discouraged and were forced to once again attempt a change, not letting the flywheel gather speed.

**Success comes from many tiny incremental pushes in the right direction.**

### 4. New technology should be viewed only as an accelerator toward a goal, not as a goal itself. 

Good-to-great companies used new technology primarily to accelerate their momentum in the direction they were already going in, but never to indicate the direction itself. They saw technology as a means to an end, not the other way around.

Comparison companies often felt new technologies were a threat, and worried about being left behind in a technology fad, scrambling to adopt them without an overarching plan.

Good-to-great companies, on the other hand, carefully contemplated whether the particular technology could accelerate them on their path. If so, they became pioneers of that technology, otherwise they either ignored it or merely matched their industry's pace in adoption.

Walgreens, a major drugstore chain, provides an excellent example of how new technology can best be harnessed.

At the beginning of the e-commerce boom, the online drugstore company Drugstore.com was launched amid major market hype. The mere perception of being slower in adopting online business cost Walgreens 40 percent of its share value, and the pressure was on for them to lunge at this new technology.

Rather than yielding, they considered how an online presence could help them in their original strategy: making the drugstore experience even more convenient and further raising profits-per-customer.

Just over a year later, they launched Walgreens.com, which advanced their original strategy through, for example, online prescriptions. While Drugstore.com lost nearly all of its original value in a year, Walgreens bounced back and almost doubled its stock price in the same time.

**New technology should be viewed only as an accelerator toward a goal, not as a goal itself.**

### 5. Level 5 leaders drive successful transformations from good to great. 

All good-to-great companies enjoyed _level 5 leadership_ during their transition.

Level 5 leaders are not only excellent individuals, team members, managers and leaders, but also single-mindedly ambitious on behalf of the company. At the same time they remain humble. They are fanatically driven toward results, and want their company to continue performing even after they leave.

Far from being ego-driven, level 5 leaders are modest and understated. Level 5 leaders share the credit for their company's achievements, downplaying their own role in them, but are quick to shoulder blame and responsibility for any shortcomings.

Take for example Darwin Smith, who transformed Kimberly-Clark into one of the leading paper consumer goods companies in the world. He refused to cultivate an image of himself as a hero or celebrity. He dressed like a farm boy, spent his holidays working on his Wisconsin farm and often found his favorite companions among plumbers and electricians.

By contrast, two out of three comparison company CEOs had gargantuan egos that were counterproductive for the long-term success of the company. This was most evident in the lack of succession planning.

For example Stanley Gault, the legendarily tyrannical and successful CEO of Rubbermaid, left behind a management team so shallow that under his successor, Rubbermaid went from Fortune Magazine's most admired company to being acquired by a competitor in just five years.

**Level 5 leaders drive successful transformations from good to great.**

### 6. The right people in the right place are the foundation of greatness. 

Asking "who" must take precedence over asking "what." The transformation from good to great always began with getting the _right people_ into the company and the _wrong people_ out of it, even before defining a clear path forward.

The right people will eventually find a path to success. When Dick Cooley took over as the CEO of Wells Fargo, he realized he could never understand the major changes that would follow from the imminent deregulation of the banking industry.

But, he reasoned that if he got the best and the brightest people into the company, somehow together they would find a way to prevail. He was right. Warren Buffett subsequently called Wells Fargo's executives "The best management team in business," as the company prospered spectacularly.

Good-to-great companies focused more on finding people with the right character traits rather than professional abilities, reasoning that the right people can always be trained and educated.

With the right people, companies did not need to worry about how to motivate them. They focused on who they paid, not how they paid, and created an environment where hard workers thrived and lazy workers left. In top management, people either jumped right off or stayed for the long run.

Good-to-great companies never hired the wrong person even if the need was dire, but hired as many right people as were available, even without specific jobs in mind for them.

When good-to-great companies did see they had the wrong person, they acted immediately. They would either get rid of them or try to cycle them to a more suitable position. Delaying dealing with the wrong people only frustrates the rest of the organization.

**The right people in the right place are the foundation of greatness.**

### 7. Success requires confronting the nasty facts, while never losing faith. 

Good-to-great companies constantly walked the line of the so-called _Stockdale paradox_, named after a US admiral captured during the Vietnam War.

As a high-ranking officer detained at the infamous "Hanoi Hilton" prison, Stockdale was tortured repeatedly by the enemy and did not know if he would ever see his family again. Despite the dire circumstances, he never lost faith that somehow he would get home.

Neither did he indulge in foolish optimism like some of his fellow prisoners, who believed they would be home by Christmas, and were heart-broken when it did not happen. Later, Stockdale credited his survival to his ability to confront the facts of his situation while still retaining faith.

In the same manner, good-to-great companies confronted the brutal facts of their reality, and yet still retained unwavering faith that somehow they would prevail in the end.

Whether they faced stiff competition or radical regulatory changes, good-to-great companies dealt with the facts head-on instead of burying their heads in the sand, and still managed to keep their spirits up. For example, when Procter & Gamble (P&G) invaded the paper-based goods market, the two major existing players reacted very differently.

The market-leader, Scott Paper, felt that their game was up and that they could never compete against a giant like P&G. They tried to diversify and stay in categories where P&G did not compete.

At the same time Kimberly-Clark relished the opportunity to compete against the best, and even held a "moment of silence" for Procter and Gamble in one of their executive meetings.

The result: two decades later, Kimberly-Clark actually owned Scott Paper and dominated P&G in six out of eight product categories.

**Success requires confronting the nasty facts, while never losing faith.**

### 8. Leaders must create an environment where the brutal facts are aired without hesitation. 

A strong, charismatic leader can be more of a liability than an asset if it means others wish to hide the unpleasant truth from him.

In management meetings, leaders must take the role of a Socratic moderator, asking questions to uncover truthful opinions, not giving ready answers. Leaders must also encourage debates to rage in the meetings so the best possible decisions are reached.

Consider Pitney Bowes, which went from being a postage meter producer about to lose its monopoly to being a major document handling solution provider and outperforming the general stock market by a factor of seven. Regardless, management at Pitney Bowes spent most of its time in meetings discussing worrying facts, like "the scary squiggly things that hide under rocks" rather than celebrating their successes.

When mistakes are made, they must be studied carefully to understand what went wrong, but blame must not be assigned, since it would discourage people from airing the truth.

_Red flag mechanisms_ which raise alerts at critical business signals can force managers to pay attention to the harsh facts.

Good-to-great companies did not have more or better information than the comparison companies, they merely confronted it and dealt with it more honestly.

**Leaders must create an environment where the brutal facts are aired without hesitation.**

### 9. A culture of rigorous self-discipline is needed to adhere to the simple Hedgehog concept. 

Dave Scott is a former triathlete who used to bike 75 miles, run 17 miles and swim 12 miles every single day. Despite this grueling regime, he still had the self-discipline to rinse his daily meal of cottage cheese before eating it to minimize his fat consumption.

Good-to-great companies were filled with people with the same level of diligence and intensity as Dave Scott, working toward the simple strategy, the Hedgehog concept, which their company was following.

Consider Wells Fargo, a bank which understood that operating efficiency was going to be an important factor in the deregulated banking world. They froze executive salaries, sold the corporate jets and replaced the executive dining room with a cheap college-dorm caterer. The CEO even began reprimanding people who handed in reports in fancy, expensive binders. All of this may not have been necessary for Wells Fargo to become a great company, but it demonstrates they were willing to go the extra mile.

A culture of discipline is not the same as a single disciplinary tyrant. Tyrannical CEOs did sometimes manage a temporary spell of greatness for their companies, but they soon crumbled after the tyrant left.

Take for example Stanley Gault, the CEO of Rubbermaid who admitted he was "a sincere tyrant" and expected his managers to work the same 80-hour weeks he did. Once he left, Rubbermaid lost 59 percent of its value in just a few years, as no enduring culture of discipline was left behind.

**A culture of rigorous self-discipline is needed to adhere to the simple Hedgehog concept.**

### 10. Final summary 

The key message in this book is:

**Companies can make the leap from mediocrity to greatness by pursuing a simple strategic concept with the right leaders and people working in a culture of rigorous self-discipline.**

The questions this book answered:

**Why were good-to-great companies identified and studied?**

  * Good-to-great companies can teach others how to make the same leap.

**How does strategic management differ at good-to-great companies versus mediocre ones?**

  * Finding a simple "Hedgehog concept" provides a clear path to follow.

  * Success comes from many tiny incremental pushes in the right direction.

  * New technology should be viewed only as an accelerator toward a goal, not as a goal itself.

**How do the people and culture differ at good-to-great companies versus mediocre ones?**

  * Level 5 leaders drive successful transformations from good to great.

  * The right people in the right place are the foundation of greatness.

  * Success requires confronting the nasty facts, while never losing faith.

  * Leaders must create an environment where the brutal facts are aired without hesitation.

  * A culture of rigorous self-discipline is needed to adhere to the simple Hedgehog concept.
---

### Jim Collins

Jim Collins is an author, lecturer and consultant, who among other things, has taught at the Stanford University Graduate School of Business and is a frequent contributor to _Fortune_, _BusinessWeek_ and _Harvard Business Review_. His previous book _Built to Last_ sold over 4 million copies.

The author was inspired to write _Good to Great_ when a business acquaintance pointed out that his previous book examined only how great companies stay great, not how they can become that way in the first place.

