---
id: 552bdc526466340007590000
slug: good-works-en
published_date: 2015-04-14T00:00:00.000+00:00
author: Philip Kotler, David Hessekiel and Nancy R. Lee
title: Good Works!
subtitle: Marketing and Corporate Initiatives that Build a Better World...and the Bottom Line
main_color: CB2932
text_color: CB2932
---

# Good Works!

_Marketing and Corporate Initiatives that Build a Better World...and the Bottom Line_

**Philip Kotler, David Hessekiel and Nancy R. Lee**

_Good Works!_ (2012) offers an insightful look into the way doing good can actually help companies prosper. Based on contemporary, real-life examples, it provides business-minded people with the tools and strategies they need to make a difference in the world and turn a profit at the same time.

---
### 1. What’s in it for me? Discover why modern businesses have to care about more than just profit. 

When you see a charity or a good cause being sponsored by a big business, what do you think? Most likely you'll mutter that the business doesn't really care, they're only really interested in profits — this is just a bit of good PR for them.

Thinking like this does these businesses a huge disservice. In fact, it's vital for any business to partake in what's known as _Corporate Social Responsibility,_ and participate in a worthy cause. Doing it right will bring a business not only some good PR, but better, more motivated staff, happier customers, and, of course, will have helped improve the lives of disadvantaged people.

These blinks show us how companies from Coca-Cola to General Mills have helped themselves and the world through good works.

In these blinks you'll discover

  * why, if you want to be good, you have to be in it for the long haul;

  * how pet owners saved mutts without ever meeting them; and

  * why Westerners helped some of the world's poorest mothers by buying more diapers.

### 2. Companies can increase their sales, reduce costs and find better employees by tackling social issues. 

Imagine that you want to eat some chocolate, but you have to choose between two brands. In terms of cost, they are exactly the same, except that one of the brands offers to donate five percent of the cost to charity.

Having never tasted either, which one would you buy? You'd probably opt for the one paying a percentage to a good cause.

And you're not alone! Customers tend to support companies who are known to support initiatives that deal with social issues. In fact, according to one study, 94 percent of customers indicated they would likely _change_ brands if the new brand made tackling social issues part of their business.

As we can see, charitable thinking can be a powerful tool for businesses to maximize their chances with customers and increase their bottom line.

However, supporting initiatives for good can do more than just help companies earn more customers.

In fact, many (mostly environmental) initiatives can help companies cut costs.

Take AT&T, for example, who decided to do their bit for the environment by reducing the amount of paper they consumed, and adopted electronic communication wherever possible. Not only did this attitude resonate with customers, but it also saved them millions of dollars.

Then there's The Body Shop, whose good image can be traced back in part to their promise to sell only products that are fair trade and not animal tested. This commitment generated great publicity, and thus saved them money on PR campaigns.

Moreover, _Corporate Social Responsibility_ creates more satisfied and loyal employees. In fact, according to one study, 69 percent of employees consider Corporate Social Responsibility to be an important factor in choosing their employer.

Thus, by investing in social issues — ones which potential employees feel are important — companies have greater access to the best possible employees.

### 3. Focus on untapped social issues that can lead to long-term partnerships. 

What do you remember about the letters you've received from charities that were supported by businesses? You likely remember the cause, but can you remember their branding?

Probably not. For companies, brand recognition is crucial. So how can you remedy this?

One way is by focusing on a social issue that isn't already saturated by other companies. You should look for issues that can help establish your brand and lead customers to link your business with the cause you support.

This is exactly what the Yoplait yogurt brand did with their "Save Lids to Save Lives" campaign. Customers could send in lids of Yoplait products to their parent company, General Mills; for every lid the company received, they donated a certain amount to a charity. As a result, Americans now link the yogurt brand with breast cancer research.

Whatever your strategy, make sure that you are involved in a cause that is relatively untapped. Otherwise, you risk losing visibility.

What's more, companies should choose issues that are relevant in the long term. For example, while supporting a city after a nasty storm will surely make for a good headline, it's nonetheless a good deed that will soon be forgotten.

Rather, you should concentrate on permanent issues, e.g., global warming or poverty, and do your part to solve them. As these issues seem unlikely to disappear, you will earn constant attention for your efforts in tackling them.

Finally, choose issues that are somehow linked to your products or to the interests of your company. For example, if you produce your goods in a developing country, why not invest some of your earnings in the local infrastructure?

Not only would you be doing a good deed, but you'd also be creating opportunities for potential talent that previously couldn't reach you due to crumbling streets or malfunctioning trains. It's a win–win deal.

### 4. Companies can get great publicity by advocating the right Cause Promotion. 

No one likes being the bearer of bad news. Yet, strangely, companies are actually rewarded with great publicity when they inform the public about a cause which demands their attention.

This _Cause Promotion_ raises awareness for social problems, and demonstrates that the company cares about this problem, too.

Take General Mills' Pink Together Campaign from 2007. The company created a MySpace page where survivors of breast cancer, doctors and others had an opportunity to share their stories with the world. General Mills also produced badges and backgrounds that MySpace users could use to show their commitment to the cause.

Their MySpace campaign reached more than 2.5 million, not to mention all those the company reached via the Pink Together badge on General Mills' product labels.

Once you've raised awareness, it's important for you to provide customers with an easy way to help tackle the social issue you are advocating. In essence, providing an opportunity to participate makes you your customers' partner in doing something good.

Take the largest US retailer for pet products, PetSmart, for example, who raised awareness of the staggering number of dogs and cats that are put to sleep each year simply because they have no owner.

PetSmart tried diligently to change this, in part by opening in-store pet adoption centers run by local animal welfare groups and also by giving customers the opportunity to donate money to help strays.

No matter what your Cause Promotion is, it's important that it can be easily tied to your company's products or values.

The Body Shop, for example, lobbied for a ban on animal-tested cosmetics in the EU. Throughout their campaign, they had an opportunity to highlight the fact that their products were produced without animal testing, and anyone who was interested in their cause suddenly had great place to buy cosmetics.

### 5. Cause-Related Marketing allows companies and consumers to join forces to achieve something good. 

Raising awareness doesn't just garner positive publicity. It can also be used to sell products directly. By employing _Cause-Related Marketing_ strategies, companies encourage consumers to do good simply by purchasing their products.

Just as companies add promotions to their products, they can also do things like making a donation every time someone purchases one of their products.

Pampers, for example, did this by teaming up with UNICEF, buying one dose of tetanus vaccine for each pack of diapers sold. This way, parents in the United States could support parents in developing countries in a simple, convenient way.

And it worked! Studies show that people who were aware of this campaign bought significantly more Pampers diapers, increasing sales by 29 percent.

Similarly, the Korean conglomerate CJ started a campaign to help malnourished people in Africa. They put an extra bar code on their products which customers could use if they wanted to donate money. For every donation received, CJ made a matching contribution.

Cause-Related Marketing works best for companies whose products appeal to large audiences. The donations people make are small, and only really make a difference when they pile up, i.e., when you are selling in large quantities.

For example, because of their wide audience, American Express was able to make a difference by pledging to put one cent toward the restoration of the Statue of Liberty each time someone used an American Express card, and pledged a single dollar whenever a new account was made.

In the end, they donated $1.7 _million_ — and this was back in 1983!

Such a campaign wouldn't have amounted to much if American Express didn't have such a wide customer base.

### 6. Creating lasting change sometimes means altering consumer behavior for the better. 

Social responsibility is a bit like medicine: You can try to relieve the symptoms — for which your patients will surely be thankful — but isn't it better to cure the disease? In this blink, we'll see how companies can act as "doctors" for social disease.

To start, let's look at the limitations of monetary donations for a good cause. While they can alleviate _some_ people's suffering, the reality is that monetary donations can't help everyone.

Think about it: You can pay for the heart surgery of everyone who needs it _today_, but that won't solve the problems that actually lead to heart disease. In other words, monetary donations don't stop the unhealthy _behaviors_ that cause heart attacks.

If you want your project to have a meaningful, lasting impact on society, you'll need to change people's behaviors.

One company that exemplifies this concept is Subway, which not only provides customers with information on maintaining a healthy diet, but also sells sandwiches that have been approved by the American Heart Association's Heart-Check Meal Certification.

By tackling the cause — rather than just the symptoms — Subway can hope to make a real, positive difference in society.

But companies should only commit to such projects if they're in it for the long haul.

The fact is, it's not easy to change people's behavior, especially if that change is inconvenient. Be honest: despite knowing how bad unhealthy foods are for us, how many of us eat them anyway? Changing our diets would mean losing out on certain pleasures.

This is an approach that requires patience; you need to understand that the success of your campaign can't be measured in days or months but rather in years.

### 7. Workforce Volunteering helps companies do good while improving employee’s morale and skills. 

Have you ever felt like you're just pushing papers around at work, and not doing anything truly meaningful? Wouldn't it be awesome to do something that's actually useful for your community? Increasingly, companies are tapping into this desire and supporting employees who want to make a difference through _Workforce Volunteering_.

Workforce Volunteering is a great way to put employees' skills to use to achieve something good for society as a whole. Companies are well positioned to help employees do this by providing them with the time and resources they need to tackle social issues.

In 2011, for example, IBM launched a Workforce Volunteering program and encouraged their employees to spend time working for charities like Grupo Puentes or Business in the Community.

Between January and June 2011 alone, IBM employees worldwide put in more than 2.5 million hours of volunteer service.

But what do companies have to gain from Workforce Volunteering?

Employees who have opportunities to use their skills for good feel more motivated, and also acquire other valuable skills in their volunteer work.

Moreover, they tend to feel greater loyalty toward their employer, and take greater pride in their work. That warm feeling of knowing that you've not only worked for your own livelihood but have also done something for your community and society is truly wonderful, and employees associate that feeling with the employer who made their volunteer work possible.

Take the Luxottica Group, for example, which sends groups of employees to developing countries to provide the visually impaired with old, donated glasses. To make sure they get the right prescription, volunteers have to conduct eye exams — often in unusual surroundings.

But Greg Hare, the director of Luxottica's foundation One Sight, sees this as a benefit: "What better way to teach teamwork, flexibility, creativity and the power of a positive attitude than to take like-minded adults to a developing country for two weeks?"

### 8. Socially Responsible Business Practices are key to your company’s future. 

People generally fall into two groups: those who see business as an opportunity to make big money and those who are suspicious of its ability to be a force for good. However, there is a chance to combine both worlds — to both make money and be a force for good — through _Socially Responsible Business Practices_.

Increasingly, consumers and employees expect companies to work in a socially responsible way. Studies show that 69 percent of employees consider it important that their company lives up to its social responsibility. In contrast, a measly six percent of consumers agreed that a company should _only_ be interested in improving their profits.

Many companies see this data and react accordingly. The retail store Target, for example, will give their employees $5,250 a year toward an MBA degree.

Fundamentally, the future workforce and consumer base are entirely dependent upon a functioning society. Consequently, companies _must_ support societal well-being if they want to continue existing in the future.

Coca-Cola, for example, supports bottlers in South Africa by providing an HIV prevention program. Employees get information about HIV and how to prevent an infection, as well as free condoms.

Not only does Coca-Cola's initiative help workers, it also helps Coca-Cola maintain its workforce, as fewer people will contract this dreadful virus.

Other companies, such as Starbucks, are paving the way in combating environmental damage.

Starbucks has pledged to use the LEED (Leadership in Energy and Environmental Design) standards in all of its shops, thus making them more "green" by using renewable energy and producing less waste. After all, if humanity can't figure out how to manage environmental problems, there might come a day when there's no one left to sell products to!

But if enough companies make helping society and the environment a priority, they'll discover that they can do good just as easily as they can make money.

### 9. Final summary 

The key message in this book:

**Companies have a great opportunity in charitable and socially minded works: they garner publicity for the company, increase employee happiness and produce more loyal customers. Not only does the power of doing good unlock potential profits for companies, but climate change and other disasters may also make this kind of corporate innovation a necessity.**

**Suggested** **further** **reading:** ** _A Better World, Inc._** **by Alice Korngold**

Many of us are quick to assume that big corporations are the enemies of the environment. _A Better World, Inc._ explains how the opposite is true: companies are in a better position to solve some of the world's biggest problems than many governments and campaign groups. This book outlines why, and lists the steps companies can take to improve our planet, while raising their profits at the same time.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Philip Kotler, David Hessekiel and Nancy R. Lee

Philip Kotler is an American marketing consultant, professor and author. He has written over 55 books on various marketing-related topics, such as _The Ultimate Book of Business Gurus_ and _Business Minds_.

David Hessekiel is the president of the Cause Marketing Forum, an organization dedicated to helping businesses and nonprofits come together to improve the world.

Nancy Lee is the founder and president of Social Marketing Services, Inc. She conducts seminars and workshops on social marketing, and is an adjunct faculty member at University of Washington's Evans School of Public Affairs.

© [Philip Kotler and David Hessekiel: Good Works!] copyright [2012], John Wiley & Sons [Inc. or Ltd. as applicable] Used by permission of John Wiley & Sons [Inc. or Ltd. as applicable] and shall not be made available to any unauthorized third parties.

