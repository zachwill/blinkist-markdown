---
id: 560949058a14e600090000d2
slug: got-your-attention-en
published_date: 2015-10-02T00:00:00.000+00:00
author: Sam Horn
title: Got Your Attention?
subtitle: How to Create Intrigue and Connect with Anyone
main_color: F07F30
text_color: A35621
---

# Got Your Attention?

_How to Create Intrigue and Connect with Anyone_

**Sam Horn**

_Got Your Attention?_ (2015) is the ultimate guide to grabbing and holding the focus of distracted people in today's fast-paced world. You'll learn how to overcome the impatience and chronic distraction of your audience to make yourself a compelling, engaging speaker.

---
### 1. What’s in it for me? Learn how to get the attention you want and deserve. 

So you need to get someone's attention. How do you do it? Do you scream and shout, jump up and down, wave your arms, while yelling, "Hey! Look over here!"

While this may get a taxi to pull over, it won't get you the attention you need in your career or life.

In today's fast-paced, high-stress market, everyone is competing for attention — whether at work, for job interviews, at conferences or for entrepreneurial projects. To grab the attention of the people who matter most to you, you'll have to develop a more sophisticated strategy than yelling loudly. 

In these blinks, you'll learn tips and tricks to get the attention you need to land the jobs you want, stand out among colleagues or convince a panel of experts that your start-up idea is gold. 

In these blinks, you'll discover

  * why opening with fire is a good way to get attention;

  * why no one cares about your GPA in a job interview; and

  * how a 13-year-old girl grabbed the attention of thought leaders.

### 2. To keep your audience hanging on every word, pique their curiosity and fuel their imagination. 

People today are impatient. As soon as you open your mouth, people in the audience have already made up their mind: about what kind of person you are and what you can do for them. 

If you don't give them something exciting to latch onto, they'll tune out immediately, and start paying attention to the next act. 

So how can you avoid this fate?

You need to pique your audience's curiosity about _your_ knowledge and what _you_ have to offer. 

Here is where _Did You Know?_ questions play a key role. Offer your audience three important bits of information that they don't know, but are eager to know. 

It's easy to do: just craft three, one-sentence Did You Know? questions to start your conversation. These questions might be about the problem you've set out to solve, the importance of your work, a dramatic shift in the field, or the unmet needs in the market that you're filling. 

Let's say you're talking about a book that addresses landing a job in today's tough market. Your three questions might look like this: 

_Did you know that..._ 80 percent of the 3.6 million jobs available in 2012 were never advertized?

_Did you know that..._ on average, only 20 percent of job applicants actually get an interview?

_Did you know that..._ in 2013, 53.6 percent of college graduates in the United States under 25 were jobless or underemployed?

Granted, making people curious is half the battle. Now you need to fuel their imagination to maintain their curiosity about your subject, and that means pitching a solution. 

Let's stay with the book example. Your solution for your talk might sound something like this: 

_Imagine that..._ once you read this book, you'll stand a chance of finding 40 percent more openings for your dream job or boosting your chances of getting an interview by 30 percent!

This technique works, as when you phrase potential outcomes like this, you're helping your audience actively think about what you're saying.

### 3. Act out the problem you plan to solve by showing and asking. Your audience will do the rest. 

Have you ever heard the saying, "When you advertise fire extinguishers, open with the fire?" 

This explains a helpful fact. People tend not to know what they want until you show it to them. 

Cari Carter knows the importance of _opening with the fire_. When she pitched her product concept — a car hook for hanging your purse — to investors, she grabbed the attention of the room by lugging a full-sized, driver's side car seat to the front of the room before even saying hello. 

After this, she began to mime driving a car, and then posted her question:

_"_ Have you ever been driving along and had to stop suddenly, only to see your purse and cell phone fly off the passenger seat? Imagine a hook that..."

And before she went any further, a man in the audience exclaimed, "I'll take two! One for my wife and one for my daughter!"

Carter's success is straightforward: she didn't just describe her idea, she demonstrated the pressing need for it — _the fire_ — and in doing so, immediately had investors wanting her solution. 

But we can't all lug car seats around! A more convenient way to demonstrate problems is through _Show and Ask_ — not tell. Showing and telling a problem's solution can be confusing, especially if your product is something weird like a "receipt aggregator." That only begs the question, "A what?"

In cases like this, it's better to show and _ask_.

So instead of explaining your product, guide your audience to the issue it solves with a question. For example, "Have you ever returned home from a business trip and been faced with the impossible task of tracking down your receipts?" 

Your question will spur your audience's imagination, as each person tries to picture a solution to the problem. Perhaps they'll think of a small box that keeps receipts in order. 

This step will make your job all the easier, as you don't have to explain the problem and solution in detail — your audience is already imagining both!

### 4. Share what’s rare to place yourself in the spotlight and apart from the pack. 

While humility can be an admirable trait, being too humble can quickly become your greatest weakness, especially when trying to get the attention of your boss or any other authority.

In fact, successfully competing for the attention of important people means showing _and_ proving your competitive advantage. 

If you want to land a job, you need to _show_ how you top the competition. And honestly, it's not bragging if you're just explaining your accomplishments — they might be the thing that makes you stand out, getting you that interview. 

But what's the best way to prove your advantage over others?

You need measurable results that show a decision maker that you can achieve the same for his company. Therefore, it's key to support all your assertions with specific examples of how you saved, managed or made money for your previous employers, sponsors and investors. 

But what if your strengths are obvious?

Then be sure to point out ones that are less apparent. If you don't, you likely won't be noticed. 

This is particularly true when you find yourself in situations where the competition boasts many of the same strengths that you have as well. In scenarios like this, being able to identify a "secret" strength might be easier than going head-to-head with comparable competitors. 

Say you're competing for a job interview at NASA with other talented graduates from elite universities. They're all bright, and have displayed impressive GPAs to back this up. 

But instead of doing the same, you share the unique fact that you've won an international competition to plan a manned mission to Mars. You've set yourself apart from the pack — and as a result, landed the job!

### 5. Before making a proposition, first ask yourself why your subject would say no. Then work for the yes. 

Have you ever felt that a person stopped listening to you even before you began speaking? 

It's a common reaction of conversation partners who are prejudiced against you — a result of a person saying "no" in his mind, before you even open your mouth. 

How can you avoid this?

Ask yourself, "Why would the person say no?" and make the first words out of your mouth an argument against those reasons for saying "no." 

This works because while many people will make up their minds to say no before hearing your argument, you _can_ change opinions by making that "no" obsolete. 

At 13 years old, Cassandra Lin pitched an idea at the _Business Innovation Factory_ conference. With a big smile, she leaned in toward the audience and said, "I know what you're thinking. What's a 13-year-old going to teach me about innovation?" 

In less than a few seconds this teenager had everyone on her side, simply because she knew her audience would distrust her because of her age. 

But what if it's too late, and somebody has already said no?

Then it's your job to bring up new evidence that motivates the person to reverse his decision. For instance, a local bookstore owner repeatedly turned down an offer to sponsor a boys' soccer team, saying that putting his store's logo on jerseys wouldn't improve his business. 

One of the boy's fathers then approached the owner, saying, "I imagine you get asked for donations all the time, but our coach could sign your bestselling book on a day of your choice. Not just that, but one of the parents is a social media genius who could blog and tweet about the sponsorship to generate publicity around the signing event."

The father's solutions showed the owner how he could benefit from the sponsorship, and convinced him to accept the offer!

### 6. Pay quality attention to others and they’ll do the same for you. 

Have you ever been really excited about a project and called up a friend to pitch it? You felt he would be excited too, but in the end, he didn't seem turned on at all. 

What happened? Perhaps it was the language you used in your pitch. When trying to persuade people, it's important to speak in the terms that they use on a regular basis. 

One way to arm yourself with the language necessary to convince decision makers is by studying their websites and marketing materials, to know exactly how they speak. 

For instance, the author once attended a National Press Club event where Elon Musk, the visionary founder of Space X, was taking questions. 

The author's son asked how he could get hired at Space X. Musk's succinct response was, "Don't tell me about the positions you've held; tell me about the problems you've solved."

Taking Musk's advice, the author's son and his friends revised their resumes to highlight specific problems to which they had found solutions. Doing so dramatically increased their chances of landing interviews at the company, as they had incorporated the hiring criteria of the founder, in effect by adopting his language and perspective. 

But speaking the right language isn't enough. You'll also need to show your audience that you understood the message! Listening is the second fundamental aspect of connection. 

A study from the US Department of Labor Statistics found that 46 percent of employees who quit a job said they did so because they didn't feel listened to, which made them feel unappreciated. 

So if you like being listened to, you have to listen yourself. This means not interrupting people when they speak, not finishing their sentences and resisting the urge to look over your conversation partner's shoulder to watch someone else walking by. 

This last point is important, as when your eyes wander, your mind wanders, too.

### 7. Keep current if you want your information to be interesting. Please don’t quote Aristotle! 

We've all been in situations in which an older person drags on, telling stories about the past. Whether about some ancient movie star or out-of-print book, why is it that old stuff can be so boring?

Often, when people tune out while listening to someone speak about something "before their time," it's not because they are rude, it's just because they can't relate. 

Consider this speaker at a global young entrepreneur's conference. He starts off relating a story about Eddie Arcaro, a Hall of Fame jockey who won two Triple Crowns in the 1940s. Then he goes on to reference events from World War II — but it's clear that nobody is paying attention. 

What happened here?

The speaker's references were simply too outdated for his audience. If he had spent just a little time researching his audience, he would have realized that they were all in their 20s — thus World War II references made their attention wane, even though the speaker himself was an advertising legend. 

The bottom line is that people don't want to hear the same information all the time. And if you use older references, chances are that people have already heard it. They'll roll their eyes, assume you're out of date and worse, think you're boring. 

Let's say you're an executive who has written a book on leadership, and have started every chapter with an inspiring quote. The problem is, all the quotes come from what millennials call _dead white guys_, like Albert Einstein, Aristotle and Thomas Edison. Sure, great thinkers — but outdated thought leaders. 

Replacing these quotes with words from contemporary innovators like Richard Branson, Steve Jobs or Ken Blanchard would make the content and thus the entire book more appealing to young people who are interested in leadership!

### 8. Make your speech relevant. Find that thread so you can pull your audience together. 

Imagine that you're presenting the results of your research. You begin with all the relevant statistics you've calculated, and quickly you are looking at a sea of bored faces. What happened?

Connecting to people requires more than just presenting a theory. For most people, agreeing with your hypothetical scenario isn't enough; they need to be able to apply your knowledge practically.

Let's look at Curemark CEO Dr. Joan Fallon's TEDx talk on autism. Given the growing concern surrounding autism, Fallon adopted a practical approach in her talk, by asking the audience first how many knew someone with autism and how many understood the difficulties facing families with autistic children.

The quick survey immediately made the real-world relevance of the talk sink in with the audience. Only then did Fallon begin her speech, saying that instances of autism have increased by almost 80 percent over ten years.

In a short moment, Fallon had made her topic real and pertinent to the entire audience.

A relevant theme is crucial; yet people still won't care if you don't offer a proposed solution. For instance, a candidate at a political rally spent the entirety of his allotted 20 minutes bemoaning the terrible state of American schools and stressing that teachers aren't paid enough.

The audience certainly agreed, but also knew that the candidate was a county-elected politician and didn't have the authority to change the education budget.

Thus the crowd grew increasingly impatient as the speaker described a problem he couldn't do anything about. Instead, he could have said, "You might be wondering what effect I can have on this issue. Well, I've already met with three members of the board of education to propose a plan which would…"

By outlining a solution instead of just a problem, you grab the attention of your audience!

> _"If what you're sharing doesn't have real-world relevance for them, why should they pay attention? It's simply not a high-enough priority."_

### 9. Illustrate your ideas with an example of an individual who experienced what you’re talking about. 

It can be seriously difficult to put yourself in someone else's shoes; sympathizing with others can be even harder. People can relate easily to one person, but not to many at once. 

So while we can bemoan the fate of one hungry child, an entire country filled with hungry children dulls our sense of empathy. This situation is described in psychology as the _empathy telescope_, which states that it's easier for one person to care about another person than about many people. 

A captain of an oil tanker lost his dog when his ship caught fire off the coast of Hawaii. Afterwards, he told people how grateful he was that his crew were rescued, but that he couldn't stop thinking about his dog that had been lost at sea. 

His story went viral, and donations started pouring in to save the dog. In the end, the US Navy launched a successful rescue mission with some $250,000 in donated funds! Because of the telescopic nature of human empathy, it was easy for people to deeply feel for the captain and offer financial support. 

But to make the minds and hearts of your audience feel the most, it's essential to illustrate your ideas with real-life examples. Doing so will increase the credibility of your ideas. Why is this?

Many decision makers believe that "stories" are better left to the likes of Disney, fairy tales for bedtime reading. These people think stories are something a person invents. 

To avoid this, you need to give examples with factual anecdotes about a person who did something related to your topic. Stories too are also more effective when pared down to 60 seconds. By telling short, real-world stories, you'll give your audience live proof that makes your pitch more believable.

> _"Why can we read novels for hours at a time and it's not hard work? It's because authors put us in the scene so we're there."_

### 10. Final summary 

The key message in this book:

**We're suffering from an impatience epidemic. People today can't focus, and it's a struggle just to get someone to notice you. As a result, connecting with others is difficult. But there is a solution: to hook people quickly by paying them quality attention and letting them know why** ** _you_** **matter.**

Actionable advice:

**Grab attention with a confident posture.**

Did you know that something as simple as the way you stand can affect people's attention toward you? An attention-holding posture can be yours in three simple steps. First, hold your head and chin up with your eyes focused forward. Then be sure your feet are shoulder-width apart, keeping them stable and grounded. Finally, center your hands in front of you about a foot apart as if you're holding a basketball. That's it! This is your power posture. 

**Suggested** **further** **reading:** ** _POP!_** **by Sam Horn**

This book shows you how to craft messages and ideas that _stick_ in the minds of your audience because they are Popular, Original and Pithy. Author Sam Horn offers a highly adaptable and systematic approach to creating pitches, titles and taglines that grasp people's attention and win them over to your brand, product or idea.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Sam Horn

Sam Horn, also known as the "Intrigue Expert," is a messaging and branding strategist with 20 years of experience working with clients like Intel, NASA and Boeing. She has also helped thousands of entrepreneurs and executives produce books, keynote speeches and TEDx talks.

