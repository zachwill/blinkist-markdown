---
id: 524c3d791a96d45767000001
slug: great-by-choice-en
published_date: 2013-10-03T14:27:27.000+00:00
author: Jim Collins & Morten T. Hansen
title: Great by Choice
subtitle: Uncertainty, Chaos, and Luck – Why Some Thrive Despite Them All
main_color: 2DADE2
text_color: 1B6787
---

# Great by Choice

_Uncertainty, Chaos, and Luck – Why Some Thrive Despite Them All_

**Jim Collins & Morten T. Hansen**

The world is an uncertain place, constantly changing and often chaotic. While many companies are unable to survive in this chaos, some companies are not only able to survive in these shifting conditions but even thrive in them. _Great by Choice_ analyses why these companies succeed while most others fail. 

_Great By Choice_ is the result of exhaustive, in-depth research into the business environment. It argues that success is not the result of a company being more innovative, bold or open to taking risks, nor is it a result of mere luck or chance. Success in fact comes from a mixture of discipline, evidence-based innovation and a fear of failure that borders on paranoia. It is this recipe, rather than luck, which enables certain companies to become great.

---
### 1. In unpredictable business environments, companies that are well prepared prevail. 

In 1911, two teams set off on a race to become the first explorers to reach the South Pole. One team, lead by Roald Amundsen got there first, planted the Norwegian flag, and returned safely. Robert Falcon Scott's team arrived at the pole 34 days later. Losing the race was devastating, but things got even worse on the journey back. They never made it home; every member of the team froze to death.

So why did these two teams have such different fates?

Preparation was one major difference. Amundsen was obsessive in preparing for the trek and spent years beforehand scouring the globe for useful knowledge; for example, he learned the art of polar survival from Eskimos and tried every potential food source, even dolphin. On their trek, his team carried extra supplies as insurance against delays and carefully marked their supply depots with black flags which stood out against the white landscape. Amundsen hadn't known exactly what the Antarctic would throw at them, but he had made sure his team was as prepared as they could be. In short, he left very little to chance.

Scott's team, however, carried only a fraction of the provisions compared to Amundsen's team; leaving them at risk of starvation should unexpected delays occur. Whereas Amundsen relied on proven Eskimo technology like dog sleds, Scott used untested technologies like motor sledges, which failed in the extreme conditions. This lack of preparation, both in acquiring knowledge and in carrying sufficient supplies, led to delays, failure and eventually death. 

Just as Amundsen and Scott faced uncertain conditions in the Antarctic, companies too face turbulent and ever-changing environments. Don't let conditions determine your success; prepare to survive and thrive in any environment. The way to succeed is to follow Amundsen's example: be prepared for _any_ eventuality.

**In unpredictable business environments, companies that are well prepared prevail.**

### 2. Companies that thrive in uncertainty rely on discipline, evidence-based innovation and preparedness based on paranoia. 

The future is unknown. No one can accurately predict what will happen in the coming months let alone years or decades.

Yet, in these uncertain environments some companies not only survive, but thrive. To understand why, we need only to analyze the practices of the so-called _10X companies_ : highly successful companies that outperformed their industry average by _at least_ a factor of ten.

So what is the secret to their success?

Like the Arctic explorer Roald Amundsen, 10Xers ensure they are prepared for any event or conditions by building up their knowledge and provisions.

This outlook is reflected in three core behaviors:

First, 10Xers display _fanatic discipline_. Discipline in this context does not mean everybody in the organization obeying orders but rather consistency in action. Once Amundsen had identified his route and created a schedule for reaching the pole, he never wavered from his plan. Likewise, once 10X leaders identify their goals and preferred methods, they stick to them doggedly.

Second, 10Xers use _empirical creativity_ when making decisions. What matters is evidence. They aren't interested in others' opinions or following the established consensus, but in finding out what really works. Amundsen didn't listen to the opinion of experts when deciding the location of his base camp; he studied the evidence himself and chose an area no one had previously considered. Similarly, 10Xers use evidence to help determine when and where they should innovate.

Finally _Productive Paranoia_ helps 10Xers survive changes in their competitive environment. Their leaders remain hypervigilant; they are never comfortable but always fearful of what might go wrong. Amundsen ensured that he was prepared for the worst on his expedition. Likewise, 10Xers channel their fears to create policies that prepare their companies for difficult times.

**Companies that thrive in uncertainty rely on discipline, evidence-based innovation and preparedness based on paranoia.**

### 3. 10X companies set themselves targets which they hit precisely year after year – no matter what the conditions. 

Roald Amundsen trekked to the South Pole in an extremely disciplined manner. Every day he marched a similar distance, about 15.5 miles, no matter what the conditions. When the weather and terrain were favorable, he would march this set distance and then stop and rest, even when the team could go further. Conserving energy in this way meant that even when the weather or terrain became difficult, his team was strong enough to march on and hit their target distance. His policy enabled the expedition to maintain a steady pace.

This approach to achieving success is known as _Twenty-Mile Marching_, and it is one of the strategies that allows 10X companies to thrive in uncertain times. First, the marker by which success will be measured is identified. While Amundsen had his daily distance, companies use longer-term markers like an annual growth percentage or a certain level of innovation. They will then drive themselves to reach this target _consistently_ through both good and bad times.

When twenty-mile marching, the pressure is always on: the marker must be met in difficult times, but must not be overshot in favorable ones. Though this may seem counterintuitive, it is crucial — and it takes self-discipline. Push too hard and your company may become unbalanced and unable to adapt to changing circumstances.

The electronics company AMD (Advanced Micro Devices) nearly destroyed itself chasing rapid success in the 1980s. Attempting to gain a 60% growth rate, the company over-borrowed, leaving itself low on resources when its market collapsed. It fell behind competitors such as the 10X company Intel, which secured steadier and more sustainable rates of growth over the same period.

**10X companies set themselves targets which they hit precisely year after year — no matter what the conditions.**

### 4. 10Xers are bold and innovative, but only when the evidence supports such a tactic. 

10X companies are fiercely independent in their decision making and creative output. Some 10Xers like Apple become famous for their constant innovation, and many analysts believe that this boldness in innovation is the key to these companies' success.

To a certain extent, this is true; however, 10Xers are not innovative just for the sake of it. They do not take foolhardy risks by launching innovative, ground-breaking products without first understanding their potential for success. They first gather all manner of empirical evidence to build a deep and thorough understanding of the marketplace, and then adjust the direction of their innovations to take advantage of opportunities.

This so-called empirical creativity is implemented through a policy of _firing first bullets, then cannonballs._ A 10X company will first test the market with a series of low-risk, low-cost innovations, essentially firing many 'bullets' in many directions. Once a good target has been identified they then pursue that opportunity with full force: they fire cannonballs to smash it.

In 2001, Apple fired a bullet by launching a small-scale MP3 player compatible only with Mac computers. This bullet — the first iPod — was low risk, low cost and low distraction, in that it didn't distract the company's resources and time away from the main product, Mac computers. Initial sales were promising, so Apple fired another bullet: it launched iTunes, an online music store also restricted for use on Macs. Consumers liked the iPod and the ability to download music cheaply and legally — the bullets had hit their targets. This was the empirical evidence Apple needed to launch its cannonballs: iTunes and iPod for non-Mac computers. The cannonballs smashed the targets identified by the two bullets, and the rest is history.

**10Xers are bold and innovative, but only when the evidence supports such a tactic.**

### 5. Innovation isn’t everything; it must be combined with discipline in business practice. 

There is a common misconception that in a constantly changing environment, the most innovative companies are also the most successful. In reality, innovation alone is no guarantee of success.

Of course, every company must innovate to a certain degree, and every industry has an _innovation threshold_ :a level of innovation firms must meet to avoid being left behind by their competitors. This threshold is higher in some industries than in others. The information-technology industry, for example, has a relatively high innovation threshold. Products become obsolete quickly, so companies must innovate constantly to stay competitive. The airline industry, on the other hand, has a low innovation threshold; there is no need to come up with a new way to fly every month.

While companies that fail to make their industry threshold cannot hope to succeed, innovation at higher levels seems to bring little advantage. This is because a company that focuses too much on innovation will become unbalanced. It allocates too many resources to chasing innovative breakthroughs, leaving crucial elements of business underprepared and underfunded. What is needed is the discipline to ensure that all areas of business, such as manufacturing, marketing or accounting, are given the attention they require.

Intel was able to beat their rival, Advanced Memory Systems (AMS), despite the fact that the latter was initially the better innovator. Intel succeeded because they remained focused on all areas of their business, from innovation to manufacture and distribution. They obsessed over costs and production processes, ensuring that their clients could expect their product to be delivered on time and on cost. While AMS beat them in the laboratory, Intel won where it matters: in the marketplace.

**Innovation isn't everything; it must be combined with _discipline_ in business practice.**

### 6. 10Xers are "productively paranoid" – they fear the worst and obsessively prepare for it. 

10Xers are never comfortable; they are always worried about the future. Even in successful periods, they are always asking themselves, "What if things in my industry change?" "How will my company cope?" and "Where will my next rival come from?" They then put this paranoia to productive use by using it to ensure they are prepared for any situation.

One way 10Xers prepare for difficult times is by hoarding huge reserves of cash. On average, their ratio of cash to assets is three to ten times higher than their competitors.

Their paranoia leads them to be hypervigilant. They constantly watch their environment for potentially hazardous developments such as new potential rivals, new legislation or changing financial conditions.

This vigilance enables them to develop a deep understanding of their industry, which, combined with their obsessive preparation, gives them a huge competitive advantage. It allows them to identify the defining situations on the horizon and take advantage of them.

In 2001, the airline industry was rocked by the terrorist atrocities of 9/11. While many airlines were forced out of business, one company, Southwest Airlines, was not only able to survive the chaos but also to post profits. It even managed to grow its business, offering new routes and services. Although Southwest Airlines hadn't foreseen the 9/11 tragedy, they had built a financial foundation that would allow them to survive a great shock, and their understanding of the market enabled them to identify new opportunities in the midst of a crisis.

**10Xers are 'productively paranoid' — they fear the worst and obsessively prepare for it.**

### 7. 10X companies create durable and specific operating procedures which breed consistency and success. 

In 1979, the US airline industry was deregulated, allowing new airlines to be created and therefore increasing competition. In response to this the CEO of Southwest Airlines, a 10X company, issued a list of ten operating procedures to help the company succeed in the changing environment. This list contained a series of commandments such as "Use 737 aircraft" and "Stay out of food services" for the company to follow.

The list proved so successful that over the next quarter of century, 80% of the commandments remained the same. Despite all the changes in the highly volatile airline industry, only 20% of the operating procedures were changed. This gave the company the consistency and clarity it needed to succeed year after year.

The success of the procedures was due to them being _specific, methodical_ and _consistent_, or "SMaC." They were precise enough to cover a range of issues, durable enough to be used in the long term, and yet specific enough to work in different situations.

10Xers are experts at developing SMaC operating procedures. They base their lists on the huge amount of empirical evidence they collect, knowing what procedures will work and which will not. They also have the self-discipline to strictly follow their list. Southwest Airlines could have abandoned their operating procedures when the airline industry suffered one of its many crises, yet they stuck with it because they knew it worked.

Because they work so hard on getting them right in the first place, 10Xers will only tweak their operating procedures when absolutely necessary. This gives 10X companies the direction and confidence to succeed.

**10X companies create durable and specific operating procedures which breed consistency and success.**

### 8. Neither luck nor circumstance make 10X companies great – hard work and ambition do. 

There is a worrying trend in modern society which suggests that success is largely the result of luck or circumstance. The notion is that the success of society's great leaders and companies has less to do with their attitude or tactics than with them just happening to be in the right place at the right time. 

Yet the truly great reject this notion. They know that they receive the same amount of luck as everyone else; rather, it is a mixture of hard work and ambition that makes them stand out from the crowd. For 10X leaders, it's about getting the best _return_ on their luck by ensuring that they make the most of any opportunity that comes their way.

Take Bill Gates, for example. The Microsoft founder had much good fortune. He was privately educated, had access to computers at college, and was lucky enough to read the issue of _Popular Electronics_ magazine that happened to contain a particularly interesting article: one that inspired him to launch his first product.

Yet, this wasn't what made Bill Gates great; after all, many others in his generation had the same good fortune. What made him a success was hard work and ambition.

After reading the article, he changed his life plans. He dropped out of college and moved to a state where he could pursue his goal of developing a groundbreaking software product. He worked all hours, often skipping food and sleep. He knew he could be successful, and he wasn't prepared to stop until he achieved greatness. In short, through hard work and ambition, he turned his luck to his advantage.

**Neither luck nor chance makes 10X companies great — hard work and ambition does.**

### 9. Final summary 

The key message of this book is:

**The most successful companies don't let chance determine their fate. They are obsessive in their preparation, ensure that they have the evidence to back up their decisions, and practice the discipline to continue with their plans through both good times and bad. It's this mixture of consistency and evidence-based analysis that allows some companies show long-term success in a chaotic world.**

**Neither luck nor chance are the reasons why some companies become great. Every company gets roughly the same amount of luck; it's what you do with this fortune that matters.**

The questions this book answered:

**How do some companies achieve long-term success in an uncertain world?**

  * In unpredictable business environments, companies that are well prepared prevail.

  * Companies that thrive in uncertainty rely on discipline, evidence-based innovation and preparedness based on paranoia.

**What tactics do 10X companies use to beat their competitors?**

  * 10X companies set themselves targets which they hit precisely year after year — no matter what the conditions.

  * 10Xers are bold and innovative, but only when the evidence supports such a tactic. 

  * Innovation isn't everything; it must be combined with _discipline_ in business practice.

  * 10Xers are 'productively paranoid'– they fear the worst and obsessively prepare for it.

  * 10X companies create durable and specific operating procedures which breed consistency and success.

**How do 10Xers get the best return on their luck?**

  * Neither luck nor chance makes 10X companies great — hard work and ambition does.
---

### Jim Collins & Morten T. Hansen

Jim Collins is a business consultant and author of many bestsellers including _Good to Great_ and _Built to Last._ His books have sold over ten millions copies worldwide. He now operates a management laboratory where he conducts research and teaches.

Morten T. Hansen is a management professor at the University of California. He is the author of _Collaboration: How Leaders Avoid the Traps, Build Common Ground and Reap Big Results._ He consults and gives talks for companies across the globe.

