---
id: 561437d036323300071f0000
slug: great-people-decisions-en
published_date: 2015-10-08T00:00:00.000+00:00
author: Claudio Fernandez-Araoz
title: Great People Decisions
subtitle: Why They Matter So Much, Why They Are So Hard and How You Can Master Them
main_color: A62128
text_color: A62128
---

# Great People Decisions

_Why They Matter So Much, Why They Are So Hard and How You Can Master Them_

**Claudio Fernandez-Araoz**

_Great People Decisions_ (2007) explains the many ways in which personnel choices can make or break an organization. Full of practical, immediately actionable advice, this step-by-step guide to finding and hiring the right candidate for the right job is helpful for businesses of all shapes and sizes.

---
### 1. What’s in it for me? Become a recruiting mastermind. 

Look at the rows of titles in a bookstore and you'll see hundreds of works expounding the benefits of having a great team. And yet, you're likely to find very few books on how to actually hire and put together a great team.

Recruiting is still largely a hit-and-miss affair, but it doesn't have to be. There is a science to recruiting the best people; if you learn it, you can improve your staff decisions exponentially, resulting in incalculable upsides for your company. So what exactly should you do?

These blinks, based on the knowledge of one of the world's leading recruitment consultants, provide you with the essential tools you need to start making great hiring decisions.

In these blinks, you'll discover

  * why the dead hand of procrastination can leave us with a poorly functioning team;

  * why, even if your team seems perfect, you shouldn't stop thinking about making changes; and

  * why the best way to find a top candidate is to ask someone else.

### 2. Great people decisions are vital to maximizing your career potential. 

There are plenty of books out there that promise to reveal the secrets to business success. But although they might offer plenty of sound advice, they often miss one of the most important factors on your journey to success: other people.

Books boasting the best business secrets don't teach you how to make _great people decisions_, that is, how to find great people and put them in the right places.

While there are, of course, many factors that will determine your career successes, being adept at people decisions is critical.

It's certainly true that genes, education, career choices and personality, among other factors, play an important role in boosting some people toward success and leaving others behind. However, people decisions are just as important as these other factors, and perhaps even more so. 

First, as your career progresses, you will be more likely to move into roles that require you to manage more and more people. Naturally, being highly skilled at finding the right people for the right positions is essential.

Second, you can never do your best work alone. According to career expert Marcus Buckingham, no one has unlimited potential. If you want to keep developing and succeeding, you'll need the help of others who can complement your skills and help you move forward. 

Unfortunately, despite the great importance of people decisions, many of us fail to develop this valuable skill. This is in part because people tend to think that the ability to judge people is based on "gut" feelings and intuition — it's not something that can be learned, but something you either have or don't. 

But this simply isn't true. The ability to make great people decisions — to judge whether someone is good at a particular job or in your team — is one that can, and indeed must, be learned.

How exactly? That will come a little later; the next step is to look at the benefits that great people decisions bring to organizations.

### 3. Companies can only thrive when they have the right people in the right places. 

Image you're an anaesthetist who only manages to revive your patients 33 percent of the time. Would you be considered a success? Of course not. In fact, you'd be arrested for such serious malpractice!

Incredibly, according to the father of business excellence, Peter Drucker, when it comes to making effective people decisions, executives only get it right 33 percent of the time.

This is a shocking statistic, especially considering that there is essentially no factor more important for a company's success than putting the best performing people in the right roles.

When people think of the factors that lead to success or failure in business, they often think of the need to find business ventures, partnerships and markets, responding to competition, knowing when to acquire and merge, and so on.

Yet, competence in any of these areas requires a different set of skills that no one is likely to have. So, if a company wants to master each of these business areas, it needs to have the right teams in place.

This view is supported by the work of prominent business expert Jim Collins, who rates hiring the right team as the _most_ important thing any company can do. He argues that until an organization has filled 90 percent of its roles with the right people, doing so should remain its highest priority.

As important as great people decisions are for a company today, these decisions will only become more important in the future. Indeed, in the fastest-growing areas of the economy (biotech, media, entertainment and software), the quality of staff is far more important than an organization's physical assets. For example, it's not powerful equipment that creates quality software, but great programmers.

It's thus important for companies and managers to refine their skills related to people decisions today, as their success tomorrow could depend on it!

### 4. Difficulty assessing what a role needs, along with a lack of suitable candidates, make great people decisions challenging. 

We now know that hiring the best people and putting them in the right places will go a long way in advancing both your career and your company. And yet, so many of us fail to make great people decisions. Why? 

There are four main reasons for this trend; in this blink, we'll focus on the first two.

The first reason is the most obvious: there are just not that many exceptional candidates out there. There are, however, plenty of "average" people who could perform a given job more or less competently. As a matter of statistics, you're more likely to end up hiring one of them, rather than one of the relatively few high-performing candidates.

Second, correctly assessing which people will work for which role isn't always as straightforward as we would like to think, and there are a number of reasons why this is the case.

For one, although it might be easy to assess which candidates have the correct "hard" skills for a role, it is not easy to judge their _soft skills._

For example, many roles (especially managerial roles) require skills that are hard to pick out on a CV. If a candidate will have to know how to manage conflicts within their team to be successful in the role, what information on their resume would help you judge their ability to act as a negotiator or conciliator?

Additionally, you might hire the perfect candidate for the current role only to see the role change over time, causing this perfect candidate to suddenly appear less than perfect.

Imagine you're a start-up and decide to hire a CEO who is an expert in scaling and growing businesses. But what happens when your market gets a new competitor and your business starts to decline? Suddenly you need a CEO who is innovative and can outthink your rivals, not just someone who is good with growth. Your current CEO just wouldn't fit the bill anymore.

### 5. Politics, desperate candidates and psychological biases prevent us from making great people decisions. 

Would you consider yourself a rational person? Probably. In fact, most of us think that we make decisions by carefully (and rationally) weighing the choices available to us. But this simply isn't the case.

Our minds are far from perfect, rational computation machines, and we're prone to psychological biases that affect the way we think and make decisions. This is no less the case when it comes to people decisions. Two of the most common biases are procrastination and herding.

We all tend to _procrastinate_, that is, leaving things alone until they absolutely must be changed. When it comes to people decisions, we most often see procrastination in practice when we know that we need to let a failing employee go, but don't. Instead, we put off making the difficult choice until even more damage has been done.

_Herding_ is also fairly straightforward: while we might think we act individually, we actually feel most comfortable following the herd. So, when we're deciding who to hire for a role, we defer to the majority verdict, even if we think that verdict is incorrect.

A final factor that prevents us from making great people decisions are candidates' and recruiters' motivations.

On the one hand, candidates can sometimes be desperate for a job or an opportunity to move up the career ladder. Depending on their individual circumstances, many feel they _need_ the job, and this can cause them to exaggerate or lie to get it. In fact, one poll even found that 95 percent of college-age people would, by their own admission, lie to get a job!

On the other hand, recruiters have their own incentives. Hiring isn't always fair; nepotism, favors and hubris can become real problems in the hiring process. As a result, companies are sometimes left with employees who simply aren't suited to their respective roles.

### 6. Making great people decisions requires you to know when to make them. 

If you want to start making great people decisions, the first thing you need to learn is how to identify _when_ it's time to make a change.

We often make critical decisions at the very last possible moment. As you've learned, procrastination is among the psychological biases that keep us from making the best people decisions.

Unfortunately, in the modern world, procrastination can be disastrous. For instance, while we wait for a failing manager to turn things around, the market can run away from us before our very eyes.

This situation has to be be avoided. As soon as it becomes clear that change is needed — for whatever reason — companies have to make a move. If this means letting people go, so be it; don't try to avoid the unpopular decisions, just be honest and take action. 

But in order to identify the situations where changes are necessary, managers need to get in the habit of evaluating their teams, both in terms of what is needed now, and what will be necessary in the future. 

The first step in identifying these situations is to analyze your team's competencies. Do you have strong performers? Managers with great potential? Are the people who will succeed your executive team already on your staff? If you answered "no" to any of these questions, it's time to think about making changes.

It's also important to remember that just because your team is doing well now, that won't necessarily be the case in the future. It's important to keep an eye on your future needs when making people decisions.

For example, the team that has been working miracles during a serious market downturn might not be the right team for your company once you've made it through to the other side.

Now that you have a firm understanding of when to implement change, the following blinks will concentrate on _who_ you should be looking for.

> _"In good times and bad times alike, we tend to postpone making important people decisions until it is too late."_

### 7. In order to make great people decisions you have to know what you are looking for. 

Once you've recognized that change is necessary, it's time to start making great people decisions. But where should you begin? It all starts with knowing what to look for.

When building your team, you aren't looking for the "perfect employee," as they almost certainly don't exist. Rather, your job is to ensure that your employees have the right combination of skills.

You'll have to take many things into consideration when hiring to fill a role, such as an applicant's IQ, experience, potential and strength in the core competencies necessary to perform their job functions well. 

But there is also _emotional intelligence_ (EI), which refers to a person's ability to manage both their own and others' emotions. 

Try as you might, it's unlikely that you'll find a candidate who excels in _all_ of these areas. You will undoubtedly have to make trade-offs. So which skills should you prioritize?

No matter what role you're hiring for, you'll want to ensure that the candidate who gets the job has high levels of EI. Whatever other strengths they may have as a candidate, without EI they will probably fail in the long run. IQ is also universally important, but it shouldn't be weighted as strongly as EI.

The next considerations involve the role itself. If you're trying to find someone for lower management or for a non-management position, then you should be looking for high levels of potential measured by candidates' ambition and their ability to learn as they go.

If, however, you're recruiting someone for an executive role, experience becomes more important than potential.

Likewise, it's extremely important that you clearly define which competencies candidates will need _before_ you start recruiting. Make sure you have the discipline to carefully analyze what you need instead of hiring on the fly!

> _**"** Each combination of job and organization calls for a distinctive set of competencies for outstanding performance."_

### 8. Use sourcing and benchmarking to find the best candidates. 

When it comes time to fill a role, most companies post an application online and wait for people to apply. Simple, yes — but also ineffective. This method only allows you to reach the people who are looking for a job _and_ using that particular site to find one. As a result, many potential candidates get left out. So what should you do instead?

One way is to develop a shortlist of the best candidates using a technique called _sourcing._

Sourcing consists of asking people (usually other executives, managers, consultants, and so on) whether they can recommend anyone who would be suited to the role you need to fill. This method takes advantage of the networks and connections already in place in the professional world. 

You've probably heard of the phrase "six degrees of separation," referring to the idea that we're all connected to one another by a maximum of just six links of friendship. So, if you ask the right people, the right candidate won't be far away.

By the same token, it's also important to consider that the best candidates don't always come from the outside. Be sure to include candidates who are already part of the company on your shortlist. 

But how many people should you put on your shortlist? The ideal number depends on the job, but there are nonetheless some ballpark figures to consider.

In general, you need roughly 20 people to pick from to ensure you find a good match. Since narrowing down to just 20 candidates can be tough, use _benchmarking_ to help prune your list.

To do this, create a profile of your ideal candidate for the role and then judge each candidate based on their likeness to this profile. This lets you quickly and easily weed out those who clearly wouldn't make a good fit.

Using this very technique, one US company that was recruiting a new manager for their Asian market managed to cut down a shortlist of 100 top candidates to just 12 of the very, very best.

> _"The trick is to find the best potential candidate for each situation, considering both insiders and outsiders."_

### 9. Use the power of structured interviews to assess and find the right candidate. 

How many job interviews have you attended in your life? How did they go? Were the questions relevant, or did they seem bizarre? And what about the interviewer: did he or she tend to ramble? 

You probably answered "yes" to at least one of these questions. In fact, most job interviews are not particularly effective; they are largely unstructured, and the questions don't elicit information that is actually valuable. 

So don't make the same mistake! Instead, plan your interviews carefully using thoughtfully selected questions.

The first step in conducting a productive interview is to clearly define the core competencies of the role for which you are hiring. What are the most important characteristics that define the ideal candidate? Does she need to be results focused? Is strategic thinking essential? Should she have strong interpersonal skills?

You then need to ask interview questions that will help you judge a candidate's strength in each of these competencies. The best way to do this is by asking _behavioral questions_, that is, questions that require the candidate to explain how she has handled past situations.

If you want to learn about a candidate's team leadership competence, for example, you can ask her to describe times when she has made her team more effective, or has helped them achieve a goal. 

However, making judgments in an interview is difficult, and asking the right questions isn't worth much if you can't glean any good information from the answers.

As you've learned, human beings are notoriously bad at making judgments, and further compounding the problem is that fact that we also tend to make snap judgments.

If someone talks in a loud, confident voice in an interview, you may be impressed by their apparent authority without realizing that what they've said is actually completely vapid. It's important to enlist professional experts who can adequately train you, and whoever else will be hiring, in order for you to become incisive when judging people and their expressions.

### 10. Finally, you have to attract the candidates you have chosen and help with a supported integration process. 

If you follow all the advice presented thus far, you will be well on your way to finding the best candidate for the job. But there's still one factor left to consider: what if they don't want to join?

Recruiting is a two-way affair: you want to hire the best person for the job, and they have to want to work for you. If either of these is missing, it's "no deal."

In order to win over the best candidates, you have to make your business attractive for high-caliber employees.

Most obviously, if you want good people, you're going to have to pay them a good wage. Even if you think you're the best company to work for, if the compensation isn't there, the best candidates simply won't be interested.

Next, you have to sell your company to them by demonstrating your passion. Tell them why the job you're considering them for is so great. Maybe your company is helping make the world a better place, or maybe the company's success means the candidate would really be going places. 

Also, go the extra mile to show them how much you want them on your side. As an example, the author even travelled to another continent in order to convince a candidate to join his team — that's real dedication!

Once a deal is finalized, you should do everything you can to integrate new hires. New personnel often face a steep learning curve, and without help and support, the probability of failure is high.

So look after them carefully, teach them all they need to know about the job (even before getting started) and support them in their early days with the company.

If they fail to integrate, then be honest with yourself and the candidate. Recognize that you've made an incorrect people decision and pull the plug.

### 11. Final summary 

The key message in this book:

**Plenty of factors influence a company's performance. Despite often being overlooked, people decisions actually have the greatest impact of all on a company's success. By placing a high priority on the decisions you make regarding personnel, you can better position yourself for success — regardless of your company's stage of development.**

Actionable advice:

**Always check references.**

Passion for the job isn't the only thing that motivates interview candidates. Money, prestige and career desperation can all play a role in how badly someone wants the job. As a result, people will sometimes say anything to get a job, which means that a great interview performance doesn't necessarily make a great candidate. Thus, before you carry through with an offer, always check the candidate's references to make sure they are as experienced and competent as they say they are.

**Suggested** **further** **reading:** ** _Leaders Eat Last_** **by Simon Sinek**

_Leaders_ _Eat_ _Last_ explores the influence that neurochemicals have on the way people feel and consequently act, and examines the discrepancies between how our bodies were designed to function and how they function today. Ultimately, we need true leaders to direct us back on the right path.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Claudio Fernandez-Araoz

Claudio Fernández-Aráoz is an author, speaker and internationally recognized expert on talent and leadership. He is also the author of another bestselling book, _It's Not the How or the What but the Who_.

© Claudio Fernández-Aráoz: Great People Decisions copyright 2007, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

