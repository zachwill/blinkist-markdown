---
id: 54d88fd6383963000a140000
slug: great-work-en
published_date: 2015-02-09T00:00:00.000+00:00
author: David Sturt
title: Great Work
subtitle: How to Make a Difference People Love
main_color: 829999
text_color: 4E5C5C
---

# Great Work

_How to Make a Difference People Love_

**David Sturt**

Whether you're an entrepreneur or a corporate employee, Great _Work_ lays out the five primary skills you need to make a difference that people will love. These blinks look at iconic innovations from recent history to offer clear examples of how you can create or improve upon your ideas so they have a positive impact on people's lives.

---
### 1. What’s in it for me? Learn how to make a difference with your work. 

Drawing on a study of around two million instances of award-winning results in corporations worldwide, the author charted the ways in which high performers made a difference.

He captures examples of entrepreneurs and organizations helping themselves _and_ their employees by encouraging passion and productivity at work.

These high performers share five skills: asking the right question; seeing for themselves; talking to their outer circle; improving the mix; and delivering the difference.

You'll find out that making a difference and doing great work just means finding out what people truly love.

You will learn how to make a difference that people take to heart, and to go on making a difference throughout your career.

In these blink you will learn

  * the story behind _The Cat in the Hat_ ;

  * how Instagram was created from a single feature of a completely different app; and

  * how the inventor of instant photography came up with the Polaroid.

### 2. Think about how the way you do your job can benefit others. 

Imagine that you're recovering from surgery in a hospital. Which would you prefer: a janitor who noisily rushes through his cleaning routine in the ward, or one who recognizes your suffering, and works carefully and quietly?

Clearly, the way this janitor does his job affects your well-being. In the same way, the way you do _your_ job will always have a huge impact on others.

Yet most of us work with our heads down, so immersed in checking off our daily to-do list that we miss all of the other factors in work life. The noisy janitor, for example, isn't trying to be insensitive. He's just preoccupied with getting all the rooms cleaned as quickly as possible.

However, we can broaden our perspective by working with our head up. _Job crafting_ can help us change our perception of work — from being something we just need to do, to a vehicle through which we can make a positive difference in people's lives. In this example, it means the janitor changing his routine so he causes the least disturbance and noise.

But crafting your job isn't enough. You can also _reframe_ in order to discover what benefits you can provide for others.

Reframing is when you make a mental link between your job and a greater purpose: its value to society and its potential to make a difference.

Reframing is about considering how your work affects others, and trying to see a larger purpose beyond the immediate goals of your job.

A hospital janitor might see the potential to improve patients' mood simply by talking to them and making subtle rearrangements in their rooms.

You too can take your work beyond the to-do list by reframing your role.

### 3. Great work builds upon the good work that already exists. 

Would you consider the person who invented the wheel to be a great inventor? Yes? Then what about the one who invented the axle?

The wheel was a smart piece of work, but good work is just the starting point — the foundation upon which _great_ work is made.

Civilization's game-changing innovations are the result of adding and tweaking good ideas and inventions in order to create something even better.

As Carl Sagan said: "If you want to make an apple pie from scratch, you must first create the universe." You need the ingredients — such as apple, wheat and cinnamon — to exist before you can turn them into delicious apple pie.

However, if you only build upon what you already have, you're placing considerable constraints on what you can do. But don't see this as a problem; these constraints are actually the ideal starting point. They can lead to amazing creativity.

For example, when illustrator Ted Geisel was trying to make a positive change in the way children learn to read, his boss narrowed down his options: he could use only the 225 words that every six-year-old knows — most of which are two syllables long and few of which are verbs — and then write a story they could read.

Rather than becoming frustrated by the limitations of this project, he embraced them as a challenge. His breakthrough came when he found two words on this restrictive list that rhymed: "cat" and "hat."

In 1957 he published the first version of _The Cat in the Hat_ and forever changed children's literature. Geisel is the author you know as Dr Seuss, and his works remain popular to this day.

> _"Good is the foundation of great."_

### 4. ASK: Great work begins by asking the right question. 

In 1944 a three-year-old girl asked her father the right question as he was taking a picture of her: Why couldn't she see the photo right there and then?

Her father explained that the films needed to be developed in various chemical baths in a darkroom. For the kid, this answer meant nothing.

Fortunately, her dad decided he didn't like that answer either, and set out to do something about it.

Great work comes from taking the time to question what's missing in our lives that the rest of the world would love, too. Everything we use in our day-to-day existence was formed by the question of whether there are improvements that could be made to the status quo.

Back to the three-year-old girl: three years after she asked about the picture, her father, Edwin Land, gave the world something that people would love when he co-founded the Polaroid Corporation, forever revolutionizing photo development with the creation of instant photography.

When asking yourself these crucial questions, make sure you keep three things in mind:

  * _Tackle the problem_. Problems are opportunities, so don't run away from them! Edwin Land wasn't satisfied with the initial answer he gave his daughter, and decided instead to look more closely at the question she asked.

  * _Consider what you're good at_ and try to use it in your solution. Land had worked with a group of scientists to create a polarizing filter for sunglasses. He put this knowledge to use when answering his daughter's question.

  * _Think outside the box_. Think big! Try to imagine what kinds of things people would love if anything were possible.

By asking the right questions and applying the right attitude, you'll be able to change the world for the better.

### 5. SEE: Use your unique perspective to make great work. 

Today, Netflix is a household name with global success, which makes it easy to forget that, earlier on in their existence, the company was in serious trouble.

At that time a large part of their business involved mailing DVDs to customers, who then returned them. But mail sorting machines destroyed DVDs packaged in normal envelopes.

This was costing the company loads of money, but it only took a trip to the sorting office for Netflix co-founder Jim Cook to find a solution: a low-cost "Netflix Envelope" sturdy enough to protect DVDs.

So what can we learn from this? Essentially, the best work often comes when you take a look for yourself.

We all see things differently, and our own perspective draws on our own ideas and our personal knowledge.

Seeing things for yourself isn't just about being observant of the things around you — you can also gain perspective from looking into the past.

Take a page out of Amazon's or iTunes' books: they're constantly looking into your search and purchase history in order to see what you might be interested in _now_. You too can find patterns in the past, and use them to form insights about how to give people what they love.

One of Whirlpool's most famous products resulted from an examination of the past. After considering trends in washing machines over the years, they reintroduced the top loader, which would become one of Whirlpool's most successful projects ever.

### 6. TALK: Often the greatest ideas emerge when you engage beyond your inner circle. 

Did you know that 80 percent of the 16,000 words that we utter each day are spoken to the same five-to-eight person group of friends and trusted confidants?

That leaves you with only 3,000 words for the people you don't usually talk to — and it's these people who have the greatest impact on your creativity. Talking to people you don't usually talk to can lead to ideas that you couldn't put together on your own.

Your inner circle is a comfortable place to start conversations about your ideas and aspirations. However, because they think like you, support you and don't want to hurt your feelings, you can't count on their objectivity, and this can put you at a disadvantage.

But we _do_ find divergent thinking, unexpected questions, difference of opinion and added expertise from our _outside circle_ — people who operate beyond our more intimate peer group.

These people will give you feedback that contradicts your ideas, and that's an asset, because it shows you how your ideas or approach might be going wrong.

But it's crucial that this challenging feedback is high quality. You can obtain this by presenting your ideas to others in a way that taps into their desire to share their opinion.

You aren't merely asking them to solve your problem for you, you're inviting them to participate in making a difference. This shared responsibility gives them an opportunity to focus their insights and talent in a way that is meaningful to them.

Simple phrases like "Can I get your thoughts on something?" "I have this idea but don't know where to start, so can you help me?" or "Have you had any experience with something like this before?" can be great starting places for productive conversations.

Whatever you do, make sure that you don't limit your conversations with outsiders to just 3,000 words!

> _"There is a very natural, very human fear in sharing one's ideas. And yet it seems to be the x-factor, the differentiating thing."_

### 7. IMPROVE THE MIX: Work on your ideas before putting them into practice. 

So far, we've spent a lot of time talking about how to approach work in a way that will give you positive, creative ideas. But this isn't all. You must also edit and improve your ideas before implementing them in order to make a difference people love.

Great workers create changes in their mind before making them a reality. Your goal should be to model, adjust and play with your awesome ideas _before_ executing them, if you want to give them the greatest chance for success.

There are three tricks which can help you along the way.

First, _add something new_. Simple additions that lead to massive improvements are all around us: just think of the rolling suitcase (suitcase plus wheels) or heated car seat (car seat plus heater).

Start by brainstorming with a ton of ideas, always asking yourself whether these additions improve the product or concept and whether people will love it.

Also keep in mind that these additions must be useful. Sometimes we're so eager to innovate that we end up with additions no one cares about. How many of your remote control's features do you actually use, for example? And how many are just annoying or confusing?

Second, you can improve by _simplifying_. When we want to improve something, our first impulse is to add to it. But additions don't always lead to improvement.

In fact, removing things can be a great way to make a difference that people love. Think of the iPod: by removing a set of buttons and replacing them with a scrolling wheel, Apple removed complexity from their MP3 player while also making it a design icon.

In essence, look at the features that people don't love. Those are the ones you can probably remove.

Finally, _harmonize all your ideas_. Your ideas for improvement should be clearly related to one another. Make sure that everything works together.

> _"Most ideas aren't grandiose ideas. They are little improvements."_

### 8. DELIVER THE DIFFERENCE: A great product is not finished after production – it’s finished when it makes a difference. 

Kevin Systrom and Mike Krieger once created an app which they named Burbn. The app was designed to allow users to share their location with friends, as well as share photos.

Never heard of it?

That's because it was a complete failure. People didn't care about "checking in," and the app was too slow and had too many features.

This kind of failure is common with new ideas; it's how you handle these failures that will separate you from everyone else.

Great work does not end once the product is complete. It ends when you make a difference.

However, to change this definition of "complete," you need a _growth mind-set_ rather than a _fixed mind-set_.

People with fixed mind-sets believe that their successes are the result of their natural ability and intelligence, and don't pay attention to negative feedback or learn from failure.

On the other hand, people with a growth mind-set know that success isn't innate — it's the result of learning and failing. They're always open to improvements led by feedback, good or bad.

Systrom and Krieger could have easily given up on Burbn, or fixed its obvious errors.

Instead, they embraced a growth mind-set, opting to find out whether their app had a redeeming quality that people loved. The answer was "photo sharing." They took this information and ran with it.

They added cool filters and borders and made the photos shareable across multiple social network platforms, and finally relaunched their app in 2010 under a new name: Instagram.

### 9. Final summary 

The key message in this book:

**Everyone can make a difference in their work, as long as they have the right mind-set and a willingness to explore. By mastering the five skills of asking, seeing, talking, improving the mix and delivering the difference, you too can make a difference that people love.**

Actionable advice:

**Don't lock yourself inside your inner circle.**

Close colleagues and old friends offer a kind of ease and comfort that allows us to freely share ideas, but this familiarity can actually stifle your creativity. The next time you have an idea, invite outsiders into the conversation in order to learn from their unique perspective.

**Suggested further reading:** ** _Linchpin_** **by Seth Godin**

_Linchpin_ explains why you should stop being a mindless drone at work and instead become a linchpin — someone who pours their energy into work and is indispensable to the company. It is not only better for your career but it also makes work far more enjoyable and rewarding.
---

### David Sturt

David Sturt is the executive vice-president of consultancy O. C. Tanner, as well as an advisor for numerous _Fortune_ 1000 leaders. He speaks to audiences worldwide on engaging employees, inspiring contributions and rewarding outstanding results.

