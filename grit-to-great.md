---
id: 56786a880bac9b0007000024
slug: grit-to-great-en
published_date: 2015-12-25T00:00:00.000+00:00
author: Linda Kaplan Thaler & Robin Koval
title: Grit to Great
subtitle: How Perseverance, Passion, and Pluck Take You from Ordinary to Extraordinary
main_color: F4C169
text_color: 755D32
---

# Grit to Great

_How Perseverance, Passion, and Pluck Take You from Ordinary to Extraordinary_

**Linda Kaplan Thaler & Robin Koval**

Many people have talent, but few become stars in their chosen profession. In _Grit to Great_ (2015), two women at the top of the advertising business offer their guide to developing the resilience and hard graft that will help you succeed.

---
### 1. What’s in it for me? Get up off the sofa and start achieving your goals. 

You probably wonder from time to time what your life would have been like if you'd followed your dreams and became a rock star, a famous athlete or a billionaire businessperson. Maybe you think that you just weren't talented enough.

However, as these blinks will show you, talent is not enough. What you need is _grit_. Grit is what made Michael Jordan such a successful basketball player, it's what made the iPhone possible and the reason you know who Oprah Winfrey is. So what is grit? It's the ability to work hard and have a get-back-on-the-horse-again mentality. It's what you need to develop if you want to fulfill your dreams. 

In these blinks, you'll learn

  * why daydreams can be dangerous;

  * how Jia Jang's failure as an entrepreneur made him famous; and

  * how to write a bestseller at the age of 98.

### 2. Talent won’t get you as far as hard work and passion. 

Have you ever had the feeling that others excelled without trying, seemingly coasting on their talent? In schools the world over, some kids never study yet get great grades while others are bent over their books all the time just to keep up. But the truth is that those who rely on talent won't get far in life.

In fact, talent is totally overrated. That's because plenty of talented people have the potential for greatness, but realize from a young age that they can get by on their talent without hard work. On the other hand, less-talented people strive for success and self-improvement. They seek out subjects and jobs they care about, not just ones they are naturally good at. 

So, while it might seem as though talented people are the winners of the world, it's actually those that have to work harder who end up being the most successful. That's because while talented people dream, others achieve. 

Take a look at a study conducted in New York by the psychology professor Gabriele Oettingen comparing the study habits and later careers of graduate students. The analysis found that the students who spent more time daydreaming about their aspirations than actually knuckling down to work ended up with fewer job offers and smaller paychecks following graduation than those who put in long hours! 

Hard work will help get you the job you want. For instance, the authors run an advertising agency, and a few years ago they were desperate to land a big new client — the restaurant giant Wendy's. 

Their strategy?

They pulled 70- and 80-hour weeks for months, even going as far as to work at Wendy's themselves to learn the inside scoop on their operations. Not a waking minute went by that they weren't preparing for their presentation. Well, they landed the contract and Wendy's said they chose the firm because they worked the hardest — and therefore would be the best.

> _"Grit is about sweat, not swagger. Character, not charisma."_

### 3. At some point you need to take a risk, plunge into the unknown and trust your abilities. 

When the highwire artist Nick Wallenda traversed the Grand Canyon on a 1,400-foot-long wire, he slipped and had no safety net to catch him. But he didn't fall. Instead, using nothing but his ability and intensive training, he managed to stay on the wire. 

And just like Nick, there's going to come a time in your career when you need to lose the proverbial safety net. That's because if you've got grit and passion, your career will take off, and if you want to ride the rising tide of this success you will have to let go of your safe zone. This is the moment when people with grit start taking calculated risks and diving headfirst into uncharted waters.

But that doesn't mean you should jump into just any situation. For instance, say you're the CEO of a company and are itching to enter a new market. It's essential to scope out the risks and competition first, because there's nothing worse than failing to assess potential dangers and taking your company down as a result. 

However, if you do fall short, grit will help you embrace and learn from rejection. For example, maybe you were an aspiring entrepreneur but couldn't face being turned down by investors. Well, while rejection might sting, you can always shine a positive light on it. 

Just take Jia Jiang, who left the safety of his tech job and, inspired by Steve Jobs, set out as an entrepreneur. He kept getting rejected and pretty soon was on the verge of giving up, but instead buckled down and turned his rejection to his advantage by starting the _100 Days of Rejection_ project. Jiang endeavored to do a different task every day for a hundred days — like ordering just a quarter of a shrimp at a restaurant or trying to borrow $100 from a stranger — each of which was meant to help him overcome his fear of rejection.

He put his project online and became famous in the process. Now he's got a book deal and has appeared on TED talks.

> _"Rolling over is a lot less painful than falling on one's face."_

### 4. Success doesn’t come overnight and patience is essential to meeting your goals. 

Did you know that Van Gogh only sold one painting before he died? It might come as a shock since his paintings have wowed art lovers across the world and fetched record-breaking prices. But even though he didn't get recognition during his lifetime, Van Gogh still produced over 2,000 paintings!

How?

Through dedication and grit. 

That's because most projects take a long time to make real progress and even longer to find success. As a result, many people lose motivation and patience halfway through and simply give up. But there's another option.

You can keep motivated during long-term projects by taking joy in the little achievements that mark your way. For instance, state attorney Mike Moore, made famous by his legal battle against Big Tobacco, spent years trying to hold huge tobacco conglomerates responsible for countless lung cancer-related deaths. But his attempts were rejected time and time again by a variety of different state attorneys. 

However, over the years, he slowly built a stellar team, was motivated by the small measures of support he _did_ receive from other state attorneys and finally got the courts to force tobacco companies to pay $100 billion to 46 different states, covering the medical costs of smoking.

But some people will feel that as they get older, less is possible. Therefore, it's essential that as you age, you keep your motivation up and never settle. Instead, make productive use of your time by planning every year. Don't let a "not-yet" attitude rule your decisions and never feel like you're too old to do the things you're passionate about. You aren't too old, and you never will be. 

For instance, Captain James Henry, who was raised on a farm but went on to become a fisherman, didn't learn to read until he was 92. Not just that, but he wrote an autobiographical bestseller called _In A Fisherman's Language_ at the ripe age of 98.

### 5. Success means learning from your experiences and adapting to new ones. 

We've all experienced failure and everyone knows it can be tough. However, it's not your failures that define you, but what you do with them. For instance, even star poker players are dealt losing hands some of the time, but the best of them know how to learn from their mistakes and grow through them.

And it's not just true for card players. Regardless of what you do, over the course of your career you'll inevitably face failure and seemingly insurmountable obstacles, but every single one of these challenges is a chance to learn and improve. For instance, Eleanor Longden was diagnosed schizophrenic after hearing voices in her head that played on her most deep-seated fears of failure and abandonment. 

But she didn't let her illness defeat her. Instead she began studying psychology, earned her Ph.D. and is now on the board of a global support and research organization that helps people with the same mental illness she has. 

However, rising from adversity doesn't mean that grit should come from the fear of losing everything. Instead, it should be about gaining more than you'd ever imagined. In fact, people with grit aren't successful because they fear failure, but because they see what they stand to gain by accomplishing their goals. 

This way of thinking enables people with grit to accomplish what seems impossible and conquer any challenge they meet. In other words, they don't fail, they fail forward. For instance, early on, Oprah Winfrey was demoted from her first job as a television anchor because her boss didn't think she was "made for TV." But obviously she didn't give up. She was determined to succeed and we all know how she turned out: one of the biggest TV stars in history.

### 6. Helping others will boost your grit. 

Do you ever wonder how humans thousands of years ago managed to survive, despite the elements, diseases and wild animals? The key to their success was group work: helping each other, sharing food and increasing each other's quality of life was essential to the development of the first human communities. The same is true today.

In fact, helping others will actually boost your _own_ motivation. For instance, have you ever lent a hand to an elderly women by carrying her heavy bags? Have you offered a blanket, shelter or food to a homeless person? If you have done these things, or anything like them, you've probably felt like a better person and experienced a wave of gratification.

Well, it's this gratification that can increase your grit. Just take Navyn Salen, who used to be a stay-at-home mom with three kids. One day she made the decision to help African children suffering from malnutrition. It took her years to build an NGO and the necessary support to expand her network, but after years of investing her every free moment, she has now managed to help over 2.5 million children in 44 countries.

So, helping other people can lead you to do things you wouldn't have thought possible, but it also helps you build character and meet new people. That's because helping others will give you a deeper connection to your own emotions and personality. For instance, you'll see that you can act selflessly and be kind. But you'll also meet other like-minded people to collaborate with — something that will take your work to the next level.

Not just that, but once you meet people who share your values, why not offer some of your grit to them? It's easy, just teach other people to be kind, resilient, and to persevere. The result will be increased motivation for everyone involved.

> _"With grit, there's no telling how far you can go, how much you can do, or how successful you can be. So what are you waiting for?"_

### 7. Final summary 

The key message in this book:

**The key to success is** ** _grit_** **: resilience, patience, passion and hard work. So, instead of daydreaming and coasting on your natural abilities, buckle down and strive for your goals. Doing so means using failure to your advantage, taking calculated risks and courageously facing the unknown.**

Actionable advice:

**Two birds with one stone: run a marathon and give up junk food.**

Lots of people wish they could run a marathon yet many of us still spend Saturday sprawled on the couch with a bag of chips. But it's easy to meet this goal while kicking your junk food habit. Just make a training plan and every time you feel tempted to indulge in fast food, reach for your running shoes instead!

**Suggested** **further** **reading:** ** _Mindset_** **by Carol Dweck**

_Mindset_ discusses the differences between people with a fixed mindset versus those with a growth mindset. Our mindset determines the way we deal with tough situations and setbacks as well as our willingness to deal with and improve ourselves. This book demonstrates how we can achieve our goals by changing our mindset.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Linda Kaplan Thaler & Robin Koval

Linda Kaplan Thaler is an Advertising Hall of Fame icon and the mind behind some of the most well-known advertising campaigns of our time. She is the chairman of Publicis Kaplan Thaler and was the CEO as well as co-founder of the Kaplan Thaler Group.

Robin Koval, president and CEO of the Truth Initiative, is the other co-founder of the Kaplan Thaler Group. She's also a faculty member of New York University's Steinhardt School.

