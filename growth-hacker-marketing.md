---
id: 54bd242c633234000a650000
slug: growth-hacker-marketing-en
published_date: 2015-01-21T00:00:00.000+00:00
author: Ryan Holiday
title: Growth Hacker Marketing
subtitle: A Primer on the Future of PR, Marketing and Advertising
main_color: D87A31
text_color: 8C4F20
---

# Growth Hacker Marketing

_A Primer on the Future of PR, Marketing and Advertising_

**Ryan Holiday**

_Growth Hacker Marketing_ charts a major departure from traditional marketing practices, relying heavily on the use of user data and smart product design. This book illustrates how today's top technology companies, such as Dropbox and Instagram, have used this strategy to gain millions of users.

---
### 1. What’s in it for me? Reach customers easily, effectively and cheaply through growth hacking. 

While the business world obsesses over the latest marketing buzzwords, from big data to virality to social media, they're overlooking the most basic question: "How can I get more customers?"

A business may want its product to "go viral," for example, yet still rely on old-school marketing approaches to get there. The truth is, if you want more customers, you have to re-evaluate every part of your company, not just your marketing strategy.

That's what _Growth Hacker Marketing_ is all about. To successfully market today, you need to understand that the product itself is the best way to capture new customers.

With that in mind, these blinks explain how companies like Dropbox, Instagram, Twitter and Groupon all used cheap and easy growth-hacking techniques to grow exponentially.

After reading these blinks, you'll know

  * how Dropbox created serious buzz without a big launch party;

  * why you shouldn't consider "everybody" as your target market; and

  * how the author used growth hacker marketing techniques to sell this book.

### 2. Growth hacking is a new, low-budget form of marketing aimed at rapid growth. 

Today's top internet companies, like Dropbox and Groupon, are now household names, but how did they build up their brands so quickly?

Instead of taking the traditional approach to marketing and buying huge billboards and newspaper ads, these companies used _growth hacker marketing_, a low-budget approach which differs from conventional marketing.

For instance, while traditional marketers ask, "How can I get customers?", growth hackers use technology to answer that question, tracking user behavior and adjusting products accordingly.

Consequently, this new approach requires growth hackers to work on products, breaking down the division between marketing and product development — and in the process, redefining marketing.

Because for today's start-ups, marketing is no longer about using big budgets to help big companies secure a one-percent annual growth rate; rather, it's about using a small budget to help a small upstart become the next big thing.

After all, most start-ups can't pay for traditional mass media marketing campaigns. So to attract a massive customer base, they have to get creative. And that's why Instagram, Dropbox and Twitter have all used growth-hacker techniques to reach millions of users.

It's also important to note that growth hacking aims to achieve rapid growth by continually improving the product. This is not the case for more traditional approaches; for traditional marketers, the most important stage occurs before a product is launched, when marketers build up "buzz."

But growth hackers don't have to launch a product by throwing a party and inviting celebrities. For them, the most important stage occurs _after_ the launch, when they get busy measuring everything they can (from click rates to Facebook "likes") and applying what they've learned to improve the product, with the aim of boosting growth.

And since these start-ups aren't developing consumer goods like laundry detergent, the products don't _have_ to be perfect at launch, but can be optimized over time.

If you're interested in trying growth hacking for yourself, press on! The next blinks will describe four steps you can take to growth hack your product.

### 3. Figuring out what people really want from your product is the first step of growth hacking. 

Ready to implement growth-hacking techniques to boost your brand?

The first step of growth hacking is all about having a product people want. Because even if your prototypes are bug-free and flawless, they'll fail if there's no actual demand for them.

It's worth noting that creating a product people needed wasn't always a consideration for traditional marketers, who believed they could sell anything with a clever campaign — even stuff people didn't really want.

But growth hackers think differently, and are focused on creating a product that truly satisfies the needs of a specific group of people — a concept encapsulated in the term _product market fit_.

The idea is that satisfied customers will become evangelists for your product, spreading the word for free and allowing you to bypass advertising costs.

So to determine product market fit, ask yourself: How does this product correspond to people's needs? Is it useful? Does it add something to people's lives?

For example, Instagram first launched as a social network with optional photo capabilities, but the founders realized people were almost exclusively interested in using its filter-enhanced photo feature. So they concentrated on this element and achieved product market fit, eventually becoming so successful that Facebook acquired the company for $1 billion.

Product market fit may seem elusive, but it's much easier to achieve than you might think, as long as you're paying attention to what people want.

Some authors blog extensively before they publish a book. This allows them to use the online interaction to understand what topics engage their readers, to then develop books based on these insights.

Also, authors can ask readers for feedback on cover and title ideas online to make sure that, in the end, people really like what they're producing.

> _"Today it is a marketer's job as much as everyone else's to make sure product market fit happens."_

### 4. Targeting the right group of people is the most effective growth strategy for a start-up. 

The next step of growth hacking is about creating customer awareness around your product. Because, after all, even the best product in the world won't be successful if people don't know it exists.

For instance, before he co-founded Reddit, entrepreneur Aaron Swartz created a collaborative encyclopedia (similar to later-launched Wikipedia) and WatchDog.net (similar to Change.org). Although both were great ideas, they failed because Swartz didn't get enough attention for them.

To avoid this fate, growth hackers — like other marketers — are focused on developing effective growth strategies. But unlike traditional marketers, growth hackers devise new, creative tricks to do so.

One example of a typical growth-hacker innovation would be to install an invite-only feature to create hype. And that's precisely what Dropbox did upon launching.

Since users needed an invitation to join, the service had an aura of exclusivity. As a result, the waiting list soon ballooned from 5,000 to 75,000 people. Today, anyone can join, and the platform now boasts some 300 million members.

If you want to replicate that kind of growth, there's an important principle to remember: Don't target everybody, just the right people. Since most people won't become customers, it would be a massive waste of time and resources to try to reach them.

Instead of going after "everyone," growth hackers like to target _early adopters_, or people who are eager to try new technologies and trends. Because when these kinds of customers become loyal fans, they're likely to gush about your product to their friends, helping your brand grow organically.

To this end, Uber gave away free rides during South by Southwest (SXSW) 2013, which caters to influential (and at the event, taxi-deprived) techies and hipsters. The company actually waited a year to implement this promotion rather than paying for advertising, as ads could have reached many people, but not so many early adopters.

### 5. The third step of growth hacking is about making your product go viral. 

_Viral_. It's the word of the decade!

And although many people still view virality as something arbitrary and magical, as if it could happen to any product, growth hackers know that there's a reason some things go viral and others don't.

So what's the key to unlocking virality? Well, growth hackers ask themselves a few simple questions: Why should customers share this? Is sharing easy? Is this product worth talking about?

Think about it like this: Whenever people share your content, it's like they're doing you a favor for free. So to really make it worthwhile for your customer, you have to give them something they _want_ to share. You can do so by following a simple, two-step process.

First, make your product worth sharing. And second, encourage sharing!

For example, Groupon launched a "Refer a Friend" campaign that gave customers a $10 credit when a referred friend made their first purchase. So essentially, Groupon encouraged its customer base to spread the word and rewarded it for doing so.

In addition to creating incentives to encourage sharing, publicity can also play an important role in making something go viral. This is mainly because, as virality scientist Jonah Berger first noted, a product or an idea is more likely to become popular when it's noticeable.

This was certainly the case for music-streaming service Spotify, which benefitted from Facebook's enormous user base when it integrated its service with the social media giant. It worked because, once people saw that their friends were listening to Spotify, they wanted to try it too.

Apple has also effectively tapped customers to generate free publicity. For instance, by manufacturing iPod headphone cables in white, instead of the typical black, Apple ensured its customers would become free walking advertisements — as you immediately recognize Apple cables when you see someone wearing them on the street.

Creating this kind of brand awareness without an expensive campaign is a cornerstone of growth hacking.

> _"Virality isn't luck. It's not magic. And it's not random." –Jonah Berger_

### 6. The fourth step of growth hacking focuses on improving your product to retain customers. 

Too many marketers stop once they've attracted customers to their product. "Job done!" they say.

But with this kind of mind-set, it's only a matter of time before you start losing unsatisfied customers. If you want to achieve sustainable, long-term growth, you have to focus on keeping your customers around _after_ you've won them over.

And to execute a marketing strategy oriented toward customer retention, you need to find the right metric to measure performance. Luckily, there are many tools aimed at measuring how many customers stick around — that is, your _conversion rate_.

How to define the conversion rate will vary for each business (the classic example is measuring what percentage of website visitors become paying customers), but once you've established it, you can figure out how to improve it. 

For example, since Twitter garnered major publicity early on, many people visited the site and created accounts, but few actually became active users.

To improve their low conversion rate, Twitter hired growth hackers who discovered that people were more likely to stick around after they manually selected accounts to follow on their first day.

So instead of giving each new user a default list of 20 accounts to follow, Twitter prompted customers to follow ten people on their own. This small tweak worked, earning the service more active users.

Twitter's actions align with an important principle of growth hacking: Find ways to improve your service and convert inactive users into active ones.

This is very important, as focusing on customer stickiness is the best way to maximize _return on investment_, or ROI — that is, the money you earn compared to the money you invest. And it's actually much cheaper to improve your service and win over inactive customers than it is to attract new users.

In fact, according to market research firm Market Metrics, profits from sales to existing customers is 60 to 70 percent. Meanwhile, profits to a new customer average five to 20 percent. In other words, profit margins and ROI are lower for new customers than existing customers!

Now that you've learned all about growth-hacker marketing, we want to reveal a surprise: The author actually used the very same growth-hacker marketing techniques we've been discussing to make this book a success. The next blink explains exactly how that worked.

### 7. The author effectively used growth-hacker marketing to promote this book! 

Need proof that growth hacking actually works? The author used these very techniques to launch and promote his book!

He started by "testing" his book idea. So instead of capping months of hard work with a big hardcover book launch, the author wrote an article about growth hacking for the business magazine _Fast Company_.

Publisher Penguin Books showed interest in the idea, and created a short ebook building on the article. It was inexpensive to publish, and allowed Penguin to test audience response.

Since the ebook was well-received, the author expanded the ebook and launched it as a paper version.

Although the author already knew there was an audience for his book, there was still some important growth hacking to do: He had to make the _right_ audience aware of what he was working on.

So he encapsulated lessons from the book into articles and published them on influential sites — like MarketWatch, The Huffington Post, Hacker News and others — for free, reaching his target audience.

The author also contacted well-known growth hackers, like Sean Ellis and Andrew Chen, asking them to promote his book to their social media followers.

And in addition to targeting the right audience, he also connected and rewarded his existing fans. For instance, when he published the initial ebook, the author added a page telling interested readers how to sign up for his newsletter to get more free content.

About ten percent of his readership signed up, which benefited them (since they were happy to receive his content), and also allowed the author to build an email list.

Using this list, he later informed his loyal fans that he'd published an extended hard-copy version of _Growth Hacker Marketing_, hopefully prompting them to buy the book.

As you can see, the author's experience proves that easy, cheap and effective marketing can be done by anyone, anywhere!

### 8. Final summary 

The key message:

**Growth hacking marketers have developed a wide array of simple, low-cost and creative techniques to help companies — no matter how small — achieve rapid growth. Growth hacking has essentially redefined marketing, by breaking down the division between marketing and product development.**

Actionable advice:

**Reward your users for learning how to use your product.**

Making sure that your customers know how to use your product is fundamental. If your service is anything other than clear and easy to use, you risk frustrating your customers and eventually losing them. One way to educate customers is by creating a tour and offering an incentive to complete it. For instance, Dropbox does this by giving users extra storage space when they watch a video tour that explains the basics of the service.

**Suggested further reading:** ** _Go Pro_** **by Eric Worre**

_Go Pro_ offers a complete crash course in network marketing. It gives you a rundown of all the skills you need to become a network marketing professional, and inspires you to develop the attitude that will turn your toil into fortune.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ryan Holiday

Ryan Holiday is a partner at StoryArk, a creative marketing company, and was the former director of marketing at American Apparel.

