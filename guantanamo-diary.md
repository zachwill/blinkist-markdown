---
id: 55ad00646536380007170100
slug: guantanamo-diary-en
published_date: 2015-07-24T00:00:00.000+00:00
author: Mohamedou Ould Slahi, Edited by Larry Siems
title: Guantánamo Diary
subtitle: None
main_color: F0943D
text_color: 8A5523
---

# Guantánamo Diary

_None_

**Mohamedou Ould Slahi, Edited by Larry Siems**

_Guantánamo Diary_ (2015) is the edited testimony of a detainee at the Guantánamo Bay detention center in Cuba. These blinks will walk the reader through the story of one man's interrogation, incarceration and torture at the hands of the US government.

---
### 1. What’s in it for me? One inmate’s extraordinary account of his incarceration in the world’s most notorious prison camp. 

In 2005, Mohamedou Ould Slahi (MOS), an inmate at Guantánamo Bay, began compiling a detailed account of how he ended up in US custody. Stretching back to his first round of questioning in early 2000, the document described his arrest and interrogation in various countries, including Senegal, Jordan and, of course, Cuba, at the US naval base on the island's southeastern tip.

Although he has never been tried and continues to maintain his total innocence, he remains in prison, over a decade after his initial arrest.

This document, heavily edited and redacted by his US captors, has managed to reach the public domain. These blinks, based on his account, tell you his gripping story.

In these blinks, you'll discover

  * why Mohamed Ould Slahi's torture was sanctioned by the US government;

  * why doctors are used to keep prisoners fit for torture; and

  * why US authorities pretended to be MOS's family.

### 2. The author, a Mauritanian citizen, has been held without charge in Guantánamo since 2002. 

You've probably heard of Guantánamo Bay, but how much do you know about the actual experiences of the prisoners detained there? One long-term prisoner is Mohamedou Ould Slahi, or MOS for short, and this is the story of how he found himself in one of the world's most infamous detention centers.

The author was born in 1970 in Mauritania, a country in northwestern Africa. He was the ninth of 12 children born to a traveling camel trader, who passed away shortly after the family moved to the capital of Nouakchott. As a result, the author was his family's main provider from a young age.

In 1988, he received a scholarship to the University of Duisburg in Germany, and traveled there to study electrical engineering. In 1991, he became involved with the anti-communist movement in Afghanistan and took an oath of loyalty to al-Qaeda, an organization that at the time was supported by Western nations, and the US in particular.

But things changed when the communist government collapsed and the Afghan mujahideen began to wage war against one another. At this point, the author returned to Germany and, according to his account, cut all ties with al-Qaeda.

He then completed his degree, remaining in Duisburg with his wife. But since his German visa was set to expire shortly thereafter, he applied, successfully, for a Canadian visa, relocating to Montreal in November 1999. While in Canada, he attended a Mosque previously frequented by an Algerian immigrant and al-Qaeda member by the name of Ahmed Ressam.

What's so important about Ressam?

Ressam was arrested on December 14, 1999, by the US Border Patrol for refusing inspection and giving false information. However, his arrest uncovered something greater: his plan, known as the Millennium Plot, to bomb Los Angeles International Airport (LAX).

After discovering Ressam's plan, the police questioned the Montreal immigrant community about it — and the author in particular.

### 3. Traveling from Canada to Mauritania, the author was apprehended in Senegal. 

In January 2000, the author had been away from his home country for 12 years and decided to return to visit his family. He caught a flight to Dakar, Senegal, and planned to drive the remaining distance to Nouakchott, Mauritania with his brothers.

But the trip didn't go at all to plan.

After arriving in Dakar, the author met two of his brothers and a couple of their friends, but the group was intercepted on the way to their car. The author, his brothers and their friends were handcuffed by special agents, thrown into a cattle truck and taken into custody.

The following morning, Senegalese and American interrogators questioned them. The author was asked if he knew someone who went by the name Ressam and who had been arrested in connection with the Millennium Plot. He claimed to have no information about the man and was transferred from Senegalese custody into the hands of US officials.

Leaving Senegal, he went to Mauritania with his American interrogators, who took him to the country's Security Police headquarters. He was awoken during the night for further interrogation, during which he was accused of masterminding the Millennium Plot and was asked to give information on Ressam and other terrorists.

After multiple interrogation sessions, sleep-deprived and undernourished, the author was a shell of his former self.

Rather than helping him, the Canadian government assisted with the investigation by providing transcripts of the author's phone calls. This information led the author's American interrogators to insist that he divulge the meaning of the words "tea" and "sugar," which had been used in several of his calls. The author insisted that it was not a code, and that he was merely talking about tea and sugar.

After days of interrogation, the author was released on February 19, 2000, and finally continued home to Mauritania. But little did he know that his ordeal was just beginning.

> _"So why was I so scared? Because crime is something relative; it's something the government defines and re-defines whenever it pleases."_

### 4. The author suffered repeated interrogations in Mauritania before being sent to Jordan. 

Although he was released, it wasn't long before US interrogators took an interest in the author once again.

One day, while attending his niece's wedding, the author received a call from Mauritania's Director General of Security. The two men met and drove together to police headquarters, where the author was again questioned.

He was held there for two weeks before American investigators arrived, again demanding to know the coded meanings of various conversations he had had. For instance, what had he actually meant when he told his younger brother to "concentrate on your school"?

But their interest went beyond coded language; he was also questioned on numerous other topics, such as the number of computers he owned or why he had made phone calls to certain places such as the United Arab Emirates — a country he had never actually called.

Meanwhile, the interrogators demanded that he confess before the charges against him worsened. But the author continued to proclaim his innocence, insisting that he had nothing to confess to. During this interrogation, he was denied access to water until the end of the session when, to add injury to insult, he was struck in the face with a water bottle.

The author was once again released without being charged. The Americans left and he returned to his job in technology and media.

But this was only until November 20, 2001, when the Director General of Security of Mauritania paid him another visit and asked him to return to police headquarters. The author complied, driving himself to the police department expecting that the questioning would be brief and that he would soon return home.

Instead, he spent seven days in Mauritanian custody, during which time his family was forbidden from visiting.

Then came November 28, Mauritanian Independence Day. Like a package being sent by airmail, the author was put on a special CIA rendition plane and was flown to Jordan to face more interrogation.

> _"The government is very smart; it evokes terror in the hearts of the people to convince them to give up their freedom and privacy."_

### 5. In Jordan, the author’s interrogators threatened him with torture if he didn’t confess. 

While the author's family were forbidden from visiting him while he was in Mauritanian custody, they didn't even know that he had been sent to Jordan until a year afterwards. In fact, they may never have found out at all if the author's brother hadn't read an article about the author in the German magazine, _Der Spiegel._

But even when they discovered where the author was, his family had no idea how poorly he was being treated. The author arrived, blindfolded and handcuffed, to Amman, Jordan, on the morning of November 29, 2001, and was immediately taken to the notorious House of Arrest and Interrogation.

Why notorious?

This prison in Amman had been accused by Human Rights Watch of utilizing torture methods that included everything from sleep deprivation to hanging prisoners by their hands and feet while inflicting extended beatings. During the author's time there, the facility held at least 13 other detainees. It also had a quarantined area for female inmates and a basement where detainees reported the most violent treatment.

When the author arrived, he was beaten and interrogated by Jordanian officials under orders from the US government. His interrogators continued attempting to link him to the Millennium Plot, asking him again about his connection to Ressam. At one point, the author caught a glimpse of his file, which listed his name and accused crime: participation in terrorist attacks.

The abuse continued. The author could hear people being tortured in adjacent rooms and his interrogators told him that if he didn't cooperate and confess to his crimes, he would be tortured like his neighbors. He was pushed up against walls and struck in the face multiple times by interrogators, who never stopped their barrage of questions.

Unfortunately for the author, Jordan was just a taste of what was to come.

### 6. In 2002, the author was transferred to Guantánamo Bay, where interrogation methods brought his condition from bad to worse. 

On July 19, after spending eight months locked up in Jordan, the author was blindfolded, stripped naked, shackled and diapered. He was then sent, under US orders and in a CIA rendition plane, to Guantánamo Bay, also known as GTMO, where he became inmate #760.

What awaited him at GTMO?

At the time of the author's transfer, a number of factors had led to increased abuse of prisoners at the facility. For instance, the Special Interrogation Plan, authorized and signed into effect by then US Secretary of Defense Donald Rumsfeld, had expanded the range of methods interrogators could use on detainees.

One inmate, Mohammed al-Qahtani, a Saudi citizen and enemy combatant detained in GTMO, was questioned using a Special Interrogation Plan: he was kept nude, was forced to stand and hold painful positions and was subjected to 20-hour interrogation sessions, isolation and cold.

After being used on al-Qahtani, the plan was applied to other prisoners. The Senate Armed Services Committee reported that a Special Interrogation Plan for the author was circulated by military interrogators in January 2003.

Having not heard from his family in months, the author complained, fearing that they didn't know where he was. In response, he received a letter but quickly realized it was a blatant forgery: he didn't have a brother with the name mentioned in the letter, his own name was misspelled, his family didn't live where the letter said they did and the author, familiar with the handwriting of everyone in his family, could tell none of them had written the letter.

It wasn't until seven months after his arrival at GTMO that the author received the first _real_ letter from his family. Completely cut off from his loved ones, the author was about to enter a whole new phase of interrogation.

### 7. At GTMO, the author was interrogated by various US government agencies, accused of terrorist activities and tortured. 

While at GTMO, people from a variety of government bodies interrogated the author and accused him of participating in multiple terrorist attacks. In fact, different organizations were involved in each stage of his interrogation.

For instance, his interrogations in Senegal, Mauritania, Jordan and GTMO were all conducted by different agencies — sometimes it would be the FBI, sometimes the CIA and sometimes even special interrogators for the military.

Meanwhile, the list of crimes he was accused of had grown more and more daunting: being part of the Millennium Plot to blow up LAX; participating in the 9/11 attacks, despite the fact that they occurred a year and nine months after his first arrest, when he was in Mauritania; and finally, serving as a senior recruiter for al-Qaeda, an accusation corroborated by a Department of Defence letter the author was shown, stating that he had recruited three of the September 11 hijackers.

Despite being accused of so much, the author was never officially charged, and was still tortured in several different ways. Once a Special Interrogation Plan was approved for him, the author was subjected to increasingly violent interrogations.

The torture methods used on him, which the author euphemistically describes as the "recipe," included forced standing with his back bent and hunched — thereby exacerbating the pain in his sciatic nerve — sleep deprivation, denial of clean water, sexual abuse, headbutting, extreme cold and threats, including threats to his family, of permanent imprisonment and of being "erased."

In addition to these cruel methods, the author was also exposed to interrogation by four different teams for up to 24 hours at a time!

It was only a matter of time before these tactics would break him.

### 8. As the author’s interrogation continued, he started admitting to any and all accusations. 

After months of serious torture, the author began confessing to all charges, even ones that hadn't been raised previously.

Why? It all began on August 25th, 2003, when a group of guards staged the author's abduction.

A group of hooded figures appeared, punched him numerous times, put a bag over his head, attached shackles to his wrists and ankles and transported him to a high-speed boat. The intention was to convince the author he was being taken someplace where even more violent interrogation awaited him.

In reality, his abductors took him around the island to a secret camp still within GTMO.

While there, the author was kept in isolation, punished for praying, sexually abused and threatened with being sent to Israel or Egypt for further questioning. The guards also deprived the author of sleep using the "water diet," a process in which they force fed him water so that the constant need to relieve himself prevented him from sleeping.

As a result of the various torture methods he was subjected to, the author suffered several physiological and psychological effects: his back was in pain, he was disoriented, he was losing weight and was having hallucinations, a symptom his interrogators took advantage of by whispering threats and accusations through the ducts in his cell.

In fact, the author was in such a poor state that a doctor treated him in order to get him "healthy" enough for his torture to resume.

The result?

The torture he was subjected to was so painful that the author began confessing in September 2003, admitting to things he hadn't done. His testimony was thousands of pages long and included false incriminations of other people as well, a fact which deeply saddened him.

The author even wrote an admission to a _new_ accusation, participation in an attempt to blow up the CN Tower in Toronto; but even confessing wouldn't be enough to secure his freedom.

### 9. Despite his confession and legal attempts to secure his release, the author remains in GTMO to this day. 

Following the author's confession and cooperation in taking a polygraph test, his conditions improved somewhat. Once he confessed and incriminated others, he was given hot meals and more regular showers.

But things really started to change when a "new era" of detention began in 2004. Over time, the author was allowed to pray in peace and was given books including the Bible, _Star Wars_, historical fiction titles and the classic novel _The Catcher in the Rye_.

Sometimes he would even watch movies with the guards and was given a laptop to type out his confession. However, despite the niceties he was afforded, the author still had no idea what was happening in the outside world.

As it turned out, the outside world was very interested in him: the International Committee of the Red Cross came to meet with him in September 2004 and, after that, journalists followed. But talking to the media was tricky, as the author's interrogators would sometimes pretend to be journalists in order to get more information.

While his conditions had improved, the improvements were nothing compared to the freedom he desired. So, he and his legal team have fought resolutely against his continued detention, with solid legal precedents to do so.

In 2008, in the case _Boumediene v. Bush,_ the US Supreme Court ruled that Guantánamo detainees have the right to challenge their detention via habeas corpus. Following the ruling, the author submitted a habeas corpus petition that was heard by Judge James Robertson in 2009. On March 22, 2010, the same judge ruled that the United States didn't have the right to keep the author in custody and ordered his release.

So why is he still in prison?

Four days later, the Obama administration appealed the court's ruling and, on November 5, 2010, the DC Circuit Court of Appeals sent the author's file back to US district court for rehearing. With the case still pending, the author remains behind bars.

### 10. Final summary 

The key message in this book:

**The prisoner MOS was detained, interrogated and tortured by the US government using violent and coercive tactics. His written experience and testimony is a plea to the American public to question the right of their government to operate in such brutal, inhumane ways.**

**Suggested** **further** **reading:** ** _The New Jim Crow_** **by Michelle Alexander**

_The New Jim Crow_ (2010) unveils an appalling system of discrimination in the United States that has led to the unprecedented mass incarceration of African-Americans. The so-called War on Drugs, under the jurisdiction of an ostensibly colorblind justice system, has only perpetuated the problem through unconscious racial bias in judgments and sentencing.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Mohamedou Ould Slahi, Edited by Larry Siems

Mohamedou Ould Slahi is a Mauritanian citizen and, since 2002, a prisoner in the American-run Guantánamo Bay detention camp in Cuba.

Larry Siems, the editor of Slahi's manuscript, is a writer, human rights activist and the author of several books, including _The Torture Report: What Documents Say About America's Post-9/11 Torture Program_.

