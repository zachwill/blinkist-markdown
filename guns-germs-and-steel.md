---
id: 51017264e4b0991857b92612
slug: guns-germs-and-steel-en
published_date: 2013-02-13T00:00:00.000+00:00
author: Jared Diamond
title: Guns, Germs, and Steel
subtitle: The Fates of Human Societies
main_color: None
text_color: None
---

# Guns, Germs, and Steel

_The Fates of Human Societies_

**Jared Diamond**

_Guns, Germs, and Steel_ explains how today's unequal distribution of wealth and power between people is based on factors of their respective environments rather than innate characteristics. It is an explicit refutation of any racist theory concerning current inequalities. The book uses a mixture of case studies, statistical analyses and deduction to support its arguments.

---
### 1. When conflicts occur, technologically advanced, centralized societies are the ones that win. 

Human history is made up of confrontations between groups of people, ranging from mild competition for resources to outright war. Where these groups have differing degrees of technological advancement and political centralization, the overwhelming trend is for the group with the more technologically advanced, centralized society to win.

The reason technology is so important to victory is obvious: one atom bomb can defeat an entire army. But even when the technological differences aren't so dramatic, history shows that the side with superior technology usually overcomes their opponent even when severely outnumbered. The Spanish Conquistadors that attacked the Inca empire were severely outnumbered hundreds to one, but they managed to slaughter their foe thanks to their steel weapons and armor, against which the Incas, with their stone weapons and cloth armor, could not compete.

Of course, the superior armaments of the Spaniards were just one manifestation of their overall technological superiority. They also possessed ocean-going ships, navigational technology and the ability to write home accurate, glowing reports of the New World to encourage its successful colonization. The Incas had none of these things. The level of technology in a society underlies its competitiveness economically as well as militarily.

The second point, centralization, is the foundation for any large undertaking a society makes. Whether maintaining an army, funding a fleet for exploration or simply enacting public works like irrigation systems or border defenses, the centralization of both resources and decision-making processes is key. Thus a higher degree of centralization makes for more competitive societies.

**When conflicts occur, technologically advanced, centralized societies are the ones that win.**

### 2. The root of all human technological and organizational complexity is the switch from foraging for food to producing it. 

For most of the species' history, humans have been hunter-gatherers. But around 8500 BC, a group of hunter-gatherers in the Middle East discovered the technique of actively sowing and caring for the plants they wanted to gather. Thus _agriculture_, the deliberate production of food, was born.

Agriculture made the land vastly more productive. On average, about 0.1% of the biological material in a wild environment is edible to humans. But by cultivating that environment to grow only plants useful as food, humans can consume up to 90% of the biomass produced.

This food production resulted in a food surplus, which in turn impacted human societies in two very important ways: it fueled population growth and allowed some members of the society to focus on things apart from the constant search for food. These effects are the direct progenitors of centralized organization and technological advancement.

First, enlarging populations put the simple organization of hunter-gatherer groups under pressure, forcing them towards the solution of political centralization.

Second, the members of society who no longer needed to constantly search for food began to build specialized knowledge, becoming political leaders, priests, military commanders, craftspeople, philosophers and inventors, and fuelled technological advancement.

Finally, the food surpluses created a feedback loop by allowing more children to be born, which in turn put even more pressure on the food production system to feed them as they matured. The response was to intensify food production, resulting in larger surpluses, further cycles of population growth, and, eventually, today's population of billions.

**The root of all human technological and organizational complexity is the switch from foraging for food to producing it.**

### 3. Increasing population density forces societies to centrally organize. 

Centrally organized societies have several advantages over non-centrally organized ones: they can better concentrate troops and resources in a conflict. They also have the ideological upper hand, as their soldiers die willingly 'for King and Country' rather than looking out for their own self-interest.

The sole critical factor driving this organization is population density, which is itself a function of the food surplus.

Within the small populations of traditional hunter-gatherer societies, all members know one another and most people are in fact related. Consequently, conflicts can be resolved through interpersonal bonds, decisions can be made via discussions involving all members of the society, and a resource given by one person to another can be directly repaid, even if at a later date.

Increased population density puts pressure on these traditional strategies for conflict resolution, decision-making and economic reciprocation.

When the threshold for 'everyone knowing everyone else' — approximately a few hundred people — is breached, none of these stratagems work very well anymore; for example, if two strangers come into conflict, they have no reason to refrain from killing each other and no mutual family or friends to intervene.

Most societies that faced the failure of their traditional strategies countered it with centralization. Conflict resolution and the ability to use force was taken from the people and reserved for the judiciary and the police, decision-making was constrained to the government and bureaucrats, and economic reciprocation was replaced first by the centralized storage and re-allotment of goods and then by the invention of money. The absolute dominance of centrally organized societies today demonstrates the success of this solution.

**Increasing population density forces societies to centrally organize.**

### 4. The process of technological advancement, once started by food production, builds exponentially on itself. 

The initial requirements for technological advancement are sufficient food and resources to allow a section of society to indulge their curiosity. These conditions are met through agricultural food production, but as time goes on the technology itself becomes an ever more important driver of technological advancement.

Some see technological progress as responses to specific needs. However, just as some companies first develop products and then persuade consumers that they need them, technological advances are mostly the result of human curiosity and general tinkering. Once the technology exists, it can be adopted, refined or recombined as a response to an external pressure, like the need to intensify agriculture to feed a population. Thus the more technology already exists in a society, the more resources exist to advance it further.

A society that develops food production early will therefore have an ever-increasing technological advantage over societies that adopted agriculture later, technological advantage builds exponentially on itself.

For the process of advancement to really rocket along, one final ingredient is necessary: connectedness with other societies. Not only do connected societies increase their pool of technology available by transferring innovations via trade, espionage and force, but they also foster the rapid adoption of new technology due to the need to be competitive with their neighbors. In contrast, isolated societies often abandon technological improvement, as the resource investment is not worth it in the absence of competition.

**The process of technological advancement, once started by food production, builds exponentially on itself.**

### 5. Lethal epidemic diseases, a result of animal domestication, potentiated the dominance of advanced societies. 

Epidemic diseases are history's greatest murderers; until World War II, more people died from the diseases spread by wars than from warfare itself. Epidemics of smallpox and typhus destroyed entire civilizations during the invasion of the New World, contributing significantly to its successful conquest. This last example typifies the pattern of disease exchange. The technologically-advanced society spreads its lethal germs to the more primitive one and no comparably fatal disease blights them in return.

The reason has to do with a significant element of food production: livestock. Humanity's lethal epidemic diseases — smallpox, typhus, and in the modern era, HIV and SARS — all started as diseases of animals. Once humans domesticated animals, animal diseases had the opportunity to jump the species boundary to humans. The diseases then spread through these agricultural human populations leaving only the resistant individuals alive, creating a resistant population over generations.

When these societies capitalized on their technological and organizational advantages to conquer the rest of the world, they brought their diseases with them. The societies opposing them, without domesticated animals, lacked immunity to the diseases the invaders carried with them, and had no maladies of their own to infect the invaders with in order to balance the playing field. Thus, they died in vast numbers from their invaders' diseases while their invaders continued to enjoy good health.

**Lethal epidemic diseases, a result of animal domestication, potentiated the dominance of advanced societies.**

### 6. The plants available determined how easily people were able to switch from foraging for food to producing food. 

Climate, plants and animals differ immensely around the world. The varying success of humans developing agriculture is directly traceable to these differences in natural environment.

Agriculture means deliberately selecting and cultivating certain plants to provide a reliable food supply. The plants selected depend on the range naturally available in the environment. Fertile environments in both tropical and temperate climates support large ranges of plant-life, although the two differ significantly from each other. Fruit trees grow well in the tropics, while smaller annual plants with edible seeds, leaves and fruit grow well in temperate areas. Of the twelve most important crops today, only one is a tree crop — bananas. In retrospect, temperate areas had, on average, a better range of plants for the purposes of agriculture.

The other important point about the range of plants available is the breadth of selection. Healthy humans need protein, carbohydrates, fat, vitamins and minerals in their diets. Most individual plant species don't contain all these nutrients, so agricultural systems need to be based on more than one crop. The better the biodiversity in the environment, the more likely it is that a broad enough range of plants to support human nutrition can be domesticated.

Large, biologically diverse areas of temperate climate were the best places for developing agriculture. But even within these areas, randomness dictated that some areas grew more nutritious plants than others, such as larger seeds, better protein content, and more complimentary combinations. It was these areas that fostered the earliest and quickest development of agriculture by humans.

**The plants available determined how easily people were able to switch from foraging for food to producing food.**

### 7. The presence of domesticated animals in food production systems resulted in critical advantages. 

Of the three nutrients required by humans — carbohydrates, fat and protein — protein is the one most likely to be lacking in plants. Domesticated animals supply this protein deficit, but their usefulness doesn't end there. By plowing fields and fertilizing the land, animals also increase the yields of plant crops. This further supports the growth of human populations and drives the organizational and technological benefits derived from population growth.

Animal uses were not limited to agriculture, either. The horse, for example, was not only the major form of transport for most of human history but also humanity's most effective engine of war, only being supplanted at the beginning of the 20th century.

Finally, livestock are the original source of human epidemic diseases, which in turn have influenced many major conflicts.

But as productive as domesticated animals are, not everyone could reap their benefits. Not only did the numbers of large animal species vary dramatically from continent to continent, but not all animals were equally domesticable.

To be domesticable, an animal must possess an entire set of necessary characteristics; for example, although zebras are very similar to horses, they were never domesticated. The problem was that unlike horses who are only occasionally bad-tempered and aggressive towards humans, zebras are almost universally so. Thus zebras could not be domesticated and the humans in Africa were left without a tame animal as versatile as the horse.

**The presence of domesticated animals in food production systems resulted in critical advantages.**

### 8. Food production and its consequences spread at different rates depending on geography. 

Only in a few areas around the globe was food production developed independently. This was because production required a set of wild plants from which a nutritious, reliable diet could be harvested. However, once invented, food production spread around the world, albeit at very different rates depending on the similarity between the original environment and the new one in which food production was to be adopted.

Plants adapt to the environment they exist in, becoming dependent on the temperatures and amount of rainfall in that environment. Consequently, they fare poorly when transplanted to very dissimilar environments. Alpine crops from the Andes, for example, could not grow in the surrounding tropical lowlands, and thus never took the next geographical step to the Mexican highlands where they would have grown well.

Plants also rely on seasonal changes in the length of the day to guide their life cycles. Day length remains the same as long as you stay at the same latitude but changes once you move north or south, closer to or further from the equator. Food production therefore spreads more easily east to west, meaning that on continents with a dominant north-south axis (such as the Americas), the spread of crops was slower than on continents with a dominant east-west axis (such as Asia).

Finally, the spread of technology mirrors the spread of food production. This was probably because the earliest technologies were all used in connection with food production: wheels with horses and writing with agricultural records. Not only that but agricultural trade established routes that technological advancements could then be passed along as well.

**Food production and its consequences spread at different rates depending on geography.**

### 9. Both extremely isolated and extremely connected areas tend to lose, rather than gain, advantages over time. 

Isolation causes the loss of useful advantages that aren't necessary for day-to-day survival. Consider the people of Tasmania, who despite reaching the island via ocean-going boats had lost this capability by the time the island was re-discovered by European explorers. The absence of competition in this context had made maintaining the technology not worthwhile.

At the other end of the spectrum is China, where the absence of competition came from being excessively connected. One of the first places to independently develop food production, China also unified itself very early on and has never been protractedly fragmented since. Underlying this persistent unity is the geographical closeness of China's agricultural areas and its traversability via two extensive river systems — the Yellow and the Yangtze. Unification left China with no significant competitors in its part of the world and therefore no consistent driving pressure for technological advancement. Although China possessed ocean-going fleets before the Europeans, they never colonized foreign lands. A single decision disassembled those fleets, and without local competitors there was no need to revisit that decision.

Contrast this with Europe, which has been responsible for most major colonization efforts around the world. Divided geographically into multiple peninsulas, crisscrossed by mountain ranges and without any river systems long enough to link all areas, Europe developed a horde of independent nations.

These geographical divisions, while being sufficient to foster separate national identities, did not prevent trade and competition. The consequence of this was that no abandonment of technology went unpunished: competitors raced ahead and forced the abandoning society to re-evaluate its choices. 

**Both extremely isolated and extremely connected areas tend to lose, rather than gain, advantages over time.**

### 10. The dominance of certain societies today is a consequence of their ancestors’ environments. 

The world today is a very unfair place. People from certain countries and backgrounds — overwhelmingly European — possess a blatantly disproportionate amount of the world's wealth and resources. This is a direct consequence of their ancestors' geographical location.

Eurasian societies dominate the world because their continent provided the best environment for the development of food production. As the largest landmass, it has the largest diversity of plants and animals as well as the largest temperate climate zone where the best crop plants grow. Of its diverse range of animals, a higher proportion was domesticable than on any other continent.

Thus food production started earlier in Eurasia than anywhere else in the world. It then spread rapidly along the continent's ideal east-west axis, unhindered by significant natural barriers. Both food production and the subsequent technological progress it spurs are autocatalytic processes, meaning they accelerate at an increasingly fast rate. Hence the Eurasian's original advantage was magnified over time, until it culminated in their conquests of the last 500 years.

In contrast to Eurasia, the Americas had multiple disadvantages. The dominant north-south axis significantly slowed the spread of crops and technology to the point where one society invented writing and another the wheel, but the two never exchanged these ideas. Mass extinctions at the end of the last ice age also left the Americas with few species of large domesticable mammals.

In Africa, the Sahara desert blocked the dispersal of crops, animals and technology to Sub-Saharan Africa, and sadly few of the continent's magnificent animals proved to be domesticable.

Hence, the environment of these continents did not provide natives the tools to resist European imperialism.

**The dominance of certain societies today is a consequence of their ancestors' environments.**

### 11. Exploring the real cause behind modern inequalities refutes racist explanations based on differences between humans. 

The modern global distribution of wealth and power between peoples is unquestionably unequal. European societies and their (former) colonies possess disproportionate amounts of the world's resources and hold both political and military sway. Without knowledge of the underlying cause of this, people may be tempted to attribute the phenomenon to characteristics of European people — that is, explain it in terms of biological differences in e.g. intelligence or developmental capabilities. Such explanations are inherently racist and have absolutely no evidence to support them.

In fact, average intelligence is likely to be higher among people living in hunter-gatherer societies because the environment requires intelligence and problem-solving skills for survival. Conversely, once humans gather in large cities, disease becomes the leading source of evolutionary pressure, and hence disease resistance, not intelligence, is the main trait driving survival.

Despite the lack of evidence for the racist explanation, people may be tempted to believe it for lack of alternatives. It is therefore necessary to examine the true causes behind inequality today and absolutely refute any suggestion they be based on biology. Moreover, by exploring the underlying causes, we may be able to find novel solutions to address current inequalities.

**Exploring the real cause behind modern inequalities refutes racist explanations based on differences between humans.**

### 12. History should be considered a scientific discipline alongside biology and ecology. 

The scientific method is characterized by the formulation of a hypothesis — a prediction about how an object, entity or environment will behave — and then an experiment to test that hypothesis. To be reliable, these experiments should be both _controlled_ and _repeatable_.

Say you wish to find out if butterflies are more attracted to red flowers or blue flowers. For the experiment to be controlled the flowers must be the same shape and size and in the same environment to prove that the color is really what makes the difference. To be repeatable, the same result should be found each time the experiment is carried out.

However, there are many branches of biology that cannot conform to these standards; for instance, ecology and evolution. They are both enormously complex disciplines: they cannot be controlled and they operate over such impractically large frames of reference (time for evolution and space for ecology) that they cannot be repeated. Although some smaller-scale experiments can be done, the majority of investigation into these subjects is through observation and comparison in nature. Sometimes nature provides useful experiments, like the way the Galapagos Islands provided isolated "miniature laboratories" for Darwin. Nevertheless, no natural experiment is completely controlled and therefore all results are somewhat unreliable.

Despite the inherent difficulties, we consider both ecology and evolution scientific disciplines, and we should extend that consideration to history as well. Working over long timeframes and large areas allows us to average out all the small events and see things in terms of statistical trends. This provides scientific perspectives on history, and allows us to examine causes and effects in ways that can be useful even when addressing current problems.

**History should be considered a scientific discipline alongside biology and ecology.**

### 13. Final summary 

The key message in this book:

**The unequal distribution of resources and power between societies today is due to historical differences in peoples' environments, not biological differences between peoples.**

The questions this book answered:

**What are the reasons behind the unequal distributions of wealth and power among societies today?**

  * When conflicts occur, technologically advanced, centralized societies are the ones that win.

  * The root of all human technological and organizational complexity is the switch from foraging for food to producing it.

  * Increasing population density forces societies to centrally organize.

  * The process of technological advancement, once started by food production, builds exponentially on itself.

  * Lethal epidemic diseases, a result of animal domestication, potentiated the dominance of advanced societies.

**How did different environmental factors cause these differences?**

  * The plants available determined how easily people were able to switch from foraging for food to producing food.

  * The presence of domesticated animals in food production systems resulted in critical advantages.

  * Food production and its consequences spread at different rates depending on geography.

  * Both extremely isolated and extremely connected areas tend to lose, rather than gain, advantages over time.

  * The dominance of certain societies today is a consequence of their ancestors' environment.

**Why is it necessary to explore these causes?**

  * Exploring the real cause behind modern inequalities refutes racist explanations based on differences between humans.

  * History should be considered a scientific discipline alongside biology and ecology.
---

### Jared Diamond

Jared Diamond is a respected American scientist and a Pulitzer prize-winning author of several popular science books. Originally trained in physiology, Diamond is noted for bringing together multiple scientific fields in his works. He currently occupies the position of Professor of Geography at UCLA.

