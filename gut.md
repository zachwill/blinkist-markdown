---
id: 56afadd9377c1600070000a5
slug: gut-en
published_date: 2016-02-05T00:00:00.000+00:00
author: Giulia Enders
title: Gut
subtitle: The Inside Story of Our Body's Most Underrated Organ
main_color: 27BCC3
text_color: 177175
---

# Gut

_The Inside Story of Our Body's Most Underrated Organ_

**Giulia Enders**

_Gut_ (2015) takes an entertaining yet scientific look at an organ that is just as interesting and important as the brain — the gut. By tracking a piece of cake as it works its way through the digestive system, you'll come to appreciate the gut for the sophisticated and impressive ecosystem that it is.

---
### 1. What’s in for me? Have some guts! 

How would you react if someone at a dinner party started talking about their gut and their latest bowel movement? You'd probably be pretty disgusted. After all, the digestive system isn't a topic for polite conversation. 

But maybe it should be! As it turns out, our gut is much more fascinating than it is gross. In fact, it's one of the most complex and most astonishing organs we have. 

If you're one of those people who get disgusted whenever someone mentions anything associated with digestion, prepare to rethink your assumptions — because these blinks will help you learn to stomach your own stomach! We'll follow a piece of cake on its journey through the body, and learn things about the gut that you never thought possible.

In these blinks, you'll also learn

  * what lactose intolerance really is;

  * how depression can be healed in mice; and

  * how microbes influence our consciousness.

### 2. The gut is a unique and impressive organ – nothing to be ashamed of! 

We usually don't talk about the inner workings of our digestive system. Indeed, many find the topic downright gross. However, the complex functions of the gut are more fascinating than disgusting and, as far as organs go, the gut is extremely underrated. Furthermore, all of us would benefit from knowing a bit more about what happens when we eat. 

Most people know nothing about exactly what goes on in our gut, which, in more scientific language, is called the gastrointestinal tract. Yes, we have all flushed away the unpleasant final product that ends up in the toilet, but few of us are familiar with the important work that led to that product.

Of course, there is one part of digestion that we do focus on. That would be the initial part, when we chew up and savor the taste of our food. But we're only concerned with the rest of the process if there's a problem like indigestion. 

That's because, once we swallow, the food enters an area of smooth muscle tissue that is outside of our consciousness.

In fact, our digestive system has a nervous system of its very own, allowing our digestive system to independently perform all of its functions. The work it does takes place without the involvement of our conscious mind, which makes the gut a very unique human organ.

Another incredible fact about our gut is the mind-boggling amount of bacteria living inside of it. Our entire digestive system contains up to 100 billion bacteria. This accounts for 99 percent of all microorganisms in our entire body! Of course, these bacteria exit the body, too: there are more bacteria in one gram of excrement than there are people on the planet.

But this is nothing to be ashamed of. It's part of the important and incredible work the gut does every day. We take it for granted — but what our gut does for our mind and body is something we should be very grateful for.

### 3. Food’s journey through our bodies begins with our outer senses. 

Let's admire the work our gut does by tracking a piece of cake as it journeys through the digestive system. This journey actually begins before your first bite. In this case, it begins while you're standing outside the bakery, and you first lay eyes on that cake.

As you stare at the cake through the shop window, you begin to imagine the sweet aroma and taste, and your mouth begins to water. Unable to resist, you decide to buy it.

This impulse to buy the cake doesn't come from nowhere; it's an instinctual reaction to the sight of delicious food.

Sight is closely linked to the process of eating. When we see something we want to eat, our salivary glands get going and kick-start the production of gastric acid in our gut, preparing it for the digestive process. Our brain, which is perhaps telling us to lose some weight and stay away from the cake, is helpless against the power of the gut.

Once you enter the shop, the sense of smell gets involved, drawing you closer to the tasty treat. Tiny scent particles released by the cake travel through the air and into your nostrils. The particles reach your mucus membrane, where they dissolve and then travel to your brain, which increases your desire all the more.

Then you finally have the cake in your hands. And as you begin the process of eating, your mouth and sense of taste both come into play. 

As you chew, your tongue and jaw go to work. After chewing, your tongue positions the food against the palatal area of the mouth, in preparation for swallowing. Your soft palate and pharynx then close off the sinus passages and the cake enters your esophagus, or gullet.

When this happens, the cake enters the smooth muscular tissue zone. And it's here that your food enters the realm of the unconscious.

> Your jaw is your strongest muscle. It can create a pressure of approximately 175 pounds.

### 4. From the esophagus, our food enters the gut and the small intestine. 

Imagine thousands of fans at a sports stadium, all doing the wave. Your esophagus moves in a similar way, with an undulatory motion that eases food from one end to the other. This work is performed automatically and autonomously: once that piece of cake is one-third of the way down your esophagus, you have no control over the muscles that push the food along its journey.

Even if you were to do a handstand, the esophagus would continue moving the food toward the stomach. This is work that your esophagus has mastered: it's been doing it since you were a baby in your mother's womb, swallowing half a liter of amniotic fluid every day.  

  

From the esophagus, the piece of cake descends into the stomach. There, the food is processed for about two hours, until it is fully broken up by gastric fluid. At this point, the cake has been broken into pieces approximately 0.2 millimeters in size.

All this information probably makes it obvious why digestion is an unconscious activity. Who would want to spend two hours consciously breaking down a piece of cake?

As new food continues to be swallowed during a meal, your stomach expands to accommodate. It's so flexible that it's nearly impossible to eat more than it can handle.

Interestingly, emotions can have the opposite effect on the stomach. Stress and anxiety can cause your stomach to contract and you'll find yourself losing your appetite. But these emotions can also cause problems in the stomach, with the gastric fluids eating away at your stomach lining and creating ulcers.

When everything goes smoothly, though, the tiny pieces of cake get moved from the stomach to the small intestine. This connection is made by a small area called the pylorus, which helps push the food along.  

  

When the food hits the small intestine, the really important digestion starts. This is when your body begins to extract important nutrients from the food.

### 5. The real digestion takes place in the small intestine. 

The small intestine is in constant movement. Inside, the walls are made up of _intestinal villi_, tiny finger-like protrusions that move and manipulate the food as it continues its journey. Each millimeter of the small intestine contains about 30 villi — and they only know one direction: forward!

Tiny electric shocks cause the intestinal muscles to contract in a rhythmical motion, pushing the food along. During this process, digestive fluid, which has extracted the nutrients of the food, is absorbed into the body. One piece of cake would last about an hour in the small intestine before entering the large intestine.   

  

The small intestine likes to keep things tidy. After performing its duty, it goes about cleaning itself up. As it tidies up, it growls. 

Contrary to popular belief, when you hear your gut growling, it isn't telling you it's hungry; it's actually your small intestine cleaning up. And when you respond to this sound by eating, you might actually be interfering with this process!

But before our piece of cake hits the large intestine, it enters an area called the _ileocecal valve_. Unlike the work done in the small intestine, which requires a lot of energy, the process here is rather calm in comparison. The ileocecal junction allows the body to absorb any remaining fluids, including B12 vitamins and gastric acid.

Like the stomach, this area can also be affected by stress and anxiety. This can result in the unpleasant experience of diarrhea. Our digestive system deals with around ten liters of fluid each day — from beverages and saliva to gastric fluids and _chyle_, a substance created as our body breaks down fatty foods. It's no wonder that interruptions in the process can result in some fluids sneaking through.  

  

At a minimum, the entire digestive process takes ten hours. But it's possible for digestion to take up to 100 hours, from the first bite until the end of the process.

### 6. The origin of allergies and lactose intolerance are assumed to reside in the gut. 

When you think of allergies, what's the first part of the body you think of? You probably imagine red, itchy eyes, skin rashes or a runny nose. The gut probably isn't the first place that comes to mind. Well, this blink might change that.   

  

There's an interesting theory about the role your gut plays in the allergies you experience.

It begins with how proteins are broken up during the digestive process. Sometimes this doesn't go as smoothly as it should. Hazelnuts, for example, can leave behind small pieces of protein that don't get absorbed into the bloodstream when the small intestine is doing its work.

If this happens, the pieces can end up getting wrapped in fat droplets, which are then absorbed into the lymphatic system through lymph vessels in the small intestine. This brings these pieces into contact with our immune cells. And when these cells discover these remaining proteins, the cells sometimes respond as if it's a dangerous nucleus, triggering an allergic reaction to fight it off.

Even worse, if this happens again, our immune system is positioned to expect the "attack" and respond with an even more powerful allergic reaction.

Lactose intolerance falls into a similar category.

It begins at the entrance of the small intestine, at an opening called the _papilla_. This is where gastric fluid — produced by the liver and pancreas and containing important enzymes — is introduced to further break down the food. 

However, the gastric fluid of the papilla doesn't contain the enzymes needed for breaking down lactose. These enzymes are produced by the cells further down the small intestine. But sometimes there aren't enough of these enzymes around, and lactose enters the large intestine and becomes nutrients for gas-producing bacteria.

Anyone with lactose intolerance knows what happens next: flatulence, gas pains and diarrhea. Unfortunately, as we get older, 75 percent of the population will experience a genetic change that will shut off the production of the enzyme responsible for breaking down lactose.

### 7. Surprise, surprise: the gut can influence our brain! 

Have you ever been accused of thinking with your stomach? Well, as it turns out, this saying isn't as far-fetched as it sounds.

As we've learned, our gut has its own nervous system that allows it to work on its own, instinctually. This is called the enteric nervous system, or intrinsic nervous system, and it's made up of around 500 million neurons. And compared to the rest of the organs in our body, the variety of neurons that make up this system is only surpassed by the brain itself. 

Neuroscience has told us a lot about how the brain works and how it is responsible for the emotions we feel. But as we follow the paths of communication between the brain and gut through the central nervous system, the question arises: Does our gut also influence our emotions?

Science has been looking into that very question. And experiments conducted on mice indicate that the probable answer is yes.

The mice that were being observed fell into two categories: active, happy mice and depressed, inactive mice. The experiment showed that depressed mice that were given bacteria to support digestion quickly became active, showed fewer signs of stress and performed better in learning and memory tests.

Furthermore, mice that had their vagus nerve severed, the nerve mainly responsible for communication between the gut and brain, showed no improvements when given the treatment.

It all supports the theory that a healthy gut leads to a healthy mind.

While our brain is designed to receive our outside senses of sight, scent, touch and hearing, our gut sits in the middle of our body and is perfectly placed to be our internal sensory organ. Considering the amount of work going on inside our bodies, it's not such a bad idea to let your stomach do some thinking.

### 8. The gut contains a rich and vital world of microbes. 

As well as having its own nervous system, the gut also accounts for 80 percent of our immune system. Considering that most of the dangerous bacteria and pathogenic germs enter our body through our mouth, however, this fact might not come as a surprise. 

But not all of the bacteria our bodies are exposed to is dangerous. In fact, our health is dependent upon the work of microorganisms. 

While in the womb, we are in a sterile environment and 100 percent of our cells are human. But as soon as our protective amniotic sac is broken, a whole world of microbes joins the party. In fact, after our birth, microbes come to constitute 90 percent of the cells in our body!

It may sound scary to a germaphobe, but our bodies are rich ecosystems carrying around millions of microorganisms. Without them, we wouldn't be able to survive.

Cultivating good microbes in our gut happens during the first three years after birth. Mother's milk is a fine source of healthy microbes such as bifidobacteria, which helps prevent us from becoming overweight.  

  

Mother's milk also contains the kinds of bacteria that help us digest and break down our food. The food we're best equipped to break down depends a lot on the diet our mother has. For example, mothers in Africa will provide their children with the kind of bacteria that can help digest a diet that is based on plants and fibrous food.  

  

We're still learning about all these microbes. In 2011, scientists discovered enterotypes — bacterial families that bundle and act together. They found three different types of enterotypes, one of which will predominate in a person's gut.

Since this discovery, studies have looked into the role different diets play in determining which of these three enterotypes will appear in the bacterial ecosystem of someone's gut. It's possible that this research will provide a link to Traditional Chinese Medicine, which places people into one of three distinct groups, according to their long-term diet.

### 9. It seems that microbes in the gut can influence our consciousness. 

Yes, it would seem that even after three million years of evolution, we are still learning about the importance of the many microorganisms in our gut. There are as many as 100 trillion microorganisms in our digestive tract, a community called the _gut flora_. And they may have an intimate relationship with our brain. 

Consider this question: Is it possible that microbes in our gut tell our brain what kind of food we're craving?

It might sound a little out there. How exactly do microbes in the gut send messages all the way to the brain — a place that is protected from all but the tiniest of particles?

The answer? Amino acids. Bacteria produce amino acids like tyrosine and tryptophan, which can pass through the brain's protective layers. Once inside, these substances get transformed into biochemicals like dopamine and serotonin, which make us happy and drowsy. You can think of it as a reward system for giving your body certain nutrients.

Just how deep this connection goes is something science is still figuring out. For instance, there appears to be evidence that abstaining from certain foods can lead our brain to cease longing for those foods. And this may be because our gut no longer hosts the microbes that are attracted to such foods.  

  

Another example of microorganisms influencing behavior is the curious case of _Toxoplasma gondii_. This microbe is most common in cats but can find its way into humans and rats. Rats are usually scared off by cat urine; when infected by _Toxoplasma gondii_, however, they're attracted to it. The parasite literally changes the behavior of its host, in this case to fatal effect.  

  

The microbe has a similarly deadly influence when it finds a human host. _Toxoplasma gondii_ has been shown to cause humans to act in unnaturally dangerous ways. Research is still ongoing, but a study in the Czech Republic showed that infected people were more prone to auto accidents.

### 10. Instead of feeling disgusted by microbes, we should learn to integrate them into our lives 

The example of _Toxoplasma gondii_ shows that some microorganisms taking up residence in our gut are bad news. But we certainly shouldn't write off all microbes — after all, we spend our entire lives with them, no matter how much disinfectant we use!

Our attitude toward microbes has evolved over time; at the turn of the twentieth century, for instance, there were two opposing views.

On one side was Nobel Prize-winning Russian immunologist Ilya Mechnikov, whose work proved that certain microbes, especially lactic acid, could be beneficial. As part of his research, he observed Bulgarian farmers, who lived long and healthy lives and were especially fond of their yogurt, a food high in lactic acid bacteria.

Unfortunately, on the other side were the people who rallied behind the discovery of penicillin and the revolutionary benefits of antibiotics. The less bacteria, the better, they argued, and since the 1940s it's proved difficult to argue otherwise.

But the advantages of bacteria became clear to those who tried to create baby formulas that replicated the benefits of mother's milk. Scientists were able to replicate the milk perfectly, and yet, when babies drank the stuff, they always ended up suffering from diarrhea. What was missing? The bacteria that are found on a nursing mother's nipple.  

  

In more recent years we've embraced the benefits of bacteria and can easily find probiotic products in supermarkets. We now know that probiotic bacteria can produce fatty acids that protect your stomach and help the immune system.

These benefits have been found to extend to what we call _prebiotics_ as well. These are fibrous foods that manage to go undigested through the small intestine and produce healthy bacteria in the large intestine. It is recommended to eat 30 grams of prebiotics a day, though people on average often consume only half that much.

### 11. All’s well that ends well: Defecation is a complex interplay between the conscious and unconscious. 

Well, we've reached the last stop on our journey through the gut: the large intestine, also known as the colon. 

As your food enters the large intestine, the digestion process has been finished. Any remaining water is reabsorbed and the feces is prepared for defecation. At the end of this process, the food waste reaches the rectum, which is where our sphincter muscles reside.

As a child, you learn to avoid messy accidents by controlling your sphincter. But what you may not know is that there is a second, internal sphincter muscle that we don't control.

Like most of our digestive system, the inner sphincter operates automatically. When the remains of your food arrive, it lets a small portion of the waste descend from the colon to the rectum; this sets off nervous-system sensors that in turn tell our brain exactly what is going on. This information includes whether it is gaseous or solid waste and whether we need to immediately head to the toilet or not.

Your brain evaluates this information and you then consciously control what happens from this point on. You select the appropriate time to open the outer sphincter and use the toilet or to inconspicuously release some gas.

If it's the toilet you need to use, this is when your conscious and unconscious gut start to work together. The inner and outer sphincter need to work in harmony for this final defecation to take place. If you postpone going to the toilet too often, you disturb the inner sphincter muscle, which can result in constipation.  

  

So, it's a long and very interesting journey our food goes on. But it is only at the very beginning and the very end of this journey that we interact with our food on the conscious level.   

  

And the final, conscious interaction? Don't forget to flush!

### 12. Final summary 

The key message in this book:  

  

 **Our gut is an incredibly fascinating organ, comparable to the brain in terms of complexity and importance. Our large intestine harbors a rich world of microorganisms that serve our well-being.** **  

** **When we make conscious food choices, we can influence these microorganisms.** **  

** **  

**Actionable advice:  

  

 **Do something good for your gut flora.**  

  

Eat some prebiotic foods such as artichokes, asparagus, green banana, garlic, onions, parsnips, whole wheat, rye, oats or leeks.  

  

 **Help your bacteria.**  

  

You feel much better when you help your bacteria process the food you eat every day. Therefore, better grab the whole-grain bread instead of that baguette. 

**Suggested further reading:** ** _Grain Brain_** **by David Perlmutter**

_Grain Brain_ (2013) outlines how what we eat can cause or mitigate serious brain disorders like anxiety, ADHD and depression. Eating well is crucial to your brain's functioning well, and these blinks tell you why.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Giulia Enders

Giulia Enders holds a PhD from the Institute for Microbiology and Hygiene of Hospitals in Frankfurt am Main, Germany. In 2012, her theories on the human gut won a science slam in Karlsruhe and Berlin.

