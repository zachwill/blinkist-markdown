---
id: 548f06b16532350009220100
slug: hack-attack-en
published_date: 2015-01-02T00:00:00.000+00:00
author: Nick Davies
title: Hack Attack
subtitle: The Inside Story of How the Truth Caught Up with Rupert Murdoch
main_color: C3332D
text_color: C3332D
---

# Hack Attack

_The Inside Story of How the Truth Caught Up with Rupert Murdoch_

**Nick Davies**

_Hack Attack_ details the riveting story of the phone hacking scandal that rocked the British media in 2011. Focusing on the rise and fall of Rupert Murdoch's _News of the World_, the books offers an inside look into the seedy world of tabloid journalism.

---
### 1. What’s in it for me? Learn how a phone hacking scandal brought Rupert Murdoch and his News of the World to their knees. 

It's one of the biggest scandals of our generation, and it led to an actual pie in the face for media mogul Rupert Murdoch. But the hacking exposé at the British _News of the World_ tabloid was about more than just scandal. It was about investigative journalism, the way money can trump decency, and a story that gripped the entire world.

_Hack Attack_ is the whole story, as chronicled by one of its major players. After all, it was the investigative work of journalist Nick Davies that kept the story alive and eventually led to _News of the World_ shutting down.

These blinks tell the whole story, from a seemingly innocuous bit of tabloid press to a major cover up involving politicians, police, media, and one (formerly) untouchable billionaire.

After reading these blinks, you'll know

  * why Murdoch ended up with a pie in the face;

  * about the shady role the police had to play in the scandal; and

  * why spying and hacking drew the ire of all of Britain.

### 2. One easy way to get information is to just steal it, for example, through phone hacking. 

As with many other historic moments in British history, the phone-hacking scandal involved the Royal Family. This story, however, isn't about deposing monarchs or warring kingdoms.

It centers around _phone hacking_, the illegal act of accessing someone else's voicemail messages without their knowledge or consent.

For example, phone hackers can simply exploit a loophole in telecommunications safety. Voicemail inboxes can be accessed remotely, and from any phone — all you need is the PIN.

And people usually don't change their PIN from the factory settings, which could be a simple code such as 0000. If you guess the PIN, you can access your target's voicemails from anywhere.

The most nefarious means of accessing voicemails is by _blagging_, i.e., obtaining other people's personal information through lying, persuasion and cunning.

Blaggers call phone companies and impersonate the owner of the phone, using their silver tongues to either acquire someone's PIN or have it reverted to the easily guessed factory setting.

Among the original and most talented phone hackers was Glenn Mulcaire. While others may have had to learn the skill, Mulcaire was practically born to be a blagger. Almost instinctively, he would use his quick thinking and guile to effortlessly extract private information from his unwitting victims.

Mulcaire began his work as a private investigator but soon caught the eye of the editors at tabloid newspapers. One of them, _News of the World_ (NoW), took a particular interest in his skills.

In fact, they were so interested that, by 2001, they were paying Mulcaire £94,000 per year as a retainer!

But it was 2005 when Mulcaire got his game-changing assignment: to hack the phones of the British Royal Family.

> "Mulcaire was the man. He was born to bullshit."

### 3. Scandal plagued NoW after two of its reporters were caught. 

Mulcaire's skills were critical in scoring a hit on what could only be considered the most lofty of targets, the British Royal Household.

He was commissioned by Clive Goodman, who worked under the newspaper's editor in chief, Andy Coulson, and was in charge of stories about the Royals.

On 25 October 2005, Goodman, with the consent of Coulson, paid Mulcaire a commission of £500 per week to intercept voicemail communications from the Royal Household.

Mulcaire's work generated stories that led to lucrative sales for the newspaper, but over time, the desire for ever juicier stories eventually landed _NoW_ in hot water.

One story involved Prince William's drunken behavior following his brother Prince Harry's graduation ceremony from military school.

While the story was a front-page hit, it also set off alarm bells for the military because it very accurately reflected a complaint made by General Andrew Ritchie to Prince Harry's secretary about his brother's sloppy behavior. A complaint which had been left on voicemail.

Mulcaire had worked for various other reporters at _NoW_, and both Coulson and his assistant, Greg Miskiw, were fully aware of his activities.

However, when Mulcaire and Goodman were arrested on 8 August 2006 on suspicion of intercepting voicemail messages, _NoW_ essentially offered them up as scapegoats, claiming that this was a case of a rogue editor and reporter working on their own.

The police launched an investigation called _Operation Caryatid_ into the hacking. In a raid on Mulcaire's house, police uncovered files that suggested that the hacking was more widespread than they had initially thought.

However, their investigation concluded that only Goodman and Mulcaire were implicated in hacking, leaving the rest of _NoW_ in the clear.

But, with evidence contradicting _NoW_ 's position right in front of them, why didn't the police investigate further? As you'll see in our following blinks, the answer lies in the influence of one wealthy man: Rupert Murdoch.

> _"It only happened once." — Stuart Kuttner, managing editor of NoW 1987–2009._

### 4. The man behind the curtain has a name: Rupert Murdoch. 

Australian businessmen Rupert Murdoch has been skulking behind the scenes for decades.

He is the owner of various tabloids. Throughout his life, he has built a huge media organization, News Corporation (also known as News Corp), which owns more than 800 subsidiaries and has total assets worth around $60 billion.

He, in conjunction with his family trust, owns 12 percent of News Corp shares, yet also controls 39.7 percent of the votes.

At one point in time, Murdoch owned the _Sun,_ the _Times,_ the _News of the World_ and the _Sunday Times_, which together served 37 percent of British newspaper readers.

In addition, Murdoch supplies 60 percent of Australia's daily papers and an astounding 70 percent of Sunday papers!

News Corp also owns the global publisher HarperCollins, as well as various American newspapers, such as the _Wall Street Journal._

Murdoch has a strong hold over the television industry in Britain and abroad.

For example, News Corporation owns 39.1 percent of the British satellite television service British Sky Broadcasting (BSkyB), which provides content to ten million homes, and competes in an oligopoly composed of only two other television services, BBC and ITN.

News Corporation even made a move to buy the remaining shares of BSkyB, but this plan fell through in July 2011 after the full extent of the phone hacking debacle became public knowledge.

Murdoch isn't just a dominating presence in British television; he also owns the American film studio Twentieth Century Fox, which is responsible for the notoriously right-wing Fox News Network, often acting as a mouthpiece for Murdoch's own political convictions.

Clearly, Rupert Murdoch's reach and influence through his media outlets is considerable. It wouldn't be far-fetched to call him an "international media mogul."

### 5. Murdoch has cozy relationships with some politicians. 

As you might expect, being at the center of an international media conglomerate comes with certain perks, one of which is political influence. The relationship between Murdoch and politicians is often smooth when they share the same political ideology.

This was the case with Margaret Thatcher, the British prime minister from 1979 to 1990. Their relationship was essentially effortless, as they were both strong adherents of _Neoliberalist_ political philosophy, which calls for deregulated markets, privatization of public services and limited government interference.

Their shared ideology led Murdoch's _News of the World_ and _Sun_ to strongly back Thatcher's prime ministerial campaign, and she had no qualms about returning the favor.

Indeed, Thatcher repeatedly helped Murdoch circumvent government regulations designed to foster competition.

For example, Murdoch secretly met with Thatcher when he acquired the _Times_ and the _Sunday Times_, which Thatcher's government declared to be bankrupt, and thus exempt from laws that would prevent a monopoly takeover.

However, as government records revealed some 33 years later, the bankruptcy was a lie.

Things were only slightly different with Labour Prime Minister Tony Blair.

Blair had a much more turbulent relationship with Murdoch than did Thatcher. He even challenged Murdoch directly by blocking his bid to purchase the Manchester United soccer team, and he went further, by establishing the television regulatory and competition authority Ofcom.

Despite his flagrant snubbing of Murdoch's interests, some speculate that Blair's decision to invade Iraq was in fact guided by a fear of the media mogul.

Murdoch wanted Great Britain to invade Iraq, and had considerable influence on public opinion.

Blair feared that by opting not to invade, Murdoch would galvanize opposition against Blair by publishing critical stories about him and his government.

2003, the year of the invasion of Iraq, was already a troubling time for Blair due to other political issues, such as a proposed referendum to join the EU in which Blair had to avoid the ire of Murdoch's anti-EU press.

Now that we've revealed the man behind the curtain, our final blinks will deal with the fate of _NoW_.

> _****__"No, I don't think so . . . We tried." — Rupert Murdoch on whether his newspapers shaped the agenda of Britain's invasion of Iraq_

### 6. The Guardian challenged NoW’s claim that the phone hacking was only an isolated incident. 

We know today that _NoW_ 's initial statements about phone hacking were a lie. But how exactly did this lie become exposed?

On 8 July 2009, the _Guardian_ published a story claiming that the phone-hacking at _NoW_ was in fact _not_ an isolated incident. The article provided numerous pieces of evidence that debunked the notion that phone hacking had only happened once and that the _NoW_ hadn't continued to hack phones.

Firstly, the Royal Family wasn't the only victim of phone hacking. Gordon Taylor, CEO of the Professional Footballers' Association, had also been hacked, and was subsequently paid a £1 million settlement not to take it to court.

Secondly, the Metropolitan Police had been sitting on evidence gathered during Operation Caryatid, which implicated others at _NoW_ in the phone hacking.

The _Guardian_ 's report set in motion a two-year series of stories and investigations into _NoW_ 's phone-hacking operations.

And yet, even after the _Guardian_ 's report, the police still didn't feel it was necessary to reopen the case and investigate further.

Originally, the police stated that only a handful of people had been victims of phone hacking, and all potential victims had been alerted. It later came to light, however, that the police had found Mulcaire in possession of 91 PINs, with 600 potential targets — far more than the police had reported.

The move to obfuscate the scale of phone hacking was headed by the assistant commissioner of the Metropolitan Police, John Yates.

Exactly why Yates elected to conceal the scope of the hacking from the public remains a mystery. What isn't a mystery is that he had an inappropriately friendly relationship with Rebekah Brooks, the CEO of News International, a subsidiary of News Corps, and the _NoW_ 's publisher.

While holes were starting to appear in News Corp's official line, the biggest revelation was yet to come.

> _"They weren't talking about a rogue reporter. What they were describing was a rogue newspaper."_

### 7. News Corp eventually did something that would enrage even the most indifferent of people. 

Hacking the phones of the Royal Family didn't generate much public scrutiny. Hacking the phone of a kidnapped girl, however, elicited unprecedented rage.

Milly Dowler was 13 years old when she disappeared while walking home from school in Walton-on-Thames in 2002. Six months later, her body was uncovered in local woodlands.

But what does any of this have to do with _NoW_?

In 2011, the author received a shocking anonymous tip that the _NoW_ had been hacking Dowler's phone _after her abduction_.

The author followed this up by examining a _NoW_ article that was published after Dowler's disappearance. It claimed that a mentally ill women had tricked Dowler's friend into divulging her phone number to her, and then contacted a job agency assuming her identity.

The agency then replied to the woman's inquiries by calling Dowler's phone number and leaving a message on her voicemail.

But how could _NoW_ possibly have had this information?

One of the only reasonable explanations is that _NoW_ hacked Dowler's phone. After speaking to police contacts, the author found that they were also in the process of uncovering evidence of this incident.

After the story broke on 4 July 2011, the public became outraged by _NoW_ 's flagrant invasions of privacy.

Even the _Times_ — formerly loyal to its owner, Murdoch — which had until this point been ignoring the phone hacking scandal, now spurned _NoW_, calling their misdeeds "beyond reprehensible."

The scandal escalated as further stories broke that _NoW_ had hacked the phones of the families of soldiers who had died in Afghanistan.

Even other right-wing publications, such as the _Daily Mail_, condemned _NoW_. The _Mail_ wrote of Murdoch, "never again must one man be allowed to hold such power."

Denial became an exercise in futility, and the walls of _NoW_ were crumbling down.

### 8. Murdoch’s personal reputation was damaged, and NoW would never publish another piece of news again. 

In the end, the controversial tactics _NoW_ used to procure their juicy stories came back to haunt them, and eventually destroy them.

On Sunday, July 10, 2011, _NoW_ released its final edition, and then closed for good. The announcement had been made the preceding week by Rupert's son, James Murdoch.

Many saw this as a move to placate an enraged public, but it ultimately added more fuel to the fire. Twitter was alight with protest, with many accusing the Murdochs of saving themselves at the expense of hundreds who lost their jobs.

Murdoch's own employees were disgusted by his actions; in fact, 30 journalists from Murdoch's _Sun_ subsequently quit in protest.

What's more, the police and public were looking for justice, and found it after a police inquiry named _Operation Weeting_. 

The result of the investigation was several charges and convictions, among them an 18-month jail sentence for _NoW_ editor Andy Coulson.

Lord Justice Leveson also chaired a public inquiry into _NoW_ initiated by Prime Minister David Cameron. The investigation was far more open to the general public than a formal inquiry. In a public inquiry, the public has an opportunity to participate at all levels, both providing evidence and also listening to testimony.

It was this openness that gave one member of the audience the opportunity to throw a pie dish full of shaving cream right into Murdoch's face during his testimony.

Murdoch's pie was the proverbial "icing on the cake," the perfect sign of his fall from grace in the eyes of the British public.

Six years of deception and obfuscation wasn't enough to cover up the full extent of the nefarious nature of Murdoch's tabloids.

### 9. Final summary 

The key message in this book:

**Tabloid journalism is a ruthless enterprise that always walks the line between ethical and unethical, sometimes diving head-first into depravity. With juicy gossip going to the highest bidder, anyone's trust can be sold for the right price.**

**Suggested further reading:** ** _Manufacturing Consent_** **by Edward S. Herman and Noam Chomsky**

_Manufacturing Consent_ takes a critical view of the mass media to ask why only a narrow range of opinions are favored whilst others are suppressed or ignored.

It formulates a propaganda model which shows how alternative and independent information is filtered out by various financial and political factors allowing the news agenda to be dominated by those working on behalf of the wealthy and powerful. Far from being a free press, the media in fact maintain our unequal and unfair society.
---

### Nick Davies

Nick Davies is an award-winning author and investigative reporter. His numerous gongs include Reporter of the Year and Journalist of the Year at the British Press Awards. He is currently the special correspondent for the _Guardian_.

