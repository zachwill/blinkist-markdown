---
id: 53d8bb913930340007560000
slug: hackers-and-painters-en
published_date: 2014-07-29T00:00:00.000+00:00
author: Paul Graham
title: Hackers and Painters
subtitle: Big Ideas from the Computer Age
main_color: BA3236
text_color: BA3236
---

# Hackers and Painters

_Big Ideas from the Computer Age_

**Paul Graham**

In _Hackers_ _and_ _Painters,_ author Paul Graham examines the creative nature of computer programming and the programming languages that facilitate it, as well as how programmers can use their skills to potentially make a fortune.

---
### 1. What’s in it for me? Learn to better understand your “nerd” friends and programming colleagues. 

Most people who aren't into computer programming tend to think of it as boring and analytical. But if you're a programmer yourself, you know that programming is a very creative process, akin to painting. Just like artists, good programmers focus on creating great things, and the possibilities of what you can do are virtually limitless.

And this is not the only misconception non-programmers have about programmers. Yes, programmers may dress unfashionably or act awkwardly in social situations, but this is mostly because they're smart people and couldn't care less about social conventions. It's this rebellious spirit that makes them so good at programming, a state of mind that could help anyone come up with innovative ideas.

In these blinks, you'll discover:

  * what programming languages are and why there are so many,

  * why the motives of computer hackers initially baffled the Federal Bureau of Investigation, and

  * why perhaps it's time you too learned how to code — if you want to become obscenely rich.

### 2. Morals are as fleeting as fashion; and nerds are unpopular because they’re unaware of both. 

What do you think fashion and morals have in common? At first glance, not much. But actually both phenomena are limited to a certain time and place.

Obviously, fashion sense is tied to a specific time: take a look at photos from the 1990s and you'll see how far tastes have come since then. And if you look at images from foreign places like Japan, for example, even today you may be astonished at what is currently in fashion there, too.

It may be a surprise then to realize that morals are just as temporary and location-dependent. These _moral_ _fashions_ affect a whole spectrum of moral issues, including how you should treat other people and what values you cherish the most.

An extreme example: the moral compass of many Germans during World War II was very different than from the values of citizens in modern Germany today.

So, both clothing fashions and moral fashions are constantly changing. Can you think of any subgroup in society that is oblivious to these changes?

Of course: _nerds,_ or smart people who are not _socially_ _adept_ and so don't try to fit in with their peers. Nerds use their time and energy to become smarter without bothering to adhere to fashionable conventions, because being fashionable isn't a goal. This may be most apparent if someone you know wears a neon denim jacket every day for five years, but it also applies to moral fashions.

The result is that nerds are not very likely to be popular among their peers, especially in high school. To be voted prom king or queen, a student usually has to be in tune with the current moral and clothing fashions.

But thankfully after high school — in the real world — where being fashionable doesn't matter much anymore, nerds seem to do just fine.

### 3. Computer hackers are like artists, in the sense that both groups want to make good things. 

If you think of the word _hacker_, what comes to mind? Most people probably imagine a cold, calculating person who illegally breaks into computers through tedious, analytical work.

Now, think about an artist, like a painter. You've probably conjured up an image of an inspired genius who pours her soul on to a canvas.

Let's clear up a few misconceptions. First, in the computer world, the term hacker refers to an outstanding programmer who can do almost anything, not necessarily a criminal. What's more, hacking requires a very creative mind, closer to a painter's than say, a mathematician's.

In fact, both hackers and painters are similar in the sense that they both solve problems by creating concepts rather than implementing them.

For example, the author was originally taught he should work on computer code on paper until it was perfect, and only then, transfer the code to a computer. But he found that an artistic approach worked far better: he would just start writing code and solve problems as they occurred, much like a painter might just start sketching something and work from there, rather than perfectly planning her work beforehand.

Another similarity is that both hackers and painters produce work that has abstract value, which is hard to measure through metrics such as tests or media attention. The only measure of value for such "work" is how well people like it. For software, this depends on how well the software pleases the user by meeting his needs, and for art, on how well the work pleases the audience.

The author came to understand these similarities when he went to art school after graduating with a degree in computer science. He found both fields to have more similarities than differences, because both aim at the same fundamental goal: _making_ _good_ _things_.

### 4. Hackers need to be rebellious to be good at what they do. 

You may think that successful computer programming requires you to adhere rigidly to rules and conventions. After all, not doing so would result in a bunch of errors, right?

In fact, becoming a hacker often means violating some rules. Just like in any other profession, you can only learn to hack by looking at the work of those who came before you. And if you want to take a really close look, you may well have to bend and break some laws yourself, as it's possible that the work you're interested in is protected by intellectual property rights.

Hackers have a kind of _intellectual_ _curiosity_ about current technologies such as cutting-edge software, and yet to study it, they may have to break into someone else's computer. These endeavors are often illegal. Hackers are thus considered criminals, even though they're most likely committing a crime out of curiosity, rather than to steal anything.

Originally, when breaking into computers was first classified as a crime, the Federal Bureau of Investigation (FBI) struggled in its cases, as often a hacker's main motive was simple intellectual curiosity. As far as criminal motives go, this was — for the FBI — totally unheard of.

You may now be thinking that hackers should simply abide by the law, but in fact it is their rebellious nature that makes them so good at what they do.

Why?

Because being rebellious means questioning authority, whether the government or experts in your field. And questioning established ideas is the only way to come up with innovative ones, a prerequisite for good programming.

Most hackers are nerds in that they are smart people who don't really care about social conventions, so they are especially skilled in challenging everything — and then improving on it.

In the following blinks, you'll find out how nerds use their rebellious genius to make tons of money.

> _"To do good work, you need a brain that can go anywhere."_

### 5. Launching a start-up can be a fast way to get rich, and there’s nothing wrong with that. 

Many people dream of one day starting a company around a great idea they've had. But if you're a good programmer, you really should go for it and found your own company.

Why?

Because launching a start-up really is one of the fastest ways to becoming rich.

When working for a large corporation, your wage won't substantially increase no matter how hard you work. If you found a start-up, on the other hand, you'll have to work very hard but at least every hour of work you put in will make it more likely your company will succeed and you'll get a big payout.

This is what happened to the author. He and his start-up's two co-founders worked long hours on their online shopping application called _Viaweb_, and eventually sold it for millions.

Some may protest that this is the wrong goal, and that the whole dynamic of concentrating wealth in the hands of the rich is a mistake. But actually, concentrating wealth is not only fair, it's good for everyone.

Why?

Because wealth is not the same as money. Wealth constitutes the things that people want, whereas money is only the _medium_ _of_ _exchange_ that people trade for wealth.

With this distinction in mind, it's obvious that wealth can be created by anyone, without diminishing the wealth of others. For example, if you own a classic car and refurbish it in your spare time, you're creating new wealth without depriving anyone else of it. This means wealth is not limited, so the rich aren't "hogging" it.

Also, isn't it possible that the richest individuals, such as CEOs, professional athletes and so on, really have worked harder and more productively than others, so that they deserve their wealth?

For example, if you have to work ten times harder in your start-up than the average corporate employee, isn't it fair that you earn ten times the income? Especially when in doing so, you'll create a product that everyone can enjoy and benefit from.

### 6. The end-user’s opinion is the ultimate test for how successful your product and company are. 

As discussed before, just like painters have to please an audience, hackers need to please a user. This means that when you found a company and begin designing your product, the end user has to take center stage in your mind.

First of all, you should create and launch a prototype of your product as soon as possible to collect feedback from real users. This strategy is called _Worse_ _is_ _Better,_ because launching even a pretty stripped-down prototype early on gets you valuable feedback that allows you to fix mistakes and improve the product to better suit your users' needs.

One example of the efficacy of this approach comes from a surprising source: the famed author Jane Austen. Before finalizing her books, she used to read them aloud to her family, who then made suggestions about what they would like to hear in the book. They were the first users for her "prototype."

If you succeed and manage to make a product that matches users' needs and interests, they will probably want to buy it. Otherwise they won't, no matter how many other sophisticated features it has.

For example, if you were to design a chair, do you think anyone would buy it if it looked great but was horrendously uncomfortable? Probably not.

To gain users, you have to satisfy their needs, and the more users you have, the more successful your company will be. This is also a virtuous cycle: users generate revenue, which you can then reinvest to make your product even better and get even more users.

What's more, if someone is considering acquiring your company, they will value it based on the number of users you have. An app that has 100,000 users will have more potential buyers than an app with just 300 users.

### 7. Programming languages differ according to their purpose, and are being improved all the time. 

"C++, Python, Java…" You've probably sometimes heard programmers listing all the languages they can code in. What they're referring to are the _programming_ _languages_ in which they are able to give commands to computers.

But computers don't actually understand these languages. They only comprehend _machine_ _language_, meaning specific combinations of ones and zeros that tell the computer what functions to perform. This means programmers also need a compiler to translate the programming language into machine language.

This sounds reasonable, but it might beg the question: why do we need so many different programming languages?

The reason is simply that different programming languages are good for different kinds of tasks. Just like with spoken languages, it can be easier to express certain things in one language than in another.

For example, if you want to tell your computer to add variable Y to variable X, you can do it in two lines of code in the language Lisp, whereas you'd need four lines in the language Perl.

Sometimes the necessary concepts for your program don't even exist in the language you've chosen, so you have to either switch or find a workaround in the first language.

For example, if you tried to add Y to X in the language Python, you'd run into trouble because it doesn't fully recognize variables like that. So you'd have to find a workaround, which in this case would mean writing six lines of code.

This is why there are so many languages in existence. But the field is far from stagnant: existing languages are constantly evolving and new ones are being developed. This is because languages are developed by programmers who adapt them according to their own preferences and requirements.

Up until the 1980s this was not possible, since programming languages were developed only by institutions and large companies. These days, anyone can afford the necessary technology to adapt and develop current programming languages, or even come up with new ones.

> _"If we were all using the same language, it would probably be the wrong one."_

### 8. A good programming language is designed with taste and according to a hacker’s needs. 

If you were to look at 1,000 paintings and divide the good from the bad, you'd obviously make that judgment based on your own taste. The same is true for hackers who decide between good and bad programming languages. They appreciate good taste in the programming languages they use, and as they advance as programmers, their own taste changes and develops, too.

For example, if you've designed your own programming language and years later, publish a new version, it will surely be better than the previous one. This is because you didn't have the skills to make the original version as good as the updated one, and your taste has also improved along the way.

So what's a good programming language? One that caters to a hacker's needs.

For example, if a language is too complicated or limiting, hackers will simply use another one. If, however, a language meets the needs of hackers, it will become popular, which means they will embrace it and develop it further to perfect it. So good languages will inevitably get better over time, as hackers voluntarily work on finding and fixing bugs in it.

Good examples of this dynamic can be found in open-source languages such as Perl and Python, which were privately designed by individuals but later released to the public so anyone and everyone could change and improve them.

### 9. Businesses prefer popular programming languages, but sometimes obscure ones could give them an edge. 

If you're not a programmer and want to start a business that requires programming, you probably have no idea which programming language your company should use. So how should non-programmers like business managers and project leaders choose the language for their programmers to code in?

In general, businesses prefer using popular, widespread languages.

This is because a program written in a commonly used language will likely be compatible with many other programs written in that same language.

Another reason is that it's easier for companies to find and recruit programmers who code in popular languages. This can sometimes be crucial: if you've recruited someone to write a program in a rare language like Lisp and she suddenly quits her job, you'll be in real trouble. Chances are you won't be able to find a replacement and will have to throw the whole project away.

Since companies have more demand for coders with popular language skills, it also means that there will always be more hackers using them than obscure ones.

Despite these arguments in favor of popular languages, there are certain downsides, too.

Since certain programming languages are optimized for certain tasks, forcing programmers to use a popular language for everything means you're possibly not getting the best performance from your product.

Most likely, your competitors have this same problem. So if you were to switch to a more optimal language, you could gain a significant edge. What's more, your competitors could no longer analyze your position based on their own, since your products would be based on completely different technical platforms.

This is what the author did when he created Viaweb with the little-known language Lisp, giving him a technical advantage and leaving his competitors wondering what exactly he was doing.

### 10. You could actually stop getting spam emails if you wanted to. 

It was mentioned earlier that a good hacker can do virtually anything. One fun example is tackling a problem you're probably all too familiar with: _spam_, the seemingly unstoppable flow of unsolicited advertisements that bombard your inbox.

So if you're a hacker sick of spam, how would you deal with it? You'd probably come up with a few different methods.

First, you could approach the problem by looking at the individual properties of spam emails. For example, many spam emails begin with something like "Dear Friend," so you could write a piece of code that automatically puts all messages starting with this phrase in your spam folder.

Second, you could employ statistical filtering, whereby every word in an incoming email would be analyzed, with the computer calculating the probability of the message being spam. Then the computer would put the likely offenders in your spam folder.

Perhaps the best option however would be to personalize this statistical approach, saving certain messages from the spam folder even if the statistics indicated they might be spam. For example, the author decreed any messages with the word "Lisp" as non-spam, as the word was included in many legitimate emails about the programming language. He also decided that messages from the addresses he had corresponded with were safe.

But if you're a good hacker, you will also consider the consequences of your actions. In this case, by stopping spam you risk also missing some important emails, because spam filters are never 100-percent accurate and can easily intercept legitimate mail, too. So you'll probably have to sift through your spam folder every now and then to check for legitimate messages, but afterwards you can always improve your spam filter to not make the same mistake again.

So while spam can be extremely irritating, trying to filter out all spam can also bring its own problems.

### 11. Final Summary 

The key message in this book:

**Contrary** **to** **what** **most** **people** **think,** **computer** **programming** **is** **artistic** **work,** **and** **a** **programming** **nerd's** **rebellious** **nature** **can** **result** **in** **the** **creation** **of** **inspiring,** **innovative** **things.** **By** **founding** **a** **start-up** **around** **a** **great** **idea,** **a** **computer** **programmer** **can** **even** **make** **a** **fortune.**

Actionable advice:

**If** **you're** **a** **programmer** **with** **a** **business** **idea,** **go** **for** **it!**

Very few people can become wealthy working as programmers for big corporations, as a company's success has little or no impact on wages. So if you want to become rich and you have a good business idea, go for it: found your own start-up! Yes, you'll have to work much harder than at a regular 9-to-5 job, but at least you can make your own decisions about what to do, create interesting new products that benefit your users and at the end of the day, every hour you put in will increase the chance that you'll sell your company for a fortune.
---

### Paul Graham

Paul Graham is a British programmer and venture capitalist. He is the co-founder of start-up Viaweb that was sold in 1998 to Yahoo! and eventually became Yahoo! Store.

