---
id: 5ab6e7e3b238e10005bf58b9
slug: hacking-growth-en
published_date: 2018-03-26T00:00:00.000+00:00
author: Sean Ellis & Morgan Brown
title: Hacking Growth
subtitle: How Today's Fastest-Growing Companies Drive Breakout Success
main_color: ED2F38
text_color: BA252C
---

# Hacking Growth

_How Today's Fastest-Growing Companies Drive Breakout Success_

**Sean Ellis & Morgan Brown**

_Hacking_ _Growth_ (2017) provides online business owners with a game plan for taking their company to the next level. Sean Ellis and Morgan Brown give readers a step-by-step guide through a dynamic and endlessly repeatable process that will spark growth and transform any sluggish business into a vibrant, growing enterprise teeming with loyal customers.

---
### 1. What’s in it for me? Make your product the next big thing. 

Any business owner from any generation will tell you that growing a small company into a big one isn't easy. But now, with so many new possibilities in online marketplaces, it's a whole new ball game.

Online business may present a host of new problems, but it also provides an unprecedented amount of data, not to mention the ability to communicate with customers in amazingly useful ways. These opportunities — to test your product and learn from customers — are two of the key factors in hacking growth.

These blinks explain the growth hacking playbook. They provide a systematic way of collecting, analyzing and testing data that just about any business can use to quickly make the most of its time and resources.

In these blinks, you'll also find out

  * what a college football team can teach us about efficient business practices;

  * how data about your customer's internet connection can be a game changer; and

  * how to find your company's North Star.

### 2. Growth teams use inter-department collaboration and strong leadership to get results. 

Here's a common scenario that any number of entrepreneurs face today: you're the CEO of a small business, and though you're making a nice profit, you need to find a way to take it to the next level.

What you need to do is prioritize growth by establishing a _growth team_. A growth team is made up of members from different departments within your company, and it harnesses the power of collaboration to focus exclusively on finding ways to grow.

Let's look at a real-world example. Prior to forming a growth team, the software company BitTorrent had 50 employees working in the traditional departments of engineering, marketing and product development. This brought them good results until 2012, when their growth plateaued.

The problem was that too many customers were using the basic, free version of their product. And despite making improvements to the premium, paid version, few people were making the upgrade.

Things changed, however, when an innovative project-marketing manager (PMM) came aboard, created a growth team and sparked the kind of fresh perspective they needed. By looking at engineering issues from a marketing point of view, it became clear that the lack of upgrades wasn't due to a quality issue. Most customers were simply unaware of the premium version and what it offered.

Armed with this insight, the marketing and engineering teams joined forces to raise awareness by prominently promoting the premium version to users of the free version. As a result, upgrades skyrocketed, and revenue increased by 92 percent.

But in order for your growth team to succeed, it needs to have a strong leader. It needs someone who can unite the interdisciplinary team and keep them on course for improvement.

This leader will identify the target area, set clear goals and establish a time frame for the accomplishment of these goals. For example, he or she may decide it's best to focus on developing new marketing channels and set a goal for the team to come up with three potential new ones by the end of the month.

The growth leader is also responsible for keeping the team focused on moving forward and steering them clear of distractions. While attractive new ideas can be distracting, the team leader must recognize when these ideas don't serve the current goal and need to be put on the back burner.

### 3. To develop a must-have product, you need to know your customers well. 

There are countless entrepreneurs hoping to come up with the next Airbnb, Yelp, Facebook or Amazon. Each of these companies experienced rapid growth, and that's probably because they all have something in common. They each released a _must-have_ product or service that people loved.

Even if you're already sure that people love your product, you've got to get a grasp on your customers' experiences and opinions by reaching out to them. Only then will you really know if your product is a must-have.

The _must-have survey_ is a simple and reliable method for figuring out how your customers feel about your product. And all it takes is asking at least a few hundred customers a single multiple-choice question.

Here's the question: "How disappointed would you be if this product no longer existed tomorrow?"

And here are the three possible answers:

A. Very disappointed   

B. Somewhat disappointed   

C. Not disappointed

You can be confident that your product is a must-have if at least 40 percent of your customers choose "Very disappointed." Once you reach this milestone, you are in the perfect position to start using the growth hacking techniques that are described in the blinks ahead.

However, if the amount of customers answering, "Very disappointed" is under 40 percent, you must continue to improve and develop your product. If this is the case, don't fret — there are quick, efficient and inexpensive ways to experiment with how you can communicate your product's value.

One method is _A/B testing_, which is a way of testing the effectiveness of two different messages or product variations. The project-management tool Basecamp used this method to test potential marketing taglines, for example, and that's how they discovered that the prompt "See Plans and Pricing" attracted twice as many new customers as "Sign Up for Free Trial."

Plus, the great thing about A/B testing, whether it's being done face-to-face or online, is that it doesn't have to be expensive. Crucial insights can be gained by testing out new approaches, like showing test audiences a simple video demonstration or an inexpensive prototype that highlights potential new features. Even if the tests prove unsuccessful, the feedback you'll get will prove extremely valuable.

### 4. Identify key data metrics to track growth-hacking progress. 

In the movie _Field of Dreams_, the main character hears a mysterious voice: "If you build it, they will come." And, sure enough, after spending every penny he has to build a baseball park on his property, people miraculously arrive.

While this might make for an inspirational Hollywood story, in reality, you've got to do more than just build something great if you want people to come and keep your business thriving.

One of the best ways to drive traffic to your product or service is to figure out which metrics matter most to generating growth.

Standard metrics for online businesses include web traffic, user acquisition and returning users, but your business is sure to have specific metrics that are uniquely relevant to your growth. It's important to identify these unique core metrics as soon as possible. If you don't, you may find yourself drowning in data and distracted from what really matters.

To find out which specific metrics you need, ask yourself this question: Which customer actions can be measured to reveal how positive their experience with your product is?

Facebook's core metrics include how frequently users log in, how much time they spend on the site, how active they are in creating posts and comments and how many friend requests are being sent. Since Facebook's revenue is based on selling ad space, these are all very important metrics. Each shows how many people are looking at the site and how engaged they are.

Nevertheless, it's important to find one key metric to serve as your _North Star_. Your North Star is the one metric that, above all others, best measures the core value your product is delivering. This measurement will keep teams focused on growth and the efficient use of time and resources.

At Facebook, the North Star is the number of daily active users. This is more important than the number of posts being made or friend requests being sent because it offers a simple and general indication of Facebook's growth.

Now that we know which stats to monitor, let's look at some real growth-hacking techniques.

### 5. Rapidly develop new ideas in the first two stages of the growth-hacking cycle: analyze and ideate. 

At the start of the 2008 season, the Baylor University football team had been struggling for over a decade. But things changed that year: the new head coach, Art Briles, introduced to the team a fast-paced, no-huddle offense.

This new high-speed game helped Baylor not only out-hustle its opponents on game day; it also allowed the team to run more plays during practice. In other words, Baylor was able to learn more in less time. And this is the kind of efficiency you can implement by embarking on the four-stage _growth hacking cycle_.

The first stage is known as the _analyze stage_, which involves collecting and analyzing your data to gain customer insight.

For this you can prepare a series of questions, such as, "How do our customers typically behave?" "What are the characteristics of our best customers?" "What events lead customers to stop using our product?"

For each question, you should prepare a list of probing sub-questions, like, "What time of the day are customers most actively purchasing the product?"

The marketing experts should use surveys and interviews to collect this valuable data, and, once it's been analyzed, trends and patterns should become apparent. These insights can then be shared and discussed in a team meeting, where members can begin brainstorming some growth-hacking ideas.

This brings us to the second stage, the _ideate stage_, which is when every member of the growth team helps develop ideas based on their area of expertise. After getting things started at the team meeting, you can give members the next four days to submit as many ideas as they can think up.

All proposals should go through the _idea pipeline_, which works by offering a structured format to log, track and evaluate all the incoming ideas. This would involve giving each one a short name such as "Loyalty Rewards" and briefly describing the proposal's who, what, when, where, why and how.

Make sure you note both the action and the expected beneficial outcome, like this: "By rewarding customer loyalty, we'll increase the number of returning customers by 30 percent."

Finally, don't forget to make a note of the metrics that can be used to track how well the idea is performing. In the case of a customer loyalty program, the effect could be tracked through the customer retention rate.

> _"_ _The best way to have a good idea is to have lots of ideas."_

### 6. Rapidly test new ideas in the last two stages of the growth-hacking cycle: prioritize and test. 

While the first two stages of analysis and ideas are about finding and implementing growth hacks, the final two stages are about testing your new ideas.

The third stage is to _prioritize_, which involves putting the ICE scoring system to use. To prioritize your ideas using the ICE system, each member of the growth team rates their ideas based on three factors: _impact_, _confidence_ and _ease_.

Impact is determined by quantifying expected growth — the greater the growth, the greater the impact. Confidence is determined by the team member's conviction that the idea will succeed, which is something that should be backed up by hard data. Ease should be rated according to how much time and resources the idea will require during testing.

Each of these categories receives a score between one and ten. For example, if a team member suggests offering customers a $10 credit for any referral, the impact on new sign-ups might be a seven, while the confidence rating might only be a four due to having limited data on user referrals. And since the idea is rather simple to implement, the ease rating could be an eight.

To arrive at the final ICE score, you must first add each rating and then average out the sum. So in this case, you'd get a sum of 19 and then a final average of 6.33. After each idea is given a score, they should then be sorted from highest to lowest, with the best-scoring ideas getting immediately shortlisted for further team discussion and testing.

This brings us to the fourth and final stage in the growth-hacking cycle: the _testing stage_. This is when you bring ideas before customers and determine which ones deserve to be fully implemented.

At this stage, it's recommended that you work with a data analyst to come up with strict guidelines so that you produce a reliable set of results.

For example, always use a statistical-confidence interval of 99 percent that indicates a 1 percent margin for error in the results. That way you can be sure that your tests are providing accurate insights.

Remember, as with the Baylor University football team, the more often you can repeat the growth-hacking cycle, the more you'll learn and the more growth you can achieve.

### 7. Attract customers by creating a message that resonates and narrowing down your marketing channels. 

Attracting new customers is always a challenge. But, as crazy as it sounds, it's not always worth the effort. These days, a lot of businesses spend more money on acquiring new customers than those customers will ever generate in revenue. This raises a pressing question: How can you attract new customers without going bankrupt in the process?

The first message your business presents to a potential customer plays an extremely important role. It must quickly communicate information _and_ resonate with them in a meaningful way.

These days, the average attention span of someone online is a mere eight seconds — so that's how long you have to explain why your product is beneficial to a potential customer's life. Short-and-sweet taglines are particularly useful. Just consider the slogan for the original iPod: "1,000 Songs in Your Pocket."

Of course, we can't all be a marketing geniuses like Apple's Steve Jobs, but we can learn from him and use words that a typical customer would use to describe why your product is beneficial. These words can be taken from a social-media post, a product review or a customer survey.

You can perform a creativity hack by collaborating and brainstorming dozens of variations on a single message, picking out the ones with the greatest potential and using them in a simple A/B marketing test.

You can also bring some extra focus to your efforts by limiting how many marketing channels you use.

Here's how: Start by creating a list of possible channels that fit your business model. For example, if you plan for people to find your product via a search engine, then you would make sure that search-engine optimization is at the top of your list.

When you have your list of possible channels, you can then test them to find out which ones you should prioritize. This test is based on six different criteria: how much testing will cost, how much time the tests take to set up, how much time it will take to collect the results, how well the test targets your desired customers, how flexible the test is to adjustments and how many subjects will be tested.

Give each of these categories a score between one and ten, add up the sum and then average out the score. Then test the channels with the highest score.

> _"_ _A good headline can be the difference between 1,000 people and 100,000 people reading."_

### 8. Make it easy for fans to become customers. 

Even if you succeed in attracting potential customers to your product, the next challenge is to turn them into purchasing customers. Only 2 percent of most sites' web traffic leads to an actual sale. But look on the bright side of this teeny percentage: there's lots of room for improvement.

A great way to help bring in customers is to create a _funnel report_, which allows you to pinpoint where customers tend to drop off.

You can start a funnel report by mapping out all the main steps a customer takes, from first contact to making a purchase, and then note the conversion rate between each step — that is, the percentage of customers who actually proceed from one step to the next.

If you were to do this for Uber, the first two steps are to download the app and create an account. And let's say that, last month, 10,000 customers downloaded the app and 8,000 of them went on to create an account. This gives an 80-percent conversion rate.

Step number three is booking a ride. Let's say 500 of the 8,000 customers take this step. Well, the conversion rate would be quite low, at 6.25 percent. Looking at this funnel report, a manager at Uber would spot a red flag and try to fix the problem of customers signing up but failing to actually use the app. Maybe the booking process needs to be simplified?

As you can see, once you pinpoint where the customers drop off, you can begin to spot the problem and come up with solutions. Customer surveys are also a good way to do this, whether they're emailed to customers or appear as pop-up surveys on your website.

Whichever method you use, these surveys should always be brief and focus on learning why the customer did or didn't take the next step. To help solve Uber's problem, a couple of days after a customer creates an account, the app could produce a survey asking the question, "Is there anything preventing you from ordering a ride?"

Given the prompt, customers could provide useful information, like feeling uneasy about riding alone in a car with a stranger at the wheel. This would help Uber brainstorm and test different ways of reassuring customers, such as highlighting the fact that all drivers have undergone background checks.

### 9. Keep customers loyal and active by creating customer habits. 

If you've ever struggled with a big lifestyle change, like trying to start a new diet or exercise regimen, you know that the first couple of weeks are the toughest. But if you've managed to stick with it, then you also know that it gets easier over time.

This process is known as the power of habit formation, and it applies to growth hacking as well. Many businesses have found success by helping customers form habits by offering rewards through a process known as _engagement loops_.

Amazon is a prime example (no pun intended), since their Amazon Prime program is a perfect engagement loop. The company makes an offer, rewards customers for taking it and then provides an excellent service to keep the customer happy and coming back for more.

Let's take it one step at a time. First, Amazon makes the offer of signing up for a Prime membership, which it does through promotional emails, displays on their website and various other notifications. Once a customer signs up, this is followed by the most important step in the engagement loop: rewarding the customer for taking the offer.

By signing up for Amazon Prime, the customer gets a number of benefits, including instant streaming of movies and TV shows and free two-day shipping. So the customer gets instant gratification. Then Amazon reassures customers that they made the right decision by reminding them about how much money they're saving every time they make a purchase.

These rewards feel good, and it doesn't take shoppers long before they come to expect this good feeling every time they go to an online marketplace. And since Amazon is the only business with a reward program like Prime, it's built a very loyal customer base of habitual Amazon shoppers.

Prime has been so incredibly successful that the results even surpassed Amazon's expectations. Each year, more than 90 percent of the Prime subscribers have renewed their membership.

A good way to start your own engagement loop is to test different reward strategies in order to find out what customers value the most. You can start with two of the most successful types of reward programs: _social recognition_ and _user achievements_.

Social recognition is what Yelp uses by designating its most active users with "Elite" status, which includes invitations to special parties and events, and a badge to highlight their status on their online profile.

Rewarding user achievements is another effective way to make customers feel good. For example, the fitness company Fitbit sends a congratulatory notification to its users when they take their ten thousandth step.

### 10. Understand your customers, and you’ll earn more from them. 

At this point, you might be thinking that all these tips are great, but when do we get to growing revenue? Indeed, increased revenue can be one of the best signs of company growth, and knowing where the majority of your revenue is coming from is truly valuable information.

That's why identifying and understanding your _customer segments_ is a growth hack that will lead you to increasing your best revenue streams.

You can break down customer segments in a variety of ways, from age to location to membership type. But that's not all. Once you've segmented on a basic level, you can look for similarities between segments and the revenue they're bringing in. This is another piece of data that can generate both ideas to test and potential growth hacks to implement.

For example, HotelTonight is an app that helps users book last-minute accommodations. It once segmented its customers into two groups: those who accessed the app via Wi-Fi and those who accessed it using cellular data. This led to the surprising revelation that twice as much revenue came from the cellular-data segment.

The possibility for this discrepancy might be that a slower and more expensive cellular-data connection made the customers more eager to make a quick purchase and not hunt for more bargains. So the HotelTonight team decided to test advertising directly to data users, an experiment that paid off handsomely. More of these users signed up, and profits increased.

Another way to optimize revenue is to always be developing and unveiling new features. This is especially useful in making your current customers happy and willing to keep revenue coming in.

Meanwhile, you can find out what kind of new benefits and features your customers want through the use of surveys. For example, One of BitTorrent's surveys asked customers to rate six different potential features — such as previewing files before downloading — that they were thinking about adding to an updated premium version. To help persuade customers to fill out the survey, all participants were automatically entered to win a free upgrade.

No matter what kind of business you're part of, there will always be customers to listen to, valuable data to analyze and a new growth-hacking cycle just waiting to get started.

### 11. Final summary 

The key message in this book:

**Growth hacking is a proven and effective business strategy that can be adopted by companies of any size, in any field. Its key tenets are cross-functional teams, rigorous data collection and analysis and rapid experimentation and testing.**

Actionable advice:

**Launch a pricing optimization survey.**

Start getting the most out of how much your product costs by asking your customers the following four questions:

  * At what price point would you no longer consider paying for (your product)?

  * What is the most expensive price that you would still consider paying for (your product)?

  * What price point would you consider a great deal for (your product)?

  * What price point would you consider too inexpensive and cause you to doubt the quality of (your product)?

The answers to these questions will point you toward the optimal price range for your product. And with further testing, you can find the perfect final price.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Startup Growth Engines_** **by Sean Ellis**

We've all heard success stories of start-ups attracting millions of users and earning billions of dollars virtually overnight. _Startup Growth Engines_ (2014) shows us what all these companies have in common: a new approach called "growth hacking". These blinks reveal how your business can use viral marketing techniques, freemium business models and other growth engines to rapidly achieve business success.
---

### Sean Ellis & Morgan Brown

Sean Ellis is cofounder and CEO of GrowthHackers.com. He's responsible for the term "growth hacker," as well as organizing the Growth Hackers Conference.

Morgan Brown specializes in marketing for start-ups and has given talks around the world on how to grow a business. He is also a cofounder of GrowthHackers.com and the COO of the real-estate news company _Inman News_.

