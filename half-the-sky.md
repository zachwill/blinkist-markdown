---
id: 56407da43866330007620000
slug: half-the-sky-en
published_date: 2015-11-12T00:00:00.000+00:00
author: Nicholas D. Kristof and Sheryl WuDunn
title: Half the Sky
subtitle: Turning Oppression into Opportunity for Women Worldwide
main_color: 7886AD
text_color: 434B61
---

# Half the Sky

_Turning Oppression into Opportunity for Women Worldwide_

**Nicholas D. Kristof and Sheryl WuDunn**

_Half the Sky_ is about unlocking the greatest untapped resource on Earth: women. It outlines some of the most serious problems facing women throughout the world, such as human trafficking and gender-based violence, and why it's so difficult for the world to overcome them, and what we stand to gain if we do.

---
### 1. What’s in it for me? Discover the awful truth about global gender inequality and what we can do about it. 

After illegal drugs and weapons, human trafficking is the largest international crime industry. All over the world women are treated like slaves, sold and shipped around the globe to be sexually abused by men. How can this be? 

As these blinks will show you, modern-day sex slavery has it roots in a deeply misogynous culture worldwide, which affects women and men alike. 

Gender inequality is truly a global challenge but many of the things we do to better the situation actually end up making it worse. So how do we move forward? How do we create a more equal world?

In these blinks, you'll learn

  * why prostitution should not be regulated but outlawed;

  * why so many women fall back into prostitution once they've been freed; and

  * what iodizing salt has to do with fighting gender inequality.

### 2. Sex slavery exists because certain women are viewed as “discounted humans.” 

Today, the number of enslaved women trafficked into brothels every year is greater than the number of African slaves who were sent to plantations in the 18th and 19th centuries. Many die from AIDS when they're only in their twenties. How is this possible?

It's important to understand the difference between prostitution and sex slavery. The former is mostly voluntary: women may be economically pressured into it, but they're not physically forced to do it. China has the highest number of prostitutes of any country. 

Sex slaves, on the other hand, are often locked in a room and forced to work around fifteen hours a day, seven days a week. They're often unpaid, and frequently beaten and humiliated. India has the highest number of sex slaves. 

Humiliation is key to keeping women trapped in these situations. Once you break someone's spirit, you can coerce them into almost anything. Some women are even manipulated into going out into the street with a smile on their faces and bring men back to the brothel. 

One 15-year-old Thai girl reported that she was forced to eat dog feces just to break her will. 

The sex slave trade operates on an implicit contract: men satisfy themselves with lower-class girls so upper-class girls can maintain their virtuousness. 

In fact, in India, border officers often allow traffickers and their slaves to get in. They're tougher on terrorists, weapons and smuggled or pirated goods like copied DVDs. That's because, like many, they believe that prostitution is inevitable: it's the only outlet for men who don't get married until they're around 30. They also believe that it keeps good, middle-class Indian girls safe by sacrificing peasant girls (who are often Nepalese) instead. 

People get away with this in the modern world for the same reasons people got away with enslaving Africans centuries ago: slaves are perceived as discounted humans.

### 3. The movement against sex slavery needs more charisma, unity and follow-up work to move forward. 

Figures like Martin Luther King Jr. and Mahatma Gandhi were so successful as proponents of the peace movement largely because of their charisma. The global movement against sex trafficking would benefit a lot from having such leaders. 

That's why we need to invest more in emerging leaders like Zach Hunter, who formed Loose Change to Loosen Chains (LC2LC), a student-run campaign against modern slavery, when he was only in the seventh grade. 

In addition, the sex slavery abolitionist movement would be more effective if it were more unified. Opinions are still divided on prostitution: some believe it's fine for consenting adults while others believe it's inherently degrading. 

However, there is a consensus that _forced_ prostitution is wrong. People need to unite, support crackdowns on brothels and work on providing victims with social services, like job training and drug rehabilitation. 

This would be more effective than the legalize-and-regulate model, which aims to legalize prostitution and reduce its harm by handing out condoms, restricting the spread of AIDS and preventing underage girls from working in brothels. 

Believe it or not, rescuing girls from prostitution is the easy part. It's much harder to stop them from falling back into it afterward, thanks to the social stigma that follows them in their communities and the fact that many of them have drug addictions. 

Take Srey Momm, a Cambodian brothel worker who was rescued from her brothel several times by the World Assistance for Cambodia (formerly known as the American Assistance for Cambodia), but she always returned because she was addicted to the methamphetamine she got there.

There are two main ways of helping victims and preventing them from returning to their brothels. First, we need to make them realize that women don't have to be submissive to be feminine (this often means supporting strong local women) and, second, we need to invest in their education.

### 4. Sexism and misogyny are difficult to defeat because they’re deeply embedded in human culture. 

Here's an appalling fact: women between the ages of 15 and 44 are more likely to be maimed or killed by male violence than by war, cancer, malaria and traffic accidents combined. 

Rape and other violence toward women isn't just a result of male libidos and opportunism. Sexism and misogyny are deeply rooted in our cultures.

Sexism is pervasive throughout the world and women perpetuate it, too. In fact, women often run brothels, feed their sons before their daughters and have their daughters genitally mutilated. 

Zoya Najabi, a 21-year-old woman from a middle-class family in Kabul, was physically abused by her husband, his brother, his mother and his sister. She was beaten and left in a well until she nearly froze and drowned — supposedly because of her shoddy housework. Najabi believes she shouldn't have been beaten because she was obedient, but a husband can beat his wife if she is disobedient. 

Sixteen-year-old Noel Rwabirinba, a male child soldier in the Congo, believes that soldiers have a right to rape people. Unfortunately, sexism is entrenched in the minds of men and women alike. 

That's what makes it so hard to change. Misogyny can only be undone through education and strong local leadership. 

Unfortunately, foreigners often end up doing more harm than good because they're unaware of local customs. 

In one such case, a UN project gave women in a particular area of Nigeria a new type of cassava, a root vegetable with a good yield. In their society, however, women always managed staple crops while men managed cash crops. So when the women started earning more money from the cassava crop, the men took over and used the profits for beer. The women were left with even less income than before the project started.

### 5. There are sociological and biological reasons for maternal mortality. 

Roughly five jumbo jets worth of women die in labor everyday, even though maternal mortality could easily be prevented. Even worse, this issue is almost never covered in the news. 

Some of the more effective approaches to solving the problem aren't even expensive or complicated. One study found that providing girls with a $6 uniform every 18 months increased their chances of staying in school and thus lowered the number of pregnancies. Keeping girls in school longer also delays marriage and pregnancy until the girls are old enough to give birth more safely. This illustrates that it's not always medical problems that cause women to die during childbirth. 

Maternal mortality has sociological causes. It results from a lack of education, rural health systems and a general disregard for women. 

Simeesh Segaye, a 21-year-old Ethiopian, for example, was left crippled with a fistula, leaking urine and feces, and no baby. Her parents and husband saved $10 to pay for the bus to take her back to the hospital, but the passengers didn't allow her on because of her smell. Her husband left her and her parents built a separate hut for her. 

Segaye stayed curled in a fetal position for two years because of the pain. Finally, her parents sold all of their assets and paid $250 for a private car to take her to the hospital. Her fistula was treated but her legs were still bent. After months of physiotherapy and a few operations, she was finally able to stretch them. 

If the people around her had been educated, had better health infrastructure and more respect for women, Segaye might not have had to endure two years of pain and anguish alone.

> _"Maternal mortality is an injustice that is tolerated only because its victims are poor, rural women."_

### 6. Religion plays a big role in gender inequality. 

Each year, more and more people declare themselves atheists, but the majority of the world population is still religious. This has a big impact on gender equality. 

Secular liberals and conservative Christians often square off on abortion, for instance. This is called the "God gulf" and it shapes American policies on family planning. 

A lack of funding for abortion often results in more unwanted pregnancies, unsafe abortions and deaths of women and girls. 

And religion, gender inequality and abortion aren't only problems in the US. When religious leaders in any place denounce abortion it results in more unsafe and illegal abortions. In fact, a women dies in one out of every 150 unsafe abortions in Sub-Saharan Africa. 

Religion also has a big impact on gender inequality in some Islamic countries. Many countries where women endure abuses like honor killings are Muslim.

This doesn't mean that Islam is inherently misogynous, however. When Muhammad introduced Islam in the seventh century, it was actually very progressive compared to Christianity.

This may partly explain why it's harder for many Muslims to shrug off the gender discrimination in their holy book than for many Jews or Christians. Unlike the Torah and the Bible, the Quran tends to be read more literally. 

Meanwhile, Islamic feminists interpret the Quran in a completely different way. They argue that because Islam was originally progressive, it shouldn't stray from that spirit by becoming backward. 

The Muslim world used to have deep cultural ties to slavery but managed to eradicate it. It can do the same for women by offering them complete emancipation. 

Moreover, Muslim leaders are realizing that gender inequality prevents them from tapping into their nation's greatest unexploited economic resource: women.

### 7. One of the best ways to eliminate gender inequality is through education. 

Education is vital to gender equality. It empowers women and girls to stand up for themselves and integrates them into the economy. 

However, contrary to what most Americans think, building more schools isn't the best way to increase education. 

There are actually much simpler solutions, like iodizing salt, which can prevent brain damage. One study showed that iodine deficiency can take 10 to 15 points off a child's IQ. 

Another strategy is to provide girls with feminine hygiene products, such as pads and tampons. Girls who only have cloth rags and are embarrassed about possible leaks often skip school during their periods. 

There are a few others ways we can work to improve gender equality. Any movement for equality must adhere to the following four guiding rules:

  * We have to bridge the God gulf by building links between liberals and conservatives. 

  * We must resist the temptation to exaggerate. The humanitarian community has done a lot of harm by exaggerating their findings, so that people are now skeptical of them. 

  * We must help women by financing local projects or volunteering. 

  * We have to be less narrow-minded in our concerns about human life. If conservatives are concerned about the lives of unborn fetuses in the US, they should also be concerned about the lives of sex slaves in Asia. 

Funding education, iodizing salt and eradicating fistula won't solve everything, but it would put the issue higher on the international agenda. 

Still, we cannot limit ourselves to these four rules — we must also be open to new ideas. For example, new studies have shown that, while education is still the best way to lower fertility rates, improve children's health and create a more just society, television is also extremely influential. 

Take one area in Brazil: after a Globo television network started broadcasting there, the number of births went down. Women from lower socioeconomic classes stopped having children to better emulate the TV soap opera characters they admired. Clearly, TV can be useful for introducing people to new ideas.

> _"Educating girls is one of the most effective ways to fight poverty."_

### 8. Final summary 

The key message in this book:

**Men and women are not treated as equals. Women throughout the world face a number of serious problems, such as sex trafficking, violence, poor healthcare and poor education. If we empower women and teach men to view women as equals, there is no telling what sort of global problems these newly empowered women could resolve. We need to treat fellow humans humanely and unlock all the human potential we have on Earth.**

Actionable advice:

**Volunteer or donate.**

The next time you come across a project that takes on gender inequality, get involved, whether it's helping women in New York City or a tiny village in Sudan. You don't have to spend a lot of time or money — every bit helps. 

**Suggested** **further** **reading:** ** _I Am Malala_** **by Malala Yousafzai**

_I Am Malala_ chronicles the fascinating life of the young Pakistani girl, Malala Yousafzai. From humble beginnings in a rural village in Pakistan to speaking in front of the United Nations, Malala's moving autobiography chronicles the unbelievable transformation of a girl who stood up to the Taliban and became the youngest-ever recipient of the Nobel Peace Prize.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Nicholas D. Kristof and Sheryl WuDunn

Nicholas Kristof and Sheryl WuDunn are the first married couple to win a Pulitzer Prize in journalism, which they received for their coverage of China as correspondents for _The_ _New York Times_. Kristof also received a second Pulitzer for his reporting on the genocide in Darfur. WuDunn previously worked as a foreign correspondent in Asia, a business editor and a television anchor, and is currently a banking executive.

