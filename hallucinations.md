---
id: 54cf944f323335000a760000
slug: hallucinations-en
published_date: 2015-02-06T00:00:00.000+00:00
author: Oliver Sacks
title: Hallucinations
subtitle: None
main_color: 79F2C6
text_color: 39735E
---

# Hallucinations

_None_

**Oliver Sacks**

This book explores the complex realm of hallucinations, and explains how they happen not only to people who are ill, but also to those who are completely healthy. Drawing on various studies, patient cases and the author's own experiences, it describes the different causes and types of hallucinations, and shows that they're actually a common phenomenon that manifest in a variety of ways.

---
### 1. What’s in it for me? Discover the science behind hallucinations. 

Many of us have at one point in our lives experienced hallucinations. They're common: they can come as a result of fever, disease or intoxicating drugs. Yet, most of us are unaware of the various types.

For example, did you know that hallucinations are not only visual, but also come in the form of mystery sounds or even smells?

In these blinks, you'll find out about the many types of hallucinations and why they occur.

You'll also find out

  * why many amputees continue to feel their missing limbs;

  * how a hallucination left one person feeling like a peach; and

  * why, for some people, falling asleep can be a terrifying event.

### 2. Visual hallucinations can be caused by blindness, impaired sight or sensory deprivation. 

Have you ever seen, smelled, heard, or felt something, only to realize it was your imagination? _Hallucinations_ are perceptions that occur without a corresponding external reality. In other words, you hallucinate when you involuntarily sense something that's not really there.

People who become blind, or whose sight is otherwise impaired, often experience visual hallucinations. In fact, one study showed that out of nearly 600 elderly people with visual problems, about 90 percent experienced some form of hallucinations. Fifteen percent experienced _complex_ hallucinations, where they "saw" things like people, animals or entire scenes. Eighty percent had _simple_ hallucinations, meaning they saw shapes, colors and sometimes patterns.

When a person with partial or severe blindness experiences visual hallucinations, it's called the _Charles Bonnet Syndrome_ (CBS). Charles Bonnet, a Genevan naturalist, first described it in 1760. He observed it in his grandfather, and later, in himself.

Oliver Sacks once had a blind patient named Rosalie who had complex CBS hallucinations that played before her like a movie. She saw highly realistic people wearing colorful Eastern dress, and they walked in and out of the scene before her without addressing her in any way.

One reason blind or partially blind people experience hallucinations like this is that the hallucinations spring from the same areas of the brain as visual perception. That means they're physiologically distinct from the images we create when we deliberately imagine something.

Sensory deprivation can also cause visual hallucinations. For example, people hallucinate when they're exposed to the same monotonous scene, like darkness, for a prolonged period of time. This is often called the _prisoner's cinema_.

The prisoner's cinema is so powerful that all people seem to experience it eventually after some period of isolation. Visions can range from simple geometric patterns to complex scenes, because of the increasing excitability of the visual cortex.

> _"Though she had not seen anything at all for several years, she was now 'seeing' things, right in front of her."_

### 3. Losing your sense of smell can cause olfactory hallucinations, while auditory hallucinations can happen to anyone. 

Try to imagine what your last meal smelled like. If you're like most, it's difficult to, because it's uncommon that people can imagine smell vividly.

If a person's sense of smell becomes impaired however, it's not uncommon for them to hallucinate smells.

_Anosmia_ — the complete loss of smell — affects about five percent of people, and 10 to 20 percent of them experience olfactory hallucinations.

People who've lost a lot of their sense of smell without losing all of it also typically suffer from sensory distortions, called _dysosmia_.

Sacks once corresponded with a patient, Mary B., who suffered from dysosmia. For her, perfume, coffee and cars smelled unbearably strong. Dysosmia also distorts people's sense of taste, because taste is very dependent on smell. For Mary B., fruits and vegetables began to taste metallic and rotten.

While it's hard to imagine smells, it isn't hard to imagine sounds. Involuntary auditory hallucinations are a widespread phenomenon too, however.

Auditory hallucinations are actually common among the general population — unlike olfactory hallucinations. We used to think that hearing voices, for instance, was a symptom of schizophrenia. Actually, however, most people who hear voices _aren't_ suffering from severe mental disorders.

The most common auditory hallucination is hearing your own name. It's also not unusual to hear simple or unremarkable voices talking about everyday things. They may issue simple commands, by telling you where to put your glass down, or which horse to bet on, for example.

One potential explanation for hallucinating unremarkable voices is that we might have a "barrier" that prevents us from hearing our own internal voices. But for some, the barrier becomes breached, so they effectively "hear" their own thoughts.

### 4. Degenerative diseases can cause complex and often multi-sensory hallucinations. 

You're probably somewhat familiar with _Parkinson's_, the degenerative disease. Its most commonly known symptoms are the shake: rigid and slow movements of the people it affects.

James Parkinson first wrote about the condition in 1817. It wasn't until over 60 years later, however, that researchers discovered a key feature of the disease: over a third of people affected by it also hallucinate.

In fact, it's typical for degenerative diseases like Parkinson's to be accompanied by hallucinations. Parkinson's disease damages the brain stem. _L-dopa –_ a drug that treats it — heightens dopamine responsiveness, like LSD does. These factors can lead to complex and multi-sensory hallucinations.

Howard H., a psychotherapist who was diagnosed with Parkinson's, began to observe this in himself. He started to feel that everything was covered in a peach-like fuzz.

People who experience hallucinations like this don't always consider them bad. Agnes R., a 75-year-old who's suffered from the disease for 20 years, reports that her hallucinations are fascinating and enjoyable. She hallucinates skating rinks and tennis players on the tops of churches and rooftops she can see from her window.

Alzheimer's disease and other forms of dementia can also cause hallucinations. People who experience these hallucinations often imagine that people or things around them have been duplicated or changed in some other way.

For example, one of Sack's patients, who had Alzheimer's, suddenly began to believe that her husband was an "imposter" who was trying to take his place after murdering him. Another patient was convinced she'd been transferred to a perfect replication of her home each night. In these cases, the hallucinations are embedded in disorientation, confusion and delusions.

> _"She now receives love, attention, and invisible presents from a hallucinatory gentleman who visits faithfully each evening."_

### 5. Phantom limbs, doubles and shadows are hallucinations of the body. 

Ancient texts often refer to auditory and visual hallucinations; they're not a new discovery. It was only in 1870, however, that a neurologist began to investigate the feeling of still having a body part after it's been removed. His name was Silas Weir Mitchell, and he called the phenomenon _phantom limbs_.

This type of hallucination is common. In fact, virtually all people who undergo amputations experience it.

Phantom limbs are different from the hallucinations associated with sight or hearing impairment. They appear immediately, or shortly after an amputation.

Unlike other hallucinations, people feel that their phantom limbs are just a normal part of their body. They resemble the limb that was amputated, in both size and shape. They can also be moved voluntarily, sometimes with great refinement.

This was true of Paul Wittgenstein, a great concert pianist who lost his arm in WWI, but continued to teach with both. Sacks met with Erna Otten, one of his pupils. She said that when he played piano, his right stump was very involved. He told her to trust his hand movements, because he felt every finger on his right hand.

There are other body hallucinations, called _doubles_ or _shadows_, that are even stranger than phantom limbs. One man who had to get a parietal lobe tumor removed, awoke one night while awaiting surgery believing that someone had played a sick joke on him. He thought that someone had placed a dead, cold leg in his bed, but it was actually his own, and it was still attached to his body just fine.

Other people lose feeling or use of the left side of their body after suffering a right hemisphere stroke. Similarly, they may become convinced that the left side of their body belongs to someone else.

### 6. Drugs and delirium can induce vivid hallucinatory experiences. 

Many people take drugs to get high. There are a wide variety of intoxicants that give people more intense experiences of the present, or a sense of pleasure.

Psychedelic drugs can also include vivid hallucinations, however. These drugs may be derived from plants or synthesized in labs.

In 1938, the botanist R. E. Schultes and the chemist Albert Hofmann catalogued almost one-hundred plants that contain psychoactive substances, in their book _Plants of the Gods_. More continue to be discovered to this day.

The chemicals in these substances can directly stimulate many complex brain functions. _Mescaline_, for example, naturally occurs in some cactuses, and it often causes people to hallucinate complicated geometrical patterns.

Psilocybin is another psychedelic compound found in many different mushrooms. It causes feelings of euphoria, and complex and colorful visual hallucinations.

Sacks himself experimented with psychedelic drugs after becoming a doctor, to gain better understanding of the brain's neurological complexities. On the weekends he often took LSD — which was first synthesized by accident in 1938 by Hofmann, who wrote about his experiences.

_Delirium_, which is often caused by intoxication or fever, can also be paired with hallucinatory experiences.

People who experience delirium resulting from a high fever sometimes suffer from _Alice-in-Wonderland syndrome._ This condition causes people to think their body is shrinking and growing. They may also experience rhythmic and pulsing visual hallucinations, or imagine their body is swelling.

Delirium is also linked to alcohol toxicity or withdrawal. In those cases, people may experience vivid hallucinations of sight or sound.

> _"Finally, it seemed to me, I could see a hand stretched across the universe, light-years or parsecs in length."_

### 7. Migraines and epilepsy can cause hallucinations that derive from electric disturbances in the brain. 

Most people associate migraines with intense headaches. But did you know you can have migraines _without_ the headaches?

It's common that people see a visual _aura_ before the pain of their migraine sets in. A few lucky people, however, _only_ experience the visual elements of migraines.

_Migraine auras_ are hallucinations that result from an electrical disturbance in the visual areas of the brain. They can make the person's entire visual world completely unintelligible for a few minutes.

The visual representation of a migraine often appears in the form of a dazzlingly bright zigzag shape called a _scintillating scotoma_.

This phenomenon is considered a hallucination because it's fully generated by the brain, but it doesn't consist of complex images like most other visual hallucinations. It appears as something very simple.

Like migraines, epileptic fits also appear suddenly and are caused by neurological electrical disturbances. And like migraines, they can give rise to hallucinations.

Epilepsy typically affects parts of the brain that deal with higher functions, so the hallucinations it causes are more complex.

For example, Valeria, a 28-year-old doctor, has had epileptic seizures since she was 15. She typically sees halos around the objects in her surroundings before she loses consciousness.

Because of this, she was initially misdiagnosed with migraines. Health professionals sometimes confuse the two disorders because they have similar visual symptoms and underlying electrical disturbances.

With epilepsy however, auditory hallucinations are just as common as visual ones. Some affected people hear hissing or rustling sounds, or the beating of a drum. They can even hear more complex sounds, like music.

People with epilepsy can even hallucinate tastes and smells before their seizures. One woman said that before the onset of her seizures, she could smell peaches so vividly it was hard to accept they weren't really in the room.

### 8. Sleep disorders and in-between sleep states can cause hallucinations. 

Have you ever heard of the gruesome creature that assaults victims in their sleep? It's commonly called the _incubus_ if it's male, and the _succubus_ if it's female. People crafted these mythological demons, but the experiences associated with them can be real hallucinations caused by sleep disorders.

Sleep disorders like _narcolepsy_ can cause very disturbing hallucinations. Narcolepsy is characterized by an abrupt onset of overwhelming sleepiness and the weakening of muscles, causing sudden sleep attacks. These attacks can be followed by _paralysis_, by which the person can't move or talk and many experience vivid hallucinations.

While most people have stable sleep-wake cycles, people with narcolepsy don't. Instead, they may go through dozens of microsleeps each day, and their hallucinations are often auditory, tactile and visual.

Christina K., a correspondent of Sacks, was prone to _sleep paralysis_, which is similar. She'd lie down to sleep, and almost immediately feel her body going numb. She'd feel like someone was sitting on her back, and pressing harder and harder down on her. Finally, the thing would get off and lie there beside her breathing. When she turned to face it, she saw an eerie man in a black suit, and he'd start screaming random numbers and hysterically laughing.

_Hypnagogic hallucinations_ — involuntary images that appear before sleep — are much more common. It's estimated that the majority of people experience them at least occasionally. They mostly consist of simple imagery or sounds that appear while the person is falling asleep.

Hypnagogic images often have extraordinary colors and detail, and they transform rapidly and spontaneously. This suggests that the person's default neurological networks are running through random activity, while the brain is resting and not focused on the outside world.

### 9. Grief, traumas or other extreme emotional states may lead to hallucinations. 

Most hallucinations stem from mental or physical impairment, illness or sensory deprivation. The hallucinations themselves are usually considered meaningless, and irrelevant to the affected person's life.

Hallucinations that result from extreme emotional states are different, however. People can hallucinate when undergoing severe grief or trauma.

Grief can cause hallucinations, often in the form of compulsive, imagined returns to past experiences. Bereavement hallucinations, for instance, are deeply tied to feelings of grief.

Elizabeth J., a woman who lost her husband, wrote to Sacks about hallucinations her young son experienced. He would sometimes see his father run past their house a few months after he died. The two used to run together, and it seems his hallucinations were a neurological response to his mourning.

Such hallucinations are not uncommon. In fact, a study of 300 recent widows and widowers found that nearly half of them hallucinated their dead spouses. Moreover, the likelihood of these hallucinations occurring increased with the length of the marriage. This seems to indicate that bereavement hallucinations are tied to the bereaver's emotional needs.

It's also common for people to vividly re-experience traumatic events when suffering from _post-traumatic stress disorder_ (PTSD). Anyone who's survived a terrifying experience — like abuse, war or a natural disaster — may succumb to deep fears about safety, and they can have sudden hallucinatory flashbacks.

In these cases, the person often re-lives the original trauma in high detail. These flashbacks can be spontaneous, but they can also be provoked by smells, sounds or sights associated with the original trauma.

People suffering from PTSD may go into profound and even dangerous delusional states when they hallucinate. A traumatized veteran might suddenly see the people around him in the supermarket as enemy soldiers and want to open fire, for instance.

> _"Hallucinations of ghosts — revenant spirits of the dead — are especially associated with violent death and guilt."_

### 10. Final summary 

The key message in this book:

**Hallucinations are often caused by physical impairments, such as a loss of sight, smell or hearing. Degenerative diseases, sensory deprivation, drugs and even certain mental states can also induce them, however. Overall, hallucinations result from unusual brain activity, and though there is a wide range of their causes and effects, there's a neurological basis behind all of them.**

**Suggested further reading:** ** _Musicophilia_** **by Oliver Sacks**

_Musicophilia_ explores the enriching, healing and disturbing effects of music. It delves into fascinating case studies about disorders that are expressed, provoked and alleviated by music.
---

### Oliver Sacks

Oliver Sacks is a world-renowned neurologist, writer and professor at the _New York University School of Medicine_. He's written several successful books, including _Musicophilia: Tales of Music and the Brain_ and _The Man Who Mistook His Wife for a Hat_. His book _Awakenings_ was adopted into an Academy Award-nominated film.

