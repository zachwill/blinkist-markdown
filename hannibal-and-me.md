---
id: 54c662573732390009680000
slug: hannibal-and-me-en
published_date: 2015-01-29T00:00:00.000+00:00
author: Andreas Kluth
title: Hannibal and Me
subtitle: What History's Greatest Military Strategist Can Teach Us About Success and Failure
main_color: EB2F41
text_color: CC2939
---

# Hannibal and Me

_What History's Greatest Military Strategist Can Teach Us About Success and Failure_

**Andreas Kluth**

_Hannibal and Me_ details the famous Carthaginian general Hannibal's quest to conquer Rome. The author Andreas Kluth draws from Hannibal's biography valuable lessons on strategy, coping and finding the meaning of life.

---
### 1. What’s in it for me? Discover yourself through a brilliant ancient field commander. 

What can we learn from the life of Hannibal, the perhaps greatest military commander in history? _Hannibal and Me_ makes the case that the brilliant strategist, who won over many allies of Rome, can teach us about everything from parental influence and goal-setting to dealing with both crisis and potentially blinding successes.

In these blinks you'll learn how Hannibal's personal development and warfare tactics can be transferred to our modern life, since, ultimately, we all face similar challenges.

You'll also discover

  * how choosing the right setting can make your weakness irrelevant;

  * that Hannibal's victories made him lose sight of the big picture: capturing Rome; and

  * why a midlife crisis shouldn't necessarily be a crisis.

### 2. Your parents have a great influence on you – whether you emulate them, yearn for their affection or rebel against them. 

Do you admire your parents' accomplishments, or would you want to live a completely different life? No matter how you answer, your life is ultimately influenced by your parents.

Some wish to emulate their parents, seeing them as role models. If your parents are successful in a way you admire, it's likely that you'll feel pressure to follow suit.

Hannibal's father, for example, was a highly regarded Carthaginian general. For Hannibal, it was clear even at an early age that he should emulate — if not exceed — his father's valor. Despite all his father's successes, he was unable to defeat the Roman army in the first Punic War, and so Hannibal made it his life's mission to exceed his father's reputation by conquering Rome.

Yet, even if your parents were absent in your life, it's still common to search instead for the idea of a parent. Unfortunately, many people don't know their parents — they may be orphans or might even be abandoned by their parents. This doesn't mean, however, that their parents don't influence them.

For instance, a child might start a search for their missing parent, often knowing only their name or having just a picture to go by. The child might satisfy her expectations through this quest to find her parent, which would help in forming a stable identity, or maybe the search will continue forever, preventing the child from ever settling down. But even this restlessness is a form of parental influence.

On the other hand, children who receive too much attention from their parents tend to rebel against them. If a child becomes overwhelmed by their parents' presence, then he or she will typically rebel, perhaps by seeking out rebellious relationships or by dressing provocatively.

But even in her rebellion, the child is still influenced by the parents: her actions are a response to those of her parents, without which the rebellion would have no basis.

> _"To understand your life, you have to begin with your parents."_

### 3. Some people – heroes on a quest – know their ultimate goal early in life and some – wanderers – will find their determination later. 

Do you know your purpose in life? Congratulations! That means you've got a head start on making it come true. But don't worry if you don't. A later search for your life's purpose, as well, can open you to amazing possibilities.

For many, the quest to fulfill their life's purpose is lifelong. It starts, however, by forming a _tentative identity_ based on parents' influence. Eventually, they start looking for their own individual purpose.

This quest can take various forms: it can be something truly magnificent, such as winning the Nobel Prize, or it might be more ordinary, such as becoming an excellent craftsman or a respected member of the local community.

While this dream is often adjusted later in life, in the beginning of the journey it's simply the vision that's important.

Hannibal's lifelong dream was to conquer Rome. All his life he worked to achieve this dream, and his invasion through the Alps and his first military victories against Rome brought his dream within reach.

Hannibal had the benefit of knowing what he wanted from childhood, and that clarity of purpose gave him complete focus. His military success against the Romans was the result of this lifelong focus.

Not everyone is so lucky. _Wanderers_, in contrast with _heroes_ like Hannibal, first need to find themselves before they can find their purpose, which can actually be advantageous.

Because they don't know from the onset what they want from life, wanderers have the benefit of exploring themselves. A hero's path is determined by her quests, but wanderers have the chance to see the bigger picture. As a result, they are able to see different opportunities that might remain elusive to those with tunnel vision.

But that's not all that Hannibal's life has to offer us. In the following blinks, we'll examine the lessons of Hannibals wins and losses.

### 4. You can defeat your opponents by making your weaknesses irrelevant and undermining their strengths. 

Sometimes people stand between you and your goals. And sometimes these rivals will be stronger than you. By looking at Hannibal's life, however, we can gain insight into how we can defeat even superior enemies.

First, you must make your own weaknesses irrelevant by carefully choosing the right settings for your confrontations.

Hannibal's weakness was always having fewer soldiers than his enemies. To accommodate for this setback, he chose to fight where the Romans couldn't use their numbers to their advantage, for example, at choke points or fords where fewer soldiers could stand side by side.

People, like armies, all have individual weaknesses. An impulsive person, for example, often lacks patience. If you know this to be true of yourself, then you can choose an environment for your confrontation that makes this weakness irrelevant.

If someone tries to exploit your impatience by pestering you, don't give in and snap. Instead, find a neutral area to confront him, such as a café, perhaps with another person there, to ensure that you don't do something you'll regret later.

Second, you can undermine your opponent's strengths by attacking him in a way he doesn't expect.

For example, when Hannibal fought the battle of Cannae, his greatest victory over the Romans, he first let the Roman footmen charge to meet his army. But in the midst of the battle, when the Roman soldiers had become tired, he had his cavalry charge into the back of their army, thus beating them with their own tactic.

So how can you apply this to your life?

Imagine that a slandering colleague is always making you the subject of his gossip. He'll surely be surprised, however, if his dirty laundry suddenly becomes the talk at the water cooler! Not only will he be surprised, but he'll find out for himself that slanderous talk hurts others.

### 5. The big picture is more important than single victory. 

What is the best way to measure your success in life? Is it your career? Your bank account? Your family life? You might be successful in any one of these categories, but a truly successful life is one that is successful in terms of the "big picture."

Unfortunately, singular successes make it easy to lose sight of the big picture, despite being far more important.

In part, this is because of the human desire to be successful. When we earn lots of money, excel at a sport or craft or become respected among friends and family, it's easy to latch onto those successes and use them to measure our quality of life.

But while these singular successes are necessary in order to find fulfillment, it's important not to become shortsighted.

For instance, career-focused people might end up becoming successful managers at a big company only to suddenly realize that they're now too old to raise a family. A successful athlete might ask herself what she'll do with her life once she's no longer able to perform at her best.

Hannibal, too, felt the consequences of losing sight of the big picture. During his invasion in Italy, he never lost a single battle. Despite being outnumbered most of the time, he and his army still emerged victorious.

Nevertheless, the Romans didn't surrender. Hannibal didn't recognize that the foundation of the Roman Empire was its capital. As a consequence, he never attacked Rome directly, and was instead content with his victories on the battlefield. These victories caused him lose sight of the big picture: capturing Rome and winning the war.

> _"Success requires good strategy, and good strategy is about setting the appropriate ends and not losing sight on them."_

### 6. Success brings the dangers of hubris, distraction and captivity. 

Victory can sometimes be just as destabilizing as disaster. It can distract, restrict and even blind you if you aren't careful. Hubris can turn your greatest success into your downfall.

With success or power often comes arrogance, overconfidence, _hubris_. In fact, research shows that the feeling of power that comes with success can lead people to believe that they're invincible!

Hubris is what lead to a fall from grace for Eliot Spitzer, the once promising attorney general and governor of New York. Spitzer had built a reputation in prosecuting illegal prostitution, but during that time he himself had visited prostitutes on several occasions. He thought his power brought him certain privileges, but when the story came out, his career was over. Hubris struck him down.

Furthermore, the distractions that follow success can also lead us astray. With great success often comes enthusiastic public interest, which can hinder you in continuing to do the things that made you successful in the first place.

This was verified in an academic study examining corporate bosses who had become famous, finding that they often subsequently spent more time writing their memoirs and giving speeches while their companies began to fail!

Lastly, the most insidious side-effect of success is the sense of captivity, being imprisoned by our own imagination. Before we accomplish our goals, our minds are open to new ideas to help us on our quest. But once we've achieved success, such as the climax of a career, it's hard not to become afraid of losing what we've accomplished. This feeling can cause us to become conservative, not take risks and ultimately squander our potential future success.

For Hannibal, his undefeated track record made him afraid to lose his nimbus, and, fearing the possibility of failure if he finally laid siege to Rome, he elected to simply not do so. Indeed, his previous successes stood in the way of his ultimate goal: capturing Rome.

### 7. Be careful with enviers as they can harm you when you least expect it. 

We all feel envy, and it's a terrible feeling. Envy can even be ruinous — not just for us, but also for the people around us. This is especially true in politics, where envy has brought down many great people over the years.

Great successes always brings enviers into the arena. There are always people who are more successful in life than others, and while many accept this gracefully, others don't.

Many cultures have their own metaphors and stories that deal with envy. Scandinavian cultures, for example, have the fictional town of Jante, where the townsfolk punished anyone who dared to stand out on account of their great achievements. Australians speak of the mythical tall-poppy syndrome, which makes us want to cut down anybody who rises above the rest.

Enviers will often prove hostile. Their strategy is to try to find a successful person's weakness and then they strike when it's least expected.

When enviers can't harm successful people directly, they typically turn to indirect methods, doing things like destroying their good reputations and collaborating with other enviers. These clandestine attacks often come as a nasty surprise for the objects of their envy.

Hannibal's biography offers an example of the lengths enviers will go to squash success:

While Hannibal was never beaten in Italy, he was defeated at his home at Carthage, where he attempted to defend against the greatest Roman general, Scipio.

After having returned home to Rome, some Roman senators became afraid that Scipio would use his reputation to become a Roman dictator. However, because they couldn't attack him directly, they instead accused him of corruption.

Although they were unable to prove corruption, the political pressure on Scipio eventually became too great, and Scipio chose to go into voluntary exile. With Rome's greatest general's political career destroyed, his enviers won that particular battle.

### 8. The only way to overcome grief is to go through the entire grief cycle. 

Have you ever lost a loved one? If you have, then you know that while grief is important, it's also important to move on.

People typically grieve very similarly, following a pattern called the _grief cycle_. It consists of denial, anger, bargaining, depression and finally acceptance of their loss.

The Swiss doctor Elisabeth Kübler-Ross found out that people, whether they've lost their job or have been diagnosed with cancer, go through a similar emotional journey after receiving bad news.

Upon learning the bad news, we first try to hold onto a fallacious, yet more acceptable reality, choosing denial over acceptance. In the second stage, we come to accept this reality, but become full of anger. We blame others, both people and divine beings, for the alleged injustice.

Once we become exhausted by our anger, we try bargaining. For example, religious people might pray to God to help them and in return promise to be better people. Next, we finally come to understand that our loss is irreversible, but, unable to move on yet, we succumb to depression.

It is only in the final stage that we fully accept our loss and are ready to move on.

The Romans went through all the stages of grief after Hannibal racked up one crushing victory after another in his battles against the Roman army:

When the first news about his victories arrived in Rome, people simply couldn't believe it. They became angry, and raised yet another army to defeat Hannibal. Then they bargained with their gods to help them and at some point began to feel completely helpless. After cycling through grief, they accepted their losses and moved on, continuing to fight Hannibal.

### 9. To succeed in your midlife transition, you have to leave behind the person of your youth. 

At some point, we all go through a midlife crisis. It is how we handle this crisis that determines whether we'll be happy in our old age.

Many people undergo a major life transition in their forties. According to psychologist Carl Jung, all stages of life are important in our personal development, but the years around age forty are especially crucial to our fulfillment.

Today we call this transition the "midlife crisis," but according to Jung, it wasn't necessarily a time of pain. Instead, if the journey during these years is healthy, it can lead to lifelong satisfaction! It's when the journey leads astray, however, that it fills us with discontent.

How can we ensure that our midlife transitions will be successful?

Successful transitions can only be achieved when we abandon the persons of our youth.

In our youth, we often try to live up to the expectations of our social environments, such as those of our teachers, parents and friends.

Hannibal's father, for example, was a great general, whose biggest enemy was Rome. Hannibal's campaign against Rome was really nothing more than the realization of the expectations his father had placed on him.

In a midlife transition, you have the opportunity to abandon identity of your youth and forge a new one — one that is more authentic and natural.

Not everyone succeeds in this transition, however, because leaving your old life behind can be difficult.

Hannibal, too, failed to drop the person of his youth. After his campaign against Rome failed, he could have lived a content life as a respected citizen of Carthage. Instead, he left his home and traveled to Asia Minor in search of other enemies of the Roman Empire to ally against Rome. Throughout the rest of his life, he remained the person of his youth: the general who wanted to conquer Rome.

### 10. The ultimate goal is self-actualization, and history leaves us wondering if Hannibal achieved it. 

What do president Abraham Lincoln and physicist Albert Einstein have in common? According to American psychologist Abraham Maslow, they both achieved the greatest success in life: _self-actualization_, the fulfillment of our human potential.

For Maslow, self-actualization describes the ultimate success in life. According to him, everyone has something at which they excel, and that something should be one's profession.

Maslow said, "A musician must make music; an artist must paint; a poet must write; if he is to be ultimately happy. What a man _can_ be, he _must_ be."

But you can't achieve self-actualization if you only live the life your social environment has prescribed for you — you have to live the life that _you_ want to live.

According to Maslow, however, this is only possible once your basic needs have been fulfilled.

You must first satisfy the physiological and safety needs that are required for survival — you need things like food, water, sleep, shelter and physical safety before you can even think about self-actualization.

With these basic needs satisfied, we then yearn for love and a sense of belonging, as well as friends and family. If we have these, then we desire esteem, from both others as well as ourselves.

Only when all these fundamental needs are fulfilled do we start to set our sights on something bigger: the fulfillment of our human potential, or self-actualization.

Maslow considered this kind of success to be exceedingly rare, so rare in fact that he estimated that, in all of human history, only two percent of the human population have achieved it.

History can't reveal whether Hannibal achieved self-actualization, because it can't tell us what Hannibal thought about his life. And ultimately, that's what's most important: self-actualization is measured by your own impression of your life.

> _"Don't agonize about success or failure. Just do what you must do as well as you possibly can."_

### 11. Final summary 

The key message in this book:

**Our lives all follow similar arcs, and that of Hannibal, the conquering Carthaginian general of antiquity, was no exception. Hannibal's life, from his childhood motivations to his ultimate goal of overtaking Rome offer insight into how to know and achieve our greatest purposes.**

**Suggested further reading:** ** _The Art of War_** **__****by Sun Tzu**

Thousands of years old, _The Art of War_ is a Chinese military treatise that is considered the definitive work of military tactics and strategy. It has greatly influenced military tactics as well as business and legal strategy in both the East and West. Leaders such as general Douglas MacArthur and Mao Zedong have drawn inspiration from it.
---

### Andreas Kluth

Kluth is a German-American journalist and author, who worked at various posts with the magazine _The Economist_ since 1997.

