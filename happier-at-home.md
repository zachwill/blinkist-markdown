---
id: 56b8e9a7d119f900070000cd
slug: happier-at-home-en
published_date: 2016-02-12T00:00:00.000+00:00
author: Gretchen Rubin
title: Happier at Home
subtitle: Kiss More, Jump More, Abandon Self-Control, and My Other Experiments in Everyday Life
main_color: 69C7E0
text_color: 3D7482
---

# Happier at Home

_Kiss More, Jump More, Abandon Self-Control, and My Other Experiments in Everyday Life_

**Gretchen Rubin**

_Happier at Home_ (2012) is a guide to transforming your home into a sanctuary that reflects your family's personality. By helping you identify both your and your family's needs, this book gives you everything you need to start changing your home and family life for the better.

---
### 1. What’s in it for me? Find happiness in small things. 

Many people think that the road to happiness is paved with hardship. They think they'll need to make some dramatic, scary life changes — quitting their job, for instance, or being more extroverted and practicing positive thinking 24/7.

Well, they are wrong. 

If you're searching for a way to enjoy more ease and comfort, more cheerfulness and joy in your life, here's the good news: tiny, seemingly negligible changes to your home, your lifestyle and the way you talk to your loved ones can add up to you being a much happier person.

Reading these blinks, you'll discover

  * why your nose can reduce your anxiety;

  * about a family with climbing ropes in their hallway; and

  * how being more snail-like will help you live more happily.

### 2. The happier-at-home project is about finding happiness in your daily routines. 

Get ready to start a nine-month project of happiness!

Each month, you'll focus on a different principle that can make your life at home more enjoyable. If you stick to it, this nine-month plan will have you enjoying your everyday routines and cherishing nearly every moment. Sound good?

Then let's get started!

Creating a happier family is all about adapting your home to both your needs and the needs of each and every family member living there. For example, one particularly creative family with four children installed climbing ropes and monkey bars in their hallway. This made being at home all the more enjoyable — for both the kids and the parents!

But before you can create a happier home, you need to figure out what needs changing. There are four things that will help you do that:

First, you have to find out what makes you feel good at home. Is it the pictures or trinkets that remind you of your family? The smell? The lighting?

Next, figure out the primary factors that make you feel bad at home. It could be anything, from major disturbances to a scary painting in your bedroom that keeps you awake at night.

Third, ask yourself how you can _feel right_ more often — that is, how you can develop a lifestyle, home and routine that reflect your values and align with what's important to you.

Finally, search for ways to develop an _atmosphere of growth_ at home, an approach to life that keeps you focused on maintaining a go-get-'em attitude even after you've accomplished a goal. For instance, if something breaks in your home, you shouldn't just fix it, but also adjust your behavior so that the problem doesn't arise over and over again. 

Now that you understand the fundamental goals of the nine-month project, our remaining blinks will explore exactly which actions you should take during that time.

### 3. Getting rid of the unnecessary opens up space for the things that truly matter. 

Is your home full of stuff that you keep around simply because it _might_ come in handy one day? If you want a happy home, you might want to change that.

To create a home that offers happiness and comfort, you should surround yourself with possessions that you truly know and cherish. Examine every object in every closet and every drawer and ask yourself: How often do I use this, and how much do I like it? If you don't use or like it, get rid of it.

Then get familiar with the things you decide to keep. For example, you should actually read the instruction manual for your coffeemaker or for your video camera (if you decide to keep these manuals). By familiarizing yourself with everything in your home, you'll be both competent and comfortable dealing with (basically) any situation within it. 

In addition, make sure that the things that matter most to you are in full view. The author, for example, transformed her home into a shrine to her family, displaying photographs of her loved ones throughout the house. 

Decluttering your home isn't just a way of improving your living space; it can also improve your family life. After a thorough decluttering, you'll suddenly have more time to think clearly about what's good for your family.

Had the author constantly felt pressured to fix things around the home and clean up clutter, she probably would have forced her eldest daughter to take piano lessons just to get some time to herself.

But she decluttered her life, getting rid of the things she didn't need, and she had enough spare energy to address her daughter's needs and realize that what her daughter really wanted was to make films and edit videos all day long. So, she gave her a book about film editing.

> _"We often don't spend enough time and attention thinking about how possessions could boost happiness..."_

### 4. If you want a happy family, you have to be the change you want to see in your loved ones. 

Having a happy relationship with your loved ones means being happy yourself.

Indeed, achieving a happy marriage is mostly a matter of understanding that nobody but _you_ is responsible for your happiness. So, if you want a peaceful and relaxed marriage, try to be peaceful and relaxed yourself. Over time, partners will influence each other, copying each other's language and habits. For example, if you quit smoking, your partner is 67 percent more likely to follow suit.

Similarly, if you want a caring and affectionate relationship, be caring and affectionate yourself. Try kissing your partner more often, as kissing boosts feelings of intimacy, and at times says more than a thousand words.

For example, when the author's husband seemed preoccupied and distracted one evening, she gave him a long kiss instead of simply asking what was wrong. That was all he needed to cheer up and relax.

These same principles apply to the relationship you have with your children. Their happiness and self-esteem, and their behavior toward you and others, is bound to reflect the way you behave around them. 

Rather than being a dramatic parent who makes a fuss about every little mistake, try to reassure your kid by _underreacting_ to problems.

As an example, when her eldest daughter dropped an open bottle of nail polish on the carpet, the author didn't yell at her and make a mountain out of a molehill. Instead, she calmly told her daughter to search the internet for ways to remove the stain, and to clean it accordingly. In the end, the stain was gone, nobody was upset and her daughter even learned a new skill.

Finally, it's absolutely vital that you pay attention to each of your loved ones individually. One simple way to do this is acknowledging every family member whenever they leave or return home with a warm greeting.

### 5. Happiness comes from the inside. 

As you've learned, the less cluttered your home is, the happier you'll be. But it's also true that your general happiness can generate domestic happiness. So what else can you do to increase your personal feelings of joy?

You can start by protecting yourself against unhappy people; such individuals can drain all the happiness out of you.

Unlike happy people, who are enthusiastic, optimistic and full of energy, unhappy people are apathetic, plaintive and intent on making everyone in their life as miserable as they are.

There are three types of _happiness leeches_ : the needy and pessimistic _grouches_, the _jerks_ who gossip and embarrass others and the _slackers_, who use feigned helplessness to get attention.

Protecting yourself against happiness leeches of all varieties starts with the awareness that, while happiness is contagious, some people are simply immune to it. It's impossible to cheer up a bona fide grouch, so don't waste your time trying. 

Furthermore, you can become happier if you learn to be mindful of the present moment by tuning in to your senses. A particular sense of smell, for example, can bring back pleasant memories, while also reconnecting you with what's there right now. 

For instance, if you ever feel yourself being overcome with anxiety, try sniffing lavender oil; this can bring you back to the (unthreatening) present moment.

Third, if you want to feel good, you need to look good — and knowing yourself can help you look your best. 

There are countless temptations, like indulging in unhealthy foods or smoking cigarettes. To resist these temptations, it helps to know which strategies are easiest for you to follow and then to act accordingly. Maybe you need to avoid temptation altogether (never eating those delicious Christmas cookies) or enjoy them in moderation (eating just one cookie at a time).

> _"Home is a state of mind, but it's also a physical experience."_

### 6. Broadening your familial circle can also improve your happiness. 

Family is something that you can never have too much of. And although your relationship with your spouse and children is what matters most, that doesn't mean you have to abandon the family you grew up with. 

Your parents and siblings are part of who you are, and connecting with them more often can improve your overall happiness. The author, for example, started working on a children's book with her sister. Despite all the time and effort they had to devote to the project, they enjoyed the extra time they spent in each other's company, and the project ultimately brought them closer together.

Appreciation for your family is a wonderful feeling, and practicing gratitude can help you feel even closer to them.

So develop habits that remind you of how grateful you are for your family. For instance, whenever you lie awake at night and everyone else is asleep, remind yourself of how grateful you are to be at home, surrounded by the people you cherish most.

Moreover, getting to know your neighborhood can broaden the feeling of having a big and happy family.

Deepening your connection with your neighborhood will make you feel even more at home. Start with the places and buildings. For example, you could take a walk through your neighborhood and try to learn more about the landmarks in your area. 

While you're out, try to remember personal events that are connected to specific places in your neighborhood, like how you and your family once took a special walk down a certain street.

But your neighborhood isn't just a collection of streets and buildings. Get to know your neighbors, too, and treat them as if they were your family. Be considerate and affectionate, and help them out whenever you can. Remember: the bigger your family, the happier you'll be.

> _"Gratitude is the key to a happy life."_

### 7. The happiest home is a state of mind. 

Snails may be slimy and generally pretty gross, but they do have one thing going for them: they get to bring their homes with them wherever they go. With the right perspective, you can do the same.

If you truly know yourself, you can feel at home no matter where you are.

In order to create a happier home, your private space must reflect the real you. But discovering who the "real you" is can be a difficult and even painful exercise, as it requires you to confront your own faults. 

For instance, you may discover that, despite what you keep telling yourself, you actually dread family trips because you hate being away from home and sleeping in an unfamiliar bed. But both you and your family expect you to love these special outings!

If your home is a reflection of who you are, then everything you find within your space can also be found in _you_ as well. If your home is warm and welcoming, then that means you are, too. 

In this way, it's possible for you to carry the things you cherish most about your home — the memories and warmth and comfort — wherever you go. This way, you can feel at home anywhere.

So when's the best time to start your quest to feel happy at home, no matter where you are? Right now!

If you're like most people, you see happiness as something that you will strive for in the future. In fact, according to one study, when people were asked how they'd be in ten years, 95 percent expected their lives to be better than they are in the present.

But it's in your best interest not to postpone happiness. If you wait around too long, you might not ever have the chance to do what really makes you happy.

> _"Happiness is a state of mind and the moment to experience it is right now."_

### 8. Final summary 

The key message in this book:

**Finding happiness in your everyday life requires you to develop a home that actually reflects your needs and the needs of your family. With a little soul searching and a few practical exercises, you can transform yourself and your home into generators of real happiness.**

Actionable advice:

**Always take the atmosphere of your home into consideration.**

Next time you buying something for your home, ask yourself whether you really need it and whether your family will feel comfortable with it, or whether it will make you feel disconnected from your own values. This simple exercise will keep your home free of clutter, and also ensure a relaxing and comfortable atmosphere. 

**Suggested** **further** **reading:** ** _The Happiness Project_** **by Gretchen Rubin**

What is happiness and how can we bring more of it into our lives? Gretchen Rubin asked herself this question because although she fulfilled all the prerequisites for a happy life — an intact family, a good job and enough money for a rainy day — she found herself frequently unhappy. During her year-long _Happiness Project_, she read about various techniques and theories on increasing happiness and tried to become happier with their help.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Gretchen Rubin

Gretchen Rubin is a highly acclaimed author and the host of _Happier with Gretchen Rubin_, a podcast about positive habits and happiness. Her books include _Better than Before_ and _The Happiest Project._

