---
id: 58f4a77c1507cf00046b996c
slug: happier-en
published_date: 2017-04-21T00:00:00.000+00:00
author: Tal Ben-Shahar, Ph.D.
title: Happier
subtitle: Can You Learn to be Happy?
main_color: E9E748
text_color: 6B6A21
---

# Happier

_Can You Learn to be Happy?_

**Tal Ben-Shahar, Ph.D.**

_Happier_ (2008) is a guide to living a more meaningful and pleasurable existence. These blinks will challenge you to identify what matters most to you and to reflect on how you can make that a bigger part of your life.

---
### 1. What’s in it for me? Walk the path to happiness. 

Most people in the West have never had it better. We're better fed, better paid, and, courtesy of the internet, better informed than any previous generation. Despite this wealth and comfort, we lack something very important: happiness.

Across the Western world, scores of people are unhappy with their lives. Many search long and hard for a solution to this lack of fulfillment — and many never find it.

This is where these blinks come in. They explain what exactly happiness is and how we can all go about finding it. If you are unhappy with your situation, read on to discover how you can shift your thinking and find an answer to your problems.

In these blinks, you'll discover

  * why you should study like you make love;

  * why an emotionless life would be pretty dull; and

  * why "meaning" is essential for a happy life.

### 2. Happiness is the utmost human goal and a strong contributor to success. 

Most people have heard the classic mantra coined by the reggae sensation Bobby McFerrin: "Don't worry, be happy." But have you ever considered why people hold happiness in such high esteem?

For starters, most people consider happiness a valid life goal. If someone asks you why you're doing something, and you answer that it makes you happy, your answer is valid in and of itself and can't be challenged.

All other responses, whether they're related to fame, money, power, respect or any number of other things, are secondary to happiness, which is the ultimate goal. If you were to say that you were doing something for money, someone might ask, "Why do you need money?"

In other words, regardless of whether people are working towards money or any other external goal, in the end, these aims are merely steps on the path to happiness. Just consider someone who wants money to afford more surfing vacations and a great underwater camera. For him, money is simply a tool to get the things that will make him happy.

There's nothing new about viewing happiness as the ultimate goal for humans. It has been the focus of philosophers throughout the centuries. David Hume, the eighteenth-century British philosopher, said that all human pursuits from the arts to science to law exist solely to enable people to achieve happiness.

Not just that, but happiness and success are also deeply intertwined. Research on well-being conducted by psychologists Sonja Lyubomirsky, Ed Diener and Laura King has even found that happy people do better in different areas of life and work like relationships, health and finance.

How come?

It might be because people with a positive outlook see the world as an inherently positive place, which makes them open to good things happening. So, whether you see happiness as an end in itself or a step along the path to success, there's no doubt that it's worth pursuing.

> _"Happiness is the meaning and the purpose of life, the whole aim and end of human existence." Aristotle_

### 3. Pleasure and meaning are central to living a happy and fulfilled life. 

So, happiness is essential, but what exactly is it?

It stems from two factors: letting in positive emotions and seeing life as purposeful. Or to put it differently, _pleasure_ and _meaning_.

Pleasure is the emotion that underlies motivation and therefore the pursuit of happiness. Without emotions, we would never have the drive to do much of anything at all. Simply put, emotions spur us on.

That makes perfect sense since the word "emotion" is a combination of the prefix _e-_, meaning "away," and the Latin _movere_, meaning "to move." In this sense, emotions break us away from our standstills, moving us in new directions and prompting us to take action.

Just consider jealousy, a powerful emotion that pushes people to do better than those around them. Or exhilaration, which makes us try things that provide a rush.

And of all the human emotions, psychologist Nathaniel Branden singles out pleasure as the most necessary aspect of a satisfactory life. After all, people whose lives feature no pleasant moments don't have the motivation they need to achieve happiness.

But pleasure isn't all that's necessary. A fulfilling life depends on integrating pleasure with meaning to find true happiness. Taking ecstasy might be an extremely pleasurable experience, but it can't produce happiness without meaning to make it fulfilling.

So to be truly happy, we must pursue experiences that not only trigger pleasurable emotions but are also meaningful and personal. No matter how pure our goals, only when they meet these criteria will they actually produce happiness.

Just consider a banker who feels fulfilled in her career and happy knowing why she was motivated to get into the field. She might have a more meaningful and spiritually fulfilling life than a priest who joined the church only because of parental pressure.

### 4. Happiness requires a balanced approach to the present and future. 

By now you probably understand that you should be striving for happiness, but that's easier said than done. To be truly happy, you'll need to assess both your present life and what's to come.

Interestingly enough, people weigh the importance of the present and the future differently, and these variations break down into four types.

The first group contains hedonistic people whose sole focus is on maximizing pleasure in the current moment without any regard for the future.

Then there are people with no interest in life whatsoever — either now or in the future. These people are nihilists.

The third group is composed of people who live for the future. They compete in the rat race of life, working hard and suffering in the hope of better days to come.

And finally, there are people who enjoy the present, understanding that their current activity will also benefit them in the future.

This last group of people is the happiest, but each person is actually a mix of all four. The goal is to endeavor to be more like the fourth type.

This balance is crucial because those who work solely toward future happiness rarely find it. That being said, society tends to reward those who try to achieve happiness down the line by enduring suffering today. These people can be described as having a "no pain, no gain" philosophy.

Just take school children. They're encouraged to struggle and work hard, no matter how miserable it makes them because earning good grades will make them happier later in life.

The problem is that this future happiness is perpetually postponed. When a student earns the high marks she needs to attend college, she's told to work hard to earn a degree that will land her a great job.

Then, once she gets hired, she's told to climb the corporate ladder. As a result, she never gets to enjoy the fruits of her labor. So, instead of putting off happiness in the present to be happy in the future, set appropriate goals to make sure you can have both.

### 5. Happiness comes from a specific purpose and appropriate goals. 

Earlier you learned that having a purpose in life requires meaning. But fulfilling such a purpose can actually be quite a challenge. Say your purpose is to improve the world. Where do you begin?

Well, to make sure that you're advancing your purpose while making yourself happy, it's important to set future goals that are in line with the principles of meaning and pleasure. Basically, this means having goals that bring you closer to your future dream while ensuring you a happy, pleasant life in the current moment.

To find such a path it's essential to focus on _self-concordant goals_. These are targets that you choose personally for yourself, not ones that are imposed by others. They should be a product of your will to express yourself and your true desires rather than a means of impressing other people.

Such goals can only be found close to your heart, which is why it's important to take a moment to reflect on the aspects of life that give you the most pleasure. Try to identify both long- and short-term goals that will guide you along your path.

Say you're set on helping animals. It's a great calling and a long-term ambition. Once you've decided on this purpose, you can divide it into short-term, specific actions, like volunteering at the local animal shelter and participating in animal rights protests.

Finally, to be sure that you're moving forward, add deadlines to your short-term goals. It might help to divide them up into three months, one year and five years, giving yourself plenty of room to build toward your long-term goal.

Now that you've got a sense of how to build happiness in your life at large, in the next blinks you'll learn how to make yourself happier in your education, work and relationships.

> _"What lies behind us and what lies before us are tiny matters compared to what lies within us."_ _Ralph Waldo Emerson_

### 6. Enjoy school by focusing on what you love. 

Did you take pleasure in all those caffeine-fueled cram sessions in college the night before big exams? Probably not, but it doesn't have to be that way.

When learning new things, finding pleasure and meaning is the best way to feel fulfilled. For students, that means first reflecting on how happy your chosen field makes you. Once you've done that, you can identify your true self-interests and pick subjects that really bring you joy.

It won't just make school more enjoyable; it will boost your motivation too. You'll find that studying becomes more pleasant and you'll see academic challenges as an important step on your journey.

You can think about the stages of knowledge acquisition through the lens of lovemaking. You want the _foreplay_, like reading, researching and writing, to be pleasurable. And you know that it will all be in the service of a _climax_ when you have mastered the deepest intricacies of your field.

If you want to learn Japanese, find a teacher who employs creative methods. You'll have a great time studying, and the climax will come when you're completely fluent, communicating without any effort at all.

Another tool to increase your overall learning success is striving for a state of _flow_. This concept was most famously employed by the psychologist Mihaly Csikszentmihalyi. Flow occurs when you become so engrossed in something you love that you're entirely immersed in the process.

When you're in a state of flow, you don't feel the anxiety and stress that learning can trigger. Instead, you just act — almost without thinking.

But to achieve such a state you need to avoid pushing yourself to too high a level before you're ready. By remaining at a level you can enjoy, you can let flow happen naturally.

> _"To different minds, the same world is a hell, and a heaven."_ Ralph Waldo Emerson

### 7. Finding a career that matches your passions will make you happy. 

Many people are so discontented with their jobs that you can hear them audibly moaning in the office. It makes you wonder, is it worth taking the time to identify what you actually want to do all day?

Well, one thing is certain, if your work is in line with your calling, your life will be much happier.

Just take the psychologist Amy Wrzesniewski, who discovered that people who view their jobs as their vocation see work as a reward in itself. These people care about money, but it's not why they show up every morning. They do their jobs because they love them.

It makes perfect sense that such people would be happier than those who work for external motivators like money, promotions and fame. But to find your calling, you'll need to commit to the search. After all, education systems and professional placement tests often fail at this task by encouraging people to do what they're good at, not what moves them.

To overcome these societal pressures and identify your calling, try using the _Meaning, Pleasure and Strength Test_ or _MPS_. Begin by writing three lists:

First, answer the question, _what do I find meaningful?_ You might answer: writing, music, working with kids, solving problems.

Second: _where do I find pleasure?_ It could be horseback riding, reading, music and time with children.

And finally, ask yourself, _where are my strengths?_ You might say empathy, problem solving and getting along with kids.

The next step is to notice the overlaps. From the answers above it's clear that working with kids would be both pleasurable and meaningful for you.

Then consider other areas of your life. It might be that you're highly organized, enjoy planning and love to travel. Having added this new information to the lists, you might decide to become a music teacher.

While it may not be the most financially lucrative career choice, you can rest easy knowing it's the choice that will make you the happiest.

### 8. The more effort you put into cultivating meaningful relationships in your life, the happier you’ll be. 

Imagine you get home from a long day at work. You're absolutely exhausted, and your best friend invites you out for a night on the town. Despite being tired, you say yes, and by the end of the evening, you actually feel energized.

It may seem contradictory to get an energy boost from such an outing, but dedicating time to deep social relationships will boost your long-term happiness. Just consider the positive psychologists, Ed Diener and Martin Seligman, who looked at the differences between "very happy people" and those who were less satisfied.

The only significant difference between the groups was that those who saw their lives as happier had strong circles of family, friends and/or romantic partners. So, while spending time with the special people in your life may not be enough to make you happy, it certainly plays a major role.

After all, by sharing your life with others, you invite them to share their lives with you. You open your thoughts, feelings and experiences to the people around you. Such exchanges increase the meaning of life as sharing joy is a pleasurable experience, and sharing sorrow can be comforting during painful times.

Friends are great for that, but stable and fulfilling romantic relationships also have a profound impact on happiness levels. While researching human well-being, the psychology professor David Myers found that a deep, caring and intimate lifelong relationship is one of the best predictors of happiness.

This correlation is only logical because when people love each other unconditionally and feel loved for being exactly who they are, they're free to express their true selves. But to be successful, romantic relationships need to hold both pleasure and meaning.

A hedonistic relationship that's solely focused on pleasure and lust will rapidly lose its meaning, while a relationship based on shared values but lacking pleasure will also struggle. Balance is key.

### 9. Final summary 

The key message in this book:

**Happiness is the greatest human goal, and anyone can find it by identifying what really matters in their life. Such clarity can be used to create a life that's full of pleasure and purpose, leading you to accomplish your ultimate wishes.**

Actionable Advice

**Boost Your Happiness Level**

Write a list of things that bring you joy. These are your _happiness boosters:_ a _general booster_ brings you instant happiness while an _explanatory booster_ is something you think about but aren't sure will make you happier — like volunteering at a dog shelter or learning an instrument. Each month, choose two to three boosters to focus on and work hard to take them seriously by creating rituals and regular habits.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Flourish_** **by Martin E.P. Seligman**

_Flourish_ (2012) reveals how optimism, motivation and character have the power to help us lead better lives. Based on extensive academic research and complemented by interactive exercises, these blinks introduce a new theory about what it means to live a good life.
---

### Tal Ben-Shahar, Ph.D.

Tal Ben-Shahar is an American and Israeli writer and university lecturer. His courses in Positive Psychology and The Psychology of Leadership were the most popular in the history of Harvard University. Among other books, he's also the author of _Being Happy: You Don't Have to Be Perfect to Lead a Richer, Happier Life._

