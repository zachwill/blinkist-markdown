---
id: 548f05306532350009060100
slug: happiness-by-design-en
published_date: 2014-12-31T00:00:00.000+00:00
author: Paul Dolan
title: Happiness By Design
subtitle: Change What You Do, Not How You Think / Finding Pleasure And Purpose In Everyday Life
main_color: 69A8DE
text_color: 456E91
---

# Happiness By Design

_Change What You Do, Not How You Think / Finding Pleasure And Purpose In Everyday Life_

**Paul Dolan**

Drawing from his own behavioral research and economist experience, Dolan explains how happiness arises and how we experience it in our everyday lives. He exposes some of the traps we fall into when trying to be happier and demonstrates some simple tools for adapting your environment to feel happier without having to radically change who you are and what you think.

---
### 1. What’s in it for me? Learn how to overcome the barriers to happiness. 

We all want to be happier. That's why we spend millions on books, videos and courses, which teach us how to be happy. Yet despite this, many of us spend large chunks of our life in the doldrums. We can't seem to make ourselves happy all the time.

These blinks, however, are different. They show how our brain and our environment unconsciously affect our happiness levels, and not necessarily in a positive direction. Crucially they also show how we can manage these factors to ensure we remain happy in our lives.

In these blinks you will discover

  * why going to gym can lead you to eat unhealthily

  * why hearing French music makes you buy French wine

  * why happiness begins with switching your phone off.

### 2. You are happiest when you experience your daily activities as pleasant and purposeful. 

Imagine being forced to spend the rest of your life watching your favorite comedy show. Would you really be happy? Chances are that eventually you'll feel there's not much point to your life. In the end, you might as well have spent your whole life working instead.

But why is this exactly? Well, happiness isn't just caused by joy, but by feeling that what you're doing has some purpose. In order to be happy, you need to experience a combination of joy as well as meaning in your activities.

Every day, your life consists of a mix of activities that you tend to regard as pleasurable or purposeful. For example, catching up on your favorite TV show is pleasurable, while creating a presentation for your work is purposeful. 

In order to get the most out of your day, it's best to alternate pleasurable activities with purposeful ones. For instance, at work, you can consciously integrate "pleasure" breaks into your day, like having a nice lunch and a chat with a colleague, or taking a short afternoon walk.

Interestingly, whether you prefer to have more pleasure or more purpose in your daily life largely depends on your personality.

There is no one-size-fits-all, so find the mix of work and play that suits you best. You might be more of a "_pleasure machine,_ " requiring more fun to be happy , or you could be a "_purpose engine_ " valuing your work more than your downtime.

Lastly, happiness is about feeling good in the moment. Being merely satisfied with your life situation may not indicate that you are happy every day, as happiness is more about spontaneous emotion than contemplating your general situation.

To feel happier, then, you should focus on actively experiencing pleasure and purpose in your daily activities.

In the next blinks, we'll see why it's also vitally important to focus consciously on these activities.

> _"You are happiest when you have a balance between pleasure and purpose that works best for you."_

### 3. Happiness isn’t caused directly by what you do, but rather by what you pay attention to. 

A meaningful job, exciting hobbies, optimal health: you would think these are the perfect ingredients for happiness, wouldn't you? Sure, but only if you actually _pay attention_ to them.

In order to live a happy life, you need to be able to concentrate on what makes you happy.

Focusing on what makes us happy, though, is often easier said than done.

While we may like to feel happy, each one of us can only think of one thing at a time because our attention is a limited resource. Unfortunately, we often dwell on negative thoughts about past and future events rather than positive thoughts about the activity we're doing in the moment.

As you go about your day, it's normal to not think about what you're doing in the moment. We often let our minds drift to doubts and worries. We've all had the experience sitting at dinner with friends yet our minds are stewing over the next work day.

We tend to give more attention to unfamiliarities and novelties rather than routines in our lives and, unfortunately, we forget to cherish the good things we do on a daily basis. The first few times you take a ride in your new car might feel really exciting, but soon the novelty fades and driving it becomes routine. Now you're focused on that horrible coffee stain on the driver seat.

Feeling happy, then, is not so much about changing what you do; it's about what you give your attention to. This might seem a bit tricky to control, but it's absolutely achievable.

First, recognize the typical attentional "mistakes" you unconsciously make and you'll be able to figure out some personal strategies to stop making them.

Let's take a look in the next blink at what these mistakes are.

> _"The key to being happier is to pay more attention to what makes you happy and less attention to what does not."_

### 4. Your behavior and attention are largely driven by unconscious mechanisms that are prone to mistakes. 

We saw in the previous blink that we often drift towards things in our life that make us feel unhappy. Why? It's because the majority of what we do is lead by unconscious processes.

Your brain has two systems that process information and determine how you should behave. _System 2_ is the conscious brain you use to make considered, well-informed decisions. _System 1_, however, is far more influential. It's the ancient, primitive part of our brain that is governed by instinct and operates on impulse and habit, which enables us to act automatically. This is often useful when it comes to always knowing where your car keys are because you always put them in a certain place, but it can also lead to detrimental decisions.

Our focus often wanders onto negative things because our System 1 reacts to our surroundings, which leads to knee-jerk decisions. Without consciously knowing it, what we think and how we behave is changed by our environment.

In one study on the sales of French and German wines, customers were exposed to French accordion music playing in the background whilst they shopped. As a result, 70 percent of the sales of French wines over German wines were accounted for by the French music, yet only 14 percent of the customers reported being aware of it.

The problem is that instinctive and impulsive decisions are catalyzed by our environments, sometimes leading us to behave in ways that actually work against what we need to be happy.

For example, you might scarf down a giant candy bar because it was convenient to grab at the cash register, despite that you had decided to start eating more healthily. So, having given in to temptation, you feel guilty and unhappy.

### 5. In trying to achieve goals that will make you happy, you fall victim to behavioral spillover. 

Say you think that getting physically fit will make you happy, so you start going to the gym. Then, driving home from your first exhausting workout, you pass your favorite fast food restaurant. Instead of resisting, you impulsively "reward" yourself with a big, greasy burger and some fries.

Why? Because our past, present and future decisions are not separate; they impact each other, and we are sometimes oblivious to it.

Often, different behaviors are connected by the same motivation (to lose a few pounds, for example), and your success or failure in acting out one behavior effects your future behavior; this effect is called _behavioral spillover._ Trouble is, these behaviors are largely attributable to your impulsive System 1, so you often aren't aware of how it works until it's too late.

Behavioral spillover can be advantageous and help you reach your goals. One positive behavior can encourage another positive one, or you may act positively because you want to make up for behaving negatively before.

For example, after giving the bathroom a thorough clean, you suddenly decide to tackle the kitchen, too. Or, after gorging yourself on rich food at that restaurant, you decide to walk home to burn some calories.

Unfortunately, behavioral spillovers can also make it difficult to act the way we consciously want to, which typically leads to guilt and frustration. Like the example at the start of this blink, you may allow yourself a bad behavior (consuming a large burger and fries) because of your previous good behavior (working out at the gym).

Even worse, one negative decision may lead to another. We've all experienced that after clicking around on the internet all day, you give up on eating a healthy dinner and order takeout, defiantly claiming, "There's no point now anyway, I'm having a lazy day!"

But to really make yourself happy, be aware of negative behavioral spillovers and see the gain in the positive ones.

> _"You need to consider not only what you do or feel but also what effect your current actions and feelings might have on what you do and feel next."_

### 6. You often misjudge your desires and goals, including how they impact your happiness. 

The prospect of being happier is the drive behind a lot of your decisions and actions. You may strive towards a particular goal at your work because you think it'll make you happier when you reach it. Yet be careful when thinking about how future situations will affect your happiness, because we're susceptible to making mistakes when doing so.

Consider that when people are asked to picture how being unable to walk would affect their happiness, they overestimate the impact because they are consciously focusing on the problems that come with the disability. In actuality, though, we get used to major changes — positive or negative — far more quickly than we expect.

How we think about the future is also colored by how we recall the past. It's the peak moments of pleasure and pain that stick with us the most.

Say you're at your friend's wedding and you trip and fall on your face in front of everyone. That one moment of embarrassment might negatively affect your whole day, and even change your disposition about attending future weddings.

Worrying too much about future situations or achievements can prevent you from being happy right now. For example, focussing too much on achieving, especially in your job, may negatively impact your health and relationships, and distract you from your current experiences.

We also often begin goals with unreasonable expectations for ourselves and when we get frustrated, we end up settling for less, making our path unhappy most of the time. For instance, when we try to get fitter, we often aim for extreme exercise routines that are way beyond our capabilities. Then after inevitably failing to reach our expectations, we take a "what-the-heck?" attitude, leading to no more exercise.

So, when reaching for a better future, don't forget about your quality of life in the present!

> _"You must be alert to what you are sacrificing as well as how you are benefiting from fulfilling your ambitions."_

### 7. To decide what makes you happy, listen to immediate feedback from both yourself and others. 

As we've seen, we're prone to letting our past and future cloud our happiness in the present. So how can we make decisions that bring us happiness both today and tomorrow?

We can learn to listen to feedback from our activities. Focussing on the feelings that come as a result of your daily activities will help you ascertain what really brings you happiness.

To start, you can use tracking methods such as the DRM ("Day Reconstruction Method") to discover which activities and people bring you the most pleasure and purpose.

For one or two weeks, write down all of your activities each day, their length, who you were with, and rank them on a scale from one to ten to represent how much pleasure and purpose you experienced when doing them.

You might also want to ask other people for their thoughts and advice. Friends and family will of course have a more objective view on your current situation and might be in a good position to say how a decision might impact how you'll feel in the future.

To get the most out of other people's advice, ask specific rather than general questions. You might ask your partner, for example, "How do you think our daily routine would look in a few months if we decide to get married?"

Use this feedback to make better decisions and foresee your future happiness. Think about the immediate feedback you get from your activities in the past, as well as objective information from those around you. But avoid over-thinking. For example, we often spend a huge amount of time on minor decisions, like choosing the tiles for the new bathroom, and not much time on big decisions, like which house to buy.

> _"If you can more accurately monitor the feedback for your happiness from your decisions, you might be able to make more decisions that make you happier."_

### 8. By using unconscious mechanisms, you can design your environment to steer your attention toward things and behaviors that make you happy. 

By now, you should see how what you do affects your feelings, and you may wish to change how you behave in order to be happier.

The best way is by deceiving your unconscious brain.

You can start by redesigning your immediate environment to automatically make you happier. Even simple, small changes to the settings for your daily activities can alter your feelings towards them significantly, as well as help you form healthy habits and break bad ones.

For example, changing your office light bulb to a blue bulb can have a huge effect on your efficacy, as it's been shown to biochemically increase alertness.

You can also set up primes and defaults that make you behave the way you wish with little effort. Fill your environment with incentives and reminders for positive behavior. So, if you want to go for a run in the morning, lay out the clothes you need the night before, and make a playlist of your favorite music to run to.

It's a great idea to have people around you who support your goals and it'll help to seek out people who have similar objectives to yours. For example, buddying up with a friend with the shared aim of losing weight will combine mutual support with friendly competition and will help both of you keep to your goal.

For extra incentive, you can make public promises regarding your goals. This will make you feel more obliged to reach them. If you want to quit smoking, tell everyone at your office. You'll have the added motivation of avoiding the embarrassment of explaining yourself if you light a cigarette, and you might even receive some help.

Lastly, it's important to stay realistic about what you want to achieve, why you want to achieve it and divide big plans into bite-sized steps.

### 9. To be happier, pay attention to what you’re doing, choosing to ignore distractions. 

Every day, we let ourselves get distracted. Our phones and new technologies are often what capture our attention almost constantly throughout the day. It's become increasingly difficult to fully engage in a single activity and our brief attention span has become an obstacle to being happy.

The reality is, distraction costs time and energy and is destructive to happiness. In fact, just the act of moving your attention from one thing to another, such as from your work email to your Facebook feed takes "_switching cost._ " That is, your brain needs time and energy to re-orient itself between tasks, which leads to a decrease in efficacy in both tasks.

Imagine you're trying to finish a paper for university whilst watching a film and texting your friend. Attempting to do all three simultaneously means you'll do all three poorly. This multi-tasking stops you from consciously attending to any experience, focussing on what you gain from it and how it makes you feel.

If you immerse yourself in an activity, like talking to your friend without checking your phone, you'll gain far more enjoyment and purpose from it.

So, what can you do to avoid distraction?

First, establish new defaults to break bad habits. You could switch off all non-essential notifications on your phone, keep it on silent as much as possible and even consider dropping internet on your phone altogether.

You can also make commitments to pay attention to your friends and family. For instance, when sitting down for a meal, play the "phone-stacking game" whereby everyone must place their phone in the middle of the table at the start of the evening and the first one to check theirs has to foot the bill.

Finally, make a mindful effort to focus on the "here and now," whether it's work or pleasure. Being in the moment becomes easier with some practice and will allow you to benefit more from your experiences.

> _"You only have so much attentional energy and it will make you happier, more efficient, and healthier if you are able to focus it properly."_

### 10. Final summary 

**The key message in this book:**

Rather than overhauling your personality and radically changing how you think, you can increase your happiness by having a balance of pleasure and purpose in your everyday activities, focussing on their positive aspects and making small adjustments to your surroundings.

Actionable advice:

**Spend more time with people you like and less with those you don't!**

It may seem obvious to socialize with people we like, but often we hang around with people who don't bring out the fun or the purpose in our lives. So, make a concerted effort to surround yourself with people who do bring out your pleasures and purposes.

**Do something for someone else.**

Caring for others can foster a strong sense of purpose within us and make us feel incredibly good. One study showed that on average, participants felt happier when they spent $20 on somebody else rather than themselves. Consider what you might do regularly to help or give to others.

**Suggested further reading:** ** _Happiness_** **by Richard Layard**

In Happiness, economist Richard Layard examines what it is that makes us happy and how anyone can achieve greater happiness. Basing his studies on insights from such diverse fields as psychology, philosophy and neuroscience, Layard presents compelling arguments that are great food for thought, encouraging readers to question their daily habits and practices.
---

### Paul Dolan

Paul Dolan is a Professor of Behavioural Science at the London School of Economics and Political Science and is acclaimed for his extensive research on the connection between happiness, well-being and behavior. He advises both US and UK government on public policies regarding health care and economy.

