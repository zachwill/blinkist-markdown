---
id: 53722ab86264390007040000
slug: happiness-en
published_date: 2014-05-13T14:03:57.000+00:00
author: Richard Layard
title: Happiness
subtitle: Lessons from a New Science
main_color: 2583BA
text_color: 1B5F87
---

# Happiness

_Lessons from a New Science_

**Richard Layard**

In _Happiness_, economist Richard Layard examines what it is that makes us happy and how anyone can achieve greater happiness. Basing his studies on insights from such diverse fields as psychology, philosophy and neuroscience, Layard presents compelling arguments that are great food for thought, encouraging readers to question their daily habits and practices.

---
### 1. What’s in it for me? Money won’t make you happy, but you can learn what will! 

The pursuit of happiness is as old as humankind. Only recently, however, have scientists and researchers begun to examine exactly what it is that makes us happy.

That's where Richard Layard's _Happiness_ comes in. In his bestselling book, the famed British economist presents and investigates much of this research and shows that, while money and material wealth can bring great happiness to people in poorer countries, wealth has no such power in the already abundant Western world.

Layard also uses research in neuroscience to demonstrate that the human search for happiness is fundamental and innate — even infants respond with happiness or unhappiness to certain stimuli that our brains are hardwired to respond to.

In the following blinks, you'll find out why happy people are less likely to suffer from numerous diseases and health problems, like heart attacks, and more likely to have stronger bones and better skin.

You'll learn why the pursuit of happiness is behind every decision you make.

Finally, you'll discover why higher, progressive taxation is arguably a key factor in increasing people's overall happiness.

### 2. Contrary to popular belief, happiness can be measured. 

Most of us believe that happiness is a mysterious phenomenon — a feeling or state of being that eludes measurement and explanation. Yet, in fact, there are several scientific ways to measure happiness.

In most studies, participants are asked to simply assess how happy they are with their life situation in general. For instance, the General Social Survey — a longitudinal study conducted in the United States — asks participants the following:

_Taken all together, how would you say things are these days — would you say that you are very happy, pretty happy or not too happy?_

The resulting data of such studies suggests that the average rating of US citizens' happiness hasn't significantly increased since 1945.

Clearly, such studies depend on rather subjective assessments of one's own happiness. However, neuroscientists have also experimented with some more objective methods.

Using EEG, for instance, they were able to identify certain areas of the brain that were active whenever subjects were happy or when they encountered things associated with increased happiness — like receiving a gift or compliment, or when presented with images of people they like.

Such studies have revealed that positive feelings — such as pride, joy or gratefulness — usually correspond to greater activity in the left frontal area of the brain. In contrast, negative emotions — like fear, anxiety or anger — are linked with the right frontal area.

The EEG approach can even work with infants: when they suck on sweet foods, their brains' left frontal area is activated. Sour tastes, on the other hand, lead to greater activity in the right frontal area.

Moreover, it's even possible to directly induce human emotions. For example, experiments have demonstrated that stimulating the left frontal part of the brain with a strong magnet can automatically lift a person's mood.

### 3. Happiness is good for your health. 

Being happy doesn't just put a smile on your face — it's also beneficial to your physical health.

Whenever we experience positive emotions, our brains release certain neurotransmitters — also known as "happiness hormones" — which have been shown to have a positive effect on many of our bodily functions.

While being happy results in greater levels of these hormones, it also leads to lower levels of the stress hormone _cortisol_. Cortisol has a negative impact on our immune system, causes us to age more rapidly, and actually thins our skin and weakens our bones. Increasing one's happiness thus means less stress, and less stress means better health.

Consequently, happy people have a stronger immune system, preventing them from catching a common cold and other diseases. Moreover, even if happy people do become sick, they tend to recover faster and have fewer symptoms than unhappy people do.

More significantly, happiness also decreases the risk of suffering from more severe health conditions. For example, one longitudinal study showed that happier people are at a lower risk of having a heart attack. They were also shown to be at a lower risk of developing restricted blood flow in their arteries — a high-risk factor for heart attacks.

In general, experiencing happiness tends to have extremely positive effects on our overall health and increases our general well-being. For example, one survey of 750 actors nominated for an Oscar showed that the nominees who'd actually won the coveted award went on to live, on average, four years longer than those who'd lost.

There are many more examples that prove the point: experiencing happiness doesn't just _feel_ good — it's is also good for your health.

### 4. The pursuit of happiness is a main driver of human behavior. 

Think back to the last time you made a decision. Now ask yourself: What led me to make that particular choice?

Like most people, whenever you have to make a decision, you tend to choose the option that leads to your greatest happiness and well-being — whether that decision concerns what to eat for dinner, which job to take or your next holiday destination.

Why?

First, the pursuit of happiness — the desire to be safe and well — is one of the main, innate drivers of our behavior. Indeed, it has developed over the course of human evolution: the capacity for happiness evolved in our ancestors so they could differentiate between behavior that was good or bad for their survival and reproduction.

For example, eating foods rich in calories made them feel happy, as did mating. Thus, today, most of the things that bring us happiness — such as eating, sex, good friendships — were, at some point, crucial to human survival and for the transfer of genes from generation to generation.

Similarly, those things that cause us to feel unhappy — like hunger, thirst, loneliness or being poisoned — were detrimental to our survival. Consequently, we developed emotions to signal that we should avoid situations that would cause unhappiness.

Take fear, for example. Fear helped our ancestors avoid dangerous situations, such as facing a predator, and hence to avoid the risk of being killed. In their hostile environment, our ancestors' capacity for experiencing fear was an essential attribute. For example, people who ran away from hungry saber-toothed tigers lived longer than those who attempted to pet them.

Hence, both the pursuit of happiness and the avoidance of unhappiness were part of our ancestors' survival strategy. And the pursuit of happiness remains a strong determinant of our behavior — even if today very few of us are at risk of being devoured by tigers.

### 5. For decades, we’ve been getting wealthier – but not happier. 

We tend to believe that, as Western societies have progressed in many ways, our happiness has increased too. Yet, since the 1950s, most people in the Western world have not gotten any happier.

In fact, the average income in the United States has doubled since the 1950s and, with it, the average standard of living. However, surveys have not found that more people describe themselves as happy. Clearly, the increase in wealth did not automatically lead to an increase in happiness.

This can be seen in most European countries, too: the average income continues to grow, yet this doesn't appear to make people happier.

Furthermore, despite vastly improved economic circumstances, increasing numbers of people are suffering from depression and alcoholism. In fact, in the U.S. and other Western countries, major depressions were at their most prevalent during the economic growth of the 1960s and 1970s.

The same is true of alcohol consumption. With the exception of France, there has been a reported dramatic increase in the amount of alcohol consumed in most countries. In Germany, for example, alcohol consumption has quadrupled since the 1950s.

Finally, in the latter half of the twentieth century, statistics show that crime has increased significantly. Indeed, between 1950 and 1980, crime has increased by a massive 300 percent in most countries, despite the strong economic growth and exceptionally low unemployment rate of the times. This includes all types of crime — fraud, violence, burglary, car theft and so on.

Because of the increase in crime, people began to report that they felt unsafe in their own cities, and that they trusted other people less. Furthermore, this decrease in happiness is evident in the fact that the disintegration of families became more common.

### 6. Money doesn’t make you happy – as long as you earn less than your neighbor. 

Would you rather live in a world where you earn more than you do today but less than everyone else, or one where you earn less than today but more than everyone else?

Given the choice, most people would prefer to live in the latter world.

Why?

Humans are a competitive species predisposed to compare themselves to their neighbors, siblings and colleagues.

So, while the absolute figure on your paycheck doesn't necessarily lead to greater happiness, having an income that's relatively higher than other people's does make a difference.

That's because people who earn more than their colleagues are more likely to feel respected and valuable. In contrast, those earning less than others — such as their peers — tend to feel worthless and unhappy.

In societies where individuals are encouraged and coerced into fierce competition with each other, many people feel as if they're in a rat race. In this futile, seemingly endless pursuit, individuals try to outperform others, who, in turn, attempt to outperform them as well.

The result? No one gets anywhere.

As a concrete example, consider the reunification of Germany. The effects of the reunification on people from the former East was that, though their wealth and standard of living increased dramatically, their confidence took a nosedive.

That's because they'd become accustomed to comparing themselves to the people of other socialist countries in Eastern Europe who were even poorer than them, hence they felt relatively rich and prosperous. But with Germany's reunification, they began to compare themselves with the wealthier Western Germans, and were left feeling relatively poor.

### 7. We get used to everything, so material gain can’t make us happy in the long run. 

Like every other animal, humans quickly become accustomed to new stimuli, environments and circumstances. We get used to the new people in our lives, such as a new spouse; we get used to becoming parents; and, of course, we get used to having a bigger house or a higher income.

For this reason, winning the lottery, receiving a sizeable bonus at work or finding a new job that offers you more money only bring temporary happiness. Even though you achieved something positive in your life, the positive emotions you're rewarded with fade as you become accustomed to the new status quo.

Indeed, you ultimately end up feeling only as happy as you were before your good fortune. Becoming accustomed to any gains in your life — for instance, a higher standard of living — drives you to want increasingly more of everything just so you can experience another small but temporary happiness boost that will leave you longing for the next.

The result? You'll wind up needing more and more just to maintain the same level of happiness.

Hence, we're driven to work more so we can earn more, and to earn more so we can buy and consume more — all with the goal of attaining permanent happiness. Yet, because we get used to everything we gain, this approach simply doesn't work.

As with any drug, a little kick or "hit" is short-lived — a high simply can't last forever — and thus cannot lead to a state of permanent happiness.

To use a metaphor, it's like trying to win a race by running faster, then realizing you're actually running in a hamster wheel. Much like Alice learns from the Red Queen in _Alice Through the Looking-Glass_, running gets us nowhere: "It takes all the running you can do, to keep in the same place. If you want to get somewhere else, you must run at least twice as fast as that!"

### 8. While more money doesn’t make you happier, too little of it can make you unhappy. 

On average, people living in the wealthier nations of the Western world are happier than those living in poor, developing countries.

This is also evident in countries that were once much poorer, but that gained some wealth in recent decades — like India, Mexico, Brazil and South Korea. In these countries, as the average wealth increased, so did people's reported levels of happiness.

Why is this?

The reason is simple: being poor, fearing for your life and lacking the material basis to sustain your family are hugely detrimental to happiness.

Increasing people's income in such cases of extreme poverty leads to increased happiness because getting rid of this lack of the basic means of survival results in a great pain being lifted.

For instance, if an increase in wealth means that you don't have to worry about your children starving to death every day, it will have relieved you of a profoundly heavy burden, and thus dramatically increased your happiness.

However, as we've already seen, wealth and happiness don't necessarily go hand in hand. Past a certain point, the positive effect of money on happiness comes to a halt. As studies suggest, beyond an annual salary of 20,000 USD, happiness stops increasing in concert with income.

The lesson is clear: While in the poorest countries money has the power to increase happiness, in the Western world, where most of the population have a satisfactory level of wealth, money obviously isn't the key to achieving the greatest happiness for the greatest number.

What, then, _is_ the key? How can we in the West increase our happiness in the long term?

### 9. Mostly, happiness depends on things you can’t buy. 

Since 1981, the _World Values Survey_ has asked more than 350,000 people in fifty countries about their general happiness. The resulting data show that the following five factors, in descending order, have the strongest influence on our overall well-being:

  1. _Family relationships_ and our _close private life_

  2. Our _financial situation_, especially for those at risk of poverty

  3. Our _work_, as something which gives our life purpose

  4. _Community_ and _friends_ as a source of trust and belonging 

  5. Our _health_, especially for those suffering from severe illnesses, and most extremely in cases of mental disorder.

As many studies indicate, family is the most important factor in our happiness — much more so than our financial situation.For example, going through a divorce has double the impact on one's level of happiness as losing 30 percent of one's income. Moreover, married people have been shown to live far longer and to enjoy much healthier lives.

Our work and social environment also have been shown to have a strong impact on our well-being. For instance, losing a job is often a devastating experience, leading to diminished self-confidence and self-worth. These losses are far more pertinent to one's well-being than the mere loss of income. Furthermore, our work, and the community we belong to, give our lives purpose: true companionship and a sense of belonging are two of the most positive things that people can experience in their lives.

Next, aside from health, two other factors appear to have a significant impact: _personal freedom_ and _personal values_.

People who live in stable and peaceful countries where they're free to pursue their interests are reported to be happier than those living in restricted societies. People who have their own personal values are also shown to be happier — for instance, those with positive philosophies of life (such as religious people) who value their lives and appreciate what they have.

### 10. A country’s goal shouldn’t be economic growth, but the greatest happiness for the greatest number. 

As we've learned, even though material wealth has steadily increased, people haven't become happier in the last fifty years.

One reason for this might be that our governments are simply pursuing the wrong objective. Economic growth has been the central goal of all Western societies in the past decades, while factors known to be crucial to our happiness — like, family, friends and health — have been left by the wayside. Even though the notion has been obsolete for a very long time, Western governments still seem to believe that if the economy grows, people will be better off.

Why?

Following the Second World War, when many people were poor and craved material wealth, using a country's GDP as the main indicator of its population's well-being seemed quite appropriate. Yet in today's world of relative abundance, the health of a Western country's GDP doesn't correspond to the happiness, needs and desires of the people.

Therefore, politicians should attempt to discover what the people of today actually need rather than satisfying the needs of generations long since passed. To that end, any fixation on increasing material wealth should be jettisoned, as we've learned that we already have everything we need to lead happier lives: for instance, family, social bonds and meaningful work.

If this seems wrongheaded or wildly unrealistic to you, just take a look at the tiny Himalayan kingdom of Bhutan. Since the 1970s, Bhutan's main objective hasn't been the gross national product, but the _gross national happiness_. To this end, Bhutan's wealth is redistributed in order to minimize extreme poverty and status competition among the population.

According to the author, the people of Bhutan appear to be far happier than people living elsewhere.

### 11. One effective way of achieving greater happiness in a society is increasing taxation. 

Would you agree that greater happiness can be reached by increasing taxation? If your answer is no, you're not alone in that opinion.

However, consider what would happen if taxes were high and got higher the more people earned. The result would be people being discouraged from seeking happiness where it can't be found!

A higher, progressive income tax would lead people to work less and devote more time to their families and other proven sources of happiness. It would also discourage them from focusing on money, which — as we've seen — is not a path to increased happiness. Also, higher, progressive taxation would help reduce people's competitive drive and diminish their tendency to compare themselves to others.

Indeed, working less hours and focusing less on money enables greater happiness. We already do more work than is good for us and spend too little time on those things that truly matter for our well-being such as spending time with friends and family. Higher taxation would therefore cause many people to achieve a much healthier work-life balance.

Also, with higher, progressive taxation, the futility of always hunting for the next "high" is made obvious, and therefore we avoid the unhappiness that results from getting used to whatever fleeting pleasure we might gain. In short, higher taxes make the extra work necessary to win the next, small raise seem pointless, thus making it easier to spend more time outside of work.

Finally, the drive to compete with others in terms of income is a zero-sum game: when one gains the advantage, another loses, so the overall happiness effect is leveled out at zero. Therefore, taxes that make it difficult to earn more than others effectively put a stop to the rat race, making greater happiness a possibility for all.

### 12. Politicians should focus on what really makes us happy. 

As we've seen, the primary source of happiness is family. But what can governments do in this regard? Quite a lot, it seems. For instance, they could make our working environments more family-friendly by enforcing flexible working hours and parental leave, establishing mandatory child care in offices and so on.

One proven way to encourage people to spend more time with those who make them happy is to reduce commute times and the continual switching of workplaces. Always being on the move makes it harder for people to establish stable relationships and friendships, and to truly belong to a functioning community. Governments could thus offer incentives that encourage working people to remain close to family and friends.

Governments should also try to minimize unemployment, as being unemployed causes much unhappiness, damages self-esteem and — if people fear unemployment and have to work hard to avoid it — harms societies as a whole. Therefore, a government that wants its citizens to be happy should ensure that the unemployed are offered jobs.

Also, governments should prioritize prevention and treatment of mental disorders, as they are one of the greatest sources of unhappiness that still aren't given enough attention. For instance, in the U.S., a mere 7 percent of health care funds is allocated to mental health. The situation isn't much better in Europe, with just 11 percent spent on mental health care in Germany, and 13 percent in the U.K.

Finally, a society should invest in children's education. Most importantly, school lessons in subjects such as moral values and emotional intelligence — which are known to increase happiness — should be introduced as a mandatory part of education.

### 13. Final Summary 

The key message in this book:

**While wealth has steadily increased for decades in the Western world, people's overall happiness hasn't. Hence, if we want to be truly happy, we should stop focusing on acquiring wealth and start paying attention to the things that actually make us happier: friends, family, meaningful work and health.**

Actionable advice:

**Take stock of what you already have.**

Many people find themselves stuck in a cycle, always trying to chase the next "hit" — the thing that will make them instantaneously happy. But any gains we might achieve by pursuing these goals — like a salary raise or a position of greater power — give us only temporary satisfaction and a fleeting sense of pleasure. Instead of endlessly chasing things which cannot bring you true, permanent happiness or satisfaction, take stock of what you already have. In doing so, you'll probably notice that you already have almost everything you need to be happy in your existence — such as family, friends, meaningful work and good health — and will feel your craving for the next hit begin to subside.

**If you want to improve your health, try to increase your happiness.**

Much research has revealed the tremendous health benefits of happiness: an increased lifespan, lower stress, slower aging, a stronger immune system, a quicker recovery time from illness — and the list goes on.

Given that exercise can increase happiness because it prompts the release of endorphins in the brain and is generally good for our mental health, exercising is a good way of setting in motion a positive, self-reinforcing cycle where exercising leads to happiness and, in turn, happiness leads to better health. 

**Suggested further reading: _The Happiness Advantage_ by Shawn Achor**

_The Happiness Advantage_ looks into the origins of happiness and the positive effects that happiness has on our productivity. Based on extensive research in positive psychology, the book offers concrete tips on how to increase your own happiness and thus your chances for success.

### 1. What’s in it for me? Banish negativity and suffering from your mind forever. 

The key to happiness is something that we all wish to possess. If you doubt that, just consider how many tons of books about happiness burden shelves worldwide. 

And yet, so many of these books fail to teach you one crucial element: you can't become happy through external things like success or wealth. It's only by looking inside yourself that you can find real joy. These blinks, based on the thinking of a highly influential Buddhist monk and philosopher, show you how to do this, providing the basics you need to attain true happiness.

In these blinks, you will learn

  * why fleeting pleasure will ultimately lead to pain;

  * what the hedonistic treadmill is; and

  * why you are not one but two entities.

### 2. Real happiness is a long-term mental state we have to consciously work toward. 

Are you happy? Interestingly, how you answer that question might depend on where you're from. There's a wide gap between the Western conception of happiness and the Buddhist idea of true, long-lasting happiness. 

Many Westerners think of happiness as a momentary, fleeting feeling. The intensity and duration depend on circumstances outside their control. A Westerner might feel happy about passing an exam, for instance, or winning a game or having a pleasant encounter with someone.

Happiness, however, shouldn't be limited to such fleeting moments. Real, profound happiness is something more: it comes with having a healthy state of mind. 

That means cultivating a mental state unburdened by memories and future plans. The only thing that matters is what's happening _now_ : the present. So the key to leading a happy and fulfilling life is being at peace with the present.

This approach to happiness mirrors Buddhist thought, which holds that one can attain a state of profound and sustained well-being when freed of negative emotions. Buddhists denote this state with a Sanskrit word: _sukha_.

Happiness is something that can be cultivated, but you do have work at it. Studies have shown that about 25 percent of our potential for happiness is defined by our genes — but that means 75 percent is up to us!

So you have a lot of power over your happiness. The way you think, live and perceive the world around you has a major impact on your mental well-being. 

All in all, happiness is a matter of interpretation. It may be very difficult to change the world, but changing how you _interpret_ it is manageable.

### 3. Seek happiness within yourself, not from the outside world. 

You can't buy happiness. In fact, external factors in general have a limited impact on happiness. Wealth and social status do have some effect on your happiness, but they only account for about 10 to 15 percent of it. 

Happiness would be out of reach if it was purely an external phenomenon. After all, our desires know no limits; the amount of control we have over the world, in contrast, is very limited.

Consider love, for example. There's no way to ensure that your lover will always love you back. So if your happiness depends on that, you're setting yourself up for disappointment and heartbreak. 

People often think they have everything they need to be happy — and yet still feel unhappy. Psychologists Philip Brickman and Dan Campbell call this the _hedonic treadmill._ It's as if we're running on a treadmill, sweating and straining, but never advancing an inch. We eagerly pursue the new and the exciting, but our level of happiness doesn't rise.

A person might get excited about buying a new car, for example, only to have that joy squashed when a newer model comes out. That's what happens when you seek happiness in the outside, instead of focusing on your inner being.

It's useless to cling to temporary things like cars. Accepting that is the first step toward finding inner peace and true serenity. True happiness comes from reaching such a high state of inner well-being that you're no longer broken by failure or blown full of false pride by success. 

Etty Hillesum, a writer who died in the Holocaust, wrote about this just a year before she lost her life in Auschwitz. "When you have an interior life," she wrote, "it certainly doesn't matter what side of the prison fence you're on."

### 4. Happiness is not the same thing as pleasure. 

Before you can achieve true happiness, you have to understand what it is — and then pinpoint those things that encourage it. But many people start down the wrong path, confusing happiness with other enjoyable emotions, like pleasure.

Pleasure might seem like the fastest and easiest road to happiness, but, sooner or later, you'll come to a dead end. Pleasure is momentary by nature: it occurs under certain circumstances at a certain time.

A pleasurable experience can quickly become neutral or unpleasant because pleasure is so unstable and fleeting. A good meal, for example, is enjoyable up to a certain point; if you eat to excess, however, you'll get sick.

Long-lasting happiness shouldn't be confused with pleasure. A lot of people assume that fame or wealth would bring them all they desire, but such things offer only short-term satisfaction and won't have much influence on your well-being.

In fact, one study found that even winning the lottery doesn't change a person's happiness that much. It causes a temporary spike in happiness but does nothing to prevent that happiness from falling back to former levels.

Chasing immediate, pleasurable desires ultimately leads to disappointment. And we only focus on external pleasures because we're too scared to turn our attention toward the place where it really counts: our inner world. 

That's why people seek external stimulation and distractions, like shopping, alcohol or drugs. Unfortunately, those things only pull us away from each other and, if we're not careful, alienate us from ourselves.

Western countries have staggering depression rates because of this. Life becomes meaningless when you lose that crucial connection to yourself and the world around you.

### 5. Happiness isn’t about avoiding suffering. It’s about freeing yourself from worries about life. 

In developed countries, about three-quarters of people claim to be satisfied with their quality of life. And yet, levels of unhappiness remain high. Why is this?

The answer has to do with the way people understand what happiness is. A lot of people think happiness is just a momentary respite from suffering. They might feel happy on the weekend because they don't have to work, for example.

But the concept of happiness is elusive and deceptive. As we've seen, most people think happiness hinges on temporary things like relationships or money, so they're at a high risk of becoming unhappy if they lose their partner or their job. This partly explains why 15 percent of North Americans experience a major episode of depression before the age of 35. 

Buddhists call this depressive state _dukkha_ — the opposite of _sukha_. _Dukkha_ doesn't only refer to temporary unpleasantness; it's a high vulnerability to suffering that can cause a person to feel like life is meaningless when things don't go as hoped.

How can we end this cycle of suffering? Buddhism holds that suffering is universal and unavoidable, but that our pain doesn't actually stem from the suffering we experience. Rather, it stems from the unhappiness _we_ create.

If a person loses her job, for instance, she'll be consumed by unhappiness. That unhappiness doesn't actually result from the loss of the job itself, though. It arises from this person's worries about loss of wealth, status and career prospects.

Giving up this unnecessary worrying is the key to moving toward true, long-lasting happiness and leading a more fulfilling life.

### 6. The ego is the true source of suffering and conflicted emotion. 

Have you ever felt like everyone around you was self-centered or self-absorbed? Most people put themselves before all else. Buddhism, however, sees such egotism as a source of misery.

The ego causes a lot of problems and conflict with personal identity. To sort this out, Buddhism distinguishes between two "I"s: the natural, instinctive "I" we use when we say "I'm hungry" and the conceptual "I" we imagine but that doesn't actually exist.

Think of it this way: you might act tough or proud around your colleagues or friends, but that's just an imagined identity. You only act like that because that's how you want to be perceived by those people.

But it's this second "I" that slowly becomes the core of a person's being. They'll turn away from anything they think threatens it, and gravitate toward anything that pleases or flatters it.

That's why a minor joke made at your expense at work might feel like a major attack on your character. It's a blow to your ego and comes off as a threat to your identity when it really shouldn't be such a big deal.

Our egos also arbitrarily assign certain qualities to the people and things around us. When you think "this is nice" or "this is ugly," you unconsciously let your mind superimpose these attributes on the thing you're thinking about. 

This habit leads to labeling a person as good or evil based only on one aspect of that person's personality. Egos are stubborn, too: once you put a label on something it can be hard to think of that thing in any other way.

Further trouble arises as the gap between the real world and the ego's conceptions widen. The ego will reach a point where it falters and shatters, taking your self-confidence with it and leaving you with only frustration and suffering.

### 7. Free yourself from your own ego. 

The concepts of identity and status are closely connected to unhappiness. That's why it's imperative to detach from your ego.

A person who clings to a specific self-image will do anything to make sure that that image is recognized and accepted. Such a person will devote all their time and energy to seeking validation. Ever worked with someone who spends more time smiling and cracking jokes than doing actual work? Such people are usually overly invested in having their identity praised and accepted.

According to Buddhist thought, real confidence and inner peace can only be achieved through _egolessness_. The ego is simply too fragile to be a reliable foundation. When you think too highly of yourself, you're putting yourself at risk of being emotionally shut down when things do go wrong.

That's why it's important to separate yourself from your ego. Detaching from your ego makes you less vulnerable and gives you inner strength, putting you on the path toward true, profound happiness!

Humility is another key part of ego-free happiness. So acknowledge your limits! If you're humble, you won't need validation from other people, a freedom that can prevent much distress and disappointment. In that sense, humility is the source of inner freedom.

And when you become humble and relinquish your ego, you can also focus more on other people's concerns. You'll be much more attuned to the sufferings of others once you stop regarding yourself as of prime importance.

History shows that real heroes are people who freed themselves from their egos. Gandhi, Martin Luther King, Jr., Mother Teresa, the Dalai Lama, Nelson Mandela — these people focused on helping others because they'd ceased to focus on themselves.

### 8. Your thoughts and emotions can be your worst enemies. 

Thoughts and emotions can be your best friends — but they can also be your worst enemies.

The main sources of pain in our lives often aren't what we think they are. During war, for example, it's been shown that depression and suicide rates actually _decrease_. The most powerful causes of distress are elsewhere. But where?

When you feel like your life is falling apart, it seems like everything in the world is against you. Any little thought or perception can become a source of suffering. So, after experiencing a breakup or the death of a loved one, for example, even the smallest thing can trigger a storm of rage or despair.

It's not unusual to be consumed by negative thoughts during difficult times, but those thoughts are often out of sync with reality.

Think about your last breakup, for example. There was probably a time when you completely hated your ex-partner. But now, after some time has passed, how do you about this person? You probably realize you focused too much on his or her negative traits when things were falling apart.

Negative emotions can get out of control when we dwell on them like this. They can fester and grow like an untreated infection.

If you let yourself burst into rage whenever you're cut off in traffic, for example, that can turn into an even worse habit. You might start pointlessly losing your temper in even more petty situations.

That's why it's essential to identify your triggers. Overcoming your triggers is an important part of the journey toward inner peace. You might be tempted to make witty but mean-spirited remarks when you're mad, for instance, but that only makes you and others around you feel worse.

### 9. Examine your negative thoughts and emotions so you can overcome them and achieve inner peace. 

If you don't control your own emotions, they might end up controlling you. How can you prevent this?

Buddhism holds that opposite emotions cannot arise at the same instant. We might fluctuate between love and hate, for example, but it's impossible to feel both for the same person at the same time.

Consequently, you can gradually eliminate hate and other negative emotions by familiarizing yourself more and more with altruistic love and kindness — because love and hate simply can't exist at the same time.

Positive thoughts work as an antidote to the negativity in your life. They're the only true source of long-lasting well-being.

If you suppress your negative emotions, though, they'll eventually resurface in other ways, perpetuating your unhappiness and furthering your troubles. You need to acknowledge the existence of these negative thoughts so they don't consume you.

Negative emotions, thoughts and memories can be overcome, however. And once they are, they'll dissipate without leaving a trace in your mind. They'll be absorbed back into your consciousness like waves going back into the sea and slowly lose their grip on your mind.

You can only understand your negative thoughts by thinking deeply about their roots. You'll be able to deconstruct them and see that, at the core, they don't have any substance.

So if you're feeling overwhelmed by anger, look inwards. You'll realize your anger is just an emotion and you don't have to let it control you. After all, it's just a temporary state of suffering and you're the one who's reacting to it. This kind of thinking allows you to detach yourself from your anger and prevents you from succumbing to a momentary fit of rage.

### 10. Final summary 

The key message in this book:

**Achieving long-lasting happiness is hard work. It requires overcoming your ego, reconciling yourself to negative emotions, reorienting your world view and re-examining the definition of concepts like "happiness" and "pleasure." You can't do those things overnight, but if you stay dedicated to the path, you can bring yourself to a high sense of** ** _sukha_** **– an achievement much more fulfilling than any temporary pleasure you might get from wealth or fame.**

Actionable advice:

**Eliminate your pain by focusing on it.**

When you experience something that causes pain, whether physical or emotional, bring it into focus, even if it's difficult. Visualizing the pain will lessen it, because you'll understand that it's an emotion separate from your true identity.

**Suggested** **further** **reading:** ** _Altruism_** **by Matthieu Ricard**

_Altruism_ (2015) examines our need to care for others, a compulsion that is essential in both humans and animals. These blinks explain how and why caregivers do what they do through the lens of philosophy, economics and evolutionary theory.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

### 1. What’s in it for me? Never take happiness for granted. 

What are the most basic human emotions? Certainly, they include fear, love, hate and, of course, happiness. Such emotions are doubtless central to what it means to be human. 

Well, not so fast. In fact, we didn't always regard happiness as something natural or deserved — or even particularly human! Long ago, happiness was seen as something from the gods, and something it was definitely better not to mess around with.

We owe a debt to the great minds of history. They enabled our modern conception of happiness, transforming it from something mysterious and arbitrary into something to be pursued in our free time!

In these blinks, you'll learn

  * why the Dark Ages really were that dark;

  * what Marx's followers had to say about happiness; and

  * why droves of Americans sued the government after 1776.

### 2. Socrates, Plato and Aristotle didn’t fully agree on a definition of happiness. 

We all have a say in our happiness, right? If you're not feeling happy, go out and change that. Having a bad day? Eat some chocolate!

But people didn't always think like this.

Let's take a quick look at Athens. It wasn't until after the city was democratized in the fifth century BCE that people began dreaming of a happy life that they could influence.

Prior to the downfall of the Persian Empire, people thought happiness was simply out of their power. Due to all the variables that brought utter misery at the time — poverty, inferior medical technology, political suppression and so on — happiness seemed better left to the gods.

After the Empire's defeat, though, Athens began to blossom. As democracy progressed, people began experiencing a new freedom, and this inspired some to believe that they may have some influence over their happiness.

This is what Socrates and his student Plato believed — that, by using their ability to reason, people could have more control over their own lives, and thus their happiness. Socrates and Plato argued that it wasn't just up to fate, luck or the gods. It was up to people themselves. To them, happiness was the ultimate goal, something far greater than mere earthly satisfaction. Longing for such transcendent happiness was a natural human tendency.

Aristotle, on the other hand, saw things a little differently. Like Socrates and Plato, he believed that humans were part of a higher order. But, unlike them, Aristotle held that we must look to the world; only there could we unearth our role as humans and the true role of human happiness.

This is represented beautifully in the famous fresco _The School of Athens_, by Raphael. Here we see Plato pointing toward the skies while Aristotle holds out his right hand, palm facing toward the earth.

### 3. As Europe entered the Renaissance, happiness became attainable for individuals. 

The European Middle Ages are sometimes referred to as the Dark Ages because they sit between the "light" of the Roman Empire and the European Renaissance. But it was also dark because everyone was grumpy and miserable.

During the European Middle Ages, many people had a very negative outlook both on life and on the idea of happiness. Full of despair, they felt trapped in their own bodies, which frequently caused them pain.

In short, it was a dark time. Also during this time, the Black Death obliterated almost a third of Europe's population. 

A thirteenth-century manuscript by Italian cardinal and deacon Lotario dei Segni, who later became Pope Innocent III, nicely sums up the atmosphere of the time: "Happy are those who die before they are born, experiencing death before knowing life."

Later, in the fourteenth and fifteenth centuries, as the European Renaissance gathered momentum, people lightened up somewhat, and happiness became something attainable. The transition was slow, but in the fifteenth century, people began philosophizing about life and happiness.

For example, Italian philosopher Giovanni Pico della Mirandola's 1486 work _On the Dignity of Man_ is regarded by some as a cornerstone of the Renaissance. He writes that man's dignity is within God — that man can position himself in the universe wherever he wants and determine his own level of greatness or depravity. He argued that, as we ascend toward God, we'll get happier. Indeed, religion is what leads to _perfect happiness_. Although this perfect happiness can't be found on Earth, we can obtain _natural happiness_ through philosophy and being the best we can be.

### 4. By the end of the eighteenth century, happiness was commonly regarded as a human right. 

People continued to embrace happiness throughout the Enlightenment. And at one point people were sure that the only way to conceive of happiness was to understand innocence and sin. Since it was believed that sinners would spend eternity burning in hell, people figured that innocence was the answer to a happy life. So what did they turn to in order to achieve happiness? The Garden of Eden.

The quest for Eden resulted in a work by biblical scholar Bishop Pierre Daniel Huet. In 1691, he wrote _A Treatise on the Position of the Earthly Paradise_, which claimed that the remains of Eden were to be found in Babylonia (modern day Iraq).

However, as the Dark Ages receded further into the past, the notion of innocence and sin no longer defined the happiness debate.

At the beginning of the eighteenth century — the time of the Enlightenment — people's orientation shifted and Eden became less interesting than Earth. This is what Voltaire and the philosopher Claude-Adrien Helvétius — who, in 1795, composed a poem called _Happiness_ — proposed. To them, Earth was paradise — Heaven incarnate.

Soon, many jumped on the bandwagon and started regarding Earth as a place for paradisiacal delights. This was manifested through, for example, the creation of _pleasure gardens_. These could be compared to modern amusement parks, and they boasted music, games and refreshments — places for fun and joy on Earth. 

And so, eventually, happiness evolved into a human right. It was now not only attainable; it was a natural condition. That is, a happy life was seen as part of our purpose — a goal nature intended us to reach. By the middle of the eighteenth century, most people viewed happiness as a natural right, and this is where our modern notion that everyone has a right to happiness stems from.

Despite the age of happiness, though, sombre terms such as "melancholy" also appeared in literature and became popular by the end of the eighteenth century, as we'll see next.

> _"The great goal of the century, it was expressed time and again. Does not everyone have the right to happiness?"_

### 5. Sadness is okay – after all, it’s also a means to happiness. 

Despite the belief that happiness was natural, notions of sadness and melancholy began cropping up by the end of the eighteenth century. People born during the Enlightenment were taught that they were supposed to be happy, but as the eighteenth century came to a close, people were also talking about a new, strange sadness. 

This can be seen, for example, in Goethe's 1774 publication _The Sorrows of Young Werther._ The book became popular among young people who were discontented with their lot in life, and Goethe's work gained a following. People even dressed like Werther, sporting blue frock coats and vests.

Later, in the nineteenth century, the poet Jean Paul introduced the notion of _Weltschmerz_, or "world suffering," to express the inexplicable sadness and pain felt by people at the time. This term was later popularized by another Romantic poet, Heinrich Heine.

But all this doom and gloom — and the new terms for it — actually ushered in new conceptions of happiness. Sadness became a starting point, an emotion that gave new impetus to the search for happiness.

By the latter half of the nineteenth century, people had started regarding pain and suffering as serving a purpose: both could lead to joy. This also dovetailed nicely with the Christian tradition of struggling through a life of sin and suffering in the hope of ascending to heaven and a reward of eternal joy and happiness.

Around this time, English poet Samuel Taylor Coleridge addressed joy in his poetry, describing it as something that we merge with. This merging is both delightful and resplendent, a means of experiencing something greater than ourselves.

> _"At once a cloud and a shower, joy is both the force that pours down upon us and the precipitate of that power."_

### 6. In the young United States of America, people were responsible for their own happiness. 

The American Dream — the freedom to work hard and create a wonderful life for yourself — drove countless people to set out in search of whatever bounty the land of opportunity had in store for them. But really, the American Dream does not guarantee happiness for everyone.

In fact, after the Declaration of Independence in 1776, hundreds of disgruntled people filed lawsuits against the American government or against other citizens.

Drafted by Thomas Jefferson, the American Declaration of Independence states that it's a self-evident truth that everyone has certain inalienable rights, including the right to _the pursuit of happiness._

In addition to spawning different interpretations of what exactly "the pursuit of happiness" meant, it also led people to attempt to legally claim what they thought was theirs: the happy life. This resulted in hundreds of discontented people suing the government, because they thought their happiness was protected by law.

Later, however, the prevailing belief was that everyone was on their own in the journey toward happiness. In response to the lawsuits, Benjamin Franklin argued that happiness _was_ a right, but that it wasn't simply handed to everyone. He instead instructed people to "catch it yourself."

Franklin believed everyone was responsible for realizing their own happiness, whatever that meant for them — be it wealth, tight family bonds or anything else that they might pursue. Franklin used wine to illustrate his point, saying that God had given us the opportunity to grow happiness all by ourselves, for example, by cultivating wine.

Franklin wasn't the only one to adopt such a notion. In the final blink, we'll see who else shared his opinion, as well as learn about a prominent faction that didn't.

### 7. Communists believed happiness would come from reigniting a sense of community. 

Unfortunately for Benjamin Franklin, not everyone believed that the pursuit of happiness ought to be an individual endeavor.

Although Franklin's liberal sentiment was famously shared by, among others, economist Adam Smith, people began to question the dominant concept of individuality, a cornerstone of capitalist societies.

For instance, Friedrich Engels, an admirer of Karl Marx, reasoned that humans had lost their soul and spirit — their _human consciousnes_ s — due to "brute individuality." He argued that happiness would come to those who could help humanity regain this lost consciousness.

For communists, happiness came from looking to the past, the only place where we could return to our roots and reclaim our human consciousness. According to _The Great Soviet Encyclopedia_, throughout history, happiness was seen as an innate human right. However, in societies based on class, the ruling class's desire for happiness always mercilessly overpowers the oppressed classes' struggle for happiness.

The communists held that the predominant approach to happiness — the egoistic pursuit of individual interests — incapacitated human consciousness. It made humans indifferent to others and uninterested in morality. Therefore, if humans were to regain their lost happiness, society had to return to a community, preferably a classless one.

No matter which viewpoint you subscribe to, there are countless fascinating perspectives to be found in the long and thought-provoking history of happiness.

### 8. Final summary 

The key message in this book:

**Throughout history, we have held vastly different views on the concept and pursuit of happiness. From Ancient Greek philosophers, to Benjamin Franklin, to communism, our idea of happiness has evolved in unexpected and fascinating ways.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: _Stumbling on Happiness_** ** __****by Daniel Gilbert**

_Stumbling on Happiness_ explains how our brains make us think about the future. It employs accessible language and everyday examples to help us understand complex theories from psychology, neuroscience and philosophy.
---

### Richard Layard

Richard Layard (b. 1934) is a renowned British economist and the director of the Centre for Economic Performance at the London School of Economics. Layard's research on unemployment was the basis for the 1998 reformation of the job market in the United Kingdom, which significantly decreased the level of unemployment.

