---
id: 5e64dfe66cee070006a1dbb8
slug: happy-accidents-en
published_date: 2020-03-09T00:00:00.000+00:00
author: David Ahearn, Frank Ford, David Wilk
title: Happy Accidents
subtitle: The Transformative Power of "Yes, and" at Work and in Life
main_color: None
text_color: None
---

# Happy Accidents

_The Transformative Power of "Yes, and" at Work and in Life_

**David Ahearn, Frank Ford, David Wilk**

_Happy Accidents_ (2017) is about the authors' experiences in Four Day Weekend — an improv comedy troupe from Fort Worth, Texas. It chronicles the success Four Day Weekend achieved over the course of 20 years and reveals how anyone can use the principles of improv comedy to become more positive, creative, and better at dealing with obstacles in life.

---
### 1. What’s in it for me? Learn life lessons from improvisational comedy. 

Improvisational comedy might not be the most obvious source of wisdom for personal growth and developing a business, but Four Day Weekend's 20 years of improv experience has taught them a lot about living. Their success has been impressive, as they've gone from being unemployed dreamers to seasoned international performers — but what's even more important are the lessons they've learned about applying their improv skills to life.

As Four Day Weekend tell their improv students, their lessons aren't a crash course in "how to be funny." They're an introduction to a practical philosophy and a set of skills that can be applied to life beyond the improv stage. In these blinks, you'll dive deep into the wisdom of an art form that thrives on saying "Yes, and" at all times. Along the way, you'll learn how to communicate better with your coworkers and loved ones, start moving towards true self-actualization, and give back to the people that matter.

Ready? Action!

In these blinks, you'll learn

  * why a focus on being in control can stop you from being creative;

  * how to find the silver lining in a crisis; and

  * the importance of being authentic.

### 2. Improvisational comedy can teach us valuable lessons in positivity. 

Four Day Weekend had almost nothing to their name when they started out as an improv group in 1996. They had previous experience in comedy and certainly some youthful optimism, but they had never managed a business.

So how did a limited-run comedy show morph into a global phenomenon that has performed for two US presidents and held thousands of workshops teaching positivity to business leaders and future generations?

To put it simply: "Yes, and."

"Yes, and" is a foundational principle in improvisational comedy. It indicates that a performer should always take whatever crazy suggestion is thrown at her and not just run with it, but build on it. The result is an atmosphere where creativity can flow — and there's no such thing as a bad idea. Going even further, "yes" is a positive notion that suggests productivity, while "and" communicates a desire for connection and teamwork.

This is something that a large hospital in the US learned in 2016. The hospital requested Four Day Weekend's help with improving its internal communications after an audit ranked it last in terms of employee satisfaction and customer service.

The troupe encouraged the hospital staff to practice daily "Yes, and" exercises with their coworkers to improve their interactions and teamwork. Later that year, the hospital was re-ranked — and came out at number one in employee satisfaction! In just eight months, "Yes, and" had taken it all the way to the top.

The flip side of "Yes, and" are the words "No" and "But." These are barriers to creativity and the natural enemies of improv comics; they stop performers from thinking freely and building on each other's ideas. Improv teaches us to convert these words into "Yes, and" whenever possible, as this approach tends to open the doors that "No" closes.

To identify confidence and creativity in their improv workshops, for example, Four Day Weekend ask participants if they can paint. Usually only around a third of adults say they can. When they ask an audience of young children, though, all of them say yes. When it comes to creativity, children have a "yes" mindset. They believe in themselves and each other much more than adults do.

When we refuse to let judgment dominate, we can welcome new ideas into play and live a "Yes, and" life. It's a path that doesn't require any special skills or background — just an open mindset and a willingness to collaborate.

### 3. Real success is about the team, not the individual. 

Before forming Four Day Weekend, most of the group's members worked at a comedy club in Texas. They found some success there, but the club's owner was greedy and controlling; behind the scenes, things soon turned toxic.

Performers stopped supporting each other and retreated inward in an effort to build themselves up. As a result, the improv rarely worked. For the future members of Four Day Weekend, this negative scenario provided a powerful lesson: there really is strength in numbers.

That strength is embedded in the part of "Yes, and" that's easiest to forget about _._ The part that represents collaboration, knowledge-sharing, and teamwork. The "and."

Good team players see an opportunity for "and" when they see a _hotspot –_ Four Day Weekend's term for the make-or-break moment in a scene. Instead of letting a fellow comic fail, they jump in and help out with the power of "Yes, and." Believing that a team both rises and falls together makes for a great improv comedian; it's something non-performers can learn a lot from, too.

It's just as important for support to work the other way around, too — for the team as a whole to respect and support individuals. Four Day Weekend were reminded of this after the departure of their musical director Paul, a true entertainer and improviser. 

With Paul gone, the troupe had to adapt to the new director, Ray. When a review listed some faults in the musical improv section he featured in, the group realized that they weren't fully tapping into Ray's talents. It required teamwork to bring those talents to the forefront; instead of leaving musical improv to Ray alone, everyone chipped in to help. In the end, the performance of the group as a whole benefitted and became much more well-rounded.

Just as important as supporting those on your team is showing appreciation to your supporters. Early on in their history — when Four Day Weekend were still struggling with ticket sales — a theater critic for the _Fort Worth Star Telegram_ asked to follow the group around and write a feature on the show. The group agreed and went out of their way to include the critic and treat him with respect and kindness. In the end, Four Day Weekend got a glowing review and a lifetime friend; the critic even became an early advocate for the troupe.

In the end, there's little difference between business and improv comedy when it comes to teamwork — supporting others and showing appreciation leads to success no matter what the endeavor.

### 4. To get anywhere in life, you have to take risks and learn when to let go of “no.” 

There's no way around it — risks can be a little scary, no matter who you are. But if you really want to achieve something, the single most important thing you can do is step outside your comfort zone and get the ball rolling.

Now, this goes against the natural inclination toward the _prevent defense_ — the fear of putting yourself out there because you don't want to look stupid. But sticking with that inclination means missing a lot of opportunities.

In 1996, Four Day Weekend were a group of broke friends in Dallas, Texas with a desire to make people laugh. But to really develop, the friends knew they needed a change. So they said "Yes, and." They took a risk, putting together all the money they had and downsizing to nearby Fort Worth, which had an even smaller cultural scene.

There, they managed to negotiate a six-week run at a local venue, and even convinced the theater manager to operate the lighting for them in exchange for beer. Their "Yes, and" was infectious! But another aspect was just as significant — Four Day Weekend knew that real risks are only possible when you let go of "no."

"No" is a big word. It exercises control, but that doesn't necessarily help in every situation. In fact, it can limit your ability to adapt and think on the spot.

Take Four Day Weekend's corporate workshops, where participants are asked to do "Yes, and" exercises and create improv comedy from each other's often strange scenarios. Surprisingly, those with the least responsibility in a company are considerably better at these exercises than the CEOs. Powerful people, it turns out, are often expected to use the word "no" in business. That makes it a lot harder for them to let go and see where the unexpected takes them.

This doesn't mean that you can't turn things down, in improv or in life. It simply shows that there's a difference between a _considerate no_ and a _reactive no_.

Say a coworker suggests a new strategy — something you know hasn't worked in the past. A considerate no involves listening first and then explaining why the strategy isn't great, while a reactive no is an insensitive, flat-out rejection.

The considerate no is productive; it validates your coworker's thinking while preventing her from imposing an impractical idea. Letting go of the reactive no also creates a far more creative and open atmosphere — one in which new ideas and a "Yes, and" mindset can flourish.

### 5. In order to achieve anything, you have to believe in what you’re doing. 

If the members of Four Day Weekend hadn't pushed themselves to achieve their collective vision, none of their success would have transpired. But they truly believed in their comedy show — long before anyone else did — and that belief helped them to persevere when times got tough.

The troupe's first show sold out, but it was mostly due to attendance by family and friends. Over the next few months, they'd often only sell a fifth of the tickets. Despite being broke, Four Day Weekend reinvested any money they did make back into the group. It was their way of _requesting success_ — combining their intentions with a deep-seated belief in themselves in order to make their goals materialize. Their philosophy was to persevere, always request success, and strive to swing the balance in their favor.

On the other hand, it helped that their passion was authentic — if they hadn't been true to their common goals, they would have had a much harder time. The members of Four Day Weekend were always genuinely passionate about improvisation, and this bonded them when they were struggling financially. But it's still possible to lose sight of a shared passion when obstacles get in the way.

In 2007, Four Day Weekend had the chance to pitch a pilot in Los Angeles. Caught up in dreams of Hollywood stardom, they lost sight of their core values. When the pilot failed, they blamed each other and began to drift. Luckily, they reconnected thanks to some introspection; asking what they really cared about helped them rediscover the passion for improvisational comedy that had brought them together in the first place.

The _why game_ is a great way to enable this kind of productive introspection on a very personal level. Ask yourself why you're doing what you're doing in life. Once you have an answer, ask _why_ it's the answer, and continue to dig deeper. Eventually, you'll find yourself getting to the bottom of what drives you. Combine that knowledge with self-belief, and you'll be requesting success and making your goals a reality before you know it!

### 6. Always look to maximize your Return on Improvisation. 

In the investment world, everyone knows that "maximizing returns" is the goal — basically, earning back as much as possible relative to what you've invested. But what about maximizing more than just money? What about prioritizing the way we make people feel just as much?

That's what _Return on Improvisation_ is all about.

This idea is key to Four Day Weekend's improv, and it's the reason they work so hard to give audiences a unique experience every night. It's also one of their guiding principles when their customers are left unsatisfied.

One night, a woman and her husband — a wheelchair user — tried to attend a Four Day Weekend show but didn't receive enough staff assistance to get into the venue. Upset, they left and sent Four Day Weekend an angry email about the experience. 

Four Day Weekend were horrified; they had always taken pride in making their show accessible. As an apology, they offered the couple a refund, free tickets to another show date, and dinner at one of the best restaurants in town. The couple accepted and sent the group a glowing message of thanks and appreciation afterwards. Investing in their customers on a personal level allowed the group to make things right.

Four Day Weekend even built on the negative experience. Realizing that the theater staff were integral to Return on Improvisation, the troupe became more invested in the staff's opinions. This allowed the group to deliver a better experience to customers and improve the Return on Improvisation for everyone.

Valuing people as more than just customers can ultimately lead to accomplishments worth more than any financial gains, too.

In February 2015, while performing for the US Armed Forces in Kosovo, the troupe met a young Texan soldier in the audience — coincidentally named George W. Bush! He told them his dream was to get a picture of his namesake, the ex-president, so the group did their best to pull some strings. In the end, their "Yes, and" approach yielded not only a photo of George W. Bush, but also a personal call from him to thank the soldier for his service. By interacting with the soldier on a personal level, Four Day Weekend made an existing fan feel valued _and_ gained a new fan in the ex-President.

Clearly, a positive "giving back" mindset benefits us, just as much as it does the people around us. This culture of mutual reciprocity has the power to sustain everyone in the long run.

### 7. Adaptability can turn a crisis into a happy accident. 

Improvisation relies on adapting to situations in order to change them for the better. Adaptation is an important skill in all areas of life, in fact — just look at NBA star Michael Jordan. Early on in his career, Jordan was criticized for his less-than-stellar defensive record. But rather than getting angry, he turned the criticism into an opportunity for self-improvement and became one of the best defenders in the league.

Jordan knew, just as Four Day Weekend did, that a crisis might be a _happy accident_ in disguise.

A great example of a happy accident came in 1998. Soon after starting their show in Fort Worth, one of Four Day Weekend's members, David, was due to get married. He had planned to use a local venue's outside area for the reception. But then, crisis struck — in the form of a rainstorm.

The group was forced inside, to an abandoned high-capacity theater. Surprisingly, what started as a wedding day crisis turned into a happy accident with the discovery of Four Day Weekend's new home!

Of course, perspective is key to turning a crisis into a happy accident. If you interpret events with a positive "Yes, and" mindset, you can transform potentially negative experiences into positive ones.

Four Day Weekend had another chance to demonstrate their adaptability when they were offered the opportunity to work with Southwest Airlines, who wanted the group to deliver a training session. Unfortunately, due to budget constraints, the airline couldn't pay the full fee. Following their "Yes, and" mindset, the group accepted publicity in the inflight magazine as compensation.

It just so happened that the magazine was read by Democratic Congresswoman Linda Sanchez — who then arranged for Four Day Weekend to perform in front of Congress. All of this transpired because Southwest Airlines hadn't been able to pay the fee! If the group hadn't adapted and said "Yes, and," this amazing happy accident would never have come to pass.

### 8. Don’t stop reaching for greatness, even if it feels like you’ve made it. 

So you've requested success. You've turned crises into opportunities through the power of "Yes, and." You've learned to adapt as new situations present themselves, and you've practiced return on improvisation.

Now what?

Well, now you need to keep striving. You need to set _new_ goals.

Even after performing in front of Congress, Four Day Weekend refused to give up on making great comedy. For them, the journey is ongoing. Whenever an admirer asks Four Day Weekend how to make it in show business, they reply: "We'll let you know if it ever happens."

This kind of ethic means that you never allow yourself to get complacent. After all, Four Day Weekend argue, 90 percent of show business is hard work and only 10 percent is talent. And from their story, the hard work is pretty clear to see!

Four Day Weekend cite Emmitt Smith as the perfect example of this. When he became the number-one rusher in the NFL, the Texas hometown hero was asked what motivated him. He responded that, rather than looking for motivation in external things, he created it himself — even conjuring up imaginary opponents on the field when no one else was around to play against.

Clearly, Smith isn't one to rest on his laurels. You shouldn't be either.

But while keeping yourself motivated and achieving your goals is great, real success is about more than that. It's about positively impacting the people around you.

That's why Four Day Weekend started giving back to their community once their show became nationally celebrated. The group provide free shows and complimentary tickets to service members and people in need. After all, improvisation involves giving as well as taking — both onstage and off. Practices like these have increasingly integrated the group into the civic life of Fort Worth, the city they love and that effectively launched their career.

Their charitable actions led the group to perform at high profile events, including the mayor's twenty-fifth wedding anniversary and a party celebrating the purchase of the Texas Rangers. Eventually, they were also offered positions as Entrepreneurs-in-Residence at Texas Christian University.

Realizing they had a shelf life, the group decided to expand their training center to give the younger generation more opportunities while the original troupe gradually reduced their shows. These efforts to give back stemmed from a desire to ensure that Four Day Weekend continue to do good in society, long after the original members have left.

Four Day Weekend argue that the idea of leaving a positive legacy is all too often ignored in business, where fighting your way to the top has taken precedence over mutual exchange and encouragement. A better way, they argue, would be for people to learn the lessons of improv — to start saying "Yes, and" in business and in life.

### 9. Final summary 

The key message in these blinks:

**The world is in need of more positivity, creativity, and teamwork — all of which are cultivated in the values of improv comedy. We could all benefit from adopting the improv principles of "Yes, and" to boost happiness and effect change in our own lives, as well as the lives of those around us.**

Actionable advice:

**Give "gifts" to people around you.**

Prioritize _really_ listening to people; make your family, coworkers, and friends feel heard when they speak. Note their reactions, and see if you can feel the mood between you shifting and becoming more lighthearted. Keep an eye on this over the course of a few days to see if your listening has helped a new culture of positivity take hold.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Power of No_** **, by James and Claudia Altucher**

Now that you've learned about the power of "Yes, and" and the ways that "no" can limit opportunities, it's time to take a look at the other side of the issue. In _The Power of No,_ authors James and Claudia Altucher outline the right times for saying "no."

In the blinks to _The Power of No_, you'll learn how to follow your dreams and express gratitude — by being able to say "no" to the things that are damaging your relationships and personal growth. To harness the potential of this little word, head on over to the blinks to _The Power of No_!
---

### David Ahearn, Frank Ford, David Wilk

David Ahearn, Frank Ford, and David Wilk co-founded Four Day Weekend and are Entrepreneurs-in-Residence at Texas Christian University. As leaders in culture transformation and team empowerment, they've performed thousands of shows to audiences around the world — including military veterans, company professionals, teachers, students, and two US presidents.

© David Ahearn, Frank Ford & David Wilk: _Happy Accidents_ copyright 2017, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

