---
id: 580b6259d4d2ce0003dd242a
slug: happy-city-en
published_date: 2016-10-24T00:00:00.000+00:00
author: Charles Montgomery
title: Happy City
subtitle: Transforming Our Lives Through Urban Design
main_color: 2298AB
text_color: 186A78
---

# Happy City

_Transforming Our Lives Through Urban Design_

**Charles Montgomery**

_Happy City_ (2013) explains how urban planning can help us live healthier and more joyful lives in the big city. From the history of urban sprawl to design blunders, to strategies that encourage residents to socialize, relax and exercise, these blinks reveal the hidden features that can make or break city life.

---
### 1. What’s in it for me? Learn how urban design can foster happiness. 

New York, Tokyo, Paris, London, Berlin — these are among the most vibrant cities in the world. Each has its own particular vibe — and it's the vibe of a city that makes it famous, that enchants both tourists and citizens, that signals the vitality, creativity, sociability and happiness of the people who live there.

But what makes for a city's positive vibe? What is it that makes residents feel content? Are there ways to actively _create_ a happy city?

Well, yes, there are.

In these blinks, you'll learn how urban design influences our happiness; how cities can be designed to reduce stress and foster social interaction, communality and social equality; and how sensible city planning can inspire and make us feel truly happy.

You'll also find out

  * why parks with no trees are good, but not great for our happiness;

  * why driving a car in a city is a harrowing experience; and

  * why California won't necessarily make you happier than Ohio.

### 2. City suburbs were designed to make us happier, but things haven’t quite worked out that way. 

If you took a trip back in time to visit your favorite nineteenth-century metropolis, it probably wouldn't be your favorite city anymore. Back then cities were dirty, disease-ridden and overflowing with people. This miserable situation led urban planners to ask themselves: What could we do better?

In the early twentieth century, urban planners realized that cities would be better off if they were spread out over a larger area. This seemed like a big improvement on the cramped cities of the industrial revolution — plus, the newly invented automobile allowed city-dwellers to escape to the country every now and then.

And so the suburbs were born. For a while, townspeople were able to live healthier lives than they had in the congested city centers.

Fast forward to the present, however, and it would appear that the tables have turned. The modern city center offers far better living standards than it did in the past, while living on the outskirts of major cities has left citizens unhappy and exhausted.

Suburban residents are, after all, removed from many things. All destinations — from schools to medical facilities and even bars and places to hang out — require a long commute. People who live out in the suburbs spend more time on the road, which generally leaves them more worn down than inner-city citizens.

In 2008, two economists compared German citizens' estimates of how long it took them to get to work with how satisfied they were with their lives. A pattern emerged: the further the commute, the less satisfied these people felt.

Spending all your time on the road also leaves you with less time to socialize. This too impacts your overall happiness. To prove the point, economist John Halliwell studied the Gallup World Polls from 2003 to 2010. He found that, when it comes to what makes us happy with our life, relationships with other people trump everything else — yes, even income.

While the suburban sprawl was a great idea in theory, it made people unhappier in practice. So, can cities still turn things around?

> _"The street wears us out. It is altogether disgusting. Why, then, does it still exist?"_ \- Le Corbusier

### 3. Banning cars and staying on top of maintenance keep public spaces appealing. 

How can you bring people closer together? Here's an idea: create a space where they can all hang out. That's what urban planners do when they create common spaces — but not all common spaces are created equal. Traffic and wear and tear can make that public park a lot less fun to visit.

Cars make a lot of noise, which leaves us feeling distracted, unsafe and unwilling to spend time outside. This is enough to keep people from getting in touch with their neighbors. A 1971 study of two San Francisco streets documented the behavior of the people who lived there. In the street with less traffic, each resident had, on average, three local friends and six local acquaintances. Those living in the street with heavy traffic typically only had one local friend and three local acquaintances.

Back in 1962, Copenhagen's City Council decided it was time for a quick fix for their traffic problem. Cars were prohibited to drive on downtown roads, creating a network of car-free roads called Strøget. The experiment had its fair share of detractors, most of them arguing that the Danish simply weren't people who hung out on the street. Little did they expect that the Strøget would soon be filled with happy urban residents walking, chatting and watching the world go by.

Aside from banning cars, you can also make cities nicer to socialize in by keeping public spaces neat and tidy. Garbage, graffiti and cracked pavement have been clinically proven to create subconscious feelings of fear and anxiety, particularly among the elderly. We're much more likely to visit places that are clean and cared for. Why? Well, whether we realize it or not, they make us feel safe.

Public spaces can give city residents a moment of respite. But when it comes to creating new public spaces, there are many approaches to choose from: What kinds of public spaces should every city have? And what's the best way to design them?

> _"A person is so far formed by his surroundings, that his state of harmony depends entirely on his harmony with his surroundings."_ \- Christopher Alexander (in _The Timeless Way of Building_ )

### 4. Parks that are small but dense and diverse make urban residents happiest. 

Nature is an essential feature of every urban landscape. Even the smallest natural spaces can help city-dwellers feel better.

Improvised experiments at the BMW Guggenheim Lab in Manhattan captured the way people felt while walking the streets of the Big Apple in a tour group. According to participant reports, they felt most unhappy while passing the bare facade of a public housing project; but, just a little way down the road, there was a restaurant built in almost the same way as the housing project, but with a brown wall on which vines were growing. This small addition of nature led passersby to declare this the happiest spot on the tour.

This goes to show that you don't need huge plots of land to satisfy a need for nature. Small natural spaces are enough to work wonders on our mood, especially if they're filled with lots of different examples of flora. Most city lawns and parks are wide open spaces with few trees. But research shows that the more varied and complex a garden landscape is, the happier it will make us.

Biologist Richard Fuller and his colleagues tested this in a survey where participants shared their experiences with parks in the area around Sheffield, England. Surely enough, the visitors of parks with a dense and diverse array of plants and animals reported more positive feelings than those who visited parks with fewer trees and wide lawns.

In this way, cities can be designed to help us get in touch with nature on a daily basis. But this isn't the only psychological benefit urban planning can have; some urban environments facilitate our interactions with other people, too.

### 5. Crowdedness makes us want to hide from the world; a good city helps residents escape when they need to. 

Cities that make people happy must pull off a special balancing act. They need to be able to bring people closer together — without, of course, bringing them _too_ close, like cities of the nineteenth century did.

Ever lived in a busy, lively city and found yourself craving more and more time alone? You might not be the only one. It's been suggested that the crowdedness of large cities leads directly to isolation. Social psychologist Stanley Milgram pointed this out when comparing the behavior of people in small towns to that of people in big cities. People in small towns were much more willing than city-dwellers to help strangers, for instance.

Milgram reasoned that this was the result of the sensory overload created by crowded cities. When we're surrounded by bustling crowds, our nervous system tends to get a little overwhelmed. As a result, we're more likely to create protective barriers by distancing ourselves from other individuals.

The same phenomenon also became apparent back in 1973, when psychologist Andrew Baum studied the behavior of students living in two different dormitories at Stony Brook University in New York. In the first dormitory, students lived along one long corridor, with a shared bathroom and a communal lounge area at the end. The same amount of students lived in the other dorm, but the floor was separated into suites with a shared bathroom and lounge areas to be shared between three student bedrooms.

The students living in the first corridor complained of stress and unwanted social interactions, and were less likely to make friends with other residents. Students living in the dorm with suites, on the other hand, talked with each other more and were more likely to help each other out.

So, the ideal city offers us social environments that balance our need for privacy while encouraging us to engage with a small group of fellow citizens. The thing is, you'd be hard-pressed to find a city that does this perfectly. Urban planning is complex, and a few key biases mean that we tend to get things wrong more often than we get them right.

### 6. Decisions concerning urban life aren’t immune to bias and poor planning. 

There's a whole host of psychological phenomena that can lead us to make terrible judgments about city planning. Whether you're picking a new place to live or designing a future city, you should be on the lookout for biases and shoddy plans.

Let's test this out with a simple question: Would you rather live in California or Ohio? If you're like most people, you'd pick California hands down. After all, the California we imagine is a world of perfect weather and great surfing. Ohio, on the other hand, just makes us think of bleak winters.

The thing is, though, that climate isn't the only important factor that determines our happiness in urban environments. Crowdedness, pollution and the way social networks function in different cities can all later emerge to bug us in what we thought was the city of our dreams. So, do your research and think twice next time you're contemplating moving cities!

Unfortunately, urban planners aren't immune to poor judgments like these either. All too often, big decisions are made based on the situation in the present, without considering how things might evolve in the future.

For instance, 1960s Atlanta was facing serious traffic problems. The obvious solution? Build more roads! The highway expansion project seemed to have solved the problem — at least, at first. In the long run, the better roads gave people a reason to buy more cars, and after five years the city streets were jammed once again.

Also in the 1960s, architect Oscar Niemeyer designed the modernist city of Brasília as the new capital of Brazil. His vision was praised as a city perfectly designed for an orderly, healthy and egalitarian future. The chaos of the typical Brazilian city was nowhere to be found in his simple, clean and geometric design.

However, the end result left inhabitants feeling disoriented. Their perfectly ordered surroundings were strange and lonely, something that Niemeyer had never anticipated.

### 7. Self-propelled mobility makes us happier, and clever cities know how to encourage this. 

It's a shame that so few city-dwellers choose travel by bike or on foot. Cars, whether on the street or in parking lots, take up a disproportionate amount of public space. And, as you probably know, driving through a city is hardly ever a relaxing experience.

It makes sense, then, that people who ride a bike to work are happier than those who drive. In a traffic jam, our body releases stress hormones. When these hormones build up over time, they can weaken our blood vessels, bones and immune system — not a great thing to put yourself through before a long day at the office!

Self-propelled commuters, on the other hand, actually enjoy their walks or rides to work. Even children have been known to prefer walking to school rather than being driven there. Clever city planners will capitalize on this by designing environments in a way that encourages residents to move themselves.

Even glamorous Paris did this when they introduced the Vélib' bicycle-share system. Stations with bicycles for residents to borrow were installed throughout the city, allowing residents simply to walk up and borrow a bike from one station, then drop it off at the station nearest to their destination. This was enough to drastically increase the number of people moving by bike in the city.

Roads that look safe and interesting can also make walking seem like less of a chore. In London and New York, simple improvements serve to encourage residents to get on their feet more. Rather than filling the city with drive-in malls, where people leave their cars and only shop within the building, people prefer walking along the trendy street, even if shops are more than 15 minutes apart.

### 8. The happiest cities are driven by urban planning that redistributes resources to the less privileged. 

In the early 2000s, Enrique Peñalosa, the mayor of Colombia's capital, Bogotá, made some radical changes to the lives of urban residents. He introduced car-free days, fast bus lanes, better bicycle paths and reduced traffic on central avenues — all of which made the people of Bogotá much happier, especially the less privileged.

The benefits of driving a car are great, but only if you can afford it. For others, cars take up a lot of private and public space. On the other hand, just about anyone can afford a bike or a public transport ticket. Both require a lot less space and also can help boost everyone's well-being. Enrique Peñalosa knew this when he took public space away from cars and used it to make room for the TransMilenio, a fast bus system that every commuter could benefit from.

Interventions like these aren't just urban improvements — they also have political significance. Changes in the public space of Bogotá left some of its wealthier, car-owning citizens angry. But, despite their protests, Peñalosa stood by his decisions. And it paid off! Today, TransMilenio is a success story that's sparked imitators in cities around the world.

Meanwhile, people in Bogotá reported that their lives were improving, a feeling they hadn't experienced in decades.

### 9. Final summary 

The key message in this book:

**While many modern cities make for wasteful, crowded and unpleasant living spaces, urban planning offers innovative strategies to make the most out of metropolitan environments. By helping us enjoy nature and our community, and encouraging energy-efficient modes of transport, the changes suggested in these blinks can help turn any urban environment around.**

Actionable advice: ****

**Do your research!**

Next time you're thinking about moving to a new city, do your research and plan a visit. This will help you take all the important factors into account — from traffic to public transport to crowds and public parks — and then work out whether the city will drive you crazy, or whether it's designed to keep you happy.

**Suggested further reading:** ** _Triumph of the City_** **by Edward Glaeser**

_Triumph of the City_ extolls the virtues of the city as one of civilization's greatest inventions. Cities not only connect people but also help them accomplish great things. And although many of today's urban metropolises face real challenges in a new economic order, there are many ways for cities to succeed.
---

### Charles Montgomery

Charles Montgomery is an acclaimed journalist, specializing in urban engagement. In 2005, his book _The Shark God_ won the Charles Taylor Prize _._

