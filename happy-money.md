---
id: 5450254c3831390009250000
slug: happy-money-en
published_date: 2014-10-31T00:00:00.000+00:00
author: Elizabeth Dunn & Michael Norton
title: Happy Money
subtitle: The New Science of Smarter Spending
main_color: EF4C30
text_color: BD3C26
---

# Happy Money

_The New Science of Smarter Spending_

**Elizabeth Dunn & Michael Norton**

_Happy Money_ (2013) explains how you can and should spend your money to maximize your happiness, through five simple principles that can be applied to your everyday life.

---
### 1. What’s in it for me? Start spending your money in a way that maximizes your happiness. 

In a world where so much attention is devoted to money and happiness, it's bizarre that the link between the two is so poorly understood.

Data and research consistently show that happiness isn't related to how much money you have or how much money you make, but rather on how you spend it.

These blinks highlight five principles which show you how to spend your money in a way that makes you happier. While some may seem counterintuitive, each suggestion is highly applicable in your daily life.

In the following blinks, you'll also discover:

  * how buying a vacuuming robot can boost your happiness;

  * why you should cut up your credit cards; and

  * why you'll be happier if you spend money on others rather than on yourself.

### 2. Cash, cars and luxury houses might seem to guarantee happiness, but it’s not the case. 

Most people assume that winning the lottery would make them happier. They think once they can buy that luxury car or dream beachfront house, then they will truly be happy.

Let's examine this notion more closely.

Sure, there is a connection between money and happiness. In fact, at least 17,000 academic articles have been written supporting the existence of such a connection, so clearly there's something to it.

But we are mistaken in assuming that the connection between money and happiness is linear and automatic; that is, that more money will always make us substantially happier.

One study showed that Americans typically assume that if their income were to double, from $25,000 to $55,000 a year, they'd be roughly twice as happy. But statistics show that this only results in a 9 percent increase in happiness.

So why does having more money not make us as happy as we'd think?

It's simple: your level of income has little influence on how much you smile, laugh or enjoy yourself on a daily basis.

In fact, research has shown that just thinking about wealth can suppress behaviors which would make you happier. In one study, showing participants a photograph of money made them more likely to choose to engage in solitary activities, such as personal cooking classes, rather than social activities which might make them happier, like having dinner with friends.

So while having more money won't necessarily make you happier, having money in general isn't a bad thing. What if the problem actually lies in how you choose to spend your money?

The next five blinks will lay out principles of spending money in a way that will make you happier. ****

> _"What if people spent their money differently — and better?"_

### 3. Things don’t make you happy; experiences do. And the memories you make keep you happy. 

Food costs money. Clothes are expensive. Taxes can't be avoided. Unless you live on a self-sufficient farm, off the grid and somehow under the federal radar, you can't avoid spending money to live.

So instead of treating it as a chore, how can we spend money so it makes us happier?

The answer is to spend less on material things such as cars and electronics and use your cash to have _experiences,_ like attending an open-air concert or having dinner out.

Such advice is backed up by data, too. One survey found that when asked to consider their latest purchases, 57 percent of Americans felt they'd experienced more happiness after spending money on a vacation rather than on a piece of jewelry. Only 34 percent felt the other way around.

Another study showed that American adults over 50 who consider themselves generally happy tend to mostly spend their money on leisure activities, such as going to the movies, attending sporting events, joining a gym, and so on — all experiences.

But what is it about an experience that makes us feel happier?

Experiences create _memories._ Each time we enter a concert hall or baseball stadium, we're filled with sights, sounds and smells that stimulate our senses. Friends or family that participate in the experience help make the memory even more profound, and increase our sense of belonging. A simple object just doesn't have that sort of influence.

And when we think about our memories, we're able to relive that experience and feel happy, time and time again.

Experiences too don't have to be expensive or grandiose to be meaningful. Studies show that even when someone has just a little cash on hand, they'll get more pleasure from purchasing an experience such as listening to a new song rather than buying a cheap product.

### 4. Appreciate the big things by making them bite-sized; frequency equals more pleasure. 

Would it surprise you to hear that most Londoners have never visited Big Ben? Even though this historic landmark is just a subway-ride away, locals just don't seem to bother.

This phenomenon illustrates a tendency in human behavior: when something is abundantly or easily available to us, we appreciate it less.

This means that even your favorite things, like a morning cup of hot coffee, can become less pleasurable if you indulge too frequently.

The way to combat this is to turn these common things back into special treats. Instead of guzzling 10 cups a day, allow yourself a luxurious afternoon cappuccino only every now and then.

This way, by limiting your access to the thing that brings you pleasure, you will paradoxically increase its impact. The afternoon cappuccino will start to feel like a reward and will actually make you more happy.

Another way to increase the pleasure you gain from a treat is to remind yourself how _special_ it is. Don't just think in generic terms, but consider the details: where the coffee beans were grown and how they were roasted to create the cup's rich combination of flavors.

One study showed that car owners increased their appreciation for their vehicles through this approach, too. When a car owner was asked to detail the specific characteristics of her car — its make, model or horsepower — the owner actually enjoyed driving the car more.

A third way to increase your enjoyment is to _divide your treats_ into small pieces.

Studies show that most people would rather experience a greater number of small pleasures than a smaller number of large pleasures. This is because our brain interprets several pleasurable events as being more beneficial than a single, pleasurable event.

Some companies have figured this out, as can be seen for example in the pricing of a massage at the luxurious _Beverly Hills Spa_ in California. While a 90-minute massage costs $230, three 30-minute massages cost $330, as people get more pleasure from three massages than just one.

### 5. Household chores can be outsourced so you can spend more time on pleasurable things. 

It's said that money is the root of all that's evil, but really, there's nothing intrinsically evil about money itself. Money can be very helpful, giving us more time to pursue our passions.

Having money can help you save time by outsourcing chores, for example, such as house cleaning to a service provider, so you don't have to dust and mop yourself.

With money, you can also purchase _time-saving goods_.

A Roomba vacuuming robot, as just one example of a time-saving good, costs approximately $300. Though this may sound expensive, consider how much time you waste vacuuming, and what you could do with that extra time instead. You could perhaps use that time to learn a musical instrument — something that will give you much more happiness than vacuuming.

Yet when you're looking for time-saving goods, don't spend hours hunting for a bargain that might just save you a couple of dollars. Time is precious! You should be spending that time on activities that make you happy, so spend a few extra dollars and relish the time your choice has freed up.

Unfortunately, most people don't seem to understand that money can be used in these ways to increase their happiness.

Since the 1960s, in most countries income levels have risen, but people are still not using their time in ways that make them happier. In fact, studies show that people spend most of their free time shopping or doing housework, both of which increase feelings of tension and depression.

So don't hesitate to outsource or automate activities that aren't making you happier, and at the same time spend money to enhance activities that _do_ make you happier.

For example, if you love reading and listening to music, you can buy yourself an e-reader and better headphones so that you get more from these activities.

### 6. Don’t fall into debt; save gradually and savor the anticipation of getting what you want. 

Check your wallet: How many credit cards do you have? We all have a few, perhaps too many.

The attitude of "consume now, pay later" is widespread; we expect to be able to buy what we want immediately and then worry about the bills later.

But when we consider happiness, the opposite philosophy is far more beneficial for us. The greatest pleasure we gain from a purchase lies in the _anticipation_ of that purchase _._ Studies show that vacationers tend to be happier in the weeks before a trip than on the trip itself.

Researchers call this phenomenon a "wrinkle in time," because of our uneven perception of past and future: future events provoke more positive emotions than identical past ones. We might think more warmly of Christmas early in November, than on its memory in January.

So what does this have to do with credit cards?

Simple: Don't use them. Not only will you be able to enjoy the anticipation of a purchase while you save, but in fact, credit cards are a direct path to debt and sadness.

Credit cards make it far too easy for us to overspend. One study examined a group of students asked to bid on tickets for a sold-out sporting event. The first group was told they had to pay with cash the next day; the other group could use credit cards. Interestingly, the credit card group bid twice as much as did the group that had to pay with cash.

This odd tendency is because people believe paying for an item in the future will somehow decrease the misery involved in paying.

But really the opposite is true: studies have shown that debt decreases happiness. So rather than falling into debt, save gradually and savor the anticipation of getting what you want.

> _"Consume now and pay later."_

### 7. Spend money on others. It’ll increase your happiness more than if you spend it on yourself. 

So far we've seen how you can best spend money on yourself to increase your own happiness. But in fact, an even more powerful generator of happiness is to spend money on others!

We all have an innate need to give, and satisfying it makes us happy. This need can be observed in children as young as two years old, and across all countries and cultures.

A recent study shows that whether you're an American college student buying a scarf for your mother or a Ugandan woman buying life-saving malaria medication for a friend, spending money on others produces more happiness than spending it on yourself.

This is because other people's happiness is contagious: when we see the smile of someone to whom we've just given a gift, we too feel happy. This deepens our connection with that person.

And yet, many of us are wholly focused on accumulating wealth for ourselves, even though we could be happier by giving some of it away.

Consider Bill Gates and Warren Buffett, two of the richest people in the world, both of whom have pledged to give most of their wealth to charity. And after donating 99 percent of his wealth, Buffett said he couldn't be happier with his decision.

If you'd like to follow in their footsteps, organizations like _DonorsChoose.org_ make it easy for you to see how your gifts are making a difference.

The final benefit in donating your money to others is that it also makes you more motivated at work, as you know you're working for a salary that will make a difference in other people's lives.

So why not spend a little less on yourself and become happier as a result?

> _"Spending money on others provides a bigger happiness boost than spending money on yourself."_

### 8. Change your financial habits so you produce more happiness for yourself in the long-term. 

We've covered a number of principles on how you can spend your money to become happier. Now, let's examine how you can apply these principles in your everyday life.

First, be aware that these principles aren't mutually exclusive: apply as many of them as possible for the maximum boost to your happiness.

For example, if you treat a friend to a delicious cappuccino, you're both giving a gift and spending on an experience — a happiness double-whammy!

Second, take a look at your expenses and see if you can reallocate some of your spending to happiness-increasing items.

In 2010, the two biggest spending categories for an average American household were housing and transportation, neither of which contributes much to happiness. Donating to charity, which would produce happiness, was the smallest category.

To address this, you don't need to sell your car and give the proceeds to _Doctors Without Borders_. Rather, just ask yourself if you could move a small sum, like $5 a week, from one category to another.

Third, start saving money to bolster your finances against potential surprises which could dramatically decrease your happiness.

Set a solid target, like saving 5 percent out of every paycheck, so that you can handle a surprise expense, like a house renovation. Just make sure that you save in a way that doesn't hurt your current happiness.

In these ways you can apply the principles to your everyday spending, and become happier in the long run as a result.

### 9. Final summary 

The key message in this book:

**To maximize your happiness, try to use your money in a way that gives you more time and freedom to do what you want. Buy experiences rather than things, and spend your money on others rather than on yourself. But don't just buy stuff; savor the anticipation before and revel in your increased levels of happiness!**

Actionable advice:

**Track your spending.**

Devote the next week to analyzing how you spend your money. Jot down every purchase you make and analyze your spending habits at the end of the week. Then consider how you can best change your habits to be in line with the principles covered in these blinks. Could you spare $5 for a charitable cause? If so, you'll experience a substantial happiness boost!

**Suggested** **further** **reading:** ** _Secrets of the Millionaire Mind_** **by T. Harv Eker**

_Secrets of the Millionaire Mind_ explains how people unconsciously develop rigid attitudes and behavioral patterns in their relationship to money that they learned from their parents — and that will determine their future wealth. It presents the key guiding principles and thought patterns that millionaires live by and anybody who wants to get rich should adopt.
---

### Elizabeth Dunn & Michael Norton

Elizabeth Dunn is an associate professor of psychology at the University of British Columbia in Vancouver. Her work has been featured in top academic journals and hundreds of media outlets worldwide.

Michael Norton is an associate professor of marketing at Harvard Business School, who in 2012 was selected as one of the "50 People Who Will Change the World" by _Wired_ magazine.

