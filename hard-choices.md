---
id: 54eaf7bc393366000aa40000
slug: hard-choices-en
published_date: 2015-02-26T00:00:00.000+00:00
author: Hillary Clinton
title: Hard Choices
subtitle: None
main_color: BEB3AF
text_color: 736C6A
---

# Hard Choices

_None_

**Hillary Clinton**

_Hard Choices_ offers a first-hand account of the trials and impressive diplomatic successes of the early years of the Obama administration. In this telling memoir, former Secretary of State Hillary Clinton places you at the administration's negotiating table where key policy decisions were made.

---
### 1. What’s in it for me? Gain insight into Hillary Clinton’s thoughts over her time as secretary of state. 

In _Hard Choices_, Clinton describes the Obama administration's transition to "smart power," a combination of soft power, allegiances and partnerships with hard power and military intervention. As secretary of state, Clinton oversaw this strategic shift in American foreign policy, with long-lasting consequences worldwide.

Although Clinton's time in office was surely one of the most difficult in recent memory, these blinks explain how she deftly handled difficult relationships with China and Iran while improving ties with Mexico and Japan. You'll also get a glimpse into what she might do differently if she was elected president.

After reading these blinks, you'll learn

  * what Hillary Clinton thought about the Benghazi incident;

  * why US foreign policy has shifted toward Asia; and

  * how Hillary Clinton would tackle women's rights as president of the United States.

### 2. Strengthening relations with Asia was a key component to Obama’s foreign policy. 

When US President Obama assumed office, American foreign policy shifted from a focus on the Middle East to the Asia-Pacific region.

What inspired this transition?

First and foremost, the administration was eager to reassert the United States' position as a power in Asia. Important allies in the region, such as Australia, Japan and South Korea, needed nurturing.

Second, East Asia offers huge economic and strategic opportunities for the United States. More than half of the world's population is centered there, and across the continent, countries are developing rapidly as economies boom.

Some of these emerging powers maintain democratic governments, like Malaysia and Indonesia. The United States sees these as interesting potential partners, both in terms of economic and military alliances.

In contrast, the rogue, nuclear-armed North Korea continues to threaten global security, while China's rising military potential and its expansive politics in East Asia need a counterbalance.

Seeing the need for renewed political efforts in Asia, the Obama administration did a number of things.

One initiative was a proposed trade agreement with Asia and Latin America, called the Trans-Pacific Partnership (TPP). The administration claims the TPP is intended to lower trade barriers and raise labor standards as well as protect the environment and intellectual property. At the time of writing, negotiations over the TPP were still ongoing.

In addition, the United States has renewed its engagement in multilateral organizations such as the Association of Southeast Asian Nations (ASEAN), by participating in most ASEAN meetings.

America has also flexed its military muscle, maneuvering the aircraft carrier USS George Washington to Asia in response to North Korea's continued threats against South Korea.

Furthermore, the administration (including both Clinton and Obama personally) met with Japanese leaders some 14 times in 2010, and offered assistance to Japan for recovery efforts following the disastrous earthquake and tsunami in 2011.

In fact, Clinton's very first official trip as secretary of state was to East Asia; what's more, she visited the Pacific Region some 36 times during her first three years in office. She made seven trips to China alone, as this rising superpower posed quite a diplomatic challenge.

### 3. In dealing with China, the United States has had to balance its own conflicting interests. 

Imagine you own a business. One of your critical trading partners is generous in lending you money, yet he's also a control freak and terrorizes the neighborhood. What do you do?

While simplified, this is essentially the nature of the United States' ties with China.

America has a strong interest in maintaining good relations with China. Both countries are concerned about terrorism and nuclear weapon proliferation, and their economies are closely entwined. China is the largest foreign creditor to the United States and a very important trade partner.

Yet China consistently violates both human rights and international borders.

Indeed, China is an authoritarian state where repression and coercion are routine. In 2010, for example, lawyer and activist Chen Guangcheng was arrested and imprisoned for _legally_ defending citizens' rights against local authorities.

China also strives to expand its territory. Actions such as those in 2013, when suddenly Chinese authorities declared an "air defense identification zone" over much of the East China Sea, exemplify this. In fact the East China Sea is _not_ Chinese territory, and China has no right to create such a zone.

The Obama administration navigates this complicated relationship by attempting to focus on common ground, without forgetting the stickier points where Chinese and American goals diverge.

In 2009, for instance, the administration established a high-level _US–China Strategic and Economic Dialogue_ to discuss North Korea, the financial crisis and other important issues.

The Obama administration has also expanded trade between the two countries, increasing trade volume from $387 billion in 2007 to $562 billion by 2013.

Yet at the same time, the United States has attempted to rein in China's territorial grabs and protect human rights, by encouraging smaller Asian nations to join forces. Some actions include encouraging countries to ignore China's air defense zone, or granting human rights activists asylum in the US Embassy.

> _"We have no interest in containing China. But we do insist that China play by the rules that bind all nations."_

### 4. The Obama administration has worked to reduce the threat from Iran's atomic program. 

As with China, diplomacy between the United States and Iran has long been challenging.

Since 1979, Iran's leaders have dubbed America "The Great Satan." This did not change when Obama was elected.

Iran's nuclear ambitions pose a serious challenge. In 2008, Western intelligence agencies learned of the construction of an enormous yet secret uranium-enrichment facility beneath the mountains of Qom. This facility, it was believed, would bring Iran much closer to building a nuclear bomb.

Moreover, the facility's size and configuration were thought incompatible with the goals of a peaceful nuclear program; and between 2003 and 2009, Iran conspicuously increased the number of centrifuges to enrich uranium from 100 to 5,000, far exceeding the country's nuclear energy needs.

There's no question that if Iran were to possess a nuclear bomb, it would become not just a problem for the United States, but a global problem. Since 1979, US property and troops overseas have been attacked several times, by either the Iranian military or the Iranian-funded group, Hezbollah.

In 1983, for example, Hezbollah bombed the US embassy in Beirut as well a barracks for US Marines, killing 304 people. 

The United States has used many diplomatic measures to impede Iran's atomic plans. Following mounting international pressure, Iran eventually allowed International Atomic Energy Agency inspectors to visit the Qom facility.

And when Iran requested nuclear fuel rods to produce medical isotopes in 2009, the United States offered instead a deal: Iran would be supplied with fuel rods in exchange for most of its uranium stockpile, a strategy which would have delayed a suspected Iranian weapons program for months.

When the Iranian authorities turned down the deal, the United States convinced the international community to impose the most severe sanctions on Iran in its entire history.

But when Hassan Rouhani became president of Iran six months later in 2013, he negotiated a separate deal with the United States to eliminate any highly enriched uranium and in turn, the international community would provide sanctions relief to the tune of billions of dollars.

### 5. Poor communication and inadequate security measures contributed to the tragedy in Benghazi. 

On the evening of September 11, 2012, Islamic militants assaulted the diplomatic compound in Benghazi, Libya, resulting in the deaths of four Americans, including Ambassador Christopher Stephens.

In the weeks following the attacks, the media and the US Congress criticized Clinton, saying that as secretary of state, her responsibility is to keep the people in her department safe.

But was Clinton personally responsible for the tragedy?

Subsequent investigations did find gaps in State Department security procedures. In September 2012, the Accountability Review Board began investigating what went wrong in Benghazi. A report later that month outlined some key issues.

It turned out that the Diplomatic Security office and State Department offices guiding policy weren't working together effectively. Moreover, American civil servants in Benghazi felt that officials weren't considering their security requests a high priority.

While the compound had indeed undergone security upgrades, such as the fortification of its outer walls, these measures were found to be insufficient. All in all, the Accountability Review Board concluded its report with 29 recommendations.

As the head of the State Department, Clinton accepted some responsibility, but maintains that she was not solely responsible and certainly didn't fail her staff.

While important security requests may indeed have been forwarded to Clinton, she couldn't be held personally responsible for the department's lack of response.

All cables written to the State Department are traditionally addressed to the Secretary of State. However, Clinton isn't actually the primary recipient, as she has neither the time nor the expertise to respond to two million security requests per year!

Security matters are instead addressed by officials who are responsible for staff security.

Importantly, in accordance with the Vienna Convention of 1961, the department had contracted local militia to guard the compound. Yet these groups failed to defend it against fellow Libyans.

And finally, the report found that the State Department simply lacked the funds and personnel resources to operate at its full potential.

### 6. The Obama administration achieved considerable diplomatic success in Latin America. 

Mexican drug cartels represent a serious problem for both Mexico and the United States.

Every year, thousands of Mexicans die as a result of drug-related violence, while the activities of organized crime networks impede economic and democratic development.

Ninety percent of all illegal drugs in the United States are transported through Mexico, while 90 percent of the weapons used by Mexican drug cartels come from the United States.

In 2009, both nations devised a multi-pronged strategy to fight drug and gun trafficking.

Mexican President Felipe Calderón requested financial aid to purchase helicopters and other military gear to better fight Mexico's well-armed cartels. The United States promised to both provide that aid and train thousands of Mexican police officers. 

In addition to such measures, hundreds of additional guards were stationed on the American side of the border, to fight gun running and drug smuggling. 

This cooperation has yielded promising results. In 2009, Mexico extradited more than 100 fugitives to the United States. Since the cooperation began, some two dozen high-level drug traffickers have been captured or killed.

Alongside its efforts to cooperate with Mexico, the United States has also worked out a positive compromise for Cuba's membership in the Organization of American States (OAS).

Cuba has been suspended from the organization since 1962. By 2009, however, the majority of OAS members wanted Cuba to join their ranks.

So Venezuelan President Hugo Chavez initiated a resolution to lift the ban on Cuban membership, yet the United States was opposed to making Cuba an active member in the OAS.

Foreign Service Officer Tom Shannon and Clinton worked out a clever alternative resolution: Cuba's suspension would be lifted, but if it wanted to participate, it would have to both formally request readmittance as well as raise its democratic standards.

This resolution offered a compromise all parties could accept, and it passed in June 2009.

### 7. The Obama administration decided on a “reset” with Russia, which initially inspired progress. 

Relations between the United States and Russia have been strained for more than half a century.

How would the Obama administration handle this long-standing animosity?

Obama and Clinton decided on a "reset." The idea was to essentially "let bygones be bygones" and focus on the countries' shared interests instead.

Indeed, since the "reset," the US and Russian governments have managed to find common ground.

Russia and the United States first worked out a new treaty to limit the number of nuclear weapons in both countries. As the Strategic Arms Reduction Treaty (START) was set to expire in 2009, Obama initiated negotiations on a new treaty during his first official visit to Russia.

Throughout 2009, the US State Department and its Russian counterpart worked out a _New START_. The timing was critical; after the original START expired, Russia would not allow any foreign weapons inspectors on Russian soil for almost a year.

But in April 2010, Obama and Russian President Dmitry Medvedev signed the treaty, which was ratified by the Senate some months later.

Over time, the presidents of the two countries developed a good personal relationship that facilitated further cooperation. For example, both Obama and Medvedev agreed to impose strong sanctions on Iran and North Korea, and to expand their cooperation in counterterrorism.

And in 2009, Russia and the United States reached an agreement to allow the United States to transport equipment through Russia to Afghanistan. In return, Russia would earn transit fees.

Things looked promising between the two former Cold War powers until in 2011, when Medvedev announced that he would not seek re-election.

### 8. When Putin was re-elected president, common ground dwindled and the US switched strategies. 

The honeymoon for Obama and Medvedev didn't last. When Vladimir Putin returned to the Kremlin as president again in 2012, US and Russian relations took a turn for the worse.

Russian foreign policy changed with the return of Putin, and as a result, the common ground previously solidified between Russia and the United States began to fall apart. From the start Putin was dismissive of Western overtures, and even declined Obama's invitation to the G8 summit.

In a newspaper essay, Putin announced plans to regain Russia's lost influence on its former Soviet states. He used boycotts and other leverage to isolate these states from the West.

In 2014, Russian troops invaded Crimea, just as the country had invaded Georgia in 2008, another move which soured relations with the Obama administration.

The United States naturally hoped the former Soviet satellites would remain aligned toward the West. What's more, discord was palpable even beyond the former Iron Curtain.

Putin, for example, propped up Syrian President Bashar al-Assad's regime despite the UN's attempt to orchestrate an international response to his brutal crackdown on public demonstrations, demanding Assad's ouster.

So the Obama administration changed tack, deciding to place less of an emphasis on finding common ground between the two countries.

Clinton in particular wanted to keep the United States from appearing too eager to work with a leader like Putin. Consequently, the US government declined Putin's invitation to a Presidential-level summit in Moscow, shortly after he was re-elected.

Clinton further advised the Senate Foreign Relations Committee to strengthen ties with NATO and foster European energy autonomy, to prevent the European Union from depending too much on Russian gas.

So far we've looked at US foreign relations through the lens of the Obama administration. Our final blink will look at what, if anything, Clinton would do differently if she were elected US president.

> _"Putin was under the mistaken impression that we needed Russia more than Russia needed us."_

### 9. Clinton would focus on women’s rights and environmental policy if elected US president. 

What would Hillary Clinton do differently as president?

At the forefront, Clinton would ensure that women's rights were protected on a global level.

Women make up the majority of the world's unhealthy, unfed and unpaid population, often where laws or customs prevent women from pursuing an education or paid work.

All over the world, men overlook or downplay women's vast — and often unpaid or underpaid — contributions to the economy. In many countries, women too are underrepresented or excluded from the political arena.

Of the peace treaties signed since the early 1990s, only 10 percent involved women negotiators, and only three percent had even a single woman signatory.

What's more, sexual violence against women plagues every nation. As a shocking example: in Papua New Guinea, 70 percent of _all women_ will become a victim of rape or physical violence during their lifetimes.

Clinton as US president would also distinguish herself in terms of climate policies.

Some emerging economies are reluctant to take serious steps against climate change, as doing so might mean allocating funds for environmental protections instead of dealing with endemic poverty, for example.

Many environmental regulations too can harm a country's economic interests. Requiring factories to reduce emissions, for example, can threaten to slow economic growth.

But if we do _nothing_ about our changing climate — that is, if we simply bury our heads in the sand — we'll have to endure extreme weather, like tsunamis, heat waves, fires and major floods.

In fact, these extreme weather events are _already_ on the rise.

Moreover, climate change represents a significant national and international security threat. A shortage of fresh water caused by severe drought, for example, could lead to many deaths or a mass exodus, destabilizing weak states and acting as a catalyst for international conflicts.

> _"Never stop working to make the world a better place. That's our unfinished business."_

### 10. Final summary 

The key message in this book:

**The Obama administration faced some tough situations during Hillary Clinton's time as secretary of state. Yet Clinton managed to approach all nations and situations with an open mind, searching for common ground irrespective of past enmities, to impressive success across the globe.**

**Suggested further reading:** ** _The Audacity of Hope_** **by Barack Obama**

_The Audacity of Hope_ is based on a keynote speech Barack Obama delivered at the 2004 Democratic Convention, which launched him into the spotlight of the nation. It contains many of the subjects of Obama's 2008 campaign for the presidency.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Hillary Clinton

Hillary Clinton is a former US Senator and First Lady of the United States, and served as the sixty-seventh Secretary of State during the first Obama administration. She has also written a number of books, including several _New York Times_ best sellers.

