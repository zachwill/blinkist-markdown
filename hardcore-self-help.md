---
id: 590b2d1eb238e10007a619fa
slug: hardcore-self-help-en
published_date: 2017-05-11T00:00:00.000+00:00
author: Robert Duff, PhD
title: Hardcore Self Help
subtitle: F**K Anxiety
main_color: FFD633
text_color: 594B12
---

# Hardcore Self Help

_F**K Anxiety_

**Robert Duff, PhD**

_Hardcore Self Help_ (2014) is your guide to overcoming anxiety and living a peaceful life. These blinks examine some of the different forms that anxiety disorders take and offer some techniques to help ease anxiety.

---
### 1. What’s in it for me? Learn effective strategies to fight your anxiety. 

Every once in a while, you might have a bad dream from which you wake up drenched in sweat. For most of us, nightmares are a familiar but rare experience — but what if your nightmares were to recur every night? And what if they didn't just haunt you at night, but also at the most inopportune times during the day?

For some people, anxiety is a daily companion, an unwanted element and pursuer that is nearly impossible to shake off. In cases like this, you have to fight back.

These blinks will give you advice on how to cope with anxiety. You will learn what type of anxiety you might have, see what you can do to fight it off yourself and understand why it is imperative to consult a therapist nonetheless.

You will also learn

  * how breathing right can hold back your panic attacks;

  * how to explain your anxiety to your loved ones; and

  * why you might want to give your anxiety a name.

### 2. Anxiety comes in different forms, and some are incredibly difficult to live with. 

Do you experience heart palpitations and difficulty breathing? If you're in otherwise good health, these symptoms might be caused by anxiety.

While anxiety comes in a variety of forms, these issues generally fall under the category of anxiety disorders, each of which has different symptoms. Everyone feels anxious at some point in time, but anxiety can also take much more serious forms.

Just take _phobias_, which most people are familiar with; they refer to disorders in which a specific situation or object, like a spider, brings on an overwhelming sensation of fear.

Another type of anxiety disorder is _Post-Traumatic Stress Disorder_, or _PTSD_, which can be the result of traumatic life experiences, like being abused as a child or going to war.

And then there's _Obsessive-Compulsive Disorder_, or _OCD_, which is another anxiety disorder in which fear drives obsessions. The symptoms of OCD range from an obsessive need to touch particular objects at certain times, to personal tics that dramatically restrict daily routines.

So, there are many forms of anxiety and, with the help of a professional, you can determine which type, if any, affects you. If you do have an anxiety disorder, it's essential to treat it, because anxiety can wreak absolute havoc when left to its own devices.

For example, PTSD can cause a patient to relive flashbacks of traumatic events, experience panic attacks and display sudden violent outbursts. The brain of a PTSD sufferer is essentially locked in fight-or-flight mode, which prevents her from living her everyday life, as even little surprises can cause tremendous anguish.

OCD also takes a serious toll on the patient's daily life by forcing the sufferer to obsessively abide by particular routines to keep his life in order. As an example, an OCD patient might have an obsessive need to open and close the door 12 times before leaving the house. His anxiety may make him believe that if he fails to do this, his entire family will die a violent death.

These are just a few of the ways anxiety can rule your life — but it needn't. In the next blink you'll learn precisely how treatment can help you regain control.

### 3. Anxiety is bred by your body and your mind. 

For many people, the situation is familiar: you're lying in bed, ready to fall asleep, but just can't seem to turn off your brain.

In fact, lots of people experience mental anxiety that can keep them awake through the night and even bring on panic attacks. So, where does this anxiety come from?

One common source is _catastrophizing_, in which anxiety builds up to the point that you imagine an otherwise ordinary situation spiraling out of control.

For instance, say you have a test tomorrow. Rather than accepting that you've prepared and studied well, you imagine failing the exam. You go on to fantasize about getting rejected from college and eventually imagine how you'll never be able to get your dream career on track. These scenarios, where things get blown way out proportion, are common.

Another form that anxiety frequently takes is to imagine what other people think of you at any given moment, skewing toward people seeing you less than favorably.

To illustrate, if your partner arrives home in a bad mood, you might think it's all your fault. You fear that you've done something wrong and end up trapped in a downward spiral of uneasy feelings.

In these ways, your mind can cause anxiety attacks, but so can your body.

This type of anxiety is a product of intense fear and biological predispositions to certain symptoms, like heart palpitations or an upset stomach. Combinations of such symptoms can lead to a panic attack, even without your mind spiraling out of control. What's worse is that panic attacks brought on by the body can come seemingly out of nowhere.

There are also cases of people who are themselves biologically predisposed to panic attacks, and their brains readily switch into panic mode without any external impetus.

The randomness with which such attacks occur often causes immense stress to the people who suffer from them. They fear the possibility of another attack, which breeds anxiety and makes them more likely to actually have one. These episodes result in shortness of breath, dizziness and a feeling of being choked.

### 4. Breathing techniques can help give you a break from your anxiety. 

In a Hollywood movie, when a character has a panic attack, they often try to slow down their breathing to regain control. This happens to be one lesson from the movies that actually works — conscious breathing is, in fact, a great way to counteract anxiety.

By focusing on your breath, you allow your body to calm down, but breathing also distracts you from whatever triggered the anxiety. The explanation for this is really quite simple: people just can't multitask. If you pay close attention to your breath, your mind will relax since it can't focus on the cause of your anxiety too.

It's a good idea to practice conscious breathing to make sure you're prepared for the event of a panic attack. To do so, just take a deep breath while counting to four, hold it as you count to seven and then slowly breathe out at eight. You'll find that in no time at all, you're relaxing and letting go.

So, breathing can help stop a panic attack in its tracks, but how do you prevent one altogether?

Well, you can reduce the risk of suffering such an episode by giving yourself relaxation breaks. This is especially vital since, in modern societies, we tend to make ourselves constantly available to others. Our phones are always receiving messages, and because we frequently collaborate with people from across the globe, we often end up working late.

The constant low-level stress this produces is terrible for our mental health and increases the chances of panic attacks. So, to counteract it, just shift things down a gear.

For instance, clearly define your office hours and only work during these times. You could also download your e-mails and go to a cafe without WiFi where you can respond at your leisure, offline, without new distractions popping up. The key is simply to not be available all the time.

By taking time for yourself, you'll be able to concentrate more on being in the moment and will be less likely to fall into a spiral of stress and anxiety.

> _"Symptoms of panic are fundamentally incompatible with deep breathing."_

### 5. It can be difficult for other people to understand your anxiety, but you can give them a hand. 

Anxiety is an isolating experience and can make you feel insecure. But this feeling gets even worse when the people around you don't understand how or why you suffer.

This is a major issue, as many people without anxiety disorders of their own struggle to understand the anguish that anxiety can cause in the lives of others. This is due in large part to the fact that mental illnesses, like anxiety, are totally different from physical illnesses.

Physical illnesses are visible, making them easy for others to witness and understand. But with mental illness, it's difficult to grasp or imagine the problem without suffering from it yourself.

However, that doesn't mean the people in your life don't want to help you. Your family and friends will likely try to support you, but they won't know what's helpful and what's not. In the end, this will probably just cause you more stress.

That being said, you can help the people around you to get a better grasp of your anxiety, and one strategy for doing so is to write a letter.

A letter will let you get all of your feelings out on paper so that the other person can sit down with them and really take them in. In this letter, you might tell your loved one how anxiety makes you feel, how appreciative you are of them and explain that you know how difficult it can be to be around someone with an anxiety disorder.

But it's also important to tell them that you might not always want to talk about everything you're feeling — that you need some time to work through your issues on your own before you bring someone else into the inner chaos of your anxiety.

By getting this out in the open, you'll be helping those around you understand and support you in positive ways.

### 6. Battle your anxiety with professional help and keep reminding yourself how far you’ve come. 

By now, you've got a few tools at your disposal for counteracting your anxiety. But sometimes, you just can't go it alone, and it's always much easier to fight anxiety with professional help.

Some people think that medication is sufficient, but on its own, it will never be enough. It can certainly help some people, but it tends to be much more effective to find a good therapist who can guide you in unpacking the causes of your anxiety.

Beyond that, therapy is a tremendous opportunity to trust another person who can push you to improve, while recognizing your concerns. Therapy can help you figure out what type of anxiety you have and show you how to treat it without causing more harm. This is crucial since, when left untreated, anxiety can cause you to fall into a downward spiral of depression and even suicidal thoughts.

During therapy, it's a great idea to focus on the progress you're making. Doing so will help you keep your head up during hard times, which is especially important because your fight against anxiety may at times feel like a never-ending battle with few victories.

By focusing on all the gains you've made since your first panic attack, even if they came in small steps, you'll boost your confidence to continue the fight.

A good way to do so is by personalizing your anxiety. For instance, the author named his anxiety "Fred" and started thinking of him as a person rather than an illness. Whenever he had a bad day, it was because Fred was in a bad mood and was trying to destroy his life.

By working toward getting Fred out of his life, he slowly found that Fred was less and less present over the years. In the end, this was the motivation the author needed to "kick Fred's ass" out for good.

### 7. Final summary 

The key message in this book:

**Anxiety disorders can be hugely debilitating, but there are straightforward techniques that can help you calm yourself down and treat yourself better. That being said, you can't always fight anxiety alone; professional advice, as well as conversations with friends and family, are essential to helping you free yourself from anxiety.**

Actionable Advice

**Practice 4-7-8 breathing every day.**

Every day, take 15 minutes to calm down and relax. Begin breathing, focusing on the sensation of the air entering and exiting your body.

First, try to breathe in for four seconds, hold your breath for four more and then breathe out for a final four. Then, change the pattern to 4-7-8 for a few minutes; breathe in for four seconds, hold your breath until seven and then release at eight.

By practicing conscious breathing in a safe, nonstressful space, you'll be able to apply it when you most need it: during a panic attack that comes out of nowhere.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _My Age of Anxiety_** **by Scott Stossel**

This book offers a candid, valuable glimpse into the world of the clinically anxious. The author takes us through his personal struggle with anxiety while presenting us with scientific, philosophical and literary work about the condition and the treatments available for it.
---

### Robert Duff, PhD

Dr. Robert Duff is a psychologist on a mission to teach people about mental health in an accessible way. In addition to writing, he hosts a mental health podcast on his website, _www.duffthepsych.com_.

