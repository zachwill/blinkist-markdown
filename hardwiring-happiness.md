---
id: 572c5cd3d77ba60003517013
slug: hardwiring-happiness-en
published_date: 2016-05-10T00:00:00.000+00:00
author: Rick Hanson
title: Hardwiring Happiness
subtitle: The New Brain Science of Contentment, Calm, and Confidence
main_color: 2FB9EC
text_color: 1C708F
---

# Hardwiring Happiness

_The New Brain Science of Contentment, Calm, and Confidence_

**Rick Hanson**

_Hardwiring Happiness_ (2013) isn't just another self-help book singing the praises of positive thinking. It presents the latest research behind the neuroscience of happiness and explains how you can reprogram your brain to focus on the good, rather than obsessing over the bad.

---
### 1. What’s in it for me? Learn about your negativity bias and how to overcome it. 

Did you ever notice that, on the evening news, hardly any positive information is presented? Why is that? The bleak truth is, we like to hear bad news; we have what is called a negativity bias. Since the dawn of humanity, we've always been drawn to negative information. But worry not! We can overcome this bias and learn to be happy.

In these blinks, you will learn about your inbuilt tendency to pay attention to everything that bothers you and to ignore everything that makes you smile. You will learn how the structure of your brain can make for happy or sad thoughts, and how applying some simple techniques can foster positive thinking and reinforce happiness.

You'll also learn

  * why positive feedback is unlikely to stick in your mind;

  * that our brain functions as if it's being pursued by a ferocious predator; and

  * how recollecting the taste of chocolate can bring you up when you're down.

### 2. Having happy or sad thoughts depends on the structure of your brain, but people tend to focus on the “bad.” 

When you were growing up, did you get along with everyone and easily fit in? Or were you constantly on the sidelines, getting teased and retreating further inward? Even if you were popular on the schoolyard, you probably share some common traits with those who easily feel rejected.

This is because bad experiences trigger stronger and more memorable emotions than good ones.

For example, think of the last job evaluation you received: It may have been brimming with compliments and positive feedback. But if it contained one small criticism, you probably ended up fixating on it, instead of all that praise.

That's how it is for most people, because humans have a built-in tendency to focus on the negative rather than the positive.

In fact, in 2001, psychologist Roy Baumeister found that people pay more attention to angry faces than to happy ones. So, when someone glares at you, your subconscious immediately picks up on the hostility.

All that said, your tendency to focus on happy or sad thoughts depends on a certain part of your brain. Some people have what scientists call a "happy amygdala," the amygdala being the part of the brain that's in charge of emotional responses.

Research shows that a happy amygdala heavily stimulates the _nucleus accumbens_ — the part of the brain that drives us to fulfill our goals. People with happy amygdalas tend to be optimistic, focusing on opportunities rather than difficulties. In turn, these positive thoughts can strengthen our desire to take action and achieve our goals, thereby creating happy experiences and generating positive feedback to the brain.

Unfortunately, the majority of people have a "sad amygdala." This leads to fear-based reactions that release cortisol and adrenaline in the bloodstream and make us feel anxious and edgy.

In the next blink, we'll take a closer look at this gloomier brain type and learn what can turn a frown upside-down.

### 3. The human brain is constantly evolving and can change for the better – or for the worse. 

Chances are you've seen pictures of a human brain: It's that spongy organ in our head that sort of resembles a weird cauliflower. As you undoubtedly know, this odd-looking thing is extremely complex, constantly learning and developing.

Far from a static entity, the brain is changing all the time. In fact, it changes with each experience we have.

Consider the 2000 study by neuroscientist Eleanor Maguire: She found that London taxi drivers have a particularly large lump in their _hippocampus_, the part of the brain responsible for memory and visual and spatial orientation.

No, the lump wasn't a brain tumor. It was actually a sign of the area being overdeveloped due to the amount of memorization taxi drivers undertake in order to navigate the streets of London. The drivers were constantly exercising specific neurons in this area of the brain and, like a muscle, it eventually became bigger and stronger.

Exercises like these allow our brain to grow and develop, and in the same way we can also train our brain to be happy.

In 2013, psychologist Wil Cunningham found that children who grow up with parents that fail to provide a warm and loving environment can develop the "sad amygdala" we learned about in the last blink.

Fortunately, these brains can relearn happiness.

Psychologists such as Stanley Schachter guide patients through mental exercises in which they imagine being surrounded by a loving family and receiving positive reinforcement. Repeated exercises like these can gradually change the structure of the brain by strengthening its ability to feel happiness and turn the sad amygdala into a happy one!

So, if our brain can be rewired for happiness, why is modern society filled with so many sad people? Let's find out in the next blink.

### 4. Evolution has created a brain that zooms in on all the small stressors of modern life. 

If you were ever the last one chosen for a game of soccer during school recess, then you may be familiar with what happens as a result: The bloodstream gets flooded with stress hormones, the heart races and it feels like it's a matter of life or death, not a childish popularity contest.

Such overblown human reactions actually date back to when feelings of panic often did save lives.

During the rather violent course of human history, the threat of death caused human beings to pay special attention to anything unfavorable.

In prehistoric times, human beings were always under threat of violent attack, whether by another person or by a wild predator. Our survival was constantly at risk.

As crazy as it may sound, soldiers who fought in WWI or WWII had a better chance of surviving than our hunter-gatherer ancestors from 10,000 years ago. During the two world wars, one soldier out of every 100 died; 10,000 years ago, one person out of every eight met a violent end.

Today, our brain retains the traces of these terrifying times, and it has evolved to find causes for anxiety all around us.

This explains why an unfriendly face, a loud noise or a speeding car might startle us: we still pay special attention to things that could potentially have a negative impact.

But that's not all. The permanent stress of modern life constantly activates our human fear of death.

Whether we are facing an armed robber or a stressful deadline, the exact same neural connections are activated. This means we are stressed out pretty much all the time; we worry about money, our job, politics, relationships, anything!

For our brain, it's like spending the whole day being chased by a 10,000-year-old saber-toothed tiger. Essentially, we function as if our life is constantly in danger, and this has us focusing on all the negative things.

### 5. Human beings have a negativity bias that strongly affects them, but happiness can bring relaxation. 

So far we've learned a lot about how and why our brain focuses on the negative aspects of life.

But this tendency is actually so common that it has a name. It's called the _negativity bias_.

We can see this bias in the evening news. Each episode begins with attention-grabbing bad news. We're simply riveted by stories of tragic earthquakes or violent criminal acts.

But bad news doesn't just affect nightly news ratings; it also strongly affects our happiness.

Whenever we are confronted with negative input our nervous system goes on alert. In fact, our bodies will react as if the crimes and tragedies we see are actually happening to _us_.

This negative input activates a fight-or-flight response: Stress hormones like adrenaline and cortisol are released into the bloodstream and energy resources are drained, preparing us to react quickly to the perceived threat. Naturally, our mind falls into an anxious state.

As a result, we tend to react to negative input — bad news, road traffic, challenges at work — with fear or aggression.

This is how we end up with road rage, frantically honking the car horn and yelling. Or we might even end up shouting at our co-workers when things are going badly.

On the other hand, we can use positive input to help us fall into a healthier, more relaxed state.

One of the author's patients would combat panic attacks with trips to her garden. Whenever she felt panicky, she would step out for a few minutes to calm herself and be soothed by nature.

These calming experiences help relax the nervous system by slowing down the heart rate, lowering blood pressure and even encouraging good digestion. All of this helps reduce the stressed-out, aggressive mindset.

We'd all like to be healthier and less anxious, but what else can we do to overcome our negativity bias? Let's have a look in the next blink.

> _"The negativity bias doesn't mean you can't be happy. But if you're happy, you're happy in spite of it."_

### 6. There are ways to be consciously on the lookout for the positive aspects of life. 

Have you ever found yourself enjoying a rare moment of happiness and noticing things you hadn't before? You might suddenly notice that the birds are chirping, the flowers blooming, and life seems all right again. Well, such pleasant moments needn't be so rare.

Bringing positive input into your everyday life can actually be done quickly and with ease.

For example, when you complete a task, even a small one like answering an e-mail, don't just move onto the next thing. Stop for a moment and recognize that you've accomplished a goal and let yourself feel good about it.

You can also allow yourself to take some time in the morning to open the window and breathe in some fresh air. Let the positive input sink into you, then consciously start the day with that happiness and allow yourself to keep that frame of mind throughout the day.

At first, you might need some reminders to help you recognize positive input.

One idea is to start a "Good Year box." At the end of every day, take the time to think of at least one positive thing that happened, write it down on a piece of paper and place it in the box.

In the moment, this will train your brain to recognize reasons to be happy. And, at the end of a year, you can open the box, read your notes and happily reflect on all the good experiences you had.

Another helpful practice is to start each morning by taking some time to focus on something positive. It may be as simple as acknowledging the fact that you're in good health, or that you're waking up in a quiet and safe place.

This practice will help you let go of any negativity you may be feeling and train your mind to focus on things you can be grateful for.

### 7. We can counter our negativity bias by strengthening our positive experiences. 

Have you ever looked out over a beautiful landscape or admired a stunning sunset and felt the desire to pause time, to sustain that moment forever? Well, you should! Indeed, we can do exactly this with all our positive experiences, even the smallest ones.

While positive thinking isn't a new idea, recent research suggests that we can rewire our brain's negativity bias into a positivity bias by strengthening our positive experiences.

And you can strengthen any positive experience by simply taking the time to relive and savor it.

Try this exercise: bring up the memory of eating your favorite food. It might be as extravagant as a dinner you had at a great restaurant or as simple as your favorite vanilla ice cream. Now, try to imagine the pleasure that that food gave you.

While you're doing this, stretch out this mental experience for as long as you can. Keep that delicious and happy feeling going; if you get distracted, just try to bring yourself back to it. You can do this exercise to strengthen any positive experience and help move yourself toward a positivity bias.

We've seen how our mind prioritizes negative input, so we have to consciously work on staying positive.

It's sort of like ice-skating: sustaining positivity is like balancing on the ice. Strengthening that experience and staying with the happiness is like gliding around and skating a figure eight.

To further strengthen your positive experiences, change your attitude and make time for good things.

For example, say you struggle to get your kids out of bed and ready for school. Instead of getting frustrated and scolding them, try a positive approach. Lie down with them and playfully coax them out of bed and into the day.

Not only is this more fun and positive, it's also more effective; it creates the kind of happy experience that you and your children can then strengthen and hold onto.

> _"You've been gifted by the universe… Every atom heavier than helium… was born inside a star. You're literally made out of stardust."_

### 8. Happiness can heal past traumas and ameliorate feelings of pain and grief. 

Let's face it: life can be harsh. A difficult childhood or the traumatic loss of a loved one — these are painful experiences to live with. But there is hope. Even a small moment spent with a beloved pet can help heal old wounds.

Trite but true: new positive experiences help us overcome old traumas — even ones that have haunted us since childhood.

For example, one day the author was dogsitting a pair of Cardigan Welsh corgis. As they were playing, he would lie down and they would run up to him and playfully lick him, much to the author's delight.

In fact, this was such a happy event for him that he decided to put it to good use.

He used the positive experience to counter a painful childhood memory. When the author was four, his grandmother had locked him out of the house, telling him the cows would eat him while he was stuck outside!

But after his dogsitting experience, he began to merge the two events, linking the happy times with the corgis to the past trauma. Now, whenever he recalls his grandmother, instead of negative thoughts, he automatically thinks of the positive experience with the corgis.

Even in life's worst situations, happiness can heal the pain and grief.

The author tells another story about a woman who lost her beloved cat. At first, she was, naturally, devastated, spending days full of pain and grief. But then she started searching for ways to be positive.

She brought up her memories of all the positive experiences she had had with her cat and savored them, letting them sink in for at least 30 seconds. She was strengthening the events, and this gradually healed the wounds caused by the loss.

Another example is when the author was diagnosed with cancer. Even then, he found that there was positivity to be taken from this experience. He realized how delicate life really is and could therefore value and enjoy his time even more.

> _"If you take care of the minutes, the years will take care of themselves."_

### 9. By creating new positive experiences your life will be happier and much more fun. 

Contrary to the opinion of some, happiness is not in short supply. Truth is, there is no limit to the amount of positive experiences you can create for yourself to help rewire your mind toward happiness.

If you need a positive experience to strengthen, you could create one by imagining yourself in a place you've always wanted to visit or surfing the perfect ocean wave. Or you could increase the chances of having a new experience by walking a path you've never taken and noticing new things.

In fact, finding pleasure in details you've never noticed is a great way to create positivity.

For example, while he was writing, the author found new appreciation for the clever design of his keyboard. Admittedly, this positive experience was a very small one. But any positive experience, as long as it makes you happy, deserves to be valued.

These experiences can also help us conquer our fears.

Even the author found it difficult to enter college: He was shy and burdened with past experiences of being picked on at school. So, when his roommate invited him to hang out with a group of girls, he wasn't exactly eager.

But he forced himself and the evening turned out great, creating a new positive experience that helped him overcome his fear of social situations. In the following days he could replay the event to strengthen it and help him stay happy.

Sometimes these experiences stem from good intentions.

Neuroscientist Jorge Moll found that people who give money to a good cause end up happier. His 2006 study showed that the reward centers in the brain are firing at a higher rate in altruistic people than in those who remain stingy with their cash.

As it turns out, there are many benefits to being altruistic. By feeling happy for a friend who has a positive experience, rather than feeling jealous or threatened, you will multiply your _own_ happiness.

After all, it's much more fun to share happiness than to try to hoard it.

> _"Liking without wanting is heaven, while wanting without liking is hell."_

### 10. Final summary 

The key message in this book:

**We need to rewire our brains in order to focus on the positive. Our brains still carry the instincts of our prehistoric ancestors. Back then, it was a brutal fight for survival and it was crucial for us to focus on the negative, because it could have been a threat. Now, in an era of relative peace, we have to rewire our brains to notice the positive things around us. By focusing on the positive, we allow ourselves to enjoy life.**

Actionable advice:

**Share your happy experiences with your friends.**

Don't limit a positive experience by keeping it to yourself. A great way to revive the good feelings connected to something that makes you happy is to share it and relive it through your friends and loved ones.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Buddha's Brain_** **by Rick Hanson**

_Buddha's_ _Brain_ is a practical guide to attaining more happiness, love and wisdom in life. It aims to empower readers by providing them with practical skills and tools to help unlock their brains' potential and achieve greater peace of mind. Specific attention is paid to the contemplative technique "mindfulness" and the latest neurological findings that support it.
---

### Rick Hanson

Rick Hanson is the bestselling author of _Just One Thing_, _Buddha's Brain_ and _Mother Nurture_. He holds a PhD in psychology and delights readers with his direct and personal style. He is also a beloved speaker and coach, sharing his knowledge on how the brain can make human beings happier and better.

