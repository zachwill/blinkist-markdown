---
id: 536a02996631340007660000
slug: hatching-twitter-en
published_date: 2014-05-06T10:07:17.000+00:00
author: Nick Bilton
title: Hatching Twitter
subtitle: A True Story of Money, Power, Friendship, and Betrayal
main_color: 2CABDD
text_color: 1D7091
---

# Hatching Twitter

_A True Story of Money, Power, Friendship, and Betrayal_

**Nick Bilton**

_Hatching_ _Twitter_ tells the story of the four men responsible for Twitter as we know it. It's a tale of backstabbing, superstar glory and billions of dollars that chronicles the drama and defining decisions that made Twitter what it is today.

---
### 1. What’s in it for me? Discover the true drama behind the creation of Twitter. 

Friends turned enemies, backstabbing secret meetings, and a leader ousts another, only to be ousted himself in return.

Shakespeare? 

Nope, this is the story of Twitter. 

Twitter is one of the technologies that has had the biggest impact on our everyday lives. It's used to share groundbreaking news and allows people to create close connections with celebrities, politicians and world-leading thinkers. Not to mention sharing those all important selfies. 

But few people know the story of how Twitter became a multi-billion-dollar company used by hundreds of millions of users worldwide. For a long time, Jack Dorsey, the techie with the ego of a superstar, had proclaimed to the world that he was the true founder of Twitter.

Sorry Jack — this book punctures that balloon. 

In these blinks you'll find out the real story of the relationship between the founders of Twitter — and not Jack's media-fuelled fantasy.

You'll know more about the technology behind Twitter, and how its founders imagined it could change the world.

And you'll learn how a few strange but creative developers, some of whom never even finished college, built a multi-billion-dollar behemoth.

**This is a Blinkist staff pick**

_"Who's the good guy and who's the bad guy?! I love how these blinks tell the story of Twitter in a way that now I really feel like I understand the stories of Ev and Jack."_

– Ben S, Audio Lead at Blinkist

### 2. Before founding Twitter, Evan Williams helped start the blogging craze with the site Blogger. 

So many of us use Twitter to share our lives, thoughts and opinions — but have you ever asked yourself how it all started? It might shock you, but the birth of Twitter began with a college dropout from Nebraska named _Evan_ _Williams_. 

Ev, as Williams was known, showed a strong entrepreneurial instinct from a very young age.

He was already trying out business ideas in his teens, and during high school he went door to door selling a VHS tape that explained what the internet was. 

He later brought this entrepreneurial spirit to California and the flourishing start-up scene of the Bay Area. It was there, after learning how to code, that he and a few friends started a business called Pyra. As a side project, Ev built a journal-like website to help Pyra employees track their workflows. The employees used it so much that in 1999 he launched it as a public service called Blogger. The idea behind Blogger was to allow ordinary people who lacked computer programming knowledge to easily create their own internet journal or diary. By releasing Blogger to the public, Ev helped invent the idea of a _blog_. 

But although Blogger quickly became popular among internet users, Ev's struggles to keep up with the business' bills and paperwork led the other Pyra employees to quit in frustration. He was left all alone to run the business, which he ran out of his own living room.

But Blogger continued to explode in popularity because people loved the opportunity to express their own perspectives, opinions and experiences. By 2002, Blogger's continual growth had allowed Ev to hire a few programmers and improve his office space. And the site continued to grow, until in 2003 Google bought Blogger (and Pyra) for millions of dollars.

This money would later be crucial for Twitter's genesis.

### 3. After selling Blogger to Google, Evan Williams joined up with a neighbor to work on an audio blog platform called Odeo. 

One day, Ev's neighbor came knocking on his door. He had recognized Ev's photo in an issue of _Forbes_. His neighbor, _Noah_ _Glass_, also a tech enthusiast, shared his ideas with Ev and soon became his best friend. 

Noah had a similar idea to Blogger: Odeo, an audio blog that would allow people to make audio posts. Ev liked the idea, and agreed to fund it with money from the sale of Blogger. 

But Odeo was characterized by a chaotic, if creative, staff. Noah had a scattered working style, and originally hired some hacktivists who were intrigued by Odeo, but disorderly in their work habits. One of these was _Rabble,_ who was regularly involved in anti-government protests.

Noah later added two staffers reknown for their creativity: _Jack_ _Dorsey_ and _Biz_ _Stone._ Jack had some quirky ideas: he once put his phone number on a T-shirt he wore around town as a walking billboard. Biz Stone was a former employee of Ev's at Blogger/Google, and known to all as a jokester. When he gave up millions in Google stock options to join his old boss at Odeo, his Google managers thought he was kidding.

This mix of creativity and chaos soon led the Odeo office to spin out of control — especially because of Noah. Noah liked to party hard, and the Odeo staff were working in his messy apartment, where Noah's wife would complain about the noise and disorder.

Meanwhile, although Ev was not yet involved with Odeo's day-to-day operations, he wanted to prove to the tech world that Blogger was not a one hit wonder, and he could start another company. So when Noah ran into financial problems and had to ask Ev for more money or shut Odeo down, Ev jumped on the opportunity. In exchange for a $200,000 investment and the use of his apartment as an office, Ev took over as Odeo's CEO.

### 4. While Odeo was struggling to survive, a few employees had an idea for a way to share their statuses with friends. 

Desperate to prove his worth as an entrepreneur, Ev started to put his stamp on Odeo — yet it continued on its stuttering path.

Despite its substantial growth, Ev had trouble leading Odeo towards its launch. He managed to recruit $5 million of investments in 2005 — most of which were from investors betting on Ev as much as the audio blog idea — but the problems at Odeo were the same as at Pyra. Staff and investors complained about Ev's inability to manage the paperwork and make big company decisions. And Ev and Noah were constantly fighting about Odeo's direction, which left their friendship in pieces. Ev wanted to wait until the product was polished, but Noah wanted to launch it sooner rather than later. Things were at a deadlock. 

So Rabble took matters into his own hands. 

In the middle of yet another Ev–Noah showdown, Rabble simply switched the site on: a bold move that reflected the staff's frustrations with Noah and Ev's bickering. 

But no matter what happened, Odeo was doomed to fail. In 2006 Apple announced that its latest iPod would feature podcasting. Odeo's staff knew they couldn't compete with Apple, and started searching for a new idea to save Odeo.

In a conversation with Noah following Apple's announcement, Jack brought up his _status_ _idea_. It came from a LiveJournal (another social networking site) feature which allowed users to post a short status message on their blog saying what they were doing at the moment. Noah immediately grasped the potential: this idea wasn't just about updating people on what you're doing, but connecting people. For a generation spending more and more time alone with a computer, it could bring people together and cure loneliness. 

But because Ev now doubted Noah's leadership, he told Biz and Jack to work on the text message status update idea without involving him.

### 5. Noah Glass was instrumental in turning the status idea into Twitter – but his disorderly behavior cost him his job. 

Despite the attempts to cut him out of the loop, nothing would keep Noah out. Throughout this period, his marriage was struggling, and he became obsessed with the possibility of curing his and others' loneliness with the new status idea. 

Noah threw himself into developing the project. The fact that Ev was running both Odeo and the development of the status idea frustrated him: he wanted to contribute to the idea because it felt so personally meaningful to him. 

Noah became obsessed with finding the right name. Looking through the dictionary for hours at a time, Noah finally found the perfect word for the status concept: Twitter, "the light chirping sound made by certain birds." 

As the idea developed, the conceptual foundation of Twitter started to come together. Jack originally thought it should be a simple, clean application that only showed one update at a time; however, it was soon realized by both Ev and Biz that it would be more effective as a stream of updates made by oneself and other users. Noah brought in the idea of time stamps for each tweet, and convinced the others that his name, "Twitter," was the best. Finally, it was ready. The first tweet ever was made by Jack on March 21, 2006: "just setting up my twttr." 

But Noah's excitement made him mess up one time too many. He drunkenly told members of the public about Twitter at a start-up insiders party before it was officially launched. This was the final straw, and many came to the realization — as Ev had before — that Noah was a liability.

And so in July 2006 Noah was fired from the company he started.

### 6. Despite Twitter’s success, its founders constantly battled over who should lead – and in what direction. 

In August 2006, the Twitter office reacted to an earthquake in San Francisco by tweeting to each other. This idea of using Twitter to share news soon became a promising one. But its potential wasn't yet universally recognized. 

Twitter officially launched at a Love Parade rave party in San Francisco. Unfortunately, the idea didn't capture the partygoers attention, and that night Twitter gained fewer than 100 users. 

It was only after Twitter won the Best Start-Up award at the SXSW tech festival in March 2007 that the idea took off. During the festival Twitter set up screens that showed live tweets.The attendees started using Twitter to share information about festival events, or to tell others where they could get free beer. Twitter became so popular during the festival that some people coined the word _flocking_ : the flooding of a bar by Twitter users after cheap drink offers had been tweeted online. This real world impact created more publicity and added even more users. 

As Twitter grew over the following months, a battle developed between Jack and Ev over who should lead Twitter's development. Ev was CEO, but he had put Jack in charge of the project. And it was Jack who had been leading the idea's development, with innovations like the 140 character Tweet limit and the addition of usernames. Meanwhile, Ev's attention was increasingly distracted by another project, his business incubator Obvious Corporation. So Jack and Ev's relationship continued to deteriorate as they disagreed on Twitter's direction and leadership. 

Eventually, the ownership and structure of the company was formalized. Ev had personally financed Twitter and so kept 70 percent, Jack would be CEO and get 20 percent. Co-founder Biz Stone and the staff would split the rest. What does that mean in hard cash? Consider this: in December 2013, one percent of Twitter was worth $320 million.

### 7. The money Twitter needed to raise as it grew created power struggles. 

In 2007, Yahoo offered Twitter $12 million. The Twitter founders expected $100 million and turned Yahoo down. The offer was proof that the idea was going places — but there was still work to do.

As Twitter started to grow seriously it ran into scaling issues. Twitter had 250,000 users around the time of Yahoo's offer. "Technorati" (tech enthusiasts), like those at the SXSW festival in 2007, loved the site and use quickly spread between them. 

But then celebrities, politicos and news sites like Ashton Kutcher, Senator John Edwards and the _New_ _York_ _Times_ started to sign up, bringing with them a massive new influx of fans, constituents, and readers.

Yet the site was still relying on architecture built during the initial two-week experiment at Odeo — and was now crashing all the time. Amazingly, the crashes led to media reports about how Twitter was overwhelmed with users, which made Twitter seem popular, which led to even more users! 

This success resulted in some huge investments. In 2008 Twitter raised $18 million, including a $4 million investment from Amazon CEO Jeff Bezos.

But both Ev and the new investors were unhappy with Jack's leadership. As CEO, Jack was liked by the staff, but he struggled with leading or making decisions; despite the investments, he also struggled to manage the company's bills. And at the same time as being CEO for Twitter, Jack was also taking fashion design classes. 

Ev and the investors became impatient, and Ev demanded that Jack decide: be a CEO — or a dressmaker. 

Incapable of finding a solution, Twitter fired Jack in 2008, and Ev took over as CEO. As a consolation, Jack was made a silent chairman on the board — a move that would later be fateful for Ev.

### 8. Despite its enormous success, Twitter was constantly blighted by boardroom disputes. 

Jack felt that he was responsible for the invention of Twitter — and was furious at being ousted. 

He started telling anyone who asked that he was the founder of Twitter and that he still played an important role. For example, Jack would answer journalists' questions about internal Twitter decisions that he no longer knew anything about.

Ev's actions helped support Jack's lies. Although he no longer had any power inside Twitter, Jack was allowed to keep his Twitter email address and his non-voting seat on the board. This led some people in the media to think he had simply switched places with Ev from CEO to the board, and had not been fired. 

But instead of confronting him about his unsanctioned media blitz, Ev still included Jack in media events like the tech industry Crunchies Awards in 2009. Jack even went to Iraq with the US State Department and met with national leaders. Throughout the visit the press consistently referred to him as the founder of Twitter. 

Then Jack started using his story to plot revenge against Ev.

For years, Jack used his Twitter-founding fame and the confusion surrounding his company role to acquire powerful allies. In 2009, Peter Fenton invested in Twitter, and was convinced by Jack's version of the story. Peter then became Jack's first ally on the board. In 2010, VP of engineering Mike Abbot went to complain to Jack about Ev's leadership, not knowing that Jack was no longer involved in the company. Jack suggested to Mike that he should share his complaints about Ev with the board members and the other senior executives. 

Ev finally confronted Jack. He told him that there was not one single inventor of Twitter, and Twitter had transformed far beyond the initial idea. But by then it was too late — the lie was too big to control.

### 9. With the help of celebrities and politicians, Twitter’s popularity, influence and responsibility reached new heights. 

Soon after its conception, Twitter became a beacon for celebrities, politicos and the media. They realized that Twitter allowed them to gain a level of influence like never before. And as they flocked to the site, users rushed to join them — and the site just kept on growing. 

A key event in this development was when the actorAshton Kutcher beat CNN to one million followers, which proved that Twitter could enable one individual to reach as many people as a giant media operation. Twitter started to be discussed on TV shows, and in 2009, Ev was a guest on Oprah.

Twitter's growing influence was soon exploited by politicians and policy makers. Whereas before, world leaders and influential figures would meet with the top journalists of major media outlets to communicate with the people, they could now come to Silicon Valley. In 2009, California governor Arnold Schwarzenegger even held a town hall-style meeting at the Twitter office. 

Twitter's immense influence was cemented in 2009 when _Time_ included Biz and Ev in their annual 100 most influential people in the world. Jack was furious to be left out of the list. However, Biz arranged to have him invited to the celebration dinner — making it in fact the _Time_ _101._

Their popularity also brought them face to face with difficult philosophical questions. In 2009, Iranians used Twitter to help organize anti-government protests. It also became an important way for officials and the media to collect information about what was happening there. 

But Twitter had scheduled maintenance just when the protests broke out. 

The US State Department then asked Twitter to change the timing of the maintenance so that Iranians could keep using Twitter during the protests. Although some employees in Twitter always wanted it to be apolitical, they agreed to delay the site maintenance. Despite their attempts to explain the decision in non-political terms, it seemed as though Twitter was intervening in Iranian politics.

### 10. As Twitter grew in size and success, Evan Williams struggled to keep on top of running the business. 

Like Noah and Jack before him, Ev was having trouble guiding Twitter towards building revenue while managing a giant organization. Although he preferred a hands-off approach to leadership, and liked to work with people he knew and trusted, he was criticized for hiring too many friends. So the board recommended that Ev take on famous CEO coach Bill Campbell in an attempt to help build Twitter's revenue. And what was one of Bill's first pieces of advice? Stop hiring friends. 

But Ev ignored him and hired his friend Dick Costolo as chief operating officer (COO). Dick did help revenue by making multimillion-dollar deals with Google and Bing to promote Tweets on their search engines. But other employees felt Ev didn't communicate enough with the board or senior staff, and they took it personally when he didn't consult them about important decisions.

In 2010, Ev increasingly overlooked daily tasks as CEO to focus on Twitter's redesign. For example, at one point Twitter was threatened by UberMedia, owned by billionaire Bill Gross. Gross wanted to buy up a Twitter clone together with the actor Ashton Kutcher. And if they could buy third party Twitter service TweetDeck (a social media dashboard), Gross would already have 20 percent of Twitter's users. Some senior staff pushed Ev to act and buy TweetDeck first — but Ev stalled. 

Meanwhile, Jack was continuing to hold secret meetings with Twitter staff to devise his return to the company. His whispering campaign even reached Ev's coach Bill Campbell, who was telling Ev he was doing a good job — while telling the Twitter board that Ev had to go. Even the last friend Ev hired, Dick, met with Jack twice in 2010.

But Ev was caught up with Twitter's redesign and the company's growth — and was oblivious to the forthcoming coup.

### 11. In a cruel mirror image of how he became CEO, Evan Williams was pushed out of Twitter by a board coup. 

Twitter's investors wanted Twitter to focus on building revenue. Ev thought he was doing a good job — even his coach was telling him so — and was totally taken by surprise when he was fired. 

Thinking he was going to one of their regular coaching sessions, Ev was suddenly told by Bill Campbell that he would be replaced as CEO. Campbell told Ev that the board wanted to make Dick CEO because Dick could focus on revenue and take Twitter onto the stock market. With the Google and Bing deals, Dick had already proven that he could build revenue. 

Horrified, Ev jumped on the phone and soon realized that board members Fred Wilson, Bijan Sabet, Evan Campbell and Peter Fenton all wanted to vote him out. He had no idea that his friend Dick was also involved in the plot — when Ev confronted him, he denied everything. Ev and Biz tried to delay the vote, but the board organized an official meeting to oust Ev from the company.

Of the seven board seats, Fred, Bijan and Fenton voted against Ev. Ev, Jason Goldman and Dick voted against it — so Ev wouldn't know he was involved. But it was Ev's decision to make Jack silent chairman years beforehand that gave the board the final crucial vote that pushed Ev out. As Jack raised his hand, Ev realized that Jack had been planning this for a long time. In a flurry of reorganization, Dick was made interim CEO, Jack executive chairman and Ev was left as president of product.

Jack was back.

### 12. Finishing his years-long campaign to recover his influence at Twitter, Jack returned in 2011. 

Triumphant, Jack returned to Twitter as a superstar. He was back, comfortably running product development as Twitter's Executive Chairman. And Jack's long media blitz had made him a celebrity, with his version of Twitter's history now the best-established story. 

Meanwhile, Ev and his allies were slowly pushed out. Although at first he enjoyed being able to focus on product instead of the kinds of headaches he had as CEO, he soon realized that Dick was ignoring his new product ideas, and decided to take a leave of absence. In March 2011, he officially left his day-to-day position in product. Biz also didn't feel comfortable in Dick's Twitter, so he started avoiding the office to figure out how to leave the company, and in June 2011, he was gone — two founders of the company had now left. 

Jack continued to seek out the spotlight to spread his vision for Twitter. Despite the apolitical values instilled by Biz and Ev that Dick was trying to uphold, Jack held a Twitter town-hall session with President Barack Obama in 2011. 

Although some staff were happy to see the perceived founder come back to Twitter, others were put off by his arrival. In his return speech, he criticized Ev's Twitter even though many of the people who had worked with Ev were still there. He announced a new Twitter, Twitter 1.0, which would be closer to his original conception of Twitter as a way for people to share news about themselves. 

But despite his superstar image, Jack was not as popular within the company as outside it. His press tours and wavering on product ideas frustrated the staff. 

Jack was unfazed: he had regained the role he felt he had always deserved as leader of the Twitter product.

### 13. Dick Costolo steered Twitter towards maturity and huge revenues. 

As the new CEO, Dick had a lot on his hands: he had to develop a more mature Twitter, increase revenues, and continue fixing bugs and outages — while transitioning the leadership from Ev to himself. 

To start with, Dick wanted Twitter to have a more mature image. Not long after Dick was made CEO, Snoop Dogg showed up at the Twitter offices and kicked off an impromptu marijuana-smoking session and concert in the cafeteria. Dick was furious: this was exactly the type of immature behavior that shouldn't happen in the future. 

He also moved the Twitter offices to a bigger space reflecting the company's established size. Significantly, he left behind one of Ev's favorite pieces of art: a big sign that was ironically hung upside down and read "Let's make better mistakes tomorrow." Under Dick, they would no longer be the kind of company that accepted or flaunted mistakes in the name of creativity or growth. 

Overall, Dick succeeded in his goals. Employees came to love Dick, the professional culture he had instilled and the new office in San Francisco. Twitter increased their revenues through advertising, and fixed their inconsistent uptime issue, which had dogged them since their foundation, giving the site an uptime of almost 100 percent. And he authorized Twitter to refuse access to some third party companies so there wouldn't be a threat, like when Gross once tried to buy up 20 percent of Twitter's users.

The consequence? In 2013, Twitter hit the stock market valued at $31 billion. 

From Noah, Ev, Jack and Biz sitting in Ev's living room, trying to plan how to turn a few thousand dollars of Ev's money into a revolutionary media platform, Twitter had grown into a multi-billion-dollar company that was changing the world. But now, it was run by a different cast.

### 14. Final Summary 

The key message in this book:

**_Hatching_** **_Twitter_ is about how Twitter was conceived — told from the perspectives of its founders. Despite painful layoffs, stressful relationships and chaotic working environments, Twitter successfully solved its behind the scenes problems to become one of the most influential start-ups ever.**

Actionable advice:

**Beware of mixing business and friendship.**

When big sums of money are involved, the temptation can sometimes be so great that we start to lose sight of the value of friendship. Keep your close friends out of your business deals, just to make sure nothing goes sour — money comes and goes, but good friends can be for life.

**Avoid making enemies**.

As you strive to become successful in your career, you'll bump into people you really don't like. It may be tempting to treat these people with disrespect and turn them into enemies, but remember: you never know when you'll meet them again. Keeping on good terms with as many people as possible will increase your opportunities in the future.
---

### Nick Bilton

Nick Bilton writes the Bits Blog for the _New_ _York_ _Times._ He's an expert on technology, culture, business and the way the internet affects our lives. He is also the author of _I_ _Live_ _in_ _the_ _Future_ _and_ _Here's_ _How_ _It_ _Works._

