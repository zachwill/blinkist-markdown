---
id: 58445c6ba6ba9500046bed43
slug: headscarves-and-hymens-en
published_date: 2016-12-05T00:00:00.000+00:00
author: Mona Eltahawy
title: Headscarves and Hymens
subtitle: Why the Middle East Needs a Sexual Revolution
main_color: BD263D
text_color: BD263D
---

# Headscarves and Hymens

_Why the Middle East Needs a Sexual Revolution_

**Mona Eltahawy**

_Headscarves and Hymens_ (2015) chronicles the many levels of abuse suffered by women in the Arab world and what brave feminist activists are doing about these injustices. These blinks describe the various forms of oppression women face, from child marriage to virginity tests, and call for a sexual revolution in Islamic nations.

---
### 1. What’s in it for me? Open your eyes to the systemic oppression of women in Arab countries. 

There is much controversy in the Western world surrounding the veiling of Arab women. Feminists argue that the veil is a means of oppression, enforced by men; traditionalists counter that veiling is a sign of cultural awareness. And Western liberals kindly remind us that we should restrain ourselves from intruding too much into other people's affairs.

What we're missing in these debates, however, is that the veil is just one of many issues concerning the broader issue of women's rights in Arab countries. To tell it straight: women face a dire situation in the Arab world.

In these blinks, you'll learn about the Arab world's misogynist culture; how the oppression, abuse and punishment of women is justified by religion and a male-dominated society; and how a nascent Arab feminist movement plans to liberate women from their status as second-class citizens.

In these blinks, you'll also learn:

  * what the difference is between a hijab and a niqab;

  * what happens to a Saudi woman when she doesn't wear the veil; and

  * why female genital mutilation is considered a "ceremony" in many Arab lands.

### 2. Arab women live amid hostile, misogynistic environments. 

Most Westerners are aware that women in the Arab world don't enjoy equal rights, but might not know of the astounding daily abuse under which many women suffer.

The author believes Islamic religion encourages discrimination, and its influence is directly related to the promotion of a misogynistic culture throughout Arab nations.

Misogyny — hatred of women — is rampant in the Arabic-speaking areas of the Middle East and North Africa. Many people in these regions subscribe to ultra-conservative interpretations of Islam, ideas which spawn societies obsessed with the control of women.

This is especially true among Salafi groups or those who follow the Sunni sect of Islam, as well as in political groups such as the Muslim Brotherhood or Shiite militias in Iraq. In general, the social control of women and inequality between the sexes are the norm throughout the Islamic world.

In many places, family matters are handled by religious courts that enforce Islamic laws. These laws are supposed to protect families but fail to prevent atrocities such as child marriage, marital rape, sexual harassment and domestic violence.

In Egypt, for example, a court can decide that an Egyptian man may beat his wife with "good intentions" and not face any consequences. In Yemen, 2013, an eight-year-old girl was forced to marry a man five times her senior. She died on her wedding night from the internal bleeding she sustained as a result of her husband raping her.

But despite such horrific cases, voices for child marriage are more prevalent than those opposed. Yemeni clerics essentially support pedophilia by referencing the example of the Prophet Mohammed, whose second wife was a child when they were wed.

Because of practices like child marriage, the World Economic Forum's Global Gender Gap report, which measures levels of inequality, found that no Arab country qualified to appear in the list of top 100 countries working to close the gender gap.

Morocco, a nation that has been questionably praised as supporting "progressive" family policies, was listed at 129; Yemen appeared at the very bottom of the list.

While the situation of women in many Arabic-speaking nations is dire, many people aren't aware of the specifics of what being an Arab woman is like in daily life. Let's find out why.

### 3. When it comes to misogyny, Arab women and Western liberals remain largely silent. 

Since they're brought up in a culture that rewards obedience, Arab women often don't know how to express their feelings toward their male oppressors. At the same time, people living in Western countries often shy away from directly critiquing what is seen as misogynistic behavior.

Here are a few reasons why silence is so prevalent among both groups.

Arab women often remain silent to avoid embarrassing their communities. A woman who is critical of social "norms" risks exposing friends and family to investigations by internal forces, such as community members or the police, for which she might feel ashamed; she also risks drawing the attention of external forces, such as Islamophobes looking for any justification in their criticism of Muslim society.

Thus it takes a tremendous amount of courage for an Arab feminist to fight for women's rights and admit that Muslim society is inherently misogynistic.

The author believes this courage is comparable to that which American women of color found when raising their voices against sexism in the black community, despite concerns of hurting people around them.

In contrast, many Western liberals stay silent on Arab women's rights issues because they want to "respect" other ways of life. This isn't surprising, as Western society has a tendency to support _cultural relativism_, or double standards that in this instance, are used to justify non-involvement in the fight for equality. 

In an article published in Foreign Policy in 2012, the author criticized Westerners for their tacit support of the most conservative aspects of Arab societies and their failure to address Arab women's secondary status in society.

Luckily, Arab feminists throughout history have, against all odds, summoned the courage to raise their voices. The author is following in the footsteps of these brave women.

### 4. While there are many reasons to wear a headscarf, Arab women have little choice in the matter. 

Conservative Islamic societies expect women to cover their faces. But few critics have looked behind the veil, so to speak, of this religious custom to understand how it came about.

In the Arab world, there are two ways to wear the veil. The _hijab_ covers the head and chest. The _niqab_ covers the head, chest and face.

Interpretations of the Quran and the Hadith, a collection of stories attributed to the Prophet Muhammad, instruct women to cover themselves fully, except the face and hands. A woman who wears a veil, then, is considered pious, modest and respectful of tradition.

While custom is primarily motivated by religious belief, the choice to veil oneself can be personal, too.

For other women, wearing a veil is a way to gain freedom in a male-dominated society which holds women responsible for the desire they trigger in men.

Others wear the veil to protect themselves from sexual harassment. The author began wearing a veil at the age of 16 in Saudi Arabia, following a pilgrimage to Mecca during which she was harassed twice.

But the choice to wear a headscarf exists only for a fraction of Arab women, mostly those of privileged means. This needs to change. The author wore a headscarf until she was 25; the decision to stop wearing the veil was much more difficult than the choice to put it on.

Having grown up in a wealthy family, the author is aware of the privilege she enjoyed, having a choice to veil or not veil. In Saudi Arabia, morality police patrol the streets; a woman without a veil, aside from facing social stigma, might end up being lashed or even imprisoned.

In other countries, Arab women without a veil are seen as bringing shame upon themselves and their families. Thus until women's rights take precedence over religious custom, women have no choice.

Veiling isn't the only custom in which women in Arab countries experience oppression. A tradition that's potentially even more damaging is that of preserving a woman's hymen for her wedding night.

### 5. Virginity is held sacred, thus Arab girls are “protected” with dangerous, often deadly, genital cutting. 

While Arab society demands its women be covered, it raises the stakes when it comes to sex. For many Arab cultures, it's critically important that a woman's hymen be "protected."

An intact hymen, the small membrane that partly covers the opening to a woman's vagina, is believed both historically and in many unscientific circles to be a guarantor of virginity. Thus the state of a woman's hymen before marriage is diligently policed by Muslim families as well as clerics.

These purity advocates believe that a girl's hymen must remain intact until her wedding night. The pressure of this cultural "norm" causes many mothers to shame daughters if they dare explore their sexuality.

To ensure virginity, some Arab societies "protect" girls through the harmful cutting of their genitalia.

In general, families are committed to such an act as their honor is at stake; in some cases, if an Arab girl is found to have a damaged hymen before marriage, she'll be murdered to avoid the shame.

The practice of _female genital mutilation_ (FGM) is intended to control the sex drive of Arab girls and thus "protect" their hymens. FGM involves the partial or complete removal of a girl's exterior genitalia.

The intended result of this procedure is to decrease a girl's sexual desire and thus maintain her virginity as well as keep her faithful to her husband once she is married. It's even common for mothers to bring their daughters to FGM "ceremonies," to further remind young women of what's expected of them.

FGM, however, does little to decrease female sexual desire, but FGM victims _are_ less likely to experience pleasure during sexual intercourse.

A key point about FGM is that it's a cultural and not religious practice. In countries like Egypt, for example, Muslim and Christian girls are cut, although there's no mention of FGM in either the Quran or the Bible. In contrast, stories in the Quran and Hadith discuss the importance of female pleasure.

FGM victims often complain of bleeding or problems with urination; later on, they can suffer infections, infertility and complications in childbirth. Some girls even die during, or after, the procedure.

Today FGM is considered a violation of human rights by the United Nations and the World Health Organization.

> 90 percent of married women in Egypt have their genitals cut to remain "pure" for their husbands.

### 6. Arab women face sexual harassment and physical abuse both in public and at home. 

Women living in Arab countries face the threat of abuse constantly. Outside the home they are frequently sexually harassed; the police also pose a threat; and often, home is no safe zone.

According to a 2013 United Nations survey, 99.3 percent of Egyptian women reported experiencing sexual harassment in public, the majority of instances being unsolicited touching or verbal attacks.

Yet once an instance of sexual harassment happens, problems only compound as female victims have practically zero rights.

An abused woman like Dalal, a 16-year-old Jordanian girl, ended up in a forced marriage with her rapist. Often men can avoid punishment or social shame for their crimes by marrying their victims.

And if a victim goes to the police, the danger only increases. In Egypt, for instance, police conduct "virginity tests," essentially a form of rape. During such a test, a man disguised as a doctor "checks" to see if a woman's hymen is still intact.

The author and 12 other women were sexually assaulted by security forces following a women's rights protest in Egypt in November 2011. The author's arm and hand were broken during the assault.

So the public sphere isn't a safe place for Arab women. Yet home is also dangerous, as women are literally the property of their husbands.

Many Arab societies rely on sharia, or religious laws based on the Quran, to resolve issues of domestic abuse. Yet these laws only protect men. The few Islamic governments that have passed national laws regarding domestic abuse, such as Saudi Arabia, often fail to enforce them.

In Iraq, for example, a man faces a maximum of three years in prison for murdering his wife, as opposed to a life sentence in most other Western countries. In the United Arab Emirates, men are _encouraged_ by clerics to discipline their wives and daughters, as long as they can do so without leaving marks.

To add insult to injury, many victims of domestic violence in Arab countries are blamed for bringing the attacks upon themselves.

### 7. Arab feminists are using the internet to reach out to other women, sparking meaningful change. 

Despite the constant fear of repressive, demeaning or even violent treatment, Arab women are beginning to take a stand against misogyny in society.

While many Arab nations that follow sharia law maintain that women are not ready to receive equal treatment in society, Arab feminist activists are bravely proving them wrong.

In Saudi Arabia, the royal family handed its authority over domestic issues to state clerics, so that any reform against the regulations of sharia would be interpreted by authorities as an attack on Islam. The rulers did so as they'd rather insist that women are unprepared for change than risk clerical outrage.

Clerics always have held that girls playing sports was a dangerous step toward Westernization. The Saudis have long banned women from playing sports or participating in the country's Olympic team.

In 2008, however, activist Wajeha al-Huwaider launched an online campaign to protest the ban. With an overwhelmingly positive response, by 2012, the Saudi Olympic team had two female members.

The internet is a powerful platform for social change. In 2011, a Saudi woman named Manal al-Sharif was taken into custody for driving a car and jailed for nine days. Saudi law forbids women to drive; in general, women's freedom of movement without a male companion is strictly constrained.

Once al-Sharif posted online a video of her driving a car, it prompted another 12 women to upload videos of themselves driving as well, triggering a wave of online support to change the restrictive law.

These online protests are irrefutable proof that Arab women are not just ready for change, they're demanding it.

### 8. Even during liberating revolutionary movements, women have had to combat sexual violence. 

While feminists today fight for women's rights, such activism in the Middle East is nothing new. Feminist action in the Arab world goes back to 1923, when Egyptian feminist Huda Sha'arawi started a movement for women to shed the veil.

More recently, Arab women in 2010 and 2011 poured into the streets during the Arab Spring, taking part in demonstrations against oppressive government regimes.

Unfortunately during many protests, women continued to suffer sexual violence at the hands of both authority figures and fellow protesters.

Women in Syria who took part in protests against President Bashar al-Assad were raped and tortured by loyalists. Many women protesting in Egypt against the rule of President Hosni Mubarak were sexually assaulted by people in the enormous crowds.

In 2014, during the presidential inauguration of Abdel Fattah el-Sisi, a woman was gang-raped in Cairo's Tahrir Square. It wasn't until women's rights groups and initiatives such as HarassMap and Tahrir Bodyguard put their power behind the issue that the government finally came to terms with sexual violence on the streets of Egypt and took measures to stop it.

But while Arab women are still violently repressed, even during social movements that claim liberation for all people, they continue to fight for equal rights and social change.

After all, political revolutions that fail to address social and sexual abuse will never transform the misogynistic status quo.

One positive step toward addressing these issues is the introduction of sex education in regions where women suffer disproportionately from sexual violence. This could help change repressive laws, such as those that forbid extramarital sex and allow rapists to marry their victims.

And finally, positive change can only come from people speaking up. The author is doing her part by putting her beliefs down in words and knows that everyone has a role to play.

Those who can't fight physically in the streets should take a stand by whatever means they can!

### 9. Final summary 

The key message in this book:

**Every day, women and girls in Islamic countries suffer horrific abuse and repression while the world looks the other way. To encourage change and achieve equal rights for women, Arab women need to claim their rights and Westerners need to stand with them in solidarity.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Sex and the Citadel_** **by Shereen El Feki**

_Sex and the Citadel_ (2013) offers a revealing look at the sex lives of people in Muslim countries, especially Egypt, which, about 200 years ago, was a hotbed of sensual and sexual activity, but has since become a conservative and sexually repressed society. These blinks take you through the taboos, censorship and gender discrimination that many Muslims continue to resist.
---

### Mona Eltahawy

Mona Eltahawy is an Egyptian-American journalist based in New York City. She has covered current events in Egypt and general political and social issues in the Middle East. _Headscarves and Hymens_ is her first book and expands on her controversial 2012 article published in _Foreign Affairs_ on Muslim men, entitled _Why Do They Hate Us?_

