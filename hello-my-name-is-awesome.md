---
id: 562e306538643100073b0000
slug: hello-my-name-is-awesome-en
published_date: 2015-10-28T00:00:00.000+00:00
author: Alexandra Watkins
title: Hello, My Name is Awesome
subtitle: How to Create Brand Names That Stick
main_color: E12D4C
text_color: C72843
---

# Hello, My Name is Awesome

_How to Create Brand Names That Stick_

**Alexandra Watkins**

_Hello, My Name is Awesome_ (2014) is about the most important decision a new business faces: choosing a brand name that will launch the company into the stratosphere where it belongs. You might think picking a name is straightforward, but you have a lot to learn! This is your guide to negotiating the snakes and ladders of the brand-naming process — and coming out on top.

---
### 1. What’s in it for me? Learn the dos and don’ts of naming your company. 

"A rose by any other name would smell as sweet," quipped Shakespeare in _Romeo and Juliet_. When it comes to your loved one this is certainly true. In business, however, nothing could be further from the truth. Having the right name will have a huge impact on what people think of you. The wrong name always stinks.

While a great name will make it easier for people to connect to your product, a bad name can alienate, confuse and quite simply bore potential customers. As these blinks will show you, a funny spelling, a quirky made-up word or a pun might seem like a good idea at first, but if you're not careful it might come back to haunt you. Luckily, there are tried-and-tested ways to make sure this doesn't happen. 

In these blinks you'll learn

  * how to make people smile when they see your brand name;

  * why naming your company XOBNI is a bad idea; and

  * why pen company PenIsland should maybe have brainstormed a little bit longer.

### 2. To leave a great first impression, ensure your brand name is super “sticky” and makes people smile. 

Do you remember the first time you heard of a website called Facebook? What was your immediate impression? Did the name make it sound like a hip place to be?

Chances are, the answer is yes. And that's exactly how it should be: a good name should create positive, brand-aligned associations without being difficult to understand. The name Facebook, for instance, instantly conjures up thoughts of people connecting. 

Flickr is a more challenging brand name. Even though it's a successful company, this photo-sharing platform has long struggled simply because people don't know what its name stands for. You definitely don't want people drawing a blank when they hear your name.

That's why, in order to discover the perfect name for your company, you should follow a conceptual approach captured by the acronym SMILE. It stands for:

_Suggestive_ : Amazon is a great name because it implies that the retailer carries everything from A to Z, and is massive like its namesake river. If the company had initially been called Bookshop.com, it probably would have followed a very different path.

_Meaningful_ : to convey meaning to potential customers, don't name your company after yourself, since strangers don't associate anything with your name.

_Imaginary_ : an evocative, visual name is hugely powerful. Think of Timberland, which suggests woods and outdoorsiness. This was exactly the effect intended by the founders. 

_Legs_ : choose names that create space for wordplay. Ben & Jerry's is masterful at doing this with its ice cream flavors: think Chunky Monkey and Half Baked. Choose fun, playful names people might even want to see printed on a T-shirt.

And finally, a recent _Fast Company_ article showed that 50 percent of every business is driven by _emotion_. For instance, just think of the fragrance, Obsession. 

Above all, making people smile when they see your name is key. Because as we'll see in the upcoming blink, you definitely don't want to leave anyone scratching their head.

### 3. To avoid confusing customers, don’t fall into the Seven Deadly Sins of Naming. 

Whenever you encounter a brand name that utterly befuddles you, chances are that the company violated one of the Seven Deadly Sins of Naming. 

Any one of these violations will leave your customers scratching their head. And to remember what those sins are, we can follow the acronym SCRATCH:

_Spelling Changed_ : It's not clever to spell your brand name in a non-intuitive way. Häagen-Dazs, for instance, has a major spelling problem! Use voice recognition software to make sure your name doesn't fall into this trap: to see what potential customers may find when they google you, ask Siri to search the name. 

_Copycat_ : if you name your product iSomething, it screams copycat and undermines your brand. 

_Restrictive_ : Canadian Tire is a major chain that stocks more than just tires — it also sells toys and tents. Many Canadians have figured out that the store carries a wide variety of goods, but expansion has proved difficult for the company, since its growth is limited by its name.

_Annoying_ : your name shouldn't annoy potential customers. Suffixes like -ly, -mania or -topia sound forced and contrived. 

_Tame_ : if your name is flat and uninspired, it won't work. Your brand name simply won't stand out if it uses generic words like "cloud."

_Curse of Knowledge_ : when you communicate with potential customers who are unfamiliar with your world, insider knowledge can be a curse. If you sell cycle pumps, don't name your company after the Schrader valve. Even though "Schrader" might be meaningful to you, anyone who doesn't know what the word means will be confused. 

And finally _Hard to Pronounce_ : think about it, how would you pronounce THX? The AV company should have used a different name. Along the same lines, don't use backward names, like the San Francisco software company that called themselves XOBNI, a reversal of "inbox."

### 4. To find the perfect name, work independently to draft a creative brief and then reach out to others for feedback. 

In modern marketing practices, collective brainstorming is treated like the holy grail of generating compelling new ideas. But oftentimes, finding a great brand name through brainstorming is close to impossible. It's better to go a different way:

First, draft a creative brief. This is basically an ingredient list of everything you need to combine to create the perfect name. It includes everything from target audience to words to avoid and more. This way, you'll be able to define exactly what your brand stands for and figure out what you want the name to communicate. 

Since the creative brief is basically a map, make sure you approach it honestly and thoughtfully. And set aside enough time to write it!

You may notice that drafting a creative brief seems like a solitary opportunity. It should be! Extroverts tend to dominate group brainstorming sessions, which means that potentially good ideas can get lost. 

Brainstorming works better when you're on your own. So, once you have your creative brief, collect all your name ideas and then dive deep into the internet: search through movie titles, song titles and images somehow connected to your creative brief. This is a great way to generate even more ideas. 

And once you've come up with your name, take steps to ensure it's the right one. Let others review your idea and give feedback. Find something everyone agrees on.

To clarify, this doesn't mean you should share ideas with random outsiders on open platforms; you're looking for the strongest name, not something watered-down and safe. 

Also, remember to trust yourself. It's OK to be different! The best names often seem jarring at first — just think of Google.

So now you have the name. It's time to take care of your domain.

### 5. Securing the right domain name is essential. 

On the web, your domain name is key, since that's where people will go to find you. And companies have big problems when their domains don't align with their brand name. 

Many firms start out with an imperfect URL. At first, Facebook was located at theFacebook.com. In such cases, companies have to belatedly spend huge sums of money to buy the domain that actually fits their identity. So, try to figure out your _real_ name from the beginning.

Still, it's possible that the domain you want won't be available. Then what? Well, you can hire a Search Engine Optimization expert to make sure your brand shows up in the search results for your keyword. 

Or you can add a word to your domain name: GetDropbox.com instead of Dropbox.com, for example. Similarly, it's okay to have a longer domain, if it's easier to comprehend and memorable. It's better to be RapidCityBookNook than RCBN.com.

You can also try alternate extensions, like .net or .biz. Otherwise, you can make a lowball offer for the domain you want. Just try it! It often works. 

It's a smart idea to buy domains that contain common misspellings of your brand name and redirect readers from them to your homepage. For instance, Flickr also owns Flicker.com.

But a word of warning: some companies have tried to use country codes in creative ways, but such schemes can have unexpected consequences. Many of the sites that used .ly, which is Libya's country code, were stunned when the government shut down all domains which were in violation of Sharia Law!

Also, make sure you have rights for the trademark before securing the domain.

Lastly, make sure the words you use in your domain name don't have an unintentional meaning. 

Using strategic capital letters can help.

A firm called PenIsland should avoid the domain name penisland.com, for instance. Same for TherapistFinder, which became therapistfinder.com.

There you have it: everything you need to secure a domain name where people can find you.

### 6. Final summary 

The key message in this book:

**You don't have to be a genius creative to find the right name for your brand. You just have to follow certain rules: a good name shouldn't be complicated — it should be easy for potential customers to remember and love.**

Actionable advice:

**At some point, it might make sense to change your name.**

If you find a better fitting name and believe it will boost your brand, don't waste any more time with a clunker. Plus, it's a great reason to get in touch with your customers. 

Before the internet, changing the name of your brand was hard, hard work, but today it's much easier: you can redirect customers to your new domain and send out emails to your newsletter subscribers, announcing the name change.

There can certainly be cons: maybe you're emotionally attached to your own name; maybe some of your team doesn't agree on the name change; maybe it's expensive to trade out all your brand assets for new materials. 

But if your old name confused customers, the pros may outweigh those cons!

**Suggested** **further** **reading:** ** _Hooked_** **by Nir Eyal**

_Hooked_ explains, through anecdotes and scientific studies, how and why we integrate certain products into our daily routines habits, and why such products are the Holy Grail for any consumer-oriented company. _Hooked_ gives concrete advice on how companies can make their products habit-forming, while simultaneously exploring the moral issues that entails.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Alexandra Watkins

Alexandra Watkins is the founder of the naming firm, Eat My Words, and a recognized expert on brand names.

