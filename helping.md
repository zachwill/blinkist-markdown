---
id: 5d2b2b5b6cee070008fd8416
slug: helping-en
published_date: 2019-07-15T00:00:00.000+00:00
author: Edgar H. Schein
title: Helping
subtitle: How to Offer, Give, and Receive Help
main_color: 5741DE
text_color: 5741DE
---

# Helping

_How to Offer, Give, and Receive Help_

**Edgar H. Schein**

_Helping_ (2009) explores a common phenomenon; when we offer help to other people, we're often met with resistance, ungratefulness and even resentment. Drawing from a variety of real-life scenarios, author Edgar Schein describes the social and psychological dynamics that underlie this most fundamental human activity and, perhaps more importantly, how we can ensure that our help is both welcome and genuinely useful.

---
### 1. What’s in it for me? Help yourself be a better helper. 

Helping others is one of the most fundamental human activities. Our whole civilization is built on it. We're linked, as if by a long chain of hands, by the ways in which we offer help to one another. A mother feeds her child, a bricklayer passes a tool to his workmate, a therapist helps her patient.

But why does it so often go wrong? How is it possible that the man who tries to rescue a burning house is sued for damages? Why do patients get angry with their doctors? Why is workplace collaboration sometimes fraught with tension? We're often called upon to help in our daily lives, and the experience can be unsatisfactory for both the helper and the person being helped. 

Throughout the following blinks, we'll explore how our social and psychological dynamics can be an obstacle to healthy helping relationships and the ways in which we can be better helpers.

In these blinks, you'll learn

  * how to respond when asked for directions;

  * that our closest relationships can be like economic transactions; and

  * why a _humble inquiry_ is the best way to begin when helping someone.

### 2. Helping is intrinsic to human society, and we don’t always recognize it. 

Imagine a soccer team in full flow, from its strikers to its central defenders. To win the match, everything depends on each team member successfully helping the other players. If help isn't quickly provided, then the striker gets tackled before he can shoot, the attacking midfielder has nobody to pass to and the goalkeeper is left vulnerable. 

Just like in a game of soccer, helping is intrinsic to many aspects of our lives. In fact, it's so ingrained in our every-day lives that we tend to forget just how important it is. Just think of your workplace. If you couldn't rely on getting help from your colleagues when you need it, or they from you, you probably wouldn't be able to accomplish much. 

So how do we help each other?

Let's look at supervisors and subordinates in a work environment. To achieve an objective at work — to finish a project or improve sales, for instance — the boss and her subordinates are dependent on each other's help. They enter into a kind of psychological contract. This is most clearly noticeable when one side fails to provide enough help and tensions flare. We've all been hollered at when we haven't pulled our weight, and the group deadline is approaching. 

The very glue of our society is based on helping each other. Just think of all the different situations in which people provide help. It might be the stranger offering directions to backpackers stumbling blindly around a foreign city, the good friend supplying a word that's on the tip of your tongue or the suicide hotline operator advising someone in serious distress.

Our lives are defined by these vital reciprocal relationships, without which there is no family, no work, no games and no society or civilization to speak of.

### 3. Helping can be split into three basic categories. 

What connects a doctor delivering a baby with a divorce lawyer? What links someone saving a life at the swimming pool with the grandson doing his elderly grandmother a favor? Let's find out by looking at the three types of helping.

The first of these categories is _informal help_. This form of help is the most common and covers all of the routine cooperative, collaborative and alturistic behavior in our lives. It's taken for granted as the basis for civilized society. The mother helping her child to tie his shoelaces, the shopper lifting a can of tomatoes off the top shelf for an elderly person or the first aid responder performing CPR at the scene of an accident — all are examples of informal help.

Next, comes _semi-formal help_. We seek semi-formal help when we require assistance with our homes, cars, computers and other areas that require complicated technical solutions. In contrast to informal help, its semi-formal cousin usually entails paying someone to help us with a small, technical task, such as calling a hotline to help fix a frozen PC screen, or a bank to assist us with opening an account. 

Finally, there is _formal help_. This kind of help is more high-risk than semi-formal help and entails legal or medical assistance from highly qualified, trained professionals. The pregnant woman who needs the expertise of an obstetrician, or the corporation that hires a management consultant to advise them, are both examples of seeking formal help.

Most books about "helping" are on formal help, but we require informal and semi-formal help much more frequently throughout our lives. For instance, it's a more common occurrence to ask a friend for relationship advice than it is to hire an asset manager, yet much of our critical attention goes on the latter.

So, while most of what's written on the topic is on formal help, it's informal and semi-formal help that shapes our lives the most. And it's often in these areas that we struggle to form the best helping relationships.

### 4. Human relationships are a lot like economics, even when we’re helping each other. 

We don't like to admit it to ourselves, but we know that many of our social relationships revolve around a simple mantra: "if you scratch my back, I'll scratch yours." The nature of give-and-take can sometimes feel transactional.

In fact, we can see how closely our social relationships mirror the logic of economics by looking at some common expressions in the English language. We "pay" our respects and "pay" attention. We speak of the "pay-off" when something goes well. And we are "owed" the courtesy of a reply. Likewise, you can be "sold up the river" if you're tricked into something you didn't want to do. You can "sell yourself short" if you don't do yourself justice in an interview. 

Economics is deeply embedded in our daily lives. We expect to have "equitable" social interactions with loved ones, colleagues and people we meet in passing; we expect a fair "exchange." For instance, when we decide to share a personal problem with a good friend, we expect them to listen attentively and take us seriously. Equally, when we tell our friend that we have something important to tell them they expect their attentiveness to be rewarded with our honesty. It wouldn't be an "equitable" exchange if your friend simply yawned and got up from the table or, equally, if after requesting their attention, you decided to abruptly change the topic.

Although it sounds counterintuitive, helping resembles an economic exchange, too. Consider the dynamics at work in the following scenario. If you offered money to a homeless person so they could buy themselves food, you might expect some kind of thanks — even just a grateful nod. And if they didn't acknowledge your help at all, the whole equilibrium of the "helping relationship" would've been upset. Therefore, it'd be necessary to "compensate" for your loss of face, either by consoling yourself with your moral worth or by criticizing the homeless person. You might reassure yourself that you are a good person for acting so charitably or bitterly think of the homeless person as an ungrateful wretch.

To understand how helping relationships work, we need to acknowledge the reciprocal dynamic of human society. This dynamic makes things much easier when giving or receiving help. Next, let's look at how helping resembles the world of theater.

### 5. Human relationships are a lot like theater, including when we help each other. 

"All the world's a stage," as Shakespeare put it. And half of the time, we all feel like actors, fools and jesters. 

This too is revealed in the English language. For instance, even when we're a long way from the theater, we still might remark that someone "gave quite the performance." Also, we're told to get our "act together," or that we are not "acting like our usual selves." Sometimes we even talk about our surroundings as if they _were_ the stage. We long for a "change of scenery" or wonder what is "going on backstage."

What's more, the theatrical "roles" we perform exist within the context of a social hierarchy. When we're children, for instance, we quickly learn how to act with deference to our parents and teachers. Then, as adults, we learn what kind of behavior is required of us to maintain authority, whether we're parents, teachers or bosses.

As subordinates, we learn how to incline our heads and adopt a listening posture, while as superiors, we take on an authoritative tone of voice. Even our clothing signals what kind of role we play: subordinates usually wear more casual attire in the office, while the managers set themselves apart with a sharp suit or business dress. 

As with everything else, helping is a kind of performance, too. When we provide or ask for help, we necessarily take on roles that resemble a choreographed performance. For example, when you enter a gas station looking puzzled, the attendant will deliver the customary line of attendants everywhere: "May I help you?" They'll maintain the familiar neutral-but-helpful demeanor. Then, when we accept more detailed help — from a lawyer or a counselor, for instance — the helper won't stray beyond the careful norms and etiquette of people in those professions.

By standardizing the roles of helper and client, we create a framework in which both can "maintain face." The social dynamics, which means that the helper has a temporary advantage over the client, are carefully managed when we play these roles.

These are the cultural rules of our society. We must play our roles _appropriately_, in a complementary way, to make giving and receiving help a pleasure rather than a chore.

> _"In the daily drama of life we play our roles in such a way that our own face and the faces of others are preserved."_

### 6. Helping – and receiving help – can be problematic. 

You'd think that helping would be the easiest thing in the world, but just like with many other aspects of our lives, asking for help isn't always as simple as it seems.

For a start, receiving help can feel like a loss of status. Consider the male stereotype that real men don't ask for help — they figure it out for themselves. It reveals the deep anxiety that lots of people feel when asking for assistance, especially men in Western countries within a competitive work culture.

Indeed, it's often the case that people feel "one down" when asking for help. The author recounts an occasion when he was working as a consultant for a group of European executives. When he bumped into them in the executive dining room, he discovered that they wouldn't meet his gaze. He asked a colleague why this was and learned that they were ignoring him to save face because they were ashamed to have even sought consultation. They thought it would weaken their status in the eyes of other executives.

Conversely, helping transfers status onto the helper, and this can be equally counterproductive. It's very difficult to say "I don't know how to help" because when we help people we feel empowered. It's difficult to refuse the sense of one-upmanship that helping grants us.

For instance, if you're asked to solve a computer malfunction and you don't know how, it can be tempting to try to fix it anyway. Of course, if you lack the expertise, you might end up making things worse! As helping opens up the possibility of an ego-boosting social encounter, it's difficult to say no — even when our help might be worse than useless.

Much of what goes wrong with helping is because of this status imbalance. When we ask for help and feel disempowered, we might not communicate properly with those who assist us. Rather than act in harmony with our helper, we get irritable, resistant and appear ungrateful. On the other hand, if we provide help and don't receive gratitude because the client feels one down and defensive, we take offense.

All problems relating to helping begin with this imbalance between helper and client. Acknowledging this is the first, crucial step to overcoming the obstacles that prevent a healthy helping relationship. In the next blink, we'll look at the different ways in which we complicate the helping relationship from the position of the client.

### 7. As receivers of help, there are different ways in which we complicate or hinder the helping process. 

You might have heard the phrase "he doesn't help himself" when referring to someone engaged in a self-defeating spiral. What does that mean in more detail, though?

Firstly, sometimes we don't reveal the full extent of our problem. Consider the spouse who frequently stands before the bedroom mirror and asks his wife whether his clothes still suit him. Rather than his fashion sense, his real anxiety might be the weight he's put on. Similarly, when the company manager asks for assistance with team-building, her real problem might be that she has lost faith in her subordinates, but can't face that fact.

Sometimes we even attack the helper. There are occasions when we might ridicule those providing help, because we feel powerless and want to restore equilibrium. A good example of this is the patient laughing in their counselor's face in an exasperated manner: "Nothing you do is helping! You're useless!"

And there are times when we have preconceptions about the people helping us. We might stereotype them, assuming they're like a bad teacher or unhelpful parent from our past. Naturally, this will prevent us from responding to the help provided in a productive way.

Another issue is that we might become over-dependent on our helper. Think of those times when you've been unable to solve a difficulty, and then found someone who can. Whether that's helping you with directions or acquiring a bank loan, your first reaction is often to collapse in gratitude and relief. And you'll forget that to solve the problem, you might be required to act yourself rather than transfer the whole burden onto the helper. For instance, when recovering in hospital from a serious illness, we must learn how to get out of bed, go to the bathroom and dress ourselves. Over-reliance on the nurses means that we're not able to build up our strength. In other words, relying too much on the help of others means we're not able to help ourselves as much as we should.

So what can we learn here? Though the onus is on the helper to provide the best assistance they can, as clients, we can "oil the gears," so to speak, by being sensitive to the many ways in which we frustrate the helping process.

### 8. As helpers, we can sometimes be seriously unhelpful. 

We've all heard the phrase "sometimes you need to be cruel to be kind." It's what we're told when helping someone in the usual sense would be counterproductive. But there are other ways in which our willingness to help can set people back too.

When you're too eager to help someone, you might fail to see the extent of their problem or even misidentify their problem entirely. This is because people who request help might really be testing the waters and hiding a more serious difficulty that they actually require help with. For instance, if your child asks for help with his homework, it could mean that you immediately jump in to help without a second thought. But if he is still emotionally distraught after you've helped him, it could be that the problem wasn't the homework at all, but perhaps something bigger, like being bullied at school, that he's disguising.

In situations like this, it's important that we don't focus on satisfying our own eagerness to help, but instead try to identify the underlying issues that really require our assistance.

Another way that we can be unintentionally unhelpful is when we pressure people into taking our help. There are times when, though someone requests help and we're certain that our advice or practical assistance is just the right thing for them, it'll actually make things worse. And because the person who requested help doesn't want to offend, they'll let us continue regardless. For instance, people may let a friend give them an impromptu massage when they complain of a stiff neck, but are too polite to say when the amateurish masseuse makes it worse.

And conversely, sometimes we're too reticent with help-giving. There are occasions when our unwillingness to intrude on someone's difficulties can be a real problem. Treading on eggshells, we'll take care not to offend someone's sensitivities and avoid asking if they require help. This is more often the case in professional circumstances. For instance, the PhD supervisor might assume that their candidate — who, though intelligent and mature, appears to be struggling — would be offended by a simple suggestion. And so they refrain and the problem is compounded. 

Sounds like a minefield, doesn't it? In the final blink, let's look at the best way to help more effectively.

### 9. To find out how to help someone, it’s best to begin with a humble inquiry. 

We're sometimes reminded that we've got two ears and one mouth. Or in other words, that we'd be better off listening more and talking less. Helping people is all about listening. But before we listen, we first need to make a _humble inquiry_.

To avoid the misunderstandings intrinsic to helping, it is best first to inquire. As we've already seen, the problem that we're presented with often isn't the full story. By inquiring in an open-minded way we can tease out the rest. 

Consider this scenario. A woman approaches you and asks for driving directions to a certain street, in an agitated manner. Rather than just pointing her to the street, you ask her gently where she's headed exactly. She informs you that she's driving to the big toy store, to buy her little girl a late birthday present. She's new to the area, she adds. Knowing how terrible the traffic on that road is, you know that she'd be better off heading to a nearby car-park and then walk to the store. Through this simple inquiry, you are more helpful than by simply sending her off with the street directions.

One of the other crucial functions of this inquiry is to put the helper and client on an equal footing. For instance, when a weakened hospital patient needs assistance to reach their bedpan, if we communicate with humility, we can help them more effectively. Rather than bossing them around in a humiliating fashion, it's better to _ask_ them if they are comfortable or ready to take the next step. By attempting to alleviate the loss of status, we create a helping relationship that allows the patient to move more confidently.

Another objective of the humble inquiry is to restore confidence in the person seeking help so that they might begin to _help themselves_. For example, a child might be embarrassed to tell their parents that they're not able to tie their shoelaces. But by restoring their self-esteem — perhaps by asking them if they knew that we _all_ find it difficult to tie shoelaces at first — they might attempt the knot with more confidence.

What can we take from this? Well, people are often reticent and insecure when they ask for help. To ease the whole process, we have to be humble and encouraging.

### 10. Final summary 

The key message in these blinks:

**Social dynamics complicate the way we help each other. When we receive help, we suffer a loss of status and self-esteem, while providing help gives us the upper hand in a way that can be counterproductive. To successfully assist others, we should be sensitive to these dynamics. As help-givers, it's best to inquire with humility about the problems we're attempting to solve.**

Actionable advice:

**Keep checking if your help is still required**

When you're helping someone, check periodically whether your help is proving useful or not. You don't need to overdo it, but it's good to keep an open line of communication. This will ensure that you provide the right kind of assistance, and it'll also save you from wasting time and energy on a futile endeavor. Helping your child with their math homework? Make sure that it's really necessary — and that you're not doing it just to prove that you can still do long division! 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Radical Candor_** **, by Kim Scott**

As you've learned, the best way to help others is to listen effectively to their concerns and respond fluently. This is valuable advice for those in management positions — the best leaders always know when to take advice and when to lead. 

If you're looking to establish a productive, open work environment for your employees, we highly recommend the blinks to _Radical Candour_ by Kim Scott. Here, you'll find the tools to get the best out of everyone under your supervision and learn to be the kind of boss your employees will be proud to follow.
---

### Edgar H. Schein

Edgar H. Schein is a world-renowned expert on organizational culture. He has lectured at the MIT Sloan School of Management and made important contributions in the areas of career development and group process consultation. His landmark work, _Organizational Culture and Leadership_ (1985), is a classic reference book for managers and organizers everywhere.

