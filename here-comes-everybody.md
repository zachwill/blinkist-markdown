---
id: 53cf931e6434330007140000
slug: here-comes-everybody-en
published_date: 2014-07-22T00:00:00.000+00:00
author: Clay Shirky
title: Here Comes Everybody
subtitle: The Power of Organizing without Organizations
main_color: F04D52
text_color: BD3D40
---

# Here Comes Everybody

_The Power of Organizing without Organizations_

**Clay Shirky**

Thanks to advancements in communication technologies and the widespread availability of the Internet, we can now contact one another and share information at unprecedented rates. _Here_ _Comes_ _Everybody_ explains how these changes aren't just affecting the way we communicate; they're affecting the way we organize, too. As the obstacles and expenses of communication diminish and the reach of our communication expands, we're now experiencing a significant shift in the ways we get together.

---
### 1. What’s in it for me? Find out how the internet has changed how we communicate and coordinate. 

When did you start relying on Wikipedia instead of a physical encyclopedia? When did you start following news stories online, instead of buying a newspaper?

In the same way it's important to know how food affects your body, it's important to know how technologies affect the world around us.

The internet has profoundly changed how we communicate with one another and _Here_ _Comes_ _Everybody_ details how internet platforms have made huge changes, not only to how we communicate with one another, but to how we organize and coordinate groups.

In the following blinks you will discover:

  * how bloggers made a Senate Majority Leader resign from his post in the US Senate,

  * why someone with a PhD in photography might be just as qualified as someone with an iPhone camera,

  * how one woman's lost phone attracted the attention of over 1 million people, and

  * why people involved in huge online social communities like Facebook would not be content with only socializing online.

### 2. The internet has completely changed how quickly and efficiently we form groups. 

Have you ever paused to think about how much the internet has changed the way we coordinate with each other?

If you consider our social lives, the change is pretty clear. These days, rather than calling each of our friends individually, we can simply send a group message on Facebook and watch our Friday night plans fall into place. But when you stop to think about how the internet has influenced the way we rally together for a cause, it's rather remarkable.

For example, take the case of a woman named Ivanna, who left her phone in a New York City cab one night in 2006.

The phone was found by a girl named Sasha but, even after Ivanna and her friend Evan contacted her through Ivanna's phone, she refused to return it.

In response, Evan created a website with the hope of getting the police and the public interested in returning the phone. In only one day, the website went viral, even being featured on the front page of Digg.com, a popular news aggregator. By that evening, Evan was getting flooded with 10 emails a minute from people wanting to help.

Ivanna's phone didn't just gather a few people's attention; it attracted a huge network, including professionals such as lawyers and police officers.

The website eventually drew in over one million viewers and even _The_ _New_ _York_ _Times_ and CNN reported about it, at which point lawyers, police officers and online detectives were also examining the story.

In the past, assembling such a network of professionals would have cost a considerable amount of money and red tape. Five years ago, it would have been unlikely and ten years ago it would have been impossible.

> _"Anything that increases our ability to share, coordinate, or act increases our freedom to pursue our goals in congress with one another."_

### 3. Sharing and cooperating are the main group functions that have benefited from digital platforms. 

If we are to create effective groups, we must be able to share and cooperate with one another. Thanks to new methods of communication introduced by the internet, these two central forms of group activity have improved drastically.

Digital platforms that allow sharing now give us the chance to share items in the way we want to on a scale that we have never experienced before.

For example, when you upload pictures to the website Flickr, you can make them public or private. If you make them public, you immediately allow Flickr's 87-million-plus members, and anyone on the world wide web who wants to join Flickr, to view your portfolio.

This immediacy in reaching out to others on such a vast scale is unprecedented. Imagine how much time and money it would have cost to show 87 million people your photos just a few years ago!

Cooperating, on the other hand, is slightly more complex than sharing, as you have to adjust your behavior in order to align with other people, and vice versa.

So, what's the simplest form of cooperation? Conversation. By conducting verbal exchanges with someone, whether in person or otherwise, you're cooperating for the purpose of relaying information.

Conversation has become far easier with digital platforms, as we now have the ability to communicate a single message to a group without being physically present in the group. The "carbon copy" email function and group text messaging are prime examples.

Twenty years ago, if you weren't physically present in the group, this type of communication would have required making hardcopies of your message for everyone individually and then spending the money to mail it to each person.

### 4. Online communities like Wikipedia are built on cooperation and love. 

How exactly do new digital platforms foster effective organization? Now that extensive coordination can happen involving very little cost, we can achieve complex work without a huge institution looking over our shoulders.

Take Wikipedia, for example, which was one of the main catalysts in changing how we organize.

You might think that, especially for a platform so vast, Wikipedia would need a budget, a work formula, and a management system. However, the only system it uses is one of a _spontaneous_ _division_ _of_ _labor_. No one needs to know all the details of a specific Wikipedia page in order to contribute to it. By collaborating with other Wikipedia users, each page becomes the result of the combined knowledge of its users.

This also means that tens, hundreds, or even thousands of users are actively monitoring a given page the majority of the time. Therefore, any errors can soon be corrected, sometimes even within seconds.

What makes Wikipedia work so well? It's a labor of love.

Contributors' sense of community and the satisfaction of knowing they've contributed to or improved something is all part of what keeps contributors returning to check on their edits and add to more pages.

It's also highly rewarding for Wiki authors, because it allows them to contribute to a topic that matters to them. Whether it's an article on asphalt or String Theory, the spontaneous division of labor, the passion of the contributors and the sense of community surrounding each article all help it grow.

And Wikipedia has not been destroyed by vandals adding false information to pages because people watch the pages, so inaccurate information is usually spotted and corrected. Edits on pages are provisional and any user has the authority to undo an edit or page deletion if they wish.

### 5. Coordinating an organization through a digital platform has far fewer managerial and cost obstacles than non-digital platforms. 

Have you ever considered how many organizational obstacles modern digital platforms help us avoid? New forms of communication have reduced the need for organizations to employ a hierarchy of employees and cost of retaining them.

Traditionally, large businesses or government agencies incur significant costs in both time and energy.

Microsoft, which manages around 100,000 employees, has to budget for employee payroll, communication between managers and their staff, and for office equipment like desks and chairs.

Without paying for these needs, the organization would collapse.

While these costs are still inescapable for organizations that operate outside of the internet, new methods of communication enable us to coordinate large-scale operations for next to nothing.

Rapid forms of communication, like email, texting and social media platforms have broken down previous barriers to forming groups quickly.

For example, prior to Flickr and other photo-sharing websites, gathering photographs from an event would have been a Herculean task, requiring tracking down attendees and requesting that they submit their pictures. However, with Flickr, people can upload photos and tag them.

We can see the effects of online coordination following the annual Coney Island Mermaid Parade in New York.

Without any financial incentives or management, Flickr users willingly upload their photos and tag them as "mermaid parade." Their photographs are automatically assigned to the mermaid parade group, which then grows as users keep adding the tag to their photos.

> _"The collapse of transaction costs makes it easier for people to get together… it is changing the world."_

### 6. Regardless of how much technologies advance, they’ll never replace our desire to meet up in person. 

We have seen how new technologies affect the way we organize. But it is important to understand that they only work because they _support_, rather than _replace_, our desire to coordinate and socialize.

Before the internet, telecommunications technologies helped us stay in touch with loved ones, but even then we prefered to meet up in person.

Inventions like the telegraph and AT&T's Picturephone were partly intended to reduce the need to travel. If we wanted to reach a loved one or friend, we could simply call them and hear their voice, rather than driving or flying. However, travel hasn't declined in correlation to our increase in telecommunications use.

Communication and travel are not mutually exclusive. We want to both communicate with _and_ see those we care about in person! Even with the internet, which allows for video chat, as well as for extensive online communities to converse in real time, meeting up with others is still our number one social goal.

In the website MeetUp, for example, users register their location and interests and are then offered suggestions for in-person meeting groups they might find interesting. Within a year after the site started in 2002, six of the top 15 most active MeetUp groups were formed around interests in websites and online gaming communities, such as Ultima and LiveJournal.

That these comprised a little less than half of the top 15 most active groups shows that cyberspace is no replacement for reality. Members of these groups coordinated on MeetUp due to their interest in certain websites and then used these shared interests as a reason to get together in person.

So what _has_ changed because of these new approaches to organization?

### 7. Forming a group no longer depends on sharing a physical location, meaning even niche organizations can flourish. 

Communities and groups no longer need to depend on physical location because every webpage essentially functions as a community; users convene on a website, creating a communal space.

For instance, by allowing users to post comments on web pages and giving them the option to participate in forums or chat rooms, the internet serves as a platform on which users can interact with each other. It is essentially nurturing groups' basic functions.

And because these websites are accessible worldwide, those with specific interests can seek out others with similar interests far more easily.

Let's say, for example, you are Pagan and in your town you know of only one or two other Pagans: not an optimal number to form an interesting group! With the internet, however, you're not confined by your town's borders. In fact, there are no real boundaries. Whether you want to start a Pagan group in your state, or get in touch with Pagans all over the world, you have a vast range of online options and possibilities.

Furthermore, since you have the entire internet-using population at your fingertips, which is an unthinkably large pool of people, it's far more likely that you'll find a substantial amount of Pagans to constitute a group of which you would like to be a member.

### 8. One industry is changing the most: the media. 

Digital platforms like social media sites serve one main function: they make it easy to share information. Activities such as reading the newspaper or tuning into a radio broadcast are no longer our primary information sources.

Newspapers, television and other mass media used to be the main way to inform the public of news, and these types of media have suffered the most from today's cheaper and faster communication. Social media is replacing old media by turning everyone who posts on the internet into a publisher. After all, if you post on Twitter or Facebook, you're technically publishing or broadcasting that information.

Moreover, digital platforms such as blogs don't adhere to the traditional format of journalistic reporting, which allows them to create newsworthy material in an innovative and interesting way.

Traditional reporting depends on sources like press briefings and government reports. However, while these sources often create newsworthy content, the content loses relevance after 24 hours.

Digital platforms, on the other hand, can afford to stray from this traditional method. Because their reporting costs are so low, they can publish or broadcast anything they think is interesting, not just what they think will captivate their audience or what advertisers will pay for.

In 2002, Senate Majority Leader Trent Lott spoke out in favor of segregation, saying America wouldn't "have all these problems" if Strom Thurmond had won the 1948 presidential election.

Most traditional media outlets viewed Lott's comment as a small part of an insignificant event, so they didn't report it. Some bloggers reported it though, and over a matter of days, millions of readers read reports and editorials criticizing Lott. The outrage gained so much momentum that Lott resigned a few weeks later.

In this case, it was the blogs that created newsworthy material that traditional mass media had overlooked.

> _"To speak online is to publish, and to publish is to connect with others."_

### 9. For some industries, the line between professional and amateur has become blurred. 

Have you ever wondered what someone means when they call themselves a "professional" on the internet? It's unclear since most of us have access to information that was previously exclusive to experts. As a result, we're seeing a rise in amatuer publishers and a dip in the professional market.

The line between professional and amateur in journalism is particularly blurry.

It used to be easy to tell who a journalist was because they were the ones writing for publications and businesses that owned equipment to help proliferate information, such as printing presses and radio stations.

Now, anyone who posts publicly to the internet is considered a publisher, because they're communicating information to a public environment that's accessible to anyone who is online. So if anyone can be a publisher, this implies that anyone can do the same job traditionally associated with professional journalists.

The same dynamic is happening in photography.

Professional photographers are under threat because of the sheer amount of photographs and amatuer photographers online. Today, taking and displaying a photo doesn't require access to a darkroom, getting published in a physical publication or even owning a camera.

Take a look at the website iStockPhoto.com, for example, where anyone can upload photographs and sell them for advertising or promotional materials.

If an advertising company is comparing two photographs — one taken by someone with minimal experience and the other by someone with 20 years experience — and the former photo is a better fit for the advertising campaign, then they're going to use the former.

Whether or not the rejected photo was taken by a professional photographer with many years of specialized training is irrelevant.

As a result, many professionals in media and communications-based industries are seeing their worth diminish, whereas amateurs are now enjoying unprecedented exposure.

> _"An individual with a camera or a keyboard is now a non-profit of one, and self-publishing is now the normal case."_

### 10. Final summary 

The key message in this book:

**The** **internet** **has** **created** **a** **new** **era** **for** **humanity,** **in** **which** **communication** **is** **faster** **and** **cooperating** **is** **no** **longer** **restricted** **to** **physical** **location.** **Although** **this** **has** **enabled** **us** **to** **create** **groups** **and** **organize** **more** **rapidly,** **it's** **essential** **to** **remember** **that** **these** **tools** **facilitate** **our** **human** **qualities** **rather** **than** **replace** **them.**

Actionable advice:

**Stay** **abreast** **of** **the** **newest** **communication** **technologies.**

If you're in the media industry and your work isn't garnering the attention it deserves, maybe it's time to reconsider your approach. Are you up-to-date with all the latest technologies? Are you using digital platforms to keep ahead of the news curve?

**Remember** **your** **need** **for** **physical** **community!**

If you have a huge social network around you but still feel a sense of loneliness, perhaps you need to spend more time engaging in the physical community. Having 1000 friends on Facebook might make you feel popular, but none of that matters if you don't have a solid group of people that are supportive and care for you in person.
---

### Clay Shirky

Clay Shirky teaches and consults on how the internet affects society. He is currently an Associate Professor at New York University's Interactive Telecommunications Program and the Journalism Department. He has consulted for Lego, the US Navy, BBC and Procter and Gamble and his work has been published in _The_ _New_ _York_ _Times_, _Wall_ _Street_ _Journal_ and _Wired_ magazine.

