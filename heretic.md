---
id: 579260be172bb20003045632
slug: heretic-en
published_date: 2016-07-28T00:00:00.000+00:00
author: Ayaan Hirsi Ali
title: Heretic
subtitle: Why Islam Needs a Reformation Now
main_color: 196580
text_color: 196580
---

# Heretic

_Why Islam Needs a Reformation Now_

**Ayaan Hirsi Ali**

_Heretic_ (2015) takes an unblinking look at Islam and issues a call for reformation. By examining the fundamental scriptures of the Qur'an and Islamic law, we can find plenty of evidence to suggest that Islam has far too much justification for violence written into its core belief system. Find out why it's not too late to change things and how the time might be perfect for an Islamic reformation.

---
### 1. What’s in it for me? See why Islam is inherently violent and needs reformation. 

Ever since the terrorist attacks of September 11, 2001, we've lived in an era of brutal violence committed in the name of Islam, especially these days, with the rise of terrorist groups such as Islamic State (IS) and Boko Haram.

But what is it that motivates Muslims to commit terrorist crimes? And how can such crimes be justified by religion?

These blinks offer insights on the religious foundations of Islamist terrorism, and the ways in which Islamic scriptures support violence against anyone who doesn't believe in Islam or abide by its rules. See why a Muslim reformation is urgently needed, and why it has stalled so far.

In these blinks, you'll learn

  * why it's so difficult for Muslims to criticize Islam;

  * why some Islamic countries still apply seventh-century sharia law; and

  * how an Islamic reformation will emerge.

### 2. Islam is not a peaceful religion, and Islamic terrorist groups and Islamic states commit violence in its name. 

In order to understand why crimes are committed in the name of Islam, and to be able to resolve this problem, we need to look at the core concepts and foundational texts of Islam. More importantly, we need to reconsider what they mean.

Because as it stands now, Islam is not considered a peaceful religion.

Of course, the vast majority of Muslims are peaceful. However, the holy text of Islam, the _Qur'an_, justifies violence by explicitly allowing for it in certain situations. For instance, the Qur'an says violence is justifiable in cases of blasphemy, adultery or when family honor is threatened.

More importantly, though, Islamic terrorists believe that the Qur'an _calls_ for violence.

The Kouachi brothers believed that the magazine _Charlie Hebdo_ had committed blasphemy by printing cartoons depicting Muhammad. Therefore, they believed their killing spree at the magazine's offices on January 7, 2015, was justified by the Qur'an.

They clarified this belief by sparing the life of a female employee, explaining that they didn't kill women and telling her, "What you are doing is bad. I spare you, and because I spare you, you will read the Qur'an."

In addition, Islamic terrorist groups such as Al Qaeda, Islamic State and Boko Haram cite religious texts when advocating violence.

But these beliefs aren't just the interpretation of individuals or terrorist groups; some countries also follow these beliefs. In Pakistan, criticism of the Prophet Muhammad or of Islam is considered blasphemous and punishable by death.

In Saudi Arabia, the Islamic royal family allows beheadings. One recent spate occurred in August, 2014, when a beheading occurred almost every day. Meanwhile, in Iran, stoning is an acceptable form of Islamic punishment and homosexuality is a crime punishable by hanging.

If terrorists and states believe that Islam supports their violent acts, it makes sense to hold the religion and the Muslim community accountable for perpetuating this belief. In the next blink, however, we'll see why this is difficult.

### 3. There are three groups of Muslims. 

To believe in Islam is to believe in the Qur'an, which contains the revelations of the Angel Gabriel to the Prophet Muhammad, and the _Hadith_, which details Muhammad's life and words.

All Muslims share these core beliefs. But, obviously, not every Muslim has the exact same beliefs. One major source of their differences comes from the Prophet Muhammad's changing behavior over time.

At the beginning, Muhammad was in Mecca and, much like Jesus, went door-to-door peacefully spreading his message. He told people that Allah was the only God and that he was Allah's messenger.

Later, after the Prophet went to Medina, people who didn't believe or refused to submit to Allah were attacked and given the option of either converting or dying. Due to these conflicting images, there are now three different groups of Muslims.

The first group are fundamentalist _Medina Muslims_, who honor the Prophet's later behavior.

Their beliefs are based on _sharia_, or Islamic law, which has basically gone unchanged since the seventh century. They see it as a requirement to impose their faith on others, and they do not recognize other religions. They also glorify martyrdom, and justify the deaths of those unwilling to convert to Islam.

The second group are _Mecca Muslims_, devout Muslims who don't practice violence. This is how the majority of the world's Muslim community identify themselves.

But the beliefs of Mecca Muslims can still be at odds with modern culture, which can result in Muslim immigrants in Western societies withdrawing and shielding themselves from outside influence. For example, protective Muslim parents in these countries often only allow an Islamic education for their children. This, in turn, can lead the kids to either abandon their faith to escape religious pressure or rebel and become a Medina Muslim.

The third group are _Modifying Muslims_ who think critically about their Islamic faith and, at their most extreme, might leave the religion altogether.

These Modifying Muslims could become the leaders of Muslim reform — indeed, Western nations should support them in doing just that.

> _"...an increasing proportion of organized violence in the world is happening in countries where Islam is the [main] religion."_

### 4. A Muslim reformation depends on the abandonment of five religious tenets and a reinterpretation of the Qur’an. 

While there have been calls for change and a Muslim reformation in the past, these attempts have rarely said what precisely needs to be changed.

So, to be a bit more specific, here are five central tenets of the Islamic faith that need to be changed or abandoned.

The first tenet is the semi-divine status that Muhammad has and the literalistic interpretation of the Qur'an.

The second is the idea that people should focus more on life after death than on life before death.

The third is the promotion of sharia — Islamic law –as the general governing system.

The fourth is the empowerment of individuals to enforce Islamic law.

And finally, the fifth tenet is the authority to wage _jihad_, or holy war.

These five interrelated tenets are all inherently harmful. The most dangerous consequence is that people can use them to justify violence through sharia law and jihad. But all of them promote outdated beliefs and are considered by many to be untouchable, beyond criticism.

Tawfik Hamid is an Islamic reformer and former member of the same radical organization as Al-Qaeda leader Ayman al-Zawahiri. He says that the literalistic reading of the Qur'an is easily misused to justify Islamist terrorism.

For example, the quote "Fight those who do not believe in Allah," from verse 9:29 in the Qur'an, is often interpreted to justify violence, to literally fight anyone who doesn't believe in Allah.

Interestingly enough, history suggests that advocating for a reinterpretation of the Qur'an isn't so outlandish. Previously, contradictory statements in the Qur'an have been renounced and reinterpreted, and Islamic doctrine has adapted over time. One way this happens is through _abrogation_, a term for what happens when newer revelations replace older ones.

We should be hopeful that, through abrogation or reinterpretation, the more militant verses from, say, Muhammad's Medina period, will be de-emphasized in favor of less-militant parts of the Qur'an.

> _"Multiculturalism should not mean that we tolerate another culture's intolerance."_

### 5. Muslim reformation has been blocked by the inability to criticize Islam and the interconnectedness of religion and state. 

Making a Muslim reformation happen is easier said than done. As we've seen, many Islamic countries reward criticism of Islam with severe punishment or even death — that's obviously a major reason why many attempts at reform have stalled.

Part of the reason criticism is considered blasphemous is the belief that the Qur'an is unquestionably timeless and perfect. Therefore, the idea of reform simply doesn't exist in Islamic doctrine, unless it means returning to Muhammad's first principles.

This is the kind of regressive "reform" that Islamist terrorists fight for: they adhere to the Hadith that contains the words and deeds of Muhammad, where it states that Muhammad's generation would be better than the rest.

Another reason for rejecting criticism is the fear that critical thought could lead many to leave Islam.

Yusuf Al-Qaradawi agrees with this assessment. Al-Qaradawi is a prominent leader of the Islamist Muslim Brotherhood, and he's said that if Islamic law didn't punish defectors, Islam would have ceased to exist.

Another roadblock to Muslim reformation has been the connectedness of Islamic religion and state.

In Europe, the separation of church and state in the early modern era played a crucial part in the Christian Reformation. But religion and state are still strongly intertwined in the Muslim world. This bond includes 17 Muslim-majority nations that claim Islam as the state religion.

Part of the reason for this connectedness is that Muslim clerics view Islam as more than a religion. For many people it is seen as a holistic system that encompasses every aspect of life, informing their political, social, personal and religious beliefs.

> _"In no other modern religion is dissent still a crime, punishable by death."_

### 6. Islam differs from other religions in its unquestioned religious beliefs and strict social order. 

Unlike Christianity or Judaism, Islam is unique in the way it views both its sacred Prophet and Scriptures.

For Christians, Jesus is a divine being — but they understand that the Bible was written by men. And while Jews believe in the sanctity of the Torah, they attribute the writings to Moses, who is seen as an imperfect human prophet.

However, Muslims believe in the divine perfection of both Muhammad _and_ the Qur'an. That's why criticism of Islam is viewed as blasphemy.

But criticism is vital for reform and change to occur, and for this to happen, Muhammed needs to be seen as a human who lived in the context of his time. Likewise, the Qur'an needs to be seen as a historically constructed text of its time and something that is now antiquated and in need of reinterpretation. Plus, unlike for Abraham or Jesus, there is plenty of evidence that Muhammad was a historical human figure: in addition to being a prophet, he was also a conqueror and lawmaker.

But what sets Islam apart from other religions is its strict social order, which further entrenches Islam as more than a religion. As social anthropologist Ernest Gellner points out, the religion draws up a "blueprint of social order."

Muhammad himself established a system of moral and political rules known as the _Constitution of Medina_ that evolved into sharia law. It provides rules such as the prohibition of stealing and the right way to organize Muslim society.

Its rules are instilled through a sense of honor or shame within Muslim society. For instance, if a Muslim does or says something shameful, he or she risks being expelled from the community entirely; in some cases, this is done through a so-called "honor killing," a murder committed by another member of their family or community.

In many ways, Islam can be seen as a religion of submission. After all, the commandments in the Qur'an aren't suggestions; they're laws that dictate believers' religious duties.

> _"A key problem for Islam today . . . Christians worship a man made divine. Jews worship a book. And Muslims worship both."_

### 7. Islamic sharia law is outdated but still applied, and some expect it to be enforced by all Muslims. 

You may have heard the word _sharia_ on TV or read about it in newspapers — it's quite a controversial subject these days.

In previous blinks, we've seen that sharia is another name for Islamic law, that it was derived from the Qur'an and how it organizes all public and private life. This sharia is still enforced today, despite the fact that it permits outdated and cruel forms of punishment.

For starters, sharia contains rules on which types of beatings a husband is allowed to give his wife. It also offers a variety of punishments to those who wage war against Allah and His Messenger, including execution, crucifixion and cutting off hands and feet.

In fact, these kinds of punishments, as well as beheadings, stonings and lashings, continue to exist in many Islamic countries, including Saudi Arabia, Sudan, Iran and Pakistan.

Under Sudan's Islamic sharia law children automatically assume the religion of their father, and Muslim women are forbidden to marry non-Muslims, even though this rule does not apply to men.

Outdated laws such as these have remained unchanged since the seventh century because they are seen as God-given, coming directly from Muhammad's divine revelations. Therefore, it would be blasphemous even to suggest they are wrong.

As such, enforcement of sharia law is expected from all Muslims, even within the family.

In 2009, a man in Arizona drove his car over and killed his 20-year-old daughter, Noor al-Maleki, for liking makeup, boys and Western music.

The father was sentenced to 34 years in prison, but many in the local Iraqi community defended his brutal actions: one woman told _TIME Magazine_ she approved of the father's actions, since the daughter's behavior was not in accordance with Islamic law.

> _"Social control begins at home."_

### 8. Jihad has ingrained violence in Islam and must be forbidden by Islamic authorities. 

Jihad is another term that's heard often these days. You might think that Islamic terrorists and jihadist groups misuse Islamic scriptures to justify their violence.

However, these scriptures actually don't need to be changed in any way to support violence and terrorism in the name of Islam. After all, jihad is deeply ingrained as a religious obligation of Islam.

Jihad is as old as the Qur'an; Muhammad himself waged a holy war against unbelievers, and since then, fighting on behalf of Islam has been embedded in the Qur'an.

In verse 8:39, it says that Muslims shall fight the unbelievers "until . . . there prevail justice and faith in Allah altogether and everywhere."

So, according to the Qur'an, the goal of every Muslim must be to make Islam the world's only religion. It might sound extreme, but you can go on any social media platform today and find plenty of people talking openly about reaching this goal and trying to recruit others to help.

While it's been suggested that those calling for jihad are impoverished, uneducated or brainwashed, this isn't the case.

Take Aafia Siddiqui, also known as "Lady Al-Qaeda." She's a former MIT scientist who turned to Al-Qaeda and was caught planning a chemical attack in New York. For these kinds of attacks to end, and for Islam to be considered a peaceful religion, Islamic authorities need to forbid jihad.

This means that clerics, imams, scholars and Islamic national leaders need to take action and prohibit jihad. If they don't, there are simply too many Islamic scriptures that jihadists could use to justify their violence.

History has set a precedent for this: Christianity used to be an extremely violent religion, as the Crusades will attest to, but Christian leaders eventually gave up its militancy. And to help the clerics make this happen, people have to stop suggesting that Islam is a peaceful religion! Such a suggestion will only justify the authorities' not taking action.

> _"It is not we in the West who must accommodate ourselves to Muslim sensitivities; it is Muslims who must accommodate themselves to Western liberal ideals."_

### 9. Fortunately, there are early signs of an Islamic reformation. 

Despite all the obstructions, there is already a growing number of people in favor of a Muslim reformation. After all, Muslims are just like everyone else: they want the best life possible for themselves and their children. Radical fundamentalism has little to offer in that regard.

Three factors are coming together to make this the perfect time for real reform.

First, the internet and social media are bringing like-minded Muslims together and giving them a safe way to spread the call for reform to a wider audience.

Second, Islamist governments are failing to deliver on their promises, causing Muslims to look to Western countries for a better life. And with more Muslims moving to Europe and North America, they'll inevitably be led toward reform.

Even if the increase in Muslim immigrants leads to conflict, "cocooning" or even turning toward radical fundamentalism, common sense will prevail in the long run. It's only a matter of time before these options become less appealing than reform, because it's the only option that leads to a better life.

Third, religious reform-minded groups are emerging in important Islamic states. For instance, millions of women in Afghanistan believe that they should be able to vote and are lining up to do so despite Taliban threats.

Furthermore, the horrors of Islamic State, Boko Haram and Al-Qaeda are seen as deplorable to many Muslims. Take the Arab Spring, for example. Although it was a failure in many ways, it shows that many ordinary Muslims want major changes.

One of the best signs of reform comes from the government of the United Arab Emirates: they've called the threat of Islamic extremism a "transnational cancer" that is in need of "urgent, coordinated and sustained international effort" to confront it.

> _"The right to think, to speak, and to write in freedom and without fear is ultimately a more sacred thing than any religion."_

### 10. Final summary 

The key message in this book:

**There's no way around the fact that Islam is partly to blame for the many acts of terror committed in its name. It is impossible to separate these acts from the religion itself, or to suggest that it is all the act of brainwashed people. To make Islam a truly peaceful religion, it needs reformation, and the present is the perfect time to put some important changes in motion.**

Actionable Advice:

**Have your say!**

Don't be intimidated by anyone into silencing your thoughts. In order to resolve conflict, we first need to be able to voice our doubts or criticism about a subject — any subject, from Qur'anic interpretation to nuclear waste — and then have a peaceful discussion.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Muqaddimah_** **by Ibn Khaldūn**

**_The Muqaddimah_** **(fourteenth century, first English edition 1958), a classic text on the Islamic history of the world, focuses on the rise and fall of civilizations. It offers a unique glimpse into the world of the fourteenth-century Arab Muslim, and is regarded as a foundational text in several academic disciplines.**
---

### Ayaan Hirsi Ali

Ayaan Hirsi Ali is an award-winning human rights activist and fellow at Harvard University's John F. Kennedy School of Government. Growing up Muslim in Somalia, she moved to the Netherlands, where she went from cleaning factories to becoming a member of the Dutch Parliament. Her other books include _Infidel_ and _Nomad_.

