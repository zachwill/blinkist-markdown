---
id: 5a23ac82b238e100062d55c3
slug: hes-just-not-that-into-you-en
published_date: 2017-12-08T00:00:00.000+00:00
author: Greg Behrendt and Liz Tuccillo
title: He's Just Not That Into You
subtitle: The No-Excuses Truth to Understanding Guys
main_color: D03EB1
text_color: 9E2F86
---

# He's Just Not That Into You

_The No-Excuses Truth to Understanding Guys_

**Greg Behrendt and Liz Tuccillo**

_He's Just Not That Into You_ (2004) offers women useful advice on how to identify when a male love interest simply isn't interested in them. The authors provide classic examples of the ways women make excuses for men's disrespectful behavior, or how they fool themselves into thinking that a man's lack of interest is their fault. By taking a refreshingly simple approach to men's supposedly complicated mixed messages, this book empowers women to move on and find someone who _is_ right for them.

---
### 1. What’s in it for me? Get the message he’s sending. 

_"_ He's a little reserved." "It's not his fault he's a timid guy." "He's just in a very busy period of his life."

Have you ever caught yourself defending guys who aren't giving you the love and attention you need? Well here's the no-excuses truth that might seem a little brutal, but which will liberate you further down the line: maybe he's just not that into you!

In these blinks, you'll come to learn that mixed messages don't exist when it comes to dating, and why you're better off untangling yourself from men who aren't fully committed to you. You'll discover the telltale signs that show whether you're dating someone who is simply not that interested.

You'll also find out

  * which word is a weapon of mass destruction in relationships;

  * how to handle the mystery of the vanishing man; and

  * what to do about those mythical happily-ever-after stories.

### 2. He’s just not that into you if he’s not calling you or asking you out. 

Men still occupy most managerial roles in the workplace. They know how to command an office full of people, so why do so many women still dupe themselves into believing that men are incapable of picking up the phone and asking them out?

Too many women are far too eager to make excuses like, "Oh, maybe he's just shy," when they know deep down that's not the case. Or, there's the age-old excuse that he's scared of ruining the friendship the two of you share, which women also readily accept.

At the end of the day, men like to get what they want. So, if he's really into you, you'll know it, because he'll actively make an effort to pursue you in order to win you over.

When a man is attracted to a woman, either seeing her as a future girlfriend or just a hookup, he won't be able to stop himself from trying to take things further; in other words, jeopardizing a friendship isn't an issue for him. If a guy claims that he's "scared" of ruining your friendship, what he's actually saying is that he's not into you.

To verify this claim, the authors of this book polled 20 of their male friends, aged 26 to 45, who were in long-term relationships. They found that none of their relationships started with the women asking them out first. In fact, one guy said outright that it would've spoiled the fun for him if his girlfriend had done so.

Another way some women try to excuse the fact that a guy hasn't called them is by convincing themselves that he's really busy. In reality, no guy is too busy to go for what they want.

The word "busy" serves as the _weapon of mass destruction_ in relationships. Sure, it sounds like a reasonable excuse, but it's used by men who can't be bothered to call.

In the same poll, the authors found that 100 percent of the men they polled never found themselves too busy to call a woman that they were genuinely interested in.

> _"Men (for the most part) like to pursue women. We like not knowing if we can catch you. We feel rewarded when we do."_

### 3. He’s just not that into you if he doesn’t want to be your boyfriend or have sex with you. 

The beginning of a relationship is always extremely confusing. When a guy and girl have been seeing each other for a while, the same question arises: "Are we boyfriend and girlfriend?"

In a lot of cases, if a guy starts regularly spending time with a girl, or sleeping with her on a frequent basis, she'll often start to think they're heading toward boyfriend/girlfriend territory. Some women tend to think this even after a guy has explicitly said he doesn't want a girlfriend.

If he's not interested in being your boyfriend, it's not because he has "intimacy issues" which he'll eventually overcome; realistically, if he doesn't ask you to be his girlfriend, he's just not that into you.

In the end, if a man is definitely into a woman, he'll want her all to himself. And the only way to ensure that and win her over is to ask her to be his girlfriend.

The poll that the authors conducted showed that 100 percent of the men surveyed said that fear of intimacy has never stopped them from embarking on a new relationship with a woman they really liked. One man ridiculed fear of intimacy as an urban myth, while another said that it's what men tell women when they're decidedly not into them.

Another sign of disinterest that you should watch out for in the early stages of a relationship is if a guy doesn't want to have sex with you. In the poll, every man said that they'd never been truly interested in a woman that they didn't wish to sleep with. So, remember: if he doesn't show any sexual interest, he's not that into you.

### 4. He’s just not that into you if he cheats or is married to someone else. 

When men cheat, they often have a plethora of excuses to choose from: you gained weight, my sex drive is higher than yours, I'm just not attracted to you in the same way. Most women take these excuses to heart, absolve the man in question from blame and instead blame themselves — but this isn't how they should react at all!

If he's having sex with another woman, he's just not that into you. In fact, he's being completely disrespectful.

It's difficult to maintain the same level of sexual attraction when you're in a long-term relationship, and it can also be the case that one partner has a higher sex drive than the other. The best way to deal with both these obstacles is to have an open conversation with your partner about them, which would allow you to respectfully address any issues.

But, unfortunately, a lot of men choose the disrespectful way of dealing with their relationship problems. Many choose to betray and humiliate their partners by cheating on them — and to add insult to injury, they often make it seem as if it's their partner's fault, at a moment when she is at her most vulnerable.

If a man ever treats you this way, recognize that this isn't the behavior of a man who cherishes and respects you. Save yourself the heartache by leaving that person and finding someone who treats you right.

In much the same way, women in the reverse situation who are dating married men often find ways to excuse their behavior. But the truth is that a relationship built on good intentions should never exist in secrecy.

To keep her hanging on, a guy may tell a woman that he's not ready to leave his wife yet because he's not willing to put his children through a divorce. Clearly, there's no point in holding out hope that his marriage will ever end. Instead, what you should be doing is looking for a relationship that you can openly enjoy; look for a man who's emotionally available rather than one who is married and conflicted.

### 5. He’s just not that into you if he’s ended your relationship. 

When someone breaks up with you, it's common to hope that they'll call you up just to tell you how much they miss you. You can end up fixating on this idea so much that you convince yourself that your ex is still interested in you.

But if they do make contact, the best thing you can do is hang up and move on with your life. In the end, the cold, hard truth is that if a man has broken up with you, even if he calls, texts or emails, he's still not necessarily into you.

Perhaps there's a part of you that believes if you stay in touch, he may eventually remember how amazing you are and your relationship will get back on track — but this is rarely the case.

Most of the time, a guy who keeps contacting his ex-girlfriend simply can't handle being alone and is using her to make the switch to singlehood easier. Your ex may even say he misses you, but if he was really still into you, he'd do his utmost to get you back.

All of the men the authors polled said that when they broke up with someone, it definitely meant they no longer wanted to be in a relationship with them.

However, what about situations where the guy disappears without officially breaking up with you?

If a guy pulls a Houdini on you, it's a clear sign of his disinterest, and, you guessed it; you urgently need to move on.

The reason women try to contact men who have pulled a disappearing act on them is because they want some clarity. But by ghosting, he's already made his intentions crystal clear: he's not interested in pursuing a relationship anymore. By trying to contact him, you're just making his silence even more deafening.

If someone doesn't have the decency to break up with you properly, don't waste another minute of your time on them.

### 6. He’s just not that into you if he’s putting off marriage. 

"He's not ready yet" or "Money's really tight at the moment" are just two of the excuses some women make for their partners when they show no interest in tying the knot. 

A man may even tell you that he has "issues" with the institution of marriage to worm his way out of the situation. But whichever way he explains himself, or whatever excuses you concoct for him, if a guy who's been in a stable, long-term relationship says he's not ready for marriage, what he's really saying is that he's not sure he wants to marry you. 

Out of the men that the authors polled, 100 percent said they wouldn't have a problem marrying a woman they were sure was "the one." One man even said that he could hardly imagine a man who's stupid enough to have an issue with marrying the love of his life.

So, when a man finally finds the love of his life, it'll be game-changing for him. He's highly unlikely to want to jeopardize his situation by telling the woman in question that the idea of being legally tied to her for the rest of his life is seriously off-putting.

If your partner is showing disdain toward the idea of marriage, don't feel ashamed if you have the desire to get married; it doesn't make you needy or unliberated to feel such a way.

It's perfectly acceptable to ask your long-term boyfriend if he can see himself being married in general, or specifically getting married to you. There are lots of men out there who want to get married someday — imagine all the wedding planners, florists and taffeta manufacturers who would be out of business if there weren't!

Another issue that may be playing on your mind while waiting for your boyfriend to pop the question is that your biological clock is ticking. So, you can either stick around and hope that you'll one day play the role of your boyfriend's wife, or you can take yourself out of the relationship and meet someone who wants to spend a lifetime with you.

### 7. Remember to treasure your freedom after bad experiences and don’t listen to silly happily-ever-after stories. 

So, you've worked up the courage to let go of the guy who's just not that into you. Maybe you've treated yourself to luxurious bubble baths or some chocolate to make yourself feel better, which is a great way to spend your time.

However, during this stage, some women find themselves preoccupied with thoughts and activities that are a waste of time. A lot of women end up indulging in happily-ever-after stories that aren't healthy or realistic.

You've probably heard such stories before — the one where the man doesn't call the woman he slept with and she waits for him to phone her for weeks on end. Finally, he manages to get in touch with her and suddenly they're together and deeply in love. Or the one where the man treated his woman horrendously for the first few months of their relationship, but now they're happily married with two children.

These stories are a complete waste of time, as they don't help you heal and move forward. Sometimes, there are exceptions to the rule, but if you're honest with yourself, these exceptions probably don't apply to your situation.

Instead, they make you hope against all reason that you could still have a future with the guy who just isn't that jazzed about you.

So, the next time a good friend tries to console you with a story about a woman who was treated badly by the guy she was seeing, but that it all miraculously worked out in the end, just stick your fingers in your ears and yell "la-la-la-la!"

Another aspect to consider after breaking up with a man who was just not that into you is how good it feels to be out of the negative relationship.

It takes a tremendous amount of emotional energy to keep making excuses for someone and to keep trying to "figure him out." Now that you no longer have to sit around obsessing over him, you can fill your days with positive activities, like going to yoga or meeting up with friends.

Think of how wonderfully empowering it'll be to have the mental strength to stand there and say, "He wasn't really interested in me," and be fine with it. Just because he isn't or wasn't that into you, doesn't mean that you can't flourish as a single woman and find someone who's very much into you in the future.

> "_Make a space in your life for all the glorious things you deserve_."

### 8. Final summary 

The key message in this book:

**When it comes to men and romantic relationships, mixed messages don't exist. If he likes you, you'll know it — he'll schedule your next date, call when he says he will and be willing to commit to you wholeheartedly. If he's not behaving this way, he's just not that into you. Women and their female friends often try to make excuses for disrespectful male behavior, but the most empowering action you can take in these situations is simply to walk away and not waste your time.**

Actionable advice:

**Evaluate your romantic relationship from the perspective of a good friend.**

If you suspect that the man you're dating is not good for you, get a tape recorder and record yourself telling the story of your entire relationship. Now listen to the tape, but imagine that it's not your own story but that of your best friend. What advice would you give her? Would you tell her she deserves better? If you would want something better for her, ask yourself why you shouldn't want the same sort of happiness.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Labor of Love_** **by Moira Weigel**

_Labor of Love_ (2016) is your guide to the history of dating. These blinks walk you through the social, cultural and economic shifts that have shaped modern rituals of courtship and explain the curious fads and fashions of flirtation that have come and gone through the ages.
---

### Greg Behrendt and Liz Tuccillo

Greg Behrendt is an American author and comedian. He worked as a script consultant on the HBO series _Sex and the City_ and has also hosted self-help television shows.

Liz Tuccillo was also a writer for _Sex and the City_ and published her first novel, _How to be Single_, in 2008. The book was made into a feature film in 2016.

