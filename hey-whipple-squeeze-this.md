---
id: 547dbb283166390009a30200
slug: hey-whipple-squeeze-this-en
published_date: 2014-12-04T00:00:00.000+00:00
author: Luke Sullivan with Sam Bennett
title: Hey Whipple, Squeeze This!
subtitle: The Classic Guide to Creating Great Ads
main_color: 46454C
text_color: 46454C
---

# Hey Whipple, Squeeze This!

_The Classic Guide to Creating Great Ads_

**Luke Sullivan with Sam Bennett**

_Hey Whipple, Squeeze This!_ has become a seminal guide to the world of advertising for those who have been in the business for decades, for newcomers, and for anybody intrigued by what happens when creativity meets commerce. The title is an irreverent nod to an unconventional 1970s campaign for Charmin toilet paper that featured an annoying shop clerk called Mr Whipple, who couldn't stop squeezing the product. It sets the tone for Sullivan's honest and practical insights into the sometimes crazy creative process of advertising.

---
### 1. What’s in it for me? Learn how to write ads that work. 

When we read about advertising, we often think about _Mad Men_ : A bunch of suave, creative geniuses who lounge around drinking all day until—boom! their idea blows the client away and the day is saved.

In fact, advertising is a lot messier. It involves grappling with clients and ideas and partners and products, all in pursuit of the dream of conveying the simplest message for customers to relate to. But how? How can you make earth-shattering ads that seem to sum up the whole world in 30 seconds or less?

_Hey Whipple, Squeeze This!_ explains the secret to getting a job in advertising, tells you how to write ads for many mediums, and provides numerous tricks to squeeze the genius out of your ideas.

In these blinks you'll learn

  * why writing ads is like washing a pig;

  * how Volkswagen played with its own weaknesses in classic ads; and

  * how to write conversations that sound real.

### 2. To get started in advertising, you need an exceptional portfolio. 

Many years ago, a few good ideas and a passion for the business were enough to get you hired by an agency. Today, it's a different story. The industry has become so popular that you'd better have an outstanding portfolio, or _book_, if you want to get your big break.

Your book is both your job application and your talent, and should set you apart from the crowd. This is vital, as the competition for ad jobs grows more intense every day, as more and more students complete advertising degrees with highly polished books. But you don't have to go to ad school to create an outstanding book.

You need just eight or nine hypothetical campaigns that have been worked to an excellent standard. How do you do this? By concentrating on campaigns based on good ideas. Agencies aren't impressed with a good _finished_ ad — they're much more interested in _ideas_ that reflect unique talent.

Here's an example. A famous _Nike_ ad shows Michael Jordan jumping and scoring, with the line "Michael Jordan: 1. Isaac Newton: 0." This ad could be in its earliest stage, even crudely hand drawn, and the brilliant idea would still shine through.

But where do great ideas come from? Begin by developing ideas around products that you like and use. Then take a look at old, award-winning ads. Why did they work, what was their secret? After analyzing them, return to developing your own ideas, but this time choose products that you'd never use yourself.

For example, if you're a guy, consider ideas for bridal magazines, or if you're a girl, develop ads for jock straps. This will show agencies that you can apply your talents in more than one field.

> _"A book full of cool ads doesn't guarantee you a great job. A book full of brilliant thinking does."_

### 3. Creating ideas is hard work. 

Sorry, but _Mad Men_ just isn't an accurate portrayal of work in advertising. It's not all affairs and parties! The truth is, the whole process of creating ads can be extremely chaotic, and creating successful work takes serious effort.

Working in advertising means giving your best for every new client. But this can be challenging when each client is wildly different to the next. One day you might work for the financial sector and ponder market-indexed annuities, while the next you could be thinking about the difference between "kibble" and "bit" for a dog-food ad.

Because of this, the creative process is like "washing a pig." What on earth does that mean? Well, it's an absurd process, with no beginning, no end, and no rules. Still not clear on what that entails? Well, think about how would you approach the task of cleaning a hog.

Let's say your account person tells you they need a pig washed by 3 p.m. tomorrow — this is the "advertisement" your client needs. You quickly jump online and search in vain for a guide on pig washing — this is your hunt for ideas and inspiration. But you've never washed a pig before, so despite the instructions, your first attempts fail and the pig gets away every time — just like coming up with ideas that never seem good enough.

It's nearing 2 p.m. and your partner has just figured out how to keep the pig in place by feeding it vanilla wafers. Finally, you've got it pretty clean — this is when your idea thankfully seems to be working. But then at 3 p.m., in comes your client. He says that he actually needs a warthog washed — and that's when you discover that all your work was useless and you have to start over from the beginning.

However, once you've reached this point, there's no reason to cower! Read on to the next blink to find out how you can pick yourself up, dust yourself down and get the creative process going again.

> _"Creativity is exactly like washing a pig. It's messy. It has no rules."_

### 4. Even though the creative process is tough, there are ways to regain your flow. 

If you're a creative working on an idea, you probably feel incredibly stressed from time to time, or perhaps all the time. Nevertheless, you've got to stay calm, even when you're blocked. There are a bunch of tricks that can help you get into the flow again.

Like Nike says, sometimes it's best to _just do it_. Without thinking about it, start writing something. Start by scrawling down, "This is an ad about . . ." Next, put down whatever comes to mind. You'll be surprised how quickly you'll discover the words you needed.

Another trick is to put yourself into the customer's head, by thinking hard about how they'll _feel_ when using the product. Emotional appeals are far more powerful than rational reasons when we buy goods and services. Say you're working for Hollister, who create casual clothes for teens — you'll want these teens to feel cool, comfortable and, most of all, confident!

After determining what feeling you want to evoke, you've got to get into the right headspace. The author once had to produce work for a magazine called _Family Life_, and show that raising a child is one of the most moving things you can do.

However, he'd never raised a child himself, so he sat down to read a book about the joys and insanities that come with parenthood, and to sift through photos of happy families. The right mood hit him, and soon he struck gold, with the tagline "Life is short, childhood is shorter."

Finally, if you work with a partner, use your relationship productively. You and your partner may initially approach the task separately to preserve your different perspectives. But if you both reach a block, don't hesitate to bring your ideas together — even the bad ones. That way, your incomplete work can be combined into a winning concept.

Now that we know how to get into the zone for creating ads, how can we pin down what makes a great ad? Read on to find out.

### 5. Keep it simple, keep it honest! 

Seen any great ads on TV lately? The ones that impress us seem to tell surprising stories and present them immaculately, with great visual effects. Seems like pretty complicated work, right? But they often come from nothing more than clear and simple statements. How can we ourselves come up with these?

First, try to find one great adjective that represents the brand. Your audience has a limited attention span and is under constant bombardment with information, so it doesn't have time to find out all that your brand stands for. Instead, it's your job to make it clear to them from the outset. What you need is just one memorable word to make a lasting impression.

Here's an example of how this works in practice. At each speech he gives, the author asks his audience what Volvo stands for, and gets the same one-word answer every time: "Safety." But Volvo isn't even on the top ten list of the safest vehicles on the market. Instead, they've got a brand image that could only have been achieved by fantastic advertising.

However, there's one thing Volvo could do to polish the brand image still further. To create a great ad, you've got to find out the truth about the product, or say an old truth in a new way. For example, we think about hair coloring as a means to look younger. But what if we saw a dye job as a self-esteem builder?

In this way, you can even turn unpleasant truths into success. In 1964, Volkswagen flipped its underdog status with a wry ad showing a small Beetle with the line: "It makes your house look bigger." Suddenly, the weakness of the ultra-compact vehicle design became attractive, and in a funny way, too.

However, times have changed since 1964. Advertisers today have a whole host of new challenges to face if they want their product to stand out. Read on to find out how they tackle them.

### 6. Brand image must be backed up by actions, and it has to stand out. 

Decades ago, ads written for TV, magazines, radios and billboards could depend on the strength of their ideas for success. But now, with the internet and smartphones, advertisers are working in an entirely different world.

We've got information at our fingertips, and people aren't willing to just sit in front of the TV and hear companies extol their products' virtues. Ads have to be backed up by actions, as consumers can question company messages and find the truth online. Companies must operate the way their ads promise.

In their "Give it Back" campaign, Coca Cola didn't just _say_ they cared about sustainability — they also used recyclable materials to display their products in stores, so customers could see it and believe it.

In fact, we can take this approach even further, by considering how a company can use its influence to improve lives. The technical term for this is _brand generosity_, where a product is designed to bring not just convenience, but beauty and joy to consumers.

A stunning example of brand generosity was created by the agency Farfar to raise awareness of a new Nokia navigation system. They constructed huge arrow-shaped screens and hung them on cranes throughout London. People near the arrows could text the name of their favorite place to the screens, and the arrow would rotate in the direction of that place and calculate the exact distance to it.

Now that we've seen the approaches that are necessary to win the support of consumers today, let's take a closer look at the nuts and bolts.

> _"The thing is, we don't get people talking about our brands by feeding them product benefits out of the sales guy's spec sheet."_

### 7. Successful TV ads first surprise us, and then make us think. 

Most of us dodge the commercial breaks for a trip to the kitchen. So how can TV ads, or _spots_, grab the attention of their viewers?

By telling the story in a powerful and visual way. The Suffex Safer Roads Partnership achieved this with an ad that went viral online within weeks.

Picture this: In slow motion, a father sits in his living room, pretending to drive a car. Suddenly, his expression changes to one of pure horror. His wife and daughter run to him and wrap their arms like seat belts around his body. Seconds later, the whole living room explodes. The voice-over brings it home with the line: "Embrace life. Always wear your seat belt."

This ad is powerful because it is structured effectively. The first two seconds and last five seconds of an ad are crucial, and require advertisers to make the most of both in different ways.

In the first two seconds, you've got to show something unusual. This creates a "curiosity gap" and keeps us watching to find out what happens. In the Suffex spot, this was the somewhat bizarre image of a grown man driving an imaginary car.

In the last five seconds, you have to resolve the story and make a lasting impression — a fairly vital part of an ad! Again, the Suffex spot exemplified this by tying the initial unusual image into the overall message in a thoroughly thought-provoking way.

You need both the unusual _and_ the thought-provoking to make a spot worth watching. If you only have an unexpected image, then the spot won't be memorable. On the other hand, if you only have a great surprise ending, the "wow factor" will be gone after the first viewing. But if you have both, you can be sure people will watch your spot again and again.

> _"Try to write the last five seconds first. If you can't, you don't need to write a spot, you need to develop the premise for a spot." — David Fowler, in Creative Companion_

### 8. For radio commercials, keep it natural. 

With only audio at your disposal and just enough time for a few sentences, radio ads require a yet another kind of creative thinking. The challenge to grab people's attention is even tougher — but still achievable!

Radio ads can still create images in peoples' heads by setting the scene with background sound. For example, if the ad takes place in a restaurant, the buzz of people chatting, the clatter of dishes stacking and the clinking of cutlery will be enough to set a vivid scene.

Now that you've grabbed your audience's attention, it's vital that you use their time well. Despite popular opinion, a successful radio ad doesn't have to be funny. Instead, it's often the natural and sincere ads that truly work.

For example, a First Tennessee Bank ad featured an older man sharing his delight about buying a house as a young man. Because his voice was sincere, natural and honest, customers trusted the ad — and the bank.

Conversations between two voices can also create a more dynamic and lively ad. But again, dialogues must sound natural. For example, "Mmm, this drink, now with one-third fewer calories, sure tastes great, Dad!" doesn't sound at all natural. Product features are best described in a voice-over, while you should put your best efforts into creating an authentic conversation that captures a feeling.

How? In real conversations, people speak in fragments, complete each others' sentences, or move from one phrase to another without finishing them, so you should encourage narrators to ad lib sometimes. The impact of a voice can also be strengthened by writing a text with fewer words and more room for intonational nuances, pauses or even a powerful silence.

Silence, naturalness and succinctness — these are all manifestations of the _simplicity_ that makes for good advertising. Though creating ads can seem like a pretty complicated ride, the insights we've learned from these blinks will help us to take a step back, relax, and take our creativity to the next level.

### 9. Final summary 

The key message in this book:

**Advertising can be one crazy ride, demanding persistence and creativity. But by working hard and keeping it simple, you too can produce ideas that shine. This way, you'll be able to produce ads that suit your client and their medium, as well as surprising your customers in ways that they'll surely remember.**

Actionable advice:

**Give your ideas the presentation they deserve!**

You may have worked long and hard on your ad — why waste the effort by presenting it carelessly? Being a confident and convincing presenter is a skill that will benefit you throughout your whole career. It also helps to know your audience, so get familiar with the company culture by spending time with your clients. This way you can get a grip on their sense of humor, find out what they expect from ads, and also build a relationship of trust for possible future business.

**Suggested further reading:** ** _Confessions of an Advertising Man_** **by David Ogilvy**

_Confessions of an Advertising Man_ is a collection of advice and techniques for building successful advertising campaigns and agencies. Written in the era of _Mad Men_, the book is still considered essential reading in the advertising industry, but also provides advice for aspiring managers in any business.
---

### Luke Sullivan with Sam Bennett

Luke Sullivan is renowned for his 32-year career in advertising under eminent agencies such Fallon and GSD&M. He is currently chairman of the advertising department at the Savannah College of Art and Design.

Sam Bennett was a contributing author to the updated edition of this book. She is currently the director of brand and digital strategy at Martin Williams.

© Luke Sullivan: Hey Whipple, Squeeze This copyright 2012, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

