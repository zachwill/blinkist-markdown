---
id: 593e4dbeb238e100051fdd93
slug: hidden-figures-en
published_date: 2017-06-12T00:00:00.000+00:00
author: Margot Lee Shetterly
title: Hidden Figures
subtitle: The American Dream and the Untold Story of the Black Women Mathematicians who Helped Win the Space Race
main_color: 73B1E5
text_color: 406380
---

# Hidden Figures

_The American Dream and the Untold Story of the Black Women Mathematicians who Helped Win the Space Race_

**Margot Lee Shetterly**

_Hidden Figures_ (2016) reveals the untold story of the black female mathematicians who helped send John Glenn on his first orbit around the Earth and Neil Armstrong to the moon. These courageous, trailblazing women answered the call of duty by leaving their teaching jobs in segregated Southern schools behind and helping to shape the modern space program.

---
### 1. What’s in it for me? Learn the secret history of NASA and its unknown staff. 

Even today, the upper echelons of science and math are largely dominated by white males. In 1970, only 1 percent of American engineers were black. And, by 1984, that number had inched all the way up to 2 percent.

So it probably seems unlikely that, during World War II, a group of black women were at the forefront of wartime American science and technology.

Well, it didn't seem unlikely to the author. She grew up in Hampton, Virginia, and had always been surrounded by black adults in the community who had worked their entire careers in science, math and engineering.

This is the story of the black women who played an integral part in the development of World War II machines and the space race.

In these blinks, you'll find out

  * how workplaces were segregated;

  * what opportunities the black women at NACA and NASA had; and

  * how John Glenn put special trust in one of these black women.

### 2. The story of black women mathematicians at Langley began in the 1940s. 

Neil Armstrong didn't fly to the moon and back on his own. There were many people hard at work behind the scenes, most of whom haven't had their stories told until now.

Before it became the _National Aeronautics and Space Agency_ in 1958, NASA was known as NACA, or the _National Advisory Committee for Aeronautics_, and its research headquarters were located at the Langley Memorial Aeronautical Laboratory, in Hampton, Virginia.

NACA, founded in 1917, was originally a place where warplanes and other kinds of machines for flight were developed. This all changed during the Cold War with the Soviet Union when NACA turned into NASA and devoted itself to winning the space race.

In the 1940s, Langley hired its first black employees as "computers," since they would be performing mathematical computations. Prior to the forties, racial discrimination prevented these jobs from being accessible to black people.

But this changed thanks in part to pioneering civil-rights activists like A. Philip Randolph, who threatened to send 100,000 protesters to march on the capitol to bring discrimination to national attention.

So, in 1941, President Franklin D. Roosevelt issued Executive Order 8802, which desegregated the defence industry, and Executive Order 9346, which created the Fair Employment Practices Committee.

While these orders allowed black women to work at Langley, it was still a segregated workplace. In fact, the first group of black women there were known as the "West Computers," since they all worked on the west side of Langley's campus, separate from the white employees.

Nonetheless, they were still at the heart of Langley's operations, and their story is a vital part in key twentieth-century developments like World War II, the Cold War and the space race, as well as the civil rights movement and the transition to electronic computing.

Some of these women have been duly recognized, such as Katherine Johnson, who was given the highest civilian honor, the Presidential Medal of Freedom, in 2015. Yet the majority of the world is still unaware that there was a team of all-black, all-female math whizzes helping the United States explore space.

### 3. World War II helped open the door for the trailblazing West Computers. 

In the 1940s, there were specific jobs that had been designated for black people.

There was your average black job, such as being a servant or working on a farm. Then there was a "good" black job, like owning a barbershop or working at the post office. And then there was a "great" black job, such as being a teacher, doctor, lawyer or preacher.

But working in an aeronautics lab such as Langley didn't fall into any of these categories. The chances of landing such a job were considered absurdly slim, especially within the black community.

However, the ladies of the West Computer group had an unexpected advantage. As the United States entered World War II, demand for airplanes skyrocketed, as did the need for mathematicians to help design them.

By 1943, Langley was testing a variety of new airplane designs and found itself in desperate need of more mathematicians to make calculations and help maximize power, safety and efficiency.

This marked the birth of the modern US aircraft industry. Between 1938 and 1943, it would jump from being the nation's forty-third largest industry to the largest industry in the world.

So this demand, along with Roosevelt's call for racial equality in federal jobs, opened the door for black female mathematicians.

The precise number of female computers who worked at NACA is still unknown. One study from 1992 estimated the amount to be in the hundreds, but it could also easily have been in the thousands. And as for black women, the author suggests there were around seventy between 1943 and 1980.

But it wasn't easy for these women. In 1940, only 2 percent of black women had a university degree, and, of those who did, a majority wound up teaching in their hometown. But to work at NACA, they had to move to a new city, give up those teaching jobs and leave their families behind.

On top of that, they had to endure six-day workweeks and commute on overcrowded, segregated buses.

And these were but the first of many difficulties they would have to overcome.

### 4. In the face of discrimination, the West Computers displayed bravery and resilience. 

It's hard to imagine what a black woman went through in order to work at a prestigious, yet predominantly white, research institution over 50 years ago.

But you can be sure they had to endure racism, segregation and humiliation — constant reminders of their second-class status.

Even finding housing in Hampton, Virginia, was a struggle.

The white computers didn't need to worry about the area's housing shortage since they could live in the Anne Wythe Hall dormitory and use a special bus service to get to Langley.

The black computers, on the other hand, were forced to fend for themselves and secure private housing in a neighborhood much farther away from work.

And once they got to work, there were still separate bathrooms, separate water fountains and separate offices to put up with.

Even in the dining hall, a sign reading "Colored Computers" indicated where they were allowed to eat.

Despite these degrading circumstances, the West Computers were determined to set a positive example with grace and strength.

One day, Miriam Mann had enough of the dining room sign, so she quietly scooped it up and tucked it away in her purse. However, another sign replaced it, and after Miriam removed that one, yet another appeared, and it went on and on until, finally, whoever was making the signs gave up.

Katherine Johnson was another brave pioneer at Langley who'd given up her teaching job to work as a computer. She refused to walk to the opposite end of the campus in order to use the black bathroom there and used the closer white bathroom instead.

She also made other breakthroughs, like becoming a member of the flight-research division. Initially barred from the editorial meetings there, she was later able to join them and went on to become the first woman in the flight-research division to author her own report, an innovative paper on orbital flight.

### 5. The West Computers helped pave the way for both racial and gender equality at Langley. 

It's important to remember that the women of Langley's West Computer division did not only face racial discrimination; they also had to deal with the frustrations of gender discrimination.

As a result, it was almost impossible for the West Computers to get promoted, no matter how deserving they were.

Black women had simply never been promoted within an engineering team.

Meanwhile, they had to look on as white mathematicians, especially white male mathematicians, enjoyed special privileges, such as being mentored by senior engineers.

It wasn't unusual for promising male workers, with the same job description as the West Computers, to get taken under someone's wing and be treated to lunchtime or after-hours sessions of conversations and shared drinks. Inevitably, these white men were the ones who were chosen for important apprenticeships and, eventually, were promoted as heads of a section, branch or division.

For a black woman, the highest managerial position to aim for was the supervisor of the West Computers.

This is the position that Dorothy Vaughan reached in 1951, becoming Langley's first black manager. In this role, Dorothy was integral to helping other women, both black and white, secure promotions. All the while, Vaughan, a married woman with four children, was also juggling childcare arrangements and lifting her family into the ranks of the middle class.

Christine Darden was another West Computer who watched less educated men get promoted ahead of her. When she asked her division chief why this happened, he was surprised; he'd never heard a woman voice such a complaint.

Indeed, women were generally expected to give up their careers once they had kids. But expectations weren't always met — they _couldn't_ be met by black women who had to continue supporting their families.

Christine made her case and enlightened her division chief. For her efforts, she was finally promoted to an engineering team. This launched a forty-year career at NASA, and she went on to become one of the world's leading experts on sonic-boom research.

### 6. Langley wasn’t just a science and engineering lab; it was also a laboratory for race and gender relations. 

By the 1970s, technological advances had made the role of human computers obsolete. But by that time, the West Computers had lived through some of the most important civil-rights battles in the country.

Unsurprisingly, Roosevelt's executive orders of 1941 didn't result in immediate nationwide workplace integration and racial harmony.

Even after the landmark court decision of Brown v. Board of Education in 1954, which made further steps toward ending segregation by declaring that "separate but equal" schools were not, in fact, equal, there was still an uphill battle to fight.

And Langley's home state of Virginia was an arena in which many of these fights were fought. Virginia Senator Harry Byrd was at the head of a counter movement that sought to preserve segregation. One of his foot soldiers was the politician J. Lindsay Almond, who became Virginia's governor in 1958, and declared that "integration anywhere means destruction everywhere."

Together, they threatened to defund Virginia schools if integration persisted, which resulted in the entire school system of Prince Edward County shutting down from 1959 to 1964.

This was the political climate the West Computers found themselves in.

But despite these unfavorable circumstances, the women provided vital contributions that made airplanes faster, safer and more aerodynamic. Through complex equations and computations, they could describe every function of the planes.

And they often had to run these numbers while trying not to think about the repercussions of their work. A bad calculation could result in casualties — and, in the case of US military bombers, even correct calculations cost lives.

Their important work culminated in the space race, where Katherine Johnson was heavily involved in calculating the trajectories of the NASA flight that sent the first US astronaut into space in 1962. Later, she also provided crucial work for the Apollo moon landing.

In 1962, before orbiting the earth, John Glenn asked Katherine to personally do the math so that he would feel certain about his safety. And seven years later, Glenn would once again ask her to run the numbers for the Apollo 11 moon mission.

Both times, Katherine Johnson's numbers were solid.

### 7. Final summary 

The key message in this book:

**Women are barely represented in most of our popular depictions of historical events, especially those involving science, engineering and math. And the achievements of black women are even less recognized. But the truth of the matter is that black women were integral to many landmark achievements in the twentieth century, including the Apollo mission that sent Neil Armstrong to the moon.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Immortal Life Of Henrietta Lacks_** **by Rebecca Skloot**

_The Immortal Life of Henrietta Lacks_ tells the story of a poor tobacco farmer who died from cervical cancer, and her cell strand, HeLa, which scientists used to develop a cure for polio and other diseases. In a fascinating and revealing investigation, author Rebecca Skloot uncovers the history of Henrietta and her family, of the exploitation of black Americans by the medical industry, and of Henrietta's immortal cells.
---

### Margot Lee Shetterly

Margot Lee Shetterly grew up in Hampton, Virginia, where she came to know many of the women she writes about in _Hidden Figures_. She is a recipient of the Virginia Foundation for the Humanities research grant and a fellowship member of the Alfred P. Sloan Foundation.

