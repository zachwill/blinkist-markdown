---
id: 5a2dcbc7b238e10006427a15
slug: high-performance-habits-en
published_date: 2017-12-11T00:00:00.000+00:00
author: Brendon Burchard
title: High Performance Habits
subtitle: How Extraordinary People Become That Way
main_color: FEFF36
text_color: 666615
---

# High Performance Habits

_How Extraordinary People Become That Way_

**Brendon Burchard**

_High Performance Habits_ (2017) explores the six habits that can turn an ordinary person into an extraordinarily productive one. Performance coach Brendon Burchard draws on the data and statistics from one of the largest studies of the world's most productive people ever conducted to explore their habits and find out what makes them tick.

_"Burchard's research into the habits of high performers across the globe unearths some fascinating and practical insights. For example, did you know that you'll be more motivated to do something if you think you're doing it for someone else's sake?"_ — Ben H, Head of Content at Blinkist

---
### 1. What’s in it for me? Perform your way to the top. 

Are you a hard worker whose efforts still don't set you above the crowd? And, despite aiming for the stars, do you still tend to get caught up in answering emails and checking off tedious to-dos? Then you're probably like most people: an average performer, stuck in the hamster wheel of life.

Luckily, these blinks demonstrate why this needn't be the case forever. In fact, a new body of research shows that the highest performing people in this world aren't born with an extraordinary ability to achieve, and neither does their success stem from some specific kind of personality.

Instead, the key to long-term success lies in certain habits — and with the help of just six of them, identified through research on high performance, you'll be well on your way to extraordinary achievements.

You'll see why seeking clarity, generating energy, raising necessity, increasing productivity, developing influence, and demonstrating courage will help you perform your way to the stars.

You'll also learn

  * what CEOs and professional athletes have in common;

  * why you'd benefit from having your mother depend on your success; and

  * how to understand and tame the seductions of email.

### 2. The key to high performance isn’t personality or inherent gifts; it’s good habits and high confidence. 

Have you ever met someone who makes it all look so easy? Maybe she's earned multiple degrees while working two jobs, all without breaking a sweat. Or maybe he's the kind of guy with a Midas touch, and every project he tackles turns into a smashing success. Chances are they're what's known as a _high performer_.

The author, Brendon Burchard, has conducted one of the biggest studies on high performance in history, examining people from over 190 countries to understand exactly how they achieve their long-term success.

He concluded that gender, race, age and personality traits have very little to do with high performance. What really matters are certain key _habits_, like keeping yourself physically fit.

In other words, it's not who you _are_, but rather what you _do_ that's important. The author also discovered that these habits didn't form by accident. High performers took them on deliberately.

Now, don't confuse these habits with "life hacks" or some simple, magical changes that take zero effort to implement. High performers outperform their peers because they _consciously_ and _consistently_ practice these habits.

Another common trait is their confidence in being able to master even difficult tasks, like big new projects at work or learning new languages. Again, this isn't an inherent trait; it's an earned confidence achieved through diligent practice.

This is good news for you, as it means you too can gain this confidence through practice. With continued practice, you'll gain more knowledge, master more skills, and your confidence will grow, making it easier to keep learning and growing. This loop of continual growth and self-improvement is the hallmark of high performance.

So far, so good, right? Now, without further ado, let's look at those habits.

> _"Motivation is what gets you started. Habit is what keeps you going."_

### 3. High performers have self-awareness and a clear purpose in life. 

When was the last time you asked yourself the big questions, such as, how do I want to be remembered? Or, what do I want to do with my life?

Many will only think about these questions on their birthday or New Year's Eve. But high performers ask these questions all the time; it's a habit the author calls _seeking clarity_, and it keeps them goal oriented.

It also gives them a strong purpose, direction and focus in everything they do, since they know precisely how their actions are helping them reach their goals. Conversely, when you lack clarity, you can end up sulking, mired in negative emotions.

Clarity comprises four fields that you need to work on to improve:

First is _the self_ and knowing exactly what kind of person you want to become.

High performers are focused on becoming the best version of themselves and being remembered this way. This could mean, for instance, being kind, attentive and humble. Once you determine the self you want to be, the question becomes: Have you behaved this way so far and if not, what do you need to change?

Second is the _social sphere_ and being aware and intentional about your interactions with others.

High performers don't have an autopilot for socializing. If there's a lunch date, meeting or party coming up, the question becomes: How can I shape this meeting in a positive way?

The third is the _field of skills_ and knowing precisely which talents need to be developed.

High performers will focus on a primary profession or field of interest and work on giving themselves time to practice while avoiding all distractions. If you want to be a great writer, this would mean setting aside time to write, not just learn about writing, and then getting feedback to learn what needs improving.

The fourth field is _service_ and finding a way to give back to others. High performers excel at working on behalf of others and not just themselves. Doing so helps motivate them while also endowing them with a passionate drive and granting meaning to their work. The question to ask here would be: Who needs me?

### 4. High performers have a positive outlook on life and are physically and mentally fit. 

If you were to list the habits you imagine a successful CEO might have, you might think of efficient scheduling and the ability to keep distractions to a minimum. But you might not think of exercise.

People often associate CEOs with mental fitness, but research shows that they're also physically fit, and their energy levels tend to be similar to those of professional athletes. The author calls the second habit _generating energy_, and it's key to maintaining a high performance level.

Neuroscientists have found that regular exercise increases the production of new neurons in the areas of your brain that are related to learning and memory. Exercise also improves mood and reduces stress, all of which adds up to greatly enhanced leadership performance.

But everyone knows that exercise is good for you, right? High performers stand out because they make routine exercise a habit and stick to it, while underperformers are great at coming up with excuses to avoid working out.

As for generating mental energy, this is achieved by having a positive outlook on life.

Data shows high performers are more cheerful and positive than their peers, even though their personal and professional lives are no less difficult and troubled. They continually and intentionally focus on the good while avoiding getting mired in negative thinking. And the research suggests that this positive thinking directly relates to high performers leading happier emotional lives and having more mental energy.

To get yourself into the habit of positive thinking, take a moment each morning to ask yourself what you have to look forward to in the day ahead. Maybe you're meeting an old friend for lunch or celebrating a coworker's birthday.

Keep in mind that this isn't just about staying in a good mood: Neuroscientists believe that anticipating positive events releases as much dopamine — the hormone associated with happiness — as experiencing the event itself.

In this way, having a positive outlook is a very powerful tool since you get twice the joy — both in anticipation of the event and again when it actually happens!

### 5. High performers use inner and outer expectations to stay motivated. 

Let's imagine two runners about to start a race, waiting for the starting pistol to go off. Both have a similar track record, and each has put in the same amount of training. But there's one difference: one racer is thinking about the personal glory of winning, while the other is thinking, "I have to win this for my mother."

Who's going to win?

Probably, the latter.

That's because raising the stakes improves performance. The author calls this third habit _raising necessity_.

High performers will bring an extra urgency to the work they're doing, usually in the form of an outside obligation that is added to their own internal desires. This provides them with more motivation and increases their likelihood of success. Underperformers, on the other hand, will rely solely on their own desire to succeed. This makes their success a preferable outcome, but not a necessary one.

So, to put this into practice, you first need to set a high standard for yourself. Remember, you want to master your craft, so don't settle for simple and easily achievable goals.

Now, you'll also want to attach your personal goal to an external obligation so that a positive outcome will also benefit someone else in your life. If your local charity will gain exposure and a massive windfall of donations as a result of your project's success, you'll likely work twice as hard to ensure it comes together.

Another high-performer habit for raising the stakes is simply to share your goal with a lot of people.

Outside expectations are created, and the act of voicing your goal out loud can increase its importance. No one wants to fail publicly, so the more people you tell, the more committed you'll feel.

When the author set a goal to create an online video course on personal development, he not only told his friends and family about it, he also invited them to be the first to try it out and offer feedback. So his closest social circle was both aware and looking forward to him completing this project, and these expectations all-but guaranteed that Burchard would meet his deadline.

### 6. High performers avoid distractions and are smart with their deadlines. 

It's terrible to always feel busy and run down, yet still not getting enough done. This is a clear sign that there's an imbalance between the energy you're spending and the results you're seeing.

This brings us to the fourth habit of high performance: _increasing productivity_, which you can do by learning how to separate the important work from the unimportant work. This way, you only spend energy on the tasks that really matter.

It's common for underperformers to pay a lot of attention to small tasks that make them feel productive in the short run but add up to very little in the long run. One of the main offenders is email, on which people spend an average of 28 percent of their working week. This is because the simple act of answering an email can give you a reassuring feeling of accomplishment, even though it's likely distracting you from more important work.

Another important aspect of productivity is timing. Data shows that underperformers are over three times more likely to fall into a _false deadline trap_, which means that they set a deadline that's not strictly enforced. Knowing that the due date is preferable, not a must, you won't feel any motivation to meet it. In short, you'll be unproductive.

But high performers are great at _planning_, which means creating clear and challenging deadlines and goals. Having a visible finish line ahead of you is a great way to maintain focus, fight distraction and keep your energy levels up, and data shows that a person with a clear and challenging goal will always outperform someone with no strict deadline.

Big projects that are carried out over long periods of time can be especially challenging, especially when it comes to staying focused and keeping the momentum going. In these cases, you'll find it helpful to break the long-term goals down into subgoals of four or five small steps that will get you to that finish line. This way you can stay focused and move forward, even when there are months or years still to go.

### 7. High performers are appreciative, giving and aware of what others need to succeed. 

There's a popular notion that it's "lonely at the top," meaning that once you reach the highest rung of the professional ladder, there will be no one around with whom to share your achievement.

But this isn't the case for high performers, as the research shows them to be quite capable of establishing meaningful and lasting connections with their peers.

High performers are appreciative and giving people who aren't afraid to also challenge those they work with and expect the same from others.

In 2016, the American Psychological Association's Work and Well-Being Survey revealed that only half of the workers in the United States feel valued and recognized by their superiors. High-performing managers wouldn't stand for this. In his study, Burchard found high performers routinely praising and cheering on their staff. They're also more likely to notice and appreciate good work.

High performers were found to have a _giving mind-set_ — the fifth habit. They're well aware of the struggles and desires of others and use that knowledge to provide people with what they need.

They also know that their staff need the trust and freedom to make their own decisions. Having this power is a great motivator, and high performers are both aware and respectful of this.

Being perceptive and aware of what people need to grow allows high performers to assign the right tasks to the right people so they can reach the next level.

And when a high performer needs something themselves, they don't hesitate to ask for a favor. This is something underperformers often resist for fear of being judged or rejected, even though statistics show that the average person will get a positive response three times more often than they expect. In fact, fears of being harshly judged by our peers are generally overestimated. The truth is, most people are too busy to spend time thinking about you, so relax!

### 8. High performers aren’t afraid to take risks and are open about their ambitions. 

Are you the kind of person who prefers to be left alone in their comfort zone? If so, how much pressure does it take for you to break out?

For high performers, risk-taking isn't unusual at all. In fact, _demonstrating courage_ is the sixth and final habit of high performers, and it goes hand-in-hand with gaining a positive perspective on challenging situations.

Again, attitudes like these aren't inherent personality traits; they're characteristics that high-performing individuals have practiced and worked at over time. They recognize that taking bold action involves a higher risk of failing — that's why these moves are considered bold! But high performers have learned how to overcome their fear and take action.

With practice, you too can make bold decisions. And just like high performers, you can even come to enjoy the thrill of taking risks.

Making a risky move is like any other learned skill — it gets easier the more you do it, so all you need to do is start taking those leaps. Like parachuting, the first time is always filled with panic and dread, but after each jump, it gradually becomes a little easier and less stressful.

Most people will avoid any kind of struggle, but being extraordinary and reaching your goals means learning how to greet challenges with a smile. It's all about having the right perspective, and rather than complaining about life being difficult, higher performers will see each new challenge as an opportunity to grow.

One of the commonest bold acts is being open and honest about one's true ambitions. This is another behavior that many will avoid due to unwanted judgment or ridicule.

While the average person prefers to work toward their dreams in silence, high performers don't hold back despite being fully aware that someone might call them "delusional," "unrealistic" or even "crazy." For them, it's all part of life's precious struggle.

So don't waste another day. Now's the time to open yourself up to the world and all the good and bad it has to offer. By being open, you'll find that there are a lot of people out there just waiting to help turn your dreams into reality.

> "_Courage is not fearlessness, it is taking action and persisting despite that fear."_

### 9. Final summary 

The key message in this book:

**High performers are not born extraordinary; they grow through steady and persistent practice that involves a conscious attempt to master certain habits. They are highly aware of the purpose of their work, stay energetic, use external motivators, know how to increase productivity through careful planning, regularly connect and give to those around and take bold risks.**

Actionable advice:

**Seek clarity in your social interactions.**

Don't just enter a social interaction on autopilot. Ask yourself beforehand: How can I be a good person in the upcoming situation? What will the other person expect? What kind of mood and energy do I want to create, and how can I act to achieve that outcome?

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Code of the Extraordinary Mind_** **by Vishen Lakhiani**

_The Code of the Extraordinary Mind_ (2016) unveils a method for overcoming the madness of everyday life, one that enables anyone to stand out from the pack and become an extraordinary individual. The author lays down ten laws that anyone can easily follow to experience a radical transformation and find meaning and happiness in each day.
---

### Brendon Burchard

Brendon Burchard is among the world's most successful high-performance coaches and the founder of the Experts Academy and High Performance Academy. He is also the best-selling author of books such as _The Motivation Manifesto, The Millionaire Messenger_ and _The Charge_.

