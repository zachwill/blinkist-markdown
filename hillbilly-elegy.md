---
id: 59779529b238e100057033a6
slug: hillbilly-elegy-en
published_date: 2017-07-28T00:00:00.000+00:00
author: J.D. Vance
title: Hillbilly Elegy
subtitle: A Memoir of a Family and Culture in Crisis
main_color: 60A2BF
text_color: 46778C
---

# Hillbilly Elegy

_A Memoir of a Family and Culture in Crisis_

**J.D. Vance**

_Hillbilly Elegy_ (2016) is an autobiographical walk through the life of a man who grew up in an impoverished neighborhood of Middletown, Ohio. These blinks tell the story of a boy who, despite a turbulent childhood, beat the odds and pulled himself out of poverty.

---
### 1. What’s in it for me? Take a hillbilly’s journey to success. 

The American Dream is usually a tale of climbing out of poverty, breaking away from a disadvantaged upbringing, and aiming for the stars. If you achieve the American dream, you show that anything is possible — even when the odds are against you.

In these blinks, a classic American dream story is told from the perspective of a former hillbilly turned successful entrepreneur. J.D. Vance grew up against a family backdrop of frequent fights, money problems, and a revolving door of father figures, but still managed to make it, first in the Marine Corps and later in a leading Silicon Valley investment firm.

In these blinks, you'll learn

  * how hillbillies got their name;

  * what led to a city's demise; and

  * how three rules and a loving grandmother helped a troubled boy out of his misery.

### 2. J.D.’s grandparents grew up as middle-class “hillbillies” – an option that no longer exists today. 

J.D. Vance was born a _hillbilly_. He grew up poor in an Ohio steel town that had been bled dry of both its jobs and its hope. That's why he identifies with millions of working-class white Americans of Scottish and Irish descent — people who seldom have college degrees and who experience poverty as a family tradition.

But to truly understand this lineage, we need to go back to the lives of his grandparents. They were hillbillies too, and their experience was typical of this group.

Known endearingly as Mamaw and Papaw, they were born around 1930 in Jackson, Kentucky. They were _hill people_ or hillbillies, as residents of the Appalachian Mountains are called — sometimes derogatorily.

In search of work, they left Kentucky for Middletown, Ohio where Papaw landed a job at Armco, a major steel company. And he wasn't the only one. During the 1950s, Armco aggressively recruited Kentuckians, flooding Ohio's towns and cities with people like J.D.'s grandparents.

This factory job allowed the couple to retire comfortably middle class. But today, the situation for hillbillies is much different. Towns like Jackson, Kentucky have been devastated by poverty.

As a result, in many instances, hill people are synonymous with "poor people." Practically a third of Jackson is impoverished, including half of its children. Although the public schools are in such disrepair that the state has seized control of them, local parents have no choice but to enroll their children in high schools that send barely any students to college.

The people of Jackson are also in generally poor physical health. A 2009 ABC news documentary on Appalachian America reported that young kids suffer painful dental problems, often caused by overconsumption of sugary soft drinks.

But while Jackson is a prime example of Appalachian poverty, this plight is common to cities and towns across the region, which have been drained by the outsourcing of jobs. In the blinks that follow, you'll learn how this industrial shift affected the personal lives of the hillbillies who call Appalachia home.

### 3. J.D.’s mother was born into a tumultuous home, and raised in poverty. 

J.D.'s uncle describes the childhood home he shared with Bev, J.D.'s mother, as a happy middle-class household. But to an outside observer, his rosy description seems wide of the mark. In fact, it was constantly on the verge of collapse. Here's the story:

Bev was born in 1961 and, by the mid-1960s, her father's drinking had gotten out of control. One Christmas Eve, Papaw returned home drunk, demanding a freshly cooked dinner. When one failed to materialize, he hurled the family's Christmas tree out the back door.

On another one of Papaw's particularly violent nights of drinking, Mamaw told him she would kill him if he ever came home drunk again. And she wasn't kidding. A week later, when Papaw fell asleep drunk, she got a container of gasoline from the garage, doused her husband with it and lit him on fire. Incredibly, he managed to survive the incident with only mild burns, but the marriage was rocky, to say the least, and J.D.'s mom had a hard time.

Vance himself was born in 1984 and raised in a poor neighborhood of Middletown, Ohio. When two bikes were stolen from his community in the same week, it was clear to him that the area had gone downhill since his mother's childhood.

Meanwhile, the steel company, Armco, which had once been an economic lifeline for his grandparents, was rapidly winding down. American manufacturing jobs were being exported to Asia, and Armco was following suit, allowing the city to slip into poverty.

The working class white people who called the city home had nowhere else to go, and the declining value of their homes trapped them in increasingly poor neighborhoods. To this day, Middletown is engaged in an economic struggle. Few local businesses are doing well, and many have closed their doors altogether. A street that once served as the city's source of pride is now primarily a rendezvous for drug addicts.

> _"My home is a hub of misery."_

### 4. A rough upbringing deeply impacted J.D.’s early years. 

Around the same time J.D. was taking his first steps, his parents were filing for divorce. For several years after they split up, his father was increasingly estranged and, at the age of six, the young boy was adopted by Bob, his mother's new husband.

During the same period, his mother got her nursing license. While she had never gone to college, she had a deep commitment to education. She overloaded him with books, and his family was generally a happy one.

Unfortunately, it wouldn't last; when he was nine years old, fights and violence became commonplace in his home too.

During that year, his mother and Bob decided to move to Preble County, a few miles northwest of Middletown. After the move, their fighting escalated, often keeping him awake.

Simultaneously, J.D. started to do worse in school. The trauma he was experiencing at home was affecting his grades and taking a toll on his health.

But despite all these issues, more trouble was around the corner. His mother had been in a years-long affair with a local firefighter, and when Bob finally confronted her about it, she intentionally crashed her brand-new minivan in her first failed suicide attempt.

After this frightening event, J.D. and his mother moved back to Middletown, which was now overridden with drugs and alcohol. His mom suffered from alcoholism herself, and one day, to apologize for her drinking, she took him to the mall to buy him some football cards. On the way there, in response to some small criticism, she stepped on the gas, threatening to kill them both.

She eventually pulled over without crashing the car and instead of a fatal crash, gave him a serious beating. She was so out of control that the police had to take her away in handcuffs.

Luckily, his biological father would soon come back into his life with a new vision. He had become a devout Christian, and it wouldn't be long before J.D. came to love his father and the church.

### 5. As a teenager, J.D. relocated nearly constantly as his mother was institutionalized. 

J.D. grew up with a constantly changing cast of father figures as his mom swapped boyfriends frequently. But there was always one stable male force in his life: his grandfather, Papaw.

Papaw would practice math with him and although he had failed to do so himself, taught him to respect women.

As a result, Papaw's death when J.D. was thirteen, hit the boy hard, and his mother even harder. It was then, while struggling with her father's passing that his mother was finally admitted to a psychiatric clinic.

At this point, she was dating Matt, a firefighter. She was having trouble processing her father's death and began screaming at the people who loved her — Matt, but also J.D.'s older sister, Lindsay. She started taking prescription narcotics and soon lost her job at the hospital after skating through the emergency room on rollerblades.

Not long after that, she attempted suicide again by cutting her wrists. After this second failed attempt, she was admitted to psychiatric care and enrolled in drug rehabilitation.

As for J.D., he moved in with his grandmother, Mamaw, and would continue to move around repeatedly during his teenage years. When his mother got out of the psychiatric hospital, he moved back in with his biological father. While his mother insisted that he move into Matt's home in Dayton, Ohio with her, he vehemently refused; he had friends in school that he didn't want to lose.

Soon after this, he moved back to Mamaw's house. Life with his dad had been calm and peaceful, but Mamaw wanted him back, and he wanted to be with her.

While he found tranquility in Mamaw's home, this peace wouldn't last. His mom remarried again, this time to a man named Ken, and moved J.D. with her into his house. It was the teen's fourth home in just two years.

He was understandably worn out. This perpetually rotating carousel of people and places had taken its toll.

### 6. Living with his grandmother turned J.D.’s life around before he found stability in the military. 

Now in Miamisburg, at the home of his mother's new husband Ken, J.D. felt trapped in a stranger's house. He had never felt so lonely in his life.

Naturally, his grades reflected this feeling, and he nearly dropped out of high school. But luckily for him, his mom's relationship with Ken wouldn't last. When they broke up, he moved back in with Mamaw. Living with her for the next three years would save his life.

Mamaw had three rules: get good grades, get a job, and help her out. She strictly enforced them, and J.D. happily complied.

During this period, he learned a lot thanks to the support of his grandmother. For example, when he began taking advanced math classes, his grandmother spent $180 to buy him a graphing calculator, teaching him about her values and compelling him to commit to his schoolwork.

Despite her strict rules, he was finally living a harmonious life and having a lot of fun. Things were getting back on track, and his grades started to creep up.

But Mamaw's house didn't just give him a temporary haven from the world; it also gave him hope for a better life down the line. As a result, he aced the SAT, but more importantly, he was happy.

Like many young people, however, he didn't know what to do with his life. His choices were basically college or the Marines and, scared of the unstructured college experience, he opted for the latter.

As a result, he spent his first four years of adulthood in the military, learning to live like a grown up; he learned everything from the importance of physical fitness to personal hygiene and even how to manage his finances, taking classes on how to balance a checkbook, save for the future and begin investing.

But most importantly, he learned that leadership isn't about bossing people around; it's about earning respect and listening to others.

### 7. The military helped J.D. excel in life, but with success came an awareness of being different. 

While he was in the Marines, J.D.'s grandmother passed away. She was a longtime smoker and, at 71, her lungs simply gave out. It's probably already clear that Mamaw was the best thing that ever happened to him, and the effort she'd put in paid off.

In large part thanks to her support, he started college at Ohio State University in 2007. He was thrilled to begin this new chapter and to make a home for himself in Columbus.

The Marines had given him a sense of invincibility; he was earning A's and had no trouble taking care of himself. This deeply felt optimism contrasted powerfully with the pessimism he felt when thinking about his neighbors back in Middletown.

He finally believed that, if he worked hard enough, he could land himself a well-paying job. So, in 2009, after just two years, he graduated from Ohio State with a double major, summa cum laude, and another dream: law school.

He remained committed and in 2010 got a place at Yale to study law. This new chapter was overwhelming for him but for good reasons.

Yale challenged his expectations of himself. After all, he didn't know a single Ivy League graduate growing up. He was the only person in his immediate family to attend college and the only person in his extended family to go to professional school.

So, while he excelled, earning good grades and landing a job working for the chief counsel of a US senator, he was always an anomaly at Yale. He was one of the poorest kids there, and it was clear to him how different he was from his classmates.

### 8. In escaping poverty, Vance teaches us all a timeless lesson. 

During his time at Yale, J.D. fell for a classmate. Her name was Usha, and they would soon start dating, helping him to feel at home in his new Ivy League environment. The significance of this transformation really can't be overstated since Yale is all about who you know.

His experiences in this environment taught him about what economists call _social capital_ — the networks of people and institutions that humans use to their economic benefit. After all, at Yale, networking is as natural as breathing. Students don't submit résumés upon graduating; they just know people. As a result, they can shoot a friend an email or have their uncle make a call, and count on a job or at least an interview.

J.D. knew that the only way he'd get an interview when he applied for a position with a powerful federal judge was if his professor pushed his application. It was with this new-found social capital that he finally made it. He beat the odds and would never be poor again.

This accomplishment is truly stunning since there are a number of studies showing that "adverse childhood experiences" like those suffered by J.D cause children to perform poorly in school, and later suffer from anxiety, depression, obesity and even heart disease. However, J.D. overcame his traumatic upbringing, graduated from Yale, found a good job and happily settled down with Usha.

What can we learn from his experience?

We can build policies that are based on a more fundamental understanding of the barriers blocking the success of people like J.D. Such an approach could recognize that the real problems these kids face occur at home. We could enact housing policy that prevents the segregation of the poor.

> _"I was upwardly mobile. I had made it. I had achieved the American Dream."_

### 9. Final summary 

The key message in this book:

**J.D. Vance was raised in a broken, rustbelt home in Middletown, Ohio. Growing up poor and in a rough neighborhood set him up for failure, but he overcame all the odds to become tremendously successful. His story shines a light for all the other hillbillies who hope that another world is possible.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _White Trash_** **by Nancy Isenberg**

_White Trash_ (2016) retells American history from the perspective of the poor whites who were by turns despised and admired by the upper classes. These blinks trace the biopolitical, cultural and social ideas that have shaped the lives of white trash Americans from early colonial days to the Civil War, through the Great Depression and up to the present day.
---

### J.D. Vance

J.D. Vance was raised in the American "rustbelt" in a city called Middletown, Ohio. He enlisted in the Marine Corps after high school and served in Iraq before graduating from Ohio State and Yale Law School. He now works at a leading Silicon Valley investment firm.

