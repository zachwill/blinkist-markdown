---
id: 5da9c86a6cee0700074e3b2e
slug: hinch-yourself-happy-en
published_date: 2019-10-23T00:00:00.000+00:00
author: Mrs. Hinch
title: Hinch Yourself Happy
subtitle: All the Best Cleaning Tips to Shine Your Sink and Soothe Your Soul
main_color: A7A3A4
text_color: 737071
---

# Hinch Yourself Happy

_All the Best Cleaning Tips to Shine Your Sink and Soothe Your Soul_

**Mrs. Hinch**

_Hinch Yourself Happy_ (2019) is all about celebrating the fun side of cleaning. While many of us may look upon housework as a tedious chore, Mrs. Hinch is here to share her passion for it and help people realize that it doesn't have to be avoided or delayed. So get ready to spring clean your attitude and transform your home into a spotless and calm oasis.

---
### 1. What’s in it for me? Turn dreaded household tasks into an enjoyable pastime. 

Do you let the dust bunnies pile up for weeks on end until you can't procrastinate any longer? Do you feel like cleaning up means you have to tackle your entire home at once?

Well, Mrs. Hinch is here to tell you there's a better way. With some simple tips, you'll find that cleaning doesn't have to be the bane of your existence. With a little help and change of perspective, you may even find a little joy in housework.

After all, who wouldn't like to come home to a place that is clean and pleasant smelling? With these tips, you'll be on your way to waking up or coming home to a serene and welcoming haven. And isn't that what your home should always be? 

In these blinks, you'll find out

  * why you should never underestimate the cleaning power of baking soda;

  * how to treat your drains before they start clogging; and

  * what to do next time you spill wine. ****

### 2. Avoid boredom and stress when cleaning and use to-do lists to stay motivated. 

Do you find cleaning an irksome or tedious chore that should be avoided whenever possible? If this is the case, it may be due to the way you approach the job at hand. If you treat cleaning as a mindless routine where you vacuum every Friday and clean the bathroom every Saturday, then it's bound to become a dull slog.

So why not make it more interesting by turning each housework session into a unique experience? 

The best way to approach cleaning is to start with a tour of your house or apartment to spot the areas that need the most attention. When you're cleaning on autopilot, certain areas can easily get neglected. But when you start with an attentive scan of your surroundings, you'll be cleaning the areas that really need it while avoiding the mind-numbing drudgery of a routine.

Doing regular inspections will also make you aware of which tasks need daily attention, such as mopping up around the kitchen sink and stove; which tasks can be taken care of on a weekly basis, such as deep cleaning the bathroom; and which tasks can be completed on a monthly basis, like wiping down your fridge.

While you're at it, don't bend over backward to make things look perfect. Cleaning can be a soothing and enjoyable activity so long as you take your time and relax.

What can really help is to have a focused to-do list. So when you're giving your surroundings a close inspection, simply jot down the areas that require attention in the form of a list that you can work through item by item.

There are several benefits to making a list, not least of which is that it can turn a seemingly overwhelming job into something manageable. Oftentimes, we put off cleaning because we don't know where to begin. With a list, you not only recognize where to start but also see that there really aren't that many tasks involved after all.

Plus, there's the priceless satisfaction of being able to cross an item off the list once you've accomplished it.

But perhaps most helpful of all is the flexibility that comes with a to-do list. Now, whenever you find yourself with a few minutes of downtime or a whole afternoon to spare, you can check the list and find a task that fits your schedule. This way, you can keep your cleaning momentum going and make sure nothing gets neglected.

### 3. When cleaning your bathroom, start with the bathtub and then move to the toilet. 

If you have trouble working up the motivation, just imagine how much nicer and calmer it will be to spend your free time in rooms that are clean and orderly. Of course, everyone prefers bathrooms that are shiny and pleasant smelling rather than grimy and stinky. So let's go ahead and start with some tips on how to get your bathroom spick-and-span.

The best way to tackle this room is to begin with the bathtub. So get things started by getting all the surfaces clear of soaps, soap dishes, shampoos, towels and any other products that might get in the way of a thorough cleaning. 

Next, grab your favorite cleaning product and give the tub and the walls around it a good spritzing. For shower tiles, the author recommends Astonish Mould. For the bathtub itself, Mrs. Hinch relies on Flash Bathroom; and for shiny faucets and showerheads, she uses Viakal. But don't let the Viakal linger on any surface for too long since it can corrode them.

Once the product has been applied, the author recommends using Minky pads, the antibacterial scrubbing pads that can make short work of whatever grime may have built up around your shower or bathtub. And don't forget to give those bathroom shelves a good once over as well.

With everything sufficiently scrubbed, all that's left is to give the surfaces a final rinse, then you can step back, let everything dry and enjoy the pleasant smells and gleaming shine of a well-cleaned tub.

Now, the time has come to tackle the dreaded toilet.

On a daily basis, it's good practice to give the toilet a wipe down with Cif Power wipes or whichever multi-purpose cleaning wipe you prefer. But that doesn't mean you should neglect to give your throne a deep-clean at least once a week. For this job, you should bring out the bleach.

Before dealing with the bowl, start with the lid and seat and then move downward to underneath the rim of the bowl and the rest of the inside. You may want to consider using an additional cleaner that has a nice smell to it, such as fragrant pine. Once the bleach has been applied, let it sit for a while to disinfect. Then give the toilet a flush, rinse the cleaning products off and let it air-dry.

### 4. Spruce up your kitchen by cleaning your fridge and creating an inviting space for your cleaning products. 

Let's continue our full house cleansing by moving on to the kitchen, a room with no shortage of surfaces and crevasses in need of de-griming. But for now, let's focus on one of those tricky areas that we tend to put off — the fridge.

Before you start, turn down the temperature in your fridge to avoid wasting energy. Then, empty out your fridge _completely_. Next, go through everything and throw away the items that have far surpassed their sell-by-date. This step alone will be a major improvement.

With that done, remove all the fridge shelves and drawers and soak them in your sink with some warm water and a detergent, such as Fairy Platinum. After they've soaked for a few minutes, wipe them down and let them air-dry.

While that's happening, it's time to apply some cleaning product and start scrubbing the inside of your fridge. Don't be afraid to really get in there and reach the back where spilled liquids can accumulate. When all is looking good as new, you can rinse away the product and let the fridge air-dry as well.

Once everything is dry, you can put the drawers and shelves back, along with your food, and enjoy the sight of an immaculate fridge.

There's another frequently ignored area of the kitchen that, when properly tended to, can make your overall cleaning experience a lot better. This is the cupboard space underneath the kitchen sink, which is often where we keep our cleaning supplies and yet, ironically, is usually a dingy and dirty area. Fortunately, with a little elbow grease, this space can easily be spruced up and turned into an inviting home for all your cleaning products.

No one wants to rummage through a gross and sticky collection of cleaning supplies — it can put you off housework altogether. So the real benefit here is that by giving this area some attention, you can easily make cleaning less of an unpleasant ordeal.

So start by taking everything out from under the sink and giving the area a good wipe down. Then, set about reorganizing your products so that it is easy to find what you need. The author recommends using baskets to help organize different products and to use self-adhesive hooks to hang up your cleaning cloths. If the area is still overcrowded, see if you can find a second nook to store things in a manageable way.

### 5. Tend to your drains with regular cleanings and be sure to treat your laundry with care. 

Like your refrigerator, the drains of your kitchen and bathroom sinks and your bathtub are easily neglected until they become a big stinky problem. Everything might seem fine until the moment your drain clogs up, and you're dealing with standing water and bad smells.

To avoid this, you'll want to take preventative action by cleaning your drains at least once a month.

All this simple procedure requires is for you to pour a small cup of baking soda down the drain and then give it two or three minutes to dissolve. Next, pour a cup of white vinegar down the drain. This part is actually quite fun, because when the vinegar hits the baking soda it creates a fizzy foam that can make you feel like you're performing some crazy chemistry experiment. 

After giving the vinegar-soda mix around ten minutes to work its cleaning magic, you can follow it up with some sweet-smelling disinfectant, such as Zoflora. Pouring a cup of this down the drain will kill off any remaining germs that may have been festering in the pipes, and leave a pleasant smell behind.

Then, you'll need to finish things off by pouring a full kettle of boiling water down the drain to rinse it all away.

Now, let's switch gears and look at some tips for how to get your laundry clean and smelling better than ever. For a perfect and enjoyable laundry experience, you need to make sure you have the following ingredients: a washing detergent and a stain remover to clean the clothes, fabric conditioner to make them super soft to the touch, and your favorite scent-boosting product to get them smelling great.

You should also be mindful of the wear and tear that frequent runs through the washing machine can inflict on your clothes. So if you want them to last as long as possible, you may want to consider giving them a gentle clean by leaving them to soak overnight in a big bowl of water mixed with a cup of fragrant disinfectant, such as Zoflora.

### 6. Improve your sleep by refreshing your mattress and developing an evening cleaning routine. 

It's important to get a good night's sleep, so many of us are always on the lookout for how to improve our eight hours of nightly shut-eye. Yet few people will think about one of the most obvious ways to improve the sleep experience: making sure your bed is clean.

Step number one is to strip your bed of all sheets and blankets and give your mattress a good vacuuming. Try to be as thorough as you can since this is intended to vacuum up all the dust and dead skin cells that have accumulated over time.

Next, it's time to put our magical baking soda back to work by sprinkling it over the top of the mattress. When it sits for a couple of hours, this amazing ingredient will absorb any and all smells — even the smell of cat pee — with incredible effectiveness. After that time, you'll just need to run the vacuum one more time to get rid of it all.

That's it! Put on some new sheets and you're sure to sleep more easily on your freshly cleaned bed. You'll probably want to repeat this process around once a month.

You can even make cleaning up part of your process for winding down before going to bed.

Not all cleaning requires a lot of energy. In fact, some tasks can be soothing, low-energy experiences that are perfect for when you're getting ready for bed.

Giving the surfaces of your home a quick dusting with a damp cloth and some disinfectant can be a pleasant and satisfying exercise. You could also spend a few moments organizing your pantry and kitchen cupboards, getting rid of any old clutter while getting your teas and biscuits in order.

There are a few simple end-of-day tasks to make sure your home is smelling great when you wake up tomorrow. 

You can give your sofa a quick spritzing with a fabric spray, fold up any blankets and give the pillows a fluffing. You could also put some toilet cleaner in the bowl and let it sit overnight.

And finally, just before bed is a good time to place an aromatic wax melt into a warmer, so that it will be ready to be melted the next day and fill your home with good smells.

> _"If I know that when I get up the house is going to be clean and tidy I sleep better."_

### 7. Take the time to clean your couch and make it cozy. 

The couch is a main attraction in many homes. You may be sitting on one right now! But here's a question: When's the last time you cleaned it?

Let's face it; couches can collect a lot of crumbs, spills and worse. And since they're a central part of most living rooms, it's wise to keep them clean.

However, before cleaning begins, you need to make sure that your cleaning product of choice won't stain the material. So do a spot test first to make sure the product is safe.

Once you're good to go, you can get underway by removing all cushions, throw pillows and blankets. Then you can start vacuuming the couch, removing all those crumbs while making sure to get into all the cracks and crevasses.

Next, use a lint roller to brush all the surfaces and fabrics of your couch and pick up any stubborn hairs. Naturally, this is an important step for pet owners.

Then it's time to start the washing process, and for this step, you should wear some rubber or latex gloves. 

Start by putting together a mixture of warm water, fabric conditioner and laundry detergent in a bucket or big bowl. Then take a clean white cloth (to avoid any chance of your cloth bleeding color onto the couch), soak it in the soapy water, wring it out and start scrubbing. Once you're done, you can use a body brush to bring fibers of your couch back to life. Then simply give it some time to air dry. 

There are also some steps you can take to make your couch more comfortable. The key is to have a good assortment of pillows and blankets stacked in a basket near the couch so that you can add or remove as needed. Now, whether you're reading, watching TV or stretching out for a moment's rest, you'll have just what you need at arm's reach.

### 8. For some final touches to your home, clean your rugs and spruce up your lanterns. 

Nothing can ruin an otherwise perfect room quite like a dirty rug. And nothing can brighten up a room quite like a perfectly clean rug. In fact, having clean rugs in your home is so important that it was one of the first things the author posted about on Facebook.

You may think that getting your carpet clean would be a huge chore, but it's actually a rather straightforward process, especially if you've already cleaned your couch. The modus operandi is basically the same.

So get things started with a bucket containing a mixture of warm water, laundry detergent and fabric conditioner. Soak your white cloth and wring it out, then start scrubbing your rug in sections, stopping regularly to rinse out your cloth.

It's true that this is a physical task, so to make it more fun, it's a good idea to have some music playing — something with a good rhythmic beat that you can scrub along with.

Once you're done, use a brush to go over the carpet fibers and get them standing up again. Then it's just a matter of letting the rug air dry.

Now, along with a cozy couch and a clean rug, there is one other item that can contribute to a peaceful living room, and that's candlelight.

If you don't have any old-fashioned lanterns at home, you should think about rectifying this situation. They do more than just provide light. They can also provide a warm and cozy ambiance to every room in the house.

But like everything else, lanterns can get dusty and dirty if they're not tended to. So the first step is to remove any wax residue from old candles. The author recommends using an old credit card to scrape away melted wax. Then, use your favorite multi-purpose cleaner and a durable cloth to wipe down the outside of your lantern.

A tip for removing dust and hair from the inside of the lantern is to use a dryer sheet. And finally, once the wax and dust is removed, you can give the whole lantern a wipe down with some white vinegar. Then it will be ready to glow brightly with a new candle.

### 9. There are many ways to get rid of stains and treat smells with the right products. 

At this point, you may be thinking, "What about that stain on my carpet, couch or clothes that won't go away no matter how much scrubbing I do?" Well, the author knows your pain. 

Stains are indeed the worst, but don't go throwing things away just yet as there are many tips for getting rid of even the most stubborn stains.

One of the trickiest stains are pollen marks, especially when the pollen comes from lilies. However, if you manage to catch the stain early on, you can use cellophane tape to lift the pollen off the fabric. Otherwise, try pre-treating the stain with a bit of undiluted detergent gel and then run it through the washing machine on the cold setting.

Red wine and blood are also notorious for their staining abilities. First, react quickly by blotting the liquid with a kitchen towel and then pouring a bit of salt on the stain to soak up any remaining liquid. You can then add a stain-removing product like Vanish before putting it in the wash.

Finally, if you're the parent of a small kid, you've likely had to deal with the occasional urine stain as well. For this, the best remedy is to scrub the stain with a mixture of white vinegar and baking soda.

Whenever you're dealing with a troublesome smell like urine, you should keep baking soda in mind. Often times we try to get rid of a smell by opening the windows and airing out the room, but this isn't getting rid of the source of the problem. This is why baking soda and cleaning products like Febreze are actually much more efficient at cleaning smelly cushions and mattresses. Febreze and baking soda actually attack the source and remove the smelly particles that are creating the problem in the first place.

So there you have it. With these tips you should be well on your way to giving your home a good cleaning that will make every room more pleasant to be in. Just remember, don't feel like you have to tackle it all at once. Instead, make a list and take it one task at a time. Before long, you may find yourself looking forward to cleaning!

### 10. Final summary 

The key message in these blinks:

**Cleaning doesn't have to be a chore. When you focus on making your home a more welcome and enjoyable place, cleaning can be fun. With the right supplies, you can get your house smelling great, and everything from couches and rugs to beds and refrigerators can be made clean and pleasingly fragrant. With a few handy tips and ideas about which products to use and how to use them, you can quickly turn your home into a beautiful, sweet-smelling haven.**

Actionable advice:

**Take photos of your efforts to motivate yourself.**

If you find it hard to motivate yourself to do housework, start with one task, like cleaning and decorating your kitchen counter. Once you're done, take a photo and post it on Instagram. The feedback and delight of your friends will do wonders to motivate you to keep going. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Life-Changing Magic of Tidying Up_** **, by Marie Kondo.**

Mrs. Hinch may be the current guru of cleaning, but let's not forget about the guru of tidying up. After all, it's hard to give your home a proper clean when it's overflowing with clutter, so now's the perfect time to head over to our blinks for Kondo's 2011 book, _The Life-Changing Magic of Tidying Up_. 

Kondo takes us through all the steps for getting our homes clutter-free and refocusing our attention on the things in our lives that give us joy. It's essential advice for anyone looking to create a calm and welcoming home.
---

### Mrs. Hinch

Sophia Hinchliffe — aka Mrs. Hinch — is a 29-year-old woman with a passion for cleaning. After getting married in 2018, she launched an Instagram account devoted to home life and cleaning that became an instant success, attracting over one million followers in just three months. _Hinch Yourself Happy_ is her first book.

