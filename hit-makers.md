---
id: 59db838eb238e10005594af1
slug: hit-makers-en
published_date: 2017-10-12T00:00:00.000+00:00
author: Derek Thompson
title: Hit Makers
subtitle: The Science of Popularity in an Age of Distraction
main_color: 5EC58F
text_color: 397857
---

# Hit Makers

_The Science of Popularity in an Age of Distraction_

**Derek Thompson**

_Hit Makers_ (2017) looks into the cultural phenomena of popularity and fashion, as well as the science behind them. These blinks offer an up-close examination of why some products, songs and works of art take off, while others fade into the past.

---
### 1. What’s in it for me? Discover what makes a hit. 

The process of creating, designing or composing a hit is by no means an exact science, whether it's a work of visual art, an exciting new product or a catchy tune. With so many different social and cultural factors involved, it's tough to predict what exactly will generate positive responses.

But these blinks can help. They'll show you some important characteristics of hits and outline some strategies that help a product stand out. Once you know more about what makes a hit, you'll be one step closer to breaking into the mainstream.

In these blinks, you'll find out

  * why repetition is so important;

  * how awards can actually diminish your popularity; and

  * what happened to the laugh track.

### 2. Popularity is about exposure, not quality. 

Meandering through an art gallery or museum, you might assume that you're seeing the cream of the crop when it comes to works of fine art. But the reality is that things become popular for plenty of reasons besides their actual quality.

Just take the paintings of Claude Monet. Many of them, like _The Japanese Footbridge_, depict ponds filled with colorful water lilies. These water lily paintings are world famous, with museumgoers forming small crowds around them just to catch a glimpse.

Now compare this popularity to that of another impressionist painter, Gustave Caillebotte. The National Gallery of Art in Washington, D.C. held a special exhibition of his paintings, in which the gallery described him as a relatively unknown French impressionist.

This description was accurate, since few people had heard of him. But despite this lack of name recognition, Caillebotte's work is incredible. His paintings depict nineteenth-century Paris in an exquisitely delicate impressionistic style. In his own time, around 1876, he was seen as one of the most innovative impressionists around.

In other words, Caillebotte's work was at least on par with Monet's, but Monet became famous while Caillebotte remains obscure. Why?

Simply put, it's because of exposure to an audience, which is a fundamental ingredient in popularity.

In addition to being an artist, Caillebotte was himself an art collector who was fond of the work of his impressionist friends. Three years after his death in 1894, some of the paintings in his collection were displayed in the first large exhibit of impressionist work at the Musée du Luxembourg.

Among them were several paintings by Monet, Degas and five other impressionists, but none of Caillebotte's own works. As a result, the seven impressionists chosen for the exhibit rose to recognition and remain famous to this day, just because they were thrust into the spotlight at the right moment.

> _"Studying the patches of Monet and the brushstrokes of Caillebotte won't tell you why one is famous and the other is not."_

### 3. People are drawn to objects that are novel, yet familiar enough to be recognizable. 

For most people, a bullet train or a soda fountain doesn't conjure thoughts about the history of design. But behind every object lies a story, and the history behind these sorts of objects in particular has much to tell us about the ways people perceive and consume everyday goods.

The common factor between them is Raymond Loewy, a French orphan with a talent for drawing who single-handedly brought modern design to the United States.

When Loewy arrived on US soil in 1919, he found inspiration in the modernist buildings of New York's skyline. In just two decades, he channeled this inspiration into a project to redefine American design.

He produced plans for trains and cars in sleek, aerodynamic shapes, as well as classic objects of American culture like the Lucky Strike cigarette package and the first ever Coca-Cola soda fountains.

In producing these hallmarks of design, Loewy was abiding by what was then an undiscovered recipe for good design: a combination of familiarity with more exciting or novel characteristics. In so doing, Loewy developed the first theory of what people like and, therefore, what becomes fashionable. His findings led him to a principle he called _MAYA_, or _most advanced yet acceptable_.

The idea is simply that people are drawn to two things in an object. First, they like things that are typically recognizable, and second, they grow weary of overly familiar things. Because of this, he reasoned, successful designs are those that strike a balance between these two aspects, a notion that has been confirmed by subsequent studies.

For instance, Paul Hekkert, a Dutch professor of design and psychology did a study in 2011 on why people like or dislike various objects. In the experiment, he had participants rate objects like cars and phones in terms of their typicality, novelty and overall aesthetic appeal. Unsurprisingly, the study found that people prefer objects that encompass both typical and novel elements.

As an example, while Loewy's train designs were clearly recognizable as trains, the streamlined, bullet shape of his locomotives set them apart.

### 4. The human taste for music favors the repetitive, but some variation is key. 

When you stumble upon a new song that you love, do you find yourself listening to it on repeat? You're not alone.

Human taste in pop music gravitates toward repetition. In fact, 90 percent of the music people listen to is music they have heard before. You need look no further than the Billboard Hot 100, which tracks the popularity of music in America, to know that once a song becomes popular, it stays popular for months.

But this desire for repetition doesn't just materialize in binge listening to a new hit single; it also means that people seek out songs with something familiar about them. This familiarity lets people predict, to some extent, how the tune will go and makes them feel good.

Now you're probably thinking, why don't people get bored of all that familiarity?

Well, because of the way human brains are wired, slight variations are sufficient to popularize a repetitive musical pattern. Funny enough, this is a trait humans share with mice, and experiments on these rodents can explain the human attraction to repetitive music.

If you play a mouse a B note, it will pay attention initially and, as it gets used to the sound, will stop listening. But if you add a minor variation to the sound, say a C note, the mouse will become suddenly alert.

The C doesn't just grab the mouse's attention, it also makes it so that when the tune goes back to B, it keeps listening. This single variation is sufficient to break the mouse's complacency and return its interest to the music.

In the experiment that proved this, the most effective pattern for keeping a rodent's attention was BBBBC-BBBC-BBC-BC-D, which is uncannily similar to the structure of successful pop songs that follow a pattern of verse-verse, chorus, verse-chorus, bridge.

In this way, the human taste for music is much akin to that for design objects; as long as they contain a slight variation, people are profoundly drawn to repetitive and familiar sounds.

### 5. Popularity has its dark sides, namely sexism. 

A few years back, the model, actress and athlete Geena Davis was watching a children's TV show with her daughter and was appalled by what she saw. Davis realized that popular culture readily enforces sexism, a tendency that's particularly pronounced in the American entertainment industry.

So, in 2009, Davis, along with the entertainer Madeline Di Nonno, founded the Geena Davis Institute on Gender in Media, which endeavors to explore and expose the ways popular culture promotes sexism.

Between 2010 and 2013, the foundation studied 120 recent movies that enjoyed popularity in the United States and abroad, focusing especially on the gender roles of the protagonists. The foundation found that under a third of the thousands of protagonists it studied were female, and female lead actors only appeared in 23 percent of the films.

When it came to film characters with powerful business positions, things got even worse. Out of such characters, just 14 percent were female, which unfortunately reflects the current gender ratio of CEOs in the United States.

Similarly, when films depicted scientists or government officials, just 12 percent were women. Simultaneously, women were regularly depicted in sexualized clothing — twice as often as men, in fact. And comments on their good looks were five times more common than for their male counterparts.

One obvious explanation for these clear trends is structural; Hollywood is a male-dominated industry and men occupy the vast majority of powerful positions, like those of producers and directors. But beyond that, sexism is actually popular with audiences.

Audiences rate movies lower when they feature women who act in stereotypically masculine ways or men who act in feminine ways. And therein lies a major downside of popularity: it favors and often reinforces biased views of differences like gender, rather than actively challenging them.

### 6. Popularity can only be manipulated to a certain degree and can backfire in some cases. 

Authorities and peers can have a big impact on popularity. If an authoritative or famous person calls something popular — boom, it is! Likewise, some Facebook pages look popular, but they've been artificially boosted by fake likes.

The good news is, this kind of manipulation only goes so far.

Even the most adept marketing won't be able to convince an audience to buy a poor-quality product, or, for that matter, a work of art that's entirely at odds with their sensibilities. For instance, Lady Gaga's album _Artpop_ received a scathing rating on an initial test release done through the website SoundOut. Nonetheless, her label refused to be deterred and aggressively promoted the album, which went on to sell poorly compared to her other hit albums.

It just goes to show that audiences have _some_ autonomy from marketing. That being said, popularity might not actually be that desirable; in fact, it can actually deter people from desiring a product.

For instance, the market researchers Balazs Kovacs and Amanda J. Sharkey conducted a 2014 study of book reviews on goodreads.com. They found that books that had earned awards were not as highly rated as those with no formal accolades.

Why?

Well, books that have received awards produce higher expectations from readers, but they also attract readers who might not have chosen a book of that style in the first place. This broader audience, with inflated expectations, is set up for disappointment.

The experiment also uncovered a more surprising discovery: when books become too popular, say after receiving an award, a backlash is unleashed against them. Basically, once a book attains a certain level of popularity, some people decide that it simply can't be good if that many people like it. The book loses its exclusivity and no longer appeals to the snobbish taste of discerning readers.

So, popularity has its downsides — but it also rarely lasts forever. You'll learn why in the next blink.

### 7. Popularity makes people feel comfortable laughing at jokes. 

A priest and a rabbi walk into a bar. The bartender looks at them and asks, "what is this, some kind of joke?"

Did this make you laugh? If it did, science can explain why. One of the reasons people find things funny is that they violate expectations, but in a nonoffensive way.

At least that's the conclusion drawn by the sociologist Peter McGraw in his 2010 book, _Benign Violation Theory_. According to McGraw, people laugh the most at jokes that contradict their expectations, but without being overtly politically incorrect or otherwise unacceptable.

For instance, the question that the bartender asks in the joke above surprises the audience, while also avoiding treading on sensitivities related to religion.

Beyond that, people are more likely to laugh at something if they perceive others as doing the same — in other words, when it's popular. After all, others' laughter is a sure sign that a joke is both funny and acceptable in a particular social context. And that's why the _laugh track_ was the true star of American TV in the 1960s.

The laugh track, an effect that plays the sound of laughter at key moments in a show, was invented by Charles Douglass in the 1950s, readily making punchlines feel popular and, therefore, safe.

That being said, in humor, as in other arenas, popularity can fade away — and the laugh track again provides a good example. In the twenty-first century, this motif has all but vanished from TV shows because, at a certain point, it failed to add any humor. TV series used be like theater sketches and, in this context, a laugh track to punctuate the scene was absolutely appropriate. But as shows became more cinematic, creating the illusion of an alternative reality, laugh tracks only served to disrupt the illusion.

In other words, since the world is always changing, popularity is fickle and virtually impossible to control.

### 8. Popularity is often a result of chance factors. 

Many a star would sell his soul, along with that of his grandmother, if he knew it would cement his enduring popularity in the public eye. Ultimately, however, popularity isn't something that can be controlled — at least not entirely. In the end, chance plays a major role in determining the appeal of any given thing or person.

Just take Duncan Watts, a Microsoft computer scientist who studied the reactions of thousands of interconnected people to a new product by way of a simulation in a model universe. In Watts's simulation, two elements were used to decide whether a product would become popular in a given population: _vulnerability_, or how open people are to trying something new, and _density_, how well connected each person is.

For instance, a hermit might be extremely flexible and feel ready to try all kinds of new products, but since she comes into contact with very few people, she's unlikely to influence other people's opinions of these products.

After setting these factors, Watts ran the simulation a number of times. He found that points of vulnerability and density easily led to cascades, wherein information rapidly moved from point to point, lighting up the entire network.

That being said, such cascades were incredibly rare, occurring in only 0.1 percent of cases. So, even if all the ingredients are perfectly measured, the success of a product depends on various other uncontrollable elements.

It's this sheer randomness that means yesterday's flop can easily become tomorrow's hit. This is precisely what happened with the song "Rock Around the Clock." When it first came out on the radio in 1954, audiences didn't much care for it. But a year later, the song made its way into the soundtrack for the film _Blackboard Jungle_, a revolutionary movie about an interracial school.

The movie took off and the song did along with it, eventually earning its place among the most famous rock 'n' roll hits of all time.

### 9. The concept of going viral is a myth. 

The internet is awash with sudden, and often puzzling, instances of popularity. For instance, who would think that audiences would go wild for a potbellied man singing and dancing to a song like "Gangnam Style"?

We all talk about videos like this going viral, but the truth is actually a bit different. In fact, the entire notion of a viral hit is a myth.

According to the theory behind this supposed phenomenon, if you produce something with enough appeal, a single internet user can share it, prompting a landslide of further shares and ultimately incredible popularity. But in reality, this is never what happens.

In 2012, scientists at Yahoo were studying how content spreads on platforms like Twitter, and found that 90 percent of tweets don't get shared at all. Not only that, but out of the 1 percent that did get shared more than seven times, not a single one in their study actually went viral.

What's at play instead is the power of broadcasting. In other words, rather than moving from person to person, content that actually spreads like wildfire is moving from a single, well-connected source to thousands or millions of people.

Obvious examples are large sporting events like the Super Bowl, which is viewed by around 100 million people, or major online news sites that have audiences of similar sizes.

Because of this mass base, when the _New York Times_ publishes a spinach pancake recipe, it's sure to reach a huge audience. People go on to talk about it and assume that the recipe went viral. But the reality is that a single, extremely powerful broadcaster has just fed the information to millions of people.

It's just one more example of how there's no perfect recipe for popularity. Sure, you can seek greater exposure and work to generate innovative yet familiar ideas — but in the end, chance and sheer interconnectivity will always be important factors.

And remember, popularity isn't necessarily all it's cracked up to be; some of the greatest feats of human creativity will never be recognized by the mass audiences they deserve.

### 10. Final summary 

The key message in this book:

**There's a science to popularity, but it's not an exact one. While factors like exposure, the proper balance of familiarity with novelty and the influence of repetition can explain the appeal of many popular people and things, in the end, sheer chance plays a key role in determining what people like, and what makes it into the mainstream consciousness.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Tipping Point_** **by Malcolm Gladwell**

_The Tipping Point_ discusses why certain ideas, products and behaviors spread like epidemics and what we can do to consciously trigger and have control over such epidemics.
---

### Derek Thompson

Derek Thompson is a senior editor at the _Atlantic,_ where he focuses on media- and economics-related topics. He is a regular voice on radio, appears often on television and was named in _Forbes_ ' "30 Under 30" list. _Hit Makers_ is his first book.

