---
id: 5577d6313162620007010000
slug: holacracy-en
published_date: 2015-06-12T00:00:00.000+00:00
author: Brian J. Robertson
title: Holacracy
subtitle: The New Management System that Redefines Management
main_color: FED533
text_color: 806B19
---

# Holacracy

_The New Management System that Redefines Management_

**Brian J. Robertson**

_Holacracy_ (2015) describes a revolutionary new management system championed by some of today's most forward-thinking companies, like Zappos and Medium. These blinks explain how authority and responsibility are defined and distributed within a Holacracy — and why this system leads to a more effective and dynamic organization.

---
### 1. What’s in for me? Implement Holacracy in your organization and watch productivity skyrocket. 

Have you heard of Holacracy? It is the revolutionary new management system perfectly suited to the modern, rapidly changing, dynamic business world. Holacracy is not a top-down, inflexible hierarchy, nor a rudderless flat system, but something in between — mixing agility with clear direction.

We at Blinkist know how powerful Holacracy can be: since implementing it within our organization, productivity has exploded, everyone feels more motivated and nobody complains about going to too many meetings.

These blinks, based on the work of the inventor of Holacracy, show you how you can begin to implement Holacracy in your organization too.

In the blinks, you'll discover

  * why you should "check in" to all your meetings;

  * why you should split your company into circles; and

  * how any tension within your business can be resolved.

### 2. In today’s fast-changing world, traditional management structures are obsolete. 

In a world of fast-changing technology and remarkable innovation, it's clear that the 21st century is in full swing. But many of today's businesses still rely on organizational structures and strategies developed in the 19th and 20th centuries. If the rotary telephone is so out of date, why are we still hanging on to old-school management hierarchies?

This question is particularly unsettling when you consider that today's most common organizational structures were built around the ethos of _predict and control_, which is completely at odds with the ever-changing post-industrial age.

The predict-and-control paradigm involves people at the very top of an organization developing strategies that are implemented throughout the rest of the company in a trickle-down fashion. Workers have no flexibility to change the centralized strategy — only the very top tier of management has the authority to decide what needs doing and how to do it.

This approach worked during the heavily-industrialized days of the early 20th century, when textile factories and coal mines were ubiquitous and the economy was stable. But today, it just doesn't make sense: the modern world is a dynamic place, with great competition, constantly changing markets and rapid technological development. Businesses have to be highly flexible, which is simply not possible within a top-down authoritarian structure.

In order to be flexible and agile, organizations have to adopt management systems that harness the power of every _human sensor_ — the people who monitor the business environment and know how to deal with or communicate relevant changes within it. Since each employee, at _every level_ of the company, has a unique role and perspective, they're privy to vital insights that top managers simply don't see. And in order to run a truly dynamic 21st century company, it's crucial to have as much information as possible.

Predict-and-control organizations don't make this possible. Top-down management structures stifle feedback and input from anyone lower down in the hierarchy. Thus, most companies simply don't have the capacity to evolve and adapt in today's dynamic business climate.

Luckily, there's a new management model that harnesses the power of every human sensor, bringing the organization in line with the modern world. It's called _Holacracy_, and we'll be exploring it in the upcoming blinks.

### 3. Holacracies distribute authority and assign responsibility throughout an organization. 

As we've just learned, top-down hierarchical structures aren't effective in today's dynamic marketplace. But what's the alternative?

Imagine a management model that completely reverses the predict-and-control ethos — a wholly democratic approach that gives every employee an equal say in company strategy.

Well, that probably wouldn't work: letting everyone have input in every single decision will just lead to unproductive anarchy, where making any choice requires tons of meetings and no one takes ownership of ongoing projects.

In order to have a viable alternative, you need something between this flat-as-Kansas hierarchy and the top-down approach.

That's where Holacracy comes in: instead of having an all-powerful authority at the very top of an organization or an anarchic model where nobody knows who does what, Holacractic organizations carefully spread authority across the company and clearly assign responsibility.

This is possible in a Holacracy because both authority and responsibility are defined by a constitution. Much like a national constitution (which outlines the scope and limitations of federal power), Holacracies operate according to their own governing documents and principles. This means no one can ever say, "I didn't know that was my responsibility!"

Instead, everyone, from the CEO to customer service representatives, knows exactly what they're responsible for, as well as what they have the power to change.

And that's why some of today's most innovative companies, like Zappos, have become champions of the Holacratic approach. CEOs love Holacracy because it lessens the burden of responsibility while maintaining a clear organizational structure; meanwhile, staff feel empowered to autonomously identify and implement improvements within their scope of authority. It's the epitome of a win-win!

So, how does a Holacracy actually work in practice? Keep reading to find out!

### 4. A Holacratic organization is structured around a system of interdependent purposes. 

Let's say you're ready to implement Holacracy within your organization. Where do you start? Well, first your founders, board or executive team must identify the company's _purpose_ — its reason for existing. This purpose should shape everything your company does.

For instance, e-learning company Blinkist defined its purpose as such: "to transform all knowledge into something usable." And as we'll see later on, Blinkist's purpose reverberates throughout the entire organization.

But now that you've defined your purpose, it's time to organize your company along Holacratic lines. Here's where it's useful to know the etymology of the word "Holacracy": a _holon_ is both a complete single entity, and also part of a larger whole.

Think of a human cell; it is both a microorganism in itself and also a constituent part of the human body.

As the name suggests, holons are integral to a Holacratic organization, which is structured around self-contained _circles_ and _roles_.

Taking a step back, consider the fact that most top-down companies are structured around departments that group people with similar jobs together. Well, Holacratic organizations replace traditional departments with something different, circles.

So, at Blinkist, there's a content circle consisting of all the roles related to content, such as the content editor, audio editor and publishing role, among others.

All these roles work together to achieve the purpose devised specifically for that circle, which itself is derived from the company's overarching purpose. For example, the Blinkist content team's purpose is to "Build great processes for engaging, relevant content."

Furthermore, circles are made up of holonic units called _roles_. Each role has its own purpose within a circle, which is again developed by thinking about how the role can best support the broader purposes of both the company and the circle.

Accordingly, the Blinkist Publishing Role's purpose is to "Ensure that content is created and published."

You can already see how, within a Holacratic organization, holons at every level work together to fulfill their own purpose while supporting the company's larger purpose. In the next blink, this process will become even clearer as we dig into the nitty-gritty of individual roles.

### 5. Clearly defined roles are the cornerstone of Holacratic organizations. 

As we've learned, a Holacratic company is structured around circles and roles. Now it's time to explore each of these units in a bit more detail, beginning with roles.

For starters, consider the notion of job descriptions, the traditional alternative to roles. Although most companies rely on them, job descriptions tend to be vague and unclear, not to mention the fact that people hardly ever read them, thus making them ineffective.

In reality, most jobs entail a number of additional responsibilities that fall outside of the formal job description. These are functions you're implicitly responsible for.

We've all been there: Your boss comes into your office demanding reports you'd never heard of before. "The guy before you always did them," he says. Not surprisingly, these implicit accountabilities lead to confusion.

In contrast, Holacratic roles make accountabilities and authority very clear: every single aspect of a role is spelled out explicitly, eliminating any implicit expectations or potential overlap. To ensure this happens, each role should have the following:

_A purpose_ defines why the role exists. For a human resources role, it might be "to ensure that the best-suited staff are hired, and that employees feel valued in their role."

_Domains_ are areas that a role has exclusive control over, such as "performance evaluation process" or "content in recruitment advertisements." No other role can interfere with these domains without explicit permission from the domain owner.

_Accountabilities_ explain the functions the role has to execute. Examples could include "ensuring open positions are filled within the company" and "ensuring that employee feedback is handled correctly."

As a result, an employee in the HR role knows exactly what's expected of him, how his role fits into the organization as a whole, and where his authority lies.

### 6. Roles are grouped together in a series of circles; these circles are connected thanks to overlapping links. 

Now that we understand how roles function within a Holacratic organization, let's move on to circles.

Roles are organized within a series of _circles_, going from smaller to progressively bigger. So, each role (for example, social marketing) is part of a circle (the marketing circle), which may be part of another, broader circle (say, the growth circle). This is then part of the widest possible circle, one that involves the whole company: the _anchor circle_.

As stated before, each circle has a specific purpose that's aligned with the company's broader purpose. But that doesn't mean these circles are organized according to a strict hierarchy — the anchor circle doesn't tell the smaller circles what to do. Rather, each sub-circle has the authority and responsibility to make decisions.

But of course, there does need to be communication between all the circles; otherwise, having a company-wide strategy would be impossible. And that's precisely where _links_ come in.

To keep everything aligned, Holacratic organizations rely on two "link" roles that connect each circle to another one. For instance, links connect social marketing to marketing, marketing to growth, and so on.

First, there's the _lead link_. The larger circle appoints this role to act for them in the sub-circle. The lead link's job is to align the sub-circle with the broader strategies of the mother circle.

So, when the anchor circle decides to change the company's strategy — for instance, to focus only on revenue growth — the lead links pass along this strategic shift to the other circles.

Then there's the _representative link_, responsible for communicating the sub-circle's learnings and findings to the bigger circle. Having a representative link ensures that findings from the "street level" of the organization are passed across circles.

Imagine the push for revenue growth is leading to lower quality of products and unhappy customers. The representative link would transmit that information from the customer relationship circle to the anchor circle.

Now you know how Holacratic companies are organized. But how do they actually operate? In the next blinks, we'll look at the day-to-day practices of working in a Holacracy.

### 7. Circles refine their purposes and roles in governance meetings. 

Holacratically structured companies swear by their management model, claiming that it keeps their organizations flexible and agile. This is in large part thanks to a very strictly defined meeting form called the _governance meeting_.

Roughly every month, each circle holds a governance meeting, which is when roles within the circle are tweaked, removed or created.

This way, each circle's purpose stays in line with the company's broader purpose. Governance meetings also ensure that there are roles addressing every task that must be accomplished to fulfill that purpose. In addition, the meetings help reduce tensions among all roles within a circle.

It's worth emphasizing that this is _not_ when operational discussions take place. For example, an HR circle might discuss starting a new project to help document the recruiting process, but wouldn't discuss how a particular candidate is faring at the moment.

Circles do not discuss projects; they only discuss what they want to attain.

So, to make sure governance meetings stay on track and relevant, every meeting needs to follow a strict structure.

And for that, it's necessary to have a _facilitator_ and a _secretary_ : the secretary documents what is said for future reference, while the facilitator ensures that everyone gets a chance to speak, the discussion stays focused and on-topic and that the meeting follows a strictly-defined format.

To start, each governance meeting should begin with a _check-in_. At this point, every attendee briefly provides an update on their current status, whether they're ill, stressed, happy, and so on.

Then, any _administrative concerns_, such as if someone has to leave early, should be mentioned.

Next, build an agenda: anyone who has something to discuss, like roadblocks or proposals, should give a headline for the item so it can be added to the agenda.

Once that's done, you can start working your way through the agenda. Each item should be discussed in brief detail, with follow-up actions defined. For this, you'll also have to follow the _Integrative Decision-Making Process_, which you'll learn about in the next blink.

### 8. Governance meetings follow a strictly defined processed to address proposals, tensions and roadblocks. 

In the contemporary workplace, meetings tend to drag on needlessly, with simple decisions often taking hours as the discussion moves back and forth. With Holacracy, this doesn't happen because Holacratic organisations follow the Integrative Decision-Making Process.

To understand how this happens, imagine that the person filling the online marketing role feels overwhelmed by too many content marketing accountabilities; in other words, she has identified a _tension_.

After introducing this tension in a governance meeting, she presents a _proposal_ to counter it. For instance, the individual in the online marketing role may propose creating a new content marketing role to handle those residual accountabilities.

Next comes a round of _clarifying questions_ from the rest of the circle. They can ask for clarification about the proposal and the proposer can respond, but there's no further discussion.

Then there's the _reaction round_ : everybody in the meeting has the chance to react to the proposal. These reactions must be personal (they cannot react on behalf of another role and can be positive ("I think that's great because...") or more reserved ("Hmm, I don't know if we need a new role here."). Again, there's no back and forth, just a reaction to the initial proposal.

After the reaction round, the original proposer can _amend or clarify_, that is, offer a tweaked proposal or clarify their original idea in order to answer questions.

Then comes the _objection round_, where everyone has the chance to object to the proposal if they think it will hurt the circle. For instance, the marketing data analyst might say, "But I want to get all the online marketing data, including content marketing, from one source. Two roles would create a hassle for me."

If there aren't any valid objections, the proposal is accepted. But if there are some, there comes an _integration round_, during which the group works together to change the proposal to address the objections. In this case, someone might propose making the content marketing role part of a larger content marketing and content intelligence role.

Either way, once you have something everyone agrees on, the proposal can be integrated into the team, in this case with the creation of a new role.

### 9. To maximize operational efficiency, circles hold weekly tactical meetings and create concrete projects for each role. 

In the previous blinks, we saw how circles are used to define roles and responsibilities during governance meetings. But there's one more aspect of Holacratic management we haven't covered.

In order to maximize operational productivity, circles are also responsible for creating _projects_ divided into _next actions_.

Specifically, a project is something you want to achieve in order to help the circle fulfill its purpose. Each project should have a concrete outcome — this way, you know when it's complete.

For instance, if the marketing circle's purpose determines that they need to start content marketing, the circle should create a project called, "Set up functioning content marketing process." As you can see, this goal articulates the desired outcome.

Once the project has been set up, you have to populate it with next actions, concrete steps that bring you closer to your goal.

So for your content marketing project, next actions might be to create an editorial calendar for marketing, or to research ideas for the first marketing article.

This is how projects work within individual roles. But to align projects within the broader circle, you'll need to hold weekly _tactical meetings_.

A tactical meeting is an operational meeting; participants can discuss their progress on projects, goals and any operational tensions that have come up. One of the key elements of these meetings is a _checklist_, which means that each week, the facilitator will go through a list of the most important metrics and the role responsible for each metric will report on whether they have been met. This way, everyone in the circle gets a snapshot view of how operations are progressing.

Tactical meetings also give roles the chance to discuss _operational tensions,_ areas where they need assistance from the rest of the circle to help them fulfill their goals and complete their projects.

All in all, tactical meetings promote smooth operations in a Holacratic organization.

### 10. Holacracy-powered organizations respond to today’s fast-changing business landscape with a dynamic approach to strategy. 

Having an effective strategy requires you to look toward the future. But in an ever-changing world with highly dynamic markets, the future is wildly unpredictable.

Conventional organizations do not take this into account. For them, strategy is all about setting rigid goals and defining the right paths to reach those goals. This isn't a particularly effective approach; after all, rigid goals limit your flexibility and restrict you to one path. And since no one can possibly know the future, your path might be headed in the wrong direction.

However, Holacracy-powered organizations still manage to develop strategies that respond to today's volatile business environment. Instead of stubbornly sticking to the "right" path, Holacratic organizations are flexible and responsive to the changes happening around them. 

This is because Holacracies rely on a dynamic steering paradigm, which requires the company to constantly adjust and refine its strategy in response to feedback and new input. This remains possible because every member of the organization functions as a human sensor. Holacracy's strength in this area also comes from its ability to turn tensions into meaningful changes, allowing the organisation to constantly improve itself.

With such a dynamic approach, a company can always shift its strategy when the market requires it, allowing it to be highly flexible and perfectly suited to a volatile environment.

Finally, in order to be truly responsive, your strategy should be based on _rules of thumb_ (or _heuristics_ ). A rule of thumb is simply a set of general and memorable guidelines rather than specific directives, which allow a company to operate according to a cohesive but flexible strategy.

For example, the author's company uses the strategic heuristic, "Prioritize X, even above Y." In other words, when the company encounters two opposing choices that both have advantages (like choosing between improving standards or embracing new markets), they decide what to do based on their heuristics.

Strategic heuristics are created based on the current market situation and the strength of the company. So, when the time comes to make a decision, the company has a simple rule of thumb directing them to "prioritize growth, even over quality." As a result, there's no need to create an overly complex, rigid master strategy, since this simple guideline already helps employees make decisions.

### 11. Final summary 

The key message in this book:

**In today's fast-changing world, traditional predict-and-control management approaches are obsolete. Instead, to remain flexible and respond to changes in the dynamic marketplace, many organizations are now embracing Holacracy, an innovative organizational structure that allows the company to continuously evolve and adapt.**

Actionable Advice

**Start with a guinea pig!**

If you're interested in implementing Holacracy in your organization, you're bound to encounter some resistance. So instead of trying to roll it out all at once, start with a single team, preferably one that's fairly independent and functional. This is your guinea pig. Have them adopt Holacracy and see how it goes. If it's a success, other teams and departments are bound to become interested in this new management system, which will then enable a company-wide rollout.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: _Blinkracy_** ** __****by Ben Hughes and Sebastian Klein**

_Blinkracy_ (2015) is all about an innovative organizational approach based on empowering employees and eliminating the need for managers. With insights from the Berlin-based startup Blinkist, which restructured its own workplace using this model, these blinks describe how you can implement it at your own firm.
---

### Brian J. Robertson

Brian J. Robertson is the creator of Holacracy. He developed it by experimenting with different organizational methods and practices at his own software start-up. He has also founded the firm HolacracyOne, which advises companies that are making the transition to Holacratic management systems.

