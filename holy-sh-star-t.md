---
id: 58a2d94cbcb3590004c82722
slug: holy-sh-star-t-en
published_date: 2017-02-16T00:00:00.000+00:00
author: Melissa Mohr
title: Holy Sh*t
subtitle: A Brief History of Swearing
main_color: D9B252
text_color: 735E2B
---

# Holy Sh*t

_A Brief History of Swearing_

**Melissa Mohr**

_Holy Sh*t_ (2013) is a journey through the history of swearing. Starting in ancient Rome and coming up to the present day, these blinks delve into the cultures of different periods to highlight the rich evolution of swear words and obscenities throughout history.

---
### 1. What’s in it for me? Explore the checkered past of the sh*t we’ve said throughout the ages. 

We've all heard someone complain about the rude language in films and on TV. And while you might feel indifferent to some of the words other people find offensive, phrases that an older generation consider neutral might sound harsh to your ears.

Welcome to the history of swearing!

Cursing, profanity and bad language aren't new developments in human history. These practices have always been with us, with the words that cause offense simply changing over time. Much like music trends, some swear words come back, while others are forever forgotten. These blinks will satiate your curiosity about the words we use as obscenities and why we swear in the first place.

A polite word of warning before you start, though: as you might have guessed, these blinks contain very strong, offensive language.

In these blinks, you'll learn

  * how Victorians reacted to the word "leg";

  * which current swear words were part of everyday speech in the Middle Ages; and

  * why "fuck" became popularized as a swear word during the first half of the twentieth century.

### 2. The Romans swore like sailors, but about different things than modern people. 

Imagine you're strolling through the column-lined walkways of Pompeii, Italy, a city preserved just as it was nearly 2,000 years ago. As you're taking in the ancient Roman architecture and basking in the history of the place, you suddenly encounter some seriously obscene graffiti.

That's right, the Romans were no strangers to swearing, although they swore slightly differently to how we do today; the words thought of as most offensive in ancient Rome were the result of a unique take on sex and gender.

Instead of seeing sexuality in terms of heterosexual or homosexual, as is common today, the Romans viewed sex as either an active or passive act. Being active was related to masculinity and, therefore, implying that a man held a passive role in a sexual act was considered the greatest offense. According to Roman sexual norms, an "active" man should have the desire to penetrate; whether that desire was for women, men or boys was totally irrelevant.

Given this cultural norm, the worst insult you could hurl at a Roman man was to accuse him of _cunnum lingere_, or as we know it today, cunnilingus. As a result, one of the pieces of graffiti in Pompeii reads _Corus cunnum lingit_, or "Corus licks cunt." Naturally, it's somewhat shocking for us to read these words now, and they were just as obscene in Roman times.

The tremendous taboo surrounding cunnilingus can be attributed to the ancient Roman belief that penetrating another person is desirable, while being penetrated _by_ someone is a sign of femininity. Therefore, a man, with the ability to penetrate, who instead chose to perform cunnilingus was shamed and seen as emasculated.

One Roman epigram that highlights this fact reads, "Zoilus, you spoil the bathtub washing your arse. To make it filthier, Zoilus, stick your head in it." To break it down a little, since Zoilus performs cunnilingus, the speaker asserts that his mouth is dirtier than his rear.

> Romans shouted obscene language relating to genitalia and sex during wedding ceremonies to promote fertility.

### 3. The Old Testament saw the rise of religious swearing. 

So, ancient Romans used claims about oral sex to slander people, but the Old Testament employed an entirely different type of swearing: the _oath_. In this context, swearing wasn't a way to shock and offend people, but was instead used to back up claims.

In other words, the Old Testament teaches people to swear oaths by God because doing so is essential to the smooth operation of society.

An oath is a promise made in the eyes of God, a guarantee that one will do what one says. To swear by God was to catch his attention, making him a witness to the speaker's statement. In this way, God would become a guarantor of the person's word, ensuring its validity and allowing all manner of deals and agreements to go forward.

Naturally, since oaths relate to God, they must only refer to matters of great importance and be taken very seriously. Therefore, when a person swore an oath, it was taken as the truth. Oaths taken in vain were considered destructive and sinful, with the potential to reduce God's omnipresent powers.

The logic here is that false oaths force God to bear witness to statements that were never meant to be fulfilled, effectively making him complicit in a lie and dishonoring him. As examples, consider expressions like "by God" and "by God's hands," which took the form of an oath, but with the force of an expletive.

In this way, such phrases act as intensifiers, rather than promises, to God. According to the third commandment, these were considered an offense to God, as they took his name in vain.

### 4. Euphemisms are a no-no in the New Testament. 

If the Old Testament is all about oaths, the New Testament focuses on obscene language, or more specifically, its regulation. To do so, it outlines rules for speech that forbids vulgar language and even restricts euphemisms.

For instance, the Old Testament is full of euphemistic language, like one passage from _Song of Songs_ that reads, "My beloved thrust his hand into the opening, and my inmost being yearned for him." Here, "hand" actually refers to "genitals," although lots of scholars claim this piece of writing has nothing to do with sex and is instead about God's love for Israel.

In the New Testament, though, Jesus condemns euphemistic speech by saying, "On the day of judgment you will have to give an account of every careless word you utter." According to Jesus, abstaining from the use of obscene language, hurtful words and lies isn't enough; any words without a positive effect are likewise considered wasteful distractions.

However, while the New Testament maintains that such unnecessary words should be avoided, they don't bear the same weight as language that explicitly compels people to do bad things. This vulgar language falls into a category of its own and, for the apostles, such talk was considered to lead to sinful thoughts, upon which people would eventually act.

For example, _The Letter to the Ephesians_, a piece of writing that could have been penned by the apostle Paul, says that fornication and impurity of any kind should not be referred to. This is essentially to say that Christians must not only abstain from taking part in bad deeds, but must also avoid uttering words that lead people to think impure thoughts, whether they be sexual or scatological.

### 5. Many contemporary swear words were commonplace in the medieval era, but religious swearing carried the most power. 

Most people think of words like "bollocks," "cunt" and "whore" as modern curses, but they date back to medieval times, where they were commonly bandied about. The difference is that, back then, these and other words relating to the body weren't considered obscene.

For instance, "cunt" and "arse" were the standard words for describing those respective body parts and were often found in children's books designed to teach Latin. In fact, these books even contained such useful phrases for a young boy as "you stink," "turd in your teeth" and "he is the biggest coward that ever pissed."

Not only that, but words relating to excrement were also standard, as there was no such thing as privacy and human bodily functions weren't considered taboo. Just take Erasmus, a sixteenth-century scholar who traveled and taught all over Europe. His declaration that "it is impolite to greet someone who is urinating or defecating" implies that, as late as 1530, such practices were normal behavior.

But what _was_ offensive was vain swearing and, in medieval times, doing things like taking the lord's name in vain were considered obscenities with the power to shock and offend. One of the predominant phrases of this type was "by God's bones," which was primarily used to add emphasis to statements.

In addition to vain swearing, _false swearing_ was considered one of the most sinful things you could do. Sincere oaths were the verbal contracts of the Middle Ages, guaranteeing the political relationships and legal systems of the time.

As a result, a person's innocence or guilt could be proven through an oath alone; in other words, by simply finding enough people who would swear by his innocence, a person could be acquitted of a crime through a process known as _compurgation_. Given this incredible power bestowed upon oaths, it's no surprise that false swearing was considered the ultimate sin.

### 6. During the Renaissance, obscenities relating to excrement made a comeback. 

As Protestantism rose across Europe and oaths lost their power to shock and offend, swearing began to shift away from the holy and back toward the obscene.

Protestants believe that God lacks a physical body and therefore can't be touched. As a result, oaths necessarily couldn't be thought of as directly reaching God and therefore began to lose their perceived efficacy. In addition, the sheer volume of oaths people were required to take made their swearing by God less meaningful.

At the same time, the fall of feudalism and the rise of capitalism were compounding this cultural shift. After all, oaths were no longer necessary to maintain networks as, in capitalism, the market self-regulates honesty. For instance, as it became economically worthwhile to be respectable and maintain a good reputation, promises and contracts began to take priority over oaths.

And as this change occurred, obscenities became even more powerful than they were in the Middle Ages. The sixteenth century saw the rise of the _privy_, which, as you might have guessed, is a place in which one can enjoy privacy. Privies were often small rooms protruding from houses, in which a person could leave a _sirreverence_, a common, yet elaborate word for "turd." This euphemism is a combination of "save" and "reverence," an apology often given before an offensive utterance.

In turn, the increase in physical privacy erected figurative walls between people and caused them embarrassment at the sight or mention of bodily functions — a sentiment that wasn't felt during the Middle Ages.

Interestingly enough, such instances were only embarrassing if they involved people of an equal or superior social status. In other words, you could easily discuss poop as long as it was with someone lower down the class ladder than yourself.

### 7. The Victorians were so repressed that even the word “leg” conjured thoughts too taboo for public discourse. 

As industrialization took hold across the Western world, the Victorians of nineteenth-century Britain found an outlet for swearing — but under a different guise. At this point, privacy was no longer a luxury of the rich; the middle and lower classes were becoming increasingly civilized, which in turn made sexuality and excrement deeply shameful for everyone.

As a result, bodily functions that were once carried out in public with total nonchalance were now hidden, only performed behind the closed doors that the population could now afford. In this way, bathroom time fell out of sight and out of mind, becoming taboo in the process.

So, to avoid the typical words used to describe feces and bodily fluids, like "shit" and "spit," the vocabulary was Latinized and the words became "defecate" and "expectorate." People considered these words more refined as they were pulled from the dead language of well-educated people.

But, as delicate speech came to signify class, even normal body parts became unmentionable.

Victorian society was marked by deep class divisions and refined language came to be associated with the upper class, while more direct words, viewed as "bad language," were associated with the uneducated, lower class.

As a result, swearing was commonly connected to ignorance and it was thought that people who swore lacked the necessary language skills to express themselves eloquently. This belief led to the middle class going to extreme lengths to avoid taboo subjects, using language to distance itself from the lower class.

In fact, to even hint at certain body parts during polite conversation could have been a major misstep. For instance, the word "trousers" was strictly off-limits in polite circles because, if one was to remove one's pants, one's legs would be bare, which implied the existence of other parts further up.

By the same token, "leg" was unmentionable, with "limb" being the preferred term. Then, once "limb" became too obscene, it was euphemized to "lower extremity."

> Swearing is likely kept in the "primitive" right half of the brain responsible for automatic speech, along with counting and those "ums" and "ahs" we use.

### 8. In the Victorian era, obscenities became swear words as people began to use them figuratively. 

The Victorian era was marked by an extreme repression in language and it gave rise to modern swear words. These were obscenities used in a nonliteral manner, solely to assert emotional power.

For instance, _bugger_ was one of the most prevalent. It began its life as a word literally referring to the penetrating party in anal intercourse, but over time its literal meaning faded and it transformed from an obscenity into a swear word. These days, when one describes something as "buggered over," it's meant as "messed up" and does not imply any literal meaning.

There's evidence of this word from long ago, as it was permitted to appear in print as _b-gg-r_ and was used so commonly that four-fifths of the British public said it on a regular basis. In essence, because of its nonliteral meaning, it had become a less offensive way of saying "fuck," which, even as _f--k_, was not allowed in print.

Bugger was just one of many swears that rose to prominence during this time. Another was "bloody," which first came into use as an intensifier that skirted its literal meaning. This is where swearing becomes more familiar to a modern audience, as words like this were used to strengthen those that followed.

For instance, in 1836, Mary Hamilton described a group of women as "bloody whores." She didn't mean that they were "covered in blood," but rather intended to emphasize the fact that they were "whores."

In turn, _fuck_ began to assume its modern-day usage. This began in 1790 when George Tucker, a Virginian judge, penned a poem in which a son argues with his father and the father responds, "G-- d--- your books!" and "I'd not give ---- for all you've read." The second phrase can easily be translated into the modern phrase "I don't give a fuck," and this was the first recorded example of the word.

> Researchers have found that you can keep your hand immersed in extremely cold water for an extra 40 seconds if you swear instead of using neutral word to express your discomfort.

### 9. Today, a new class of obscene words carry the most weight – racial slurs. 

Throughout the First and Second World Wars, swearing was so normal in the armed forces that saying "fuck" barely garnered a response. A collection of slang words from the First World War might shed some light on this, as it explains that "fucking" simply meant a threat like a shell or attack was "incoming."

In fact, the word was so frequently uttered that the phrase, "get your rifles!" provoked greater urgency than the same phrase with "fucking" included. This just goes to show how frequent swearing can reduce its impact, and the same goes for sexual swearing.

For instance, by the turn of the twenty-first century, movies, websites, books, television, magazines and all sorts of media with sexual themes had become widely available to the public and were no longer encumbered by taboos.

As a result, people now think and talk about sex all the time, whether in person, on TV or through pornographic material. Even supposedly "ordinary" magazines that feature racy material are openly sold at newsstands. Nevertheless, it's worth mentioning that not all words are fair game; after all, the word "cunt" is still considered highly offensive in most contexts.

So, with typical swear words losing their punch, racial slurs have begun to rise in prominence as derogatory words, likely to incite violence. For example, "nigger" was originally used in 1574 to refer to dark-skinned people coming from Africa, but over the last 60 years it has come to carry a tremendous stigma, offending people of all ethnicities.

In this way, "nigger" is a fighting word; that is, it provokes hurt, anger and is often met with physical retaliation. Just take Jerry Spivey, a district attorney in North Carolina who was removed from office after people overheard him at a bar saying "look at that nigger hitting on my wife." This offense was inexcusable because, by uttering this vile slur, he was seen as breaching the peace and inciting violence.

Or take David Howard, who lost his job after asking his colleagues to be "niggardly" with their budget since money was tight. Interestingly, this word, stems from "niggard," a word that appeared two hundred years before the slur it resembles, and actually means "frugal."

### 10. Final summary 

The key message in this book:

**Swear words have a tremendously rich and varied history. What you may think of as modern obscenities often date back centuries and were created and shaped by the social, religious and economic customs of their time.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Language Instinct_** **by Steven Pinker**

_The Language Instinct_ (1994) provides an in-depth look into the origins and intricacies of language, offering both a crash course in linguistics and linguistic anthropology along the way. By examining our knack for language, the book makes the case that the propensity for language learning is actually hardwired into our brains.
---

### Melissa Mohr

Melissa Mohr is an American writer who holds a PhD from Stanford University in English Literature with a specialization in medieval and Renaissance literature.

