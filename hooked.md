---
id: 530ca8da3533320008000000
slug: hooked-en
published_date: 2014-02-25T08:00:00.000+00:00
author: Nir Eyal
title: Hooked
subtitle: How to Build Habit-Forming Products
main_color: F2DE46
text_color: 59521A
---

# Hooked

_How to Build Habit-Forming Products_

**Nir Eyal**

_Hooked_ (2014) explains, through anecdotes and scientific studies, how and why we integrate certain products into our daily routines, and why such products are the Holy Grail for any consumer-oriented company. _Hooked_ gives concrete advice on how companies can make their products habit-forming, while simultaneously exploring the moral issues that entails.

---
### 1. What’s in it for me: Find out how you can get people addicted to your products… in a good way. 

Think about all the products you use on an everyday basis: your smartphone, your must-check apps and favorite websites. How did these products succeed in getting you to adopt them in your daily routines?

_Hooked_ not only explains the psychological processes that go into forming habits, it also shows how companies can build products to take advantage of this tendency.

You'll find out why designing a habit-forming product is the best sales boost your company could ever hope for.

You'll also discover why the scrolling Twitter feed is so addictive. (Hint: it's no coincidence).

Finally, you'll also come to understand the ethical implications of habit-forming products, which will help you avoid becoming a glorified drug dealer.

### 2. It’s difficult to change or replace established habits. 

Every New Year's eve, people make resolutions to quit drinking, eat more healthily or get more exercise. When midnight strikes, we really are fully committed to making these changes.

So why is it that on January fifth most of us find ourselves sitting on the couch munching chips and guzzling beer?

Well, the short answer is that it's due to our _habits:_ activities we've become so accustomed to doing that we engage in them without much conscious thought.

Habits emerge because our brain is eager to save time, so in most situations it will make us do whatever it was that worked last. For example, a habit of biting your nails when nervous probably emerged because your brain remembers that nail-biting once helped you release stress, so now you do it unconsciously.

The trouble with habits is, it's very difficult to permanently change them. In fact, research has shown that even if we change our routines, the neural pathways of the old habit remain intact in our brains and are very easily reactivated. This is illustrated by the fact that two thirds of alcoholics who finish a detox program start drinking again within a year.

No wonder we have such trouble with a simple New Year's resolution. So how can you possibly succeed in adopting a new habit?

The easiest way is to repeat it frequently. One study showed that students who wanted to get into the habit of flossing their teeth regularly were more successful the more frequently they engaged in flossing.

If it can't be repeated often, the new habit has to be very useful to still be adopted successfully. Consider online retailer Amazon: Most people don't use Amazon everyday, but shopping there still constitutes a habit for many of us, despite countless other online stores to choose from.

Why?

Amazon's direct price comparison between other retailers is so handy, that users make a habit out of shopping there even if it is only infrequently.

### 3. Habit-forming products generate high revenues and are hard to compete with. 

Looking at the most successful businesses and products available today, especially online, we find that many of them share one common trait: they are _habit-forming_. Habit-forming products are the ones that gradually find their way into our daily routines, like a smartphone that we have gotten used to checking automatically each morning for new messages.

There are many advantages to selling habit-forming products.

First of all, they attract long-term customers. Habits are hard to ditch so customers will likely use the product for longer, increasing their _lifetime value_ to the company. This means they will generate cashflow for a longer time.

What's more, customers love to tell their friends and family about habit-forming products, meaning they do your advertising for you. For example, one of the reasons Facebook became such a hit was that people got into the habit of using it and started inviting friends, tagging them in photos and other posts, attracting even more users.

Second, a habit-forming product has a strong competitive position.

This is because changing and replacing habits is so hard that for a new product to usurp your customers, it will need to be significantly better than the old one; no minor improvement will do the trick.

This is illustrated by the enduring success of the QWERTY-keyboard layout. It is impossible for new competing layouts, even though they are technically better, to get people to break their habit of typing on a QWERTY, because they are not a significant improvement.

Finally, companies can be more flexible when pricing habit-forming products. This is because customers have become dependent on the product to keep up their habit, so they won't be so sensitive to price changes.

An example of this can be seen in online games, where users initially play for free. After a while, the freeplay option stops, but because the gamers have built a habit out of the game, they are willing to pay to continue.

### 4. Habit-forming products require users to go through the four stages of the Hook Model repeatedly. 

Due to their many advantages, many companies strive to deliberately make their products habit-forming. But how can this be done successfully?

They should try to adhere to the so-called _Hook model_. The Hook model is a cycle consisting of four steps that, when repeated often enough, will lead the user to form a habit around the product in question.

The four stages are:

  * _The trigger:_ an external event that gets us to try a product the first time, for example a TV commercial.

  * _The action_ : what we need to do in order to use the product, for example registering on an online community.

  * _The reward:_ the fulfillment of the need that originally motivated us to take action, for example being entertained if the motivation was boredom.

  * _The investment:_ something of value that we have invested in the product, such as time, money or information.

The last step leads back to the start of the cycle and as these steps are repeated over and over again, the user starts to develop internal triggers instead of external ones. This means they will feel the impulse to use the product all on their own, even without external stimuli.

Over time, the internal triggers become stronger so that eventually the user will not even think about whether or not she wants to use the product, but rather just does it. The cycle becomes an unstoppable chain-reaction.

The Hook Model is a fascinating self reinforcing cycle that shapes our long-term behaviour. Next, let's look at some of the individual steps more closely.

### 5. To start the process of habit-building, products need an external trigger. 

People do not acquire habits spontaneously. People didn't wake up one morning and suddenly decide that they would become faithful and frequent users of an app that gives their pictures a retro-look. Rather, it's a process, and the first step requires an external trigger. In the case of the app, it was probably a friend who mentioned it or an advertisement that compelled people to try it.

The external trigger is necessary, because initially the product is not yet part of the potential customers' habits.

For example, though you might currently check your Facebook account multiple times a day, you probably needed an external trigger to start using it initially. Most likely this was an invitation from a friend who was already on Facebook.

These external triggers are "calls-to-action" and can take various forms. They can be triggers that companies pay for, like advertising that encourages people to use their products, or _relationship triggers_ that rely on viral dynamics: users inviting other people to try the product, as in the Facebook example.

To be effective, the trigger must offer the user a simple choice of actions to take. If the trigger is overly-complicated, it's unlikely that the user will engage the product.

For example, imagine you want to join a social networking site, but it's very difficult to find the registration page, or filling out the registration information takes a very long time. These kinds of obstacles make you more likely to give up and forget the site altogether. If, however, the site has a simple "Register now" button, you'll be far more likely to sign up, and the trigger will have been effective.

### 6. Once we develop internal triggers that make us use a product, we’re hooked. 

As you've seen, frequency is a key part of building habits. If we use a product very frequently, we'll likely form a long-term habit around it.

But of course companies can't rely solely on external triggers to get us to use their products, as frequent triggers are usually too costly, like advertisements, and depend too much on chance, like whether a potential customer happens to see an advertisement or not.

This means that for a company to successfully lead a customer through the Hook model multiple times, sooner or later the trigger must start to come from within the customer.

So how is this achieved?

Well, we normally use products to solve a problems we have. Most typically, they have to do with avoiding pain or feeling pleasure.

For example, think about why we buy things like food, clothes or smartphones. Usually, the reason is either to avoid some kind of pain like hunger, cold or the suffering of social disconnection, or to get pleasure, like feeling stylish or being entertained by games on the way to work. Or it could be both.

If we make a mental connection between a product and the desired solution to one of our problems, we form an _internal trigger_ for using that product. The product will get a positive association from being used to successfully solve our problems.

Often, the most powerful internal triggers are negative emotions. So for example, the internal triggers many of us have for social networks and smartphones range from boredom to fear of social disconnection to the stress of living in uncertainty, which makes us Google every unknown in our lives.

Once we've developed internal triggers for using a product, we will start feeling the impulse to use the product more and more often. But triggers only serve as cues: whether or not we actually use a product depends on other factors as well, as you'll find out next.

### 7. Every product needs to motivate and, most importantly, enable potential users to use it. 

Have you ever seen a hypnotist hypnotize a volunteer from the crowd so that once a trigger word is said out loud, the "victim" does whatever the hypnotist has told them to?

For good or bad, with habit-forming products, the trigger alone is insufficient to generate action.

According to one model of behavior, a person only takes action if three preconditions are fulfilled: yes, a trigger is needed, but the person must also _be sufficiently motivated_ to take action and _be capable of doing so_, otherwise the trigger is useless. Companies should therefore strive to increase these aspects of their products as well.

If they are unable to do so for both, companies should prioritize increasing the user's ability to use the product. This is because it is far easier and cheaper to make the product easier-to-use than it is to increase the users' motivation. A good way to do this is to simplify the steps needed to use the product. So for example, a social networking site wanting to help their users engage their product could make their registration process shorter and simpler.

In terms of motivation, users are motivated to take action if they get the outcomes they desired from the product. In general, human motivation usually consists of simple goals like seeking pleasure and avoiding pain, seeking hope and avoiding fear, and seeking social acceptance and avoiding rejection.

Emotions are also a strong motivator, which is why they are often used in advertisements. For example, consider the way sexual images are often used to advertise almost everything, even fast food restaurants. This is because the advertisers are hoping to evoke emotions in their key audience - teenagers - by promising pleasure.

As we have seen, a trigger will make us use a product, as long as we can and want to do so. The question is, what makes us use it over and over again, so it becomes a habit?

### 8. Variable rewards are key in making users dependent on a product in the long term. 

While motivation is important to get us to try a product, we won't be able to sustain it unless the product actually produces the results we wanted.

For users to want to use a product frequently, it must reliably deliver on what it promises. In other words, it must give us a reward for our actions and create the outcome we were expecting.

But this alone is not sufficient to keep us motivated in the long run. For this we need variable rewards.

Why?

Studies have shown that our craving for rewards actually causes a stronger emotional reaction than receiving the reward itself. Thus anticipation plays a crucial role.

If the expected reward is predictable, we won't get as excited about it as quickly, compared to if the reward is unpredictable. This is because variability has been proven to affect our brain chemistry in a way that increases our drive for rewards. As an example, think of how the scrolling feed on Twitter provides us with an unpredictable mix of news and messages, and how rewarding it feels to read it.

To truly make users dependent on a product, a mix of different kinds of rewards should be used. They can relate to social needs, like getting"likes" or friend requests on Facebook, to gaining resources, like winning on a slot machine, or to delivering personal satisfaction, like completing difficult levels in a computer game.

But at the end of the day, the reward must always relate to the user's initial motivation to use the product. For example, on online question-and-answer forums, people usually start contributing because they crave the reward of social recognition. When some sites attempted to offer them cash rewards instead, they found that they were not effective, as they were no longer in line with the users' original motivation.

### 9. If users have invested something, be it time, money or effort, into a product, a habit will likely follow. 

Now you've seen how a trigger leads to action and provides the user with a reward. So what is the final step in the Hook model that leads them back to the trigger and another cycle?

In short: investment. When users have invested something into the product, be it time, effort, money or even their personal data, they are more likely to use it again.

Why does this work?

First of all, we see things as more valuable if we've invested something in them. One study demonstrated this by showing that if we ourselves have made something with our own hands, we value it more highly than comparable works made by others. Similarly, if we invest time and effort to build, for example, a social network online, we will value it more.

Second, we try to be consistent in our own behaviour. Studies show that what we've done before tends to be a pretty good predictor of what we will do in the future. This means that if we feel we've invested in a product by using it frequently, we'll likely keep using it in the future as well.

Finally, we have the tendency to adjust our world-view to suit our behavior, after which that new world-view will then generate more of said behavior.

For example, think of the first time you tasted beer or wine. Did you like it? Probably not. But because you saw that other people seemed to enjoy it, you kept trying it. Your behavior influenced your preferences until slowly you got used to the taste and even began to like it. Similarly, once users feel invested in your product, they will likely change their preferences so they depend on it.

If this cycle of trigger-action-reward-investment is repeated often enough, users will form a habit around the product. In short, they will be hooked.

The immense power of the Hook model also raises questions about how it should be used responsibly, which will be discussed in the following blinks.

### 10. Companies should use the power of habit-building products responsibly. 

When we speak of getting users "hooked" on a product, the first association that springs to mind is addiction. Indeed, because the Hook-model helps companies change people's behavior, it is crucial they use it responsibly. Otherwise they are merely engaging in unethical manipulation. For example, if a fast food company were to inject their food with some addictive substance, we would deem that unethical.

Sometimes though, even manipulative products are considered acceptable, as long as they enhance their users' lives. So for example companies like Weight Watchers have technically "manipulated" thousands of their customers into losing weight, but we probably would not deem their actions wrong, because their product enhances their customers' lives.

So how do companies or entrepreneurs know whether they are right or wrong in this respect?

They simply have to ask themselves two questions:

  * Does the product enhance the users' lives?

  * Would the entrepreneur herself use the product?

If both questions are answered with a "No," the product in question is likely morally wrong.

To take an extreme example, a drug dealer would probably love to get her clients hooked on her product, but it won't enhance the users' lives, nor, probably, would the dealer herself use it.

On the other hand, your company may sell a product that you would use yourself, but strictly speaking it does not enhance the customers' lives. It could still be classified as entertainment though, which is a justifiable product.

Of course, at the end of the day every salesperson, product designer, entrepreneur has a responsibility for her product and how it affects customers. But the Hook model's exceptional power makes this moral responsibility even greater, especially as that same power can be harnessed to help people solve their problems.

### 11. To use the Hook model effectively, you have to know your product and what your customer wants. 

Every entrepreneur and product designer wants to create a successful product that people use and rely on frequently. The Hook model can help achieve this, but it's practical application requires you to know your product and your customers intimately.

First of all, it's important to understand that not all products need to be habit-forming. Whether you're creating a new product or enhancing an existing one, you should ask yourself if product should really be habit-forming.

For example, life insurance policies hardly need to be a habit-forming product, since people tend to just buy the one, and that's it. Only products that rely on users' constant and frequent engagement benefit from being habit-forming.

If you do want to create a habit-forming product, you have to start by analyzing your customers' needs and the things your product offers. This is because ultimately, your product needs to provide a solution to your users' problem often enough for them to build a habit out of using the product.

If you're creating a new product, you need to find out what exactly your potential customers hope to gain from it and how you can offer it to them. You can then adjust aspects of the Hook-model, like the triggers and rewards, accordingly.

If on the other hand you want to upgrade or enhance an existing product, you should first identify your existing habitual users and find out where their habit came from. This might help you, for example, spot some similarities between them and then allow you to use that insight to get other customers to adopt the same habit.

At the end of the day, there are as many ways to apply the Hook model as there are products out in the world. But as long as you manage to connect your product's offering to the needs of your customers and lead them through the Hook model often enough, your product is bound to be a success!

### 12. Final summary 

The key message in this book:

**Habit-building products are the Holy Grail of consumer companies, for they gain a loyal following and practically sell themselves. People form habits through the so-called Hook model, where a trigger makes them take action, which in turn results in a reward, after which the user begins to feel invested due to the spent time and effort, and begins the cycle again.**

Actionable advice:

**Pick up habits**

Many people struggle when trying to pick up new habits. But in fact, the secret to forming a new habit - be it flossing or working out - is repetition. Whatever daily routine you wish to pick up, just repeat it frequently when you're getting started and you'll find it has become integrated into your daily life in no time at all.

**Learn to spot habit-forming products**

To understand how habit-forming products take advantage of your basic psychology, try to identify all the various triggers that get you to use products during a single day. Is it an external or internal trigger that cues you? If you think about it rationally, do these products manipulate you into doing something negative, or do they enhance your life?
---

### Nir Eyal

Nir Eyal is a writer, teacher and consultant who has long advised start-ups and other businesses on designing successful products. He is the founder of two start-ups, both of which have since been acquired, and he contributes regularly to magazines like _Forbes_, _TechCrunch_ and _Psychology_ _Today_.

