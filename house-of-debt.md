---
id: 5469fb6330343600090b0000
slug: house-of-debt-en
published_date: 2014-11-17T00:00:00.000+00:00
author: Atif Mian and Amir Sufi
title: House of Debt
subtitle: How They (and You) Caused the Great Recession, and How We Can Prevent It from Happening Again
main_color: F83254
text_color: D12A47
---

# House of Debt

_How They (and You) Caused the Great Recession, and How We Can Prevent It from Happening Again_

**Atif Mian and Amir Sufi**

When we bailed out the banks during the Great Recession, we didn't actually address the real factors that caused the economic downturn. The actual problem lay with excessive mortgage lending to those who couldn't afford it, which led to heavy debts and, eventually, huge collapses in consumer spending. To avoid the consequences of this boom-and-bust cycle in the future, the authors propose new ways of restructuring debt and stimulating the economy.

---
### 1. What’s in it for me? Learn why massive consumer debt was the ultimate culprit in the Great Recession. 

The Great Recession had a massive impact on the global economy, and its harsh effects are still being felt. _House of Debt_ shows why massive consumer debt was the real villain behind the economic downturn, and explains what we can do to prevent another recession in the future.

In addition, you'll learn

  * why a higher credit score can lead to a greater chance of a default;

  * why abolishing the gold standard helped people get out of debt; and

  * why the dot-com bubble wasn't nearly as severe as the Great Recession.

### 2. Severe recessions are caused by a huge build-up of consumer debt, followed a large drop in household spending. 

What causes severe recessions? Some economists and analysts think they're triggered by natural or political disasters. Others think these economic crises arise when irrational beliefs infect public consciousness.

But this is rarely true. Events like the Great Recession are, in fact, caused by high household debt.

From 2000 to 2007, US household debt essentially doubled in size, reaching $14 trillion. Likewise, the ratio of debt owed to income earned also jumped to an unprecedented level, from 1.4 to 2.1.

Similarly, household debt was also the key factor during the Great Depression. In the 1920s, the ratio of consumer debt to income more than doubled, while urban mortgage debt tripled.

These large debt loads cause severe consumer spending cuts, and intensify the harsh effects of a subsequent recession.

In fact, there's a clear correlation between the amount of household debt before a recession and the amount of spending cuts observed during the downturn.

This was borne out by multiple studies showing that across Europe and Asia, increases in household debt in the decade leading up to the recession correlated to declines in spending after 2007.

For example, Ireland and Denmark both had greater jumps in household debt than the United States, and also more severe cuts in household spending.

Thus we can predict the severity of a recession by looking at data about consumer debt, and gauging likely cuts in consumer spending.

As we'll see, these household debt increases are devastating to the economy, because without these elevated levels of debt, banking-crisis recessions are unexceptional. In fact, banking-crisis recessions with low levels of private debt are similar to normal recessions.

> _"If it weren't for the Great Recession, the income of the United States in 2012 would have been higher by $2 trillion, around $17,000 per household."_

### 3. Declines in housing prices disproportionately affect the poorest borrowers. 

The desire to own a home drove many into serious debt in the early 2000s — ultimately creating a housing bubble. This bubble disproportionately affected the poorest borrowers for two reasons.

First, interest payments flow from the poor to the rich: If you buy a house using a loan from the bank, you're the _junior claim_ on the mortgage; the bank is the _senior claim_.

Any losses in house prices are absorbed first by junior claims on the mortgage; only further losses affect the senior claims lender.

The second reason those with less are disproportionately affected? For most borrowers, the majority of their equity is tied into their home.

So when housing prices drop, their equity drops along with it. So those who borrow the most also lose the most. Meanwhile, the rich borrow less, have less debt and consequently suffer less as a result of economic downturns.

Before the recession, the poorest fifth of the US population had 80 percent debt to 20 percent assets. By comparison, the richest fifth had ten percent debt and 90 percent assets.

And these startling debt-to-asset ratios are only the beginning: Borrowers are hit even harder by _negative equity_ and _foreclosures_.

By 2011, nearly 25 percent of all mortgaged properties had a negative equity, meaning these homes were worth less than their initial value. So if the owners wanted to sell, they would owe the difference to their lender.

Foreclosure is another harmful consequence of the housing bubble. When homeowners default on payments, the banks can _foreclose_ the property and sell it at discounted levels.

Foreclosures have a profoundly harmful effect on the economy, because they tend to push down all housing prices. For example, a foreclosure will reduce the price of other homes in the neighborhood, even if they carry no debt.

During the recession, there were _three times_ more foreclosures than in 2001 (the previous peak). In fact, foreclosures were so common that together with short sales, they made up 40 percent of total house sales.

### 4. During a recession, indebted households cut their spending, affecting jobs across the whole economy. 

When homeowners see their net worth collapse, they sharply cut spending. As we'll see in later blinks, this affects jobs across the entire economy.

The degree of these spending cuts depends on individual indebtedness: Borrowers with a heavy debt tend to react strongly to shocks to the value of their assets. So when their house's value drops, their buying habits will change in response.

For example, in California's Central Valley, the effects of the Great Recession were stark: Households saw their net worth reduced by half due to tumbling housing prices in 2007. By 2008, spending on cars had fallen by 35 percent. And then by 2009, overall spending was down by 30 percent.

By contrast, those without much debt didn't see a significant decline in net worth, so they continued spending at pre-recession levels.

But eventually, reduced consumer demand from the indebted households led to nationwide job losses, which impacted the entire economy.

How does this work? Well, it's easy to see why lower consumer spending in areas hit by a collapse in housing prices would lead to the loss of _nontradable jobs_ — those dependant on local spending, like retailers and restaurants.

But these weren't the only kinds of jobs that were affected. So were _tradable jobs_ — jobs related to the production of consumer goods, like cars and electronics.

In fact, there was a massive drop in these jobs nationwide, even in areas that didn't experience a significant debt-driven downturn.

For example, Iowa didn't have a housing bubble. In fact, its debt levels were far lower than the national average. The state even increased spending during the recession. Despite this, tradable jobs fell by ten percent, because indebted households nationwide had less disposable income to spend.

Are you trying to figure out what role the financial services industry played in the Great Recession? Well, read on to find out.

### 5. Banks fueled the housing bubble by granting mortgages to borrowers who couldn’t afford them. 

As we've seen, declining housing prices had a devastating effect on the economy. But what caused the housing bubble?

One major factor: Mortgages were given to households with low credit scores and declining income growth. Astonishingly, in low credit-score neighborhoods, borrowers with lower income growth received more mortgage credit than those with higher income growth.

In fact, between 2002 and 2005, mortgage application denial rates in low credit-score neighborhoods declined from 42 percent to 30 percent. Accordingly, the number of mortgages in those areas grew by 30 percent each year.

By comparison, in neighborhoods with high credit scores, there was only a tiny decline in denial rates, and the number of mortgages grew by just 11 percent annually.

Housing prices soared due to this expansion of credit, creating a bubble. And although some believe this occurred because lenders irrationally thought they'd be safe to take on borrowers with bad credit if prices magically continued to rise, this wasn't the case. As we know by looking at housing supply in various cities, prices rose _due_ to debt, and not the other way around.

We can see this clearly when we compare cities with an _elastic_ housing supply, like Indianapolis (which is flat, spacious and can expand with housing demand), to those with an _inelastic_ supply, like San Francisco (which has a terrain that restricts new housing developments).

When lenders started giving mortgages to people in low credit-score zip codes living in elastic cities, demand for housing went up. But more houses could be built, so prices didn't spike.

On the other hand, in inelastic cities, supply couldn't keep up with demand. House prices in high credit-score zip codes increased by 50 percent. And crucially, low credit-score housing prices saw a much steeper house price increase, up by 100 percent.

So ultimately, the housing bubble was fueled by zealous lending to borrowers who had little chance of paying off their new debts.

> _"Everyone was selling mortgages. One day bagging groceries and the next day selling my grandmother a mortgage?"_

### 6. Complex financial instruments hid dangerous lending practices and fraudulent activity. 

Why didn't anyone notice that these high-risk mortgages were so dangerous _before_ they collapsed and brought down the economy? It's partly because they were pooled together with other mortgages, so their risks were thought to be offset by safer investments.

The history of this financial practice goes back to the 1970s, when the US government began buying mortgages from local banks, combining them and then selling investors a cut of the pooled interest and principal payments — what's called _mortgage backed securities_ (MBS). These mortgages had to satisfy strict criteria (for example, the borrower had to have a high credit score) in order to be accepted.

But in the 1990s, mortgages that didn't conform to these criteria found a home with _private-label MBS_, which used a structure called _tranching_. These mortgage pools had multiple layers, from a super-safe senior tranche to lower, high-risk layers.

This complex system of tranches misled some investors, who were overly optimistic about the level of risk associated with the precarious underlying mortgages. Ultimately, private-label MBS led to lowered lending standards.

For example, in order to be accepted into one of these mortgage pools, the borrower's credit score had to be above 620. However, borrowers with credit scores just above this cut-off defaulted more often than those just below it. Furthermore, borrowers with credit scores just below 620 (those holding mortgages that weren't pooled as securities) were less likely to default.

Two factors explain the divergent default rates which occurred among borrowers with similar credit scores.

First, these private mortgage pools didn't require any proof of income. On the other hand, when banks kept a mortgage instead of securitizing it, they were much more careful to check income.

The second reason for these divergent default rates? Ten percent of loans in private-label MBS were misclassified as owner-occupier, even though they were _actually_ investor-owned. Owner-occupied mortgages are viewed as less risky and are therefore more likely to attract investors.

This practice highlights the level of fraud used to fool investors into thinking that they were buying super-safe securities.

So, now that you understand what led to the Great Recession of 2007, read on to find out what we can do to avoid similar economic collapses in the future.

### 7. Bank bailouts aren’t the best solution for debt-driven crises. 

When the Great Recession first reared its head, the US government used taxpayer money to protect banks, so that they could carry on lending money. Bailouts were supposed to improve the economy, but they didn't work.

To understand why banks were in trouble, it's worth remembering that loans are assets to a bank. So when indebted households began defaulting around 2006, bank assets suffered.

In autumn 2008, the cost of financing at banks shot up, which meant that loans to businesses were in high demand. The borrowing and lending system that powers so much of our economy was in distress, and it only improved after the Fed intervened to supply funding.

But by early 2009, the government had pumped huge amounts of equity into large financial institutions, thus protecting shareholders and creditors, and ultimately allowing banks to continue lending.

But bank loans to business continued falling in late 2008, despite the government bailout — indicating a massive drop in demand for bank loans. So though banks were no longer in trouble, the economy still suffered.

So, what was going wrong? Well, as we learned in previous blinks, it all comes down to collapsed consumer spending.

This was reflected in attitudes toward the downturn: One study asked small business for their biggest concern during the financial crisis. Just five percent cited bank financing or interest rates; on the other hand, concerns about poor sales were at 35 percent, up from ten percent before the recession.

Compare this to what happened when the dot-com bubble burst in 2000, leading to a $5 trillion drop in stock prices. This seems like a huge sum, but ultimately, the consequences for the entire economy weren't nearly as bad as what happened during the Great Recession.

Why is that? Well, owners of these hyped stocks tended to come from wealthy households with little debt. And in fact, spending across the economy actually increased between 2000 and 2002, despite the stock collapse.

### 8. Debt forgiveness will boost a recession-addled economy more effectively than government spending and stimulating inflation. 

What should we do to improve the economy after a debt-driven downturn? Today, the Federal Reserve can try to stimulate spending by lowering interest rates, but it can't control the amount of currency in circulation and it also can't force inflation.

And although government spending can improve economic vitality — every dollar of defense spending leads to $1.50 of economic output — it's financed by taxpayers, hurting the very people who need that money for consumer spending.

Instead of fighting recessions by stimulating inflation or engaging in public spending, governments should restructure and forgive debt, which would benefit both debtors and creditors. Debt-driven recessions not only arise due to debt, but also lead to deflation and wage decreases — a vicious circle which makes it harder for consumers to pay back debt, thus extending the lifetime of a recession.

This approach has a historical precedent: During the Great Depression, creditors could demand repayment of loans in gold. Then the United States left the gold standard, the dollar lost value and the gold standard repayment clause was abolished. Since debtors could repay with newly devalued dollars, the policy was, in effect, a huge debt forgiveness program.

In our time, the best solution to high levels of housing debt may be mortgage _cram downs_. Borrowers who can't pay their mortgage could arrange new payment plans with lower interest and lower overall balancing, ultimately ensuring that they were no longer in negative equity.

To see how this works, imagine you're about to default on your loan. The bank repossesses your house and resells it at a steep discount. But instead, you could negotiate a cram down: declare bankruptcy, keep the house and pay back a smaller amount. And ultimately, the bank wouldn't lose as much money through the highly reduced prices associated with foreclosure.

Ultimately, this approach would motivate you to keep up your payments, and the bank to maintain high standards — while still forgiving some of the debt.

> Fact: The debt forgiveness program we embarked on when we abolished the gold standard during the Great Depression was worth almost as much as the GDP of the United States.

### 9. A shared risk mortgage structure could prevent the next recession. 

How can we reform our financial system to prevent the next severe recession?

One proposal for avoiding debt-driven recessions is for _shared responsibility mortgages_ (SRMs), which are more like equity than debt. Under this system, the lender protects the borrower if the home price drops, while the borrower agrees to share five percent of capital gains on the house.

So if house prices rise, the interest payments remain constant, like standard mortgages. However, if the house prices drop, the total mortgage payments would fall in line with a local house price index, allowing the borrower to keep up with payments (thus avoiding default), while still maintaining equity in the house.

And then, even if someone had trouble paying their mortgage, they could still sell their equity — allowing them to move somewhere cheaper and avoid a foreclosure.

In addition to preventing foreclosures, SRMs would also lead to less negative equity and (if housing prices fall) less decline in net worth among low-income households. And by tying mortgage payments to the local house price index, SRMs also encourage more household spending, preventing job losses across the economy.

In addition, one of the biggest advantages of a world with SRMs is that they would produce billions of dollars in spending due to more stable housing prices and higher consumer spending — with absolutely no increase in government expenditures.

How does this work, exactly? Well, for example, higher consumer spending in 2006 would have prevented the economy from losing at least one million jobs, according to the authors' calculations. Each job saved adds even more to overall spending, due to the _spending multiplier_.

When we factor the spending multiplier into our SRM calculation, total job losses in 2006 could have been reduced by up to four million — ultimately preventing a recession.

### 10. Final summary 

The key message in this book:

**Recessions are usually preceded by a huge build-up of consumer debt and then a large drop in household spending, leading to job losses across the entire economy. During the Great Recession, banks fueled the housing bubble by giving mortgages to borrowers who couldn't afford them, and complex financial instruments hid dangerous lending practices and fraudulent activity.**

**Suggested** **further** **reading:** ** _After The Music Stopped_** **by Alan S. Blinder**

_After The Music Stopped_ explains and analyzes the causes of the last decade's great financial crisis. It details the mechanics of the underlying problems as well as the sequence of events as panic began to set in. Finally, it also explains how the US government managed to halt the chaos and rescue the economy.
---

### Atif Mian and Amir Sufi

Atif Mian is an economics professor at Princeton University and director of the Julis-Rabinowitz Center for Public Policy and Finance.

Amir Sufi is a finance professor at University of Chicago and a researcher at the National Bureau of Economic Research.

