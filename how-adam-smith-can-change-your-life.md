---
id: 54cf8574323335000a260000
slug: how-adam-smith-can-change-your-life-en
published_date: 2015-02-02T00:00:00.000+00:00
author: Russ Roberts
title: How Adam Smith Can Change Your Life
subtitle: An Unexpected Guide to Human Nature and Happiness
main_color: F8E03F
text_color: 5E5518
---

# How Adam Smith Can Change Your Life

_An Unexpected Guide to Human Nature and Happiness_

**Russ Roberts**

_How Adam Smith Can Change Your Life_ revives the work of the influential economist and philosopher, Adam Smith — especially his groundbreaking book, _The Theory of Moral Sentiments._ The author applies Smith's ideas to modern life, showing us how to become happier and more virtuous people, improving our relationships with those around us and ultimately even changing the world!

---
### 1. What’s in it for me? Learn what Adam Smith can teach you about happiness and love. 

It's a fair bet that most of you will have heard of Adam Smith and his most famous work, _The Wealth of Nations._ The majority of those who have heard of Smith will consider him to be, above all else, an economist beloved by conservatives and libertarians.

However, there is much more to him than this. In another, less well known book, _The Theory of Moral Sentiments,_ he turned his considerable skills to philosophy and psychology. These blinks take Smith's book and show you how to use his theories and understanding to transform your own life.

In these blinks you'll discover

  * why despite his wealth and fame Michael Jackson wasn't happy;

  * how being nice to people can help change the world; and

  * why Bernie Madoff probably didn't sleep too well.

### 2. Humans are capable of behaving morally despite being inherently selfish. 

Imagine learning that an earthquake in a distant land caused millions to die. And now, imagine learning that your little finger needs to be amputated due to a severe infection. Which scenario would bother you more, the deaths of millions or the loss of your pinkie? Well, although you might want to tell yourself otherwise, you'd probably care more about the amputation of your little finger than the death of a stranger.

Philosopher Adam Smith used the term _self love_ to describe this tendency. Self love refers to the notion that humans are inherently selfish, that we can't help placing our lives and interests above everything else.

Although self love is entrenched in human nature, Smith claimed it had limits. After all, would you actually sacrifice other people's lives just to save your little finger?

Of course not! As Smith argued, anyone with a conscience would consider that deal unacceptable. And yet, if we're fundamentally selfish, why are there such clear limits to our self-interest?

According to Smith, our conscience functions like an internal observer who observes and judges our actions, thus guiding us and discouraging immoral behavior. In other words, this observer pushes us to act honorably.

So if you want to purposefully improve your behavior, you'll pay close attention to this spectator; that way you'll see where and how your actions are unsatisfactory.

For instance, when the author started doing podcasts, he had a habit of talking way too much and not letting his guests speak. But after listeners complained about his excessive chattiness, the author tapped into his internal observer and listened to the show from another person's perspective. And when he realized that the listeners were right, he changed his attitude and made an effort to give his guests more time. He ultimately improved his podcast because he listened to his internal observer.

### 3. If you want to be happy, you have to reject false praise and earn respect. 

Everyone wants to know the secret to happiness. Well, in the eighteenth century, Adam Smith thought he'd found it. And we can sum up his ideas on happiness in one quote: "Man naturally desires, not only to be loved, but to be lovely."

Or in modern parlance, people not only want to be respected and honored, we also want to be _worthy_ of honor and respect.

That might be a surprising thought: After all, many believe that being praised will make them happy, but in fact that's not true. Praise won't make us happy unless we feel we've earned it.

To that end, compare these two prominent men: Warren Buffett, one of the most successful investors in history, and Bernie Madoff, a former stockbroker famously convicted of fraud. Before Madoff was caught, he became extremely rich conning people out of their savings.

Well, according to Smith, there can be no doubt who was happier. Because although Madoff was praised prior to his arrest, he knew he didn't deserve the adulation — therefore he couldn't possibly have been happy. On the other hand, Buffett earned respect fairly, so it could actually make him happy.

So how can we apply this insight to our own lives, and become happier? The most obvious answer to that question is that we should reject false praise.

Because often, especially if we are powerful and influential, we attract praise from people looking to please us, even though we don't deserve it. But if we want to be happy, we cannot fool ourselves by accepting the adulation.

After all, it might be nice to believe the new guy at the office when he tells us that we're the perfect manager, but we should take his compliments with a grain of salt. Because if we don't, we run the risk of being loved without being lovely.

### 4. Don’t sabotage your own happiness by deceiving yourself. 

As we learned in the previous blink, if we want to be happy, we have to reject false praise. But there are still other kinds of lies that can corrode our happiness.

To that end, Adam Smith argued that people often trick themselves into thinking their behavior is more honorable than it actually is. And he called this mental process _self-deception_.

As it happens, self-deception is quite common. After all, it's difficult to admit to our own bad behavior; instead, it's often easier to lie to ourselves, to convince ourselves that our behavior is acceptable.

Smith first arrived at these insights in the eighteenth century, but they have since been backed up by science. And in fact, researchers have identified something called _confirmation bias_, which is when people emphasize evidence which backs up their theories, while ignoring everything that negates their previously held beliefs.

For instance, two economists from different schools will have contradictory predictions for the effects of a government stimulus package, although both are based on the same evidence. Those who support government spending will predict success; those who don't support it will anticipate failure. Because ultimately, both economists simply want evidence that confirms the value of their work.

And actually, confirmation bias is also evident in our own lives. To that end, imagine that your little brother asks for help with his homework while you're writing an important paper. After you decline to help, you convince yourself that you made the right decision, telling yourself: "By working on my paper, I'm more likely to get a good grade, thus becoming a positive role model for my brother; also, since my good grades will lead to a higher future salary, I'll be able to help my little brother financially." But these justifications are self-deception: you're trying to convince yourself that you're more honorable than you are.

And although self-deception might not seem like a huge problem, if we don't reject false praise for ourselves, too, we sabotage our own happiness.

> _"The first principle is that you must not fool yourself — and you are the easiest person to fool." — Richard Feynman_

### 5. We often look for happiness and appreciation in the wrong places. 

Try to imagine a happy person. What do you see? Well, if you're like most of us, you probably associate happiness with _wealth and fame_.

That's because people with celebrity and money have the highest status in our society. And since these celebrities have wealth and fame, many of us aspire to have the same.

So in order to emulate celebrities, we buy shiny products like costly gadgets and luxurious clothes to telegraph our own connection to wealth and fame. We think this will make us more worthy of respect.

And yet, according to Adam Smith, chasing wealth and fame won't bring happiness. After all, in order to become famous, you have to work extremely hard, to exchange ease and tranquility for anxiety and a frantically paced life. That's _hardly_ heaven-like...

For example, look at celebrities like Michael Jackson and Marilyn Monroe. They had all the fame in the world, and still they didn't seem happy!

That's why, for Smith, there's another path to happiness: Instead of chasing wealth and fame, be _wise and virtuous_. Because if you choose the latter route, you'll be worthy of praise and respect, which will _actually_ make you happy.

But what do wisdom and virtue actually mean? Well, to Smith, these values are all about acting with _propriety_. That is, acting in ways others find appropriate and acceptable.

To understand propriety, imagine your friend looking worried after you ask about her vacation. So you follow up by asking whether there's something wrong. Each question is appropriate to what your friend expects from you, and will lead her to trust and respect you.

However, our actions must always be appropriate to the situation. So when we see a stranger on the street with a worried look, it wouldn't be appropriate to ask her what's wrong.

> _"We have all the tools of contentment at hand already. You don't have to conquer Italy to enjoy the fundamental pleasure of life."_

### 6. By being virtuous, you can change the world! 

Being wise and virtuous won't only improve your personal relationships, it can actually help you change the world!

That might be surprising. After all, most people think that in order to profoundly influence society, we need to do something "big." By the same token, many think that being rich and powerful is the only way to have an impact on society. That, for example, encouraging moral behavior and lessening evil is a matter of laws and regulations — something only a government official could enact.

But actually, that's not true. The government's job is to manage a country's infrastructure (e.g., to promote law-and-order or to implement financial regulations), and _not_ to control and shape society.

And in fact, top-down changes are bad for society, as evidenced by countless examples throughout history — Pol Pot and Stalin, to name two — which led to tyranny and failure.

That's why societal change needs to come from the bottom up, from people like you! And you already possess all the tools you need to change the world. All you have to do is to commit yourself to virtuous action: That will promote good behavior in others and allow you to shape society's values for the better.

To clarify, imagine that one day at work, your colleague tells an offensive joke at someone else's expense. Here, you have an opportunity to shape your company's values: If you laugh, you will condone and normalize your colleague's behavior; however, if you frown and admonish the joke teller, you will help make his behavior unacceptable, which will lead to a better company culture.

And to extend this point, you can see how, when we all commit to being virtuous and wise, our individual actions (though perhaps relatively negligible) can actually add up to broad, sweeping societal changes. In that sense, our virtue can literally make the world a better place.

> _"Being trustworthy and honest maintains and helps to extend the culture of decency beyond your own reach."_

### 7. Final summary 

The key message in this book:

**Be honest, virtuous and wise! Because if you commit to these values, you will not only improve your own life, you'll also have a profound positive influence on the people in your life — and ultimately, on society as a whole.**

Actionable advice:

**Follow Adam Smith's three-step approach to becoming a virtuous person.**

First, be _prudent_ ; look after yourself and avoid risky behavior. Second, be _just_ ; don't harm others — especially not out of self-interest. And finally, be _beneficent_ ; do whatever you can to help others and do good.

**Suggested** **further** **reading: The Wealth of Nations** ** __****by Adam Smith**

_The Wealth of Nations_ is a profoundly influential work in the study of economics and examines exactly how nations become wealthy. Adam Smith advocates that by allowing individuals to freely pursue their own self-interest in a free market, without government regulation, nations will prosper.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Russ Roberts

Russ Roberts is a research fellow at Stanford and author of several books on economics including _The Price of Everything: A Parable of Possibility and Prosperity_. He hosts the award-winning weekly podcast EconTalk.

