---
id: 567866de0bac9b0007000015
slug: how-asia-works-en
published_date: 2015-12-23T00:00:00.000+00:00
author: Joe Studwell
title: How Asia Works
subtitle: Success and Failure in the World's Most Dynamic Region
main_color: F8D548
text_color: 786723
---

# How Asia Works

_Success and Failure in the World's Most Dynamic Region_

**Joe Studwell**

_How Asia Works_ examines the economic development of nine Asian countries and, in the process, sketches a blueprint for other developing nations seeking to achieve sustainable economic growth. Joe Studwell explains why some Asian economies have boomed while others have fallen behind, revealing what history has proved works — and what doesn't.

---
### 1. What’s in it for me? Understand how economic development in Asia works. 

In the last decades, Japan, South Korea and Taiwan have delivered some of the most impressive developmental success stories the world has ever seen. Other Asian countries — such as Malaysia, Thailand and the Philippines — have attempted to follow in their footsteps. However, the economic growth achieved by the latter was only sustainable for a short period of time. So what was it that made these economies develop so differently? 

To achieve successful development, there are certain steps that states can take — and they must be taken in the right order. After all, we learn to walk before we run. Now we will explore these steps and policies and see how a state can be transformed into an economic powerhouse. 

In these blinks, you'll find out

  * why financial deregulation can be harmful for economic development;

  * why farming is actually less efficient on large scale than a small one; and

  * why South Korea promoted three different car manufacturers on their small market.

### 2. To start development in poor countries, boost agricultural output by promoting household farming. 

For a long time, Asia lagged behind the West's technological advances. But then Japan, South Korea, Taiwan and, most recently, China, started to catch up and have now transformed into economic powerhouses. How did they do it?

Successful Asian states promoted small-scale _household farming_ instead of moving prematurely to large-scale farming. Household farming maximizes output by effectively using the available labor force, while large-scale farming generates few jobs and low agricultural output.

This may seem counterintuitive at first, as huge farms appear to be the most efficient method of agricultural production. However, unlike in the manufacturing sector, increasing scale in agriculture doesn't create higher output or better quality. Only fertilizer and intensive labor can improve yields and quality.

What's more, the mechanization that comes along with large-scale farming can be harmful in poor countries because it reduces the amount of farming jobs available and the yield each plant can produce. Mechanization only makes sense if labor is scarce, which is typically not the case in poor countries.

In fact, human labor-intensive techniques are crucial to getting the highest yields possible. 

For example, shade-tolerant vegetables like celery can be cultivated in the shade of taller plants, which makes it possible to grow more within the same area — but this requires planting and harvesting by hand. Such techniques create agricultural outputs that dwarf those of machines and large-scale farming. 

Promoting household farming also has other positive effects: it creates jobs in developing countries.

Since the industry and service sectors are not strong in poor countries, the labor force has no other employment alternative than agriculture. That's why it makes sense to have an agricultural sector composed of household farms that offer lots of jobs until better alternatives of employment come along.

### 3. A well-executed and far-reaching land reform is key to promoting household farming. 

So how exactly do you promote household farming? First you need to solve the problem of _land_ : Who owns it and who needs it? 

The solution is _land reform_ : the redistribution of land among the people.

Let's look at some countries where land reform helped successfully develop their economies.

Take Japan, where, after World War II, the US-backed regulation was struggling to gain popular support, in part because of the unpopularity of the American forces themselves. Wolf Ladejinsky, an agricultural adviser, had the answer: as a witness to the Russian Revolution, he understood that land reform was crucial to creating support from the working class. And his input led to surprisingly radical legislation.

At its center was a maximum three-hectare limit for farms. This meant that wealthy landlords had to turn over their excess land, which was then redistributed among poorer farmers. As a result, by the early '50s, rural output and consumption had grown above pre-war levels, and economic inequality was considerably reduced.

Another example of a successful land reform occurred in Taiwan after the Chinese civil war, when the defeated Kuomintang government was forced to flee to the island state. By 1953, advised by American policy makers, they decided to seek more popular support by implementing land reform.

The results were staggering. By redistributing land worth the equivalent of 13 percent of the country's GDP, the amount of farmers who possessed their own land rose from 30 percent in 1945 to 64 percent by 1960. At the same time, the Gini coefficient — the standard measure of inequality, where 0 represents perfect equality and 1 perfect inequality — improved from 0.56 at the beginning of the 1950s to 0.33 by the mid-1960s.

Following these land reforms, gross output of foodstuffs increased by half in Japan, and three-quarters in Taiwan, which created a stable economic base for further growth.

> _"Without the dispossession of landlords in Japan and Taiwan there would have been no increased agricultural surplus to prime industrialization."_

### 4. Protectionist policies are necessary to make the transition to a manufacturing economy. 

You might be thinking: successful modern economies can't be based solely on agriculture, right? 

Right. But agricultural policy is the starting point of development. Once the foundation of a strong agricultural sector is created, the economy can shift toward more value-adding industries. 

In fact, the next logical step to developing an economy is boosting the _manufacturing industry_.

Why? 

There are two reasons why promoting manufacturing industries like steel, cars and textiles is the best policy to create growth — after all, it's much more effective than trying to enhance the service sector.

First, manufacturing does not need a highly educated workforce because it relies heavily on machines that can be used with minimal training. Second, manufactured goods trade more easily on the global market than services, which often require the free movement of labor.

That said, the manufacturing industry can't grow without some help. In fact, a local manufacturing industry needs to be _protected_ before it can become competitive.

In developed countries, many people believe that all you need to generate wealth is competition, which is promoted, for example, through free trade. However, apart from offshore financial centers like Hong Kong or Singapore, no country has become a worldwide contender through free trade alone.

In fact, many economic giants like Germany, the United States and the United Kingdom used protectionist policies, i.e., policies that protect an industry against competition by limiting imports, to nurture their early industries until they were strong enough to face global competition. 

They did this for a good reason: when industries are sheltered from global competition, the companies can imitate and improve foreign technologies until they're able to produce competitive products themselves.

This holds true for developing countries today, and while free trade should be a country's ultimate goal, it's only feasible once its manufacturing industry has been developed.

> _"Businesses need to be protected until they are mature, just as children are by their parents." — Sugi Koji_

### 5. Governments need to invest in technological development and support entrepreneurs. 

It's clear that a developed economy doesn't emerge overnight. It takes decades, if not centuries, of careful planning and action. 

So where does the journey begin?

First, the government must actively invest in the manufacturing sector.

For example, Japan's industrialization began in 1870 when the government started a number of pilot factories in basic manufacturing industries like silk reeling, mining and cement. Both machinery and professional workers were imported to bring the companies up to international standards. 

Their first goods were mostly poor imitations, inferior to Western products. However, they served the local economy's needs and, after being sold to private entrepreneurs in the 1880s, most of the pilot companies started generating profit. 

The second step is when the manufacturing industry starts to stand on its own two legs — at which point the government needs to use legislation to support entrepreneurs.

For example, it might be necessary to lift import duties on certain goods to ensure an industry gets a necessary supply of raw materials — even if that damages local economies by exposing them to foreign competition. In Japan, this step was taken when the entrepreneur Shibusawa opened a giant steam-powered cotton mill in 1882. 

It was the biggest investment ever made in the Japanese cotton industry and signposted the arrival of _economies of scale_ in Japan. The government supported Shibusawa by lifting import duties on raw cotton, thereby sacrificing the interests of local cotton farmers, who were then at the mercy of global competitors. But this decision boosted Japan's economy overall: the Shibusawa mill singlehandedly ended Japan's chronic trade deficit, and by 1914 cotton textiles accounted for 60 percent of all Japanese exports. 

In the next blink, you'll learn about the third factor of a successful industrialization policy: export discipline.

### 6. Successful industrialization requires a system that forces businesses to export and encourages competition. 

We've seen how important it is to protect new local companies from international competition. But companies eventually have to reach a stage of _maturity_ when they can survive on their own. 

This process takes a long time, and successful governments have to keep spurring companies' development through two key measures: _promoting exports_ and building _domestic competition_. 

For example, countries like Taiwan and Japan promoted exports by subsidizing the companies that exported the most. 

But South Korea took things one step further. It gave companies access to bank credit depending on how much they exported. Companies that didn't export enough lost all state support. They were thus forced to merge with their more successful peers or go out of business altogether. 

South Korea also knew it needed domestic competition to prepare its companies for the world market.

When it started its car industry in 1973, South Korea ensured vigorous competition by setting up three private firms to compete in a domestic market of only 30,000 cars per year. 

Other Asian governments who ignored the importance of competition did so at their own peril. 

Take Malaysia, which let a state monopoly supply its much larger domestic market of 90,000 cars a year. At first, things seemed fine — but the gulf between the competitive and noncompetitive nations eventually became clear.

During the '80s and '90s, Asia experienced its "boom years" of high growth. This concealed the lack of a competitive industry in certain countries. But when a financial crisis shook the Asian world in 1997, the Northern countries, which had bolstered export and competition, recovered much faster than the Southern countries, like Malaysia and Thailand. Despite having similar GDP levels at the end of World War II, today Korea and Taiwan have GDPs per capita four times larger than Thailand and Indonesia.

> _"Without focus on manufacturing for export, there would have been no way to engage tens of millions former farmers in the modern economy."_

### 7. Premature financial deregulation can stifle development. 

We often hear economists lobbying for financial deregulation — but is this always the best policy?

If deregulation is premature, it can actually undermine the government's ability to develop a competitive technological industry. 

Consider the case of Malaysia, which deregulated its stock exchange in 1989. As a result, money crucial to the development of the economy was instead diverted into speculation: banks were quick to lend money to high-earning speculators, leaving little money for businesses requiring loans. Indeed, a study in the 1990s by the central bank found that Malaysian companies could only obtain a very small fraction of the financing they needed from banks.

Due to insufficient support from the financial sector, technological development and a world-class manufacturing industry became nearly impossible. 

Other countries were wiser, regulating their financial sector to direct money to the right causes. 

In Japan, Korea and Taiwan, the state maintained control over the financial system during crucial phases of development and limited financial speculation. Banks were given incentives to invest according to the government's interests, with central banks giving discounted loans to banks to facilitate exports or upgrade technology. 

Despite the fact that financial systems were still quite wasteful and inefficient, these incentives were enough to ensure that the most important projects were financed.

Under certain circumstances, early financial deregulation can even work for upcoming states. Singapore and Hong Kong are two such exceptions. Their location makes them perfect shipping hubs, and because of their small and concentrated population, they don't require a lot of jobs that could only be provided by manufacturing or agriculture. Instead, they could concentrate on building financial services and trade from the start.

> _"Without financial repression, it would not have been possible to pay for an accelerated economic learning process."_

### 8. Stripping back faulty communist policy set China’s economic development in motion. 

Let's use what we've learned, and look at China's development. For a long time, China's economy stagnated because of two communist policy mistakes: 

First, the communists believed agriculture was only efficient at mass scale. But the process of collectivization led to widespread famine and 30 million people died from starvation.

Secondly, they wanted to be self-sufficient, but didn't understand that trade was necessary to develop their manufacturing industry. By limiting international trade, China was cut off from the foreign technology it required to develop its industries.

This all changed when Chinese leader Deng Xiaoping kick-started China's progress with three development strategies other northeastern Asian states had already used:

First, he gave peasants more autonomy by reinstating household farming.

Under Mao, farmers had received compensation for meeting a quota of produced goods, but there was no reward for exceeding this quota.

But Xiaoping implemented the "household responsibility system." In this system, quotas were reduced and farmers were allowed to sell their excess production on an unregulated market. This new system led to a massive boost in agricultural output: today, Chinese rice yields are among the highest in the world. 

Second, he helped develop competitive products by opening China up to international trade and technology.

It began in 1980 when China made a deal with the power company Westinghouse, which allowed the company to share the technology necessary to start producing basic turbines in China. Today, the three biggest thermal turbine producers in the world are Chinese companies.

Finally, he took control of the financial institutions and invested in development.

Nowadays, almost all banking assets in China are owned by banks run by the state. And the three policy banks established in 1994 — the Export-Import Bank of China, the Agricultural Development Bank of China and the China Development Bank — are still the main forces driving development policies like export discipline and agricultural output through their investments.

> The China Development Bank had almost $900 billion in outstanding loans at the end of 2011, twice what the World Bank lends globally.

### 9. China has made great progress, but still faces many problems. 

Does China's economic shift mean it's about to become a rich industrial country? 

Not quite.

There are still big problems to solve in China. For one, it depends too much on state-controlled businesses.

State companies have shown they can thrive in industries where technology — such as thermal turbines — evolves predictably. But modern consumer markets need a higher level of flexibility. 

For example, the state runs many large automotive companies. But, instead of developing their own cars, they rely on foreign joint ventures for new designs and technology that consumers desire. And because the state-owned companies flood the market with cars developed by foreign companies, China's private automotive manufacturers like Geely and Chery struggle to develop their own popular products. Before the country can truly thrive, it needs to start supporting and protecting its own firms.

Another big problem is the high income gap between rural and urban areas. On average, an urban income in China is three times larger than a rural one, whereas in northeastern Asia the two are roughly equal.

The government has already implemented some policies to tackle the problem.

In 2006, local governments were banned from levying taxes on farmers and subsidies to farmers were increased. Also, a stimulus program invested heavily in transportation, medical, educational and farming infrastructure in rural areas.

These seem like steps in the right direction, but the income gap hasn't decreased since the policies were implemented.

One reason might be that peasants don't own their land in China, making them reluctant to invest in and develop their farms, and their incomes do not improve. They also can't sell their land for a profit. What's worse, the government can always terminate the farmer's contract and repossess their land if deemed necessary, leaving the farmer high and dry.

If China really wants to reduce the income gap, they'll need to take land reform to the next level, and allow farmers to own land, following in the footsteps of Japan and Taiwan.

> _"Without more policy support from government, China's private sector will not fulfill its potential._

### 10. Final summary 

The key message in this book:

**To kick-start economic advancement in developing countries, governments should promote household farming, build a competitive manufacturing industry and harness the power of the financial sector to benefit the economy as a whole.**

**Suggested** **further** **reading:** ** _Dealing with China_** **by Henry M. Paulson**

_Dealing With China_ reveals China's journey to becoming the economic superpower it is today. These blinks explain the advantages and disadvantages of this rapid growth, and offer insights into how the US and China should work together to face today's global challenges.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Joe Studwell

Joe Studwell is a journalist, blogger and public speaker currently doing a mid-career PhD at Cambridge University.

