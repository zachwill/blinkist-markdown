---
id: 534d52776633610007620000
slug: how-brands-grow-en
published_date: 2014-04-15T06:12:08.000+00:00
author: Byron Sharp
title: How Brands Grow
subtitle: What Marketers Don't Know
main_color: C32741
text_color: A82238
---

# How Brands Grow

_What Marketers Don't Know_

**Byron Sharp**

In _How Brands Grow_, Byron Sharp tackles conventional marketing wisdom, disproving many of the conventional marketing myths with scientific facts and establishing some scientifically proven principles marketers should use.

---
### 1. What’s in it for me? Find out the reality behind successful marketing practice. 

How do marketing professionals decide how to market a particular brand? 

Do they look at the empirical evidence to see what has been _proven_ to work? Or do they simply do what they've been doing for decades? 

As Byron Sharp argues in _How_ _Brands_ _Grow_, they mostly do the latter, i.e., they work on the basis of long-held, conventional beliefs rather than up-to-date empirical evidence. _How_ _Brands_ _Grow_ aims to correct this by providing the reader with answers to the questions that marketers ask themselves every day, e.g., whether to put a product on sale, or whether to focus all efforts on retaining existing customers or getting new ones? 

In these blinks, you'll find out why people would rather "satisfice" than optimize their purchasing decisions (you'll even learn what that means). 

You'll also learn why advertising isn't necessary to hold onto loyal customers, but is crucial for acquiring new ones.

Finally, you'll find out why, whenever you need a coffee, there's probably a Starbucks close by.

### 2. Marketing practice should use evidence provided by marketing science, not rely on traditional beliefs. 

For thousands of years, it was very common for medical doctors to practice bloodletting on their patients: just a few centuries ago, it was used as a cure for every health problem imaginable. Then, as science advanced and empirical evidence mounted, we came to learn that bloodletting is largely ineffective.

Similarly, marketing practice has long operated on the basis of beliefs that have no empirical foundation.

One of these established beliefs is that brands need to have an equal amount of loyal customers and customers who switch between brands ("switchers").

Take, for example, the toothpaste brands Colgate and Crest. In 1989, a market analysis revealed that Colgate's consumer base was made up of 21 percent loyal customers and 68 percent switchers, while Crest's comprised 38 percent loyal customers and just 46 percent switchers.

For the marketing managers at Colgate, this data was worrisome enough to convince them that they should produce more persuasive advertising to keep their customers loyal.

However, like many maxims of marketing, this belief is wrong.

In marketing, there is a scientifically proven pattern known as the _double_ _jeopardy_ _law_ which states that brands with a smaller market share have fewer customers and, what's more, that those customers are less loyal than those of bigger brands.

This reveals that the buying behavior of customers is related to a brand's size, and that it's therefore only natural that Colgate, with its 19 percent market share, has fewer loyal customers and more switchers than Crest, which has a market share of 37 percent.

Thus, these figures shouldn't concern Colgate's marketing department, as they aren't the consequence of a weak marketing strategy, but simply of the brand's relative size.

As the above example illustrates, in order for it to be effective, marketing practice should ground itself in the findings of marketing science. That's because marketing science can reveal the _actual_ causes and effects of marketing.

### 3. To grow your customer base, focus efforts on getting new customers, not on stopping existing customers from leaving. 

If you've ever considered how brands grow, you probably came to the correct conclusion that it all comes down to how many customers they have. But how exactly do brands increase their customer base?

In general, a customer base grows in two ways: acquiring new customers and holding on to existing ones.

However, one of the established maxims of marketing is that retaining customers has a greater impact on a company's growth than acquiring new ones.

For instance, an influential business article published in 1990 focusing on how companies should manage their customer base proposed that company profits can increase by nearly 100 percent for every 5 percent of customers retained.

But that's wrong.

The argument was based solely on a thought experiment. Moreover, the inaccurate evidence was presented in a misleading way: rather than a 5 percent drop in leavers, the decrease was of 5 _percentage_ _points_ — in other words, from 10 percent to 5 percent, which amounts to an actual decrease of 50 percent, not 5 percent.

In contrast to retaining existing customers, acquiring new customers is absolutely essential to growing a brand.

The defection rate of a company's customers is largely determined by its size — which means that it's difficult to control. Brand loyalty is dependent on market share, so the market leader will have the lowest defection rate, while the smallest company will have the highest.

For example, a study of the defection rate of Australian banks revealed that CBA — Australia's biggest bank, with a 32 percent market share — had a rate of just 3.4 percent. In contrast, the smallest bank included in the study — Adelaide Bank, with its 0.8 percent market share — had a much higher defection rate of 8 percent.

Therefore, since brands have limited control over how many customers they retain, they should instead focus their efforts on getting new customers.

### 4. Just under half of a company’s sales come from non-frequent users. 

How often do you buy Coca-Cola? Every day? Once a week? Once a year?

In terms of consumption, there are two kinds of buyers: light and heavy. _Light_ _buyers_ purchase a given item only occasionally, while _heavy_ _buyers_ purchase it more frequently.

For example, some people are heavy buyers of Coca-Cola, meaning they purchase it regularly — that is, on a daily or weekly basis. Others buy such soft drinks on only a handful of occasions throughout the year — for instance, when they go to the cinema.

Most brands operate on the principle that the ratio of light to heavy buyers follows _Pareto's_ _Law_, i.e., the mathematical formula that states that 80 percent of the effects are produced by 20 percent of the causes. When this formula is applied to sales, it suggests that the heavy buyers of a product (the top 20 percent of customers) are responsible for the majority (80 percent) of sales. On this basis, the marketing efforts of most brands are focused on maintaining the consumption of heavy buyers.

But evidence indicates that, in sales, the ratio is not so extreme: rather than 80/20, research suggests that the ratio is approximately 60/20. In other words, a little more than half of a brand's sales come from the heavy buyers among its customers, while light buyers are responsible for the remaining sales.

In fact, one 2007 study investigated the presence of this ratio in the sales of body sprays and deodorant products of various brands, such as Dove, Nivea and Adidas, and found that the heaviest buyers (the top 20 percent) were accountable for between only 46 and 53 percent of sales.

Marketers usually focus their efforts on retaining heavy buyers, and neglect light buyers who they assume account for just 20 percent of sales. But those light buyers actually account for up to 50 percent of sales, so marketers would do well to consider them in their marketing strategy.

### 5. Attitudinal commitment to brands is weaker than marketing mythology makes it out to be. 

One of the more popular marketing strategies that brands deploy is creating a relationship with customers to increase their loyalty. Behind this strategy is the belief that branding has a massive influence over customer preferences.

For example, in a famous study that focused on consumer preferences, subjects participated in a blind taste test to determine which drink they preferred, Coca-Cola or Pepsi.

In the test, subjects were given two soda samples: one sample was labeled as either Coke or Pepsi, while the other was unspecified but could only be either Coke or Pepsi. In reality, both samples were identical.

When the labeled soda was identified as Coke, the majority of subjects said they preferred it to the non-labeled soda. In contrast, when the labeled soda was named as Pepsi, the subjects preferred the non-labeled one.

The explanation for this was attributed to the loyalty of Coca-Cola's customer base that was created by the company's successful marketing campaign.

However, the emotional bond between customers and brands is much weaker than conventional wisdom assumes.

For one thing, people's beliefs are inconsistent: in fact, if you pose the same question to the same people on two separate occasions, only half of them will give the same answer the second time around.

For instance, in a survey concerning Australian financial services brands, participants were asked on two occasions whether they agreed with the statement that the brand "would value me as a whole person, not just a transaction."

On the first occasion, 11 percent agreed with the statement when it concerned the brands ANZ and NAB.

But on the second occasion, only 53 percent of those who'd previously agreed with the statement in terms of ANZ agreed again, and a mere 44 percent agreed with the statement when it concerned NAB.

Additionally, most consumers don't care much about the brands they buy. Although many consumers appear loyal to a given brand, they continue to purchase a brand's products because they like the products, not necessarily the brand itself.

### 6. Marketing should focus on making brands noticeable instead of trying to differentiate them. 

Most of us have wandered the streets of an unfamiliar city with our stomachs rumbling and gone to eat at the nearest fast food restaurant. But have you ever considered why you choose a particular brand over others?

Marketers focus on capturing your attention by making their products and services different from all the others.

This _differentiation_ means that companies try and sell unique products. With so many brands out there fighting for our attention, uniqueness helps brands stand out.

For instance, McDonald's, Pizza Hut and KFC each sell one specific type of food: burgers, pizza and fried chicken, respectively. Because these products are different from each other, they're supposed to appeal to different customers.

But within a particular market sector, the differentiation between brands doesn't vary much: although McDonald's, Pizza Hut and KFC sell different food products, they still compete as fast food brands.

According to the logic of brand differentiation, McDonald's only competes with other burger restaurants, like Burger King, and not with KFC and Pizza Hut. But, in reality, all the above brands compete with each other in the crowded fast food market.

So rather than aiming to differentiate a brand, marketers should instead aim to distinguish the brand in the marketplace by making it more visible, and they can do this by investing the brand with distinctive characteristics.

Because offering different products doesn't actually distinguish a brand in the marketplace, brands should try to stand out from each other by other means like using noticeable and recognizable logos and colors, such as Coca-Cola's red background or McDonald's golden arch. 

So if you do get hungry while visiting an unfamiliar place and end up looking for a fast food restaurant, you probably won't have your mind set on a particular kind of food and will simply choose the first fast food chain you find. In this case, the highly visible golden arch of McDonald's is a determining factor in your choice, as it's noticeable and recognizable.

### 7. Advertising works thanks to its effect on memory and should target mainly light buyers. 

The previous blinks suggest that advertising isn't as effective as commonly thought. But is that reason enough to abandon it completely?

To answer that, let's first look at how advertising works on consumers.

The main purpose of advertising is to influence what people buy by affecting the consumer's memory. Advertising achieves this by constructing and renewing so-called _memory_ _structures_.

Simply put, a memory structure is every association that a person has with a given brand and, the more positive associations someone has, the more likely they'll be to purchase a brand's products.

Furthermore, these memory structures must be renewed continually. As time passes and people change, brands must stay up to date to remain relevant to consumers. Even well-established brands like Coca-Cola have to do this, which is why they regularly update their advertisements.

Coca-Cola used to be sold in drug stores throughout the U.S., thus marketed to trigger associations with teenagers' visits to buy the drink at the drugstore during their fun, carefree summer break.

Nowadays, Coca-Cola ads are designed to remind consumers of those fun times in their lives when they drank Coca-Cola — like a night out with friends when everyone was drinking rum and cokes.

Both advertisements triggered consumers' happy memories of drinking Coca-Cola, thus encouraging them to purchase it again.

However, the advertisements of a brand doesn't need to target its entire consumer base, as it's mainly light buyers who are influenced by them.

Heavy buyers simply don't need to be persuaded to continue buying a product, as they stick to their brand no matter what. Light buyers, on the other hand, change their minds constantly about what they'll purchase, so their ultimate decision is informed by many factors, including a brand's advertisements.

For light buyers, advertisements work mainly by ensuring that the potential customer doesn't forget the brand. In other words, it targets the memories of light buyers who associate Coca-Cola with a good time.

### 8. Marketers should be careful when using price promotions as a strategy. 

We've all seen "ON SALE!" signs in store windows throughout our lives. But have you ever wondered why brands use these price promotions?

Because their effect on sales is directly visible, usually resulting in a short-term spike in sales.

This occurs because these promotions attract _non-frequent_ _buyers._ _S_ uch buyers tend to switch between brands and might buy whichever has the cheapest products. Thus, the effect on sales that's prompted by price promotions is only short term: once the promotion is over, the brand's product pricing returns to normal and sales return to their regular level.

However, more sales don't necessarily translate into higher profits. When you decrease a product's price, you also decrease its profit margin.

So for brands to make a profit on sale items, they have to calculate the price reduction on the basis of a product's _contribution_ _margin_, which is the revenue the product must generate to cover its costs.

For example, if a sweater is usually sold with a 30 percent contribution margin, then reducing its price by 10 percent means that you must sell 50 percent more sweaters to compensate for the discount.

Another issue with price promotions is that they can have negative effects after the sale period.

That's because consumers have a reference price for a given product — a rough expectation of what a product's price should be. Thus, when a brand sells its products at a reduced price for a prolonged period of time, the reference price in the minds of customers decreases.

So, if the sweater is sold at a discount of 10 percent, customers get used to that price and might be unwilling to purchase it at full price.

This might be a welcome effect for the brand — if they can manage to maintain their profit margins. If they can't manage to hold the lower, discounted price for a prolonged time because of high production costs, they will ultimately bankrupt themselves.

### 9. Brands increase their sales when more people find them easier to buy. 

Since profit increases are dependent on an increased customer base, good marketers focus their efforts on attracting new customers.

But getting consumers to notice a brand isn't easy.

In part, that's because people consume more media than ever before. One 2005 study investigating media consumption in the U.S. found that approximately 30 percent of the average person's day was spent listening to, watching and reading media like TV, online content and newspapers.

Thus people are exposed to several hundred advertisements every day, rendering the impact of any one advertisement extremely small.

Furthermore, consumers tend to "satisfice" rather than optimize their purchasing decisions. In other words, we tend to settle for products we consider satisfactory instead of spending lots of energy and time searching for the optimal product.

So what can brands do to increase their number of customers? The most effective way is for them to focus on becoming _easier_ _to_ _buy_.

There are two aspects to this: mental and physical availability.

Mental availability refers to the probability that consumers will think about a particular brand when they're deciding which brand's product to purchase. And the larger the market share a brand has, the more likely the brand will come to mind when consumers are about to make a purchase.

For example, which brands come to mind when you want a coffee? For many people, the first name is Starbucks.

The second aspect has to do with making your brand physically available. It's obviously not sufficient to prompt consumers to think of your brand while they're making purchasing decisions. That brand also has to be available to buy whenever a consumer might want it, otherwise no sale can be made.

For example, if the name "Starbucks" pops into your head when you need a coffee to go, but you happen to be in a place that has no Starbucks, you'll probably settle for whatever coffee shop is around. That's the reason there are so many Starbucks stores in each city.

### 10. Final Summary 

The key message in this book:

**Marketers shouldn't just blindly follow marketing myths because they're conventional or because they might've worked out at some point in the past. Instead, they should consider the empirical evidence of marketing science and do what's already been proven to work, as this will increase their chances of success.**

Actionable advice:

**Make your brand stand out.**

When you're creating your brand, you have to figure out how to get it noticed among the competition. Often, brands believe they should try to make their product different from those of other brands. But that's wrong. You should take steps to make your brand stand out, perhaps by giving your product very bright packaging or a memorable name.
---

### Byron Sharp

Byron Sharp is a professor of marketing science at the University of South Australia. He has written over a hundred articles on research in marketing and focuses mainly on establishing empirical laws that can be used in marketing practice.

