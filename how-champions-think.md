---
id: 55c7c31166643600071c0000
slug: how-champions-think-en
published_date: 2015-08-10T00:00:00.000+00:00
author: Dr. Bob Rotella and Bob Cullen
title: How Champions Think
subtitle: In Sports and in Life
main_color: 2069A1
text_color: 1B5887
---

# How Champions Think

_In Sports and in Life_

**Dr. Bob Rotella and Bob Cullen**

_How Champions Think_ (2015) is a concise guide to the psychology behind success, as used by athletes and business professionals. Chock-full of fascinating examples and useful tricks, these blinks will set you on your path to achieving your maximum potential everyday.

---
### 1. What’s in it for me? Think like a winner and you’ll become one! 

Whether you're a CEO, a student, a salesperson or a sports star, you want to perform the best you can everyday. You spend your life striving to reach your true potential.

And yet, chances are that you are further from that potential than you think. Many of us just can't reach the top, as negative influences keep us down or societal pressures dissuade us from the struggle to succeed.

Too often we base our goals on what _others_ expect us to achieve, rather than what _we_ actually want!

It doesn't have to be this way. You can unleash your true potential by taking a page from the world of sports. These blinks show you how to find your true potential and become a champion in whatever you do.

In these blinks, you'll discover

  * why the only reality is that which you create;

  * why all champions are essentially optimists; and

  * why there is no barrier in life that you cannot overcome.

### 2. Competing against others is challenging. Yet you are your own toughest competitor. 

Whether in business or in sports, most of us want to be the best — perhaps even a champion!

But although so many people dream of becoming exceptional, most end up staying average at what they do. Why is this? Essentially, they expect far less of themselves than they should.

The exceptional few rise above the rest because they take their dream and use it to create _process goals._ Process goals let you turn your dreams into reality!

By helping you make and sustain your commitment, process goals help you realize your true potential.

So how do you develop your own process goals?

Start by picturing your dream as a ladder. Each step of this ladder represents one day, one challenge, one possible output, all which lead you to your desired outcome.

Say your goal is to win a sports tournament. Your goals may involve lots of training and everyday practice. As an athlete, you may want to tweak your workout, find an expert to teach you new techniques or improve your diet to ensure peak performance.

Now let's say you're a salesperson looking to make $150,000 in commissions this year. You might start setting yourself a goal for the number of calls you make before 10 a.m., 12 p.m., 2 p.m. and so on, in order to track your effectiveness and efficiency.

As you start to set process goals, some might seem impossible from the outset. As a result, most people tend to set low goals as they're afraid of failing.

Yet when you set yourself very high goals — goals that many would consider unreasonable — you will, eventually, achieve more than those people who settled for less.

Remember: never set limits on what you can accomplish. You'll reach any goal if you persevere!

> _"Even your failures will be better than most people's best."_

### 3. Negativity beats you down and keeps you from being your best. Smile! Choose optimism. 

You can't read the newspaper or watch the news on television without being overwhelmed by events rife with misery.

It's not unlikely that you subconsciously absorb all this negativity, which can make you sad and pessimistic.

Though you might not realize it, a negative environment can prevent you from reaching your true potential.

This is because we let our lives be guided by what's already been achieved. We rarely choose the unexplored path, for fear of failing and being judged by those around us.

In 1954, for example, experts said a person could not run one mile in under four minutes. The result of such negative thinking? No athlete tried to do so.

Yet a man named Roger Bannister, with hard work and perseverance, broke the record. He set the foundation for other athletes to then break his record, showing the world that the impossible can be possible if you're determined!

Many athletes followed Bannister's example and successfully conquered the four-minute mile.

While there's no perfect correlation between optimism and success, there is an almost perfect correlation between pessimism and failure.

So choose to be an _optimist_ — all you have to lose is your negative attitude!

Optimism is a little bit like faith. It's not something that you know based on evidence, but rather a feeling that you create to get you through tough times.

A simple trick that you can use in moments of stress is to smile. When you smile, you signal to your brain that you're happy and relaxed — ready to give your all.

### 4. Confidence is the key to success. It is something you choose and must work to build everyday! 

We often assume that confidence comes with winning, but if this were true, how could someone win for the first time?

Having confidence, much like optimism, is a choice you make. If you visualize your goals, keep practicing and maintain a record of your smallest to greatest accomplishments, you can start to build confidence that will fuel you to keep working hard.

Remember: the journey to your goal is more important than the goal itself!

Before professional basketball player LeBron James became the superstar he is today, as a rookie he could only make some 29 percent of his three-point shot attempts. To improve his performance, James consulted with the author, who suggested he shoot 400 variations on three-pointers everyday. 

Through repetition, James was unconsciously visualizing his goal; and by practicing his execution, during a big game, he could let his subconscious mind lead the ball into the hoop effortlessly.

James not only improved his three-point shooting by 40 percent, but also learned that being naturally talented is not enough to be the best basketball player in the world!

American football coach Vince Lombardi once said, "Winning isn't everything. It's the only thing."

How you interpret this statement will determine the way you lead your life. If you want to be a champion, you must learn to _only_ celebrate and remember the positive experiences — and let go of the negative ones.

At school, we're taught to revisit our mistakes to improve, but by doing so we take our correct answers or good performances for granted.

Don't dwell on your failures or losses, but look at your accomplishments and cherish them. See where you are at, and visualize where you still want to go.

### 5. The courage to create your own reality will get you through the down times. 

Confidence is the key to success. The self-image that you construct for yourself will likely determine what you become in life.

In this process, your parents, teachers and coaches play an essential role. While such role models can help you find your motivation, eventually you will need to find inner strength on your own.

The inability to get motivated is what stops most people from becoming exceptional or maintaining top performance, on or off the court.

Once improvement stops, whether with sport, at work or in our daily lives, it's easy to become disenchanted about the game, about life itself!

Some people even "burn out" and start to hate what they once loved. When this happens, there are two options. Either you can renew your lost passion by remembering what you first loved about your role or situation, or choose to change your situation entirely. 

What's most important is that you bring enthusiasm and passion everyday to whatever you decide to do!

For example, we tend to hesitate before leaving a job, a relationship or a situation as we cling too tightly to being "realistic" or safe. When really, we should be thinking passionately by _creating_ our own reality!

Don't let yourself be scared by a new challenge. Instead, prepare to test your limits and create your own reality.

For instance, if you're struggling in a tough job market, make a change by walking up to the company of your dreams and offering to work as an unpaid intern for two months.

But what if you're afraid of running out of luck? That's perfectly natural. Find out how to banish your fear of failure in the final blink.

### 6. If you feel like a loser, you’ll lose. If you think like a winner, you’ll win. Transform your habits today! 

Nobody likes to lose. Unfortunately, if you lose often, you can fall under the spell of _learned helplessness._

You might even stop trying to improve as you're convinced yourself that failure is your destiny. The truth is, it's easier to reject any sign of hope than it is to change what's setting you back.

If this sounds like you, then it's time to remove the mental barriers sabotaging your life. Once you do so, you'll begin treating failure as a simple accident that can be easily corrected.

So where do you start?

First, identify the things that trigger habits and bad behaviors.

Say you're trying to get in shape, but after an eight-hour shift, you can't wait to kick back with a drink on the couch at home. Before you know it, you'll be snacking in front of the TV and be too tired to exercise.

What happened? You've let your habits create bad behaviors that get in the way of your goals.

After recognizing vicious cycles like these, you need to transform them into _virtuous cycles_.

Build a step-by-step, rigorous plan to make your habits work for you, not against you! If you want to stick to an exercise routine, for example, ask friends to remind you of your goal.

If you have a roommate or partner, ask them to not offer you wine or ice cream after dinner; if you live alone, don't keep alcohol or snacks in the house, to avoid indulging.

Soon you'll realize that you can make any changes you want in your life. With a positive attitude, continuous hard work and attention to process goals, you'll be on your way to becoming a champion!

### 7. Final summary 

The key message in this book:

**Success is all about an optimistic mind-set. By establishing goals, building confidence and facing your fears, you'll be able to take your performance to the next level, whether in business or on the playing field.**

Actionable advice:

**Keep a positivity journal.**

Next time you feel down because you didn't perform as well as you hoped, write down all the positive things you accomplish every day. After a week, read these positive notes and start the new week with these thoughts in mind. This way, you can reinforce positivity and make it a part of your subconscious mind.

**Suggested further reading:** ** _Bounce_** **by Matthew Syed**

In _Bounce_, Matthew Syed explores the origins of outstanding achievements in fields like sports, mathematics and music. He argues that it is intensive training, not natural ability that determines our success, and people who attribute great performances to natural gifts will probably miss their own chance to succeed due to lack of practice.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dr. Bob Rotella and Bob Cullen

Dr. Bob Rotella was the director of sports psychology at the University of Virginia for 20 years. He has coached successful players from 70 major championships in men's, women's and senior professional golf, as well as other star athletes in tennis, baseball, basketball and football.

Bob Cullen is a journalist and writer who has for years collaborated with Dr. Bob Rotella.

