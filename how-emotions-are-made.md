---
id: 59e4adfdb238e1000617b414
slug: how-emotions-are-made-en
published_date: 2017-10-20T00:00:00.000+00:00
author: Lisa Feldman Barrett, PhD
title: How Emotions Are Made
subtitle: The Secret Life of the Brain
main_color: E45F8F
text_color: A64568
---

# How Emotions Are Made

_The Secret Life of the Brain_

**Lisa Feldman Barrett, PhD**

_How Emotions Are Made_ (2017) challenges everything you think you know about emotions. From learning how our brain registers anger, fear and joy to how we think about these emotions culturally, you'll come away with a new understanding of the ways in which emotions are created and how their scope is determined by society at large.

---
### 1. What’s in it for me? Learn how the brain and culture construct emotions. 

We're encouraged to think of our emotions as almost elemental forces that emerge from us fully formed. Think of the 2015 Pixar film, _Inside Out_, which featured a little girl's emotions as cartoon characters like Fear — a cringing purple nerd — and Joy — a sunshine-colored exuberant woman, both of whom lived in her head and influenced what she did. Most of us think about our feelings this way; as distinct and sometimes overwhelming sentiments to be encouraged or mastered.

But we're wrong. Emotions are constructed by us, by our brains and by culture — a reality that's far more nuanced and complex than our standard depictions of them. These blinks will guide you through the true story of our emotions.

In these blinks, you'll learn

  * about the classical, prevalent, and most probably false view of emotions;

  * why tech companies' investment in emotion-recognition systems is pointless; and

  * how Tahitian culture describes "sadness."

### 2. The prevailing, classical view of emotions envisages them as hardwired into our brains. 

How hard is it to control your emotions? The consensus is that you may try to, but you simply can't.

The notion of emotions as reflexes — sometimes artifacts of evolution existing in a realm beyond the rational — has been around for millennia. It's called the _classical view_, and it's been espoused by everyone from Aristotle, Buddha, Darwin, Descartes and Freud, right through to modern-day thinkers such as Steven Pinker, Paul Ekman, and the Dalai Lama.

This passive take on emotions is taught in psychology textbooks, and it's reflected in the way the media discuss them. The classical view also sees emotions as _universal_. It's assumed that emotions are hardwired and are automatically triggered in distinct regions of the brain.

It's thought that there's a set of emotions that can be found across all humanity, and that each of these has an underlying property or "essence." This concept is called _essentialism._ It assumes that each one of us is not only equally emotionally expressive but also capable of automatically recognizing the same emotions in others.

It's as if the brain were pre-wired with neurons for specific emotions. Once neurons are triggered, they produce physical responses. These characteristics are known as _fingerprints,_ and it's through these that emotions are identified.

Imagine you have an annoying colleague. He will trigger "anger neurons," which will consequently cause your blood to boil and your mouth to form a scowl.

Or maybe a friend dies. In this case "sadness neurons" send off signals which might start you crying.

### 3. Scientific evidence doesn’t support the classical view of emotion and money is being wasted researching it. 

Think about it: Do you always make a "sad" face when you're upset? Or an "angry" face when you're raging? Of course not. Each emotion can be expressed in many different ways.

These varied responses to emotional stimuli undermine the classical view of emotion.

There's no single distinct articulation of sadness. There's an entire vocabulary. And the same can be said for awe, gratitude or happiness. Every response is specific to the situation.

Experiments have shown that there is no single brain region dedicated to any specific emotion, or, in fact, to emotion alone. This also means that the fingerprint theory in the classical view doesn't work either.

These were the author's findings at her Interdisciplinary Affective Science Laboratory. The neuroscientists there analyzed brain-imaging undertaken between 1990 and 2011. They divided the human brain into tiny virtual cubes like 3D pixels, and computed the probability of increased brain activation within each cube, while the patient experienced fear, sadness, anger or happiness.

They found that each "emotion" region of the brain increased in activity during non-emotional thoughts and perceptions, too. So they showed that though there are expressive patterns to emotion within society, there is no _single_ and _obligatory_ response.

In short, just because someone smiles, it doesn't mean she's necessarily happy.

In spite of this, science and tech industries continue to waste money in the field. They're trying to identify emotions by physical signs like facial muscle movements, body changes and brain signals.

Or consider _SPOT_ (Screening Passengers by Observation Techniques). This was a "technique" used by Transportation Security Administration (TSA) agents in 2007. The idea was to "spot" suspicious potential terrorists based on facial and body movements. The results were a flop and it cost American taxpayers $900 million.

### 4. Emotions are created spontaneously and concurrently and are based on individual experiences. 

It might seem counterintuitive to imagine emotions as responses that are _not_ natural, innate, or involuntarily triggered, but that's what the author advocates. She has no truck with the classical view and much prefers what's been called the _theory of constructed emotion_.

This theory states that emotions are created spontaneously and concurrently in several areas in the brain.

Each emotion, so the theory goes, is grounded in a given individual's experience. Every response is predicated on anticipated sensory inputs, whether it's for vision, hearing or taste. The brain uses each input to either affirm or alter its predictions.

The author argues that all emotional responses are created similarly. Prior experience and sensory inputs guide action.

For instance, you might experience a spectrum of anger responses. Each comes with its own neural patterns as well as bodily changes and movements. The brain can generate any of these responses. It has selection mechanisms that determine which anger response fits the situation best.

Sometimes, you might scowl. Another time, you'll smirk a little as you plot your revenge. Maybe you'll shout, or quiet down. Variation is normal.

And it's normal because there are a wide range of responses, all stimulated as their context demands.

The author thinks this new theory of emotions works in the same way that Charles Darwin's theory of evolution undermined the concept of "biological essences." People who believed in the "biological essence" of species saw each one as being fixed and having a specific set of attributes. Thanks to Darwin, we know now that species are in fact populations of individuals, each varying subtly depending on its environment.

For the author, emotions are the same as Darwin's species. Our emotions are not innate or fixed; instead, we construct emotions as architects of our own experience.

### 5. We have a predictive system for all the body’s goings-on, including our emotions. 

You know how it is when you're driving a car. If you've driven a lot, your brain will make your body shift gears and engage the clutch without much thought.

But what is it that allows you to act with such muscle memory?

This "autopilot" system is called _interoception_. It's the way the brain keeps your body — with its hormones, immune system and nervous system — running smoothly. It's managing an ongoing and predictive system so that you don't have to be conscious of what's going on at all times.

Interoception is one of the core components involved in emotion creation. The brain's interoception system is continuously processing internal and external sensations. This raw data is repurposed as emotion by the process of interoception.

That's all well and good, but how is interoception experienced?

Well, there are two basic spectrums, each covering two _affects_. Affects are aspects of consciousness. They're always there, whether or not the brain is currently using them as building blocks for emotions, thoughts or perceptions.

The first spectrum covers the affects of pleasure and displeasure. The second covers agitation and calmness.

A real-world situation will help. Imagine the pleasantness of the sun warming your skin or remember an uncomfortable stomachache. You'll experience strong _affective_ feelings, but they're not actual emotions _per se_. They don't in and of themselves make you happy or sad, respectively.

Scientists believe affects are innate. Babies, for instance, perceive the affects of pleasure and displeasure from birth. This, in turn, results in wailing and crying.

### 6. Our interoception system regulates our body budget, determining how our bodies’ resources are spent. 

Several parts of the brain operate in tandem to implement interoception. The author calls these areas the _interoceptive network_, and it contains two specific components.

Firstly, there's the _body-budgeting region_. This uses past experiences to gauge what the body requires, and then sends pre-emptive instructions through the body to control its internal environment. It might, for instance, tell your heart and respiratory system to speed up or issue instructions to metabolize more glucose because you're about to break your jogging record.

Secondly, there's the _primary interoceptive cortex_. This component represents internal sensations such as the pounding you feel when your heart beats.

Both the body-budgeting regions and the primary interoceptive cortex form a feedback loop that helps regulate the _body budget_ itself. Your body budget is responsible for controlling your body's resources such as glucose, cortisol, and heart rate.

It's from the state of your body budget that your emotions are stimulated.

No matter what you're doing, your body is using up resources, even if you're lying on the sofa while your organs get on with their jobs. You could be metabolizing, thinking hard, or running — it all counts. You replenish resources by eating, drinking, and sleeping. You can also reduce the rate of your body's expenditure by relaxing or even having sex.

You even use up resources thanks to your imagination. Say your boss walks by you at work. While she may do nothing more than pass by, if this stresses you out, your interoception system will tell your body budget that it requires more energy.

When trying to handle all the signals it's receiving, sometimes your body budget can become unbalanced, which you might experience through an agitated affect. Often, such feelings are due to a body's lack of resources in a certain situation.

It's then that your brain tries to explain the imbalance; by firing off emotions. Say you're worked up, or, to use the language of affects, displeased and aroused. You might associate that sensation with fear.

### 7. Emotion concepts are culturally constructed beliefs about emotions. 

How would you describe sadness? Curiously, in Tahitian there's no way to describe this emotion. Instead, Tahitians use a word meaning something like "the kind of fatigue associated with the flu."

What does this indicate? It shows that our reality is organized by the concepts we use for understanding our environment. In turn, these concepts depend on culture.

Consider the frivolous yet illustrative muffin–cupcake debate. What is the difference between the two baked goods? At heart, there's no chemical difference between them. They're both the same shape and made with essentially the same ingredients. You can even add similar things, like nuts or bananas, to them.

The difference is cultural. One is eaten as dessert, the other at breakfast. This is an example of social reality, where objects are infused with meaning and function through social agreement.

Examples of social agreement are countless. Just think of paper money, which has no inherent value. We have simply agreed that different colored and sized papers equal 20 Haitian Gourdes, one US dollar or 500 UAE dirhams.

The point here is that _emotion concepts_ are also formed by cultural convention. Once we know the concept, we'll experience the emotion.

Just think of the history of smiling. Nowadays, our emotion concept associates happiness with smiling. But that wasn't always the case. The author claims that the ancient Greeks and Romans had no word for "smile". It just wasn't a significant cultural gesture associated with happiness. In fact, smiling only came into vogue in the eighteenth century once wider access to dentistry became possible. The smile was, properly speaking, an invention of the Middle Ages.

On the other hand, no doubt the Romans had other highly significant cultural gestures that would mean nothing to us now.

### 8. We learn culturally laden emotion concepts from birth, but we have the power to learn more. 

Sad, happy, angry, disappointed, depressed: these aren't universal emotions. They are, the author claims, concepts that we start learning from the moment we're born, from our parents and from society.

That's not to say babies don't have feelings ­– those are tied to the concept of affects, as we've already seen.

On the other hand, emotion concepts are taught, and often explicitly.

Imagine a baby screaming. A parent might respond by asking it, "Are you _angry_ that it's nap time?" or "Are you _sad_ that mommy is leaving for work?"

The questions here inherently link crying to anger or sadness. And it doesn't stop at childhood either. That's because our brains retain the capacity to combine new experiences with previous ones, to learn new concepts and reshape old ones.

This learning process isn't a bad thing. It means we become better at distinguishing between emotion concepts, which in turn means it's easier to regulate them.

Until recently, for example, there was no English word to describe the emotion of feeling pleasure at someone else's misfortune. It wasn't an alien emotion; there just wasn't an easy way to express it. English speakers had to import the German word _Schadenfreude_ to do the task. If we hear a new expression often, it's more likely that we'll identify it more in our own experience, thus making it easier to "feel" more frequently.

With practice, we can also better distinguish between emotions such as, for example, distress and discomfort. We know as adults that the pain of dead leg won't last that long, but once it might have been terrifying to us.

So, emotions are constructed from experience. When we invest in cultivating new experiences, these will, in turn, become the emotional seeds of our future.

### 9. Final summary 

The key message in this book:

**The brain creates emotions through a complex system that also regulates the body's energy levels and expenditure. What we often think of as an inborn "emotion" is, in reality, a construction made by the relationship between culture, the brain, and our interpretations of our bodily sensations.**

Actionable advice:

**Lay down your weary head**

Next time you're feeling depressed, take a nap or go for a walk. Those activities will help rebalance your body budget. You'll feel restored, and this will mean you'll be able to expend energy in a way that will help reframe your mind's focus, and, with it, your emotions.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Emotions Revealed_** **by Paul Ekman**

_Emotions Revealed_ (2003) puts emotions under the microscope, revealing where they come from and how to recognize them, whether they're yours or someone else's. If you've ever wanted to know if someone was being dishonest or trying to deceive you with a friendly smile, these are the blinks for you!
---

### Lisa Feldman Barrett, PhD

Lisa Feldman Barrett is University Distinguished Professor of Psychology at Northeastern University. She had also holds appointments at Harvard Medical School and Massachusetts General Hospital. Barrett received the National Institutes of Health Director's Pioneer Award for her research on emotions in the brain and has published over 200 peer-reviewed, scientific papers that have appeared in _Science_, _Nature_, _Neuroscience_ and other top psychology and cognitive neuroscience journals.

