---
id: 5444aeb46161330008000000
slug: how-google-works-en
published_date: 2014-10-20T00:00:00.000+00:00
author: Eric Schmidt and Jonathan Rosenberg
title: How Google Works
subtitle: None
main_color: 5086CF
text_color: 325482
---

# How Google Works

_None_

**Eric Schmidt and Jonathan Rosenberg**

_How Google Works_ shares business insights from one of the most successful technology start-ups in history. Written by the former top executives at the company, the book lays out, step by step, Google's path to success; a roadmap that your company can follow, too.

---
### 1. What’s in it for me? Learn Google’s secrets to foster a culture of innovation at your own company. 

_Google_ is one of the most successful technology companies in the world, notable especially for its rapid rise to the top. From its founding in 1998, Google quickly become a global leader in internet search and has since expanded into other key technology areas, such as email, mobile technology and social networking.

Although now more than 15 years old, Google still hasn't lost its start-up mentality. _How Google Works_ shows how Google's leadership — founders Larry Page and Sergey Brin, CEO Eric Schmidt and Head of Products, Jonathan Rosenberg — created a corporate culture that has attracted top engineers, employees who work tirelessly to develop innovative products, year after year.

In these blinks, you'll learn about Google's focus on hiring _smart creative_ s — employees who combine technical know-how with a curious and ambitious personality. Managing headstrong creatives, not known for following standard marching orders, isn't easy; but the results — as Google's success proves — are worth it.

In the following blinks, you'll also discover:

  * how to fix a thorny technical problem just by putting a note up on the wall;

  * why making one decision can take up to six weeks, and why that's a good thing; and

  * why giving employees time to goof off might help your company create the next _Gmail._

### 2. Great products developed by phenomenal employees are crucial for business success. 

Over the past few decades, sweeping technological advancements have totally changed our lives and our world. Today, we can store limitless amounts of information online and effortlessly connect to the internet through practically any mobile device.

In the business world, these innovations have shifted everyone's attention to product development. This has happened in two ways.

First, the internet has pushed consumers to demand better products by giving them more information and more choice. If customers don't like your app, for example, they can simply download another.

In this new consumer market, winning and retaining customers is all about having the best possible product.

Second, technological advances have changed the business world by enabling companies to develop products quickly and cheaply. Even a small team of engineers, for instance, can develop a new product in mere months and then release it to millions of people online for free.

Because of these developments, good products now trump fancy marketing strategies. And this crucial insight has been Google's guiding principle since its very beginnings.

Google's founders from the start concentrated solely on developing the greatest search engine possible, confident that if they succeeded, money would follow. (And it did!)

Google's search engine algorithm is an example of the kind of game-changing product that can only be created by skilled employees called _smart creatives_.

_Smart creatives_ combine conventional business savvy and technical expertise with a creative sensibility. They are competitive, ambitious and curious — the kind of people who will pull all-nighters to solve a thorny problem or even ignore your direct orders just to prove a point.

Smart creatives are the very essence of Google, and in fact, Google founders Larry Page and Sergey Brin are exemplary specimens of this sort of worker.

They've approached hiring simply by bringing in the very best engineers and giving them as much freedom as possible to develop amazing products.

### 3. Make hiring and retaining smart creatives a top priority in your company. 

As we've seen, smart creatives are the key to developing great products. So how can your business attract employees that fit this description?

To start, make hiring one of your top priorities. In most organizations, hiring new employees falls to the supervising manager. Yet the opinions of just one person are highly subjective. Focus instead on creating a hiring _committee_ to represent your company's different viewpoints and roles (for example: an engineer, a salesperson and a manager).

This is exactly how hiring happens at Google. Committees review candidates against hard data and supporting materials such as interview reports, compensation history, resume and references.

Although all these supporting documents are valuable, hiring smart creatives also requires imagination. Looking at a resume and assessing a candidate based on experience isn't how you find the most interesting, intelligent and adaptable employees.

In addition to asking standard interview questions, ask candidates about their interests and passions. You should also challenge candidates and catch them off-guard with surprising questions — like asking how they paid for college.

Along these lines, former Google CEO Eric Schmidt asks _himself_ one question before making any hiring decision: If he were stuck at an airport with the prospective employee, would they be able to hold an interesting conversation?

And once you've hired a smart creative, what can you do to keep them at your company?

This question is important as smart creatives are restless and ambitious by nature. At some point, they may want to leave your company and try something new.

To prevent this from happening too soon, find ways to challenge and engage your employee's intellect. That way, they'll feel stimulated and motivated, which will suppress their urge to leave.

For example, when a top engineer wanted to leave Google, Schmidt convinced him to stay by inviting him to participate in top meetings with founders. Attending these meetings allowed the engineer to learn new things about the business, and as a result, he ended up staying at Google for two more years.

> _"The most important skill any business person can develop is interviewing._ "

### 4. Build a creative, free-thinking company culture that attracts excellence-driven employees. 

What can you do to make your company more attractive to smart creatives? The answer lies in your company's _culture_ — that is, the values on which your company is built.

So what kind of culture attracts smart creatives? Such employees thrive in environments that encourage creativity. Here are the three key aspects of a creative company culture:

  1. Interaction among colleagues is easy.

  2. Employees feel free to speak their own mind.

  3. Employees can make decisions independently.

The third point is crucial: You have to find a way to give your employees the freedom to make their own decisions. If you can't do that, then at least make sure you have a good reason when you say "no."

Defining your values is another important aspect of creating an appealing company culture. For example, in 2004, Google's founders created a document outlining the company's guiding principles. These included phrases like, "Don't be evil," and "Make the world a better place."

Taking these steps will help you create an environment to attract top talent, but it won't discipline or motivate employees. It also won't transform regular employees into smart creatives.

What it will do, instead, is attract the kind of people who are drawn to a certain company culture, rather than the kind of people who are only seduced by a big paycheck. And finding people who are motivated to work hard regardless of how much they're paid will certainly help your company succeed. 

For example, in 2002, founder Larry Page saw some ads he didn't like on a Google search page. He printed the web page and posted it on a bulletin board in the company kitchen with the caption, "THESE ADS SUCK."

A team of engineers saw Page's note on a Friday. And although the team wasn't officially responsible for ads, they were inspired by the problem and worked on it — during their own free time — over the weekend. By the following Monday, they had found a solution.

It's not that Google itself inspires this level of devotion. Rather, the company's culture attracts people who are intrinsically motivated and dedicated workers.

### 5. To prepare for unexpected challenges, create a strategic foundation that leaves room for change. 

Most CEOs may think their company needs a business plan, but they're wrong. In fact, most traditional plans inevitably lead to failure.

That's because a typical plan doesn't leave room for change. And yet, running a business requires that you respond to new challenges whenever they arise.

Let's say that you've set out a long-term strategy for your technology business. What if a new technology or competitor emerges, requiring you to react quickly? You won't be nearly as responsive if you're wedded to a fixed plan.

That's why in terms of strategy, you'll be better prepared for the unexpected if you create a strategic _foundation,_ rather than a static plan.

Here's the distinction: a plan is a step-by-step guide for running your business; a foundation is an outline of guiding principles.

For example, when Jonathan Rosenberg wrote the strategic foundation for Google in 2002, he initially planned to deliver a typical, MBA-style business plan, outlining leverage, distribution, marketing techniques and so on.

Instead, he ended up writing something broader, because he realized that's what would actually help guide his employees.

His foundation had three points for success:

  1. Every new product should be based on a great technical insight: a new way of applying design or technology concepts to either lower the cost or increase the functionality of a product.

  2. Concentrate on creating fast growth on a global scale, as competitors can easily catch up to smaller advantages. The best way to create scale and growth is by developing a _platform_ : a combination of products and services that creates new markets by bringing people together.

  3. Be as open as possible. When you can, share your information with the world.

Establishing this foundation allowed Google's smart creatives to apply their special skills and talents to creating innovative solutions to unexpected problems.

### 6. If you’re leading smart creatives, it’s your job to enforce a lively discussion, not a decision. 

In traditional companies, decision making tends to be hierarchical. Senior management makes the big calls, while everyone else nods in agreement.

However, if you're lucky enough to manage a company staffed by smart creatives, things work differently. The decision-making process becomes as important as the decision itself, because if employees don't support the decision, they simply won't follow it.

For example, one time founder Sergey Brin disagreed over a point with an engineer. Instead of forcing his subordinate to go along with his decision, Brin proposed a compromise: half the team would follow him, and the other half could follow the engineer.

But this still didn't suit the engineer, and in the end, the whole team ended up following the engineer's solution — and not Brin's.

This process is effective because it involves everyone, and ensures that employees will support the final decision.

If you want to foster this kind of decision making in your own workplace, create a way for every opinion and every position to be considered and discussed. As manager, it's up to you to enforce a discussion, not a decision.

At one point, Eric Schmidt, Larry Page and Sergey Brin all disagreed on a new product feature. In the end, none of the founders won out, but by discussing their options, they came up with another, even better solution.

As you can see, good decisions require consensus. But of course, when everyone debates different options, reaching the right decision can take forever.

And yet, timing is important. Important decisions can and should be discussed at length, but it's still critical to use deadlines so that decisions don't arrive so late as to be useless to the company.

For example, when Google was considering a deal with _AOL_ in 2002, Schmidt recognized the gravity of such a decision and called for daily meetings on the issue for six weeks.

This way, his team had enough time to discuss and debate various options, but ensured that a decision still would be made within an appropriate timeframe.

> _"Getting everyone to say yes in a meeting doesn't mean you have agreement, it means you have a bunch of bobbleheads."_

### 7. Smart creatives thrive in open and collaborative working environments. 

Information is a valuable currency. This might explain why so many managers maintain a Scrooge-like mentality when it comes to sharing what they know. But that's simply not an option for those who oversee smart creatives.

When you're managing creatives, you need to have an open approach to information to foster a more creative, collaborative working environment.

At Google, quarterly company reports aren't just presented to the board of directors but they're also given to every employee to examine.

Additionally, the company's intranet, called _Moma_, stores information about every single product under development. It also houses each employee's individual objectives and weekly status reports.

This way, everyone knows what everyone else is doing, making it much easier for employees to collaborate and share knowledge, even across different departments.

But openness isn't just a positive attribute to cultivate on an organizational level, it's also important to encourage employees to be open as well. Make it clear that any discussion will be welcome.

Although most companies tend to be strictly hierarchical, smart creatives work best when they have the opportunity to speak their minds.

Google's founders nurture this tendency in their employees by hosting weekly, company-wide meetings. Before the meetings take place, every employee has the opportunity to submit a question into a database online. The founders make sure to answer every question, and employees can also upvote individual questions to give them priority.

And as a consequence of these pro-openness policies, team members at Google now feel comfortable starting their own conversations with colleagues. One employee, for example, wrote a "user manual" about himself, with advice about how to work with him most effectively.

Another employee set up "office hours," creating time for anyone who wanted to come ask her questions.

> "The most effective leaders today don't hoard information, they share it."

### 8. You can’t artificially enforce innovation, but you can create a climate that encourages it. 

Why did you hire smart creatives in the first place? To innovate, of course. But can you actually force people to be innovative?

Well, many companies do try to artificially enforce innovation, for example by appointing a "Chief Innovation Officer." But this is rarely effective.

In fact, one of Google's employees was previously "Head of Innovation" at _Yahoo_. His job entailed stimulating innovation at the company by organizing presentations targeted at engineers.

Of course, it was a hopeless task. Even the executive's 12-year-old daughter pointed out that wasting an engineer's precious time by lecturing him on innovation wasn't very innovative in itself. So the worker left Yahoo, and got a job at Google instead.

But if there's no way to enforce innovation, how can you be a company that creates innovative, surprising products? Although you can't make your employees be more innovative, you can still follow a few simple tips to create a climate that stimulates innovation.

First, set high goals that challenge your employees. As a rule of thumb, try "_10xing_ " every goal you initially set. For example: Quartz watches are 10 _times_ more precise than even the most accurate mechanical watches, and cost 1/10 as much.

Another way to encourage innovation is by being open to risk and failure. At Google, for example, 70 percent of the company budget goes to core projects, 20 percent to budding ventures, and the final 10 percent to new experiments. That way, even projects that seem risky still get funding, which keeps innovation moving.

And finally, if you really want to have an innovative company, allow your smart creatives to do what you hired them for: be smart and creative.

That's why Google allows every engineer to spend 20 percent of his time at work pursuing whatever they like. In fact, one of the company's most successful products, _Gmail_, was the product of an employee's 20-percent time.

> _"Giving the customer what he wants is less important than giving him what he doesn't yet know he wants."_

### 9. Final summary 

The key message in this book:

**To develop a culture of innovation like Google has at your own company, make it a priority to hire and retain "smart creatives," or uniquely talented and motivated employees. You may have to give up some power or adjust your leadership style, but the results will be worth it.**

Actionable advice:

**Use hiring committees to assess job candidates.**

When you're hiring for a new position, invite representatives from all company departments to take part. Set up a hiring committee to evaluate and discuss each candidate; this will ensure that whomever you hire will naturally have the support of the whole company.

Also, ask decision makers to use their imagination to assess whether prospective employees are smart, curious and creative. You should also make sure interviewers ask each candidate a few unexpected questions about their interests and passions.

**Suggested** **further** **reading:** ** _What Would Google Do?_** **by Jeff Jarvis**

The age of the Internet has dawned, but very few companies seem to understand how profoundly it has changed the business landscape and what they must do to thrive. The most obvious exception? Google. _What Would Google Do?_ endeavors to explain what strategic choices fuel the success of Google and other web 2.0 companies like _Amazon_.
---

### Eric Schmidt and Jonathan Rosenberg

Eric Schmidt is the former CEO of Google, and Jonathan Rosenberg was Google's Head of Products.

