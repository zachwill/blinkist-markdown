---
id: 553dfe163839350007000000
slug: how-i-raised-myself-from-failure-to-success-in-selling-en
published_date: 2015-04-27T00:00:00.000+00:00
author: Frank Bettger
title: How I Raised Myself from Failure to Success in Selling
subtitle: Sales, Trust Building and the Road to Enthusiasm
main_color: F8EF4A
text_color: 5E5B1C
---

# How I Raised Myself from Failure to Success in Selling

_Sales, Trust Building and the Road to Enthusiasm_

**Frank Bettger**

In _How I Raised Myself From Failure to Success in Selling_ (1947), veteran salesman Frank Bettger shares his secrets to sales success. Offering practical advice and proven sales strategies, these blinks will teach you how to win clients and skyrocket your sales career.

---
### 1. What’s in it for me? Become a great salesperson through learning the craft’s key tricks. 

Sure, we all learn from our mistakes, but what if you could spare yourself the pain and learn instead from the failures of others? If you're in sales, these blinks give you the blueprint to do just that.

Based on the ideas of Dale Carnegie's bestseller, _How To Win Friends and Influence People_, the blinks you're about to read reveal the trade secrets of one man who went from failed insurance salesperson to one of the highest-paid salespeople in America.

In the following blinks, you'll also learn

  * what a salesperson can gain from being enthusiastic;

  * how to quickly conquer your fear and move forward; and

  * how important it is to remember a client's name.

### 2. If you feel it, you will be it. Fake being enthusiastic until the real energy kicks in and inspires you. 

Have you ever wondered how salespeople manage to stay so upbeat and positive, even though every day people tell them no? A salesperson probably gets ten rejections for every successful sale.

The secret is _enthusiasm_. And luckily, being enthusiastic is something anyone can learn.

So what's the first step? You simply need to act enthusiastic to eventually be enthusiastic.

Consider Frank Bettger, who before becoming a salesman was a baseball player in the minor leagues. Although Frank was ambitious and eager to join the big leagues, he failed to show that ambition on the field and eventually got fired.

So what exactly was Bettger's problem?

He was paralyzed by his fear of failure. And in trying to hide his fear, he made safe plays and appeared unexcited by the game.

After being fired, Bettger played for a lesser league, taking a huge pay cut in the process. He naturally wasn't enthusiastic about his new position. But he made sure not to let it show. Instead, he forced himself to be enthusiastic about his new team.

Interestingly, Bettger's initial "fake" enthusiasm helped him overcome his fear of failure, allowing him to focus more on the game itself. His playing improved, he inspired his teammates and he was even able to negotiate better pay.

You might be thinking, "Sure, good for Bettger, but how does this help me?" Enthusiasm isn't just for people who play sports for a living; it's also the key to success for salespeople.

Once he retired from baseball, Bettger became an insurance salesman, but after ten months on the job he already wanted to quit. He realized however that his lack of enthusiasm for his new job was holding him back, just as it had when he was playing ball.

Bettger worked hard to be enthusiastic, and took this new attitude to his next sales meeting. As a result, he nailed it and made his very first sale!

So if you act enthusiastic, you actually start feeling enthusiastic. And enthusiasm makes everything easier!

> _"Enthusiasm alone did it; nothing but enthusiasm."_

### 3. It’s not about you! Learn exactly what your clients need by listening generously to their concerns. 

Let's say you sell combs. Do you think you could ever sell a comb to someone who is bald? Probably not, even if it was the most extraordinary comb on the planet.

The fact is, you'll always find it difficult to sell something a client doesn't need.

To find out what your clients _do_ need, however, you should ask questions. A good place to start is to ask questions about the work your client does. For instance, if "X" is your client's business, "How did you get into X?" or "What do you love about X?"

Learning what motivates or moves your client is key to understanding what he or she needs.

Asking questions doesn't only help you learn what your client's needs are, but it also makes your client feel heard and helps build your relationship.

For instance, the author once spoke with a client for over six hours in one sitting! The client was so moved by the author's undivided attention that he bought a policy without hesitation.

Why is this important? Listening to another person both lets you learn what they need and makes them feel significant. Clients can be much more responsive to a generous ear than to a honed sales pitch that really seems more about _you_ than about them.

Be careful to focus on your clients' needs and not your own!

A certain magazine salesman had a terrible time selling magazines to businessmen. The basic problem was that he was confusing his own needs with the needs of his customers. The salesman would tell his prospects why _he_ _wanted_ them to buy a magazine, not why they _needed_ one.

Realizing his error, the salesman changed his pitch to emphasize that his clients were busy men and didn't have time to waste on the irrelevant information in other magazines. What his clients _needed_ was his magazine, written just for businessmen. The result? His sales skyrocketed overnight.

### 4. A “no” isn’t always set in stone. Do some sleuthing to uncover your client’s real reason for objecting. 

So now you know how to ask questions and be enthusiastic. Yet these tips alone won't guarantee you tons of sales or instant success.

Inevitably as a salesperson, you'll face rejection, and you'll have to find a way to overcome the "no." The secret is to find out _precisely_ why your client is rejecting your offer.

This is not always easy, as people won't always admit their true reasons for saying no.

According to the author's own research, only 38 percent of his clients would honestly divulge the reason why they were turning down an offer.

To uncover the truth, you'll have to do some investigating.

After trying unsuccessfully for years to sell to a certain company, the author finally found an in. But overnight, the client got cold feet, saying the company was in the red and there was no way it could afford a new insurance plan.

Luckily, the author knew he needed to dig a little deeper.

He asked the client if money was really the only issue; unsurprisingly, it turned out there was another problem that was causing the client to balk. The client explained that the insurance policy didn't sufficiently take into account his sons' security.

Armed with this new knowledge, the author rewrote the policy, taking into account the client's concerns. And as a result, he sealed the deal.

Another way to overcome rejection is to show your clients why their objections may have no basis in truth. But how do you do this?

A certain wealthy Mr. Lindsay turned down an insurance offer from the author, saying he didn't need a policy, as his vast wealth would allow him to always bail himself out.

Knowing his client's logic to be faulty, the author explained that in the event of Mr. Lindsay's death, his family would be responsible for raising the huge sum needed to cover the necessary inheritance taxes. Realizing this, Mr. Lindsay decided to buy a policy.

### 5. Be informed and stay on top of your industry to build trust; be trustworthy to keep that trust alive. 

So perhaps you've had some sales successes, but how do you earn the trust of your clients and keep them coming back for more? The key is keeping up to speed with the events in your field.

If you stay informed about your products, your industry and the market in general, people will _naturally_ come to trust you.

Early in his career, the author worked in an office with 16 other salesmen, but just two of them did 70 percent of the business in the company.

Realizing that the two most successful salesmen were also continuously consulted by the other salesmen, the author realized that the duo's success was a result of them being the best informed.

The two top salesmen also regularly read the most important industry magazines and newsletters, which is why they were always up to date with the latest news. Their impressive knowledge of the field helped them build strong relationships with clients, which in turn produced a lot of sales.

So how can _you_ build trust with _your_ clients?

Knowledge alone won't earn a person's trust. To gain the trust of your clients, _you_ actually need to be trustworthy.

The author found this out the hard way early in his career, when he exaggerated the premiums of the insurance policy he was selling to earn a client's business.

The client then checked the numbers with the company and uncovered the author's exaggeration. As a result, the author lost a sale.

But this mistake cost the author more than that one sale. It cost him the trust of his client, his colleagues and worst of all, his self-respect.

Learning from this, the author adopted a new motto: "I shall never again want anything I'm not entitled to; it costs too much!"

> _"Know your business and keep on knowing your business."_

### 6. Make it a goal to make your clients feel important. Remember their names and their interests. 

Imagine you're dealing with two different salespeople. One greets you warmly, smiles at you, asks about your family. The other is stiff, unsmiling, and barely remembers your name.

If you're considering the same product at the same price from both of these salespeople, which salesperson would you buy from? Clearly, the one who made you feel important.

So how can you make your clients feel important?

The first rule of thumb is simple. Start by remembering your clients' names!

While remembering names can be challenging, you'll find doing so much easier if you use the tools of _impression_ and _repetition._

If you don't catch a client's name at first, have them repeat it. If it's a name you're not familiar with, ask your client to spell it out. Whatever you do, ensure you get a clear _impression_ of your client's name.

While _impression_ can help you establish a mental picture of a name, _repetition_ is necessary for the name to stick in your memory.

After hearing a name, it is essential that you repeat it within ten seconds. An easy way to do this is to make a habit of repeating the name in your response, such as "How do you do, Mr. Musgrave?"

By using these tips, you can set a strong foundation that not only makes your client feel important but also increases their willingness to do business with you.

Doesn't this seem simple?

Yet your job isn't over when you make a sale. By staying in touch with your client afterward, you can ensure a longstanding relationship, and may even find your customers selling your product for you.

For example, one successful refrigerator salesman makes a point of giving customers a follow-up call after a new purchase. His customers are often so pleased with their purchase and his diligent customer service that they tell friends about the experience, thereby creating new potential clients.

The lesson? If you take care of your clients, they will likely return the favor and take care of you!

### 7. Gain control by making the sale before the sale. And speak up honestly when you’re feeling afraid. 

Have you ever heard someone talk about "making the sale before the sale?" This key to sales success refers to scheduling appointments — a more important task than you might think.

While it's tempting to start selling to a client as soon as you meet them, you'll find it much easier to gain the trust of your client and maintain control of the situation if you start by selling an appointment, rather than your product. Yet what exactly does this mean?

For starters, by scheduling a meeting, you ensure that your client is free from other obligations and can therefore devote time and attention to you.

Setting an appointment also shows respect for your client's busy schedule by letting them know you value their time, and tells them you're serious by showing them that you value your own time, too.

Try it out! People are naturally more responsive to a request for ten minutes of their time than to a sales pitch being shoved in their face.

But securing an appointment with an important client is only half the battle.

Say you show up to your meeting and find your client is more intimidating than you first anticipated. What's the best thing you can do in this situation?

The best thing you can do is admit that you are scared.

Once, while trying to sell to a leading automobile executive, the author became so nervous he could hardly speak. Yet what he did next made all the difference.

He said, "Now that I've finally met you, I am so nervous I can hardly talk."

By voicing his fear, the author managed to overcome it, and even impressed the executive with his honesty.

Yet if the author hadn't spoken up, he probably would have come off as unfriendly or impolite, and that would have caused him to make a poor impression on his client.

### 8. Final summary 

The key message in this book:

**Becoming a successful salesperson is no easy task, but with a little determination anyone can accomplish this goal. As long as you stay enthusiastic about what you do, you can find success in sales. By continuingly developing your skills and making your customer your priority, you can build a sales career of which you can be proud.**

Actionable advice:

**Keep a notebook to help remember client details.**

Start a list of all the personal details your clients share with you, such as their hobbies or passions, their childrens' names and ages, and so on. Consult your list before a meeting with a client to jog your memory. With these details at your fingertips, you'll be able to establish a closer connection, making your clients feel good while increasing your chances of a sale.

**Suggested** **further** **reading:** ** _To Sell Is Human_** **by Daniel Pink**

_To Sell Is Human_ explains how selling has become an important part of almost every job, and equips the reader with tools and techniques to be more effective at persuading others.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Frank Bettger

Frank Bettger started out as a baseball player before moving on to a stellar career in sales. Mentored by famous self-help author Dale Carnegie, Bettger found success by applying his teacher's knowledge the art of selling.

