---
id: 5e6b90b56cee07000645d7ec
slug: how-language-works-en
published_date: 2020-03-17T00:00:00.000+00:00
author: David Crystal
title: How Language Works
subtitle: How Babies Babble, Words Change Meaning, and Languages Live or Die
main_color: F59131
text_color: 8F4D0E
---

# How Language Works

_How Babies Babble, Words Change Meaning, and Languages Live or Die_

**David Crystal**

_How Language Works_ (2005) unlocks the secrets of how and why we communicate. Language is one of the defining characteristics that makes humans human. But because it's such a fundamental concept, we rarely take the time to think about where it comes from or how it evolves. These blinks examine the historical and personal origins of language and the many different ways it affects our daily lives.

---
### 1. What’s in it for me? Uncover the mysterious ways in which language is born, evolves, and even dies! 

It's been said that language is one of the main attributes that sets human beings apart from other species. No other creature on Earth has evolved a means of communication as sophisticated as human speech. But where did it come from? And how do we learn to go from baby talk to complex sentences that can describe everything from the tiniest detail to the most abstract idea? Come to think of it, why are there so many languages out there?

Language is so essential to our lives, it's a wonder why we know so little about it. It defines us both as a society and as individuals. You can find out so much about a person by the way they speak and write.

These blinks help you understand what language is, how we use it, and how it defines us. They explore the intricate structure of human language, the steps we all take to learn how to use it, and how language is constantly evolving with the ever-changing world.

In these blinks, you'll discover

  * why teenagers can't always understand sportscasters;

  * how palm trees helped linguists define an ancient language; and

  * what we can do to protect dying languages.

### 2. Language is an organized system of communication focused on productivity and duality of structure. 

These blinks are all about language. But what does that mean? What exactly is language? Well, for most of us, language can mean many different things.

We try to read a person's mood using "body language." We teach our kids not to use "bad language." If we want to agree with a good idea, sometimes we'll say, "Now you're talking my language!"

But when professional linguists discuss language, they mean something very specific.

**The key message here is: Language is an organized system of communication focused on productivity and duality of structure.**

Now, the terms _productivity_ and _duality of structure_ are pretty abstract. It's hard to get an idea of what they actually mean. So let's break them down, starting with productivity.

Productivity means that a language can be built upon and used to express an infinite number of ideas. Just think about how you can build a sentence. It's possible to create a sentence that goes on forever. All you have to do is simply keep using the word "and." This is an example of productivity in action.

Or consider another example. We can take the building blocks of a language — it's words, phrases and sentences — and use them to create new words which express new inventions and concepts. Every year, the world's leading dictionaries make international news by formally inducting new words and phrases, like "traumatology" and "gig economy" into the lexicon. And every day, we create unique combinations of words to form sentences that express our thoughts, ideas, and emotions. The majority of these sentences combine words in ways they've never been used before and never will again. That's pretty remarkable!

So, that's productivity. Now what about duality of structure?

Duality of structure breaks language down into smaller building blocks. All languages can be broken down into one of two categories. The first is the meaningful elements of individual words, and the second is the distinctive but meaningless elements of individual sounds.

Take the letters "G" "E" and "T". Individually, the letters have no meaning. But combine them together and you've created the word "get," a word with a clear meaning. This is the duality of structure, and it is unique to human language. The communication we see between animals does not have duality of structure. A dog can't build a sentence or even a word out of multiple barks.

So, that's what language is. In the next blink, we'll look at how we use it to communicate.

### 3. Language is primarily expressed through speech and writing. 

So, now we have an idea of what language actually is. But how is it expressed? How do we use language to communicate with each other?

**The key message in this blink is: Language is primarily expressed through speech and writing.**

Let's start with speech. Of the forms of language, speech is considered primary. It's a form that transcends all cultural boundaries. It's used by all human societies, no matter how remote.

Speech is so important to us that the human body has actually evolved to help us speak. Our ears and brains, for example, are hard-wired to recognize speech patterns from any surrounding noise. Interestingly, the human body's adaptation for speech comes at the expense of other abilities. The larynx, for example, is positioned higher up in humans compared to other animals. This helps us facilitate speech, but it also leaves us vulnerable to choking on food.

Now, what about writing? As opposed to speech, writing and, by extension, reading are not natural processes. They are learned skills that first needed to be invented, then taught. And because they're invented, our bodies haven't evolved to suit them. The human eye has not evolved to process written language in the same way that the human ear has adapted itself to speech.

Despite it being the natural form of language, speech has historically been looked down upon. For centuries, linguists felt that everyday speech was careless and disorganized. The written word, on the other hand, provided authority and style. And because writing gave language permanence, the rules of grammar were structured around it.

These grammatical rules then jumped from written to spoken language. They started affecting how people spoke. Take this example: it used to be natural for people to drop their g's from words like "walking" and "talking" -- which became "walkin'" and "talkin'" But as literacy became widespread and more people became aware of how these words were spelled, dropping the g became a sign of poor education. In other words, because your speech didn't match how words were written, you didn't know how to speak "properly."

Today, spoken and written languages are viewed as equal, alternative forms. Each one is seen worthy of specialized study. Writing is permanent, visual, and it tends to be more formal. Speech, on the other hand, is dynamic and, more often than not, spontaneous.

### 4. The basic rules of language are learned at a very young age and are built upon for the rest of our lives. 

When we start to pick apart a language to study its rules, it can seem impossibly complicated. Maybe you remember struggling to analyze sentences in grammar class. But — even without a grammar class -- we've all learned these fundamental rules. Just speak to almost anyone. From a pretty early age, we can all carry on a coherent conversation.

How does this happen?

**The key message here is: The basic rules of language are learned at a very young age and are built upon for the rest of our lives.**

Babies start learning language from very early on. They begin with phonetic sounds like "ba." It's no small thing. English alone has over 40 individual vowel and consonant sounds and about 300 ways they can be combined. As a child's baby talk becomes more emotive, the parent responds appropriately, using different tones when the baby coos or laughs or cries. This helps the baby to distinguish between an even wider range of sounds.

Baby talk sounds cute, but most of us don't think it actually means anything. We're wrong. Research has shown these sounds aren't random at all. The baby's brain is producing a very small set of sounds that are essentially practice for what will become speech. You can even hear this in progress. At around nine months, parents begin to hear a rhythm to these sounds. They will hear different tones and pitches that they will often ascribe intention and meaning.

As children develop, their hearing and cognitive abilities outpace their speech. By the time they're a year old, they may be able to recognize dozens of words but can still only pronounce a few basic sounds.

Most skills in hearing and cognitive ability develop rapidly over the first few years. But some take much longer. The skill that seems to take the longest to fully develop is intonation. The basics are there early on. You can hear a toddler say a single word like "mommy" with a range of different intents: as a question, as a demand, or even in distress. But learning the entire range of subtle meanings that are possible in a statement is a process that often continues into our teens. An adult listening to a sportscaster will be able to accurately say who won a game based only on how the first half of the score is read. Children and even young teenagers have difficulty making such fine distinctions.

### 5. From the moment of its creation, language has been in a constant state of evolution. 

One of the most common misconceptions about language is that it's fixed, or that it's structured around a strict set of rules. We get this idea from the so-called "grammar police," who pounce on every little mistake like it's the downfall of civilization. In reality, nothing could be further from the truth. Our languages are constantly changing. If they don't, they die.

**The key message here is: From the moment of its creation, language has been in a constant state of evolution.**

It's impossible to pinpoint the exact origins of language. There are a number of theories. Some suggest that early humans started imitating sounds of animals. Others think that words developed based on instinctive sounds reacting to pain or surprise.

However it began, it's clear that language began changing almost immediately. The question of how it changes is difficult to answer. For one thing, the changes don't happen all at once. It takes time for a new word or pronunciation to spread and enter common speech. And while we can always identify a change after it's happened, it's almost impossible to catch it in progress. A historical dictionary will give you an approximate date for when a new word was created. But that's based on its first written appearance. Its first spoken appearance always took place at some unknown point prior to that.

While it's tricky to examine how a language changes, it's easier to understand _why_ it does. 

There are often practical reasons for linguistic change: the invention of new objects and ideas requires new words to describe them. We see this everywhere from science and technology to advertising and marketing. Just think of relatively new words like "digitize" and "clickbait." 

There can also be social reasons for linguistic change. Individuals will mimic the speech, either consciously or subconsciously, of people they look up to. It's very common for people to change their social standing by attempting to lose an accent or dialect they believe is perceived as lower class.

Perhaps the biggest cause of linguistic change is distance. As people began to explore the world and move further away from each other, their language diverged to create new words for their new experiences. And as they came into contact with new people with different languages, those sounds and words would have an influence on each other. This can be seen clearly in the wide variety and number of borrowed words in our language. English alone has absorbed words from over 350 different languages.

### 6. Language families are valuable tools that provide insight into the earliest days of humankind. 

We've talked a lot about language, but we haven't really discussed where it came from. Which was the first language? And did all languages develop from the same source?

The questions of where language comes from and why it evolves aren't just academic ones. Exploring these issues can help us answer why there are so many languages in the first place. To better understand questions like these, historical linguists have classified the world's languages into what are called _language families_. Linguists systematically compare and contrast different languages to determine how, or if, they are related and to find a potential common source or _parent language_.

**The key message in this blink is: Language families are valuable tools that provide insight into the earliest days of humankind.**

For example, English belongs to the Germanic branch of the Indo-European family. The parent language, Proto-Indo-European, is believed to have been spoken before the year 3000 BC.

The exact location of its birth is the subject of some debate, but linguists can make educated guesses. They can point to the fact that this language had no word for "palm tree" to argue that the origin was in north-central Europe rather than the Mediterranean. But there also seems to be no common word for "oak," which is a common European tree. So maybe its origins were closer to Asia. Wherever it started, the language has spread far and wide. Over the millennia, thanks to migration and colonization, this language developed a number of branches, including Greek, Celtic, Indo-Iranian, and Italic or Latin. 

But how exactly do linguists know these languages are connected? Well, they compare similar words and structures in modern languages and work backwards through the centuries. For example, the word for "father" is similar in French, Spanish, Italian and other romance languages, all derived from the Latin word "pater." The connection helps us understand how modern languages develop and how they relate to each other.

Analyzing the historical similarities and differences among language families better enables us to see how languages have and continue to influence one another. But despite this constant give and take, no single "universal" language has emerged. We continue to be a multilingual species.

### 7. Multilingualism is the normal human condition. 

If language is one of the defining characteristics that makes us human, our specific language is one of the key attributes that defines us as individuals. The language we speak carries a lot of information about where we come from, our social status, and even the values and ideals we hold most dear. This is one of the biggest reasons why language remains such a volatile political issue.

But the language — or should we say languages — we speak doesn't define us to just one place or one identity.

**The key message here is: Multilingualism is the normal human condition.**

There are no completely monolingual countries. Even countries that are dominated by a single language, such as the US or Japan, have significant populations that predominantly speak something else. And every country manifests its multilingualism differently. In some, most of the multilingual speakers are found in urban centers. In others, they're in rural areas that don't often come into contact with the larger outside world. But they do have one thing in common: it's unusual to find a country where all languages are in harmony.

When we say someone is "bilingual", we usually take that to mean they are fluent in more than one language. That definition is actually somewhat misleading. While there are certainly individuals who have achieved that, they're the exceptions. Usually someone's dominant language will interfere with the second or third, in pronunciation, accent or vocabulary. Most people achieve a varying degree of proficiency in other languages. For example, it's possible to read or hear another language without mastering the ability to speak it.

Foreign language learning has a number of benefits, from increasing our ability to participate in a global economy to promoting awareness and tolerance of other cultures. People who struggle with learning a foreign language will often claim they simply don't have a natural aptitude for it but there's little proof to back that up. The keys to success are motivation and attitude, assuring that the learning material is personally relevant to the learner, and consistent exposure to the language. That's all.

While the vast number of languages in the world can seem intimidating, they're actually of great benefit. This is why it's so important to preserve languages that are on the verge of extinction. Discover more in the next blink.

### 8. Endangered languages can be revitalized through concerted community efforts. 

A language dies with the last person who speaks it. When we lose a language, we lose a specific cultural identity. We may lose invaluable scientific information related to folk medicine or anthropology. For these and other reasons, it's vitally important to protect and preserve these endangered languages.

**The key message here is: Endangered languages can be revitalized through concerted community efforts.**

Languages are dying out faster today than at any other time in recorded history. It is estimated that about half of the roughly 6,000 languages in the world today have fewer than 100 speakers. All these will probably die out this century. Languages die for various reasons. Isolated communities can be completely wiped out thanks to such causes as famine or natural disasters. But it's more common to see these smaller communities assimilated into a larger one. This can either be voluntary, due to immigration, or not, thanks to colonialism.

Language assimilation appears to be universal across cultures. To begin with, speakers of the minority language experience tremendous pressure to learn the new, dominant language. As their proficiency increases, they find themselves using their original language less and less. When this pressure is passed on to their children, a feeling of shame can be associated with their original language. As time goes by, the original language may only be used among family members, if at all.

So languages are dying out. Is there anything we can do to reverse this trend?

It is possible to bring languages back from the brink of extinction, but it isn't easy. First of all, the community itself has to see the value in preserving the language for future generations. The language has to be documented and analyzed. It requires not just teachers able to pass on the language, but students who are eager and willing to learn it. All of this requires time, money and a great deal of effort.

However, it can be done. In New Zealand, a series of initiatives known as language nests began in 1982 to revitalize the endangered Maori language and introduce it to children under the age of 5. These initiatives have succeeded thanks to support from both the Maori community and the New Zealand government. We can also point to successful efforts to revitalize indigenous languages in North America, Japan and elsewhere.

While we can't predict the long-term success of these revitalized languages, every case has shown that revitalizing the language brings with it a renewed interest and pride in one's indigenous cultural identity. Revitalization also preserves deep-rooted traditions and beliefs that add a unique color to the diverse palette of humanity. The loss of a language is devastating, not just to the communities who spoke it but to all of us.

### 9. Final summary 

The key message in these blinks is

**Language is fundamental to our existence. The human body has even evolved to help us speak. Made up of complex rules and structures, languages provide insight into the way we think, behave and believe. They define us, both as a species and as individuals. We can't afford to take something so fundamental for granted. We have to look after them, preserving them for future generations, while still giving them the space they need to evolve along with us.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Txtng_** **, by David Crystal**

**Language didn't just evolve in the past, and it won't just continue to evolve in the distant future. In many ways, it's evolving right in front of us. Modern technologies have radically reshaped the ways we use language to communicate, and it's continuing to do so today.**

**For a case in point, just grab your smartphone, if it's not already in your hand. Open up your text messaging app, and you'll see an example of one of the most notable ways that language is currently evolving. To find out how the language of texting differs from the language of everyday speech, and to learn how it provides us with a new avenue of human expression, communication, and innovation, check out our blinks to** ** _Txtng_** **, by David Crystal**
---

### David Crystal

David Crystal is one of the world's leading linguistic scholars. He has written or co-written dozens of books on the subject of language, including _The Stories of English_ and _Shakespeare's Words_. In 1995, he was awarded the Order of the British Empire for services to the English language.

