---
id: 53e89b7533383300071d0000
slug: how-much-is-enough-en
published_date: 2014-08-12T00:00:00.000+00:00
author: Robert Skidelsky and Edward Skidelsky
title: How Much is Enough?
subtitle: Money and the Good Life
main_color: 60B498
text_color: 386958
---

# How Much is Enough?

_Money and the Good Life_

**Robert Skidelsky and Edward Skidelsky**

_How_ _Much_ _is_ _Enough_ sets out to critically inform the reader about the moral, historical and economical backgrounds of modern day capitalism, its obsession with accumulating more and more, and its consequences. It also vigorously outlines an ethical alternative to this lifestyle, in which our obsession with "more" is replaced with "the good life."

---
### 1. What’s in it for me? Learn how to escape the rat race in exchange for a happier life. 

One thing is for sure: once in a while, we all envy our neighbors for their fancy car, their expensive vacation trip to the Caribbean, their towering home or their big screen TV. We see what they have that we don't, and think: "I've _got_ to get one of these, or something better if I can!"

But you aren't the only one. Everyone thinks this way — in fact, it's part of being human. But what is it about the fundamental human condition that makes us think this way? According to the authors Robert and Edward Skidelsky, our ceaseless pursuit for more and more things is driven by the _insatiability_ _of_ _wants_, an innate part of our humanity that we simply can't part with.

Today's Western capitalistic societies suffer from a severe perversion: the fundamental desire for more. While once condemned as evil in earlier civilizations, insatiability has now become the _ideal_ that we must strive towards if we want to be considered successful.

Armed with the timeless contemplations of Aristotle and the economic insights of John Maynard Keynes, the Skidelsky duo leads a vigorous, well-informed and intelligent campaign against the ills of modern day capitalism and the love of money and wealth it has continuously fostered in the last 250 years.

These blinks will show you:

  * how Adam Smith became the most influential economist ever,

  * why we could be working closer to 15 hours a week,

  * why states could afford to pay citizens to make them happier, and

  * how greed went from a vice to a virtue.

### 2. Western societies are richer than ever, but that hasn’t translated to less work. 

Imagine you lived in a world in which you spend most of your time doing the things you enjoy, and would work only if you wanted to. Sounds remarkable, doesn't it? Well, that's exactly what famed economist James Maynard Keynes predicted roughly 100 years ago would be happening in our near future.

In an essay in 1928, Keynes predicted that in 100 years Western societies would have obtained so much wealth that no one would have to work more than 3 hours a day.

It's certainly true that we've obtained wealth. In fact, history's technological development has increased work efficiency so much that _all_ our material needs could be easily satisfied without a need for much work.

Indeed, a life of rewarding leisure should _already_ be a possibility for everyone.

Instead, however, most of us still have to work very hard to meet our needs, and struggle to find the time for recreation.

Although Keynes' was accurate in his prediction that we would see an increase in GDP per capita, i.e., the average annual income per household, his forecast of a 15-hour work-week failed spectacularly.

The 15-hour work-week hasn't caught on because there has been an unequal accumulation and distribution of wealth over the last decades, in which wealth is heavily concentrated in the hands of the few, while the masses still have to work 40 hours a week _or_ _more_ just to make ends meet.

Consider, for example, that the richest one percent in the USA receive about 18 percent of the national income.

And even those who _could_ afford to live the life of luxury and leisure often still work well over 40 hours a week in pursuit of greater wealth.

In addition, the continual advancements in work automatization, due to technological innovations from the 20th century, haven't proved to be a blessing for all like Keynes had hoped. In fact, quite the opposite: these innovations helped produce a huge army of underpaid service contractors.

So _why_ is it that Keynes's predictions failed so miserably?

### 3. Contemporary capitalistic societies are stricken by an undying desire to acquire more than we have. 

How often do you replace things like your TV or your car? Do you do it because you _must_? Or because you _want_ to enjoy new technology?

The answer to this question reveals the fate of the 15 hour work week: in our frantic effort to gather more goods, we have to continue working in order to afford them.

That tendency to constantly acquire new goods is known as _the_ _insatiability_ _of_ _wants_. But how does it work, exactly?

Part of the _insatiability_ _of_ _wants_ is rooted in our psychology. Some psychologists argue that our constant want for more is driven by a desire to kill the boredom that's typically felt by those from affluent societies.

Sociologists, on the other hand, argue the reason is the _relativity_ _of_ _wants_. Relativity of wants says that people always want the things they don't have that others have — or, in an effort to distinguish themselves, something that no one else has.

What's more, capitalism acts as fuel for this innate insatiability. Indeed, capitalistic market competition drives firms to produce new products and manufacture want through marketing.

Consequently, you can never have "enough" money under capitalism. Whereas in the past a banker might have bought an estate and then retired to a life of leisure, in a contemporary capitalist society, a banker would likely speculate with the estate and try to expand his fortune by buying more estates, even in his old age.

Moreover, the _monetisation_ _of_ _economy_ has made all goods and services exchangeable for money. In turn, capitalist societies have developed an obsession with money that then amplifies the relativity of wants.

In turn, we value things in terms of cost rather than quality, caring only about what something is worth compared to someone else's goods.

We see this even in our language. We talk, for example, about education as an "investment," rather than as part of the path towards self-edification.

This skewed perspective of money leaves us with one profound question: Where did these capitalistic ideas actually come from?

> _"Capitalism is a two-edged sword."_

### 4. Economic science helped turn avarice from a vice into a virtue. 

Throughout history, discoveries and observations have been made that have totally altered the course of human thought. Consider, for example, Martin Luther's Ninety-Five Theses, or Descartes's "I think, therefore I am." So too has economic thought influenced our societal development.

In fact, science of economics developed in a way that now downplays greed as self-interest.

It may come as a surprise, but long ago working for money was considered a contemptible but necessary evil to sustain oneself and achieve a life of relative comfort.

However, beginning about 500 years ago, as evidenced in the writings of Niccolo Machiavelli, the unebbing pursuit of personal wealth began to gain favor as the best guarantee to both govern people and secure social freedom.

Then, in the late 18th century, Adam Smith famously wrote that humans ought to live out their desire for self-improvement in free, unrestricted competition, believing that free competition would in fact serve public well-being, as if led by an "invisible hand."

Smith is considered the founder of modern economics, and contributed hugely to the shift in our understanding of work. As a result, economics became a discipline that encouraged selfishness as a fundamental virtue for a thriving social life.

Whereas earlier wealth was a necessary evil, suddenly, around the Industrial Revolution, it became an end in itself. This shift in understanding led to many creative — but also a lot of destructive — forces, ultimately forming a _Faustian_ _bargain_ :

The pursuit of wealth during the Industrial Revolution brought numerous important technological innovations. However, it also made the downside of chasing wealth painfully obvious.

Someone's new wealth comes at the expense of others — the wealth accrued in the hands of few was balanced by masses of impoverished workers, poorly paid and bereft of a high quality of life.

Like Faust, who promised to trade his soul to Mephistopheles for infinite wisdom, we made a bargain with the devil by accepting the evil of the selfish struggle for the sake of wealth.

> _"How did we come to… a system in which the love of gain was released from its moral constraints?"_

### 5. We’ve lost our sense for what is “enough.” 

The values extolled by modern economics have not been without consequence. In our quest to acquire wealth, we've forgotten that wealth should be a means to an end.

As we've seen, accumulating wealth and pursuing self-interest was once mainly understood to merely be a means to subsist. However, once we sanctioned it as the fundamental purpose of our existence, it became impossible _not_ to see wealth as an end in itself.

Because all our goals can be seen in relation to what others have or have accomplished, it has become seemingly _impossible_ to give up the pursuit of money. There is always someone with more, and therefore always more to be had. Thus, the struggle to "keep up with the Joneses" continues throughout life.

And when we've thrown the concept of "enough" out the window, then there is nothing to tell us to stop amassing wealth. Why stop at owning three cars, then, if you can have five?

In recent decades, political and economic theories have undermined our chances of conceiving any situation in which we can have enough of anything.

For example, authors like John Rawls based their theories on the autonomy of the individual, i.e., our innate competence in determining our life's outcomes. While the prioritization of individual autonomy has helped us come closer to creating a fair and equal society, it has also allowed us to subjectively determine our own concepts of good, often abhorrently skewing what we may know as innately good to serve our own pursuit of what we may consider wealth. Religiously motivated terrorism and Neonazism, for example, fall in this category.

This philosophical starting point makes it impossible to define what sort of ethical goal "getting more and more" might possibly fulfill.

Consequently, the ethics of pursuing wealth have been reduced entirely to an individual's right to self-determination, serving no other greater purpose. In other words, we are free to pursue as much wealth as possible because "why not?"

Now that you understand the ruinous nature of modern capitalism, these next blinks will help us to organize a way of life that leads to true happiness.

### 6. Contemporary conceptions of happiness result in less of it. 

Today, happiness is often assumed to be the ultimate goal of all our efforts. But do we even really know what "happiness" means?

The ancient Greek word _eudaimonia_, commonly translated as "happiness," was understood not as a subjective state of mind, but rather living admirably and honorably according to objective criteria.

However, the meaning of happiness has been narrowed over time from a socially agreeable way of life to a subjective assessment of how we feel.

This shift has turned avarice into a virtue: happiness is understood as the result of actions that serve the individual's self-interest, or guarantee them a "surplus of pleasure over pain" at all costs, even at the expense of other people's happiness.

Thus, in today's world, there's simply no possibility left to objectively define what happiness could mean. For instance, for a drug addict, the next hit might serve his or her pleasure best, but surely wouldn't serve an objective concept of happiness that we'd all agree to.

Furthermore, _happiness_ _economics_, which aims to measure our happiness in relation to material wealth, fails to come to grips with an objective sense of happiness, and often produces strange, paradoxical findings.

For example, some statistics say that the level of happiness in the UK didn't rise in conjunction with a massive increase in income, while others say they indeed did rise! How do we explain this discrepancy?

The problem is that these studies rely on an individual's subjective assessment of their own happiness. However, these self-reports invariably depend heavily on individual and cultural conceptions of what happiness means.

As a result, simply estimating your happiness on a scale of 0 to 10 without any further specifications produces subjective results. For example, a clinically depressed person will have a completely different perspective on what a 0 or a 10 means than a steadfast optimist would.

Without a concept of happiness that _everyone_ can agree on, these statistics won't tell us much about the possible connection between wealth and happiness.

> _"Sound moralists like Solomon and Socrates have told us over and again that happiness lies in love and virtue, not riches."_

### 7. We have to remember the ancient concept of the good life. 

As long as we have been able to, human beings have contemplated the meaning of happiness. Perhaps we should look into the past to discover what ancient thinkers had to say on the subject.

One such thinker, Aristotle, saw the _good_ _life_ as an end in itself. For him, every species has its own _telos_, i.e., a final state of perfection, which, from the very beginning of its existence, each species develops towards.

For humankind, the _telos_ is the good life, or _euzen_. But what does _euzen_ actually look like?

For Aristotle, the good life is one that is thoroughly public, where politics and philosophy are considered leisure activities to be conducted just for their own sake. This, he thought, was the best kind of social interaction.

So where does wealth come into play in the good life?

For Aristotle, _economy,_ or _oikonomika_, was just the material subsistence of familial households, which provides the family with the goods necessary to lead the _good_ _life_.

However, he was not ignorant to the exchange of goods for money, which he called the _natural_ _art_ _of_ _wealth-getting._

This _natural_ _art_ contrasts with the _unnatural_ art of _usury_, in which the exchange of goods for money transforms from the means to produce the good life to an end in itself.

As a result, things lose what Marx called their _use-value,_ i.e., value based on utility, and are instead reduced to their "exchange-value," or the profit their sale generates.

According to Aristotle, usury corrupts the way that societies function: warriors fight only for pay, doctors care about fees rather than patients, and so on.

Prophetically, Aristotle also worried about the insatiable nature of seeking wealth for its own sake. For a life centered around wealth, there is no _telos_, no final destination: there is simply always more money to be had.

### 8. There is the possibility to live a decent way of life today. 

Things have changed considerably since Aristotle's time. Although he has laid the groundwork for what constitutes the good life, modern society will see things at least a little differently. So what should we consider in our quest for the good life?

We must understand that the basic goods, both material and immaterial, that are required for the good life have four characteristics:

First, they're _universal_, meaning they aren't unique to particular parts of the world and have persevered across different cultures throughout millennia. For example, every culture across the globe values the preservation of life in some form; consequently, murder is condemned in every society.

Second, they're _final_, i.e., they aren't means to acquire other goods. You can think, for example, of how community is its own thing worth striving for, and isn't merely the sum of self-serving individuals.

Third, they're _self-sufficient_ : they aren't aspects of some other good, but are ends in themselves. Deep friendships, for example, are not merely part of another social interaction, such as work groups. They're their own good!

Lastly, they're _indispensable_. These goods, like your physical health, would cause serious harm if you lost them.

Keeping these characteristics in mind, there are some basic goods that must be guaranteed in order to ensure a good life.

For starters, we need health and security. We also can't forget respect, or the recognition of our views and interests. Self-determination and autonomy, too, are critical.

We also need to include friendship. In fact, friendship was so important to the Greeks that they catalogued all stable affectionate relationships, from platonic friendships to family, to romance.

Also important is harmony with nature, i.e., the responsible and sustainable use of nature's wealth, as opposed to its reckless destruction.

Finally, a good life makes room for leisure. To the ancient Greeks, leisure isn't just relaxation and rest, but was actually free activity with a purpose, giving us time to learn about things like the arts, philosophies and morals.

> _"What we wish to see more of is leisure, a category that...is so far from...idleness that it is almost its polar opposite."_

### 9. The state has to increase its influence on the economy. 

At this point in history, we've built up large governing bodies that are greatly invested in our lives. What, then, can states do to help their citizens create the good life?

First, we need a _basic_ _income_, i.e., an unconditional payment to all citizens that enables them to choose how and how much they work.

There are two common objections to basic income:

First, that no one would have the incentive to work, and second, that states just couldn't afford it.

However, in an affluent society, our goal _should_ be to reduce the incentive to work and to make leisure more attractive.

Not only that, but as Nobel laureate James Meade showed, a basic income could indeed be financed through an intelligent combination of capital taxes and state-owned investment trusts.

Other options include currency trading, or rent in the form of pollution permits, such as carbon credits.

Second, the pressure to consume must be reduced.

Calming the frantic desire to consume is crucial, as _conspicuous_ _consumption_, i.e., keeping up with the Joneses, is the primary driver for overwork.

One way to slow down consumption is to impose a progressive consumption or expenditure tax in place of an income tax.

For example, while necessary items for everyday life such as groceries, wouldn't be taxed, conspicuous consumers who, for example, buy a learjet or a fifth car would face heavy taxes on those purchases.

These taxes could be scaled progressively, where levels of taxation change according to how necessary the items are. Such a tax would promote savings over consumption, thus calming our impulse to buy.

Advertising must also be reduced, as it fuels the insatiability of want. For example, reforms that would forbid firms from writing off ads as business expenses would make advertisements more expensive, and thus less attractive for businesses.

These changes, however, are just part of the solution. Ultimately, we must decide whether we want to continue an unfulfilled life in pursuit of wealth or make the changes necessary to achieve the good life.

### 10. Final summary 

The key message in this book:

**Accumulating** **wealth** **seems** **like** **almost** **everybody's** **goal** **in** **today's** **capitalistic** **societies.** **But** **it** **doesn't** **have** **to** **be** **that** **way!** **Today's** **Western** **societies** **are** **wealthy** **enough** **to** **enable** **us** **to** **lead** **a** **carefree** **and** **spirited** **life** **without** **having** **to** **slave** **away** **at** **work** **nearly** **as** **much** **as** **we** **do.** **All** **we** **must** **do** **is** **curb** **our** **insatiability** **for** **wealth,** **and** **strive** **for** **the** **true** **values** **of** **the** **good** **life.**
---

### Robert Skidelsky and Edward Skidelsky

Robert Skidelsky is the famed author of the three volume biography on economist John Maynard Keynes as well as an Emeritus Professor of Political Economy at the University of Warwick.

His son, Edward Skidelsky, is a lecturer in philosophy at the University of Exeter as well as author of the book _Ernst_ _Cassirer:_ _the_ _Last_ _Philosopher_ _of_ _Culture_. In addition, he regularly writes for the publications _New_ _Statesman_ and _Prospect_.

