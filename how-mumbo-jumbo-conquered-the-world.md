---
id: 572258e75ed4340003a94acf
slug: how-mumbo-jumbo-conquered-the-world-en
published_date: 2016-05-03T00:00:00.000+00:00
author: Francis Wheen
title: How Mumbo-Jumbo Conquered the World
subtitle: A Short History of Modern Delusions
main_color: F3E355
text_color: 635D23
---

# How Mumbo-Jumbo Conquered the World

_A Short History of Modern Delusions_

**Francis Wheen**

_How Mumbo-Jumbo Conquered the World_ (2004) takes a detailed look at irrational tendencies and how they have come to pervade and pervert the modern world. These blinks walk you through bogus philosophies, from neoliberal political and economic dogma that predominated in the 1980s to New Age gurus peddling hollow advice and false hope.

---
### 1. What’s in it for me? See how irrational philosophy and superstitious ideology blight our world today. 

Have you ever felt that something is not quite right in Western societies these days? Have you ever stopped to wonder why so many people are disillusioned with mainstream politics, or how "self-improvement" books have become an $11-billion industry?

_How Mumbo-Jumbo Conquered the World_ examines the sad truth that many aspects of our lives are determined less by reason than they are by superstition and irrationality.

In these blinks, you'll discover

  * how economic superstitions contributed to present levels of income inequality;

  * what post-structuralism is and why you should be wary of it; and

  * why the "clash of civilizations" theory has no grounding in reality.

### 2. The dominant economic theory of neoliberalism is bad for society at large. 

Have you ever wondered why rich people keep getting richer while the poor only fall deeper into poverty?

The answer boils down to the fundamental philosophy that shapes most mainstream Western politics, whether on the right or left of the political spectrum: _neoliberalism_. Neoliberalism is a belief that free markets rather than government intervention are the path to economic prosperity.

This belief is often associated with the "supply-side" economics that rose to popularity in the 1980s. However, supply-side economics was just a clever rebranding of the long debunked "trickle down" theories, which said the rich getting richer would allow wealth to run "downhill" within a society to lower- and middle-class people.

The proponents of this philosophy maintain that such a system is far more effective at redistributing wealth than government. But history tells a different story. In fact, the neoliberal policies that entered the UK and US mainstream in the 1980s have proven disastrous.

For instance, as the UK Conservative Party entered office in 1979, then-Prime Minister Margaret Thatcher began slowly dismantling the welfare state, a social safety net set up at the end of World War II.

She slashed government spending in her first year in power and such policies quickly proved detrimental to the country. For instance, inflation rose by 11 percent, the manufacturing sector hit a deep recession and by the end of Thatcher's third year, unemployment had risen from 5.7 percent to 13 percent.

Meanwhile, on the other side of the Atlantic, Ronald Reagan entered the Oval Office in 1981, cutting the top income tax rate from 70 percent to 50 percent. As a result, the national debt went from $900 billion to $3 trillion over a five-year period.

These policies decimated the working and middle classes. Under Reagan, real wage levels dropped and jobs were offshored to cheaper labor markets such as China. The result was a ten percent unemployment rate in 1982, the highest since the Great Depression.

### 3. Self-help literature is a product of neoliberalism and a waste of money. 

It's no coincidence that self-help books saw a huge rise in popularity at the same time as neoliberal economic policies began destroying the economies of the United States and the United Kingdom. After all, it was a time when millions suddenly found themselves desperately seeking new paths to success.

Unfortunately, some of the best-selling books of this genre are not as life-changing as they claim. In fact, some of the most successful pieces of self-help literature made millions of dollars without laying out a single original thought.

Just take the self-help industry superstar, Tony Robbins, whose net worth is about $480 million. In his best-selling title, _Unlimited Power_, he cloaks common-sense principles in metaphors and presents them as groundbreaking ideas.

For instance, Robbins says that to bake the best chocolate cake in the world, all you need to do is follow the recipe. In other words, a good way to learn a skill is by following an example set by someone else — not such a groundbreaking idea after all.

But then how did he attract such widespread interest?

Because success in the modern world isn't about what you say; it's about presenting your words in the way that makes them seem most appealing.

And Robbins isn't alone. Plenty of other famous self-help authors have used utter nonsense to pave their way to media recognition and fame. For instance, new-age "guru" Deepak Chopra rakes in $20 million a year just by selling books and holding spiritual retreats.

But the wisdom he's selling tends to be rooted in irrational ideas that any logical person could easily tear apart. For example, he claims that aging is a process that we "learn" by watching others age, and that this principle also applies to earning a fortune. That is, you can get rich by learning from those who already are.

Many people are misled into thinking that the more of his books they buy, the better they'll "learn" to be rich. And despite the obviously bogus nature of his ideas, he's earned a celebrity following that includes big names like Michael Jackson and Hillary Clinton.

### 4. “End of history” or “clash of civilization” theories are bogus misrepresentations of our complex world. 

So, self-help icons rise to fame not because they make meaningful contributions, but because they use a flamboyant rhetorical style. In a similar fashion, academics who tell of the "end of history" aren't famous because their views are correct, but because of the way they dress them up.

So-called "end of history" theories work on people's emotions and have no grounding in reality. For instance, the scholar Francis Fukuyama published his best seller on the topic in 1992, titled _The End of History and the Last Man_. It espouses the theory that Western neoliberal capitalism is the dominant and final political and economic force.

When he wrote the book, Western media was full of captivating analysis regarding the end of the Cold War, but Fukuyama's ideas grabbed the public's attention in an entirely different way.

Even so, the book didn't rely on reason or logic. For instance, it suggests that little has changed sociopolitically since 1806. Fukuyama even argued that Nazism and Communism had little effect on the way the world functions.

End of history theories are, therefore, nonsense and "clash of civilizations" theories are their not-too-distant relatives. In 1996, Samuel Huntington published another best seller, _The Clash of Civilizations_. In this book, he made the claim that future conflicts won't be due to political or economic differences, but to differences in culture.

But this idea is misguided from the start. For example, he begins by breaking humanity into seven or eight civilizations. He labels Greece, which most people view as the mother of Western civilization, not as Western at all, but rather of the Slavic-Orthodox civilization, because Greece experienced a military dictatorship in the 1960s and 70s.

Yet he can't seem to explain why Spain is considered Western, despite the fact that the ruthless dictator Francisco Franco ruled the country for decades.

### 5. Beware the allure of poststructuralist nonsense. 

So, the political mainstream and self-help literature have been thoroughly steeped in nonsense and, since the 1980s when _post-structuralism_ became fashionable in American universities, so has academia.

By the early 1990s, post-structuralism, a school of philosophical inquiry, had become accepted and even prescribed in humanities departments across the United States and continental Europe.

But what is it?

Post-structuralism is based on the concept that stable meaning doesn't exist. So, contrary to the structuralist notion that meaning is the result of material factors like social context, post-structuralism asserts that meaning is constantly changing and, therefore, has no definite form.

Adherents of post-structuralism also believe that every system of thought or social organization should be considered a "text," subject to infinite interpretation. As a result, even concrete fields like chemistry are supposed to be understood in the same way as a work of fiction.

Not only that, but poststructuralists characteristically employ vague, complex language to sound intelligent. For instance, they use big words with ambiguous meanings like "hegemony," "signification" and "knowledges" in the plural.

These vague terms let poststructuralists interpret the world however they choose. But, ironically enough, author Barbara Ehrenreich recalls that her son had marks taken off a college paper for using a common word, "reality," without qualifying it through the use of inverted commas.

So, post-structuralism is vague, but why is this such a big issue?

Well, allowing for infinite interpretation of all things can be dangerous. A great example involves the notoriously anti-Semitic articles penned by French philosopher Paul De Man in the 1940s. The noted poststructuralist Jacques Derrida controversially defended De Man's work, saying that since texts are open to infinite interpretation, a text seen as anti-Semitic could also be "read" as being against anti-Semitism.

In fact, post-structuralism helped create a dangerous environment in which history was increasingly overinterpreted. For example, the best-selling historian David Irving began controversially treating the Holocaust like a "text" that could be interpreted as having never occurred. As a result, poststructuralists began claiming that particular tragedies, like the Holocaust, were exceptions to their philosophy.

### 6. Beware politicians that brand their right-wing ideas as socially liberal. 

In the late 1980s, a leftist British politician was campaigning for nuclear disarmament and the rolling back of Margaret Thatcher's neoliberal economic policies. His name was Tony Blair, and you might be surprised to hear what happened after he led the Labour Party to victory in 1997.

Blair's economic policies turned out to be just the same as Thatcher's. After being elected Labour leader, he took the party in a different direction, dubbing it "New Labour" and adopting new positions that entirely contradicted his previous ones.

For instance, in 1997, instead of making good on the party's promise to raise income taxes on the rich to pay for public infrastructure, he implied that he would reduce them if he could. It wasn't long before Blair's New Labour jumped on the bandwagon of the same free-market nonsense he had so wholeheartedly opposed in the 80s. It was at this time that he began calling for a new political position known as the "third way".

What's that?

Basically, third-way parties employ language that resonates with both the right and left while coating it in "progressive" values. It all began in 1994 when Demos, a third-way think tank, brought on a marketing company to research the issues important to young voters.

The results found that Britain's youth were more concerned with concepts like "connectedness," "empathy" and "sexuality" than core socio-economic needs. With this knowledge in mind, the Labour party heavily utilized such language, moving their rhetoric away from concrete policies and toward abstract "progressive" values.

This linguistic shift let them maintain contradictory policy positions that were interpretable as both right and left at the same time. For instance, in 1998, Blair claimed that "entrepreneurial zeal" will produce "social justice."

It might sound progressive, but in actuality he was espousing the same neoliberal hogwash as his Conservative predecessors: a free-market will lead to equitable distribution of wealth.

### 7. We should look to the past for examples of how to counter false logic. 

We have seen that bogus irrationality is passed off as wisdom fairly often. But what can we do about it?

In truth, we just need to work together and learn from each other to produce rational solutions that benefit all of humanity. Luckily, we can take some inspiration from the past.

Civilizations have always evolved through the sharing of ideas and culture — just consider something as simple as food. While fish and chips is thought of as classic British cuisine, it wouldn't exist at all if it weren't for cultural interchange. More specifically, the dish was made possible both by potatoes being brought to Europe from the Americas in the sixteenth century, and the Jewish community's tradition of frying fish.

The same is true for the false neo-conservative dichotomy of "Islam vs. the West." In fact, the West would have never experienced the Enlightenment of the late seventeenth and early eighteenth centuries if it weren't for the vast troves of knowledge, especially in mathematics and astronomy, shared by the highly advanced Islamic world with the relatively backward people of Europe in the Middle Ages.

But to improve humanity's future, it's also essential to consider how people have fought restrictive and irrational ideas in the past. For instance, the founding fathers of the United States were inspired by the Enlightenment philosophy of reason. As a result, they separated church and state, allowing for both the freedom of religion without persecution and the right to live without religion.

It was by founding the country on these principles that the founding fathers hoped to spur free thought and rational debate in a newly tolerant society. This philosophy was adopted in direct opposition to what they perceived as the irrational and oppressive governance of Europe.

So, whether it's espoused by divine monarchists, neoliberal fundamentalists or radical poststructuralists, irrationality must be challenged wherever it emerges — or humanity will be doomed to needless illogical suffering.

### 8. Final summary 

The key message in this book:

**The modern world is increasingly ruled by fear, fundamentalism and irrational philosophy. The economy is premised upon long-disproven "trickle-down" theories and renowned universities are full of dogmatic, pseudo-intellectuals. To save humanity from this dark reality, we must learn from one another.**

Actionable advice:

**Beware the free-trade propaganda.**

On the surface, free trade sounds great; it promises cheaper consumer goods worldwide. But that's only half the story. Free trade also entails the opening up of cheap labor markets to Western corporations. While these corporations profit tremendously from the lower manufacturing costs, these savings don't benefit society as a whole. In fact, the net result is tremendous unemployment and growing income inequality.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with _How Mumbo-Jumbo Conquered the World_ as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Bad Science_** **by Ben Goldacre**

We often swallow scientific-sounding language used in advertisements or on the news without any further thought. But if we analyze it a little, we often find that it's merely pseudoscience. _Bad_ _Science_ shows us that this bogus science can lead to serious misunderstandings, injustice and even death.
---

### Francis Wheen

Francis Wheen is an award-winning author and journalist. He is a regular contributor to _The Guardian_ and is renowned for his writings on Karl Marx.

