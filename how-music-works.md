---
id: 5328581236353800087b0000
slug: how-music-works-en
published_date: 2014-03-18T08:52:44.000+00:00
author: David Byrne
title: How Music Works
subtitle: None
main_color: FF6433
text_color: B24624
---

# How Music Works

_None_

**David Byrne**

_How Music Works_ sets out to explain the workings of music from ancient history up to now. Writing from an insider's perspective, David Byrne delves into different aspects of popular music, based on current research, music history, technical knowledge and his life-long career in the new wave band _Talking Heads_.

---
### 1. What’s in it for me? Learn how music works from a true master. 

Many rock stars have written books about their careers, but never before has a rock star said that he would explain _How Music Works._

Yet these blinks do just that. From writing songs to producing records, from the age of Neanderthals to the latest hits, you'll get a behind-the-scenes peek at the world of music.

You'll discover why the sounds of traffic influence how birds sing and how the music we create is similarly influenced by our surroundings.

You'll also find out why some clubs become vibrant music scenes while others remain in obscurity.

Finally, you'll read about the societal power of music, including how it can even help lower crime rates and lift young people out of devastating poverty.

### 2. It’s our surroundings, not just our emotions and skills, that determine the kind of music we make. 

Think about a piece of music that affects you. Do you believe that it came from a "place within" the artist?

If the answer is "yes," you're not alone: most people believe that music emerges out of some interior emotion. Consider how easy it is to conjure up an image of a composer suddenly arrested by inspiration, and furiously scribbling down an already fully formed composition.

We also usually assume that musical compositions depend solely on their creators' abilities. For instance, most people think that Western medieval music is harmonically "simple" because composers hadn't yet developed the ability to use more complex harmonies.

But these assumptions are wrong. It actually works the other way around: we create music that fits the context available to us.

For example, long notes and slowly progressing melodies work beautifully in stone-walled Gothic cathedrals, since sounds will resonate for a long time in this setting. Medieval music that was played in these cathedrals had to be harmonically "simple" because shifting musical keys would result in dissonance, as notes overlapped and clashed.

Furthermore, with the introduction of new technologies came new ways of singing. The arrival of the microphone, for example, meant that singers no longer needed great lungs to project their voice. This led people like Frank Sinatra and Bing Crosby to change their vocal dynamics radically; they thereby became pioneers in the practice of singing "to the microphone."

But this phenomenon of adapting music to its surrounding context is not limited to humans. Research shows that the songs of different animals have evolved to fit their environment. In San Francisco, for example, birds have gradually raised the pitch of their singing so they could be heard above the increased traffic noise. And, in the last few decades, whales have adapted their calls to be heard over the increased shipping noise.

### 3. The advent of recording technology has changed how we create and listen to music. 

In 1878, the first music recording was made, freeing music from its live, real-time context. This has had a massive impact on both how music is made and experienced.

As recording became a ubiquitous practice, it changed how we played instruments and sang. For example, bands had to play at a more precise tempo as unsteady tempos sounded sloppier without the "distraction" of a performance's visual element. The result was that players had to learn to play to an internal metronome.

Also, the use of vibrato became commonplace. Vibrato means a wavering in pitch, a technique often employed by string players or singers. It obscures the exact pitch of a performance and, regarding singing, was therefore originally considered as "cheating."

But since even the tiniest discrepancies in pitch were perceptible in a recording, vibrato became useful for covering up mistakes. Nowadays we're so accustomed to vibrato that we take it for granted; indeed, we'd probably even find classical string playing and opera without vibrato strange.

Recording technology has also changed the way we hear music.

American composer John Philip Sousa was opposed to recorded music, afraid that the technology would substitute actual human beings making music and ultimately reduce the expression and experience of music to a mathematical system of "revolving things," like megaphones, wheels and discs.

To some extent, his fear was realized: when we think of a piece of music, we now think of the _sound_ of a particular recording. Similarly, the live performance of that same piece is now usually seen as an interpretation of the recorded version.

However, twentieth-century philosopher and music critic Theodor Adorno had a slightly more optimistic perspective on what happens to our experience when the visual performance of music is removed: he claimed simply that music could be appreciated more objectively.

### 4. The composition and sonic quality of music has changed with the advent of digital technology. 

The invention of digital technology was an unforeseen consequence of phone-related research by Bell Labs. In 1962, the researchers figured out how to digitize sound by dividing sound waves into "slices" that could be translated into ones and zeros. This has greatly affected music's sonic quality and its composition.

When a digital sound recording is made, the audio gets divided into a finite number of slices. We perceive these slices as a continuous audio spectrum even though the sound is made of distinct steps.

Even though many of us can't tell the difference, the sonic quality of digital recordings does indeed differ from analog media, which reproduces spectrums of sound with an infinite number of gradations. 

One effect of digital recording people _do_ tend to hear is what's often referred to as "digital cleanliness." Those people experience such crisp recordings as soulless and choose to stick with their cassettes and vinyls.

Digital technology hasn't only changed how music sounds but how it's composed. For example, pop recordings made with computers are often _quantized_, meaning that the tempo doesn't vary. Quantizing is so widespread because it makes a song's rhythm tight and simultaneously facilitates easy editing and arranging on the computer screen.

Some people, including the author, feel that, even though quantization can be a great help, it sometimes makes music sound too uniform.

Another effect digital technology has had on composition arises from the use of MIDI, or musical instrument digital interface.

MIDI works by encoding musical notes as ones and zeroes rather than actual sounds. But since MIDI isn't particularly sensitive to the nuances of, say, string instruments, it's mostly keyboards and percussion pads that can trigger MIDI well. So, by inclining composers to use specific instruments, MIDI actually ends up influencing how music is composed.

### 5. Whether a band is recorded individually or together greatly affects the resulting music. 

Recording music isn't just about capturing the sound of a band or musician. It's an art in itself, one that affects music immensely. And, as with most other art forms, you can combine different ways of recording, each with its pros and cons.

First, there's the divide-and-isolate approach to recording, where the producer and recording engineer bear a huge responsibility for how the record ultimately sounds. In the late 1970s, this was the prevalent way of recording music: isolating each instrument to get as clean a sound as possible.

For example, the drummer would be placed in an isolated booth and the bass player would be surrounded by sound-absorbent panels so as to completely separate the different players. This way, the producer could take apart the music and put it together again, thereby maintaining control throughout as much of the process as possible.

As you might expect, this meant that some of the organic interplay between the musicians disappeared, which had a large impact on the resulting recording.

In fact, many musicians feel that a recording will represent the band more accurately if the members are recorded together without any of the usual sonic isolation. Despite the risk of a muddy or sloppy recording, many bands find this a comfortable way to record their music since they're used to playing together.

Moreover, being able to hear the whole band playing also tends to render the performances more natural and inspired.

Today, no dogma in music recording is adhered to quite as strictly as back in the late 1970s. There's no need to choose between divide-and-isolate or all-together-at-once approaches; sometimes several approaches will even coexist on the same recording, each one affecting the music differently.

### 6. Collaborating is an important part of music’s essence and can be an aid to creativity. 

Collaborating has always played an important role in music creation. Just think of the many renowned and respected songwriting duos like Lennon and McCartney or Jagger and Richards.

Popular as ever, nowadays collaboration doesn't even require the collaborators to be in the same country. Instead, collaboration can be based on transferring digital music files back and forth between the musicians.

To make their record _Everything That Happens Will Happen Today,_ Byrne and producer Brian Eno had a long-distance collaboration. Eno would email his musical sketches to Byrne; Byrne would add vocals and send it back to Eno; and so on until their album was completed.

Even though there's a bigger risk of miscommunication when working this way, there are also great advantages, like having time to think before having to come up with the next idea.

And there are many creative benefits to working in a team, no matter how the collaboration is carried out.

First, weaker ideas have a better chance of being improved. For example, the author's song "Psycho Killer" was originally intended as a ballad, but when the other members of Talking Heads joined in, the song found a more energetic vibe that later proved very popular with their audience.

Second, collective improvisations might lead you to places you wouldn't otherwise reach. Talking Heads, for example, evolved a way of writing music that was based on their collective improvisations where one person's response to another's input could shift the whole piece in a new direction.

This could result in both pleasant surprises and destructive impositions, but either way it became the fuel for many of the band's songs and extended the authorship of the music to the whole group.

Accepting that the creative decision-making is shared by your team means that you can better concentrate on those decisions you're responsible for. In other words, though your control is restricted by collaborating, this can actually be a blessing.

### 7. Declining record sales have made the music business more flexible. 

Record sales have suffered a steep decline since 1999 due to the changing landscape of music distribution.With the rise of digital distribution, record sales have decreased drastically and today only very few artists are able to live off of their record sales alone.

Consider the fact that, of all albums released in 2009, only 2.1 percent sold over 5,000 copies. And in 2006, only 35 of the albums released sold more than one million copies within the calendar year. By 2010, the number was down to ten.

Predictably, this situation has resulted in most retail music chains, like Tower Records and Virgin Megastores, shutting down.

It also means that the standard royalty deal — where the record company finances the recording and the artist gets a percentage of the record sales — no longer works for most mid-level artists. In most cases, royalties won't even cover a record's production costs, so you'd have to sell an awful lot of records just to make a living.

Having been the focal point of the music industry, records now function merely as the loss leaders that generate sales for more profitable items, like concert tickets and merchandise.

Yet there's an upside to this changing landscape: the music business has become more flexible.

This can be seen in the fact that musicians are increasingly choosing to work independently of traditional label relationships. Take, for example, Radiohead, who left their label EMI to release their 2007 album _In Rainbows_ online; or Madonna, who bid farewell to Warner Bros. to sign with concert promoter Live Nation.

Also, the ease and facility of home recording and digital distribution makes the DIY route — where everything is self-written, self-produced and self-marketed — appealing to many artists.

The traditional means of selling and distributing records have become less feasible since record sales started to decline. Luckily, however, the flexibility of today's music business gives artists a wealth of alternative possibilities.

### 8. The venue matters; if it’s suitable, a vibrant music scene can emerge around it. 

When it comes to enjoying live music, one major part of the experience is the venue. Sometimes a vibrant music scene can even start evolving around a specific venue. Take, for instance, New York's CBGB, which went from being a biker bar to the backdrop of an emergent musical scene in 1974.

Actually, through a few converging factors, a vibrant music scene can emerge from _any_ venue.

Firstly, the venue needs to offer performers certain options and perks. This means that bands should be paid fairly. At CBGB, the scene didn't take off until its owner was persuaded by singer and guitarist Tom Verlaine to allow his band to "play for the door"– i.e., the small admission fee went to the artists. Before that, CBGB had just been a biker bar that didn't draw a lot of customers; but, by allowing unknown, unsigned bands to get paid to play their own music, the venue made it possible for a scene to emerge.

Performing musicians should also be allowed to get in for free on their nights off. This way there will always be an audience and the chances of the venue becoming a popular hangout will increase.

A second factor is the physical design of a venue, as this is essential to its success. CBGB facilitated the rise of many bands and songs by "being the right size, the right shape and in the right place." It wasn't too big, lending it an intimate atmosphere, and had crooked uneven walls and a looming ceiling that made for remarkably good sound.

Furthermore, the performers could never really hide from the audience as there was no backstage area. Instead they had to mingle with the crowd, which is one way that the crowd generated support for them.

Creating a scene is important. It's the only way for local musicians and artists to have an outlet for their talents, and all that's needed is a venue that follows a few simple principles.

### 9. Encouraging amateurs to make music can have widespread social and cultural benefits. 

For a very long time, arts teaching and funding wasn't aimed at fostering creativity among the general population, otherwise known as "the amateurs."

With the advent of sound recording, music education shifted its focus from teaching students how to create music to how they should understand music, with a special emphasis on certain "superior" kinds of music.

This cultural hierarchy encourages consumption rather than creation and risks discouraging amateurs who want to compose their own music. In the US, for example, music education has been drastically downsized. The No Child Left Behind program's emphasis on test scores made US schools in most states reduce their arts programs by more than half.

But there's a great value in amateur music making since music can work both as a social glue and a self-empowering agent of change.

One useful byproduct of music making is the empowering feeling people get when creating something and the socializing that comes with playing in bands.

And, moreover, as an organized system, music can even fight crime and poverty.

For example, the musician Carlinhos Brown established several music and culture centers in formerly dangerous neighborhoods in Brazil. He was inspired by his own upbringing in Candeal, where local kids were encouraged to sing, join drum groups and create music in other ways to keep them away from dealing drugs. These initiatives energized them, gave them hope and eventually made the crime rate drop.

Consider also El Sistema, a music education program that began in a Venezuelan parking garage in 1975. Not only has it fostered many high-level musicians over time, but it has also systematically fought poverty by giving kids from disadvantaged backgrounds an opportunity to improve their situation.

Music can change people's lives in ways that go beyond their being emotionally or intellectually moved. When music becomes a part of an entire community, it can even turn into a moral force. Therefore, the creativity of amateurs should be prioritized to a much greater extent.

### 10. Far from being mere entertainment, music is a part of what makes us human and it impacts our well-being. 

Music, it seems, has always been an essential part of human life.

Some theories claim that music originated with the non-verbal sounds mothers make to their children, while others connect music to sounds in nature or animal noises. But, although theories about music's origins vary, all agree that it emerged the same time people did.

The earliest evidence we have of early man actually making music dates back about 45,000 years and proves that Neanderthals were playing flutes based on the same basic notes produced by the white keys of a piano today.

Furthermore, humans have evolved many specialized skills that seem to be related to music making. For instance, studies show that infants can distinguish different musical scales, and prefer harmonies we think of as consonant, or harmonies that are felt to be "stable."

Infants can also hear what are called relational pitches. This means that if an infant knows the "Happy Birthday" song, they will recognize it regardless of what note you start the song with. Even computers don't have this ability yet.

Finally, music has a profound effect on our psychological well-being; many would agree that a life without music is a life significantly diminished.

Actually, studies show that music, more than many of the arts, triggers activity in numerous parts of the brain. In fact, some people with brain damage can only get through their daily routines with the help of music. Based on this discovery, a group of therapeutic techniques called Melodic Intonation Therapy has been developed.

Over time, we have developed specialized skills for enjoying and creating music. The musical forms and structures we enjoy are often shared despite often vast cultural differences. Moreover, there's even mounting evidence that music is good for our psychological well-being. All in all, this seems to show that music is, and always has been, a large part of who we are.

### 11. Final Summary 

The key message in this book:

**Music is deeply dependent on its context, be it the specific venue where it's played, the way it's been recorded, or the digital technology used to create it. But what always remains the same is music's importance to us as human beings: it can bring us together and be a self-empowering tool. And, with modern technology and a music business that's more flexible than ever, there are countless ways to get music out into the world.**

Actionable advice:

**Empower yourself and develop your brain through music making.**

You might not be able to play a proper scale, but it can be extremely empowering to learn to play a musical instrument. Creating music engages both your mind and body, and studies show that regular active participation in music stimulates the development of many different areas of the brain. So get someone to show you a couple of chords on the guitar and start jamming.
---

### David Byrne

David Byrne is a Scottish-born artist, Rock and Roll Hall of Famer and cofounder of _Talking Heads_. He's received many awards, including an Oscar and a Golden Globe, and is the author of _Bicycle Diaries_ and _The New Sins_.

