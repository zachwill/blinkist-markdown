---
id: 5aae8546b238e100069e8f6f
slug: how-not-to-be-a-boy-en
published_date: 2018-03-20T00:00:00.000+00:00
author: Robert Webb
title: How Not To Be a Boy
subtitle: None
main_color: FF3395
text_color: CC2978
---

# How Not To Be a Boy

_None_

**Robert Webb**

_How Not To Be a Boy_ (2017) is writer and actor Robert Webb's personal account of what it was like to grow up as a boy in rural England. In particular, what it was like to be the kind of boy who wasn't all that into typically "manly" stuff. Webb's tale is both a heartfelt autobiography and a humorously critical assessment of the pressures society can put on young men.

---
### 1. What’s in it for me? Take a peek inside the mind of Peep Show’s Robert Webb. 

All of us, at one point or another, will likely feel the pressure of expectations. These might be expectations from parents or teachers or society at large. Growing up in rural England, Robert Webb experienced what it's like to be a teenage boy who's expected to love sports, shun all intellectual pursuits and be so full of raging hormones that he couldn't possibly be taught how to hold a productive conversation. Meanwhile, there's a whole list of things that boys aren't expected to be, such as emotionally sensitive.

If you think this sounds like a recipe for legions of horrible boyfriends — well, it is! But against all the odds, Webb managed to stay intellectually curious, make it through Cambridge University and become an internationally respected comedy writer and actor. He's perhaps best known as one of the stars of the long-running television show _Peep Show_, which has brought viewers on a hilarious and sometimes frightening journey inside the heads of its two lead characters, Mark and Jeremy.

Robert Webb is here to tell everyone that there's no such thing as a "male brain" that's responsible for making men behave in typically male ways. These so-called typical male behaviors are the result of generations of outdated expectations.

In these blinks, you'll discover

  * why the right pair of socks can be very important;

  * which Prince song Webb will never forget; and

  * how not to play chess.

### 2. Most behavioral gender differences aren’t biological; they’re shaped by social expectations. 

No one sets out to be a bad parent, but some of them can be appallingly stubborn in how they reinforce gender stereotypes. It's common for a parent to say, "Oh, Sally is such a girl. One day, a boy tried to get her to play with an action figure, and she just put a dress on it and tucked it into bed!"

What many parents don't understand is that a child may adopt certain stereotypical behaviors, but that this doesn't mean they're biologically determined to behave this way.

Whether parents want to admit it or not, most cognitive and behavioral traits are not biological or gender-related at all. In fact, there's no such thing as a "male brain" or a "female brain." In November of 2015, an article in _Science_ magazine explained how neuroscientists determined that _all_ brains are unique, and therefore can't be categorized by gender.

So, if a boy doesn't want to wear a pink sweater, this isn´t because of biology. Chances are, any recognizable gender differences are the result of social expectations.

In her book _Delusions of Gender_, psychologist Cordelia Fine points to a study from 2000 that shows just how biased society is about gender. In the study, mothers were asked to examine a sloped walkway that had an adjustable level for how steep it could be. Their task was to judge the steepness and try to determine whether their eleven-month-old toddler could succeed in climbing it.

The results showed that mothers consistently underestimated the girls' ability to successfully climb slopes that were quite steep. Conversely, the mothers routinely overestimated the ability of the boys, expecting them to be able to climb slopes that often proved to be too steep.

With gender biases like these in society, is it any wonder that men and women will grow up to have different personalities?

### 3. The strict dress code for boys is enforced by a culture of bullying. 

Here's some food for thought: women have been comfortable wearing pants for around a century or so, but it's still generally unacceptable for men to wear a skirt or dress. Why is that?

Indeed, the clothing boys are expected to wear is strictly regimented. 

When Webb was ten years old, a dramatic crisis ensued when his Aunt Tru tried to give him the wrong socks before a soccer match. They were _girl_ socks — can you imagine!?

Young Webb was flabbergasted that his aunt couldn't tell the difference. Distraught, Webb tried to explain that, first of all, they were clearly too long and, second, they had a pattern printed on them! Didn't she know that the only acceptable socks on a playground are plain white with, at most, a black stripe or two at the top?

Sure enough, poor Webb was ridiculed mercilessly as soon as his playmate, Matthew Tellis, spotted the socks. He began laughing, pointing and calling Webb a girl. Immediately, the other boys gathered around and joined in the fun.

Since the strict male dress code is enforced at a young age by terrible bullying, most boys learn to defend themselves swiftly and decisively in such situations.

Webb was no exception. He immediately countered Matthew's taunts by suggesting that he would probably like the socks better if they were covered in shit.

This was a rather cutting remark since it referenced an incident especially embarrassing to Matthew. Once, during class, Matthew had to go to the bathroom, and the teacher let him leave on the condition that he return in 20 seconds. Matthew, knowing that the teacher was in a bad mood, took this 20 seconds far too seriously, cut his business in the boy's room short and returned with a bit of that business on his pants.

It was a masterstroke of playground politics on Webb's part. He deflected the attack upon his socks and successfully shifted everyone's attention onto the unforgettable day of Matthew's soiled trousers.

But none of this would've been necessary if it hadn't been for society's useless gender stereotypes.

> _"The thing about football — the important thing about football — is that it is not just about football." -_ Terry Pratchett

### 4. People’s absurd stereotypes about gender don’t stand up to scrutiny. 

It might seem ridiculous, but many pregnant women still have to deal with people who assume that they're preparing to give up their career now that a baby is on the way. And there are still those who look on in amazement when they see a man in a coffee shop looking after his toddler.

So it's safe to say that people still buy into absurd stereotypes about gender.

The absurdity of these beliefs becomes especially apparent when you compare them to previously widespread misconceptions about race and religion.

For example, it's not uncommon to hear people say things like, "Mark is like all boys, he can never sit quietly." And, "Sure, boys can learn to enjoy reading, but it doesn't happen naturally." Or how about: "What's great about men is that they're so straightforward and uncomplicated — none of those messy emotions to deal with."

Now, instead of males, imagine if someone was saying these very same things about Muslims or Asian people. You'd probably consider it unacceptable, and that's how it _should_ be considered, no matter who's being stereotyped.

Anybody who knows about human nature knows that generalizations like these are simply wrong.

For example, Webb's brother Mark is a man who closely matches what society expects men to be: he's a father, he coaches a local boy's soccer team, he drives an Audi and he is a manager for an agricultural firm. At school, he was willing to get tough, and even violent, to make sure he was respected.

However, Webb remembers Mark as also being very kind, the sort of person who would take the time to teach his younger brother how to sing and whistle. Mark also babysat his brother on nights when their mom was busy. And one night, he even kissed Webb goodnight — on his mouth!

So don't believe the ridiculous social stereotype that says men don't have an emotional, nurturing and caring side. Because they most certainly do.

### 5. Boys are expected to enjoy rough, physical interactions, not intellectual pursuits. 

There are some very particular, and often hilarious, expectations about male behavior. They're expected to swagger, to be steely-eyed, to meet the world with a cocksure poker face — and they're especially expected to do these things when meeting and dealing with other men.

Even going back to childhood, there is a prescribed, rough-and-tumble manner in which boys are expected to interact.

Webb grew up in rural England, with two older brothers. Sometimes, when Webb was seated at the kitchen table, his brother Andrew would walk behind him and give his head a push. Sometimes it was just a gentle nudge. Other times it was strong enough to dunk Webb's head into his bowl of cereal.

This kind of roughhousing was always done with affection, though, as were the ritual beatings the brothers would engage in every Tuesday afternoon. In Andrew's room, the trio would engage in all sorts of play-fighting, which, more often than not, ended up with Webb banging his head against something quite hard and then pretending it didn't hurt like hell.

Furthermore, men certainly aren't supposed to cry out in pain, just as they're not supposed to be intellectually inclined.

This supposed lack of cerebral leanings makes even less sense than the other rules since boys are expected to grow up and be in charge of things. Nevertheless, no schoolboy with aspirations to coolness would dare show a genuine desire to be intellectual, since all braininess is strictly uncool.

The enduring assumption is that smart students can't be good at practical things such as sports. Therefore, all things intellectual are considered unmanly. This goes hand in hand with the widespread belief that the hormones raging within every teenage boy make it hard for them to follow the rules or excel in the classroom.

This faulty logic — that studying hard or enjoying intellectual pursuits are signs of effeminateness — can be quite detrimental to a young man's academic career. Being bullied for getting high grades certainly doesn't encourage the development of good study habits.

### 6. Boys are not encouraged to develop social skills, so men tend to avoid social contact. 

One day, while riding the bus to a school outing, Webb was approached by his classmate Gareth, who was holding a chess board in his hands.

Gareth wasn't exactly the type of kid Webb would expect to play chess, but he agreed to play a game and started things off with the standard opening move of sending a pawn forward two squares. What happened next is a classic example of how poorly boys communicate.

Gareth's opening move was to send his queen out and place her right in the spot where Webb's pawn had just moved from — a blatant violation of at least three or four different chess rules. Flummoxed, Webb couldn't bring himself to tell Gareth that he'd made an invalid move, so he continued playing for a few more turns until Gareth crowed checkmate.

But then Gareth wanted to play again, and Webb had to draw the line and refuse, leaving Gareth to walk away in a huff.

This kind of situation plays out all the time between schoolboys. The logical thing, of course, would have been for Webb to pleasantly explain to Gareth how the game of chess is played. Gareth could then have expressed his gratitude, and the two boys could have enjoyed a nice game of chess. But such a conversation would have required a level of communicative skill that boys rarely reach.

Unlike girls, boys aren't expected to become experts in human interaction — yet another stereotype that has unfortunate consequences. Indeed, all too often, boys grow into men who avoid social contact and have trouble resolving conflicts.

As an adult, Webb once chatted with a male friend who explained that his neighbors had been using his trash cans to get rid of their rubbish. But rather than having a conversation to resolve the conflict, the friend decided to pack up and move away! Sure, it was to a very similar house in the same neighborhood — but still, this is the level of avoidance a man will resort to in order to avoid conflict.

Since boys are thought to be ill-equipped to learn good manners and conversational skills, they become men who either avoid conflict or get into fights. And since fighting is the more frowned-upon response, the only available option is avoidance. Not only can this result in a grown man moving away from his annoying neighbors; it can also lead to isolation and loneliness.

### 7. Male socialization often makes for bad boyfriends who can’t even be bothered to dump their girlfriends. 

Typically, teenage boys are strange creatures — all awkwardness and self-assertion, filled with both a horrible fear of sex and an overwhelming urge to experience it. Somehow, many boys still manage to get into a relationship at this stage, but having sex is no guarantee that things will get better.

Male socialization often makes for bad boyfriends.

When he was 17 years old, Webb began dating Isabel, his first girlfriend. For the first two weeks, things went pretty well, but then both Webb and Isabel were introduced to his awful boyfriend personality. Just how bad was Webb's dark side? How about ignoring her at school, being overly critical of her friends and refusing to spend time with them, not laughing at any of her jokes and never asking one question about her life?

Webb was dealing with a common situation: he was dating someone he wasn't in love with. But since he had no communication skills, he didn't know how to deal with it. To make matters worse, Isabel wrote him a heartfelt love letter that asked him if he was just in the relationship for the "Sunday sex" or if it was truly meaningful to him. An emotionally intelligent person would see this as an opportunity to be honest and politely break up. But, instead, the author responded with a love letter of his own, and Isabel believed him.

This is one of the many societal plagues caused by male uncommunicativeness: bad boyfriends who can't even break up properly.

In this scenario, dumping Isabel was never an option, because that required a level of tact, bravery and willingness to forego sex that Webb did not have. So, instead, Webb wanted Isabel to break up with him. This way, he neither had to figure out how to make the relationship work nor give up the regular sex, all while insisting on doing everything his way.

So Webb just waited until Isabel was fed up with his refusal to have productive conversations, or engage in anything beyond sex, and finally dumped him.

### 8. It’s normal for men to cry, and even heterosexual men can have a sexual relationship. 

Have you ever seen a grown man cry? Maybe it happened once, when a guy you know was overcome by emotion during a movie or a Bruce Springsteen song. Even if you have witnessed this rare sight, chances are that the man in question didn't want you to see him engaged in such a quintessentially unmanly activity.

But on their own, behind closed doors, men are free to do all sorts of unmanly things, including bawling their eyes out.

Once, when we was 17, Webb found himself laying in bed with his friend Will and crying. Webb's mother had recently died of cancer and, at this moment in bed next to his friend, the reality of her death finally hit him.

It was fortunate for Webb that Will wasn't someone who held on to strict rules about masculinity. Will not only didn't run away from the situation; he actually held his hand and comforted him, while the Prince song "Sometimes it Snows in April" played on the stereo.

When they feel safe and comfortable, men will do more than cry. Sometimes, they'll even explore their sexuality.

Yes, even a straight man can have sex with another man — especially if it's a man you've had a crush on for a long time, as Webb did on Will. Now, lying in bed together, something extraordinary began to happen and no amount of testosterone was going to prevent it from going further.

It may have been a matter of curiosity, or just tender and affectionate sympathy on Will's part, but when Webb touched Will in a sexual way, he didn't resist. Years later, the two would meet again and Webb didn't resist when Will touched him. These encounters weren't exactly steaming-hot sex, but both men shared intense, loving feelings for one another.

The romance eventually ended when Will fell in love with a woman, but the two of them managed to remain friends and enjoy road trips together through rural Lincolnshire. They'd engage in heated cultural debates, and crack each other up while trying to see who could come up with the best impressions of their mutual friends.

### 9. Men aren’t great at expressing their emotions, and they can become tyrannically overorganized. 

It can be a cringeworthy moment to see a woman get berated by a man, only to have her uncomfortably smile and say, "That's just his way of saying 'I love you.'"

However, there is some truth to this stereotypical scenario, as it's yet another example of how bad men are at communicating their emotions.

Following the death of his mother, Webb decided to stay at his father's house for a while, which meant parting ways with his stepfather, Derek, and his younger sister, Anna-Beth.

This decision saddened Derek because he was going to miss having Webb around, but rather than explaining his feelings, he made Webb feel guilty by saying that Anna was going to be lonely without him. But the worst was that he said Webb's mother would have wanted Webb to stay at home.

This guilt trip just made the author angry, and it certainly didn't make him change his plan to move in with his biological dad. But as he was walking out the door, Webb did catch the genuine sadness in Derek's voice when he said, "We'll miss yer."

Of course, Webb should have known that in going from one dad to the next, he'd still be dealing with the strained communications of two men trying to have a conversation.

In the case of his biological father, Webb was faced with a different kind of man — one who was obsessively organized. As he got older, his dad's fussiness grew to alarming levels: there were endless household chores to be dealt with, like making the empty milk bottles spotless before they could be put outside the door for the milkman. And immediately after each bath, the tub had to be scrubbed clean.

This was just the tip of the iceberg, and the pressure from all this fussiness led to their interactions often ending in the honored tradition known to many men: angry words and slamming doors.

After he moved out of his father's home and began attending Cambridge University, Webb went on to become the actor and comedian we know today. And now, as a writer and a father, he knows that being a man doesn't have to mean you're a bad communicator.

### 10. Final summary 

The key message in this book:

**Despite all the discussion around gender, we still live in a world where boys are not supposed to be different, intellectual, emotional or good at social connection. As a result of our still too narrow conceptions of masculinity a lot of men don't know how to be honest about their feelings, struggle with social connections and can even become tyrannically overorganized. By insisting that it's ok for men not to be interested in typically "male" pursuits, that it's perfectly normal for men to cry and show their emotions, we can give our idea of masculinity a much needed overhaul.**

Actionable advice:

**Guys, be aware of physical disparities when you flirt.**

Remember that, as a man, you are typically physically stronger than the woman you are flirting with. If you have trouble putting yourself in her shoes, imagine what it would be like to have a huge, very strong gay guy start hitting on you insistently. Imagine how disconcerting it would be if he started leaning into you, grabbing you or even just not taking no for an answer.

Remember this the next time you try to flirt with someone who is more "petite" than you are.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Raising Cain_** **by Dan Kindlon, Ph.D. and Michael Thompson, Ph.D.**

_Raising Cain_ (1999) explains how boys have to navigate a society rife with misguided ideas about masculinity and filled with cruel classmates who are ready to pounce on any sign of weakness. Discover how these conditions can create emotionally stunted and suicidal young men, and find out what can be done to help remedy their situation before it's too late.
---

### Robert Webb

Robert Webb is a comedian and actor. He's starred in several award-winning shows, including _That Mitchell And Webb Look_ and _Peep Show_.

