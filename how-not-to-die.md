---
id: 596111c5b238e10006ad56cc
slug: how-not-to-die-en
published_date: 2017-07-13T00:00:00.000+00:00
author: Michael Greger, MD, and Gene Stone
title: How Not to Die
subtitle: Discover the Foods Scientifically Proven to Prevent and Reverse Disease
main_color: 21994F
text_color: 166635
---

# How Not to Die

_Discover the Foods Scientifically Proven to Prevent and Reverse Disease_

**Michael Greger, MD, and Gene Stone**

_How Not to Die_ (2015) explains how a plant-based diet can extend your life while transforming your quality of living. These blinks offer a wealth of health-boosting nutritional information and hands-on dietary advice that you won't get from your doctor.

---
### 1. What’s in it for me? Get the simple secret to a long, healthy life. 

Diets aren't just about getting you swimwear-ready for the summer. Eating the right way can profoundly affect your health, and while immortality might not be on the menu, you can certainly prevent or delay the inevitable. Your dinner choices can keep you in good health even as you make it to a ripe old age.

With so much contradictory advice coming from every source, from your friends to your social media feed, it's hard to know where to start. But there are some clear, easy-to-follow rules backed up with scientific evidence, and eating more healthily certainly needn't be a joyless, bland experience.

In these blinks, you'll find out

  * how adding one meal containing meat impacted the health of vegetarians;

  * why apples are good, but blackberries are ten times better; and

  * how oregano adds so much more to your marinara sauce than simply delicious seasoning.

### 2. Poor diet is the number one cause of premature death, and it’s often ignored by the medical industry. 

Today many people live to 100 and beyond, but living longer doesn't mean living _healthier_.

According to a study published in the _American Journal of Medicine_ of 42,000 autopsies of patients aged over 100, a large percentage died from diseases rather than simply old age, despite being assessed as healthy right up until their deaths.

How come?

The primary culprit is diet. Just take a "Nutritional Update for Physicians" published in the _Permanente Journal_ in 2013, which said that the meat, dairy, eggs and processed foods that dominate the typical American diet are bad news. The study found that people who ate diets rich in these food groups were at greater risk of heart disease, diabetes and a number of other chronic ailments. Not just that, but excessive animal fat and processed meat also resulted in high cholesterol and an elevated chance of heart disease.

If you don't accept that, just compare the American diet to that of another nation. After adopting US eating habits, Japanese-Americans are at as great a risk of suffering a heart attack at 40 as their Japanese counterparts are at 60.

So, food is the issue, and the American medical profession is sorely lacking in nutritional knowledge. It's so bad that only 25 percent of all US medical schools offer even _one_ course in nutrition, marking a 37 percent decline from just 30 years ago.

This is no accident. In 2001, a bill was introduced in California that would require physicians to receive 12 hours of nutritional training over four years, but the California Medical Association opposed it. The California Medical Board requires all doctors to receive 12 hours of training in pain management and end-of-life care but has less interest in using nutrition to stop people getting sick.

Instead, doctors are being taught to prescribe drugs. As a result, the US market is responsible for one-third of the $1 trillion plus spent annually on prescriptions worldwide, and around 70 percent of Americans take a prescription drug regularly.

### 3. A plant-based diet can provide better treatment than pharmaceuticals. 

In 2014, the _American Journal of Clinical Nutrition_ published an article which found that vegetarians of 12 years who began eating meat one day per week saw a drop in life expectancy of 3.6 years.

So are vegetarians healthier?

Well, cultures that eat plant-based diets certainly experience much less disease than we do in modern American society. Just take the China-Cornell-Oxford Project of the 1980s. It studied the eating habits of rural Chinese people and found an inverse correlation between eating a plant-based diet and experiencing heart disease.

In Guizhou province, where rates of animal-based food consumption were among the lowest in the country, not one death among men under 65 could be attributed to coronary disease.

Not just that, but plant-based diets can even help _reverse_ disease. The body has a tremendous capacity to heal itself given favorable conditions. As a result, within around 15 years of quitting, a smoker can enjoy lungs that are as healthy as if she had never smoked a day in her life.

Lifestyle-medical pioneers Nathan Pritikin and Dean Ornish put patients with advanced heart disease on plant-based diets similar to those traditionally enjoyed by some populations in Asia and Africa, hoping to slow the progression of their diseases.

But the heart disease of their patients didn't just slow down. It reversed. The patients saw marked improvements in their conditions, and their bodies began dissolving the plaque that had built up in their arteries.

Nonetheless, doctors find it easier to prescribe drugs than to change a diet, even though medication poses potential threats. _Lipitor_, a statin drug meant to lower cholesterol, is the best-selling prescription drug of all time. Its potential side effects include liver and muscular damage as well as higher chances of developing diabetes. Despite these dangers, some US health authorities think it should be added to public drinking water like fluoride. Meanwhile, plant-based diets have been shown to be just as effective and come with none of the risks.

### 4. Fruits, especially berries, help deter cancer and boost the immune system. 

Everyone has heard the saying, "an apple a day keeps the doctor away." But is that true? Well, if you throw in a handful or two of berries, it just might be!

A healthy diet should contain four servings of fruit a day, one of which should be berries. Keep in mind that means _whole_ fruit and not fruit _juice_. Harvard research has found that drinking fruit juice correlates to a higher risk of type-2 diabetes because of its high ratio of sugar to fiber, whereas whole fruit has been correlated to a lower risk of the same condition.

Beyond that, eating fruit has been found to improve lung function. Just a single extra serving of fruit per day could result in a 24 percent decrease in the risk of Chronic Obstructive Pulmonary Disease, or COPD. This is due to fruit's rich antioxidant content, which limits cellular damage and reduces inflammation.

And don't worry about the natural sugar in fruits causing weight gain. Only the fructose in _added_ sugars is associated with health problems. The fiber, antioxidants and phytonutrients in fruit itself can combat the adverse effects of fructose. Natural sugar from fruit can even balance insulin spikes caused by foods with a high-glycemic index, like white bread.

So, fruit is great for you and berries are especially important. They're known for their immune-boosting functions, their cancer-fighting abilities and ability to prevent diseases of the liver as well as the brain.

Take a 2014 study of 14 patients with hereditary colon polyps. After just nine months of eating black raspberries, the polyp load of these patients was halved.

Interestingly, this incredible antioxidizing power is believed to come from their pigmentation, and berries are second only to herbs and spices as the most antioxidant-rich foods, with an average of ten times more antioxidants than other fruits. For comparison, apples contain around 60 units of antioxidants, while a cup of blackberries contains 650!

> _The study of the teeth of ancient civilizations, where no toothbrush or floss was ever used, found almost no cavities. Candy bars hadn't been invented yet._

### 5. Vegetables are vital to a healthy diet. 

Do you remember how, when you were a child, your parents wouldn't let you leave the table until you'd finished all your veggies?

Well, all those leafy greens may have extended your life, since vegetables are on the front line of preventing life-threatening diseases.

In fact, whole vegetables, as opposed to processed ones, have been found to protect cellular _telomeres_ — nucleotide "caps" that keep DNA healthy as cells divide and age. Not just that, but vegetables like broccoli and cabbage can aid liver and lung function while cutting the risk of lymphoma and prostate cancer.

Over ten days, a 2010 study fed longtime smokers 25 times more broccoli than is consumed by the average American. When tested after the tenth day, this group's blood contained 41 percent fewer DNA mutations than that of the smokers who didn't eat broccoli.

What about kale, also known as the "queen of greens"? Kale might even reduce cholesterol levels. There's a 2008 study in which 30 men with high cholesterol drank three to four shots of kale juice daily for three months. The level of bad cholesterol in these subjects was dramatically reduced, while good cholesterol was raised by the equivalent of running 300 miles.

So, some vegetables are especially powerful, and others are pretty good for you too. Eating five servings of veggies a day is the magic solution. Out of these five, two should be leafy greens like kale, arugula and chard. One should be a cruciferous vegetable like broccoli, cabbage or cauliflower. And the other two should include carrots, beets and mushrooms.

Cruciferous vegetables are essential as they produce _sulforaphane_, a potentially powerful, anti-inflammatory, cancer-fighting molecule. These vegetables are best eaten raw since the enzyme that activates sulforaphane is destroyed by heat.

Chopping them up before cooking is fine, however, since doing so activates the enzyme, thereby forming sulforaphane. After just 40 minutes, the molecule is preserved, and the vegetable can be safely cooked.

Dark leafy greens are important since they contain the most nutrition per calorie of any food on earth. If you don't like them, just try blending them into a fruit smoothie.

### 6. Beans and whole grains are great for your health. 

Beans get a bad rap for the, uh, unpleasant side effects they produce, but actually, they're super nutritious. The American Institute for Cancer Research recommends eating a serving of beans or legumes with every meal since they offer an animal-free protein with added benefits, like fiber.

Soy is the most popular bean in America, but processed versions of soybeans, like tofu, should be avoided. Tempeh, young whole soybeans or other whole soy-based foods are better options.

And if you don't like soy, no problem! Navy and pinto beans can lower bad cholesterol just as much as soy. Lentils are a good choice for legumes as they can cut a sugar spike, even hours after a meal. Beyond that, they're loaded with prebiotics, can relax the stomach and slow the rate of sugar absorption.

If you're pressed for time, canned beans are just as healthy as dried ones, with one exception: many makes of canned beans use large amounts of salt but rinsing the beans removes key nutrients along with the brine. That's why it's important to get sodium-free versions.

So, you should eat beans three times a day, and the same goes for whole grains, which can deter debilitating diseases. A 2015 analysis found that people who eat whole grains — whatever their other dietary habits — live longer lives.

Whole grains can cut the risk of heart disease, type-2 diabetes, obesity and even strokes. What's even more exciting is that the healthiest ones are the most colorful, because the more color a grain contains, the higher its antioxidant content.

Now, you might be having nightmares about stodgy whole-wheat pasta, but new technology has improved its taste, and there's nothing to fear. Even popcorn is a good choice as long it's air popped, and you skip the butter.

And remember, packaged grain products that are labeled "multi-grain" or "stone-ground" are _not_ whole grains. That's why it's important to check nutritional labels and confirm that the ratio of carbohydrates to fiber is five to one or lower.

### 7. Nuts and seeds are super nutritious. 

Did you know that a single daily serving of nuts or seeds can make all the difference in fighting disease?

The Global Burden of Disease Study carried out between 1990 and 2010 found that eating too few seeds and nuts was the third leading dietary risk for death and disability worldwide.

Consumption of nuts and seeds could save 2.5 million lives annually, and researchers have found that a single serving of brazil nuts is capable of lowering cholesterol levels even faster than statins!

Not just that, but the _phytates_ found in nuts and seeds detoxify excess iron from the body, which can create free radicals linked to colorectal cancer. While phytates used to be considered inhibitors of mineral absorption, they've since been found to increase bone density.

And don't worry about the calories. While nuts and seeds may be high in them, clinical studies have found no weight gain when these foods are added to a diet. The body likely fails to absorb some of this fat, and, in any case, nuts boost the body's fat-burning metabolism.

So, nuts and seeds are great for you, and there are many to choose from. The most recommended seeds are chia, hemp, pumpkin, sesame and sunflower. A single serving is a quarter cup of straight seeds or two tablespoons of seed butter.

If you want other ways to incorporate nuts and seeds into your diet, try adding them to sauces and dressings. Just take tahini, a sesame seed paste that makes a rich addition to salad dressings. Or consider peanuts; while they're technically a legume, most health studies lump them with other nuts. Peanut butter is a classic treat and a surefire way to get a kid to eat celery.

Regarding nuts proper, walnuts are the most nutritious. They're even among the most antioxidizing foods and boast high levels of omega-3s.

Beyond that, pistachios can even serve as a substitute for Viagra! Studies have found that three to four handfuls of these tasty nuts can increase blood flow to the male genitalia, decreasing erectile dysfunction.

### 8. Herbs and spices make healthy foods even more beneficial. 

Herbs and spices can add flare to a dish, but these fragrant flavorings are also powerful fighters of disease, especially cancer.

They contain the highest levels of antioxidants of all food groups. A bowl of whole wheat pasta, tomato sauce and broccoli contains 150 units of antioxidants. Add just a single teaspoon of oregano to the mix, and that number jumps to 300!

Or consider a 16-week study of Alzheimer's patients in 2010. It found that participants who took saffron had much better cognitive function than those who received a placebo.

Beyond that, spices like cloves, cinnamon, oregano and nutmeg inhibit an enzyme known as _monoamine oxidase_, which can spark depression. This is great news since the drugs that do the same thing have serious potential side effects, like brain hemorrhages.

But of all the herbs and spices, turmeric is the best, especially for cancer. Turmeric contains _curcumin_, a molecule that gives the spice its deep yellow hue and seems to make it effective in the treatment of colon, lung and pancreatic cancer. A quarter teaspoon of the spice, dried or fresh, is recommended daily.

While turmeric is eliminated from the body quite quickly, simple black pepper can suppress this process. The proof here is in the curry. Turmeric and black pepper are core ingredients in Indian curry powder, which clearly has health benefits: American women have ten times the rate of colorectal cancer and 17 times the rate of lung cancer of their Indian counterparts.

That being said, turmeric supplements are best avoided. These capsules focus solely on curcumin, and there's no proof that this molecule is the only disease-fighting derivative in turmeric.

And turmeric isn't for everybody. People with gallstones and, to a lesser degree, those with kidney stones, should limit their intake. That's because turmeric encourages the pumping action of the gallbladder, which can cause pain in sufferers. Beyond that, turmeric is rich in _oxalates_, which can play a role in the most common type of kidney stones.

> _Cilantro can help reduce inflammation levels in arthritis sufferers and those suffering from gout._

### 9. When it comes to beverages, water reigns supreme. 

Imagine you're absolutely parched. You open the fridge door and grab the nearest beverage to hand. Unless you're reaching for a bottle of water, chances are your choice isn't going to be very healthy.

Ideally, you should aim for at least five 12-ounce beverages per day, and there's nothing better than pure H2O. It's so good for you that the Beverage Guidance Panel, which publishes nutritional information on drinks, lists water as number one on its six-tier scale.

Many articles claim that people should drink eight glasses of water per day, but there's little scientific support for this suggestion. And if you hate water, remember that it can come from a variety of sources like other beverages or fruits and vegetables.

So, water is essential, but a couple of other beverages are also good choices. The first is coffee, which benefits the liver and the brain. Those who drink more than two cups of java per day experience half the risk of both chronic liver issues and suicide.

The second is tea, which can have powerful medicinal effects. Just take a Tufts University study which found that hibiscus tea significantly dropped blood pressure in prehypertensive patients more effectively than a placebo.

But while water, coffee and tea are all good choices, just about every other beverage is a bad one. First off, milk is a big no-go. The Beverage Guidance Panel has even ranked it alongside beer, recommending zero ounces per day. They argue that milk has been linked to prostate cancer.

Nobody should be surprised that soda ranks last on the scale, and beer, while it accounts for the fourth largest source of antioxidants for average Americans, still contains 100 times fewer antioxidants than fresh blueberries.

While moderate drinking — one drink a day for women and two for men — has benefits for heart disease, booze is also correlated with cancer. Not just that, but according to a 2008 study, the only people who saw benefits from alcohol were those who lived sedentary and unhealthy lives.

### 10. Exercise coupled with a healthy diet is the recipe for a long life. 

Do you remember a time when kids played outside all day long? Plenty of things have changed now that kids sit around playing video games for hours.

This shift has been terrible for health; our sedentary lifestyles are major killers. Just consider a 14-year study by the American Cancer Society. It followed over 100,000 Americans and found that men who remained sedentary for six hours per day or longer were 20 percent more likely to die than those who sat around for three hours or less. The same numbers even held up for those who ran or swam for an hour a day!

Such sedentary habits have produced a situation in which over two-thirds of all American adults are overweight, and childhood obesity has tripled in just 30 years. When we compare the eating habits of today with those of the 1970s, we find that Americans would need to walk two hours more every day just to burn off the extra calories they're consuming.

While healthy eating is essential, its benefits can only be increased by adding exercise to the mix. The daily recommendation is 90 minutes of moderate or 40 minutes of intense exercise. Moderate choices include hiking, fast walking and swimming recreationally, while vigorous ones include basketball, circuit weight training and tennis.

However, these recommended numbers contradict the guidelines issued by the US Office of Disease Prevention and Health Promotion, which claim that just 20 minutes per day is sufficient. Rather than abiding by the science, these recommendations suggest what they think is realistic. Something, they calculate, is better than nothing.

When planning your workout regimen, just remember a study published in the _International Journal of Epidemiology_ in 2011. It found that a simple hour of walking per day can cut your mortality rate by 24 percent.

### 11. Final summary 

The key message in this book:

**Switching to a plant-based diet can help you live longer and more healthily. Many of the debilitating diseases we suffer from today are merely a result of eating animal-based foods. By changing your diet, you can prevent or even reverse conditions as serious as heart disease or cancer.**

Actionable Advice

**Stay on track with your balanced plant-based diet by cooking creatively and monitoring your eating habits.**

By consuming more nuts, seeds, spices, whole grains and fruits, you'll find yourself eating a balanced, nutritious and interesting diet that you enjoy. Keep a daily log of what you eat, marking down each essential food group and the number of servings you consume. You can use the author's app — Dr. Greger's Daily Dozen — for this very purpose!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Wild Diet_** **by Abel James**

_The Wild Diet_ (2015) is your guide to using the biology of fat-burning to lose weight. These blinks explain what makes high-intensity exercise, plant-based, protein-rich diets and hydration so effective, and provide you with health hacks that you can start applying right away.
---

### Michael Greger, MD, and Gene Stone

Michael Greger, MD, is an internationally renowned doctor, author and speaker. Additionally, he serves as director of public health and animal agriculture at the Humane Society of the United States and runs the website NutritionFacts.org.

Gene Stone is the author of several books on plant-based nutrition, including the bestseller, _Forks Over Knives_.

