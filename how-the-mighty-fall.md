---
id: 53aa82d36337660007070000
slug: how-the-mighty-fall-en
published_date: 2014-06-24T00:00:00.000+00:00
author: Jim Collins
title: How the Mighty Fall
subtitle: And Why Some Companies Never Give In
main_color: F13044
text_color: B22432
---

# How the Mighty Fall

_And Why Some Companies Never Give In_

**Jim Collins**

In _How_ _the_ _Mighty_ _Fall,_ influential business expert Jim Collins explores how even successful companies can suddenly collapse, especially if they make the wrong decisions. He also offers leaders advice to prevent them from making the same mistakes.

---
### 1. What’s in it for me? Learn how businesses come back from decline. 

Have you ever wondered why some big companies are there one minute and gone the next? Or why brands you saw all over the place not too long ago seem to have vanished into thin air? Cell phone manufacturer Nokia and photography company Kodak spring to mind. Just where have they all gone?

_How_ _the_ _Mighty_ _Fall_ explains why some companies go from massive success to crushing failure. The author argues that the failure of successful businesses isn't due to the changing economic climate or bad luck, but to their leaders who steer them in the wrong direction and exacerbate crises through mismanagement.

The book is based on years of research on successful and not-so-successful businesses. It follows the best-selling works _Built_ _to_ _Last_ and _Good_ _to_ _Great_, and shows you what type of leader a business needs, and what kind it definitely needs to avoid.

In these blinks, you'll learn

  * why nobody has a Nokia anymore,

  * where it's OK to make a hole in a boat and

  * why one company tried to launch a product a day!

### 2. Every company and institution, no matter how great, is vulnerable to decline. 

In the first few centuries after the birth of Christ, the Roman Empire dominated Europe and the Middle East. It was massive, stretching from Portugal in the West to Iran in the East. Among those alive back then, there would have been no doubt: this gigantic, all-powerful empire would last forever. Alas, it did not: within just a few centuries it was gone, banished to the trash heap of history.

The decline of the Roman Empire shows that, no matter how vast or successful something is, it will always be in danger of collapsing.

But what causes decline? What can make a huge entity come crashing down?

The decline of a large organization is _always_ self-inflicted: collapse isn't the fault of outside factors or bad luck; it's the direct result of mismanagement.

Remember how everybody had a Nokia phone 15 years ago?

In the years since, the company has gone from being the market leader to having a mere 3 percent market share. Why?

While their competitors, such as Apple and Samsung, were researching and innovating in smartphone technology, Nokia's management decided to innovate in other, less profitable areas. Thus, when smartphones became popular, Nokia was completely out of the race.

At this point, it's important to point out that we're not talking about laziness. Successful firms very rarely collapse because they fail to act: usually it's because they act in the wrong way. In fact, failing firms often show very high levels of innovation and energy.

Bank of America is a great example of this: in the 1980s, the bank was eager to update outmoded practices. They hired a young, energetic CEO, they closed huge swathes of loss-making branches and they ended the practice of hiring people for life. Yet throughout this period, the company posted some of the most spectacular losses in banking history.

### 3. After experiencing great success, companies become arrogant and tend to succumb to hubris. 

You may or may not be familiar with Greek tragedy.

In Greek tragedies, we often find the heroes in trouble after succumbing to _hubris_, i.e., they're so overly confident that they strive for too much and end up failing.

But the ancient Greeks aren't unique in their susceptibility to hubris: successful companies also run the risk of letting their confidence get the better of them.

Take cell phone manufacturer Motorola. Between the late 1980s and early 1990s, Motorola enjoyed a period of fantastic success and their annual revenues rocketed from $5 billion to $27 billion.

A success which led to massive overconfidence and big mistakes.

One such mistake was the development of the StarTAC cell phone. The StarTAC had a problem: it used an analog system at a time when other phone manufacturers were starting to go digital. Instead of stopping production of an outdated product, Motorola succumbed to hubris: it pushed on with StarTAC, boasting that "43 million analog customers can't be wrong." Of course, the phone failed and Motorola's market share plummeted from 50 percent to 17 percent.

Another danger from overconfidence is _arrogant_ _neglect_, i.e., when companies lose interest in their core business.

When companies become successful, they often look to branch out from their main market into new areas. Sometimes their desire is so strong that they forget about what brought them success in the first place.

This is what happened to Circuit City, a consumer electronics corporation. After achieving a lot of success, they started exploring other markets, from selling used cars to renting DVDs. In order to be successful in these areas, they invested a lot of time and resources — so much so, in fact, that the consumer electronics business was left to rot.

Losing this main area of business hit the company hard, and it eventually went to the wall.

> _"Motorola fell from being the #1 cell phone maker in the world to only having 17 percent share by 1999."_

### 4. Companies can lose their focus if they chase unsustainable levels of innovation and growth. 

Why do successful companies fail? Is it out of laziness?

Actually, most of them fail for the complete opposite reason: they try to be too innovative and grow too quickly.

When companies push for unsustainable levels of innovation, they fail to maintain good business practices, such as keeping costs down or concentrating on profitable markets. And a company that forgets the basics can never succeed.

Household items manufacturer Rubbermaid is a great example of this. Once declared by Fortune magazine to be America's "most-admired company," it has since fallen out of popularity.

Rubbermaid was admired for its high levels of innovation, yet the company took it to an extreme, aiming to introduce one new product to their range _per_ _day_ : a strategy that led them to create nearly 1,000 new products in just three years.

But all this innovation came at a cost. In pushing so hard for new products, they totally lost control of their costs and constantly failed to meet their orders. Their lack of discipline undermined their innovations and they suffered a rapid decline until they were eventually taken over by a rival. 

Companies can also overreach by looking to grow too fast.

This is especially dangerous for publicly listed companies with lots of shareholders to please, often under immense pressure to chase profits as quickly as possible.

For example, before the financial crisis of 2008, banks chased quick profits at all costs. This led them to borrow heavily, invest in risky, yet highly profitable products, and ignore costs.

While striving for growth in this way brought them short-term rewards, overreaching left them massively at risk in the long term. When the system failed in 2008, many of them incurred major losses and some even went bankrupt.

### 5. As the first visible signs of decline occur, companies choose to ignore it or blame others. 

What do we do when someone criticizes our heroes? Do we take their comments into consideration? Or do we ignore their criticisms and continue to believe our heroes are perfect? Usually, when someone tells us something we don't want to hear, we shut out their views and put our _rose-colored_ _glasses_ on.

Companies can also fall into this trap. And in their case, ignoring valid criticisms can lead them to gigantic mistakes.

This is what happened to Motorola. In the 1980s, the company was a huge force in the cell phone market. At the time, cell phones were very expensive and wouldn't work in certain locations. Motorola thought they had the perfect solution: a satellite phone that would work anywhere.

Over the next few years, they started developing a satellite phone named Iridium. But, as they were developing it, the quality of normal cell phones grew and their cost plummeted. It soon became clear that the quality of a cell phone would far outclass Iridium. So what did Motorola do? They put on their rose-colored glasses and ignored the criticisms.

When Iridium finally came out, it was pricier and lower in quality than the competition and, not surprisingly, it failed. In total, the project cost Motorola $2 billion, most of which could have been salvaged had they stopped it when they had the chance.

However, ignoring criticism is just one way that companies fail to deal with bad news. Firms also seek to blame their own failures on others.

The best way for any company to deal with bad news is to use it in a constructive, practical way. Yet so many firms don't do this. They prefer to simply blame their situation on outside factors and continue on the wrong path.

> _"Rather than accept full responsibility for setbacks and failures, leaders point to external factors or other people to affix blame."_

### 6. Facing the catastrophe, companies tend to take risky, sweeping changes or simply give up. 

If you've ever been on a sinking boat, you know that the natural response to crisis is panic. It's also the response most business leaders have when decline sets in. Which, in turn, can lead them to make rushed decisions that only deepen the crisis.

One such response is to search for the _silver_ _bullet_, or one sweeping solution that will solve all problems.

Many firms think that the best solution is to do something completely different from the way they'd always done things. For example, they try to implement a new, unproven technology or change the culture of the business — or even look for a new market altogether.

Although these sweeping changes sometimes provide a short-lived boost, they're rarely able to reverse the decline.

A great example of the use and failure of a silver bullet can be seen by the actions of Hewlett Packard (HP) in the 1990s.

Faced with low growth, HP made sweeping changes to its business culture. First it got rid of its old, rather predictable CEO and replaced him with Carly Fiorina, a flashy, media-savvy figure who, through a series of advertisements and media appearances, sought to completely update the company's image. She thought that a more modern-looking company would do better.

But it didn't work. Too much change actually caused the company to lose drive and focus. HP's progress continued to falter and within a few years Fiorina was fired.

But there is a more drastic solution to decline than the silver bullet: a company can simply give up.

This is what happened to the once-proud company Scott Paper. After trying in vain to stop the rot, they ended up giving in. A new CEO was brought in to cut the company's losses, which cost it 11,000 jobs, and what was left of it was sold to their archrival.

### 7. Since decline is always self-inflicted, it’s important to work on improving your attitude. 

So far we've seen what leaders do wrong to put their businesses in danger. Now we'll look at what leaders should do to ensure that this never happens.

We'll start with the most important factor for making the best decisions: having the right attitude.

The right attitude starts with admitting that you're not as brilliant as you think you are. So many successful leaders think their achievements are purely down to them, a mindset that can lead them to become arrogant and single minded.

Instead, leaders must _always_ remember that success is at least in part due to luck. If they realize the role that fortune plays, it'll help temper their arrogant behavior.

The next step is for leaders to admit that there's always more to learn. Leaders usually fall into two categories: _knowing_ _people,_ who think they know everything already, and _learning_ _people,_ who never stop asking "Why is this happening?" and "What can this teach me?"

The most successful leaders are learning people. One example of such a leader was Sam Walton, the founder of Wal-Mart. On one occasion, some Brazilian entrepreneurs came to him to ask about how to do business. However, their roles soon reversed and he ended up wanting to learn from them!

Finally, a leader should never let hubris take hold, and instead remember where they came from. Only when leaders remember this can they stop their companies from overreaching: just remember the unfortunate case of Circuit City.

There's nothing wrong with entering new arenas and doing research on innovations as long as you stay focused on your core business: where your success came from.

### 8. If you find yourself falling, stay calm and disciplined instead of taking massive risks. 

Taking risks is something all business leaders will have to contemplate in their careers, especially when their business is struggling. There's nothing wrong with adopting a risky strategy as long as you remember that it might fail.

When thinking about taking a risky strategy, you need to ask yourself, "What will happen if things go wrong? Will the consequences severely hurt the company or will we be able to recover?"

A good way to understand what risks are worth taking is to consider the _waterline_ _principle_. Imagine you're on boat and a hole is blown in the hull every time you make a wrong decision. Whether the hole is above the waterline, where repairs can be made, or below it, where water will rush in and sink you, depends on the decision you make. You need to ensure that you only take risks whose damage you can repair, never those that will sink your boat.

So, you now know that you shouldn't take dangerous risks, but how do you know which risks are most dangerous?

The general rule of thumb is to never make sweeping changes to how you do things. Risking massive alterations to your business strategy will only lead to instability and lack of focus. Remember HP: it hired a new CEO who made extensive changes to its culture and image, but the changes only led to stuttering performance as the company lost its business discipline.

Instead of making big changes, you need to take small steps. If you have to take a risk, make it a small, manageable risk. If it's a success, you can build upon it with another.

> _"Most overnight success stories are twenty years in the making."_

### 9. With the right determination and willpower, even fallen companies can recover. 

When you're in deep trouble and it seems like there's no way out, the natural temptation is to give up and walk away. Companies in dire straits often feel this way, too, seeing surrender as the only option to their plight.

But there's no reason to react this way: a firm can turn things around until that fat lady starts singing. That is: if they act the right way.

The first thing business leaders must do to reverse decline is adopt the right attitude. They need to believe in themselves and their company even if everybody else thinks they're done for.

Winston Churchill is a great example of someone who believed in himself when no else did. In the early 1930s, he found himself out of government, blamed for causing Britain's poor economic position, and, to top it all off, he was suffering from depression. Everybody thought he was a spent force.

Yet Churchill never gave in and, at the start of the next decade, he was the British prime minister and a war leader. The rest, as they say, is history.

Bouncing back from failure doesn't only depend on the right attitude but also a lot of hard work.

Photocopying company Xerox did just that when they realized just how deep the trouble was that they were wading in: their stock value had dropped an incredible 92 percent in less than two years. And so, Anne Mulcahy came on board as the new CEO and immediately got to work. In four years, she didn't take a single weekend off in the battle to make the firm profitable again.

Her rigorous schedule and devotion to the cause eventually paid off: only five years after it looked like it was going under, the company posted an annual profit of $1 billion.

> _"As long as you never get entirely knocked out of the game, there remains always hope."_

### 10. Final summary 

The key message in this book:

**Any** **company,** **no** **matter** **how** **big** **and** **powerful,** **can** **experience** **failure** **and** **decline.** **And** **often,** **when** **decline** **sets** **in,** **it's** **exacerbated** **by** **the** **company's** **panic-driven** **actions.** **To** **avoid** **this,** **leaders** **must** **stick** **to** **good** **business** **practices,** **try** **to** **stay** **calm,** **and** **continue** **to** **do** **what** **the** **company** **does** **best.**

Actionable advice:

**Take** **baby** **steps.**

Whatever you want to achieve, never try and leap too far: you'll have a much higher chance of succeeding if you advance step-by-step.

**Your** **failure** **is** **no** **one** **else's** **fault.**

If something is going wrong, don't ignore it or blame someone else: look at why it's happening and see how you can solve it yourself.
---

### Jim Collins

Jim Collins is a best-selling business expert and author, having written the highly successful works _Good_ _to_ _Great_ and _Built_ _to_ _Last._ He contributes to _Harvard_ _Business_ _Review,_ _Fortune_ and _Businessweek_, and he advises business leaders in the social and corporate sectors.

