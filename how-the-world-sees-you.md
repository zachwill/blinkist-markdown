---
id: 575935c3496d1c0003e86299
slug: how-the-world-sees-you-en
published_date: 2016-06-15T00:00:00.000+00:00
author: Sally Hogshead
title: How the World Sees You
subtitle: Discover Your Highest Value Through the Science of Fascination
main_color: E15D2D
text_color: AD4823
---

# How the World Sees You

_Discover Your Highest Value Through the Science of Fascination_

**Sally Hogshead**

_How the World Sees You_ (2014) offers a window into yourself, helping you discover what fascinates you and how you can be fascinating to other people. Find out how to use your unique personality to improve your performance and your chances of success, and learn how to read those around you so you can put together the perfect collaborative team.

---
### 1. What’s in it for me? The path to success comes by being more like yourself. 

Are you eager to succeed and make more of your talents? Chances are, then, that you've already consulted a few self-help books or manuals on how to maximize your skill set. These manuals probably advised you that, first of all, you have to change in some fundamental way. If you're an introvert, you should become the life of the party in six simple steps; if you're a creative, somewhat chaotic person, you were probably told to stay organized, keep your desk clean and follow strict schedules.

Well, did it work? Not likely.

This is exactly the approach you won't find in these blinks. The truth is, you don't need to change at all! The key to being successful is to find out who you really are and then create the perfect niche that matches your unique traits and talents.

In these blinks, you'll find out

  * the connection between an unnamed Mesopotamian from 3500 BC and Steve Jobs;

  * why you might be a bit like a long-last artistic masterpiece; and

  * how your efforts to sell your groundbreaking ideas are like the attention span of a goldfish.

### 2. You’ll only succeed in the right environment, and some indicators can tell you if you’re there or not. 

Perhaps you've heard this story: a priceless painting is sitting up in some forgotten attic, collecting dust for decades. Then, one day, someone stumbles upon it and sets off a million-dollar bidding war. Do you ever feel like that painting, waiting for someone to recognize your potential?

The problem is, you have to be in the right environment for people to notice your unique talents and the great things you have to offer.

Take Joshua Bell. He's one of the world's most famous violinists — but even a musical genius needs the right environment.

He participated in an experiment in which he busked anonymously in a Washington, D.C., subway station. While he played, the people passing through the station were naturally caught up in their daily routines and failed to notice his amazing talent. In the end, he only made $32.12 whereas, under normal circumstances, he could easily earn $45,000 for a single performance.

So, whether it is a priceless painting, Joshua Bell or you, without the right environment your talent will go unnoticed. You need to stay on the lookout for the kind of environment that will maximize your unique contributions.

Luckily, there are some simple indicators that can tell you when you've found it — or if you might already be there.

A good sign is when your boss approaches you to ask for your feedback and advice. The same goes for when clients decide to work with you rather than with more affordable competitors; this shows you that your good work is being recognized.

On the other hand, you might not be in the right environment if your emails and calls are going unreturned. And if you're self-employed, it should raise a red flag if you have to attract clients by offering the lowest price.

So, finding the right environment is crucial to success, and in the next blink we'll take a look at the obstacles that can stand in your way.

> "_You will not make a difference by being quiet. You only make a difference by being heard."_

### 3. To succeed, you need to overcome distraction, competition and the threat of being a commodity. 

Let's say you've finally finished that novel you've been working on. How can you get it published and get on your way to becoming a famous author?

There are three things you must be aware of.

First, you have to overcome the distractions that can keep people from connecting with your ideas.

It's difficult to get someone to pay attention to your ideas if they don't listen or bother to look at the portfolio you sent them. So, to get your message out there, you have to grab their attention right off the bat.

Sadly, the average attention span has shrunk to a mere _nine seconds!_ So, if you want to have an impact on your audience, you have to act quickly and start strong. And if you're trying to sell that novel, one way to grab a publisher's attention is to start the book off with a memorable and captivating opening.

Second, avoid the competition by creating your own niche.

There are plenty of competitive entrepreneurs out there today, each fighting to be the best in their field. But no matter how hard they try, there will always be someone better.

You're best off avoiding the desire to outperform everyone by trying to be the world's best. Instead, you should aim for success by being _different_ and creating a niche that plays to your own unique set of skills. Rather than trying to top Ernest Hemingway, embrace your own quirky writing style or subject matter and attract readers by setting yourself apart from the rest.

Finally, avoid being perceived as a commodity.

Commodities are replaceable and thus, if you're perceived as one, you'll end up losing clients to competitors. To find long-term success, you need a loyal customer base; otherwise, you'll always be searching for new ones, which is a costly and time-consuming process.

To retain your customers, become the opposite of a commodity — become _someone_ _special_ to them.

You can do this by connecting with your customers and showing them that you care. If you encounter a complaint, respond personally. And if you get a letter from a reader, reply thoughtfully and start building a loyal fan base.

> "_Different is better than better."_

### 4. When you are fascinated, you are in a state of flow and perform at your best. 

Are there things in your life that fascinate you and allow you to forget the world around you?

Fascination allows you to enter a state of _flow_, a psychological state in which you can focus on something completely. The stronger your fascination, the more likely you are to enter into this state.

_Flow_ was discovered by psychologist Mihaly Csikszentmihalyi, who defined it as being completely, blissfully and effortlessly immersed in an activity.

And this could be any activity: solving a crossword puzzle, doing woodwork or having a deep conversation with a friend.

More importantly, being in a state of flow allows your brain to perform more efficiently, which improves your performance.

Neuroscientists have shown this by comparing the brain activity of expert golfers and novices as the two groups imagined performing a golf swing. As they took their imaginary swings, the experts were able to focus effortlessly and enter the state of flow.

This same efficiency appeared in the experiment's brain imaging: the experts showed relatively low brain activity while the novices struggled hard to focus on the task and, consequently, showed relatively high brain activity.

If a simple job already taxes your brainpower, as it did with these novices, more demanding tasks can overwhelm you, leading you to make mistakes or crumble under the pressure. But someone who is fascinated with what they do will enter a state of flow and will still have enough spare brainpower to meet a challenge.

And fascination can be contagious as well.

You may have heard of actors and musicians saying they are at the top of their game when they perform alongside the best in their field. This is because fascination and flow are contagious to those around us.

Of course, the activities that are most likely to fascinate you are those that suit your unique personality traits. Let's take a closer look at this important factor in the next blink.

> _"Fascination is … a neurological state of intense focus, one that creates an irresistible feeling of engagement."_

### 5. Knowing about fascination advantages can help you succeed, and the first one is power. 

Were you ever scolded as a child for doing what came naturally? Perhaps you were too bossy, too cautious or too hyperactive. But childhood traits like these could prove to be the foundation for your greatest strengths, or your _fascination advantages._

There are seven fascination advantages and each of them has a unique way of helping you perform better.

Many people struggle to become someone they're not — to try and get a job or fit into a role that doesn't suit them. To prevent this from happening to you, recognize one or two of your personal fascination advantages to discover the work that fascinates you and matches your personality.

As we've already seen, we're much more likely to excel at a job that fascinates us.

In addition, being aware of other people's fascination advantages and personal traits can help you set up productive teams. This way, you will know how each of your employees can contribute specific skills and strengths to a team.

For example, one person might be more detail oriented and another more of a promoter; knowing their traits, you can position them so that they can both excel and achieve great things for you, too.

The first fascination advantage is called the _power advantage._

People with this advantage are very confident and excel at convincing others of their opinion. They are willing to take calculated risks and won't shy away from making difficult decisions.

While the power advantage is helpful in many situations, it's crucial to a role like that of a CEO, where difficult decisions and convincing others is part of the daily routine.

Be careful, though: having too many people with a power advantage in one team can be disastrous, with every group discussion becoming a fight for control.

But the power advantage is just the first of many. In the next blink, we'll take a look at the next two fascination advantages.

> _"You don't learn how to be fascinating. You unlearn how to be boring."_

### 6. The passion advantage is about people and emotions, while the mystique advantage is about loving facts. 

Do you know someone who's always the life of the party? These are the kinds of people who exude charisma and have that magnetic personality that draws people to them. If so, chances are you know someone who has the _passion advantage_.

People with the _passion advantage_ are friendly, emotionally open and can connect quickly with other people.

Since they tend have high levels of energy, they're often enthusiastic about ideas or projects, and their enthusiasm can be contagious. In addition to having good social skills, they tend to rely on their intuition rather than on hard facts, and it usually works well for them.

People who are high in passion are usually very good at giving presentations and forming relationships with customers. Though they can have difficulty hiding their emotions, their openness helps them communicate their message and persuade audiences.

At the other end of the spectrum, people with the _mystique advantage_ are level-headed observers who listen before they speak.

Mystique personalities are often quiet intellectuals who like to think things through before they act. In contrast to passionate personalities, mystique individuals aren't open with their emotions and you shouldn't be surprised if they reject your idea of joining in a karaoke duet at an office party.

These people have great time-managements skills and are very analytical. They like to make rational decisions and can excel at formulating business improvement strategies by studying performance data.

Mystique personalities make great experts, and if you're looking for someone to fill a scientific role, join the IT department or become a business analyst, look no further.

So, passion is about people and mystique is about data. Next, we'll investigate the main traits of perfectionists and project managers.

### 7. While the prestige advantage drives people to excel, the alert advantage is about caring for details. 

When you finish a project, do you always feel you could have done better? Then you might fit the bill for the next advantage.

The _prestige advantage_ is all about striving for excellence and the desire for self-optimization.

The most important motivation for people with this fascination advantage is to exceed expectations. Whether it's a work project, running marathons or hosting a dinner party, being "good enough" is not an option; these competitive individuals will never settle for second-best.

They'll often have a business hero or public person that they admire and model themselves after to become better.

When someone with a prestige advantage gives a presentation, it will usually involve the following:

First, they will study a previous successful presentation, such as Steve Jobs's presentation of the iPhone. Then, they will stand in front of a mirror and do a few training performances. Finally, after the presentation, they will conduct a self-evaluation, determine what went wrong and how they can improve.

But when it comes to attention to detail, no one beats the people who have the _alert advantage_.

The alert advantage involves careful precision and a passion for details, which makes these people the perfect project managers.

They also like to stay on time, within the budget and have an eye for details that might otherwise fall through the cracks. As you might sense, this trait comes in handy for anyone involved in quality management.

If someone you're about to meet sends you a text to say that they'll be five minutes late it's a sign that they have the alert advantage. This is the kind of advantage you might also look for in a doctor, so they won't miss any ominous signs during your next check-up.

### 8. The innovation advantage is about having new ideas and people with a trust advantage are very reliable. 

In Mesopotamia, somewhere around 3500 BC, someone came up with the brilliant idea of the very first wheel. While we don't know anything about the inventor, it's a safe bet that this person's fascination advantage was the _innovation advantage._

As the name suggests, the _innovation advantage_ characterizes people who are always coming up with new ideas.

These people are full of creativity and hold little regard for the status quo. Instead, they keep coming up with new ideas and ways to disrupt it.

Just about any great scientific and technological breakthrough can be traced back to people with this advantage, be it that unknown Mesopotamian, Nikola Tesla or Steve Jobs. While other people are busy trying to improve an existing product, innovators will work on developing a new one from scratch.

In leadership positions, these individuals tend to look at the big picture and excel at creating a vision for their team to fill with details. In this environment, they pair well with alert people, who would never miss a single detail that the innovator is after.

But if you are looking for consistency and dependability, look to those with the _trust advantage_.

It's always nice if someone in your team has the trust advantage because they work hard and will commit to getting things done. If they are asked to perform a task, you can be certain that they will do it well.

There are some tell-tale signs that suggest someone has a trust advantage. For instance, do you remember that classmate who always got straight As at school, even if they hated the subjects they were studying?

They had the ability to keep their head down and work hard to finish whatever task was at hand. You can also be sure that a colleague has the trust advantage if he likes to stick to routines, shows up at the office at the same time every day and never misses a deadline.

So don't hold back any longer! Find your environment, discover your fascination advantage and be extraordinary.

> _"You do not have to be perfect at everything. But you have to be extraordinary at something."_

### 9. Final summary 

The key message in this book:

**There are seven** ** _fascination advantages_** **, overarching personality traits people can use to help improve their performance and fascinate others. Everyone has one or two strong fascination advantages and identifying these traits will help you honor who you are and become more successful.**

Actionable advice:

**Know about your dormant advantage.**

While reading these blinks, you might have realized that some fascination advantages describe you very well while others don't describe you at all. These advantages are called dormant advantages and tasks that require you to exercise them are usually very exhausting. As a result, you might want to think about how you can change or even delegate these tasks. Remember, you don't have to be good at everything.

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Fascinate_** **by Sally Hogshead**

In _Fascinate,_ author Sally Hogshead helps us realize our potential for fascination. By explaining in vivid language exactly how fascination works and how you can trigger it in others, _Fascinate_ provides you, your company and your brand with the tools to fascinate. These "seven triggers of fascination" can help you to increase the odds of success, both in your personal life and in business.
---

### Sally Hogshead

Sally Hogshead is an advertising expert who has appeared various times on NBC's _Today_ _Show_ and is a frequent contributor to _The New York Times_. Her other books include _Fascinate: Your 7 Triggers to Persuasion and Captivation_.

