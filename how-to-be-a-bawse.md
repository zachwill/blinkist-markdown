---
id: 5a968b0bb238e10008828405
slug: how-to-be-a-bawse-en
published_date: 2018-03-02T00:00:00.000+00:00
author: Lilly Singh
title: How to Be a Bawse
subtitle: A Guide to Conquering Life
main_color: C52743
text_color: C52743
---

# How to Be a Bawse

_A Guide to Conquering Life_

**Lilly Singh**

_How to be a Bawse (2017)_ offers major insights on how to achieve success in life. Drawing on firsthand knowledge from her rise to stardom, Lilly Singh, the YouTube sensation, covers topics such as motivation and discipline, and rounds them off with personal anecdotes and practical tips.

---
### 1. What’s in it for me? Learn how to be a bawse. 

Ever wondered how YouTubers get millions of subscribers, and then go on not only to meet celebrities but to become celebrities themselves? Well, now you can learn all the tricks of the trade from the YouTube sensation Lilly Singh, who's here to teach you how to become a _bawse_.

By following her tips and advice, you'll learn how to excel in any area of life — not only on YouTube. She lets you in on the ideologies and actions underpinning true success, so you have the tools necessary to stop dreaming and start becoming who you want to be.

In these blinks, you'll learn

  * what a _vision board_ is and how it can be useful;

  * why being the dumbest person in the room is a good thing; and

  * that, occasionally, you need to break the rules.

### 2. Become friends with your mind by improving your self-awareness. 

Your mind can be both a delightful asset and a difficult obstacle. Sometimes it generates brilliant ideas, but it may just as often hold you back, refusing to let go of old ways of thinking. Clearly, you want your mind to work for you, and that means you need to befriend it. And the best way to make your brain into your buddy is to develop self-awareness.

The first step toward self-awareness is recognizing the intricate layers of your inner issues. Each issue has at least three layers that need to be peeled back before you can reach the root cause.

To explain this strategy further, let's take a look at an issue the author is dealing with: she tends to act like she isn't currently interested in having a romantic partner. At the first layer, she tells people that she's too busy with work and building her career. At the second, she tells herself that the commitment of a relationship would impede her progress toward her goals. At layer three is yet another reason for her not wanting a relationship: she doesn't know how to balance her career aspirations and a healthy partnership.

But, at the fourth and deepest level, she avoids relationships because of her childhood experience. She was never exposed to healthy relationships, and this resulted in her believing that they only bring out people's ugly side.

If you want to resolve a personal issue, you must first dig beneath these layers and identify its exact root cause, all while being completely honest with yourself. Only then can you start investing time and effort into resolving it.

The second step toward self-awareness is conquering the thoughts and emotions that negatively impact you. Understanding why you feel certain emotions at particular times is important because it will put you in control of your feelings.

Here are some pointers on managing typical thoughts and emotions: First, when someone brings out a negative reaction in you, try not to take it personally. In all likelihood, there are issues in that person's life — issues that have nothing to do with you — that are influencing his or her behavior toward you. Second, remember that happiness is a better motivator than fear. And, thus, you should work toward something that makes you happy.

Identifying the root cause of your issue, and managing negative thoughts and emotions, are the two key steps in developing self-awareness.

> _"If you can't control people, then control your reaction to them."_

### 3. You can train your mind for success. 

If you want to be physically stronger, you head to the gym and hop on the elliptical or do a few reps on the bench press. Likewise, in order to strengthen your mind, you've got to train your brain — and the brain, like the body, has different sets of muscles. Here are three methods for exercising one of those muscles, your _self-control muscle_.

First, you need to set goals for yourself. For example, set the goal of finishing a task in a certain amount of time. Once you've reached that goal, try to beat it. Second, remember to reward yourself every time you reach your goal. You can try asking a friend to hold your phone hostage, and tell them not to give it back until you've finished the task you're working on — so getting your phone back is like a reward. The third method is challenging yourself to break a bad habit. If you're a nail-biter, try to abstain from that habit for 24 hours, resetting the clock every time you gnaw your nails before that time is up.

As important as it is to practice self-control, it is just as important to train yourself to hit pause. Every now and then, you've got to give your mental muscles a moment to recover.

The author learned firsthand that taking a break was the only way to keep up with a hectic schedule. Once, when at a celebrity retreat in Italy, she got so burnt out that during a face-to-face meeting with the musician and artist Pharrell Williams all she could think about was sleep!

Clearly, you don't want to miss out on a once-in-a-lifetime opportunity due to sheer exhaustion. So how can you stay sharp?

The answer is meditation. This doesn't mean you have to arise at daybreak or hire a guru. Meditation is flexible, and there are many different versions of it. So pick the practice that's best for you. For the author, the suitable meditative practice is sitting on the ground, cross-legged, in front of a candle. With her eyes shut, she does some deep breathing while smiling. Finally, she whispers to herself whatever's on her mind, and this process helps her unravel her thoughts and relax.

With plenty of practice, you'll have a powerful mind in no time!

### 4. Dreams can only be attained if you’re fully committed and have a vision. 

What do distraction and fear have in common? Here's what: they're the two biggest obstacles on your path to success.

To accomplish anything great, you need to fully commit, and that requires learning how to control any fears and distractions that may get in your way.

The author has faced her share of fear and distractions, but she didn't let them throw her off course or diminish her commitment. As an example, let's take her video shoot with Seth Rogen and James Franco. When the two actors arrived on set, she was initially distracted by her own nervousness. And then, when it was time to give her creative pitch, she was overcome with fear that they wouldn't find her idea humorous. But instead of letting it completely derail her, the author kept her cool and pushed through, and she managed to finish the shoot in 45 minutes.

How exactly did the author manage this kind of commitment? Well, she learned how to cast aside her fears in critical moments. She imagines her fears as a jacket that she can take off when she needs to.

Along with full commitment, another thing you'll need on your path to success is a vision.

A useful way to manifest your vision is to make a collage that illustrates your goal. After you've created this collage of images — let's call it a _vision board_ — place it somewhere you can see it, and it will serve as a regular reminder of your goals and lead you to act in a more goal-oriented way.

Your vision board is your brainchild, and you should protect it as if it were your real child. In other words, don't hesitate to make necessary sacrifices to keep your vision board safe, and discard plans that require replacing it. You need to focus all your energy on your brainchild. For the author, this meant focusing all her energy on becoming a famous YouTube personality.

Work out what your dreams are, and don't hold back on making them come true.

> _"Inspiration is the fuel for hustle."_

### 5. Development is a long-term game, for which you need a long-term perspective. 

Imagine your personal and professional growth as a growing empire. And remember: an empire without a strong foundation will inevitably fall.

Rome wasn't built in a day, and it's foundation probably wasn't, either — so, when building your own foundation for success, be prepared for it to take a while. There's no quick way to the top. Doing something outrageous or dangerous may make your YouTube video go viral, but that notoriety will vanish just as quickly as it appeared. In contrast, if you build your YouTube subscriber base over time, through hard, honest work, you'll create a loyal following.

To succeed, you'll also need to establish a foundation of knowledge, tools and personal skills. That means investing time, effort and money in yourself. As her success grew, the author invested money in a support team, starting with a manager — and this foresight paid off, enabling her to make more videos, land more brand deals and increase revenue.

Not only is having a strong foundation necessary for success, but it also frees you from stressful must-win situations.

Once you have a foundational track record of success, no single accomplishment, and no single failure, will make or break your career. This decreases stress — a known performance inhibitor — and makes it easier to relax and focus on performing at your optimal level.

Stress can definitely inhibit performance, but that doesn't mean you should avoid it entirely, since people with stress-free lives are much likelier to procrastinate. The best way to make your life just stressful enough is to set deadlines for yourself.

We all know that procrastination is detrimental to productivity, and yet many of us still tend to postpone things until the eleventh hour. This is where deadlines can save the day.

To illustrate the importance of deadlines, let's take a look at one of the author's dreams. She wants to be an actress. The author knew that the best way for her to achieve this goal was to be in Los Angeles, so she set herself a deadline: move there by December 1, 2015. This self-imposed deadline propelled her into action and she made the move.

Success isn't going to happen overnight, so start setting deadlines and buckle up for the long haul.

### 6. Surround yourself with the right people and distance yourself from unconstructive validation. 

Like inspiration, validation is something we all desire — and that means it can be very good at motivating us.

However, getting too much validation often spells doom for ambition.

It feels great to have someone validate you. Having your peers, family members or coworkers tell you how great you are is sure to put you in a good mood. But here's the problem: an overabundance of validation in your life can result in your no longer feeling motivated to seek additional validation. This is a problem because the desire for validation is often what drives people to work hard and strive toward their dreams. So, whenever you can, dismiss validation that's unwarranted or offered too often, since it may make you complacent and unmotivated.

In the author's case, she attributes some of her success to how her mother handled validation. Her mother offers validation very sparingly. For example, when the author signs a huge new deal, her mother simply says, "That's really good."

Furthermore, to be successful, you need to hang out with smarter and more experienced people than you. This may require leaving your comfort zone and seeking out people who you feel are more experienced, talented and knowledgeable than you. What you're looking for are people who can challenge, intimidate and teach you, because these are the people you'll learn the most from. You will have to leave behind your ego and accept that you're not the best, and instead focus on gathering knowledge from others.

Just ask Snoop Dogg and he'll tell you the importance of surrounding yourself with great people. The rapper credits his long-standing career to being the dumbest person on his team.

So if you want to enjoy success like Snoop Dogg's, make it your goal to be the "dumbest" person in the group.

### 7. Stand out from the crowd by being fully present and breaking the rules. 

How can you differentiate yourself from the throngs of other success-hungry people?

Well, to stand out, you need to break the rules for the right reasons.

This was exactly what the author did in order to meet her childhood hero, the actor and wrestler Dwayne "The Rock" Johnson. Having learned that it's considered inappropriate in Hollywood for celebrities to directly message each other to meet up, the author knew she couldn't grab a coffee with The Rock by simply sending him a text. And, as hard as she tried, neither she nor her team was able to make the arrangement. So she eventually decided to text him, Hollywood taboo be darned. And to her surprise, he replied to her message and they met up the very next day.

Another way to stand out is to be fully present.

This time the author was in an interview with a YouTube-channel management company. The CEO kicked off the interview by asking what he could tell her about the company. Instead of replying, she turned the question around with a smile and asked what they could do for her. This bold statement set the tone of the rest of the interview. When the CEO asked her what her goal is, she responded, "World domination," followed by a pause and then a detailed version of her aspirations. Later that day, she got an email from the company, informing her that the CEO claimed, "She's an absolute star," and that she had completely won him over.

To successfully embody a powerful presence that leaves an impression, like her performance at the interview, the author uses the following tools: first, she focuses on the situation at hand, taking in the room's energy, and turning off her phone. Second, she focuses on being her genuine self. This means that she smiles when there's a reason, such as when hearing a person's name for the first time, and not simply to be pleasant and likeable.

Working on your presence and breaking the rules when necessary will help you stand head and shoulders above the rest of the competition.

### 8. Become successful by drilling positivity and strong values into your mind. 

What happens when you consume unhealthy foods? Well, you compromise your body. Similarly, when you consume negative thoughts, you compromise your mind.

Thankfully, positive thinking is a skill we can develop.

People talk negatively about others because it's a quick-and-easy way to temporarily feel superior. However, this negativity comes at a price. First, such talk is a waste of precious time, since negative conversations tend to be protracted and repetitive. Negative talk also prevents you from noticing or appreciating other people, which in turn will prevent you from learning and embodying the positive traits of others.

To train yourself in positive thinking, there are only two steps you need to follow.

The first is to practice stopping yourself from saying negative things. Take the bad thought and pretend it's your personal secret. Wait until you're alone and then whisper it to yourself. The second step involves preventing negative thoughts altogether. You can practice this step when you meet a new person. Right away, pick out one admirable trait, such as that person's calm personality. Then remember this characteristic as that person's identifying feature. Over time, this process will train your mind to seek out only the positive aspects.

In addition to thinking positively, having clear values will be useful on your path to success. By "clear values," the author doesn't mean a jumble of vague notions you sort of believe in; she means actionable moral guidelines that you can list in a personal manifesto. So simply jot down the list of values you want to live by, and add to it whenever a new one arises. An example of a value could be refraining from joining in on gossip.

In order to put your value system into action, you need to learn it by heart. This means reading your list once a week, and reciting your values to yourself daily.

Clear values are the foundation for a meaningful life, so grab a pen and paper and write that manifesto!

### 9. Opportunities for change are created by kindness and the promotion of what you love. 

Does being nice and working hard pay off? Or is the saying true, and nice guys really do finish last?

Well, what the author can say about kindness is that it creates opportunities. An example of this is when the author saw Justin Bieber's manager, Scooter Braun, at a YouTube party. Being an admirer of his, she wanted to greet him, but didn't know what to say. Eventually, she decided to approach him and simply say, "Hey! I think you're really awesome and here's why…" Braun, who just so happened to be going through a tough day, was really appreciative of the author's kindness. The next day, he posted an Instagram picture of their meeting to express his gratitude, and has since supported her career by re-posting her videos.

So being nice can pay off, but you need to be proactive about it.

Niceness means a lot of things — smiling, behaving politely, being patient and leaving the last piece of cake. But if you want to be kind like a _bawse_, you need to be proactive, which means actively thinking up and behaving in ways that will make an impression. To do this, you need to make "being nice" a daily task on your to-do list.

For instance, the author makes sure that she talks to everyone when shooting a video. After the shoot, she creates an email thread, thanking everyone who worked on the set. This action has earned her a reputation for pleasantness.

Finally, if you want to generate change, you need to promote what you love.

Bashing on what you hate doesn't create a lasting effect. To create real, long-lasting change, you need to find solutions instead. To do this, the author focuses on promoting what she loves. When a reporter asked her opinion about drama involving Katy Perry and Taylor Swift, she replied that she respects both women for being strong and inspirational figures for young girls. After all, that's what's important, not the drama or whatever tiff they got into.

By promoting strong and inspirational women as one of her values, the author established herself as a respectable _bawse_ herself.

### 10. Final summary 

The key message in this book:

**Becoming a successful and confidence-exuding** ** _bawse_** **requires a strong foundation of hard work, discipline and values. With time, you'll be able to train your mind to function effortlessly in this success-optimizing way.**

Actionable advice:

**Align your mind, body and soul.**

Your mind, body and soul are all players on _your team._ All three need to be in sync for you to be and function at your best. To determine whether you've lost synchronicity, ask yourself, are there things you constantly feel guilty about? Or do you fail to do things you say you're going to do? If so, then you need to take a moment and realign your team and make sure that your thoughts, actions and emotions are working in harmony.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to be F*cking Awesome_** **by Dan Meredith**

_How to be F*cking Awesome_ (2016) guides you through real, actionable steps to achieve your goals without making any excuses. The book provides some straightforward principles that will help you avoid the common loopholes that stop people from living successfully.
---

### Lilly Singh

Lilly Singh is a Canadian comedian, actress and YouTube celebrity, more widely known as IISuperwomanII, her YouTube username. Her YouTube channel boasts over 13 million subscribers and her success has led to tours and performances around the world. _How to Be a Bawse_ is her first book.

