---
id: 561c1c4e36356400076f0000
slug: how-to-be-a-power-connector-en
published_date: 2015-10-14T00:00:00.000+00:00
author: Judy Robinett
title: How to Be a Power Connector
subtitle: The 5+50+100 Rule for Turning Your Business Network into Profits
main_color: CA2841
text_color: CA2841
---

# How to Be a Power Connector

_The 5+50+100 Rule for Turning Your Business Network into Profits_

**Judy Robinett**

_How to Be a Power Connector_ (2014) offers a hands-on introduction to networking in the modern business world. By laying out the theory behind strong networking and providing practical tips for turning theory into practice, this book sets you on your way to achieving your career goals.

---
### 1. What’s in it for me? Discover how to become a great networker. 

Do you want to be more successful and increase your chances of reaching your goals? Of course you do. So you probably know that you need to network. A good network is like a spider's web; without it, you'll never entrap what you're after.

But the time when being a great networker was enough is over. Today, you should strive to become a _power connector_ — networkers who don't just build networks with themselves in mind, but always seek to maximize opportunity for everyone in their network. 

So how do you go from being just a skilled networker to becoming a true power connector? These blinks will show you.

In these blinks, you'll learn

  * why a professional network is like a street gang;

  * why Buddhists should start networking with Catholics; and

  * how to be "relentlessly pleasant."

### 2. Strategic relationships are crucial for your personal success. 

In our highly-complex world, you simply can't do business in isolation. You need _strategic relationships_ — relationships that provide mutual value through information, contacts, money and so on, such that everyone involved can better succeed. 

Strategic relationships have numerous advantages. Most crucially, people will base judgments about your identity and social status on your network. If people know you know Mark Zuckerberg personally, for example, they will look at you differently, even though _you_ don't share his accomplishments (like founding Facebook). Your network, therefore, differentiates you from other people.

In addition, you have more power if you belong to a powerful network. Your network is like a street gang. You don't have to have committed a crime to inspire fear or respect in others. You can reap those benefits simply by _belonging_ to a group with that kind of reputation.

Finally, a strong network can give you access to private information or earlier access than would otherwise be possible. If you ever need a piece of information or need a favor, you can simply ask your network. If they can't help you, they can ask _their_ people, and hopefully you can get what you need. That's the power of a strong network.

So, do you have a strong strategic network? A good way to find out is by asking the following questions:

  * How big is your _strategic quotient_ (SQ), that is, the proportion of strategic relationships you have compared to other network relationships?

  * How many people do you talk to on a regular basis? And does this communication create added value?

  * Do you have a "wish list" of people you want to connect with? And do you have a plan to make your wishes come true?

In our following blinks, you will learn the best ways to tackle these questions as well as how to grow and curate your strategic network in ways that will bring you closer to achieving your goals.

### 3. Be smart about figuring out who is most important for your network. 

When was the last time you helped someone by introducing them to someone else who could solve their problem? Well, _power connectors_ do this all the time.

Power connectors bring people (especially those with resources) and networks together in order to form a group that works for the greater good. 

To illustrate this, imagine a scientist who works at a biotech start-up in the United States. The company wants to move overseas, and the scientist's father-in-law is close with the mayor of a small town in Germany, where a pharma facility recently closed down. So, he creates the connection between his boss and the mayor via his father-in-law, and the biotech start-up is able to reopen the plant.

Power connectors, like the rest of us, have limited time. Thus, organization and priorities are extremely important. It's a good idea to categorize your relationships with people in rings, like an onion, with the most important at the center, using the _5+50+100-Rule_ : 

Your _Top 5_ represents your inner circle. These are the five people who you really feel close to, like your spouse, your parents, your best friend, your business partner — the ones that would go to hell and back for you. You think about these people daily, and contact them more than once a week.

The next circle is your _Key 50_, the 50 people with whom you have valuable relationships, like your friends and associates. You contact these people frequently (ideally once a week), and always try to add value to their lives.

The last circle is your _Vital 100_ — your more distant friends and business acquaintances. These are people you really like but don't talk to more often than once a month. However, if you called them, they would surely help you out. It is for this reason that you should maintain these relationships.

Once you've categorized connections according to the 5+50+100 rule, you'll have the foundation for strategic-relationship planning.

> _"People who act as bridges between groups...are more likely to be rewarded financially and otherwise." - Ronald Burt_

### 4. When building your network, look for diversity and depth. 

It might sound obvious, but people have a lot in common with their friends — at least, they share the same interests and values. But is this similarity healthy for your business network?

Actually, not at all. Heterogeneity drives innovation and creativity, so it's worth your time to get out of your comfort zone and make your network _wide_, _deep_ and _robust_.

A _wide_ network means that you have connections to people different from yourself; be it their industry, their interests or even their age, these people's perspectives are different than yours. Think, for example, of the differences between members of the Baby Boomer generation and the Millennials. If you want a wide perspective, you need both.

A _deep_ network is one that is multi-layered, in the sense that it gives you many ways to achieve a goal. For example, if you want to reach your city's mayor, how many contacts in your network could help you do this? If this is your goal, then you should strive to have as many as possible.

A network's _robustness_ describes how willing people are to help. Are the people in your network responsive? Do they return your calls and eagerly answer your questions? If they do, then your network is robust.

Developing a wide, deep and robust network might seem like a tall order. Luckily, there is one simple trick you can use to make it happen: look for people who disagree with you. 

For example, if you're a conservative Catholic, look for Buddhists for your network. You don't have to discuss your faith, and you might find plenty of things in common that you never expected, like the spirit of charity or a hardcore passion for the local football team. 

You want these different people in your life because the greater their difference, the likelier it is that they can connect you with people you couldn't have connected with on your own.

### 5. Find the right environment for your network to flourish in. 

Think about where you want to go in your professional life. In every area and industry, there are centers of power where you can best find the resources to get you where you want to go.

These places, or _ecosystems_, are characterized by high activity and influence. An ecosystem is basically a web of business and private relationships based on common interests. Think of the ecosystem as the ideal environment for your work, whatever that work may be, where potential contacts and opportunities are plentiful.

If you're writing a book, for instance, you can basically work wherever you want. However, unless you're in the right place, you might find yourself stuck when it's time to publish. To have the best chance at publishing, you have to find the publishing ecosystem in your country. 

Before trying to enter a new ecosystem, it's important to figure out a few things:

Who do you want to meet? (In this case, a publishing agent.)

What do you have to offer? (Maybe you have a script about butterflies, and know that the publishing agent is a lepidopterist.)

Lastly, are you a good match for the target ecosystem? Do you look and act the part of someone suited to that environment? Settling these questions will make finding the right ecosystem a lot easier.

It's also worth remembering that you have your own ecosystems — like your family and friends, or your passions and interests.

Another such ecosystem is your local community. Volunteer work for your local community isn't just naturally rewarding; it also introduces you to many different people you may not otherwise have met.

Steve Jobs took this a step further when he advised giving money or time to the charities of people you want to meet. You never know who's going to turn up at that charity's Christmas party!

But now that you've built a strong network, it's time to learn how to use it efficiently.

### 6. Preparation and targeting are the first two steps of power connecting. 

So how can you get the most from your network? The secret lies in being a good power connector.

The power-connecting system consists of four phases: _preparation_, _targeting_, _reconnecting_, and _connecting your connections_. This blink is about the first two phases.

During the _preparation_ phase, you assess where you stand right now and where you want to go, and then set up a plan for getting there. 

Start by making a list of everything you've ever accomplished in business and private life, and then categorize your accomplishments according to ecosystems. For example, if you went to business school, you could create an "academia" category.

Then make another list containing all your contacts and rank them according to the 5+50+100 rule. 

Facebook, LinkedIn and your address book are great places to pull from for your list. But don't forget people like your Starbucks barista or your mechanic! Make your list as comprehensive as possible in order to get a complete overview.

Once you have your skills and contacts in order, what's next?

In this second phase, you identify a _target_ — someone you want to connect with — and prepare for your first encounter with that person. During the contact itself, there are some rules you should definitely follow.

First, dress the part based on who you want to meet, whether they're business people or sports trainers. Always remember: quality is visible. It's worth it to invest in nice clothes. 

Compliments on jewelry or ties can be good icebreakers; this is especially true if you wear it yourself.

When it's time to initiate contact, follow the Marriott 15/5 rule. Whenever a Marriott Hotel employee is within 15 feet of a guest, he is supposed to acknowledge her with eye contact and a friendly nod. Within five feet, he should smile and greet her.

Once you feel comfortable making contact with your target, it's time to start thinking about phases three and four.

### 7. Following up and connecting your connections are the two final steps of becoming a power connector. 

Imagine that you've just met with the CEO of your favorite company and are leaving her office. The two of you had an interesting, pleasant conversation, and she even gave you her contact details. You're feeling good — but what now? How do you solidify this connection as part of your strategic network? 

It's now time to _reconnect_. But time is of the essence! It's crucial to reconnect within 24 hours. Why? Because it shows that you care and that you are a master of communication.

This could be as easy as sending an email. Simply thank the person for the pleasant conversation, add value by providing them something that they'll surely be interested in (like a news article or similar), and tell them about your work so they know what they can help you with.

Be sure to nurture these relationships by showing that you care about the little things. If you know a personal contact is going through a rough time, a simple "How're you doing" can be worth more than 100 fancy business contacts.

The final phase in power connecting is about adding and creating value for your new connection by introducing them to the right people in your network. In other words, you _connect your connections_ for the greater good.

The goal here is to develop _power triangles_ in which a power connector serves multiple people, including himself.

For example, imagine that Marie, a power connector, goes with her software developer friend Claudia to a tech conference. There, Claudia introduces Marie to Annette, who heads a coding academy.

Coincidentally, Annette is looking for a speaker for another programming conference, and Marie immediately thinks of Eric, a gifted C++ developer, and sets him up with Annette. In doing so, Marie has helped Eric get in touch with many young programmers, helped Annette find a speaker _and_ added a new contact, Annette, to her network. Everybody wins!

### 8. Power connectors use social media like LinkedIn and others to their advantage. 

How many contacts do you have on LinkedIn or Xing? How many people are you connected with on Facebook or Twitter? In this digital world, it has never been easier to stay in touch with other people. Nevertheless, there are some things to take into account when using social media.

The great thing about social media sites and email is that you have an opportunity to reach out to people whom you'd never otherwise meet in real life, and strengthen your network in the process.

It was this technology that allowed a 12-year-old business student to reach out to the famous business magnate, Richard Branson, for tips.

Making introductions via LinkedIn is not only convenient, it can also strengthen your power circle by increasing the interconnectedness of your network. But be careful with your introductions. Always be sure that the people you want to connect are actually a good match, and that their connection will provide value for the both of them.

It's also important to note the strengths and peculiarities of various social media channels and email. 

For example, it's easier to address someone via email if you use a good subject line. A headline like "Re: We met at conference X — here's the info about crowdfunding you asked me for" is great because it provides them with instant context for who you are, why you're emailing them, why they should find it important and why reading this email isn't a waste of their time.

Twitter, in contrast, gives you the opportunity to connect to _expert strangers_, that is, people you don't know yet but who are experts in areas relevant for you. You can build a relationship with them by following them, retweeting their tweets and asking smart questions or sharing articles they might be interested in.

While social media is extremely easy and convenient, you should never forget: nothing beats face-to-face contact.

> _"The key to building a strong LinkedIn network is to treat every member just as you would any connection."_

### 9. Strategically connecting, speaking up and supporting other women are good for female power connectors. 

Women and men are different, and that's no less true in the world of business. Here are three strategies that women specifically can use to enhance their power-connecting abilities. 

First, _connect strategically_ in every direction possible. Unfortunately, support for young female leaders isn't that common. As a woman, you'll have to be active in seeking out assistance. 

Kay Koplovitz, TV network president and founder of Springboard, suggests building relations with superiors whom you hold in high regard. Offer your help, provide information and add value for him or her to build a lasting, mutually beneficial relationship.

Second, _speak up_ for yourself, but do so in a nice way. 

Women often have trouble telling other people what they want to achieve. While they tend to be natural networkers, they nonetheless rarely make the best use of their resources. 

The best way to go about this is not to scream and shout, but to be "relentlessly pleasant," a term coined by the president of the University of Michigan, Mary Sue Coleman. As the saying goes: "You can catch more flies with honey than with vinegar."

Finally, make a point to _support other women_ by offering your help and your good reputation. It's important to "pay it forward," so to speak, and insist on the success of other women. Not only is this the right thing to do, it also helps grow and strengthen your network.

You might even start your own _stiletto network_, that is, an association of women who actively help each other in reaching their goals.

With some careful maneuvering and a commitment to helping other women achieve, you can bring yourself closer to achieving your own career goals.

> _"Respecting yourself enough to ask what you want shows others that they should respect you too."_

### 10. Final summary 

The key message in this book:

**Modern business is complicated and you can't do everything by yourself. To accomplish your goals, you'll need to develop a strong network of people who are ready and willing to help you get where you need to go.**

Actionable advice:

**Come out of your shell.**

Being in a large group of people can be intimidating. The next time you feel uncomfortable and shy, try to overcome your shyness by acting _as if_. Act the part of a super-confident person who anyone in the world would love to talk to. And always remember the person to whom you're talking might be just as shy as you are!

**Suggested** **further** **reading:** ** _Strategic Connections_** **by Anne Baber, Lynne Waymon, André Alphonso, Jim Wylde**

_Strategic Connections_ (2015) offers practical tips on developing the skills to become an effective networker. In an increasingly connected world, networking has never been more important. Find out which skills and knowledge you need to succeed in the new collaborative workplace.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Judy Robinett

Judy Robinett began her career as a social worker. Before becoming highly involved in the US venture capital scene, she serves as CEO of several public and private companies. Her networking know-how has led some to humorously call her "the Yoda of strategic relationships."

