---
id: 57096d418b5d6e000300004b
slug: how-to-be-alone-en
published_date: 2016-04-12T00:00:00.000+00:00
author: Sara Maitland
title: How to Be Alone
subtitle: The School of Life
main_color: B22467
text_color: B22467
---

# How to Be Alone

_The School of Life_

**Sara Maitland**

Today's socially focused culture leaves us very little time to ourselves and unprepared for times when we may actually need to be alone. _How to Be Alone_ (2014) shows us that we don't need to be scared of solitude, and that there are many benefits and joys to be found in being alone. So don't be afraid, go solo!

---
### 1. What’s in it for me? Unlock the awesome power of solitude. 

Imagine there's a guy who lives down the road from you. He lives alone. In fact, you often see him wandering around the area by himself. Bizarrely, though, he seems to enjoy it.

What a loser he must be! No "healthy" person could possibly enjoy so much solitude.

Well, you're wrong. Solitude is an immensely powerful and positive thing. Those of us who truly experience it, can be, in fact, far healthier and more relaxed than the rest of us. But this doesn't mean that we should dump our friends and partners in favor of a lonely existence. There is a happy medium, as these blinks will explain.

You'll also discover

  * how to ensure your children get enough time alone;

  * why taking a bath is time well-spent; and

  * why society dislikes a loner.

### 2. Finding time alone will boost your creativity and self-knowledge. 

Do you ever wish you had more time to yourself? These days, society puts a lot of pressure on us to stay socially active and focus on work, which means less time for ourselves. And yet, alone time can improve your personal well-being and creativity.

That's because, first of all, spending time alone allows you to discover your real "self."

When you're alone, you can focus on a deeper understanding of who you are and what matters to you. Only by spending time alone, free of outside influences, can you discover these important parts of yourself.

For example, the famous author Henry David Thoreau spent over two years alone in the woods of Walden Pond rediscovering himself. This time allowed him to assess his own values and those of the society around him. All of this led to Thoreau publishing his groundbreaking book _Walden_ in 1854.

With this kind of dedicated time alone, you can unlock and increase your creativity.

In fact, many creative people in various fields have recognized solitude as a main source of creative output.

The influential author Virginia Woolf believed that spending time alone was vital to creativity. In her 1929 book, _A Room of One's Own_, she saw the lack of solitude as being linked to the relatively small number of female writers at the time. Woolf saw that females were not lacking talent, imagination or intelligence; they were lacking time alone to allow their creativity to blossom.

The link between solitude and the happiness of creative freedom can be seen throughout history.

Another example is the actress Greta Garbo. At the age of 35, after having appeared in 28 movies, Garbo decided to retire from acting and live a simple and solitary life. She continued to have an active social life, but by giving herself plenty of time alone, she was able to enjoy peace and creativity.

So make sure you schedule yourself some alone time and reap the joys of solitude.

### 3. Embrace the joys of solitude through a transcendent connection with nature. 

Now that you recognize the benefits of spending some time alone, you might be wondering how you're supposed to find this peaceful solitude.

One of the best ways to find seclusion is to escape from the city and enter nature.

By immersing yourself in nature, you can find a sense of connectedness to the world around you. But to achieve this, you should make sure you are completely alone as even bringing along your dog can be a distraction.

When you're completely alone, and you focus your attention on the natural environment around you, you'll start to feel united with nature. This connection with nature is a beautiful and even mystical experience. Thoreau and others who have sought to escape the pressures of society through solitude have described it.

People who have had this beautiful experience call it _transcendence_, or an interaction with something that goes beyond the conscious mind.

Some have found spirituality in the transcendent experience; others have simply found joy and happiness. But, no matter what, solitude is a requirement in order to achieve a truly transcendent experience.

For instance, whether someone in the Middle Ages was training to become a monk or a knight, both would spend time alone before their initiation into that profession. This period of solitude was called a _vigil_ and was meant to prepare them for the next phase of their lives.

To cite another example, in Australia, young Aborigines are sent on a _walkabout_ : a six-month period of solitude meant to prepare them for adulthood. In many cultures throughout history, spending time alone has been considered necessary for the transition into adult society.

So now that we've seen the many benefits to solitude, in the next blink we'll find out why people who spend time alone are often described as selfish, unnatural — and sometimes even dangerous!

### 4. People who enjoy solitude often get a bum rap. 

Most people consider it socially acceptable when someone takes an occasional hiking trip on their own or secludes themselves for a brief period to work on a project. But when it comes to people who choose solitude as a way of life, they can often be looked down upon.

This reaction is strange because modern society nurtures individualism.

Today we can see many examples of people who are accepted for expressing their own individuality through bold fashion and lifestyle choices, tattoos or other body modifications.

Yet, modern culture finds it threatening when people choose a lifestyle of solitude. If people support unique individuality, why is it considered suspicious for someone to choose being alone as a means of self-discovery?

It looks like our evolution might be to blame.

Philip Koch, a philosophy professor, researched the negative reactions to solitude — social anxiety and mistrust, to name a few — in his book _Solitude_. He found that, due to human evolution finding success through companionship, people have an inherent reaction to solitude as being unnatural.

Others might consider seeking solitude as a pathological or harmful behavior. This is likely due to psychological research that has determined positive relationships with others to be necessary for an individual's happiness. But if this research was true for everyone, then those who chose solitude wouldn't find happiness.

We also have a tendency to view someone who is isolated as putting themselves in danger. If they hurt themselves, who will be there to help them?

Ultimately, these views are an unfair social stigma. If one desires the freedom of solitude, they should embrace it. After all, the problems of society don't have to influence your own pursuit of happiness.

If you want time alone, go for it!

### 5. In order to be alone, you have to recognize and overcome your fears of solitude. 

Perhaps you're curious about living a life of solitude, or maybe you've recently lost a partner and have suddenly found yourself alone. Whatever the case may be, there are steps you can take to feel at ease on your own.

First of all, it's important to identify any fears you might have about being alone.

Sometimes, if we tell ourselves we want time alone but never seem to find it, it might be due to a subconscious fear of being alone. This fear can even result in a negative reaction or anger toward a friend who is experiencing happiness through solitude.

You can overcome these fears by simply giving yourself small doses of alone time that test your comfort levels.

For example, rather than taking a shower, try taking a bath and allowing yourself to linger and focus on the fact that you are alone. You can even try to focus on being alone while you're with others in a store or riding public transportation.

While doing this, it's important to realize that solitude isn't dangerous or scary. History actually shows us that it's healthy.

Anthony the Great was the founding father of monastic living and remained in isolation for 20 years. When he came back to society, he was both mentally and physically fit. Impressed by his way of living, many people became his followers.

The same can be said of the British-born Tibetan Buddhist nun Jetsunma Tenzin Palmo. Starting in 1976, she lived alone in a cave for 12 years during which time she remained healthy, and accepting and welcoming of others.

So, once you overcome your fears, you can start looking for other ways to enjoy your solitude.

> _"Conversation enriches the understanding, but solitude is the school for genius."_ — Edward Gibbon, English historian.

### 6. Expand your periods of solitude with some “alone time” or a solo adventure. 

Even if you've come to accept that there are many benefits to solitude, you might be doubting that you'll find the time to fit it into your busy schedule. But there are some easy ways to squeeze more alone time into your life.

One simple way is to spend more time focused on solitary activities you already enjoy.

While many might consider reading, watching movies or listening to music as part of our important alone time, these are actually things that keep us from ourselves.

These are vital activities and can be enriching, but it is important to consider the fact that, if you are concentrating on the writing or music of another person, then you aren't truly spending time alone.

A better solution is to take solitary walks in nature. These can be a great source of happiness for anyone and it doesn't cost any money. Such walks are the perfect way to give yourself time to reflect, connect with nature and feel better about the idea of being alone.

Likewise, running is a great way to get more in tune with your body, mind and nature. It's a simple step to turn running into a meditative exercise that can lead to self-discovery and fulfillment.

Once you feel ready for longer periods of solitude, it might be time to go on a "solo adventure."

For this you should choose a challenging but achievable trip that you can do on your own.

For example, this could be a simple camping trip, a boating adventure or a visit to a foreign country by yourself. Ask yourself what you'd really enjoy and consider your realistic options. Whatever it ends up being, you'll be spending valuable time with yourself by developing your sense of fulfillment and learning from your own experiences.

Once you start expanding your periods of solitude, you might experience daydreaming. We'll explore this sensation in the next blink.

### 7. Use solitude to explore reverie, a state of “active imagination.” 

Do you ever think back to childhood and fondly remember keeping yourself entertained with just your imagination?

As it turns out, the happiness we get from solitude can come from these experiences, which we tend to lose touch with as we get older.

Psychologist Donald Winnicott has traced our adult ability to enjoy solitude back to when we were infants. During this time of early childhood, we could feel content to be alone after our basic needs, like being fed, were met by our parents. In these situations, we felt safe and secure, and free to wander around on our own and explore our surroundings.

Unfortunately, as we grow older we no longer have the security and freedom of such moments. Often, once we enter the world of schools and social pressures, we can lose the sense of safe alone time and our ability to happily experience solitude.

But it doesn't have to stay this way. We can get our safe alone time back by learning how to engage with our active imagination and daydreaming.

Active imagination, or _reverie_, is what psychoanalyst Carl Jung used to examine his subconscious. He would spend time alone daydreaming, recalling memories and dreams and taking notes to record his reactions. By writing it down in a notebook, he could figure out which thoughts and memories made him happy and, as it turned out, much of it was related to childhood solitude.

Jung encouraged this reverie in his patients. He suggested they embrace solitude, return to thoughts of joy and happiness and reclaim the kind of safe alone time they experienced when they were children.

While we can't change our own childhood, we can foster solitude later on in life and pass on the benefits of safe alone time to others.

### 8. Alone time is one of the greatest gifts you can give your children. 

Even though many of us hold on to meaningful childhood memories of solitude, few of us realize the importance of passing this along to our own children.

Because, in today's society, it's more common for parents to teach their children about the dangers of being alone. Kids are thus denied the chance to learn the important benefits of solitude.

We should, however, be more concerned about creating a safe environment for children to spend time in on their own. Childhood experts Anthony Storr and Richard Louv offer advice on how you can create such an environment.

Even infants can use their active imagination: you don't have to constantly engage with them. Even when they're in your arms, they can let their eyes wander and explore their surroundings. This allows them to be on their own in a safe environment.

Give toddlers alone time: give them space outdoors in parks or even in the woods where they can explore independently under your supervision. You'll be surprised to see how creative and peaceful they can be in these environments.

When it comes to story time, don't be overprotective. Let kids enjoy stories where children encounter danger by themselves. Many parents try to protect their kids from stories that might be too frightening, but kids actually enjoy being a little scared.

Many fairy tales, like The Snow Queen, have been passed down for many generations and contain tales of young boys or girls battling evil on their own. These stories are important for teaching children that, even if they are alone, they still have the strength to overcome challenges.

But remember, since everyone is different, the amount of solitude a person needs differs from one person to another.

### 9. Determine how big or small your dose of alone time should be. 

You might think that the amount of alone time a person needs comes down to one simple question: Is she an _introvert_ \- someone who is shy, or an _extrovert_ \- someone who is socially outgoing?

But it's not that simple.

First of all, the division between extroverts and introverts is nearly impossible to test given that most people will respond differently depending on the context.

Let's say you ask someone if she likes going to parties, thinking the answer will tell you if she's an introvert or extrovert. The rational response would be that it depends on what kind of party it is and who else will be there.

Another important thing to consider is that these categories are culturally biased.

For example, introversion is appreciated in Japanese culture, whereas extroversion is highly valued in the United States. Therefore, if you were to ask someone from each of those countries the same personality questions, you'd get two different sets of answers based on the kind of behavior that's considered socially acceptable.

There's a wide range of traits that make each of us different and unique — not just two categories.

The same goes for the amount of solitude that's right for us.

There isn't any psychological theory behind it: you simply have choose what works for you. Our society values individualism and unique differences, and the same should apply to your solitude. You wouldn't want anyone telling you what to wear, and you shouldn't let anyone tell you exactly how much time alone you should enjoy.

It's only natural that people in different parts of society are going to need more or less time alone. The more you learn and discover about yourself, the easier it'll be to decide how much time you need.

Whatever the case may be, we should respect one another's decisions and not interfere when it comes to spending time alone.

### 10. Final summary 

The key message in this book:

**In a society that values freedom and individualism, you should not be afraid to execute your right to solitude. There are numerous benefits and joys of solitude. Discover who you are, connect with mother nature and enhance your creativity by simply being alone.**

Actionable advice:

**Start small and simple.**

When you're tempted to find some time alone but unsure where to start, think about expanding something you already like to do, like a bike ride or a visit to your local café. Then think about making this activity 15 to 30 minutes longer while focusing on your individual experience. You might be surrounded by people but you'll be at peace with your solitude.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Stay Sane_** **by Philippa Perry**

In _How to Stay Sane_ (2012), British psychotherapist and author Philippa Perry shows you how to better nurture relationships while using self-observation, "positive" stress and the power of stories to achieve and maintain your mental health.
---

### Sara Maitland

Sara Maitland is an accomplished British novelist and feminist. She has written numerous works of fiction and nonfiction, including _A Book of Silence_, her first work, and _Daughter of Jerusalem_, a novel that won the Somerset Maugham Award.

