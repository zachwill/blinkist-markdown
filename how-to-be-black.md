---
id: 5891b73e78a78e0004051a8d
slug: how-to-be-black-en
published_date: 2017-02-02T00:00:00.000+00:00
author: Baratunde Thurston
title: How To Be Black
subtitle: None
main_color: 55CD29
text_color: 2A6614
---

# How To Be Black

_None_

**Baratunde Thurston**

_How To Be Black_ (2012) is the funny, revealing and insightful autobiography of Baratunde Thurston. Thurston attended private schools and Harvard University, and the experience of being black in a predominantly white milieu taught him a great deal about what white and black people have come to expect from one another. These blinks tackle a difficult subject with humor and empathy.

---
### 1. What’s in it for me? Gain a new perspective on what it means to be black. 

The only black kid in the animated series South Park is named Token — a blatant admission of what most TV series were at pains to hide: black characters were just that, token characters. In South Park, Token is exposed to all sorts of black stereotypes, like when Cartman is sure he owns a bass guitar since his family is black. Baratunde Thurston had a similar experience growing up, but the stereotypes were slightly different.

This is the story of what it's like to grow up black in a primarily white neighborhood while still trying to maintain contact with the black community. It's a glimpse into one man's life, but also into our own prejudices and ideas about what it means to be black.

In these blinks, you'll learn

  * how being African-American isn't black enough for some people;

  * why a black student union makes sense, but a white one doesn't; and

  * how a black guy's black colleague can be his worst enemy.

### 2. Growing up as a black kid in 1970s America wasn’t made any easier by having an African name. 

School can be tough for anyone, but if your name is Baratunde, things start going downhill around minute one of the very first day. Imagine the teacher taking roll call, zipping through the Johns and Jennifers and then stumbling to a halt: "Barry Tune? Baritone Dave?"

Simply put, it's not easy having an African name in America.

The author spent his childhood listening to his name getting butchered by white American teachers. He's been called "Barracuda" and "Bartender," while other teachers, panicking in the face of all those syllables, just shortened it to "Brad." For the record, Baratunde is pronounced: baa-ruh-TOON-day.

After a while, he got used to it. Now, he even takes a certain pleasure in hearing how new acquaintances might mangle it. He's waiting for the day that someone slips up and says "Beelzebub," or comes up with a way to inadvertently add a Q.

He's also discovered that Africans living in the States aren't necessarily fond of his name, either.

The name Baratunde comes from Nigeria, an offshoot of the more common name, Babatunde.

You might think that a Nigerian would smile after being introduced to an African-American with such a name. But that's not the case. Once, Baratunde called a Nigerian friend but got stuck talking to the friend's father instead. The man was outraged to hear that someone who wasn't Nigerian would be using this name.

His friend's father asked him if he even knew the meaning of his name. And when the young Baratunde was about to tell him that it means "grandfather returns" or "the chosen one," the man cut him off and shouted, "No! It means 'grandfather returns' or 'the chosen one!'"

Unfortunately, this wasn't an isolated event. Many other Nigerians would later react the same way after hearing his name.

> _"Most of the Nigerians I've met use my name to remind me that I'm not that black."_

### 3. Baratunde’s mom defied many of the stereotypes people have of a black woman. 

If you were told to envision a guy who loves going camping, listening to Mozart and cooking organic vegan dishes, what kind of image would you have in your head? In the author's experience, you're probably not picturing a guy who looks like him.

But black people fit this profile as well.

Baratunde grew up in Washington, DC, with a single mom, Arnita Lorraine Thurston, who made a living as a cook and a phone-book delivery person. It wasn't much, but it afforded them a small house.

It was the late 1970s, and like many other people, Arnita was caught up in the emergent health-food movement. She would go to the local organic co-op and bring home rice crackers, rock-hard organic cereal and skim milk.

Naturally, as a child, Baratunde wasn't all that happy about this: a vegan donut covered in carob just isn't as satisfying as one that's been deep fried and dipped in chocolate.

Arnita also loved the outdoors and took Baratunde and his friends on many field trips, such as hiking the Blue Ridge Mountains and camping out in the wilderness of North Carolina.

A black mother can also be what's known as a "tiger mom."

Arnita was demanding of Baratunde, and she made sure he had a full program of extracurricular activities.

This meant learning to play the double bass in the DC Youth Orchestra Program, which led to a performance at the Kennedy Center. And after Baratunde had his bike stolen by some hoodlums, Arnita quickly signed him up for tae kwon do classes.

On top of all this, he also had his all-black Boy Scout obligations, with more camping and cultural events to take part in.

Arnita also made sure Baratunde knew about his cultural heritage. When he was eight years old, she bought him a book about apartheid. And as he grew up, she would regularly quiz him about African nations, making sure he'd studied the map of Africa that hung on their kitchen wall.

### 4. There were many awkward moments in Baratunde’s introduction to life at private school. 

No one thinks twice about that fact that President Obama's daughters, Malia and Sasha, go to a private school. But rewind 20 years, and it's a different story.

When the author was growing up, being a black kid in a private school was truly a fish-out-of-water experience.

After spending some time in the world of public schools, the author was enrolled at Sidwell Friends, a private school.

He not only stood out as one of the only black children; he'd also picked up the unpolished language of public schools, such as "axing" people a question, which caused even more unwanted attention. Soon enough, however, he was speaking like his classmates.

But there was no escaping the fact that he was the only black student in his class.

This was especially painful whenever they studied something connected to black culture. When the class was studying _Uncle Tom's Cabin_, by abolitionist author Harriet Beecher Stowe, everyone would turn to Baratunde as if he were some sort of authority on nineteenth-century literature.

There were some things to learn about making friends as well.

Baratunde was getting to know the world of affluent white people, and he found it interesting to see other black people in this new context.

At one point, early on at Sidwell Friends, he was paired up with another black kid who asked Baratunde if he knew what an "Oreo" was. That was an easy one. Baratunde was well acquainted with the delicious little cookie sandwiches.

But the boy pointed out another black kid who was talking with some white friends, and gave Baratunde another definition of "Oreo": someone who is black on the outside, but white on the inside — an imposter who thinks he's better because he hangs out with the white kids.

### 5. Black people can be equally proud of their American nationality and their African ancestry. 

If you grew up with Mexican, Portuguese or Chinese friends, you might be familiar with them having to run off to another class in the afternoon or even on Saturdays.

Well, African-Americans have those groups, too; they exist to strengthen the bond with their cultural heritage.

Baratunde attended a group called Ankobia, which is derived from the Twi language of Ghana and translates to "those who lead in battle." Filled with children from less privileged families, Ankobia was a far cry from the elite atmosphere of private school.

The group was put together by activists in the African-American community to help ensure black children grew up to become strong adults. This meant teaching them to resist the temptation that many people in poor black neighborhoods succumb to: dealing and doing drugs.

Baratunde was one of 15 boys who met every Saturday morning at a local school and spent five hours that started with exercises, led by the memorable Baba Mike.

After countless jumping jacks, push-ups, crunches and karate kicks, Baba Mike would finish off the training by walking on their abs while the kids struggled to keep their feet in the air. And that was just the start! After that it was time to learn practical skills like carpentry and electrics.

The kids were also taught the basics of firearms; it was assumed that knowledge of them would come in handy at some point in their lives.

Finally, there was the reading list, which was filled with the works of Malcolm X, Martin Luther King, Jr., and other prominent role models.

Sometimes they would be visited by elders in the community who'd actually lived in Africa. These people would talk and answer questions about ancestral religious and cultural traditions.

### 6. If black people are gathered together, it shouldn’t be taken as a threat to white people. 

It's to be expected during lunchtime at a school cafeteria: kids gather together with like-minded friends.

At Sidwell, the few black students grouped together, which caused the white students no small amount of worry. Ironically, they managed to feel excluded by this and questioned whether these gatherings should be seen as a threat to the other students.

It took some explaining before the white students at Sidwell understood why the black students needed some sense of unity.

Obviously, the table of black students stood out, but nobody was going to ask a table of white students why _they_ were sitting together. It's simply a normal thing for friends to do.

And so it is with Baratunde and his pals. Naturally, the few black students would want to sit together at lunch and develop bonds of friendship.

More disconcerting was the idea of needing a white student union.

Again, the white students felt threatened by the black students getting together to form a student union, which led to the question of organizing a white student union at Sidwell.

To most people, the idea of an exclusively white organization brings to mind segregation and the Klu Klux Klan, but the student that raised these questions was actually oblivious to these things.

Like others, she didn't understand what the small handful of black students at Sidwell were facing with a majority of white students. But Baratunde explained how Sidwell was essentially already one big white student union, which was exactly why the black students needed representation.

So, anxious white people, keep your hats on. The foregathering of black people doesn't need to be seen as a threat.

### 7. Black-white friendships are great, but don’t ask to touch a black person’s hair. 

When you were growing up, did you ever have an older relative who liked to playfully mess up your hair? It was kind of annoying right? Well, there's actually never a good time to go touching anyone's hair, even if the hairdo seems exotic to you.

Even friends don't touch a black man's hair.

Baratunde eventually grew a pretty cool afro when he was in school, but he was shocked at the number of complete strangers who were mesmerized and would tell him how beautiful it was and then, inevitably, ask to touch it.

They usually wouldn't even wait for a response — they'd go to touch it while they were in the process of asking. This happened often enough that Baratunde developed a quick yet graceful maneuver that allowed him to avoid the touching and, at the same time, tell them, no, it's not cool to touch his hair.

However, if it was a friend who tried to do this, Baratunde would take a moment to explain why this isn't an acceptable thing to do.

It's not hard to understand: you don't go patting someone on the head if you respect that person. Patting is a gesture usually reserved for pets. And with the long and terrible history of how white people have treated black people, this gesture is far from appropriate.

What _is_ welcome and helpful, though, is being a "black friend" to white people. Sometimes Baratunde sees this as doing a service to America.

This is due to the number of benefits white people gain by being his friend.

First of all, when a white person is seen with a black friend, it's automatically assumed that that's one cool white person.

Plus, when that cool white person says something really dumb, such as "black people have dancing in their blood," he can get away with it by saying he has a black friend, which of course means he couldn't possibly be racist.

On a more serious note, Black America does also benefit from these friendships. It's important to communicate with the white community, answer their questions and cut through the misunderstandings.

### 8. Boston can be unfriendly toward black people, but Harvard University offered an accepting environment. 

Massachusetts has a reputation for being a pretty liberal state, but Boston is actually not the friendliest of cities for a black person.

The city has a pretty dark past: In the 1970s, there was fierce opposition against busing students from the inner-city to better schools, and it led to violent protests in the streets.

When Baratunde and his mom arrived in Boston to visit Harvard University after he'd been accepted there for undergraduate studies, it was a pretty cold atmosphere: nobody would say hello or even look them in the eye. The first person to greet them on campus was another freshman, who was also black.

Any fear he might have had at first, though, eventually proved to be unfounded — it actually wasn't too difficult being black at Harvard.

He had some doubts about being accepted by his peers at college, but these quickly vanished when he met his black roommate, Dahni-El.

He'd already hung up an African flag, and since Baratunde was wearing a traditional Ghanaian Kente outfit from a recent African trip, they proudly stood out as a fairly militant duo in their first days there.

As many other less privileged students did, Baratunde and Dahni-El shared their funds to help each other get through the expensive college life.

They also signed up for the Dorm Crew, which is basically a team of student janitors who travel around the campus, cleaning dorm rooms and scrubbing toilets.

While this might sound demeaning — or like another instance of white people taking advantage of black labor — the Dorm Crew wasn't an all-black team. Also, it paid well and turned out to be one of Baratunde's favorite things since it provided some much needed alone time to relax.

### 9. The workplace poses its own set of challenges when dealing with black and white coworkers. 

After graduating from Harvard, Baratunde joined the workforce at a local telecommunications firm in Boston. And this is when he started to learn some new lessons about what it's like being black in the workplace.

First of all, not all black colleagues are willing to help.

Baratunde did run into some friendly black coworkers who would greet him, offer useful advice and even share an inside joke to liven up a company meeting. But just as often, a black coworker would turn out to be his nemesis.

This would often be the result of the coworker's having been the only black person at the company before Baratunde showed up. Feeling threatened by this new black intruder, the coworker would constantly feel the need to outperform Baratunde at each and every task.

Baratunde also met people he refers to as "black deniers." These were people who refused to acknowledge their own blackness, or that they had anything in common with Baratunde at all.

White colleagues came with a different set of obstacles.

One particularly annoying habit was that they often expected black colleagues to speak on behalf of the entire black community.

This can lead to some tricky situations, such as being cornered in an elevator and asked questions about Barack Obama. Baratunde, knowing that the coworker thinks his point of view is shared by all black people, was left with three options:

Avoid the question by changing the subject; confront the colleague and ask: "Do you really think I have the official black stance on Obama, as if all black people think the same thing?"; or give an honest reply about what you like and dislike about Obama's work.

The last option might seem like the way to go, but, actually, it will just lead to more questions every time people want a black take on an issue.

So go with one of the first two options. You might only be stretching the truth if you just say, sorry, gotta get back to work.

### 10. Final summary 

The key message in this book:

**When a black person enters the world of white privilege, such as private schools, high-level universities and companies, the latent discrimination is apparent. But by paying attention to a few guidelines, and in the author's case, also incorporating humor into them, black and white people stand a better chance of understanding one another and reducing racism.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Between the World and Me_** **by Ta-Nehisi Coates**

_Between the World and Me_ (2015) is an open letter to the author's 15-year-old son about the realities that face black men in America. Filled with personal anecdotes about the author's personal development and experiences with racism, his letter tries to prepare young black people for the world that awaits them.
---

### Baratunde Thurston

Baratunde Thurston is a Harvard University graduate, a communications expert, comedian and popular public speaker. Formerly a writer for _The Onion_, he now blogs for _Cultivated Wit_ and can be followed on Twitter: @baratunde.

