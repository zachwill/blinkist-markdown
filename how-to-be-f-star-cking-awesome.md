---
id: 5a88cd2eb238e1000705416a
slug: how-to-be-f-star-cking-awesome-en
published_date: 2018-02-20T00:00:00.000+00:00
author: Dan Meredith
title: How to be F*cking Awesome
subtitle: Sticking a finger up to the law of attraction and a thumb up to action
main_color: DF2D4C
text_color: C42843
---

# How to be F*cking Awesome

_Sticking a finger up to the law of attraction and a thumb up to action_

**Dan Meredith**

_How to be F*cking Awesome_ (2016) guides you through real, actionable steps to achieve your goals without making any excuses. The book provides some straightforward principles that will help you avoid the common loopholes that stop people from living successfully.

---
### 1. What’s in it for me? Learn how to be successful from a real-life success story. 

We all know people who seem to have it all: a cool job, a beautiful partner, amazing friends, plenty of money — basically, an awesome fucking life. Who doesn't want that?

But knowing what you want is only half the battle; you also have to know _how_ to get what you want.

Written by a successful entrepreneur, this book will show you how you can achieve your goals through the very same principles used by the author himself. You'll learn that it's OK to be selfish and that you can't be successful without being yourself — among other lessons.

We've taken the eleven principles identified by the author and combined them into six easy-to-follow blinks. By following them, you, too, can become a real-life success story.

In these blinks, you'll find out

  * why you shouldn't dress up like Lady Gaga;

  * how to deal with haters; and

  * what a "brain dump" is and when it's useful.

### 2. To start living the life you want, you need to be selfish. 

You may think that living a good life involves pleasing others, but to fulfill your life goals, you need to first start taking care of yourself.

Making yourself a priority — or being selfish — will lead to a better life not only for you but also for others around you. If you work on improving yourself, you will focus on becoming a better caregiver, parent, sibling or friend.

Imagine a teacher coming to class with a bad flu instead of staying at home to rest. In his poorly state his performance is affected, and, as a result, the students won't benefit from his being there at all. It would be better for everyone if the teacher put his own needs first and focused instead on getting better before he thought about attending to anyone else.

Taking care of yourself and your life is crucial. Remember that your life is unique and so you shouldn't be trying to live it any other way; trying to live someone else's life would be a waste of time and energy. For example, the author took the career path he thought he _should_ have, every week working 100 hours in corporate jobs that paid well, yet he never felt happy. In fact, he felt burnt out, stressed and unfulfilled. This was because he was following someone else's dream and ignoring his real passion, which was to start his own business and become his own boss.

Being selfish involves balance in areas of your life like nutrition, exercise and mental health.

If you are sick mentally or physically, it will be difficult to start a business or approach another equally big goal. Therefore, you need to focus on keeping yourself balanced by

  * working out and getting into shape even if you hate the thought of exercising;

  * eating nutritious food but not being too strict about it; and

  * developing or having a strong support group of friends and family. Stop hanging out with those who bring you down.

It is important to prioritize yourself and check that key areas in your life are in balance. That way you will be fit and well enough to start working toward achieving your goals.

### 3. What prevents most people from reaching their goals is their inability to express themselves. 

Have you ever tried to impress someone by acting differently, either to get a job or to win over that potential partner? So many people try to fake who they are to get ahead, not realizing that this strategy is sure to backfire.

Rather, the best tactic is to stop concerning yourself with what other people think of you. If you fear being judged by others, and to spare your ego, you end up losing a lot of opportunities. For example, the author used to be guided by negative feelings and "what-ifs." He spent many years afraid to even say "hi" to people because he feared being ridiculed or simply not liked. The truth is that, of course, the reality is never as horrifying as the worst-case scenarios you imagine.

What's more, not only should you not worry about what others think of you, but you should also be willing to show your authentic, "weird" self. People can smell fakes from a mile away, and besides, your uniqueness is what will draw people to you.

So don't even try to copy Lady Gaga by donning similar costumes and ripping off her pop tunes; you'll only turn people off because no one's interested in a fake or a copycat. Instead, be original and trust your own ideas and personality.

Sometimes it can be hard to show your real self, but you shouldn't be afraid to ask for help when you need it. We're hesitant to ask for help or advice because we're afraid of coming across as weak. Furthermore, we think it's unfair to be bothering others with our problems.

However, admitting that you need guidance isn't a sign of weakness or that you're a failure. Contrary to what most people might think, people like helping others. So when you ask someone for help, it's a win-win situation because you're giving them a chance to be useful and share their advice or expertise!

Don't be afraid to ask for help and don't be afraid to be yourself. By letting go of your fears, you'll allow yourself to open up and be honest, which is what we'll be looking at next.

### 4. Being honest about your abilities will increase your value. 

Let's be honest, nobody likes to admit their weaknesses or shortcomings — not even to themselves. But to achieve our goals, we have to be willing to look at both sides of ourselves, and not just our best aspect.

Be honest with yourself, and you'll start reaching your goals. To do this, you should first write down your strengths and weaknesses. Although you yourself should come up with your strengths, you'll probably need others to help you list your weaknesses. Reflect on your life, focusing on where you are currently, and make it a priority to change anything you don't like about it.

The author devised a form and sent it round to his friends and colleagues, asking two questions: "What do you really think of me?" and "How can I improve?" The responses were sent anonymously, and though they were brutally honest, they helped the author gain a different perspective on what he needed to change.

Another advantage of being honest is that it'll help you discover areas where you can be of value to others.

People appreciate those who are ready to admit that they're not the best but that they are pretty great at what they can do, and that they still have some room for improvement. Only once you've identified your strengths and weaknesses can you show others where your skills lie.

After recognizing those key areas, you can start to generate work around them, making sure you set your prices to reflect your skill level so that you're charging less in the beginning. When the author was starting out as a personal trainer, he charged £15 per hour. Only after he gained more experience and confidence in his skills did he begin charging around £100.

Take a deep, honest look at yourself and evaluate your skills. Focus on improving areas in which you shine, and you'll be increasing your value in no time.

### 5. To build a network, you need to be interesting and resilient to haters. 

Once you have the momentum to lift your project or goal off the ground, you'll want to tell people about it, and for that, you need to be interesting.

Being an interesting person helps draw attention to your products before your business becomes well known. To become more interesting and well-rounded, you must build and develop your own opinions and thoughts — on books and articles, for example. Then, when the opportunity presents itself, you can share your insights with others. And remember, having a diverse range of interests will make you all the more appealing.

Another great strategy for being interesting can be summed up in the quote, "If you want to be interesting, be interested." This means that you should be legitimately interested in what makes other people tick. When the author wants to find out what motivates other people, he asks them a simple question: "So, how do you spend your free time?"

Bear in mind, however, that not everyone will think you're interesting, or even like you. Instead of letting it demotivate you, use this spitefulness to boost you into action.

Haters are going to hate, and the hard truth is that there's no way to get rid of them. But be mindful that some judgments contain a small piece of truth; understand these slight criticisms to help you grow.

Make sure that you're able to separate the jealousy-filled comments from constructive criticism. It's not helpful if someone tells you that your business is awful, but if they tell you that your business doesn't have a logo, which means it might fail, then there's some valuable information you can use.

Also, keep in mind that there will always be hatred toward those who are doing well or doing their own thing. Haters are jealous and feel threatened because they haven't achieved the same level of success in their own lives. Understand that the hate is their problem and not yours, and walk away from it.

Now that you've got the gist of why being yourself, being honest about your abilities and finding a supportive network are important for living an awesome life, the next blink will show you how to turn your healthy habits into concrete goals.

### 6. You need a consistent system to get things done. 

It's not possible to talk about goals without using the words "plans" or "habits" in the same sentence. OK, maybe it _is_, but hopefully, you get the idea just how vital planning and habits are in making anything happen.

To start planning, you need to set up your priorities with a one-two-three ranking system.

This system was developed by the author, who was first struck with the idea when he observed a successful man from his town. This man achieved all of his goals by his twenties by tackling three to five items on his to-do list every day. Improving upon this system, the author added a one-two-three ranking, marking each task from a one (a supreme priority) to a three (not critical).

Before starting this ranking system, make sure you have a "brain dump." This is where you scribble down all your ideas, tasks and things to be accomplished on a piece of paper so that you can clearly see and identify which need prioritizing.

So, planning your habits seems easy enough, but how can you find the time to do it?

One strategy is to try "the dentist appointment," where you set aside a daily one-hour time slot to complete a certain task, rather like setting aside time for a check-up. Make sure you have a reliable environment that holds no distractions and where you can consistently work. The author preferred planning and doing concentrated work on the train. He picked a journey that would take two hours so he could take a break at his destination and then commit two more hours to working on the return trip.

So, we've discussed planning and routine, but there are still two more important principles coming up that will keep you on the path to success.

### 7. Surround yourself with like-minded people and don’t forget to give credit where it’s due. 

Ever shared your goals and aspirations with a friend or family member, only for them to not understand, or be unable to relate?

This is why motivation and inspiration are best fueled by connecting with people who have similar dreams. But who exactly should you be looking for, and where?

It's vital that you don't just surround yourself with those whom you look up to, but also with people who are chasing similar goals. You could try joining or creating a Facebook community that focuses on your goals, invest in a mentor or coach to help you improve or attend seminars or workshops with your target audience. The latter is just what the author did; he went to classes with fitness professionals, and through natural conversations, he was able to spread the word about his fitness-focused copywriting business and gather clients organically.

Finally, remember that people are the fundamental blocks that build most of your goals and businesses, so don't forget to be pleasant to them. Always give credit to those who have helped you — you never know when you'll need their support again.

Conversely, for those who try and shut you down — the haters — don't react or lash out. Perhaps try replying to them when you're in a calmer and happier mood, but not when you're upset. A good tip is to avoid checking your email first thing in the morning until you've established the right mood.

Also, consider what the haters are going through and that they're maybe having a rough time — perhaps they're experiencing a financial crisis, have just lost a loved one or experienced a health scare. These events could cause anyone to act out of character, so try your hardest to be understanding and patient.

We've covered a lot of ground today in these blinks, but now it's time to take in all the information and put it to use. Half of the work is learning, and the other half is action. Don't just read this and continue with your day as you normally would; put these principles to use immediately in your very next project.

### 8. Final summary 

The key message in this book:

**Now is the time to be fearless and strike out toward your goals. Don't let haters or insecurities take advantage of your limited time in this life. Instead, think about what you really want and make it happen!**

Actionable advice:

**Emotionally detach yourself from the outcome**

One important principle to begin practicing right away is letting go of the unknown outcome of a situation — that is, letting go of the crippling fear over whether your project will be a dud or a success. Detach yourself emotionally from the outcome as this will decrease your stress and make everything feel lighter and more manageable. Remember, the real failure is in not trying at all.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _You Are a Badass_** **by Jen Sincero**

_You Are a Badass_ (2013) is your guide to living life to the fullest. Sincero provides an analysis of exactly what's holding you back and provides powerful strategies geared toward breaking bad habits so that you can truly live out your dreams.
---

### Dan Meredith

Dan Meredith is the best-selling author of _How to be F*cking Awesome_. He has run his own copywriting business, premium gym service, marketing agency and a large self-help group on Facebook called "Coffee with Dan," which has more than 4,000 followers.

