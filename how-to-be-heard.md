---
id: 5a927133b238e10006d081f2
slug: how-to-be-heard-en
published_date: 2018-02-26T00:00:00.000+00:00
author: Julian Treasure
title: How to be Heard
subtitle: Secrets for Powerful Speaking and Listening
main_color: FFA936
text_color: 80541B
---

# How to be Heard

_Secrets for Powerful Speaking and Listening_

**Julian Treasure**

_How to be Heard_ (2017) identifies proven ways to become a powerful speaker, the kind that commands people's attention and keeps them hanging on your every word. Author Julian Treasure also examines the steps people can take to improve their listening skills, allowing them to better understand what's being said and making the listener feel that they're being heard. These tips can help improve both communication skills and our most important relationships.

---
### 1. What’s in it for me? Make yourself heard and make those around you feel heard. 

Now more than ever, there are countless distractions vying for our attention at any given moment. If you hope to reach someone and have your message be heard, you need to know how to get someone's attention and keep them focused on your words rather than on their Twitter feed. This takes a keen understanding of what people want to hear and how they want their information delivered.

These are the details author Julian Treasure explores, outlining how any successful communication is a two-way street, with a confident speaker on one side and a receptive listener on the other. Treasure's advice isn't just useful for public speakers — this is advice that can help any relationship suffering from frequent communication breakdowns.

In these blinks, you'll discover

  * how sound can reduce crime;

  * how one bad joke cost a jewelry chain £500 million; and

  * what a "sodcaster" is, and how you can avoid being one.

### 2. Sound has the power to affect us in many different ways. 

Of course sound is important to us, even when we don't pay attention to every single sound around. But, surprisingly enough, sound also has physiological and psychological effects on us.

In the physiological sense, sound can influence our overall health, especially when it comes to getting a good night's sleep. According to the World Health Organization, around 8 million people in western Europe suffer from sleep deprivation due to traffic noise that exceeds acceptable levels. This not only leads to higher levels of stress and depression, but it also weakens the immune system and makes us more prone to violence and anger.

As for the psychological or emotional effects of sound, a study from Sweden's Lund University describes how music can trigger emotional associations and images in our mind's eye. For example, when people hear the first two notes of John William's classic theme song from _Jaws_, a sense of fear is automatically heightened as the image of a shark enters their mind.

In a similar vein, sound can also affect us on a cognitive and behavioral level, having a notable impact on how we think and how productive we are.

Research shows that ambient noise in an open workspace, such as overheard conversations, are so distracting that they can reduce productivity levels by two-thirds. Likewise, a survey of 1,800 British workers showed that approximately two hours of productivity each day were lost due to unwanted noise.

Last but not least, there's the way sound can change our behavior.

The mayor of Lancaster, California, R. Rex Parris, tried to reduce crime in the city by setting up speakers throughout the downtown area, known as The Boulevard, that played bird songs, lapping water and other calming sounds. According to the town's Sheriff, crime in Lancaster subsequently dropped 15 percent — and the town has also made plenty of media headlines as a result.

Now that we have a better understanding of the potential of sound, let's look at some of the obstacles it faces.

### 3. Obstacles to effective communication include hyperbole, people pleasing and avoiding difficult emotions. 

When you're saying something and no one's listening, it can leave you feeling utterly defeated. So let's see how you can improve your communication skills by first looking at some common pitfalls that should be avoided.

We often use hyperboles, like "amazing" and "genius," to describe normal, everyday things like a new pair of shoes. We do this to get a favorable reaction or to impress others, but it also devalues the true meaning of the words, thereby making them less effective.

Another habit that reduces effectiveness is our overwhelming desire to be right, and when we're so focused on this, we tend to miss out on what the other person is really trying to say. In studying the communication between physician and patients in the United States and Canada, researchers found that patients are interrupted and corrected by their doctor, on average, 18 seconds into their statements.

Your message can also suffer when you're overly concerned about _people pleasing_, or seeking approval from others.

This is perhaps most common during our teenage years, an impressionable time when we tend to dress and act like others in the hopes of fitting in. Unfortunately, people pleasers will often be seen by listeners as lacking genuine authenticity, and they'll thus doubt whether or not the speaker can really be trusted.

Another roadblock to good communication is how we deal with difficult, emotionally charged situations. Quite often, people will keep quiet or try to delicately fix the emotions surrounding an incident.

For example, the author's aunt recalls a time when her parents came back from the hospital after her pregnant mom was supposed to deliver a baby. Instead of telling her that the baby was stillborn, as was the case, her parents remained silent. The confusion and difficult emotions around this incident would continue to affect their communication for years to come.

### 4. Listening skills are honed through experience, and what we say can have serious consequences. 

People love to debate how much of our strengths and weaknesses are determined by genetics. Naturally, our aptitude for listening and speaking has also entered into this discussion. However, research has shown that our ability to listen is determined more by our personal experiences than our genes.

In studying the lives of twins, the data suggests that their abilities are directly determined by their own unique experiences. Even identical twins with nearly identical genes and a similar upbringing can develop very different listening skills.

If one twin spent more of his childhood in front of the TV while the other had his nose in a book, it's likely that the bookworm will develop much better listening skills than his sibling.

The data also shows that there's an emotional component to our listening skills. For example, when a couple is in the honeymoon phase of their relationship, they tend to be much more open and intent in their willingness to listen.

As for speaking, the important thing to remember is that we need to pay close attention to the words that come out of our mouths, since they greatly influence what people think of us.

Take an infamous speech made by Gerald Ratner in April 1991 at the Institute of Directors Conference. At the time, Ratner was representing his chain of jewelry stores in the United Kingdom, named Ratners. In his attempt to give a funny, self-deprecating speech, he said that his store's products are so affordable because they're "crap," and are a lot like a prawn sandwich but don't last nearly as long.

In a shining example of the power of words, Ratner's comments quickly made newspaper headlines, causing customers to avoid the stores and the company's value to plummet by £500 million. One moment Ratners was a leading jeweler, the next — after Gerald Ratner's speech — it was facing bankruptcy.

> "_Hearing is a miracle, and far too complex and extraordinary to be taken for granted the way we do."_

### 5. Eye contact and empathetic listening help build strong, communicative relationships. 

Do you know someone who avoids movies with a lot of dialogue because he finds them hard to follow? Or maybe you work with someone who's always asking others to explain what just happened in a meeting. These are just some of the telltale signs that a person could use some help improving their listening skills.

Fortunately, there are some quick tips that can help anyone become a better listener.

For starters, making eye contact will automatically result in better listening. In his book _Bodily Communication_, social psychologist Michael Argyle says we make eye contact 70 percent of the time while we are listening, compared to only 40 percent of the time when speaking.

Eye contact is a sure sign that we aren't trying to multitask while listening, since multitasking spreads our attention quite thin and leaves far fewer mental resources available to process what's being said. So, with eye contact, we're not only showing respect by giving our complete attention, we're also bound to gain more insight into the feelings and intent of the speaker — and therefore be a better listener.

Another way to get the most from a speaker's words, and to create stronger relationships, is to practice _empathetic listening_.

According to Marisue Pickering, a leading voice on interpersonal communication at the University of Maine, there are four traits to empathetic listening: putting the feelings and thoughts of others before our own; letting your guard down and being open with your emotions and opinions; imagining ourselves in the experiences and perspectives of others; and, finally, avoiding judgment or criticism while being receptive.

By doing these things and being an empathetic listener, you're bound to understand someone's emotions better and, as a result, build a closer relationship with them.

Parents often make the mistake of practicing critical listening rather than being empathetic. When a child shares her worries and difficulties, it's common for a parent to respond either with criticism or with a plan for how to fix the problem.

Neither of these responses offers the child a sense that she is being emotionally understood, which can weaken the relationship between the two and prevent the child from sharing her thoughts and feelings in the future. But if the parent tunes into the child's emotions and validates her feelings, their bond will improve.

> "_It's like going onto their island and experiencing their reality, rather than staying on your own turf . . ."_

### 6. Get your message across by using storytelling and clear, unambiguous language. 

Even though speaking and listening go hand in hand, society places a greater emphasis on how to talk. And since we've already covered some strategies for better listening, let's look at how to improve your chances of being heard.

One of the best methods to ensure you are being heard is to employ classic storytelling tropes, such that your words resonate with your audience.

Everyone likes a suspenseful tale with a hero going on a journey that has a happy ending. In fact, researchers from the University of Vermont and the University of Adelaide looked at 1,737 pieces of fiction and found "rags to riches" stories such as _Cinderella_ are the most popular. So consider following a template like this, if you can.

Another storytelling tool you can use is to speak from a place of intention. When the social justice activist Bryan Stevenson gave a TED Talk in 2012, he was eager and determined to shed some much-needed light on the inequality of the US justice system. The audience could feel how personally invested he was in this topic and was riveted by his presentation, giving him a long standing ovation at the end.

But these tools won't be very useful unless your message has clarity. In the world of academia and politics, you'll often find people using lingo and words that tend to confuse and bore the average listener. And the best political speakers, like former US presidents John F. Kennedy and Barack Obama, win applause by sticking to simple, clear language.

In 1961, Kennedy boosted the morale of millions of Americans by saying, "I believe that this nation should commit itself to achieving the goal, before this decade is out, of landing a man on the moon . . ."

Likewise, one of Obama's trademark efforts was the Plain Writing Act of 2010, which forbid federal agencies from using language that the general public can't understand.

> "_If you want to seize and hold people's attention, become a good storyteller."_

### 7. To avoid muddled or annoying speech habits, use good posture and be aware of your volume. 

In our current culture, pop stars and actors are put on a pedestal, based on the broad assumption that they were born with a natural talent for performing. But there's no reason to buy into this misconception. The truth is, performance is a skill.

So if you can't stand public speaking now, there are ways to improve.

The first piece of advice for anyone hoping to be a great public speaker is to have good posture. Certain postures and repetitive behaviors can give us a tense voice, which is ill-suited to public speaking. One such posture is a "text neck," which develops from all the time we spend hunched over laptops and looking down at our devices.

Here's how you can find out if you have a text neck: stand up straight and put your back up against a wall. If your neck is naturally jutting forward, then the muscles in your neck could use some exercise. While your back is against the wall, you can help straighten out this posture by imagining a string pulling your head straight up while your chin is tucked in. Practice this exercise for one minute every day.

Another important piece of advice is to be aware of your volume. _Sodcasting_ is a new word that's used to describe people who are unaware of how their loudness imposes on others. While it was initially used to describe young people blaring their music on buses and trains, it can also describe how someone is clueless about the volume of their voice.

Once, the author was in an airport lounge, with people peacefully browsing on their laptops. Then, in walked a man who was on his phone, carrying on his half of the conversation so loudly that everyone in the room was instantly annoyed. Don't be this person.

Instead, always be aware of the volume of your voice and how to use it. If you're too loud, you can irritate your audience and your message could quickly be lost.

### 8. Final summary 

The key message in this book:

**To become an effective speaker, you must first learn to be a good listener, since the two skills are closely related. Practicing empathetic listening will enable you to become consciously aware of what you're saying, and will lead you toward the most effective way of articulating it to others.**

Actionable advice:

**Turn off all the alerts for your incoming e-mails and messages.**

Before you start your day and begin working, turn off the alert notifications for incoming e-mails and messages on both your computer and your phone. Every time you receive an alert, your concentration is disrupted, which greatly affects your productivity. Instead, make it a habit to check your e-mails and messages just three times a day: after waking up, during lunch and at the end of day.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Sonic Boom_** **by Joel Beckerman with Tyler Gray**

_Sonic Boom_ (2014) highlights the many ways sounds permeate not only our environment but also our personal lives. From your company brand to your personal space, sound has the power to make us remember and even can encourage us to buy. These blinks show you exactly how to harness the power of sound in your business and life.
---

### Julian Treasure

Julian Treasure is an acclaimed international speaker whose TED Talks have accumulated over 40 million views and have been ranked among the top ten TED Talks of all time.

