---
id: 5e5914a06cee07000673bbec
slug: how-to-be-right-en
published_date: 2020-03-02T00:00:00.000+00:00
author: James O’Brien
title: How to Be Right
subtitle: In a World Gone Wrong
main_color: None
text_color: None
---

# How to Be Right

_In a World Gone Wrong_

**James O’Brien**

_How to Be Right_ (2018) looks at some of today's most divisive issues through the unique lens of author James O'Brien. As the liberal host of a radio call-in show, O'Brien has gotten into plenty of arguments with people who've tried to convince him of one point or another. Using some particularly memorable conversations as examples, O'Brien shows how many of today's popular opinions can be dismantled by applying some scrutiny and sticking to the facts.

---
### 1. What’s in it for me? Find out how a professional talk show host takes down misguided opinions. 

James O'Brien's radio phone-in has over a million listeners. Over the past 14 years, he's been in a unique position to hear from a wide variety of British citizens and listen to their concerns. One thing he's come to realize is that many people these days are taking what they read online for granted. They're not questioning or challenging the opinions they're exposed to.

In talking to a wide variety of callers, O'Brien has gained a great deal of experience in getting people to look at the bigger picture. In fact, many of his conversations have gone on to become viral sensations thanks to O'Brien's ability to tear apart a flimsy argument.

For many of today's most pressing issues, from immigration and Brexit to feminism and Trump, O'Brien has a few memorable callers. Each of these conversations perfectly reflects the ongoing problem of unchallenged opinions. The conversations also show just how easily badly formed opinions fall apart once someone begins to challenge them with logic, reason, and facts. In these blinks, we'll analyze some of O'Brien's conversations and discover how to uncover the truth behind the news.

In these blinks, you'll also find out

  * how pro-Leave arguments can quickly fall apart;

  * how arguments against political correctness can be a reaction to a false rumor; and

  * how examples of a "nanny state" are often worthy fights against corporate greed.

### 2. The treatment of Islam in the press has led people to embrace dangerous ideas that threaten human rights. 

When the author was growing up in Britain in the 1970s and 80s, there were extreme tensions brought on by IRA bombings. At the time, his father was a reporter, and because of the apostrophe in his last name, he received letters accusing him of supporting the Irish militants and "having the blood of murdered children on his hands."

Unfortunately, the kind of logic this demonstrates hasn't diminished over time. Only now, rather than every Irish person being looked at as a potential terrorist, it's every Muslim. In fact, thanks to online comment boards and social media platforms like Twitter, it would appear that the willingness for broad generalizations has only increased.

But perhaps even more disconcerting is that news and media outlets like the _Sun_, the _Daily Mirror_, Fox News, Breitbart, and even the _Daily Telegraph_, have used fear-mongering tactics to grab readers' attention, stoking tensions even further. _The Sun_, which is the best-selling newspaper in the UK, ran commentary under the headline, "If We Want Peace… We Need Less Islam."

As the host of a radio call-in show, the author has spoken with people who've clearly been influenced to categorize all Muslims as being somehow guilty of terrorist acts.

One such caller was Richard, from the town of Marlowe, who felt that Muslim people owed an apology for the attack on the Paris offices of _Charlie Hebdo_. After all, those responsible for the attack claimed to be acting in the name of Islam. The author spoke with Richard for some time about how it was far from reasonable to ask someone who had nothing to do with the attack to apologize because a person claiming responsibility invoked the word "Islam." To make his point, O'Brien raised a scenario where someone committed an act of terrorism in the name of Richards everywhere. Surely, Richard the caller wouldn't feel the need to apologize, right?

Unfortunately, in the years that followed, people continued to call with similar views to Richard. One caller by the name of Martin suggested that Muslims as a group need to be better at "weeding out their own bad apples." Indeed, it seems the fact that even though there are multiple, very distinct and different branches of Islam, such as Sunni and Shia, too many still stick to the popular idea that all Muslims are the same.

### 3. Many Brexit supporters were seemingly fooled by a deceitful Leave campaign that went largely unchallenged. 

These days, it's all too easy for an idea to enter the mainstream without being properly scrutinized. So, one of the things the author tries to do on his radio show is to challenge the opinions that are being lazily retweeted and shared on social media.

Often, he simply asks the callers to defend and support their positions with details and facts. This was certainly the case with callers who accepted the reasons pro-Leave spokespeople gave for wanting to break from the EU. One of the more popular reasons was that Britain was being forced to obey unfair or somehow hurtful EU laws.

Caller Andy, from Nottingham, voted to leave under the belief that Britain would benefit from "independence and so we could control our own laws." As he does when any caller brings up "laws," the author asked Andy to name one law that he was eager not to be forced into obeying.

After some back and forth, Andy admitted he couldn't name one law, shifting his position to immigration, which is what many conversations about Brexit ultimately end up focusing on. This is because the pro-Leave faction, which has the support of right-wing news outlets like the _Daily Mail_, has regularly suggested that immigration has somehow been harming the economy, driving down wages or otherwise affecting the daily lives of the average British citizen.

The facts, however, are that the only evidence of wage compression related to immigration is strictly a small one in the unskilled labor market. This wouldn't affect Andy, who'd recently started his own business. So in the call Andy admitted that it was seeing "mobs of immigrants" in the city center who were "not willing to integrate" that was really bothering him. The author then asked how leaving the EU was going to change anything about the supposed mob of immigrants who were already living in Nottingham.

Andy then back-peddled a bit, clarifying that it was not a matter of race, and that he didn't like mobs of "Englishmen" in the middle of town either. By the end of the conversation both Andy and the author were laughing since he seemingly voted Leave because he doesn't like mobs of people or following laws he doesn't know anything about.

But what isn't funny is that Britain's decision to leave is already hurting small businesses like Andy's. And while the pro-Leave faction claims that the initial economic downturn will be short-lived, this is another statement that requires a lot of unquestioning faith to believe in.

### 4. When people oppose homosexuality on moral or religious grounds, their argument doesn’t bear scrutiny. 

Unlike other issues that become topics of public debate, no one is making the claim that homosexuality is taking away jobs or eating up public tax money. Indeed, the argument against homosexuality often comes from a moral standpoint, though the origin of that morality is usually dubious when questioned.

Take for instance, the persistent belief that homosexuality is a personal lifestyle choice, which, it bears pointing out, is a belief that has so far only been voiced by male callers on the author's show. Whenever this line of argument comes up, the author has a standard reply: "So when did you choose to be straight?" This often cuts right to the absurdity of the argument since it implies that everyone is equally attracted to all sexes and makes a decision. The caller may then get flustered and bring up religion.

Caller David suggested that the Bible, both the Old and New Testament, is unambiguous in telling us that homosexuality is a sin. When it comes to the New Testament, the author likes to point out the difference between the Gospels, and what Jesus is quoted as having said, and what someone like Saint Paul, who never met Jesus, said. It's accurate to say that Jesus was never quoted as saying anything about homosexuality. That means more than a letter that Paul wrote about his personal concerns, which has been open to a lot of different interpretations over the years.

One of the most frequently cited passages of the Bible concerning homosexuality is in the Old Testament: Leviticus chapter 18, verse 22, which reads, "You shall not lie with a male as with a woman; It is an abomination." If someone brings this up, you can then remind them of the fact that chapter 11, verses 10 to 12, also state that it is an abomination to eat anything from the sea that does not have "fins and scales."

If that's not enough to convince someone that certain parts of the Bible might be outdated, you can remind them that Leviticus chapter 19, verse 19, also considers wearing "garments made of two different kinds of thread" to be an "abomination." And working on the Sabbath? That's considered bad enough to be stoned to death.

> _"Caller: 'It's against nature.' James: 'Not their nature. Anyway, flying thousands of feet in the air in a giant tin can is against nature, but you don't get your knickers in a twist about that.'"_

### 5. The phrase “political correctness” has become politicized and used to incite anger over non-issues. 

George Orwell's _Nineteen Eighty-Four_ envisages a future dystopia that continues to impress with its insights into modern society. One such detail is what the book calls "Two Minutes Hate," in which the people are prodded into a daily ritual of getting angry about abstract and unclear matters. It doesn't matter what the subject is — just that people feel enraged. In fact, the point is that people can easily redirect that rage anywhere.

This anger is exactly what the tabloid newspapers in Britain excel at. Every day, they give people some vague and often nonsensical reason to be angry. Often, these excuses to get enraged are framed as an example of "political correctness gone mad!"

A classic example of this is the legacy of _Winterval._ Winterval is a harmless bit of news that has persisted for years thanks to right-wingers who want everyone to believe British traditions are being corrupted by cultural invaders. The common misconception is that the word Winterval was introduced to replace Christmas. As caller Andrew from Erith complained, "You can't celebrate [Christmas] in case it offends other people… You have to call it Winterval now."

But if Andrew had actually questioned this idea rather than accepting it at face value, he'd have found out that Winterval was an idea invented by Mike Chubb, a city planner in Birmingham. All Chubb wanted to do was find a way to make the end-of-year festivities in the city last longer. By calling it Winterval, which is short for winter festival, Chubb believed he'd found a way for the city to have a three-month celebration. The goal behind this lengthy festival was to cut costs by only having to put up one set of decorations for multiple holidays. Included in these holidays was, of course, Christmas.

Nobody was trying to rename Christmas or suggest that celebrating Christmas in Britain was going to offend anyone. It was only a matter of one person trying to save a few pounds by putting together a festival for all celebrations taking place from October to December.

Nevertheless, outlets like the _Daily Mail_ saw it as an opportunity to frame it as another example of political correctness gone mad, and it's not the only time they've done it. Similar misrepresentations of the facts have surrounded allegations of the Union Jack flag being taken down from public buildings in order to appease Muslims. But if anyone were to question this rumor, they'd find out that, as with Winterval, it's simply not true.

### 6. In conversations about feminism, people have revealed a desire to return to troubling traditions. 

When people complain about immigration and political correctness, they often say they're worried about the erosion of traditional values. But the truth is, many traditions aren't necessarily worth holding on to.

Consider the troubling fact that in 1984 UK courts wouldn't consider it rape if a married man forced his wife to have sex. And it was only in 1975, with the passing of the Sex Discrimination Act, that husbands and fathers were no longer asked to step in as guarantors when women sought money loans.

Clearly, you don't have to look far into Britain's past to find social values that are better left in the dustbin of history. Yet, whenever the issue of feminism pops up on the author's radio show, it's astonishing how easily people judge equality of the sexes as an attack on "men's rights." And when this argument comes up, it can only be seen as a desire to turn back the cultural clock to a time when women had little say in decisions about sex, safety, and objectification.

One of the more disturbing examples of this kind of thinking followed the 2018 incident where 25-year-old Alek Minassian killed ten people in Toronto and used his status as an _incel_ — or "involuntarily celibate" — as the reason for his rampage. In the wake of this tragedy came the opinions of Canadian psychology professor and internet phenomenon Jordan Peterson. Peterson advocated for what he called "enforced monogamy." In his view, some form of "sexual redistribution" needs to occur because if women are free to choose their sexual partners they will invariably choose men of "high status," leaving men like Minassian to feel angry at God and act out with violence.

Peterson has a lucrative YouTube channel that earns him a reported $80,000 a day. His books have sold over a million copies. So while you may find the idea of "sexual redistribution" hard to take seriously, it's difficult to say that others haven't been listening. In fact, Peterson's gained a sort of hero status among the alt-right movement, as well as those worried about "men's rights."

But even intelligent people can wring their hands about feminism and worry that men will soon be unable to say anything in the workplace without being called a pig. But once again, this is an unrealistic and exaggerated concern. To these people, you can remind them that, historically speaking, the real concern is that rampant sexism has always been a sure sign of growing fascism in society.

### 7. Arguments about a “nanny state” are often a cover for feelings of selfishness and superiority. 

Like the dangers of Islam, immigration, political correctness, and feminism, you've probably heard libertarians complain about "nanny states." This is the term often invoked whenever a nation's tax money gets put to use in order to protect us from ourselves.

More specifically, arguments about this topic arise because certain advocates or politicians have taken steps to try to protect us from rampant capitalism, where there's no limit to what corporations can do in the name of profit.

Take caller Henry who phoned in to complain about a proposed sugar tax that would require people to pay a little more for sodas and sugary fruit-flavored drinks, which can contribute to obesity and diabetes. Henry believed that the sugar tax was unfair since he likes to drink the occasional soda and why should he have to pay more just because other people are, in his opinion, "too stupid" to understand that too much sugar is bad.

This kind of elitism is common around the topic of nanny states. There have always been people who like to think that things like being born with more money or access to education makes them more entitled and somehow better than others who don't have such advantages. And it's worth pointing out that phrases such as "nanny state" are essentially a way to cloak a spirit of selfishness. These people simply don't want to see money from their taxes going to the less fortunate.

But at the heart of this issue is a misunderstanding that we're somehow _not_ being manipulated by corporations with millions of pounds at their disposal. Corporations like fast food franchises with powerful advertising campaigns that shamelessly target kids. Or gambling machines which have been expertly designed to be addictive and separate people from their money.

The author made this point when caller Gary complained about celebrity chef Jamie Oliver's efforts to improve school lunches. As far as Gary was concerned, Oliver should mind his own business and not try to tell him what his kids should be eating. The author had to remind him that both schools and fast food corporations care more about money than his kids' health, so maybe having one guy from a TV cooking show advocating for better meals for kids isn't such a bad thing.

### 8. Trump reveals just how effective it can be to provide people with scapegoats and get them to shut off their brains. 

If there's one thing that the political rise of Donald Trump has taught us, it's that politicians can go far by telling people what they want to hear. In this case, it's appealing to a sense of entitlement and offering people a scapegoat for their anger. In particular, Trump appeals to the white people who carry the notion that they are of a higher status, and he uses very simple slogans and catchphrases like "Lock Her Up" and "Fake News," that allow those people to turn off their brains.

From early on, Trump excelled in blurring the line between lies, truths and alternative realities. This is essentially what Trump does when he uses the term "fake news" to describe facts. At the same time, he puts out lies, like having the turnout for his swearing-in be described as "the largest audience ever to witness an inauguration."

The author was able to track just how quickly Trump's ideas were able to gain traction in Britain on his radio show. It wasn't long before his callers were using the "fake news" tactic to try to win arguments.

For example, in July 2018, Trump was scheduled to visit London. Many were angry that protesters were allowed to fly a balloon in the form of Trump as a big orange baby wearing a diaper on the day of his arrival.

Caller Jack phoned in to voice his disapproval and to suggest that Trump, in his standing as a US president, deserves nothing but respect. Jack used an analogy, asking that Trump should be looked at like a step-father and that you wouldn't disrespect your step-father if he were coming over for a visit, would you? The author elaborated on this hypothetical scenario by asking whether this was the same step-father that publicly mocked a disabled journalist as well as the Gold Star family of a fallen US soldier. To which Jack replied with multiple cries of "Fake news!"

But anyone can take a quick look at official records to see that Trump did indeed mock these people publicly because they didn't agree with his policies. Trump's past behavior also includes the very real time he once bragged about grabbing women "by the pussy." So, if this were indeed the author's step-father, then it's unlikely that he would have let him in his house — or even have invited him over in the first place.

### 9. Final summary 

The key message in these blinks:

**These days, thanks to the abundant use of social media platforms and comment boards cloaked in anonymity, many people are getting away with providing unchallenged opinions. This even extends to politicians and the people behind such developments as Brexit and the election of Donald Trump. Many of the right-wing talking points, like anti-Islam positions, panic over immigration, and political correctness, do not stand up under scrutiny. On his radio talk show, James O'Brien has spoken with many people who have latched on to right-wing talking points, and found that they soon fall apart once you challenge these ideas with facts and simply let their proposals play out to their logical, misguided ends.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next: _Talking to Strangers_** **, by Malcolm Gladwell**

_How to Be Right_ shows how wrong-headed arguments can fall apart under scrutiny, but talk radio isn't exactly the same as being face-to-face with someone who has a very different opinion than you. How do you bridge gaps, empathize and communicate with people who see the world in a completely different way?

For the answer to a very relevant question like this, we recommend our blinks to _Talking to Strangers_. It is a powerful exploration of how we misjudge and misunderstand other people, sometimes with terrible consequences, making a powerful case for more tolerance and patience in our dealings with others.
---

### James O’Brien

James O'Brien has been the host of his own current affairs radio call-in show on the London-based talk radio station LBC for over 14 years. He's also had his own daily talk show on ITV and has been a presenter on BBC Two's _Newsnight_. As a journalist, he's contributed to a variety of news outlets, including the _Daily Mirror_ and _TLS_.

