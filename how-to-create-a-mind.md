---
id: 56cb941b9bb2160007000063
slug: how-to-create-a-mind-en
published_date: 2016-02-22T00:00:00.000+00:00
author: Ray Kurzweil
title: How to Create a Mind
subtitle: The Secret of Human Thought Revealed
main_color: 54B7D2
text_color: 336F80
---

# How to Create a Mind

_The Secret of Human Thought Revealed_

**Ray Kurzweil**

_How to Create a Mind_ (2012) offers an intimate examination of the nuts and bolts behind how the brain works. Once we understand exactly how people think, perceive the world and decide to take action, the creation of true artificial intelligence seems a possibility that's just around the corner.

---
### 1. What’s in it for me? Find out what the human mind is made of (and how to build one yourself). 

Sure, computers can do amazing things. The incredible speeds at which computers can crunch numbers and solve problems is astounding — and that's only the beginning.

Yet we're seemingly still a long way from conscious computers, or robots that are genuinely creative or that possess free will. Or are we?

In these blinks, you'll learn the basics about how the human brain works and processes information — and quickly understand that there's no reason to think that a powerful computer couldn't do the same, at least in theory.

In these blinks, you'll learn

  * why the smallest whiff of perfume can unearth a wealth of memories;

  * how you brush your teeth elucidates how your brain handles information; and

  * when exactly artificial intelligence will become as commonplace as the washing machine.

### 2. The human brain stores information in a strict, orderly fashion. 

Memory is a curious thing. You might not think about an event for what seems like years, but a small, sudden detail — like the smell of your grandma's cookies — brings a moment back with amazing clarity.

Interestingly, this phenomenon tells us a lot about how information is organized in the brain.

The brain stores and organizes information according to patterns. Think, for example, of the last time you were walking down a sidewalk. How many details can you remember of this trip? Could you describe even a single person who might have passed you by?

Such common details can be hard to visualize. However, with the help of certain techniques, a sequence of memories can be coaxed back.

Police sketch artists, for example, show a person a collection of varying facial features to help trigger and recover memories. Because the brain stores information as a sequence of patterns, seeing a similar set of eyebrows could help a person recall the rest — in this case, a face.

Triggers like this occur all the time. The things you see, smell or otherwise experience with your senses (like grandma's cookies) can unlock memories from ages ago.

Countless memory techniques are based on this pattern principle: if you can get your hands on just one piece of a pattern, the rest of the sequence will reveal itself.

Some simple thought experiments show exactly how the brain retrieves information.

You can no doubt recite the alphabet. But can you say it backwards? In theory, this should be a simple task. Your brain has all the information it needs, stored away somewhere. However, accessing that information any other way than from A to Z is challenging!

Similarly, it's difficult to play a piece of music you've memorized if you start not at the beginning but at some point in the middle. This is because the brain stores information in _sequential order_. Thus starting out of order feels "unnatural," and the brain struggles to put the pieces together.

### 3. The neocortex is the part of the brain responsible for our memory and thinking. 

The _neocortex_ is the brain's outermost layer and accounts for the majority of its mass. Its size is unique to modern human beings. The neocortex is responsible for many important brain functions, such as object recognition, language comprehension and body control.

The brain's neocortex stores and organizes information hierarchically. This makes sense, as everything we do, including thinking, can be broken down into hierarchies and patterns.

To illustrate this, think of the steps involved in the process of getting ready for bed. Most likely, your first step is to brush your teeth. But within this step, there are other steps: putting toothpaste on the toothbrush, actually brushing, rinsing with water and so on. 

Your brain stores these steps-within-steps in _cortical columns_, or uniformly organized groupings of neurons found in the neocortex. There are about 500,000 such columns in the neocortex, each containing about 60,000 neurons.

Within each cortical column are _pattern recognizers_, comprising of about 100 neurons. All in all, there are approximately 300 million pattern recognizers in the neocortex.

Some of these are low-level recognizers that fire whenever certain pieces of a pattern are present. This information then flows to the higher-level recognizers, which put the pieces together.

For example, when you look at a word, low-level recognizers will fire when they see the lines and shapes that distinguish individual letters. High-level recognizers then detect the combinations of letters that create a complete word.

Our senses constantly receive input, and this input is compared with past experiences and patterns. To keep up, the brain's recognizers fire continuously!

Thanks to pattern recognition, your brain can usually predict what a thing is even if you see only a small part of it — and even if it's something you've never seen before!

> _"Human beings have only a weak ability to process logic, but a very deep core capability of recognizing patterns."_

### 4. All “roads” in the human brain lead to the neocortex. 

Like any good manager, the neocortex is a proficient networker. Depending on the task, it cooperates with other parts of the brain to get a job done. 

Whenever we see, hear, touch or smell an object, the sensory information we receive starts its journey in the _sensory cortex_ and moves through the _thalamus_ before reaching the neocortex.

Located in the midbrain, the thalamus plays a part in the instinctive reaction as to whether a sensation, like a touch or taste, is pleasing or displeasing, before that information is passed to the parts of the neocortex called the _insula_.

The thalamus and neocortex are in constant communication. In fact, this communication is so critical that a person with a damaged thalamus could fall into a coma — or even die. 

The neocortex relies on another specialized part of the brain, called the _hippocampus,_ a small region located in each brain hemisphere that tells the neocortex which events to remember.

Whenever sensory information reaches the neocortex, the hippocampus decides whether it's connected to something new or noteworthy, or perhaps a twist on an older sensation. Then the neocortex treats the information accordingly.

For example, your hippocampus is important in recognizing new faces, and keeps you on good terms with friends by alerting you if someone has a new haircut. Many symptoms of Alzheimer's disease, such as trouble learning new things, are caused by a damaged hippocampus.

When it comes to controlling movement, the neocortex divvies up the work with another part of the brain, called the _cerebellum_.

The cerebellum is responsible for certain instinctual reactions, called _basic functions_. When you instinctively reach out to catch a rapidly thrown baseball, that's the cerebellum at work. 

The cerebellum is actually a relic of a time when the human neocortex was much smaller. Nowadays, most movement is controlled by the neocortex. Surprisingly, however, refined or graceful movements, such as dancing or penmanship, are still controlled by the cerebellum.

### 5. Even a person’s sense of creativity and feelings of love are produced by the brain’s neocortex. 

To be human is truly fantastic. So fantastic, many think, that quintessentially human experiences and activities, such as creating art, couldn't possibly be accounted for by purely neurological processes. 

But is this really true?

As you learned earlier, sensory input from the world travels to the _insula,_ a portion of the brain's neocortex. And it's here that _spindle cells_ originate. 

These cells are long neurons that connect relatively distant parts of the neocortex. Spindle cells are heavily involved in creating feelings of sadness and anger, or love and sexual desire.

The other areas of the neocortex try their best to control these feelings and make sense of them. But excited spindle cells are difficult to tame, as they're connected to so many different areas of the brain. And this is the reason why people don't always make the best decisions when in the throes of passion or anger!

Similarly, creativity originates directly in the brain's neocortex. The larger your neocortex, for example, the more profound your creative potential.

One of the inherent qualities of the pattern recognizers that make up the neocortex is the ability to create metaphors and to interpret multiple meanings from symbols, a key element of poetry and art.

Pattern recognizers fire 100 times a second, allowing your brain to potentially recognize millions of metaphors in a single second.

If we were to somehow strengthen the neocortex, it could result in a person becoming more creative. One way to do this in practice, however, is through collaboration. After all, two heads are better than one!

In the future, however, we might expand human intelligence by incorporating some kind of nonbiological neocortex, or artificial intelligence.

Clearly, modern science has a good grasp on how the brain's neocortex and other regions create the human mind, as we understand it today. The next blinks will explore how we could apply this knowledge to machines, with the goal of developing true artificial intelligence.

### 6. Current technology is already far down the road toward creating artificial intelligence. 

Inventors don't always start from scratch. Sometimes they find inspiration for innovation in nature. 

One such bit of inspiration is right between your ears: the brain. If we could create an artificial mind, why not simply develop a "digital" neocortex that emulates the functions of the human brain? 

The secret to creating an artificial mind lies in developing systems that learn by themselves — just like the brain's neocortex. And thanks to one statistical method, this is now becoming possible.

Research into _artificial intelligence_ (AI) dates back to the 1930s and 1940s, the early days of computing. 

Initially, researchers were daunted; even if they could develop a sufficiently powerful processor, they'd still have to feed it tons of information to create a truly artificial mind. At the time, this was a considerable obstacle.

All this changed in the 1980s when scientists started working with a mathematical solution based on probabilities. This method, called the _hierarchical hidden Markov model_ (HHMM) after Russian mathematician Andrei Markov, empowers software to learn and become smarter, much like a child. 

Software that employs this model learns through pattern recognition. Just as the pattern recognition performed by the brain's neocortex, HHMM is based on a hierarchical system.

First, the software makes a prediction about what will come next in a series of data. For example, if it reads a three-letter word that starts with a T and an H, it might predict that the next letter will be an E, based on its past learning experience. If the prediction is correct, this triggers another prediction in the next level of the hierarchy, that the following word in the series would be a noun.

This method is being used today with voice recognition software, for example. What's more, variations of HHMM-based software are being used by millions of iPhone users when they ask Siri, Apple's automated assistant, where the nearest restaurant might be.

> _"The story of evolution unfolds with increasing levels of abstraction."_

### 7. Conscious artificial intelligence with free will is more than just the stuff of science fiction. 

If you've ever tried to chat with iPhone's Siri or any other online bot, you know such software is seldom convincingly human. At least not yet. 

But Artificial intelligence (AI) systems are quickly becoming smarter and more autonomous. IBM's _Watson_, for example, provides an early look at the possibilities of AI. In 2011, millions of people watched as Watson defeated some of the best contestants on the American game show, Jeopardy!

Using a combination of programs, Watson was able to read and comprehend 200 million pages of natural-language documents, including all of Wikipedia. Even the tricky, pun-filled phrasing of the quiz show's clues didn't phase the processing power of Watson. 

Watson works by combining all possible answers to a question, and then performing a statistical analysis to identify which answer is likeliest to bet the right one. 

But what if Watson were instead programmed to grapple with the problems of the world? Could artificial intelligence even develop a consciousness, or free will?

Philosophers have long pondered what it means to be conscious. Rene Descartes famously said, "I think, therefore I am." He essentially meant that so long as a person is aware of having an experience, then that person is also conscious. But what about free will?

The concept of free will too is still hotly debated. Indeed, research has shown that the brain actually initiates actions before a person is even aware of the decisions he or she is about to make. It could be the case even that the idea of free will is really more a feeling than a reality.

The author argues that when it searches through all that self-attained knowledge, Watson actually is thinking.

And if today's machines can emulate the brain enough to think, future machines could think and perform acts of free will, assuming that free will exists, in a way that will make machines difficult to distinguish from humans.

> _"...the digital neocortex will be much faster than the biological variety and will only continue to increase in speed."_

### 8. In the 2030s, genuine artificial intelligence will be commonplace in society. 

If we can indeed create true artificial intelligence, when can we expect such machines to be a "normal" part of society? 

As it turns out, technology is rapidly catching up with the requirements of AI. The author's so-called _law of accelerating returns_ describes the predictable, ever-accelerating growth of information technology. Assuming technological development follows this trajectory, in the very near future it will be advanced enough to simulate the human brain.

Intel has already begun manufacturing 3D computer chips which effectively circumvent the problem of space on a circuit board by stacking components on top of each other, rather than shrinking them further. The company expects such chips to be an industry standard by the end of the decade.

The author estimates that processors required for a digital neocortex would need to be capable of running 1016 calculations per second. Today's fastest supercomputer in Japan can already perform calculations at this speed.

A digital cortex would also need to be able to store around 20 billion bytes of data, assuming it has about 300 million pattern recognizers with 72 bytes of memory each. Again, this is a level of processing power that even average computers today can exceed. 

We will soon see convincing examples of AI in 2029, the author claims, and AI will become commonplace in the 2030s. According to the author, all the portrayals of AI in movies and on television - where robots are often treated as conscious beings - has prepared us to welcome conscious artificial intelligence into our lives.

The author's track record for these kinds of predictions inspires confidence that this will indeed be the case. In the 1990s, the author made 147 predictions for 2009 — and only three of those proved to be false!

### 9. Final summary 

The key message in this book:

**If you were to open up your computer and look at all the components, it would be easy to get lost in the complexity. And yet, if you take a step back, you could elegantly describe its functions with a few pages of text and specific formulas. The same can be said for the human brain; and it is this understanding that will bring us closer to the first truly artificial intelligence.**

**Suggested** **further** **reading:** ** _The Singularity is Near_ by Ray Kurzweil**

**_The Singularity Is Near_ (2005) shows how evolution is drawing ever closer to a dramatic new phase, in that by 2029, computers will be smarter than humans, and not just in terms of logic and math. This event will not only profoundly change how we live but also pose serious questions about humanity's future.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ray Kurzweil

Ray Kurzweil is a pioneering technological futurist as well as a prolific inventor, including groundbreaking work in speech recognition software. His books include the bestseller _The Singularity is Near_ and _The Age of Spiritual Machines_.

