---
id: 5e54fc3d6cee070006e3e3e0
slug: how-to-do-nothing-en
published_date: 2020-02-26T00:00:00.000+00:00
author: Jenny Odell
title: How to Do Nothing
subtitle: Resisting the Attention Economy
main_color: None
text_color: None
---

# How to Do Nothing

_Resisting the Attention Economy_

**Jenny Odell**

_How to Do Nothing_ (2019) is a study of what's gone wrong in contemporary society and what we can do to fix it — and ourselves. Ironically, the most effective tactic against our 24/7 culture of productivity might just be doing nothing. When we stop, step back, and refocus our attention, Jenny Odell argues, we can begin to see the contours of a better, more meaningful existence.

---
### 1. What’s in it for me? See the world through a different lens 

"Time is money." It's an age-old adage, but it's taken on new relevance in today's digitally-driven, 24/7 economy. Whether it's work or self-improvement, we've come to believe that we should have something to show for our time. Anything that doesn't produce value in those terms is simply too expensive.

Writer and artist Jenny Odell thinks it's time to take a different approach. Never switching off, is a recipe for a bland and shallow existence where we simply follow the crowd and fail to do our own thinking. The result? A world of mediocrity, misery, and selfishness.

Odell's alternative is deceptively simple — doing nothing. Above all, this is a question of attentiveness. Learning to direct and focus our attention is a way of opening up an entirely new and deeper experience of the world around us. In the age of social media and its all-out assault on our attention, this is an act of resistance that might just change everything.

In these blinks you'll learn 

  * why social media makes us behave like brands; 

  * what a rose garden can teach us about attention; and

  * how attentiveness to others can make us more empathetic.

### 2. When work and leisure become indistinguishable, doing “nothing” looks like a waste of time. 

In the 1880s, American laborers began pushing for an eight-hour workday. They didn't just want more time to heal their sore muscles, though. As a popular trade union song of the era put it, the purpose of the struggle was to secure "eight hours of work, eight hours of rest, and eight hours of what we will." This was a demand for a life _outside_ work — the right to do nothing in particular for at least one third of the day.

In the twentieth century, reformers made the eight-hour workday a reality. Their laws remain in effect, but the distinction they drew between work and leisure looks increasingly shaky today.

**The key message here is: When work and leisure become indistinguishable, doing "nothing" looks like a waste of time.**

It was long assumed that economic risk was the business of capitalists and investors. Workers were expected to clock in, get the job done, and go home. Do that, they were told, and their positions and wages would be safe. This arrangement lasted as long as labor movements could enforce it through strikes and political pressure. That ended in the 1980s after a series of historic losses defanged once powerful trade unions and workers' parties.

In his 2011 book, _After the Future_, the Italian philosopher Franco Berardi links these defeats to the emergence of a new idea — that we are all capitalists. This means that we can't expect economic security. Just as companies navigate perilous markets in search of opportunities, we must compete against each other for one-time jobs or _gigs_.

The way to thrive in this ultra-competitive _gig economy_ is simple — never switch off. This isn't just what critics like Berardi say, however. Take a notorious 2017 advertising campaign for Fiverr, a platform that helps users find freelancers to complete mini-jobs. As their ads put it, "doers" in today's world "eat coffee for lunch," run on sleep deprivation, and happily interrupt sex to take calls from clients.

Fiverr's suggestion that these were good things was widely ridiculed, but the company hit upon one of the essential features of the gig economy — the disappearance of the boundaries between work, rest, and leisure. This shouldn't surprise us. If the day consists of 24 potentially monetizable hours, time becomes an economic resource that's just too valuable to be spent doing "nothing" or what you will.

### 3. Social media makes us behave like one-dimensional brands. 

If we are all capitalists, it makes sense that we spend much of our time behaving like corporations. Thanks to social media, this work never ends. Even our leisure is numerically sifted and appraised via likes on Facebook and Instagram. We monitor the performance of our personal brands as though we were analyzing shares on the stock market. This profoundly alters the way we act when we're online.

**The key message here is: Social media makes us behave like one-dimensional brands.**

In his 1985 book _No Sense of Place_, the technology expert Joshua Meyrowitz explored the effect of electronic media on social behavior. Despite its age, it's an eerily prescient analysis of social media.

In the 1950s, Meyrowitz went on a three-month trip. When he returned, he was eager to talk about his adventure. He didn't tell everyone the same story, however. His parents heard the "clean" version, his friends got the racier one, and his professors were given the "cultured" narrative.

All these different editions were equally true — they were just tailored to different audiences or contexts. But now imagine his parents had thrown a surprise party on his return and invited all these groups. Meyrowitz ventures that this would have gone one of two ways: he would have either offended one group or created a new account "bland enough to offend no one."

This is _context collapse_ — the fact that everyone is always in the same room at the same time. The disappearance of different contexts goes even further in our own digital age. Today, our online audience knows so much about us that it's extremely difficult to project different definitions of ourselves to different groups.

The result? Awkward acts, like taking unpopular positions or admitting mistakes in public, come to be seen as weaknesses and liabilities. Ultimately, this means we create a version of ourselves that's acceptable to everyone at all times — a personality with the edges smoothed off.

This isn't just a race to the mediocre bottom, though. It also runs against something that's completely normal and human — changing over time. But that's the point. Social media isn't designed to foster human expression and dialogue. It functions as a tool for personal brand management, and the two pillars of any brand identity are internal coherence and continuity over time.

> _"Friends can see a person who lives and grows in space and time, but the crowd can only see a figure who is as monolithic and timeless as a brand."_

### 4. If we want to live more meaningful lives, we need to reappraise what is valuable. 

Today's economy doesn't have an off button. Every moment of every day is a financial resource to be captured, optimized, and appropriated. Snooze and you can expect to lose. But despite this obsession with creating measurable value and optimizing every facet of life, many people still recognize a different truth. This holds that meaning is often the product of accidents, chance, and serendipitous encounters — the very "off time" our 24/7 cult of productivity attempts to eliminate.

**The key message here is:** **If we want to live more meaningful lives, we need to reappraise what is valuable.**

In the fourth-century BC, the Chinese philosopher Zhuang Zhou penned a story called "The Useless Tree." In it, a carpenter comes across a large and ancient oak. He deems this tree to be worthless because its old, gnarled branches aren't any good for timber.

Later, the tree appears in the carpenter's dreams and quizzes him on his definition of usefulness. Timber trees, the oak argues, are very "useful," yet these trees are invariably felled in their prime by humans. "If I had been of some use," the oak then asks, "would I have grown this large?" Uselessness, it seems, can be useful after all — you just have to take the oak's perspective. This point, the oak concludes, is missed by those who only see potential timber when they look at trees.

Zhuang's oak has a real-life counterpart. The hills above San Francisco Bay near the author's home in California were once full of towering, thousand-year-old Sequoia redwoods. During the 1850s gold rush, however, loggers felled all these ancient giants — except for one. Today, this wizened redwood is known as "Old Survivor," and it has a lesson for us.

Old Survivor was able to escape the fate of its neighbors for two reasons. First off, it was too twisted to entice loggers. Secondly, it was perched on a steep, rocky slope that reinforced the idea that cutting this tree down just wasn't worth the bother. In other words, Old Survivor was _too weird_ and _too difficult_ to proceed easily toward the sawmill.

This is a great example of what the author calls _resistance-in-place_ — making yourself into a shape that resists being appropriated by the dominant system. Just as the worth of trees doesn't have to be measured by their suitability for timber, value in our lives doesn't have to be measured in terms of productivity. As we'll see in the following blinks, there is another way to define meaning.

### 5. Doing nothing allows you to truly pay attention to the world. 

One evening in 2015, just before sunset on a cliff overlooking the Pacific ocean, a greeter checked guests into an area of foldout chairs cordoned off with red rope. After being shown to their seats and reminded not to take photographs, they watched the sunset. As it disappeared below the horizon, they applauded and refreshments were served.

This strange scene was part of an artwork by Scott Polach entitled _Applause Encouraged_, and it neatly encapsulates an important aspect of doing nothing. 

**The key message here is:** **Doing nothing allows you to truly pay attention to the world.**

Polach's work didn't create the sunset — it drew attention to it. This makes the work a great example of what the author terms _attention-holding architecture_. This is a framing device that encourages the kind of sustained contemplation that habit, familiarity, and distraction often close off.

Attention-holding architecture comes in different forms. One of the author's favorite examples is a public garden near her Oakland home, the Morcom Amphitheater of Roses.

Nestled into the side of a hill, the bowl-shaped garden contains dozens of branching paths that wind their way through and around roses, trellises, and oak trees. There are hundreds of ways to navigate this space, and just as many places to sit. It's designed to hold your attention — the garden _wants_ you to stay awhile and lose yourself in its sights and fragrances.

It's also possible to think about this concept in a more abstract way. Take the practice of _deep listening_. This was developed by American musician and composer Pauline Oliveros as part of her search for inner peace during the turbulent years of the war in Vietnam.

According to Oliveros, hearing and listening refer to different things. The former is simply the physiological means of perceiving sound. Listening, by contrast, means directing your attention to sound and its psychological significance. Deep listening, in other words, is a metaphorical architecture encouraging us to be more receptive to the world.

You can see how this works if you think about birdwatching. Oftentimes, when the birds are hidden, birdwatchers aren't actually watching anything — they're _listening_. And the first step toward being able to distinguish different birdsongs is breaking a wall of meaningless sound into discrete units. Like the sunset, these sounds are already there, but they only become perceptible and meaningful when you stop and do nothing but pay close attention to the world.

### 6. The brain usually only notices a tiny part of the data it processes, but silence can help widen your perspective. 

Attention is like breathing. It's always there, in the background. When you do notice it, you might be surprised to discover how shallow it is. And just as mindful breathing can train you to consciously deepen your breath, it's possible to train yourself to be more attentive. The trick is realizing just how much your brain is already processing.

**The key message here is: The brain usually only notices a tiny part of the data it processes, but silence can help widen your perspective.**

In the 1990s, Arien Mack and Irvin Rock, two psychologists based at Berkeley University, designed an experiment to get to the bottom of _inattentional blindness_ — the things we don't see because we're not paying attention.

They asked subjects to look at a cross on a screen and say if one of the two lines was longer than the other. This was just a decoy, however. As participants studied the crosses, small stimuli flashed up on their screens. When these were within the area circumscribing the cross, subjects were aware of them. When they fell outside this area, they went unnoticed.

The idea that we notice things when they're in our field of vision makes intuitive sense, but there's more to it than that. When stimuli outside the area took particularly distinctive forms like a subject's name, they _did_ notice it. But when these stimuli were made less distinctive, like spelling out "Janny" instead of "Jenny," they once again went unnoticed.

Mack and Rock concluded that the brain processes far more information than they had assumed and was only deciding later on whether stimuli would be perceived or not. This suggests that attention is like a key — it's the means by which the door dividing unconscious and conscious perception is unlocked.

So how do you find this strange key? Well, try embracing silence. Take the composer John Cage's work _4'33"_, a three-movement piece in which a pianist sits in front of her instrument without playing a single note. Every time it's performed, the audience becomes attuned to the ambient sounds of a concert hall. Suddenly, they hear the musical potential of a cough or a scraping chair. This was Cage's intention. As he put it, "everything we hear is music."

Walk down the street with this idea in your mind and there's a good chance you'll experience familiar scenes with newfound clarity. Like taking a conscious deep breath after years of shallow breathing, you might just wonder how you've never really heard any of these sounds before!

### 7. Choosing to be attentive to others takes the sting out of frustrating everyday experiences. 

Imagine you're sitting in your car at an intersection. You're late for work and the traffic is hell today. Then, just as the light turns green, another car suddenly dashes in front of you and cuts you off. How do you react? Chances are, you're going to focus on your own experience — in this case, the inconvenience caused by someone else's reckless selfishness. But say you learned that the other driver was rushing their injured child to the hospital — you'd probably look at the situation differently, right?

In most cases, it's impossible to know what really motivates other people's actions, but considering the possibilities _can_ improve your experience of the world.

**The key message here is: Choosing to be attentive to others takes the sting out of frustrating everyday experiences.**

In 2005, the American novelist David Foster Wallace gave a commencement speech at Kenyon College, Ohio. In it, he offered graduating students a bleak sketch of what adult life had in store for them. Time and again, he said, they would find themselves in hideously, fluorescently lit supermarkets at the end of long days at work and terrible traffic jams. When this happened, they would have two choices.

The first was to see the world only from their own point of view. When you do this, everything is about your own hunger, tiredness, and desire to get home. From this perspective, other people — whether it's other road users, fellow shoppers, or sales clerks — appear simply as obstacles. This, Wallace suggested, is a surefire recipe for irritation and misery.

But there is an alternative. When you pause to pay attention and think through other people's possible motivations, you begin to see that their lived realities are just as complex and deep as your own. The woman in front of you who just snapped at you, for example, might not always be like this. She might just be having a rough day. Maybe her dad just died, or an unexpected bill left her short for the month.

It doesn't really matter whether this is true. The point, Wallace argued, is that this kind of attention to possibilities transforms the way you perceive others. Rather than seeing them as things in your way, you begin to see them as fellow inhabitants of a shared space in which your interests are no more or less important than anyone else's. This ultimately makes it possible to experience even a crowded, loud, slow, consumer-hell-type situation as meaningful.

> _"But if you've really learned how to think, how to pay attention, then you will know you have other options."_ — David Foster Wallace

### 8. Final summary 

The key message in these blinks:

**After the defeat of once-powerful labor movements in the 1980s, a new idea emerged: that all of us are capitalists. This notion eroded the separation of work and leisure. In today's economy, every hour of the day is potentially monetizable, meaning that we've come to see "doing nothing" as a luxury we just can't afford. That, however, is a mistake. When we pause and really engage with the world, we can find deeper, more satisfying meaning in everyday experiences.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Trick Mirror_** **, by Jia Tolentino.**

_How to Do Nothing_ received widespread critical acclaim when it was published. Among those who loved it was Barack Obama, who put it on his Best Books of 2019 list. But Odell wasn't the only critic of contemporary culture to catch the former US president's eye.

In _Trick Mirror_, journalist and writer Jia Tolentino offers readers an unforgettable and deeply personal journey into the inner self. In a series of essays brimming with creativity, Tolentino takes up themes ranging from social media to the cult of self-optimization and women's role in contemporary fiction. So if you've got a taste for bold thinking, check out our blinks to _Trick Mirror_, by Jia Tolentino.
---

### Jenny Odell

Jenny Odell is an artist and writer based in Oakland, California. She teaches at Stanford University and has been an artist-in-residence at Facebook, the Internet Archive, the San Francisco Planning Department, and the San Francisco garbage dump. Her art has been exhibited in galleries around the world.

