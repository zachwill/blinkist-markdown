---
id: 55103d0c376430000a000000
slug: how-to-fly-a-horse-en
published_date: 2015-03-27T00:00:00.000+00:00
author: Kevin Ashton
title: How to Fly a Horse
subtitle: The Secret History of Creation, Invention and Discovery
main_color: 29A8CF
text_color: 1A6982
---

# How to Fly a Horse

_The Secret History of Creation, Invention and Discovery_

**Kevin Ashton**

_How to Fly a Horse_ (2015) delves into the process of creation and ultimately discovers that the very act itself is far more ordinary than we often think. In fact, in building upon the creative work of generations of thinkers, anyone can create, as long as they have the grit and determination to do so.

---
### 1. What’s in it for me? Discover how to unleash your natural urge to create. 

Have you ever stood in awe beside a Picasso or a Rodin and thought, I wish I had that creative talent? Ever witnessed the latest tech whiz kid make a billion and think, why can't I innovate like that?

Such thinking is common in our society and it highlights a common fallacy: that creativity is the result of individual genius. Few have it, and most of us don't.

These blinks will show you how this mind-set is completely wrong. They show that creativity is a relatively simple process that we all have the ability to carry out. Read these blinks and you will discover how you can become a creative genius in whatever you choose to do.

In these blinks you'll discover

  * why every creator stands on the shoulders of giants;

  * how a 12-year-old slave revolutionized the vanilla industry; and

  * how Steve Jobs designed the iPhone, step by step.

### 2. Creativity isn’t the result of genius, but of the ordinary process of thinking about how to overcome a problem. 

How many times have you heard that Mozart was a genius, composing complete symphonies in his head before putting them to paper? Or that great inventors were somehow radically different from the rest of us?

These are myths, plain and simple. In fact, no creator is fundamentally different from you or me.

The _creativity myth_ preaches that few are chosen for creative greatness, and that their success relies on magical flashes of genius insight.

But if you actually look at the life of any genius, it's not superiority and spontaneous inspiration that will highlight their success, but careful thinking.

Take Archimedes, the person who first cried out "Eureka!", and who is credited with discovering the properties of density. When taking a bath, he noticed that the water rose and fell again as he entered and exited the tub. In short, Archimedes realized that water displacement could be used to measure volume.

But he didn't immediately relate this finding to density. Rather, it came after a long period of thinking about the problem and trying to develop solutions for it.

In fact, the act of creating itself is little more than simply thinking about how to solve a problem — something we all do.

Many experiments have demonstrated this to be true, such as psychologist Karl Duncker's Box Experiment. In the experiment, Duncker asked subjects to attach a candle to a wooden door using only the candle, a book of matches and a box of tacks. He discovered that there are three solutions — melting wax to fix the candle, tacking the candle to the door, and finally, emptying the box of tacks, tacking it to the door and placing the candle inside — and that the processes of arriving at these solutions were the same for each person who attempted them.

For example, everybody who thought of tacking the box of tacks to the door went through the _same_ thought process: eliminating other ideas, thinking about building a platform with the tacks and _then_ considering using the box as a platform.

> _"Creating is not magic, but work."_

### 3. Creation means simply adding to a long line of innovations from a long line of forgotten, ordinary people. 

So, innovation isn't the product of moments of genius. Likewise, it's never the result of only a single person's work.

Everything we make, no matter how novel or interesting, depends upon thousands of people and generations of ancestors who contributed to our idea.

No creation is entirely new. They all build on the work of innumerable (and often unknown) thinkers from the past.

Consider, for example, the case of Edmond, a 12-year-old Black slave from La Réunion, who is credited with discovering how to make the vanilla pod self-pollinate. Before his discovery, vanilla was only grown in Mexico, as Europe didn't offer the right conditions for growth.

Edmond's master, Ferréol Bellier-Beaumont, was a botanist who kept his thumb on the pulse of developments in the field made by the naturalist Konrad Sprengel, whose work demonstrated that plants reproduced sexually.

He passed on this knowledge to Edmond when he showed him how to manually fertilize watermelon. Later Edmond realized that a similar process could be used on vanilla! Without Ferréol and Sprengel, and the countless thinkers before them, Edmond would never have been able to "create" a method of pollination for vanilla orchids.

This process of building upon innovations is ongoing, and with every new creation, fresh opportunities for future innovations are brought forth. Each new solution will bring many benefits, but they will also present new, unforeseen problems that need to be solved creatively.

The invention and development of Coca-Cola from a bottled patent medicine to a canned refreshment, for example, solved many problems, such as the desire of American soldiers in Korea to have access to portable, canned soda.

But Coca-Cola has since created its own problems: the high fructose corn syrup in its recipe contributes to obesity in the United States, excess caffeine can cause vomiting or diarrhea and aluminium cans take years to decompose if left in landfills.

Yet solving the problem of aluminum waste, for example, also brought the widespread introduction of aluminium recycling.

> _"A species that survives by creating must not limit who can create. More creators means more creations."_

### 4. Creation consists of taking gradual steps rather than leaping into the unknown. 

The first telephones were miles away from modern-day smartphones. But the transformation didn't happen overnight. Rather, it came in baby steps.

Indeed, creation comes from taking these small steps, not giant leaps. It is the result of asking minor questions, finding the solutions and then discovering new questions.

For example, Steve Jobs didn't just come up with the iPhone in a single night. Rather, he thought about the then-current smartphones and continuously asked himself, "Why don't they work?"

The problem was clunky, hard-to-use keyboards. And the solution? A big screen and a pointer.

Then a new problem: What kind of pointer? Solution: a mouse.

Problem: No one wants to carry around a mouse. Solution: A stylus.

Problem: A stylus is easy to lose. Solution: Use our fingers!

There was no singular "Eureka" moment for Jobs as he created the iPhone. Rather, he carefully thought through theoretical steps, narrowing down the solutions as he solved problems.

However, in the process of creation it's possible for us to encounter _inattentional blindness_, whereby we miss important information because our attention is directed elsewhere.

We can see these principles in action in a study which sent a clown on a unicycle down a street full of people who were busy messing with their phones. Unbelievably, only a fraction of the people managed to acknowledge this highly unusual event!

Though they did see the clown, it didn't actually _register_ with them. They were too busy concentrating on something else — texting, making phone calls and what-not — to actually sense the clown.

Inattentional blindness can hamper our creativity, as it prevents us from seeing every possibility, especially the ones we wouldn't expect.

Steve Jobs' journey toward the creation of the iPhone was only possible because he was able to draw from a wide range of ideas and solutions, not just the ones that follow conventional wisdom. To think like Jobs, we need to open our minds and see everything, not just what we expect to see.

> _"Asking, 'Why doesn't it work?' is creation inhaling. Answering is creation breathing out."_

### 5. Creation is nothing without work and failure. 

Don't expect the cure for cancer or an artistic masterpiece to just fall from the sky and land in your lap. Like all great creators, we need to _work_ and accept failure before we can produce anything good.

Creation without work is a concept that is at odds with reality. If you want the results, you'll have to put in the hours to achieve them. Doing so requires complete commitment and the ability to say "no" to distractions.

Some people use rituals to help them stay on track. For example, the great Russian composer and innovator of twentieth-century music, Igor Stravinsky, followed a very particular routine: each morning he would play a Bach fugue on the piano before setting out to work for ten hours. He'd compose before lunch, and orchestrate and transcribe after lunch.

He did this for years, working hard without waiting for inspiration to jolt him into action.

But despite all your hard work, you will inevitably fail on your path to creation. Indeed, almost nothing will be perfect the first time, or even good enough for that matter. For this reason, it's critical to learn how to be resilient in the face of failure.

Take the novelist Stephen King as your inspiration: he regularly scraps 300 pages in his quest to produce a good book.

These failures will often be followed by rejection, but even this has value in that it shows you where to go next and what to change for your next attempt.

Whatever you do, don't be like Franz Reichelt, who ignored both experts and his _own_ data, all of which said that his parachute design was flawed. Despite this, he leapt from the Eiffel Tower in 1912, and needless to say did not survive.

> _"To succeed in the art of new, we must fail freely and frequently."_

### 6. To work creatively in a team, always say “show me.” 

While you have the power to get plenty of creative work done on your own, it's sometimes better to work with a team that shares your goals.

Small, isolated, highly motivated groups are the best kind of teams for creative projects. Groups of two are even better.

The primary focus of this group should be the _creative conversation_ through which creative problems are identified and solved. In these conversations, the team will establish a series of goals, toward which the individual team members are free to work on their own.

The creators of _South Park_, Trey Parker and Matt Stone, also use this method, and are able to create and finish an episode in only six days. First, they identify and solve problems together. Afterward, they work individually, with Parker doing the initial writing and directing which is then refined and polished by Stone.

However, sometimes you'll find yourself in teams where brilliant thinking isn't encouraged. This is unfortunately the case for many companies today.

Creativity threatens the stability of the status quo, and most organizations therefore stifle creative people, either by design or by accident.

But there are organizations who realize the power of creativity, and who take active steps to encourage creative thinking.

One way to bring novel ideas out into the open is by using a process known as "_show me_." This process revolves around the simple idea that to truly understand and appreciate a new idea, you have to be shown, not just told.

Not only does this bring new ideas to the forefront, it also gives people the immediate opportunity to test the validity of their ideas.

Take Clarence "Kelly" Johnson, for example, who as a newcomer at the Lockheed plant in California boldly declared that their new airplane design, the Lockheed P-80 "Shooting Star," was flawed. His boss, Hall Hibbard, was intrigued. "Show me," he said, asking Johnson to improve the design. So he did!

Johnson worked on the problem, solving it with a "twin" tail and thus helping to create America's first jet fighter.

> _"Partners create together by helping each other create individually."_

### 7. Final summary 

The key message in this book:

**You don't have to be a genius to create. In fact, anybody can do it. With consistent hard work and repeated failure, you will succeed in creating something new, thus adding to the generations of creative thought which serve as the foundation for novel thinking.**

Actionable advice:

**Your ideas are never too wacky.**

The next time you're worried that your idea is too off-the-wall to become useful or accepted, stop and think: "Great! This means I could be onto something!" Wacky ideas should be treated with the same scrutiny and persistence as any other idea. Even Judah Folkman, who revolutionized cancer therapy with his thesis that tumors were created within the patient's own blood supply, was initially rejected for having a new and left-field idea!

**Suggested** **further** **reading:** ** _Where Good Ideas Come From_** **by Steven Johnson**

_Where Good Ideas Come From_ examines the evolution of life on earth and the history of science. This _New York Times_ bestseller highlights many parallels between the two, ranging from carbon atoms forming the very first building blocks of life to cities and the World Wide Web fostering great innovations and discoveries.

In addition to presenting this extensive analysis, replete with anecdotes and scientific evidence, Johnson also considers how individual and organizational creativity can be cultivated.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Kevin Ashton

As co-founder of the Auto-ID center at MIT, Kevin Ashton pioneered a new generation of computing which he calls the "Internet of Things." In addition to speaking about innovation and technology, Ashton has led many successful technology start-ups and some interesting social experiments.

