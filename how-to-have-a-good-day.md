---
id: 583d641ee536360004b64ed8
slug: how-to-have-a-good-day-en
published_date: 2016-12-01T00:00:00.000+00:00
author: Caroline Webb
title: How To Have A Good Day
subtitle: Think Bigger, Feel Better and Transform Your Working Life
main_color: D2A757
text_color: 6B552C
---

# How To Have A Good Day

_Think Bigger, Feel Better and Transform Your Working Life_

**Caroline Webb**

_How to Have a Good Day_ (2016) explains how you can make the most of your working day, with advice based on recent findings in the fields of psychology, economics and neuroscience. These blinks will teach you how to navigate the challenges of the modern workplace like a pro and boost your energy level during nerve-racking or tiring days.

---
### 1. What’s in it for me? Turn every day into a good day. 

Most of us can probably picture one of those great days at work when we're at the top of our game: we're bubbling over with ideas, can handle any problem and even get along with that one especially surly coworker.

Of course, not every day can be as smooth sailing as this. But if you follow the advice offered in these blinks, you can turn almost every day into at least a _good_ day; you'll feel good about yourself because you get things done, focus without too much effort and bravely withstand the urge to procrastinate.

On top of that, you'll feel comfortable among your colleagues and, when giving a presentation, you'll find that your audience really listens to you. Sound good? Then let's get started!

In these blinks, you'll learn

  * why thinking about a past catastrophe can boost our confidence today;

  * how the schedules of top violinists, master chess players and elite athletes are alike; and

  * why your interest in sports could earn you a job in a totally unrelated field.

### 2. To have a more productive day, start by clearly laying out your intentions and goals. 

Have you ever found yourself nearing the end of a day and feeling as if it had been completely wasted? Here are a few tips to help minimize that feeling.

First, clearly set out your intentions for each day and try to eliminate any activities you know tend to distract you from achieving them.

For instance, one of the author's clients, Martin, is the strategy director of an aircraft manufacturer. He noticed that whenever he felt overwhelmed with his daily tasks, he would turn by default to distracting habits like checking news websites.

So, to increase productivity, identify what those distracting habits are for you and put extra focus on avoiding them. Keep your sights on the day's goals.

Second, frame your goals in positive language. Goals framed positively can also be called _approach goals_ — that is, goals that state the positive outcome you're hoping to achieve, such as "make my product irresistible." The contrast to this would be an _avoidance goal_, like "stop losing customers."

There is research to back up the benefits of this strategy: a 1997 study found that students using approach goals improved their performance, whereas the opposite was true for those using avoidance goals.

Third, make a _when-then plan_ to prepare for obstacles that could come between you and your goals.

Take the author, who is not naturally an early bird. When she founded her own consulting business and no longer had a boss expecting her at work at nine in the morning, she was tempted to sleep in and while away her mornings.

So she committed to a when-then plan to overcome this obstacle: _when_ I wake up, first I'll take a short walk, _then_ I'll check my e-mails. This little rule helps get her out of bed and face each day in a good mood.

> _"We can exert more control and start to enjoy more 'well-planned luck.' And as a result, we can all have many more good days."_

### 3. Manage overload, beat procrastination and stay focused by making a plan and scheduling enough breaks. 

Crammed calendars and full to-do lists at work make it easy to feel overwhelmed. Let's look at some of the ways to deal with these situations.

First, the physical: lean back on a couch, exercise ball or even your office chair for a few minutes and focus on your breath until you've calmed down enough to think clearly.

Then, to order your chaos into manageable chunks, make a plan and decide where to start.

First, write down everything you need to do in the next few days or weeks. Mark the most important tasks and take one step toward completing that task today, no matter how small it is.

Let's take Angela as a case study. Angela was an attorney who wanted to run for a post in her company, which self-elects rather than appoints managers. However, the task of "start election prep" always felt too daunting. The enormity of those three words gave her anxiety, so she kept avoiding the task and procrastinating.

What eventually worked for her was splitting the task into smaller steps, breaking it down into something like, "have a talk with my boss about my idea."

Once she'd split up the big task into small, manageable steps, "election prep" became much less overwhelming and she was able to tackle it.

Another essential prerequisite for focus is scheduling breaks at least every hour and a half.

Focus inevitably wavers throughout the day and needs to be recharged periodically. Over the course of 90 minutes, our brains go from highly focused to scatterbrained, which is why we end up doodling or playing on our phone if we're forced to concentrate for longer.

When famed psychologist K. Anders Ericsson studied people at the top of their fields, like world-class violinists, athletes and chess players, he found that they also practiced in blocks of 90 minutes or less, with short breaks in between.

### 4. Surround yourself with pleasant relationships by building rapport with the people you meet and resolving tensions directly. 

Getting along with the people around you is essential for getting the best out of your day. Here's how to kick-start those relationships!

First, build rapport when you interact with someone at work. Don't just awkwardly slip by them in the office kitchen when you're both reaching for a mug — engage them and ask open questions like, "how are you spending your holidays?"

Find commonalities or shared areas of interest by showing a genuine curiosity in your coworkers, as well any other people you meet. Creating this sense of connection is important because it creates a culture of trust and collaboration in the workplace.

Highlighting shared interests is also important when looking for work, as employers are more likely to hire people they share similarities with.

Let's take a look at a study conducted by sociologist Lauren Rivera from Northwestern University, where she asked recruiting managers about their most recent hires. She found that 74 percent admitted feeling some similarities with their new hires, whether it was based on a shared interest in sports, technology or something else. This indicates that managers prefer to hire and be around people with whom they share commonalities.

To have a good day, it's also important to address and resolve any tensions with coworkers, as they can really sap your enthusiasm.

The best way to go about this is to openly explain to your coworker what you're feeling and why in a polite but firm manner.

For example, let's take Simon, a real estate advisor who was annoyed with a client who kept promising him specific assignments but rarely followed through with them when the time came.

Instead of reproaching his client or bottling up his frustration, Simon was up-front and communicated how he felt: "I'm confused because I received positive feedback from you, but didn't get the project. Would you mind telling me what you were dissatisfied with and what I can do better next time?"

This approach helped his client understand him without feeling attacked and getting defensive, and they ended up having an in-depth discussion about what the client was looking for.

### 5. Address decisions systematically by using thinking routines and breaking down complex problems into an issue tree. 

Do you struggle with making difficult decisions because they make you feel overwhelmed? The next time you feel this way, try these simple steps:

First, develop a versatile routine that helps you reach sound decisions in all manner of situations.

The routine might be as simple as asking yourself a set of questions like, "what are the alternatives and potential disadvantages to this choice?" or "what would the worst-case scenario be, and what are some of my options if that happens?"

Peggy is an advertising art director who developed a routine that works for her. Her trick is to always invite colleagues to give her feedback about her current work. While she doesn't agree with them all the time, their input often helps her catch potential problems in her campaigns, thereby improving her decisions.

Remember, good advice can come from anyone, not just experts. Peggy once received valuable feedback from someone in customer support about an air freshener campaign. The support representative had on-the-ground knowledge suggesting that customers didn't understand the visuals of the campaign, and Peggy was able to adjust accordingly.

Another tip for optimizing your problem-solving skills is to break down a complex problem with an _issue tree_.

Start by jotting down the key issue — let's say your business is doing poorly, which might make your central question, "how can I increase profits?"

That question is now the trunk of the tree. Now, write down the two possible options that form the branches of the tree, in this case increasing revenues or reducing costs.

Then, think of concrete actions that would help you realize those options; for instance, you could dismiss employees to decrease costs, or launch a new product to increase revenue.

These suggestions make up more branches of the issue tree, until eventually you'll have systematically mapped out many potential next actions you can take to tackle your problem.

### 6. To get your message across, involve your audience and make your presentation memorable. 

Have you ever been giving a presentation, only to look around and see that most of the audience wasn't actually listening? To avoid this in the future, just follow these simple techniques.

First, remember that your audience will be much more receptive if you involve them and make them feel as if they're choosing what to learn.

For instance, Emma organizes training programs covering new pedagogical techniques for teachers. In the past, it was hard to get any of the participants to deviate from their own, entrenched methods, thus making it difficult to get anyone to actively listen during the training session.

So Emma tried a new type of meeting, in which ten teachers each presented their own methods at different stations around the table. Participants were then free to walk around and stop by the stations they were interested in.

Emma's strategy gave the teachers more agency in terms of what to learn, which in turn made them much more enthusiastic about the training.

Second, make your presentation interesting by incorporating videos or posters that will keep the audience on their toes. Make sure to utilize a whiteboard if there is one; people will internalize much more of your message if you draw and write in real time, rather than just using prepared slides.

An experiment at Stanford University showed that people will recall nine percent more of a chart's content if they see how it comes together as it is drawn, rather than just seeing the completed chart on a slide.

And throughout it all, make sure to use short and simple sentences in a fluid manner to hold your audience's attention and emphasize why your audience should care about what you're telling them.

### 7. Build your confidence by thinking of problems you overcame in the past and boost your energy with positive thoughts. 

It's Monday, you're tired and longing for the weekend already — but you've got a meeting with a dissatisfied customer first thing in the morning. What do you do?

First, keep your cool by taking some distance from the situation. Imagine it's not you but a friend who has to face the client. What advice would you give? You can take this even further by talking to yourself in the second person to gain a more distant perspective.

Next, think of a past situation you handled well and ask yourself what resources helped you then. Maybe it was your wit, fearlessness or supportive friends, all of which can probably help you through the present situation, too!

Let's take Jacquie, a college PR officer, as an example. Once, an earthquake cut off her college's power and water supplies. But instead of seeing it as a disaster, Jacqui spun the earthquake into a positive PR story for the school.

The school still managed to hold a graduation ceremony amidst destroyed buildings, a story that the national media covered as a success story of perseverance and community — thanks in large part to Jacqui's attitude and skills in dealing with the media.

Thinking back to how she managed that situation continues to make Jacquie feel like she can handle anything.

Turning to positive thoughts in challenging times is another surefire way to inject some energy and cheer into your day.

Try out these tricks next time you're in a tough situation. Start by identifying the mental, physical and temporal patterns and triggers that affect the ebb and flow of your energy. Maybe, for example, you always feel lethargic after lunch.

Then, find ways to boost your energy during the energy lows. Maybe it's getting up to make a cup of tea, having a five-minute chat with a coworker or taking a walk around the block.

You can even try a _gratitude exercise_ : think of three things that happened to you today that you're grateful for, even if it's just a small thing like remembering your umbrella — or forgetting it, and running like a little kid through the rain! Even small thoughts like this can make a big difference.

### 8. Final summary 

The key message in this book:

**It's normal to feel exhausted at times, but developing positive routines, learning from the past and connecting with colleagues are all good ways to make the most of your waking hours. Know yourself well enough to recognize what you might need to help boost your energy, and you can turn almost any day into a good day.**

Actionable Advice

**Cut back on e-mail**

Every time you check your inbox, you interrupt the work you've been doing; it also takes additional time to refocus once you try to get back to your work. This amounts to lots of lost time over the course of a day, so set a few times during the day — or even just one — that you check your inbox, and keep it closed at all other times to keep your productivity high.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Checklist Manifesto_** **by Atul Gawande**

Drawing from his experience as a general surgeon, Atul Gawande's _The Checklist Manifesto_ reveals startling evidence on how using a simple checklist can significantly reduce human error in complex professions such as aviation, engineering and medicine.
---

### Caroline Webb

Caroline Webb is a management consultant who worked for McKinsey for 12 years before launching her own company, Sevenshift, which specializes in helping clients increase their productivity, energy and enthusiasm. Her work has been featured in the _New York Times_ and _Forbes_.

