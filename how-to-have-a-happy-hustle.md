---
id: 5e9065296cee0700061cc549
slug: how-to-have-a-happy-hustle-en
published_date: 2020-04-15T00:00:00.000+00:00
author: Bec Evans
title: How to Have a Happy Hustle
subtitle: The Complete Guide to Making Your Ideas Happen
main_color: FC7935
text_color: B44E19
---

# How to Have a Happy Hustle

_The Complete Guide to Making Your Ideas Happen_

**Bec Evans**

_How to Have a Happy Hustle_ (2019) is an empowering guide to making ideas happen. Puncturing the mystique surrounding successful startups, Bec Evans reveals how anyone can grow an idea into a business by starting small, thinking creatively and getting feedback from their target market. Most importantly, by focusing on the process of testing and building an idea, connecting with people, and learning from mistakes a happy hustle redefines success to include personal growth, fulfilment as well as financial gain.

_How to Have a Happy Hustle_ won the Startup Inspiration category at the Business Book Awards 2020.

---
### 1. What’s in it for me? Learn how to create a startup, and have fun in the process. 

Have you ever heard stories about startups and wished you could start one, too? Many people yearn to leave their day jobs and strike out on their own, but doubt that they'll be able to come up with a good idea for a startup, much less figure out how to execute it. 

Stories about successful startups usually make it seem like their founders are geniuses who are struck by divine inspiration. But in fact, anyone can come up with a good idea if he's prepared to put in some time and hone skills like creative thinking, empathy, and resilience in the face of failure.

In these blinks, you'll find a step-by-step guide to creating your own side hustle or startup, including everything from strategies for coming up with a good idea to ways to pitch to potential investors. 

In these blinks, you will discover 

  * why doing fieldwork is vital for every entrepreneur; 

  * how to escape your inner critic; and

  * why quitting can be the most courageous thing you can do.

Last but not least, in Blink 10, you'll also find an exclusive blink crafted by the author herself, covering content not found in the book!

### 2. Your most annoying problems can be a great source of inspiration. 

Usually, problems are something from which we want to run away. Who wants to sit and dwell on everything that's wrong with life and the world? No one. If you're looking for the next brilliant startup idea, though, you're going to have to come to terms with problems. Believe it or not, they're actually the best place to find inspiration. 

Finding great ideas means becoming a _problem seeker_, analyzing frustrations as you go about your day, including the things that trip you up at home, on your commute to work, or online.

For example, one British businesswoman was getting dressed one morning when she accidentally ripped her favorite pair of jeans. She was mortified — replacing them would mean hours of tedious shopping. She couldn't easily buy online because retailers' varying sizing models made it impossible to know what would fit her. Looking for a solution to her problem — one shared by millions of women — led her to start an innovative website dedicated to helping women find clothes that actually fit them. 

It's not just current problems that can provide inspiration, though — you can also look to the past. That's how Anurag Acharya found the inspiration for Google Scholar. Remembering how hard it was to access academic articles when he was a student in India, he created a search engine that specifically looks for academic publications, benefitting scholars all over the world. 

Of course, not all problems are created equal. You can create an inventory of annoyances, from burning your toast to finding Excel spreadsheets hard to navigate. But how can you know which you should focus on?

Well, choosing the perfect problem is both systematic and intuitive. It helps to take a step back and evaluate each problem against objective criteria, like whether you have the expertise necessary to tackle it, whether there's a big market for the solution, and so on. But ultimately, you should trust your intuition. If the problem really excites you, go for it! 

Once you've chosen a problem, write a _problem statement_ — something that sums up the problem and who's affected by it in as specific a way as possible. The more detailed you are, the more clues you'll have about how to solve it, giving you a valuable road map for the work ahead.

### 3. To identify your audience clearly, you have to go out and get to know them. 

You may have come up with a really great problem that you're excited about working on. Fantastic! But in order to develop it properly, you'll need to find out how the problem actually affects other people. 

A _persona,_ a model __ of your invention's target user, is a great way to start — especially if your target market is significantly different from you. Evans once visited a team of software coders who were designing a game for middle-aged women with children. In order to make sure they were designing with the user in mind, the young male coders developed a persona called Barbara. They then tried to put themselves in Barbara's shoes, constantly imagining what her life might be like and what her preferences might be. It worked. For example, instead of their original ideas to add gunfire and explosions to the game, they decided Barbara would prefer fireworks and popping champagne corks.

Of course, a persona is based on assumptions, and, as such, it can fall prey to your natural biases. A good way to avoid this is through good old-fashioned research. Many successful companies use this tactic — Amazon, for example, encourages its employees to go out and chat with the people who use its services, keeping them connected to the real world of the user. If you're unsure about how to interview people, remember that one of the best tools at your disposal is _open-ended questions_. These give people the space to talk about what really matters to them. 

Bear in mind, of course, that what people think they do and what they _actually_ do are often very different. Researcher Paul-Jervis Heath of the design agency Modern Human once asked people about their spending habits. The responses he received were very mature, but when he asked the same people to keep a diary of daily expenses, he noticed that their _actual_ behavior was wildly different from what they'd reported. They weren't lying, though. They were just fooling themselves into thinking the behavior they aspired to was their _actual_ behavior. 

In order to gather more accurate insight into your target customer, start observing people's behavior out in the real world. Get into this habit by doing fieldwork observing people in a public space, then try more targeted observations. Ask people who match your persona whether you can watch how they go about their day at home or work. Make sure you take detailed notes, and write up your observations as soon as possible afterward. 

This research will be vital in really getting to know your target audience, and creating a product that they truly need, not just one you _imagine_ they might need.

### 4. To find a solution to your problem, practice divergent thinking. 

Too often, we think that for any given problem there's a single, perfect solution out there just waiting to be discovered. The trouble is that this kind of thinking stifles creativity, which then makes problem-solving much harder! 

The truth is that there are many possible solutions to virtually every problem, and in order to find the best of them we need to be creative and generate as many ideas as we can. Comparing these solutions then sharpens our thinking and makes us less likely to cling to any one of them. Working like this, allowing our minds to roam free and explore creative solutions that might not be obvious at first, is called _divergent thinking_.

It's divergent thinking that enabled global design agency IDEO to come up with innovations such as the first computer mouse. IDEO invests time in exploring every angle of a problem and every possible solution, and uses unconventional methods in the quest for inspiration, like taking lengthy field trips. To an outsider, the value of these trips might not be obvious. But the folks at IDEO know that the 80 percent of their time that is "wasted" is exactly what enables them to generate genuinely innovative ideas in the other 20 percent. 

Like a muscle, our capacity for divergent thinking can be strengthened. But how? Well, one of the best tools is brainstorming in groups which helps you generate multiple ideas and when done in a supportive environment, avoids your inner critic taking over.

The Google investment fund Google Ventures uses an exercise called Crazy Eights to help entrepreneurs generate creative solutions. First, participants each fold a blank sheet of paper into eight panels. They're then given five minutes to sketch eight different solutions to a given problem, one on each panel. This adds a helpful visual element, and the time constraints help circumvent any doubts participants might have about their drawing skills. 

But one of the _best_ ways to encourage divergent thinking is to work with diverse teams. Start by posing a problem to a group of people. Have them generate as many solutions as possible on Post-it notes, then discuss them, grouping common ideas together and using that to generate novel and wild ideas. This will lead to robust debate, which is the point of the exercise. Working together in a productive, generative way will allow a lot of high-quality, conflicting solutions to emerge; all you'll have to do is pick the best of them!

### 5. Choose a solution, and get it into the world – fast. 

There's nothing quite like the rush of invention, and along the way, you may find yourself getting really attached to the ideas you generate. Ultimately, though, you'll have to choose just one.

That can be tough, and it means that you're going to need to change modes. Start analyzing each idea with a critical eye rather than a creative one. To get some perspective, make a list of the criteria that you'll use to evaluate your solutions. Make them as objective as possible, and designed to assess whether your idea will work as a business. For example, if you want your business to deliver craft coffee to office workers, then useful criteria might include considerations such as whether or not you can realistically keep the coffee hot. You could consider the competition, too; might a bigger enterprise like Uber Eats muscle in on that corner of the market? This is an exercise in cold-heartedly killing your darlings until you're left with an idea that is both exciting and workable. 

Now that you've got your solution, it's time to develop it. There may be many reasons to put off giving shape to your idea. You may feel like you don't have enough time or money to start, or you don't know enough yet. But those are just excuses. All you really need to begin is the willingness to start somewhere. 

The first thing you'll need to do is name your creation; brainstorm until you find a name that's catchy and memorable. Don't worry, you can change the name later, but a working title is a great way to make your project seem more real.

Then you need to write a _concept statement_, which includes the name of the invention, the problem it addresses, the solution it offers, and, most importantly, the benefit it will offer to its users. Once you've sketched out the rough answers to these points, practice condensing your statement to the length of a tweet. That will help you create a very concise and specific "elevator pitch" of your concept. If you're having trouble articulating the product's specific features, try sketching it — this can help you get specific.

Now that you've got to the essence of your solution, you can create a prototype. This doesn't have to be fancy — it can be as simple as a drawing, a paper-and-glue scale model, or a Facebook page outlining your idea. The idea is to take the first step toward materializing your idea, which helps test it and allows other people to understand what you want to do.

### 6. Test your product on the right users. 

Ever heard of the _Harry Potter_ series? Most people have; the books are now unquestionably a worldwide phenomenon. But they nearly didn't even make it to print. J. K. Rowling's first book was rejected _twelve times_ before a publisher gave a few chapters to his eight-year-old daughter to read. She was so desperate to know what happened next in the story that he knew the book had the makings of a hit.

It's essential to test a product with the right target users if you want to know its real potential. Doing so saves you valuable time, and money, too — after all, there's no point in creating something unless your target audience wants and needs it.

But how can you test a product if you haven't created it yet? By developing a _minimum viable product_, or MVP. An MVP is different from a prototype in that it actually needs to function. For example, if you're a baker who creates extravagant wedding cakes, then a successful MVP could be a small version of your most elaborately decorated cake rather than, say, a single plain sponge. Your clients can taste all the flavors of the cake and admire your artistry, but the MVP won't take you too long to make.

Once you've created your MVP, you need to test it. That might mean professional user testing, requiring you to hire a researcher to observe people interacting with your product. Or you could share a simplified version of your product online and invite responses. If you want to open a shop, you could begin by testing it out as a market stall or pop-up.

Testing your MVP in the real world is a vital step in moving your idea from concept to a product with which the target user can interact. Of course, the whole point of testing is to identify problems as soon as possible, tweak, and then test again. So don't be discouraged if there are things that don't work at first. 

Once you've got a workable MVP to which people respond well, you need to take it a step further: get your target users to put their money on the line. For example, the author offered a free online course to motivate people to write every day. When that generated a good response, she created a paywall that allowed people to extend the course for a small fee. When that was successful, she knew she was on to something. If users are really excited about your product, they'll be prepared to pay for it.

### 7. Master the art of the pitch. 

If you've got a great idea, it's not uncommon to keep it to yourself out of concern that someone else might steal it. But keeping it to yourself can also be destructive, causing you to miss out on valuable feedback that could help improve your idea. 

Daunting as it might be, if you want to create a successful business, you'll have to learn to communicate your ideas to others, and to do this convincingly, you'll need to master the art of the _pitch_. When you pitch, you share your vision as simply, yet concretely, as possible. Your aim is to persuade and sell the benefits of your concept to an audience. It's essential to practice your pitch on whoever you can: your pets, your family, your buddies at the bar — whoever will listen! Pay attention to visual cues from your audience; are they engaged and nodding along, for example, or do they look like they want to nod off? 

To keep your audience's attention, keep adapting your pitch so that it's as enticing and concise as possible. How? Well, remember that effective pitching is as much about _how_ you communicate as _what_ you communicate. If your story is compelling, but your shoulders are slumped and your head is bowed, the audience isn't going to buy what you're selling. Luckily, body language that projects confidence can be learned. Practice standing tall, planting your feet, and staring directly at your audience. You'll not only look more confident, you'll start feeling more confident, too. 

Once you're ready to get in front of an audience, start pitching to everyone who you think could give you valuable feedback. Take the time to listen closely to people's responses, scary as that might be, and be sure to record them as well. Feedback is a gift that will help you refine your idea. Remember it's not personal, so the more critical, the better!

Once you've amassed all your feedback, you need to figure out a way to process it. One effective method is to listen back to any recordings, or read through your notes, and write down people's responses on Post-it notes, using direct quotations so that you don't misinterpret what they say. 

Cluster together each key idea that emerged in the feedback, and then write down the main takeaways and the specific actions that can result from them. You've just taken the first step toward refining your idea.

### 8. Cultivate a growth mindset and be prepared to fail 

How do entrepreneurs learn how to succeed? It might come as a shock to discover that first, they need to learn how to fail.

Failure is so important, in fact, that in their very first week at the prestigious Singularity University, located on the NASA campus, students are given an assignment requiring them to go out into the world and fail as often as possible. 

Why the fixation on failure? Because people who are comfortable failing bounce back more quickly. They don't take it personally, and they're prepared to take risks. These are the hallmarks of what Stanford Professor Dr. Carol S. Dweck calls a _growth mindset._ Entrepreneurs who think like that are much more likely to succeed — not because they don't make mistakes, but because they're equipped to deal with them.

In 2012, the founders of online marketplace Teddle had to admit their startup idea wasn't working. In spite of all their hard work, user numbers for the website simply weren't growing. When they analyzed the data, they realized that 75 percent of visitors to their site were searching for cleaners — a service they didn't provide. Armed with that insight, they were able to pivot __ and use their existing technology to create a new online marketplace for cleaning services. The service, called Hassle.com, was hugely successful and was eventually sold for millions of pounds.

If you approach your own failures with a similar growth mindset, you'll be able to find out whether there's a way you can pivot your endeavor in a new, more promising direction. That might mean focusing on a slightly different problem, allowing you to get more specific about your target market. When the author realized that her online writing accountability program was especially relevant to academic writers, she was able to adapt her marketing materials to target them more explicitly, which helped her sales to soar.

But sometimes, none of these efforts is successful, and you'll realize that the best course of action is just to quit. There's nothing wrong with that — in fact, it's very courageous to face reality head-on and know when to change course. Even wildly successful businesspeople such as Richard Branson have business histories littered with failed ventures — like Virgin Brides and Virgin Cola. 

Like Branson, you shouldn't let failure dampen your dreams of becoming an entrepreneur. After a period of rest and reflection, and armed with greater self knowledge and business experience, you simply need to dust yourself off and begin again.

### 9. Grow your target market by understanding your community. 

Imagine you're standing in the proverbial dragon's den, pitching your idea to a panel of steely-eyed investors. One of them asks the question you've been dreading: "How big is your market?"

It's one thing to have a great product that users like, and another to have a product that can attract a big enough market to be profitable. In order to grow your idea, you'll need to grow your core group of early users and attract a wider audience by finding the right marketing techniques.

As with all aspects of founding a startup, this is a process of trial and error. When developers launched the soccer app _I Am Playr_ in 2010, an impressive 50,000 users signed on. However, 20,000 users soon dropped off, and the team couldn't attract new ones. They frantically tweaked their product to find new ways to appeal to users. Six months after their launch, they hit the jackpot by inventing a feature that rewarded players for sharing the app with five friends. Suddenly, they were attracting 80,000 users every day, and they kept growing. The key to their success lay in motivating their core users to recruit their friends, instead of just launching a mass marketing campaign. 

What makes some marketing ideas work and others fail? You'll be able to avoid a lot of stabs in the dark by really tuning into the needs of your specific audience. George Burgess started developing the technology that would eventually become study app Gojimo while he was still at school himself. He first tried to expand his market by getting as much coverage in traditional media as possible. While that got him attention, it didn't help his user numbers grow, because he'd made a key marketing error: he was ignoring the fact that his student audience didn't actually read newspapers. Only when he tuned in to his community did he hit on the right marketing strategy: targeted Facebook ads at crunch times when students were frantically studying for exams. Sure enough, user numbers soared. 

Of course, it's good to remember that growth isn't an end in itself. The size of your ideal market depends on your values and your ambitions for your business. You may prefer to go for quality of engagement over quantity of users.

### 10. Gather emotional and financial resources to support your hustling. 

Whether you're doing a side hustle on the weekends or plunging into a full-time entrepreneurial role, you'll need support to keep going for the long haul. 

First, you'll need to make sure you have enough money to give your product time to develop and reach the right market. That's known as having _runway –_ just like an airplane, your project needs enough time to be able to take off properly. 

There are many different models for attracting funding. Apart from the traditional routes of pitching to venture capitalists or angel investors, you can also appeal to your wider community and try to raise funds through sites like Kickstarter or Gofundme. This approach has the added value of doubling as market research, helping you gather valuable feedback on user needs and wants. Startup accelerators, which provide investment as well as mentorship and training to budding startups, are another great source of funding. 

Financial investment is important to enabling your project to flourish, but even more important is investing in your own well-being. Having runway isn't only about having a financial buffer; it's also about having the emotional and mental resources to allow you to keep building your business. If you're externally successful but neglecting your well-being, your startup won't be sustainable.

Entrepreneur Vikas Shah now knows that only too well. From the outside, his life looked enviable: he was the founder of several incredibly successful startups and traveled the world lecturing at distinguished universities. But intense loneliness, depression, and anxiety plagued him, leading him to the realization that he had to refocus his life around things that really made him happy, like time with friends and family. 

Having a happy hustle means building a business that works _for_ you. That starts with enjoying what you do and allowing your genuine curiosity to lead you to explore and grow your ideas. It also means taking small steps and appreciating the journey you're on. Cultivating a growth mindset will allow you to take failure less personally, and constantly reflecting on what kind of business you want to build will help you stay true to your desires. 

Perhaps the most important element of your emotional runway will be the support of your community. That can be family, fellow founders, business mentors, online networks, or even your early supporters and customers. Hustling is happiest when both failures and successes can be shared.

### 11. A blink from the author: the era of the side hustle 

This is an exclusive blink, covering content that's not in the original book, but created by the author as a side project to her main hustle.

Side projects are having a moment. With rapidly-changing workplaces, many people juggle their gigs, have portfolio careers, and they self-identify as multi-hyphens.

We're in the era of the side hustle according to Henley Business School, who found that 1 in 4 UK adults have a secondary business or job that brings in extra income.

Though the definition of a side hustle is all about money, that's not what drives people to start one - the research shows three quarters of us start one to follow a passion or explore a new challenge. It's this motivation for fulfilment that creates a happy hustle.

What's so great about the era of the side hustle is that anyone can be part of it. You don't need permission to start one, there's no exams to pass, you can learn the skills as you go along. You can make the most of rapidly evolving technology to design and develop your business idea and reach new customers. You just need to start.

Take 13-year-old Kirsty Devlin who started her first business at school in Bolton, England. Helped by her two little sisters, Devlin washed cars, until she earned enough money to set up a second business selling brightly coloured wristbands on eBay. Since then, she's always had a side project on the go. She said, "I'll never stop creating side hustles as long as I'm alive."

For teenage Devlin to get going, she asked herself: "What can I do with the limited resources I've got to make some money?" She started small, without spending much time or money to get going. Starting small bypasses the fear centres of the brain - the bit that tells us to not bother trying - and it reduces the risk of failure and allows us to build the confidence that will help us keep going.

Now a social entrepreneur, Devlin helps people on low incomes learn to code. She admits it's hard work, but she loves seeing the impact it's having, "I'll never stop creating, because it feels like fun."

Having a side hustle involves time and effort, all done alongside your current commitments. But if you start small, work on an idea that excites and motivates you, build it step by step, you'll increase your chance of success and find great fulfilment in the process.

### 12. Final summary 

The key message in these blinks:

**In order to create your own happy hustle, you need to be pragmatic about what it takes to turn ideas into successful startups. You shouldn't wait for creativity to strike, but instead, spot opportunities in the problems you encounter every day, and think of innovative ways to solve them. Then you need to get your ideas into the world as quickly as possible and get real feedback from your target market. Along the way you should be prepared to fail repeatedly, and have the resilience to see every part of the process as an opportunity to learn and grow your ideas.**

Actionable advice: 

**Steal little pockets of time to work on your side hustle.**

One of the main reasons people give for never developing their creative ideas is that they have no time. While it's true that most of us have busy schedules, it's always possible to steal an hour or two in a week to work on your project. You can schedule it, make it a daily practice or be spontaneous. Try getting up an hour early in the morning, or going for a walk at lunchtime to think about your goals instead of idly staring at your phone. Even a shopping trip can become an opportunity to do some user research! You don't need to go on a retreat in order to work on your startup. Every little chunk of time counts.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Making Ideas Happen_** **by Scott Belsky**

By now, you're probably filled with enthusiasm about the possibility of creating your own side hustles or startups. Well, in _Making Ideas Happen_, author Scott Belsky lays out even more ways to make creative visions a reality.

The blinks to _Making Ideas Happen_ delve into the nitty-gritty of innovation, showing how modern technologies such as the smartphone force us to work reactively rather than proactively, responding to mundane emails instead of developing our creative ideas. The blinks also outline ways to find a great creative partner who complements your skills, and to explore bringing innovative thinking into the office environment. If you're ready to continue your journey to being a successful entrepreneur, then take a look at _Making Ideas Happen_.
---

### Bec Evans

Bec Evans is a former head of innovation whose side hustle became a startup. As well as running Prolifiko, she's a writer, speaker and coach. She helps busy people find the time for creative side projects and coaches budding entrepreneurs to make their ideas a reality.

