---
id: 576b98b1e0dd250003d61574
slug: how-to-kill-a-unicorn-en
published_date: 2016-06-27T00:00:00.000+00:00
author: Mark Payne
title: How to Kill a Unicorn
subtitle: How the World's Hottest Innovation Factory Builds Bold Ideas That Make It to Market
main_color: FFFA4C
text_color: 66641E
---

# How to Kill a Unicorn

_How the World's Hottest Innovation Factory Builds Bold Ideas That Make It to Market_

**Mark Payne**

_How to Kill a Unicorn_ (2014) is about how to approach innovative projects in a way that will make the outcome truly great — a sort of road map for innovation, filled with practical examples encountered by the consulting company Fahrenheit 212.

---
### 1. What’s in it for me? Leave the land of unicorns and make innovation happen. 

Are you familiar with the land of unicorns? You know, that glittering land of eternal rainbows where everything works like magic. That imaginary place where all fantastic ideas are born — and where they usually remain until the end of days.

In business, an idea is labeled a "unicorn" if it looks great at the start but will only work and yield a profit in an imaginary world.

In these blinks, you'll learn how to kill these unicorns and make true innovation happen. With the help of numerous examples given by the global innovation firm Fahrenheit 212, you'll gain an understanding of how to formulate innovative ideas and solutions that actually work in the real world.

You'll also learn

  * why an innovation-making team focuses on Money and Magic;

  * about the _wow_ and _how_ of innovation; and

  * that an easy-to-steal whiskey bottle is an innovation.

### 2. To make innovation happen, build a diverse team and allow them to debate. 

Fahrenheit 212 is a global innovation firm founded by the author. Even if you haven't heard of it, you've probably encountered its work; it's helped many of the biggest companies in the world — including Samsung, Coca-Cola, Nestlé and Toyota — develop innovative ideas, strategies and products.

So what can Fahrenheit 212 teach you about generating innovation?

First, team up with a diverse group of people. At Fahrenheit, they create dream teams with _Money & Magic_. That is, they combine money experts — commercial, financial and strategic whizzes — together with experts who know how to "magically" navigate consumer wants and needs.

What makes this the winning combo? Well, consider an innovative idea or strategy that appeals to consumers but doesn't bring in profit. Without a black bottom line, the idea probably won't be sustainable in the long run.

Let's see how Fahrenheit 212 puts this advice into action.

To help turn Samsung's new translucent LCD screen into profitable technology, Fahrenheit brought together a team of analysts and financial experts (Money) with designers, writers, architects and film producers (Magic). Together, they came up with creative product solutions that were also financially and strategically viable.

Next, in order to arrive at those great innovative ideas, forget brainstorming — try debating with your team instead.

A 2003 study conducted at UC Berkeley showed that debate and criticism, rather than inhibiting the development of ideas, tend to stimulate it.

In the experiment, researchers divided 265 students into groups and asked them to come up with a solution to a traffic-congestion problem. One group was asked to brainstorm without criticism, while the other group was told to debate, with all members being free to challenge each other's ideas.

After 20 minutes, the debate group had come up with far more creative ideas than the brainstorming group, showing the importance of constructive criticism and an array of viewpoints in the creative innovation process.

> _"We work this way not because we like debate for debate's sake, but because it's more effective at generating a greater proliferation of ideas..."_

### 3. Become innovative by understanding the interests of the consumers and the business. 

So you've assembled your dream team. What next? Let's see how the Fahrenheit team tackled a problem faced by a bank in Dubai: on average, customers only bought two of the bank's products, neglecting a wide range of other services.

Fahrenheit approached this problem by first adopting the _consumer's point of view_. Reaching out to the bank's customers revealed the root of the problem: customers didn't fully trust the bank. They feared that the product terms might change to their disadvantage, and they therefore tried to limit risk by buying only a couple of products.

Equipped with this new knowledge, Fahrenheit began checking every _business-related aspect_ of the bank, from IT systems to profit-and-loss statements.

And it quickly became apparent that the bank's products were suffering from being siloed. Often, one department's initiatives were totally disconnected from those of another department. Also, each product used different software, making it impossible for customers to see the benefits of accumulating different kinds of financial assets in the bank.

Aware of both the customer and business issues, the Fahrenheit team was now ready to solve the problem.

They created an IT and product system called _Mosaic,_ which connected the bank's various products. If you opened a checking account, for example, the terms of some other service — a car loan's interest rate, say — would also improve.

In addition, Mosaic worked smoothly on tablets, thus ensuring that customers could conveniently see how their numbers changed with the addition of new products. This built trust, because customers were now reminded of the potential financial benefits, as opposed to risks, of accumulating their assets in the bank.

> _"A root cause of the unacceptably high failure rates permeating modern innovation practice is a baked-in assumption that if you solve for the needs of the consumer, the needs of the business will eventually sort themselves out."_

### 4. To create successful innovative projects, don’t forget the how in all the wow. 

Has an idea ever struck you like lightning, leaving you elated and deliriously happy? When we're suddenly hit with an idea, it often sparks a euphoric feeling — a feeling of _wow_.

"Wow" refers to those grand ideas that seem to hold equally enticing possibilities for both consumer and creator, the ones that have the potential to heal a broken market or make available truly new and valuable products and services for society.

But before you get carried away by such visions, remember that just feeling a spark of inspiration isn't enough. The wow must be accompanied by the _how_.

The practical part of a project — how you're going to make the wow come to life — is just as important as that initial burst of inspiration. So, ask yourself questions like, "How do we do it?," "How expensive is it?" or "How will we generate profit?"

Neglecting to ask the "how" questions is like planning an exquisite dinner made up of spectacular dishes that you haven't the faintest idea how to make.

Grand innovations aren't only a matter of eureka moments. In fact, the "wow" doesn't need to come first. You should actually start with the "how" — that is, the more logistical tasks, like considering the financial or operational reality of the business. From there, you can work your way to wowing the market.

Let's revisit the Fahrenheit team to see how they integrated this into their approach to the Dubai bank problem.

Instead of counting on a "wow" factor to inspire them, they first asked themselves a whole lot of "how" questions, like "How do we unite different business models so that they become one trustworthy entity?" This then led to the solution of connecting the bank's various products.

### 5. What big companies see as radical innovations are often just obvious ideas for start-ups. 

We've all heard those miracle start-up stories of major innovations being hatched in some garage or dorm room.

Now everyone wants to mimic the start-up approach, including established companies. Mid-size or even big companies often come to Fahrenheit 212, wanting, for instance, to adopt more of the brave and innovative strategies that make start-ups thrive.

Start-ups don't see their own ideas that way, however; in fact, they often consider their ideas, projects or strategies obvious.

Let's examine the upstate New York craft-spirits company, Tuthilltown Spirits, as a case study of a start-up employing strategies so "obvious" that they may seem brilliantly radical to an outsider.

When Tuthilltown Spirits was entering the market, the founders knew they were competing with decade-old or even century-old distilleries.

Consequently, they thought it was a clear priority to figure out a way to make their whiskey age fast. They managed this by using much smaller barrels than those typically found in major distilleries. They also experimented with honeycomb-pattern holes in the barrels, which accelerated the aging process.

And, unlike most whiskey distilleries, they also produced different varieties of whiskey. This was a natural decision for them, because they were working with local farmers and the different grain varieties they got from them.

Another internally obvious solution was to use half-size bottles for distribution. They did this because, though their whiskey yield was still relatively low, they still wanted to reach a variety of retailers.

This idea turned out to be a real success. Because the smaller bottles were easier for shoplifters to grab and steal, store owners often placed them in the store's high-visibility areas to prevent theft. This gave the brand a lot of extra exposure.

So, if this is how start-ups innovate, what can established businesses do? We'll explore that in our next blink.

### 6. Established businesses should innovate by asking the right questions, not by snatching at quick fixes. 

When you encounter a problem at work, do you immediately start trying to figure out how to get around the issue as quickly as possible?

If you do, you might be going about your problem-solving in entirely the wrong way.

To show that the quick-fix approach isn't all that effective, let's look at a hotel brand worth billions. First, the problem: the company had found that fewer and fewer top customers were revisiting the hotel. What's more, they were having difficulty devising attractive perks for customers that competitors couldn't match.

Though a fancy chocolate on the pillow or fine caviar on ice might distinguish the hotel in the _short-term_, it wouldn't take long for competitor hotels to catch on and adopt similar ideas.

To really stand out, the hotel brand wanted to develop an innovative loyalty program that would compel customers to revisit it in the _long-run_.

So, the Fahrenheit team was brought on board, and its first move was simply to ask the top customers why they'd stopped visiting the hotel. They answered that once they'd earned their platinum member card, they had no incentive to revisit the hotel. They'd reached the top, and so had to book with another hotel. In other words, they weren't really loyal at all; they just enjoyed the challenge of climbing the loyalty program ladder.

So this was the _actual_ problem the hotel faced: how could they engender true loyalty in their customers? Unlike the unlimited expiration date on loyalty we have in close relationships with friends or family, the hotel's loyalty program only ran for 12 months. After that, customers were pushed back to square one — a policy unlikely to inspire loyalty.

Fahrenheit solved this problem by giving the loyalty program a lifetime guarantee. When customers reached the maximum amount of loyalty points, the hotel wouldn't withdraw them at the end of the year. In this way, the hotel's loyalty program was like an actual relationship between close friends — enduring and reciprocal.

By identifying the underlying cause of the surface-level issue, they were able attack the problem at the root and make innovation really happen.

### 7. Final summary 

The key message in this book:

**Creating innovative solutions is all about approaching problems from two sides — the consumer side and the business side. If one of them is left out, it's very likely that the solution will be neither good nor lasting.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Sprint_** **by Jake Knapp, John Zeratsky, Braden Kowitz**

_Sprint_ (2016) is a guide to start-up success, broken down into a five-day plan that lets you test new ideas and solve complex business problems. These blinks give you everything you need to move quickly from an idea to a prototype and, ultimately, make a decision on whether or not to launch.
---

### Mark Payne

Mark Payne is the cofounder, president and Head of Idea Development at the successful innovation consulting company Fahrenheit 212, which has offices in both New York and London. His firm has been called the "Epicenter of Innovation" by _Esquire_ and his innovation advice has been featured in _Fortune_, _Businessweek_ and _Wired_.

