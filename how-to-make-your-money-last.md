---
id: 57c4168e8a12b60003822224
slug: how-to-make-your-money-last-en
published_date: 2016-09-01T00:00:00.000+00:00
author: Jane Bryant Quinn
title: How To Make Your Money Last
subtitle: The Indispensable Retirement Guide
main_color: DE2C45
text_color: C4273D
---

# How To Make Your Money Last

_The Indispensable Retirement Guide_

**Jane Bryant Quinn**

_How To Make Your Money Last_ (2016) helps you deal with a burning question when you've ended your career and started retirement: What now? These blinks show you how to manage your retirement funds to make sure you get the most out of this new stage of life.

---
### 1. What’s in it for me? Prepare yourself to enjoy a better retirement. 

Retirement is a time of freedom, rest and discovery. After years of hard work, rising early and coming home late, your time is your own — and you deserve it.

Although retirement sounds like a long-term vacation, being retired isn't as easy as you might think. Retirement requires careful preparation. You'll need to adjust your spending, take care of your health and plan your free time wisely.

These blinks will show you how to best prepare for retirement. You'll learn about different retirement plans, ways to ensure your financial well-being and different paths toward starting a new life. Importantly, you'll discover how to make retirement a wonderful time.

In these blinks, you'll also learn

  * what America's best retirement plan looks like;

  * why working as long as you can before retirement does pay; and

  * what the number four has to do with perfect retirement planning.

### 2. Retirement is a challenge for which you should prepare. Get to know its five stages. 

Yesterday, you were an active member of the workforce. Today, you're retired. What will be your first step in this new phase of life?

Many people feel lost at the start of retirement. Fortunately, you can make sure you're not this type of retiree by planning ahead.

Retirement is challenging, as it's a leap into the unknown. After all, what's the first thing you say when you introduce yourself to a new person? You share your profession: "I'm a lawyer," or "I'm a teacher."

As a society, we often define people by job. Going into retirement can then feel as if you've lost your social identity.

Plenty of people lie in bed all day when they first retire. While this might sound like a vacation, even vacations get boring if you're not doing anything. Planning your days is crucial.

There are five retirement stages. Let's go over each to ensure your retirement will be successful.

First, there's _preretirement_. During this stage you slowly disengage yourself from daily work. You start imagining what your life will look like when you retire.

Second, there's the _honeymoon_ or your first taste of freedom. As a newly minted retiree, you now get to spend as much time as you like on leisure activities, such as fishing, painting, reading or playing with your grandchildren.

Next comes _disenchantment._ In this stage, the days may start to feel long, and you might start missing your daily routine at work.

You overcome disenchantment by entering the fourth stage, _reorientation_. Here you refocus on the things that matter most to you and start adjusting to your new financial situation.

Once you've reoriented, you reach the fifth stage: _stability_. Stability is about settling in and enjoying the new rhythm of your life.

> _"Retirement is an adventure, demanding all of your creativity and force. So keep experimenting. No one but you can invent your new life."_

### 3. Be healthy, sort out your healthcare coverage and try to keep your treatment costs down. 

It's important to take care of your health during your retirement. You never know when doctor bills might start to add up, so make sure you have adequate insurance.

If you want to receive the best coverage for the best price, however, you need to understand how health insurance works.

In the United States, your age plays the biggest role in determining your coverage. Americans over 65 have Medicare — a national insurance program — available to them, if they meet Social Security requirements, which stipulate that an individual needs to have worked at least 40 quarters. Medicare ensures that those who qualify are covered for the rest of their lives.

It's more complicated for people under 65, as there is no federal health safety net. People under the retirement age usually have health insurance provided by an employer.

Don't worry about losing your coverage if you're out of a job, however. The Affordable Care Act (ACA), often called Obamacare, makes health insurance affordable for everyone and forbids companies from refusing coverage if an applicant has a preexisting medical condition.

Of course, you should aim to be healthy _before_ you retire. Try to keep treatment costs down, too. Take advantage of preventive services, such as mammograms, cancer screenings or colonoscopies, as the ACA makes most of these services free.

Remember: the cheapest medical care is treatment that you don't have to pay for at all!

Don't be afraid to discuss price with your doctor, either. Compare treatment costs and let your health provider know if you've found a better deal. If you're not comfortable negotiating, now is the time to learn!

Finally, try to stay away from expensive hospital emergency rooms. Chain stores such as CVS and Walgreens offer walk-in clinics that can address most minor ailments. These clinics offer flu shots, too.

### 4. Find the right balance between retirement revenue and living costs by creating a spending plan. 

Your regular salary ends when retirement starts. That means it's time to start managing your budget!

Organizing your spending is a core part of retiring well. You can't expect to be rescued by stock market gains or a winning lottery ticket if you burn through your savings early.

So try to _rightsize_ your life as early as possible, by finding a balance in which your annual budget matches your cost of living.

Many people make the mistake of going into retirement thinking, "I've saved $250,000. That should be enough!" But numbers mean nothing if you live beyond your means.

You need to calculate what you can _afford_ to spend, considering retirement revenue and expenses, and then set out a detailed spending plan.

Let's look at the details of how to build a spending plan.

Start by going through bank statements and checkbooks to get a clear understanding of your spending habits.

Next, cut out all work-related expenses. You'll no longer be buying suits or spending piles of cash on commuting, for example.

Then factor in the money you'll start to receive as a pensioner: your pension, real estate revenue, Social Security, annuities and so on.

Don't forget to add income from financial assets. If you have $100,000 in savings or investments, for example, you'll probably earn something like four percent interest on those accounts.

Once you've compared revenue and expenses, you'll see how much you have left to spend. Only then can you figure out if you need to curtail your lifestyle. If you find you need to live on a much tighter budget, for example, you might have to make big changes, like selling your home and moving into an apartment.

### 5. Social Security retirement benefits are great; your benefits grow every year you don't claim them. 

It's still possible to receive an income after you've retired, as the US federal program Social Security pays retirement benefits to qualified individuals.

Social Security retirement benefits are the best available retirement plan in the United States. These benefits provide lifelong income which is immune to inflation. Your Social Security payments can also be claimed by your spouse when you pass away.

The best part about Social Security benefits is that they are risk-free. It's a level of security of which a stock market investor could only dream!

Social Security benefits are easy to qualify for, too: you have to be over the age of 62 and have worked for at least ten years.

Your Social Security retirement benefits grow every year you don't collect, so be strategic when you decide it's time to claim your checks. The earlier you do so, the lower your payments will be.

You can claim retirement benefits as soon as you're 62, but your monthly checks will be 33 percent higher if you wait until you're 66. And if you wait until you are 70 years old, your checks will be 76 percent higher. See, the wait makes a big difference!

Some people have to claim benefits earlier, of course. Yet if you can, think twice before you decide to collect. Ask yourself: Is there any way I can get by for another three or four years? You might have left a job, but could you work part-time, for example?

You could consider covering bills with savings, too. It might make you insecure to live off your savings for a while, but doing so could make what you claim from Social Security much higher down the line.

### 6. Pensions are paid either monthly or as a lump sum. Take your time to decide which is best for you. 

Traditional pensions aren't popular these days, as most people prefer to collect benefits as a lump sum.

Let's look closer at the differences in pension options.

Pension plans offer two ways to receive benefits. One is through fixed monthly payments. You'll never stop receiving payments, even if you live to be 120 years old!

The downside of fixed monthly payments is inflation. Your benefits don't change with the rate of inflation, so a rising cost of living, for example, will slowly chip away at that fixed amount.

You can also receive pension benefits as a lump sum, and then store your money in a bank account.

The downside in doing so is that you have to personally invest your money to make it grow, putting it in stocks or bonds. Yet if you invest, you also take on risk; you could jeopardize your retirement savings if you bet on the wrong stocks!

As you weigh these two options, base your final choice on the state of your health and your skills as an investor.

If you're risk-averse and healthy, decide for a traditional pension paid out in monthly installments. There's no need to risk your entire pension if you don't want to become an investment banker!

You might want to seek guidance from a financial advisor, but be careful: many are scam artists. Moreover, if you're in good health, you might enjoy a long retirement.

If you're a talented investor, however, go for the lump sum. Monthly payments are probably too little for you if you know your way around money and want to take on investment risk.

### 7. Lifelong annuities come with risks but can be good options for managing retirement income. 

Receiving a final paycheck is painful. It's especially tough when you don't know when the next check might arrive.

There's another way to receive additional money once you've retired, however.

Insurance companies offer financial products called _annuities_. Annuities are basically pension plans with regular payments, adjusted for inflation, that you "purchase." You can buy annuities through insurance agents, financial advisors or banks.

Imagine a retired man who is 65 years old. If he buys an annuity worth $100,000 at current rates, he would receive a $556 monthly payment for the rest of his life. The older you are when you purchase an annuity, the higher the monthly payment. If you purchase an annuity when you're 70, for example, at current rates you would receive a monthly check worth $642.

Annuities aren't risk-free, however. Be aware of the _sucker factor_.

Imagine this man instead dies a year after he purchased an annuity for himself alone. In this case, the insurance company wouldn't pay out his annuity to his relatives. It is this sort of situation that allows insurance companies to meet payment obligations for people who live longer than average.

While annuities carry some risk, they're still a good retirement savings option. Imagine the same man invests his $100,000 in stocks instead. If the man earned $333 monthly, for example, this would constitute a stable investment return. Yet it would still be _less_ than an annuity would pay.

So if you factor in the stress of managing stocks and the potential risk of losing everything, annuities are an attractive possibility for managing your money during retirement.

### 8. Retirement plans are a great way to save. It's never too late to start, even if you’re about to retire. 

Ask retirees what they wish they had done differently, and most will answer, "I wish I'd saved more money." But it's never too late to start saving if you use a retirement plan!

Retirement plans are a great option for soon-to-be pensioners.

Employment-based retirement plans are simple to understand. A certain amount of money is automatically deducted from each paycheck and set aside, usually invested in mutual funds.

Employees contribute an average of 8.8 percent of their salary from their mid-50s to mid-60s. For employees age 65 or older, it's 10.1 percent.

The best part of a retirement plan is that it may allow you to set money aside that is tax-deferred — or depending on the plan, even tax-free!

The type of retirement plan for which you're eligible depends on where you work, but conditions are roughly the same across the United States.

And it doesn't matter if you're about to retire and haven't yet started saving: it's never too late!

Yet you might be thinking, "What's the point of saving money I'll need in a couple of years?" Remember you won't withdraw all your cash from your plan at once. A big chunk will stay in your account and will keep growing, with a tax break!

That means you can earn a lot in the last few years of your career. It can even make your savings plan profitable.

The amount you can contribute to your plan _is_ capped, however. In 2016, you could save a maximum of $18,000 a year, plus an extra $6,000 for contributors over the age of 50.

### 9. Keep expenses under control by spending no more than four percent of your savings per year. 

How much money can you take out each year without running out? That can seem tricky, but the _four percent rule_ can help you.

Financial advisor Bill Bengen came up with the four percent rule 25 years ago, and it's been the golden rule of financial planning ever since.

According to the four percent rule, you can withdraw four percent of your total savings each year of your retirement. If you stick to this percentage, your savings should last at least 30 years.

Let's say you start with $100,000, which means you can withdraw $4,000 for your first year of retirement.

If inflation runs at three percent the following year, you can withdraw $4,000 along with an additional $120 (three percent of $4,000). The third year, you can withdraw $4,120 and another $124 for inflation, and so on over the next years.

It's still important to invest your money or generate income from interest. Research on the four percent rule has shown that it's wise to invest half your money in government bonds, with maturities of four to ten years.

Invest the other half of your savings in _blue chip stocks_ — stocks in well-established, highly profitable companies that come with little risk, such as Boeing or General Electric.

If you don't feel comfortable placing half your savings in stocks, then lower your yearly withdrawal rate. All in all, it's up to you to balance your risks and expenses.

Retirement is supposed to be relaxing and enjoyable. So make sure you get the most out of it by planning in advance. It's never too early to start thinking about your retirement!

### 10. Final summary 

The key message in this book:

**Retirement can be the best time of your life, or it can be frightening and stressful. It all depends on how well you plan ahead. Sort out health care needs, assess how much investment risk you want to take on and find a pension plan that works for you. It's never too early to start preparing!**

Actionable advice:

**Plan your free time, too!**

Retirement is supposed to be about leisure, so make lists of the activities you'd like to explore. Are you interested in art? Languages? Technology? Figure out what you want to devote yourself to now that you're not bound to a day job, and find like-minded people with whom to share your new pursuits!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Total Money Makeover_** **by Dave Ramsey**

_The Total Money Makeover_ (2013) is a step-by-step guide to turning your financial situation around, no matter how nasty it seems. By following these seven simple steps, you can put financial security back into your life and begin planning for a comfortable, contented retirement.
---

### Jane Bryant Quinn

Jane Bryant Quinn is a personal finance expert and author of several bestselling books, including _Making the Most of Your Money NOW_, _Smart and Simple Financial Strategies for Busy People_ and _Everyone's Money Book_.

