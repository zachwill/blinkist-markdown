---
id: 568b01e7de78c0000700002d
slug: how-to-raise-an-adult-en
published_date: 2016-01-06T00:00:00.000+00:00
author: Julie Lythcott-Haims
title: How to Raise an Adult
subtitle: Break Free of the Overparenting Trap and Prepare Your Kid for Success
main_color: FFD43C
text_color: 7A661D
---

# How to Raise an Adult

_Break Free of the Overparenting Trap and Prepare Your Kid for Success_

**Julie Lythcott-Haims**

_How to Raise an Adul_ t (2015) reveals the ways in which the most common parenting method today, helicopter parenting, is doing more harm than good, both for parents and kids. These blinks outline a better way to parent — one that actually raises children to become truly independent adults.

---
### 1. What’s in it for me? Make your children happier and more independent. 

Having children is one of the biggest events in many of our lives and it's only natural that we want to do everything in our power to help our children and make sure they get what they need.

However, as these blinks will show you, this eagerness to help our children has spiraled out of control, leading to a kind of parenting that is actually harmful and does not prepare them for their lives as adults. It's high time we start doing something about it. It's time to start raising adults.

In these blinks, you'll learn

  * why you shouldn't tell kids they can achieve anything;

  * how overparenting can lead to drug abuse; and

  * what it means to grow up.

### 2. Too much parenting just isn’t good for your kids. 

When it comes to parenting, there can be "too much of a good thing." Overeager parents, or _helicopter parents_, seem to be everywhere. The term, coined in the 1990s, describes the type of parents who constantly hover over their children rather than raising them to be independent people. Today, this is standard parenting. And it's not good. At all.

Flashback to the 1981 abduction and murder of six-year-old Adam Walsh, an event upon which a hugely popular movie would later be based, contributing to an atmosphere of fear among parents in the U.S. All of a sudden parenting was no longer about preparing children _for_ life; instead, it was about protecting them _from_ it. 

After all, there's a lot to be afraid of. Accidents, illnesses, strangers — any of these things could be potentially disastrous. But to a great extent these fears are irrational. It's actually more likely for a kid to die in an equestrian accident than to be kidnapped, for instance.

It's not just fear that motivates helicopter parents, though. They also parent with the hope that it will give the kids the best opportunities later in life. That's why they manage their children's extracurricular time so stringently. Sure, kids with helicopter parents often make it into a good school or a big business. But that doesn't mean they're prepared for life.

Moreover, helicopter parenting is a means to an end: achieving something the parents think is important. But just because the _parents_ see something as necessary doesn't mean it will make their children happy. 

Helicopter parents are also motivated by a mistrust in the system. Many parents don't view their schools as effective, and make an attempt to intervene and thus become overly involved. In addition, their desire to get their kids into the best colleges can lead to some extreme behavior. There are even parents who will hire a lawyer if they think their kid got the wrong grade!

But wait — are we arguing that it's wrong to look out for your kid's well-being? No, not in theory. But helicopter parents take it too far and, as you'll see in the following blinks, this can have major consequences.

### 3. As a result of overparenting, many kids today suffer psychological problems and abuse drugs. 

Kids need to learn basic life skills, like how to manage commitments and deadlines, how to talk to strangers and how to take care of a household. Unfortunately, many kids who leave for college after having grown up with helicopter parents never learned these valuable skills. But sometimes the negative effects of helicopter parenting are much more fundamental and difficult to correct than a simple lack of life skills.

For one, kids with helicopter parents are more likely to develop certain psychological problems.

According to a 2013 American College Health Association study, 83.4 percent of all college freshmen felt overwhelmed by college obligations, and 8 percent even considered suicide. We can't chalk all this up to overparenting. But it certainly plays its role in making young adults feel unable to handle and cope with life's hurdles. When overeager parents take care of everything for their kids, their kids don't develop confidence in their abilities. 

One part of the problem is that many parents don't share stories of their own struggles with their children and instead focus solely on their success stories. Their children, as a result, believe that they have to meet unrealistic expectations, and feel stressed when they can't live up to them. Combine this with a lack of resilience and a lack of intellectual and emotional freedom, and you have a recipe for disaster. 

As a result of all this, we overdiagnose and overmedicate our children to enhance their performance.

In fact, 11 percent of American children have been diagnosed with attention deficit hyperactivity disorder, and about 6.1 percent have been prescribed medication for it. 

Part of this overdiagnosis can be attributed to the fact that kids diagnosed with ADHD are allowed more time on tests and are given medication that can boost their performance. Many parents seek the diagnosis as a way to help their children to perform better.

These medications are also used illicitly by many college students to improve their performance. For some, drugs offer the only chance to cope with unreasonable expectations.

### 4. Overparenting makes it harder for the kids to find a job later in life. 

Overparenting isn't only harmful for kids in their youth; the effects of helicopter parenting can stretch far into adulthood.

For example, adults who suffered overparenting as children have much worse job prospects. Employers want to see maturity; they want someone who can judge risks and persevere and get things done. Of course, that requires a good deal of independence — a character trait that is often absent due to overparenting. 

Sometimes overeager parents even get involved with their kids' jobs, doing things like calling their boss for them if there's a problem. But once their children start to work on their own, parents should only be there for moral support. 

It's not just kids who suffer from overparenting. The parents themselves suffer too. Helicopter parents are so obsessed with making sure everything goes perfectly with their children that they often feel constantly exhausted and depressed, worried that they aren't doing enough for their kids. Often the parent's ego finds expression in their children. Everything the children do — how they dress, how they perform — is seen as a reflection of the parents. 

When kids are surrogates for the parents' ego, the parents _have_ to be involved. They don't have the luxury of letting their children be independent. 

All this hyper-involvement in children's lives has to do with aiming for success in a deeply flawed higher-education system. 

Because everyone wants to get into the same colleges, students' grades and standardized test scores have to be perfect. The line between those that get in and those that don't is so thin that, in the end, it's more about day-to-day performance than it is about raw intelligence.

Furthermore, helicopter parents' obsession with their kids scores on standardized tests like the SAT is fundamentally misguided. SAT scores correlate more with socioeconomic status than cognitive ability: the students who can afford the most prep and testing get the best scores. In essence, it fails as a measurement for college readiness.

Up until now we've seen how _not_ to parent. Our remaining blinks will look at parenting strategies that will actually prepare your kids for success.

### 5. You should strive to be an authoritative parent – but not an authoritarian. 

Would you say that you have a parenting style? We all like to think of ourselves as totally unique, especially when it comes to how we raise our kids. But in reality, parenting methods can be grouped into four different categories. 

The first style is _authoritarian_ parenting. These parents are strict, and expect obedience and respect without having to explain the reasoning behind their actions or orders. This style of parenting is both demanding and unresponsive. 

Then there's _permissive/indulgent_ parenting, which involves attending to a child's every need and complying with every request. These parents are reluctant to establish rules or set expectations, and are undemanding and responsive. 

Even worse is a third style, _neglectful parenting_. These parents are totally uninvolved in their child's school and home life, emotionally distant and often even physically absent. They are undemanding and unresponsive.

The fourth style, the one that every parent should strive for, is _authoritative parenting_.

Authoritative parents are demanding, yet responsive. They set high standards, expectations and limits for their children, and back them up with consequences. At the same time, they're also emotionally available and responsive to their children's needs. Moreover, they reason with their kids, give them freedom to explore, and let them fail and ultimately make their own choices. 

In essence, this mode of parenting is a combination of the authoritarian and permissive parenting styles. Authoritative parents enforce rules but explain the reasons behind the rules so that their kids actually understand them. They treat their children as independent, rational beings, and while the parents remain involved, they accept imperfection and independence. 

Only as an authoritative parent will you be able to do the job required of you: raise independent young adults

### 6. Great parents teach their kids life skills and to work hard – without losing sight of playtime. 

It goes without saying that children's needs change throughout their lives. But no matter where they are in their development, it's important that children are allowed to be children.

Playtime, for example, is very important. It should be unstructured, spontaneous and based on the children's decisions — not the parent's. Play is an opportunity for children to develop, as they try new things, test hypotheses and observe the world around them. They need a certain degree of freedom in order to do this constructively.

Moreover, children look up to their parents as role models. It's therefore important that parents demonstrate to their kids that they too enjoy relaxation and hanging out with friends.

Play is also important later in childhood, as it is the best way to build skills and develop competencies. Some schools, such as Montessori, use this to their advantage by incorporating play into the curriculum. 

But child development isn't all play. Children must also learn to think for themselves and learn the value of hard work.

Schools today place too much focus on teaching children facts and too little on independent, critical thought. Children should not only "do well" in school, but also reflect critically on what they do. 

While it's tempting to simply impart your wisdom to your children, it's important to resist this urge and engage them in a dialogue. Let them speak for themselves, in school or with others. Let them adopt perspectives and use their own reason instead of just fulfilling tasks or learning facts.

Finally, kids shouldn't simply be told that they can achieve anything. They need to learn to put in the hard work to achieve their dreams. This can be easily taught with simple responsibilities, like chores. This will teach them to see a job through for everyone's benefit. It also teaches autonomy, perseverance and accountability, as they can easily see the fruits of their labor.

### 7. You have to truly listen to your children and let them find their own path. 

Children weren't put on the planet to walk the path that was set for them. They're all individuals, each with their own passions and aspirations. Our job as parents is to help them find it.

Children need a sense of purpose, but also the ability and the freedom to find their own paths.

Parents, especially those who are highly educated, are more likely to see their children's intellectual capabilities over their children's actual interests. If a child has the intelligence and innate skills needed to be a medical doctor but would rather work as a plumber, then she'll never be a happy (or even a good) physician.

Helping children find their passions means teaching them to listen to their intuition. Parents have to step back, see their kids for who they truly are and allow them to follow what interests them, not just what they're good at. After all, kids will only put effort into something they enjoy. If you want them to be successful, you have to teach your children to find their own way.

These lessons aren't just for early childhood. They also apply to their choice in college. 

If your kid doesn't make it into Yale, that's okay. Neither you nor your kid should consider this a failure. There are many great colleges out there, each with diverse rankings in their various fields of study. Try and help your child find a college that is _actually_ suited to his or her needs — not just one that's prestigious.

> _"In a striving, ambitious, and competitive community, such as the one in which I live, not taking time to listen to your kids starts early."_

### 8. Reclaim some personal time, and stand up for your new way of parenting. 

Many parents live for their children. But they're _not_ their children — they have their very own lives that they need to look after as well.

Research shows that kids think of their parents as their heroes, but when we look at parents today, "heroes" aren't what we find. Instead, parents are stressed, unhappy and unfulfilled. This has to change — both for your sake and for your child's. 

An adult is someone who can take care of their own basic needs while also taking time for relaxation. Helicopter parents, in their manic obsession with every aspect of their children's lives, can't do either, and in this way aren't really adults.

Authoritative parenting gives you the chance to become an adult again — to discover your passions and purpose, to learn to say "no" and to prioritize your health and wellness. Your child needs you to be happy, healthy and ultimately human if they're to find their own purpose in life. 

For example, instead of going to every single one of your kid's football games, spend time doing a sport you enjoy as well. Your child will be just as happy and you'll be a fitter, better parent.

It's only natural that you struggle with this new way of parenting. It's likely that you'll be the only parent who doesn't attend _every single_ soccer game, and people may use things like this as a reason for why _their_ child got into Harvard and _yours_ didn't. Don't just listen in silence! Stand up and explain the reasoning behind authoritative parenting.

Not everyone will be easily convinced. In these cases, it's helpful to find a like-minded community to which you can bring your partner and family. You can find many like-minded parents online who care more about independence than full schedules — parents who want their kids to do what they want, not what they're told to do.

### 9. Final summary 

The key message in this book:

**While fashionable, overparenting is highly detrimental to children. It stresses them out, robs them of opportunities to develop essential skills and ultimately serves as a barrier to success. A better way is to teach kids to be independent, decide for themselves and follow their own path.**

Actionable advice:

**People will stand in the way of you and your new parenting style. Help yourself stay the course by internalizing some basic truths.**

The truth is that the world is a much safer place than the media would lead you to believe. You don't have to shelter your children from the world. Let them experience it! They need to have an opportunity to experiment, hypothesize and sometimes fail on their own terms. That's the only way to actually help them find out what they want to do with their lives, and to learn to love the hard work that goes into mastery.

**Suggested** **further** **reading:** ** _Excellent Sheep_** **by William Deresiewicz**

_Excellent Sheep_ casts a critical view on the most prestigious American colleges and calls into question the academic quality of elite institutions such as Harvard and Yale. Ultimately, _Excellent Sheep_ reveals not just how elite American colleges stifle independent thought, but also how they directly contribute to reproducing class inequality.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Julie Lythcott-Haims

Julie Lythcott-Haims served as Dean of Freshmen and Undergraduate Advising at Stanford University, and has spoken and written widely on helicopter parenting. She is also a poet and a playwright.

