---
id: 5aae760fb238e100069e8f5d
slug: how-to-read-and-why-en
published_date: 2018-03-19T00:00:00.000+00:00
author: Harold Bloom
title: How To Read and Why
subtitle: None
main_color: FF3339
text_color: CC292D
---

# How To Read and Why

_None_

**Harold Bloom**

_How to Read and Why_ (2000) offers a broad introduction to some of the giants of Western literature, from William Shakespeare to Thomas Pynchon, and explains why we should read the great masterpieces composed by the world's greatest writers.

---
### 1. What’s in it for me? Learn how to read and interpret the greats of the Western canon. 

Unless you were an English major, it can be hard to get a proper grasp on Western literature. What should you read? Why should you read it? And, once you finally pick up one of the true classics, how should you go about reading it?

These blinks provide some answers. They'll introduce you to some major literary themes and provide you with techniques that'll both enrich and deepen your reading experience, no matter which book you decide to tackle.

Covering authors from Cervantes to McCarthy, Shakespeare to Ellison, this introduction is the perfect starting point for anyone who wants to become a more sensitive and savvy reader.

In these blinks, you'll learn

  * what one Norwegian heroine has in common with a troll;

  * why reading is an inventive act; and

  * how beauty can be found amid the banal.

### 2. There are five principles that will improve your reading experience. 

Have you ever taken a look at _War and Peace_ or _Moby-Dick_, or any other massive literary classic, and thought, "I can't read _that_ book, it's too difficult." Well, the Western literary canon certainly does include some famously impenetrable tomes ( _Finnegans Wake_, by James Joyce, for example). But, more often than not, books are only hard to read because readers don't know how to read them.

To get the most out of a book, you've got to put in work as a reader. So here are five principles that'll make your reading more effective.

First, let go of any useless academic jargon. Just because a professor once taught you to read through a particular theoretical lens — be it Marxist or feminist or postcolonial — doesn't mean _you_ have to read that way. Theory often obscures more than it illuminates.

Second, read for yourself. Too many people use reading as a means of arming themselves for cultural or intellectual warfare. Don't conscript books into your own ideological army and use them to push your personal beliefs on others. Use them to improve yourself.

This second principle dovetails nicely with the third: don't regard reading as selfish.

Reading is an enjoyable and rewarding pursuit; it expands personal horizons and reveals new inner vistas. Because of this, people often regard reading as an indulgence, a selfish leisure activity or mere "entertainment." But this is a blinkered view. We _need_ readers, for it is they — the scholars and the self-seekers — who will bring new insight to the world around them and illuminate it for others.

Fourth, a reader should be an inventor. By reading well, you'll eventually develop enough self-trust to form strong opinions about each book you read, and this will enable you to invent your own system of thought. You've selected _Don Quixote_, and not the Bible, as the cornerstone of your worldview? More power to you!

Finally, a good reader is sensitive to irony. Irony — when a person says one thing but means another — is essentially a subtype of literary metaphor, and grasping it is crucial to understanding the intent of writers such as Shakespeare or Emily Dickinson, both of whom employ it to surprising, and spectacular, effect.

With these five principles in mind, let's explore why one should read in the first place.

### 3. The reasons for reading are many, but the foremost is that it will enrich your life. 

Why do people read cookbooks or self-help books or books on mineralogy? Silly questions, right? Each book's subject makes the reasoning self-evident: to improve one's cooking, to better oneself, to learn about minerals.

But what about literature? Why do people read Herman Melville or William Faulkner? Well, in the land of the literary, there is no unambiguous, inbuilt answer. The reasons are many, and most of them have to do with self-enrichment.

For instance, reading enables you to live multiple lives.

By occupying the position of the protagonist — be it Ishmael, the narrator of Melville's _Moby-Dick_, who braves the high seas on the doomed _Pequod_ in search of the white whale; or Darl Bundren, grappling with the task of burying his abusive mother in an inconvenient gravesite, in Faulkner's _As I Lay Dying_ — you can broaden your sense of the world and what it means to live in it.

Reading literature also provides knowledge, though it's often not the kind of concrete knowledge offered by nonfiction. Rather, you learn about otherness: how other people feel and think and navigate the often rough terrain of life.

For instance, by reading Ralph Ellison's _Invisible Man_, you learn what it's like to look for meaning in a world defined by systemic racism, a lesson that will inevitably expand your sense of what it is to be human. And this is what reading _should_ do: create a bridge between your experience and the diverse experiences of other humans.

Another reason for reading is pleasure, and specifically that elusive pleasure known as the _sublime_. Most people only ever experience it when they fall in love, but it can be found in books as well. It's that euphoric, transcendent emotion that washes over one when the spiritual and physical seem to merge, and the world itself seems an aesthetically pristine creation.

### 4. Short stories offer valuable lessons about how to see the good in moments of despair. 

If you feel unready to plunge into literature's deep end by picking up a 600-page masterpiece, why not test the waters by reading a short story?

A great place to start is with the nineteenth-century Russians. Alexander Pushkin, Nikolai Gogol, Ivan Turgenev and Anton Chekhov remain some of the best short story writers of all time. And they built the foundation for many of the modern masters, including Ernest Hemingway, D. H. Lawrence, Vladimir Nabokov and Flannery O'Connor.

One of the things that books can teach us is how to spot the good and truthful amid suffering and despair. The stories of Turgenev, Chekhov and Hemingway impart this lesson with great skill.

The authorial voice of these writers — that is, the general tone of their writing — is often cold and detached, as if they don't care a whit for their characters or the bleak circumstances in which they often find themselves.

But by reading the stories closely and by paying as much attention to what is said as to what isn't, you may notice that, in truth, these seemingly desolate stories are about seeing and embracing fleeting moments of joy.

For instance, in Chekhov's "The Kiss," a woman mistakes a young officer for her lover and gives him a clandestine kiss. Though he never again sees the woman who bestowed it, the officer grows obsessed and tormented by the kiss. The story ends with the protagonist retiring alone to bed.

Between being kissed and seeking slumber, however, the officer goes walking. Outside, he touches a bedsheet that's been hung to dry in the night breeze and is overcome by the chill, rough texture of the material. Then, seeing a red moon reflected in the lake, he himself reflects on what an incomprehensible joke life is.

But this moment isn't freighted with existential despair. It's filled with joy and hope and tenuous beauty. Chekhov's story teaches us that life, even when generally banal and tragic, is filled with such isolated moments of poetic beauty, of misplaced kisses and red moons rippling in a lake.

By writing these moments, authors show readers how to find beauty in their own lives.

### 5. Poetry can transcend despair and give us a deeper appreciation of life. 

Poetry isn't the most popular form of writing these days. And this is a shame, for, as the speaker in one of William Carlos Williams's poems puts it, "It is difficult / to get the news from poems / yet men die miserably every day / for lack / of what is found there."

To unearth what can be "found there," read poetry aloud whenever possible and commit your favorite poems to memory. Memorization will unlock the hidden pleasures that poetry has to offer. Only by internalizing a poem, and letting it live within you, will you begin to notice its many facets.

Poems must be reread and memorized because they often don't use plain language. Rather, poetry is highly metaphorical, and it therefore demands special reading. Poetry's metaphors also broaden our communicative abilities.

By resurrecting dead metaphors — reminding us, for instance, that referring to a river's terminus as a "mouth" was once a creative and metaphorical innovation — and by providing us with new ones, poetry gives us new language for communicating with others and ourselves.

Because of its metaphorical nature, poetry can often seem opaque or just downright nonsensical, so it's helpful to imagine that it's intended for our wisest, cleverest self, the part of us that's not controlled by rationality and logic.

The philosopher John Stuart Mill thought poetry should be read as though it were being overheard. The author goes a step further, suggesting that poetry should be read like an overheard message intended for some essential otherness within ourselves.

By employing surprising metaphors and oblique phrasing, poetry broadens our means of self-communication, giving us access to those more elusive chambers of consciousness, such as the parts of the mind associated with dreams and nightmares.

Poetry also teaches us self-reliance. The heroes in the poems of Lord Tennyson, John Milton and Samuel Taylor Coleridge are admirable examples of self-reliant moralists, but the true paragons of the philosophy of self-reliance are Walt Whitman and Emily Dickinson.

Self-reliance is a quintessentially American quality. Ralph Waldo Emerson's essay "Self-Reliance," published in 1841, fired the hearts of a generation, and in the work of Dickinson and Whitman is where you'll find the first, and most passionate, defenses of what might be called _the_ American religion.

### 6. Shakespeare’s plays can give the attentive reader priceless rewards. 

No consideration of the Western canon would be complete without an in-depth look at the English language's most famous poet: William Shakespeare.

In the author's view, Shakespeare is the greatest and most important author of all time. His plays not only afford a better understanding of human nature, they also provide a blueprint of the many scenarios we're destined to repeat.

In terms of literary power, Shakespeare's plays have only one rival: the Bible. Shakespeare's lines read like a kind of secular scripture, and his writing has inspired generations of experts, people who've worked to uncover the plays' seemingly endless insights into human nature. Without fail, the Bard has more to tell us about ourselves than we can find to say about him.

One can read Shakespeare in many ways, but there's only one way to go about those readings: with patience and unwavering focus.

To demonstrate why Shakespeare will probably continue to demand our rapt attention for thousands of years to come, let's consider a passage from one of his most complex plays, _Hamlet_.

Hamlet's most famous soliloquy opens thus:

To be, or not to be, that is the question:  

Whether 'tis nobler in the mind to suffer  

The slings and arrows of outrageous fortune,  

Or to take arms against a sea of troubles,  

And by opposing, end them.

Some have taken this to mean that Hamlet is considering killing himself, since "not to be" would seem to mean "to be dead." In short, he's thinking of exiting life's "sea of troubles" by ending life itself. This, however, probably isn't entirely accurate.

Hamlet's real question is actually this: to act, or not to act? For much of the play, Hamlet wrestles with this question, rehearsing all the possible outcomes and tormenting his brain about the difference between actions of free will and actions required by circumstance.

This distinction continues to puzzle us, more than 400 years later. It stands to reason that it will nag in the minds of future generations as well.

### 7. Henrik Ibsen and Oscar Wilde took playwriting in both tragic and absurd directions. 

After watching a production of the Norwegian playwright Henrik Ibsen's _Hedda Gabler_, Oscar Wilde observed, "I felt pity and terror" — the emotions that, according to Aristotle, all tragedy should inspire — "as though the play had been Greek."

Indeed, Ibsen's play is considered a tragicomic masterpiece.

It helps, when approaching _Hedda Gabler_, to know something about Norwegian folklore, namely about alluring female trolls called _huldres_. A huldre is, by all appearances, a gorgeous woman, except for one distinguishing feature: she has a cow-like tail. When the huldre gets married, however, her tail falls off.

Now, as you may have noticed, "huldre" sounds an awful lot like "Hedda," the name of Ibsen's protagonist. In the play, she's pregnant with the child of the horribly boring man she's married to, yet she's also always involved in schemes that threaten to expose her metaphorically trollish, and destructive, nature.

Despite her troll-like actions, however, Hedda is a sympathetic character, because what she does is driven by an abhorrence of the soul-killing boredom of Norwegian domestic life.

Even when she's burning someone's manuscript, essentially ruining that person's career, she's also relatable and humorous. And the tragedy of her death is real and heartbreaking.

Ibsen certainly had a talent for humor, but that humor seems muted in comparison with the zany comic genius of Oscar Wilde.

Often called "nonsense comedy," Wilde's particular style, which he perfected in his plays, has less in common with the writings of Shakespeare than those of Lewis Carroll, the author of _Alice in Wonderland_.

For instance, in _The Importance of Being Earnest_, every character is utterly outrageous, which points up the essential absurdity of human nature and our misguided hang-ups about "decent behavior." To give you a sense of the play's general tenor, there's Lady Bracknell, a supremely quotable character, responding to tragedy:

When informed that someone has "lost both his parents," Lady Bracknell replies, "To lose one parent, Mr. Worthing, may be regarded as a misfortune; to lose both looks like carelessness."

### 8. Though Don Quixote invented the form, Moby-Dick is the true progenitor of the modern novel. 

The first modern novel, _Don Quixote_, was written by Miguel de Cervantes, one of Shakespeare's contemporaries. Still considered one of the best novels of all time, it introduced the world to a duo without precedent in prior literature: the possibly insane Spanish knight after whom the book is named, and his sidekick, Sancho Panza, who remains loyal to Quixote — even when he attacks a windmill that he mistakes for a warrior.

Cervantes neither explains what motivates Quixote's puzzling and profound quest, nor provides a clear moral, and this ambiguity and open-endedness established the foundation for the modern novel. 

However, the real forefather of modern fiction is Herman Melville, whose book _Moby-Dick_ is the true blueprint of the modern novel as we think of it today.

Captain Ahab, obsessed with catching and killing the white whale, is an amalgam of mad men: part Don Quixote, part King Lear, part Macbeth. Ahab, like Macbeth, is also both a hero and a villain.

Despite these Shakespearean influences, however, _Moby-Dick_ is an incredibly original novel, and Ahab is the true grandfather of the great questers in American literature.

For example, _Blood Meridian_, Cormac McCarthy's modern classic, is indebted to _Moby-Dick_ in many ways.

One of the book's characters, Judge Holden, is like something out of a nightmare. He's the leader of a group of marauders that, in the mid-nineteenth century, is hired to clear a swath of territory and kill the Native Americans who occupy it.

An astute reader might recognize some of Judge Holden's Ahab-like qualities. But the Judge, a huge, seven-foot-tall man without a hair on his body and as pale and impervious to injury as a slab of marble, would seem to have as much in common with the white whale itself as with the man who hunts it.

> William Faulkner once claimed that Moby-Dick was the one novel he wished he'd written.

### 9. As I Lay Dying, The Crying of Lot 49 and Invisible Man offer different versions of the quester for truth. 

After _Moby-Dick_, the protagonists of many great American novels come to embody the American values of self-reliance and determination. Let's call these protagonists questers for truth.

Darl Bundren is the quester in William Faulkner's _As I Lay Dying_. He and his family must complete an unpleasant task: carting the body of their dead mother through fire and flood, literally, to reach the burial spot she designated before dying.

But Darl's quest is as much mental as it is physical. His mother, abusive during life, seems capable of inflicting cruelties even after death, and Darl must deal with this grim inheritance.

The novel is narrated by other members of the Bundren family, but only Darl comprehends the truth about them. He, like a character out of Chekhov, must face the meaninglessness of life, but instead of finding solace in fleeting moments of beauty, Darl goes mad.

Or consider the protagonist of Thomas Pynchon's _The Crying of Lot 49_. A woman who begins spotting clues about a secret organization called Tristero, Oedipa Mass is another sort of quester. She, too, is obsessed with finding the truth — but, in this case, the question is whether Tristero actually exists or is just the elaborate invention of a paranoid mind.

Pynchon, like Cervantes, provides no clear answers. Rather, the reader is left to decide for herself.

And then there's _Invisible Man_, Ralph Ellison's novel about a black man who's driven underground, and literally into the sewers, by racism.

In this book, the quester is seeking a spiritual sort of truth. He must go through trail after agonizing trail and, in the end, the truth he discovers utterly overturns his preconceptions. It's a complex, ambiguous truth — the kind that exists in a world where a preacher can also be a hustler.

In all of these novels, the truth that the questers encounter is by no means simple. Just like Cervantes, the creator of the form, these novels offer answers that are ambiguous, that complicate as much as they clarify. In short, they offer the only kind of truth commensurate to the truths of lived experience.

### 10. Final summary 

The key message in this book:

**Reading is an endlessly rewarding endeavor. With patience and focused attention, you can unlock the pleasures and insights that the great writers have bequeathed us. Reading well and deeply can expand your consciousness and open your eyes to your own human nature, as well as to the diverse experiences of others.**

Actionable advice:

**Read aloud and memorize.**

Sometimes great writing will withhold its biggest pleasures and meanings until you read it aloud, either to yourself or to others, especially with poetry and the poetic writing of authors like Shakespeare. Similarly, certain passages can be stubborn until you memorize them, carry them with you and go over them from time to time. Through memorization, you can allow the words to become part of you, which can provide an understanding and connection that would otherwise be impossible. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Read a Book_** **by Mortimer J. Adler & Charles van Doren**

Since _How to Read a Book_ was first published in 1940, the blank sheet of paper that faces you when you start an essay or report has been replaced by the blinking cursor in a blank Word document. No matter: this classic bestseller, revised in 1972, is still a great guide to tackling a long reading list, extracting all the relevant information and organizing your own conclusions. Be the boss of books with this effective approach to reading and understanding texts of all kinds.
---

### Harold Bloom

Harold Bloom is the Sterling Professor of Humanities at Yale University. An expert on William Shakespeare, he is the author of over a dozen books, including _Shakespeare: The Invention of the Human_ and _Wallace Stevens: The Poems of Our Climate_.

