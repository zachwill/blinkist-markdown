---
id: 5649f7de66343100071d0000
slug: how-to-read-literature-like-a-professor-en
published_date: 2015-11-16T00:00:00.000+00:00
author: Thomas C. Foster
title: How To Read Literature Like A Professor
subtitle: A Lively and Entertaining Guide to Reading Between the Lines
main_color: F63144
text_color: C72837
---

# How To Read Literature Like A Professor

_A Lively and Entertaining Guide to Reading Between the Lines_

**Thomas C. Foster**

_How to Read Literature Like a Professor_ (2003) is an accessible and engaging introduction to literary analysis. With illuminating examples drawn from both classic and contemporary literature, these blinks provide readers with the tools they need to detect deeper meaning in any fictional text.

---
### 1. What’s in it for me? Learn how reading like a literature professor gives new depths to the experience. 

Most of us read books and find enjoyment in things like the arc of the story, the portrayal of the characters or the author's powers of description. Nothing wrong with that, but what if there is more to reading than just comprehending what is written on the page? 

Turns out, there is often a deeper meaning to a text than we might at first realize. Novels, stories, poems — all are part of a larger literary world, where themes are developed and texts wink ironically at other, older texts. Some literary works have even had such an impact that their coinages have become embedded in language itself.

In these blinks, you will be introduced to this world. You'll learn how to read like a literature professor and understand what knits one text to another. In the end, you might even get a new perspective on some of your favorite books.

In these blinks, you'll also discover

  * that a young woman in a Thomas Pynchon book is really a knight;

  * why a flea can play a seductive and erotic role; and

  * how Ernest Hemingway used irony to create new meaning from a classic poem.

### 2. Literary analysis uses memory, symbols and patterns to enrich our reading experience. 

What's your favorite book? You've probably read it more than once. You may even know exactly how many times you've read it, but can you explain what it is about that book that keeps you coming back? 

The truth is that not all readers are created equal. Some of us only focus on the _basic level_ of the story. That is, we follow what happens to a character — the choices they make and how those choices affect other characters. 

But it's more than an exciting plotline and likeable characters that make a book our favorite. More experienced readers are able to tune into what happens on the _emotional level_ of a book. They ask things like "Why did this passage make me feel that way?" or "Where have I seen this happen before?" 

By using the codes and patterns of literature, we can uncover the elements beneath the surface of the books we love. From archetypal characters to plot devices, narrative styles to literary references, there are dozens of these elements at play. So if you're determined to become an able and experienced reader, where should you start? 

Well, it helps to group these elements into three major categories: _memory, symbols_ and _patterns._

_Memory_ refers to the way in which clever readers seek to make connections between the book they're reading now and others they've read before. They'll ask themselves: "How does this character compare to others I've encountered?"

A shrewd reader will also keep their eyes peeled for _symbols_ — when an element in a story represents something other than itself. They'll ask, "What does this event (or object, or place) stand for in the story?" Finally, the literary reader discerns _patterns._ An apparently trivial detail that crops up again and again in a story may speak volumes about the main character. 

In the next blink, we'll dig a little deeper into the different elements that help us explore memory, symbols and patterns in a novel.

### 3. The tale of a quest is the underlying structure for countless stories. 

Why did you go to the supermarket this week? Probably for some humdrum reason, like a shortage of bread. But what if your grocery shopping was actually a knight's quest for self-knowledge. Maybe that seems ridiculous, but authors do something similar all the time; they take a conventional structure and change the details. 

In _Sir Gawain and the Green Knight_, a young knight journeys to a faraway land in search of the Holy Grail. On his journey, he fights dragons, overcomes obstacles and learns something entirely unexpected at the end. This story features the conventional structure of the _quest_, which comprises a _quester_, a _destination_, a _stated reason to go_, _challenges along the journey_ and a _revelation_. 

In your case, the quester might be a hungry person rather than a knight, and the stated reason to go might be a loaf of bread rather than the Holy Grail. Yet, the story's structure is exactly the same. After all, it's not the bread that's at the heart of the story; it's what happened to you on your way to attaining it.

The quest structure appears in many novels, altered to fit different characters, settings and situations. For instance, Thomas Pynchon used it to structure his 1965 novel, _The_ _Crying of Lot 49_.

Pynchon's quester is a young woman who drives between San Francisco and Southern California to carry out the will of her deceased and wealthy lover — that's her _stated reason to go_. On her journey, she encounters strange characters, bizarre outsiders and gets tangled up in a dark conspiracy. Those are her _challenges along the journey._

While the truth behind the conspiracy is never resolved, the character discovers that she's capable of independence and self-reliance. This is the unexpected _revelation_ of the quest. 

But the quest structure is just one of many universal structures that appear in literature, and it's not the only thing that different books have in common.

### 4. All books borrow ideas from other books, whether it’s obvious or not! 

If you've ever read a book that reminded you of another one, you might have been disappointed by the author's lack of originality. But, really, no literary work is completely original. Authors never stop borrowing ideas from one another, whether that borrowing is obvious or subtle, deliberate or inadvertent. 

This interaction is called _intertextuality_, and it's a literary element that can greatly deepen the reading experience. Let's take a novella by D.H. Lawrence, for example — _The Virgin and the Gypsy_. During the narrative, a violent flood destroys a homestead. This flood is remarkably similar to Noah's flood in the Bible, which suggests that the flood in Lawrence's novel isn't just a flood, but a comment on the cleansing powers of destruction. 

From nearly every book we can tease out some universal message about the human condition, a message that is relevant no matter what time or place you live in. Some claim that each of these messages will always relate back to those communicated in Shakespeare's works. Whether this is valid or not, Shakespeare has been and always will be a seminal figure in English literature and language. 

Many authors have even paid tribute to Shakespeare with intertextual references to his plays within their own works. The musical _West Side Story_ takes _Romeo and Juliet_ as its model for a modern tale of star-crossed lovers, while the title of Woody Allen's film _A Midsummer Night's Sex Comedy_ makes a brazen nod to Shakespeare's _A Midsummer Night's Dream_. 

Whether found in the Bible or folk tales or _Alice in Wonderland_, there are many depictions of the human condition that writers can adapt, appropriate and be inspired by. Reading widely gives you the full range of these depictions, and allows you to deftly pick out the universal message contained within each tale.

> _**"** There's only one story. Ever. One. It's always been going on and it's everywhere around us."_

### 5. Symbols help us detect hidden messages within a story. 

Imagine you're walking down the street and a woman about five yards away lifts her arm in the air and begins moving it around above her head. It would be rather strange if you didn't realize that she is _waving_. 

Waving is more than a mere movement of the hand, however; it's a _symbol_. It signifies to another person that you recognize them, that you're greeting them and that you want them to recognize you. In literature, anything can take on meaning when you read it as a symbol. 

Take Mark Twain's _The Adventures of Huckleberry Finn_, for example. In this novel, the Mississippi River is a symbol — and it stands for more than one idea, too. 

At the beginning of the novel, the river represents destruction, as its flooding kills people and livestock, and submerges homes. But for Jim, a slave, the river is also a passage to freedom. The Mississippi thus comes to symbolize both danger and safety. 

Symbols have also helped authors communicate messages subtly. In the past, censorship prevented authors from exploring taboo topics in their novels. So, many authors began using symbols to communicate banned topics without getting caught.

In the seventeenth century, John Donne's wrote a poem called _The Flea_, in which the narrator's sexual urges are frustrated by his lover's frigidity. Strict censorship laws prohibited Donne from mentioning sex directly. So how did Donne resolve the dilemma?

By using a flea as a symbol. The titular flea is a symbol that unites the lovers, by sucking the blood of the speaker and of his mistress. Their flea bites symbolize the consummation of their relationship, as well as the voice of the poem's yearning. Of course, this is a rather tricky symbol to recognize at first. 

A good book, however, often points you toward symbols. So keep an eye out — if something pops up again and again in a text, take a closer look. You might have a symbolic message on your hands!

### 6. Setting shapes our perception of a story subtly, yet profoundly. 

Suppose you read a story about suffering and hardship set in a harsh, icy landscape; now imagine reading the same story, but this time it is set in a lush, blooming landscape in the middle of spring. Would you respond to the story in same way?

Probably not. Surroundings play a role of incredible importance in literature. Edgar Allen Poe's writing illustrates this particularly well. Take his novel _The Fall of the House of Usher_, for example. In the first lines of the book, descriptors such as "dreary," "rank" and "vacant" stand out. 

By the time the characters are introduced, we're already certain that the lives they lead are bleak, grim and gloomy. Why? Because of the atmosphere evoked by the setting from the outset. 

In literary works, settings are not only used to establish a story, but also to form the very heart of the story itself. William Faulkner's most influential works are set in Yoknapatawpha County, a fictional yet all-too-accurate representation of society in the Deep South. 

As well as geographical location, setting also encompasses the climate and weather. Seasons are often used as symbols in literature, and their meanings are quite intuitive: spring suggests the happy days of childhood, summer evokes passion and romance, autumn often symbolizes aging and winter often alludes to dying. Symbolic seasons are often employed by authors to underline the message at the heart of their text. 

For instance, Anita Brookner's 1984 novel, _Hotel du Lac,_ portrays a woman who recovers from a troubled romance and reflects on her youth by hiding away in a resort. She arrives at the resort in late September — the autumn of her life. 

Settings often play such an instrumental role in literature that they're rather like a character of their own! So when you read, don't just explore the who and the what, but dig deeper into the where and the when.

> _"In a sense, every story or poem is a vacation, and every writer has to ask, every time, where is this one taking place?"_

### 7. Irony subverts your expectations as a reader, communicating ideas in surprising and powerful ways. 

With your solid understanding of the codes used by authors to engage their readers, it's time to meet a literary wild card: _irony_, an element of texts that throws all the rules out the window.

Simply put, irony occurs when the opposite of what we expect to happen, happens. And in order to understand irony, we must distinguish between _expectation_ and _intention._

Remember the oft-used structure of the knight's quest? If you recognize this structure in a story, you'll more or less know what to expect from the plot. So, if the plot takes an unexpected turn, you'll know that irony was involved. 

Take Samuel Beckett's influential play, _Waiting for Godot_. The two main characters wait each day by the side of the road, hoping that a mysterious figure, Godot, will arrive and bring change. 

Despite the expectations of the characters (and readers), Godot never shows up. This creates a curious feeling for the audience, and points them in the direction of the subversive social comment at the heart of the play.

We also find irony in the actions of characters. For instance, in _Mrs. Dalloway_, a novel by Virginia Woolf, war veteran Septimus Warren Smith commits suicide. His motivation? The enemies were coming. These "enemies," however, are actually two doctors. The standard perception of doctors as healers and helpers is subverted here; instead, Smith sees them as a terrible threat.

Irony may even be woven into the very title of a novel. Take Hemingway's _A Farewell to Arms._ This title refers to a warmongering poem. Indeed, the first line of the poem is "To arms!" Hemingway, however, joins the title and the first line of this poem to build the phrase "A Farewell to Arms" — the very opposite of a patriotic rallying cry. 

In this way, irony is an element that keeps readers on their toes. Once you're able to detect irony, you'll no longer take literary conventions for granted. In other words, you'll have become a truly experienced reader!

### 8. Final summary 

The key message in this book:

**By seeking out the patterns and symbols of a book, as well as the connections it has with other texts, you can read between the lines to uncover the universal message at the heart of a story. In this way, enriching your own reading experience is simply a matter of paying close attention.**

Actionable advice: 

**Take a walk in someone else's shoes — or not!**

If you've picked up a book written in a time and place you're unfamiliar with, do a little research before you begin to read. With knowledge of the major historical events of the period, the cultural and social pressures of the time and the life of the author, you'll be in a better position to explore the messages that are woven into the text. Or, simply read a story about an entirely different walk of life from your own perspective, and read closely. You'll be surprised at how much you connect with the universal insights offered by the story, no matter how foreign it seems at first. 

**Suggested** **further** **reading:** ** _How to Read a Book_** **by Mortimer J. Adler and Charles van Doren**

Since _How to Read a Book_ was first published in 1940, the blank sheet of paper that faces you when you start an essay or report has been replaced by the blinking cursor in a blank Word document. No matter: this classic bestseller, revised in 1972, is still a great guide to tackling a long reading list, extracting all the relevant information and organizing your own conclusions. Be the boss of books with this effective approach to reading and understanding texts of all kinds.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Thomas C. Foster

Foster has been teaching literature since 1975. An English professor at the University of Michigan-Flint, his book has gained popularity among English teachers and students alike. Foster is also a speaker in many high schools across the United States, giving talks and workshops on how to teach and understand literature.

