---
id: 5944f1e6b238e10005eece43
slug: how-to-run-the-world-en
published_date: 2017-06-20T00:00:00.000+00:00
author: Parag Khanna
title: How to Run the World
subtitle: Charting a Course to the Next Renaissance
main_color: E1522D
text_color: A13B20
---

# How to Run the World

_Charting a Course to the Next Renaissance_

**Parag Khanna**

_How to Run the World_ (2011) is a guide to diplomacy in today's chaotic world. These blinks paint a picture of how a new kind of diplomacy can make the world a better place, exploring the potential for new and meaningful partnerships across borders and sectors.

---
### 1. What’s in it for me? Find out how you can actually bring about global change. 

If you could make any wish come true, what would it be? At this point, it's almost an embarrassing cliché to answer the question with, "world peace."

But it's true, isn't it? 

Don't we all want a more equitable, peaceful world, where all people have their basic needs met, such as food, water, homes, and education? Politicians and philosophers have been envisioning this utopian future for ages, but we're still trying to crack it. Sometimes, though, simple techniques from the past might be the solution with little more than a simple update.

Diplomacy has been around for ages, but the modern age requires a twist to the old image of powerful men sitting around and deciding the fate of the world. So how can we affect global change for the good? These blinks will show you!

In these blinks, you'll learn

  * why anyone can be a diplomat today;

  * why shooting for the stars might not be such good advice for developing countries; and

  * how the old adage, "Give a man a fish, and you feed him for a day. Teach a man to fish, and you feed him for a lifetime," really does apply to foreign aid.

### 2. The world is a chaotic whirlwind and changing this requires rethinking diplomacy. 

Wouldn't it be great if every element of society just worked in perfect harmony all the time?

For the vast majority of us, the answer is an obvious "yes," but thanks to self-interest, the world currently functions more like a mosh pit at a rock concert than a well-oiled machine.

Just consider all the different actors at play, all pursuing their own interests. There's the Global North, the Global South, politicians, academics, multinational corporations and religious groups, just to name a few. Nobody's totally innocent in this chaotic hodgepodge, and everyone may seem suspect

Each of the actors is so ambitious that their interactions invariably result in massive power struggles. That's why a mosh pit is such a good comparison: all these different forces are wildly, sometimes even violently, moving around, colliding into each other.

Obviously, that's no way to run the world. To change the situation, we need a new diplomatic system, a _Mega-Diplomacy_ that makes every influential force negotiate and work together with every other.

But before we get there, let's first talk about diplomacy in general.

The concept of diplomacy has existed for millennia. In fact, the ancient Mesopotamians, who lived in what is now Iraq, used diplomacy to convey important messages from the divine from one city-state to another.

Later, the ancient Greeks turned diplomacy into a tool for engaging in trade and politics. And _much_ later, in the nineteenth and twentieth centuries, diplomacy became a secretive process of negotiation, carried out by the ultra-powerful in dark, smoke-filled rooms.

It's important to know this history because, of course, that's not the type of diplomacy being called for here. In the modern world, especially with emergent technologies, diplomacy is far more than a means of negotiation and a defense against war.

For diplomacy to be _mega_ is for it to be a complex web that connects a vast array of actors who work together to create a better future. But who are these mega diplomats? That's what we'll cover next.

### 3. Being a diplomat means being influential, proactive and collaborative. 

Have you ever heard of an organization called Americans for Informed Democracy or AID? This group recently put on a diplomatic workshop involving hundreds of students.

But the participants weren't just representing countries. They also represented actors like Greenpeace, the World Trade Organization and major oil cartels to simulate negotiations on all sorts of topics, from agricultural subsidies to the national debt.

This simulation is extremely useful because it's an accurate depiction of how power functions in the twenty-first century. Nowadays, any influential entity can play the role of a diplomat.

In this sense, diplomacy isn't just about representing nation-states; instead, the new diplomats are entrepreneurs, activists, academics and even celebrities. The list goes on, and anybody who has influence can be a diplomat.

Consider the charity _Oxfam_. Oxfam spends millions of dollars to make sure UN peacekeepers in Rwanda have radios, but also invests in pharmaceutical corporations in order to have a say in things like vaccine policies. In this way, Oxfam is a great diplomat because it uses its authority to forge meaningful connections.

All diplomats should aspire to a similar position by proactively approaching situations with a willingness to collaborate. The first step, working proactively, is crucial.

In cases in which diplomats aren't being proactive, a good approach is to connect their promotion to their performance. For instance, imagine a French diplomat who won't land his cushy job in New York City without successfully completing a project in Sudan to bring fresh water to schoolchildren.

From there, diplomats must understand the synergy they can achieve through collaboration. Since diplomats aren't experts, there's no way for them to understand everything about the work they do in fields like governance and development. They need to team up with people who _are_ experts in these fields. By combining their skills with those of others, diplomats can produce better policies and more favorable outcomes.

### 4. Global stability depends on regional stability and new approaches to diplomacy. 

Despite countless years of work to forge a global security system, war is upon us. In reality, it always has been. Clearly, something isn't working. Instead of aiming for a system of _global_ security, then, we should first set our sights on the construction of such systems _regionally_.

In fact, as you read this, the world is forming new regional systems, each with its own rules. Just take some of the current regional tensions, whether between Saudi Arabia and Iran or China and India.

Because of their regional nature, these conflicts preclude broader global solutions. That being said, regional approaches _can_ be incredibly effective in dealing with such situations.

This is precisely why we're seeing regional systems emerge as key international players, such as the European Union (EU), the Union of South American Nations (UNASUR) and the Association of Southeast Asian Nations (ASEAN). All three of these systems are meant to foster trust and peace within their respective regions.

Diplomacy can help in this process of building stable regions, but not the kind of state-to-state diplomacy we're used to. What's called for instead is diplomacy of the independent, crowdsourcing variety.

Such an approach is already picking up steam as new entrepreneurial initiatives, built around mediation and diplomacy, have emerged in recent years. These projects are led by independent organizations and NGOs that offer diplomatic services including legal assistance, political advice and conflict resolution.

Consider the Independent Diplomat, an organization founded by the former British diplomat Carne Ross. This group offers diplomatic services to states, regions and even so-called "stateless states" like Kosovo.

Just recently, it worked with the exiled government of Burma to foster communication between the ousted government and the junta on, among other things, how to build democracy in the country. Since this organization operates independently, it can work freely, without a higher authority telling it what to do.

This freedom enables the group to use a crowdsourcing approach in which staff work directly with clients, thereby staying continuously abreast of new developments. As a result, information doesn't get stuck behind official walls or bureaucracy.

### 5. Colonialism has left weak, fractured states in its wake, but a new kind of colonialist can fix that. 

In today's world, colonialism is widely considered to be a negative term referring to a damaging practice that fragmented the world. But can it be reappropriated for good?

Well, let's start by fully understanding the damage colonialism has done. It left scores of states in terrible shape, incapable of forging new, strong governments.

This posed huge problems as decolonization began when former European colonies were left to form their own states. In the shape they were in, they were largely unable to sustain the infrastructure and administrative apparatus built by their European colonizers. Civil wars and military coups became commonplace, which, in turn, prevented former colonies from becoming proper sovereign states.

Even decades after decolonization, many such places like the Congo and Afghanistan remain fragile, without any real governing authority. They're incapable of handling issues like outbreaks of disease, rampant unemployment and rapidly growing populations. They can barely keep people fed, much less tackle a complex and colossal problem like a stagnant economy.

Because of this, they receive assistance. In fact, in 2005, as many as 130 nations received food aid from donors and charities of different types. But how sovereign can a state be if it depends on charity to feed its people?

Case in point: Indonesia. In 2004, after the country was devastated by a tsunami, the government was conspicuously absent. In its stead, millions of dollars' worth of food and other aid flooded the country from foreign governments, citizens and corporations.

But there's a better strategy. Diplomats, or _new colonialists_ as you can think of them, can help put these splintered states back together.

So, while a myriad of organizations, governments and individuals endeavor to help weak states, in order for such a process to not become colonialism 2.0, states need to be helped to help themselves. In other words, instead of acting as a different type of occupying force, new colonialists should provide resources that help states resolve their issues on their own.

To do so, they should intervene in domestic policy, remove rogue leaders and empower the citizenry to take action.

> _"Identity crises occur . . . when a society abandons one set of values without agreeing on the next."_

### 6. Poor countries should aim for realistic goals, focus their energy and invest in public-private cooperation. 

There are plenty of poor countries in the world that are rich in natural resources but nevertheless consistently fail to pull themselves out of poverty. Why?

Well, by trying to emulate emerging economies like those of the BRIC nations — Brazil, Russia, India and China — poor countries often get in over their heads. In the process, they end up hampering their plans for development.

A better approach is to set realistic goals and focus on niche markets in which these smaller developing countries can excel. After all, the BRIC countries didn't get to their positions of global power by copying the success of the West. Rather, they focused on doing what made sense in their own economic contexts and doing it is as well as they could.

Other developing nations should take a lesson from them and do the same. Consider the Persian Gulf states like Saudi Arabia and Qatar, which thrive by focusing their efforts. Some focus on oil and gas exports while others dig into tourism or shipping.

As such, a highly impoverished country like Tajikistan should take Nepal or Kyrgyzstan as a benchmark for its own goals. While Nepal and Kyrgyzstan may not be global stars like Qatar and the United Arab Emirates, they've taken advantage of their natural resources — and their beautiful mountains in particular — to build thriving tourism industries.

To create such success for themselves, poorer nations should endeavor to forge public-private partnerships. In fact, if you look at examples of countries that have risen out of poverty, many of them have done so through the introduction of public and private cooperation. Such collaboration is central to sustainable economic growth.

For instance, in India, a Delhi branch of the family-owned Tata power helped the municipality of Delhi eradicate electricity theft. Or take the case of Saudi Arabia's oil company, Aramco. The company allied with foreign universities to build the King Abdullah University of Science and Technology, ensuring globally respected standards for Saudi Arabia's national education.

### 7. Helping impoverished countries implies focusing on their immediate needs and building their independence. 

Did you know that there are roughly 2 billion people in the world who live on less than $1.25 per day? It's true. Poverty is everywhere you look and aid agencies might actually be to blame.

In the end, dependence on aid impedes poor countries from making true progress. As a result, despite the fact that there are 230 aid agencies worldwide, people still go hungry. Foreign organizations that send money from abroad — like the World Bank or the UN — are so encumbered by bureaucracy and writing reports that they can't invest in enabling the big developments that could make lasting change.

As a result, the underdeveloped countries they are supposed to serve simply become dependent on their donations without building any capacity to progress on their own.

For instance, certain extremely poor countries like Burkina Faso, Haiti and Gambia receive 50 percent of their national budgets from outside aid. That causes absolute dependence on foreign assistance and the stagnation of any meaningful progress.

So instead of giving aid, people should focus on the immediate needs of such countries, which is a much more manageable task than tackling poverty itself. Simply put, helping poor nations means going out and learning what they need to progress.

This is a crucial step. Their needs extend far beyond money; they also include things like access to clean water, food, education and shelter. These are all needs that can be met by mobilizing people in the field — not in their New York City offices.

While many organizations attempt to execute this approach, after a few years of implementation, many of them become bureaucratically overburdened, politicized and generally much less effective.

It's important for our new diplomatic systems to dedicate themselves to building businesses that can attract foreign investment and working to meet tangible needs like getting highways, schools, and hospitals built where they are needed most.

### 8. Final summary 

The key message in this book:

**The world is a convoluted mess of global actors, each driven by its own self-interest. War, poverty and suffering are rampant and the only way forward is a new diplomatic system that fosters communication among all actors involved. This mega-diplomacy could also help poor countries build their independence and become self-sufficient over time.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _World Order_** **by Henry Kissinger**

_World Order_ (2014) is a guide to the complex mechanisms that have governed international relations throughout history. These blinks explain how different countries conceive of different world orders and how they are held in balance or brought into conflict.
---

### Parag Khanna

Parag Khanna is a global strategist, theorist and the best-selling author of _Connectography_ (2016) and _Technocracy in America_ (2017). He's an expert on future geopolitics, and a Senior Research Fellow at the Lee Kuan Yew School of Public Policy at the National University of Singapore.

