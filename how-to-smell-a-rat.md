---
id: 54fdd19b626661000a8d0000
slug: how-to-smell-a-rat-en
published_date: 2015-03-12T00:00:00.000+00:00
author: Ken Fisher and Lara Hoffmans
title: How to Smell a Rat
subtitle: The Five Signs of Financial Fraud
main_color: E22D4E
text_color: C92846
---

# How to Smell a Rat

_The Five Signs of Financial Fraud_

**Ken Fisher and Lara Hoffmans**

_How to Smell a Rat_ gives you the tools you need to avoid becoming the victim of financial fraud. It catalogs legendary fraudsters' favorite tricks.

---
### 1. What’s in it for me? Learn how to spot financial con artists and fraudsters. 

What would you do if you lost all the money you saved?

It's crushing just to think about, right? That's why in the history of obscene crimes, Ponzi schemes have to rank near the top. What kind of soulless psychopath steals people's hard-earned money by the billions, while smiling right to their faces?

It's hard to think of all the people who lost everything they had worked so hard to save up, but they were up against truly terrifying forces. People like Bernie Madoff and Allen Stanford were incredibly sneaky and clever about how they won clients' trust; and they were so ruthless they even bankrupted charities and community organizations.

That's why these blinks focus on knowing how to protect your investments, spotting these kinds of conmen, and ensuring your hard-earned money is safe.

In these blinks, you'll learn

  * the telltale signs of a con;

  * why fraudster Allen Stanford hired anti-fraud lobbyists; and

  * why the U.S. Securities and Exchange Commission won't save you from being had.

### 2. Make sure your investment adviser doesn’t also have custody of your assets. 

From celebrities like Steven Spielberg to charities like the Elie Wiesel Foundation, many people entrusted their money to investment specialist Bernard Madoff. Yet, in 2008 Madoff was revealed as a fraud who had swindled his clients out of a whopping $65 billion.

How can you avoid a similar fate?

There is a common thread across investment fraud: a financial adviser has been given control of the assets.

So whenever you do any type of investing, don't place your money and assets into just anybody's hands. Instead, employ a _custodian_, i.e., a financial institution that safeguards your securities (either physically or electronically), and keeps you informed about your accounts.

Custodians minimize the risk that your money will be stolen or squandered, as they act as a buffer between your money and the financial adviser, and have neither access to your accounts and investment profits, nor do they win a commission when you buy certain stocks.

They ensure that none of your money mysteriously disappears, bar third parties from withdrawing your money without your express consent, and verify every investment and transaction.

With a custodian, you can access your accounts online 24/7, and they also provide their own account statements, which you can cross-check with those your investment adviser provides.

But if your custodian is _also_ the person who makes your investment decisions, you're in trouble.

Usually, your financial manager can trade on your behalf, but can't remove your money from your account because he is not your custodian.

But if you give your financial adviser custody of your funds and allow him to invest them directly — if he is unscrupulous — then you've unwittingly enabled him to line his own pockets with your money and then cover his tracks with false or misleading account statements.

With no independent custodian's statement with which to compare your statements, it's easy for you to be scammed, and hard for you to do that math.

> _"If the manager has custody, he can take money out the back door — any time he wants."_

### 3. If an investment adviser’s returns on investment are constantly great, look out. 

You're on your way to meet your money manager — nervous, because industries you've invested in are nosediving. However, at your meeting, your manager tells you that your portfolio is doing great.

Is it time for a sigh of relief? Not necessarily. It could simply be the case that you're being lied to.

So how do you discern a great investor who can beat the market from a greedy rat who's after your money? Keep a look out for these warning signs:

If someone is promising you incredibly good returns: No one can honestly promise returns of 40 or 50 percent. The only people to have ever promised returns of that magnitude were master fraudsters like Nicholas Cosmo and Charles Ponzi.  

  

Of course, it's entirely possible that an adviser has gigantic annual returns once or twice in his career, but there is no secret formula to beating the market by such an extreme margin, year after year.

Equally suspicious are consistent above-average track records. Most markets are volatile — they jump up and fall down _a lot_. Simply put: consistent returns aren't normal.

In fact, according to Global Financial Data, extreme returns are the norm: Two thirds of all S&P 500 annual returns since 1926 were either below zero or above 20 percent, with one third being "average," i.e., between zero and 20 percent.

Just think of the market downturn from 2000 to 2003 or the 2008 recession, when global stocks were down 41 percent. It's extremely unlikely that anyone would perform the same in 2008 as in other years!

Moreover, all great money managers have their bad years. When the author compared the performance of legendary investors like Warren Buffet, he found that even they got it wrong around 30 percent of the time.

> _"Con artists use claims of great, non-volatile, consistently positive returns to ensnare victims and keep them docile."_

### 4. Your adviser should be able to explain investment strategies in layman’s terms. 

"Try our new, time-tested money making strategy today! We're arbitraging away proprietary options and employing a split-strike conversion to insure your investment!"

Could you understand any of that? No one can. Yet, this is often what investment jargon sounds like to the inexperienced.

However, you should _never_ invest in a strategy that you don't understand. Doing so would be like buying a painting in a pitch-black basement. Just because someone says the paintings are nice doesn't mean you'll like them in the light.

Also, you're absolutely intelligent enough to understand any straightforward investment strategy as long as it is explained properly. So, if your financial adviser can't or won't explain it to you in plain English, then it's time to sound the alarm. 

This doesn't necessarily mean he's trying to cheat you. It's entirely possible that he doesn't understand the strategy himself, doesn't have time for you or is simply trying to impress you with intelligent-sounding investor-speak. All the same, these are bad signs.

Furthermore, don't buy into a set of flashy tactics as if it were a real strategy.

_Tactics_ aren't strategies, but a set of tools. _Strategy_, on the other hand, is the plan that informs how you will be using those tools.

If you want to build a wardrobe, you'll need more than a box full of tools. You'll also need a blueprint, otherwise you won't know how to build it.

An investment strategy is like your blueprint, while tactics, e.g., investing in options, are like a hammer.

So, if your adviser tells you "we're going to earn great returns by investing in derivatives," he's keeping you in the dark. Which derivatives? And where will the returns come from? He's given you the tactics without the strategy.

Typical scammers try to sell their victims a flashy-sounding tactic as if it were a strategy, such as Madoff, who promised to make lucrative returns by using "split-strike conversions.

> _"Rats play on confusing strategies… to keep clients from questioning them too closely."_

### 5. Be weary of flashy displays or exclusivity. 

Bernie Madoff advertised his funds as being "exclusive." To invest with him, you first had to be introduced by a trusted source. Being accepted by Madoff must have felt being granted access to an elite social club **–** it most certainly didn't feel like handing over all your money to a fraudster.

Exclusivity itself doesn't mean much. If an adviser claims to cater to an exclusive, elite group of clients, it could mean a number of things, but it could also just be a trick. Some people find things more desirable if they're difficult to obtain. For them, having an exclusive adviser feels like a great achievement. But in their eagerness to "join the club," they become less likely to question their adviser's investment tactics.

By itself, exclusivity isn't a good reason to choose an adviser. It neither lowers fees nor improves long-term performance. However, there are legitimate reasons that an adviser would only take on certain investors. For instance, his strategy may be inefficient below a certain investment threshold (based on economies of scale).

Flashy displays, too, shouldn't inspire confidence, unless they benefit the investors. In the world of financial advisers, any expenditures are ultimately the client's expense. Marble furnishings and a lion-skin rug could be a sign that your adviser isn't dealing with client fees very efficiently.

If extravagant expenditures are in fact in the client's best interests, then your adviser should explain to you that plan. If he can't, then the flashy displays and expensive toys could indicate that your adviser is distracted by opulence **–** either that or it's an attempt to distract clients.

Remember: A top financial expert doesn't have time for expensive toys.

### 6. Charity donations or a good reputation aren’t proof an adviser is trustworthy. 

Imagine you've met with your financial adviser for the first time, and he offers to manage your money for free. You get all his expertise and he gets nothing. Sounds like a great deal, right? Why should you leave his office only to have to seek out another adviser whom you'll surely have to pay?

The truth is, financial advisers don't work for free. No matter what, your adviser is going to get paid. If _you_ don't pay him, then he'll make money other ways, e.g., through product recommendations. This can represent a major conflict of interests — between your interest and the interests of those who are paying your adviser.

Even worse, he could embezzle the money you've invested. Bernie Madoff, for example, didn't charge an adviser's fee, just transaction costs. Even if your financial manager isn't a fraud like Madoff, he has a huge incentive to trade if transaction costs are the only way he gets paid, regardless of whether it's in the investor's best interests.

Moreover, you can't just tell a money manager's trustworthiness from his reputation either. Anyone can earn — or buy — a good reputation, even charismatic fraudulent psychopaths.

All it takes is a few donations here or there and then a loud mouth to make a big deal out of it. Indeed, charitable donations, whether to nonprofits, respectable political initiatives or the arts, make people look generous, respectable and committed to public welfare.

Many a con man has used that tactic. Madoff himself donated more than $1 million to the Lymphoma Research Foundation in 2007, and fraudster Allen Stanford hired lobbyists to support the Financial Services Antifraud Network Act in 2002.

Both these charitable deeds added to their reputability, but still they cheated their clients.

> _"Charity's nice, but a skilled con knows some victims will believe a 'nice guy' won't embezzle."_

### 7. A friend’s recommendation or overlapping social circles is not enough to vet an adviser. 

When you're searching for a place to get a good haircut, all you have to do is ask your stylish friend for a recommendation. But when it comes to financial advisers, your friends' recommendations are suspect.

In fact, it's definitely not in your best interest to choose your financial adviser based solely on your friends' recommendations.

It takes time and care to select a trustworthy adviser, and there's no guarantee your friends paid their due diligence. It's very possible that they have no idea how to discern whether an adviser is any good. They could have even chosen their adviser based on a recommendation from _their_ friend; who knows what their criteria are for trustworthy advisers!

Moreover, con artists know that people tend to trust referrals from friends, and viciously exploit that tendency. Madoff, for instance, made a system out of it. He had "unofficial" agents who referred him to their friends **–** sometimes even receiving compensation for their referrals.

Of course, your friend's referral isn't meaningless. You will just need to make sure that their adviser stands up to closer scrutiny.

You should also resist the urge to trust advisers just because they belong to your peer group.

We're much more likely to trust people who belong to our peer group **–** people who, for instance, belong to the same church, club or an alumni association **–** and this is especially so if it's a tightly knit group.

While the two of you may enjoy some solidarity, that's not enough of a reason to put them in control of your finances. You can't assume that someone is a more capable or trustworthy financial adviser just because he belongs to your tennis club!

Fraudsters tend to exploit their affiliations and the default trust that comes with them. Bernie Madoff marketed heavily to his own Jewish community, bankrupting friends and charities along the way.

### 8. The SEC can help, but ultimately, it’s up to you to make sure your money’s safe. 

Part of the U.S. Securities and Exchange Commission's (SEC) mission is to keep investors safe from fraud. But what exactly do they do? And why are there still so many rats out there?

For one, the SEC ensures high standards of transparency for big-time advisers. All companies and individuals who receive compensation for their investment advice must register with the SEC if they manage at least $110 million worth of assets (unless they only manage hedge funds).

The SEC mandates that investment advisers disclose all the material information that might interest investors in the _Form ADV_, essentially a questionnaire which asks for information like the money managers' qualifications, past felony charges, compensation, etc.

The SEC also schedules surprise inspections for all registered firms, looking for things like improbably high returns, and enforces securities laws.

While the SEC does its best, it still fails to catch all the rats before it's too late.

The SEC rarely detects fraud ahead of time. They simply lack the resources necessary to monitor all registrants at all times.

Moreover, a thorough inspection can take weeks, and it's impossible to scrutinize every single document, meaning important information can be easily glossed over. Or the documents themselves could be filled with misleading data from the outset.

For example, when legendary crook Allen Stanford registered his firm, he didn't list himself as someone who directs investments (which he did), and instead listed himself as merely an indirect owner. That way, he wasn't required to disclose suspicious information about his past, such as his past bankruptcy.

While the SEC does a good job keeping tabs on degenerate firms, they can't know everything. Ultimately, it's up to you to diligently vet your financial advisers.

### 9. Final summary 

The key message in this book:

**Crooked financial advisers employ many tactics to trick their clients into handing over the key to their fortunes. Luckily, if you know what to look for, you can avoid the pain and frustration of hiring a rat.**

**Suggested** **further** **reading:** ** _The Big Short_** **by Michael Lewis**

These blinks examine the causes of one of the biggest financial crashes in history, and what it uncovers is shocking. What's more, it delves into how a few individuals spotted the storm on the horizon and actually managed to profit from the crash.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ken Fisher and Lara Hoffmans

Ken Fisher is the founder, CEO and Chairman of the global money management firm _Fisher Investments_ as well as the author of a number of New York Times bestsellers, including _The Only Three Questions That Count_ and _The Ten Roads to Riches_. In addition, he has written the famous _Forbes_ Portfolio Strategy column for the last 25 years.

Lara Hoffmans is a contributing author to Fisher's bestsellers, and is also a content manager at _Fisher Investments_ and a contributing editor for _MarketMinder.com_.

© [Kenneth Fisher and Lara Hoffmans: How to Smell a Rat] copyright [2009], John Wiley & Sons [Inc. or Ltd. as applicable] Used by permission of John Wiley & Sons [Inc. or Ltd. as applicable] and shall not be made available to any unauthorized third parties.

