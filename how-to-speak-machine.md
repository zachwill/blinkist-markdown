---
id: 5e21a9136cee0700073abce7
slug: how-to-speak-machine-en
published_date: 2020-01-21T00:00:00.000+00:00
author: John Maeda
title: How to Speak Machine
subtitle: Computational Thinking for the Rest of Us
main_color: None
text_color: None
---

# How to Speak Machine

_Computational Thinking for the Rest of Us_

**John Maeda**

_How to Speak Machine_ (2019) prepares us for a future where computers will play an increasingly dominant role in business, politics, and our personal lives. It helps us understand the inner workings of the machines we use every day and how their programming can perpetuate social issues or be used to exploit our personal data. By learning how to speak machine, we can arm ourselves with the knowledge we need to ensure that the future is inclusive and safe for everyone.

---
### 1. What’s in it for me? Become tech-literate to navigate our digital future intelligently. 

We already live in a world where we unlock our phones hundreds of times a day to refresh our emails, send text messages, and check social media. Even so, do we really understand how the programs we use work?

Lack of in-depth knowledge about computers might not seem so significant right now. After all, we're able to use apps and software in our everyday lives without ever considering what _nesting_ or _recursion_ are. But what happens when technology advances so much that it leaves the less computer-literate in the dust? Understanding the digital world has never been more important for the average user.

Of course, technology also opens up enormous possibilities for business owners and entrepreneurs. With the low costs associated with getting a digital product off the ground, there might never be a better time to form a start-up — if you know how to speak machine.

In these blinks, you'll find out

  * how a computer program can be like a Russian nesting doll;

  * when the words _lean_ and _agile_ have nothing to do with physical fitness; and

  * how computers can be racist.

### 2. Machines are good at repeating tasks endlessly. 

Think back to the last time you ran laps around a track or sprinted on a treadmill. By the end, your heart was probably racing, and you were out of breath. Whether you were the fittest person in the gym or not, you would have eventually gotten tired. A computer, on the other hand, can run figurative laps around a track forever without taking a break.

The "track" a computer program runs on consists of lines of code written by a human programmer. Code is based on _if-then_ logic, where if one condition is met, another action follows.

To illustrate this, take the first simple computer program the author encountered when he was in seventh grade. A friend showed him how he could get the computer to type out his name, Colin, over and over again to infinity with just two lines of code:

10 PRINT "COLIN"

20 GOTO 10

Colin's program is an example of a simple loop, which functions like a conveyor belt on an assembly line. Each task is performed in sequence until, eventually, you reach an endpoint and start again. However, there's an even more elegant way in which computers work in loops. This is called _recursion._

If loops are like assembly lines, recursion is more like a Russian _matryoshka_ nesting doll that contains progressively smaller copies of the original, outermost doll. However, because they're made of physical material, you'll eventually have made the smallest doll possible. Computers, though, can contain infinitely small or infinitely large copies of the same code.

To visualize this infinite recursion, take a look at the name of an operating system created in the 1980s by MIT's Richard Stallman. The system was created to compete with Unix, a different operating system, so it was appropriately named the GNU Project, or GNU's Not Unix. Notice how this name _contains_ recursion. The "G" itself stands for "GNU." Yes, another acronym. So you can see where this is going. If you continue to expand the acronym every time, you'll just wind up with more of the same one. Expand "GNU," and you get GNUNU. Do it again, and you're left with GNUNUNU, and so on forever.

These loops and recursions can only be stopped with a command or by an error getting in the way. So you can just imagine the power of a machine that never tires while doing exactly what you tell it to do.

> "_There's one thing that a computer can do better than any human, animal, or machine in the real world: repetition."_

### 3. Computers think exponentially. 

Remember when you first learned how to draw a cube on paper? Suddenly turning a couple of two-dimensional squares into a three-dimensional cube with just a few extra lines probably seemed pretty magical at the time. But did you ever consider how much new space you were able to visualize with each added dimension? When you turn a square into a cube, you create the illusion of an exponentially larger amount of space — the difference between 100 square millimeters and 1,000 cubic millimeters.

While we humans don't usually view the world in terms of exponential increases or decreases, this is a totally ordinary and natural way of thinking for computers. They do this through _nesting,_ in which loops are placed inside other loops. To envision this, think of a single unit of time, like one year. A year is made up of several nested loops — 12 months with 30 days in each month, with 24 hours in one day, and so on. In the same way, code dealing with smaller details can be nested inside code dealing with larger details — and there's no limit to how large or small you can go.

If one computer's ability to examine infinitely large and small scales isn't impressive enough, guess what happens when groups of computers communicate with one another? Right — their collective computing power is increased exponentially. If one computer can't handle a task, it just outsources it to another machine or group of machines to which it's connected.

Nowadays, companies like Google and Microsoft control _clouds_ consisting of hundreds of thousands to millions of computers which require exorbitant amounts of energy to function. These clouds can run loops in any dimension and ask each other for help millions of times per second — and all of our devices are connected to them. In a sense, each individual computer is a tentacle invisibly connected to the giant, powerful octopus of the cloud.

If you find yourself working with computers, be wary — playing with these exponential scales can make you lose touch with reality. When you spend every day manipulating details at sizes most people can't even imagine, you might start feeling a bit like the god of your own digital world. And once you're trapped in that mentality, it becomes increasingly hard to escape.

### 4. Machines are quickly growing more and more lifelike. 

Have you or a friend ever asked Siri or Alexa to tell you a joke or call you by a funny nickname? While these are just amusing party tricks for now, what happens when the AI responses become a little less robotic and a little more human? At what point does AI become so lifelike that we start considering it _actually_ alive?

Some AIs have already been able to imitate humans convincingly. Dr. Joseph Weizenbaum's famous computer program from the 1960s, Eliza, could carry on a conversation in English. Eliza's responses were coded to follow a simple set of if-then rules. For example, if you mentioned something about a relative, like your mother, Eliza would respond, "Tell me more about your mother." These responses mimicked real human conversation realistically enough that Weizenbaum's students thought Eliza was an actual person.

If this AI from the 1960s obeying basic if-then logic can convincingly imitate life, imagine how impressive AIs will become as technology advances. Computers can already learn how to complete tasks with very little human input. Consider _deep learning,_ a type of machine learning in which computers are taught to "think" by repeatedly observing a behavior and then figuring out how to execute it on their own. Deep learning used to be infeasible due to the enormous computing power it requires, but this is no longer an issue. And now, AI can beat human grandmasters at chess just by watching them play.

So does this mean artificial intelligence will eventually exceed human intelligence? This theoretical point in time, known as the _Singularity_, sounds a lot like science fiction. However, if you understand that computers think and grow exponentially, it starts to seem more realistic. Computer experts think so too — scientist and inventor Ray Kurzweil has already founded Silicon Valley's Singularity University, where he and others study this potential future.

Based on our knowledge that computers never tire and constantly work to execute their tasks more effectively, we can guess what will happen as AI becomes indistinguishable from human intelligence. When AI devices speak to us, they'll analyze our reactions as they smile, insert a nervous humanlike "umm," or even flirt.

While humans sometimes read each other's emotions incorrectly, AI won't — which means they'll be highly likable. And they won't just be able to beat us at games like chess — they'll be beating us at almost everything. Soon, machine-speaking humans will have the power to design and maintain the AI destined to replace us.

### 5. Machines have changed the way businesses manufacture and sell products. 

Picture a feedback box in your company's kitchen that allows staff from all departments to suggest improvements. Such feedback boxes are great, but it takes time for someone to read all the suggestions, let alone implement them. It would be much easier if the suggestions were all automatically collected, read, and sorted so they could easily be responded to, right?

In the days of digital products, tech companies are able to do just that.

Back when companies only sold physical goods, the goal was to create as perfect a product as possible before shipping it to any customers. But with hardly any associated manufacturing costs, tech companies are able to release different variations of a product before deciding on the final version. This is called _A/B testing,_ and it lets companies see which one customers respond to best.

President Obama's fundraising team carried out a successful example of an A/B test during his 2012 campaign. The team selected random groups of people from their mailing list to test the effects of different email subject lines, with the goal of determining the most profitable one to send to the rest of the list. The winner? "I will be outspent," which ultimately generated over $2 million more in revenue than another variant, "The one thing the polls got right…!"

But while minimal manufacturing costs make this kind of testing possible, they also mean that old models of a product quickly become obsolete. This has given rise to the _lean_ or _agile_ business model, where products are released in a bare-bones state, or at least somewhat incomplete, and then improved later. _Lean_ refers to keeping the product as simple as possible, while _agile_ refers to a company being able to respond to customers' needs quickly.

Thanks to the lean and agile model combined with information from A/B testing, companies can send out incremental updates to their products over time. While these allow your device to be constantly improved, they can also give companies unfair leverage. Consider Apple, which regularly releases software updates that get downloaded while you sleep. That's very convenient — until the latest version causes your device to run sluggishly, forcing you to buy an expensive new model with a faster processor.

### 6. Digital consumption allows companies to get up-close-and-personal with your data – for better or for worse. 

Open up Netflix or any other video-streaming service, and you'll be greeted with a homepage full of shows and movies you've already watched, plus recommendations for what to check out next. Sometimes, these recommendations are spot on, while other times, they're way off. That's because the algorithms that predict what you'll like and dislike aren't perfect — yet. But that could soon change, as companies are able to collect more and more data about your specific preferences, background, and beliefs.

In the earlier days of tech, customers purchased CD-ROMs containing finished pieces of software. But now, software and other digital products are released before they're fully complete, and their content is constantly shifting based on consumer feedback. So instead, customers pay a few dollars per month for regular access to a service instead of making an expensive one-time purchase. For companies, this means customers need to be pleased with their product repeatedly over time. And the best way to do that is to learn all about them, so the companies know exactly what the consumers really want.

Having a company know everything about you sounds pretty scary. But keep the benefits in mind. It's this two-way communication between a customer's data and a company that allows Netflix to recommend new programs, helping you discover more things you'll enjoy, or lets Gmail learn your writing style and offer automated email responses just for you.

In exchange, every action you take on a computer can be converted into data and sent back to someone, somewhere in the cloud. If you receive a pop-up survey, for example, your actual answers to the questions might not be the most important information for the survey sender. Perhaps your cursor lingered over an image for a particularly long time, which indicates you were interested in it. The company can then advertise to you based on what it thinks you like.

At this point, you may be thinking, "how do I stop companies from learning all this stuff?" But there's no way to turn off the two-way communication completely, and currently there aren't many legal regulations for how companies can collect and use your data.

The first real step forward was the European Union's 2018 General Data Protection Regulation (GDPR), which forces companies to acknowledge the fact that they're collecting your data and ask for your consent. But the US still has no similar legislation. More policymakers will need to learn to speak machine if we're to avoid exploitation of our data in the future.

### 7. There’s a diversity problem in the tech world, and machines can perpetuate it. 

When you think of famous computer scientists, Alan Turing probably comes to mind immediately. But did you know that many of the first computer programmers were women?

Unfortunately, many hardworking women have been written out of the history of computing, and these days, they're being excluded from the modern tech industry. Right now, only 21 percent of people employed in the US tech industry are women, even though the overall percentage of women in the United States is around 50 percent. Similarly, African Americans and Hispanics make up 7.4 percent and 8 percent of the tech sector respectively, while their representation in the private sector is 14.4 percent and 13.9 percent.

So what could explain this gap? For one, the most highly cited reason for leaving the tech industry is harassment. Women and people of color are the most likely groups to experience this at work. At the same time, companies are striving to preserve a "culture fit" within their teams. From their perspective, advancements in tech are moving at breakneck speed — so the faster company decisions can be made, the better. New hires who are already "just like us" will probably share a mentality which results in fewer disagreements.

But like-minded people making quick decisions also let issues slip through the cracks. A more diverse team could recognize these and deal with them. Consider the example of a popular social media company who released two offensive real-time image filters just months apart. One filter added slanted eyes to the user's face, caricaturing Asian eyes, while another filter darkened lighter skin to make it look black. Not only did this cause hurt and offense, but it also led to a costly PR backlash, all of which could have been avoided.

There are diversity issues in tech which go even deeper, with discrimination being encoded in the machines themselves. Take the since-discarded Amazon hiring tool created in 2014 that was programmed to assist in choosing potential candidates. Based on the data about the company's existing employees, the program ended up demeriting any résumés that included the word "women's," as in "women's college."

Without an environment where employees feel comfortable expressing different viewpoints, companies have less opportunity for innovation and growth. Fortunately, some tech-company executives are now working to address the inclusion issue. One example is Annie Jean-Baptise, head of Google's new "product inclusion" initiative, who is currently working to increase diversity in the company's equipment suppliers and populate its image database with more diverse representation.

Initiatives like this allow companies to serve their customers better and broaden their user base, as well as address societal inequalities.

> _"The challenge of righting the imbalance in the tech industry presents an immediate opportunity for new business growth and disruptive innovation."_

### 8. Machines process data, and data alone can’t always paint a complete picture. 

We know that machines can run endless loops without ever tiring, at scales we can hardly even imagine. Not to mention, they're all connected to each other, so they possess enormous collective power. As that power grows, and their intelligence even surpasses ours, where does that leave us humans?

Fortunately, there are plenty of ways in which we're still superior to machines. A particularly important one is our ability to interpret qualitative data, whereas machines can only collect quantitative data.

Take the example of a famous soup company whose best factory operators were growing older and retiring. To replace these human employees, the company created an AI encoded with if-then rules for making perfect soup in the same way as the retirees. But when the AI was tested, the soup it made tasted terrible. How could that have happened, when it followed the instructions perfectly? The testers asked one of the human operators to explain why the soup had come out so badly, and he responded simply, "It smells bad!"

Sometimes, machines seemingly do everything perfectly yet still fail. Other times, they succeed at doing exactly what we program them to do, which can be just as bad. Consider COMPAS, an algorithm that suggests sentences for crimes. It uses data on past sentences to inform its decisions — which, sadly, causes it to suggest harsher punishments for black defendants.

What these machine mishaps show is that we need to evaluate computer-generated data carefully, and not just pay attention to the raw numbers.

A computer can spit out a statistic such as "90 percent of users spend most of their time checking their blog's viewing statistics." As a web designer, this might lead you to think that the view counter is an important feature to users and should be improved. However, a real user could explain to you that the view counter is right smack in the middle of the landing page when they visit the blog. And worse, the constant reminder of how many people have seen their posts — or haven't — discourages them from continuing to use the site.

So don't be too worried about becoming obsolete just yet. Even with all their strengths, machines are still imperfect — just like the humans who create them.

### 9. Final summary 

The key message in these blinks:

**Speaking machine involves understanding the fundamental differences between the ways computers and humans think. Machines think in logical loops, repeating tasks endlessly until stopped by a command. And they process quantitative, rather than qualitative data, which they can't interpret in the same way humans can. If more people learn these differences, we can confidently face a future where computers play an even more dominant role in our lives and where no one will get left behind.**

Actionable advice:

**Disable third-party cookies.**

You're probably used to seeing a message asking you to "accept cookies" when you visit a website for the first time. Likely, you immediately click "accept," which isn't a bad thing — cookies help websites provide a better experience for you by learning your specific preferences. However, these cookies contain your personal data, and if you're in the United States, they can be sold without your consent to _other_ websites you've never visited. To take back control of whom you allow to collect your data, head to your browser settings, and disable third-party cookies.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Deep Thinking,_** **by Gary Kasparov**

In these blinks, you've heard about deep learning and how machines can teach themselves to beat humans at complex games like chess. But do you know the full story of the famous AI, Deep Blue, which was able to beat chess grandmaster Gary Kasparov? _Deep Thinking,_ written by Kasparov himself, is a classic read that explains the boundaries of AI and the role that human creativity will continue to play in the future.

So if you're interested in an inside perspective on a revolutionary moment in computer and chess history, head on over to our blinks to _Deep Thinking_ by Gary Kasparov.
---

### John Maeda

John Maeda is a designer, computer scientist, author, and former president of the Rhode Island School of Design. He is an executive at Automattic, a web developer, where he aims to increase diversity and inclusion in the tech world. He has written interdisciplinary books on design and technology, such as _The Laws of Simplicity_ and _Design by Numbers._

