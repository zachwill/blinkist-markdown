---
id: 57c93eef08dc1b000317fe51
slug: how-to-start-a-start-up-en
published_date: 2016-09-05T00:00:00.000+00:00
author: ThinkApps
title: How to Start a Start-up
subtitle: The Silicon Valley Playbook for Entrepreneurs
main_color: 2163A6
text_color: 1C528A
---

# How to Start a Start-up

_The Silicon Valley Playbook for Entrepreneurs_

**ThinkApps**

_How to Start a Start-up_ (2015) is a practical guide to founding your own company. From pitching for funding to hiring employees, these blinks offer tips, strategies and insights about the first steps a start-up should take to forge a path toward solid, sustainable growth.

---
### 1. What’s in it for me? Find out how to get your start-up off the ground. 

Start-ups: they're springing up everywhere, all the time. But does the fact that start-ups are ubiquitous mean that successful entrepreneurial ventures are easy to create? Well, not really — especially if you don't have the guidance and knowledge you need to get you started on the right track.

First of all, you should make sure you have passion, a great idea and a great product. But what then? These blinks will show you. As a practical guide on how to go about launching a start-up, they'll provide you with useful advice on everything from product development to hiring and managing employees.

In these blinks, you'll also learn

  * what a former executive at LinkedIn and PayPal has to say about delegating work;

  * how to present your start-up to others without confusing them; and

  * why the homepage for the start-up Wufoo makes dinosaur sounds.

### 2. Start-ups need talented founders, streamlined processes and independent team members to grow. 

Did you ever build a house of cards as a kid? These flimsy constructions need a strong base and consistent design if you want them to stay standing. Similarly, all start-ups need solid foundations to achieve sustainable growth. So how can you ensure you get off to a good start? By working with more than one founder.

A start-up with two or three founders stands a good chance of sustaining itself because it has multiple skill sets built into its foundation. Say one founder is a strong business developer. He'll need a cofounder with great people skills, and perhaps another with more technical knowledge. In this way, cofounders can compensate for each other's weaknesses, creating stronger foundations overall.

Other crucial aspects in creating a sustainable start-up are _simplifying_ and _delegating_. Let's first take a closer look at how founders can boost their start-up by keeping things simple.

According to former Linkedin and Paypal executive Keith Rabois, simplifying processes can increase overall performance by 30 to 50 percent. For instance, don't come up with a long list of growth initiatives and try to pursue all of them at once. Instead, distill that list down to three items or less. This will help everyone understand what is important and allocate their time accordingly.

Rabois also advises founders to delegate projects to employees. But this doesn't mean assigning a team member a task and then micromanaging them every step of the way; this only slows processes and makes teams disgruntled.

Instead, it's best to let employees take the initiative and get their creative juices flowing. Feel free to guide your team through critical decisions by explaining your perspective. But if you really want them to start innovating, you'll need to get out of their way!

### 3. Your first hires should be passionate all-rounders ready to take on any challenge. 

On top of great founders, simplification and delegation, there's another ingredient central to sustainable start-ups: team members. Not everyone has what it takes to work in a start-up, so what should you be looking for when hiring? And when should you hire?

In the beginning, it's best to do as much as you can yourself. You'll need to save every penny to create your product and market it to customers. As your business grows, you'll need a few extra hands to get things done — and by then, you'll be able to afford it, too.

But because your start-up is still likely to be quite small, it won't make sense to go out and hire an entire team and a bunch of interns. Nope, one or two all-rounders are all you'll need. Just look at Airbnb, which only hired a total of two new employees in its first year of existence!

Just as founders handle duties across several fields, early-stage employees should be comfortable managing everything from product development to PR to logistics. Once you're ready to hire, seek out someone who can handle a wide variety of tasks.

Of course, in order for employees to stay motivated while taking care of multiple responsibilities, they need to be _passionate_.

This is something that Airbnb took very seriously when hiring. CEO Brian Chesky actually asked interviewees if they would still work for Airbnb if they had one year to live. Those who said they wouldn't were not advanced in the hiring process.

Unfortunately, not all hiring processes are foolproof. Sometimes new team members who shine in interviews lose their enthusiasm after the first few weeks. If things just aren't working out, you have to let them go. In these early stages of a start-up, you simply can't afford to have people on your team who aren't committed.

Once you've found great employees, their happiness should be a top priority. After all, you want them to stick around! Give them credit for what they achieve, and help them make their work feel like less of a job and more of a calling.

### 4. Create great first impressions with customers to start great client relationships. 

If you're getting ready to go to a job interview, go on a date or meet a group of new people, you would probably try to look your very best. Everybody knows that first impressions go a long way, so why would we think any differently about the first impressions that we give customers? Our first interactions are vital to developing stable relationships with them.

To grab the attention of new customers _and_ keep them coming back, you'll need to make a good first impression. From e-mail campaigns to public billboards, it's important that each customer's first experiences with you are positive and memorable.

Sometimes all it takes is a small special feature to get customers engaged. When customers visit the homepage of online survey service Wufoo, they are greeted by the cute little _roowwwrrrrrrr!_ of a cartoon dinosaur. Gestures such as these put smiles on the faces of customers, giving them positive and lasting memories about your company.

Unfortunately, most companies aren't able to charm _all_ of their customers with a cartoon dinosaur. Different demographics have different needs and values, as do individuals within those demographics. Responding to an individual customer's needs is a challenge, especially for younger, smaller companies. For this reason, start-ups often need to be resourceful with the team that they have.

For Wufoo, solving the problem of answering the needs of each customer meant having their software development team spend time helping and supporting customers directly. What did this entail? Well, each developer spent 30 percent of their working week interacting with customers who were having problems.

While this might seem like a lot of effort, it allowed the developers to tap directly into the needs of their customers. As a result, they were able to respond to these same needs when designing the overall service.

> _"Seducing your customers doesn't have to involve some glossy campaign. As long as you think about what emotions you elicit on your user's face, you're heading in the right direction."_

### 5. Win over customers with one-on-one interactions where you listen to them and follow up afterward. 

Fishing is, a lot of the time, about waiting. Once you've tied your line and cast it, all that's left is to wait for the fish to bite. Fishing for _customers_, however, is a different story altogether. If we want customers to bite, we need to actively seek them out.

Most start-ups already know who their ideal potential customers are. But do they know how to find them? The key here is building relationships, even on personal levels. And conferences are a great way to make one-on-one connections with prospects.

But choosing the right conference to attend is crucial. Firstly, the conferences you attend should be _relevant_ to your business. That is to say that, if you run a fintech start-up, you'd be best served heading to fintech conferences where you know you'll meet people interested in your business. It's also best to visit _smaller_ conferences, as they offer you a better chance of making small talk with potential customers, engaging with them personally and thus letting your business stand out.

Keep in mind that talking to customers doesn't mean giving them an exhaustive account of your product's features. That won't get you very far! What really works is _listening_. When we listen to people, we show them that we care for and understand their needs. For this reason, people tend to have faith and trust in those who listen to what they have to say. In your case, this faith and trust may result in people buying your product.

But what if customers don't seem interested after your first attempts? Don't despair! That's what follow-ups are for. After all, we all get caught up in the pressures of our daily routines. Sometimes all it takes to get customers on your side is a little reminder.

By frequently following up with a potential customer, you can show them that you are reliable and eager to help them with any problems they might have. Therefore, as a rule, it's perfectly fine to make regular and continuous attempts to follow up on first interactions until that potential customer shows a clear sign of interest or disinterest.

### 6. Perfect your pitch to raise funds for your start-up and ensure growth from an early stage. 

Most of us would want to get our start-ups up and running with a minimal amount of investor money. After all, it always seems more impressive that way! But at some point, virtually every start-up will need help from investors. In fact, it's better to get their support while you're ahead; this protects you from rival start-ups keen to take the lead. So how can you charm investors onto your side?

Investors are always on the lookout for companies with real products that customers love and which are making money. This demonstrates that a start-up is sustainable, that the entrepreneur can stand on her own two feet and that there's market interest in the product. Investing in these start-ups seems less risky and much more appealing. Start-ups with investor support are then able to grow and compete more effectively.

But having a great, working product isn't always enough. To win over investors, you'll need to present them with the perfect pitch. A perfect pitch should include three things: your product, the market size and your growth rate.

Make it clear what it is your start-up does by imagining your listener has no idea about the company or its context. Say you had to explain Airbnb to your grandmother; you'd probably say something along the lines of "We let people rent out extra rooms in their houses." Keeping things simple is the key!

Next, tell your potential investor about the size of your target market and your opportunities to reach interested customers. This will help them get an idea of their chances of getting great returns on their investment.

Finally, show investors how fast you're growing with the help of key statistics. Here's an example: "We started in February, and now we're already growing at a rate of 35 percent each month. Our sales total is $10,000 and we have 6,000 users and counting."

> _"If you can create a unique product with as little funding as possible, investors will be even more inclined to see what you can do with the additional resource of their money."_

### 7. Final summary 

The key message in this book:

**Building a start-up isn't easy. By taking steps to ensure you start off right, whether in your marketing, hiring processes or even your founding team, you can put your start-up on the path to success.**

Actionable Advice:

**Check your user activity and follow the curve!**

Next time you're wondering why and how your product needs to develop, start checking your user activity. Calculate the percentage of users that are active at least once per month, draw a retention curve based on when they signed up for your service and update it monthly with your data. An asymptotic curve means you're going in the right direction, and can start making the most of growth tactics. But if the curve keeps plummeting without flattening out, you need to work on getting a product-market fit.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Lean Startup_** **by Eric Ries**

_The Lean Startup_ (2011) helps start-ups and tech companies develop sustainable business models. It advocates continuous rapid prototyping and focusing on customer-feedback data.

The method is based on the concepts of _lean manufacturing_ and _agile development,_ and its efficacy is backed up by case studies from the last few decades.
---

### ThinkApps

ThinkApps, based in San Francisco, is a company dedicated to helping their clients build stunning web, mobile and wearable device apps. They're passionate about supporting top Silicon Valley start-ups and innovative enterprise companies, and are responsible for apps used by millions of users, such as PicPlayPost.

