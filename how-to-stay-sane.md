---
id: 5623b0983264660007040000
slug: how-to-stay-sane-en
published_date: 2015-10-19T00:00:00.000+00:00
author: Philippa Perry
title: How to Stay Sane
subtitle: None
main_color: 247AB2
text_color: 174F73
---

# How to Stay Sane

_None_

**Philippa Perry**

In _How to Stay Sane_ (2012), British psychotherapist and author Philippa Perry shows you how to better nurture relationships while using self-observation, "positive" stress and the power of stories to achieve and maintain your mental health.

---
### 1. What’s in it for me? Learn how to keep your cool and stay sane during tough times. 

During a stressful time in your life, have you ever felt like you're about to lose it? Have your emotions been so frayed that going insane seems the next obvious step? 

We've all experienced these "breaking point" moments, and it can be terrifying. So how do you avoid going crazy amid a crazy world? 

These blinks give you the tools to keep your cool even when it seems the world is falling apart around you. Learning to observe yourself in the moment is crucial; equally so is encouraging moments of "positive stress." 

In doing so, you'll find healthy ways to maintain your mental health and reassure yourself that you'll never "lose it" again. 

In these blinks, you'll discover

  * how your overall sanity is linked to your personal relationships;

  * why it's so hard to overcome feelings with reason; and

  * how your left brain sometimes tricks your right brain.

### 2. Your emotional right brain trumps your logical left brain when making decisions in life. 

You might think of yourself as a reasonable person who makes fairly logical decisions. Yet this perception is mostly an illusion!

We're not as reasonable as we might think we are, as we're mostly ruled by our _right brain_, the home of human emotion and instinct.

At the age of two, the brain's right side, as it develops, becomes more active than the left side. It's in this right brain that your personality is formed, guided by cues from your environment. 

When we're young, our social environment is largely made up of our main caregivers. So your personality, in terms of whom you trust and with whom you bond emotionally, is formed early on based on these social surroundings.

The brain's left side, home to language, logic and reason, develops mostly from around age three. By this time, however, it's long in the shadow of the right in terms of influence. This is why it's so hard to overcome strong emotion with cold logic — your right brain remains dominant.

Why then do we think we are inherently reasonable creatures? Well, it's because the left brain essentially "tricks" us into thinking this way.

When our right brain makes an emotional decision, our left brain in retrospect comes up with a reason that makes the decision seem logical. This process is called _post-rationalization_.

Neuropsychologist Roger Sperry conducted an experiment in which he triggered the command to "walk" in a subject's right brain, in a way so that the left brain was unaware.

Interestingly, the subjects walked when instructed. Yet when asked why they decided to walk, they came up with random reasons, such as wanting to get a drink of water or stretch. The subjects weren't lying, of course — they believed the justification offered by their left brains!

So now you know that how you reason may not be totally reliable, given the relationship between your right and left brains. Let's examine in this context then why we act the way we do in stressful situations.

### 3. Meditating or praying are methods of focused attention that help you to calm your mind. 

Are you able to turn your emotions on and off like a light switch? It's impossible, even though you might try to convince yourself otherwise. 

Yet we can ‒ and should — find some space to observe how we feel and who we are at our core. 

Practicing _self-observation_ is paramount to keeping your sanity, as it helps you notice and therefore alter the behaviors that stifle your well-being.

By observing yourself, you can create some distance from an event to better assess your feelings and thoughts in a non-judgmental way. 

Let's say you're furious over something a friend said to you. Instead of just _being_ angry, try to _observe_ your anger. This helps you to separate yourself from your feelings, and calms you down.

One of the most effective methods of practicing self-observation is to keep a diary.

Write down your feelings, thoughts or random memories that come to mind during the day. Then read what you wrote, looking for any places where you seem to repeat yourself or show emotional habits. Once you are aware of these moments, you can better get rid of them in the future.

Another way to practice self-observation is through _focused attention_. Here, you focus on and experience your body and mind in the present moment. When people meditate or pray, they are using focused attention. Doing so improves your concentration and reduces anxiety and depression.

So what do you do? Simply breathe. Sit down for one minute and focus on your breathing. 

After a short while, you'll notice that your mind starts to wander. When it does, gently refocus your attention on your breathing. You'll need to practice, but soon you'll be able to do it without effort.

Through self-observation and focused attention, you create a gap between you and your thoughts and feelings, and in turn, begin to get a better handle on them.

> _"The unexamined life is not worth living." — Socrates_

### 4. To stay sane, you need to nurture trusting relationships; but first you must know yourself. 

Have you ever caught yourself judging a person before you've even spoken one word to her? Sure, many of us have. 

The problem is, judging someone prematurely can prevent you from forming a relationship that might be actually beneficial to you.

Humans are social animals, and one way we all stay sane is to maintain and nurture healthy relationships. Yet too frequently we judge others based on our past experiences.

Let's say for example you were abandoned by a caregiver when you were young. As a result, you may not be able to trust people, as you assume that they, too, will disappoint you. You're essentially projecting your past experiences on new people you don't even know.

So what can you do instead of judging others prematurely? You can work to understand the new person and her true feelings. But before you can do that, _you must understand yourself_.

According to psychoanalyst Peter Fonagy, this means that when you're familiar with your inner emotional life, you become more sensitive to other people's feelings ‒ which in turn enables you to maintain and nurture relationships.

If you know that you're offended when a friend pokes fun at you, for example, you'll realize that your friend might also be offended if you made fun of her. Therefore, you'll know to keep any off-hand remarks to yourself!

We sometimes get stuck in behavioral patterns that hold us back from having fulfilling relationships. This is where self-observation again comes in handy.

Zara would fall in love yet watch as relationship after relationship would quickly fall apart within months. Tired of the heartbreak, she began observing herself and keeping a diary.

She soon found a pattern. She realized that she quickly jumped into bed with every new romance, then became extremely needy and demanding in the relationship. Once she recognized the cycle, she worked hard to break it — and eventually formed a healthy, long-lasting relationship.

### 5. Too much stress is bad and shuts our brains down; yet a little good stress keeps our brains healthy. 

High levels of stress can make us sick, sometimes evolving into full-blown panic or anxiety attacks.

But did you know that not all stress is actually harmful? 

If you want to keep your brain working like a well-oiled machine and continue to learn, experiencing moderate amounts of stress can actually help your brain's neural pathways grow.

This sort of stress is called _good stress,_ and occurs when we engage in something new and challenging. Such activities stimulate the brain more than usual, and in turn improve the brain's overall functioning.

Importantly, good stress also promotes longevity.

Welsh researcher David Snowden looked at a number of subjects and found that individuals who held a university degree and continued to learn and cultivate new interests throughout their lives were actively encouraging levels of good stress.

Consequently, these individuals enjoyed significantly longer lives and were less affected by the mental deterioration often observed with age, such as dementia.

But how do you cultivate good stress in your life? The answer is simple: you have to move beyond your comfort zone.

Pushing beyond your comfort zone means engaging in activities that make you a little nervous. The first step is to think about which activities are within your comfort zone; these are the things you are absolutely comfortable doing, such as reading articles online.

Then think about the things that you'd like to achieve, but for which you'd need a push, such as writing your own blog. Now think of something you're pretty scared of doing, such as writing a book.

The trick is to start in the middle: writing a blog. This will push you a little past your comfort zone but not so much that you're too frightened, and at the same time will increase your self-confidence.

Before long you'll feel ready to tackle the things you didn't have the courage for before. Your brain and body will thank you for the stimulation!

### 6. Stories or narratives shape the way we think, for better or for worse. 

Whether it be fairy tales from our childhood or the latest drama series on Netflix, we're all fascinated by a good story. 

What is it about stories that capture the imagination? Stories, or _narratives_, help structure our thoughts as well as aid us in interpreting our daily experiences, allowing us to find meaning in our world.

Many fairy tales, for example, follow a particular structure, with a villain being vanquished and a prince or princess living happily ever after. Such "happy endings" help children especially make sense of negative experiences, showing them that often things do turn out well in the end.

Narratives can also instill negative views, however. Some stories can encourage prejudice or pessimism, trapping us in a pattern of negative thinking.

Thus we need to be careful, ensuring to question the point of stories we share with each other. Too often, we just don't realize how stuck we are in certain ways of thinking, and how these habits influence our behavior.

Consider this "story." A man is driving through a long stretch of desert when his tire blows out. As he pulls over to address the mishap, he realizes he can't fix the tire himself as he has no jack.

Yet he remembers that he passed by a garage only a few miles back, and starts off on a long, hot walk. Along the way, he thinks of all the times he's had bad experiences with his car at auto shops, and the memories build until he's quite paranoid of what is waiting for him at the desert garage. 

He knows the mechanic can take advantage of him, given he has nowhere else to turn. Yet when he arrives at the garage, the friendly mechanic is all smiles — but our stranded motorist doesn't see it. All he sees is the narrative in his head and as a result, his misplaced anger keeps him from dealing with the mechanic politely and getting the immediate help he so desperately needs.

Yet don't forget that you are not a victim of how you think. Stories are flexible; they can help us see the world in a positive light, too.

> _"Part of staying sane is knowing what our story is and rewriting it when we need to."_

### 7. Make good things happen by keeping your head up and adopting a positive attitude about the world. 

We can rewrite our stories. By editing the narratives we live by, we can change the way we think, draw more positive meaning from the world around us and fulfill our goals.

Stories with a positive spin can also work wonders for personal motivation.

One artist was struggling with repeated rejections from artists' residencies. To improve her morale, the author told her a story about a salesperson who made the realization that he only made a successful sale every 50 calls.

So with each rejection, the salesperson would become more excited, knowing he was actually closer to that sale.

This story transformed how the artist viewed her rejections, and boosted her confidence to keep applying. Eventually, she was selected for the artist's residency she wanted.

While positive narratives can help us push through, visualizing good things happening and being optimistic can also increase our well-being. This is how we can attract more good things into our lives.

Say you're going to a party where you don't know a single person. If you enter the room with an optimistic mind-set and your head held high, you're likely to catch someone's attention, spark up a conversation, find some common ground and learn something new.

If on the other hand you wander into a gathering with your eyes to the ground, convinced that no one is interested in talking to you, you'll probably have a terrible time.

This doesn't just apply to parties, but to life in general. Invest in optimism! Some researchers have discovered that optimists tend to have better physical and mental health and live longer than pessimists do.

So pull your shoulders back and keep your head high — it might just prolong your life!

### 8. Final summary 

The key message in this book:

**You can benefit tremendously from the measured observation and better understanding of your inner life. Through self-observation, thinking optimistically, pushing beyond your comfort zone and nurturing relationships, you'll not only understand yourself better but also will be healthier and happier.**

Actionable advice:

**Sharpen your awareness with the** ** _30-minute exercise_** **.**

Sit down without any distractions for 30 minutes with a notebook and a pen. Pay attention to your breathing. When a thought crops up, write it down using one or two words, and then release it. After 30 minutes, take a look at your thoughts. They will make you more aware of your thinking patterns and empower you to change those patterns if they're overly negative. 

**Suggested** **further** **reading:** ** _How to Find Fulfilling Work_** **by Roman Krznaric**

If you feel trapped in your job or long for more fulfilling work, you're not alone. _How to Find Fulfilling Work_ (2012) explores the core components of what makes work meaningful and full of purpose, detailing exactly which steps you need to take to find work that brings out the best in you and keeps you truly happy.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Philippa Perry

Philippa Perry is a psychotherapist and author. She wrote the graphic novel _Couch Fiction_, and contributes to _The Guardian_ and _The Observer,_ as well as the magazine _Psychologies_.

