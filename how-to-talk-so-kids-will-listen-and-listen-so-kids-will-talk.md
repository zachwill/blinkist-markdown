---
id: 555b42eb6135330007150100
slug: how-to-talk-so-kids-will-listen-and-listen-so-kids-will-talk-en
published_date: 2015-05-22T00:00:00.000+00:00
author: Adele Faber and Elaine Mazlish
title: How to Talk So Kids Will Listen & Listen So Kids Will Talk
subtitle: None
main_color: 2FB8E9
text_color: 1E7796
---

# How to Talk So Kids Will Listen & Listen So Kids Will Talk

_None_

**Adele Faber and Elaine Mazlish**

_How to Talk So Kids Will Listen & Listen So Kids Will Talk_ (1996) is a practical, clear guide that helps parents improve communication with their children. Faber and Mazlish present realistic scenarios that can have parents tearing their hair out in frustration, and then provide precise tactics for not just coping, but turning around the situation and creating more harmonious parent–child interactions.

---
### 1. What’s in it for me? Civilize conversations with your child. 

We've all been there, sat in a restaurant about to enjoy a meal when, on the table next to us, a child starts ranting and raving. No matter what the parents do to try to calm him down, the screaming continues. Eventually the parents themselves resort to raising their voices and demanding the child pipes down. One thought crosses your mind: thank God that isn't my kid!

But how can you ensure that you never make the same mistakes? To avoid the screaming child situation you need to learn to communicate effectively with rugrats and teenagers alike, and these blinks will show you how. With the right techniques, you can minimize the tantrums and misunderstandings of family life.

In these blinks you'll discover

  * why you should never punish your child;

  * why you should never praise a child for something they haven't done; and

  * why you shouldn't tie your child's shoelaces for them.

### 2. The only way to get through to a child and change their behavior is to acknowledge their feelings. 

Picture this: you're shopping for groceries with your five-year-old and he suddenly starts shrieking "I'm hungry! I want food _now_!" What do you do? Many of us would succumb to shouting back at him or snap and tell him to shut up. But will he listen? No. He'll keep on screaming!

Instead of blaming the child, what can you do to pacify the situation?

A lot of the time, kids don't listen to their parents because their feelings aren't being acknowledged. A child's behavior is tied to how he or she feels, and often when we communicate with a child, we don't address this.

Demanding that your child be quiet in the supermarket overlooks his feelings. After all, behind his tantrum lies hunger and probably frustration at being ignored. Your child cannot see why he should behave well when he feels so bad.

The only way to get through to him is to accept and address his feelings. You can start with an acknowledgement of how he feels — this shows you are listening. This can be as easy as "I realize you're hungry, it has been a while since breakfast, huh?" or you can playfully grant him his wish in fantasy: "If I could do magic, I would click my fingers and pluck a burger out of the air!"

Remember to be honest, though. Don't try to acknowledge his feelings if you don't understand what they are, as he will pick up on this and think you aren't listening to him, which will add more fuel to the fire.

But what if you try the tactics above and you can still feel yourself getting angry? What can you do about it?

> _"Sometimes just having someone understand how much you want something makes reality easier to bear."_

### 3. Control your anger and learn a few key communication skills to convince your child. 

You know when your child simply won't listen and you snap, letting the anger get the better of you? As you've probably noticed, your irate response doesn't get your child to stop what she's doing. You can shout, use sarcasm, threaten or command, but they all have the same result — the child won't listen. She'll reciprocate by becoming more angry herself.

So, when this happens, what's the best way to go about it?

Enlist a few vital communication skills to get your children to cooperate. Each of these skills will help create an atmosphere of respect and understanding, which is crucial if you want your child's behavior to change.

To demonstrate these skills, let's consider the scenario where you have a child who is in the middle of a temper tantrum and demanding to stay up past her bedtime.

First, describe the problem you see to your child. Don't point fingers or accuse, just explain the problem. This will help her understand better.

For example, say, "When you stay up late, you feel tired in the morning."

Then offer some information as to why her behavior might not be helping, such as "When you're tired you don't have energy and you find it hard to concentrate at school."

Explaining these points helps children to figure out for themselves what needs to be done. With this information, you can give your kids the power to find out for themselves what is good for them.

By communicating this way, you'll usually find your child ready to listen to you, without any need for either party to blow a fuse.

But what if you think the only way to get your child's attention is to punish her?

> _"Children dislike hearing lectures, sermons, and long explanations. For them, the shorter the reminder, the better."_

### 4. Don’t punish your children – come to a compromise. 

Sometimes trying to get your child to do something you want is like banging your head against a brick wall. He simply refuses to listen or he rebels _against y_ our advice. These situations can be so infuriating that it feels like the only choice we have is to punish him for his repeated offences!

Yet punishment only leads to anger, and it actually hinders future progress.

When you punish a child, you encourage him to feel bitterness toward you, making him far less likely to heed your advice in the future. Punishment also stops your child learning _why_ what they did is wrong.

Say your older child stays out too late for the third night in a row during the school week. You decide to punish her with a grounding. But stopping her going out will make her feel angry and misunderstood. She won't see how staying out late is a bad idea, she'll just see that you don't approve of it.

Try another approach.

First, have a dialogue with your child about her feelings and needs — ask her why she came back late and why she feels mistreated if she has to stay home as a consequence.

Then offer up your own feelings and needs, explaining why you feel concerned if she comes home long after her curfew, e.g., worrying that she might be in danger.

Then brainstorm together to find some mutually acceptable solutions and note them all down, without evaluation. Perhaps you come up with the idea that one day there's no curfew at all, or that she has to text you to explain when she will be late and why.

Finally, come to an agreement on which suggestions you both like and try them out. Don't try to force your favorite ideas on your child; the goal is to come to a mutual agreement.

Steering away from punishment is a huge leap in the right direction to helping your child think independently about her behavior.

### 5. Let your kids become independent by allowing them to act and discover things on their own. 

Kids must eventually grow up and become self-sufficient, responsible adults. We all know this, so why does it hurt when you need to let them go? How will they know when to come home? Or what's appropriate to say or do or wear in the outside world?

Sure, your child is not supposed to become a replica of you, but even so, watching him struggle to make his own choices isn't exactly comfortable. Think about watching your child wrestling with his shoelaces — it's so much easier to just bend down and do it for him!

The problem is, when your child continually depends on you, this dependency increases, along with a feeling of helplessness or even worthlessness. Taken to the extreme, this can lead to hostility and stubbornness, with your child feeling infantilized, frustrated, and like he's not allowed to be an individual with feelings.

You need to encourage your child's autonomy. But how? You can try the following ideas.

Let your children make their own choices. If the problem is homework versus free time, let your child design her own plan for coordinating both activities. Of course, homework has to be completed, but your child can decide how and when to do it.

You can also encourage your child to seek advice outside the home, such as asking his friends or teachers.

Most importantly, never lose hope. Your child will struggle from time to time, so it's best to make peace with this. Remember, each child deserves the chance to seek out and explore answers for herself.

Even if you feel stressed that your child isn't doing what she ought to and you're dying to swoop in and save her — hold back!

Giving your child a little slack and letting her explore will set her well on the way to autonomy.

> _"By trying to protect children from disappointment, we protect them from hoping, striving, dreaming, and sometimes from achieving their dreams."_

### 6. If you want your child to grow and learn from his mistakes, offer helpful praise and never use labels. 

We've explored better communication and fostering independence, but there is one final step: Learning to praise and talk about your child's behavior.

Praising is a cinch, right? Your kid has done great, so just say it! But praise is also needed to develop self-esteem, so it can actually get a little tricky. Well-intentioned praise can sometimes evoke unexpected reactions such as anxiety or denial, or it may even be seen as manipulative. You should therefore be able to praise in the most helpful way possible.

Helpful praise is twofold: describe what your kid has done and then allow her to praise herself according to that description.

Instead of saying, "You wrote a lovely poem," say "Your poem really moved me, I especially love the second line." If it is easier, you can add words that summarize your kid's behavior, such as stating "That's what I call creativity!"

Be careful though, as praise should not hint at past weaknesses or failures. Saying, "Your last poem was really bad, but this is better!" will not help the child reap all the good feelings of his praiseworthy behavior. Also, use praise selectively to be sure it retains its value.

In addition to praising good behavior, we should avoid labelling bad behavior. Often kids are labelled as stubborn or bossy, and the label becomes hard to drop later on. Even worse, many kids adapt to their labels and start behaving accordingly, becoming convinced that they're stubborn or a slow learner, and so on. So unless you want your child to always live up to the "lazy" or "bossy" role you've given him, don't label him.

The same goes for you, too. You shouldn't cast yourself in roles such as the "bad" or "authoritarian" parent. In fact, it's best to steer clear of labels altogether.

### 7. Final summary 

The key message in this book:

**Rather than blaming your kids for all your parenting grief, you can improve communication with them by making a few changes to the way you speak to them and set the tone of a situation. Listening, sharing feelings and respecting your kids will make your job as a parent far easier, improve your children's behavior and help them become more independent.**

Actionable advice:

**Show your feelings.**

The next time you are furious about your kid coming back home late, stop and pause for a minute. Rather than grounding or punishing him, share your feelings with him about it. You might be surprised at the result.

**Value your child's opinion.**

If you find yourself matter-of-factly telling your kid how things should be — for example, that she "has" to wear a raincoat when it rains — stop and listen to your child. Let her share her opinion and look for a solution that can satisfy you both.

**Suggested** **further** **reading:** ** _All Joy and no Fun_** **by Jennifer Senior**

_All Joy and No Fun_ (2014) is a book about the trials and tribulations of raising kids. Senior examines the challenges of parenting while keeping us cognizant of the pleasures and rewards that come with it.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Adele Faber and Elaine Mazlish

Adele Faber and Elaine Mazlish are internationally recognized experts on communication between adults and children. Having sold over three million copies of their books _How to Talk So Kids Will Listen & Listen So Kids Will Talk _and _Siblings Without Rivalry,_ they have given talks internationally and appeared on renowned TV shows such as _Oprah_ and _Good Morning America_.

