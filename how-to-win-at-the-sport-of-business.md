---
id: 562497906361310007020000
slug: how-to-win-at-the-sport-of-business-en
published_date: 2015-10-22T00:00:00.000+00:00
author: Mark Cuban
title: How to Win at the Sport of Business
subtitle: If I Can Do It, You Can Do It
main_color: 246AB2
text_color: 1F5B99
---

# How to Win at the Sport of Business

_If I Can Do It, You Can Do It_

**Mark Cuban**

In _How to Win at the Sport of Business_ (2011), Mark Cuban offers unconventional advice and wisdom on how to work your way up in the business world. Comprising a collection of posts from Cuban's renowned blog, the book tells the story of Cuban's own unconventional rise to success — a story that's sure to inspire anyone willing to think a little bit differently.

---
### 1. What’s in it for me? Learn how to win the game of business. 

Have you heard of Mark Cuban? 

He's one of those guys who moved to Dallas, Texas for the sunshine and the women. But he is also one of those extraordinary characters who took a little and turned it into a lot. Specifically, he is the founder of _MicroSolutions_ and _Broadcast.com_, which he turned into multi-million dollar businesses, and also happens to be the owner of an NBA basketball team. Want to know how he did it?

In _How to Win at the Sport of Business_, Mark Cuban serves up his knowledge and personal experience from the business world on a silver platter. He also offers priceless tips on how to make your own business prosper and become a great success.

In these blinks, you'll discover

  * why whining is a good thing after all;

  * how Mark Cuban turned his passion for basketball into a business; and

  * how to get ahead of your competitors.

### 2. Mark Cuban worked his way up from sleeping on floors to owning his own company. 

When Mark Cuban moved to Dallas at the age of 24, he was looking for three things: sunshine, job opportunities and women. He had a broken-down car and a few polyester suits, and started off sleeping on the floor of an acquaintance's tiny apartment.

Cuban had several jobs, which he knew would help his career because they all represented opportunities to learn. While many people assume they need to find the perfect job right after college and commit to it, that's not necessarily the best way to start your career. Rather, it's better to build and develop a variety of skills first. 

As a young man fresh out of college, Cuban spent a few months at _Mellon Bank_, where he learned about computer systems. As a budding entrepreneur, he also endeavored to improve the company without consulting his boss first — which got him fired. 

After moving to Dallas, he got another job working with software. He bought himself a computer for $99 and started teaching himself programming. He devoted his time to this new pursuit rather than returning to university, as it was an opportunity to get paid while learning. 

When working a number of temporary jobs, there's another valuable lesson to be learned: other people's methods won't always be best for you. 

Working a variety of jobs helps you learn about yourself, your skills and your ambitions, but it can also teach you a lot about where businesses or bosses go wrong. 

When Cuban found that his bosses weren't open to their employees' ideas, he decided that he simply couldn't work there any longer. So he left and founded his own company, MicroSolutions, which sold PC software and configured computers. 

Microsolutions had its share of problems — at one point, a secretary stole nearly all the company's money — but Cuban was able to grow it into a strong company that eventually brought in revenue of over $30 million.

### 3. Cuban started out as a small business owner and later branched out into a field he loved. 

A few years after Cuban had started MicroSolutions, he sold it for $6 million. 

When you're starting a small business, it sometimes feels like it will stay small forever. But as Cuban showed, any kind of business can end up making it big. 

After MicroSolutions, Cuban founded AudioNet, which he later renamed _Broadcast.com_. It was an internet radio platform that allowed people to tune into NBA and NFL games online. Yahoo eventually became interested in the company and, in 1999, Cuban and his partner Todd Wagner sold it to them for $5.9 billion. 

Like Cuban, you can also reinvest the money you earn from your first success into a new business that's more in line with your personal interests. If you're passionate about something and have the money, why not find a way to invest in it?

Cuban wanted to make his mark on basketball, so after he sold Broadcast.com, he used the money to buy the Dallas Mavericks of the NBA. 

Next, he decided to make some changes. Cuban had learned about the importance of customer satisfaction during his early years in Dallas, so he started selling tickets to Mavericks' games for as little as $2. He went to the team's games often but he didn't sit in the front row; he wanted to sit among the regular fans, so he could have the same experience as his customers. 

Cuban's success relied on his brilliant entrepreneurial skills and good sense of timing. The next blinks will look at his most important business mantras and the lessons he learned while achieving his success.

### 4. Never stop looking for ways to improve yourself and your company. 

Cuban learned some of his most valuable lessons in his early adulthood, while working day and night to grow MicroSolutions into a successful business. 

One of the most important lessons he learned was to never stop learning. 

Take any opportunity you can to educate yourself. One example is simply to read, and to read about anything you're interested in. Most people don't take advantage of the vast array of reading material that we have instant access to, so follow a different path and read as much as you can. 

Cuban spent all his time studying, while other people were watching TV or out having fun. He read about programming, famous entrepreneurs and studied software manuals; he did everything he could to get ahead of everyone else. 

Business is a competition. If you want to win, you have to preempt your competitors, so keep your head in the game. Never stop trying to gain an edge on them. 

After all, there will always be someone trying to put you out of business. Imagine that you're one of your competitors, looking for any possible way to take your business out. What weaknesses in your company could a competitor take advantage of? Identify those weaknesses before someone else does!

You should also run your company as if you're going to compete with the best. If you're aiming to compete with Google, start acting like it — it will prime you to deal with less dangerous competitors. 

In business, you need to have an edge. You need the will to win and to be the smartest and most prepared person in the room. You have to be willing to sacrifice long hours while maintaining a positive attitude and treating the people around you well. 

The game of business never ends, either. As long as you're working, you're competing. Don't ever settle and think you've won, because there's always another company out to get you!

> _"That's what success is all about. It's about the edge."_

### 5. Put in as much effort as you possibly can. 

It's not humanly possible to control every aspect of your business. But if you want to make it, you'll have to work a lot. 

In fact, you should be prepared to work day and night toward achieving your goals. There are a lot of unpredictable variables in sports and business alike, but there is one thing you always have power over: your own effort. 

In business as in sports, you have to commit to staying up late every day, training for long hours and educating yourself. Work with customers and potential customers during the day, and prepare for the upcoming days at night. 

That's exactly how Cuban made MicroSolutions so successful. Even though he works with the Mavericks now, he still reminds himself every day to work hard and build on his success. 

A good work ethic isn't all you need, however. You also have to decide which opportunities to take, because you can't take all of them. 

More and more opportunities will start popping up as your company gets rolling. Don't make the mistake of going for them all — it will distract you from your core goals and will probably end up hurting your business. 

For example, there had been talk within the NBA of expanding the league to the international level, but Cuban said no. He knew that doing so would distract him and his team from focusing on where they were at that moment, and what they had to do to improve. Never lose sight of what you really want. 

So, achieving success is about sticking to your goals and working hard. But if you want to get ahead in business there's another crucial thing you need: money.

### 6. Pace yourself and never forget about your customers. 

When you're a small business owner struggling with money, it's tempting to look to big investors for funding. Even so, taking money from major investors can actually harm you and your business. 

It's likely that a venture capitalist will be able to help you out with financial matters in the beginning, but there's a good reason why they're often called _vulture_ investors. If someone pays for your company, you're under their control; if you can't deliver on what you promised, you'll be held responsible. It's even possible for you to be replaced within your own business. 

Instead of relying on big investors, learn how to be okay with slow growth. You don't have to establish your business quickly — in fact, it's sometimes better to grow slowly and build on your work with just a few employees. 

MicroSolutions was initially formed just by Cuban and his friend Scott. The two learned about software and dedicated most of their time to finding customers and making a name for themselves. The company grew slowly. 

When your business _does_ start to grow, don't lose sight of the most important part: your customers. It's tempting to focus on growth instead, but remember that keeping your customers happy is always the most important aspect of any business. Even if you've got the greatest idea in the world, don't forget that your customers are the ones who will bring you your success. 

Cuban always wanted to be informed whenever his companies received customer complaints. He would try to respond to them personally, even after becoming a successful billionaire; he wanted his businesses to stay customer-focused. 

In the last blink, we'll look at the importance of sales and loving what you do — and learn how whining can actually be an asset!

> _"Investors don't care about your dreams and goals."_

### 7. Some skills are crucial for making progress – and whining is one of them. 

If someone calls you a whiner, you might feel insulted or embarrassed. But you shouldn't! Whining isn't always a bad thing. 

In fact, whining can help you. It draws attention to things that are wrong and motivates you to address problems. 

If you feel the need to whine, it means you're facing some problems or challenges. When you let yourself whine, you'll be motivated to fix those problems; otherwise, you'll be stuck in your situation and will whine even more!

That's exactly why Mark Cuban loves to whine. When he saw the opening night of the Mavericks' 1999 season, for instance, he complained about the general lack of energy and fun in the building. He figured he could create a better atmosphere, so he bought the Mavericks himself. 

But, of course, you have to do more than just whine. You and your team still need to love the work you do and be able to sell your product. 

It's not enough for just _you_ to love what you do — everyone who's involved needs to love it as well. You and your team won't have the stamina to push through all the hard work ahead of you if you aren't genuinely passionate about what you're doing. Without passion, you'll just run out of steam. 

For this very reason, you should only hire people who are as motivated as you are. Seek out team members who are dedicated to growth and progress. 

Cuban hired Scott to work for MicroSolutions because he was equally passionate about growing the company. Scott even quit his previous job so he could fully commit to MicroSolutions, and spent days learning about different PCs and how to install various kinds of software. 

All in all, Cuban's story is a tale of dedication and staying true to oneself. If Mark Cuban can find his own way in the sport of business, you can too.

> _"Whining is the first step toward change."_

### 8. Final summary 

The key message in this book:

**Anyone can turn their dreams into real success, but it takes a lot of hard work. Stay dedicated, even if it means sleeping on a friend's floor. Shoot for something realistic before breaking into your dream field, but never stop trying to get better and remember to pace yourself. Don't forget that customers are the most important part of any business and, when the time is right, feel free to whine. If Mark Cuban did it, so can you!**

Actionable advice:

**Always be prepared for unexpected opportunities.**

You never know when a great opportunity will arise. So stay prepared — don't be afraid of something just because you weren't expecting it to happen. The game of business never takes a break; if you get a chance to do something great, don't let it slip away. 

**Suggested further reading:** ** _Zero_** **_to_** **_One_** **by Peter Thiel with Blake Masters**

_Zero_ _to_ _One_ explores how companies can better predict the future and take action to ensure that their startup is a success. The author enlivens the book's key takeaways with his own personal experiences.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Mark Cuban

Mark Cuban founded his first company, _MicroSolutions_, while living in a three-bedroom apartment with six people. He has since worked his way up in the business world to become the owner of the Dallas Mavericks NBA franchise, and one of the wealthiest entrepreneurs in history.

