---
id: 544d60ac3630630008000000
slug: how-we-got-to-now-en
published_date: 2014-10-28T00:00:00.000+00:00
author: Steven Johnson
title: How We Got to Now
subtitle: Six Innovations that Made the Modern World
main_color: 6CB2CE
text_color: 447082
---

# How We Got to Now

_Six Innovations that Made the Modern World_

**Steven Johnson**

_How We Got to Now_ reveals many of the hidden connections between innovations we take for granted in our modern world. It shows how innovations can have unexpected applications, and that the consequences of an innovation are almost impossible to predict. Ultimately, it illustrates how interconnected we are, as a single invention can have enormous repercussions worldwide.

---
### 1. What’s in it for me? Uncover the hidden connections of innovations in our modern world. 

Where do inventors get their brilliant ideas? Are the innovations that change our lives inevitable, or uniquely dependent on the genius of their creators?

Even more curious are the connections between different innovations. We might think that a new idea has a straightforward application: our iPod plays music, our ice tray makes ice. But in fact, inventions can have unintended and fascinating consequences that reverberate in our society and around the globe.

Our world is full of ideas that are far more interdependent on each other than we realize. In these blinks, you'll learn about these hidden connections, and the inventors who drove them.

in the following blinks, you'll also learn:

  * why one man's fishing trip in Canada led to a revolution in family planning;

  * how flash photography led to major social legislation in New York; and

  * how one woman invented software before anyone had even heard of the computer.

### 2. From aromatic flower to hovering hummingbird: Our world is more connected than you might think. 

Evolution offers a clear example of how, as living organisms, we are all interconnected. A change in one sort of plant may lead to a change in another kind of plant, through a process called _coevolution_.

Usually evolution is seen as a competitive process, where one organism survives at the expense of another, less "fit" organism. Yet organisms that coevolve have instead a symbiotic relationship, in which the changes in one organism benefit the other, and vice versa.

During the _Cretaceous Age_, some 145 million years ago, flowers evolved scents and colors to signal the presence of pollen to insects. Over time, insects' bodies changed to better extract the pollen from the flowers. In doing so, the insects pollinated the flowers, after which the flowers were able to produce energy-rich nectar, attracting the attention of ever larger organisms.

This symbiotic relationship between flowers and insects led to another remarkable evolutionary innovation: the hummingbird's wing.

Hummingbirds were attracted to the flowers' nectar, but to drink the nectar, they essentially would have to learn to hover, like a bee does. Early hummingbirds could not do this; yet gradually, the birds' wings evolved to a shape where lift was created with each downward and upward stroke. With this development, the hummingbird could then hover in mid-air, and drink nectar from a flower.

Thus the coevolution chain proceeded as such: from colorful flowers to well-equipped insects, to pollinated flowers to hungry, hovering hummingbirds!

In the past, no naturalist could have predicted such a path. Today we have a much better understanding of how evolution and coevolution connect all the world's life.

### 3. Long-zoom history sees historical change from a broader perspective, making concrete connections. 

History is seldom charted on a straight line; events overlap, narratives cross, perspectives are skewed depending on whether you're looking up close or taking a long view.

_Long-zoom history_ is one approach that takes a long view; it is used to explain historical change by looking at multiple events and settings, a method that considers "the bigger picture."

Consider technology company _Google_ and how it has revolutionized search online. If we focus only on the fact that Google's search engine is available for free, we might miss other consequences of the technology's influence. If we expand our focus to look at the company's larger societal effects, however, we see another picture.

When Google began selling advertisements alongside its free search results, for example, it sounded a death knell for paid advertising in local U.S. newspapers, many of which suffered a loss of revenue as a result.

By taking the long view, we can see how one small change in a company's business model can have significant effects for other seemingly unrelated industries.

But is this coincidence, or cause and effect? You may have heard of "the butterfly effect." It explains that a tiny change, such as the flap of a butterfly's wing, can set off a chain reaction of events that could lead to something as tremendous as a hurricane.

The butterfly effect however is different from long-zoom history, as it involves a long chain of events that is virtually unknowable. If you wanted, you could tap your foot and make the case that your tap is somehow related to an earthquake in China, even if you couldn't identify all the connections between.

Yet with a long-zoom view of an event like the evolution of a hummingbird's wing, we can concretely plot the links between flowering plants, nectar production, pollinating bees and hovering hummingbirds. The connections are concise, and we know that they exist.

### 4. Innovations can inspire other innovations often in wonderful, unexpected ways. 

On the surface, you might not think frozen fish and family planning have much to do with each other. Yet some innovations inspire other innovations, often in wonderful and unexpected ways.

It all started with a naturalist named Clarence Birdseye, who moved to Canada's northernmost province of Labrador in the early 1900s. While ice fishing with local Inuits, Birdseye made a startling discovery — an innovation that would radically change the way we eat.

He noticed that when he caught fish from a hole in the ice, the ambient temperature froze the fish almost instantly. When he thawed those same fish, he realized that the quickly frozen fish tasted far fresher than the frozen fish he consumed back home.

Thus the idea of _flash freezing_ was conceived, an idea that Birdseye went on to patent and from there, an entire industry around frozen food was born.

Yet this innovation didn't only change how we consume food; it also changed the way we procreate!

Flash freezing works great for meat and produce, and it also is an effective method to preserve human semen and eggs, through a process called _oocyte cryopreservation_. 

Oocyte cryopreservation, for example, allows women to store their healthy eggs when they're younger, to have biological children much later in life. Gay couples as well as single parents too now have more choices with regard to family planning, with access to sperm or egg banks.

When Birdseye made his discovery while enjoying a tasty fish dinner far in Canada's frozen north, he probably had no idea that it would lead to a revolution in human procreation and family planning.

To be sure, innovations can have repercussions that are completely unpredictable.

### 5. An innovation may not be the direct cause of change, but it can foster an environment of change. 

While we know tastier frozen trout didn't directly lead to the possibility of artificial insemination, the question remains: would we have come up with artificial insemination without frozen fish?

The relationship between an innovation and the social change it might inspire isn't always direct, but it can still be crucial.

The period we call the Renaissance, between the fourteenth and seventeenth centuries, was a cultural movement characterized by a shift toward self-awareness and individualism in art and politics. The ideas born from the Renaissance revolutionized Europe, if not the entire world.

Yet an interesting actor may have played a role in helping the Renaissance come about: the mirror.

Before the mirror was invented in the 1400s, the concept of the artist self-portrait did not exist. The mirror also helped in the discovery and application of linear perspective, which helped artists create more realistic-looking paintings and drawings.

Another innovation born of the Renaissance was the creation of the novel, as well as stories told through first-person narration, giving literary works a more personal, reflective bent.

Taken together, these changes pointed to a society becoming more self-reflective, with an increased emphasis on the individual and his or her place in the world.

Yet how much of this change can be credited to the mirror's influence?

While the mirror wasn't a direct cause, it may have helped create the conditions that allowed a change in thinking to come about. When people for the first time could literally look at themselves, they began to also _figuratively_ look into themselves.

So we can imagine a world _with_ mirrors and _without_ self-portraits, but not the other way around.

Mirrors led to self-portraits, which echoed the political and artistic currents of the time. It would be an oversimplification to say that mirrors drove the Renaissance, but mirrors did help provide the conditions necessary for Renaissance ideas to flourish.

### 6. From light bulbs to social legislation: Some innovations can provide light in dark times. 

Certain innovations have provided us with light in dark times — sometimes literally, in the case of the invention of the candle.

Candles were initially made from a waxy substance known as _spermaceti_, found in the skulls of sperm whales. Whale hunting was a dangerous pursuit, and candles were a rare luxury and highly valued. The demand for candles, in fact, almost led to the extinction of sperm whales.

The innovation of artificial light with the light bulb didn't only save the whales but also led to an increase in overall literacy, as better lighting allowed people to read more in the evening hours.

But the chain of innovations didn't stop there. The invention of the light bulb led to flash photography, which inspired one of the greater social reforms in the United States.

Astronomer and innovator Charles Smyth first experimented with flash photography in the late 1800s while visiting the Great Pyramids in Giza. He mixed magnesium with gunpowder to create a mini-explosion to illuminate the vast King's Chamber, so that it could be photographed.

Later, journalist Jacob Riis learned about flash photography and used it to chronicle the terrible living conditions in the _Five Points_ neighborhood in New York.

While he had already written extensively about the squalor in the neighborhood, the area's dank, dark corners and alleyways made it almost impossible to capture them visually. Using flash photography, Riis captured dozens of images that when released caused an uproar over the government's neglect of the region.

A couple years after Riis's photos were published, the _New York State Tenement House Act of 1901_ was passed with overwhelming support. It effectively eliminated the terrible conditions people in the Five Points had had to endure.

So we've seen how one man's experimentation with flash photography led to positive social change on the other side of the globe. Yet sometimes, an innovation can have the opposite effect.

### 7. Even inventions driven by good intentions can sometimes be used toward unsavory ends. 

Many inventors are motivated by the idea of making the world a better place. Unfortunately even best intentions can have unintended negative repercussions.

After the sinking of the cruise ship _Titanic_ in 1912, Reginald Fessenden, motivated by the tragedy, wanted to make sure it never happened again. A pioneer in wireless radio, Fessenden used his knowledge of sound engineering to develop a new technology called _sonar_.

Sonar works by echolocation. A ship sends out a sonic pulse, which when it bounces off any object, creates an echo. Fessenden's innovation could have saved the _Titanic_, as the ship would have been able to locate the iceberg it struck ahead of time, and move to avoid it.

Sonar has had many applications, being used to map the sea depths, even locate the sunken _Titanic_ itself! However it also has had other applications that have resulted in more serious consequences.

Sonar technology led to the development of _ultrasound_. Ultrasound is commonly used to produce an image of a fetus while in utero. We use ultrasound to determine whether a baby's growth is progressing normally, and even determine the sex of a baby before it is born.

This technology however has been used to devastating effect in places like China, where parents have a strong preference for baby boys. After ultrasound technology was introduced in China, the practice of sex-selective abortions began and rose rapidly.

The Chinese government officially banned this practice in the 1980s, but by 1990, China still reported sex ratios at birth to be as disparate as 118:100, in favor of boys.

Although the invention of sonar was driven by a sincere desire to avoid tragedy, inventors can't know or control what effects their inspiration will have in the future. The bigger picture is never completely clear.

### 8. Some inventions come about as a result of an inventor’s richly diverse background and skills. 

Some technological advances seem inevitable, while others less so. Yet how can we explain how one woman could be so ahead of the curve to write computer code in the nineteenth century?

While the invention of the light bulb is often attributed to the singular genius of Thomas Edison, the reality is he wasn't the only one with the idea at the time. In fact, prominent innovations often appear simultaneously in clusters all over the world.

Not only did Edison collaborate with a team of fellow inventors, but at least _20 people_ from other parts of the world had independently almost perfected artificial light, and half had working prototypes similar to Edison's.

This tells us two things. First, there was a clear, global interest in producing artificial light. Second, the technical knowledge people had at the time was exactly what was needed to produce the light bulb.

But how can we explain inventions that seem to come from nowhere, like computer code in the 1800s?

Ada Lovelace made a conceptual jump when she formulated the first computer algorithm in 1842. Lovelace was the daughter of Lord Byron, a creative, eccentric man who eventually went insane. Her mother encouraged Ada to devote herself to mathematics, to avoid her father's fate.

Lovelace went on to help Charles Babbage envision and invent the first general computing machine. Her contribution to the project is held to be the first example of working software ever published.

Many people think Lovelace's extraordinary ability was partly due to her upbringing, shaped by her father's romantic poetry. Even though math — Lovelace's life pursuit — seems the opposite of poetry because of its logic and clarity, she approached math like a poet, creatively and artistically.

Innovations like Lovelace's don't occur because they're inevitable but because of the diversity of an inventor's experience and skills.

### 9. Final summary 

The key message in this book:

**Our world is an intimately connected place. An innovation in one part of the world could have far-reaching consequences in places we could never imagine. While it's difficult to judge the reach of any invention in its own time, keeping a long view of historical change will help us see the patterns and relationships between innovations.**

**Suggested** **further** **reading:** ** _Where Good Ideas Come From_** **by Steven Johnson**

_Where Good Ideas Come From_ examines the evolution of life on earth and the history of science. These blinks highlight many parallels between the two, ranging from carbon atoms forming the very first building blocks of life to cities and the World Wide Web fostering great innovations and discoveries. In addition to presenting this extensive analysis, replete with anecdotes and scientific evidence, Johnson also considers how individual and organizational creativity can be cultivated.
---

### Steven Johnson

Educated at Columbia University, Steven Johnson is an author as well as host and co-creator of the _BBC One_ series, _How We Got to Now_, which is based on this book.

