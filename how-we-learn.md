---
id: 543b8dec36623900081c0000
slug: how-we-learn-en
published_date: 2014-10-15T00:00:00.000+00:00
author: Benedict Carey
title: How We Learn
subtitle: The Surprising Truth About When, Where and Why It Happens
main_color: FFD73D
text_color: 806C1E
---

# How We Learn

_The Surprising Truth About When, Where and Why It Happens_

**Benedict Carey**

_How We Learn_ explains the fascinating mechanisms in our minds that form and hold memories, and shows how with this information, we can better absorb and retain information. You'll explore the many functions of the brain and gain practical advice on how to better study and learn.

---
### 1. What’s in it for me? Discover how your mind works to optimize your study sessions. 

Whether you're a student or a professor, a novice or an expert, there is always more to learn.

Often success or failure depends on how well we retain information. A novice pilot can't let his training go in one ear and out the other: lives depend on the quality of his learning!

These blinks will help you get the most out of your studying and training. You'll discover how the brain actually forms memories, and then use this information to develop practical routines to make sure that when you study, you remember what you've learned.

If you're a university student hungry to raise your GPA, or perhaps a retiree keen on expanding your crossword vocabulary, learning how your brain works is a crucial step to mastering any task you put your mind to!

In the following blinks, you'll also learn:

  * how to remember someone's name at a party;

  * why listening to the right kind of music can boost your memory; and

  * how you can quickly become an expert at identifying skin rashes.

### 2. Memories are formed through the connection of cells and are stored in specific areas of the brain. 

To understand the most effective studying and learning methods, we must first understand the basics about the brain. How does it create memories? And how do we retrieve them?

Memories are created through the process of connecting different _neurons_, or cells which send signals within the brain to transmit information.

A memory, such as your first day at school, is created when neurons are stimulated and then form a network of many connected neurons, called _synapses_.

Each time we retrieve a specific memory, synapses essentially grow thicker. In other words, having thicker synapses means our recall of that memory or information stored in that network is faster and clearer.

But memories aren't stored all in the same place, forming one huge knot of synapses. In fact, different types of memory form in different areas of the brain.

The area of the brain that forms new, conscious memories, such as the name of the person you've just met, is called the _hippocampus_.

Fascinatingly, people whose hippocampus has been removed or destroyed are still able to retrieve older memories, which shows us that older memories are stored somewhere else: in a region called the _neocortex_. This area of the brain is divided further, into areas that control how we move or how we process what we see.

When you think about your first day of school, for example, your brain "looks" for where that sensory information is stored. If you remember clearly the dingy green color of the school hallway, then this memory would be stored in neurons located in the visual processing section of your neocortex.

So, if a memory includes lots of different stimuli — colors or smells or textures — stored by many neuronal networks in different regions of the brain, you can understand why you can remember these memories more clearly: more connections in more places means better recall.

### 3. Getting a good night’s sleep is crucial to remembering and retaining things you’ve learned. 

If you're a busy person, you might feel that sleeping just gets in the way of getting things done. But this is completely wrong!

Getting enough sleep is essential for your brain to form and consolidate new memories. While we still don't completely understand how sleep affects our bodies and our brains exactly, research suggests that sleep can help us better understand and memorize information.

In one study, participants were separated into two groups and given a memory task. Each group was shown pairs of different colored eggs, each of which was assigned a certain rank. The participants were then tested to see how much of the ranking they remembered.

However, there was one major difference between each group: participants in one group slept before taking the test, and the other group's participants did not.

The results showed that the group that slept performed far better: they remembered 93 percent of the rankings, whereas the sleepless group only remembered 69 percent.

Sleep is important. But, it's actually more complicated, in that different stages of sleep are _more_ important depending on the kinds of tasks you need to perform.

You don't actually sleep in the same manner throughout the night. So if you have an exam tomorrow and still need time to study, it's important to understand whether staying up late to cram or getting up early is more effective.

Your first hours of sleep in the early evening are important for retaining facts. If you're studying vocabulary words, it's better to go to bed early. However, if you need to think creatively, then it's more effective to stay up and study at night.

Creative thinking requires _rapid eye movement (REM) sleep_, which happens mostly in the early morning hours. So it's OK to stay up late and study as long as you get a couple of hours of sleep before the sun comes up!

Now that you understand how the brain stores and retrieves memories, the next blinks will look at practical ways to learn faster and more effectively.

> _"In a fundamental sense, sleeping is learning."_

### 4. Don’t always use the same study routine, as variation can help you remember things better. 

In school, a teacher may have suggested that you set aside a certain time of day where you could study in solitude, free from distraction. However, distractions aren't the only things that influence our ability to learn.

It _is_ true that our surroundings during a study session generally affect our ability to remember what we've learned. Your brain picks up on all kinds of environmental cues while you study — a room's musty smell, your chair's hard, uncomfortable seat, and so on.

These things aren't just idle observations! They are actually hints that can help us remember and retrieve the information that we've learned during our studies.

This was verified in an experiment where participants studied a list of words, while different types of music played in the room. When tested, the participants were able to retrieve _twice_ as many words when the same kind of music was playing than when there was a different kind or no music at all.

The results of this experiment show how environmental cues actually helped the participants' brains retrieve the information stored while studying when those same cues were present.

Using this information, consider changing your study environment to ensure that you are able to retrieve information in any situation.

As you probably won't be able to recreate your study environment while taking an exam, you could instead change your study routine. You could alternate between taking notes on a computer and by hand. Or you could study outside one day, and in your kitchen the next.

These seemingly trivial variations ensure that the information you learn is stored in different parts of your brain, which, as we've seen in previous blinks, increases retention.

> _"We need to handle life's pop quizzes...and the traditional advice to establish a strict practice routine is no way to do so."_

### 5. Cram today, forget tomorrow: to remember information for the long-term, study in intervals. 

Are you someone who crams in all the information you need to learn a day before an exam? Does this strategy actually work for you?

It might be effective for the test, but you probably won't remember the material for much longer. In fact, only by breaking up your study time can you be sure that you will retain information over a long period of time.

This is called the _spacing effect. S_ tudying the same fact over and over again in a short period of time doesn't result in effective memorization, as your brain essentially gets bored.

Think of it like this: if you are at a party and are introduced to a new neighbor, you'll try to remember his name. But try as you might, you probably will have forgotten his name by the time you leave, even if you've heard it repeated a few times.

However, if you hear your neighbor's name again a few days later — say if you overhear him introducing himself to another neighbor — then this "new" information will thicken the synapses between the neurons of your original memory, thus strengthening it and making the name stick.

So if you want to better retain information, you don't actually have to spend _more_ time studying, as long as you use the spacing effect.

For example, you have a test in two weeks and you plan to study for a total of nine hours. Rather than studying for nine hours the day before the test, a more effective strategy would be to study for three hours on three different days.

Following this study plan will help you increase your chances of remembering all the relevant information without spending _any_ extra time studying — you're simply distributing your time better!

> _"Mom was right; it is better to do a little today and a little tomorrow, rather than everything at once."_

### 6. Quizzing yourself and explaining what you’ve learned to others helps cement what you know. 

You probably know that when you have to explain a certain subject to someone, that process helps you better understand the subject yourself.

This is because reciting information is more effective for memory retention than simply reading information. If you want to get the most out of what you've learned, then you need to actively test your knowledge, rather than simply rereading passages from a book.

One way to do this is to explain the material to another person. When you put in that extra effort in explaining a certain subject, the connections between the neurons that store your knowledge will intensify, thus making the retrieval of information both easier and faster afterwards.

But what if you can't find a suitable audience for your knowledge? What then?

Testing yourself — even _before_ you know anything about a topic — can help you improve your understanding of the material later.

Say you have to take a multiple-choice test on a topic you know nothing about. You'd probably guess incorrectly most of the time! Even so, this process actually increases the chances that you'll be able to correctly answer a related question on a later test.

Psychologists at the University of California, Los Angeles had students answer a few questions about a topic they were going to learn about a few weeks later, and then gave them the correct answers to the questions after the test.

They then compared their earlier answers with their final exam results at the end of the semester. Surprisingly, students scored 10 percent higher on questions that were related to the ones they answered at the beginning of the term!

### 7. Interruptions don’t knock you off track; instead, they actually can help you learn better. 

Procrastination plagues almost everyone. We dawdle before a task, easily distracted, until we're _forced_ to hastily complete it. As you'll see, this habit is easily avoidable.

Some people believe that it's most efficient to start and finish a project right away; this way, it's done and they don't have to think about it anymore.

Yet completing a project over a longer period of time leaves room for interruption and is optimal for learning.

With more time, we can think freely, developing new ideas and committing what we've learned with our project to memory. What is the point of doing a project if you don't retain the results?

In a study, subjects were given a number of small assignments, such as completing a crossword puzzle. During the study, a researcher would occasionally interrupt the subjects so that they'd be unable to finish some of the tasks.

Afterwards, the researcher asked the subjects to list all the assignments they performed, and found that they best remembered those tasks that they were unable to complete.

So not only will you remember your projects better if you allow for interruptions, interrupted projects will also stay in your memory and possibly lead to new ideas.

Furthermore, when you're trying to solve a problem, breaks can help you to see the problem from a new perspective. That's because interruptions allow you to let go of fixed assumptions and perspectives, and view the project as with "new eyes."

So if you've been working on a math problem for a long time, it's important to take a break. Go for a walk, and try again later. You'll soon realize that you often come up with solutions to your problems when you aren't even thinking about them — that your insights seem to come out of nowhere.

### 8. Variety is the spice of memory! Don’t focus on only one skill; practice different skills at once. 

If you've ever taken tennis lessons, then your coach has probably drilled the importance of repetition: to master a backhand drive, practice it again and again — and again.

But is this really the most effective way to learn? In short: no.

Research shows that in fact, varied repetition is more effective than a narrow focus.

In one study, researchers separated children into two groups to learn how to toss a beanbag while blindfolded. The first group practiced only with one target that was three feet away; the other group had two targets, at two feet and four feet away.

After six training sessions, the two groups competed to hit a single target at a distance of three feet.

Surprisingly, the second group performed better, even though they'd _never_ practiced with the three-foot target. Why? Because the variation in their training sessions caused them to develop a more general "beanbag-tossing" ability, which could be applied to any target distance.

The advantage of adding variation to your training is that it better prepares you for unexpected situations. Variations require you to put in more thought and effort, which then intensifies the gains made through repetition.

So how can you apply varied repetition to your school work?Try varying how you study, even from hour to hour. That will increase the likelihood that you'll be able to use the skill in another context.

For example, if you're studying geometry, instead of just reading about the Pythagorean theorem, make sure you also practice the many different ways that it can be applied.

This way you'll be better equipped to manage an exam with unfamiliar questions that require you to use the theorem in different situations.

> _"Mixed-up practice…helps prepare us for life's curveballs, literal and figurative."_

### 9. Perceptual intuition helps us separate important facts from the noise of information around us. 

How does a baseball player decide so quickly whether to swing at a fast ball?

There is so much information to take in at once: how high is the ball? How fast is it traveling? Is it curving? The secret behind this ability is called _perceptual learning._

Perceptual learning, which develops _perceptual intuition_, is all about making sound "snap judgments." It's about developing the ability to successfully respond to our environment, seeking out only the most important signals and ignoring the rest.

While we're not born with this skill, we can become experts with enough time and training.

Novice pilots are often overwhelmed by the huge array of dials and instruments inside an airplane's cockpit. Experienced pilots, on the other hand, have become experts in visual perception, and therefore only need one quick look to discern what the instruments are telling them.

Because an experienced pilot has spent so much time in the cockpit, he's had plenty of time to master perceptual intuition. He already knows how the instruments are configured, what they mean and importantly, what to pay attention to.

We too can learn to differentiate and filter out unnecessary information from what we need to know, thereby building perceptual intuition with enough practice.

To do so, we can use _perceptual learning modules_. These are pictures or short videos that students use to help them hone their ability to make quick judgments from a particular set of stimuli.

In one study, medical students were shown pictures of different skin rashes (which look indistinguishable to a non-expert eye) and were then asked to quickly decide which sort of lesion or rash was depicted.

These snap decisions made the students "feel" the right answer, and they eventually became experts at intuitively identifying dermatological problems.

By applying perceptual learning models to your own studies, you too can start building your perceptual intuition.

### 10. Final summary 

The key message in this book:

**To optimize your studies, you need to have a good understanding of how your brain works, how it processes and stores information. Using this knowledge, you can devise more effective routines based on what you're studying and how you need to use the information you've learned.**

**Suggested** **further** **reading:** ** _Moonwalking_** **_with_** **_Einstein_** **by Joshua** **Foer**

_Moonwalking_ _with_ _Einstein_ takes us on the author's journey towards becoming the USA Memory Champion. Along the way he explains why an extraordinary memory isn't just available to a select few people but to all of us. The book explores how memory works, why we're worse at remembering than our ancestors, and explains specific techniques for improving your own memory.
---

### Benedict Carey

Benedict Carey is a science reporter for _The New York Times_ and has penned a number of books, including _Poison Most Vial_ and _The Unknowns_.

