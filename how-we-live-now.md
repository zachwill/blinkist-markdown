---
id: 577fb63bc088090003fe9eec
slug: how-we-live-now-en
published_date: 2016-07-14T00:00:00.000+00:00
author: Bella DePaulo
title: How We Live Now
subtitle: Redefining Home and Family in the 21st Century
main_color: F8943E
text_color: 915724
---

# How We Live Now

_Redefining Home and Family in the 21st Century_

**Bella DePaulo**

In _How We Live Now_ (2016), you're taken on a virtual trip across the United States to explore the different ways in which Americans create homes for themselves, their families and friends. These blinks reveal the latest trends in communal living as well as the forces driving people to create new, fascinating ways to live.

---
### 1. What’s in it for me? Take a peek into the households of America and learn about the modern family. 

The way we form families and live together has changed over history. Yet recently, there has been a seismic shift in how American families live and create communities.

We hear plenty about the "death" of the traditional nuclear family. Is this really true? And if it is, what exactly is taking its place?

The short answer is that there has never really been a "traditional" family; how we live together has always changed and morphed with the times. Americans today are exploring many community options, some based on modern ideas and some reaching back a hundred years.

In these blinks, you'll find out

  * why millennials prefer to cohabitate with parents rather than rebel;

  * how the internet is helping single moms find other single-mom roommates; and

  * why living alone today is more "normal" than living with a partner.

### 2. Every person in society is seeking the right balance of sociability and solitude. 

When you hear the word "alone," you might immediately think, "lonely." Similarly, the words "quiet" and "peace" are often used interchangeably.

These two-word associations reflect two ways of thinking about being with oneself.

In fact, people vary greatly in their need for solitude and their enjoyment of the presence of others. While nearly every person requires some time alone, some describe this feeling as a desire, while others see it as a craving or need.

People who need solitude, for example, say they can only fully recharge and center themselves by being completely alone. Even if a good friend were reading quietly in a corner of the room, they say, part of their attention would always be on that person.

Other types of people describe being alone as "boring" and become sad when no one is around. Even when such people want to be alone, they still enjoy hearing the footsteps or voices of roommates amid their semi-solitude.

This brings us to one of the main ways you can manage solitude: by choosing the fashion in which you live, you regulate access to yourself and others. In selecting a living arrangement, you decide how much control you will have over the extent and depth you get to know others, and in turn, how well they get to know you.

For example, if you live in a co-housing community with a common meal room and kitchen, you'll have many opportunities to become friends with people around you. You can decide how close you get to others by choosing how often you enter common spaces.

And if you share a house with someone, it's inevitable that you'll run into each other — for example, in the morning before work or late at night when returning from a party, even if you would prefer not to!

Next, we'll look at different living arrangements people have created to strike the right balance between sociability and solitude. We'll start with those that provide the most togetherness and progress to those that cherish privacy.

> _"Every human is trying to solve the time together/time alone equation."_

### 3. With parents and children getting along better than ever, living together has great benefits. 

It's often said that the family as a social institution is losing its importance, but this simply isn't true.

Young people, in fact, are increasingly returning to their parents' homes or living there longer for financial reasons. Indeed, between 1980 and 2012, the number of 25- to 34-year-olds who lived with their parents doubled. Why did this happen?

One reason is that today, young people dedicate more time and money to pursuing higher education. In a 2011 study, half of multigenerational households said that living together gave another family member the chance to continue schooling or professional training that otherwise would not be possible.

But money isn't the only reason parents and children are living together longer. The millennial generation gets along well with its parents — in fact, better than any previous generation.

This clashes with a general perception that parents of millennials are "helicopter parents," hovering over their children; and that the millennial generation should be called "generation stuck," as they're unable to cut the ties to their parents. Such perceptions overlook how millennials just seem to enjoy spending more time with their parents than any other generation before them.

In 2008, a study that surveyed parents found that 86 percent had talked to their children during the last week, and 90 percent of those talks included some advice. In 1986, in contrast, those numbers were about 50 percent.

Choosing to live in a multigenerational household also has advantages for the youngest and oldest members of a family.

The support that parents give as they raise their children is more likely to be reciprocated, as children who stay longer at home often offer more care to elderly parents. But even a situation in which an adult with children chooses to live with parents has its advantages, too.

A study found that children raised in multigenerational households have as good or even better chances of success as do children from a traditional nuclear family. In fact, they were more likely to graduate from high school and enroll in college, and were less likely to smoke or drink.

> _"A 20-year-old in 2000 is more likely to have a living grandmother than a 20-year-old in 1900 was to have a living mother."_

### 4. By balancing community and autonomy, cohousing communities are a modern version of village life. 

Communal living is often associated with hippies, free love and rock 'n' roll. But other, perhaps less sexy aspects are also important, such as caring for the environment and sharing resources.

Today _cohousing communities_ across the United States are sprouting up, inspired equally by early "hippy" pioneers and modern environmental concerns.

A cohousing community is a small neighborhood, designed to foster connections between inhabitants. They typically consist of between 20 and 60 houses, unseparated by fences, arranged around an open, common space.

The total area of the community is small enough to make it easy for residents to get to know one another, yet big enough to provide stability and continuity. There's often a large common house in the middle of the community, in which residents can meet and share meals.

The common house generally includes a kitchen, where residents can take turns cooking, and common laundry facilities. Sometimes it even includes guest rooms, a library or a music rehearsal room.

Such common spaces free up rooms in individual homes and the sense of community also allows residents to share the burden of care, such as when someone needs a babysitter, a pet sitter or even becomes ill.

Common spaces are cared for collectively on so-called work days when all residents come together and address repairs or other needs. Yet most activities are voluntary, and relationships in the community are allowed to grow at their own pace.

Indeed, within the community, there is no common ideology or religion but simply a desire to share, support and live with a smaller environmental footprint. Each individual household manages its own finances, has its own kitchen and perhaps a private backyard.

In this way, all residents can seek out social contact as frequently as they need. But with walkways designed for serendipitous encounters, shared resources and generous social support, it's no wonder that such communities turn many residents into close friends.

Living close to other people can make many things easier, especially for parents with children. Our next blink will detail another unusual way to pool resources for handling child care.

> "_Community is born from social routine."_

### 5. The best possible parenting structure for a child is not necessarily a traditionally married couple. 

Whether it's due to the rise of feminism or the increasing acceptance of divorce, the number of births by single women has increased dramatically in recent decades.

In 1970, only 11 percent of all births were to single women, compared to more than 40 percent since 2008.

So how has the modern world adopted to this new form of motherhood? _CoAbode_ is one development, an online matching service for single mothers who want to share a house with another single mom.

Started as the idea of a single mom in 2001, CoAbode reached 70,000 registered users as of 2013.

Interested mothers fill out a profile on the site, which includes questions as to the sort of budget an applicant can offer the collective household. The site also encourages discussing details such as parenting philosophies and rules on television or junk food, as well as the role of the biological father.

When CoAbode finds a match, then both moms can pool resources and potentially afford a bigger home, helping each other with child care and income.

A similarly modern arrangement is a _parenting partnership_, in which a single man and single woman commit to raising a child but without being romantic partners.

People who make such a decision naturally face a lot of criticism, with many concerned for the well-being of the child. And yet there are no studies that suggest that children raised in this fashion might be in any way harmed.

Furthermore, the intensive discussions parenting partners have before deciding to birth or adopt a child are often more thoughtful and considerate than those of many conventional couples.

Younger generations, a demographic often more open to social change, interestingly hold values that already agree with those of parenting partners. A study found that 50 percent of young people feel that being a good parent is one of the most important life goals, whereas only 30 percent say the same about maintaining a successful marriage.

> In 2013, the state of California signed a law allowing a child to have more than two legal parents.

### 6. Living alone is more popular than ever, among people both young and old. 

An article posted to Buzzfeed in January 2014, "19 Reasons Living Alone is the Best," received over half a million views.

Why would this article be so popular?

The reason is simply that never before in history has it been as fashionable, easy and affordable to live alone.

In Western society, individualism is the norm and people in general value self-expression, autonomy and privacy. Young people are marrying less, and if they do get married, they do it much later. Instead, having one's own place has become a marker of maturity for these generations.

And with modern conveniences and online services, living alone has never been so easy. Indeed, the number of single households across the world has been steadily increasing. In the United States in 1950, some 10 percent of the population lived alone; today that number stands at 27 percent. Meanwhile, Sweden leads the world with some 49 percent of people living in single households.

The trend of living alone isn't only for young people. Seniors are also exploring a living arrangement called _interdependent living_. This involves older people maintaining their independence at home but becoming more integrated within a community.

Such a set-up enables seniors to avoid impersonal institutions and lead a more fulfilled life. Not only can elderly people live alone with some assistance but also they can help people in the community.

Hope Meadows, a community south of Chicago, is an inspiring example of how matching needs can be brought together. Seniors from all walks of life are asked to live in a neighborhood with foster families. Often these seniors become surrogate grandparents for children that the foster care system refers to as "unadoptables."

Seniors receive health care and commit to working six hours a week. Duties include playing with children, tutoring adolescents or driving children to sports practices. But often these seniors end up giving much more and in general, living happier and healthier lives in the process.

What's more, some 90 percent of the children who live in Hope Meadows end up remaining with their foster families.

This experiment has inspired many more communities. For example, another group brings together children from the juvenile justice system and Vietnam veterans.

Our modern lives are less predetermined and more connected than ever before, and living spaces such as Hope Meadows reflect this by crossing the boundaries of age, race, religion and gender.

### 7. Final summary 

The key message in this book:

**All over the United States and other western countries, people are rejecting conventional living arrangements and reinventing domestic spaces. Americans are trying to find a balance between sociability and solitude, and on this basis, are designing unique living arrangements. And in the process, they're becoming happier, too.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Reclaiming Conversation_** **by Sherry Turkle**

_Reclaiming Conversation_ (2015) reflects on how we interact with one another in our increasingly digitized world. Constant interruptions, leaving messages unanswered and lack of interest have all become the norm in a world rife with mobile devices and screens. But is this what we want? And if not, what can we do about it?
---

### Bella DePaulo

Bella dePaulo is a project scientist in social psychology at the University of California at Santa Barbara. She is the author of numerous articles that have appeared in publications such as _The_ _New York Times_, _The_ _Wall Street Journal_ and the _Washington Post_, among others. She received a doctorate from Harvard University in 1979.

