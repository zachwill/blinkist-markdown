---
id: 5421333d6365640008f10000
slug: how-will-you-measure-your-life-en
published_date: 2014-09-24T00:15:00.000+00:00
author: Clayton M. Christensen, James Allworth and Karen Dillon
title: How Will You Measure Your Life?
subtitle: Finding Fulfillment Using Lessons From Some of the World's Greatest Businesses
main_color: C22741
text_color: C22741
---

# How Will You Measure Your Life?

_Finding Fulfillment Using Lessons From Some of the World's Greatest Businesses_

**Clayton M. Christensen, James Allworth and Karen Dillon**

As a leading business expert and cancer survivor, Clayton M. Christensen provides you with his unique insight on how to lead a life that brings both professional success and genuine happiness. In _How Will You Measure Your Life?_, Christensen touches on diverse topics such as motivation and how you can harness it, what career strategy is the best for you, how to strengthen relationships with loved ones, and how to build a strong family culture.

---
### 1. What’s in it for me? Learn how to grow as a professional and as a person. 

Author Clayton M. Christensen is an expert in business innovation. Having spent years working alongside high achievers and in successful enterprises, some of which he himself founded, he came to realize that a measure of a person's life needs to be more than time spent at the office.

With insightful, insider examples from his many years in the corporate world, Christensen is an able guide for readers looking to achieve the career of their dreams. Yet as a family man and cancer survivor, Christensen can also speak from the heart when it comes to work/life balance.

In these blinks, you'll discover that motivation, not money, is the source of job satisfaction, and that regardless of how hard you work at your job or how many times you're promoted, it is your friends and family that are the true source of happiness.

Through wisdom gained from the sum of his experiences, Christensen shows today's professionals how to lead a life with balance, integrity and purpose.

In the following blinks, you'll also learn:

  * why video rental company Blockbuster was destroyed by upstart Netflix;

  * why it's important to let your child make mistakes, and even fail; and

  * why IKEA's worldwide success is based on its mastery of one simple job. ** __**

### 2. Motivation trumps money when it comes to job satisfaction. 

What do you think would make you happier at work? Perhaps a little more pay might be nice, or maybe some more admiration from fellow colleagues.

Such assumptions are fairly common. In fact, the tangible aspects of your job, such as money and prestige, are not actually the things that will make you happy. If you think otherwise, go to a business school reunion where you'll see just how often professional success is often tainted with personal dissatisfaction, family failures, professional struggles and even criminal behavior.

Despite this, an unhealthy approach to the use of incentives in the workplace still prevails. Popularised by economist Michael Jensen and management theorist William Meckling, the _incentive theory_ makes the straightforward statement that the more you are paid, the better you perform.

In light of our business school reunion example, this theory seems too simplistic. What's more, studies have shown that the hardest-working people are in fact those employed in non-governmental organizations (NGOs) — people who do world-changing work, but earn very little.

In fact, it turns out that professional satisfaction and motivation are derived from work that matches your needs and interests. Psychologist Frederick Herzberg proposed that our needs and interests can be divided into two different categories — _hygiene_ factors and _motivation_ factors. This forms the basis of his _hygiene-motivation theory._

Hygiene factors cover issues such as general conditions at work, company policies, supervisory practices and job security. If these issues are not satisfactory or are lacking, it's a case of _bad hygiene_ that then causes job dissatisfaction. However, would a job with great working conditions but no room for promotion or reward be satisfying? Probably not.

Job satisfaction instead is achieved by combining hygiene factors with _motivation_ factors. Motivation factors concern recognition, responsibility, challenges and personal growth.

Consider a job that was intellectually stimulating but burdened with terrible management — would this give you satisfaction? Definitely not. It's clear that the confluence of hygiene and motivation is crucial, and the next blink presents two strategies to attain this balance.

### 3. A good career strategy combines using opportunities we anticipate as well as those we don’t. 

What's your career strategy? While most people have at least an idea of how they'd like to develop professionally, few know how to describe how exactly they plan to achieve their professional goals.

A good starting point is recognising that career strategies take two different forms: _deliberate_ and _emergent_. To understand these two approaches, we need to consider the ways in which opportunities in general arise.

Academic and renowned author Henry Mintzberg explains that opportunities also fall into two categories. The first describes _anticipated_ opportunities, those opportunities we can recognize and choose to pursue.

Deliberate strategies are often built on anticipated opportunities. Let's look at one example from Japanese car manufacturer Honda from the 1960s.

At the time, large motorbikes like the ones produced by Harley Davidson were popular in the United States, so Honda decided to launch its own range of motorbikes there, too. The goal was to get a toehold in the U.S. market, but the low quality of Honda's bikes almost killed the company.

This shows that deliberate strategies aren't always successful. Everyone can think of a time when, despite their best efforts, absolutely nothing went to plan! This is where an _emergent_ strategy comes in, by utilizing _unanticipated_ opportunities. These often arise in the implementation process of a deliberate strategy.

Honda found its emergent strategy in the United States by accident. Along with its big motorbikes, the company also had shipped its smaller, Super Cub motorbikes for employee use. Honda employees would ride up and down the hills of Los Angeles, and this unusual sight so intrigued the public that it stirred demand for the Super Cubs. Honda thus embraced the emergent strategy of selling its smaller bikes on a large scale, and thereby saved its American venture.

Creating a balance between deliberate and emergent strategies will allow you to make use of any opportunity. If you're both calculating and flexible, you'll always find the right direction.

> _"As you're living your life from day to day, how do you make sure you're heading in the right direction?"_

### 4. Your life is your “business.” To run it well, you need to properly manage your resources. 

When we talk about _resources_, we often think first of those that are business-related: assets, talent, finances and so on. This understanding is, however, quite narrow.

To gain a better understanding of how we should use our resources, we must first broaden our definition.

Consider the things that are important to you, such as family bonds, rewarding friendships and physical health. In a sense, these aspects of our personal lives are "businesses" as well, and the resources we invest in them are our personal time, energy, skills and wealth.

However, just like in business, all our resources are limited. Though there are many goals we would like to achieve, we have to manage our priorities.

It can be tempting to invest all our resources into one goal, and for many, this goal is a career. Nevertheless, it is crucial to ensure that we invest time and energy into other things that we also value.

By taking control of your _personal resource allocation process_, you can avoid the mistake of investing everything into your career.

One way to do this is to reconsider the default criteria according to which we typically allocate our time. Rather than automatically spending all your time on a work project, evaluate first whether the project is indeed the most important thing in your life right now, or whether there are other things more deserving of your time, such as your family or your well-being.

For high-achieving professionals in particular, prioritizing immediate rewards over long-term gains is a common mistake. It is easy to become overly-fixated on a promotion or future bonus.

The satisfaction these provide is instant but short-lived. Long-term goals, such as the task of raising your children well, while gradual and challenging, will provide a reward that is lifelong and far more valuable. We'll explore this further in the next blink.

> _"... for many of us, as the years go by, we allow our dreams to be peeled away."_

### 5. The relationships with your family and friends are the most important sources of happiness. 

If you're a high achiever, you may feel that the effort you put into your job is what's most rewarding in your life.

However, the effort you dedicate to family life also brings valuable rewards. It's just that these may not reveal themselves for many years. Unfortunately, underinvestment in these longer-term issues will ultimately prevent them from flourishing.

What a relationship needs most is consistent attention and care. This can be difficult to provide, and the reasons are twofold.

First, we are always tempted to invest our resources into a task with an immediate payoff, namely, our job. A spare 30 minutes after work could easily be spent as family time. But often the people and projects at work that demand your attention, as well as the promise of making money, can quickly banish thoughts of your family from your mind.

Second, those with whom you share a deep relationship — family members, friends — rarely shout the loudest when demanding your attention. Instead, they're likely to support your career without complaint. But remember: just because they don't ask for your time, doesn't mean that they don't need it.

In a sense, relationships reflect a paradox. They require consistent dedication even when it appears unnecessary. Many seem to think that they can compensate for neglecting their loved ones by showing greater care later on.

However, damage done to a family in its early stages will manifest as problems later on. For example, research shows that the most influential period in the development of a child's intelligence is within the first year, so the way parents speak to their child at this stage will shape their life as a thinker.

Finally, it's not just your family that needs you; at some point, you will depend on them. If you neglect these relationships now, you run the risk of losing support when you need it the most.

> _"The appeal of easy answers — of strapping on wings and feathers — is incredibly alluring."_

### 6. Intuition and empathy help us do the job of making our loved ones happy. 

Often companies seem to focus so intently on selling a product that they lose sight of the real needs of the customer. Unfortunately, many people approach relationships the same way.

Whether your family or your business, your real job should always be to understand and fulfil the needs of others.

This job is by no means easy. There are two tools that can help you: _intuition_ and _empathy_. A marriage works when each spouse understands what is expected of him or her. Using empathy and intuition to grasp these expectations, however, takes practice.

For example, a man comes home from work one day to find a mess in the kitchen. Intuitively assuming that his wife has had a rough day, he decides to tidy up. Expecting to be thanked, he instead is surprised to find his wife upset.

She tells her husband that caring for two demanding children is incredibly difficult, as she is unable to speak to another adult all day. What she needed most was simply for her husband to listen to her.

In this case, the husband's initial intuition was off the mark. But through this experience, he'll better know how to be more sensitive to his wife's needs in the future.

Another way to improve your relationship is if you think about it as a job. A key question to consider is, "What job does my family, friend or partner require me to do the most?"

The Swedish home furnishings company IKEA can provide us with an example. What its customers need is to quickly and inexpensively furnish their homes. The company's job is then to satisfy this need effectively. This is why the company doesn't sell a specific kind of furniture: its job is more general.

Because their needs are being met, IKEA customers remain loyal. The same goes for relationships: if you understand and fulfil the job required of you by your loved ones, they will remain loyal to you.

### 7. Raise your kids right: let them learn from their mistakes and celebrate their good behavior. 

Of all the jobs you have in life, one of the most important is educating your children. This does not simply mean teaching them all there is to know, but rather, giving them the tools to teach themselves.

The best way for children to develop their own values is by allowing them to face challenges and find solutions independently. Such challenges could include learning to work with a problematic teacher, struggling with a new sport or negotiating cliques or bullies at school.

By introducing children to everyday problems early on, their self-esteem will develop in a healthy way. You may be reluctant for your children to pursue goals on their own, or attempt to prevent them from making mistakes. However, don't be afraid to let them fail. It is far more important that a parent is there to provide support when children make mistakes, from which they can then grow and learn.

Another vital aspect of parenthood is the implementation of a healthy _family culture_. The foundation of this is family values, an informal but highly influential system of guidelines that will equip your children for any challenge they face, even as adults.

In creating a family culture, it helps to think of it like an autopilot. Once programmed, it is perfectly capable of running itself.

Let's say you want your family to be known for kindness. You can help program this value by taking every opportunity to emphasise its importance. Such an opportunity could be as simple as a conversation with your child about schoolyard bullying, or praising your child when he demonstrates compassion. In this way, kindness will be at the core of your family culture.

It's easy to slip into the mind-set that raising children is centred on controlling bad behavior. This negative approach is in fact far less effective than a perspective that celebrates the good of your family in everyday interactions. The set of values that emerge as a result will be firmly established, and able to weather any challenge.

### 8. Compromising your integrity can create a slippery slope, so don’t do it! 

What does it mean to live your life with _integrity_? It's not only about our choices when faced with a dramatic moral challenge.

Rather, integrity emerges from the decisions we make every day. In this way, integrity requires constant self-awareness.

We must remain aware of the _trap of marginal thinking_. This danger can be illustrated through the example of DVD rental company Blockbuster. Blockbuster was aware of its rival Netflix, an innovative online movie rental company, but dismissed the company as non-threatening and certainly no reason to expand its business strategy to DVD rentals by post in response.

But, while Blockbuster did nothing, Netflix crushed its competitors; so much so that Blockbuster declared bankruptcy in 2010. Though the company avoided paying the marginal costs of matching Netflix's innovative business model, Blockbuster paid the ultimate cost for mismanagement, which was business failure.

Now you know that focusing on marginal costs when faced with commercial decisions can prove disastrous. This marginal thinking becomes even more dangerous when the moral behavior of a person comes into play. In such cases, it is absolutely crucial that we don't make ill-considered decisions as individuals, no matter the situation.

Have you ever made a decision that went against what you believe, but justified it as something you'd only do "just this once?" Here's an example of how harmful these exceptions can be.

Nick Leeson was a stock trader whose marginal thinking led to the downfall of Barings, a British merchant bank. After incurring a loss on some trades he had managed, he decided that "just this once" he would hide the loss in an unmonitored trading account.

This led to more marginal thinking as Leeson attempted to cover his increasingly irresponsible actions, and soon snowballed into forging documents and conning auditors, eventually resulting in a loss of $1.3 billion. Leeson was arrested and sent to jail; and Barings, after declaring bankruptcy, was sold to a competitor for just one pound sterling.

### 9. Final summary 

The key message in this book:

**The most successful professionals are those who invest their resources not just into work, but into their family and lifestyle. By taking responsibility for the jobs that are required of us outside the workplace, such as raising our children, supporting a spouse or leading our own lives with integrity, we can achieve the work/life balance that we aspire to. Only then can we achieve true happiness.**

Actionable advice:

**Don't be afraid to let your kids solve problems for themselves!**

You probably only have your children's best interests in mind when you give them what they want. The fact is, they need to be challenged, perhaps through learning a musical instrument, grappling with a new sport or developing social skills. This will give them problems to solve, teaching them values, and providing experiences for them to be engaged in.

**Suggested** **further** **reading:** ** _The Innovator's Dilemma_** **by Clayton M. Christensen**

_The Innovator's Dilemma_ explains why so many well-managed, well-established companies fail dismally when faced with disruptive technologies and the emerging markets they help create. Through historical examples, Christensen explains why it is precisely these "good" management companies that leave big players so vulnerable.
---

### Clayton M. Christensen, James Allworth and Karen Dillon

Clayton M. Christensen is a critically acclaimed author of several books and the Kim B. Clark Professor at the Harvard Business School.

James Allworth is a graduate and Baker Scholar at the Harvard Business School.

Karen Dillon is an American investigative journalist and former editor of the _Harvard Business Review._ She was named one of the world's most influential and inspiring women in 2011.

