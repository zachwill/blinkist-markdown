---
id: 56142f0a3632330007010000
slug: hug-your-customers-en
published_date: 2015-10-07T00:00:00.000+00:00
author: Jack Mitchell
title: Hug Your Customers
subtitle: STILL the Proven Way to Personalize Sales and Achieve Astounding Results
main_color: FFB943
text_color: 805D22
---

# Hug Your Customers

_STILL the Proven Way to Personalize Sales and Achieve Astounding Results_

**Jack Mitchell**

_Hug Your Customers_ (2003) is based on the author's five decades of experience in crafting the perfect customer-centered business. "Hugging" your customers is about catering to their every need and organizing your entire company around them. Establishing a hugging culture is the most effective way to achieve financial success and keep your customers happy.

---
### 1. What’s in it for me? Become a customer service master. 

If you've ever worked in retail or customer service you'll know that most companies don't really stretch themselves much further than repeating the hollow phrase "the customer is always right." Staff may take customer service training that teaches them to smile and say "can I do anything for you?" but that's basically it. If the customer really has a problem, or needs something extra, most companies won't go that far to meet them.

But it doesn't have to be like this. Companies can, indeed _must,_ do all they can for customers if they want to continue getting their business. These blinks, based on the thinking of one of the most accomplished retail store owners in the United States, show you how to develop a culture of exemplary customer service, and get the loyalty of everyone who buys your product or service.

In these blinks, you'll discover

  * why pets should always be welcome in any store;

  * how fixing a button got the author tickets to the final of the US Open; and

  * why, if the customer needs something, your store should never really be closed.

### 2. Build a hugging culture by finding out exactly what your customers want. 

No matter what business you're in, there's one maxim that universally holds true: if you want to succeed, keep your customers happy.

That's easier said than done, however. How do you keep them happy? The best way is to develop a _hugging culture_. 

Maintaining a hugging culture means giving the customers anything they want, and that means you have to find out _what_ they want first.

The author implements this strategy in his own chain of clothing stores, Mitchell's. Mitchell's employees call their customers by their first names and commit to building lasting relationships with them — the best way to find out what they truly want and need.

When a Mitchell's employee offers a customer help, they don't just say, "May I help you?" Instead, they ask something more personalized, like "What occasion is this outfit for?" or "Is this for business or pleasure?" Asking more specific questions allows them to get a deeper understanding of what the customer is looking for.

Getting to know the customers in a deeper way also allows the employees to build relationships with them over time, so they get an even better feel for their individual preferences.

When you go the extra mile for your customers like this, it really pays off. Happy customers will help you expand to new locations, forgive you if you make any mistakes and even invite you into their homes. 

At Mitchell's, employees once fixed the button on a woman's jacket even though she hadn't bought the jacket from them. That woman turned out to be Robin Gerstner, the wife of Lou Gerstner, the former CEO of IBM. The Gerstners were so impressed by Mitchell's service that they invited the author to come to the US Open finals with them, where they promoted his company as "the best clothing store in the world."

### 3. Make yourself physically available to your customers, however they need it. 

The "hug" in "hugging culture" isn't just metaphorical. Sometimes making physical contact with your customers is very beneficial. There are a few reasons for this.

First off, physical contact makes people feel good. Everyone values that kind of emotional support.

The author learned this when he first started working in his family's business. In the early days of Mitchell's, his mother, Norma Mitchell, hugged all of the company's clients and personally made coffee for them, as they were all close friends of the family anyway. She continued the tradition as the business expanded and Mitchell's maintains it to this day.

You don't have to hug your customers to make positive physical contact, either. A range of physical interactions can bring you closer. Depending on the customer's personality, you can try a handshake or a high five.

If the customer is shy, try carrying their bags or offering them a handkerchief instead. Those small physical gestures can make a big difference.

You can physically serve your customers as well. If they ask an employee where they can find something in the store, the employee can walk them there instead of just pointing it out. The author once heard a story from a customer in another clothing company store who'd asked a worker where he could find a section of clothes, only to have the worker disrespectfully point and try to shoo him away. The customer left the store and never returned.

Go even further by physically delivering products to your customers when they need it. The author once drove across the city and waited for hours by a harbor during winter just to deliver a jacket to a special client. 

So make yourself physically available to the people you're serving, whether that means offering a hug or saving the day by delivering their goods.

### 4. Treat every customer like royalty, and take care of their children and pets too. 

It's not pleasant to enter a shop and hear a worker parrot a line like "Welcome to X, my name is Y, how may I help you today?" No one likes to be treated as if they're the same as everyone else.

That's why the best companies treat every customer like a VIP. Customers love to be spoiled.

There are a number of ways you can do this. Mitchell's sends personalized letters and flowers to customers on special occasions like birthdays or weddings, for example. They also open the store during closing hours if a customer has an emergency. A Mitchell's employee once stopped watching an important football game to go open the store for a man who forgot to pick up his clothes before a bar mitzvah. People don't forget that kind of service!

And don't stop at providing for your customers, either. You can provide for their kids, too. Everything you do for them and their family will help you earn their loyalty and trust.

Mitchell's stores have a play area for children, which is great for both the children and their parents. The children can watch cartoons on flat screen TVs and enjoy the snacks provided for them — sometimes they enjoy it so much that they don't want to leave. The play area also gives them and their parents another reason to come back.

Do the same for your customers' pets. It's an unfriendly turn off to display a huge "NO DOGS ALLOWED" sign on your front door. At Mitchell's, if a customer brings her dog, the employees learn the dog's name and remember to ask its owners how it's doing in the future.

> Top customers to the author's store were rewarded with an unforgettable dinner with Carl Bernstein, the journalist who broke the Watergate story.

### 5. Keep every part of your business in the same building the customers visit. 

Once you understand what a hugging culture is, the next step is to implement it. One good way to start is to make sure that every department of your company is housed in the same building. 

Keeping everything in one place isn't just about saving money on rent. It's good for the customers too. When the marketing, planning and selling teams are in one place, it's much easier for the team members to work together to create a great customer experience.

This happened at Mitchell's when a customer wanted purple socks added to the inventory. It was easy for the different departments to collaborate on this goal since they worked so closely together, and purple socks were added to the store literally overnight.

Keeping everyone together also makes it much easier to solve any problems that arise. If a problem appears, the different departments can come together to work toward a swift, holistic solution.

Imagine that all the vendors are suddenly busy, for example. That's a problem in a hugging culture, because each customer needs to be greeted at the door. But if all the departments are working together, someone from the marketing team can step in and cover for the vendors if necessary.

Having the different departments work closely together also makes your hugging culture more complete. At Mitchell's, employees are allowed to try out tasks in other departments if they have extra time, which gives them a deeper understanding of how things are run. And if an employee is dissatisfied with their work, they can switch to something else. Keeping everyone in the same building is better for your customers _and_ employees.

### 6. Provide your customers and employees with a data system that helps them find what they need. 

Your hugging culture depends largely on your staff members and their interpersonal skills, but that doesn't mean there's no room for technology. Technology and data are a big asset in a hugging culture — let's look at the reasons why.

Data can help customers get immediate access to the information and assistance they need when they're in the store. Mitchell's has a sophisticated data system and information terminal for helping customers in need. Customers and staff alike can use it to look through the clothing collections and get recommendations on new combinations.

If a client can't remember the exact name or look of a piece, they can find it on the database by sorting by color, style or size. M-Pix, the e-commerce and email system Mitchell's uses, also helps staff offer individual wardrobe suggestions to each customer.

Electronic data isn't just for the sales staff, either. The accounting, marketing and merchandising teams can use it too — data makes it easier for the different departments to integrate.

Data also helps you maintain stability in times of uncertainty and change. Two Mitchell's customers once mixed up each other's purchases, for example, and an employee was able to quickly sort it out by looking through the data. They figured out who had bought which clothes, then arranged for the customers to meet up and switch. 

Data can also help clients when they experience financial problems and have to adjust their price range. An online catalogue allows them to sort the items by price, or find items similar to the ones they want but in a more affordable price range. 

All in all, a customer needs an interactive data system that saves their preferences. A good data system is an indispensable tool in a hugging culture.

### 7. Design your company so that your hugging culture stays strong even during a recession. 

Customer service is vital, but how can you afford it in today's troubled economy? Fortunately, there are a few strategies.

The first thing you need to do is examine the different sectors of your company and determine which have to be stable in order for you to survive an economic crisis. Pay careful attention to your outcomes and downsize to an outcome level in accordance with your income, if necessary.

So if you spent $50,000 on marketing in 2009, for example, and your current income level just shortened to the level it was in 2009, readjust your marketing outgoings to your 2009 level too.

Be sure to hire a good financial specialist to help you recover and regrow. Having a customer-centered company doesn't mean ignoring serious financial issues. 

But never forget that your customers and hugging culture are the most important things. When you have to cut services and products, pick the ones that will affect the customers the least. It makes a very bad impression if you cut any services or products that customers have been relying on for years. 

Remember that your customers suffer during a recession too. You can support them in difficult times by allowing them to pay in installments. 

It's unwise to deny your customers access to high quality goods because of temporary financial issues. So offer them goods on credit! You'll damage your relationships with them in the long run if you don't, because they probably won't come back after the recession.

Recessions come and go, but if you commit to your customers and hugging culture, your business will be here to stay.

### 8. Final summary 

The key message in this book:

**Hug your customers — physically, emotionally and even financially! Do everything you can to help them, even if it doesn't give you any immediate financial gain. Build long-term customer relationships based on trust and respect. Hugging your customers doesn't just keep them happy and bring you success, it's more fulfilling for you, too.**

Actionable advice:

**Don't get bogged down by statistics, organizational structure or business manuals — think outside the box.**

Don't be afraid to try new things. You should always be on the lookout for new ways to serve your customers, even if no one has done them before. Caring for a client's dog might sound ridiculous, but it worked out very well at Mitchell's.

**Suggested further reading:** ** _Legendary Service_** **by** **Ken Blanchard, Kathy Cuff and Vicki Halsey**

_Legendary Service_ outlines the principles of great service and describes just how you can implement them in your company. As interacting with customers is a key element in almost any business, following this model is a surefire way to improve your company's performance overall.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jack Mitchell

Jack Mitchell is the chairman of the highly successful Mitchell's chain of clothing stores, his family business. He writes about corporate strategy and has given talks at Harvard University.

