---
id: 5746d08bb49ff200039c4d8e
slug: hug-your-haters-en
published_date: 2016-06-01T00:00:00.000+00:00
author: Jay Baer
title: Hug Your Haters
subtitle: How to Embrace Complaints and Keep Your Customers
main_color: 2EC6E5
text_color: 196E80
---

# Hug Your Haters

_How to Embrace Complaints and Keep Your Customers_

**Jay Baer**

_Hug Your Haters_ (2016) is a guide to using disgruntled customers to improve your business. These blinks explain why it pays to listen and respond to negative reviews and how you can turn your crabbiest customers into your biggest fans.

---
### 1. What’s in it for me? Get the best out of customer complaints. 

We all love praise. And we love those who praise us. But actually, there are good reasons to appreciate your company's critics every bit as much as you cherish its admirers — after all, it's the critics who tell you what to do to make things even better than they already are.

But what's the best way to deal with critics?

That's one of the questions these blinks answer, and it's a vital one, because if you don't handle complaints well, your customers will give up on your company for good — and tell everyone on Facebook that you suck. But if you do it right, you can turn your most angry and frustrated customers into devoted followers.

These blinks provide lots of good advice on how to charm your company's critics — offline and online.

And along the way, you'll discover

  * why customers of Fresh Brothers Pizza are glad if their pizza arrives a little too late;

  * what's so bad about putting an angry client on hold; and

  * why passengers who forget their stuff at Amsterdam's Schiphol airport become such great fans of KLM.

### 2. It’s crucial for your business to respond well to customer complaints. 

Do you enjoy calling customer support hotlines? Being on hold for hours listening to muzak? Or does this experience make you wonder why companies invest huge sums of money in advertising to attract what are primarily new customers while investing zilch in customer service?

In such situations, it might seem like these companies don't care about holding on to their existing customers and that's a major mistake. Because naturally, it's key for a company to retain the customers it already has.

In fact, retaining a single extra percent of your customer base can boost your profits by 25 to 85 percent!

But how can you be sure your customers will remain loyal?

Well, it helps to listen to their complaints and show them that you've got their satisfaction in mind. After all, if your customer is making the effort to complain, it's likely they're willing to stick with you, at least for now. So, if you address the customer's grievances appropriately, chances are they'll remain a customer and feel a sense of duty to your business.

For instance, Debbie Goldberg, a co-owner of Fresh Brothers Pizza, makes an effort to personally answer every Yelp review. To be fair, she receives very few poor reviews, but when she does, she takes extreme care to address the reviewer.

She starts by politely apologizing for the customer's bad experience. Then she acknowledges the problem they've raised, making it clear that the company is already working on fixing it.

And finally, she offers them a gift certificate to make up for their trouble. With these simple steps, Goldberg has retained the favor of countless customers that the company would otherwise have lost.

> _"Hugging your haters gives you the chance to turn lemons into lemonade, morph bad news into good, and keep the customers you already have."_

### 3. Learn from customer feedback and be more accommodating than your competitors. 

Imagine you're at a dance class to prepare for your wedding. If the instructor never points out to you that you're basically crushing the life out of your partner, you'll keep doing just that.

That's because we sometimes need negative feedback to improve our performance and in the case of companies, this feedback generally comes from customers.

After all, complaints are an insight into your business that can help you better meet your customers' needs. In fact, only 5 percent of your customers will even care enough to complain, which makes it all the more important to listen when they do. Just remember, it's likely that the other 95 percent are displeased for the same reasons.

This means that by listening to these complaints, you can uncover the problems your company needs to fix to keep all of its customers happy. For instance, Square Cow Movers is a small family business that helps locals move houses. In the past, the company has struggled with very poor customer reviews.

Finally, the owner, Wade Lombard, analyzed the complaints and found that much of the criticism was about customers not having important information, like when the moving crew would arrive. Uncovering this problem enabled the company to respond.

They told their employees to improve their client communication and keep them up to date on all the relevant information. And guess what?

It worked! The negative feedback became a thing of the past.

So, complaints can help you better serve your customers and you can actually beat the competition by offering the most enjoyable experience for customers. That's because other companies can mimic your products or beat your prices, but your customers will nonetheless remain committed to you if you make it worth their while in other ways.

Embrace complaints and accommodate customers as much as possible. To do so, just ask yourself what would make customers even happier with your service. And remember, if you "outlove" your competitors in this way, your customers will consider you one of a kind.

> _"Haters are the canaries in the coal mine. They are the early warning detection system for your business."_

### 4. Attend to the needs of your offstage critics by dealing with them as people and quickly resolving their issues. 

Even in this age of instantaneous digital communication, most complainers vent their frustration in private through an email to customer service and not via Facebook. And if you know what's good for your business, you'll want to keep it that way. So, to keep complaints private and retain customers, use the mnemonic _H.O.U.R.S_.

First, be _Human_. That means interacting with your customers in a personable way, showing that you see them as people, not just data points. To do so you need to show your concern and avoid boilerplate messages.

For instance, Dr. Glen Gorab, an oral surgeon, calls all his new clients before their first visit to ask them about any concerns they might have. And if any surgical complications arise, he is sure to stay in close contact with the patient, showing them that he cares. Maybe that's why, in 30 years of surgical practice, he's not faced a single lawsuit.

Second, to reconcile the problems complainers voice, it's key to use just _One_ channel. That means you should never refer clients to another person or site. After all, complainers are already annoyed, helpless and looking to you for a solution. So, if you refer them to another department or an FAQ page on the company's site, you'll only make them more frustrated.

And finally, you need to _Unify_ your data to produce the best solutions that _Resolve_ your client's issues with _Speed._ That means you should document all customer complaints. If the complainer refers to a specific problem with your company you need to address it. If they don't, you can send the complaint list to your client service team to brainstorm ways to help future clients.

The best strategy here is to compile these solutions in a go-to document that allows your team to serve customers quickly, before your frustrated clients jump ship.

> _"Haters are not your problem_. . . . _Ignoring them is_."

### 5. To win back the favor of your haters, find them, show them you care and answer them publicly. 

Someone who criticizes your company publicly is not necessarily expecting you to answer them, but you always should. To do so, you can use the mnemonic _F.E.A.R.S_.

First, to resolve public complaints you need to _Find_ them. You can use services like Google Alerts and social media listening software like Mention.net for this.

Then, when responding to these customers, you want to show _Empathy_ or your answer will be useless. So, ask yourself how you would want to be addressed if you were the one complaining.

For instance, Chris, a cat owner, used the Meow Mix Facebook page to ask about the dyes used in the pet food. He posed the question because whenever his cat vomited, the dyes left terrible stains on his sofa.

The company responded with the readymade answer that they conformed to all industry standards. Then they invited him to call with any further questions. If Chris was a bit frustrated when he posted the question, this response only made it worse.

On the other hand, an empathetic answer would have helped the company's reputation. For example, they could have acknowledged how troubling it is when a cat throws up and suggested a solution to Chris' problem.

So, empathy is key, but for the greatest impact it's important to _Answer_ publicly, don't _Reply_ more than twice and _Switch_ channels if necessary.

For instance, in order to signal that you care about your client's opinions and aren't hiding anything, your first response to a complainer in an open forum should be public. But that doesn't mean you should get drawn into a discussion. After all, such back-and-forths are a time sink and don't provide additional benefits.

Then, after your initial public answer, you might need to use a different channel, like email, to protect your customer's privacy as you answer their question in detail.

> _"Everything online is amplified. . . . If you choose to not interact, that silence is amplified, too."_

### 6. Businesses need to respond to reviews across all channels. 

Every day we are growing more connected through a variety of online channels from social networks to rating portals. The impact of this technological connection on the world of business has been enormous.

That's because all these channels offer customers unprecedented power over corporations. For instance, Google Reviews, Twitter, Facebook and online forums all offer avenues to express satisfaction — or dissatisfaction — with a given product or service.

By giving feedback through these channels, customers can affect the way the public sees a company and there's little a business can do to change this. Well, there is one thing: you can engage customers and embrace their complaints, wherever they express them.

After all, without your response, all users will see is a negative review, but once you throw your hat into the ring, users will see that the company cares about their experience and is invested in correcting its errors.

That's exactly what Scott Wise from Scotty's Brewhouse does. His company searches the web for reviews on every site and Wise attempts to answer the majority himself. It might require a bit of effort, but it really works. Wise says that offering the right response can transform the most ardent complainers into his biggest fans.

But it's also crucial to decide who's in charge of making these responses. After all, it's a time-consuming job and it's likely your team already has a lot on their plates. To solve this issue, Dell formed an extra team called _communities and communications_ that handles both its customer support forums and monitors its internet feedback.

It might scare you to engage your customers in the digital sphere, but fear will only make the situation worse. Remember, power is in the hands of customers right now and, as technology develops, it will only concentrate further.

### 7. Deal with your customers’ problems proactively and they’ll be loyal for life. 

So, responding to complaints is key, but why wait for a complaint?

After all, if you want your customers to value you, it's a great idea to correct a mistake right away, before anyone even has time to mention it. In fact, you can often prevent customers from complaining by making up for errors as soon as they occur.

For instance, when Fresh Brothers Pizza delivers a pizza late, even by as little as five minutes, they send the customer a gift certificate to apologize. Strategies like this work great because your customer will appreciate you for addressing the issue, while you dodge a nasty review.

But sometimes it's impossible to avoid a complaint, or else you're faced with an issue that wasn't even your fault.

In cases like this, if you solve a customer's problem proactively, the initial problem can even turn into a chance for an improved customer experience that makes them appreciate you all the more. For instance, say you forgot your baggage on an airplane and right before you board your next flight, an airline employee appears beside you, handing you your lost bag.

It would be amazing if that happened, right?

Well, it actually does at Schiphol, Amsterdam's international airport and the home base of KLM Royal Dutch Airlines. Here's how.

The cleaning crew and flight attendants often discover lost items as they prepare the plane for the next flight. Whenever this happens, they immediately notify the Lost and Found team, which then checks the list of passengers who contacted the airline via social media regarding a lost item.

If they find a match, they bring up the passenger's itinerary, locate them and deliver the lost item. So, get inspired by KLM and solve your client's problems proactively. They'll appreciate it more than you can imagine.

> _"The ability to predict a complaint or problem and solve it before patrons notice is customer experience sorcery of the very best kind."_

### 8. Final summary 

The key message in this book:

**Just about every aspect of your business from your pricing to your product to your marketing strategy can be copied. But your customer service is unique and no one can duplicate it. So, be personable and proactive and you can ride your customer experience all the way to the top.**

Actionable Advice:

**Invest in a killer customer service team.**

The well-educated, competent people you need to make your customer service what it should be will only join your team if you offer them a decent salary. This is no place to pinch pennies. But getting the best people is just the start. Once you've assembled your A-team, it's essential that they know all about your company and receive the training they need to succeed.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Youtility_** **by Jay Baer**

_Youtility_ goes against the grain of accepted marketing methods by declaring that information, not promotion, is the way to win customers. Counterintuitive and refreshing methods are presented, repositioning the relationship between businesses and consumers. The book outlines examples from a wide spectrum of companies, big and small, driving home the message that by helping people and being useful instead of chasing sales, companies can prosper in the long-term.
---

### Jay Baer

Jay Baer is a marketing expert and president of the global consulting firm Convince & Convert. He has advised some of the most iconic organizations in the world, including Nike and the United Nations, and is the author of five _New York Times_ best sellers.

