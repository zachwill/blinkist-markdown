---
id: 548a13d93363370009580000
slug: humanize-en
published_date: 2014-12-22T00:00:00.000+00:00
author: Jamie Notter and Maddie Grant
title: Humanize
subtitle: How People-Centric Organizations Succeed in a Social World
main_color: E82E4C
text_color: B5243B
---

# Humanize

_How People-Centric Organizations Succeed in a Social World_

**Jamie Notter and Maddie Grant**

In this practical guide for both business owners as well as employees, leading organization consultants Jamie Notter and Maddie Grant offer important tips for how to "humanize" corporations on both a cultural and organization level — all in order to position the company for effective growth in the social media age.

---
### 1. What’s in it for me? Find out how to make your company more human. 

Everybody in marketing today knows that they have to figure out how to make social media work for them. But while tinkering with Twitter, Facebook, Instagram, etc., only a few have noticed what social media is trying to teach us. That's a shame, because social media can teach us everything we need to know about how to do business in the 21st century.

The key is a new conception of what business means. Is it a giant machine-like apparatus, with a bunch of steel cranking out doohickeys on an assembly line? What if it more resembles us humans, fallible, curious, living things that we are?

That's the question that _Humanize_ tries to answer. By looking at how some of the best businesses have succeeded in making their companies more human, these blinks hold a few important secrets not just for marketers, but for anyone who wants their business to reach more people.

These blinks will show you

  * why best practices should be thrown out;

  * why good companies are courageous; and

  * why one chain of grocery stores makes all its salaries visible to the other employees.

### 2. Social media has revolutionized the way we do business. 

Over the past few years, social media has become all-pervasive. And in the process, it has transformed the way we interact with each other.

But social media hasn't only changed our personal relationships, it's also had a profound impact on the business world in several crucial ways.

First of all, social media makes it harder for companies to control their brand images. Before the dawn of social media, companies managed their brands by developing a marketing strategy based on market research; if anything went wrong, businesses would rely on PR spokespeople to release slick statements.

Today, however, companies have to contend with word-of-mouth marketing. Pretty much anyone with access to Facebook, Twitter, Yelp and other platforms can broadcast opinions about a brand.

These platforms have not only minimized the control companies have over their brands, they've also transformed the processes businesses use to get their messages out.

Earlier, when just a few forms of traditional media (newspapers, magazines, television) dominated broadcasting, all businesses had to do communicate with their customers was to place the right message on the right channel.

But because of the emergence of real-time communication platforms like Twitter (which can report news much faster than traditional media outlets), traditional media is all but obsolete today.

And finally, social media has impacted the business world in one more important respect: these new technologies have disrupted the way industries are structured.

So in the past, trade organizations and other professional associations functioned by offering benefits such as education, networking and advocacy in exchange for regular dues.

But now, people use social media to network online for free — which has lessened the influence of traditional organizations and irrevocably changed entire industries.

Clearly, the emergence of social media requires businesses to find new ways to connect with customers, but have companies actually been doing so? Keep reading to find out.

### 3. Entrenched, conventional practices discourage companies from developing more innovative approaches. 

The truth is, many businesses haven't been able to keep up with the times. Even amidst the social media revolution, plenty of companies continue to rely on the old _best practices;_ that is, standardized techniques that were developed over many years, and reinforced through benchmarking.

Although adhering to best practices has certain advantages, it can cause businesses to oversimplify the complexities of cause and effect.

For example, strategic planning — one example of best practices that's still used by many businesses—is all about coming up with a five-year plan to plot out the future. However, these plans tend to be abandoned quickly.

Why? Well the truth is, reality unfolds in complicated, hard-to-predict ways — and this is _especially_ the case in the social media age.

Another drawback of best practices is that it leads businesses to compartmentalize their organizations into discrete parts — which can cause them to lose sight of how the system works as a whole.

For example, consider human resource management, an established tenet of best practices. Most large companies organize their personnel into a hierarchy of departments headed by a management team. Employees are typically hired into particular departments, where they are expected to fulfill a specific job description.

While division of labor can lead to more productivity, too much separation can create barriers that prevent departments from freely sharing information and ideas across the whole company.

Which gets at another point: one big reason best practices are so detrimental is because they rigidly formalize business processes, thus discouraging open-ended and creative thinking.

In other words, when was the last time you were excited about a strategic planning session? And how often do an interviewer's formal, hackneyed questions allow you to express who you really are?

That's exactly the problem with best practices. They emphasize clarity and order over ambiguity, but ambiguity is a necessary condition for creative problem-solving and innovation.

### 4. To adjust to the social media age, businesses need to tap into their humanity. 

Here's something that may surprise you: innovation and success are a matter of mindset. That is, you can make your business more compelling and efficient by changing the way you think and talk about your company.

This principle doesn't conform to our conventional ideas about business; after all, we tend to view companies as machines.

Just look at the language we use: We _run_ organizations, _engineer_ our _processes_, manage our human _resources_ and measure our _outputs_. These are all very mechanical terms.

We also use similar language to describe business organization: _divisions_, _teams_, _units_ — these terms have a technical feel.

Additionally, consider that these organizational structures are based on discrete components playing specific roles within a larger system — just like cogs in a machine!

These mechanical metaphors are all-pervasive in the business sphere — and for good reason! During the Industrial Revolution, machines altered human society in a profound way. And yet, a lot has changed in the past century. And today, this kind of language doesn't adequately express the new social media age.

That's because social media is powered by the _human_ : we express our personalities on the flickering pages of the Internet by sharing our interests, activities, opinions and desires.

Recently, this more human aspect has been running up against the business world's machine-inspired mindset. Consider that while offices _operate_ only during business hours, social media is _on_ 24/7. So how can businesses respond to news and scandals that develop around their brand over the weekend, or after hours?

Ultimately, the business world needs to adjust. Social media isn't going away, and the only way businesses can exist today is by becoming more "human" themselves.

And although each company will have to find an individualized approach, there are four general goals leaders can use to humanize their organization: _openness_, _trustworthiness_, _generativeness_ and _courageousness_.

The following blinks will discuss more ways your business can adapt to the social media age.

### 5. To improve the culture of openness at your company, strive towards clarity rather than control. 

As we mentioned in the last blink, the first step of humanizing a company is to make it more _open._ That means decentralizing it and letting employees express their individuality.

Why is this step so important? Well, in the social media era, centralized systems of control no longer ensure business success. Since today's customers expect immediately feedback, it would simply take too long for every post, Tweet and email message to be approved by a central committee.

But although companies need to be flexible, completely ceding all control isn't the way forward. In fact, the most successful instances of decentralization strike a balance between openness and control by defining boundaries and then letting employees work within those limits at their own discretion.

Although this isn't explicitly connected to social media, a good example of this principle is Google's 20 percent time rule, which allows employees to spend one day a week working on an independent project — even if it falls outside their job description.

But it's worth noting that when you're promoting openness, it's important to be as clear as possible about your vision for the company.

This clarity will allow you to direct and focus your employees without necessarily controlling every single detail of their workday.

But how can you clearly convey your vision for the company to your employees? The best way is to come up with a few key principles.

For example, Nordstrom has just one rule for new employees: "Use your best judgement in all situations." Since this rule is so short, it's easy to memorize and implement at precisely the moment when a decision is needed.

Basic principles like these allow the system to achieve unity and consistency, without leaving the employees with that lingering, stultifying feeling that they're being controlled and told what to do.

Overall, an open company culture goes together with a culture of trust. So how can your company build trusting relationships within the organization? Read on to find out!

### 6. Create a culture of trust by promoting transparency within your company. 

In the social media era, _trust_ is paramount. Because how else can executives leave public relations in the hands of social media managers if they don't trust them to do the right thing?

So how can you create a culture of trust within your company? Well, the first step is introducing more transparency — something which is often hard for companies to embrace. Many businesses mistakenly view transparency as a threat; they think, _do we really want all that information floating around, for everyone to see?_ _And what if an employee unwittingly posts trade secrets?_

But companies should adjust their mindset. Sharing information doesn't have to be dangerous; in fact, it can actually be a clever, strategic move.

The view around sharing information can shift. Instead of viewing sharing as cause for concern, we can have the perspective that sharing information means other people in the system can produce results that are beneficial for everyone involved.

For example, the grocery chain Whole Foods made the radical decision to internally disclose all its salary data to all employees. To the uninitiated, the idea that employees are able to look up each other's salary might seem disastrous for morale — but the strategy works for Whole Foods.

Why? Well, when an employee notices that a coworker is earning more, she can try to figure out what makes her coworker stand out, and then adjust her own performance accordingly. In this sense, disclosing salaries motivates employees to work harder and more effectively to earn promotions and raises.

And this approach links back to our earlier discussion of open systems: transparency allows the right people to get access to the right information at the right time — without having to go through a centralized bureaucratic process.

> _"It is the risk of transparency that allows you to take advantage of the speed of trust."_

### 7. To promote innovation within your company, embrace differences and practice inclusion. 

We've arrived at the third principle of humanizing a business — _generativeness_.

Fair warning: this is a tough one. And yet, it's crucial for anyone who wants to create new value, innovate, and improve both her company's profits and also the whole world.

So how do you start? Well, since innovation is fueled by diversity, the first step to achieving generativeness is to assemble a diverse body of employees.

Employee diversity (using the term in its widest sense, to encompass identity, interests and talent) allows companies to bring together many different viewpoints and approaches, which creates the opportunity for unexpected, serendipitous ideas.

Consider Florence during the Renaissance. The wealthy Medici family played an instrumental role in creating a culture of creative and innovation, by bringing together talented artists and scientists from all different backgrounds.

So instead of seeing diversity as something that's purely a public relations issue, try to create your company's own version of the Florentine Renaissance. Because as we've seen, diversity isn't just about making your company look good in the press; it's critical for innovation.

And yet, making diversity visible is still a vital part of keeping your company accountable for proactive inclusion. But in order to do this, companies need to allow their employees to be themselves.

What do we mean? Well, many business leaders worry that employees' behavior on social media will affect the company's image: _should employees be allowed to list their company affiliation on their social media profile? What if they say something inappropriate?_

But stop worrying: companies that value diversity must be prepared to accept that they employ varied, human individuals — in all of their multifaceted complexity.

And although companies can't control what their employees say or do on social media, the risk of a public relations kerfuffle is more than worth taking — because you'll be promoting a truly inclusive and generative company culture.

> _"Creating human organizations means letting your people actually be human."_

### 8. Promoting a sense of collaboration will produce multidimensional growth at your company. 

In the previous blink, we discussed how companies can become more generative by fostering diversity. But it's also worth noting that promoting collaboration also plays an important role in advancing generativeness within your business.

To that end, companies first need to develop a narrative around their brands. That's because in today's world, simply having a good product or providing good service isn't enough. Your customers need to have a clear idea of what your company stands for. _How is your business doing good? What's the story behind your products? And what role can customers play in shaping the brand?_

This last question is an important one to address, because allowing consumers to collaborate with management in developing the brand image is a surefire way of ensuring that they become invested in the company.

Consider Nokia. The cell phone giant has integrated open collaboration into its processes through a proprietary platform called IdeasProject.

IdeasProject is unique because it treats collaboration like a two-way street: it allows customers to propose ideas they'd like to see Nokia adopt, and it also gives the company a platform to share ideas that were generated within the company but that aren't being used.

For Nokia, the advantage is clear: the company is able to harness the hive mind to generate new ideas. But the open nature of the channel also creates opportunities for the public and for developers to create new products based on concepts generated within Nokia. So the cell phone giant becomes more than a private company — it becomes a force benefiting the common good.

And that is the very thing which makes a company generative. Generative growth is never simply a two-dimensional expansion of profits; rather, it's all about multi-dimensional growth that benefits the system as a whole.

The advantages of promoting collaboration are clear, but your company still has to take a risk to pursue this next level of transparency. So what can your company do to become more courageous?

### 9. Courageous companies know how to learn from their mistakes and embrace failure. 

Here's a frustrating but common workplace experience: someone comes up with a brilliant idea, but management worries it's "too risky."

That kind of thinking stunts growth and innovation, but companies can avoid it by becoming more _courageous_.

So, what's the most important feature of a courageous company? Well a courageous company takes action without knowing the outcome.

Netflix is one example of a company that consistently takes risks. Consider its innovative monthly flat fee streaming service and its strides into original programming: In both instances, the company wasn't following anyone's example. Rather, it showed courage and took action in the face of uncertainty — with stupendous results.

Of course, not all companies have the same degree of marketing savvy. When other companies experiment, the results can sometimes be disappointing.

But failure needn't be a deterrent if you've established a _learning culture_ — that is, if your company knows how to learn from its failures.

One way to learn from failure is to encourage your company to engage in open-ended conversations. This can be challenging at first, because the way most businesses conduct strategy meetings tends to be predetermined: someone introduces a problem; then there's a discussion about it; and finally the team decides on a solution.

But to really learn, you have to be willing to embrace unexpected conversations that no one could have imagined in advance.

So for example, try finding solutions at the beginning of the conversation, not at the end. And then ask yourself: how might those solutions prompt further questions about the organization's processes?

Clearly admit your mistakes is another important aspect of learning. It may seem counterintuitive to feel good about failure, but if you want to be part of a learning company, knowing how to embrace failure is essential.

Google co-founder Larry Page articulated this view when he said that he wants to run a company that moves too quickly and does too much, rather than one that's too cautious and unwilling to take risk.

> _"Courage is not the absence of fear. Being courageous is taking action, despite the presence of fear."_

### 10. Create space for experimentation at every level in your company. 

Of course, courage isn't only about accepting failure. It's also about figuring out how to act and move forward. And that's where experimentation comes in.

Experimentation isn't about random change — or doing things differently "just because." Rather, it's about doing things that could be beneficial for the company.

It can be difficult for risk-averse managers to embrace this approach — and for good reason! The metrics we typically use to measure performance are geared towards evaluating immediate results, not towards assessing innovation. After all, when was the last time that your boss gave you permission to fail, so long as you failed in a new, different way?

But experimentation can only arise when we create space for it. And the often-cited 20% time policy used by Google (which led to the creation of popular products like Gmail) exemplifies the notion that companies can encourage experimentation.

This doesn't mean your company has to create policies that are as big or well-defined as Google's model.

Software company NFi Studios, for example, creates one-day developing binges for programming teams to hammer down important elements of the product. Because of the low time investment, employees feel less pressure to get it "right." This allows the company to experiment and fail without expending a lot of time or resources on the process.

But to truly embrace experimentation, your company has to come up with new evaluative metrics.

For example, you could develop multiple subject lines for a single email marketing campaign, and measure the click-through and open rates in order to make decisions about how to formulate subject lines for future emails.

After all, it's much easier to justify experimentation and to harness its power when you can measure its effects.

But whatever you decide to do, remember that you have the power to humanize your organization.

### 11. Final summary 

The key message in this book:

**Although social media is revolutionizing business by making it more** ** _human_** **, many companies haven't adjusted to accommodate these changes. Although each company needs an individualized approach to adapt to the social media age, business leaders can pursue four general goals to humanize their organization:** ** _openness_** **,** ** _trustworthiness_** **,** ** _generativeness_** **and** ** _courageousness_** **.**

Actionable advice:

**When you hear yourself speaking about your company in mechanical metaphors, take a step back.**

Think about why you are using such technical language, and consider how it might be influencing your business. Try to use language that enhances the more _human_ aspects of your company: So think in terms of concepts like, for example, family, emotions or desire.

**Suggested** **further** **reading:** ** _Profit from the Positive_** **by** **Margaret Greenberg** **and Senia Maymin**

_Profit from the Positive_ explains how leaders can increase productivity, collaboration and profitability by using the tools of positive psychology to boost their employees' performance. It gives clear examples of how small changes can make big differences.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jamie Notter and Maddie Grant

Jamie Notter and Maddie Grant consult with organizations on culture change through their firm Culture That Works, with specific expertise in technology change and internal (employee) and external (customer) engagement.

