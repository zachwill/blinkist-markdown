---
id: 56661047cb2fee000700001e
slug: humans-are-underrated-en
published_date: 2015-12-10T00:00:00.000+00:00
author: Geoff Colvin
title: Humans are Underrated
subtitle: What High Achievers Know that Brilliant Machines Never Will
main_color: 216BA6
text_color: 1C5A8C
---

# Humans are Underrated

_What High Achievers Know that Brilliant Machines Never Will_

**Geoff Colvin**

How is the rapidly developing world of computers going to affect our jobs in the future? In _Humans Are Underrated_ (2015), Geoff Colvin explores the ways in which computers will surpass us, and the ways they won't. He reveals which skills you should build to remain economically viable, and how you can turn the monster of technology to your advantage.

---
### 1. What’s in it for me? Learn what differentiates you from a computer. 

It's no surprise to anyone that computers are getting smarter. But exactly how much smarter? Smarter than you? No, you might think. And you might very well be right. But what happens when computers start detecting human emotions? And are you quite sure that an emotionally enabled computer wouldn't kick your ass if you were to battle in the art of storytelling? 

Well, these blinks introduce you to some important advances that computers have made in recent decades. And they are not insignificant. However, you'll also learn about those human skills that, as the title implies, do not even compare to — or cannot be substituted by — those of a computer. Because humans are indeed underrated.

In these blinks, you'll discover

  * what the "Love Machine" is all about;

  * how a computer is able to detect your emotions;

  * why spending hours and hours in front of a screen is a very bad idea; and

  * who is more likely to win the storytelling battle — humans or computers.

### 2. Don’t compare yourself to a computer, because you will lose. 

Wouldn't it be nice to increase your brain power by 100 percent every two years? Impossible, you say. 

But not for computers.

According to Moore's law, IT systems increase their computing power by 100 percent every two years. In the long run, that's a _big_ increase. For example, Sony's first transistor radio had five transistors and would barely fit into a pocket. Today, Intel's latest processor has five billion transistors and it fits in the palm of your hand.

So, will computers become infinitely powerful?

Not quite. Eventually Moore's law will end due to sheer physical limitations: you can only fit a finite number of transistors in a certain space. But until that time, you'd better not compare your brain power to a computer.

In fact, nowadays computers can surpass us in tasks we usually think are uniquely human.

For example, computers can detect emotions more effectively than humans. Paul Ekman, a famous psychologist, discovered _micro-expressions_ : the minimal movements in your 40 facial muscles that lead to certain expressions. After many years of research, Ekman figured out which of 3,000 different micro-expressions is connected to which emotion. The result is his _Facial Action Coding System_.

This is how it works: If you put all this emotional data into a computer equipped with a camera, and point it at a human face, the computer can correctly detect the emotion 85 percent of the time. Whereas humans, even with training, got it right only 55 percent of the time!

### 3. Technology is changing us more than we think – for the worse. 

Have you ever thought about how much your cell phone or tablet affects you? 

It turns out that the use of phones, computers and other "screens" decreases our social skills.

In today's world it's easy to spend hours in front of a screen without even noticing. The problem is that the more time we spend in front of screens, the worse our social skills, like reading body language or understanding emotions, become.

In fact, when sixth-graders stayed at a screen-free camp for only five days, researchers found a substantial increase in the children's emotional understanding. They did two more tests to verify the results, and each time they were statistically significant.

But what about social media? Doesn't that bring us closer?

Nope. It turns out that social media isn't that social after all. Originally, humans evolved to be sociable in order to stay connected to a tribe that could keep us safe and provide us with food. 

But today we can maintain connections from our computer at home, and find our food in the fridge. Even though at first glance that seems like a revolutionary development, it's actually the opposite. 

For example, a study showed that US teenagers who use a lot of social media are less likely to have good relationships with their parents or their peers, and are often unhappy. This might be because bonding via social media is less effective than in person or over the phone. Another reason might be that social media users typically become less trusting, so the quality of their relationships suffers.

So although computers do improve our lives in many ways, we should be careful when using them, especially since social skills like empathy have become more important — as we'll see in the next blink.

> _"People who use social networks seem to become less trusting than people who don't."_

### 4. Social skills are becoming a more important asset for humans than knowledge. 

The old saying goes: Knowledge is power. These days though, knowledge is increasingly stored in computers, with little need to keep it your head.

So where does this leave us humans?

We have to shift our main focus from acquiring knowledge to other uniquely human capabilities — like social skills.

Consider a lawyer: She has to possess skills ranging from the ability to analyze a case to figuring out a strategy to support her position: all time- and cost-intensive labor. But today a computer can analyze millions of cases and find all the supporting literature in a bleep. Computers can even predict the outcomes of Supreme Court decisions more accurately than legal experts!

So do we no longer need lawyers?

We do need them, but their focus is shifting toward skills that computers cannot replace: social skills, like building an emotional connection with a client, and helping them act in their own best interest.

But there's another reason why social skills are becoming ever more crucial.

As the world becomes more interconnected, people from different cultures are interacting more and more. One step out of place can be taken as a deep insult — and in the wrong context, it can be deadly.

For example, in 2004, during the war in Iraq, an American marine drove too close to the third holiest site in Shia Islam, the Imam Ali Mosque, causing an uproar. If the soldier had known better, they could have avoided the conflict.

But when harnessed, social skills can be incredibly powerful. A few weeks earlier, a group of American soldiers was surrounded by hundreds of enraged Iraqis. But instead of attacking them, the soldiers used their knowledge of local culture, and knelt down, pointing their guns to the ground. This simple use of body language calmed the Iraqis down, and the soldiers could withdraw in peace.

### 5. Humans will always require the skill of empathy. 

Have you ever had a bad experience with a call-center person? It's partly because they typically have to follow rigid scripts instead of having genuine _empathy_ for your problem.

We find this irritating because empathy is a basic element for every relationship — even in business.

In fact, being empathic — understanding what the other person is feeling — is becoming more important in every area of the economy. Whether you show it as a doctor or a call-center worker, empathy is the first step of every meaningful relationship.

For example, consider how American Express is using empathy in business. When Jim Bush was placed in charge of AmEx's call centers, he threw out all the scripts. Instead, the workers received information about each individual customer, and could act as they personally felt right. This led to higher recommendation scores from customers, higher profit margins and a 50 percent reduction in the employee attrition rate.

And the good news about empathy is that it's a skill everybody can learn that will probably never be replaced by a computer.

Empathy consists of two elements: understanding the thoughts and feelings of others, and reacting appropriately. For example, an empathic doctor would try to understand what the patient is going through and show an appropriate level of concern.

And this is where we have an advantage over computers.

We are hardwired to only accept empathy in its most powerful form from fellow humans, so no matter how elaborately a computer responds to our problems, we simply won't be convinced.

> _"Customers know instantly when a service professional really cares."_

### 6. Team building is becoming more important in every area from golf to business. 

Have you ever worked in a team where things just didn't click?

While everyone in a team can bring great skills to the table, their different personalities may be a bad fit.

So what determines the success of a team? While many factors such as size, stability or appropriate rewards do matter, they are not as important as _social sensitivity,_ meaning the social abilities of the subject. 

Consider the US golf team in 2008. Their coach, Paul Azinger, wanted to assemble a team that would win the Ryder Cup. In previous years the United States had lost five out of six tournaments because they couldn't work together as a team. So Azinger tried a new approach: instead of choosing the players with the best skills, he selected those whose personalities would best fit together. He made sure that each golfer felt comfortable and understood in the team. The result? One of the biggest US wins in the past 25 years.

F. Scott Fitzgerald once said, "No grand idea was ever born in a conference," but today he might be wrong. Being able to work together in a group is crucial for the success of any organization.

In the world of scientific research, the most influential work is generally conducted by teams. This system allows each member to specialize in specific topics, helping them break into new territory faster.

Teamwork is also critical in the business world. In addition to the CEO, there's usually an entire team running a business, like the chief financial officer (who emerged in the 1970s) and chief information officer (a product of the 1980s), as well as many other coordinating roles. This kind of team composition is essential for success today.

### 7. A good story is more convincing than logic. 

We've all left the cinema after experiencing a movie with a great plot that has moved us to tears.

But did you know that if you can tell a good story like that, you'll find it's more convincing for people than a purely logical argument — and can change millions of lives?

Consider Stephen Denning, the director of the Africa region at the World Bank. He realized that the World Bank had an incredible amount of information on important subjects like malaria. If this information were made accessible to, for example, Zambian health workers, they could use it to help millions of people. 

At first Denning tried to sell his idea to his colleagues with logic, charts and slides, but they didn't listen. So he tried a new method, and told them the story of a health worker in Zambia logging onto the World Bank's site to access all the information about malaria. 

The problem: the worker couldn't access the information. So despite the wealth of information that the World Bank possessed, the bank wasn't actually making any difference.

This story led to a change in the World Bank's strategy, which eventually helped millions of people.

Denning used a skill in which humans have a clear advantage over computers: he told a good story.

Stories exist to "move people to change," but a computer-written story will never achieve this because it lacks authenticity. We humans want to know and evaluate the teller of the story, and computer-written stories simply seem suspicious. Even though in recent years computers have been getting better at writing stories, from simple reports on sports matches to more complicated narratives, they still do not beat those of humans.

### 8. Computers can be creative too, but true breakthroughs come from human interaction. 

Have you ever made a meal from a recipe devised by a computer? You haven't? Well, if you get the chance, you should try it!

Computers can indeed be creative, and cooking is a great example.

IBM taught their supercomputer Watson to be the ultimate creative cook. At first, Watson scanned thousands of existing recipes and all of the food combinations involved, such as tomatoes and oregano. He was also "fed" the chemical profiles of thousands of ingredients. He was then asked to create brand new recipes. 

One of the results, the Austrian Chocolate burrito, which included the astonishing combination of ground beef, dark chocolate, mashed edamame, apricot purée and cheese, was sold from a food truck at the South by Southwest festival in Austin — and people loved it.

Clearly then, computers can perform creative tasks. But it seems true creative breakthroughs are still generated by human interaction.

Consider really creative companies like Apple, Google and Pixar. They actively encourage creativity by increasing "random" interactions between employees — interactions that make the innovation sparks fly.

For example, Google serves really good food in their cafeteria. As a result, everybody goes there. When standing in line, people often start talking with someone they wouldn't normally interact with. There are also long tables instead of small ones, which increases the chances of sitting next to someone unknown. 

Then there's Steve Jobs' methods: he designed Pixar's headquarters with one central meeting spot where everyone interacted. And at Apple, he was famous for his face-to-face meetings, which got everyone's input directly.

> _"Creativity comes from spontaneous meetings, from random discussions." — Steve Jobs_

### 9. Harness the power of computers to boost your knowledge and social skills. 

As you've seen in these blinks, humans are still superior to computers in many areas. But it gets even better: computers can also help us to excel. 

Take education, for instance: it turns out that learning at a computer is more effective than learning in a crowded classroom.

For example, the US Navy uses software to teach students how to repair technical systems on ships, a purely knowledge-based task that doesn't require any human interaction.

Then there's Stanford University, which in 2011 launched the first _MOOC_ or Massive Open Online Course on artificial intelligence, inviting everyone to follow it from their own homes around the world. Eventually, some 160,000 students from 190 countries registered, but the biggest shock was that the top 400 performers were not the elite students in the parallel classroom course — they were the online participants!

Nowadays there are even ways of learning the social skills crucial to your success by simply using software. 

For example, there's a piece of software called _Love Machine_ that helps employees build better social skills by incentivizing them to interact with each other. With it you can send a thank you message to a helpful colleague. The twist is that all the other employees can also see your thank you message. The goal is to encourage employees not to hide and protect their knowledge, but instead to share it and help others. 

As it turns out, the thank you message creates an incentive for other people to help. It sometimes even develops into a competition about who can be the most helpful!

> _**"** Learning basic concepts online is enormously faster and more effective than doing it in a classroom."_

### 10. Final summary 

The key message in this book:

**Technological progress is inevitable, but has both positive and negative consequences. If we humans want to keep our jobs, we need to build up human skills that computers can't develop, such as empathy, and take action to manage technology's negative effects.**

Actionable advice:

**Use technology to enhance your skillset.**

Take advantage of the huge range of online learning communities and learn something for free that will boost your skillset — and your résumé.

For example, you can learn leadership, decision-making or project management skills with thousands of other learners, and even get a certificate from the university when you successfully complete the course.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Geoff Colvin

Geoff Colvin is a journalist, broadcaster, author and speaker who's written books like _The Upside of the Downturn_ as well as the bestseller _Talent is Overrated_. He studied at Harvard and received an MBA from New York University.

