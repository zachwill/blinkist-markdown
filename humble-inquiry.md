---
id: 547c81a16363300009d30300
slug: humble-inquiry-en
published_date: 2014-12-01T00:00:00.000+00:00
author: Edgar H. Schein
title: Humble Inquiry
subtitle: The Gentle Art of Asking Instead of Telling
main_color: 629FAB
text_color: 3F666E
---

# Humble Inquiry

_The Gentle Art of Asking Instead of Telling_

**Edgar H. Schein**

_Humble Inquiry_ (2013) sets out the basic principles of the art of asking the right questions in the right way. It examines how your approach to inquiry affects your relationships at the office, your ability to get quality work completed and, ultimately, your success as a leader.

---
### 1. What’s in it for me? Learn communications skills help you get the best from your team. 

It's Monday morning, and your boss calls you in and says, "I've noticed that your work is poor, and I think that you're approaching the problem incorrectly…"

Put on the spot like this, how do you think you would feel? Probably pretty disheartened and downcast, right? What's more, rather than trying to improve your work, you might start resenting your boss.

Now imagine a different approach. Your boss says "How's it going with your work? Would you change anything?" With this subtle shift, you look at the situation differently, and feel engaged instead of put down.

These blinks show the power of "humble inquiry," or, in other words, how to ask instead of tell. The secrets of communication that will be revealed are vital for leaders in any sphere.

In the following blinks, you'll also learn:

  * what the best way is to win a relay race;

  * how you should behave in the presence of a Nobel Prize laureate; and

  * why we shouldn't shout at children.

### 2. If employees can’t freely express what they’re thinking, you can’t build a successful team. 

What comes to mind when you think of a great team? Consider some of your favorite sports teams whose courage and unity on the field resulted in a winning season.

So what makes a great team? Is it enough to put a bunch of talented people in the same room?

Well, no. What makes a great team is the _relationships_ between team members.

_Strong communication skills_ cement these relationships. Each member of the team, from the quarterback to the lineman, is comfortable telling each other anything.

"Go left!" "I'm open!" "Great pass!" Each player expresses what they need to say without fear; opinions are voiced freely and everyone gets a chance to participate.

Many leaders in business try hard to emulate the teamwork found in sports teams they admire, but ultimately fail due to communication barriers, often between managers and employees.

Employees are often simply too afraid to share with their boss any issues they might have, fearing that voicing their needs or criticism will reflect poorly on them. Think about it: would _you_ feel comfortable walking up to _your_ boss and telling him the company strategy was full of holes?

This lack of communication can have terrible consequences. For example, it was precisely this sort of communication breakdown — the inability of information at the lowest levels of a company to reach those at the top — that led to the devastating Deepwater Horizon oil spill in the Gulf of Mexico.

Businesses leaders therefore need to create and nurture an environment where employees feel that they can freely share their thoughts and ideas, no matter what they are.

This is, of course, easier said than done. The following blinks will show you how.

### 3. To build a good team, you need to ask questions that show trust and include and respect others. 

Imagine you're the head of your department and, to build morale, you challenge another department to a relay race.

You're the first off the mark, and as you reach the first swap, you shout "Stick out your left hand!" to your employee so you can pass her the baton.

As you're the boss, she reflexively sticks out her left hand. Unfortunately, she has a finger injury which causes her to drop the baton just as you've slapped it into her hand.

What could you have done differently? You have to remember that, as the boss, people might be scared of disagreeing with you. For this reason, you'll have to rely on the power of _humble inquiry._

Humble inquiry is about asking questions in a way that shows your colleagues that their perspective matters and, importantly, that you respect their decisions.

Before the relay race, you could have easily asked your colleague, "In which hand should I place the baton?" rather than simply making a demand.

Humble inquiry is more than just a strategy for formulating questions; it's an attitude.

As the chair of a department composed of 15 professors, the author once received a note from the dean that the department's phone bill was too high. He was also given a list of calls made by each professor to aid him in locating the problem and resolving it.

The way he saw it, there were three ways to solve the problem.

He could get the professors together to go through the list collectively; he could go through the list on his own and call the guilty parties individually; or, he could send the list to each professor, tell them about the problem and ask them how to fix it.

The third option put the most trust in and dependency on his fellow professors, so he went with that option. And it worked!

Several faculty members admitted making personal calls and racking up charges, and so promised to stop.

### 4. Humble inquiry takes on many different forms, depending on how quickly you need an answer. 

There are many ways to ask a question. You could beat around the bush, hoping you'll get the information you need without offending anyone. Or you could cut right to the chase.

Yet, however you decide to ask, do it humbly. But what does humble look like in practice?

When you use humble inquiry, you're showing that you really want to know what's in the other person's mind. When we say "really," we mean it. Humble inquiry is sincere by definition.

In addition to this, people will know how much you really care based on subtle cues, such as body language and tone of voice.

Ken Olsen, the founder of Digital Equipment Corporation, liked to wander around the company offices and occasionally stop at an engineer's desk to ask, "What are you working on?"

This simple question, a model of humble inquiry, would lead to a conversation that was pleasing both personally and professionally. Not only did these questions help Olsen get to know his employees, but also he knew exactly what each of his engineers was doing.

It was this inquisitive yet humble style that earned Olsen the respect of his 100,000 employees.

Sometimes, however, you need to dig deeper or steer a conversation in a direction that interests you. This is where _diagnostic inquiry_ comes into play. Diagnostic inquiry helps you learn more about a specific point by asking questions that are directly relevant to the point.

For example, if you're with a friend who is talking about how he's recently changed jobs, you could ask diagnostic questions such as:

  * What made you decide to make a move?

  * What could have caused this?

  * Why did it make you feel that way?

Diagnostic inquiry should also be humble in the sense that the person you are questioning doesn't feel offended by your queries, and feels their input is valued.

### 5. Use humble inquiry to steer conversations and to evaluate their quality. 

Humble inquiry can be further divided into two other types of inquiry, depending on what you need to accomplish.

For example, if you need to push a conversation in a certain direction, you can use _confrontational inquiry_. This type involves introducing your own ideas in the form of a question.

With confrontational inquiry, you're still curious and interested in the conversation, but also feel you could add something meaningful or get important information from your conversation partner.

For example, if during a meeting you notice a few colleagues squirming in their chairs, you might ask another coworker afterwards "Do you think they were squirming because they were frightened?" rather than simply, "Why were they squirming like that?"

Yet remember that the person you are questioning should never actually feel _confronted_ by your questions; they must know that your motives are always pure.

Speaking of motives, it's important to identify your own before engaging in confrontational inquiry. If you see a conversation as merely a way to test your assumptions, then don't use this strategy.

Only use confrontational inquiry when you want to help your conversation partner and you want to learn more about their perspective.

To ensure that a conversation is based in humility, you might need to focus in on the conversation itself through _process-oriented inquiry_.

Sometimes, you'll notice that a conversation has become derailed in a way that suggests that your conversation partner is uncomfortable. In these cases, you might need to probe for more information about how your partner feels about the conversation itself.

For example, you can ask her things like: "Are we still OK?", "Have I offended you?", "Is this conversation moving in the right direction?" or "Am I being too personal?"

This kind of inquiry focuses on the relationship between you and your conversation partner, and ensures that neither party is stressed and that everyone's expectations are being met.

So far, you've learned about what humble inquiry is and how it can be applied. The next blinks will detail the common hurdles that stand between you and open communication.

### 6. Just “getting things done” stands in the way of good communication, and stymies humble inquiry. 

Have you ever asked a coworker for help with a project, only to have them to reply bluntly: "No. Can't you see I'm busy with my own work?!"

Harsh answers like these often make us feel deflated and unhappy.

Unfortunately, this attitude is deeply ingrained in today's _task accomplishment culture_, and it can have dire consequences for your business.

In today's American workplace, people earn status by completing the tasks that they're assigned. So if you work as a tax analyst for the Internal Revenue Service, you'll be given a list of accounts to audit and will be expected to make it through the entire list.

Those people who are the best at going through the list and accomplishing all their tasks are usually the ones rewarded with a promotion. These newly promoted employees are then expected to manage those "below" them by dictating their tasks to them.

This approach is known as _do-and-tell_, and it fosters a culture where those at the top disrespect those who are subordinate to them. 

Not only is this disrespect unwarranted, it also poses an immediate barrier to good communication within a team.

For starters, do-and-tell favors telling over asking — so much so, in fact, that asking questions is viewed as a sign of weakness and incompetence.

A manager who asks his employees "What can we do here?" is viewed as someone who doesn't know enough about his job to earn his job title.

Telling, on the other hand, is seen as a virtue! In a revealing example, when the author asked management students what it means being promoted to manager, they answered, "It means I can now tell others what to do."

This attitude is deeply flawed, and only stands in the way of good communication and thus good work.

### 7. Our obsession with status or social rank gets in the way of humble inquiries. 

Rank plays an all too important role in business and culture today. For instance, if someone saw a CEO golfing with the office janitor, that would be hot company gossip the next day.

Why is this? This obsession with status and rank merely places unnecessary barriers between people, and prevents fruitful relationships.

In fact, these roles and status symbols can impact how and whether we engage in humble inquiry.

When we meet new people, we immediately start asking questions to determine whether they have a higher or lower status than we do, and our attitudes toward them change accordingly.

For example, the author was at a meeting when some undergraduates approached him and asked to have their picture taken with him. His automatic assumption was that, as a professor, he had a higher status than they did, and that he should just act flattered and pose with a big smile.

In contrast, when he was later introduced to a Nobel Prize-winning physicist (who the author felt had a higher status than he did), he was overcome with awe and so his demeanor changed: he became more respectful and made more humble inquiries.

As this example shows, we're a lot less likely to make humble inquiries if we believe that we have a higher status than others.

So how does all this play out in the professional world?

In do-and-tell organizations, we naturally demonstrate humility only to those who we feel have control over us in some way. In contrast, when we're in charge, we treat others with less respect.

If you're buying a suit at a store, for example, the sales associate will likely be very respectful toward you — perhaps even too respectful! It's unlikely, however, that you, as the customer, will demonstrate the same degree of respect.

Yet when you're having your suit tailored, you're much more likely to respect the directions of your tailor, because he is in control.

### 8. Having the right mind-set helps us better communicate, and makes humble inquiry easy. 

We've seen how social status gets in the way of humble inquiry. Now we'll look at how our mind-set affects which questions we ask.

We naturally don't like criticism. As a result, most of us make a concerted effort to hide our feelings.

This, of course, has consequences. Playing it safe, for example, harms our ability to form relationships, which are vital to humble inquiry.

So how can you avoid this mind-set? One way is to intentionally open up to others by revealing something to them about yourself. You will soon find that, having demonstrated that it's safe to express yourself, it will be easier for the rest of your team to open up.

If you like karaoke, for example, you might tell your colleagues how you love to sing and describe your favorite karaoke bar. Someone else then might share details about her own hobby, and soon the team will have a much stronger, healthier relationship.

In addition, we have a natural inclination to approach situations from a personal bias rather than taking a more objective stance. Of course, this can greatly damage our ability to communicate.

One of the author's students was studying for an important exam in his basement. He heard his six-year-old daughter knock at the door, despite having already told her not to interrupt him.

Angry, he shouted for her to go upstairs, causing her to run off crying.

The next day his wife told him that _she_ had sent their daughter to the basement to say goodnight and to ask him if he wanted any coffee to help him study.

If the student had simply adopted some humility and _asked_ his daughter what she needed, he could have avoided becoming angry and scaring her!

Let this be a lesson: always make sure you know what's going on before rushing to judgment.

### 9. Final summary 

The key message in this book:

**Good communication relies on good relationships. The practice of humble inquiry helps you demonstrate trust as well as interest in your conversation partner, which, taken together, acts as the underpinnings of a solid, meaningful relationship.**

**Suggested further reading:** ** _Secrets_** **_of_** **_Dynamic_** **_Communication_** **by Ken Davis**

_Secrets of Dynamic Communication_ explains how to prepare and present a speech effectively. Through the author's own SCORRE-method, we are guided through the six key components of a successful speech: subject, central theme, objective, rationale, resources and evaluation.
---

### Edgar H. Schein

Edgar H. Schein is the Society of Sloan Fellows Professor of Management Emeritus at the Massachusetts Institute of Technology and has received many awards and honors throughout his career. He has also penned a number of books, including _Organizational Culture and Leadership_ and _The Corporate Culture Survival Guide_.

