---
id: 5a6f1187b238e10009732067
slug: humility-is-the-new-smart-en
published_date: 2018-02-02T00:00:00.000+00:00
author: Edward D. Hess and Katherine Ludwig
title: Humility Is The New Smart
subtitle: Rethinking Human Excellence In the Smart Machine Age
main_color: 559976
text_color: 39664F
---

# Humility Is The New Smart

_Rethinking Human Excellence In the Smart Machine Age_

**Edward D. Hess and Katherine Ludwig**

_Humility Is the New Smart_ (2017) is your ticket to success in the new smart age of machines. These blinks are a practical guide for thriving in a world that's increasingly run by machines, both on and off the job.

---
### 1. What’s in it for me? Learn the skills you need to secure your future. 

The cult classic sci-fi film _Blade Runner_ featured flying cars, powerful artificial intelligence and a world unlike any that had ever been seen. But movies like this rarely if ever depict the nitty-gritty of the technological advancements we can expect to witness in the future. For instance, what will a factory floor, a call center or a real-estate office look like in the year 2100?

Transformative technologies like flying cars may sound cool, but the changes to the working world are likely going to be more momentous. So what can we expect from the future of work? And more importantly, how can humans prepare for a world dominated by smart technology and robotic intelligence?

Well, as AI takes off, humans will need to secure a professional niche for themselves or risk falling into obsolescence. The key to doing so is humility — cultivating interpersonal skills and collaboration and leveraging the creativity that sets us apart from machines.

In these blinks, you'll learn

  * how machines are changing the world as we know it;

  * the four skills you need to succeed in the future; and

  * what it means to truly have humility.

### 2. The technological advancements of the new millennium will force humans to work and behave differently. 

The second millennium may be young, but we're quickly approaching one of its defining eras: the _Smart Machine Age_, or _SMA_. This term describes a time when machines will become increasingly capable of performing complex tasks and nonroutine work — jobs that once required human labor.

As the SMA dawns, technologies like robotics, artificial intelligence, nanotechnology and genetic engineering will quickly come to dominate the professional and personal lives of humans. As they continue to become more sophisticated, machines will take on more manual and cognitive tasks, both in and outside the workplace.

What does this mean for humans?

Well, if you're a lawyer, a journalist, a teacher or an accountant, you can't be too certain that your job is secure. It could be taken over by a robot in the future. In fact, a 2013 study done by researchers at Oxford University found an extreme likelihood that as many as 47 percent of US jobs will be replaced by technology within the next two decades.

So human success in the SMA depends on a whole new approach; we must be _NewSmart_.

After all, we can't compete with machines in terms of processing and retaining huge amounts of information. Our options are either to complement the work of machines or do the work they can't, which includes critical thinking, emotional engagement and creative practices.

However, in a society that's hyperfocused on competition, aggression and individual success, we're often too self-involved and fixed in our beliefs to be adept at such skills. This impedes progress. We will only excel when we learn to collaborate, an ability that enables greater critical thinking, emotional engagement and creativity.

But our general unwillingness to work together doesn't mean humanity is doomed. Indeed, there's a way to acquire the skills that will help you transcend your current limitations. In the next blinks, you'll learn all about it.

### 3. Cultivating four abilities will help you rise to the challenge of the Smart Machine Age. 

So what skills are key to success in the Smart Machine Age?

Well, the first is known as _quieting ego_. This refers to the ability to trim back your emotional defensiveness, directing empathy toward others by letting your guard down and becoming more objective, open-minded and open-hearted.

For instance, when you get unflattering feedback from a friend or colleague, you should welcome the criticism. Avoid the very human impulse to tell yourself that he's just wrong.

In addition to quieting your ego, you should also focus on _managing self_. This means fostering a healthy and controlled approach to your thoughts and feelings. Without this skill, you'll grow preoccupied with your fears and insecurities, which will prevent you from connecting with others. And this may deprive you of the power of critical and creative thinking that can be derived from human collaboration.

For example, many people fear that they won't be accepted for who they are. This anxiety can be so pronounced that it actually prevents them from interacting with others at all.

The third skill you need is _reflective listening_, which is a way to free your thoughts and perceptions of the world, and transcend your cognitive and emotional biases. Reflective listening is about taking the time to truly understand other people and their ideas. Practicing this skill will help you open your mind and challenge your preconceived notions of the world. With a more open mind, you'll be better at forging productive, collaborative relationships.

And finally, to succeed in the SMA, you need to master _otherness_. This skill simply refers to the ability to establish strong connections with other people. In the SMA, a focus on creative thinking and emotional connection is of the utmost importance — so it's clear why mastering otherness is necessary. Before you collaborate with others, you first need to connect with them.

### 4. Being able to think of others and accept their perspectives is at the core of SMA success. 

****Now you know which skills will help you navigate the SMA, but before you begin developing them, you need to focus on making one important change; you need to transform your personal beliefs.

For the vast majority of us, our worldview doesn't sync well with the skills required by the SMA. To overcome this obstacle, you've got to change your _mental model_.

The mental model is the sum total of your personal ideas, beliefs and perceptions of the world. In other words, it's everything that has shaped your experience and understanding of the world, from your education to your culture, your geography to your religion. The issue is that most mental models are based on an understanding of the world that _was_, not of the new world that is to be. This past world was all about individualism and competition, which are anathema to the skills that will benefit you in the SMA.

So success means updating your mental model, and the name of the game here is _humility_. Humility is the foundation for the ideal SMA mental model.

But what exactly is humility?

You might be thinking that it's some religious concept, the idea of being submissive, meek or occupying a lowly position. But humility is actually just a mindset that allows you to lower your defenses and wash away your biases, which, in turn, enables you to perceive the world and meet people as they truly are.

To put it differently, humility is an attitude that lets you operate in a self-accurate, open-minded way that focuses on others, rather than on yourself. The idea isn't to act selflessly or think less of yourself; rather, humility will help you think less _about_ yourself.

If you can master humility, you'll be on track to developing the skills necessary for the SMA. So, with that information in your back pocket, let's move on to how you actually build those other skills.

### 5. Slow yourself down through mindful attention. 

Have you ever tried to carry out a complex task while nervous or upset? Well, just as being upset or nervous can make it hard to perform at your best, so can a nervous or tangled up ego. To break down this roadblock you need to quiet the ego and the best way to do so is through _mindfulness_.

Mindfulness is simply about paying attention; it means purposefully noticing the present moment in a nonjudgmental way. Employing this practice is simple:

Get into a comfortable position, whether it's sitting, lying down or some other arrangement. Then, focus your attention and awareness on one thing and one thing alone. This could be your entire body or a particular part of it, or an abstract concept like compassion or kindness.

Regardless of what the one thing is, focusing your attention on it alone will allow you to experience all of your personal — and thus biased — thoughts in a more detached way. This space will allow you to recognize when these thoughts are interfering with the attention you're trying to direct toward someone or something. Eventually, you'll get better at letting go of your biased thoughts, and, soon enough, you'll no longer have to identify with or act on them. Thus, you'll learn to focus less on your ego.

From there, it's but a hop, skip and jump to mastering self-management.

Self-management begins with slowing down and thinking deliberately. Some people protest at this point, thinking, "If I slow down, people will think I'm lazy or incompetent." However, such thoughts are simply your fear and insecurity at play — your worries of not being accepted or respected.

The reality is that slowing down is essential to your success in the SMA as it will allow you to act and think deliberately. After all, if you rush through life you'll have a hard time doing more than going through the motions. You'll never have the time to manage yourself and consider what you actually should be doing or thinking.

> _"...slowing down makes it easier to know when to switch from autopilot to intentional thinking mode…."_

### 6. Use simple tools to listen to others and greet them with your full attention. 

How do you become a reflective listener?

Getting started is as simple as focusing on the other person, keeping an open mind and asking clarifying questions. Making an effort to understand other people requires giving them your utmost attention, an act of focus that will prevent you from jumping to conclusions that could result in judgments.

Let's say a lower-level colleague of yours has an idea for how the company can increase sales. You might be her senior, but you should listen nonetheless. If the idea doesn't make sense, ask her why it's important. Who knows — your own preconceptions might be blocking your understanding!

But you should also prepare yourself to listen reflectively using a more pragmatic approach: a checklist. Make one for yourself by writing down every aspect that you consider key to your ability to listen reflectively. You might put down things like, "don't interrupt immediately" or "don't allow focus to drift."

For example, after a workshop on reflective listening led by one of the authors, a participant made a checklist before calling his wife and kids. He reminded himself to ask questions and avoid interrupting his family with stories about himself. Afterward, he said that the conversation was one of the best he and his wife had shared in years.

And of course, you've got to develop your ability to connect emotionally with others. To do so, indicate to other people that you're present and listening, using both verbal and nonverbal cues. This second part is crucial because showing people that you're present isn't just about greeting them; it also means displaying welcoming attention through your actions and mannerisms.

So when you meet a potential collaborator, be sure to make eye contact and welcome her with a smile; greet her with open arms and put down whatever phone or gadget you might be fiddling with.

If you follow all these steps, you'll be much more likely to welcome the other person with your full being, and to forge an emotional connection that lasts.

> _"It makes sense that in our individualistic and competitive culture, knowledge workers have been primed to 'tell' more than 'ask'...."_

### 7. Final summary 

The key message in this book:

**For better or for worse, a new age is dawning on humanity: an age dominated by machines. To succeed in this new time, humans need to focus on the attributes that distinguish us from machines; we've got to focus on emotional connection and collaboration, and on the creative thinking that these two activities give rise to.**

Actionable advice:

**Ask yourself** ** _how_** **you think.**

Managing yourself can be difficult but you can get a handle on your thoughts and feelings by considering _how_ you think. This question of course implies that there are multiple ways to think and, once you start thinking about it, you'll start to notice your different approaches. Maybe you're thinking fast and operating on autopilot. Or you could be giving yourself time to slow down and deliberate.

By simply taking space to consider these different modes, you'll already be one step closer to managing yourself and your mind.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Economic Singularity_** **by Calum Chace**

_The Economic Singularity_ (2016) takes a long, hard look at what the future has in store for us based on the technological progress we've made so far. It's clear that we're moving toward the kind of artificial intelligence that will automate most of our jobs — but how do we plan to deal with this scenario? Find out the challenges we'll face and what we need to do to prepare ourselves for the inevitable.
---

### Edward D. Hess and Katherine Ludwig

Edward D. Hess is a professor of business administration and an expert in organizational learning, leadership and innovation cultures. He's currently an executive-in-residence with the Darden School of Business at the University of Virginia.

Katherine Ludwig was formerly a corporate lawyer in finance and securities. She now works as a research associate at the Darden School of Business.

