---
id: 5a64ab61b238e100076c4e7b
slug: hunger-en
published_date: 2018-01-23T00:00:00.000+00:00
author: Roxane Gay
title: Hunger
subtitle: A Memoir of (My) Body
main_color: DE3442
text_color: AB2833
---

# Hunger

_A Memoir of (My) Body_

**Roxane Gay**

_Hunger_ (2017) is a personal, open-hearted account of what it's like to live with a body that's frowned upon by society.

---
### 1. What’s in it for me? Hear a remarkable story that rarely gets told. 

There are a lot of memoirs out there, but few tell the kind of story Roxane Gay wants you to hear. It might not be a nice one, but it's an important one to tell. Because the truth is, Dr. Gay isn't alone in using unhealthy methods as a coping mechanism for a traumatic event.

In fact, the statistics are quite clear: the United States is struggling to deal with obesity, and if more people hear what Roxane has to say, we might be closer to understanding why all those diet and exercise fads aren't working. We need more open hearts and minds, and, by asking us to understand Roxane's story without prejudice, that's exactly what this story can help us achieve.

In these blinks, you'll discover

  * how a traumatic event causes pain years later; and

  * why _The Biggest Loser_ is exploitative.

### 2. Roxane Gay had her life derailed by a violent and traumatic event. 

Roxane Gay was born to a family of Haitian-Americans who lived in Omaha, Nebraska. In her early years, Roxane was raised Catholic and believed that if she did well in school she could grow up to be a respected doctor.

What she could never have expected was that a tragic act of violence would derail those plans and set her on a completely different course.

When she was just 12-years-old, Roxane was raped by her boyfriend and a group of other local youths. The event was devastating on many levels: since Roxane had already been intimate with this boy, what she experienced was a heavy feeling of shame — as if the attack had been her fault for defying the values of her Catholic upbringing. As a result, she couldn't bear the thought of telling her parents about the rape.

So, she kept it to herself and continued to bury this secret deeper by overeating more and more food.

In the year following the attack, Roxane was sent to a prestigious boarding school where, away from the watchful eyes of her parents, she could eat as much as she wanted. In her mind, food wasn't just a way to punish her body; she believed that the more she ate, the bigger she'd get and therefore the less vulnerable she'd be to another attack.

Roxane was already old enough to understand that fat women aren't what society thinks of as desirable. So, as she quickly began to put on weight, part of her knew she was becoming sexually invisible to predatory men.

For a while, Roxane continued playing the part of the obedient Catholic girl who was expected to become a doctor, and her good grades got her into the pre-med program at Yale University. But as she began her junior year, she couldn't keep up the act any longer — so she dropped out of school to go and live with a potential partner she met online.

At Yale, Roxane felt like she had to deny the reality of who she saw herself as being now: someone ruined. Unable to cope with this any longer, she was about to start a new chapter where she'd be treated as poorly as she felt.

### 3. As a result of guilt and shame, Roxane fell into a cycle of abusive relationships. 

After dropping out of med school, Roxane Gay began a series of relationships with people who were willing to treat her in a way that matched her ugly self-image.

These people were both physically and emotionally abusive.

The physical abuse was something Roxane sought out, especially in her twenties, because she felt like her body had already been ruined during the attack, so why bother protecting it? She already felt worthless, so it made perfect sense that the men and women she was with should treat her like garbage. It also justified her own self-abuse, which she perpetuated by eating as much junk food as she could.

In her relationships, Roxane would fall into a cycle of trying to win the other person's affection — feeling so lucky that they would even consider dating her, only to be constantly criticized and belittled. Once, she made an effort to impress a partner by going to the cosmetics counter at a department store and having herself professionally made-up, but all she got in return was ridicule. She felt so awful that she still avoids makeup.

It got to a point where Roxane wished she was gay so that she would only have to deal with women, who she thought were less likely to be abusive. Through her experiences, Roxane did discover that she was bisexual, but the women she dated were never able to meet the emotional needs she had at the time. And she couldn't deny the fact that she was attracted to men.

Whether it was with men or women, her relationships were filled with drama, and the only role she knew how to play was that of the victim. So time and again, relationships followed the same dreadful pattern.

However, it wasn't just her partners who treated her badly. There have been plenty of strangers who added to her grief as well.

> _"I am always paralyzed by self-doubt and mistrust."_

### 4. Society has little regard for the lives of the overweight or the psychology behind obesity. 

Even though there's a lot of concern about an obesity epidemic in the United States, few Americans make any effort to be sympathetic to overweight people.

The moment Roxane Gay leaves her home, she is always trying to take up as little space as possible. But the people she crosses paths with don't see her as someone trying to mind her own business and go about her day. They see her as a fat lady who's taking up too much of _their_ precious space.

Wherever she goes, Roxane is made to feel like she's imposing. Businesses and public transportation don't have big enough seats, and few clothes are made to fit her body type. When she goes out for dinner, she always worries that the chairs will have armrests, making them too narrow. And even if the chairs don't have armrests, it's not uncommon for Roxane to spend a meal hovering at the edge of her chair, in fear that if she sits back, it might not be sturdy enough.

Generally, the only time an overweight person is acknowledged in public is when they're berated. The message she has received time and again is that she must change if she wants to be accepted.

On popular reality shows like _The Biggest Loser_, we're told that people should do whatever it takes to lose weight. But while the contestants on this show are made to compete to see who can lose the most weight to win a prize, no one ever stops to ponder the underlying reasons they gained weight in the first place. Instead, audiences take pleasure in watching fat people being forced to exercise until they puke and collapse, all so they can fit society's image of an acceptable body.

The truth is, obesity isn't just a matter of eating too much. Quite often there are complex emotional reasons for all that weight gain. It's not as simple as flipping a switch and being able to take control over something that's been out of control for years.

> _"The bigger you are, the smaller your world becomes."_

### 5. It’s hard to escape the humiliation and harassment that comes with obesity. 

While weight gain began as a way for Roxane Gay to become invisible to sexual predators, it had the unfortunate side effect of bringing her unwanted attention in other areas.

One thing that has been particularly humiliating is the way complete strangers use her weight as an excuse to interfere in her life and make decisions for her.

People she's never met have taken food out of her shopping cart, telling her that she shouldn't be eating such things. It's clear to Roxane that people automatically equate obesity with stupidity, which they feel gives them the right to step in and attempt to control the life of an adult.

A different sort of public humiliation occurred during a literature event in front of a live audience.

At the start of the event, all the other participants easily climbed onto a low stage that didn't have any stairs to help climb it. For five agonizing minutes, Roxane struggled to get on stage while the rest of the participants awkwardly waited for her. Once on stage, the embarrassments didn't end. Upon taking her seat, she felt the chair crack under her weight and immediately felt nauseous realizing that she was going to be squatting for the next two hours.

As you can gather, being overweight has provided Roxane with more attention than she's comfortable with, but being black and overweight has made it even more difficult to be invisible.

Roxane chose to live in small towns as an adult. This has been a way to get more privacy and to avoid the pressure a big city can place on a woman to be glamorous. However, small midwestern towns are predominantly white, making her more visible and a target for racial profiling and harassment.

In all her attempts to be invisible, Roxane has yet to find a perfect scenario, but along the way she has found it possible to treat herself in a kinder, more positive way, which we'll look at in the final blink.

> _"I avoid the handicapped stall because people ... give me dirty looks when I use [it] merely because I am fat and need more space."_

### 6. Roxane continues to work on accepting her past and treating herself with more kindness. 

These days, Roxane Gay often finds herself dealing with two types of frustration. The first is directed at herself for being unable to control her weight; other times she's frustrated at society for dictating how she's supposed to look.

But in between these emotions, Roxane is finding that it is possible to be kind to herself.

A big step in the right direction was taken when she found healthier ways to lose weight. For a few years, Roxane was binging and purging — eating a lot and then throwing it all up, otherwise known as being bulimic. But now she's cut back on the junk food and is cooking more healthy recipes at home.

She's also working at having a better relationship with her family, who are very athletic, traditionally beautiful and always pushing her to lose weight and be healthy. While it's always a struggle to change the topic of conversation away from her body, their concern makes Roxane feel loved. Yet their inability to accept her as she is can make her feel all the more lonely.

Roxane is aware that at the heart of her struggle is the fact that losing weight brings up the very same fears that got her overeating in the first place.

Sure, Roxane would love to wear whatever she wants and not have difficulty breathing and walking. But once she drops a few pounds, the panic of losing her protection sets in and the weight goes right back on. At the very least, she is aware of this psychology and is working on no longer feeling the need for that protection anymore.

On the other hand, it's important to Roxane that she loses weight for the right reasons. Part of her wants to change society's attitude toward body politics and let other women know that happiness needn't be so closely attached to your dress size.

Despite this inner conflict, Roxane knows that there's no escaping one's past, and she's OK with her history being part of who she is today. She wants others with traumatic pasts to know that it's fine to be at peace with who they are.

### 7. Final summary 

The key message in this book:

**No one wants or deserves to be talked down to or treated as a second-class citizen just because they're heavy. If someone is overweight, there's a good chance that emotional stress or perhaps even trauma has caused them to have an unhealthy relationship with food and their bodies. In a just world, society would be sensitive to this fact and not make life worse for people by making them feel inferior and shameful.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Body Keeps the Score_** **by Bessel van der Kolk**

_The Body Keeps the Score_ (2014) explains what trauma is and how it can change our lives for the worse. These blinks investigate the wide-ranging effects experienced not only by traumatized people, but also those around them. Nevertheless, while trauma presents a number of challenges, there are ways to heal.
---

### Roxane Gay

Roxane Gay is a writer and associate professor of English at Purdue University. Her writing can often be found in the _New York Times_, where she's a regular op-ed contributor. She is also the author of the bestsellers _Bad Feminist_ and _Difficult Women._

