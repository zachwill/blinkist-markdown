---
id: 597bc3bbb238e10005703528
slug: hustle-en
published_date: 2017-07-31T00:00:00.000+00:00
author: Neil Patel, Patrick Vlaskovits, Jonas Koffler
title: Hustle
subtitle: The Power to Charge Your Life with Money, Meaning, and Momentum
main_color: FFCF3B
text_color: 80681D
---

# Hustle

_The Power to Charge Your Life with Money, Meaning, and Momentum_

**Neil Patel, Patrick Vlaskovits, Jonas Koffler**

_Hustle_ (2016) is your guide to getting by while doing what you love. These blinks explain the job dissatisfaction epidemic and explore ways to free yourself from the monotonous and unfulfilling toil of the everyday by realizing your dreams — however far-fetched and unattainable they might seem.

---
### 1. What’s in it for me? Learn why hustle is the way to power through life. 

Maybe you remember the Rick Ross hit "Everyday I'm Hustlin'"? This catchy song should be your new mantra. Why? Because hustling will give you an edge and help you break away from the boring, mundane work life that's dragging you down.

To hustle is to make the small tweaks that will propel you into a new and better life. In these blinks, we will look at what that entails. You'll learn to think like a hustler and discover actionable tips on how to get where you want to be in life.

In these blinks, you'll learn

  * how half of Americans under 30 view the American dream;

  * what the three M's are and why you need them; and

  * how POP is not just an email protocol.

### 2. The rigged economic system prevents us from following our dreams. 

Are you one of those people who considers her job to be perfect and wouldn't change a thing about it? Well, the majority of folks don't fit that description. In fact, many people feel that their daily working life amounts to no less than a daunting, repetitive slog.

Just take a Gallup poll which found that around 90 percent of the world's workers don't feel emotionally connected to their jobs.

While the majority of people _want_ to move away from this lifestyle, doing so is nearly impossible because of the way society is structured. After all, people end up in these jobs by following a very specific and common path. They go to college, maybe even graduate school, because they are told that's the only way to land the kind of career they are expected to want.

But education is expensive. Outstanding student loans in America rose to a staggering $516 billion in 2007. By 2015, that number had climbed to $1.2 trillion!

This travesty means that the average college graduate owes over $30,000 in student loans. For graduate students, that number easily climbs to six figures. For most people, going to college means loading up on debt.

Simultaneously, salaries have dropped and technology, paired with globalization, has meant an overall decline in the number of jobs. And who knows when people who are lucky enough to have jobs will be hit by the next major crisis and put out of work?

In this frightening climate, people take jobs to pay off their debt and cover their bills rather than fulfill their dreams. Just take a 2015 study, in which researchers asked 18- to 29-year-olds if they thought the American Dream was dead or alive. While 49 percent answered "alive," 48 percent said "dead."

But what other option is there other than doing these unfulfilling jobs? Well, that's exactly what we'll explore in the blinks that follow.

### 3. You can achieve your dreams by leaving conventional paths behind. 

By now it's probably clear that lots of people are unfulfilled in their work lives and don't see any way to break out of the pattern they're in. However, there is an escape route: _hustling_. Hustling is an approach to life that's designed to get you money, meaning and momentum.

While some people might assume this means taking advantage of others, that's absolutely not the case. Contrary to popular belief, hustling is _not_ about stepping on the toes of other people, stealing, conning, or anything like that. It's about forging ahead despite obstacles and difficulties. In other words, to hustle is to pursue your dreams.

But if you're an artist, do you really even need money?

Well, historically, some artists haven't. These creative practitioners claim to do their work solely for the sake of art, and, for a few, money is even repellent. Just take the copywriter and cartoonist, Hugh MacLeod. He says that artists inevitably compromise their art when they're offered money for it.

That being said, you need money to sustain your hustle, and if you can't sustain your hustle, you can't make your art. After all, if you don't have the money to pursue your dreams, you won't find meaning in your work or build the momentum to sustain your passion.

The truth is, lots of artists were only able to pursue their work, and thereby create meaning and momentum, by turning a profit. Picasso is a great example. If it wasn't for his financial success, he would have stopped making the paintings that both fulfilled him and captivated audiences for generations to come.

Picasso even said that an artist must succeed enough to sustain his art. Money, whether people like it or not, is key.

> In the sixteenth century, the word "hustle" was derived from the Dutch verb "_hutselen_ " meaning "to shake."

### 4. Hustling lets you own your dreams, but it comes with risks. 

Do you own your dream?

It may seem like an odd question, but most people are merely _renting_ theirs. In other words, they're playing by the rules of the rigged system and closing their eyes to what truly brings them joy. Such an outlook means accepting the dreams doled out by the system and never having ownership over your own. It means doing the job you don't like and telling yourself it's a great opportunity.

So how are you going to achieve your dreams?

To reach your goals, you need to hustle, even if it means taking a gamble. Hustling is all about risk-taking; it's about setting out on a journey that might afford you new opportunities, change and even the fulfillment of your dreams.

Just take the electrical engineer Ernesto and his wife, a nurse named Luci. In their native Cuba, businesses are burdened by an overbearing government bureaucracy that siphons away people's hard-earned money. Ernesto often thought of starting a business but wanted to do it beyond the reach of the government, which would mean serious risks, even jail time, if he were caught.

While it was a tough decision, he finally decided to start off doing small projects for friends and acquaintances, which slowly grew into a bigger business.

By the end of his career he'd earned a handsome chunk of money and, while the hazards were real, it was all worth it in the end. His family had enough to take care of their basic needs and even afford a nice car.

### 5. Advance toward your dream one step at a time and persevere when the going gets tough. 

Breaking out of the convenience of a sheltered life — even one that's monotonous and unfulfilling — can be tough. But to snag your dream while you still can and make truly life-altering changes, you'll need to push yourself into territory that's somewhat painful and uncomfortable. However, you only need to takes small steps at a time.

What if you dream of being a star public speaker but you're terrified of appearing in front of crowds. Instead of talking to a massive crowd straightaway, start giving short speeches every night at the dinner table. Doing so might be uncomfortable, but this discomfort is actually good.

It's a small dose of discomfort that you can overcome, improving in the process. Then, when your dinnertime speeches are a piece of cake, make them longer, or speak to more people. Slowly but surely you can broaden your experience until you're living your dream.

And remember, such momentum can bring luck. When you push through and persevere, luck becomes more likely. So, while you might feel like giving up, whether on a job, a book, or a personal project you're invested in, persisting will pay off in the end.

When famous director Francis Ford Coppola was filming _Apocalypse Now_, practically everything went wrong. There was bad weather on the set, the schedule kept getting pushed back and the costs were getting out of hand. Not just that, but Coppola was in constant conflict with his actors, who were regularly drunk or stoned.

He wanted nothing more than to quit, but he didn't. He persevered instead and, all of a sudden, luck started to come his way. The weather cleared up, the actors gave powerful performances and, by the end of it, they had produced a film that won multiple Academy Awards.

### 6. To catch your dream, abide by the Three Unseen Laws of Hustle. 

By now you know a decent amount about hustling, but you don't know the Three Unseen Laws: _heart, head_ and _habits_.

The first of these is crucial because doing something that's close to your _heart_ will move you to action and keep you forging ahead. You might have a feeling of joy, anger or revenge that will push you to accomplish your goals.

Maybe you're pissed off that your smartphone earbuds keep getting tangled up in your pocket and are certain there's another way. Before you know it you're thinking through the design for a pocket-sized box that keeps your headphones neat and tidy.

Next, you must use your _head_ to observe and benefit from things that aren't immediately visible. Just imagine you're the captain of a ship. You naturally need to stay abreast of the wind and waves to navigate accurately, but you also need to check the undercurrents. You might want to avoid them or you might be able to use them to arrive at your destination faster.

The point is, while lots of people only see obstacles, a hustler keeps her head cool and sees opportunities wherever they might be.

And finally, it's important to make a _habit_ of setting milestones and deadlines. After all, everyone needs closure at one point or another. Setting intermediate goals will commit you to doing a certain amount of work, and deadlines will push you to get it done.

Say you want to write a book. You could set a one-week deadline for writing 10,000 words and make a daily habit of waking up at seven in the morning and writing for two hours. Once you achieve your one-week deadline you can set another one and keep moving toward your goal.

### 7. Make a portfolio to highlight your assets and reach for your dreams. 

Do you have a portfolio of your work? If not, you should definitely consider putting one together. After all, a _Personal Opportunity Portfolio_, or _POP_, is a great way to showcase you and your work while attracting people and companies that will help you attain your dreams.

This tool, which consists of four parts, lays out exactly who you are, which is invaluable in making progress. While a resumé describes what you've done so far, a future employer really wants to understand where you're going. That's where the first part of the POP comes in: _potential_.

If your dream is to be an online marketing manager for a cosmetics company you might try experimenting with different brands and writing up your experiences on a blog. If you can get enough traffic flowing to your site, you'll have demonstrated the potential you have to engage in the field.

The next pillar of your POP is _people_, which naturally is all about your network, both on and offline. A great way to expand this network is to connect others. By facilitating such connections you'll benefit others, but you'll also pull people into your own network.

The third piece is _projects_. This portion of your POP should detail everything you're currently working on, whether it's volunteering at the Red Cross, writing your blog or pitching ideas to new acquaintances at meetups. This section presents your interests while showing your ability to do work and get results.

And finally, the _proof_ section is essential to demonstrate your credibility. After all, if you can't back up your words with evidence, you'll never be able to impress the right people.

Anybody can say that they write compelling blog posts, but very few people can provide links, references and examples of such captivating work. Being able to do so will show that you can deliver what you say you will.

### 8. Final summary 

The key message in this book:

**Hustling tends to get a bad rap but actually, it's a positive way of persevering and shooting for your dreams. You'll always be able to find a way to reach your deepest goals as long as you're willing to step off the conventional path, push ahead no matter what and aim for what you truly want.**

Actionable advice:

**Find your talents in the mirror.**

Many people feel pressure to excel at school, get into a good college and go on to be a respected professional. However, this conventional wisdom isn't universally applicable, and you might want to do something completely different. To find out, take a deep look at yourself in the mirror and ask yourself, "what are my talents?" By taking this time to identify your true abilities, you'll find the dream you need to guide you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Unlocking Potential_** **by Michael K. Simpson**

_Unlocking Potential_ (2014) outlines practical coaching tools to help leaders, managers or supervisors better engage their teams and transform their organizations. It's simply the most comprehensive guide to becoming a great coach!
---

### Neil Patel, Patrick Vlaskovits, Jonas Koffler

Neil Patel is an entrepreneur and digital marketer who works with major corporations like NBC, HP, and Viacom to boost their revenue. He's a co-founder of the Hello Bar and Crazy Egg platforms, which help people understand visitors to their websites.

Patrick Vlaskovits is a writer and entrepreneur who has contributed to prestigious publications like the _Harvard Business Review_ and the _Wall Street Journal_. He's a regular speaker at tech conferences and is the co-founder and head of Superpowered Inc., an online work platform for audio engineers.

Jonas Koffler is a writer and media consultant who advises leaders, artists, start-ups and large companies on development, innovation and strategy. As a writer, he has contributed to a number of bestsellers.

