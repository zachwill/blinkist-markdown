---
id: 571fbe0589578600031588ad
slug: i-can-hear-you-whisper-en
published_date: 2016-04-29T00:00:00.000+00:00
author: Lydia Denworth
title: I Can Hear You Whisper
subtitle: An Intimate Journey Through the Science of Sound and Language
main_color: D9523F
text_color: A63E30
---

# I Can Hear You Whisper

_An Intimate Journey Through the Science of Sound and Language_

**Lydia Denworth**

_I Can Hear You Whisper_ (2014) is about human communication, and the phenomenon and culture of deafness. Hearing is a complex process that doesn't function the same way for everyone, and those who are deaf or hard of hearing have developed alternative methods of communication, around which a special culture has grown. These blinks give an overview of that culture and show that it's just as rich as any other.

---
### 1. What’s in it for me? Learn about the rich history of deafness and deaf culture. 

Can you imagine trying to have a conversation with a friend and not being able to hear a single word he's saying? Well, for deaf and hard of hearing people across the globe, this is part of their everyday reality.

But, of course, this fact hasn't held back the deaf community. Over the course of centuries, deaf people have developed their own highly complex language and culture to play a fully functioning role in society.

However, with the development of more and more precise medical apparatus designed to "cure" deafness, this rich culture might be under threat. These blinks take at look at such developments, how they impact the deaf community and whether or not it's right — or necessary — to eliminate deafness at all.

They also show you

  * why some hearing aids make people sound like Donald Duck;

  * what hearing has to do with your development; and

  * why the telephone was beneficial for deaf people.

### 2. Hearing is a complex process that doesn’t function perfectly in everyone. 

Most people take their ability to hear for granted. Hearing, like so many other physiological functions, is a highly complex process.

So how is sound created and how do our ears process it?

Noise is made when molecules in the air vibrate and create a _sound wave_. Our outer ear catches sound waves and works like a funnel, sending them into the _ear canal_. The ear canal then amplifies them and sends them onto the _eardrum_.

When a sound hits the eardrum, it vibrates and changes from _acoustic energy_ into _mechanical energy_. The mechanical energy then moves through three tiny bones called the _hammer_, _anvil_ and _stirrup_, and enters the _inner ear_.

Once in the inner ear, the sound enters the fluid-filled _cochlea_, where it changes into _hydro-energy_. Tiny hairs in the cochlea detect movements in the fluid and generate electrical impulses in the brain, causing you to "hear" the sound.

But this process doesn't function properly for everyone: while most people can hear sounds within a range of 20 to 20,000 _Hertz_ (Hz), other people's range is much smaller or nonexistent.

Life is very different for people who are deaf or hard of hearing. If you can't hear people speak, for instance, you have to rely on completely different methods of communication.

That's why such a vibrant culture has developed around deafness. The deaf community has had to come up with alternative ways for a lot of things hearing people rely on their hearing for. Deafness shouldn't necessarily be considered a lack of something: it's an identity in its own right.

### 3. Language development is connected with hearing, but deaf children possess the same language development skills. 

Have you ever wondered why humans are the only species to have developed language?

Scientists are still researching this question, but experts widely agree that babies are born with an innate ability to learn language. They understand some universal grammar rules and principles, like consonants, vowels, nouns, verbs, pitches, intonation and phrases as building blocks.

The younger you are, the better your natural ability to learn language functions. So if you have children, expose them to language as much and as early as possible. A child's IQ and ability to learn are directly linked to the amount of language they're exposed to.

For deaf children, language development works differently. There are two schools of thought here. One holds that deaf children should focus on sign language first; the other says that oral communication takes precedence.

Both approaches have been proven to work. So parents of deaf children tend to base their choice for which path to take on their culture.

Parents who have their children focus on sign language usually do so because they want them to be able to engage with the deaf community. Community isn't the most important thing for a deaf child, however. The most important thing is that their deafness is identified as early as possible. If a child who is deaf or hard of hearing is only exposed to teaching methods that don't suit him, he'll miss out on the early stages of language development when his brain can absorb language much more readily.

Deaf children need to be taught in a way that's meaningful and productive for them, so deaf education has a culture of its own, too.

### 4. The debate on how to best educate deaf people is centuries old. 

The concept of deafness has been poorly understood throughout history, and hearing people often assumed that deaf people were just less intelligent. Even Aristotle thought that deaf people were incapable of learning and using logical reasoning skills.

We've come a long way since then. In fact, the debate about using sign language or oral language has been going on for centuries.

Pedro Ponce de Léon, a Spanish Benedectine monk who lived in the fifteenth century, believed that deaf people should be taught with sign language. The German thinker Samuel Heinicke favored the oralist approach, as oral language enabled deaf people to be more included in society.

Deaf children tended to learn sign language faster but, for centuries, hearing people widely considered it to be weak and inferior. So oralism became the more favored approach.

One of the major players in the field of oral teaching was Alexander Graham Bell. Bell used the money and fame he earned from the telephone to become the most prominent advocate of oral deaf education in the United States. He founded _AG Bell Association_, a research organization that still plays a major role in deaf education.

Deaf education and culture continued to develop. In the first half of the twentieth century, sign language was still considered inferior to "real" language, but that started to change in the 1960s when the first American Sign Language Dictionary was written.

The American Sign Language Dictionary illustrated that sign language was just as intricate as any spoken language. People were becoming more politically conscious and coming up with new ideas about self-identity in the 1960s, too. As the deaf community took greater hold of their identity, a new term developed: deaf culture.

Early proponents of deaf culture wanted to promote deafness as an identity to be proud of. Deaf people had a heritage and culture as rich as any hearing community.

> "_Helen Keller famously said that being blind cut you off from things, but being deaf cut you off from people._ "

### 5. New scientific understanding of hearing allowed scientists to develop new hearing aid technology. 

As the deaf community began to assert their identity more in the twentieth century, something else also changed: scientists began developing tools to "cure" deafness.

Early experiments were aimed at repairing damaged ears. For a long time, scientists lacked the technology and understanding to do much for people with hearing difficulties. That started to change in the middle of the century, thanks in large part to the invention of the telephone.

The technology used in the telephone was also used to create the first electronic hearing aids. It could be used to amplify sounds, but it was essentially just a battery attached to a telephone receiver.

In the 1950s, hearing implants were invented. Once scientists discovered that hearing was produced by electrical impulses in the brain, artificial electrical hearing was suddenly a possibility.

André Djourno was the first to experiment with using induction coils to enable hard of hearing people to hear. His first patient to successfully use an implant could differentiate between some sounds but he never learned to understand speech. The implant also stopped working after a few weeks.

An otologist named Bill House later picked up where Djourno left off. He got the idea to bypass the cochlea in people whose cochleas were damaged by electrically stimulating their brains directly.

That led to the first conception of the _cochlear implant_, a device with a microphone and electrodes that collected sound and processed it into electrical signals, which could then be sent to the person's nerves.

The first cochlear implants were far from perfect. They couldn't collect a lot of sound but they were still a big step forward in hearing aid technology. In 1984, the cochlear implant by House was the first device of its kind to get FDA approval.

### 6. Hearing and non-hearing children need time to develop the neural pathways that allow them to understand language. 

We've seen how sound is processed in hearing people, but what about in deaf people?

Both hearing and deaf children need experiences to develop their neural pathways. Babies are born with around 78 billion neurons and even more are generated as the children get older. Neural _pathways_, however, can only be formed with new experiences.

A pathway gets stronger the more it's used. So the more a child hears a word, the stronger the pathway and the better she'll get at processing the word.

So a child's ability to understand sounds thus depends on how many sounds she's exposed to. If a child is hard of hearing, her brain won't develop as much of an understanding of sounds during their most formative years of development. That has an impact on what kind of implant could be the most effective for them.

If a child receives hearing treatment, such as an implant, it's important for her to get it as early as possible so their brains can make better use of it. She'll need time to get used to having conversations.

When we hear language, we don't just process its sounds. We also recall our past experiences with the words to make sense of what we're hearing. That's why we can understand broken versions of languages we know. We piece together the meaning based on what we know of the words.

The earlier a child receives an implant, the more effective it is because it enables the child to be exposed to the sounds of language during her formative years.

> "_What all these acoustic manipulations have demonstrated is that the sounds we hear have to be good enough_."

### 7. Cochlear implants are still imperfect and controversial. 

The first cochlear implants were only given to adults. That changed in the 1990s, however, when it was decided that they could be given to children as young as two.

This sparked a lot of debate. Many people questioned the necessity of cochlear implant surgery, as the deaf community had earned a lot more respect and recognition by that time. The 1990 _Americans with Disabilities Act_, for example, forbade all forms of discrimination toward people with disabilities. New tools for deaf people, like TV captions and dial assistants, had suddenly become the norm.

So a lot of people believed it was unnecessary to subject children to implant surgery, fearing that it would stigmatize deafness and destroy deaf culture. They didn't want deafness to be viewed as something that needed to be cured.

Others argued that children had the right to hear if it was possible, even if their hearing wasn't perfect. Cochlear implants could also serve as a cheaper alternative for people who couldn't afford other forms of assistance for deafness.

The fact that cochlear implants were imperfect had a big impact on the debate. One person even told the author that cochlear implants made voices sound like Donald Duck giving the Gettysburg Address.

Cochlear implants have two particular areas of weakness. For one, it's difficult to filter out the sound you want to focus on if you're in a noisy environment. So it's hard to hear the voice of someone you're speaking to at a crowded party, for instance.

This problem can be reduced if you have an implant in each ear, as two implants allow the sound to be more precisely pinpointed. Hearing at a party or football game is still a challenge, however.

Listening to music is also a challenge. Music is much more complex than speech, so it's hard to differentiate between the sounds and message through an implant.

### 8. Cochlear implants were once expected to revolutionize deaf education, but results have been mixed. 

When cochlear implants were developed, they seemed poised to revolutionize education for the deaf and hard of hearing. Such people had long suffered in traditional education systems. Many only managed to reach a fourth grade level of reading and writing.

Deaf education existed before implants, however. In 1975, the United States passed _Public Law 94-142_, which guaranteed free and suitable education to all children, so deaf children had been attending regular schools long before cochlear implants. Lots of schools staffed interpreters or teachers who specialized in education for the deaf.

In fact, in 1986, only three out of ten deaf or hard of hearing children attended a specialized needs school.

Difficulties with hearing aren't the only things deaf children struggle with, either. Their socioeconomic status and level of parental support also have a big impact on their achievement at school. That's why about 15 to 20 percent of children who receive implants still do poorly in school.

Cochlear implants didn't have a negative effect on deaf education but they didn't revolutionize it either. And, despite the increase in implants, deaf culture and sign language continue to thrive.

Sign language has survived the development of cochlear implants for a number of reasons. For one, implants aren't helpful in noisy environments like parties or public events. In places with a lot of sound, sign language is still the most effective way to communicate.

A lot of people also maintain their sign language level so they can participate in deaf culture. Cochlear implants allow them to participate in hearing culture, but sign language is still an important part of deaf art, history and identity.

All in all, the cochlear implant has had an effect on deaf education but it doesn't work for everyone, or at all times. Deaf education continues to evolve, just like any other form of education.

> "ASL [American Sign Language] is the only thing we have that belongs to deaf people completely."

### 9. Final summary 

The key message in this book:

**Hearing is not only a highly complex physical process: it's intricately connected to language, learning and communication. Deaf and hard of hearing people were long considered unintelligent because they couldn't communicate with the same methods hearing people could, but deafness started to lose that stigmatism in the 1960s. Today, deaf people have embraced their identity and culture, which continues to change with new societal developments.**

**Got feedback?**

We always like to hear what you think about our content. Let us know what you think by emailing us at [remember@blinkist.com](<mailto:remember@blinkist.com>) with _Always Hungry?_ as the subject!

**Suggested** **further** **reading:** ** _The Language Instinct_** **by Steven Pinker**

Steven Pinker is an experimental psycholinguist as well as a professor of psychology at Harvard University. He is the author of six books, two of which, _How the Mind Works_ and _The Blank Slate_, were Pulitzer Prize finalists.
---

### Lydia Denworth

Lydia Denworth is writer whose work has been in the _New York Times_, _Newsweek_ and the _Wall Street Journal_. She served as an adjunct professor of journalism at Fordham University and Long Island University. _I Can Hear You Whisper_ is her second best-selling book.

