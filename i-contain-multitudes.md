---
id: 58b40ad28566cb0004c1f3ec
slug: i-contain-multitudes-en
published_date: 2017-03-01T00:00:00.000+00:00
author: Ed Yong
title: I Contain Multitudes
subtitle: The Microbes Within Us and a Grander View of Life
main_color: FA3250
text_color: C7283F
---

# I Contain Multitudes

_The Microbes Within Us and a Grander View of Life_

**Ed Yong**

_I Contain Multitudes_ (2016) peers into the microscopic world of microbes, and offers fascinating insight into the countless ways in which our lives are influenced by them. You'll find out how ancient microbes helped make the world livable for mankind and how they continue to help all of Earth's living creatures through remarkable and essential partnerships.

---
### 1. What’s in it for me? Cherish your microbes. 

When you hear the words "microbe" and "bacteria," what's your first thought? Maybe something like, "Those lousy little beasts that, every year, steal days of your life by attacking your health and forcing you to stay in bed and drink chamomile tea — yuck!"

But did you know that microbes such as bacteria are the only reason our immune system can function in the first place? Indeed, microbes are vital to all of our bodily functions — and the human body contains more of them than actual body cells.

In these blinks, you'll learn to welcome your microbes and appreciate them as your body's little helpers. You'll discover the exceptional role that microbes have played in evolution and why to hang out with microbes is to be in good company — not only for us, but for all organisms.

You'll also learn

  * how many microbes fit on the head of a pin;

  * that microbes make a fish invisible; and

  * why some of the leaves on apple trees don't turn yellow in fall.

### 2. Microbes are everywhere, helping our planet function. 

Microbes have been around for so long that it's hard to comprehend, so let's look at it another way: If the Earth's 4.5 billion years of existence were one calendar year, humans would have shown up in the last 30 minutes of December 31st, five days after the extinction of the dinosaurs. Microbes, on the other hand, have been around since March.

That's a long time ago, and for a while they were the only living things around. But even then they were hard at work, shaping the planet we see before us.

The term "microbes" actually refers to a wide array of tiny single-cell organisms, such as various species of bacteria and fungi.

Just how tiny are they? Well, microbes are so small that a million of them could fit on the head of a pin.

But this doesn't mean their role is insignificant. Microbes are always busy breaking down various molecules all around us, which is how soil gets enriched and nutrients such as carbon and nitrogen complete their environmental cycles.

Microbes also played a vital role in creating Earth's atmosphere. Microbes were the first living things to use photosynthesis, a process whereby an organism uses energy from sunlight to convert carbon dioxide and water into sugar. The microbes then ate this sugar, releasing oxygen as they did so and creating our atmosphere in the process

This also set the foundations for the _carbon cycle_ without which life couldn't exist — the absorption of carbon dioxide by plants, the consumption of plants by animals, the exhalation of carbon dioxide by animals.

Another reason microbes are so amazing is their ability to adapt to just about any environment. You'll find them in the ice of Antarctica, up among the clouds or down at the edge of an underwater volcano, where the temperature reaches 400° C.

Microbes can adapt to these extreme environments because they evolve at an extremely rapid pace.

By forming a physical link from one cell to another, pieces of DNA can be sent and added to a genome. Therefore, microbes can share an adaptation from their neighbor and pass these new genes along during reproduction, making evolution much faster than the process of natural selection.

> _"Animals might be evolution's icing, but bacteria are really the cake."_

### 3. Apart from our own genes, every human has many microbial genes, which influence our life and development. 

If you're a fan of scientific journals, you may have read that for every one human cell there are ten microbial cells in our body. While this is an exaggeration, the truth is still impressive.

Microbes do in fact make up the majority of the cells and genes in our body.

We have around 69 trillion cells in our body, and over half of them, around 39 trillion, are microbial. There are also around 20,000 genes in the human genome, but if we were to include all the microbial genes we carry, the number would become 500 times bigger.

Every individual, no matter what species it's from, has unique and complex microbial communities called _microbiome_. Each part of the body has a different community, and though everyone's microbes are different, these communities are there to perform the same set of functions.

A microbiome is like any other natural ecosystem: Each community has a certain microbe that acts like a dominant leader to make sure things function properly, such as balancing the levels of acidity in its particular part of the body.

In this way, the health and development of all animals and humans depend on microbes.

This is especially true for our immune system. Breast milk is rich in over 200 nutrients, including human milk oligosaccharides, or HMOs. Yet babies can't digest HMOs; they're only there to feed a special microbe in our gut called _B. infantis_.

When this microbe digests HMOs, it releases nutrients in the form of proteins, which babies _can_ digest. These include anti-inflammatory proteins that coat the gut and calibrate our immune system.

The gut microbes of humans and animals serve many functions. For instance, in mice, there's a family of gut microbes called _Bacteroides thetaiotaomicron_. These activate certain genes during development to ensure that they form the right blood vessels and that their gut will have the right microbes to break down toxins and build nutrients.

### 4. Symbiosis with microbes gives some animals remarkable powers. 

In many parts of the Northern Hemisphere, when autumn arrives, a tree's leaves will turn to beautiful shades of yellow, orange and red. However, if you look closely, you'll see that some leaves remain green.

Believe it or not, this is due to one of many remarkable relationships between microbes and animals.

In this case, it's a partnership between the tentiform leafminer moth and _Wolbachia_, the world's most common microbe. Since the leafminer matures by forming a cocoon on a tree's leaf, it has a microbe that will produce a hormone to make sure the leaf stays green and doesn't fall down prematurely, killing the grub.

Another fascinating relationship involves a little cephalopod called the bobtail squid and a highly complex system of microbes that creates a light-emitting organ to keep it safe at night. The microbial cocktail works by making the squid's outer layer of cells hospitable to only one particular microbe. And when these microbes arrive, they're supplied with nutrients and made to become one with the squid.

Remarkably, these microbes then act as the squid's defense system, producing a glow that matches the sky's moonlight and effectively hides it from any hunters lurking below. With no silhouette or discernable shadow, the bobtail squid is virtually invisible to predators.

These are extraordinary examples, but helpful microbes aren't unusual — in fact, they are the rule, not the exception.

Since microbes can live just about anywhere, and can also help animals digest otherwise indigestible food, they're the animal kingdom's universal helpers. Even ten to 20 percent of all insects depend on microbes to provide vitamins and help them build cells and proteins. For instance, half of a termite's body weight is helper microbes devoted to digesting cellulose.

As we've seen, microbes are crucial to survival, so it's imperative that they're passed on to offspring.

There's a Japanese stink bug that does this by coating her eggs in a special fluid that contains essential microbes. When the baby stink bugs hatch, they have a microbe-rich first meal waiting for them. It's not unlike the important microbes we get from our mother's milk.

> To acquire another animal's microbes, many animals simply eat each other's poop.

### 5. Alliances with microbes need to be carefully balanced. 

Despite the helpful nature of millions of microbes, and the fact that there are only about a hundred microbes that are considered harmful to us, there's a huge market for antibacterial cleaning products.

In fact, there really isn't such a thing as a "good" or "bad" microbe; it all depends on the environment.

For instance, there are millions of different microbes living in our gut that help us digest our food. But if these microbes got onto our skin they could infect a wound and cause all sorts of problems.

Farmers actually take advantage of this knowledge and use the microbe _Bacillus thuringiensis_ as a pesticide. When it comes in contact with a caterpillar, it punches holes in the insect's stomach; this releases gut bacteria into the caterpillar's bloodstream. Naturally, the immune system goes into shock, killing the insect through inflammation.

This is why the right barriers need to be in place, so that microbes stay in their proper, enclosed environment.

Insects do this with the help of special cells called bacteriocytes. These hide the microbes from the immune system, fencing them in with harmful enzymes and antibacterial chemicals, while also ensuring the microbes get the necessary nutrients.

For larger and more complex animals, the situation gets more complicated. Our microbes live around our organs, rather than in them, but our body helps make sure only the good microbes get invited by setting the right conditions.

Our gut is full of powerful acids, making it an environment that only a select few bacteria can survive.

Mucus is another means of defense for most vertebrate animals. Mucus carries bacteriophages, which are domesticated viruses that feast on harmful microbes.

And last but not least, there's the immune system, which produces white blood cells that act as a border patrol and capture any microbes that sneak through. If any emergencies arise, it will make sure antibodies are built and other countermeasures are prepared.

> The average human swallows around a million microbes in every gram of food they eat.

### 6. A diverse microbiome is crucial for our health and immune system. 

There are a lot of germaphobes out there, and you probably know someone with strong opinions about hygiene. But if you really want to keep your body healthy, there are some essential facts you should know.

To stay healthy, your immune system needs to be properly tuned — like a thermostat — to the ideal setting.

Otherwise, your "immunostat" could be too low, which means that it only reacts to major threats and ignores smaller ones. At this setting, your immune system may neglect a threat that might turn into an infectious disease.

On the other hand, your "immunostat" could be too high, in which case it can be jumpy and overreact by attacking harmless microbes like pollen or even your own friendly bacteria. At this setting, you run the risk of coming down with an allergic disease.

Exposure to microbes can help calibrate our immune system to its healthiest setting. Unfortunately, however, a modern lifestyle tends to minimize such exposure.

To stay away from both infectious diseases and allergic diseases, the immune system needs to be set at the right level early on by being exposed to many microbes. This often happens naturally in childhood, when kids are frequently exposed to dust, dirt and mud.

But growing up in an urban environment means this is becoming less and less common.

People in cities are showering with sanitized water, eating processed foods and have far less contact with domesticated animals. This is part of an overall trend in society that's putting a big focus on cleanliness.

To keep the immune system working at it's best, there needs to be some healthy competition; it's harder for bad microbes to establish a stronghold in your gut when as many good microbes as possible are staying active by constantly competing for nutrients.

You can help with this by maintaining a diverse diet that appeals to many different gut-microbes.

Eating a lot of fruits and vegetables is great for this. Plant-based foods are rich in fiber, which is tougher to digest than processed foods and appeals to a wide array of microbes.

### 7. Manipulating microbiomes for our benefit could transform healthcare. 

You might have noticed that most health tips these days are pretty simplistic. Feeling tired and worn out? Take some vitamins. Have a cold? Take this medicine to kill the virus.

But since our microbiome plays such a big part in our lives it only seems natural that we should be able to manipulate this system to benefit our overall health.

This, however, is easier said than done. Our microbiomes are so large and complex that simply adding one kind of microbe hardly ever has a noticeable effect.

If you've started a diet of probiotic yogurt to help your digestive system, you might have been disappointed with the lack of results. That's because yogurt's microbial cultures are not natural to the gut, so it's hard for them to make a lasting impact.

Introducing a full microbiome, on the other hand, could save lives.

RePOOPulate is a project that helps people overcome a deadly infectious disease known as _Clostridium difficile_, which has symptoms that include fever, nausea and severe diarrhea. It's a tough disease to keep from recurring, but with a healthy stool sample from a relative, doctors can transplant an entire microbial system into the patient and get them on the path to recovery.

To make treatments more targeted and effective, doctors are also looking into ways of manipulating microbes for specific purposes.

Most treatments, like aspirin or antibiotics, are broad and affect every cell in the body in the same way. But microbes have the potential to be utilized in a highly targeted way — even releasing specific doses of a medication to a specific site.

In 2014, researchers at the Harvard Medical Institute were able to equip an _E. coli_ microbe with a genetic switch that made it turn blue in the presence of antibiotics. Like a microscopic alarm bell, the microbe could tell doctors if a patient had taken their medication.

This has inspired others to look at new ways to use gene switches. The hope is that modified bacteria could act as an early detection system for diseases and provide a warning before the first symptom even reveals itself.

### 8. Final summary 

The key message in this book:

**Microbes are everywhere, and for good reason — they're vital to our well-being! Each species has a distinct community of microbes and a way of maintaining that partnership over generations. Taking microbes into account, we can view our bodies, and those of the animals around us, as thriving ecosystems instead of just individuals. This perspective also opens up many new possibilities in how we approach and understand our medical and environmental problems.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Follow Your Gut_** **by Rob Knight with Brendan Buhler**

_Follow Your Gut_ (2015) puts the world of microbes under the microscope, showing just how much influence the little things — in this case, bacteria — have on our life. The fact is, we're crawling with bacteria, both inside and out, and if we weren't, life wouldn't be so great. Bacteria serve many important functions, like keeping us happy and healthy. It's time to learn how to treat them well!
---

### Ed Yong

Ed Yong is a renowned science writer and public speaker whose work has appeared in the pages of the _Atlantic_, _Nature_, _Scientific American_, the _New York Times_, _Wired,_ and many more. He also edits a blog on the _National Geographic_ website entitled _Not Exactly Rocket Science_.

