---
id: 53f48a743433390008450100
slug: i-is-an-other-en
published_date: 2014-08-19T00:00:00.000+00:00
author: James Geary
title: I is an Other
subtitle: The Secret Life of Metaphor And How It Shapes The Way We See The World
main_color: FA6838
text_color: AD4827
---

# I is an Other

_The Secret Life of Metaphor And How It Shapes The Way We See The World_

**James Geary**

_I_ _Is_ _an_ _Other_ demonstrates that metaphor is not just a literary tool but a fundamental way we have of discovering and making sense of the world. The author shows various ways in which metaphors are used in diverse fields, such as politics, economics, psychology, science, business and more.

---
### 1. What’s in it for me? Find out how metaphors can be used to both manipulate and innovate in science, business and politics. 

Every day of our lives, we use and hear metaphors: whether we're describing the _high_ _point_ of our day or a _sinking_ feeling, metaphors help us share vivid descriptions of what we're going through with others.

But metaphor is far more than a mere linguistic device and literary tool. It helps us communicate complex ideas and describe new phenomena. It is used extensively by politicians, economists, scientists and advertisers — sometimes for dubious purposes.

After reading these blinks, you'll start seeing metaphors everywhere in your private and professional life, and you'll better understand how they affect you.

In these blinks, you'll discover:

  * how sharks helped shipping companies save millions of dollars a year,

  * how a metaphor can completely transform your attitude toward work, and

  * why one particular metaphor can manipulate sentiments on immigration.

### 2. Metaphor isn’t just a literary device but a way of making sense of the world. 

"Life is a rollercoaster," "America is a melting pot" and, as Shakespeare's lovesick Romeo uttered, "Juliet is the sun."

All of these phrases are _metaphors_ : they _describe_ _one_ _thing_ _by_ _claiming_ _it's_ _the_ _same_ _as_ _something_ _else_.

Even though metaphor is frequently utilized by poets like Shakespeare, its reach is far greater than just the world of literature. Metaphor is one of the main ways we have of comprehending the world around us. It occurs in our writing and speech about every 10 to 25 words, or an average of six times per minute.

Aristotle once defined metaphor as "giving the thing a name that belongs to something else." The author widened this definition to include similes (using "like" to juxtapose two dissimilar things) and other methods of unexpected comparison.

Because metaphors allow us to describe things in other terms, they can help us explain new and unfamiliar experiences for which we don't have names yet. For example, a teenager who's _fallen_ _in_ _love_ (a metaphor itself) for the first time may describe this exciting new experience as a _breath_ _of_ _fresh_ _air_ or even a _bolt_ _of_ _lightning_.

The truth is, metaphor is an essential tool for communicating meanings we can't immediately grasp in many diverse and complex fields. When technological products aren't working, we claim they "act funny" — as though they were people; meteorologists say it's raining "cats and dogs" or that the city is in a "heat grip"; biologists use familiar images to help us understand physical concepts we cannot see ourselves, like the _cell_ _walls_ of plants.

So if you're one of those people who thought they'd never use metaphor again after high school English, think again. Metaphor is an important way in which we interpret our experiences every single day.

> _"Metaphor is a way of thought long before it is a way with words."_

### 3. Metaphor lends our body language deeper meaning. 

Have you ever given someone _the_ _cold_ _shoulder_ or flashed _the_ _OK_ _sign_ by sticking your thumbs up? These are examples of how metaphor occurs in many of our physical gestures.

More than just a verbal form of description, metaphor also exists in non-verbal complements to our words. Certain physical actions, like a person's posture, unite metaphor and body to convey the underlying emotion. It is through this combination that others interpret what we're actually saying.

For example, crossing your arms and slouching might convey disinterest and boredom, whereas uncrossed arms and a straight back express receptiveness and seriousness. Metaphor transmits itself through these gestures, revealing our speech to be either genuine or insincere, and influencing our communication with others just as much as our words.

Additionally, metaphor connects body and mind when our behavior is affected by certain verbal and visual stimuli. One study found that people tended to lean forward when discussing the future ("The future lies ahead of us") and lean back when discussing the past ("The past is behind us").

This process of behavior modification due to visual, verbal or metaphorical influences is called _priming_. In another study, researchers noticed that people would read texts containing metaphors for speed ("work is a _fast-paced_ environment") faster than texts containing metaphors for slowness ("progress was achieved at a _snail's_ _pace_ ").

In this way, metaphor unites our bodies with our words and intentions. In fact, we see this exhibited in many concepts, such as _proximity_ : Have you ever wondered why we refer to some people as _close_ _friends_? _Proximity_ can mean both emotional and physical intimacy. Thus, we refer to the people we love as our _close_ _friends_.

Ultimately, we link mind and body through our use of verbal and non-verbal metaphors.

### 4. Metaphors influence our behavior and allow us to make new discoveries. 

How do we interact with the world around us? Whether we're "kicking around an idea" or asking someone if they "see what we mean," metaphor instructs us how to behave in the world and molds our vision of it by unifying body and mind.

In fact, our actions are shaped by the metaphors we use.

Consider one case of psychotherapists Lawley and Tompkins, who employed metaphor in order to help a client improve his unhealthy professional relationships. After a few sessions, the client's existing worldview became clear: "Work is war." Unfortunately, this outlook made him perceive others as competitors and unwilling to accept feedback.

So what did Lawley and Tompkins do? They changed their client's metaphor. And sure enough, they succeeded: by applying the metaphor "work is playing in an orchestra," their client began to see himself as part of a group and cooperate more while improving his overall performance. Thus, we see how our outlook on the world is revealed through the metaphors we use and how it impacts our actions.

What's more, metaphors are often necessary when explaining new scientific discoveries when existing terms fail us.

One example of this was the discovery and exploration of the atom in the early twentieth century. Metaphor allowed this new and unknown particle to be described by comparing it to something familiar.

Scientists came to understand that the atom was best modelled after the solar system (with the nucleus as a star and electrons as orbiting planets). This model replaced the existing metaphor comparing the atom to a plum pudding, in which the negatively charged electrons were the "raisins" in the positively charged "pudding" of an atom.

In this case, the realization that electrons aren't static but actually move around the nucleus demonstrated the proof was _not_ in the pudding!

> _"In the brain, networks for language, action, and metaphorical thinking overlap."_

### 5. Economists can manipulate investors through metaphors. 

We're often inundated with metaphors from economists who inform us that stocks are _soaring_ or _plummeting_, and that the market might _crash_. However, to understand the strategies of economists we must first understand the purpose of metaphors.

Within the financial world, metaphors serve to encourage potential investors to invest. This works in two ways.

"Agent" metaphors are used to describe the active intent of a living thing. Economists tend to use them when the market is an upward swing (e.g., "The market _fought_ its way upward").

On the other hand, "object" metaphors are common during market slumps because they indicate passivity and the control of external forces (e.g. "The market _fell_ _like_ _a_ _brick_ ").

Why?

Because by using "agent" metaphors, economists can make it sound like the market is improving on its own volition, and thus "wants" to continue improving. Meanwhile, "object" metaphors make it sound like a falling market is the helpless victim of external circumstances and will probably recover as soon as they change.

In this way, economists attempt to sway investors into investing regardless of the market trend. They either champion a growing and active market or lessen the blow of a passive, tortured market after a crash.

But that's not all. Once exposed to agent or object metaphors, people respond according to a phenomenon called _expectancy_ _bias_, which is when someone believes they've noticed a trend and expects that trend to continue, even if it doesn't really exist.

In other words, because our brains are always recognizing patterns, people expect the market to act the same way whenever certain agent or object metaphors are used. For this reason, metaphor not only makes patterns obvious, but it also convinces people of nonexistent patterns and impacts their behavior (e.g., "The market will continue to _climb_ _higher_ ").

Whereas metaphors tend to shed light on a complex emotion in literature, metaphors used in economics tend to confuse as much as they enlighten.

### 6. Politicians always have a metaphor up their sleeve. 

Another sphere where metaphors are often employed is politics. Regardless of their party, all politicians use metaphor to sway public opinion and convince people to vote for them. Therefore, it's important to not be blindly charmed by their rhetoric but to recognize and reflect on these metaphors.

Politicians use metaphors to take advantage of the power of _suggestion,_ whereby they use certain words to trigger unconscious responses in their listeners and readers.

A study conducted by psychologist Thomas Gilovich highlighted this suggestive power. He told three groups of Stanford students to picture themselves as high-ranking US officials and then asked them what they would do in one of three distinct scenarios.

In all the scenarios, a small nation requested US assistance after being attacked by a bigger, more powerful nation, but each scenario differed in the historical allusion it drew: one was designed to allude to the relatively popular World War II intervention, the second to the unpopular Vietnam War intervention, and the third to a historically neutral situation.

It turned out that the groups acted differently depending on which historical and political situation was alluded to. Students were more likely to intervene after hearing allusions to World War II whereas those who heard allusions to the Vietnam War were less likely to get involved. And those presented with the historically neutral scenario deliberated their options longer than the other groups.

Clearly then, suggestive political images can influence our decisions.

Another way politicians manipulate us through metaphor is by comparing the nation to a body. The "nation as body" metaphor (which refers to physical health) is common when discussing immigration because it allows politicians to talk about, for example, "contamination by foreign agents." It turns out that when someone hears the "nation as body" metaphor, their thoughts on immigration can become much more critical.

### 7. Both scientists and psychologists employ metaphors in their work. 

Despite their controversial use in politics and finance, metaphors can also be used for the sake of good: they help simplify complex scientific ideas and make it easier to discuss traumatic events.

For scientists, metaphors are descriptive tools. Typically, scientific discovery starts with a hypothesis, which, until proven, is usually described by a metaphor.

For example, the _theory_ _of_ _continental_ _drift_ claims that, at one point in time, all continents were connected, after which they drifted apart. And scientists used the following metaphor to explain it: like rice pudding, the earth is brittle on top but fluid underneath. The metaphor isn't evidence verifying the theory but a visual aid to facilitate a better understanding of something we can't have actually experienced.

Another area where metaphors are often used is psychology, as they make it easier to discuss emotionally difficult subjects.

One example of this is the "clean language" technique developed by psychotherapist David Grove in his work with post-traumatic stress disorder patients. Grove noticed that his patients spoke in metaphors when discussing their trauma, so he started responding with metaphors.

If a patient stated that he was a "ticking time bomb," Grove would ask: "What kind of bomb?" or "Is there anything else about the ticking?" By staying in the realm of metaphors, he hoped to understand their disorders in the same way they did, and also to eventually improve their conditions by changing their underlying metaphors.

### 8. Metaphors can spur innovative solutions to business and technology problems. 

One of the major benefits of metaphors is their ability to show existing problems from new perspectives. As a result, they are handy for solving difficult business problems and fostering innovative technological solutions.

Consider the example of the global innovation consultancy _Synecticsworld._ It was asked by a health insurance company to analyze why people were opting out of their insurance. The company discovered that people tended to associate negative metaphors with insurance companies, such as leeches that sucked money from customers.

As a solution, Synecticsworld helped the insurance company come up with some new metaphors, e.g., health insurance as a safety net that's always there to catch you if you fall. By looking at metaphors and implementing the solution through them, the insurance company managed to address its problem.

Metaphors can also foster innovation by highlighting how the same problem is addressed in different areas.

One great example is the emerging field of biomimicry, where solutions to human problems are found by looking at how nature solves similar problems.

For instance, in the shipping industry, millions of dollars were being wasted every year because barnacles and other marine life were attaching themselves to the hulls of ships and reducing fuel efficiency. In response, scientists started to investigate why sharks didn't suffer from the same problem.

Eventually, a new material based on the structure of shark skin was developed. This drastically reduced the extra weight and drag on ships, thereby saving money and increasing shipping speed.

> _"In the act of satisfying the desire for innovation — in design, in business, or in the arts — metaphor has an uncanny knack for bringing imagination to light."_

### 9. Advertisers, too, take advantage of the persuasive power of metaphors. 

Virtually everywhere we look, we're accosted with advertisements. Every product from banjos to breakfast cereals is advertised, and advertisements often employ metaphors for the greatest pull.

One way advertisers successfully use metaphors is through _personification_, or the attribution of human qualities to inanimate objects. The goal is to get the customers to identify with products so they want to purchase them.

Let's say an advertiser animates a product by giving it a nose, mouth and eyes, and then has it talk directly to customers. This way, the product appears like an individual with thoughts and feelings.

Another method is to have the person identify with someone depicted in the advertisement. For example, an advertisement for a sports car typically has a very attractive person at the wheel. That's because the advertiser wants you to identify with the driver, i.e., to make you believe that if you would just purchase that car, you could be as attractive and successful as that person.

Another way that advertisers use metaphors is through familiar associations. After all, metaphors depict one thing as something else, so they also allow advertisers to imbue their products with meanings and associations that have nothing to do with the actual products.

For example, if you've ever seen a TV commercial for a breakfast cereal, you might've noticed that it's never presented as just some breakfast but as a manifestation of domestic bliss: a smiling family gathered around the perfectly set breakfast table where they all start their day together. Ads like this make customers associate familial happiness with the product — even though every rational being knows that a bowl of sugary grains in milk really doesn't have that kind of effect on anybody.

### 10. Metaphor plays an important part in childhood development. 

You've already seen how metaphor is often employed to describe things that we can't identify or find the words for: a vessel from outer space, for example, is referred to as a _spaceship_ or a _flying_ _saucer_.

Young children find themselves in this situation constantly as they try to understand and describe the endless variety of new things they encounter in the world. That's why metaphors are a crucial part of childhood development.

Psychologist Alan Leslie has proposed that at around 12 to 14 months of age, children begin fashioning their ideas about the world and its objects through the way they play. More specifically, they play games where objects are treated as if they were others: they might, for example, pretend that a banana is a phone.

Though this may seem like mere child's play, it's actually a core part of the child's development as it allows their minds to connect information about the two objects.

Later on, as children learn to express themselves, these connections help them describe things. They string together simple nouns to create metaphors for objects they don't yet recognize: the author's son, for example, described the sun as a "big sky lamp."

This capacity for metaphor develops as the child grows: studies have shown that infants can only fathom metaphors relating to physical states. For example, preschoolers could only describe a _cold_ person as someone who literally hasn't got warm enough clothing, not someone who's unaffectionate. The ability to mix physical and mental imagery is only seen among children above the age of ten.

Since children are creating their own metaphors all the time, they will occasionally unwittingly land on a pre-existing one. For example, the author's daughter laid her doll down on some flowers and said it was sleeping "on a flower bed," unaware that the metaphor _flower_ _bed_ already exists in gardening.

As they gain more life experience, children learn to create and understand more complicated metaphors through which they make sense of the world around them.

### 11. Final summary 

The key message in this book:

**Metaphor** **is** **not** **only** **a** **literary** **device** **but** **a** **ubiquitous** **part** **of** **life,** **employed** **in** **various** **fields,** **including** **politics,** **economics** **and** **advertising.** **It** **influences** **our** **thinking** **deeply** **and** **can** **be** **used** **to** **describe** **new** **phenomena** **when** **known** **words** **fail** **us.**

Actionable advice:

**Keep** **your** **ears** **open.**

The next time you hear a politician or pundit using metaphors in his or her rhetoric, pause to think about what the implication of each particular metaphor is. What do you think their intended goal is in using those particular metaphors: to confuse and manipulate or to clarify? This will help you stay objective even in the face of the most charismatic politicians.
---

### James Geary

James Geary is an American author who has written the _New_ _York_ _Time_ s bestseller _The_ _World_ _in_ _a_ _Phrase:_ _A_ _Brief_ _History_ _of_ _the_ _Aphorism_. He is the former Europe editor of _TIME_ magazine.

