---
id: 55ed29d711ad1a0009000044
slug: i-know-how-she-does-it-en
published_date: 2015-09-09T00:00:00.000+00:00
author: Laura Vanderkam
title: I Know How She Does It
subtitle: How Successful Women Make the Most of Their Time
main_color: 9DD7E6
text_color: 465F66
---

# I Know How She Does It

_How Successful Women Make the Most of Their Time_

**Laura Vanderkam**

_I Know How She Does It_ (2015) reveals how career-minded women balance work, family and time for themselves. Based on extensive research covering time logs to interviews, these blinks provide practical advice on time management for the modern working mother.

---
### 1. What’s in it for me? Master the work/life balance. 

In modern Western societies, most of us have more than we have ever had before: well-paid jobs, social welfare and a wealth of consumer goods. On the surface, we should all be quite happy — but, of course, so many of us just aren't.

A great deal of stress and misery in modern life comes from our inability to find enough time to both develop our career _and_ enjoy our personal life. Most of us feel like we have to give up one of these essential goals, and concentrate solely on the other. 

But this isn't true. We don't have to give up our job, nor spend every waking moment slaving away at it — we can do both, and these blinks show you how. Although they concentrate mostly on strategies for working mothers, there is plenty of excellent advice that everyone can learn from. 

In these blinks, you'll discover

  * why none of us _has_ to spend the majority of our time at work;

  * the hour at which we are generally most productive; and

  * why the family meal doesn't have to be dinner.

### 2. Interruptions make our work days seem longer and longer. 

How much do _you_ work? Most of us think we toil at work for far too long, and our work/life balance seems heavily skewed towards work. This is especially true of working mothers: the more time they spend at work, the less time they have to spend with their families. But is this true?

According to the 2013 American Time Use Survey, the average mother with a full-time job works 35 hours per week. Mothers who earn over $100,000 USD work even more — up to 44 hours per week.

This does seem like a lot, but let's take a closer look. A week has 168 hours. If we sleep eight hours each night and work 44 hours each week, we've still got 68 hours per week that _aren't spent at work_. So why do we all think we're workaholics?

It comes down to interruptions: those small, unexpected events that break up our working day can actually make it feel longer. In order to measure her working week, one woman tried to keep her days as routine as possible to get an accurate measure of her time management. But it just wasn't feasible.

Constant interruptions when she least expected them kept her from working at a regular pace. Snow days prevented her from going to work, as did days when her children's kindergarten was closed and family events, from a christening to an airport pickup. 

Interruptions like these make us constantly feel like we're behind schedule and always needing to catch up on work. But we shouldn't have to feel this way! In the following blinks, we will look how people, particularly working mothers, can arrange their lives in order to get the most out of their days, weeks and years.

### 3. Make the most of flexibility wherever you can get it. 

Working mothers all know the feeling of guilt. Whether we feel like we're neglecting our families or our career responsibilities, we end up putting a huge amount of pressure on ourselves to fulfill other people's needs.

But this pressure doesn't have to be there at all. As we've seen, there's enough time in the week to commit to work _and_ enjoy free time. It's just a case of _planning_ time to ensure you make the most of both. But how?

The first step is to find your opportunities for flexibility in your work. Today, 97 percent of full-time employees are afforded some sort of flexibility, and it's important that you make the most of it. Remember those interruptions we learned about in the previous blink? Well, it's the flexibility of your job that will help you cope with them. 

This flexibility goes in both directions: it allows you to leave work earlier in family emergencies, but it also allows you to work from home, so that when the kids are in bed you're able to catch up on what didn't get done during the day. 

There are several approaches to increasing flexibility in your job. If you work long hours and want to find more time to spend with your family, you could start working _split shifts._

This would mean working early morning hours before the kids wake up, then spending quality time with them over breakfast, after which you can return to work. Or you might leave work early to visit your kids' activities in the afternoon, but then work another "shift" from home once they have gone to sleep. 

Another idea is to _work remotel_ y from home or somewhere else at least one or two days a week. While many of us worry that working from home will cause our private and professional lives to mix in an unpleasant way, this isn't necessarily the case. A study by IBM and BYO found that people who are able to work from home could work a whopping 57 hours per week before experiencing work-life conflicts — and that was only among a quarter of participants!

### 4. Play with your work schedule to make it as efficient as possible. 

The first step to enjoying free time is preventing yourself from working too much. Of course, you could limit your overtime hours by clocking off everyday at the same time, but if this means you stop working before completing certain tasks, you might begin to feel quite inefficient. 

Your best option is to start the day with your toughest task. According to a study by Johnson & Johnson, a person's energy level peaks at 8:00 a.m. So, save your gossiping with colleagues for lunch and spend the morning working on the most difficult item on your agenda — the one that is most urgent, and requires the most effort. 

You can also maximize your efficiency by managing your meetings — they don't have to be a pointless waste of your time! There are a couple of ways to go about this. First, look through your calendar and find the meetings for which your presence probably isn't required. By choosing not to attend these kinds of meetings, you'll have gained an hour or so to work on something productively. 

For the meetings you do have to attend, try and shorten them whenever possible. Say you've got two 60-minute meetings coming up. Why not shorten them to 45 minutes each? This isn't a huge cut, but it will save you half an hour overall. It all adds up!

Finally, to ensure you have enough time for yourself, plan things to do in your free time. Schedule something in the evening at least once a week that will get you out of the office early, or at least on time. Why not sign up for that guitar lesson starting in the early evening? You'll also boost your motivation levels at work if you've got something to look forward to.

### 5. Reflect and plan ahead of time to keep your work fulfilling. 

The tips provided in the previous blink are easy and practical ways to improve your time management at work. There are two more techniques that will help you cut down stress in your job, but these require a little more thinking. 

One strategy to streamline your week is to plan it before it even begins. According to a 2013 survey by Accountemps, Tuesday is the most productive day of the week with Monday lagging behind at only 26 percent productivity! Why is this? 

Well, people tend to have a lot of meetings on Monday, and they also spend the day sorting through what needs to be done, as well as what wasn't done the week before. 

To avoid this "Monday trap," plan your week at the end of the previous one. On Friday afternoon, just before you leave for the weekend, make a plan about what you are going to do in the week ahead. This will take some pressure off your Monday schedule.

The final way to become more productive at work is simply to try and love what you do. We all know that we perform better when we enjoy what we're doing. So, why not try and work on as many enjoyable things as possible?

One woman leading a career in science, for instance, found that she was lacking drive at work. She realized that it was because her time was being taken up by administrative tasks, rather than the scientific research that she loved to do. So, she planned her weeks more consciously to ensure she could spend some hours in the lab again. Soon enough, her motivation returned. 

So far, we've discovered strategies for excelling in our careers while finding time for ourselves. The next few blinks will focus on how you can bring new energy to your family life too.

### 6. Make sure your family time is quality time. 

Planning is key to an efficient and flexible work life. But what about when it comes to your home life? Ultimately, your approach should be quite similar. 

Spending quality time with your family requires some planning and the same motivation as your work life does. But remember: being _around_ your family and being _with_ your family are two different things. 

Think through your weekday evenings and plan a shared activity with your family. No, watching TV together doesn't count! If the sun is shining, why not take your kids to the playground and visit the ice cream truck afterwards? In the winter, it might be nice to take a weekly trip to the library and find some new books together. 

Being active together is a great opportunity to spend quality time, and exercise is a proven mood booster. Why not take your kids to the pool once a week or go for a bike ride around the neighbourhood? 

A classic way to enjoy family time is through sharing a meal together — but it doesn't always have to be a sit-down dinner.

A study by the UCLA found that only 17 percent of families have dinner together every night. About 60 percent have dinner semi-regularly, but these dinners tended to be fragmented. Instead, why not try sharing breakfast together, or coming home to enjoy a family lunch. 

Another family activity that we often dismiss is taking your kids to work. If you think it'd be too boring for them, think again! We often forget that, for kids, even the simplest activities around our work life (like riding an elevator or seeing a conference room) can be an adventure. Why not take them with you on special occasions? 

This also gives you an educational opportunity to share your passions, and discuss matters of success and challenge. Once you start taking greater efforts to enjoy family time, it's also important to consider how you could spend more romantic time with your partner.

### 7. Working mothers can and should make time for their romantic life. 

When asked by the author what they'd do with more free time, most women answered they'd like to spend more adults-only time with their husbands or partners. Between juggling the demands of work and family, our love lives inevitably get neglected. But it needn't be this way!

The key to finding time for romantic moments in your busy schedule is making it a priority and being creative with timing. 

Remember those dates you had at the beginning of your relationship? Believe it or not, it's still possible to have those dates again — it just takes a little bit of planning. A date doesn't have to be at night; in fact, what could be nicer than taking a break in your busy schedule to have a romantic lunch with your partner in the city?

An important thing to remember is that administrative time with your partner, in which you plan childcare, groceries and so on, should be scheduled separately from romantic time. 

As strange as it sounds, some couples like to think that going shopping or cleaning together counts as romantic time. While it might be fun to do things as a team, romantic time works best when you can just concentrate on each other and nothing else. 

Finally, seize opportunities and savor moments as they come — you don't have to have each moment carefully planned. No one is too busy for a five-minute morning cuddle in bed, or a kiss with your partner before you take off to work in the morning.

### 8. Let go of perfectionism and start seeking support. 

There's nothing worse than finishing work and coming home to a house full of pending chores. There's no point in using time management to create free time for yourself if you just fill it up with housework. In times like these, it's important to just _let go_. 

Remember, you're not participating in some game show that gives you extra points for empty clothes hampers or supremely well-adjusted children. So, how can you begin to let go of your perfectionism?

Well, one mother decided she would let her daughter wear whatever she wanted to school, letting go of the mandatory morning fight about matching clothes. Her daughter is still alive and happy, even though sometimes her shirt doesn't match her skirt. For her part, the woman found she had less to stress about each morning. 

Another important step is learning to accept help. Nobody can do everything by themselves, and there's no shame in getting some assistance when you can. It lets you focus on what you do best. 

If your financial budget allows for it, you can get help cleaning the house, the garden or have groceries delivered to save time. Kids can also participate in housework and learn to take responsibility by doing so. 

Childcare is a good way to save yourself time, but plan it wisely. Did you know it's often easier and more time-effective to have someone come to your house for childcare, such as a nanny, rather than investing in daycare? 

Of course, it's important not to skimp on childcare. 

Not only is it vital that your child spends time in an environment where they feel safe and encouraged, your career will also benefit from it. According to economist Sylvia Hewlett, women who take at least three years for parental leave lose 37 percent of their earning power over the remaining decades of their careers. 

So, finding childcare that you feel comfortable with, even if it costs a little more, is worth it in the end.

### 9. Personal space and time can be built with some simple methods and by changing your daily habits. 

So you've made time for work, for your family and for your partner. Now it's time to make time for yourself. If you still think it's not possible, here's a little experiment for you to try: get addicted to a TV show. 

Think about it, when you are totally absorbed in _House of Cards,_ free time almost magically seems to appear out of nowhere. You might find that you have half an hour after work and before dinner where you can squeeze in an episode. In fact, you'll start to find free hours whenever you can. 

Now that you've worked out how to find these free hours, why not start using them for something more rewarding? Studies show that TV is fun, but only ranks near the middle on scales of human enjoyment. It is better than commuting or getting your car repaired, but it is less fun than socializing or having sex. 

Reading scores a little higher on the enjoyment scale, but what's even more fun is getting creative in your leisure time. Creative leisure time can include picking fresh fruit, garden work in general, going to the opera or knitting. 

Finally, remember that you're allowed to take a personal day off from time to time. Larry Kanarek, who managed the McKinsey office in Washington, D.C., observed that everybody who was quitting because of exhaustion and burnout had free vacation time that they had not yet used. 

So why take the risk? Use your vacation days before you get tired of working, and put yourself first. After all, it's only by taking care of yourself that you can continue being all the women you want to be: professional, partner, mother and individual.

### 10. Final summary 

The key message in this book:

**As working mothers, we want to do it all: build a career, raise a family and find personal time. But doing so requires some balancing and organizing, as well as letting go of your perfectionist demands. This will leave you feeling happier, fulfilled and able to appreciate the small, beautiful moments in life as they come.**

Actionable advice: 

**Work well to sleep well!**

Be careful with the kind of work you do right before bed when you are working split shifts, as staring at a screen at night may result in a restless sleep. Plan this shift as you would plan your workday, being sure to include time to unwind before going to bed. You even might find some tasks that don't require much focus, such as answering some emails, planning the next day or editing documents, that are well suited to a quiet late night session. 

**Suggested** **further** **reading:** ** _The Politics of Promotion_** **by Bonnie Marcus**

_The Politics of Promotion_ (2015) offers insights into the ways women can prime themselves for promotion in any line of work. Filled with actionable tips and strategic career advice, it provides the political savvy you need to maneuver within the workplace and secure your next promotion.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Laura Vanderkam

Linda Vanderkam lives with her husband and their four young children in the United States. _I Know How She Does It_ is her third book on the issue of time management, after _What the Most Successful People Do Before Breakfast_, published in 2013, and _168 Hours: You Have More Time Than You Think_, published in 2010. In addition to her writing, she has also made numerous appearances on TV programs, including _The Today Show,_ and has published articles in various newspapers.

