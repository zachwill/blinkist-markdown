---
id: 59887690b238e100054c1d56
slug: i-thought-it-was-just-me-but-it-isnt-en
published_date: 2017-08-10T00:00:00.000+00:00
author: Brené Brown
title: I Thought It Was Just Me (but it isn't)
subtitle: Making the Journey From "What Will People Think?" to "I Am Enough"
main_color: 55CBDB
text_color: 2E6D75
---

# I Thought It Was Just Me (but it isn't)

_Making the Journey From "What Will People Think?" to "I Am Enough"_

**Brené Brown**

_I Thought It Was Just Me (but it isn't)_ (2008) is a guide to the visceral and thoroughly human emotion of shame. These blinks explain this complex feeling, discuss how it arises and describe ways in which empathy and connecting with one another can help humans heal.

---
### 1. What’s in it for me? Take an in-depth look at the emotion and experience of shame. 

Do you ever think back to something embarrassing you did in the past and wish the earth would just swallow you up as you replay that moment over and over? Perhaps you texted someone you liked and were flatly rejected, or maybe you made a major blunder at work in front of all your colleagues.

That feeling? It's shame — a complex blend of embarrassment, pain and a sense of isolation that makes you suspect your mistake somehow marks you as a mediocre or deeply fallible person.

But in the end, absolutely everyone is fallible. Realizing that mistakes, and the profoundly negative feelings they can prompt, are shared by many helps to mitigate our experience of shame, and can let us feel safe in the knowledge that we're not alone.

In these blinks, you'll learn

  * what can trigger shame;

  * how empathy can be a powerful antidote to shame; and

  * why we should all give up on the pursuit of perfection.

### 2. Shame is a confusing and painful emotion caused by rejection at sensitive times. 

There are plenty of uncomfortable topics of conversation that most of us will try to steer clear of, but one subject that people particularly loathe discussing is the emotion of shame. As a result, many of us don't have a proper understanding of what it really is.

Shame is a visceral emotion whose exact characteristics are difficult to describe, but at its core, it has to do with a feeling of not being good enough. Articulating such an experience can be difficult — after all, discussing shame requires us to, at least to a certain degree, relive the pain it causes.

That being said, when the author interviewed over 300 people about how they experience shame, she discovered a theme; shame is a negative feeling connected to a sense of rejection and the exposure of aspects of ourselves that we tend to hide.

Based on this information, the author put together the following definition: shame is a deeply painful sensation that stems from the belief that we're not good enough, and that this shortcoming will prevent us from being accepted by and belonging to a group.

But how does shame arise?

Most often, shame occurs when people seek compassion, but experience rejection instead. For instance, one participant in the author's study spoke about how her mother persistently shamed her because of her weight. When the participant visited her mother, the first words out of her mother's mouth would be about how she was still fat, and the last would be about how she hoped her daughter could lose weight before they saw each other next.

Or consider another participant whose mother committed suicide when she was in high school. It was a time when she needed support and compassion, but she was instead ostracized by her fellow students for being the daughter of a crazy lady who hung herself.

Even from these examples, it's clear that a lack of empathy precipitates shame, and that's precisely what we'll explore next.

### 3. The solution to shame is empathy; understanding another person’s perspective without judgment. 

If you get bitten by a venomous snake, you can receive a dose of antivenom that will save you. This is a dramatic example of the many antidotes available to us, and thankfully, a powerful antidote also exists for the feeling of shame — it's called empathy.

Since everyone experiences shame, what truly matters is that we learn how to manage it.

Every participant interviewed by the author pointed to empathy as the essential factor in quickly overcoming and recovering from shame. However, receiving empathy from others isn't the only important step; it's also essential for you to empathize with them.

Most people first feel empathy by sharing a difficult experience with another person and hearing them say that they understand. Knowing that another person has experienced what you're experiencing, and that it's not unusual or uncommon, makes you feel less isolated and more accepted.

But to achieve this understanding, you need to be able to see things from the other person's perspective without passing judgment, which means being present and aware of the other person's story.

For instance, one year, the author was overwhelmed by the number of tasks she had to complete one weekend. She had promised to bring cookies to a party at her daughter's school, but she forgot. In her embarrassment, she lied to the teacher, claiming a dessert brought by another visitor as her own contribution.

Later, when she told a friend about this, the friend responded by saying that the author had done her best, that she was juggling too many things and didn't want to make a bad impression on the teacher. This response was purely empathetic; there was no judgment, just a simple taking of the author's perspective.

You now have some sense of how to relieve shame. Next up, you'll learn how to identify when exactly you're experiencing it.

> _"Something moved me away from "I'm so stupid — I'm a terrible mother" to "That was pretty stupid — I'm an overwhelmed mother."_

### 4. Dealing with shame requires recognizing it and understanding where it comes from. 

Have you ever felt like you're coming down with some sort of illness, but just decided to ignore it? If you have, you likely learned that listening to yourself is important to your physical health — and the same goes for your mental health.

As such, to deal with shame, you first need to recognize it. Through her research, the author found that the people who could notice and precisely describe their shame were much better at handling it.

Some of these people, when confronted with shame, might get a dry mouth and have difficulty swallowing. Others turn red and start shaking, while still others become incapable of even getting out of bed.

However it manifests, noticing your shame enables you to overcome it, because to truly deal with shame, you also need to understand what triggers it. Accurately identifying shame lets you start looking for these triggers.

That being said, there are no universal causes of shame, since it's largely dependent on negative personal experiences from early on in one's life. For instance, one of the people interviewed by the author, Sylvia, made an error at work and found herself on the company's loser list, a piece of paper posted in the hallway detailing all the people who had messed up that month.

While others might have shrugged it off, Sylvia, who grew up with an extremely competitive father, simply couldn't. Her father had always told her and her sister that nobody likes losers, and that there's nothing worse than being one. Naturally, being publicly shamed as a loser was deeply triggering for her.

The good news is that by knowing the triggers of your own shame, you can take a step back when it starts to flare up. Doing so will give you a better shot at processing the emotion and emerging with a positive feeling on the other side.

### 5. Critical awareness gives us a wider perspective on shame. 

On one occasion, while giving a presentation on critical awareness to a pharmaceutical company, the author found herself losing the interest of her audience. Rather than feeling shame or panic, she simply told them that they seemed more interested in the pizza that would follow the presentation than in her talk. By doing so, she was exercising _awareness_ of herself.

She went on to explain that she knew they only had a short lunch break and that the pizza was a major incentive for them to be there at all. By pointing this out, she was displaying _critical awareness_. This all-important ability refers to understanding both why and how something happens.

Critical awareness is vital to handling shame because it gives us the ability to zoom out. After all, when we feel ashamed, we can't really think of anything else. By taking a step back we can notice the social causes of our shame and address it more easily and effectively.

For instance, when we complain about our frizzy hair, our freckles or the size of our bellies, it can feel like we're the only ones experiencing such thoughts. But the reality is that beauty and body image are almost universal triggers for shame. To understand these thoughts, we need to look at society and understand the expectations it places on us.

Not only that, but critical awareness helps us see when we're being manipulated. By zooming out even further, we can see that there's an entire industry fuelling our negative feelings regarding body image, and that we consume its content in the form of magazines and TV shows.

Such media makes us feel shame, and that's exactly what this industry wants; this shame is what makes us spend so much money trying to live up to impossible standards of beauty and attractiveness.

Naturally, the results of such a process are disastrous. For instance, some 7 million women in the United States alone suffer from eating disorders. But by being able to see this bigger picture, we can realize how others suffer too, normalizing shame and taking away its sting.

### 6. Connecting with others is a form of healing, both for us and for them. 

When we feel hurt or ashamed, a common reaction is to retreat into ourselves. However, what we actually need is to do exactly the opposite; connecting with others is crucial to healing from the experience of shame.

The opposite of shame is self-esteem. There is a broad tendency to believe that certain things, namely a successful career or a terrific outfit, will boost our self-esteem — but the reality is entirely different.

The psychologists Jean Baker Miller and Irene Stiver found in a 1997 study that forming and maintaining relationships is the most reliable way to make people feel grounded and sure of their value. And this is especially true when it comes to shame.

Having friendships and support networks through which we can share our life experiences shows us how others share our difficulties. This removes the isolation from shame, makes it easier to manage and can change the way we see other people.

We can even help other people heal by reaching out to them. Such an ability is so powerful that we can actually transform shameful experiences into positive ones.

Just take a participant interviewed by the author, whose father was married to a younger woman and whose mother was married to a man with six previous wives. She was often shamed for the eccentric marital histories of her parents, and when she hears someone being judged for having a strange family she shares the story about her own.

When it comes down to it, we all have weird families, each with its own imperfections. It's only because we habitually lie about such things that visible imperfections are met with ridicule.

### 7. The lie of perfection fuels shame and makes it hard for us to care for others. 

Most of us know that the perfection we see depicted on TV isn't real. But it's still easy to get sucked into the illusion that it is.

This is a prime example of how perfection is a shame-producing lie. Just take Alex, the iconic 1980s character played by Jennifer Beals in the movie _Flashdance_. In the famous dance audition scene, Alex nails an incredible number that combines ballet and breakdance. But in reality, the scene is a combination of Jennifer Beals' face, a professional ballet dancer, a top gymnast and even a male breakdancer.

With enough editing and polish, movie scenes or photo shoots end up providing the false image of perfection that we get from the mass media. Naturally, the major problem with such images is that we're led to believe that we should admire and emulate these perfect humans; who _wouldn't_ feel ashamed trying to live up to such an unrealistic ideal?

But there's another issue with the perfection lie: it makes it difficult to fill human roles, like that of a caregiver. After all, if we expect to be perfect, we'll expect it from others too. This can only lead to difficulty in imperfect situations, such as caring for an elderly parent who is losing his cognitive faculties.

As a result, caregivers are often extremely hard on themselves, disappointed by their feelings of resentment. For instance, one of the author's interviewees, Chelsea, cared for her mother for two years before putting her into a nursing home. But when she did, she was overcome by guilt and shame for not having been able to do everything by herself.

It's clear that such ideals of perfection aren't healthy, and a good way to overcome them is to show the opposite: vulnerability. By accepting our own limitations in such situations, we can alleviate many of our shameful feelings.

### 8. Anger is a tempting way to cover up shame – but it only makes things worse. 

Have you ever totally lost it on someone just so you didn't have to be honest about how hurt, ashamed or embarrassed you were feeling? It's a common reaction, and anger is a tempting tool with which to hide shame.

In fact, shame even appears to be directly related to blaming, as people are constantly pointing fingers to avoid confronting their feelings. Psychologists June Tangney and Ronda Dearing say that people can protect themselves from their shame by projecting it outward and blaming others through a self-righteous burst of anger.

Essentially, this is a way for people to regain control over their shame by exerting power. However, anger really just makes things worse.

Many of the people the author interviewed admitted that they invariably regretted the angry outbursts that resulted from their shame. While such explosions might feel good in the moment because they relieve some of the pain, in the long run, they are simply detrimental to relationships.

People can become alienated because they can't understand where their anger is coming from. This, in turn, isolates the angry person, producing more shame as their connection to the other is weakened.

But that doesn't mean everybody should suppress their anger. Anger is actually a healthy emotion — just not when it's hiding another one. That's why, if you're feeling shame, it's important to try to stay with it, describe it and reach out to other people.

You now know that anger can be a mere mask for shame, that focussing on perfection isn't a solution and that it's essential to avoid being trapped by shame.

Rather than letting yourself fall into these unhealthy patterns, let yourself be vulnerable, turn to those around you to forge connections and share the empathy that you and others need to heal.

### 9. Final summary 

The key message in this book:

**We've all experienced shame and we'll all experience it again at some point down the road. Rather than trying to ignore this inevitability of life, it's essential to acknowledge and normalize it. By doing so, we can constructively deal with this painful emotion, connect with others and emerge from a difficult experience with our self-esteem intact.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Gifts of Imperfection_** **by Brené Brown**

_The Gifts of Imperfection_ offers an accessible and engaging walk through the ten principles that you can follow to live a more fulfilling life, defined by courage, connection and compassion towards others. Filled with relatable anecdotes and actionable advice, the book is a useful resource for readers both young and old.
---

### Brené Brown

Brené Brown is a social worker and scientist who has interviewed hundreds of people about the emotion of shame. A popular speaker, she has given various TED Talks and is the author of several other titles, including _Daring Greatly_ and _The Gifts of Imperfection_.

