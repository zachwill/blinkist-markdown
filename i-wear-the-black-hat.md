---
id: 57179bca3f35690007554dcb
slug: i-wear-the-black-hat-en
published_date: 2016-04-29T00:00:00.000+00:00
author: Chuck Klosterman
title: I Wear the Black Hat
subtitle: Grappling With Villains (Real and Imagined)
main_color: F43150
text_color: C22740
---

# I Wear the Black Hat

_Grappling With Villains (Real and Imagined)_

**Chuck Klosterman**

_I Wear the Black Hat_ (2013) takes a look at villainy as viewed both in reality and pop culture. Find out how our modern perception of villains has been formed over the ages and how even heroes can be transformed into bad guys. You'll discover how villainy is often in the eye of the beholder.

---
### 1. What’s in it for me? Get a grip on villainy. 

Have you ever wondered how the name "Hitler" became synonymous with "evil," even though Mao killed at least four times as many people? And why do we consider Batman a hero whereas real-life vigilantes are often labeled as dangerous outlaws? 

These blinks will explore the role of villainy in modern society and culture.

In these blinks, you'll discover

  * why George Bush probably won't be remembered as a villain;

  * how criminality and villainy relate to one another; and

  * why fiction changes our perception of villainy.

### 2. “Goodness” and “badness” are just figments of our imagination. 

How do you decide whether someone is "good" or "bad"? It might seem easy to decide if we believe that a person is born either good or evil. And yet, there are actually more philosophical reasons behind the way we perceive villains.

After all, our own definitions of "good" and "bad" are generally biased as they are constructed on cultural concepts, concepts generated through our shared understanding of history, the stories we were told as we grew up, and the heroes and villains in them. 

For example, history and stories have taught many Americans to believe that Russians were somehow "bad" by nature. And yet, many Russians held the opposite belief due to their own society's definitions of "good" and "bad."

Philosopher John Rawls came up with a thought experiment to explain why we are incapable of having an unbiased definition of "good" and "bad": the so-called _veil of ignorance_.

Imagine that you're given the opportunity to play God and create a brand new society from scratch. You could make whatever laws or social conventions you want. But the catch is: once the new society is created, you'll enter into it as a brand new person with no way of knowing what position you'll be in. You might be homeless, a refugee or a terminally ill person with completely different morals from the person who designed the society you now live in.

With this veil of ignorance, you would probably try to create a society that is as fair and as "good" as possible for everyone. But this begs the question: is it really even possible to define a society which every person would see as "good"?

For instance, a traumatized refugee is likely to have a completely different concept of "good" and "bad" than someone born into a rich family who has only known privilege and wealth.

### 3. The term “Machiavellian” is closely related to villainy – for seemingly good reasons. 

When you think of the term "Machiavellian," you might think of a villain who is deceitful and scheming in order to obtain power.

But when author Niccolò Machiavelli wrote his influential book, _The Prince_, he was not setting out to create an instruction manual on how to be "bad." Modern scholars have suggested that _The Prince_ may have been a criticism of the political practices of his time. 

When Machiavelli was alive during the turn of the sixteenth century, the popular opinion at the time was that monarchs retained their power due to their good character that the public respected.

However, in _The Prince_, Machiavelli suggests that power can be retained only by instilling fear into the public. This has naturally led the term "Machiavellian" to be considered villainous.

But why use the name of the author to describe such behavior?

It turns out that the act of genius it took to write such a devious how-to book on being a villain can turn the author into one as well. This is because Machiavelli didn't care whether or not his book ended up as a guide on how to use villainous methods to retain power.

So let's look at the definition of "Machiavellian" to make it easier to identify a villain.

Take George W. Bush, for example. While he's often been considered one of the worst presidents in the history of the United States, is he a real villain? Given the fact that he seemed to know very little and appeared on TV as a caring person, he was actually the opposite of a Machiavellian villain.

His right-hand man and vice president, Dick Cheney, on the other hand, had the characteristics of a true villain. As the manipulating mastermind behind the wicked acts of the Bush administration, he increased the scope of presidential power. Despite the fact that he seemed to know exactly what he was doing all the time, he didn't appear to care about how his actions harmed others, including the president himself, whose public opinion suffered greatly due to Cheney's puppeteering.

### 4. Even drug dealers and plane hijackers can be considered heroes. 

When it comes to criminals, our first instinct may be to cast them off as "bad" people since they are willfully breaking the law. Yet, throughout history and in popular culture, we've often labeled criminals as heroes, not villains.

That's because criminal behavior isn't synonymous with villainy.

Take the popular TV show _The Wire_. The cast of characters includes many drug kingpins and high-level criminals including a thief named Omar Little, a character with so many "good" qualities that he refuses to swear. 

Another "good" criminal character in the show is the drug dealer Stringer Bell: he values education and intelligence enough to enroll in community college in order to improve his business skills.

But perhaps the best example from popular culture is Walter White, the meth-cooking chemistry teacher from the series _Breaking Bad_. At the beginning of the show, White is diagnosed with cancer. In order to make enough money to provide for his family after his imminent death, he starts making and dealing drugs, and becomes more villainous as the series progresses.

And it might sound crazy, but even someone who hijacks planes can end up being regarded as a hero.

This can be seen in the 1971 case of D.B Cooper: he hijacked a commercial airliner by claiming to have a bomb in his suitcase. He ordered the plane to refuel and demanded $200,000 cash and four parachutes. After taking off again, Cooper took the money and parachuted off the plane, never to be heard from again. 

But throughout the ordeal Cooper remained so polite that a flight attendant later called him "a gentleman."

How is it that even a hostage could come to respect Cooper? Well, one difference between heroes and villains is that heroes can have the kind of self-confidence that can lead others to respect them.

For example, while Cooper displayed the Machiavellian traits of deception and lack of empathy for fellow passengers, he also pulled off his crime with impressive self-confidence and mastery. And to top it off, he was polite and soft-spoken the entire time.

### 5. If Batman were real, we’d probably consider him a villain. 

Another great example of the disconnect between hero and villain is the character of Batman. If you imagine a world where the Batman comics had never been released, what would you think about reports of a man in a weird suit beating people up?

In fact, we usually consider real-life vigilantes as being just as "bad" or worse than the criminals they're fighting against.

In 1984, Bernhard Goetz was arrested in New York City after shooting four young, black men in the subway, claiming they were about to mug him.

New York was suffering from high crime rates in the 1980s, and some people considered Goetz a heroic vigilante who was protecting the city and taking a stance against crime. This lead to his four would-be muggers being sent to jail and Goetz, later nicknamed "The Subway Vigilante" being set free.

But eventually accusations of racism surfaced and the public began to turn against Goetz. And today, Goetz is popularly regarded as a villainous vigilante who got away with a crime. 

The character of Batman, however, is considered a hero in large part because of a fictional backstory that helps us relate to his motives.

As a young boy, Bruce Wayne witnessed the murder of his parents at the hands of a mugger. The event would later motivate him to become a vigilante and fight crime in Gotham, a city not unlike the crime-ridden New York of the 1980s. In light of his tragic past, Batman's vigilantism seems justified and even heroic.

People viewed Goetz in this way after his own would-be mugging. To the general public, he appeared to be a near-fictitious character fighting crime: just as the death of Batman's parents led the creation of a hero, the attempted subway mugging of Goetz led him to heroically defend himself.

But as Goetz's racist motives and strange personality emerged, the more the public began to despise him, and the hero slowly turned into the villain.

### 6. Challenging the status quo can make people consider you a villain. 

What do Perez Hilton and Kim Dotcom have in common? Both have been very successful in beating the competition to be at the forefront of modern information distribution. But while their success has brought them fame, it has also brought them infamy.

Perez Hilton isn't only considered a villain for being an online gossip blogger — after all, it was his job to report on the sordid details of the Hollywood elite. In fact, he helped validate the online blog format as a profitable medium. However, he eventually became infamous for posting nude photos of teen celebrities with improper captions and outing gay celebrities without consent. 

In some circles, Hilton is viewed as a villain for validating the format of an online blog as a profitable way to distribute questionable content. In one day during 2007, his website received 8.82 million hits, an impressive achievement for a blogger. Many people look at Perez Hilton as being responsible for lowering the standards of what is considered "appropriate" content when it comes to celebrity gossip.

This has also lead to jealous animosity: Hilton found a new way to be "successful" in his industry and people resented the fact that they didn't get there first.

For Kim Dotcom, he became a villain after he forced change upon the entertainment industry.

In 2005, internet entrepreneur Dotcom created the file-sharing service Megaupload. This lead to FBI claims that Dotcom and Megaupload cost the entertainment industry $13 billion in lost CD sales in a single year.

But the real reason the industry sees Kim Dotcom as a villain is because he helped change the way people consume media, forcing industry giants to change their business model. Making matters worse, Dotcom openly ridiculed these media corporations for failing to see the future of their industry.

### 7. Although we consider Hitler the personification of evil, it’s not because he killed the most people. 

For centuries, the Devil was the perfect example of "badness." But as time went on, non-religious people found another benchmark for evil: Adolf Hitler.

But it turns out some villains are measured by different yardsticks.

When Americans hear the word "tyrant," three people usually come to mind: Hitler, Stalin and Mao. And, of those three, Hitler is almost always considered the most evil, despite the fact that most estimates say Stalin killed six million more people than Hitler, and Mao a whopping 61 million more!

Many Americans wouldn't even recognize Mao or Stalin on the street, but just about everyone would stop dead in their tracks if they saw Hitler.

So how does it happen that the level of one's villainy can be determined by how recognizable they are and not by the amount of evil they commit?

It might be that Hitler is hated the most because we see him as more human.

This is due in large part to a cultural element in Western society that has a dark fascination with Hitler that has revealed obscure and seemingly irrelevant characteristics about him.

For example, it is common knowledge to many that Hitler was vegetarian and likely had only one testicle. These intimate details humanize Hitler and inadvertently allow us to relate to him in a way that we don't with other mass-murdering tyrants, such as Stalin and Mao.

The fact that some of us might share medical afflictions and a vegetarian diet with a man who went on to commit the Holocaust makes the evil he committed all the more human, and therefore all the more evil. As strange as it may sound, the reason Hitler is considered more evil than Mao or Stalin could be due to the fact that their dietary habits are largely unknown.

### 8. Final summary 

The key message in this book:

**Defining villainy is not as easy as you'd think.**

**Nowadays even criminals are remembered as heroes. Sometimes, being a villain comes down to simply being smart and not caring about anyone else. Other times, people will see you as a villain for coming up with a great idea before they did. At the end of the day, being a villain depends on the culture you're in and society's shared understanding of history and morality.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Moral Landscape_** **by Sam Harris**

These blinks explore cutting-edge research in neuroscience and philosophy to explain human morality. Harris argues that morality can best be explored using scientific inquiry, rather than relying on religious dogma or theology.
---

### Chuck Klosterman

Chuck Klosterman is an author and essayist who writes for the _New York Times_. He has written many best-selling books including _Sex, Drugs, and Cocoa Puffs_ and _Eating the Dinosaur._

