---
id: 5822fa2adf93d80004cc8ac1
slug: idea-to-execution-en
published_date: 2016-11-11T00:00:00.000+00:00
author: Ari Meisel and Nick Sonnenberg
title: Idea to Execution
subtitle: How to Optimize, Automate and Outsource Everything in Your Business
main_color: FFF047
text_color: 736C20
---

# Idea to Execution

_How to Optimize, Automate and Outsource Everything in Your Business_

**Ari Meisel and Nick Sonnenberg**

Based on the true story of two friends who founded a company overnight with zero cash investment and built it up over the following year, _Idea to Execution_ (2016) is about the everyday challenges entrepreneurs face when launching a start-up. These blinks show you the steps to success, from sketching a business plan to putting tools in place that optimize your company's performance numbers.

---
### 1. What’s in it for me? Create a successful business in just one day! 

Have you ever come across a new product or service and thought to yourself, "Hey, I had the same idea!"

What if, instead of keeping your inspiration to yourself, you had seized the day and turned your idea into a thriving business?

There are many reasons why we don't act on great ideas. You probably felt overwhelmed by the complexity of starting a business from scratch or were intimidated by the amount of capital you might need to get started.

But times have changed. The fact is, it's never been easier to set up a company, quickly and with little or no investment.

These blinks show you the steps, from day zero to infinity, to starting a business and keeping it going once you've begun. The secret? Optimize, automate and outsource.

In these blinks, you'll learn

  * why a lack of available funds is not a deal breaker for an aspiring entrepreneur;

  * how to build and manage a remote team of workers; and

  * why customer engagement is crucial to a company's success.

### 2. It’s possible to start a company in just 24 hours if you need to seize an opportunity while it’s hot. 

Many people believe that starting a company requires years of careful planning and diligent market research. But sometimes it's best to seize an idea spontaneously and run with it.

While it might sound crazy, it's possible to bring a start-up to life in just 24 hours. This isn't mere hyperbole, as it's been done!

In August 2015, authors Ari Meisel and Nick Sonnenberg were having dinner when they came up with an idea: a company that provided virtual assistants. Both friends had extensive expertise in maximizing business efficiency and productivity, so it seemed quite feasible.

Just one day later, their business was up and running.

How did they realize their idea so rapidly? And why did they feel the need to act immediately?

Their timing was propitious, as Zirtual, one of the largest US-based virtual assistant firms, had just folded. This left 2,500 clients without assistants and 400 assistants without a job. Ari and Nick saw the opportunity of a lifetime — but only if they responded quickly.

The authors knew they had a narrow window of opportunity. This is, of course, true for all start-ups, regardless of industry. Savvy entrepreneurs know to grab an opportunity while it's hot.

The duo that night made a quick sketch of their virtual assistant business. Their first clients and assistants were secured from the Zirtual fallout. Within 24 hours, their company, _Less Doing Virtual Assistants,_ was up and running, using free online tools and apps as infrastructure. They didn't need a penny to get started, and their business was scalable and profitable from day one.

Over the year, Ari and Nick consolidated their company, dealing with challenges as they arose. These blinks tell the story of that adventurous year.

### 3. The authors’ business acumen helped them identify the flaws in other virtual assistant services. 

The authors were well aware of the flaws of other virtual assistant companies, and knew that their start-up could address such problems more effectively.

One of the major limitations of the virtual assistant business model was that it offered either _on-demand assistants_ or long-term _dedicated assistants_.

On-demand assistants are assigned simple, one-time assignments, such as coordinating a delivery or arranging an appointment. Because there's no continuity or long-term relationship, they can't work on large projects. Dedicated assistants, in contrast, are responsible for a wide range of tasks for a single client — but can also represent a bottleneck, since one person only has so many hours in a day.

Under the Zirtual model, all of the client's needs, whether booking meetings, conducting marketing research or designing a website, would be delegated to a dedicated virtual assistant.

But it was unlikely that the virtual assistant would have a sufficiently broad skill set to manage the variety of tasks assigned. This is where Ari and Nick identified the market opportunity: there was room for a new model to match a client's needs with assistants specifically suited to a task.

It's a simple truth that business success starts with offering customers a different or better service than what is currently available. The duo's business acumen armed them with the knowledge that busy executives were looking for a one-stop solution for hiring virtual assistants.

Ari and Nick came up with the innovation of addressing both issues with one solution: a client's specific needs would be matched with the specific skills of an assistant, but through one channel — their company.

Clients would benefit from a _team_ of assistants. The team would be small enough to remain intimate and personal, but large enough to ensure that each client request was completed by a person uniquely qualified for the task.

A manager would lead each team and serve as the client's point of contact, thus ensuring a personal touch and maximum efficiency.

### 4. Infrastructure can be built with free tools, but services should always be of the highest quality. 

If you think a start-up can't launch without sophisticated, expensive infrastructure, think again. A new business can be run easily with software tools that are not only free but also easy to use.

To manage client tasks, Nick used a free management program called Trello. Trello's structure is straightforward. It offers boards for specific projects; lists that give the status of a project; and cards that show which tasks still need to be completed.

Nick found an innovative way to use the software's project boards, deciding to assign a board to each client. He then created a template to make uniform lists for each client, ensuring continuity.

Each board now reflected a client's requested tasks and the status of each — fulfilling the basic operational needs of the start-up at no cost.

The next step was to ensure that the new business wasn't merely offering basic services but was going above and beyond each client's expectations.

The virtual assistant industry mostly targets demanding, high-earning professionals, so guaranteeing a maximum of quality would be key to the company's success.

To secure the most talented assistants in the market, Ari and Nick committed to paying employees two to six times the going rate for requested services. Doing so also meant the company would charge clients four to eight times more than other virtual assistant companies. Offering top-notch services was how they justified the higher prices. They also billed by the second and openly shared all timesheets with the client.

Here's just one example of the start-up's focus on premium service: they created a business website for a client of theirs, a real estate agent named Chip, and had a virtual assistant follow up on the leads coming through it. The assistant only booked appointments for Chip if the lead was qualified — so all Chip had to do was show up on the date and time of the appointment to seal a deal, allowing Chip to do less but win more.

This was exactly the kind of time-saving service that the company's name promised!

> _"It is our belief that people should work on only the things they derive true enjoyment from or utilizes their unique abilities."_

### 5. Be clever when hiring. Have a plan and look for potential employees with a proactive attitude. 

Applying for jobs is a grueling process. But hiring employees isn't a walk in the park either — in fact, hiring can be one of the most energy-consuming tasks for a young company.

So when you begin to look for new team members to grow your start-up, you need to have a thoughtful, efficient strategy in place.

Once Ari and Nick had established their start-up's infrastructure, they were ready to hire some high-quality assistants to build the business — seeking maximum returns for minimal effort.

First, they created two email addresses: jobs@lessdoing.com and interview@lessdoing.com.

Candidates would send an application to jobs@lessdoing.com and receive an automated response detailing the position available. The applicant was then instructed to send a two-minute video pitch and upload it to YouTube, sending the link to interview@lessdoing.com.

Right off the bat, this two-step process eliminated about 80 percent of applicants. Those who couldn't figure out the instructions properly or created a poor-quality video were immediately filtered out.

Ari and Nick figured that if an applicant couldn't come up with a creative solution to the problem, then that person certainly wouldn't make a good virtual assistant.

Second, the team made sure they identified proactive candidates with a winning attitude. Top-notch virtual assistants need to have not only excellent problem-solving skills but also the ability to plan ahead and seize opportunities when they arise.

One of Less Doing's many experienced virtual assistants offers a prime example of this ideal type. By following the Facebook posts of an important client, the assistant learned that they loved a particular brand of whiskey. The company could then send the client a bottle as a special birthday present.

It is this attention to detail and dedication to customer service that sets a proactive worker — and business — apart from the rest.

### 6. Networking can help your company grow quickly, but growth can also present structural challenges. 

Networking is essential to moving your company to the next level. You can't just make a great product; you have to get out there and show it off.

In November 2015, Ari and Nick were invited to speak at a Genius Network event that Ari had participated in a few years earlier. Joe Polish's Genius Network brings together industry leaders, well-known authors and high-powered entrepreneurs.

They didn't have sufficient time to prepare for the event, however, and delivered their presentation off the cuff. Yet the two entrepreneurs had so much to say about their start-up's methods of tracking and outsourcing tasks that they held the audience rapt for some three and a half hours!

Audience members also posed some questions about the service, giving Ari and Nick the opportunity to demonstrate their company's approach to solving problems right then and there.

After the workshop was over, 90 percent of the people in the room had signed up to Less Doing's virtual assistant services — a networking success!

Getting a flood of new clients is good news, right? Sure — but sometimes growing quickly also brings new challenges and might require restructuring.

The start-up had to quickly come up with a way to deal with this boom. Nick built a new tool, a custom-coded dashboard that would give him and Ari an overview of all their Trello client boards.

The dashboard enabled them to easily see all tasks and each task's status, so they could optimize where to focus their attention and the attention of their team.

Now the team could keep an eye on the company's overall performance while maintaining rapid growth.

### 7. Don’t let negative feedback fester; use innovative problem-solving techniques to keep ideas moving. 

When you're nurturing a project that you feel is your "baby," it's not always easy to hear negative feedback. But when you do, it's vital to act decisively to fix the issues, before they bring down your company.

By January 2016, Less Doing was thriving, but some customers were complaining that the onboarding process wasn't smooth enough.

When Ari and Nick realized that this was costing them clients, they started to proactively attack areas of improvement in the company. One of their tools was the _5 Whys method_.

First, articulate the nature of the problem, then ask why the problem exists. Once you find an answer, ask "Why?" again. Repeat the questioning process three more times. By the end, you've led yourself to the root of the problem.

This method led Ari and Nick to realize they needed to simplify their software interface and also provide clients with better instructions for using it. Once tweaked, the onboarding process for clients was seamless.

To avoid future client complaints, the entrepreneurs decided to encourage employees to think proactively by using another tool called the _Kaizen method_.

This method was pioneered by Japanese companies such as Toyota and Mitsubishi. Kaizen translates to "change for the better."

With the Kaizen method, every person in a company — from cleaners to C-level leaders — is asked for input on what the company could improve, based on personal experience.

For Less Doing, this meant asking every virtual assistant to submit one idea per week on how to improve the company's processes or methods.

Immediately Ari and Nick were presented with a problem with an easy solution. They learned that some virtual assistants weren't sure how to properly charge clients. It turns out that assistants had been performing extra administrative work free of charge, leading to some frustration.

The Kaizen method raised this issue to a level where it could be addressed, taking care of both the clients' and assistants' needs appropriately.

### 8. Once you’ve nailed a strategy for success, you can teach it to others. 

By the time your business is going strong, you will have acquired many skills and a lot of insider knowledge. You'll then have the opportunity to create a secondary source of income through teaching other entrepreneurs what you've learned along the way.

To gauge the potential of training as a new offering, Less Doing advertised on Facebook that it would sponsor a business boot camp.

Some 20 participants signed up, and the presentation overall went well. But feedback after the event made it clear that the authors had come on too strong, overloading the audience with complicated technical terms and demonstrations.

While the experience on the surface seemed like a setback, it provided Ari and Nick with some useful data. They realized that hosting live events required a huge time commitment and also wasn't the optimal format for the information they wanted to share.

This is an important lesson: you need to be flexible and ready to adjust course as you develop a new branch of your business.

Ari and Nick didn't put an end to their teaching venture; they instead reconfigured their strategy. By moving all its operations online, it eventually became a sustainable arm of the business.

### 9. There are many ways to optimize a company’s processes, but getting clients hooked is priority one. 

Building a successful company requires an entrepreneur to constantly optimize operations, such as considering which tasks can be delegated or even outsourced.

Many managers try to keep a tight grip on a business as it grows, but sometimes it's smarter to let go and step back from the day to day.

Until April 2016, Ari managed Less Doing's dashboard, ensuring no assistant was late in completing a task and that all clients were satisfied with the company's services.

Eventually, he delegated this task to a virtual assistant who performed so well that she was named general manager just two weeks after taking the reins.

Ari and Nick decided to train another virtual assistant to handle the company's financial and payroll needs. Soon afterward, she was named the financial head of Less Doing.

Each of these situations was certainly win-win for Ari and Nick, who now had more time to dedicate to improving other aspects of the company.

And there was much to improve! It's not enough for a company to offer a great service; it has to ensure that clients are truly engaged with the product.

Less Doing still struggled to get its clients to commit fully. For example, some clients continued to perform tasks themselves that a virtual assistant could have handled.

The authors sought the help of behavioral psychologist Nir Eyal, author of the bestselling book _Hooked_, to help them find a better strategy to increase client participation. They discovered that they needed to invest more time and energy in the onboarding process to ensure new clients got hooked on the Less Doing service.

So the company changed its approach. Instead of spending 15 minutes on the phone with a new client, virtual assistants were to spend at least an hour coaching clients on how to best use Less Doing software to address personal and professional needs.

This process better acquainted clients with the range of Less Doing services, allowing clients to use their time more efficiently and commit to relying on more virtual assistance.

### 10. Once your structures are solid, start working with your company’s numbers and keep investing. 

Once you've solidified the processes that keep your company running, it's time to start working with your performance numbers.

When Ari and Nick took a hard look at their company's numbers, they realized the next task was to optimize the company's "churn rate." This industry term describes the percentage of clients who drop a company's services or product after a given period, such as within three months.

Using a tool called ChartMogul, the company tracked exactly how long customers used Less Doing services. The authors soon discovered that clients who ended up dropping the service were those who used it less than once every few weeks.

This knowledge helped Ari and Nick to establish a best practice. When a client was inactive for two weeks, they made sure to personally reach out to the client and address any issues, to ensure Less Doing didn't end up losing the client's business.

This sort of extra effort might seem time-consuming, but it's always better to invest in your company than to lose clients based on inaction.

It isn't always easy to muster the motivation to go this extra mile, of course. It can also be frustrating to discover that after a year of hard work, you're still sinking a lot of time and money into your company.

But remember: if you want to grow, you have to keep investing.

By mid-2016, Ari and Nick were still working hard to improve the company. Ari logged dozens of hours talking to inactive clients, working to drive the company's churn rate from 11 percent down to three percent. Nick automated several aspects of the dashboard to help further in removing menial tasks from the team's plate. They also strengthened outreach efforts by personalizing the company's newsletter and increasing its presence on Facebook.

At this point, Less Doing's managerial team had grown, so the founders decided it was time to give key players an equity share in the company.

These moves paid off, and as of July 2016, Less Doing had its most successful month ever, chalking up $100,000 in revenue. Not too bad for a company that started as a dream over dinner!

### 11. Final summary 

The key message in these blinks:

**You don't need a lot of money to launch a start-up. All you need is the ability to use existing tools to create a solid company structure and the tenacity to search for ways to improve your services.**

Actionable advice:

**Be tough when you're running a services company.**

To make sure virtual assistants were providing the best client experience possible, Ari and Nick implemented a policy of "three strikes, and you're out." If an assistant made three crucial mistakes, the person was fired. It might seem harsh, but such a policy ensures that only the best people are working for you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Less Doing, More Living_** **by Ari Meisel**

_Less Doing, More Living_ (2014) guides you through nine fundamental steps on your journey toward becoming more effective. In these blinks, the author shares his favorite tools and techniques for optimizing, automating and outsourcing everything on your to-do list, giving you time for the things that are most important in your life.
---

### Ari Meisel and Nick Sonnenberg

Ari Meisel was a real estate developer before he was diagnosed with Crohn's disease in 2006, making it impossible for him to work. He had to develop a new way of getting things done, so he founded the Less Doing system of productivity, detailed in his book _Less Doing, More Living._

Nick Sonnenberg was a high-frequency trader on Wall Street for eight years before becoming an entrepreneur specializing in efficiency. He developed CalvinApp, which connects people to make plans or share ideas. Along with Ari Meisel, he is the cofounder of Less Doing.

