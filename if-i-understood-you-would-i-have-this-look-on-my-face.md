---
id: 59fe4717b238e100073f4207
slug: if-i-understood-you-would-i-have-this-look-on-my-face-en
published_date: 2017-11-07T00:00:00.000+00:00
author: Alan Alda
title: If I Understood You, Would I Have This Look On My Face?
subtitle: My Adventures in the Art and Science of Relating and Communicating
main_color: EB745E
text_color: 9E4E3F
---

# If I Understood You, Would I Have This Look On My Face?

_My Adventures in the Art and Science of Relating and Communicating_

**Alan Alda**

_If I Understood You, Would I Have This Look On My Face?_ (2017) explains how improvisation techniques, as practiced by actors and comedians, can be used as training methods for better communication. Alda uses illuminating examples of miscommunications from his own life to show how we all can be better at relating and talking to one another.

---
### 1. What’s in it for me? Learn the subtle art of communication from a master of improv. 

One day, award-winning actor and screenwriter Alan Alda sat in the dentist's chair for what he thought would be a routine dental operation. He asked the dentist what procedure he was about to undergo, and the dentist impatiently mumbled, "tethering." Alda had no idea what tethering was, but rather than start interrogating the man holding the scalpel, he shut up and allowed him to proceed.

If only Alda had continued the conversation. The dentist's straightforward procedure was actually pretty experimental, and it left him unable to smile properly. Needless to say, that's not great news for an actor!

Alda's experience at the dentist shows just how important good communication is. We need to become great communicators if we want to understand what others want from us, as well as letting them know what we need. Alda explains how you can take advantage of the tools he developed in his career as an improviser, actor and interviewer. With these skills, you can become a master of communication.

In these blinks, you will discover

  * what playing an invisible instrument can do for you;

  * how showing too much empathy can sometimes damage communication; and

  * how to use the "Yes, and..." technique to improve your exchanges with others.

### 2. To ensure great communication, use your ignorance to make others feel comfortable. 

Have you ever been stuck in a one-way conversation where the other person keeps rattling on, ignoring the fact that you just don't get what on earth he's talking about?

It's not a great feeling, but you can avoid it by using ignorance. Along with curiosity, ignorance is the best tool for getting the most from your communication partner. Interviewers on television use this technique all the time on scientific guests.

The author used this exact technique when he interviewed a scientist specializing in AI, a topic about which he knew nothing. Rather than asking questions requiring technical, niche answers, the author asked whether specialists ever worried that robots might turn against and overthrow the human race — a broad and pretty naive question. Such a broad question elicited an entertaining answer that the whole audience, not just those with specific scientific knowledge, could appreciate.

Another key thing to remember is that good communication is, by definition, a shared experience.

When you communicate with someone, always be sure to make your partner feel comfortable. A great technique here is improvisation, which helps build both a connection and fellow feeling.

On one occasion, the author led an experimental improvisation session at the University of Southern California. The idea was to help students feel more at ease communicating with one another when they gave presentations.

Before they spoke, they were assigned certain activities. For instance, one student had to pretend to play an imaginary instrument, and one by one the remaining students joined in, each playing their own "instrument," all following the rhythm of the first student.

This joint activity helped the students bond and relax. And, in the end, it made for far better presentations.

### 3. Good communication is both empathetic and rational. 

If we want to communicate well, then we have to take advantage of the natural resonance that communication has with our emotions.

We know about this resonance from scientific research. Scientists have observed that, when a monkey watches another monkey grasping an object, something phenomenal happens: the observer monkey's brain reacts as if it were _performing_, not observing, the action.

The neurons that are stimulated when we watch someone perform an action are known as _mirror neurons_, and they're linked to our feelings of empathy. For instance, if we register the facial expression of someone who's happy, our own happiness neurons are stimulated. We feel how they feel.

Empathy can therefore be seen as an essential component to communication since it allows individuals to really understand what others are experiencing.

There's another aspect to this empathetic side, however. It might seem odd, but it's all too easy to forget that another person's thoughts are distinct from our own. To combat this, we have to activate our rational brain.

Imagine a hypothetical study conducted on children younger than five. In it, some children observe a woman entering a room, placing a cookie on a table and then leaving. Immediately after this, a man enters the same room and moves the cookie to a cupboard. Now, if the woman re-entered the room, the children would assume she knew the cookie's new location, just as they do. But of course, she would have no idea where it is.

Although we adults no longer operate exactly as these children would, we still have a tendency to ignore what others may be thinking. If we want to overcome this, we have to engage the rational gear. In conversation, we should stay attuned to body language, facial expressions and tones of voice. If this is done successfully, we'll come closer to finding out what our interlocutors are actually thinking and feeling.

### 4. To communicate well, adjust to your audience and ensure your message is clear and engaging. 

If you really want to communicate a point and get a message across, a great strategy is to be clear and captivating. The trick is to be memorable, and there's nothing more memorable than a story.

Consider how David Muller spread the news of his creation of the thinnest glass ever made. To publicize his discovery, instead of a dull and technical spiel, he told a story. He relayed how he and a graduate student stumbled upon their invention by mistake. They had been working with graphene when they noticed some "muck" in the apparatus. They thought that an air leak might've been responsible, but, upon closer inspection, they realized that this muck was actually incredibly thin glass.

It was the engaging anecdote that ensured the media picked up Muller's creation and spread the news far and wide.

Another approach for effective communication is to use the "Yes, and…" method. This technique is beloved in the comedy improv scene. The shtick involves accepting what one person provides you with, either verbally or physically, adding to it and seeing if you can morph it into something else.

Larry, a colleague of the author, actually put the game to unexpected use when a thief pulled a gun on him. Instead of getting angry, he stayed calm, looked coolly at the young man and tried to connect by offering to help. He was trying to acknowledge the young man's situation in life and work through it from there. Larry took what he was given and went with it. And, in the end, the thief laid down his gun and the next day Larry found the young man a job.

This story goes to show that real communication can occur in mysterious ways. Be attuned to those moments when genuine human connection can overcome differences.

> _"This process of allowing something you receive from another person to transform into something else is one of the most interesting experiences in improvising."_

### 5. Improvisation techniques can assist in improving your abilities in reading nonverbal communication. 

When it comes to communication, we often over-complicate things. We try to squeeze in too much information or be too clever with our words.

It's best to go back to basics. Words aren't everything. So much of communication is nonverbal; body language, tone of voice and facial expression can all be put to good use in communication as they all operate on deep, intuitive levels.

Just a few improv techniques can help transmit the right nonverbal signs. For instance, the author asked a couple of scientists at Stony Brook University to participate in an improv game. They stood, facing one another, and, as one of them made movements, the other did his best to mirror those movements. It was critical that the leader understand that it was his responsibility to move slowly enough for his partner to follow. After a few minutes, they switched roles.

In the next stage, neither was given the position of leader, but they still had to move in sync. This required extreme attention to body language.

When each had come to grips with the other's body language, as well as their own, they moved on to speaking. Again, attempting to mirror and react to what their partner did and said.

This sort of task is perfect for learning how to read basic nonverbal clues in communication. A game like charades also works well. Another exercise could involve trying to guess the relationship between two individuals by judging body language and tone of voice.

These techniques are also good for teaching empathetic qualities and for getting better at reading feelings. The author likes to practice this by observing the behavior of others and guessing their precise emotions.

Try it sometime. Take a moment at work, or on the subway, and postulate how the people around you might be feeling.

### 6. The last two elements of great communication involve greater audience awareness. 

By now you should have the basics of communication down. But there are two final approaches that need to be mastered. First, you've got to remember that different audiences require different styles of communication — that is, sometimes an audience needs lots of empathy and sometimes empathy should be kept in check.

The work of Helen Riess, Associate Professor of Psychiatry at Harvard Medical School, has shown just this.

In one experiment, she and a series of her patients were filmed while hooked up to skin-conductance monitors, which recorded their tension levels.

One particular therapy session perplexed Riess. It was with a seemingly confident young female patient, who was having issues losing weight. The data feedback showed the patient suffering from serious anxiety during the exchange, but Riess's feedback had been reasonably stable. Clearly, she hadn't been picking up on her patient's true emotions.

Riess went back to the tapes. She began to notice the little tics and gestures the patient made during her anxiety spikes. Riess, in short, was learning empathy so she could make use of anxious moments in subsequent therapy sessions. It was a success and the patient went on to lose 50 pounds.

However, there are other times when displaying empathy is not the best idea. On one occasion a doctor was examining a friend of the author who'd been having foot pains. Overwhelmed by the cumulative symptoms, the doctor sank down with his head in his hands, as though he himself were afflicted. Unfortunately, this show of empathy only scared the patient unnecessarily.

The second approach is remembering that communicating well usually means avoiding complicated and confusing details.

Just imagine a security specialist only using technical language to explain to a company how its internal network could be compromised. The warning might go unheeded, leaving the system at risk.

A good rule of thumb is to avoid jargon. But remember: what one person doesn't understand will be crystal clear to another. So stay flexible — and improvise!

### 7. Final summary 

The key message in this book:

**Based on his own experience, Alan Alda wants above all to convey the universal importance of good communication. Using his background in acting, Alda has been able to use improvisational techniques to teach people to refine and develop what it means to communicate effectively.**

Actionable advice:

**Here's looking at you**

Eye contact has been shown to raise empathy levels. If you're addressing a crowd, you can even get a natural oxytocin hit by choosing one person to make direct eye with when you finish a sentence.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Yes, And_** **by Kelly Leonard & Tom Yorton**

_Yes_, _And_ (2015) shows you how by incorporating the techniques of improvisational comedy to the business world, you can generate better ideas and foster more effective communication, with the ultimate goal of building a team ready to meet any challenge. The authors draw on personal experience from working with leading talents such as Tina Fey, Stephen Colbert and Amy Poehler.
---

### Alan Alda

Alan Alda is an award-winning actor who has won seven Emmy Awards. He is probably most famous for playing Hawkeye Pierce, on _M*A*S*H_. After hosting PBS's _Scientific American Frontiers_, Alda was inspired to help scientists find better ways to communicate.

