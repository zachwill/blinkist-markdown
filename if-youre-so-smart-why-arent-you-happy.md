---
id: 5937bcbbb238e100051fdb66
slug: if-youre-so-smart-why-arent-you-happy-en
published_date: 2017-06-07T00:00:00.000+00:00
author: Raj Raghunathan
title: If You're So Smart, Why Aren't you Happy?
subtitle: How to Turn Career Success into Life Success
main_color: 2DB6E0
text_color: 1E7894
---

# If You're So Smart, Why Aren't you Happy?

_How to Turn Career Success into Life Success_

**Raj Raghunathan**

_If You're So Smart, Why Aren't You Happy_ (2016) offers wisdom to help you put an end to the bad habits that are keeping you from being happy. We often think that happiness is an elusive emotion, but that's just because we often get in our own way. Believe it or not, there are simple steps you can take starting today to become a happier and healthier person.

---
### 1. What’s in it for me? Find your way to happiness in life and love. 

Many people today have given up on finding love, fulfillment and happiness. And yet, people still continue to watch romantic movies, featuring passionate love and someone finding real joy in their professional pursuits. So, deep down, most people probably are still longing for such things.

But when one starts thinking seriously about achieving happiness, the mind often focuses on the negative: there are dirty dishes in the sink, you never learned how to play the guitar and, sure, you may have a nice partner, but that person isn't exactly a passionate lover. It would seem that something will always be missing. So how can happiness be discovered when the human mind tends to focus on what's lacking?

In these blinks, you'll learn

  * why you should value happiness more;

  * how to connect easily with other people; and

  * why you do have control over your happiness, just not in the way you think.

### 2. Don’t devalue happiness; instead, focus on it by identifying where it comes from. 

You're a smart person, right? So why do you always end up falling into the same depressing ruts?

It might have something to do with the _seven deadly sins_. Now, we're not talking about biblical sins, but rather the bad habits that get in the way of happiness.

The first sin is the human habit of _devaluing happiness_.

Despite our desire to be happy, we tend to ignore happiness as a goal and not give it the priority it deserves.

The author, along with fellow professors Sunaina Chugani from City University of New York and Ashesh Mukherjee from McGill University, conducted a study to determine what things people associate with happiness.

Participants were asked to make three wishes for things that they believed would lead to happiness. The researchers found that generally, people wish for things like money, fame and success because they know what they're getting. Meanwhile, they tended to ignore happiness itself because it seems too abstract a concept to wish for.

But if you think about it, shouldn't happiness be everyone's top priority?

Of course different things make different people happy — so before you can prioritize happiness, you need to define it.

Ask yourself what emotion you associate with happiness. This way you can learn to better understand where it comes from. Perhaps you associate happiness with love; if this is the case, your relationship with your partner is probably central to your well-being.

With this in mind, try creating a journal to record all the different things that bring happiness into your daily life. Recall all the memorable moments of happiness and write down what you were doing, who you were with and what led to those positive feelings.

Maybe you were traveling with someone you love, and seeing the world, or maybe you were spending time with your family during the holidays.

Once you have these events written down, you'll have a clear picture of the kind of things you should be focusing on and making time for in your life, thereby letting in more happiness.

> _"The things that lead to happiness and fulfillment are the things that make us better — more kind and compassionate."_

### 3. Don’t measure yourself against others; instead, find your own flow. 

What would you rather have, a brand new BMW or a new friend? This kind of question gets to the heart of the second deadly sin: _chasing superiority_.

One thing that many people learn in life is that feeling superior to others does not produce happiness. 

Yet people continue striving to achieve a feeling of superiority, even making it a goal, and end up unhappily pursuing it for the rest of their life. 

In order to feel superior, you must measure yourself against others. If you can do something better than someone else, you're doing well; if not, you need to improve. The problem with this approach is that it implies that your failure to perform better than someone else makes _you_ a failure.

Sure, it's natural to feel good about a job well done, but that doesn't have to lead to the pursuit of a sense of superiority.

Studies show that the more we compare ourselves to others, the less happy we are because it sets us up to obsess over every possible failure and how we look in the eyes of others.

The better path to happiness is to pursue your own _flow_. 

The concept of flow originated with the psychologist Mihaly Csikszentmihalyi, who describes it as a blissful state of total immersion in an activity. It could be anything from sports to woodworking or solving a complex puzzle — anything that makes you zone out and lose track of time.

In this state of flow, you can experience perfect focus and pure enjoyment of the moment, which also sets up the ideal conditions to learn a skill.

No matter who you are, finding your flow is deeply satisfying and enjoyable.

So when you're thinking about what to do with your life, don't chase a sense of superiority. Rather, find an activity that you love to immerse yourself in and can bring you into the flow.

### 4. Forge strong connections with others by being generous and avoiding desperation. 

One of our strongest desires is to connect with others, but one should be careful how one goes about forging human connections.

This leads us to the third deadly sin: _desperately seeking love_.

An unhealthy desire to connect manifests itself as a desperation for intimacy that includes neediness and avoidance of intimacy altogether, which can often be traced back to the earliest months of our lives.

In a series of studies started by psychologist Mary Ainsworth in the 1970s and 1980s, newborn babies who are deprived of parental affection were found to become insecure and lacking in self-esteem later on in life. And as the subjects grew into adulthood, they were found to be especially prone to both neediness and avoidance of intimacy.

Naturally, this kind of behavior only increases the chances of them being lonely because neediness is not an attractive feature. People tend to be uninterested in things that are easily available, which is why people who are the most desperate for connection often end up alone.

But making human connections definitely makes us happier.

One massive study followed a selection of men from when they entered college in 1938 all the way into the late 2000s. Researchers discovered that the happiest men were those who had the strongest relationships.

And the best way to cultivate meaningful connections is by having an altruistic spirit.

When we help others and make them feel happy, we gain a sense of accomplishment and learn that we can spread happiness. This improves our self-image and makes us more capable of connecting with others, as well as making us more attractive.

And because generosity raises our own happiness levels, it comes very naturally to us. In 2012, researchers handed out treats to 20 toddlers, asking them to share the treats with a puppet. They found that while the toddlers were happy to receive treats, they were even happier after they shared them with the puppet.

### 5. In order to be happy, we must learn to let go of our desire for control. 

The fourth deadly sin is _wanting too much control_.

A lot of people feel most comfortable when they have total control over their environment and everyone in it. But the truth is, this is always going to lead to unhappiness and increased tension because we can never truly be in complete control.

And we shouldn't want to be. Think about it — you probably don't like to feel controlled, so it's only logical that others don't like to feel controlled by you either.

Let's say you try to control your partner's diet by pushing him toward healthier food. There's a good chance he'll start eating more junk food just to show you that he's in control of what he eats. Plus, he'll likely resent you for being critical of his dietary habits.

Feeling the need for control can also lead to your finding more unhappiness in everyday situations.

In a 2008 study by psychologist Robert Vallerand, participants with a high need for control of external events were placed in an overcrowded and uncomfortable room. Predictably enough, these participants were more distressed and unhappy than the other participants.

To give yourself a better chance for happiness, then, focus on finding _internal control_ over your thoughts and feelings.

This will help you both take responsibility for your own happiness and stop blaming outside circumstances for how you feel.

So rather than falling into a depression when your significant other has to leave town after you've spent a week together, try to stay happy and remain focused and appreciative of the moments you were able to share.

By taking responsibility and exerting internal control, you can also choose not to expose yourself to situations or people that you know are bad for you. So if there's a particular person who always puts you in a bad mood, you can try to avoid crossing paths with that person.

### 6. Learning to trust and forgive is key to experiencing happiness. 

Have you ever received a perfectly nice present from someone, only to have it tainted by your suspicion that it was given with an ulterior motive?

This scenario is related to the fifth deadly sin: _constantly distrusting other people_.

Sadly, we all have a natural tendency to worry that we could be betrayed by another person at any moment. We've evolved to be distrustful because for our early ancestors, distrusting others improved chances of survival. A certain wariness made it more likely that one would live another day and procreate.

Distrust is also an effort to protect ourselves from the painful feeling of having our heart broken.

Relationship expert John Gottman found that to overcome one incident of untrustworthy behavior, we need the person who broke our trust to be trustworthy five times before they're fully forgiven. 

But if we want to live a happier life, it's best to learn how to be understanding and forgiving.

If you've been betrayed, don't wallow in unhappiness; instead, minimize the pain and try to understand the other person's side of things.

If you hate someone for what he's done, try putting yourself in his shoes and see if you can't come to understand his behavior and motivations, even just a little bit. Chances are, you probably can. Maybe his actions have something to do with his genetics or upbringing, or even his own fears.

Understanding is the key to forgiveness, and forgiveness is one of the most powerful ingredients for happiness.

You'll find that it's much easier to forgive once you understand the personal benefits of forgiving. Forgiveness doesn't mean that you need to keep the person you forgive close to you; all it means is that you let go of the hate and anger, and know that you can move on with your life.

Once you learn to forgive, you'll find that trust will come more easily.

> "_Learning how to forgive, as an impressively large number of findings have shown, is one of the most powerful ways of boosting happiness."_

### 7. Happiness requires flexibility and patience in the pursuit of goals. 

The sixth deadly sin is being either too passionate or too indifferent about the events in your life.

Here we find two different pursuits that can lead to unhappiness. One is _the obsessive pursuit of passion_ and the other _the indifferent pursuit of passion_.

Being too passionate is like being too stubborn or inflexible about the goals you hope to reach.

With this perspective, the only good, or happy, things in life are whatever brings you closer to your goal. That means everything else is bad, and thus you'll be unwilling to adapt to and unhappy about the many changes and realities life has in store.

Let's say your goal is to marry someone and have two children. If it doesn't happen, you'll surely be unhappy, but if it does happen, you might discover that you're still unhappy because some aspect of your plan didn't turn out as perfectly as it should have. Either way, you'll be the one standing in the way of your own happiness.

Conversely, someone who's indifferent to the pursuit of passion is a person who doesn't care at all about what happens. Such people aren't interested in or curious about their own life and might even marry someone they don't like just for the hell of it.

Neither of these perspectives is good — so it's best to find the healthy middle ground: _the dispassionate pursuit of passion_.

With this perspective, you have a preference of how you'd _like_ things to happen, but you're flexible if it doesn't turn out that way. In fact, you can be accepting and understand that negative experiences can actually become a positive.

All this requires is some patience and the realization that events we originally thought were nothing but horrible can transform into the most meaningful moments in our lives. Even if you get fired, it could very well end up putting you on a new career path that's twice as rewarding as before.

We really do learn the most from the biggest challenges we face.

### 8. Overthinking distracts us from our intuition, but mindfulness can help. 

We now come to the last, but not the least, of the deadly sins: mind _addiction_ — that is, the very natural tendency to overthink — which can distract us from our intuition.

When we make rash or poor decisions, we often blame our instincts, yet instinctual behavior is far from random. Our intuition is connected to our evolutionary past and can pick up on details we might not be consciously aware of.

In a Harvard University study, students were shown three-second video clips of various professors without any sound, and they were asked to provide an assessment of the teacher's personality. Sure enough, by following their intuition, students were able to use this modicum of information to accurately predict what would appear in their end-of-semester teaching evaluations.

Indeed, our instincts are quite finely attuned, and yet we spend a lot of time being distracted from what they are trying to tell us. This is why practicing mindfulness is a great way to reconnect with ourselves.

To start with, try focusing on your breathing. Be patient and attentive, and allow yourself to notice your thoughts while at the same time letting them pass without engaging them.

This will help you to avoid getting caught in the _GATE web_, which stands for Goals, Action, Thoughts and Emotions and illustrate how they beget one another.

Let's say you're in your boss's office getting a verbal warning for doing something wrong. This might lead to negative thoughts about your boss, which then provokes negative emotions. Finally, you take the action of yelling back at her with the goal of making her feel bad too.

But wait, step back from the web and instead take a deep breath and focus on your inhalations and exhalations. There is a way to leave these situations with a healthy emotional state.

It might sound too simple, but focusing on your breathing is a remarkably effective way to be calmer and more mindful.

Smart people often overthink themselves into unhappiness. So try to use your smarts to avoid the seven deadly sins of unhappiness.

### 9. Final summary 

The key message in this book:

**We can find happiness when we focus on the right things. Put yourself on the right track by finding flow in your everyday life and by making healthy, sincere and loving connections with the people around you. Happiness is not something you have to chase; it comes from within. So remember to breathe, connect with your own intuition and give your mind a rest. Happiness isn't as unattainable as you might think.**

Actionable advice:

**Find your answer in your gut**

The next time you find yourself overthinking a situation, take a moment to find a comfortable position where you can sit for a while. Now start imagining your thoughts as clouds, and watch them drift by without getting in their way. While this is going on, slowly bring your attention to your breath and the air going in and out of your lungs. You will notice that your mind gets quieter, and the answer to your question may emerge from your gut instead of from your mind.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Grit_** **by Angela Duckworth**

_Grit_ (2016) is about the elusive virtue that allows people to do what they love, find a purpose in life and, most importantly, stick with it long enough for them to truly flourish. Find out how you can discover your grit and use it to follow your calling in life — and to hang in there, even when the going gets tough.
---

### Raj Raghunathan

Raj Raghunathan is a professor of marketing at McCombs School of Business at the University of Texas, Austin. He is also an associate editor at the _Journal of Consumer Psychology_.

