---
id: 540de8a866613400083c0000
slug: ignorance-en
published_date: 2014-09-09T00:00:00.000+00:00
author: Stuart Firestein
title: Ignorance
subtitle: How It Drives Science
main_color: 5CE5E5
text_color: 307878
---

# Ignorance

_How It Drives Science_

**Stuart Firestein**

_Ignorance_ investigates the strengths and weaknesses of the scientific method and reveals the importance of asking the right questions over the discovery of simple facts. Using real-life examples from history, _Ignorance_ shows that it is our awareness of what we _don't_ know that drives scientific discovery.

---
### 1. What’s in it for me? Learn the importance of asking the right questions. 

In a world where the answer to any question is just a mouse-click away, it is now more important than ever to know what we _don't_ know.

If we can identify gaps in our knowledge — our _ignorance_ — then we can gain a better understanding of what remains undiscovered. And this can only happen by asking the right questions.

In these blinks, you will learn how scientific discovery works, and how it is driven by our ignorance. Not only will you gain insight into the workings of scientific inquiry, but also you will learn about the limits of the scientific method and why it's important to understand those limits.

By providing concrete examples from the near and distant past, this book shows you exactly how some of the world's greatest scientists were able to use gaps in our collective knowledge to make unique and elegant theories and discoveries that have helped to shape our world for the better.

Finally, you'll learn what scientists can do to help the public embrace their own ignorance and use it as a tool for discovery.

In these blinks, you'll also discover

  * why some grouchy dolphins put one scientist on "time-out";

  * why scientists get paid for their lack of knowledge; and

  * why you can't always trust a counting horse.

### 2. Although we think of it as objective truth, science is always a product of imperfect humanity. 

What exactly is a "scientific fact?"

You might think a scientific fact represents an absolute truth, that is, the _correct_ explanation for a certain phenomenon. However, this isn't true: scientific facts are the products of imperfect human beings, and therefore fall short of being completely objective.

While scientists attempt to design experiments to be as unbiased as possible, a scientist — as a human — is never unbiased. Consider a researcher who has spent years working on a particular hypothesis; she is of course eager to prove it correct. This desire to be right could easily affect how she draws her conclusions from her studies.

Many scientists as well follow the tradition of _positivism_, the idea that everything can be explained by empirical observation and through logic. This view of the world is _mechanistic_, that is, it assumes that everything has strong causal relationships. For example: if you don't sleep (cause), you'll be tired and unproductive (effect).

However, cause and effect are not always discernable, as one thing doesn't necessarily lead to another. Is a sleepy person _always_ unproductive? No. Some may be, and others not. From this relationship, we couldn't draw a concrete rule, as it would be incorrect.

What is key here is that we can't always know what we _don't_ know. While scientists may try to discover and describe everything that exists, they're limited by their own faculties in doing so.

Consider the human eye. While it is a sophisticated organ, there are still things it cannot do. For example, it can't detect ultraviolet light. As a result, we simply had no idea that ultraviolet light even existed until very recently.

Now consider the human mind. Applying this same example, we have to stop and think how we could possibly expect to understand everything in our world when some things may simply be incomprehensible, considering the limitations of our own minds.

Because of these limitations, a scientific discovery should never be considered as an end in itself.

### 3. Scientists’ desire to make predictions of the future can sometimes lead to faulty forecasts. 

Making predictions is an important part of a scientist's work. While scientists can't see the future, there are certain types of predictions that work reasonably well when using the scientific method.

For example, chemists run experiments to observe how different elements will behave under certain conditions. With enough observations, they are able to then develop a general rule about the types of reactions that resulted from their experiments. Using this rule, scientists can then accurately predict the outcomes of the same or similar experiments.

Scientific study is full of such general rules. In 1995, for example, scientists analyzed the DNA of red-haired people and found that red-haired people carried specific DNA sequences that only rarely were found with non-red-haired people.

Thus, if you can identify these specific sequences in your DNA, you can reasonably hypothesize that you'll have red hair.

Although such a process is useful in predicting some behaviors, scientists struggle when applying this method to other problems. This is especially true when making predictions about the future.

Say someone predicts that "10 years from now, our brains will have tiny computer implants, which we will use to access the internet."

These types of future predictions are almost inevitably wrong, as scientists simply lack the knowledge or the evidence to make such predictions in the first place.

Consequently, when trying to make predictions about the future, scientists should adopt a more nuanced approach. One way to do this would be to make predictions in the form of a question rather than a statement.

In 1900, Daniel Hilbert offered some predictions about the future of math over the next century. Instead of making sweeping statements, however, he decided to formulate mathematical questions that were at that time still unresolved.

This way, rather than becoming immediately irrelevant, his observations about the state of science stayed relevant throughout the following century and are even relevant today.

> _"It is difficult to make predictions, especially about the future."_ — Niels Bohr

### 4. Scientific discoveries arise when we ask questions to overcome our ignorance. 

Many people believe that _knowledge_ is the starting point for scientific inquiry. However, scientific research actually starts with _ignorance_, or the absence of knowledge.

There are two different types of ignorance.

The first is willful stupidity. This is when you deliberately ignore facts or logic, often to maintain your beliefs about the world.

The second is the absence of facts or insight. This kind of ignorance has both to do with an individual's lack of knowledge and a _communal_ gap in knowledge. This, unlike willful stupidity, is a valuable kind of ignorance, and in fact spurs scientific research.

Scientific progress happens when we question the things about which we are ignorant. This is incredibly important, as one question can give rise to many answers and inspire valuable research.

Leadership is one subject in which researchers have made many interesting discoveries. They've learned that there are many different types of leadership, such as "humble" leadership or "authentic" leadership. Yet with each new revelation, we also reveal new ignorance about the subject.

One answered question inspires new questions. For example, what are the effects of a certain leadership style on employee creativity or motivation? Or what causes someone to demonstrate a particular leadership style?

Other fields of research, such as neurobiology, might also benefit from such research. Neurobiologists could explore how the brains of different people vary depending on their leadership style.

It's clear that scientists can only benefit by embracing their ignorance wholeheartedly. For science to progress, we need scientists to tackle the big questions, not get lost in narrow, biased results.

Physicist Enrico Fermi always told his students that proving a hypothesis with an experiment should be regarded as measurement. By contrast, _not_ proving a hypothesis with an experiment is a discovery!

Thus when you prove yourself wrong, you reveal your ignorance; and have a whole new world of questions to ask and pursue.

> _"Knowledge is a big subject. Ignorance is bigger."_

### 5. Specific, smaller questions help to answer the big research questions, step by step. 

When running a race, you can't reach the finish line in one stride, but progress one step at a time. This is especially true in science.

For scientists, one of the first steps on the way to new discoveries is mundane: writing a grant proposal. These proposals take a lot of time, but they are critical in securing funding for research.

One of the most important aspects of the proposal is the research question. A scientist interested in the effects of global warming in Uganda might write, "How does global warming affect agricultural yields in Uganda?"

Scientists also need to outline exactly how they intend to collect and measure their data.

Although proposals are time-consuming, they are useful in two ways: they force researchers to think about their areas of ignorance as well as help narrow down their focus to a specific question.

In other words, they have to think small. Yet answers to the small questions help solve the big ones.

For example, as astronomer Carl Sagan published hundreds of scientific papers on the chemical makeup of atmospheres of various planets (small questions), his discoveries in aggregate helped him to think broadly on the question of life's origin (a big question).

This strategy of using smaller questions to answer larger ones is one of the foundations of modern science, known as the _model_ _system_.

One of the biggest questions in science today is understanding how the brain works. Yet the human brain is a very complicated organ, with 80 billion neurons forming 100 trillion synaptic connections. So instead of diving right into the human brain, researchers often use rats or mice, as their brains have _fewer_ neurons, thus making research questions smaller and more manageable.

Now that you understand how ignorance spurs scientific development, the following blinks will examine in depth some of the many scientific breakthroughs that have been made in this way.

> **Fact:  

** Unlike most of us who are paid for what we know, scientists are paid to think about what they don't know.

### 6. Exploring ignorance led scientists toward a “glimpse” into how animals think. 

Clever Hans was once thought to be the smartest horse in the world, as he could count. When his owner asked a simple arithmetic question, such as "What's four plus five," Hans would use his hoof to stomp out the answer.

Actually Clever Hans wasn't _that_ clever. Psychologist Oskar Pfungst eventually demonstrated that, when Hans was "counting," he was merely following cues from his owner's body language.

The case of the counting horse however was still influential, as it highlighted an area where scientists were quite ignorant: whether animals can think.

To answer this question, scientists carefully observed how animals behave. They were looking for a _glimpse_ — a moment that reveals that an animal is using its mind for cognition. Surprisingly, they found that animals displayed glimpses often.

For example, when researcher Diana Reiss trained her dolphins, she would always feed them the head and body of a fish, but not the tail as they didn't like it. When the dolphins wouldn't do what Reiss expected of them, she would give them a "time-out" by walking 10 meters from the pool and letting her distance show her disapproval.

One day, she accidentally fed a dolphin a fish tail, which the dolphin didn't like. So the dolphin swam to the middle of the pool, essentially giving Reiss a "time-out."_That_ is a glimpse.

Another way to test for basic cognition in animals is the mirror test. In his pioneering work from the 1970s, Gordon Gallup Jr. used this test to demonstrate that chimpanzees are self-aware.

Gallup anesthetized the chimps to mark their foreheads with a red dot. Upon awakening, the chimpanzees were given a mirror. They then began investigating this new red dot on their foreheads with the mirror, demonstrating that they had an awareness of their bodies, and thus of themselves.

### 7. Ignorance in physics led to the creation of “string theory.” 

Physics can explain some of the deepest mysteries of our world: from how the universe was created to how the minute particles that make up matter interact with each other.

However, right in the center of this scientific discipline is a huge knowledge gap.

Physicists know a lot about the _quantum_ _world_, the realm of subatomic particles that are responsible for energy, mass and fundamental forces.

They also know a lot about the basic characteristics of our universe, as described by the classical laws of physics and Einstein's theory of relativity.

Unfortunately, however, they have not yet figured out how to bring these two worlds together in a way that makes mathematical sense. Thus, the physics of the small and the large have two different sets of laws.

To fill this knowledge gap, scientists have developed _string_ _theory_.

Like many physicists, theoretical physicist Brian Greene was not satisfied with having two different theories to describe the laws of the universe. So he decided to look beyond classical and quantum physics and try and create a totally new theory to tackle this area of ignorance.

His contemplations led to the development of a theory based on filaments of energy, called _strings_. These strings vibrate, and in doing so, produce the subatomic particles, like quarks, bosons and leptons, that create matter and energy.

Moreover, because these strings travel through space and time, they can therefore be explained by Einstein's theory of relativity, bridging the gap between quantum and classical physics.

String theory however still remains largely theoretical. As research progresses, however, it is possible that string theory could be the unifying theory that will explain the behavior of everything in the cosmos.

### 8. A knowledge gap in neuroscience led to new insights into how memory works. 

Our brains are remarkably good at remembering things. But just how good?

Consider this study, in which participants were shown 10,000 images in rapid succession. Afterwards, they were shown a second set of images, some of which were included in the first 10,000 images.

The participants then had to say which of the images in the second group were also included in the first. They did so, with an accuracy rate of a surprising 90 percent!

Neuroscientists Larry Abbot and Stefano Fusi wondered just how it is that the brain can do this. They knew that specific memories are represented by networks of synapses in the brain, but discovered that our minds simply don't have enough synapses to remember all of our memories!

Yet we still are able to retain huge amounts of information, even though to do so, it would seem we'd need far more synapses. So what exactly was going on?

This gap in their knowledge — their ignorance — served as the starting point for their research.

By following their ignorance, they were able to discover that our brains have to _forget_ old things to _learn_ new things.

While we are quick to learn, we are also quick to forget. This process is important: we have new experiences every day, and some of these need to be cataloged as important memories. These new memories overwrite existing ones by using the same synapses, thus making it difficult (or even impossible) to access overwritten memories.

By identifying a knowledge gap and trying to fill it, Larry Abbot and Stefano Fusi gave us new and valuable insights into the workings of our brains.

We've seen just how important ignorance is. Yet ignorance is still often seen as a curse, rather than a gift. The final blink will offer advice on how to change that.

### 9. Scientists should encourage teachers and students to embrace ignorance and ask questions. 

As we've seen, ignorance can be a valuable asset. This understanding shouldn't be kept secret! Scientists need to talk about the research process and thus about the value of ignorance.

Indeed, making scientific projects more visible to the public could have far-reaching positive consequences.

For example, Galileo made waves when he published his _Dialogue_ _Concerning_ _the_ _Two_ _Chief_ _World_ _Systems_ in 1632. He chose to publish in Italian, making his work the first ever scientific manuscript to be published in a language other than Latin or Greek, thus making it accessible to a wider audience.

Other scientists followed Galileo's example: philosopher René Descartes published in French, while mathematician Gottfried Wilhelm von Leibniz published in German.

However, today's scientific papers often resemble what Latin papers were back then: written in mostly scientific language, the contents are inaccessible for a majority of people.

Scientists can break down this wall by talking publicly about their ignorance and unanswered questions. Questions are often far more easily understood than answers, and this approach could help the public to have a better idea of what's happening in science today.

Even more important than creating a public awareness of ignorance, however, is _teaching_ ignorance to future scientists.

The failures of our education system can be easily gleaned in the one question asked by nearly every college student: "Will that be on the exam?" While these kinds of questions lead to higher test scores, they don't lead to greater scientific inquiry and discovery.

Teaching students how to ask questions is more important today, as technological developments give us more access to ever more information. The internet has made it possible to find nearly any random fact we could possibly desire with a single click, and information will only become more accessible as technology improves.

It will soon be inadequate to teach facts, as facts will be easily acquired. Education must center around something else: asking the right questions.

> _"Ignorance is the beginning of a scientific process — and its result."_

### 10. Final summary 

The key message in this book:

**Scientific** **inquiry** **is** **a** **lot** **more** **about** **asking** **the** **right** **questions** **than** **it** **is** **about** **discovering** **facts.** **However,** **to** **ask** **those** **crucial** **questions,** **you'll** **have** **to** **be** **able** **to** **assess** **what** **it** **is** **that** **you** **don't** **know,** **and** **that** **means** **embracing** **your** **ignorance.**

**Suggested** **further** **reading:** **_In_** **_Pursuit_** **_of_** **_the_** **_Unknown_** **by** **Ian** **Stewart**

In this book, Ian Stewart focuses on 17 famous equations in mathematics and physics history, highlighting their impact on society. Stewart gives a brief history of the wonders of scientific discovery, and peppers it with vivid examples and anecdotes.
---

### Stuart Firestein

Stuart Firestein is the head of the Department of Biology at Columbia University, where his laboratory is investigating the mammalian olfactory system. In addition, Firestein was the recipient in 2011 of the Lenfest Distinguished Columbia Faculty Award for excellence in scholarship and teaching.

