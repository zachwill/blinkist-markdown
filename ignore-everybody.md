---
id: 54bd21d1633234000a530000
slug: ignore-everybody-en
published_date: 2015-01-20T00:00:00.000+00:00
author: Hugh MacLeod
title: Ignore Everybody
subtitle: And 39 Other Keys To Creativity
main_color: D92B3C
text_color: BF2635
---

# Ignore Everybody

_And 39 Other Keys To Creativity_

**Hugh MacLeod**

In _Ignore Everybody,_ author Hugh MacLeod encourages you to unlock and embrace your inner creative spirit and live your artistic dream. Pulled from his personal and professional experience, MacLeod reminds us that inspiration can happen at any time, and to be truly successful, you can't let criticism get you down. If you're thinking of giving up your desk job for the artistic life, this book will help you do so while offering sage advice on how to get your work into the wider world.

---
### 1. What’s in it for me? Learn how to harness your innate creativity and take it to the next level. 

Wouldn't it be great if you could pluck inspiration out of thin air? Unfortunately, creativity has a mind of its own, and it's something we can't actively control.

Yet knowing how to make the most of inspiration when it strikes is the key for any successful creative person. Your best work often comes when you let go and express yourself freely; yet to do so, you have to learn to tune out the criticisms of others and tune in to your own muse.

These blinks give you the tools to not only unleash your creative spirit but also take your inspiration a step further, to quitting that day job and truly living a successful artistic life.

In the following blinks, you'll discover

  * how creativity is a natural process that you need to learn to harness;

  * why you should never lose sight of your own creative vision; and

  * how to effectively share what you've created with the world.

### 2. You can’t force inspiration. It comes on like the hiccups: unexpectedly, no matter where you are. 

So you've had a busy week without a moment to spare for your art project. Finally, the weekend arrives and you're ready to let the creative juices flow!

You enter your studio and your mind goes blank; inspiration fail!

So why does this always happen?

It's impossible to flip a switch to get inspired. Creativity is a natural process; we're not in control of it. Inspiration is like getting the hiccups, coming often out of the blue. And just like the hiccups, you'll find that inspiration can disappear when you focus on it too much.

Creative bursts often come when you least expect them. You might suddenly feel inspired in the supermarket checkout line, or when you're just about to drift off to sleep. It's easy to make the most of impromptu inspiration, simply by jotting down the thoughts in your head; these could in the long run end up changing your creative process altogether.

When the author used to paint and draw, he found it hard to seize spontaneous creative moments when he wasn't in his studio with his art supplies. So he started drawing small cartoons on the backs of business cards, which meant he could work wherever he wanted. His ideas started springing to mind when he was on the go, and have only grown in originality and popularity since.

Yet it's important to realize that when you set out to try something new or creative, it's normal to have fears and doubts. You never know when you're going to feel inspired or whether your idea is even good in the first place.

Trust your creative instinct — this is _your_ creative project. Let things flow naturally and remember not to block yourself from creativity!

> _"Inspiration precedes the desire to create, not the other way around."_

### 3. Starting a creative project can be scary, but don’t let distraction keep you from taking the first step. 

It's common to run away from creativity. Diving into a creative project isn't always a party; as an artist, you're venturing into the unknown, and this can be scary.

It's of course natural to question whether your new project will be any good. Are your painting skills up to task? Will you chose this color, or that one? Will your movie be on film, or digital? There are plenty of details over which to obsess when starting out.

What's more, a new project requires coming to terms with the things that make you procrastinate, and removing them to the best of your ability. That said, some artists stall from the very start. If you find yourself doing the spring cleaning in November, or checking the fridge every half hour, you are clearly avoiding your creative muse.

Philosophical questions can also provide procrastinatory material. If you catch yourself debating the "commercial versus artistic" tone of your creative project, or whether you might sell your future short art film to Hollywood one day, it's time to say, "If I'm trying to answer such questions before I even have started filming, I'll never hit the record button."

Don't obsess over equipment, either. If you think you _must have_ expensive tools before making great art, then you are indeed procrastinating. Saving up for a state-of-the-art camera or recording studio can take a lifetime.

Many of history's most successful artists didn't have fancy equipment, either. Author Ernest Hemingway wrote his books with a simple fountain pen; and painter Vincent Van Gogh rarely used more than six colors on his palette.

So don't block your own creative flow. Expect a bit of procrastination, but don't let it cloud your vision. And don't let yourself get distracted by a perceived need for tools or fancy toys.

### 4. When it comes to your own creativity, friends won’t always give you the feedback you want. 

If you pen a poem or paint a landscape, you may receive some praise for your work. Yet if you are able to come up with something truly original, it's possible your friends may be at a loss for words.

Know that the more original your idea, the less advice on it anyone can actually give you.

To offer helpful critique, a person needs to fully understand the concept or idea you're working to get across. Yet if you've come up with a groundbreaking creative project, you just can't expect your friends to offer you useful advice.

Famous ballerina Isadora Duncan rejected the traditional forms of classical ballet to invent her very own, revolutionary style of dancing. Since her technique was totally new, how could she have expected constructive advice from fellow dancers or teachers? None knew exactly what she was doing, as it was happening right then and there, for the first time.

In general, however, you may not get the support you feel you need. While your friends may love you, they may also prevent you from developing or fulfilling your grand plans. Creative projects do have the power to change relationships.

Say you move to a big city to follow your artistic dream while your friends stay back at home with one less player on the bowling team. Each side has their own perspective and needs. In this way, creative projects and plans can put pressure on friendship dynamics.

While we'd all probably appreciate support, even approval, from friends every now and then, in reality, how much do you really rely on friends' advice when it comes to living your dreams?

### 5. Don’t compromise your artistic vision. Your creativity is your own and should flow directly from you. 

Picture yourself as a pupil in grade school art class. You're painting a cat turquoise, your favourite color. But the teacher wants the cat to be black, and won't give you a passing grade otherwise.

Would you change the color of your cat?

Artistic freedom is vital to creating art that truly touches people. You can only be free if you can put your true self into your art. If you had painted your cat black, your painting wouldn't have properly expressed your true emotions!

So find a way to express yourself honestly. Experiment with different techniques to show how you're feeling or what you want to say.

In the late 1940s, artist Jackson Pollock experimented with splattering and dripping paint on canvas. He didn't care what people thought, as he wasn't creating art to make money.

As it turned out, Pollock's self-expression really connected with people and years later, he became one of the most highly regarded artists of his time.

Pollock's story shows that being yourself fills you with artistic passion, which leads to great things.

Yet creating something special doesn't happen without dedication and hard work. Think of all the hours a violinist has to spend practicing the violin before she can perform the perfect solo.

This is what we call _creative drive_.

As humans, the urge to create comes naturally. We're born with a thirst to set and accomplish goals. So long as you're driven by the desire to create, you can follow your passion in life!

When you change your creative vision to please others (like painting that cat black), your desire to create becomes clouded by external forces, such as what's trendy or popular.

Stay true to yourself and your creative instincts, and perhaps one day you'll turn your artistic dream into a dream job.

> _"Part of being a master is learning how to sing in nobody else's voice but your own."_

### 6. Art can pay the bills, but it doesn’t always. Make sure you have support before you set out. 

We've all dreamed of turning our artistic passions from a hobby into a real career. What graphic designer wouldn't love to quit his day job designing cereal boxes to paint full-time in a studio of his own?

Yet as an artist, is such a future really all it's cracked up to be?

First things first: you do need a job to pay the bills. The rent doesn't pay itself! So if you don't have a generous inheritance from a great uncle, you're going to need to make a living somehow.

This is either going to come from your lucrative art job or another job that allows you to earn the money you need. And be honest: can you realistically start paying your rent and bills with just art?

Relying on your art to make a living can jeopardise artistic freedom. A day job doesn't, however; you earn cash to pay the bills, and in your free time, you can follow your inner artistic muse.

However, if you rely solely on your art for income, you may find yourself creating art purely so someone buys it. You may stop expressing controversial topics, or experimenting with style.

Remember: art you make for others always means less than art you make for yourself.

You also have to consider that if your art becomes your job, you have the potential to lose it as a hobby. People look forward to their hobbies when all their real-life work is done, as a hobby is there for enjoyment and pleasure.

But once a hobby becomes a job, it's prone to becoming that obligation you avoid so you can enjoy your free time. And with that attitude, you'll never create your masterpiece!

### 7. The internet is your agent. Post your stories, comics or videos online and find your audience. 

It's the Hollywood cliché: a famous director walks into a café, sees an aspiring actor and offers him the lead in his next film.

Unfortunately, "making it big" isn't ever quite this simple.

Even if you're extremely talented, your chances of being discovered just like that are slim to none. It's a competitive jungle out there, and everyone is waiting for their "big break."

For every successful publisher, art curator, director, photographer and designer there are thousands of up-and-coming wannabe stars shadowing their every move, hoping to be discovered.

Besides, being discovered doesn't exactly mean instant success. Publishers often sign "one-book" contracts with promising authors, only to drop whoever doesn't make the bestsellers list.

Yet you don't have to sit around waiting for someone to discover you. Do it yourself via the internet!

Before the internet age, an artist needed a middleman to publish a book or exhibit a painting. Now you can post your story or video on a personal blog completely independently.

In a sense, you allow your art to find its audience on its own.

The author gained a following through blogging, posting his cartoons online as early as 2001. Some ten years later, his blog had a monthly readership of 1.5 million and he had already sold several bestselling books.

So what could an aspiring actor do to put himself out there? He could post some acting videos online, or use social networking sites to raise money for his own independent film.

The main point is you don't have to waste time, sitting around waiting to be discovered. Be proactive, network and use the internet to get your message out to the world.

### 8. Final summary 

The key message in this book:

**You need to be passionate about art to make art. Follow your creative instincts and seize moments of inspiration. Don't get thrown off-course by procrastinating or other peoples' opinions. Stay true to yourself and your creative vision. Your art will never truly be unique until you know how to express yourself.**

Actionable advice:

**When you've hit a wall, stop** ** _trying_** **too hard to be creative.**

Creativity is a natural process; we can't control it. So if you're staring at a blank piece of paper for hours, how about a change of scenery? Go outside and get some fresh air and start thinking about something else. You'll be struck by inspiration when you least expect it.

**Suggested further reading: The War of Art by Steven Pressfield**

In _The_ _War_ _of_ _Art_, author Steven Pressfield helps you identify your inner creative battles against fear and self-doubt and offers advice on how to win those battles. An inspirational book for anyone who's had trouble realizing their passion, it offers an examination of those negative forces that keep you from realizing your dreams, and shows how you can defeat your fears to achieve your creative goals.
---

### Hugh MacLeod

Hugh MacLeod worked as an advertising copywriter for over a decade, while doodling on the backs of business cards. Now he is a cartoonist, blogger, public speaker, bestselling author and entrepreneur.

