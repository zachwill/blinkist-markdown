---
id: 5a3a4c1fb238e1000765bb6b
slug: ikigai-en
published_date: 2017-12-29T00:00:00.000+00:00
author: Hector Garcia Puigcerver and Francesc Miralles
title: Ikigai
subtitle: The Japanese Secret to a Long and Happy Life
main_color: BBDEE7
text_color: 556469
---

# Ikigai

_The Japanese Secret to a Long and Happy Life_

**Hector Garcia Puigcerver and Francesc Miralles**

Ikigai (2016) is your guide to living a long, happy life through the wisdom of Japanese culture. These blinks delve into every area of Japanese life to uncover their secrets of longevity and to explain why so many Japanese, especially those on one island in particular, live well past 100 years of age.

---
### 1. What’s in it for me? Find out what the Japanese can teach us about living a long and happy life. 

A long life, a sense of purpose, deep happiness — what if all three of these fundamental notions came from the same place?

These blinks outline the Japanese concept of _ikigai_, which encompasses your reason for living, your life goal and your source of longevity. You'll discover how you can live a long, full and happy life by finding your _ikigai_ and following a few simple health tips.

You'll also learn

  * what flow is and why it's so important;

  * how karaoke keeps you young; and

  * why friendships are your most important asset.

### 2. A deep purpose in life is the secret to longevity. 

Are you interested in living a long, healthy and fulfilling life? Who isn't?

The secret to doing so just may be found on the island of Okinawa, in southern Japan, home to the highest concentration of centenarians in the world.

And these island dwellers' secret to longevity may boil down to just one word: _ikigai_, which roughly translates to your reason for living — or your inner motivation for a specific professional activity.

It can also be described as an intersection between four different elements: what you're passionate about, where your skills lie, how you can earn a living and what the world needs. Many Japanese believe that everyone has an ikigai, or destiny, that they were born to fulfill.

However, while some people find their ikigai quickly, others must seek it out over time. If you fall into this latter category, it's important to persist; after all, ikigai will ultimately be what motivates you to get out of bed in the morning.

That's why Okinawans often attain a high degree of specialization and attention to detail in their daily work. For instance, in an Okinawan paintbrush factory, the authors met a skilled craftswoman who had spent her entire life perfecting the art of attaching individual hairs to a brush. At this stage in her career, she was able to do her job with stunning dexterity and skill.

What's more, ikigai is also the key to longevity. So, if your ikigai is your job, you should never retire. And if your ikigai is a hobby that brings you meaning and joy, don't ever give it up.

Okinawans abide by these rules and, as a result, remain active late into their lives. If they're forced into retirement, they still find ways to remain active, such as by doing gardening or other work in their communities.

The benefits of this commitment are clear. Medical studies conducted on Okinawan centenarians have found extremely low rates of both heart disease and dementia.

In the next blink, you'll learn how exactly an engaged mind enables a long life.

### 3. An active mind and low stress are key to a long life. 

Any doctor can tell you that a healthy mind and body are essential to aging with grace; in practice, however, the former is often ignored. Such common neglect of the mind is a shame, as it's just as important to a long life as your physical health.

In fact, in the same way that a sedentary lifestyle adversely impacts your body and emotions, a lack of mental work weakens your neural connections, which is why it's important to exercise your brain in different ways. The neuroscientist Shlomo Breznitz even argues that elderly people lose brain flexibility because they get trapped in patterns and routines, refusing to try out new activities.

So, how can you give your brain a workout?

Well, it can actually be quite simple; any mind game, like chess or cards, will do the trick. But if you want to go further, getting out of the house, meeting other people and experiencing social interactions are about the best exercise your brain can get.

Another secret to longevity is avoiding stress. In fact, premature aging has been linked to stress in a number of scientific studies, since stressors produce undue wear and tear on the body and the mind. For instance, in a study done at Heidelberg University a series of strenuous job interviews were administered to an aspiring young doctor, during which he was asked to solve complex mathematical equations.

When his blood was then sampled, it indicated that the stress of the interview had caused the release of antibodies, as if he had been infected by a virus or bacteria. If such a threat had existed, this immune response would be essential to wiping it out — but antibodies also attack healthy cells, leading the body to age faster than it should.

Therefore, reducing stress is key to living a long life and you can do it in a number of pleasant ways. Just try practicing mindfulness, doing yoga or taking time to exercise, all of which give you a chance to calm down and observe your body and mind more closely.

In the end, though, these are only preventative, long-term measures. In the next blink, you'll learn what to do if you're already so stressed out that you're on the verge of a total breakdown.

> _"We have to learn how to turn off the autopilot that's steering us in an endless loop."_

### 4. An intensive Japanese therapy form called Morita therapy helps people overcome stress. 

Anxiety, burn-out, stress; these conditions are becoming more and more common in modern life, and few people can escape them. As it turns out, Japan is no exception, and the country's working culture can actually be quite intense.

That being said, Japan has a tool that other countries lack. It's called _Morita_ therapy, and it can help you cope with stress.

This technique was invented by Shoma Morita, a psychotherapist and Buddhist practitioner. Originally, it was developed to deal with chronic anxiety, obsessions and compulsions. However, it also works great for stress and burnout.

Unlike some Western therapies, which tend to focus on using thoughts to influence feelings and actions, say through positive thinking, Morita therapy takes the opposite approach.

In Morita therapy, patients are asked to pay attention to and accept their feelings, without attempting to change them. From there, they take particular actions to create new emotions, which gradually replace the old ones.

There are four stages of the therapy, beginning with a phase of complete rest. For around a week, the patient lies in bed, free of all distractions. She isn't allowed to consume media, receive visitors or even speak, and her only human contact is a small amount of supervision by a psychotherapist. During this stage, the patient simply observes her emotions as they come and go.

In the second phase, the patient begins to integrate repetitive activities into her daily routine. These include writing in a diary, taking walks and doing a number of breathing exercises.

In the third stage, these activities become more physical and creative, incorporating tasks like wood cutting and painting. This series of activities produces a new set of emotions in the patient; she begins to feel joy, equanimity and engagement.

And finally, after completing these three stages, the patient is ready for stage four: reentering the world with a newfound sense of calm and purpose.

Getting plenty of rest and space from distractions in this way is great for your health, but at a certain point, you'll need to find something upon which to focus your attention. This is where ikigai comes back into the picture, and in the next blink you'll learn how.

### 5. Immersing yourself in an activity can help keep you young. 

Imagine you're skiing down a beautiful powdery slope. Skiing is your absolute favorite thing in the world, and you're in a state of complete, blissful, focused engagement. You feel as if you could do this for the rest of your life, you just might want to live forever.

The good news is that engaging in such an activity could, in fact, increase your lifespan.

Achieving such a state of _flow_ regularly can help keep you young. Flow in this sense is a technical term coined by the psychologist Mihaly Csikszentmihalyi in the 1970s. It describes a state of enjoyment and concentration so deep it blocks out all other concerns — even time itself.

Seeking out activities that produce such a state will increase your enjoyment of life and, therefore, your longevity. That's why such experiences should be prioritized over hedonistic ones like overeating, substance abuse or media entertainment, which people often indulge in out of boredom.

Flow is even shared across cultures and types of people. As a result, chess players, engineers and painters can all find themselves in a state of flow. But beyond that, it's healthy for the mind, since it focuses you on a single object for a sustained period. Ideally, your ikigai — that is, your main life occupation — will regularly produce a state of flow, but if it doesn't, be sure your hobbies do.

That being said, when setting out to experience a state of flow, calibrating the level of difficulty is essential. After all, if you choose a task that's too easy, you'll get bored and distracted, hindering you from attaining flow. On the other hand, if your activity is too hard, you'll struggle, get stressed out and eventually give up.

So, start from where you're at. For instance, if you decide to learn a new language, start at a basic level that gently challenges you. Or, if you're bored with a skill you already have, say computer programming, you can push the boundaries of your knowledge, maybe by learning a new coding language. By engaging your interests in a new, exciting way, you can find flow once again.

### 6. A few simple recommendations from some old-timers can help you live longer. 

You already know some habits to increase your longevity, but in the end, nobody knows how to live a long life better than those who are actually living one. So, let's take a look at the advice of the Okinawan centenarians.

The first tip these old folks recommend is that you worry as little as possible and make a habit of greeting others, even strangers, with a smile and an open heart. By doing so, they say you'll maintain plenty of friendships throughout your years and make your grandchildren want to visit you all the time. Such constant stimulation will help keep you young.

Beyond that, they caution that excessive worrying about things you can't change only causes unnecessary stress. For instance, worrying that you're not good enough or haven't had a successful enough career will only waste your life energy. Instead, the centenarians wisely advise you to enjoy what you have. If you do, they say, you'll likely realize you have much more than you thought.

But cutting out worry isn't the only centenarian recommendation for a long life; another is the cultivation of good habits.

For instance, waking up early in the morning is primarily a matter of habit; after doing it for a few years, it will just happen automatically. Beyond that, rising at an early hour will give you extra hours of quiet in the morning to drink your tea, clean your house and tend to your garden.

The last of these is especially important, as Okinawan centenarians believe that one of the primary reasons for their long life is that they grow their own vegetables and cook their own food. This makes perfect sense, as a garden-to-table diet is very healthy.

Finally, perhaps the most important habit of all for a long life is to enjoy and maintain your friendships. Recognizing this, Okinawans spend time chatting with their neighbors every single day.

### 7. The magic Okinawan diet is based on variety and small portions. 

The Japanese diet has been in the spotlight for years, ever since Japan made a name for itself as the country with the longest life expectancy. That being said, people live even longer in Okinawa province. To find out why, Makoto Suzuki, a heart specialist from Ryukyus University in Okinawa, did several studies on the Okinawan diet, beginning in the 1970s. Here's what he found out:

First, the Okinawan diet contains an incredible variety of foods. In fact, locals of this island eat up to 206 different foods on a regular basis, including a number of herbs and spices. For instance, every day, they eat five separate portions of fruits and vegetables. They like to determine that they're getting enough variety by ensuring their plates contain all the colors of the rainbow.

It could be thanks to this variety that the Okinawan diet is otherwise quite plain. The base of the diet is grains, like rice or noodles, while seasonings like salt and sugar are used sparingly. In fact, Okinawans eat 60 percent less sugar and 50 percent less salt than other Japanese natives, who already eat a diet that's relatively healthy by global standards.

So, variety is important, but so is small portion size. To abide by this second aspect, Okinawans say that you should stop eating when you're around 80 percent full; in other words, you should remain a little bit hungry.

There's even a word for this concept in Japanese. It's called _hara_ _hachi bu_, and simple ways to achieve it include avoiding dessert or reducing portion size.

To practice the latter, Okinawans typically serve their food on small plates, with portions of rice, vegetables, miso soup and a small snack, such as edamame beans.

They instinctively know that eating less is good for you, and modern science has actually confirmed the benefits of calorie reduction. By eating fewer calories, you can limit the level of a protein known as _insulin-like growth factor 1_. When too much of this protein exists in the body, cells age faster. As a result, eating less directly correlates to a longer life.

### 8. Antioxidant-rich foods are essential for rejuvenation and a long life. 

Superfoods have swept the earth in recent years, changing the way people think about diet as medicine. There are a few Japanese varieties of these foods you won't want to miss out on.

The first is green tea, a rejuvenating beverage loaded with antioxidants.

Recent studies have found that this beverage, one of the most popular in Okinawa, is a powerful promoter of longevity. Unlike a number of other teas, green tea is air-dried and left unfermented. Because of this, it retains its active elements, including antioxidants, and has been found to lower bad cholesterol, manage blood sugar levels, improve circulation and even ward off infection.

To make this miracle drink even more potent, Okinawans add jasmine to their green tea. This herb improves cardiovascular health and boosts immune function.

But green tea isn't for everyone. If you prefer an alternative, give white tea a try — it actually has even higher antioxidant levels than green tea.

But is there an Okinawan superfood that you can actually bite into?

You need look no further than shikuwasa, a citrus fruit that's another antioxidant-loaded Okinawan favorite. This traditional Japanese fruit is so acidic that its juice needs to be diluted before you can even consider consuming it.

It has a high concentration of nobiletin, a plant substance that's particularly rich in antioxidants. While other types of citrus, like lemons and oranges, contain some nobiletin, shikuwasa contains 40 times more than your average orange. As a result, the fruit is highly popular in Okinawa, where it's used as an ingredient in a number of typical dishes and even baked into cakes.

That being said, you might have a hard time getting your hands on this obscure Japanese citrus. If so, don't sweat it. Just try other citrus fruits, or opt for broccoli, salmon, strawberries or apricots, all of which are high in antioxidants.

### 9. Movement, even in simple forms, is a fundamental aspect of a long life. 

Have you ever met older people who seemed full of energy despite their age? Well, chances are that they lived that long because they've been physically active throughout their lives.

In fact, movement in general is important to living a long and happy life, and it doesn't have to be too intense either; observing the residents of Okinawa suggests that sports and fitness are much less important than simple, regular movement.

For instance, elderly Okinawans walk around their neighborhoods, work in their gardens and even get on stage at karaoke bars. The key to their activity isn't its intensity, but the fact that it never stops.

But if you don't want to take their word for it, consider what modern science has to say:

According to health expert Gavin Bradley, sitting adversely impacts your health. He found that after just a half hour sitting in a chair, the metabolism slows down, disrupting the healthy digestion of fat. What's more, sitting for over two hours causes good cholesterol levels to drop.

The good news is that getting up for a mere five minutes every half hour is sufficient to offset these effects; even so, most office workers neglect such a practice.

Okinawans also partake in a more concerted form of exercise. It's called _Radio Taiso,_ and it's a basic warm-up common for Okinawans and Japanese natives in general.

It's practiced in the morning or throughout the day, often in large groups. It's very common, and certain Okinawan schools, businesses and old folks' homes gather as a community to do these exercises every morning.

Originally, the routine was even broadcast on the radio, hence the "radio" in its name. But today, most people tune into their daily workout on television or online.

The exercises themselves are simple and straightforward. For instance, one movement is to lift your arms above your head before bringing them down your sides in a circular motion. The aim is just to gently warm up the joints and muscles of the arms and shoulders.

### 10. Final summary 

The key message in this book:

**Living a long, happy, healthy life depends on eating well and getting plenty of exercise, but longevity extends beyond such simple daily practices. By finding a purpose that drives you each and every day, you can focus your energy and extend your years on earth.**

Actionable advice:

**Bask in the beauty of imperfection.**

In Japanese culture, there's a belief that only imperfect objects, like a cracked teacup, can be truly beautiful. This concept is known as _wabi-sabi,_ and it can help you find more enjoyment in your day-to-day life. So, try to let go of the quest for perfection that's so common in life, and instead accept the beauty that lies in all of life's _imperfections_. The result will be extra energy, less stress and a longer life.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Life-Changing Magic of Tidying Up_** **by Marie Kondo**

_The Life-Changing Magic of Tidying Up_ isn't just a guide to decluttering, it's a best seller that's changed lives in Japan, Europe and the United States. The _Wall Street Journal_ even called Marie Kondo's Shinto-inspired "KonMari" technique "the cult of tidying up." Kondo explains in detail the many ways in which your living space affects all aspects of your life, and how you can ensure that each item in it has powerful personal significance. By following her simple yet resonant advice, you can move closer to achieving your dreams.
---

### Hector Garcia Puigcerver and Francesc Miralles

Hector Garcia Puigcerver is a dual citizen of Japan and Spain, a specialist in Japanese culture and the author of _A Geek in Japan._

Francesc Miralles is the best-selling author of _Love in Small Letters_ and _Wabi Sabi._

