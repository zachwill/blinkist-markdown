---
id: 59bfafbbb238e100052dbda0
slug: im-ok-youre-ok-en
published_date: 2017-09-22T00:00:00.000+00:00
author: Thomas A. Harris
title: I'm Ok, You're Ok
subtitle: None
main_color: 23A8B0
text_color: 16696E
---

# I'm Ok, You're Ok

_None_

**Thomas A. Harris**

_I'm Ok, You're Ok_ (1969) is a valuable guide to understanding how our past experiences and memories affect our life in the present. Even our earliest childhood years can keep us from leading the life we desire — so find out how you can take control of your emotions and break free from the past in order to have a healthy and happy future.

---
### 1. What’s in it for me? Master your inner child and parent. 

We've all been there. In the heat of an argument, or when discussing a touchy issue, you suddenly start to sound a lot like your mom or dad. Maybe it's a horribly regressive argument your mom used to drill into you about upholding female beauty standards, or maybe it's your father berating you for crying and yelling at you to "act like a man." Maybe it's uncontrollable anxiety that is triggered when you and your partner get into a fight, because of abandonment issues from your childhood.

It can be unsettling to look our inner child or parent squarely in the face — but according to the author, all of us have these two forces within us. Luckily, there are ways to recognize and control these parts of ourselves, and to set new patterns of thought and behavior.

In these blinks, you'll learn

  * why birth is inherently traumatic;

  * that many people get stuck in prejudice; and

  * why you should foster your inner child's playfulness.

### 2. Our memories are linked to powerful emotions that are better understood when analyzed. 

Have you ever heard a loved one talk in his or her sleep?

You may think these nocturnal mutterings are rather mysterious — but they're actually prompted by memories stored in the brain.

Certain parts of the brain are responsible for our memories and the feelings we associate with them.

This was first discovered in 1951 by the Montreal brain surgeon Wilder Penfield, who stimulated certain parts of his patients' brains with an electrode. Since the patients were only under local anesthesia, they could describe the different responses to the various areas being stimulated, which is how Penfield found that the temporal cortex of the brain is connected to visual memory, language and emotion.

When Penfield touched a certain point on the patient's brain, the patient would make seemingly random statements, such as describing a previous conversation or a popular television commercial.

What was really impressive was that the patient didn't only reexperience the memory but also felt emotions connected to it. In other words, the patients _relived_ the experience emotionally, not only recalled it.

You can also relive memories unconsciously.

Our own memories are most often triggered by everyday occurrences and impressions, such as sounds or smells. Once triggered, these memories can cause us to relive past experiences.

Though this process usually happens unconsciously, we can also take the time to dig up memories, especially when we want to analyze our emotions.

For example, say there's a song that makes you feel sad whenever you hear it. You may not understand this unconscious reaction, but if you were to sit down, perhaps with a therapist, you could figure out which past experience and emotions the song is attached to.

The author did this very thing with a patient to help her remember that her mother, who died when she was just five years old, used to play a particular song on the piano.

### 3. Transactional analysis recognizes three main personality components: the Child, Parent and Adult. 

When you hear the phrase "multiple personalities," you might think of someone with a psychological disorder. However, having different personalities may be more common than you think.

Psychologist Eric Berne, who passed away in 1970, developed a form of therapy called _transactional analysis_, and one of the main principles is that everyone has three main _personality components_.

Berne began to recognize these different personalities around the 1950s when he saw similar patterns in patients he was seeing.

One patient was a successful lawyer by day, but, while in therapy, he behaved like a small child, whining and impulsively acting out.

Everyone has the capacity for these kinds of behavioral changes. A child can be calm one minute and break into tears when a prized toy breaks. Or a normally peaceful adult can become an anxious bundle of nerves if his business goes bankrupt.

Dr. Berne gave a name to the three personality components that lay within us all: the Child, the Parent and the Adult.

_The Child_ is the result of experiences collected during our early years as a helpless child.

_The Parent_ is the result of memories that relate to the beliefs and behaviors of our parents and other authority figures.

_The Adult_ is our rational self, with the ability to find a healthy balance between these first two personalities and come up with good solutions and behavior.

As you can probably tell, it's not too difficult to recognize these different personalities when they surface.

For example, one of the author's patients was a mother who was suffering from insomnia. She explained that, at times, she worried that her nervous behavior was affecting her children. Clearly, this was her rational inner Adult.

During other discussions, she was in tears over feeling so helpless; her voice even changed, making it apparent that this was her inner Child.

But then her voice could also be commanding, such as the time she told the author that she believed children need to learn to respect their mother and father and know their place. This obviously came from her inner Parent.

> _"Continual observation has supported the assumption that these three states exist in all people."_

### 4. Early childhood memories can lead to feelings of insecurity and the impression that others are stronger. 

If you try to think back to your earliest memories, how far can you go? Most of us can't recall anything from our first two years of life, but that doesn't mean this period doesn't have a lasting impact.

It's important to remember that there is a difference between consciously recalling an experience and unconsciously reliving it. Evidence from transactional analysis tells us that our emotional reactions regularly cause us to unconsciously relive our earliest childhood experiences, even if we can't remember them.

These early childhood memories often give people a sense of insecurity, along with an accompanying feeling that everyone else feels secure.

Dreams also have this effect. One of the author's patients had a recurring dream that she was a tiny speck of dust in the cosmos, surrounded by huge objects. But this dream would also come with a terrible feeling of suffocation.

With the help of analysis, the patient remembered that her domineering mother believed that children should be fed a lot. And the dream was likely a remembrance of her mother trying to force her to breastfeed when she was no longer hungry.

Experiences like these — from when we were a helpless child — are what contribute to our feelings of being "not okay."

But there's little we can do to avoid this; helplessness is, after all, part of being born. From the very first moment, we move from the safety and security of the womb to the cold outside world where we're cut off from our original source of nourishment.

This is followed by early childhood experiences that give us the impression that others are secure. Since birth is the traumatic experience that makes us feel unprotected, we are comforted by the coddling of our parents. So we immediately believe that only others are strong, self-sufficient and can provide us with safety.

Thankfully, as we'll find in the next blink, there are ways we can change this thought pattern.

### 5. People tend to adhere to old patterns of behavior, but these patterns can be broken. 

Even if you've sworn never to behave like your mother or father, chances are you'll eventually catch yourself doing exactly that.

We're all programmed by old patterns of behavior that are informed by the Parent and Child parts of our personality.

The Parent component is governed by the kind of rigid rules that are passed down from generation to generation, delimiting acceptable behavior. Consequently, the Child is governed by fear of the potential consequences of its actions.

So, imagine being a white man in the 1960s, living in the racially charged times of the civil-rights movement in the United States. If you lived in a predominantly white neighborhood at this time, you might have received a request to sign a petition to fight the discriminatory housing laws that helped create urban ghettos.

For you, a white man, this decision might cause a great deal of anxiety. Your inner Parent might carry the racist beliefs of previous generations and lead you to think that you'd be disobeying them by signing the petition. And since your Child is governed by fear of disobeying authority, you might well decide not to sign.

But as we know, people can change and exercise their individuality through their inner Adult.

The Adult questions our instinctual responses and encourages us to look for new information about a subject. In the case of the petition, the Adult could lead the man to research the situation and make an informed decision.

Naturally, this is the part of us that leads to change and progress. But we first need to get our Child and Parent under control. And in the next blink we'll learn more about how to recognize and rein in these aspects of our personality.

### 6. We can learn to recognize the Child, Parent and Adult within us. 

Part of growing up is learning to recognize the emotions of others by looking at their facial expressions. In a similar way, you can learn to recognize your own inner Child, Adult and Parent.

The best way to do this is to understand the different physical cues that accompany each one.

For example, when someone speaks with their Parent voice, there's a good chance their brow will be furrowed, their lips pursed and that they'll be pointing at something or someone. The Parent will often sigh, use expressions of horror or outrage, click their tongue and cross their arms.

An extreme expression of the Parent is when they pat someone on the head. This is a very condescending gesture that is generally reserved for people to whom the Parent feels superior. Often, such condescension is indicative of some kind of prejudice.

As for the Child, clues to look out for are temper tantrums, rolling of the eyes, pouting lips and a whining tone. The Child will also tease people, bite his or her nails, be restless and get overexcited, leading to outbursts of tears or laughter.

The Adult face, on the other hand, is harder to recognize. It is actually easier to identify by the absence of the extreme behavior that marks the Child and the Parent.

But that doesn't mean the adult face is dull or neutral. The eyes, body and face are casually animated and not manic. Plus, the Adult knows how to occasionally make room for the excitement and joy of the Child.

Now that we know how to recognize the Adult, Child and Parent, let's take a look at how they interact.

### 7. The Parent, Child and Adult are susceptible to contamination and suppression. 

As the saying goes, "Two's company and three's a crowd." And this goes for the different components of our personality as well, since they don't always know how to peacefully get along with each other.

A common problem is that the Parent or Child can prohibit the Adult from functioning properly.

In transactional analysis, this is known as _contamination_, and as we hinted at in the previous blink, when the old ideas of the Parent contaminate the Adult, the result can be prejudice.

Say you were raised to believe that people become poor because they're lazy. This idea can contaminate your thinking. Perhaps you even tried to question it as a child, only to be chastised and told that concerning yourself with layabouts is just a waste of time.

As a child, you can integrate your parent's prejudices into your own, and even if someone rationally explains how some people are at a disadvantage in society, it won't matter. The only way to decontaminate is to have your Adult understand that it's now safe to disagree and question the opinions of the Parent.

Another frequent problem is when the Parent excludes the Child or vice versa.

You probably know someone who's a workaholic and doesn't make any room in their life for family or fun. This is usually the result of a strict upbringing that demanded constant obedience. People who exclude their inner Child are usually all too familiar with the proverb, "Children should be seen and not heard."

As a result, the Parent keeps the joyful and playful aspects of the Child suppressed and inaccessible.

Next, we'll look at the problems your Child can cause.

### 8. Our inner Child likes to play games that allow it to feel superior and garner sympathy. 

Children love to play games, whether it's Monopoly or hide-and-seek.

But sometimes our inner Child can play some troublesome psychological games, such as asserting superiority over others.

This tendency can start in young toddlers who insist that their toys are better and cooler than anyone else's.

Yet the reality of the situation is that our inner Child is insecure, and claiming superiority is a way for it to gain false confidence and feel better about itself, even if that feeling is only temporary.

This might explain why groups in conflict often feel the need to find a scapegoat, which allows the majority of members to feel superior and ease their inclinations toward self-doubt.

Our inner Child also likes to play the victim as a way of eliciting sympathy and care.

A common scenario here is for a person to bring up a problem and ask for help, yet go on to dismiss every possible solution.

For instance, let's say someone's unhappy with her job. A friend might suggest she talk with her supervisor, but the inner Child will say, "Why bother, she'll just ignore me." Or if the friend suggests she start looking for a new job, the inner Child will complain that it doesn't have enough time or that the job market is too difficult right now.

The aim here is to get the friend to give up and confirm the inner Child's fear that it's not okay and validate its feelings of insecurity. This way, the Child gets to be cared for by a Parent, who in this case would be the friend.

In the final blink, we'll find out how to move beyond these games to arrive at the position of "I'm okay, you're okay."

### 9. To arrive at the point of “I’m okay,” you must recognize your emotional patterns and change them. 

Everyone, at one point or another, feels so overwhelmed that they believe things will never get any easier. But if you're able to step back and look at things with a fresh perspective, you'll see that this isn't a realistic outlook.

There's a similar process of recognizing your own emotional patterns that will allow you to arrive at the feeling of "I'm okay."

Let's say you're having trouble making a career choice and don't understand what's holding you back. If you consider the Parent, you might realize that you were raised with pressure to become a doctor or a lawyer — professions that were deemed "respectable" by your parents.

This can lead to years of following a career path that doesn't bring you any satisfaction, yet your inner Child is afraid to disappoint the Parent.

Once you recognize these patterns, your inner Adult can begin creating new ones. Freed from the constraints of the Parent and Child, the Adult can start exploring possible career choices that actually suit you.

But first, you need to break the pattern of blindly following the voice of authority figures and seeking their approval in the choices you make. This will also allow the Child to let go of its fears and turn the responsibility over to the Adult, who can then make choices that reflect your individuality.

When this happens, you'll find that the Adult is great at assessing situations and making careful decisions that produce positive results.

We all want to have a life that reflects who we are and not what someone else expects of us. This can happen once we look inward and recognize the different components of our personality. Once we do this, we can start taking control.

It's easy to get stuck thinking, "I am not okay," but once you conquer your inner Parent and Child, you'll see that you are deserving of love and the chance to live life on your own terms.

### 10. Final summary 

The key message in this book:

**Everybody comes into the world with the feeling that they are not okay, and that other people are okay. This is because birth is traumatic and babies are highly dependant on their caretakers. By discarding old patterns of behavior and making new positive experiences, people can arrive at a new position: I'm okay, you're okay.**

Actionable advice:

**Listen to your thoughts.**

Whenever you have a negative thought, try to assess how realistic it is. Use the concepts of the Parent, Child and Adult to help you.

For example, if you catch yourself thinking, "I am really lazy," ask yourself if this isn't, in fact, a judgment that you have gotten from a Parent or authority figure. Or perhaps it is a mechanism of the Child to hold you back and keep you in a safe, familiar place.

Gradually, give more space to your Adult, who can make a more realistic assessment of your abilities and their bearing on your life.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Daring Greatly_** **by Brené Brown**

_Daring_ _Greatly_ explores how embracing one's vulnerability and imperfection is necessary for achieving real engagement and social connection. Through explaining our deep-seated reasons for shame, and showing how to embrace our vulnerability, the author aims to provide guidance for a better private and professional life, and to initiate a fundamental transformation in our shame-based society which, according to the author, needs to adapt a new culture of vulnerability.
---

### Thomas A. Harris

Thomas A. Harris is a successful psychiatrist who worked for the United States Navy. He later became a university professor and founded an association for Transactional Analysis, a therapeutic method based on the ideas of psychologist Dr. Eric Berne.

