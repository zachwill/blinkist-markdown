---
id: 5bf41cfe6cee07000758f168
slug: imagine-it-forward-en
published_date: 2018-11-22T00:00:00.000+00:00
author: Beth Comstock with Thal Raz
title: Imagine It Forward
subtitle: Courage, Creativity, and the Power of Change
main_color: FD3338
text_color: C7282C
---

# Imagine It Forward

_Courage, Creativity, and the Power of Change_

**Beth Comstock with Thal Raz**

_Imagine it Forward_ (2018) charts the successes and setbacks of one of America's most prolific businesswomen, Beth Comstock. Combining anecdotes from her tenure at General Electric with surprising insights and indispensable practical advice, these blinks explore the life and times of this remarkable change-maker and innovator.

---
### 1. What’s in it for me? Discover how to embrace radical change. 

Facing change is always difficult, both in business and in our personal lives. And yet, the modern world demands constant change and innovation from those who are serious about achieving their goals. What's more, the pace of change is only going to accelerate in the future, so we'd better get used to this constant state of flux.

But change doesn't have to be scary. Let's go on a journey with Beth Comstock, former vice chair of General Electric. We'll take a close look at Comstock's career and discover how she has successfully led change and navigated stumbling blocks in one of America's most prolific corporations. We'll learn how this introverted female publicist conquered a male-dominated company that had operated in the same way for over 125 years and dragged it into the modern digital world.

In these blinks, you'll learn

  * what huge life-change Comstock made to succeed;

  * how introversion can help you conquer the business world; and

  * why conflict drives better innovation.

### 2. A capacity for decisive action kick-started Beth Comstock’s career. 

Beth Comstock was the first female vice chair of General Electric (GE), one of America's biggest companies. But Comstock's professional life wasn't always so promising. When she was in her early twenties, her life was headed down an entirely different track.

That's when she took big risks to change her life and shake up her career.

Though she had long had ambitions of becoming a science journalist, by her mid-twenties, her career was stalling. Holding down two jobs as a part-time waitress and a minor news reporter in her hometown, she couldn't seem to land any of the more promising jobs for which she applied. Disillusioned, she did what her parents and her small-town society expected of her. She settled down with her college boyfriend, got married and fell pregnant.

Within only a few short months, she felt trapped. She knew there was a better life waiting for her if she only had the courage to reach for it. Throwing caution to the wind, she told her husband she wanted a divorce and moved with her infant daughter to Washington DC.

Though she was choosing to become a single mother, with all its accompanying challenges, it was clear to her that only by embracing imperfection, complications and mistakes would she be able to get the full life she desperately wanted.

Soon, her gamble paid off, and she got a job working in publicity at NBC's news bureau in Washington. Within a short space of time, she was promoted and put in charge of the entire department.

Significantly, she largely attributes her success to her capacity for action.

She believes that most people labor under the idea that only those with considerable power — those of us who are already "leaders" in some way — have the capacity to take action. More often than not, those of us who don't consider ourselves to be powerful are content to accept that we cannot effect change, whether in our personal or professional lives.

But Comstock has always tended to take decisive action. Whether it was divorcing her husband and starting a new life or boldly implementing new ideas at work, she is proud to say that she has never made excuses to herself about why she can't do what she needs to do and take the next step along the road to a better future.

### 3. Comstock’s natural introversion helped her to navigate workplace sexism. 

By 1998, Comstock's media career was flourishing. Now 38 years of age, she was vice president of NBC News and had helped transform the network into a major media player.

But once again, risk and change came knocking when the CEO of GE — the famous Jack Welch — asked her to join his company as vice president of corporate communications. She readily accepted, not knowing that this would be her toughest assignment yet.

When she joined GE, she anticipated that pivoting from the world of media to that of industry would be a major challenge. But what she didn't expect was the intense sexism she would face from her new colleagues.

During the late 1990s, GE was an extremely male-dominated environment. So much so, that at the hotel the company used for its annual executives' conference, the women executives had to use an improvised restroom next to the hotel's kitchen as the actual women's restroom in the conference hall was turned into a men's restroom.

This male-dominated culture was even more pronounced in GE's headquarters. Comstock's male executive colleagues frequently failed to invite her to crucial meetings and made it clear that they couldn't stand the idea of a woman entering their domains, much less being in charge of important strategic decisions.

Luckily, she had a secret weapon at her disposal when it came to handling this sexist hostility: she was an introvert.

Although she was often not given much latitude to contribute to meetings and was generally made to feel like an outsider — she didn't mind too much. As an introvert, she was already used to being a quiet outsider in the business arena, which is typically dominated by those with more extroverted personalities.

So, to cope with her new environment, the author fell back on the natural advantages she possessed as an introvert. For instance, introverts may not make their voices heard in meetings, but they tend to be very good listeners and observers, capable of thoughtfully processing the ideas and discussions going on around them.

In this new hostile environment, she was able to harness this introvert tendency to focus more on the ideas being expressed by her new colleagues, than on the colleagues themselves, who were often hostile toward her. In other words, her introversion gave her the ability to distance herself from her colleagues, focus only on their ideas, and not make it personal.

### 4. Comstock triumphed over adversity through bold innovation. 

When frightening situations occur, we often want to hunker down and take comfort in what's safe and familiar. But believe it or not, these times of stress and adversity can also be hidden opportunities to try something new.

On September 11th, 2001, Comstock was preparing for another ordinary day at GE. But then the first plane hit the World Trade Center — suddenly, the whole of America, including GE, was thrown into a state of crisis.

In the days following the horrific attack, many of her communications colleagues tried to keep their heads down and get through the crisis as best they could. But Comstock knew that GE, and the country at large, needed an inspirational message in their darkest hour.

As a company with a lot of business interests tied up in the aviation and airline industry, GE found its finances were hit particularly hard in the days after 9/11 when planes across America were grounded. With the company's employees deeply shaken, Comstock decided that as the company's communications chief, she had a duty to reassure the workforce.

Additionally, she believed that GE, as one of America's largest corporations, had a responsibility to act as a beacon of hope and leadership to the whole nation. She decided that GE needed to put out a special advertisement in America's top newspapers to demonstrate strength in the face of adversity.

Perhaps unsurprisingly, most of her colleagues thought she was mad. This was a time of national crisis. It would be business suicide for the company to seem to be profiting from this tragedy.

But against all her colleague's advice, the author decided to follow her gut and put out the advert — a full-page print ad depicting the Statue of Liberty rolling up her sleeves and stepping off her plinth. The text underneath was a simple message proclaiming that Americans would stand together, move forward, and never forget.

Thankfully, the author's creative gamble paid off.

Within days, the advert had been cut out and taped to the walls of thousands of New York businesses, including the New York Stock Exchange. Amazingly, the cartoon itself became a symbol of New Yorkers' defiance in the face of adversity.

This just goes to show that you shouldn't automatically reach for what's familiar or comfortable in a crisis. Instead, triumph over adversity often requires bold innovation — imagining yourself forward through the darkness, toward the light.

### 5. Worthwhile change inevitably involves conflict. 

In the early 2000s, traditional television networks around the globe found themselves competing with a strange new player in the media world — cats playing the piano. New digital platforms such as YouTube and MySpace were suddenly vying for consumers' attention. Traditional media networks would have to change to survive. But, as Comstock found out, there would be no change without conflict.

To ensure GE's television network, NBC, moved with the times, the company assigned Comstock to be NBC's new head of digital. Her job was to oversee GE's investment in new digital content for its media network. Unfortunately, some of her colleagues weren't happy about this new allocation of resources — and they weren't afraid to show it.

In her efforts to bring about much-needed change at NBC, she faced particular hostility from the head of NBC's TV group, Jeff Zucker.

Zucker had experienced a meteoric rise through the NBC ranks after successfully transforming NBC's _The Today Show_ into the most-watched morning show in America. Unfortunately, Jeff couldn't see past television. As the author attempted to drag NBC into a digital future, their conflict exploded. Zucker began to shout at Comstock in meetings, belittling her in the presence of the entire division.

When the author suggested making cuts to his department to free up money for digital, things turned physical. Zucker grabbed her by her collar and physically dragged her out of his office after one particularly heated argument. When he found he couldn't stop the digital onslaught this way, he tried another tactic — feeding stories to the _New York Post_ that Comstock was a terrible person to work with.

Unsurprisingly, Comstock was deeply shaken by these increasingly personal attacks. Nonetheless, she understands the company's decision to allow her and Zucker to fight it out. Why? Because conflict is an indispensable part of change and progress.

Great leaders accept that tension is not only inevitable but can also fuel innovation.

Conflict often improves products and ideas because the innovator is usually forced to consider divergent viewpoints in the creation process, and often the idea evolves into something better as a result. In Comstock's case, she eventually oversaw the successful acquisition of major digital platform iVillage in a deal worth $600 million.

> "_Tension is the price of admission when you are innovating."_

### 6. Storytelling helps us tackle uncertainty and work toward a better future. 

Change, as we all know, is inevitable. But in the modern economy, with its constant stream of new technologies, start-ups and disruptions to traditional industries, change comes ever more rapidly. Economists have even coined an acronym for it: _VUCA_ — volatile, uncertain, complex and ambiguous.

Unfortunately, during her tenure at GE, Comstock has witnessed the company having to learn about this uncertainty the hard way.

In the early 2000s, things were looking rosy for GE, and stocks in the company climbed to an incredible $60. By 2007, however, the company's finance department was starting to report incidences of company deals where the numbers weren't adding up. Concerned, GE hired strategy consultants McKinsey to crunch the organization's finances. The results of the analysis? Everything was fine, said McKinsey.

But everything wasn't fine. Just one year later, the subprime mortgage crisis hit. GE's capital unit had invested in these mortgages, and the fallout from the crisis led the company to the edge of financial ruin. GE's stock price crashed to a low of just $6.66, and the federal government was forced to bail the company out of impending bankruptcy.

Interestingly, Comstock learned a valuable lesson during these tumultuous times: each uncertainty holds the promise of a new and better future. But to reach this better future, you have to be able to make sense of the uncertainty and build a coherent story around it.

Too often, a story or narrative is something that corporations tack on to the end of their strategy as a final decoration. But in an uncertain world, stories shouldn't be an afterthought; they should be front and center.

After a distressing or unexpected event, whether at a personal or organizational level, people and organizations tend to lose all sense of meaning and purpose. After the humiliating near collapse of GE, many of its employees felt that their work didn't matter anymore.

This is where storytelling comes in. By creating and disseminating coherent stories about why the collapse had happened, and where the company was heading, the author found that her employees were finally able to start making some sense of this distressing event. And once they could do that, they could move on with building the company back up again.

> _"We can't make uncertainty go away. But we can change how we react to it."_

### 7. The digital era demands that old hierarchies adapt to new collaborations and openness. 

The mantra of the digital age is openness. In today's economy, open and easy access to information and ideas has brought about a radical change in the way the world does business. Old hierarchies have been replaced by networks, and centralized business bureaucracies have given way to new platforms that are shared and decentralized. So where does GE fit into this brave new world of information sharing and openness?

From 2007, GE saw how profitable this new generation of digital companies, such as Facebook, Amazon and Google, were becoming. Soon, GE's top executives began to want a piece of this new digital pie for themselves. But the question was, would they be capable of the change needed to get it?

Significantly, GE was a company born in a different time: the industrial age. This was a period when huge emphasis was put on top-down control, productivity and certainty. Therefore, to GE's senior executives, the hyper-collaborative spirit of the twenty-first century's networked era seemed like hippie-inspired insanity. GE's power had always been based on first developing and owning knowledge, and then jealously guarding this knowledge and treating it as a major asset.

But Comstock knew that this approach would not serve them in the digital era.

Why? Because today's problems, such as the environment, technological challenges and healthcare, are just too complex for one company to solve alone — even for a behemoth like GE.

Additionally, power in the digital era is open, peer-driven and participatory. Just consider the enormous clout of social media companies such as Instagram and Twitter, which rely on the participation and content of millions of users.

In an attempt to usher in this new age of collaboration and open participation at GE, she introduced an initiative.

Under her guidance, GE created a high-profile event called the GE Ecomagination Challenge. This was an exciting new contest in which inventors and entrepreneurs from all over the world were invited to submit sustainable energy innovations. In collaboration with venture capital firms, GE selected the best ideas and gave the winners a share of $200 million to develop their innovations.

At the end of their first step into the world of open collaboration and ideas sharing, GE had invested a total of $140 million in 23 new start-ups. Some of these start-ups were very successful, like Opower, a digital service helping households to conserve energy. This just goes to show that even the most traditional companies are capable of change.

### 8. Final summary 

The key message in these blinks:

**To make any sort of meaningful progress, whether in the workplace or your personal life, you can't be afraid to make bold changes. Being a cheerleader for change often brings conflict, resistance and uncertainty, but you shouldn't let that hold you back. It is usually only when we take risks and lean into the unknown that brave, meaningful change happens.**

Actionable advice:

**Don't be afraid to dabble.**

During her tenure at GE, the author has often found herself called a "dabbler" by her colleagues. Why? Because she wasn't afraid to test new ideas and innovations — to dabble in them — even if there was a good chance they wouldn't be successful.

Dabbling is a crucial part of the creative process. Psychologists believe that the difference between exceptional creatives and their more mediocre peers is not that they fail less often, but that they have a greater number of ideas. So if anyone accuses you of being a "dabbler," remind them that science tells us that theories, insights and connections are born of more ideas and innovation, not fewer.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Winning_** **by Jack Welch with Suzy Welch**

_Winning_ (2005) is a collection of no-nonsense advice and original thinking on successfully running a company, managing people and building a career. It answers the toughest questions people face both in and outside their professional lives.
---

### Beth Comstock with Thal Raz

Beth Comstock was the first female vice chair of General Electric and also served as the company's chief marketing and commercial officer. In both 2015 and 2016, _Forbes Magazine_ named Comstock one of the world's 100 most powerful women.

Taz Rahl is an award-winning journalist and co-author of New York Times bestseller _Never Eat Alone_.

