---
id: 544cd2b465633200081f0000
slug: immigrants-en
published_date: 2014-10-27T00:00:00.000+00:00
author: Philippe Legrain
title: Immigrants
subtitle: Your Country Needs Them
main_color: 61C7ED
text_color: 377187
---

# Immigrants

_Your Country Needs Them_

**Philippe Legrain**

_Immigrants_ offers a compelling case for a total revamp of the way most people view immigration and immigrants. It provides a detailed description of the case against immigration, while providing solid evidence for the great benefits, both social and economic, that migration provides.

---
### 1. What’s in it for me? Discover how migration shapes the world for the better. 

Immigration is a fiercely debated topic, which often leaves immigrants as the scapegoat for society's ills. On cable news and on the street, political pundits and concerned citizens lament that immigrants from around the world are coming to "take our jobs" and ruin our civilization, blaming them for rising crime and economic instability.

But as you'll soon find out, these beliefs aren't just racist — they're totally backward.

In fact, migrants are a boon, both for their destinations as well as their countries of origin, providing their new homes with skilled and unskilled labor as well as a previously unmatched cultural richness. What's more, some of the very institutions that opponents of immigration fear will fall by the wayside with illegal immigration, such as social welfare, can only be maintained by opening up the borders.

Not only is migration highly advantageous — it's also a human right. Societies across the globe would do well to reconsider their stance on immigration in favor of economically smart, and more humane, immigration policies.

In these blinks, you'll discover

  * why a shrinking population helped Sweden become what it is today;

  * why the Philippines _encourage_ emigration; and

  * how it's possible for a country to have too many doctors and engineers.

### 2. Migration is a human right that has been asserted throughout history. 

No matter where you live on the planet, you'll probably notice that a portion of the population has a certain animosity toward immigrants and immigration. In many countries the media is awash with stories about the "floods" of immigrants crossing the border to snatch up jobs and welfare.

However, these fears are misplaced.

For starters, the process of migration has been ongoing for thousands of years, and is by no means a recent phenomenon. Indeed, people have been on the move since the dawn of human history. Our ancient ancestors, for example, migrated to the four corners of the globe from a central point: Africa.

In the nineteenth century, technological innovations, such as the steam ship and train, accelerated the process of migration. During this period, most migration was from the Old World — Europe — to the New World of the Americas.

However, in the twentieth century, the dynamics of migration took a 180-degree turn: suddenly people were mostly moving from the developing world to the developed one.

It is this change that conjured up the idea of a mass exodus to the West. But there is no such exodus. If you look at the numbers, the migrant population remains relatively small: only a few million people migrate to the West annually, compared to the _billions_ of people who remain behind in the developing world.

Immigration only seems high because migrants head to only a handful of destination countries.

History aside, migration is also a human right.

All too often, when we look at those who enter our country, we only see one side of their experience: immigration. But migration is a two-way process: every _immigrant_ is also an _emigrant_.

People leave their home countries for an endless number of reasons, and the right to emigrate is even codified by Article 13 of the Universal Declaration of Human Rights. Thus, in impeding someone's ability to migrate — and thus emigrate — you are a denying them their basic human rights.

Nevertheless, as you'll see in the following blinks, many governments try to curb the amount of immigrants coming into their country.

### 3. Impeding migration is morally wrong, and only leads to death and exploitation. 

Often politicians, both on the right and the left, pontificate about the dangers of migration and advocate for stringent control of the borders. However, _any_ attempt at controlling immigration is morally wrong.

Firstly, it smacks of racism to view immigration as a bane to society. Of course, society is bound to experience changes with an influx of immigrants. Some of these will be objectively bad, such as the collapse of old solidarities between or within groups of people.

However, many of these changes will be good! Just think of all the opportunities for cultural exploration and the many skills and ideas that immigrants bring to their new homes.

Seeing only the negatives betrays racist attitudes. While it isn't racist to worry that _some_ immigrants might be thieves and villains, the assumption that foreigners _in general_ tend to be thieves and villains is built upon racist ideology, and should not be taken seriously.

Moreover, our attempts to control the borders, while they might curb immigration, also lead to death and exploitation. Consider the death toll at the US–Mexican border: it's so huge that no one really knows how many die trying to cross it.

However, we _do_ know that the recorded death toll at the border over the last ten years was at least _ten times higher_ than the 138 lives that the Berlin Wall claimed in its 28 years of existence.

Furthermore, those who, for whatever reason, immigrate illegally end up without rights and become exploited on the black market. Without papers to legitimate them, they work more hours for less money, and cannot afford to get ill or fight for their basic rights.

For example, the Polish immigrants that came to the United Kingdom without a work permit in the early 2000s found that their service industry jobs would not earn them enough to pay their rent, even _if_ they worked every day.

### 4. Migration is impossible to prevent and too expensive to try to minimize. 

Many places do their best to make immigration difficult. The European Union, for example, controls its external borders carefully, and the United States is building a fence across its Mexican border while increasing the pay for border patrols.

Indeed, the cost of preventing free movement is enormous, both in terms of money and lives. The US Congress, for instance, quintupled its spending on border security from 1993 to 2004 (finally reaching $3.8 billion) while tripling the size of the border patrol. Yet, despite this huge layout, the number of illegal immigrants is estimated to have stayed the same.

No matter how sophisticated border security becomes, migrants will still try their best to get through, even at the risk of great personal danger. We see this in the United States, where the straightforward crossings at the US–Mexican border are more tightly controlled, leaving migrants to cross over deserts and dangerous rivers.

The great lengths that migrants will go to to secure a new life show that immigration cannot be prevented, despite countries' efforts to create "secure" borders.

Ceuta and Melilla, Spain's enclaves in Morocco, clearly demonstrate that even small borders are not fully controllable. Even with high fences and numerous patrols, thousands of migrants make it into Spanish territory every year.

Desperate migrants always find ways to break through, and unless governments are prepared to defend borders with deadly force, none will be 100 percent controllable.

In a satirical piece on immigration policy, _The Economist_ sardonically remarked that, if the US House of Representatives planned to erect a fence along the border to Mexico, it should follow the model of East Germany's former socialist government: in lieu of lights and cameras, outfit it with bunkers, barbed wire, minefields and machine-gun posts. In other words, build a wall and not a fence.

All sarcasm aside, that really is what it would take to totally curb immigration. However, as you'll learn in the following blinks, the potential of freer migration is far too great to be squandered by attempts to prevent it.

### 5. Migrants’ countries of origin can benefit from emigration. 

When people in the West think about migration, they often only consider its effects on their own countries. Often, they miss the great impacts that migration has on migrants' countries of origin.

When Westerners _do_ take this into consideration, they usually only consider _brain drain,_ the exodus of many highly skilled members of society into another country. However, this is a major oversimplification.

Of course, for some countries, brain drain is a formidable problem, especially if they are war-torn or authoritarian.

However, the simple truth is that many migrants can't achieve their full potential at home. In order to gain more skills — and get the most mileage from the ones they have — they have to leave. This pursuit of higher wages often means that migrants send money "back home" to friends and relatives, which in turn greatly benefits their economy. And if these migrants decide to return to their native countries, they will bring back with them a wealth of experience that they can pass on to others.

Moreover, many other countries _have_ to export their skilled workers. For example, countries like India or Cuba have more trained doctors and nurses than they can actually use, so emigration is a win–win.

In fact, some countries even develop policies designed to promote emigration. Once a year, the Philippines actually celebrate their emigrants on Migrant Workers Day, during which the president awards the "Bagong Bayani" to 20 outstanding emigrant workers.

The Philippines recognizes the value of emigration: emigrants return with new knowledge and experiences, they open up market opportunities and further opportunities for development and their remittances account for at least one eighth of the country's economy.

Sweden, too, owes its development to emigration. Between 1870 and 1910, one sixth of the population left Sweden, primarily for the United States. Not only did these emigrants send back money and open up trading contacts, but they also relieved the pressure Swedish society faced regarding jobs and housing, thus leading to increased wages and productivity for those who stayed behind.

### 6. Immigrants benefit the economies of their destination countries. 

The discourse around immigration often conjures images of shifty immigrants "taking" jobs from locals or lowering wages. But does immigration really cost jobs?

The notion that the economic benefits enjoyed by one country or person rest on another's loss is incorrect. In fact, immigration leads to economic prosperity from which everyone benefits.

The superior infrastructure in rich countries allows immigrants to be more productive, and their skills help the economy grow. Many immigrants take jobs others don't (or won't) take. However, others arrive at their destination countries as highly skilled professionals in specialized industries, and their special skills allow for economic developments that increase prosperity within society as a whole.

Another common complaint is that immigrants drain a country's welfare system. Not only is this untrue, but the exact reverse is the case: migrants actually help to _sustain_ it.

Most immigrants migrate in order to earn a better living, but are not eligible for any welfare benefits. Nevertheless, their productivity and their taxes help to prop up the welfare system that they are excluded from.

Specifically, many developed countries have birth rates that are too low to sustain their aging populations. In order to provide services for the elderly or ill, young immigrants are needed to pay into those welfare systems.

But even if migrants could live on welfare, most have no desire to do so. While living on food stamps and free public healthcare in the United States would be enough to meet their most basic human needs, this is not the aim of most migrants. Many want to earn enough to send home or provide for a better education for their children.

Consequently, most work hard in order to make enough money to achieve these goals.

While specific workers will certainly be passed over for immigrants in the job market, society as a whole greatly benefits from immigration. Even those who lose their jobs to immigrants will be better off in the long run, enjoying more opportunities, more aid, better schooling and so on.

### 7. Diversity leads to creativity, prosperity and benefits for all. 

The economies of today's developed countries are increasingly based on knowledge. Thus, to achieve prosperity, different ideas and experiences have to come together.

Luckily, migration provides ample opportunity for sharing ideas. Indeed, multicultural cities are hubs of creativity, prosperity and development.

For example, cities such as London or Hollywood attract specialized foreigners who work in the same field — bankers in London and filmmakers or actors in Hollywood — to a singular international nexus. There, they have an opportunity to mix their ideas and unique experiences in a way that fosters creativity.

However, achieving these creative spaces requires openness. Take Japan, for example, which after World War II was a model for development. However, limited immigration and integration, and therefore insularity, eventually brought an end, at least temporarily, to its prosperity.

Furthermore, as we've seen in our previous blink, integration causes changes for both society and the economy. But when it comes to migration, these changes aren't bad. In fact, they lead directly to diversity and prosperity.

Most immigrants share goals with those they work with: they want to earn money, create and develop. Immigrants _want_ to cooperate with the native population, to add their unique experiences to their new homes and bring about prosperity.

A good example of this can be found in Israel's immigration history:

From 1990 to 1997, Israel saw a 15 percent increase in the working-age population. Because Jews have always been allowed to migrate to Israel, the end of the Soviet Union meant that around 700,000 Jews moved to Israel from Russia and Ukraine.

For a short time, this influx hurt wages. Soon, however, investments rose, unemployment fell and the economy prospered on the whole. This period of migration was a great boon to society, leading to an increase in jobs and diversity of services, such as the many restaurants and shops established by migrants with different backgrounds, which contributed to cultural richness and economic prosperity.

### 8. Low-skilled immigration is beneficial to a country and is hard to stop. 

When the media or populist politicians launch into tirades against the dangers of immigrants, they don't take issue with the managers or surgeons — i.e., highly-skilled labor — who arrive on their shores. Rather, they rail against the "masses" of low-skilled workers.

In order to combat the alleged dangers of low-skilled immigration, many countries employ a "points-based" system whereby immigrants may only enter the country if they can demonstrate skills assumed to be beneficial to the society.

Countries that employ these systems, such as Australia, which allows immigration almost exclusively in cases where a particular job in a particular industry is understaffed, hope to have better control over the quality of immigrants to their countries by selecting them by their profession.

However, these point systems don't work. In essence, it's impossible to know what professionals your country _actually_ needs — we just don't have that kind of knowledge.

What's more, there's no way to stop career changes once someone already has a residence permit. For example, if you let a plumber enter your country, you won't know whether she'll later discover a love for another profession.

Crucially, attempts to select only highly skilled immigrants overlook an important fact: low-skilled immigration is a benefit to society.

For starters, low-skilled workers often take jobs that no one else wants and no one else does. Many of these jobs — such as road maintenance, service work, or in-home child care — are vital to our standard of living. And guess what: they're done by immigrants.

The work they do makes life easier for everyone. Without an affordable nanny, parents are forced to stay home, which means less work, less productivity and less tax revenue.

By now, you've seen the many positive influences that migration has on societies. Our final blink will deal with changing our attitudes to make migration as smooth and fair as possible.

### 9. We need to rethink integration. 

What do people want most from immigrants? Many want immigrants to _integrate_ — to abandon the cultural practices of their old lives and adopt those of their new homes. But is this a fair demand? Should immigrants be expected to completely conform to the culture of their destination countries?

Wanting immigrants to integrate in this way suggests that all immigrants are the same, and that they all hold beliefs and share cultural practices that are wrong and need to be replaced.

Yet immigrants are extremely diverse — even when they have the same country of origin. They each have their own beliefs, which don't need to be abandoned, but can in fact overlap with new ones.

A fear of unintegrated Mexicans or militant Muslims only becomes possible if you're using an unfair generalization of diverse cultures that forces them into simplified categories.

For example, although US Americans might lazily call all Latin Americans "hispanics," the label has to stretch to fit people from 20 different countries. And while many continue to speak Spanish or Portuguese, only a tiny fraction of Latin American immigrants don't speak English well enough to get by.

It's not that they don't integrate; they often simply don't want to lose everything from their previous lives, such as their language or food. By holding onto those practices, they help to enrich the culture of their new society.

Expecting all immigrants to simply become like you won't work. However, allowing a mixture of native and immigrant cultures will.

While a common language is necessary, this shouldn't come at the cost of culture. Both immigrants and natives of destination countries should use the opportunity to redefine what they believe.

Canada does this best, seeing itself as multicultural, ever-evolving society with no fixed culture. Canadian identity is an open and dynamic concept, encompassing a broad spectrum of possibilities.

Nations, states and societies change with migration, but these changes bring the opportunity to redefine ourselves and our values.

### 10. Final summary 

The key message in this book:

**Migration is not only a human right, but also a great benefit to both migrants' destination and native countries. Despite the challenges it poses, greater freedom of movement across the globe will ultimately lead to greater prosperity and cultural richness for those countries that embrace migration.**

**Suggested** **further** **reading:** ** _The Audacity of Hope_** **by Barack Obama**

_The Audacity of Hope_ is based on a keynote speech Barack Obama delivered at the 2004 Democratic Convention, which launched him into the spotlight of the nation. It contains many of the subjects of Obama's 2008 campaign for the presidency.
---

### Philippe Legrain

Philippe Legrain is an economist and political scientist whose writings on globalization and migration appear in the _Guardian_. In addition, he has authored a number of critically acclaimed books, including _Open World: The Truth about Globalization_ and _Aftershock: Reshaping the World Economy After the Crisis_.

