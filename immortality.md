---
id: 53984fd43737310007070000
slug: immortality-en
published_date: 2014-06-10T12:56:02.000+00:00
author: Stephen Cave
title: Immortality
subtitle: The Quest to Live Forever and How It Drives Civilization
main_color: EF304D
text_color: BD263C
---

# Immortality

_The Quest to Live Forever and How It Drives Civilization_

**Stephen Cave**

_Immortality_ explores humanity's drive to live forever and the constructive and destructive forces that accompany it. The book elaborates on the various ways that we try to achieve eternal life, and ultimately offers us some advice on how to cope with our mortality.

---
### 1. What’s in it for me? Learn how to manage your fear of death. 

Have you ever pondered your existence after death? Or wondered what it would be like to live forever? If so, you certainly wouldn't be the first! Ever since we human beings became aware of our inevitable end, we have struggled to cope with mortality and the inability to imagine our experience after death.

In fact, the desire for immortality is essential to the human condition in a way that not only shapes but actually underpins our civilizational development.

In these blinks, you'll learn about the "immortality narratives" that we use to cope with our impending death, and to which all forms of culture owe their existence. In addition, you'll read about how each of these narratives, for one reason or another, are ultimately unable to actually grant us the immortality we so desire.

Finally, these blinks will show you

  * what the great heroes of antiquity and modern techno-futurists have in common,

  * the strange way in which resurrection is related to cannibalism,

  * why immortality would turn you into an even worse procrastinator and

  * why, even if we were immortal, we'd "only" live around 5,775 years.

### 2. The knowledge of our own death is what separates us from all other life-forms. 

Do you ever lie in bed contemplating your death — the moment the Grim Reaper finally knocks at your door? We know that it will come one day, yet we still hope it never will. Why is it that we suffer from these kinds of thoughts?

For starters, like all other living things, humans are "survival machines"; our ultimate aim is to stay alive at all costs. This struggle for survival is carried out through the evolutionary process of adaptation and reproduction: the individuals with the fittest genes are most likely to reproduce, thus extending its likeness into the future through its posterity. 

What makes humans unique, however, is that we struggle to survive with the full knowledge that we will nonetheless die one day. This desire to survive and knowledge of our own demise leads to something called the _mortality_ _paradox_.

On the one hand, surrounded by deaths of animals and other people, we know very well that we're going to die at some point. On the other, we can't actually imagine the experience of death, i.e., we cannot conceive non-existence.

Just try it: imagine your own death. What do you see? A tombstone? A funeral? You may think that you are imagining death, but in fact you aren't. Being dead means that there is no _you_ to _consciously_ contemplate these situations; we have no way to experience or even imagine what it's like to not exist.

Hence the paradox: our perception of death is both inevitable (as we all will die) _and_ impossible (since we cannot imagine the experience of death).

So what do we do when faced with this paradox? Our desire to survive and our inability to comprehend what it is like to not exist makes us look for ways to cheat death altogether, i.e., strive for immortality. The following blinks will explain how we go about this.

> _"Life is a constant war we are doomed to lose."_

### 3. Human society has invented “immortality narratives” to cope with the thought of death. 

Can you imagine what life would be like if we were constantly focused on our inevitable demise? How would we get anything done? The devastating thought that all our life's many deeds will have been futile after our death would make life seem absolutely meaningless, leaving us paralyzed.

This sort of tension is simply too much to bear over time. That's why we've created _immortality_ _narratives_, or stories that distract us from the death of our individual self and our communities.

These narratives help avoid a state of constant panic and chaos by instilling in us the idea that death can actually be avoided, or at least be made irrelevant. With this knowledge, we can safely live out our lives knowing that we — and our achievements — will carry on in some way after death.

Moreover, immortality narratives serve as the buttresses upon which civilization is built. Indeed, we find immortality narratives at the heart of humanity's great cultural achievements — our religions, philosophies, sciences, arts and architecture.

Just consider all the religious artifacts — temples, pyramids, churches, etc. — that have been central to our lives for millennia. Their primary purpose is to remind us of an eternal truth: the promise that we, in some way shape or form, will live on forever.

Even in modern science and ethics — most obviously in medicine — our aim is to keep humankind alive for as long as possible. For example, today's laws against the abhorrent crime of murder are rooted in ancient laws written in religious texts, such as the Bible. If it weren't for these ancient laws, we wouldn't have our modern codes of law that keep us alive at the expense of our baser instincts.

Now that we know why we've created immortality narratives, the following blinks will explore four of the most prominent ones.

### 4. The simplest immortality narrative is staying alive forever. 

Have you ever been in a life-threatening situation? How did you react? You probably didn't just give up and think, "Well, I guess it's time to die." Far more likely, you felt an incredible urge to stay alive, thus taking immediate action to save yourself.

The first immortality narrative plays off this primal urge to survive. In fact, humankind has always dreamed of an _immortality_ _elixir_, i.e., some natural way to live forever. Staying alive forever is a natural extension of our biological impulse to survive and is, as such, the most profane method of seeking immortality.

Although profane, the rise of the scientific method seems to inch us slowly towards making this fantasy into reality by breaking down the problem of mortality into solvable chunks, e.g., finding out the causes of death, disease, aging, injury, etc., and solving them one at a time. In the last century alone, medical innovations, such as antibiotics and organ transplants, have doubled our average life expectancy.

Some people, such as _transhumanists_, even believe that today's generation could live long enough to create the technology that allows us to once again double our lifespans. And within _that_ period, the process would repeat itself, allowing us to once again double or triple our lifespan, then continue repeating until we could conceivably live forever.

However, even if we do find an immortality elixir, we'd still have to face some thoroughly mortal problems. Perhaps the biggest problem with the "staying alive" narrative has to do with age. A longer life expectancy has brought about an increase in geriatric diseases, such as dementia and Alzheimer's.

Moreover, overcoming the aging process wouldn't eliminate the potentially lethal dangers of our environment. In fact, longevity researcher Steven Austad calculated the lifespan of an "immortal person" — still at risk from accidents and misfortune, such as a plane crash or murder — to be an average of "only" 5,775 years.

### 5. Even resurrection offers no guarantee of “surviving” death. 

If the methods to "stay alive" don't pan out the way we'd hoped, what would be the most obvious alternative? The answer is found in the narrative that accepts death but refuses to leave it at that: _resurrection_.

The _narrative_ _of_ _resurrection_ toys with the possibility of reviving a dead body. The biblical narration of Jesus of Nazareth's death and resurrection is perhaps the most famous of such narratives.

According to this story, we will all rise again on Armageddon — this time with imperishable bodies, forever young and healthy. Those who subscribe to this belief hold that God will put back together the decomposed particles of our once decayed bodies.

In today's world, futurologists and techno-utopians offer their own resurrection models. They posit, for instance, that one day it might be possible to upload the "psychic data" stored in our brains to a computer, and then download this data onto a new body once the original dies, thus living on in a new body. 

However, there are some fundamental problems with the idea of resurrection. For instance, there's the _Cannibal_ _Problem_ : like everything else, our decomposing bodies are part of a natural recycling process. As they lie in the ground, they're converted into energy for worms and other organisms, including people.

So even if we were resurrected, chances are that our bodies would consist mostly of other people.

This then leads us to the _Duplication_ _Problem_ : in order to be the same as before your death, you'd need to be strictly identical with your former self in every respect. But to what extent is that possible?

Given that we replace around 98 percent of our atoms every year, even the five-year-old you has little to nothing in common with your present self!

So of the many versions of you that have existed, which would be the "right" one you'd return to from the dead? This question, it seems, is unanswerable.

### 6. While the belief in a soul helped create just societies, it likely won’t get us closer to immortality. 

Because of the serious difficulties involved in reviving a cadaver, we'll need a more sophisticated concept of a second, eternal life. The solution: believing in an undying _soul_.

A _soul_ represents an individual's immaterial being that survives even after the death of the body. In fact, the very concept of the soul brought forth the most influential idea of the modern Western world: the autonomous individual.

Christianity, for example, holds that we communicate with God by means of our souls, and this spiritual self is what makes us both unique _and_ equal.

Modern secular society has taken this idea further by placing the unique individual at the heart of the political process. For example, our system of laws assumes that we are all subject to the same rights and judgment.

However, although it is a vital aspect of our culture, the concept of the immortal soul is challenged by scientific insights.

Modern biology, for example, has proven superfluous the idea of souls as an _animating_ _principle_ that separates humans from rocks or stones, first conceptualized the ancient Greeks; essentially, no evidence of a "spiritual substance" has been found in conscious beings.

Furthermore, while many consider our souls to be the same as our _consciousness,_ i.e., the human ability to think, believe and imagine, natural neurological mechanisms and structures of the brain can explain this phenomenon without the need for a supernatural soul.

Brain damage, for example, can cause cognitive dysfunction and seriously alter our personalities, influencing our moral behavior and respect for social norms. If the soul can't carry out these functions while you're still alive, what reason is there to believe that it will do so after death?

Despite wanting to continue our existence after death by leaving our bodies behind, modern science seems to indicate that this is impossible. But is there another immortality concept that is in line with modern-day knowledge?

### 7. Legacies offer us a way to live on in a more abstract sense, but even they have problems. 

The fourth immortality narrative is able to withstand the scrutiny of modern skepticism, and thus offer us some hope at eternal life. This is the concept of _legacy_.

One way to achieve this is by creating a _cultural_ _legacy_ through which we inscribe ourselves onto the everlasting realm of _the_ _symbolic_. Simply put, we can continue to exist through society's use of symbols — things such as written language, pictures, film or music — which are then used to pass on traditions.

The most effective way to build a cultural legacy is to _make_ _a_ _distinction_, i.e., accomplish some extraordinary act that enshrines you in cultural traditions that will last long after you're gone.

These extraordinary achievements form the _heroism_ that's at the heart of cultural legacy. For example, famous personalities such as Alexander the Great or James Dean all earned merits based on deeds that remain culturally relevant decades or even millennia after their deaths.

The modern world, with its millions of blogs and social media profiles, makes obvious the enormous importance that cultural legacy has for us.

And if your achievements aren't grand enough to be recorded in history books, you can strive to create a _biological_ _legacy_ by having children. Indeed, we often regard the mere fact that we are outlived by our children as a way to "live on" through them and their descendants.

Yet neither kind of legacy can deliver on the promise of eternal existence.

Although the symbolic realm of cultural legacy is abstract and non-material, it's not actually eternal: if the human race ever goes extinct, our entire cultural history and legacy will die along with it.

Our biological legacy also faces challenges. Our children are their own unique individuals that happen to share some of our genes without really representing our essence.

Now that we've explored some of the ways we might continue on after death, these final blinks will examine whether immortality is actually worth striving for.

### 8. Both immortality narratives and actual, lived immortality have serious downsides. 

Immortality seems to be universally desirable, but is it really worth the effort? In fact, it has some pretty serious drawbacks.

For instance, even though immortality narratives have proven crucial for the progress of civilization, they also unleash more destructive forces. The most vivid evidence of this are the many wars fought in the name of specific belief systems. Whether Christianity, Islam, capitalism or communism, millions of people have died in wars between societies with differing immortality narratives.

Furthermore, most immortality narratives nurture a profound egoism. Indeed, many of our ethical, political and religious norms demand seemingly selfless actions — not because they are themselves correct, however, but because they offer us personal rewards in the afterlife.

Even if you give to charity, build a school on your gap year or perform community service, the driving force is not the charity itself, but rather saving _your_ individual soul from oblivion or damnation.

Moreover, the lived consequences of immortality would likely be devastating. For example, if we lived for eternity, then it would be impossible _not_ to have experienced every possible thing at least once. With an infinite amount of time, we'd have the opportunity to do an infinite number of things.

While this might seem exciting at first, we would eventually grow weary of each lifestyle we chose to pursue in our eternal lives. Imagine: one day you're a TV chef, then you're a prime minister, and then a homeless person. Sure, it sounds exciting, but what happens once you've done all there is to do?

In addition, deadlines would become meaningless because time itself would be worthless. If you think you procrastinate now, just imagine how much worse it would be then.

Clearly, there are many good reasons to view immortality critically. But how should we cope with our impending deaths without the comfort of the immortality narrative?

> _"Most of us want to live on, to be free from the fear of death: yet such a wish alone does not prepare us for its consequence: being alive _forever._ "_

### 9. We should adopt narratives that help us embrace mortality instead of trying to overcome it. 

As we've seen, our means of coping with the inevitable end are all lacking in some way or another. Instead, we should simply embrace our finite time on the planet.

In fact, our inability to imagine the experience of being dead is our best chance to live happily in spite of our mortality. Rather than managing our fear of death by hoping for an afterlife, we should instead look at the positive aspects of our inability to experience death.

If being dead means having no experiences, then why worry about it? There's no eternal darkness or loneliness to fear — rather, as Epicurus said thousands of years ago, "Death is nothing to us."

And if we can cope with the thought of a finite existence, then we can better cherish this life instead of longing for another.

We should therefore adopt a mortality-affirming narrative of wisdom. This narrative advises that we:

  * _Identify_ _with_ _others_. While self-preservation and self-interest might be important, they serve to intensify your fear of death, because it means the end of the only thing relevant to you: yourself. If you instead situate yourself within a larger social and historical context, then the end of your individual self can seem minor in comparison. 

  * _Focus_ _on_ _the_ _present_. Don't worry about the future. If we don't concentrate on the now, we will miss all the happiness within our immediate reach.

  * _Exercise_ _gratitude_. Our existence in this universe is nothing short of a statistical miracle. In light of the millions of years of evolution and an unthinkable number of cosmic coincidences that brought plants, animals, humans and you into this world, just be grateful that it came together at all!

By adopting this narrative of wisdom, you can emancipate yourself from the addiction to existence and the need for an afterlife, and in doing so increase your own happiness.

### 10. Final Summary 

The key message in this book:

**Since** **our** **bodies** **are** **hard-wired** **to** **reject** **our** **mortality,** **we've** **created** **stories** **about** **achievable** **immortality** **in** **order** **to** **cope.** **More** **than** **likely,** **however,** **immortality** **is** **impossible,** **so** **our** **time** **would** **be** **better** **spent** **striving** **to** **enjoy** **the** **moment** **instead** **of** **battling** **in** **vain** **against** **certain** **death.**
---

### Stephen Cave

Stephen Cave holds a PhD in philosophy from the University of Cambridge, and worked as a diplomat before beginning his career in writing. He writes regularly for the _Financial_ _Times_ and has contributed to _The_ _New_ _York_ _Times_, _The_ _Guardian_, _Wired_ and other publications.

