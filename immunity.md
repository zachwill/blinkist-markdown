---
id: 57097a4d8b5d6e00030000b7
slug: immunity-en
published_date: 2016-04-13T00:00:00.000+00:00
author: William E. Paul, MD
title: Immunity
subtitle: The importance of our immune system
main_color: 4F8ED9
text_color: 335C8C
---

# Immunity

_The importance of our immune system_

**William E. Paul, MD**

_Immunity_ (2015) is a guide to that great defender of the human body, otherwise known as the immune system. It gives a detailed explanation of how immune responses defend us against all manner of diseases, even cancers, and how a dysfunctional immune system can spell big trouble.

---
### 1. What’s in it for me? Learn how your body beats infections. 

Each day, at all times, we're surrounded by infectious diseases, and there's only one thing standing between us and a life of constant sickness: our immune system!

The importance of our immune system really can't be overstated. As we'll see in these blinks, it serves as a remarkable shield against viruses and bacteria, and if it somehow fails, the results can be life threatening.

From the immunity-mobilizing effects of vaccines to the role immunity may play in the fight against cancer, these blinks take you through the history, science and basic principles of immunity.

You'll also learn

  * how eighteenth-century milkmaids led to the discovery of vaccination;

  * why mucus is essential to an immune response; and

  * what terrible things can happen when an immune system is dysfunctional.

### 2. The immune system is the key to fighting off diseases, but it also has a dangerous side. 

If asked, most people would say your immune system is a good thing, right? After all, the human immune system, especially when aided by medicine, is what defends us against viruses, bacteria and diseases of all types. However, this power doesn't come without a hook: if your immune response is somehow off, it can devastate your body, producing life-threatening illnesses.

The body must strike a fine balance. To drive this point home, let's look at a case famous among immunologists.

For millennia, the virus that causes smallpox plagued humankind. It killed hundreds of millions of people, until, on May 8th, 1980, the World Health Organization declared smallpox a thing of the past.

But how was it vanquished?

By boosting the immune system through vaccination, a technique discovered by an eighteenth-century English physician named Edward Jenner. He observed that milkmaids often contracted the less-serious _cowpox_, and that, upon recovery, they were immune to smallpox. Jenner knew that the two viruses were closely related and he hypothesized that intentionally exposing humans to cowpox could protect them from smallpox.

Thus the birth of vaccination, the activation of an immune response in our bodies that produces virus-killing cells. As a result of this discovery, the WHO launched a smallpox-eradication campaign in 1967 that used extensive vaccination with incredible success.

But the same immune response that vaccinations prompt can also go horribly wrong.

Indeed, diabetes is the result of a malfunctioning immune system. Usually the tissue of a patient is not the target of immune responses; however, the immune system of people with _type 1 diabetes mellitus_ starts attacking the body itself. As a result, virus-killing cells called _T cells_ break down the body's own insulin-producing cells, which play an instrumental role in regulating blood-sugar levels. The result of this immune dysfunction is diabetes, a serious and even life-threatening illness.

> _"Vaccines have probably had a greater impact on human health than any other human intervention [...]."_

### 3. The human body has three immune responses and three laws that govern immunity. 

So the immune system is essential and also dangerous, but what exactly happens when it responds to and starts fighting an infection?

Actually, the human body has three different types of immune responses to _pathogens_, the producers of disease. The first is to erect a _physical barrier_ that prevents potential dangers like bacteria from accessing cells and infecting them.

For instance, the human airway is lined by a physical barrier of mucus that traps bacteria. Once the bacteria are stopped by this membrane, they're either swallowed and destroyed by the acids of the stomach, or they get spit out.

The second form of immunity is called _innate immunity_. This response occurs when particular cells detect what could be dangerous bacteria and help other cells fight them off.

And the third form is _adaptive immunity_. Here's how it works:

During an innate immune response, all the bacteria-fighting cells of any given type have the same capacity to find and destroy a pathogen. But during an adaptive immune response, extremely specialized cells are produced that are particularly well suited to fighting a specific threat, such as a specific disease. Then, after the body recovers, some of these cells stick around, which arms the body against future attacks.

So, there's three ways your immune system responds, but beyond that there are three fundamental laws that govern immunity. The first is the law of _universality_, which says that the immune system can produce specialized antibodies — cells that find and attack pathogens — for virtually any threat.

The second, the law of _tolerance_, says that the immune system will not attack the bodily cells of its host. And the third, the law of _appropriateness_, states that every pathogen requires a specific immune response, and also governs when to respond and how different threats should be handled.

In the next blinks, we'll go into more detail.

> _In principle, it is possible to make vaccines that inhibit the influence on the body of addictive drugs such as cocaine._

### 4. The law of universality has been discussed in the medical community since the early twentieth century. 

When it comes to the law of universality, the greatest question is how the immune system can act with _specificity_? That is, how can it produce highly-specialized antibodies that effectively destroy distinct threats, or, as they are called, _antigens_?

Well, to answer this question we'll have to retrace the steps of the scientists who first proposed the theory of specificity and the law of universality. That path began with the German scientist Paul Ehrlich, the first person to develop a theory that explained specificity.

In 1901, Ehrlich proposed the idea that antibodies are similar to the molecules that antigens prey on. The logic being that the antigens would get tricked into approaching the antibodies, who would then attack them. Since they're found in the blood, these antibodies could bind to antigens before the antigens reached the cells they intended to destroy.

However, Ehrlich's theory had a serious flaw.

Its prediction was that the world of antigens was quite limited, confined to the molecules that could bind to the cells they were targeting. But just a few years later it was found that practically any chemical compound, when paired with a protein, could be an antigen. Therefore, the universe of antigens is virtually unlimited and Ehrlich's belief was unfounded.

In the 1950s, the scientists David Talmage and Frank Macfarlane Burnet came up with a better theory of specificity that was based on _lymphocytes_, the white blood cells that are essential to adaptive immune responses.

They reasoned that each lymphocyte has a different antigen-recognition receptor and that there are a limited number of lymphocytes that can recognize any given antigen. So, when a specific antigen enters the body and is identified by its specific lymphocyte, specific antibodies are activated by being produced, or "cloned," in vast quantities. This theory, referred to as _clonal selection,_ is now widely accepted by the medical community.

### 5. The law of tolerance is about preventing immune responses from harming the body itself. 

It's only common sense: a gun in the home increases the chances of someone being shot. But what prevents the body's arsenal — that is, antibodies — from being turned on the body itself?

Actually, while such attacks do happen, immunity's second law, the law of tolerance, usually prevents such catastrophes. It does this by sending specific cells that deter the body from self-attacking.

So, while immune-system dysfunctions that cause antibodies to attack the host, or _autoimmune responses_, do sometimes occur, they're normally prevented by specific cells. These cells are called _regulatory T cells_, or "Tregs," for short. Tregs work by regulating the standard T cells, which are capable of killing cells infected by viruses.

In fact, a failure to generate Tregs is what causes people to experience autoimmune responses. For instance, a 2002 paper by the scientists R.S. Wildin, S. Smyk-Pearson and A.H. Filipovich detailed the unfortunate story of a boy who, shortly after birth, succumbed to a series of autoimmune responses, type 1 diabetes among them.

He soon had a middle-ear infection, followed by diarrhea and pneumonia. It was found that his medical problems were a result of a genetic mutation that prevented his body from developing Tregs.

But this isn't just a human problem. In fact, autoimmune responses have also been found in experiments on rats. For example, in the early 2000s, the Oxford scientists Fiona Powrie and Don Mason did experiments on a specific breed of rats called "nude rats," which have been found to not develop a T-cell immune system.

When the scientists transferred T cells, without Tregs, from normal rats to nude rats, the rodents experienced a range of autoimmune responses, thereby showing that Tregs are necessary to keep the potentially self-destructive power of T cells under control.

> _"Violating the second law of immunology carries a great cost."_

### 6. The third law of immunity states that each pathogen requires an appropriate immune response. 

Did you know that pathogens, the things that cause disease, can live anywhere in the human body and cause disease for a wide variety of reasons? It's true, and while some are explicitly toxic to the cells of our bodies, causing infections and immediate destruction, others enter cells and live inside them, often changing the function of the cell altogether and leading to diseases like cancer.

So, since every pathogen is different, they each require their own immune response, which brings us to the third law of immunity — the law of appropriateness.

For instance, we now know that an immune response requires the appropriate pathogen-killing T cells. Richard Locksley, a scientist and physician at the University of California, San Francisco, has shown that the use of the appropriate T cells in an immune response is absolutely essential.

Locksley took mice that were infected with a parasite known as _Leishmania major_. Some of the animals managed to get the infection under control, but others did not. What Locksley proved was that this difference in immunity was not a result of the intensity of the response but rather a result of how appropriate to the parasite the T cells of any given mouse were.

So, why did some rodents have a more appropriate response than others?

It has to do with the strength of a specific cell. That's because there are specialized cells that interpret threats and spur the correct immune response. These cells are known as _dendritic_ cells and they are key players in T cell activation, found in just about every bodily tissue.

Their power lies in a special recognition mechanism that reads the nature of a threat before ordering T cells to launch the appropriate immune response. For example, say your body is infected with the flu. The dendritic cells in your body will recognize the virus for what it is and find the T cells best suited to destroy it.

> _Different pathogens have different lifestyles and inhabit distinct niches within the body."_

### 7. The immune system might even be able to fight off cancer. 

The fight against cancer is one of modern medicine's greatest undertakings. This malignant form of tumor, an abnormal growth of bodily tissue, has largely stumped doctors and scientists, but they now believe that the immune system may offer a cure.

In fact, building immunity can actually slow the development of tumors. For instance, a study done in Taiwan found that hepatitis-B vaccination has remarkably reduced the incidence of liver cancer.

The study showed that people born between 1975 and 1976, before the vaccine was introduced, had an annual liver cancer rate of 0.64 per 100,000. But for those born between 1985 and 1986, when the vaccine was used commonly, the rate had fallen to just 0.1 — an 84-percent decrease!

Why?

Well, the vaccine could have produced an immune response that reduced cancer risk. But that's not the only example of immunity fighting off tumors. A 1950s experiment done by Richard Prehn and Joan Main at the Public Service Hospital of Seattle is another good example.

The study found that tumors could contain specific antigens, molecules that cause an immune response, and that cancers could be fought through the production of the correct antibodies.

The scientific pair exposed mice to a chemical that was known to cause cancer in rodents. Then, when the tumors of the mice had reached a predetermined size, the scientists would remove them surgically. A few days later the tumors were returned to the cancerous mice and also implanted in non-cancerous mice.

In ten out of the twelve specimens, the tumors grew in the previously non-tumorous mice, but were rejected by the mice from whom they had been removed. The implication is that the formerly cancerous mice developed an immunity to this particular type of tumor.

Studies like this are a beacon of hope that an immunity to cancer is on the horizon!

### 8. Final summary 

The key message in this book:

**The human immune system is a remarkable gift of biology, but a dysfunctional immune system can be just as dangerous as a healthy one is protective. By studying immunity, scientists have discovered remarkable cures to a variety of diseases and may even be on their way to ending cancer.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _On Immunity_** **by Eula Biss**

Being afraid that vaccinating our children creates more harm than good isn't a new phenomenon. It's a fear informed by numerous cultural narratives. _On Immunity_ looks at the different historical myths and metaphors in the vaccination debate, and presents statistics on vaccination's effects.
---

### William E. Paul, MD

William E. Paul, MD, was chief of the Laboratory of Immunology at the National Institute of Allergy and Infectious Diseases, one of the National Institutes of Health. He also served as president of the American Association of Immunologists.

