---
id: 5759399f496d1c0003e862b0
slug: improving-your-relationship-for-dummies-en
published_date: 2016-06-16T00:00:00.000+00:00
author: Paula Hall
title: Improving Your Relationship For Dummies
subtitle: None
main_color: FFEE44
text_color: 665F1B
---

# Improving Your Relationship For Dummies

_None_

**Paula Hall**

_Improving Your Relationship For Dummies_ (2010) provides simple techniques for couples hoping to boost their intimacy, sharpen their communication and overcome challenges in a productive, healthy way. It offers advice for both new couples that want to start off on the right foot and long-term couples that hope to get things back on track.

---
### 1. What’s in it for me? Tools that can upgrade any relationship. 

Most of us have many different relationships. There's probably someone in your life who makes your heart beat faster, and there's probably someone else who makes you want to scream and shout in anger.

Relationships are omnipresent and multifarious. You have them with your family and your friends, with your spouse, your boss and your co workers: the list goes on. Unfortunately — might as well admit it — these relationships aren't always as blooming and healthy as one might wish them.

So how do you turn the not-so-healthy ones around? This is what these blinks are all about. They constitute a hands-on, practical guide to improving your relationships, providing you with plenty of valuable tips and tricks.

In these blinks, you'll discover

  * what sushi might do for your relationship;

  * if your relationship has the three most important strengths; and

  * why replacing broken boilers in winter shouldn't break your relationship.

### 2. A strong relationship rests on compatibility, intimacy and day-to-day stability. 

Every relationship has its ups and downs. When you hit a rough patch, it's easy to forget about the positive things and dwell on the negative. If you want to improve your relationship, however, you have to build on its _strengths_.

All solid romantic relationships have three core strengths: _compatibility, intimacy_ and day-to-day _stability_.

The first core strength, compatibility, is the foundation of your relationship. It's the degree to which you and your partner actually suit each other. Do you have the same outlook on life? Do you share the same passions or interests?

People change as they get older, and compatibility changes accordingly. However, your core principles and life goals — a desire to have children, for instance, or a belief in monogamy — generally don't change much over time, and it's these goals and principles that truly form the foundation of compatibility.

The second core strength is intimacy: how close you and your partner really are. Intimacy doesn't just appear out of nowhere; it's a product of time and deep mutual understanding.

There are three different kinds of intimacy. _Emotional intimacy_ is about how openly you share your emotions. _Intellectual intimacy_ is about how well you understand each other's thought processes. And _physical intimacy_ is about how well you connect physically, through touch or sex.

Each kind of intimacy is important, but it's unlikely that a couple will feel perfectly intimate in each arena. Identifying which area of intimacy needs work is the first step toward improving it.

The last core strength, day-to-day _stability_, concerns how your relationship plays out on a daily level. Stability is extremely valuable; indeed, a relationship isn't sustainable without it.

Your relationship simply can't survive if it suffers from daily tension, no matter how compatible or intimate you are. When it comes to romance, harmony in your daily life is absolutely crucial to long-term success.

### 3. Improving your relationship often requires boosting your own self-esteem. 

How can someone else love you if you don't even love yourself? Your self-esteem isn't just about you: it has a major impact on both your relationship and your partner.

Relationship problems are often the result of low self-esteem. Everyone feels blue or insecure from time to time, but that feeling can turn into a deep-rooted anxiety if it persists long-term. And this can lead to paranoia: you might start thinking that there's no reason for your partner to love you. Such thoughts may manifest as jealousy, for instance, or as abject dependence.

Low self-esteem will eat away at your relationship. And no one can fix your self-esteem problems but you.

But where does low self-esteem come from? Predictably enough, it's often rooted in your past. You might have self-esteem problems if you grew up in a rough environment, if you've been cheated on or if you're struggling with health or financial problems. But remember: even if your self-esteem has been damaged by past experiences, you're still responsible for how you think of yourself today.

So here are some tips for improving your self-esteem next time you're feeling down.

First off, try to leave the past in the past. Of course, that's easier said than done, especially if there's been a lot of turmoil in your family or previous relationships. Remember that negativity doesn't have to define who you are. In fact, letting go of it is one of the best things you can do for yourself.

Negative overgeneralizations might be another cause for your low self-esteem. If you've had a boss who put you down a lot, for example, you might start to feel like all bosses will look down on your performance. Furthermore, remind yourself that it's not always your fault when things go wrong. If something is your fault, keep in mind that your mistakes don't have to define you.

### 4. Accept the differences between you and your partner, and strive to grow with them. 

No two people are exactly alike. Even identical twins have slightly different DNA. So, naturally, you and your partner will have your differences, too! Accepting that reality is an important part of maintaining a healthy relationship.

There are some common personality differences that couples tend to face.

First off, there are behavioral differences. Your partner behaves differently than you, be it when washing the dishes, shopping, cooking or arguing. This is perfectly natural. And yet, we often don't accept it. Can you recall situations where you've tried to impose your own behavioral patterns on your partner? Probably.

Emotional differences can also cause trouble. Your partner won't always express happiness, sadness or anger like you do — so don't expect to always see your partner reacting as you would.

Finally, you and your partner have differences in taste. You probably have different preferences when it comes to things like food or entertainment, and these differing tastes may extend to other areas of life, too — such as the kinds of people you're friends with or the forms of sex you enjoy.

Aim to accommodate and live with these differences rather than trying to eliminate them. In fact, the best thing you can do with your differences is embrace them!

If you try to change your partner to be more like you, you'll end up hitting a wall. Accepting your partner's differences isn't just healthier — it makes your relationship more interesting, too.

Differences between romantic partners fosters learning. Maybe you were afraid of raw fish, for example, until your partner taught you to love sushi! Or maybe your partner introduced you to a new genre of music or film.

So, when you encounter a difference, stay curious. Ask your partner questions instead of resisting or condemning it. Look at the differences between you and your partner as an opportunity to expand your horizons!

### 5. Be sure to spend quality time with your partner no matter how busy you are. 

At the beginning of a relationship, you want to spend as much time with your newfound love as possible. During that sweet honeymoon period, it's easy to find time to focus on your beloved; that's all you want to do. As you settle into a routine, however, it usually becomes harder to make time for the relationship.

This can be dangerous. Spending quality time with your partner is vital, even when your life starts to get hectic. So keep your relationship on track by learning to separate things that are _urgent_ from things that are merely _important_.

Urgent things demand your immediate attention. They often come with deadlines or liability issues attached. If your heater stops working mid-winter, for example, getting it fixed would be urgent, as would completing your tax returns if the deadline is tomorrow.

Important things, on the other hand, are those that impact your core values in life. Spending quality time with your partner is important because it's crucial to maintaining a loving relationship. Important things aren't always urgent, however, which makes them easier to brush aside. Quality time with your partner isn't urgent — but your relationship cannot thrive without it.

So how do you know when you're spending quality time? A good indicator is that you're not thinking about other things like work. You're actively listening when your partner tells you something. You're giving your full attention.

Quality time makes you feel better and brings you and your partner closer together. So make time for it. If you're very busy, schedule time for your partner in advance. You're much more likely to remember a dinner or weekend getaway once it's on your calendar.

### 6. Boost your relationship by boosting your love chemicals. 

There's a reason new relationships are so much fun. Indeed, in those early stages of romance, your body produces the same chemicals that addiction triggers. Intense feelings of attraction, butterflies in your stomach, light-headedness, a loss of appetite — all are responses to certain chemicals your body releases when you're in love.

One of those love chemicals is _dopamine_, a hormone that creates feelings of pleasure. Your _serotonin_ levels, on the other hand, drop when you're in love, and since serotonin helps you stay calm, a decrease results in feelings of nervousness and excitement. Your brain also releases _phenylethylamine_, or PEA — otherwise known as the "love" molecule.

The power of these chemicals lessens over time, however, which is part of the reason attraction fades, too. Fortunately, it's possible to influence your own biochemistry — and thus improve your relationship!

It may be impossible to dose your morning coffee with PEA and dopamine, but it is possible to increase your love hormones and get that feeling of excitement back.

You can pump up your dopamine, for instance, by eating more protein-rich foods, such as meat and fish. Foods high in the amino acid _tyrosine_ — such as almonds, avocados and bananas — also give your dopamine (and mood) a boost.

Sunlight increases your dopamine, too, so be sure to get as much of it as you can.

Any activity that makes your heart race also causes your body to release dopamine. So why not try some new, high-intensity activities with your partner, like skydiving, rock climbing or poker?

Afterward, bring up your PEA levels by eating chocolate, watching romance films or reading a romantic book. Anything that makes your heart flutter will raise your PEA.

### 7. Having sex is a healthy and important part of your relationship. 

When newly in a relationship, most partners can't keep their hands off each other. The beginning is usually the most physically exciting time. As time goes by, however, that sexual excitement tends to fade. So, let's go over some tips for keeping your sex life fun and interesting.

Sex isn't merely a fun and pleasurable activity. It's actually beneficial for both partners individually, _and_ it's an important part of maintaining any relationship.

When you have sex regularly, your body produces chemicals that strengthen you against both physical and emotional pain, making you and your partner more resistant to any bumps along the road.

Studies have also shown that sex helps reduce depression and anxiety. It even boosts your immune system, so the sexually active are less likely to get sick, too.

Sex is also good for your lean body tissue. It tightens up the skin, making you look and feel healthy and youthful.

And of course, sex increases your bond with your partner. When you have sex, your body releases _oxytocin_, the chemical that makes you feel deeply connected to someone. Having regular sex also strengthens monogamous relationships. Especially if you're a man, having sex causes your body to release _vasopressin_, a chemical that encourages faithfulness.

If your sex life begins to slump, the first thing you need to do is discuss it with your partner in a safe, nonjudgmental way. The problem can't be fixed without open and compassionate communication.

Make sure your comments aren't hurtful and don't give the impression that your partner isn't good enough for you. Instead, focus on positivity and improvement. Say things like, "I have some ideas we should try out" or "I think our sex life could be even better!"

### 8. Use active listening to communicate effectively. 

Everybody makes communication mistakes sometimes — it's perfectly normal, an inevitable part of human interaction. However, if these mistakes go unrecognized and unaddressed, they can become a barrier to a relationship's success.

As with all problems, these communication mistakes can't be rectified until they're identified. So, let's go over a few of the most common stumbling blocks couples encounter when trying to communicate.

First off, if your partner shares a personal problem, don't try to solve it right away. Instead, take the time to truly listen. If you try to solve the problem before you get the full story, it may seem like you're just trying to get rid of it, like you don't really care.

Competition is another common stumbling block. For instance, when your partner starts talking about a problem, you may feel tempted to share about the time you were in a similar or worse situation. Check that temptation! Sharing your own personal story doesn't necessarily foster effective communication. Effective communication is based on _listening._

Also, beware of giving your partner the _silent treatment._ The silent treatment simply means that you stay quiet. You may act like you're listening but in fact, you're just waiting for your partner to realize you don't want to talk so that the conversation ends.

All this goes to show that good communication is based on _active listening_. Active listening is about giving 100 percent of your attention to the person who's talking. It's about trying as hard as you can to truly connect and understand.

Becoming a better listener is an acquired skill. For starters, make sure your body language shows that you're engaged with the speaker. Nod and give an "mhmm" when you hear an important point. When your partner makes a _very_ important point, it's a good idea to clarify and summarize it.

So, if your partner says they need more emotional support, you might respond with, "You need more support right now because you're going through a rough week at work, right?" Comments like that illustrate that you're engaged with what your partner is saying.

### 9. When you have to disagree, timing and approach are critical. 

There's no such thing as a relationship that's free of disagreements. As we've seen before, all couples have their differences, and maintaining a good relationship is about _managing_ those differences. So, let's go over some tips for making your disagreements as healthy and productive as possible.

The first thing you need to consider is timing. Bad timing can make a small problem explode into a big one. You probably don't want to discuss your intimacy problems on the bus, for instance. Your partner might get off before you've reached your stop!

Prior to initiating a difficult conversation, stop and consider whether there are any external factors that might have a negative effect on it. If work, hormones, alcohol or a tight schedule are stressing out you or your partner, it's probably not the best time to talk about your relationship.

And when you do find a good time and place to have your disagreement, you need to determine the best way to approach your partner. Remember that your partner might not be the first person you should talk to. Why not go over things beforehand with a close and trustworthy friend? A friend might help you process your thoughts and emotions in ways that you can't on your own.

Ask yourself if _you_ are ready, too. Are you prepared to actively listen to your partner's thoughts? Are you willing to change your behavior if necessary? It's not always easy to put yourself in your partner's shoes.

Finally, keep in mind that the way you initiate a tough conversation has an impact on how it plays out. Don't make the mistake of starting with a side topic and hoping things will end up where you want. That can come off as manipulative and put your partner on the defensive.

### 10. It takes time and support to overcome an affair, and you need to identify its underlying causes. 

Learning about a partner's affair can be a traumatizing experience. Unfortunately, a lot of relationships don't survive it.

Coping with an affair is hardest when the news first arrives. Both partners need time and support to process their thoughts and the situation. So, when you first learn about an affair, go to your closest friends or relatives — people who can help you get through the first and stormiest phase.

Don't talk to your partner until you've calmed down a bit. The first few days will probably be an emotional rollercoaster, an ill-advised time to have an in-depth discussion.

In fact, it's a good idea to spend those first few days apart if you can. That way, you'll avoid channeling all your anger at your partner, and will be less likely to say things that you might later regret.

An affair isn't merely a sexual indiscretion; it's an indication that something in your relationship isn't right. So, don't simply assume that your partner is totally to blame. Instead, look for the underlying problem that caused the affair in the first place.

You and your partner are both responsible for maintaining your relationship, which means you're both accountable when a problem arises. Blaming your partner and trying to forget the affair doesn't do anything to prevent it from happening again.

If you discover that your partner has had an affair, consider whether you might have been neglectful in some way. Think back on your own past, too. Have you ever had an affair? Can you remember how you felt at the time?

On the other hand, if you're the one having an affair, ask yourself why you're unsatisfied with the way your relationship is. Maybe you need more excitement in life. Maybe you can't discuss certain topics with your partner.

What's crucial is that you get to the root of the affair. Don't just brush it under the rug.

### 11. Final summary 

The key message in this book:

**No relationship is perfect. All partners have their differences, no matter how compatible they are. Maintaining a good relationship is about staying intimate, communicating effectively and managing differences in a positive way. Stay open to your partner's thoughts and feelings, be adaptable and don't run away from problems — even if those problems manifest as an affair. A relationship is a lot of hard work, but, in the end, it's worth it.**

Actionable advice:

**Touch each other more.**

When you touch another person, your body releases oxytocin. The more you touch your partner, the closer your bond, so don't skimp on the cuddling and hugging, or on the sex!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Attached_** **by Amir Levine**

_Attached_ (2010) is all about how to make your relationships work. This book offers you valuable insight into the science of adult attachment and how to use this insight in everyday life, whether you're in a committed relationship or are still looking for love. It also provides tips and tricks on how to find the perfect partner and reveals why some people just aren't compatible.
---

### Paula Hall

Paula Hall is a writer and psychotherapist who specializes in helping couples overcome sexual and romantic problems. She is also the founder of The Institute for Sex Addiction Training and Head of Recovery for The Naked Truth Project.

© Paula Hall: Improving Your Relationship For Dummies copyright 2010, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

