---
id: 5b8303dab238e10007734b0d
slug: in-a-sunburned-country-en
published_date: 2018-08-27T00:00:00.000+00:00
author: Bill Bryson
title: In a Sunburned Country
subtitle: Discover the delights of "down under"
main_color: 3771B2
text_color: 275180
---

# In a Sunburned Country

_Discover the delights of "down under"_

**Bill Bryson**

_In a Sunburned Country_ (2000) is Bill Bryson's personal account of his time traveling around Australia. With stopovers in major cities, out-of-the-way mining towns and treks through the vast wilderness, it's a travelogue packed with insights into the history, culture and wildlife of this unique nation.

---
### 1. What’s in it for me? Discover the delights of “down under.” 

Imagine a country so vast the commute between two major cities takes days rather than hours! Where a group of people could buy half a million acres of land and carry out secret bomb tests without anyone noticing…

Well, such a place exists — it's called Australia! But it's not just Australia's sheer size that's breathtaking. From tropical rainforests to barren deserts, modern cities to the famously rugged "outback," it's a stunningly diverse place teeming with life and history.

That's something Bill Bryson — Anglo-American travel writer and connoisseur of the curious and captivating aspects of Australia — knows better than most. _In a Sunburned Country_ recounts his time traveling the country from north to south and east to west.

Displaying his trademark ability to dig out the tastiest tidbits, it's a brilliant travelogue full of fascinating facts that'll have you checking tickets for the next plane to Oz.

In the following blinks, you'll find out

  * why Cook went down in history as the discoverer of Australia;

  * how nineteenth-century gold rushes changed the country's fortunes; and

  * why a former prime minister refused to live in the capital.

### 2. Australia is often forgotten about, despite the fact that it’s a truly unique place. 

What do you _really_ know about Australia? If you're not from "down under," probably not a lot. It's a massive country that leaves a comparatively small footprint in the global consciousness. Even the most bizarre events there barely make international news.

Take the seismic activity that baffled experts in 1993.

The disturbances took place in the Great Victoria Desert. An earthquake was ruled out, which left a couple of possibilities. It couldn't have been a meteorite strike as there wasn't a crater, and the activity was too intense to be blamed on a mining accident. In the end, the tremors were chalked up as an unsolvable mystery.

Later, in 1995, the Japanese Aum Shinrikyo cult achieved notoriety after killing 12 commuters in a nerve gas attack on the Tokyo subway. Subsequent investigations turned up a massive 500,000-acre property owned by the group near the site of the previously unexplained seismic activity.

When the authorities investigated, they found a sophisticated laboratory and evidence that members of the group had been mining uranium. More disturbingly still, the group was known to have nuclear engineers among its members. Apparently, its aim of putting an end to the world included secret bomb tests in the desert!

How did the world respond to this shocking news? The _New York Times_ ran just one story, in 1997, four years after the actual event. That's a pretty good snapshot of the kind of place Australia is — a country so vast that covert nuclear bomb tests can go unnoticed for years!

There are hundreds of other fascinating stories waiting to be discovered in Australia, and that alone makes it worth exploring. You can also see that in its nature. Around 80 percent of all animal and plant life is native to the continent, and given that it's such a hostile, hot and flat environment, there's a stunning abundance of it.

But the country's vastness makes an exact headcount difficult. It's estimated there could be around 100,000 species of insects, but the true number could be double that. A good 30 percent of all insects are entirely unknown, while for spiders the number is closer to 80 percent!

> In 1967, Harold Holt, the Australian prime minister at the time, plunged into the ocean somewhere along the coast of Victoria and was never seen again.

### 3. Bryson first came to truly appreciate Australia’s history and vastness while traversing it by train. 

The first leg of Bryson's exploration began in Sydney, on Australia's eastern coast. Accompanied by photographer Trevor Ray Hart, he took the transcontinental Indian Pacific railroad to Perth, on the very western edge of the country. The epic journey covers 2,720 miles and crosses three states — New South Wales, South Australia and Western Australia.

One day into their journey, the two intrepid travelers disembarked at the small town of Broken Hill in New South Wales. A settlement firmly ensconced in the outback, it was the perfect launching pad for a two-day excursion through Australia's famously rugged wilderness.

Along the way, they stopped off in an even more remote village called White Cliffs.

To get there, Bryson and Hart drove along dirt tracks through a seemingly endless landscape. It was an arid affair, with plenty of red sand, dust and not much else. It taught Bryson how empty and forbidding the country can be.

How inhospitable this environment is can be gleaned from how little natural life the two men encountered. An occasional saltbush or spinifex — a spiny-leaved grass — as well as a few lizards and kangaroo corpses was pretty much all they saw.

White Cliffs, a settlement with a population of only 80 people, is nestled in the heart of this region. Its name refers to the pair of sun-bleached hills inhabitants excavated to construct their homes. That's a good indicator of how hot it is — you literally have to hide under a rock!

Today, there's not a great deal to do in White Cliffs. The settlement consists of little more than a pub, a gas station — which doubles as a café and grocery store — a laundrette and an opal store.

The latter is a reminder of the place's boomtown past. At its height, over 4,500 people lived in White Cliffs — it even had its own hospital and newspaper. People flocked there hoping to make their fortunes mining opal — an Australian gemstone. Those who didn't work in the mines tended sheep stations.

White Cliff's fortunes changed in the 1890s after the area was hit by a massive drought. The town withered and its residents began to move on.

Back on the train, Bryson and his companion crossed the Nullarbor Plain. That might sound like an odd name, but it's actually pretty fitting. "Nullarbor" means "no tree" in Latin, which is an excellent description of this desolate landscape. Red soil stretches for hundreds of miles in every direction, interrupted only by the occasional bush or boulder. To grasp just how massive the plain is, imagine a desert four times the size of Belgium!

### 4. James Cook is often hailed as Australia’s discoverer, but he wasn’t the first there by a long shot. 

Australia's indigenous people, the Aborigines, first arrived on the continent thousands of centuries ago. Current evidence suggests that they've been there for around 45,000 to 60,000 years.

Europeans, by comparison, are recent arrivals. They "discovered" Australia only a few centuries ago after roaming the Pacific in search of the fabled southern continent they called _Terra Australis Incognita_ — the "unknown land of the south." But despite being so vast, it was a difficult place to find. A couple of European explorers came close but ended up missing the huge continent entirely.

In 1606, a Spanish explorer named Luís Vaez de Torres voyaged across the Pacific from South America. Incredibly, he managed to navigate the waters between New Guinea and Australia without once spotting the latter. Talk about threading the needle!

The Dutch seafarer Abel Tasman achieved a similar feat in 1642. Sailing up the southern coast of Australia for an incredible 2,000 miles, he failed to sight the massive landmass just over the horizon. Eventually, he stumbled across the island that would later be named after him — Tasmania.

But the verdict is still out on who got there first.

In 1916, two Portuguese cannons dating back to 1526 were found on Carronade Island. Their discovery suggested the presence of a European landing party, but little else is known about who they might've been.

The mantle of discoverer fell to British navigator and explorer James Cook. In 1770, he arrived in Australia aboard the research vessel _HMS Endeavour_, and his three-year stint there would go down in history as a fantastic success.

Cook's crew were the first to round New Zealand, falsifying Tasman's theory that it was connected to Australia. The ship's devoted botanist Joseph Banks also contributed his fair share — collecting around 30,000 specimens during his time in the South Seas. The haul increased the number of all known plants by 25 percent!

After sailing up the eastern coast for a couple of months, Cook laid anchor on August 21, 1770, and planted his country's flag — claiming Australia for Great Britain.

> During James Cook's voyage, his crew measured the transit of Venus in front of the sun, allowing scientists to measure how far Earth is from the sun.

### 5. In the eighteenth century, Britain started transporting its prisoners to Australia. But it wasn’t all smooth sailing. 

So what did the British do once they'd staked their claim to Australia? They started sending huge numbers of felons and working-class Brits there to build a new colony. Australia became the first country to begin life as an open-air prison.

The decision was made after Britain lost its American colonies in 1783. The country needed a new colony, and Australia seemed perfect. The decision was soon made to begin shipping "undesirables" — mostly convicted criminals and members of its underclass — to the continent.

The first vessels bound for Australia departed from Portsmouth in May 1787. Consisting of eleven ships under the direction of Captain Arthur Phillip, the First Fleet, as it was known, transported around 1,500 convicts. They were in for a rude awakening once they arrived at the new colony. Cook had described a lush and verdant island ripe for settlement, but that's not what they found.

The mismatch between reality and expectation was simple to explain: Cook had seen the country during the wet season, which explained the "natural meadows" he'd described. After anchoring in Botany Bay — today part of Sydney — those aboard the First Fleet were confronted by marsh and sand upon disembarking, eight months after setting sail. No wonder — they'd arrived in the hot and dry month of December.

Looking for greener pastures, they decided to sail further up the coast, eventually stopping on January 26, 1788, in what became known as Sydney's Circular Quay. The date is celebrated today as Australia Day.

But life wasn't easy, even after they'd set foot on dry land.

One problem was equipment. The ships hadn't been loaded with the essential tools necessary for building a new settlement. Mortars for building and plows for tilling the soil were in short supply. Skill was another issue. London's advice that skilled craftsman should be among the first to arrive hadn't been heeded.

That meant no one knew much about the natural sciences or animal husbandry, let alone the specifics needed to cope in such a tough environment. Vital know-how was also missing when it came to building the planned government farm, as only five people had an inkling of how to go about the task.

Eventually, the farm was built, but the whole matter would've been considerably easier if the ships' "passengers" had been chosen more wisely!

In the next blink, we'll take a look at what became of the city built on this site — Sydney.

### 6. Sydney is a charming city with plenty of attractions, but it’s ashamed of its humble origins. 

The second leg of Bryson's tour of Australia saw him exploring Sydney, the capital of New South Wales. He was in for some surprises.

What struck him most was the city's denial of its origins. It really doesn't like the idea that it started out as a prison camp.

The Circular Quay at the historic heart of the city was where Captain Phillip and his convict crew first set foot on the continent in 1788. Today, there are plenty of amazing sights — Harbour Bridge, the famous Opera House and Luna Park are all within spitting distance.

But there's one thing missing. When Bryson walked around the area, he couldn't find a single monument to the First Fleet's crew and passengers.

You'd think major museums, like the Museum of Sydney and the National Maritime Museum, might make good on that omission, right? Yet even there you won't find a direct mention of the fact that the city's first residents were convicts. All that's said is that they faced great hardship after disembarking.

It's part of a wider culture of denial. The Australian writer, critic and documentarian Robert Hughes notes in his book _The Fatal Shore_ that this part of the country's history wasn't even taught in schools until the 1960s. Sydney's origins were — and still are — a source of embarrassment. Many Australians simply aren't comfortable talking about this chapter of their history.

That said, Bryson had a great time in Sydney. His personal highlight was the Harbour Bridge — his favorite attraction in the whole city.

Sydney Harbour Bridge is a real feat of engineering. The arch alone weighs 30,000 tons, while at over ten stories tall and 1,650 feet long, it's the second-longest single-arch bridge in the world. New York's mighty Bayonne Bridge is a mere 0.121 percent longer.

But there were other sights to be savored, too. Sydney is a bustling, industrious place, yet head to the harbor and you'll find quaint ferries straight out of the old world. It's also blessed by wonderful, sunny weather. Perhaps that's the secret behind the happy locals and cozy neighborhoods.

### 7. The nineteenth century gold rushes transformed Australia, helping to unify the nation. 

After exploring Sydney, Bryson headed to Canberra, the capital. In a country as huge as Australia, it's only a relatively short 180-mile hop south.

It's a storied route that passes through the country's old gold mining hub. The industry really took off in the 1850s — a turning point in Australian history.

A young Sydneysider called Edward Hargraves kicked off the first gold rush. Like many others, he'd caught the gold bug, and took to the seas in search of precious metal in the hills of California. He stayed for two years but found little more than dirt and rocks.

Noticing that the Californian countryside looked a lot like that of New South Wales, Hargreaves returned to Australia and started digging just outside the towns of Bathurst and Orange. It was there that he struck it lucky.

More gold was soon found in Victoria, the colony next to New South Wales. Gold fever gripped the world, as men abandoned their homes, wives and jobs to seek their fortunes down under.

A decade had barely passed before some 600,000 new arrivals had landed in the country. Australia's population doubled virtually overnight.

The gold rush also changed the way the British thought about their colony. Sending criminals there no longer seemed like a punishment — in fact, it was like buying lottery tickets for convicts! The last prisoner transport arrived in 1868.

Gold did more than change individuals' fortunes — it also changed those of the country. No longer viewed as a vast open-air prison, Australia was now on the path to becoming a nation.

That's why Australians decided to federate the previously separate six colonies.

Before their union, these colonies had been entirely self-sufficient. New South Wales, Victoria, South Australia, Western Australia, Tasmania and Queensland all managed their own taxation systems and even set their clocks differently.

The results were often bizarre. A pub owner in Victoria, for example, had to pay as much import duty on beer from neighboring New South Wales as he did on European beer.

It was inconveniences such as these which led the territories to discuss whether they wanted to join together to form their own nation. Talks on the topic began in 1891. New Zealand was initially part of the deliberations but eventually opted out. Ten years later, a deal had been struck. On January 1, 1901, the Commonwealth of Australia was born.

Next, let's take a look at the commonwealth's new capital, Canberra.

### 8. Canberra isn’t easy to reach and Bryson didn’t think the effort to get there was worth it. 

So why did Australia choose Canberra as its new capital over prominent and well-established cities like Melbourne or Sydney?

Well, splitting the difference was just too difficult. In the end, the country decided it'd be easier to build a new city.

Canberra was constructed in the Australian _bush_. In Aussie terms, that means the countryside, as opposed to the _outback_, which is even more remote. The chosen site in New South Wales was a 900-square-mile plot of pastoral land inhabited by farming communities. Today, Canberra is the sixth largest city in Australia.

But despite growing up quickly, it's still an out-of-the-way and isolated city.

That's because it's a good 40 miles away from the Hume Highway that connects Sydney and Melbourne. If you take the main road out of Canberra going south, you won't end up anywhere particularly noteworthy. Heading west is also inadvisable. Unless you take a massive detour, you're stuck with dirt roads starting in Tumut.

Once Bryson finally got to Canberra, he was deeply disappointed.

What struck him most was how deserted the city was. Most of it consisted of straight lines and oddly empty yet green spaces. People were few and far between. The whole place seemed like one large suburb.

He didn't manage to find a proper restaurant or pub during his entire stay. Even when he met a group of local kids, they couldn't point him toward an establishment that wasn't a massive chain like McDonald's or Pizza Hut. And when he returned to his hotel, he found the bar empty.

You can see why he came to sympathize with former Australian prime minister John Howard. Howard was so unimpressed by the place that he broke with custom and decided to live in Sydney instead. The hassle of commuting was nothing compared to living in Canberra!

### 9. On the third leg of his Australian adventure, Bryson saw the Great Barrier Reef and found out about the deadly box jellyfish. 

Bryson teamed up with pal and television producer Allan Sherwin for the third leg of his tour of Australia. Together, they flew to Cairns in Far North Queensland, which would be their base for a string of adventures in the country's desert regions.

Walking along a beach, the two travelers noticed that no one was swimming. There was a good reason for that — it was box jellyfish season!

Queensland's beaches are transformed from balmy bathing spots into natural hazards every year between October and May as box jellyfish head inshore to breed. That's a problem for swimmers, as any contact with their venomous stingers results in agonizing pain and potential death.

Take it from a man who ignored warning signs and took a dip at Holloways Beach in 1992. While waving to his friends who'd wisely remained on land, he suddenly let out a series of inhuman screams. He was still screaming as he waded back onto the beach before fainting.

The reason was soon clear. His entire body was covered in whip-like slashes — the marks left behind by the jellyfish's tentacles. The pain was so great that he continued screaming even after he'd been sedated with morphine.

But not all of Australia's aquatic inhabitants are as dangerous as the box jellyfish.

One of the highlights of Bryson and Sherwin's time in Queensland was the vast technicolor extravaganza that is the Great Barrier Reef.

The reef's exact size is disputed. The area it covers could be anything from 175,000 to 214,000 square miles. One thing's certain, however — it's _big_. Bigger, in fact, than Italy, Great Britain or the state of Kansas! Small wonder, then, that it draws more than two million tourists a year.

The best way to see this natural wonder is up close. The pair took a ferry from Port Douglas, a town about 20 miles north of Cairns, reaching Agincourt Reef an hour-and-a-half later. It was there that they got a glimpse of the incredible vitality of the reef and its inhabitants.

It's an impressive array of wildlife, with more than 1,500 species of fish and 400 types of coral calling the reef home.

Bryson didn't spend much time in the water himself, though. He prefered instead to join a group of tourists on a semi-submersible boat with an underwater viewing platform. That gave him a good opportunity to observe the sea creatures below, which included butterflyfish, damselfish, parrotfish and harlequin tuskfish, as well as clams, starfish, coral canyons and a turtle.

> _"No matter how much you read about the special nature of the Barrier Reef, nothing really prepares you for the sight of it."_

### 10. The city of Darwin is a little drab, but the enormous emptiness of the Northern Territory makes up for it. 

Next on Bryson and Sherwin's itinerary was the Northern Territory.

Tucked between Queensland and Western Australia, this vast area was confirmed to be a territory by a referendum in 1998. The territory's inhabitants were offered the chance to become Australia's seventh state. But the locals, known as Territorians, weren't having any of it. They seem to like being outsiders.

That said, the Northern Territory is anything but marginal in the country's geography. Covering a staggering 523,000 square miles, it accounts for around a fifth of Australia's landmass!

The travelers' first port of call was the regional capital, Darwin. Sadly, they didn't rate it highly.

Bryson had dreamed of a tropical town with potted palms, verandas, cool drinks and Panama hats. But the reality was less exotic.

That's partly because the city had a rough recent history. For starters, the Japanese air force bombed it during the Second World War. Then came Cyclone Tracy in 1974, whose 160-mph winds flattened 9,000 houses and killed 60 locals.

All that drama left little of the city's historic heart intact. It's mostly new and has had little chance to develop the charm and quirks that come with age.

Leaving drab Darwin behind, Bryson and Sherwin headed for the town of Alice Springs — a 919-mile drive through a seemingly endless red desert.

The road to the town is known as Stuart Highway. Named after the nineteenth-century explorer John McDouall Stuart — who tried to traverse the country from north to south several times — the road takes you through a ferociously hot region with almost no vegetation. You're in trouble if you get stuck, as there's neither water nor shade for hundreds of miles.

Stuart and his contemporaries learned the hard way. In his memoirs, Stuart's travel companion Ernest Giles wrote that the lack of water drove his horse mad. One evening, it stuck its head into the campfire in search of relief!

### 11. Alice Springs is a surprisingly lively town, and “nearby” Uluru a stunning sight. 

The drive to Alice Springs took Bryson and Sherwin two days. Once they finally got there, they found a surprisingly lively town.

That's hardly a given for a place situated right in the middle of Australia. It started life as a much smaller settlement. In the 1950s, it had a population of just 4,000 people. Since then it's boomed and now has around 25,000 permanent residents, while 350,000 tourists make the trek there every year.

The town is packed with hotels, camping grounds, malls, conference centers, banks, car dealerships and American fast food outlets like McDonald's and KFC.

So what do you do if you find yourself in this thriving, out-of-the-way hotspot? One thing you can't miss is Ayers Rock — or to give it its official name, Uluru.

A majestic sandstone rock just over 300 miles southwest of Alice Springs, Uluru is the world's largest monolith. It's classed as a _bornhardt_ — a weather-resistant rock formation that remains unchanged even as the landscape around it changes over the centuries. And resist the weather it has — for 100 million years!

Bryson and Sherwin headed out to see the natural wonder. It was every bit as impressive as people claim. The scale alone is transfixing. It measures 1,150 feet in height, 1.5 miles in length, and has a circumference of 5.5 miles.

Unfortunately, all the hotels in Yulara — the nearest resort town — were fully booked, meaning the two travelers had to return to Alice Springs that same evening and weren't able to study the rock as closely as they'd hoped. But even their limited time at the foot of the great rock left an indelible impression in their memories.

The journey had been worth it. They arrived back in Alice Springs feeling happy and content — not least because of the sheer number of miles they'd put behind them!

### 12. Australia has a sinister past when it comes to the treatment of its Aboriginal people, the repercussions of which endure. 

While in Alice Springs, Bryson and his travel companion noticed the social divide between European-descended white Australians, and indigenous Australians — the Aboriginal people. They just didn't interact very much.

You don't have to spend much time in the country to realize that something's amiss when it comes to the way Australia's earliest inhabitants are treated.

Australia is an exceptionally prosperous and healthy place overall. But the rates of suicide, hospitalization, unemployment, imprisonment and childhood mortality are all disproportionately worse for Aborigines than the rest of the population. In some cases, they're as much as 20 times worse.

All that is reflected in one staggering statistic. As of 2000, the average life expectancy for Aborigines was an incredible _20_ _years_ shorter than that of white Australians.

So what's behind this inequality?

One reason is the horrific historical treatment of native Australians. For years the government in Canberra pursued a policy of taking Aboriginal children from their families.

Bryson first learned about this dark chapter in Australian history when he sat down with pro-Aborigine legal advocate Jim Brooks, in Cairns.

Brooks, a passionate defender of Aboriginal legal claims to land, told him about the _Stolen Generation_. It refers to a 60-year period — starting in 1919 and only ending in 1970 — during which Aboriginal children were forcibly taken from their families and dropped into state training facilities or foster homes.

The policy was ostensibly designed to "protect" the minors from poverty and hardship. Because the state was the legal custodian of all Aboriginal children by law, it could pursue this cruel strategy without having to answer to anyone.

Historians still don't know just how many Aboriginal children were stolen from their families in this way. Estimates suggest that it could have been as many as one in ten.

The results of the policy were catastrophic. Grief-related alcoholism and suicide skyrocketed among Aborigines. The stolen children, once released from "care" at the age of 16 or 17, faced two choices: either live in their new communities surrounded by prejudice or return to their Aboriginal communities.

Because they'd been taken at such an early age, many children no longer recalled or felt close to the communities into which they'd been born, meaning that many opted for the former route. In the end, they remained in Australia's cities, living hard, dysfunctional lives marked by alienation and lacking a sense of belonging.

### 13. Perth is a wonderful city, and its park and botanical garden are a great reminder of Australia’s natural fecundity. 

Bryson completed the last leg of his journey alone and wrapped up his tour with a stay in Western Australia. Flying from Alice Springs to Perth, he ended up in the far west of the western state.

Despite being incredibly remote, Bryson found it a charming and welcoming place. He could see why people thought it was a good idea to build a settlement here. The weather's great, and it's bright and sunny. People are naturally disposed to be in a good mood — smiles are part and parcel of everyday life.

One of the highlights of Bryson's stay was a stroll in Kings Park. In his view, it's one of the very best parks in the world, because it's such a vivid reminder of the country's staggering array of wildlife.

Take the number of plant species, for example. Australia is home to around 25,000 different types of flora and fauna. Britain, by contrast, has just 1,600 different species.

That might seem like a paradox considering how arid the country is, but there are factors like soil and isolation that help life to flourish there. In fact, the proliferation of plants is partly down to poor soil quality.

Most plants do well anywhere so long as the climate is temperate and the soil rich. An oak, for example, can grow as happily in Oregon as it can in Pennsylvania. Thriving in Australia is a different matter altogether. To grow in the country's poor soil, plants have to specialize.

Some trees learn how to tolerate high levels of nickel, for example. Others evolve an ability to get the most out of copper-rich earth or withstand drought.

So where does the isolation factor come in? Well, Australia has been an island for around 50 million years. Being cut off from developments elsewhere was a boon to its natural fecundity.

That's because competing species weren't a factor. The country's wildlife developed on its own and prospered. Isolation also plays an important role _within_ Australia, as individual fertile regions are largely cut off from one another by vast cordons of barren land, meaning they're incredibly sheltered.

And that was confirmation of a fact that had already taken shape in Bryson's mind: Australia really is a unique place!

### 14. Final summary 

The key message in these blinks:

**Australia is a unique, multifaceted country with breathtakingly beautiful geography, friendly people, yet a dark past of the abhorrent treatment of its indigenous people. Its land ranges from barren wildernesses devoid of life to islands of extraordinary fertility and diversity. From stunning natural wonders like Uluru and the Great Barrier Reef to storied mining towns that helped unify the nation, there's something to experience around every corner.**

Actionable advice:

**Don't pick up shells on the beach!**

If you're strolling down an Australian beach and stumble across a colorful shell, leave it be. The reason? It's likely to belong to a cone snail. If you're unlucky, the shell's inhabitant might just be at home. In that case, you can be sure it won't take kindly to being disturbed. Equipped with a barbed and highly venomous "harpoon," one sting is enough to leave you in agony. Some of these nasty creatures even pack enough venom to kill!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Notes from a Small Island_** **by Bill Bryson**

American-born author Bill Bryson wrote _Notes from a Small Island_ (1995) as he was preparing to leave the small Yorkshire village in which he'd lived for 20 years, and head back to the United States. Before departing, he decided to bid a fond adieu to his adopted country, Great Britain. This travelogue documents his farewell tour of a country whose landscape, culture, mores and wonderful eccentricities he'd come to love so dearly.
---

### Bill Bryson

Bill Bryson is an Anglo-American author best known for his witty and informative travel writing. His previous books included the acclaimed _A Walk in the Woods_ (1997) and _Notes from a Small Island_ (2015). Born in Des Moines, Iowa, Bryson spent many years living in the UK before returning to the States. He is currently based in Hanover, New Hampshire.

