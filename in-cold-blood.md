---
id: 57c9536608dc1b000317fead
slug: in-cold-blood-en
published_date: 2016-09-07T00:00:00.000+00:00
author: Truman Capote
title: In Cold Blood
subtitle: None
main_color: 991F2D
text_color: 991F2D
---

# In Cold Blood

_None_

**Truman Capote**

_In Cold Blood_ (1966) tells the true story of a multiple murder that baffled both police and the public in 1959. These blinks recount how investigators gradually unraveled a meticulous yet simple plan devised by two criminals without scruples or remorse.

---
### 1. What’s in it for me? Step behind the scenes of a brutal murder that shook a rural Kansas town in 1959. 

When American novelist and screenwriter Truman Capote first saw the news of a quadruple murder in Kansas in November 1959, he was intrigued. The murders quickly became a top news item, and Capote decided to travel to Kansas to learn more about the event firsthand.

Together with his travel companion, novelist Harper Lee, he befriended people in the small Kansas town where the gruesome murders took place.

As the investigation unfolded, Capote followed the events closely and even spoke extensively with the murderers after they had been apprehended.

So what happened that day in 1959? This is the story behind a brutal crime committed in cold blood.

In these blinks, you'll learn

  * what stockings have to do with the murder of a Kansas family;

  * how a sleepy sheriff's office became the center of a media frenzy; and

  * how a lone hitchhiker inadvertently saved the life of a traveling salesman.

### 2. In a small town in Kansas, two men went on a shopping spree and talked about the perfect crime. 

Which tools would you need to execute the perfect murder?

Maybe some carefully selected drugs? Or something as blunt as a chainsaw? The two men found guilty of the murder of a Kansas family in 1959 were very straightforward in their approach to committing their heinous crime.

This is their story.

One afternoon, a 28-year-old mechanic named Dick Hickock and a 31-year-old car painter named Perry Smith, both of them ex-convicts, went on a shopping spree for some strange items in Emporia, Kansas.

First, they stopped at a store to buy rubber gloves for Perry; Dick already had a pair of his own.

Then they visited a women's hosiery store to buy black stockings but gave up after finding the store only carried lighter shades.

After that, they sought out a shop where they could buy some sturdy rope. Perry knew a lot about ropes and knots, having been a Merchant Marine. His knowledge led him to select a white nylon cord for their purposes.

While the items these two men purchased were odd, alone they wouldn't cause suspicion. Yet if bystanders had overheard their conversation while shopping, they might have notified the authorities.

While in the stocking store, for instance, when they discovered there were no black stockings, Dick reminded Perry again that there were to be "no witnesses." Whether or not they wore stockings to obscure their faces was irrelevant.

Perry was still nervous. Dick reassured him that all would go off without a hitch. How could it not, when the plan was so perfectly devised?

Yet the men didn't know how many people might be in the house they intended to invade and rob, which made purchasing the correct length of cord difficult. The men knew the parents would be there, and two children for sure. But there might also be guests.

To be safe, they purchased 100 yards of cord, enough to tie up at least 12 people.

> _"The only sure thing is that every one of them has got to go."_ — Dick Hickock

### 3. Neighbors found a bloody scene at the house, in which family members were bound, gagged and shot. 

On Sundays, the Clutter family usually drove to church, as did most of their neighbors in the small town of Holcomb, Kansas.

But on this particular Sunday in 1959, the Clutters didn't show up. Concerned, a neighbor named Susan Kidwell phoned their house to check on the family.

When her calls went unanswered, Susan and a few other neighbors decided to take a trip to the Clutter residence to see if anything was amiss.

Susan saw that all of the family's cars were parked in front of the house. When she walked through the front door, she noticed that the family's breakfast sat untouched on the kitchen table and that a purse was strewn open on the floor.

The house was silent. The neighbors headed upstairs to the bedroom of Nancy, the Clutters' 16-year-old daughter.

A gruesome scene awaited her as she walked into the bedroom. Susan began to scream, and the neighbors who had been waiting out front rushed in to see what had happened.

In the confusion that followed, Susan thought that perhaps Nancy had suffered a serious nosebleed, but neighbor Nancy Ewalt could only repeat "she's dead," over and over again.

Susan called first responders. Shortly after, the group discovered the bodies of the rest of the Clutter family in other rooms. Each victim was bound with a cord running from wrist to ankle. Their mouths were taped shut.

Mrs. Clutter and her two children had been shot in the head. Mr. Clutter had been tied up and his throat slit, his body showing signs of physical torture. He was also shot through the head.

It was clear that a horrendous murder had taken place. But no one could understand why.

### 4. State detectives took over the murder investigation, and a media frenzy followed. 

In the Clutter family murder case, local law enforcement faced a dilemma, as there were no clues and no apparent motive.

Quickly the local sheriff's office was transformed from a sleepy place with a friendly, bubbly secretary who made great coffee to the focal point of a media frenzy over a quadruple murder.

To get things moving in the right direction, a handsome, 47-year-old Kansas Bureau of Investigation (KBI) agent named Alvin Adams Dewey was called in to lead the investigation.

The bureau had 19 skilled detectives, any of whom could ably take over a case that was too complicated for local law enforcement. And this case would certainly be a challenging one.

By the time the case _was_ finally cracked, 18 officers were working on it full-time!

At the first press conference Dewey organized, the difficulty of the task at hand was put on full display.

At the conference Dewey explained that there were four victims, and the case centered on determining which member of the family was the primary, or intended, victim. Logically speaking, the other victims would have been killed to eliminate any witnesses.

But figuring out the intended victim was proving difficult. As the coroner had been unable to determine precise times of death for the victims, investigators could only guess at the order in which the murders had been committed.

Essentially, the police had no idea how even to get started.

### 5. A “ghost” at the scene of the crime was thought to be a key suspect but turned out to be just a drifter. 

How would you react to seeing a ghost at the scene of a crime? When the Clutter's gardener saw an apparition in the family house, surprisingly he kept his calm.

Later that December after the murders, the Clutter's gardener, Paul Helm, was pruning trees in front of the empty Clutter home. Relatives were planning to sell the property.

As Paul glanced up at a window, he saw a ghostly figure drawing back the curtain and looking out into the garden.

But he couldn't be sure of what he'd seen. The fading winter light had obscured his vision, and as he shielded his eyes to cut the glare and look more closely, the curtain had dropped, and the figure was gone.

The gardener quickly headed across the fields of Holcomb to let the sheriff know someone was inside the Clutter house. Returning moments later, the sheriff, followed by KBI investigators and lead detective Dewey, were all thrilled to have stumbled upon a lead in this baffling case.

Just as the armed men were about to burst through the Clutters' front door, a man walked out.

Ungroomed and wild-looking, the man was in his mid-thirties and carried a holstered pistol at his hip. Police searched his car and found a 12-gauge shotgun, along with a hunting knife in the trunk. With this "evidence," investigators felt that they had potentially found the killer.

The man's name was Jonathan Adrian, and police questioned him for several weeks. In the end, they concluded that their key suspect was nothing more than a curious trespasser.

### 6. A radio program reminded a prison inmate of a conversation that inspired the Kansas crime. 

Luckily for the police, there was one person who could give them a clue as to what took place in the Clutter household that Sunday in 1959.

The mystery that police couldn't seem to solve was finally unraveled from within the Kansas State Penitentiary, of all places!

As they weren't making any progress on the case, investigators decided to reach out to the public. On Tuesday, November 17, police shared details of the case, including the victims' names, on the radio following the evening news.

An inmate named Floyd Wells was listening to the radio that evening, and immediately recognized the Clutter name.

Floyd also recognized the descriptions of the people who had been tied up, gagged and shot. Chillingly, he realized that he knew who had committed those grisly murders.

Floyd had once shared a jail cell for a month with Dick Hickock.

He had told Dick about Mr. Clutter, for whom he'd once worked as a farmhand. Floyd told Dick that Mr. Clutter was a good employer and importantly, a rich man.

Dick asked Floyd to tell him everything he knew about the Clutters, and Floyd mentioned that Mr. Clutter kept his money in a safe.

Floyd knew he should share the information he had, but he was afraid.

Snitching on a former inmate is tricky business. In prison, talking with the warden is considered high treason and could lead to trouble if other inmates hear about it.

Floyd spoke with a fellow inmate, who happened to be religious, about what he knew. The inmate told him he should tell the truth, but do so unnoticed.

The two men managed to convince the deputy warden to send Floyd to speak to the warden on some innocuous pretext. Only then could Floyd divulge his knowledge, and no other inmates would know.

### 7. Perry was a complicated person with a checkered past, whom Dick saw as a “natural killer.” 

So if Dick was the one who learned of the Clutter family's riches, how exactly did Perry become involved in the scheme?

The duo knew each other from their time as cellmates at the Kansas State Penitentiary. Initially, Dick didn't have a particularly high opinion of Perry, but this changed when he heard how Perry had beaten a man to death with a bicycle chain.

This story impressed Dick and quickly convinced him that he had found "a natural killer" — a sane person totally lacking a conscience. He believed that he could exploit this trait in Perry for his own profit.

As soon as they were both out of prison, Dick invited Perry to come to Kansas to pull off "the perfect score."

But in fact, Perry's murderous past was pure fiction. He had made up the story to win Dick's friendship and respect.

While in prison, Perry had also become close with an inmate named Willie-Jay, the prison chaplain's clerk. Having been abused as a child, Perry craved friendship and was flattered that Willie-Jay seemed to see artistic potential in him. Perry came to admire Willie-Jay deeply, considering him a kindred spirit.

Even though Dick had asked Perry to come to Kansas, Perry actually took the trip to see Willie-Jay, who he knew was set to be paroled.

But when Perry arrived, he discovered that Willie-Jay had left town just five hours earlier. With nothing else to do, he decided to meet Dick and then agreed to take part in the Clutter heist.

After the crime, the killers fled the country. Perry had always dreamed of diving for treasure off the coast of Mexico, and though doubtful of the wisdom of the plan, Dick indulged Perry in his naïve fantasies.

Perry followed news of the case as they traveled south, yet never seemed to show any sign of actual remorse. When reading about the large funeral that was held for the Clutter family, for example, Perry's only reaction was to marvel at the size and cost of such an event.

### 8. A traveling salesman offered a ride to the two murderers in Nebraska but was saved by a twist of fate. 

Being a traveling salesman isn't necessarily an exciting job, but that depends on the people you pick up while on the road. For instance, your next hitchhiker might be an outlaw!

This is exactly what happened to Mr. Bell, a traveling salesman on his way to Omaha, Nebraska.

At the tail end of a long trip, Mr. Bell saw two hitchhikers standing by the side of the road. They seemed nice enough, and he was happy to have company for the drive. He let the two men into his car.

The hitchhikers introduced themselves as Dick and Perry, and Mr. Bell said he'd take them as far as Omaha. But what he didn't know was that his new travel partners were criminals on the run and that they intended to make Mr. Bell their next victim.

The plan was that Perry would strangle Mr. Bell with a belt. Then he and Dick would hide the body in a prairie grave and make off with the victim's car and money.

Fate intervened, however, and Mr. Bell was spared a violent death.

Curiously, Mr. Bell and Dick hit it off. Sitting in the front of the car, Dick exchanged vulgar jokes with Mr. Bell and the two laughed together. All the while, Perry sat impatiently in the backseat and waited for his signal to strike.

The men had decided that when Dick said, "Perry, pass me a match," Perry would strangle Mr. Bell with his belt while Dick grabbed the steering wheel.

But Dick was enjoying joking around too much. When he was finally ready to signal to Perry, Mr. Bell spotted another man looking for a ride and pulled the car over.

With another person in the car, the murderers had to abandon their plan.

### 9. The murderers were finally cornered after making the mistake of returning to Kansas for a con job. 

It's often said that when hunting criminals, you just need to wait for them to make a mistake.

In December 1959, the murderous duo made the seemingly reckless move of returning to Kansas, their home state and the place where they'd committed the crime. They returned to Dick's old habit of con jobs, which had occupied him before the Clutter murders.

Dick was a smooth talker, and as part of a job, would quickly gain the trust of a shopkeeper before making a series of expensive purchases and then pretending to have forgotten his wallet. He would then convince the shopkeeper to accept a blank check, that, of course, would never clear.

During one con, however, the shop owner grew suspicious and took down the license plate number of the Chevrolet in which the two men drove off. He called the police to describe both Perry and Dick.

Finally, authorities had a lead in the Clutter murders.

Police tracked down the duo to Las Vegas, and on December 30, 1959, two police officers, recognizing the Chevrolet and finding a license plate match, took the two men into custody.

But proving Perry and Dick guilty of the Clutter murders, with so little evidence, would be no easy task.

Detective Dewey flew to Las Vegas to interrogate the suspects. He focused his attention on Dick, the more talkative of the two men. To scare him, Dewey pointed out the existence of an indirect witness to Dick's criminal intentions, inmate Floyd Wells.

But even more incriminating were two footprints found at the scene of the crime, prints which perfectly matched the boots that Dick and Perry were still wearing when they were arrested.

It wasn't long before Dick admitted that he'd been an accomplice to the murders that Perry committed.

A Kansas jury deliberated for just 45 minutes before condemning the two men to death, and in 1965, after a long stint on death row, both men were executed by hanging.

### 10. Upon learning that there was no safe stashed in the Clutter home, no riches to steal, Perry started the killing spree. 

So what happened on that fateful Sunday in Holcomb, Kansas?

After Dick had admitted to investigators his complicity in the killings, Perry offered an astonishingly detailed confession of the events of their killing spree.

Dick and Perry had arrived at the Clutter's home a little after midnight and snuck into the house through an unlocked side door.

They quietly searched the house for the safe that Dick's former cellmate Floyd Wells had claimed would be there. Yet there was no safe.

Undeterred, the two men shook Mr. Clutter awake, but he told them calmly that he didn't have a safe or even much money in the house.

Dick became angry, yelling and threatening Mr. Clutter with a knife. Perry and Dick then gathered up the rest of the family at gunpoint, moving each person to a different room of the house.

Perry led Mr. Clutter and his son to the basement. Before leaving them there bound and gagged, he took care to ensure they were comfortable — even placing a pillow under the son's head. When he returned upstairs, he found Dick preparing to rape the daughter, and sternly told him to leave her alone.

With the situation seemingly under control, Dick and Perry pondered what to do next. Surprisingly, Dick was hesitant to go through with his plan to not leave any witnesses, which annoyed Perry.

Having previously admired Dick's bravado, Perry claimed his next move was meant to "call Dick's bluff" and get him to admit he was a coward.

He asked Dick for the knife and slit Mr. Clutter's throat. At that point, Mr. Clutter seemed to understand what was in store for his family and began struggling furiously, almost freeing himself until Perry killed him with a shotgun blast to the head.

One by one, the other family members were also shot, though in his confession Perry was ambiguous about who killed Nancy and Mrs. Clutter.

As the murderers walked to their car having pocketed a few dollars in cash, a pair of binoculars and a small portable radio, Perry realized that he should kill Dick, too. After all, there could be no witnesses.

For whatever reason though, he didn't shoot his partner. The two got into the car and drove off into the night.

### 11. Final summary 

The key message in this book:

**The motive for a cold-blooded murder can be at once obvious and obscure, making the success of investigators dependent on the random mistakes of a criminal mind.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Poisoner's Handbook_** **by Deborah Blum**

_The Poisoner's Handbook_ details the work of New York City's first scientifically trained medical examiner, Charles Norris, and his partner, Alexander Gettler, the city's first toxicologist. It offers an insider's view of how forensic science really works by walking readers through their investigations into notorious and mysterious poisonings.
---

### Truman Capote

Truman Capote was one of the most prominent postwar American authors. Born in New Orleans in 1924, he dropped out of school at 15 and traveled to New York, where he got a job with _The_ _New Yorker_. He went on to become a celebrated writer of both fiction and nonfiction books.

