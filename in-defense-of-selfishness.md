---
id: 55ed51a211ad1a0009000099
slug: in-defense-of-selfishness-en
published_date: 2015-09-11T00:00:00.000+00:00
author: Peter Schwartz
title: In Defense of Selfishness
subtitle: Why the Code of Self-Sacrifice is Unjust and Destructive
main_color: F0D080
text_color: 70613C
---

# In Defense of Selfishness

_Why the Code of Self-Sacrifice is Unjust and Destructive_

**Peter Schwartz**

_In Defense of Selfishness_ (2015) exposes the dark side of an attribute most of us assume to be good: altruism. It explains why, despite common misconception, altruism is harmful, devaluing both individuals and societies at large — and why selfishness is the alternative that can provide us with liberation.

---
### 1. What’s in it for me? Discover why selfishness is better than altruism. 

Have you ever given something up, even though you really wanted it, so that someone else could have it? Sure you have. But did you ever stop to consider why you did that? Why did you give up something you wanted to make sure someone else got what _they_ wanted? Because it felt good? Because it would give good karma?

This kind of self-sacrifice might seem innocuous enough, but, as these blinks will show you, the idea of altruism, universally hailed as a moral one, is not only false, but irrational and dangerous, too. A society based on altruism can lead to totalitarianism and the loss of the only freedom that counts: the freedom of the individual.

So what's the alternative? Selfishness! Note that this does not mean the amoral, predatory behavior of criminals like Bernie Madoff, but rather leading a principled, honest and self-respecting life based on voluntary exchange, where neither party has to sacrifice themselves. These blinks explain more fully what selfishness really is and why it's better than altruism.

In these blinks, you'll learn

  * why discrimination is sometimes a good thing;

  * why selfishness is more rational than altruism; and

  * why altruism leads to a society of mindless yahoos.

### 2. Altruism is a harmful doctrine that propagates self-sacrifice and subordination. 

Does altruism pave the way to honesty and integrity? Can it bring about equality and social harmony? If you answered yes to either of those questions, you're wrong!

Nearly everyone assumes that it's moral to put others before yourself; even people who choose to behave unethically usually don't question the theoretical value of altruism. However, the real consequences of altruism are rarely investigated. 

Altruism is about subordinating yourself, because it requires servitude, not benevolence. Generously helping a victim of misfortune is not enough; rather, this doctrine of self-sacrifice claims you owe an unchosen debt to which the recipient is morally _entitled_. 

Altruism requires you to put other people's interests before your own and suggests you have no moral right to exist for your own sake. So, to be altruistic, you must be prepared to sacrifice your wealth, goals and interests.

In other words, altruism effectively takes away your possessions, money, time and possibly even your life. Those things are no longer yours, as they now belong to the larger group or society. You surrender yourself for the good of the group; effectively, you're now serving those who have less. 

A recent proposal regarding airline safety in the US illustrates the illogical nature of altruism. Many airlines forbid blind people from sitting in emergency rows; this policy, however, was opposed by the New York chapter of the National Federation of the Blind as discriminatory. The NFB proposed blind people be allowed to sit in the emergency rows. This proposal even turned into a Senate bill, which would endanger everyone on the plane in the name of equality — or, in other words, altruism. 

In a purely altruistic view, there are only two kinds of people: those who have needs and those who can ensure that those needs are met. That's why governments constantly sap money from those who are able to provide and give it to those who aren't. Governments often subsidize transportation or farming, for instance. People accept these policies because of their altruistic nature, not because of their moral righteousness.

### 3. Altruism does not protect the public interest, though it's purportedly founded upon it. 

At some point in your life, you've probably heard the argument that, in the name of public interest, it's sometimes good to subordinate yourself. But is this true?

No. The concept of public interest is a myth.

Proponents of altruism argue that society benefits from personal sacrifice. But this idea is false. Why would it be in the interest of the public to build a park, but not a shopping mall? Those in favor of the park would say that a park merely requires government funding because it doesn't have an entry fee; however, _someone_ still has to pay for it. Citizens pay for the park with their taxes even if they don't use it and would rather have the shopping mall.

The concept of altruism ultimately leads us to collectivism. Modern states are founded on the Hegelian idea that the state has supreme rights. The collective nature of society becomes part of your identity, as it is from your membership in the collective group that your identity derives.

And any form of collectivism ultimately makes you dependent on the society you're a part of, which effectively renders you helpless as an individual. When a member of a collective society accepts his position as nothing more than a small piece of a whole, he's more likely to blindly follow orders. In the direst scenario, collectivism leads to totalitarianism. 

States care for individuals in the same way that farmers care for their chickens or pigs. Farmers tend to the animals because the animals produce goods — ham, eggs — that benefit the farmer. You serve the same function for the state: you're a means of income. The state stands to profit by expending your labor, your time or even your life.

### 4. Altruism forces you to surrender your own mind. 

No one can take away your thoughts, right? Wrong — if you adhere to altruism!

The altruistic doctrine doesn't only control your sense of morality; it also tells you what to hold true. 

A person who lives a fully selfless life no longer has her own convictions. Altruism preaches that disagreement is bad. It's not only your actions that make you a servant to the group; you have to obey with your thoughts, too. 

This leads to people in an altruistic society believing what they're told. Two times two, in such societies, needn't equal four! In fact, Hermann Goering, the founder of the Nazi Gestapo, defended Hitler's unusual economic policy by saying, "I tell you, if the Fuhrer wishes it, then two times two is five."

This can push people into believing that even their lives should be handed over to the group. Selflessness isn't merely a means to an end — it can be an end in and of itself. That's what motivates suicide bombers to sacrifice their lives.

And giving yourself up can even take away your ability to love.

You can't love someone unless you have your own identity, and altruism robs you of that. Altruism says that you can't think of yourself as important. Love, however, isn't a selfless act!

The concepts of selflessness and public interest will also lead societies to value the redistribution of money or other goods. This means that the best parts of society have to be watered down to match the worst. If some people aren't beautiful, no one can be beautiful. The highest goal of altruism is to achieve a complete lack of selfishness. 

The greatest irony of altruism is that it doesn't even help those it purports to help. It only makes them dependent on others and conditions them to paternalism.

### 5. The widespread acceptance of altruism is based on a misunderstanding of selfishness. 

Even those who oppose altruism usually don't consider "selfishness" a viable alternative. Selfishness is bad, right?

Well, no — selfishness is not inherently harmful or immoral. We assume this only because we misunderstand the concept itself.

Selfishness has long been demonized. When you hear the term, you might think of a man cheating on his wife or a thief robbing a house — moral personifications of selfishness. That's not what selfishness really means, however. 

Proponents of altruism say that selfishness, by definition, harms others. That idea was propagated by Nietzsche, who described selfishness as rude and impulsive. But selfishness isn't simply a blithe attempt to satisfy desire. After all, there are those whose sole desire is to take drugs or commit serial murders. True selfishness, in contrast, is founded on reason.

Humans use reason — rationality and logic — to determine what's good for them. Animals, on the other hand, merely try to survive, satisfying each need as it arises. Being in animalistic thrall to the moment can't properly be called selfishness.

So selfishness must be a rational act. Selfishness doesn't push us to hurt others — because it's more rational to treat them well. Trading is much better for you than robbing, because robbing someone might get you killed. Your selfish desire to _not_ be killed would naturally motivate you to work peacefully with others. So it's actually selfishness that motivates people to operate through voluntary exchange with others, where both parties are satisfied.

If you're selfish, you regard your individual values, ideas and life as precious. Such things should be embraced and protected, not surrendered, and you should stay loyal to yourself at all costs. Similarly, you should not demand or require that others sacrifice their own values or life for you.

Even love is selfish. You form relationships with those who are important to you. It would be absurd to be with someone you don't like just because it makes _them_ happy. While an altruist might regard such amorous martyrdom as a virtue, a selfish person would call it downright irrationality.

So if you don't want to be a slave to the needs of others, selfishness is the doctrine to follow.

### 6. Selfishness means fully committing yourself to rational and life-affirming thought. 

Do you think you'll live well and prosper by giving to others and making sacrifices? Well, you won't!

Long-term happiness and survival can't be based on altruistic values. The only way to achieve those things is through selfishness.

Imagine, for example, that you're very thirsty, and someone gives you a poisonous drink. Would you still drink it to quench your thirst? Clearly not. The drink is poisonous and there's nothing you can do about that. It isn't some sort of logic puzzle you can reason your way out of — you have to accept that the drink is a threat. A selfish person, valuing survival, would dash the drink to the ground.

There's really no middle ground between selfishness and altruism. Selfishness is about staying fully committed to rational thinking, so there's no way to be selectively selfish. It's a lifestyle you need to adapt to. And don't feel guilty about being selfish — it's something to be proud of. Be proud that you've committed yourself to a belief system that's life-affirming.

Altruism, on the other hand, neglects life. If you give your kidney to a stranger, for instance, you might be preventing your own child from getting it at some point down the line, when they might need one from a close relative. So selfishness, in this case, would guarantee your family's survival.

In an altruistic view, you don't judge people for what they really are. You show mercy to those who might not show mercy themselves. Altruism isn't founded on fact — it's founded on compassion, but compassion alone is neither rational nor life-affirming.

Ultimately, altruism isn't tied to your personal concerns, so it shouldn't be a guiding principle for your life. Selfishness is the logical route. But what would a society founded on selfishness actually look like?

### 7. Selfishness results in functional capitalism; altruism results in unproductive slavery. 

What does the term "capitalism" make you think of? What about "freedom?" Those ideas are both based on selfishness — not altruism.

Freedom is a fundamental concept, but the idea of a government that grants you freedom is incompatible with altruism.

Freedom and reason are inextricably linked. That's why the United States was founded on the idea that humans are rational beings. A society that values individuals and their rights doesn't have room for masters or servants, so it doesn't have room for altruism, either.

Capitalism regards the rights of the individual as absolute. An individual's property or livelihood can't be appropriated in the name of the collective good.

A free and capitalist society requires a republic, not a democratic government. The Founding Fathers envisioned a United States based on limited state rights, rather than majority rule, so as to ensure freedom. Democracy can be dangerous. After all, it allowed Hitler to come to power. No government should have as much control over its citizens as Nazi Germany once had.

In a capitalist society, public transportation, parks and libraries wouldn't be owned and controlled by the state. Such institutions would be private, ensuring that only those who want to use them have to pay for them.

An altruistic society, on the other hand, would be highly unproductive and, in the worst case, it could result in slavery. In an altruistic view, all humans are interchangeable. _That_ kind of equality is not something to be valued at all.

An altruistic country makes everyone dependent on state welfare and robs them of their independent thinking. It is founded on paternalism and control, and unconcerned with productivity.

Only a society that embraces selfishness can truly succeed in the world.

### 8. Final summary 

The key message in this book:

**Though most people take the value of altruism for granted, it's really a destructive doctrine that robs society of its productivity and individuals of their livelihood and critical thinking. Selfishness, on the other hand, can ensure our happiness, safety, well-being and the functionality of our state. Selfishness is not something to be ashamed of — it's a rational mode of thought that praises reason, good will and even love.**

Actionable advice:

**Praise the players, not the team.**

The next time your favorite sports team secures a victory, consider the players that made it possible. You've probably heard the phrase, "It's the team that wins, not the star," but in reality, victory wouldn't be possible without the few who have particular skills. There's nothing immoral about giving credit where credit's due. 

**Suggested** **further** **reading:** ** _The Righteous Mind_** **by Jonathan Haidt**

_The Righteous Mind_ explores how moral decisions are made, concluding that moral judgments stem from intuitions, not logic. The author draws on his background in social psychology and 25 years of groundbreaking research to explain how morality both binds us and divides us and how religion and politics create conflicting communities of shared morality.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Peter Schwartz

Peter Schwartz is retired Chairman of the Board of Directors and current Distinguished Fellow of the Ayn Rand Institute. _In Defense of Selfishness_ is his third book.

