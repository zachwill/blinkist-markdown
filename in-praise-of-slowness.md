---
id: 565c83249906750007000070
slug: in-praise-of-slowness-en
published_date: 2015-12-04T00:00:00.000+00:00
author: Carl Honoré
title: In Praise of Slowness
subtitle: Challenging the Cult Of Speed
main_color: F4318A
text_color: C2276D
---

# In Praise of Slowness

_Challenging the Cult Of Speed_

**Carl Honoré**

_In Praise of Slowness_ (2005) offers both an indictment of and an alternative to the high-speed lifestyle that plagues many people today. It examines how the rat race impacts our minds, bodies and souls — and offers concrete tips on how to slow things down.

---
### 1. What’s in it for me? Escape the dictatorship of the stopwatch. 

A revolution is taking place. It's beginnings are rather tame. But maybe you've noticed that hip new slow food restaurant next to your favorite bookstore? That restaurant is just a tiny part of a growing international movement. Right now, more and more intelligent people are joining the _Slow Movement_, and saying an emphatic NO to the continuous hurry we're constantly in. In these blinks, you'll find out how the new movement is quietly transforming society — and what benefits a slower lifestyle may hold for you.

You'll also learn

  * what's so good about thinking more slowly;

  * why our leisure time has become such a serious matter; and 

  * how the Slow Movement can improve your sex life.

### 2. First we chopped time into pieces and now time is chopping our lives into pieces – if we let it. 

What's the first thing you do upon waking up? Do you pet your cat? Or open the blinds to check the weather? Nope. You look at the clock to check the time.

People have always tracked time in some way or another. As far back as the Ice Age, hunters counted the days between lunar phases, carving notches into sticks and bones. Moreover, all great ancient civilizations, like the Egyptians and the Chinese, had their own calendars.

After our ancestors learned to measure the years, months and days, they began to chop time into smaller units, like the hours of the day. The first instrument to divide the day into equal parts was an Egyptian sundial from 1500 BC.

Later, in the course of the Industrial Revolution, we became slaves to our schedules.

For the greater part of human history, life and work moved slowly, limited by the speed of our bodies and the animals we used in industry and agriculture. But since the Industrial Revolution, machines have enabled us to exceed such speed limits.

Since then, _we_ have had to adapt to the speed of our machines, and our lifestyle has become determined by schedules.

That's why many are joining the international _Slow Movement_ and fighting the tyranny of time pressure.

Many people feel that a way of life dictated by clocks and machines is bound to dehumanize us. One example of this sentiment can be found in Chaplin's _Modern Times,_ which depicts hectic assembly line workers who move and act just like automatons, and a protagonist who's swallowed up by a machine.

People today are joining forces to promote a slower lifestyle. For instance, every October the Society of the Deceleration of Time hosts a conference in the city of Wagrain to explore ways of slowing down.

But what does it matter that life is faster than ever? Read on to find out.

> _"Man measures time, and time measures man." - Italian proverb_

### 3. Time pressure hastens our thoughts and can mar our relationship with food. 

In our rush to keep up with the speed of modern life, we forget about the pleasure of enjoying existence one bite at a time.

Our accelerated lifestyle has even found its way to our dinner tables. But that doesn't mean we can't kick this unwanted guest out of our lives.

We've come to prefer food that's readily available, regardless of the health risks. So instead of sitting down to a meal with family and friends, we often eat alone, rapidly, usually while doing something else.

These rushed eating habits and ready-made meals have adverse effects on our bodies. Obesity rates, for instance, are growing, both because we stuff our body with processed food packed with sugar and fat, and because rapid eating doesn't give our stomachs enough time to tell the brain that we're full.

But these habits aren't insurmountable. We can learn to deal with food in a more mindful way. For instance, the international movement Slow Food encourages people to grow and cook their own food, and to slowly savor every bite they take. Cooking and eating slowly can be a wonderful way to unwind, like a form of meditation.

Moreover, the pressure to keep up has also sped up our thinking. Luckily, we can learn to relax and make a conscious effort to think more slowly.

But what's wrong with thinking fast? Isn't that a good thing?

Not necessarily.

Experts think that the brain has two modes of thought: _Fast Thinking_ is rational, analytical and logical, whereas _Slow Thinking_ is intuitive and creative. Research has shown that people who are used to Slow Thinking are both less stressed and more creative in their everyday tasks.

So how can you nurture your Slow Thinking? One way is meditation, which can help your mind to relax. Start by sitting in a comfortable position. Then, close your eyes, focus your attention on your breath for a few minutes and enjoy some stress-free Slow Thinking.

### 4. Until recently, the lifestyle of speed invaded the cities and degraded the quality of medicine and education. 

The process of urbanization has made us forget about our natural rhythms, and that's not just because big cities never sleep.

Speed affects numerous aspects of city life, from the way we're treated by doctors to the quality of our child rearing.

It's no secret that urban life is fast-paced: teeming streets and malls, an abundance of stimuli and options all compel us to rush rather than rest. The evidence suggests that this perpetual motion is alienating us from each other. Consider, for example, that 25 percent of Britons do not even know their neighbors' names!

Looking to medicine, faster is not necessarily better (except in emergencies). And yet, an average visit to your local general practitioner today lasts about six minutes. Rather than taking the time to ask patients about their health and their lifestyle in order to develop a holistic remedy, doctors rush to treat symptoms.

And lastly, in today's fast-paced world we inadvertently teach children to obsess over speed. Competition in school causes ambitious parents to urge children to learn as quickly as possible. As a consequence, children as young as five suffer from stress-related issues, like stomach aches and depression.

Luckily, the Slow Movement has been gaining influence in these areas of life, albeit slowly. The Italian town Bra is a perfect example of this, having written a fifty-five page manifesto outlining what they want for their _Citta Slow_, or Slow Cities: things like cutting noise and traffic, and increasing pedestrian zones and green spaces.

Similarly, more and more Westerners are turning to complementary and alternative medicine, where practitioners take the time to speak with patients and hear them out.

Finally, some schools are now adopting a slower approach to education, giving children the time and freedom necessary to develop a love of learning. Finland, for example, already practices slow schooling, allowing kids to study at a manageable pace and take time to explore subjects.

> _"The most effective kind of education is that a child should play amongst lovely things." - Plato_

### 5. We’re too stressed to enjoy life, but a little slowness can go a long way in changing that. 

There are some simple joys no one can take away from us. Or can they?

Faced with the overwhelming demands of the fast life, we begin to see everything as a challenge, even things that were once gratifying. The need to pay the bills pushes us to plan everything around our work schedule, causing us to essentially "forget" to have fun. And then, because we've had no time to relax, it's even harder to sit down and pay the bills!

By some estimates, the average American works 350 hours more per year than his European counterpart, surpassing even Japan as the country with the longest working hours among industrialized nations.

After all that work, we're left with so little free time that we feel compelled not to waste a second. This becomes another source of stress due to the abundance of available activities to choose from. Instead of just relaxing, we overthink our leisure plans.

This fast lifestyle has even entered the bedroom: sex is no longer a sensual pleasure but an efficient means of getting a quick orgasm. Today, perhaps as a result of this, as many as 40 percent of American women suffer from a lack of sexual desire and pleasure.

Luckily, people have started opposing this rat race, and are learning to enjoy life again. Particularly, the new generation is challenging the assumption that everyone has to work incredibly long hours, and are refusing to hurry. Even in Japan, more and more young people opt for a part-time job in order to enjoy more free time.

People are spending their leisure time placidly, doing things like gardening, music and knitting. They're learning to relish slow sex again, with many couples now trying tantric techniques in order to turn sex into something more than a race to the finish line.

All you have to do is introduce a little slowness into your life, and its quality will improve.

> _"To be able to fill leisure intelligently is the last product of civilization." - Bertrand Russell_

### 6. Final summary 

The key message in this book:

**So much of modern life is fast-paced. The desire to keep up fills us with anxiety as we fill every waking moment with activity. In the process, we forgo our natural rhythms and fail to enjoy our leisure or give activities the time they deserve. But all that is changing with the Slow Movement.**

Actionable advice:

**Take a break.**

Next time you want to take your lunch at your desk so you can work while you eat, remember that you are more important than your job. You deserve some time to relax! Take your lunch to a park bench instead, enjoy the sun on your face and savor each delicious bite of food.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Carl Honoré

Carl Honoré is an award-winning journalist, author and TED speaker. In addition to articles published in the _Economist_, the _Houston Chronicle_ and the _Miami Herald_, he's written two other books, including _The Slow Fix_.

