---
id: 53e0c09d3230610007030000
slug: in-pursuit-of-the-unknown-en
published_date: 2014-08-05T00:00:00.000+00:00
author: Ian Stewart
title: In Pursuit of the Unknown
subtitle: 17 Equations That Changed the World
main_color: 2693BF
text_color: 1C6C8C
---

# In Pursuit of the Unknown

_17 Equations That Changed the World_

**Ian Stewart**

In this book, Ian Stewart focuses on 17 famous equations in mathematics and physics history, highlighting their impact on society. Stewart gives a brief history of the wonders of scientific discovery, and peppers it with vivid examples and anecdotes.

---
### 1. What’s in it for me? Discover the vast influence of mathematical equations. 

Kings, queens, wars and natural disasters abound in history books, but equations rarely make it in. It is misrepresentative, considering that equations have altered the course of history time and again.

The Pythagorean theorem made it possible for us to draw accurate maps and Einstein's theory of relativity fundamentally changed the way we view time and space.

These blinks will show you that, however abstract they may seem, equations can reveal unexpected concrete relations in the real world. Of the 17 world-changing equations discussed in the book, here we will present nine of the most relevant and vivid ones.

You'll also learn:

  * how triangles, circles and squares are all the same,

  * how your car's satellite navigation is actually capable of sending you to the wrong planet,

  * how a food processor can produce chaos, and

  * how an equation can cause a stock market crash.

### 2. The Pythagorean theorem shaped our understanding of geography and Earth’s place in the solar system. 

On the surface, the Pythagorean theorem looks simple. Made up of the equation a²+b²=c², it explains the relationship between the three sides of a right-angled triangle where a and b represent the sides that form the right angle and c is the longest side.

Despite its simplicity, the theorem was a vital first step in developing the geometric techniques needed for making accurate maps: _triangulation_. Since all polygons can be cut into triangles, and all triangles can be cut into two right-angled triangles, the equation allows us to calculate the sides and angles of any polygon. In mapmaking, the area being surveyed is covered with a network of triangles, which allows us to measure distances and angles between stretches of land.

Because of technological advancements in measuring tools, explicit triangulation is no longer used. However, it's still there in the methods used to deduce locations from the satellite data.

Generalizations of Pythagoras's theorem also enable us to measure the shape of the universe. The Earth has the shape of a sphere, sure, but what about the universe? Finding the answer to this question seems like an impossible task: after all, we can't step outside to see what shape it is.

However, thanks to generalizations of Pythagoras's theorem, there is something else we can do.

Pythagoras's theorem applies to triangles in a _flat_ plane. But generalizations of it can apply to triangles in spaces that are _curved_. For example, applying triangles onto a sphere makes it possible to measure our planet. But there's more: these generalizations can also tell us _how_ curved the spaces are. So by measuring triangles within space, we can try to compute its curvature.

> _"Equations are the lifeblood of mathematics, physics and technology. Without them, our world would not exist in its present form."_

### 3. Euler’s formula revolutionized the world of math – and helps us understand DNA. 

In geometry, a polyhedron is a three-dimensional solid consisting of a collection of faces joined at their edges — the cube, for example. Euler's formula for polyhedra is a simple but remarkable statement:

If we count the numbers of faces (F), edges (E) and vertices (V), or the corners of the solid, it holds that F-E+V=2.

Euler's formula changed the world for mathematicians, motivating the development of a new kind of geometry: topology.

In topology, familiar concepts such as lengths and angles play no role, and no distinction is made between triangles, squares and circles. Lines can bend or stretch — they just aren't allowed to be torn apart.

If we're given two geometric figures, it's a natural mathematical question to ask whether they're topologically the same, i.e., can they be deformed into each other by bending and stretching?

This question can be surprisingly hard to answer just from looking at a figure. And it is where Euler's formula comes into play: it can distinguish geometric objects just by counting faces, edges and vertices. So, if we want to find out whether a polyhedron is topologically equivalent to some other solid, we calculate F-E+V and compare the results. If both equal 2 they are topologically the same, and if they don't, they're not.

This insight developed into a rich field of mathematics that explores geometric shapes.

Topology has an increasing number of applications in biology. Like two spiral staircases winding around each other, the two strands that make up DNA are intricately intertwined. And important biological processes — in particular, the way a cell copies its DNA when it divides — have to account for the complex geometry of this double helix.

Mathematicians and molecular biologists can use topology to analyze the twists and turns of DNA, and thus understand how different biological processes depend on their geometric structures.

### 4. The normal distribution is one of the most important tools for analyzing social, medical and scientific data. 

In the nineteenth century, mathematicians started seeing statistical patterns in chance events. They found that the probability of observing a particular data value is greatest near the mean value, i.e., the average, and dies away rapidly as the difference from the mean value increases. The formula for this phenomenon is called the _normal_ _distribution_ and, since its curve looks like a bell, it's also called the bell curve.

In 1835, Adolphe Quetelet, a Belgian mathematician and sociologist, collected large quantities of data on crime, births, deaths, height, weight and so on. When he plotted the proportions of people with any of those given social variables, he started to see patterns, obtaining beautiful bell curves.

This was a remarkable find. No one expected social variables to conform to any mathematical law because their underlying causes involved human choices and were thought to be too complex. This phenomenon showed that people _en_ _masse_ behaved more predictably than individuals.

The normal distribution is a routinely used technique for hypothesis testing and widely used in medical trials of new drugs and treatments. Let's say observational data — which can be flawed due to slight variations in apparatus or human error — suggests that a new drug is twice as effective as an old one. But we can test the data's reliability with the help of a striking feature of the normal distribution: the probability of errors in observational data is normally distributed.

The method works by testing the hypothesis that the new drug is twice as effective and the hypothesis that this result arose by chance. If the probability of the second hypothesis — which is given by the bell curve — is low, the study is considered reliable.

### 5. The Navier-Stokes equation provides solutions for innumerable scientific and technological problems. 

Earth is a water world. Its surface is covered in oceans, seas, rivers and lakes that _flow_. And water's patterns attracted the attention of mathematicians who wanted to try to predict the flow.

Predicting the flow became possible in the first half of the nineteenth century, thanks to the Navier-Stokes equation. A world of possibilities opened due to the equation of flow.

The Navier-Stokes equation has revolutionized modern transport. The most obvious means of transport to benefit from it is the ship, but aircraft and cars also benefit since the vital fluid for flights is air and cars have designs based on aerodynamic principles.

For example, in order to minimize aerodynamic resistance for efficient fuel consumption, the Navier-Stokes equation is so accurate that engineers often use computer solutions based on the equation instead of performing physical tests in wind tunnels.

And since there are millions of cars, ships and aircraft on the planet, that all depend on moving reliably within a fluid, the Navier-Stokes equation has revolutionized modern transport.

The equation is also widely used in medical research. It can calculate blood flow in the human body, helpful for surgeries such as a coronary bypass, which requires control of the blood flow during surgery.

It even has an application in climate models. Two vital aspects of the climate are the atmosphere and the oceans, both of which are fluids. The equation can be used to investigate climate change.

### 6. Newton’s law of gravitation enables predictions of the solar system and space mission orbits. 

Legend has it that Newton came up with his law of gravitation one day, while sitting in the grass and an apple fell from a tree and hit him on the head. Whether the story is true or not, the apple was important for Newton because it made him realize that the same law of forces could explain the motion of the apple as well as the moon.

Newton's law enables accurate predictions of the motion of the solar system. His law of gravitation determines the force of gravitational attraction between two bodies in terms of their masses and the distance between them. And the most important aspect of Newton's law of gravitation is the assertion that gravitation acts universally: _any_ two bodies anywhere in the universe attract each other.

This idea made it possible to predict the future movements of the solar system with great precision, such as the motion of the moon under the influence of the sun and earth.

Newton's law of gravitation is routinely applied to design orbits for space missions. Today the world's space agencies, such as NASA and ESA, still use Newton's law of gravitation to work out spacecraft trajectories. It all started with a simple question: What's the most efficient route from the earth to the moon (or to the planets)?

In the Apollo missions of the 1960s and 1970s, routes were deduced based on Newton's law by calculating the force with which the earth and moon would act upon the spaceship.

And since Newton's law of gravitation enables us to calculate trajectories for artificial satellites, it's what makes satellite communications, television, the Global Positioning System (commonly known as GPS), Mars rovers and interplanetary probes possible.

In the next blink, we'll look at Einstein's theory of relativity, which eventually superseded the Newtonian theory of gravity. However, for almost all practical purposes, the simpler Newtonian approach still reigns supreme.

> _"Equations have hidden powers. They reveal the innermost secrets of nature."_

### 7. Einstein’s theory of relativity changed our view of space and time – and our everyday lives. 

Einstein's theory of relativity has given us the best understanding of the origins of the universe and the structure of the cosmos to date. It has also been at the root of radical new physics.

In Newton's day, time was independent of space, and the mass and size of a body didn't change when it moved. When Einstein introduced his theory of relativity, these statements turned out to be wrong. Moreover, classical mechanics had taught every physicist to ask: speed relative to what? But the speed of light turned out to be a constant, not relative to any physical quantity.

Finally, Einstein came up with a completely new notion of gravity, not representing it as a force, but as curvature of _space-time_ : a four-dimensional model of space including three dimensions of space and one dimension of time.

The most dramatic predictions based on the theory of relativity occur on a grand scale, such as those about black holes, which are born when a massive star collapses under its own gravitation, and those about the expanding universe, currently explained by the Big Bang.

In everyday life, we benefit from the use of relativistic effects that were borne out of the theory of relativity.

Take satellite navigation systems in cars, for example. These systems calculate a car's position using signals from a network of orbiting satellites, i.e., GPS, based on very precise timing signals.

The theory of relativity predicts that the clocks on the satellites should lose 7 microseconds (millionths of a second) per day compared with the clocks on the ground. It also predicts a gain of 45 microseconds per day caused by Earth's gravity.

GPS based on classical physics without taking these relativistic effects into account would quickly become useless. Ten minutes from classical GPS would place us on the wrong street; by tomorrow it would place us in the wrong town; within a year we would be on the wrong planet.

> _"E=mc² was not responsible for the atom bomb; it was one of the big discoveries in physics."_

### 8. Schrödinger’s equation changed our notion of matter and is ubiquitous in modern electronics. 

Schrödinger's equation is a statement in _quantum_ _theory_, a branch of physics that deals with phenomena at atomic- and subatomic-length scales.

The quantum world is notoriously weird. To give you an idea of its weirdness, it's a world in which light is both a particle and a wave. It is so different from our comfortable human-scale world that even the simplest concepts change beyond recognition.

And Schrödinger's equation led to a radical revision of physics at very small scales.

In the nineteenth century, physicists found that light seemed to behave like a wave in some experiments but like a particle in others.

As physicists came to grips with the very small scales of the universe, they decided that light wasn't the only thing to have this strange, dual (sometimes-particle, sometimes-wave) nature: all matter did.

Schrödinger's equation models matter as a wave, not a particle, and describes how such a wave propagates. The equation made it possible to develop a theory able to explain the wave-particle duality: quantum theory.

Quantum theory is a radical revision of the physics of the world at very small scales: objects can be in two different states at the same time and every object has a "wave function" that describes the probability of its possible states — wave or particle.

But quantum theory isn't confined to labs: modern electronics depends on it.

Modern memory chips are built of semiconductors, a crystalline material that has electrical conductivity. Knowledge of quantum theory is required for someone to analyze how electrons move through a crystal. So semiconductor technology is based on quantum theory.

Virtually all modern household gadgets, such as computers, mobile phones, CD players, cars and refrigerators, contain memory chips and are thus dependent on this quantum knowledge.

Together with Einstein's theory of relativity, quantum theory constitutes today's most effective theories of the physical universe.

### 9. Chaos theory pervades most areas of science, and understanding chaos can help in everyday life. 

Chaos theory has grown from a minor mathematical curiosity into a basic feature of science. It shows that even simple theories can generate deterministic chaos — that is, random behavior with no random cause.

Picture a food processor. Its blades follow a very simple rule: they spin around really fast. And when the food interacts with the blade, the result is chaotic: the food doesn't spin around with the blades, it's mixed up.

A similar effect can occur in many models for natural phenomena.

In biology, there is a simple population model of animals in which the size of each generation is determined by the previous one. Theoretically, the population size of generation zero completely determines the sizes of all succeeding generations. But in actuality, we seldom have _exact_ data of an initial state. Any uncertainty grows exponentially fast and the size of a population becomes unpredictable very quickly — and chaos has occurred.

Chaos has applications that impinge on everyday life.

The main impact of chaos has been on scientific thinking. For instance, the above examples show that "deterministic" doesn't necessarily mean "predictable." And this insight has practical applications.

To name one major example, chaos theory changed the way weather forecasts are carried out. Weather is deterministic, but hard to predict: individual weather models create chaos very easily because of weather's complexity. So instead of putting all computational efforts into refining a single prediction, meteorologists now run many forecasts and, if all these forecasts agree, the prediction is likely to be accurate.

Other applications of chaos theory include a better understanding of mixing processes, widely used to make medicinal pills or mix food ingredients.

> _"Nature's balance is distinctly wobbly."_

### 10. The Black-Scholes equation led to the massive growth of the financial sector – and the risks of financial crisis. 

Financial instruments known as _derivatives_ are the greatest source of growth in the financial sector. Derivatives are not money; they're investments in investments, promises about promises — and the standard of the world's banking system.

The _Black-Scholes_ _equation_ made trading with derivatives reasonable and facilitated the financial sector's growth.

Common derivatives are _options_, meaning contracts where the buyer of the option purchases the right to buy or sell a commodity within a specified "maturity" period for an agreed-upon price.

For example, a merchant signs a contract to buy a thousand tons of rice in 12 months' time at a price of $500 per ton. After five months, she decides to sell the option. Knowing how the market price has been changing during that time, how much is the contract worth now?

If you trade such options without knowing the answer, you're in trouble: getting the price wrong means the trade can lose money. So there has to be a way to price an option at any time before maturity (when the contract terminates). The Black-Scholes equation does just that: it provides a way to calculate the value of an option that's reasonable under normal market conditions. And so it offers everyone a way into the financial sector!

The Black-Scholes equation led to the risks of trading by formula.

It works effectively under normal market conditions but the world's financial sector began to invent ever more complex derivatives, which were sometimes based on assets that weren't real but were derivatives themselves.

However, complex derivative trading still depends upon the belief that mathematical equations can accurately describe the market, though evidence points in a totally different direction: 75 to 90 percent of all futures traders lose money in any given year.

The dangers of trading by formula in a world that did not obey the cozy assumptions behind the formula are what triggered the banking crisis of 2008–09.

> _"An equation is a tool, and like any tool, it has to be wielded by someone who knows how to use it, and for the right purpose."_

### 11. Final summary 

The key message in this book:

**Mathematical** **equations** **have** **played** **a** **vital** **part** **in** **creating** **today's** **world.** **Mapmaking,** **satnav,** **television,** **music,** **civil** **aircrafts,** **exploring** **the** **moons** **of** **Jupiter,** **etc.** **–** **none** **of** **that** **would've** **been** **possible** **without** **the** **help** **of** **equations.**

Actionable advice:

**Mind** **your** **triangles.**

The next time you want to renovate your apartment, think about Pythagoras's theorem. It can be a useful tool for finding out how many square meters of new flooring you need, how to cut your bathroom tiles or how much paint you have to buy for your walls.

**If** **it's** **too** **good** **to** **be** **true,** **it** **probably** **is.**

The next time you want to trade with stocks, be aware of the fact that even good mathematical equations can't reflect the complexity of the market. A healthy dose of caution and skepticism is helpful if you don't want to lose a lot of money.
---

### Ian Stewart

Stewart is professor emeritus of mathematics at the University of Warwick, England, and fellow of the Royal Society. His main mathematical interests are Lie algebras and the theory of dynamical systems and its connection to chaos theory and biology. He is the author of several popular science books including _Does_ _God_ _Play_ _Dice?_ _The_ _New_ _Mathematics_ _of_ _Chaos_ and _Why_ _Beauty_ _Is_ _Truth:_ _A_ _History_ _of_ _Symmetry_.

