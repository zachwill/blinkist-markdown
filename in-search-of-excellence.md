---
id: 5489d5b63363370009280000
slug: in-search-of-excellence-en
published_date: 2014-12-16T00:00:00.000+00:00
author: Tom Peters and Robert H. Waterman Jr.
title: In Search of Excellence
subtitle: Lessons from America's Best-Run Companies
main_color: BE2633
text_color: BE2633
---

# In Search of Excellence

_Lessons from America's Best-Run Companies_

**Tom Peters and Robert H. Waterman Jr.**

_In Search of Excellence_ is the groundbreaking, multi-million-selling business management classic by two McKinsey & Company consultants, last re-issued in 2004. The authors conducted a study of America's top 15 companies to deduce the core principles that unite corporate giants from Caterpillar to IBM.

---
### 1. What’s in it for me? Learn the secrets of the most successful companies in history. 

The path to the holy grail of business management has proved elusive. While many loyal Lancelots and management consultants have fought the dragons guarding the secrets of management, few have survived.

Well, at least until 1982, when Tom Peters and Robert H. Waterman Jr. decided to unsheath Galahad's sword and head straight to the source. No, not some castle, but the people who actually ran the most successful companies in American history. And if the authors didn't find the holy grail of business management, then maybe it's not meant to be found — they certainly vanquished a few myths on the way.

_In Search of Excellence_ is the result of their quest — an instant classic that shows how the great companies built their successes and faced their challenges.

After reading these blinks, you'll have learned

  * how to grow without sacrificing a small business's flexibility and innovation;

  * why just walking around constitutes a great management strategy; and

  * why Procter & Gamble lets its brands fight against each other.

### 2. For decades, people have debated the best way to manage a company. 

Every business leader wants to know the best way to manage their company. But why are there so many different approaches and schools of thought?

Well, management theory has long been a topic of general concern and disagreement among academics. In the 1930s, Harvard's Chester Barnard rejected long-held ideas about bureaucratic management that were put forward earlier by thinkers like Max Weber. Barnard's work kicked off a debate about whether management was an impersonal, objective and exact science, or more of an art relying on charismatic leadership.

In the 1970s, when the authors began their research, a key area of concern among management theorists was international competition — especially in countries like Japan, where companies were growing at a much faster rate.

Observers noticed that Japan's lack of business schools had produced a different management culture than in the United States. This observation led to questions about whether Americans, with all their management classes, had adopted an overly theoretical approach.

The authors decided to find some concrete answers amid the speculation. So they assembled a list of America's top 15 companies — boiled down from 43 contenders — and spent six months conducting interviews and doing research to figure out what they all had in common. Their results were popularized in the book's first edition, published in 1982.

The 15 companies were selected using two criteria:

  1. The company's reputation among businessmen, consultants and business academics.

  2. Basic measures of growth and long-term wealth creation (like average return on capital or average return on sales) over a 20 year period. Companies were deemed excellent if they'd stayed in the top half of the original ranking of 43 companies for these measures over the full period.

So, according to these criteria, in 1982 the 15 top American companies were: Bechtel, Boeing, Caterpillar Inc., Dana, Delta Airlines, Digital Equipment, Emerson Electric, Fluor, Hewlett-Packard, IBM, Johnson & Johnson, McDonald's, Procter & Gamble and 3M.

But what attributes did all of these companies share? Read on to find out.

### 3. Excellent companies know how to get things done. 

So, what _do_ successful companies have in common? Well, the authors noticed that all the top companies they surveyed had an _action bias_. That is, these firms had the ability to get things done, regardless of the complexity of the task.

Although most companies _want_ to believe they have an action bias, not many do. Instead of getting things done, most businesses get tangled up in bureaucracy. After all, when countless committees have to sign off on any given decision, there's a high barrier to action.

Excellent companies deal with this issue by adopting _organizational fluidity_. This term refers to the ability to resolve issues, especially problems that require multiple levels of bureaucratic attention.

For example, top companies would have a system in place to efficiently cope with a product issue that needed input from a variety of departments — such as the product, legal and advertising teams.

These organizations create fluidity by promoting vast informal networks for communication, like open door policies. At IBM, for example, the chairman personally addressed any complaints he received from his 350,000 employees.

United Airlines had a similar policy, called "Management by Walking Around," which encouraged quick, casual conversations at every level of the company.

These informal communication processes created opportunities for companies to quickly address issues _without_ superfluous bureaucracy.

_Chunking_ — breaking things down — is another way companies can encourage action and optimize organizational fluidity. By creating _small groups_ — task teams of fewer than ten people dedicated to solving specific problems — companies can nimbly deal with problems as soon as they arise.

Although these task teams don't typically show up on formal organization charts (which represent more conventional units like departments and divisions), small groups tend to be the true organizational backbone of excellent companies.

Consider Canon, which organized a task team to develop and launch the AE-1 camera in just two and a half years. Today, the camera is widely regarded as a groundbreaking technological advancement.

> _''Ready. Fire. Aim.'' — Cadbury's executive_

### 4. Top businesses stay close to their customers. 

As Lew Young, former editor-in-chief of _Businessweek_, once observed, too many companies regard the customer as basically a bothersome nuisance who damages carefully laid business plans. But excellent companies take a different approach.

America's top companies have a _service obsession_. Meaning, the needs of the customer intrude into every facet of the business, from research to sales to accounting.

These companies may seem almost ludicrously committed to their customers, but in fact they're using this service obsession to make up for shortfalls in other areas.

IBM, for example, hasn't been a leader in the technology sector for decades. However, since the brand is synonymous with service, the company has maintained its position as an industry leader.

While assistants at other companies spend their days fetching coffee or pushing paper, entry level IBM staffers spend their first three years fielding customers complaints — which must always be resolved within 24 hours. This practice yields a better understanding of what products and services the company needs to satisfy its customers.

Additionally, this policy builds customer loyalty. Since IBM is committed to quickly resolving all issues, many businesses have come to rely on the company for all their software and hardware needs.

But good customer service doesn't just allow a company to resolve issues and build a customer base — it also enables innovation.

Consider the fact that Procter & Gamble, one of the most consistently successful companies _ever_, was the first consumer goods corporation that put a toll-free number on its products. Employees later reported that this move led to most of the company's product improvements.

An MIT study about the innovation of scientific instruments corroborates P&G's experience: After studying 11 new inventions, researchers found that they were all originally conceptualized by end users — not producers — of the instruments.

So ultimately, excellent companies know that customers bring value beyond the final sale.

### 5. Big firms can maintain their edge by promoting experimentation and internal competition. 

Did you know that small businesses produce 24-times more innovation per dollar than larger firms?

So what about the excellent — and huge — multinational corporations surveyed by the authors? Although these companies are big, they act small in terms of innovation.

How do they do it? Well, _internal competition_ plays a key role because it fosters a sense of autonomy and entrepreneurship — even within the large corporate structure.

And autonomy and entrepreneurship are _crucial_ to innovation, because they give employees the opportunity to develop creative ideas that go beyond their job description.

Internal competition creates the space for this kind of creativity. It brings the competitive outside market _into_ the company, thus promoting innovation and preventing stagnation.

IBM, for example, uses a "performance shoot-out" approach: Proposed products are pitted against each other, which results in real performance comparisons between prototypes.

Procter & Gamble also takes an interesting approach to adopting internal competition. In 1931, the company established a formal policy that allowed different brands in the company to actively compete. Managers typically don't have access to information about other P&G brands, beyond what's publicly known.

This competitive element motivates brands to continuously improve their products, which ultimately benefits P&G's overall sales.

There's another key characteristic that promotes innovation among the big companies surveyed in the study: These companies embrace failure by encouraging experimentation and an entrepreneurial spirit. When something doesn't work out, these companies just move on to the next thing.

3M is one such company. For example, when a new ribbon material the company was developing failed, 3M tried using it as material for a brassiere. When that _also_ failed, the company didn't give up. Eventually, it became the standard material in safety masks worn by government workers.

So as you can see, promoting internal competition and encouraging experimentation ensures innovation even at large companies.

> _''If I wasn't making mistakes, I wasn't making decisions.'' — General Johnson, founder of Johnson & Johnson_

### 6. The most successful companies sincerely care about their employees. 

There's another thing excellent companies have in common, and that is a _deeply ingrained respect for individual employees_.

In other words, top companies sincerely care about their employees and create _people-oriented_ working environments. Management respects individual employees, invests time and money into their development and holds them to reasonable expectations.

Many companies _pretend_ to be people-oriented, and give the outward appearance of caring about their employees, but it's often just a facade. This attitude typically results in two kinds of management disasters:

  1. _The lip service disaster_ : Although management _says_ they care about their employees, they do very little. For example, employees may never get proper training.  

  2. _The gimmick disaster_ : Here, management relies on fads and incentives — like "employee of the month" awards. These novelties typically fade away after short trial periods, but even if they persist, they still do very little to change the management–employee dynamic.

So, if that's what insincere people-orientation looks like, how do companies of excellence show that they genuinely care?

Well, these top companies typically adopted people-oriented policies long before they were the norm. For example, these firms were among the first to implement training programs. They also encouraged employees and managers to communicate on a first-name basis back when the traditional business culture was more formal.

Hewlett-Packard is a fantastic example of a people-oriented company. In fact, in individual interviews with 20 HP executives, 18 mentioned the company´s people-oriented philosophy as a key reason for its success.

As it happens, the "HP Way" has a long history. In the 1940s, executives decided that they didn't want HP to be a "hire and fire" company. So, instead of firing employees during the 1970s recession, the entire company took a ten percent pay cut. In the end, HP survived the recession without terminating anyone's employment.

### 7. Excellent companies are powered by inspiring values. 

As a manager, what can you do to mimic highly successful companies? Well, you might start by establishing some company values.

That's because excellent companies set out qualitative values aimed at inspiring employees at every level of the company.

The authors cite a study conducted by McKinsey & Co. that also looked at top-performing companies, but through the specific lens of company values. The results showed that nearly all of the companies surveyed had well-defined guiding values.

One such value at the Dana Corporation, for example, was an inclusive management style that prioritized simplicity.

Companies that performed less successfully shared two defining characteristics:

  1. The majority of these companies had no coherent beliefs or values.  

  2. Those that did have distinct objectives focused predominantly on attributes that could be easily quantified — such as earnings per share.

To that last point — and this may seem counterintuitive — the companies whose values were the most financially oriented and quantifiable actually fared _worse_ than firms with broader qualitative values (such as customer service, for example).

And if there's one such broader qualitative value that is shared by all top companies, it's a commitment to _innovation at every level_.

It's important to note that innovation is crucial at all kinds of companies, be they technology companies (like Apple and Hewlett-Packard), manufacturing companies (3M) or consumer goods firms (Johnson & Johnson).

And this value is characterized by the understanding that innovation is a somewhat random and unpredictable element of business. So, since it can't be centrally planned or sidelined into just one department, it needs to be the goal of the whole company.

The underlying belief here is that everyone is capable of innovation, not just the research and development departments. This mind-set leads to a company-wide culture, where everyone is always on the lookout for the "next big thing."

And ultimately, this forward-looking ethos helps the company achieve consistent growth.

### 8. When excellent companies branch out, they build on their core strengths. 

As a company becomes more successful, there's always the temptation to buy out the competition or branch out into new markets and sectors.

This is called _diversification_, and it can be done by creating a new products or buying a company in a different sector — with the ultimate hope of making more money.

Apple is a diversification success story. Consider the fact that it expanded its product range from computers to music players to phones. Today, it's even making watches!

But the truth is, it's not always so easy. Because diversification isn't necessarily linked to profitability.

That's what the economist Michael Gort discovered when he conducted the first systematic study of diversification in American businesses. Although he did notice a mild positive correlation between the number of new products a company introduced and increases in sales, he saw absolutely no positive relationship between new product launches and actual profitability.

But as it turns out, there is a secret to profitable diversification — and it's called "_sticking to your knitting_." Companies that "stick to their knitting" are the ones that diversify, but keep their new products and services consistent with their central skills and strengths.

This principle comes from Richard Rumelt's seminal study on diversification, which found that successful companies would only branch out when they could build on their existing strengths and competences.

These companies would achieve a 12.4 percent return on the money they spent; all in all, that's 30 percent more than companies that diversified haphazardly.

3M, for example, has over 50,000 products, with 100 new ones added each year. However, each one of these products relied on the company's signature coating and bonding technology.

And 3M isn't alone: From Boeing to Walmart, the dominant approach to diversification among top companies is one of sticking to core strengths.

> _"Never acquire any business that you don't know how to run." — Robert Wood Johnson, founder of Johnson & Johnson_

### 9. Even large companies run best with a simple, lean organizational structure. 

Success has its own challenges. And one of the drawbacks of growth is that as a company expands, it hires more people, ends up with larger departments — and finds itself with a super-complex organizational structure.

So, imagine a small company with a _functional organizational structure_. Employees are grouped together according to whatever function they perform, such as marketing, legal work or sales. In this structure, it's clear whom people should report to — be it the manager or the CEO.

But as the company gets bigger, it outgrows this rigid structure. And it becomes inefficient for everyone to report up the hierarchy all the time, because it prevents individual employees from dealing with unpredictable or time-sensitive issues.

Companies try to cope with this issue by implementing a _matrix organizational structure_, creating discrete teams responsible for specific products and functions. So, for example, Product Team A works with Legal Team A and Sales Team A.

But this gets complicated very quickly. Since there are many different managers running different teams, employees get confused about whom they should report to.

But excellent companies find their way around this by embracing _simple form_ — stable, unchanging organizational structures — and _lean staff_.

"Lean staff" means minimal administrative, managerial and executive layers. So at these companies, there will be just one manager or department head, instead of multiple people for employees to report to.

And even though Johnson & Johnson was a $5 billion company with 150 independent divisions — each worth an average of $30 million — it was a perfect example of this kind of organizational simplicity.

Each division is called a "company" and has its own "chair of the board." Even though the "companies" don't have their own stocks and are by no means independent from J&J's top management, they still play a very important role.

The chairmen shield their divisions from unwanted bureaucratic interference. Furthermore, since each division controls its own marketing, distribution and research, it leads to better, more efficient decision-making, and ultimately requires fewer people.

### 10. Final summary 

The key message:

**People have long debated the best way to manage a company. Well, it turns out that excellent companies have a lot in common: Top businesses put a premium on customer service. They also sincerely care about their own employees and promote experimentation within the company.**

Actionable advice:

**Let your employees use the company's resources and tools in their free time**.

If you want to promote innovation at your company, encourage your employees to freely use the company's resources — like laboratory or manufacturing equipment — in their downtime. Being able to "play" with the tools of the trade without a specific, defined purpose might lead to startling and unexpected breakthroughs.

**Suggested** **further** **reading:** ** _Good to Great_** **by Jim Collins**

_Good to Great_ presents the findings of a five-year study by the author and his research team. The team identified public companies that had achieved enduring success after years of mediocre performance and isolated the factors which differentiated those companies from their lackluster competitors.

These factors have been distilled into key concepts regarding leadership, culture and strategic management.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Tom Peters and Robert H. Waterman Jr.

_The Economist_ called Tom Peters the "über-guru of business," and _Fortune_ claimed, "We live in a Tom Peters world." He writes widely about business management and is a partner at McKinsey. Robert H. Waterman Jr. is an author and business management expert who spent 21 years at McKinsey.

