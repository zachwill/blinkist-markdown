---
id: 561c3a503635640007a80000
slug: in-sheeps-clothing-en
published_date: 2015-10-15T00:00:00.000+00:00
author: George Simon Jr., PhD
title: In Sheep's Clothing
subtitle: Understanding and Dealing with Manipulative People
main_color: D72D2B
text_color: BD2726
---

# In Sheep's Clothing

_Understanding and Dealing with Manipulative People_

**George Simon Jr., PhD**

_In Sheep's Clothing_ (1996) reveals the aggressive, undercover agenda of manipulative individuals, whose covert tactics would otherwise remain hidden. These blinks offers you tools to become aware of, prepare for and deal with the tricks used by manipulative colleagues, family members and friends.

---
### 1. What’s in it for me? Recognize the manipulators at work and stop them in their tracks. 

You're busy at work, when you see a strange interaction between two colleagues. One of the colleagues, known for his high-maintenance demeanor, starts giving the other the cold shoulder, willfully ignoring her and walking away from her when she tries to start a conversation. What do you do?

Most of us would probably excuse the behavior, if not ignore it. We'd assume that he's having a bad day, or that he must be having trouble at home. Typically such actions are not frowned upon; rather, they are explained, and even permitted.

This, of course, is wrong. Such behavior simply shouldn't be allowed; it is aggression, pure and simple. These blinks explain why people who act like this should be confronted and stopped, and provide you with the tools and strategies to help you do so.

In these blinks, you'll discover

  * why Sigmund Freud was wrong;

  * how to recognize even the most subtle of aggressive behaviors; and

  * why someone who won't answer yes or no is probably trying to hide something.

### 2. Everyone fights, but manipulators fight aggressively, without concern for their opponents. 

We've all seen it: someone makes a subtle put-down in a social gathering that manages to be hurtful without making the perpetrator look bad. Why would someone even do that? 

Some people, _manipulators,_ like to fight others and put them down in order to make themselves look better. 

This isn't to say that manipulators are the only people who engage in bad behavior; indeed, we all fight from time to time.

Assertive fighting for our legitimate needs is often necessary and constructive — as long as we respect others in the process. For example, football players have to compete against each other to win the game, but do so within the rules.

Another example can be found in politics, where ideas and personalities clash, and the best candidates with the best ideas end up being elected — at least that's the idea.

So how are manipulators any different from the rest of us? The difference is that people with manipulative personalities fight aggressively, pushing their own agenda forward without any regard for other people's feelings, rights or interests.

The reason why is simple: manipulators have an impaired conscience. Unlike the rest of us, they're completely unable to see beyond their own self-interest. 

Whereas most of us will conform to social rules (such as not intentionally upsetting people), manipulators see this deference to social norms as an act of submission, and will likely view these norms as threats to their self-interest.

For example, manipulative parents who pressure their children to perform at a high level in school or sports, without any concern for their child's needs and wants or the opinions of teachers and school staff, are more concerned with their own desires than the way their actions will affect their child. They are actively seeking the status that comes with being the parent of a high-performing student.

> _"Fighting is not inherently wrong or harmful."_

### 3. In general, aggressive manipulators prefer cloak-and-dagger tactics. 

How many times do you get punched in the face on a normal day? Unless you're a professional fighter, probably none. Does this mean that there are no manipulators in your midst? No! Most manipulators are not openly aggressive, so it's possible that there are manipulators in your life without you even knowing it. 

There are two types of manipulators: _Overt aggressive_ manipulators are openly hostile, and don't hide their attempts to intimidate, control or hurt others. In contrast, _covert_ manipulators, while just as ruthless in fighting to get what they want, choose to conceal the actual fighting.

It's important to note that this isn't the same as passive-aggression. Where passive-aggressives fight by remaining passive — such as by "forgetting" to do something you asked for because they're angry with you — covert aggressives actively use calculated, underhanded methods to get what they want. 

For example, in the same situation, a covert aggressive might simply deny that you made the request in the first place, or divert your attention to some other problem when confronted about it. 

Unfortunately, covert aggression is quite popular throughout society simply due to its effectiveness. Say you want to get rid of an employee; it's much easier to just make their life difficult by giving them too much to do or setting unachievable goals, than it is to go through the HR minefield required to fire them.

Another benefit of covert fighting is that it allows the manipulator to fight an opponent while leaving their own reputation untarnished.

Imagine a pastor whose ambition leads him to neglect his own family in order to pursue his career aims. Hiding behind his devotion to God and his parishioners, he can pursue his ambitions while maintaining a positive image of piousness and dutifulness. As a result, no one sees the obvious contradiction between his beliefs and his neglect of his familial responsibilities.

### 4. Victims often wrongly believe aggressive behaviors are due to the manipulator’s own problems or suffering. 

How often have you been told not to take it personally when someone lashes out at you, or that their bad behavior is just a result of their own issues?

This view is widely accepted. In fact, our culture pre-programs us to believe that people _only_ exhibit problem behaviors when they are anxious or have been attacked themselves.

Sigmund Freud's theories on neurosis have strongly promoted the idea that all human beings are neurotic because their basic needs and wants have been inhibited in some way. This principle has become so popular that most people assume that neurosis is always at the root of aggressive behavior. 

Say a person discreetly lets out the secret that a competing colleague is gay, while knowing full well that their boss is terribly homophobic. It's tempting to think that this indiscretion is a result of the perpetrator's own victimization — perhaps he was ostracized during childhood or is repressing his own non-normative sexual urges.

But things are different today. In Victorian times, when Freud developed his theories, society was extremely conservative and restrictive, taking a puritanical view of all natural impulses. This may well have led to neurotic dysfunctions.

Today's society, however, suffers from the opposite malady: an overly _permissive_ social climate in which many people suffer from too little, not too much, inhibition. This being the case, if someone acts with conniving and deceit today, they aren't repressed, they're a manipulator. 

But manipulators don't just rely on society's views on bad behavior. Rather, they work expertly to shift the blame and responsibility onto the victim. Manipulators have a wide array of tactics to achieve this, including minimizing, lying, denying or diverting attention away from their behavior.

For example, a person who only takes and never gives anything in a relationship might invoke their fear of intimacy in order to garner sympathy from their partner, all while deflecting suggestions of their selfish aims. They may even manage to make the victim feel guilty for being insensitive about their "issues!"

> _"When a cat is stalking a mouse for lunch, it would be ridiculous to assume that it is doing so out of fear of the mouse."_

### 5. Stopping manipulators means learning to recognize both their behavior and your own. 

By now, you've learned how manipulators go about their work, and how they get away with it. In this blink, we'll start to look at how we can identify manipulators and put a stop to their bad behavior. 

The first step to stopping manipulators from ruining your life is to recognize when you're making too many excuses for someone. For example, is there a colleague who always flies off the handle, causing you to feel like you're always walking on eggshells? Or do you have to constantly do what your partner wants because not getting their way makes them upset and uncommunicative? 

If this is the case, then you're likely being manipulated. 

In cases like these, it's time to stop making excuses for your manipulator. If you give them too much room, they will surely exploit you.

This can be difficult because we want to believe that all people are inherently good and pure in their intentions. But you simply have to realize that this is nonsense. 

To recognize a manipulator, be on the lookout for certain types of behavior that will betray their intentions. Consider this classic example of manipulation to get an idea of what to look out for: 

A woman decides to leave her husband after finding out that he has been having an affair for many years. He responds by trying to guilt her into staying, lamenting that 15 years of marriage are going down the drain and decrying her violation of their sacred promise.

When that doesn't work, he tries to shame her into staying by asking what all their friends and family will think about them. He may even try to make himself out to be the victim, claiming that _he_ is the one being abused because _she_ neglected their relationship. 

If you see any behavior like this in your life, then alarm bells should start ringing.

### 6. Be firm and assertive when dealing with a manipulator. 

Sometimes we lack the courage to stand up to our aggressors. At the same time, experience teaches us that most problems will not go away until you fight back. But what's the best way to do so?

When confronting a manipulator, keep the focus on the aggressor's harmful behavior. Make sure to focus on their actions, not their intentions. Don't let yourself get distracted by worrying about their mental state, and how it might be causing their behavior. 

For example, an abusive alcoholic who beats up her partner should first and foremost be confronted about her actions _before_ looking at the cause. Looking for explanations and reasons for the abusive behavior, and thereby turning the abuser into a victim, is not helpful. 

Next, when taking a stand against a manipulator, make sure that you communicate with them in a clear and civil manner.

Make sure that you ask direct requests and only accept direct responses. Answers other than "yes" or "no" to clear yes-or-no questions could mean that your manipulator is trying wriggle their way out of the fight.

Avoid sarcasm, put-downs and threats when confronting an aggressive manipulator; this will only serve to help them to justify their aggressive tactics.

Sometimes finding a win-win solution to the problem is the best way to resolve a conflict with a manipulator.

Above all, aggressive manipulators want to win. Bearing this in mind, you can define the terms and conditions of your agreement in such a way as to give the manipulator something they want in order to maximize the chances of resolving the conflict.

For instance, in the case of an overambitious spouse neglecting family life in order to get a promotion at work, the partner who wants to salvage the relationship could propose a deal: she will actively support his ambitions, and in exchange, he will spend more time with the family on weekends and in the evenings.

### 7. You can only beat the manipulator by knowing yourself inside and out. 

It's common wisdom that the better you know yourself, the better your relationship with the rest of the world will be; this is especially true when dealing with aggressive manipulators who might know your weaknesses even better than you do. 

To avoid being manipulated, it's important to work on the character flaws that can be easily exploited by manipulators, such as low self-esteem, naiveté or emotional dependency.

If a teacher lacks confidence, for instance, manipulative students will smell blood, and will immediately engage in power games by testing boundaries and pushing back to see what they can get away with while avoiding punishment. 

To win the battle against this bad behavior and assert her authority over the class, the teacher will have to start by recognizing her lack of self-confidence. Only then can she turn the tide in her favor.

Most importantly, you should always be honest with yourself about your needs and desires in a given situation. 

If you have the need to feel valued and respected, you might be dependent on other people's approval for your sense of self-worth. This tendency can be easily exploited by manipulators, who can offer you simple platitudes while treating you poorly at the same time. 

However, if you are aware of the fact that you crave this attention and accept it as part of your personality, you can better discern when you're being manipulated.

For example, a housewife might deny herself the opportunity to finish a college degree or pursue another long-term dream because it was important for her husband to have a full-time wife and household manager. 

But if she isn't honest with herself about having her own dreams — independently of her husband's desires — she won't be able to step outside his shadow. She must first be honest with herself about what she wants before she can take any steps to improve her situation.

### 8. Final summary 

The key message in this book:

**Unfortunately, not all people are kind or sympathetic. In fact, some are aggressively predatory, and will stop at nothing to achieve their goals — but they aren't stupid. These manipulators use covert manipulative tactics that are hard for their victims to recognize and resist.**

Actionable advice:

**Listen to your gut feeling.**

Manipulators specialize in deflecting, rationalizing and turning the tables on those who begin to see through their deceitful tactics. If you feel like you're being attacked or manipulated, trust your gut, and try not to be so easily swayed by the person who you believe is manipulating you.

**Suggested** **further** **reading:** ** _Snakes in Suits_** **by Paul Babiak and Robert D. Hare**

_Snakes In Suits_ examines what happens when a psychopath doesn't wind up in jail, but instead puts on a suit and gets a job. The book outlines the tactics these predators use, how they damage companies and how you can protect yourself.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### George Simon Jr., PhD

George Simon Jr., PhD is a clinical psychologist from Texas who has spent the last 30 years studying the behavior of manipulative personalities and counselling their victims. He is also the author of _Character Disturbance: the Phenomenon of Our Age_ (2011).

