---
id: 572f0038a35db30003e6be04
slug: in-the-name-of-identity-en
published_date: 2016-05-13T00:00:00.000+00:00
author: Amin Maalouf
title: In the Name of Identity
subtitle: Violence and the Need to Belong
main_color: 466622
text_color: 466622
---

# In the Name of Identity

_Violence and the Need to Belong_

**Amin Maalouf**

_In the Name of Identity_ (1998) explores the fallacies surrounding the idea of identity. The author uncovers the link between oversimplified, one-dimensional understandings of identity to violent cultural and sociopolitical clashes in the past and present, while arguing that identity and a global community of humankind are both compatible and desirable.

---
### 1. What’s in it for me? Learn more about your own and others’ identities. 

If someone were to ask "Who are you?", could you give an answer that wasn't just your name?

Explaining your own identity to others can be difficult. What do you mention? Your political or religious affiliation? Your sexuality? Your educational background?

These blinks will show you the ways in which we can understand identity. They'll take you on a tour around the world to various cultures and religions, and explain how and why so many aspects of our lives influence the way we feel about ourselves — sometimes wrongfully so.

In these blinks, you'll learn

  * why no religion is inherently cruel;

  * why you are truly unique; and

  * how radicalism arises.

### 2. “Identity” is a deceptive and loaded word that requires close examination. 

How do you define your identity? Do you define yourself by your gender, your nationality, your sexuality, or all three?

The truth is that it's no easy question to answer. Identity is a complex concept formed from the various affiliations that make us each unique, such as our religion, job, race, nationality, the people we admire, our hobbies, sexual preferences, etc.

But such allegiances aren't fixed, despite what we may think. Over time, we identify more with some and less with others. These changes can happen over years or from one moment to the next when one aspect of our identity comes to the forefront. For example, a wealthy person from a low socioeconomic class might feel a strong working class pride when mingling at a party of people who inherited their wealth.

While many people's identities vary from moment to moment, others have much more fixed conceptions about who they are. They might exclusively identify with one affiliation, be it their nation, religion or class, and consider the rest secondary.

However, creating rigid hierarchies about who we are can be problematic. One danger is demanding that others identify themselves in hierarchies as well, even when things aren't that simple.

The author has experienced this imposition first hand. He's a Lebanese novelist who immigrated to France when he was 27. His first language is Arabic, but he writes in French, and France has now been his home country for 22 years. And, although his roots are Islamic, he is a Christian.

Often when he has explained his atypical background to someone he is asked, "So deep inside, what do you feel like: French or Lebanese?" The author finds such questions misguided, because a person's identity isn't divisible into halves or quarters, or any fractions at all. A person isn't more one thing than the other, nor does she have many different identities. Rather, identity is the collection of all our characteristics combined together.

### 3. Our identity is affected by how others see us. 

Did you know that identity is something learned? Far from being innate, it's something that we all construct together by the way we look at others — and others look at us.

We all have the power to influence other people's identities by putting them into narrow superficial categories. Let's look at an example: it may seem obvious to you that Austrians are different from Germans, and that every Austrian is also different from every other Austrian. And yet we still lump people together as groups and treat them as one block with identical behaviors, opinions — or crimes. We say "The Americans have invaded," "The Arabs have terrorized," "The Mexicans have stolen," etc. And while these generalizations may seem innocuous at times, they can have serious consequences on people's identities.

For example, when we group people together in a negative way, we push them to identify with the part of their identity that is the most vulnerable.

This is especially true when sociopolitical circumstances attack someone's identity. Take the case of a gay Italian man in Fascist Italy. He might have been a nationalist and even a proud patriot until the Fascists came to power. But then the regime started to persecute people of his sexual persuasion, thereby reducing him to that one narrow dimension of his identity, which affected the way he saw himself. By having to defend his sexual preference, his nationalism faded into the background, and his homosexuality moved to the forefront of his identity.

Something similar happens when people feel their faith is threatened: their religious affiliation comes to reflect their whole identity. But the times might change, and if their race or gender becomes endangered, they might begin fighting against members of their own faith.

### 4. No one religion, affiliation or culture is inherently more violent than any other. 

Such oversimplified approaches to describing others can create dangerous stereotypes that affect us all.

Take Islam. Today it's a demonized identity group accused of having a long tradition of barbarism. However, a quick look into the past proves this wrong.

Indeed, Islam has a long history of openness and tolerance. For example, at the end of the nineteenth century, the capital of the Islamic world was Istanbul, a city mostly made up of non-Muslims, including Greeks, Armenians and Jews. But even long before this, Muslims had shown a remarkable capacity to coexist with others.

In contrast, Christianity's tolerance developed much later. It was not until the end of the eighteenth century and the development of Enlightenment thinking that Christian societies began accepting other religious groups. And while democracy was born in the Western world, voting was restricted to only a few rich and powerful people for many centuries.

Just as Christianity is not inherently tolerant or democratic, Islam is not inherently violent, fanatical or incompatible with human rights and modernity. The original texts of Christianity, Judaism and Islam stay the same through the ages, but the way we look at them changes depending on the time we live in.

For example, modern-day Iran uses Islamic texts to bolster its grievances with the West. Its leader Ayatollah Khomeini uses religious language like referring to the West as the "Great Satan," and calls for its destruction in the name of the Islamic Republic. But this contradicts the long tradition of the Muslim world that doesn't advocate an extremist Islamic revolution.

Such violent ideas and rhetoric are very recent developments. They are not the result of an inherently regressive, anti-modern, anti-democratic tendency of Islam, but of the cultural clashes we're experiencing today.

For instance, Khomeini has more in common with Mao Tse-tung — who vowed to destroy all traces of capitalist culture during China's Cultural Revolution — than any other historical Muslim figure. Instead, the troubling violence and tensions that exist in Muslim countries today are a product of broader, society-wide conditions that are not specific to Islam.

### 5. The West’s hegemony has marginalized other cultures, created a clash of civilizations and fueled an identity crisis. 

We often think that certain groups are angry at the West because of their religion and their deep-seated fear of Western values.

But the truth is that when the West began to dominate the world, its riches, technology and power marginalized other civilizations and cultures. Modern culture has thus become synonymous with the West's culture. Indeed, everything historically significant in recent memory has come from the West: capitalism, fascism, communism, aviation, electricity, computers, human rights, the atomic bomb.

So when radicalized Muslims attack the West today, it's primarily because they feel helpless, exploited, economically weak and culturally humiliated. This cultural experience has its roots at the end of the eighteenth century. Already back then Muslims from countries around the Mediterranean started to feel their culture becoming marginalized by the West.

In response, the viceroy of Egypt, Mohammed Ali, tried to peacefully catch up with Western culture in the nineteenth century. By incorporating Western ideas, science and technology, he helped Egypt thrive as a strong, modern state. But then the big European powers decided it was becoming dangerously strong and independent, and collaborated to impede its progress. Great Britain wanted to weaken Egypt and its parent Ottoman state in order to restore the balance of power.

By the twentieth century, the Ottoman Empire had collapsed under international pressure, and Egypt felt betrayed, humiliated, convinced that the West wanted only one thing: to inflict its will on everyone else.

Similar military and economic setbacks combined with underdevelopment made other Arab countries and their leaders feel that growth was impossible. And when they reached this impasse, many people became desperate and, in the 1970s, they began to turn to the certainty of conservatism and religious fundamentalism. Radicalism was not the immediate response, but the last resort.

> _"I am not of their religion, but I too am a man, and I ought to be treated humanely._ " Mohammed Ali

### 6. We need to create a global tribe which fosters universality, not uniformity. 

One way of dealing with the issue of identity would be to give everyone an equal voice.

But if we do that, we need to avoid creating _uniformity_.

Uniformity is the ugly lack of diversity that results from Western — often American — culture dominating all others, thereby stifling the wide range of different cultures and their artistic, linguistic and intellectual expressions. And so it's only natural that people are worried about globalization: they fear that the beliefs of the strongest will drown out the thoughts and opinions of everyone else.

Even within the West, there are fears that globalization simply means Americanization. In France, for example, the proliferation of American fast food restaurants like McDonald's and the power of Hollywood, Apple and Disney have left many people feeling anxious about their own culture's influence, both at home and around the world.

To balance globalization with local cultural diversity, we need to create a global tribe that respects the universal rights of humanity without conforming to one culture.

This _universality_ is based on the existence of fundamental human rights that may not be denied to anyone for any reason. They include the right to live free of discrimination and persecution, to choose one's life, loves and beliefs freely, without interfering with anyone else's, etc. And if we can move toward universality by emphasizing our commonalities while still maintaining our distinctiveness, we can strengthen human rights all around the world.

We can use the tools of mass media, technology and languages to do so. For example, learning to speak at least three languages (for example, our mother tongue, English and a third that we freely choose) would help forge connections, eliminate misunderstandings and encourage compromise in global interactions. And by learning to share linguistic and cultural references, we can lessen the lethal effects of narrowly defined identities by connecting us to a global tribe composed of all humanity.

### 7. Final summary 

The key message in this book:

**Identity is a multifaceted and fluid concept that is significantly affected by external social and political forces. These outside forces can degrade both personal and cultural identities, which leads to toxic feelings of insecurity and anger on both an individual and social level. To form a more peaceful, inclusive world, we must fight political, economic and cultural standardization, and create a global community in which all identities are welcome.**

Actionable advice:

**Remember that no one religion is inherently cruel.**

Next time you hear someone saying Islam is inherently incompatible with democracy or progressive politics, consider asking him about the history of Christianity. He might be surprised to learn — or to be reminded — that almost every religion has had violent, turbulent periods of extremism.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Better Angels of Our Nature_** **by Steven Pinker**

_The_ _Better_ _Angels_ _of_ _Our_ _Nature_ takes a close look at the history of violence in human society, explaining both our motivations to use violence on certain occasions and the factors that increasingly restrain us from using it — and how these factors have resulted in massive reductions in violence.
---

### Amin Maalouf

Amin Maalouf is a French-Lebanese novelist who has written seven books, including _The Gardens of Light, Leo Africanus_ and _The Rock of Tanios_, which was awarded the Goncourt Prize _._ He was the director of _An-Nahar_, the leading newspaper in Beirut, and lives in Paris.

