---
id: 53cf72f93630650007610000
slug: incognito-en
published_date: 2014-07-22T00:00:00.000+00:00
author: David Eagleman
title: Incognito
subtitle: The Secret Lives of the Brain
main_color: FEEA35
text_color: 756C18
---

# Incognito

_The Secret Lives of the Brain_

**David Eagleman**

Unbeknownst to you, a subconscious part of your brain is constantly whirring away and wielding a tremendous influence on your thoughts, feelings and behavior. _Incognito: The Secret Lives of the Brain_ (2011) is your guide to the other side of your brain, and how it shapes your life.

---
### 1. What’s in it for me: learn what your brain is doing when you’re not paying attention. 

Think of your latest success story. Maybe it was a presentation at work that went really well, or an exam that you aced or even rescuing a cat from a tree.

What would you say if someone told you that actually, most of the credit doesn't really belong to you. And neither does the blame for your failures, for that matter.

This is because at any given moment, the thoughts and feelings you are consciously aware of form a tiny fraction of what's going on in your brain. You merely have the illusion of being fully aware and in conscious control of what's happening in there.

These blinks illustrate what the subconscious part of your mind is doing through neuroscientific studies and examples.

You'll discover

  * why a professional baseball player can hit a fastball only when he's not thinking about it consciously;

  * why a brain tumor caused a 40-year-old man to become obsessed with child pornography; and

  * why to most people, apples seem more appetizing that fecal matter.

> _"If our brains were simple enough to understand them, we wouldn't be smart enough to understand them."_

### 2. Despite what we think, we’re not really in control of our thoughts, feelings and actions. 

Most people assume they're consciously aware of and in control of pretty much all their feelings, actions and thoughts. Astonishingly, neuroscience proves them wrong.

In fact, most of your brain activity stems from purely physical and biological processes of which you're completely unaware, and which you're unable to influence.

Our helplessness can be seen in the way alterations to the brain caused, for example, by accidents and diseases, affect people.

One shocking example was the case of a 40-year-old man whose wife of 20 years suddenly noticed he had — seemingly out of the blue — developed an obsession with child pornography. After a medical examination, it was found he had developed a massive tumor in a part of the brain responsible for decision-making: _the_ _orbitofrontal_ _cortex_. Once the tumor was removed, his sexual appetites returned to normal.

There are also other, more subtle ways in which we lack conscious control over our mental lives, and it turns out this is often for the best: many processes in our brain, like decision-making, actually work best on autopilot, without conscious interference that would slow it down.

For example, if you ask a musician to play a piece, but tell her to focus _only_ on the individual movements of her fingers, she'll find this very difficult. It is far easier for her to focus on the music, and let her fingers play it free from such vigilant, conscious control.

Or what about baseball, where some pitchers can throw a fastball that reaches the batter in just four-tenths of a second. It takes five-tenths of a second for the batter to form a conscious awareness of where the ball is going, so why don't batters always miss fastballs? Because they actually leave the conscious component out of their decision, and respond instinctively, much las you might duck if you saw something come at you quickly.

> _"A professional athlete's goal is to not think."_

### 3. What we think of as reality is merely an subconsciously constructed hallucination. 

Take a look outside. Do you think that what you see is an accurate representation of the world? In fact, what you see is more of a _hallucination_ than reality.

Our visual perception system is not like a video camera: it doesn't faithfully reproduce an image of the world, but instead just interprets the electro-chemical signals that reach the brain. And the same goes for other senses, like hearing or perception of time. From this mix of signals, our brain constructs a reality.

This effect can be seen any time you read a book. Though the words are mere squiggles of black on white, when they are arranged in rows like this, your brain gives them a deeper meaning.

Another example can be found in a bizarre disorder known as _Anton's_ _syndrome_, where people rendered blind by a stroke actually still believe they can see. They hallucinate a visual reality for themselves!

Of course, most of us are unaware of how our brain constructs our reality, because the neuron operations happen quickly and subconsciously. Right now, for example, you're just glad to read and understand the information on this screen, but are blissfully unaware of the constant tiny eye movements your brain is controlling to enable you to do so.

> _"For all we know, we hallucinate all the time."_

### 4. Different parts of the brain battle for control over our behavior. 

Do you think of yourself as having a single, unified personality? Most people do.

But in fact, if we examine our brains more closely, we see that this is an oversimplification: the brain comprises several subsystems that each have a different function, and that often compete for control over our behavior.

For example, we clearly have separate _rational_ and _emotional_ brain systems. As you might guess, the rational system is in charge of coolly and calmly analyzing situations, whereas the emotional system generates feelings like anger, fear, desire and so forth.

Often the two are at odds, but both are necessary for a normal life. For example, if you lacked the emotional system, you would spend all your time overanalyzing the world around you, without being able to make even the simplest decision. Emotions may be swift and irrational, but they help you to quickly make the unimportant decisions needed in daily life.

The understanding that there are several subsystems in your brain competing for control of your behavior helps explain some peculiar phenomena.

For example, have you ever considered how bizarre it is for a person to curse at herself? This odd behavior can only be explained by the division in the brain: one faction of the brain is berating the other.

Or consider how odd it is for a smoker who wishes to quit to nevertheless keep smoking. Clearly, it's possible for one part of the brain to want to quit, while another strongly desires to continue.

### 5. Our thinking patterns and preferences are largely determined by evolution. 

Just like every other part of the human body, the way our brain works is largely determined by evolution.

First, evolution has determined the limits of our cognitive functioning.

The range of thoughts we can think is actually limited to things that were useful for our ancestors. For example, if you try to visualize a five-dimensional cube, you'll find it impossible. This is because the ability to see five-dimensional objects did not provide any evolutionary advantage.

The range of tasks we are adept at is also pretty limited. For instance, we are actually fairly bad at performing large mathematical computations because our ancestors never really needed them as hunter-gatherers. On the other hand, they did need the ability to navigate social problems like detecting and punishing cheaters, so humans today are fairly adept at this.

Second, evolution also guides our preferences in matters of taste and attraction.

Most people find things like apples, eggs and potatoes tasty. Why? Because they contain sugars, proteins and vitamins, all of which were useful for our ancestors' survival.

We also have a strong aversion to even the idea of eating fecal matter. Why? Because it contains harmful microbes that would make us sick, so presumably any ancestors who were drawn to it died out long ago.

If we look at what kind of creatures we're sexually attracted to, this also tends to make evolutionary sense: we're not attracted to frogs, but rather other humans. This is because cross-breeding with other species is not possible, so being attracted to them would be pointless from an evolutionary standpoint.

### 6. Our legal system should focus on rehabilitation, not punishment. 

Do you still think people consciously make decisions to act in certain ways?

This is a particularly pertinent question when it comes to our legal system, where we still vehemently assume that people have free will and hence can be held responsible for their actions.

But, in fact, the very notion that we can definitively and accurately assess blame for criminal acts is doubtful to say the least.

That's because we humans have no say in our genetic heritage or in the way we're educated and socialized in life. Therefore, it seems that the idea of personal responsibility for our actions is in fact senseless. This is especially true when something biological in nature seems to compel someone to criminal acts.

Consider the case of Charles Whitman, an intelligent 25-year-old man who went from being a loving husband to a sniper-wielding mass-murderer in a very short time. After he was killed by the police in a shootout, his body was autopsied and a large tumor was discovered in his brain. It was pressing down on the _amygdala_, an area involved in emotional regulation. Was he to blame for his murderous rampage? Or was the tumor?

Since we can no longer blindly hold criminals responsible for their actions, we need a profound shift in the purpose of the legal system. Rather than blaming and punishing criminals, we need to focus on recognizing their problems and trying to address them.

Quite simply, each and every criminal should be treated as if they had no choice but to behave the way they did. They should be directed to personalized rehabilitation where the goal is to change their behavior to something societally acceptable, while changing the underlying person as little as possible.

### 7. Neuroscience has helped us understand the brain, but cannot explain everything. 

As you've seen in the previous blinks, neuroscience has helped us better understand human behavior, especially its subconscious parts. Happily, we can take advantage of this new understanding in order to, for example, improve some social policies.

For example, we finally understand that for the legal system to be truly just, it needs to be personalized to the circumstances of each criminal — just like their unique biology.

Perhaps we can also better reward virtue in society when our understanding of the brain's various competing subsystems makes it clear what virtue really is: the choice to do the right thing even when some parts of the brain advocate the wrong thing.

However, there are also limits to how much neuroscience can explain.

For example, due to the complexity of the brain and the way its structure is uniquely determined for each individual by interactions between genes and the environment, it is unlikely we'll ever be able to perfectly predict someone's behavior.

What's more, though we can learn much by looking at the individual parts of the brain on a physical level, we may well be missing something by ignoring a more holistic view. Perhaps people are more than the sum of their neurons.

As complexity theorist Stuart Kauffman put it, "A couple in love walking along the banks of the Seine are, in real fact, a couple in love walking along the banks of the Seine, not mere particles in motion."

### 8. Final summary 

The key message in this book:

**What we consciously perceive about the workings of our brain is merely the tip of the iceberg. Subconscious thoughts and processes are really what determine to a large extent our outward behavior.**
---

### David Eagleman

David Eagleman is a neuroscientist and junior professor at Baylor College of Medicine who has written several popular science books, including _Wednesday_ _is_ _Indigo_ _Blue_.

