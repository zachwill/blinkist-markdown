---
id: 56f123bef778930007000007
slug: information-doesnt-want-to-be-free-en
published_date: 2016-03-24T00:00:00.000+00:00
author: Cory Doctorow
title: Information Doesn't Want to Be Free
subtitle: Laws for the Internet Age
main_color: 405199
text_color: 354380
---

# Information Doesn't Want to Be Free

_Laws for the Internet Age_

**Cory Doctorow**

_Information Doesn't Want to Be Free_ (2014) is a guide to copyright laws, censorship and the needs of the modern interconnected world. These blinks explain what ownership means in the digital age and explain why we need to reform our copyright system.

---
### 1. What’s in it for me? Learn why copyright laws need to change. 

The internet, though relatively young, has already completely changed the way we communicate, socialize and learn about the world. It has also opened up entirely new modes of content consumption, be that content music, movies or books. We now live in the world of file-sharing, where paying for music, movies or software might seem outdated, because, usually, it's possible to download it for free.

When attacked, the proponents of file-sharing protest that information _wants_ to be free. But does it, really?

In these blinks, you'll learn

  * why copyright laws need to change;

  * why locking your content up is not the way to go; and

  * how a Pennsylvania school spied on their students.

### 2. Despite the internet, people still want to pay for quality content. 

People often say that the rise of the internet precipitated the downfall of record labels and copyright law. Huge record labels no longer have much say in who emerges as the next big musical star. Stars now rise and fall online, and the internet has its own criteria. It's in this environment that no-name indie bands like the Arctic Monkeys can achieve monumental success.

But how does this work?

Well, the internet has made fundamental changes to how consumers access creative content. If you want to listen to a song or look at a painting, for instance, you no longer need to pay for an album or a museum ticket; you just need to go online. As a result, people are growing accustomed to free access. We stream movies; we listen to music on YouTube; we search for images. 

But, in such an environment, artists must be unable to earn money from their content, right?

Actually, not really. If given the opportunity, people still want to pay for quality content. In fact, since the advent of art, there have always been people willing to pay for it. For instance, long ago, patrons greatly influenced the creative process of artists like Michelangelo. More recently, this power was wielded by major labels like EMI, who paid bands like The Beatles for their music. And today, thanks to the internet, lovers of art can directly pay those making it.

So, the internet is actually advantageous for artists. 

When people are given free creative content, like music on YouTube, a lot of them will check it out, appreciate it and move on without spending a dime. But there will always be a few who want to support and appreciate the artist by paying, especially if paying the artist is easy. If all it takes is typing a song name into the iTunes Store, some consumers will always pay their fair share instead of illegally downloading a lower-quality file.

### 3. Digital locks are not the way to protect your content. 

So, the internet has opened up access to content. Naturally this raises copyright concerns for creators and consumers alike. But how can a content creator make a profit and prevent the pirating and free distribution of their work?

Well, _digital locks_ are one possibility. 

A digital lock is basically an algorithmic way to scramble your content, protecting it until the proper key is entered, at which point the content returns to a readable form. These types of keys are used just about everywhere, from your Kindle to your DVD player. 

The trouble is that digital locks don't really work: any programmer with a few days on their hands could easily crack a key. So, while such locks are widely used by the likes of Amazon and Sony, they're far from secure.

Just take the hacker Muslix64, who cracked the key to a HD-DVD player in a snap. After that, the information on how to play such DVDs on any player spread all over the internet. 

What's worse, the middlemen who create the digital locks can hold your content captive. Once you let a middleman — a distributor, say — lock your content, they control who has access to it.

For instance, Hachette, one of the world's largest book publishers, learned about the power of the middleman the hard way. They allowed a digital lock to be put on all the books they sold on Amazon, but when they turned down an unfair contract renegotiation from Amazon, the reaction was immediate. Mere hours later, none of the publisher's books were available on Kindle. Not even ones from famous authors like J.K. Rowling. What's worse, Hachette basically couldn't do anything but complain. 

Clearly, digitally locking your content isn't very helpful. Furthermore, it can actually be dangerous, too.

### 4. Digital locks enable hackers to install secret spyware and viruses on your computer. 

In the previous blink, we saw just how ineffective digital locks can be. But there's more: they can also expose you to hackers and viruses. That's because digital locks enable companies to install hidden software on your computer. 

For instance, since computers at this point are designed to run essentially any program, the producers of digital-content locks have to prevent computers from running certain programs, like those capable of copying a CD. Accomplishing this requires the secret installation of what's called a _rootkit_, which is essentially a bunch of software, often malicious, that taps into otherwise inaccessible parts of your system. 

Just take Sony BMG who, in 2005, sent out over six million CDs to customers. Unbeknownst to these customers, running this CD installed a program that prevented consumers from copying music off of it.

This program — a rootkit — was an easy target for abuse. Essentially, Sony made their customer's computers vulnerable to hackers and virus producers, because the rootkit provided a gateway for installing malware, which can disable a user's system, and spyware, which surveils customers' computers, stealing passwords and other information. 

However, Sony isn't the only one doing this. Another huge scandal broke in 2009 when a Pennsylvania school gave out 2,300 laptops to its students. Installed on the laptops was hidden software that enabled the camera to turn on and snap photos without the user's knowledge. 

Thousands of unsuspecting students had their pictures taken, but it only became clear what was happening when the school used photographic evidence taken in a student's bedroom to bust him for drug use!

### 5. A war is mounting between censorship and the free internet. 

So, what's the common ground shared by resistance to copyright laws, digital locks and censorship? They all point to one simple truth: people want to be free and want the internet to be as free as they are. But naturally, that won't happen without a fight. 

In fact, over the last few years, the battle between censorship and the free internet has intensified. For instance, digital locks have been broken by internet "pirates," censorship is being protested across the globe, hackers are waging escalating cyber wars against governments and state secrets are being exposed on pages like WikiLeaks. No matter which side you're on, the free internet is a controversial topic. 

So, while multilateral contracts like SOPA and PIPA were designed to reduce internet piracy and protect copyright laws, these contracts have only produced outrage online. Millions of angry citizens called the US senate, and the legislation was repealed. 

In fact, more restrictive copyright laws will only make this "copyfight" worse. If customers feel that copyright restrictions are impinging on their freedom — if, say, videos critical of the government are banned or corporations attempt to control internet content by demanding the removal of copyrighted material — these consumers will join forces and more people will turn to the illegal corners of the internet, like the deep web. 

But censorship happens in other ways, too. For instance, tech companies like Facebook, YouTube and Google are becoming more powerful by the day, increasing the risk for greater censorship in the process. 

How exactly?

Well, most people rely heavily on sites like Facebook, YouTube and Google, which puts tremendous power in the hands of these companies. They literally determine what content you're most likely to see. If you doubt this, ask yourself how often you look past the first ten search results on Google? By controlling the content of the top search results, Google is essentially deciding which content makes it through to you, and, by extension, which companies go broke and which ones succeed.

> _"Information doesn't want to be free — people do."_

### 6. Copyright is important and must be adjusted to the changing world. 

So, the restrictions of copyright laws are a point of contention and conflict. However, the whole idea behind copyright is to protect people against exploitation.

That's why it might be time to adapt copyright to the needs of those it seeks to regulate.

It was once safe to assume that anyone who wanted to reproduce copyrighted material was seeking to make a profit. Therefore, it made perfect sense to require anyone hoping to reproduce something to first obtain the permission of the original producer. 

Today, however, the issue isn't so clear-cut, which raises the question: To whom should copyright laws apply?

We're in the twenty-first century, and everyone copies everything. It's ridiculous to require a ten-year-old fan-fiction writer to seek Warner Bros.'s approval to use characters and storylines from the _Harry Potter_ franchise.

Such laws simply shouldn't apply to the cultural activities of normal people. A more sensible solution would be to limit copyright to industrial regulation.

Furthermore, in so far as it limits our privacy, access to information and freedom of speech, copyright is also an issue of human rights.

Since we do just about everything online — our shopping, our banking, even our dating — the internet has transformed from a luxury into a necessity. 

Limiting access to this necessity by copyrighting content therefore threatens our right to freedom of expression. Just imagine if stories of human rights abuses were copyrighted: we'd be unable to access them and so unable to speak out.

And copyrights can also be used to infringe on our privacy. For instance, a few years back, Viacom sued Google and YouTube for letting users upload videos that had been flagged as private because this prevented Viacom from reviewing the videos for proprietary material. 

Luckily, the judge ruled against Viacom; if he hadn't, however, there would have been huge ramifications. Companies would then have been able to access a user's personal files to check for private material.

> _"Wanting a different copyright isn't the same as not wanting copyright at all."_

### 7. Final summary 

The key message in this book:

**As it functions today, copyright law often enables censorship and is dangerous to content creators and consumers alike. Clearly, the digital age requires a new approach to regulating creative content.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _The Information_** **by James Gleick**

_The Information_ takes us on a journey from primordial soup to the internet to reveal how information has changed the face of human history and shaped the way we think and live today. New technology continues to accelerate the speed at which information is transmitted, and to have lasting consequences for society.
---

### Cory Doctorow

Cory Doctorow is a blogger, technology activist and science-fiction novelist. He formerly served as director of the European Affairs for the Electronic Frontier Foundation, and is a regular contributor to _The New York Times_, _The_ _Guardian_ and _Wired_.

