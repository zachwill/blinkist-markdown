---
id: 55c0c42d3439630007510000
slug: ingenius-en
published_date: 2015-08-05T00:00:00.000+00:00
author: Tina Seelig
title: InGenius
subtitle: A Crash Course on Creativity
main_color: 45BE8D
text_color: 2A7355
---

# InGenius

_A Crash Course on Creativity_

**Tina Seelig**

_InGenius_ (2012) unlocks the secrets to the creativity we all have, whether we know it or not! From new thinking habits to motivating attitudes to environments with incentives, these blinks will guide you toward kindling your own creative spark.

---
### 1. What’s in it for me? Learn how to unlock your hidden creativity 

Vincent Van Gogh, Leonardo Da Vinci, David Hockney: all three must have been born with innate creative brilliance, a rare gift that separates them from the rest of us. Right?

Not so. These blinks show you that creativity isn't something that some of us have and others don't. We can all be creative, we just have to know how! Read on and you'll discover how some of the most innovative companies have unleashed their employees' innovative drive, and how you can do the same to unlock your own creative juices.

In these blinks, you'll discover

  * how one company benefited from making artificial limbs beautiful;

  * why your brainstorming sessions probably aren't working; and

  * why one company gives its employees rubber brains.

### 2. Creativity is a habit you learn, not something you’re born with. 

Creativity: Either you've got it or you don't, right? Well, although we tend to see artists, innovators and entrepreneurs as a different breed, creativity is actually _not_ something you're born with. If you don't consider yourself creative, perhaps your creative spirit simply hasn't been fostered!

Luckily, it's never too late. By putting some new thinking habits in place, you can nurture the creativity you never knew you had. So what are these habits?

The first is to combine seemingly unrelated ideas and create a surprising result. Say you combine an alarm clock and vocabulary flash cards. The result could be an alarm clock that switches off when you answer a set of questions. It may sound like a pretty useless invention, but this kind of product could be just the solution for people who want to learn a new language, but find it hard to motivate themselves to do it.

You can also kindle your creativity by _reframing_ the problems you face. Because we often see things from a limited perspective, we tend to create solutions to problems based on what we _already_ know, or _usually_ think. By actively taking a step outside these constraints, you'll be able to grasp truly innovative solutions.

One way to do this is by removing something from its usual context. For example, you might think of an artificial leg as a functional product, a tool to aid those with disabilities. But what if you took it out of a medical context, and into the context of fashion instead?

The highly successful company Bespoke did just this, by creating artificial limbs that weren't just medical aids, but also fashion accessories. By reframing artificial limbs, they discovered the possibility of producing them in a way that was aesthetically pleasing and also made their wearers feel more comfortable!

We often face problems where the solution is simple, or when it comes to us easily. Does that mean we should simply stop there? Not quite - find out why in the next blink!

### 3. Don’t stick with first ideas – better ones are yet to come! 

Sometimes we face problems where the solution seems obvious. But, just because it's your first idea doesn't mean it's the best — in fact, it's quite the opposite. The most innovative ideas often emerge at the end of a long creative process that involves numerous attempts and continuous questioning, even if things appear to be working well!

The reality is that easy solutions are major obstacles for creativity, and most of the time we don't even realize when we've fallen into their trap. The author often gives her design students the challenge of lining up according to their birth date without talking to one another. Usually students use hand signals and body language to solve the task with moderate success. Yet rarely does anybody rethink the solution and simply pass around a piece of paper so each person can write down their birthday; written communication isn't banned by the rules of the game, and it would be much faster.

So how can you make sure you keep generating and developing your ideas? One way to achieve this is through _brainstorming._ Sure, we probably all know what brainstorming entails, but are we really doing it effectively? A great brainstorming strategy actually has several vital aspects.

First, the space must be right. Seek out a large room where you can make every surface a place to map out ideas. Cover the walls in paper and use sticky notes on the windows.

Next, you'll need to invite the right crowd. A maximum of eight is ideal, and the people in attendance shouldn't only be those at the top of the foodchain. Instead, invite every stakeholder along to capture a range of ideas. Let's say you've created a new app: don't just invite coders and the design guys, welcome the input of customers and the marketing team too!

### 4. Open yourself to inspiration by observing constantly. 

Sometimes it's good to be a novice, as your inexperienced perspective may help you break through the status quo. But generally, the more knowledgeable you are about your field, the more starting points you'll have for creative problem solving.

Remember, every little bit of information counts, so never stop observing. Sometimes, even the most trivial aspects of your environment can inspire fantastic ideas.

David Friedberg, the founder of the Climate Corporation, came up with the idea of insurance for businesses that were affected by weather after noticing that a bike rental shack he passed on his way to work was always closed on rainy days. Would you have noticed a detail like that? If not, it's time to up your observational skills.

Making discerning observations is something we can train ourselves to do. Practice really makes a difference! The author's son was often challenged with surprise quizzes on the details of his surroundings by his grandfather — the color of the walls, the number of chairs in a room or the exact positions of certain objects were all fair game. The author's son was soon able to scan a room for its features with impressive speed.

You can also awaken your observation skills by immersing yourself in different surroundings altogether. Break out of your daily routines to start seeing more. Try taking a new, slightly longer route to work. Or grab your lunch somewhere other than your favourite spot. This may just shake you out of autopilot mode and open your eyes to new ideas.

### 5. Embrace failure as the next step towards your solution. 

Sure, none of us enjoy failure. But don't we learn the most from our biggest mistakes? They can even inspire us to create radical change in our lives. Failure is inevitable, so why waste time worrying about it? Instead, start looking forward to trying again. And again. And again!

Think of it like a science experiment. In order to test whether your hypothesis is true, you'll need to test it. If the results prove you wrong, you'll need to reformulate your theory, and try once more. Thomas Edison tried thousands of different materials for the glowing filament in his light bulb until he found the right one that soon lit up the world.

Creativity works in much the same way — your failures will always lead you to a better alternative. So don't underestimate the value of your first experiment's results. 

Did you know Instagram began as an app that allowed you to share your location? It was hardly a hit, but a few users liked one experimental feature that allowed you to share pictures. This feedback ultimately lead to the creation of the world's top photosharing app. So, consider even the least promising ideas — they might be the "Eureka!" of your next experiment.

Of course, if you want to experiment, you'll need an environment where you feel comfortable taking risks. 1185 Design, a company that creates branding materials, lets a number of designers work on a single project at the same time, but independently of each other. This means there's a lot of experimentation happening all at once. So how do they make it work?

From an early stage of each project, the team comes together regularly to compare and discuss results. In this way, countless ideas are shared and considered equally, in a space where each individual's contribution is valued.

Even if you value other people's input, it's sometimes a struggle to value your _own_ contribution. We all find it difficult to remain confident during our creative process. Read on in the next blink to learn how you can keep faith in yourself.

### 6. Believe you’ll find a solution, and you will. 

Creativity can be a long, hard road with many setbacks along the way. If you want to keep at it, you'll need the right attitude to fuel yourself. So what is the right attitude? Well, it's as simple as this: If you believe that you'll reach success, you absolutely will.

Take the case of neurosurgeon John Adler. In the mid 1980s, radiation therapy emerged that was used to treat tumors, but only those in the brain. Adler was convinced that the therapy could be useful in other regions of the body too, and set out to make it possible.

Despite constant setbacks and skepticism from the wider medical community, Adler persevered until he reached a solution. Ultimately, he was responsible for developing a robotic radiation surgery system that has become a vital weapon in the fight against several forms of cancer.

It's all well and good to decide not to give up on your creative journey, but it's difficult to stay true to that promise. One way you can hold on to the right attitude is by embedding self-belief in your own language.

For example, simply naming your endeavour as "creative" can make a world of difference. Randi Zuckerberg, leader of consumer marketing at Facebook, decided to rename the department "creative marketing." Within days, the team began acting with a more creative mindset. They easily outstripped their previous performance, all because they hadn't realized that creativity was part of their job before the name change!

Facebook's creative marketing department also saw a shift in their work style and space. This is no surprise, considering the huge impact your environment has on your creativity. Read on to find out how you can make your workplace the ideal space for innovative thinking.

> _"Whether you think you can or you can't, you're right." - Henry Ford_

### 7. The space you work in fundamentally shapes the way you work and collaborate with others. 

Wouldn't it be great to have as many great ideas at your desk as you do in the shower? While you can't bring the steaming hot water to your workspace, there are ways to ensure your environment is stimulating and inspiring.

Begin by appreciating that how we feel, think and act is profoundly shaped by the space we're in. We feel and think differently whether we find ourselves in a concert hall, at a doctor's office, in a lecture hall or at an airport.

Companies use this knowledge to shape their employees' work spaces in ways that promote creativity and unconventional thinking. Many forward-thinking offices even make use of foosball and pool tables to create a playful atmosphere that, in turn, generates playful ideas.

Workspaces that encourage interaction and collaboration between employees foster creative problem solving. The author tested this in an experiment where teams were challenged to solve three jigsaw puzzles for which they each had a quarter of the required parts.

In one room, each team had a table but no chairs. In another room, there were chairs for everyone but no tables. The teams with tables cooperated less and failed to exchange their pieces effectively. In contrast, the teams with only chairs quickly merged and solved all of the puzzles together in a much shorter time!

What could be changed in your workspace? Even shifting furniture around to support whatever activity or teamwork is taking place can make a world of difference. Such a simple step can turn a traditional office into a creative lab in the blink of an eye!

### 8. Make creativity a game. 

Admit it: we all love a good game! No matter what it is, if we're challenged to reach a goal, often in a competitive setting while following a set of rules, we'll be motivated for the reward that's promised at the end. We can stimulate our creativity in this way too, with new incentives to get those ideas flowing.

For instance, the company Proteus Biomedical rewards employees who file for a patent with rubber brains that are displayed in jars on their desks. This makes social recognition a reward for creative activity, and is called _gamification_, since it resembles the immediate reward mechanisms that make video games so enjoyable.

Constraints on time and resources combined with an element of competition can also drive us to be innovative. If your creative thinking has hit a wall, try creating scenarios with limited possibilities to stimulate your problem-solving brain.

For instance, time pressure can work wonders for our work performance. The author used to give her design students a big project to work on for the whole semester, and invariably the students would slack off until the last few weeks. After swapping this for three assignments, with two weeks to complete each one, the author saw students work with far greater enthusiasm to produce more creative results, even though it tripled the workload.

Limited resources can also inspire inventive ideas. Remember that scene in _Monty Python and the Holy Grail_ where you hear horses approaching only to find that it's nothing more than soldiers clapping coconut halves together? That ingenious joke has become iconic, but it was simply borne out of a lack of funds for real horses!

So, next time your innovative endeavour faces a problem or comes to a halt, why not welcome it as an extra challenge to your creative capacities? Your own ingenuity might surprise you.

### 9. Final summary 

The key message in this book:

**Everyone has the potential to develop a great creative mind. All it takes is a little nurturing! By developing creative thinking habits, a positive attitude and a stimulating environment, you'll find yourself full to the brim with innovative ideas.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _ReWork_** **by Jason Fried & David Heinemeier Hansson**

_Rework_ throws out the traditional notions of what it takes to run a business and offers a collection of unorthodox advice, ranging from productivity to communication and product development.

These lessons are based on the authors' own experiences in building, running and growing their company to a point where it generates millions of dollars in profits annually.
---

### Tina Seelig

Tina Seelig is a Gordon Prize-winning author and academic with a PhD in Neuroscience from Stanford University. She teaches a course in Management Science and Engineering at the University and has written the book _What I Wish I Knew When I Was 20._

