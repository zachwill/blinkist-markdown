---
id: 59b7f676b238e10005c1fb09
slug: inheritance-en
published_date: 2017-09-14T00:00:00.000+00:00
author: Sharon Moalem, MD
title: Inheritance
subtitle: How Our Genes Change Our Lives, and Our Lives Change Our Genes
main_color: 71D957
text_color: 3C732E
---

# Inheritance

_How Our Genes Change Our Lives, and Our Lives Change Our Genes_

**Sharon Moalem, MD**

_Inheritance_ (2014) is proof that you don't need to be a scientist to understand the importance of your genetics. These blinks explain how your DNA affects your everyday life, from how you look and what you eat to how susceptible you are to things like anxiety and disease. So arm yourself with knowledge, and discover more about the genes that make you who you are.

---
### 1. What’s in it for me? Find out how your genes influence you – and how you influence your genes. 

The science of genetics has captured the public imagination. Perhaps not since the 1950s, when Watson and Crick derived the double-helical chemical structure of DNA, has public interest been so fervid. A common legacy, genes are now also a scientific lodestar; understanding what we've inherited from the past seems to promise a new and exciting future. Indeed, we hope that deciphering the human genome will expand humanity's horizons in fantastic and unimaginable ways.

Currently, genes define what we are now. When we admire an exceptional athlete's physical ability, we ascribe it to her genes. When we marvel at a genius's mental capacity, we ascribe it to her genes. Strength, intelligence, beauty — genes determine whether you have these qualities or not.

But that's not the whole story. Our genes don't only influence who we are; we also have great influence over our genes.

In these blinks, you'll find out not only how your genes determine your looks, your sex and even your diet but how your genes change with your actions and behavior, and even with the actions of others. In short, you'll learn how your genes influence you and how you influence your genes.

You'll also learn

  * why Ötzi the caveman didn't like milk;

  * why identical twins may look very different; and

  * why smoking spoils the pleasure of your morning coffee.

### 2. A person’s looks tell us about her genes and a person’s genes give some indication of her looks. 

If you meet someone attractive, complimenting his or her genetics probably won't make the best first impression. But if you have an eye for detail, you can determine a good amount of genetic information just by looking at someone.

The human eyes alone offer information on a number of genetic conditions.

One genetic condition related to the eyes is called Fanconi anemia, which results in a person having eyes that are extremely close together. Such people are also more likely to suffer from seizures. (But let's not get too cocky with our genetic detective work: If we were to isolate what influences the amount of spacing between the eyes, we would still be looking at over four hundred genetic combinations.)

The shape of a person's eyes can also hint at genetic conditions. One of the common signs of Down syndrome is that it places the outer corner of a person's eyes in a higher position than the inner corner.

Genetics also affects the color of your eyes, and sometimes this can result in color pigments getting an uneven distribution. Other times it can result in _heterochromia_, a genetic condition that gives someone one eye that is a different color than the other. The actress Demi Moore has this condition: her left eye is green, her right eye is hazel.

Since the link between genetics and appearance is a two-way street, it works the other way around as well: knowledge about someone's DNA can help you determine a good deal about how they look.

A great example of this is the case of Ötzi, the Stone Age man: In 1991, a mummy over five thousand years old was discovered in northern Italy's Ötztal Alps.

By analyzing just one piece of the mummy's left hip, scientists could determine that Ötzi was likely a light-skinned man with brown eyes and an ancestor of today's Corsicans. But that's not all. They could also surmise that Ötzi was lactose intolerant, had type O blood and was at high risk of dying from cardiovascular disease.

### 3. We can’t predict the health or appearance of our children. 

While a sample of someone's genes can tell a pretty detailed story, there are limits to our powers of speculation, especially when it comes to passing our genes down to the next generation. For instance, even if we know the entire DNA code of a beautiful and healthy couple, we can't accurately predict how healthy or good-looking their children will be.

One major reason for this is the passive bad genes we sometimes carry: they might not affect us, but there's no guarantee that they'll remain passive in our children.

A Danish sperm donor named Ralph is an extreme case in point. Tall, blond and healthy, Ralph was considered a prime donor, a man in possession of enviable genes, and in the end he became the biological father of more than 40 children around the world.

What they didn't know was that Ralph held a passive gene for the genetic disorder known as neurofibromatosis. As a result, many of these children became sick, developing tumors, and suffering from learning difficulties, blindness and epilepsy.

Due to a phenomenon known as _variable genetic expressivity_, there's no telling whether a gene will be passive or dominant.

In fact, even twins sharing the exact same DNA can end up with different appearances and ailments.

Adam and Neil are identical twins, and even though they both carry the gene that causes neurofibromatosis, they suffer from completely different problems: Adam's face is so badly disfigured that some people mistake it for a mask. Meanwhile, his brother Neil is so good-looking that he draws comparisons to Tom Cruise, though he also suffers from seizures and memory loss.

Again, all these differences come down to variable genetic expressivity, and it shows that even when we start out with the same DNA, the end results can be wildly different.

As we'll see in the next blink, there are many other widely held beliefs that the field of genetics has come to challenge.

> _"Your body is using each gene you carry like a musician uses an instrument. It can be played loudly and softly."_

### 4. Our genes give some indication of our sex, but modern genetics knows more than just two sexes. 

Many of us were taught in biology class that there are two sexes, male and female, and that there are simple characteristics that tell us which is which. However, once you apply the science of genetics, things aren't so simple.

Those typical male or female characteristics come from someone having certain chromosomes, which are essentially a packaged set of genes.

Traditional biology textbooks tell us that the world is divided into two groups: women, who have two X chromosomes, and men, who have one X and one Y chromosome. The Y chromosome also carries the SRY gene, which triggers the development of the male sex organs and hormones responsible for increased body hair.

However, modern genetics has found that there is room for more than just two sexes.

By taking an ever closer look at human biology, scientists have found a nearly infinite number of genetic variables influencing every aspect of our sex, from the shape of a clitoris or penis to how deep someone's voice is.

And with all these variables, it's entirely possible for someone to be biologically both male and female.

This is exactly what happened with Ethan. Born with two X chromosomes, Ethan nevertheless developed as a typical boy would.

This happened because, even though there wasn't a Y chromosome carrying an SRY gene, Ethan still had what are known as SOX genes, which are also responsible for sex determination. In Ethan's case, one of the genes became active twice during development and essentially mimicked the SRY gene, causing Ethan's body to develop male sex organs despite having two X chromosomes.

Clearly, the traditional view of male and female doesn't do justice to the complexities of human beings and their genetics.

### 5. Which food is best for you depends on both your genes and your behavior. 

When you go shopping, do you pay attention to the nutritional information on the packaging? Well, perhaps you should, because if you have certain genetic conditions, this information can actually save your life.

Everyone has genes that determine how well our body will react to certain foods.

In the Middle Ages, many sailors suffered badly because they didn't have access to vitamin C. This led to scurvy, a vitamin-C deficiency that results in bleeding gums and bruising, against which they eventually defended themselves by stocking up on foods rich in vitamin C, like limes.

While most sailors and pirates needed a lot of limes to stay scurvy-free, some of them had a variation on the gene known as SLC23A1. These lucky few were highly efficient in metabolizing vitamin C, and only needed a few limes to stay healthy on the high seas.

Genetics also determines how good coffee is at waking you up in the morning.

How you metabolize caffeine depends on the gene CYP1A2. If you have only one copy of the gene, you'll metabolize caffeine quite slowly and a small cup of espresso will give you an instant kick. But If you have two copies of the gene, you'll metabolize it quickly and a big cup might still leave you yawning.

Nicotine also interacts with your CYP1A2 gene. If you're a frequent smoker, this gene gets activated and ends up making your morning brew less effective.

Certain genes can even make typically healthy foods harmful. Take the example of Jeff, a young New York City chef who became seriously ill after changing his diet from being focused on meat and dairy to being based around fruits and vegetables.

Jeff found out the hard way that he suffers from _hereditary fructose intolerance_, a genetic condition that makes him unable to metabolize fruit. As a result, foods typically considered healthy can actually damage his liver.

Other people have what's called _ornithine transcarbamylase deficiency_ — or an OTC deficiency, for short. With this genetic condition, eating high-protein foods can cause a range of unpleasant symptoms such as leg pain, nausea or hyperactivity.

### 6. Your genes can prepare you to become an athlete and respond to physical training. 

If you play a lot of sports, you might know the frustration some people feel at having to work day and night to stay at the top of their game while others seem naturally gifted. Well, this is another result of having the right genes.

Many of the qualities that are shared by the best athletes are influenced by genetics. The overall size of basketball star Shaquille O'Neal or the arm span of Michael Phelps are just two examples.

In some cases, a genetic condition can be an advantage. The world-famous Finnish skier, Eero Mäntyranta, had _primary familial and congenital polycythemia_, a condition that results in a higher red blood cell count. And when your body produces more red blood cells, it helps circulate oxygen and increases your endurance.

Your threshold for pain endurance is also influenced by genetics. In the most extreme cases, someone can have a mutation in the SCN9A gene, which transmits pain signals to the brain, and not feel any pain at all.

But training also changes the behavior of your genes and how they affect your body.

With rigorous exercise, your bones can grow in order to support the increasing muscle weight. If they didn't, tennis players like Rafael Nadal would be constantly breaking bones in their dominant arms.

When people put on weight, their genes tell their body and their bones to adapt; this is why we see many more thin people suffering from fragile and broken bones than obese people.

So, without proper exercise, your skeleton can suffer in the same way astronauts suffer from bone related issues after spending months in a zero-gravity atmosphere. In this environment, the genes that are responsible for the removal and replacement of bone cells get confused; the body thinks that some bone cells are no longer needed, and removal begins outpacing cell replacement, which ends up weakening the bones.

In the next blink, we'll take a closer look at what can change your genes.

### 7. There are many factors that can alter your genes, including your own actions and external forces. 

It's not unusual for people to blame their behavior on their genes. But what many of us don't know is that our genes are also affected by our behavior.

In fact, many daily activities can impact our genes and change them, for better or for worse.

Air travel can expose your body to harmful radiation that damages your DNA, and lying in the sun or drinking alcohol can also cause genetic mutations, all of which can lead to cancer.

But you can also change your genes in a good way, and a simple way to do that is to eat the right food.

Spinach is a perfect source of multiple vitamin-E variants that not only improve your genetic code but also protect cells from harmful free radicals, the atoms and molecules that damage your cells.

Unfortunately, not everything is under our control; the actions of others can also impact your genes.

There's even a category for all the changes that take place in our genes due to environmental influences: it's called epigenetics.

Every event in your life is picked up on and registered in your genes. If you were bullied at school, your body may have turned off a gene called SERT, which can cause a dulled reaction to stress for the rest of your life.

The events that can leave a mark in your DNA can occur before you're even born. For instance, mothers who were pregnant and traumatized during the events of 9/11 later gave birth to children with an increased risk of suffering from anxiety.

In addition, scientists in Zurich traumatized newly born mice by repeatedly separating them from their mothers. They discovered that as they grew up, the mice were more likely to give up rather than fight in critical situations.

But more remarkable is that, two generations later, the offspring of those mice were still showing the same type of behavior. The scientists found that the trauma caused a change in two genes called Mecp2 and Crfr2, both of which are also found in humans.

### 8. Knowing your genome helps others – and you, too. 

As we've seen in these blinks, there's a lot of information in your DNA, and having a better understanding of this information has a definite value.

But this knowledge doesn't just benefit you and your health; there are companies that want to benefit financially by knowing more about your DNA, too.

The tobacco industry is eager to identify the genes that cause addiction. That way, they could argue that these genes — and not the tobacco — are the real danger to people.

The Burlington Northern Santa Fe railroad company was also eager to blame DNA for damage done. After workers filed claims of being injured on the job, the company, hoping to dismiss the claims by finding a marker in their DNA that would show that the workers were genetically prone to injuries, sought to obtain worker blood samples. Luckily, the court ruled against these tests, calling them unlawful and discriminatory.

Naturally, insurance companies are also very keen on your genes. In the United States, they're allowed to calculate your premiums based on this information. So, even if you live a healthy lifestyle, you might end up paying a lot if you inherited the wrong genes.

But this information can also help you prevent diseases and live a longer, healthier life.

By knowing your genetic makeup, you can find out if you're predisposed to cancer. One sign is to find out if you have a specific mutation in the BRCA1 gene.

With this knowledge you can become a _previvor_, someone who uses the knowledge that they're at a high risk and takes action to stop the tumors from forming and repair the DNA.

This is what Angelina Jolie did when she found out she had a BRCA1 mutation. Her doctors told her that she had an 87 percent likelihood of developing breast cancer and a 50 percent risk of ovarian cancer. To prevent the cancer from ever forming, she decided to have both her breasts removed.

There are simple things you can do to prevent your genes from mutating and developing diseases, even if you find out you've inherited a particular gene variant. Avoid excessive exposure to the sun, fly only when necessary and eat healthily.

### 9. Final summary 

The key message in this book:

**Our genes can tell us a lot about who we are, but there are limits. Not only can the same gene act differently in different people; external influences, even the smallest daily actions, can also have a physical impact on your genome. By learning more about this intricate relationship, and making the appropriate lifestyle changes, you can make sure your genes stay undamaged.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Gene_** **by Siddhartha Mukherjee**

_The Gene_ (2016) offers an in-depth look at the history of genetics. These blinks take you on a journey from the field's humble beginnings to its modern day applications in diagnosing illnesses, debunking racist claims and creating genetically modified life.
---

### Sharon Moalem, MD

Sharon Moalem MD, PhD, is a scientist, physician and author who specializes in showing the world how evolution, genetics, biology and medicine work together. His books, including _Survival of the Sickest_ and _How Sex Works_, have been translated into more than 20 languages.

